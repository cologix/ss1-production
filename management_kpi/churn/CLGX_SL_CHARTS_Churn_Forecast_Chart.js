nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Churns_Chart.js
//	Script Name:	CLGX_SL_CHARTS_Churns_Chart
//	Script Id:		customscript_clgx_sl_chrt_churn_chrt
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		01/07/2014
//	Includes:		CLGX_LIB_Global.js, moment.min.js
//-------------------------------------------------------------------------------------------------

function suitelet_charts_churn_chart (request, response){
    try {
        var marketid = request.getParameter('marketid');
        var repid = request.getParameter('repid');
        var xcs = request.getParameter('xcs');
        var terms = request.getParameter('terms');

        if(marketid != '' ){
            var objFile = nlapiLoadFile(1423930);
            var html = objFile.getValue();

            //	html = html.replace(new RegExp('{arraySerSO}','g'), getSeries(marketid, xcs=1, terms));
            //	html = html.replace(new RegExp('{arraySerMTM}','g'), getSeries(marketid, xcs, terms=1));
            //html = html.replace(new RegExp('{arraySerSO}','g'), getSeries(marketid, xcs=1, terms));

            // html = html.replace(new RegExp('{arraySerSOF}','g'), getSeries(marketid, xcs=0, terms));
            html = html.replace(new RegExp('{title}','g'),clgx_return_market_name(marketid));
            html = html.replace(new RegExp('{marketid}','g'), marketid);
            html = html.replace(new RegExp('{series}','g'), getSeries(marketid, xcs, terms));


        }
        else{
            var html = 'Please select a market from the left panel.';
        }
        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function getSeries (marketid, xcs, terms){



    var stReturn = '[';

    var arrFilters = new Array();
    var arrColumns = new Array();

    //if (marketid!='0')
   // {
    //    arrFilters[0] = new nlobjSearchFilter('custrecord_clgx_churn_location',null,'anyof',arrLocation);
   // }

    if(marketid != '0'){
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_churn_location",null,"anyof",clgx_return_child_locations_of_marketid (parseInt(marketid))));
    }
    var searchSales = nlapiSearchRecord('customrecord_cologix_churn', 'customsearch_clgx_chrt_churnjax1_2', arrFilters, arrColumns);


    var searchSale = searchSales[0];
    var columns = searchSale.getAllColumns();
    var col0=searchSale.getValue(columns[0]);
    if(col0=='')
    {
        col0=0;
    }
    var col1=searchSale.getValue(columns[1]);
    if(col1=='')
    {
        col1=0;
    }
    var col1=searchSale.getValue(columns[1]);
    if(col1=='')
    {
        col1=0;
    }
    var col2=searchSale.getValue(columns[2]);
    if(col2=='')
    {
        col2=0;
    }
    var col3=searchSale.getValue(columns[3]);
    if(col3=='')
    {
        col3=0;
    }
    var col4=searchSale.getValue(columns[4]);
    if(col4=='')
    {
        col4=0;
    }
    var col5=searchSale.getValue(columns[5]);
    if(col5=='')
    {
        col5=0;
    }
    var col6=searchSale.getValue(columns[6]);
    if(col6=='')
    {
        col6=0;
    }
    var col7=searchSale.getValue(columns[7]);
    if(col7=='')
    {
        col7=0;
    }
    var col8=searchSale.getValue(columns[8]);
    if(col8=='')
    {
        col8=0;
    }
    var col9=searchSale.getValue(columns[9]);
    if(col9=='')
    {
        col9=0;
    }
    var col10=searchSale.getValue(columns[10]);
    if(col10=='')
    {
        col10=0;
    }
    var col11=searchSale.getValue(columns[11]);
    if(col11=='')
    {
        col11=0;
    }
    var col12=searchSale.getValue(columns[12]);
    if(col12=='')
    {
        col12=0;
    }
    var col13=searchSale.getValue(columns[13]);
    if(col13=='')
    {
        col13=0;
    }
    var col14=searchSale.getValue(columns[14]);
    if(col14=='')
    {
        col14=0;
    }
    var col15=searchSale.getValue(columns[15]);
    if(col15=='')
    {
        col15=0;
    }
    var lastMonthRisk = parseFloat(col0)+parseFloat(col1);
    var currentMonthRisk = col1;
    var nextMonthRisk = col2;
    var afterMonthRisk = col3;

    var lastMonthExpected = parseFloat(col4)+parseFloat(col5);
    var currentMonthExpected = col5;
    var nextMonthExpected = col6;
    var afterMonthExpected = col7;

    var lastMonthNoticed = parseFloat(col8)+parseFloat(col9);
    var currentMonthNoticed = col9;
    var nextMonthNoticed = col10;
    var afterMonthNoticed = col11;
    //Closed status
    var lastMonthClosed = parseFloat(col13);
    var currentMonthClosed =col13;
    var nextMonthClosed = col14;
    var afterMonthClosed = col15;

    if(currentMonthRisk=='')
    {
        currentMonthRisk=0;
    }
    if(nextMonthRisk=='')
    {
        nextMonthRisk=0;
    }
    if(afterMonthRisk=='')
    {
        afterMonthRisk=0;
    }
    if(lastMonthExpected=='')
    {
        lastMonthExpected=0;
    }
    if(currentMonthExpected=='')
    {
        currentMonthExpected=0;
    }
    if(nextMonthExpected=='')
    {
        nextMonthExpected=0;
    }
    if(afterMonthExpected=='')
    {
        afterMonthExpected=0;
    }
    if(lastMonthNoticed=='')
    {
        lastMonthNoticed=0;
    }
    if(currentMonthNoticed=='')
    {
        currentMonthNoticed=0;
    }
    if(nextMonthNoticed=='')
    {
        nextMonthNoticed=0;
    }
    if(afterMonthNoticed=='')
    {
        afterMonthNoticed=0;
    }

    stReturn += '\n{name: \'At Risk\',type: \'column\',  data: \n[\n';
    stReturn += '{y:' + lastMonthRisk + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=1&count=0&type=At Risk&market=' + clgx_return_market_name(marketid) + '&month=prev\'},\n';
    //stReturn += '{y:' + currentMonthRisk + ',url:\'/app/site/hosting/scriptlet.nl?script=317&status=1&deploy=1&type=At Risk&market=' + clgx_return_market_name(marketid) + '&month=curr\'},\n';
    stReturn += '{y:' + nextMonthRisk + ',url:\'/app/site/hosting/scriptlet.nl?script=317&status=1&count=0&deploy=1&type=At Risk&market=' + clgx_return_market_name(marketid) + '&month=next\'},\n';
    stReturn += '{y:' + afterMonthRisk + ',url:\'/app/site/hosting/scriptlet.nl?script=317&status=1&count=0&deploy=1&type=At Risk&market=' + clgx_return_market_name(marketid) + '&month=after\'},\n';

    stReturn += ']\n';
    stReturn += '},\n';

    stReturn += '\n{name: \'Expected\',type: \'column\', data: \n[\n';
    stReturn += '{y:' + lastMonthExpected + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=2&count=0&type=Expected&market=' + clgx_return_market_name(marketid) + '&month=prev\'},\n';
    //stReturn += '{y:' + currentMonthExpected + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=2&type=Expected&market=' + clgx_return_market_name(marketid) + '&month=curr\'},\n';
    stReturn += '{y:' + nextMonthExpected + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=2&count=0&type=Expectedk&market=' + clgx_return_market_name(marketid) + '&month=next\'},\n';
    stReturn += '{y:' + afterMonthExpected + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=2&count=0&type=Expected&market=' + clgx_return_market_name(marketid) + '&month=after\'},\n';

    stReturn += ']\n';
    stReturn += '},\n';

    stReturn += '\n{name: \'Noticed\',type: \'column\', data: \n[\n';
    stReturn += '{y:' + lastMonthNoticed + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=3&count=0&type=Noticed&market=' + clgx_return_market_name(marketid) + '&month=prev\'},\n';
    //stReturn += '{y:' + currentMonthNoticed + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=3&type=Noticed&market=' + clgx_return_market_name(marketid) + '&month=curr\'},\n';
    stReturn += '{y:' + nextMonthNoticed + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=3&count=0&type=Noticed&market=' + clgx_return_market_name(marketid) + '&month=next\'},\n';
    stReturn += '{y:' + afterMonthNoticed + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=3&count=0&type=Noticed&market=' + clgx_return_market_name(marketid) + '&month=after\'},\n';

    stReturn += ']\n';
    stReturn += '},\n';

    stReturn += '\n{name: \'Closed\',type: \'column\', data: \n[\n';
    stReturn += '{y:' + lastMonthClosed  + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=4&count=0&type=Closed&market=' + clgx_return_market_name(marketid) + '&month=prev\'},\n';
    //stReturn += '{y:' + currentMonthClosed + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=3&type=Noticed&market=' + clgx_return_market_name(marketid) + '&month=curr\'},\n';
    stReturn += '{y:' + nextMonthClosed + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=4&count=0&type=Closed&market=' + clgx_return_market_name(marketid) + '&month=next\'},\n';
    stReturn += '{y:' + afterMonthClosed + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=4&count=0&type=Closed&market=' + clgx_return_market_name(marketid) + '&month=after\'},\n';

    stReturn += ']\n';
    stReturn += '},\n';


    var arrFiltersCount = new Array();
    var arrColumnsCount = new Array();

    if(marketid != '0'){
        arrFiltersCount.push(new nlobjSearchFilter("custrecord_clgx_churn_location",null,"anyof",clgx_return_child_locations_of_marketid (parseInt(marketid))));
    }
    var searchSalesCount = nlapiSearchRecord('customrecord_cologix_churn', 'customsearch_clg_search_countactivechuj', arrFiltersCount, arrColumnsCount);
    var searchSaleCount = searchSalesCount[0];
    var columnsCount = searchSaleCount.getAllColumns();
    var lastMonthCountChurn = parseFloat(searchSaleCount.getValue(columnsCount[0]))+parseFloat(searchSaleCount.getValue(columnsCount[1]));
    var currentMonthCountChurn = searchSaleCount.getValue(columnsCount[1]);
    var nextMonthCountChurn = searchSaleCount.getValue(columnsCount[2]);
    var afterMonthCountChurn = searchSaleCount.getValue(columnsCount[3]);
    if(lastMonthCountChurn=='')
    {
        lastMonthCountChurn=0;
    }
    if(currentMonthCountChurn=='')
    {
        currentMonthCountChurn=0;
    }
    if(nextMonthCountChurn=='')
    {
        nextMonthCountChurn=0;
    }
    if(afterMonthCountChurn=='')
    {
        afterMonthCountChurn=0;
    }

    stReturn += '\n{name: \'Count of Churn Records\',type: \'scatter\',yAxis: 1, color: \'#000000\',data: [\n';



    stReturn += '{y:' + lastMonthCountChurn + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=0&count=1&type=All&market=' + clgx_return_market_name(marketid) + '&month=prev\'},\n';
    //stReturn += '{y:' + currentMonthCountChurn + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=0&type=All&market=' + clgx_return_market_name(marketid) + '&month=curr\'},\n';
    stReturn += '{y:' + nextMonthCountChurn + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=0&count=1&type=All&market=' + clgx_return_market_name(marketid) + '&month=next\'},\n';
    stReturn += '{y:' + afterMonthCountChurn + ',url:\'/app/site/hosting/scriptlet.nl?script=317&deploy=1&status=0&count=1&type=All&market=' + clgx_return_market_name(marketid) + '&month=after\'},\n';
    var strLen = stReturn.length;
    stReturn = stReturn.slice(0,strLen-1);

    stReturn += ']\n';
    stReturn += '}';
    stReturn += ']\n';

    return stReturn;
}




function returnMonthsCategories (){
    var mom = new moment();
    var categories = '[';
    categories += '\'' + mom.add('months', 0).format('M.YYYY') + '\',';
    categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\',';
    categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\'';
    categories += ']';
    return categories;
}