//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Revenue_Grid.js
//	Script Name:	CLGX_SL_CHARTS_Revenue_Grid
//	Script Id:		customscript_clgx_sl_chrt_revenue
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		05/21/2014
//-------------------------------------------------------------------------------------------------

function suitelet_revenue_details (request, response){
    try {
        var userid = nlapiGetUser();
        var roleid = nlapiGetRole();
        var market = request.getParameter('marketid');
        var month = request.getParameter('month');
        if(market!=0)
        {
            if(market=='25')
            {
                var title = 'Montreal';
            }
            if(market=='23')
            {
                var title = 'Toronto';
            }
            if(market=='26')
            {
                var title = 'Vancouver';
            }
            if(market=='22')
            {
                var title = 'Minneapolis';
            }
            if(market=='29')
            {
                var title = 'Jacksonville';
            }
            if(market=='20')
            {
                var title = 'Dallas';
            }
            if(market=='33')
            {
                var title = 'Columbus';
            }
        }


        var objFile = nlapiLoadFile(1213623);
        var html = objFile.getValue();
        html = html.replace(new RegExp('{churnsJSON}','g'), dataChurns(market));
        html = html.replace(new RegExp('{title}','g'), title);

        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function dataChurns (market) {


    var arrFilters = new Array();

    if(market!=0)
    {
        if(market=='25')
        {
            //Montreal
            var marketlist=["5", "8", "9", "10", "11", "12", "27"];

        }
        if(market=='23')
        {
            //Toronto
            var marketlist=["6", "13", "15"];

        }
        if(market=='26')
        {
            //Vancouver
            var marketlist=["28", "7"];

        }
        if(market=='22')
        {
            //Minneapolis
            var marketlist=["16", "35"];
        }
        if(market=='29')
        {
            //Jacksonville
            var marketlist=["31"];
        }
        if(market=='20')
        {
            //Dallas
            var marketlist=["2", "17"];
        }
        if(market=='33')
        {
            //Columbus
            var marketlist=["38", "34"];
        }
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",marketlist));
    }
    var arrColumns = new Array();
    var arrFilt=new Array();
    var arrCol=new Array();
    //Search for SOs within this month
    var searchSOs = nlapiSearchRecord('transaction', 'customsearch_clgx_so_noinvgrid', arrFilters, arrColumns);

    var stReturn = 'var myData =  [';

    for ( var i = 0;searchSOs != null && i < searchSOs.length; i++ ) {
        var searchSO = searchSOs[i];
        var columns = searchSO.getAllColumns();

        var customerID=searchSO.getValue(columns[0]);
        arrFilt[0] = new nlobjSearchFilter('internalid','customermain','anyof',customerID);
        //take the customers one by one and check if them have at least one invoice for las month.
        //if the customer doesn't have an invoice the Interconnection, Network, Other Recurring, Remote Hands, Virtual Interconnection, Space, Power item amounts will be considered as Revenue
        var searchInvoices = nlapiSearchRecord('transaction', 'customsearch_clgx_search_invrevenue', arrFilt, arrCol);
        if(searchInvoices==null)
        {
         stReturn += '\n["' + i +
            '","' +  searchSO.getValue(columns[4]) +
            '",' +  searchSO.getValue(columns[5]) +
            ', "' +  searchSO.getText(columns[1]) +
            '", ' +  searchSO.getValue(columns[1]) +
            ',' +  searchSO.getValue(columns[2]) +
            ', "' + searchSO.getText(columns[3]) +
            '" ],';

        var strLen = stReturn.length;
        }

    }
    if (searchSOs != null){
        stReturn = stReturn.slice(0,strLen-1);
    }
    stReturn += '];';
    return stReturn;
}