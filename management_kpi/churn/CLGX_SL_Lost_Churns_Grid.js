nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Lost_Churns_Grid.js
//	Script Name:	CLGX_SL_Lost_Churns_Grid
//	Script Id:		customscript_clgx_sl_lost_churns_details
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		07/02/2014
//	Includes:		CLGX_LIB_Global.js
//-------------------------------------------------------------------------------------------------

function suitelet_lost_churn_details (request, response){
    try {

        var marketid = request.getParameter('market');
        var statusLabel = request.getParameter('type');
        var putMonth = request.getParameter('month');
        var status=request.getParameter('status');


        if(marketid != '0'){
            var title = marketid + ' - ' + statusLabel;
        }
        else
        {
            var title ='All - ' + statusLabel;
        }

        var objFile = nlapiLoadFile(1423924);
        var html = objFile.getValue();
        html = html.replace(new RegExp('{churnsJSON}','g'), dataChurns(marketid, status, putMonth));
        html = html.replace(new RegExp('{title}','g'), title);

        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function dataChurns (marketid, status, putMonth) {




    var stReturn = 'var dataChurns =  [';
    var arrFilters = new Array();
    if(status!=0){
        arrFilters[0] = new nlobjSearchFilter("custrecord_clgx_churn_type_churn",null,"anyof",status);
    }
    // arrFilters[1] = new nlobjSearchFilter("periodname","custrecord_clgx_churn_expecteddate","is",putMonth);
    var arrDates  = getDateRange(putMonth);
    var startDate = arrDates[0];
    var endDate   = arrDates[1];
    arrFilters[1] = new nlobjSearchFilter("custrecord_clgx_churn_closeddate",null,"within",startDate,endDate);
    var arrColumns = new Array();


    if(marketid != '0'){
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_churn_location",null,"anyof",clgx_return_child_locations_of_marketid (parseInt(marketid))));
    }
    var getActualMRRs = nlapiSearchRecord('customrecord_cologix_churn', 'customsearch_clgx_churn_gridlostdal', arrFilters, arrColumns);
    for ( var i = 0; getActualMRRs  != null && i < getActualMRRs.length; i++ ) {
        var searchChurn = getActualMRRs[i];
        var columns = searchChurn.getAllColumns();
        if(searchChurn.getValue(columns[3])=='')
        {
            var salesrepid=0;
            var salesrep='';

        }
        else{
            var salesrep=searchChurn.getText(columns[3]);
            var salerepid=searchChurn.getValue(columns[3]);

        }
        if(searchChurn.getValue(columns[4])=='')
        {
            var actualpower=0;
        }
        else
        {
            var actualpower=searchChurn.getValue(columns[4]);
        }
        if(searchChurn.getValue(columns[5])=='')
        {
            var actualspace=0;
        }
        else
        {
            var actualspace=searchChurn.getValue(columns[5]);
        }
        if(searchChurn.getValue(columns[6])=='')
        {
            var actualinter=0;
        }
        else
        {
            var actualinter=searchChurn.getValue(columns[6]);
        }
        if(searchChurn.getValue(columns[7])=='')
        {
            var actualnetwork=0;
        }
        else
        {
            var actualnetwork=searchChurn.getValue(columns[7]);
        }
        if(searchChurn.getValue(columns[10])=='')
        {
            var actualother=0;
        }
        else
        {
            var actualother=searchChurn.getValue(columns[10]);
        }

        var total=parseFloat(actualpower)+parseFloat(actualspace)+parseFloat(actualinter)+parseFloat(actualnetwork)+parseFloat(actualother);

        stReturn += '\n{"number":"' + searchChurn.getValue(columns[0]) +
            '","date":"' +  searchChurn.getValue(columns[1]) +
            '","internalid":' +  searchChurn.getValue(columns[0]) +
            ', "customer":"' +  searchChurn.getText(columns[2]) +
            '","customerid":' +  searchChurn.getValue(columns[2]) +
            ', "salerep":"' +  salesrep +
            '","salerepid":' +  salerepid +
            ', "totalactual":' +  total.toFixed(2) +
            ', "actualpower":' +  parseFloat(actualpower)+
            ', "actualspace":' +  parseFloat(actualspace) +
            ', "actualinter":' +  parseFloat(actualinter) +
            ', "actualnetwork":' +  parseFloat(actualnetwork) +
            ', "actualother":' +  parseFloat(actualother) +
            ', "type":"' +  searchChurn.getText(columns[8]) +
            '", "reason":"' +  searchChurn.getText(columns[9]) +
            '" },';




    }
    var strLen = stReturn.length;

    if (getActualMRRs != null){
        stReturn = stReturn.slice(0,strLen-1);
    }

    stReturn += '];';
    return stReturn;
}
function getDateRange(monthsago){

    var today = new Date();
    var date = nlapiAddMonths(today, monthsago);

    var month = parseInt(date.getMonth());
    var year = date.getFullYear();

    var stDays  = daysInMonth(parseInt(month),parseInt(year));
    var stYear  = year;

    var stMonth = parseInt(month) + 1;

    var stStartDate = stMonth + '/1/' + stYear;
    var stEndDate   = stMonth + '/' + stDays + '/' + stYear;

    var arrDateRange = new Array();
    arrDateRange[0] = stStartDate;
    arrDateRange[1] =stEndDate;

    return arrDateRange;
}


function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}
