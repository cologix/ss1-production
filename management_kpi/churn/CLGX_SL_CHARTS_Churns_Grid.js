nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Churns_Grid.js
//	Script Name:	CLGX_SL_CHARTS_Churns_Grid
//	Script Id:		customscript_clgx_sl_chrt_churns_details
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		05/21/2014
//	Includes:		CLGX_LIB_Global.js
//-------------------------------------------------------------------------------------------------

function suitelet_charts_churn_details (request, response){
    try {

        var market = request.getParameter('market');
        var statusLabel = request.getParameter('type');
        var month = request.getParameter('month');
        var status=request.getParameter('status');
        var count=request.getParameter('count');
        var period='all';
        var currentDate = new Date();
        var currentMonth=('0'+(currentDate.getMonth()+1)).slice(-2);
        if(currentMonth==12)
        {
            var nextMonth='01';
            var nextNMonth='02';
            var currentYearNxMTH=parseInt(currentDate.getFullYear())+parseInt(1);
            var currentYear=currentDate.getFullYear();
        }
        else
        {
            var nextMonth=('0'+(currentDate.getMonth()+2)).slice(-2);
            var nextNMonth=('0'+(currentDate.getMonth()+3)).slice(-2);
            var currentYear=currentDate.getFullYear();
            var currentYearNxMTH=currentDate.getFullYear();
        }

        if(month=='prev')
        {
            period="Previous Months";
        }
        if(month=='curr')
        {
            period=currentMonth+'.'+currentYear;
        }
        if(month=='next')
        {
            period=nextMonth+'.'+ currentYearNxMTH;

        }


        if(month=='after')
        {
            period=nextNMonth+'.'+ currentYearNxMTH;

        }



        if(market != '0'){
            var title = market + ' - ' + statusLabel  + ' - ' + period;
        }
        else
        {
            var title ='All - ' + statusLabel  + ' - ' + period;
        }

        var objFile = nlapiLoadFile(1423932);
        var html = objFile.getValue();
        html = html.replace(new RegExp('{churnsJSON}','g'), dataChurns(market, status, month,count));
        html = html.replace(new RegExp('{title}','g'), title);

        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}



function dataChurns (market, status, month,count) {

    var stReturn = 'var dataChurns =  [';
    var val='0';
    var arrFilters = new Array();

    var arrColumns = new Array();
    if (market == 'Jacksonville')
    {//JAX1

        var arrLocation=['31'];
    }
    else if (market == 'Jacksonville2')
    {//JAX1

        var arrLocation=['40'];
    }
    else 	if (market =='Columbus')
    {//COL

        var arrLocation=['34','38','39'];
    }

    else if(market == 'Dallas'){
        //DAL

        var arrLocation=['2','17'];
    }
    else if(market == 'Minneapolis'){
        //MIN

        var arrLocation=['16', '35'];
    }

    else if(market == 'Montreal'){
        //MON

        var arrLocation=['5','8', '9','10','11','12','27'];
    }

    else if(market == 'Toronto'){
        //TOR

        var arrLocation=['6','13', '15'];
    }

    else if(market == 'Vancouver'){
        //VAN

        var arrLocation=['28','7'];
    }
    else if(market == 'Lakeland'){
        //VAN

        var arrLocation=['42'];
    }
    else{

        //  var searchSales = nlapiSearchRecord('customrecord_cologix_churn', 'customsearch_clgx_chrt_churn', arrFilters, arrColumns);

    }
    if(status!=0){

        arrFilters.push(new nlobjSearchFilter('custrecord_clgx_churn_status',null,'anyof',status));

    }

    if (market != 'All')
    {
        arrFilters.push(new nlobjSearchFilter('custrecord_clgx_churn_location',null,'anyof',arrLocation));
    }



    if((month=='prev')&&(count==0))
    {
        var searchChurns = nlapiSearchRecord('customrecord_cologix_churn', 'customsearch_clgx_grid_churnmonpr', arrFilters, arrColumns);
    }
    else
    {
        var searchChurns = nlapiSearchRecord('customrecord_cologix_churn', 'customsearch_clgx_grid_churnmonpr_2', arrFilters, arrColumns);
    }
    if(month=='curr')
    {
        var searchChurns = nlapiSearchRecord('customrecord_cologix_churn', 'customsearch_clgx_grid_churnmoncr', arrFilters, arrColumns);

    }
    if(month=='next')
    {
        var searchChurns = nlapiSearchRecord('customrecord_cologix_churn', 'customsearch_clgx_grid_churnmonnx', arrFilters, arrColumns);

    }
    if(month=='after')
    {
        var searchChurns = nlapiSearchRecord('customrecord_cologix_churn', 'customsearch_clgx_grid_churnmonaf', arrFilters, arrColumns);

    }
    for ( var i = 0; searchChurns != null && i < searchChurns.length; i++ ) {
        var searchChurn = searchChurns[i];
        var columns = searchChurn.getAllColumns();
        if(searchChurn.getValue(columns[0])=='1')
        {
            var val='1';
            if(searchChurn.getValue(columns[4])=='')
            {
                var salesrepid=0;
                var salesrep='';

            }
            else{
                var salesrep=searchChurn.getText(columns[4]);
                var salerepid=searchChurn.getValue(columns[4]);

            }
            if(searchChurn.getValue(columns[5])=='')
            {
                var exspacerchurn=0;
            }
            else
            {
                var exspacerchurn=searchChurn.getValue(columns[5]);
            }
            if(searchChurn.getValue(columns[6])=='')
            {
                var expowermrrchurn=0;
            }
            else
            {
                var expowermrrchurn=searchChurn.getValue(columns[6]);
            }
            if(searchChurn.getValue(columns[7])=='')
            {
                var exinterconnectionchurn=0;
            }
            else
            {
                var exinterconnectionchurn=searchChurn.getValue(columns[7]);
            }
            if(searchChurn.getValue(columns[8])=='')
            {
                var exnetworkchurn=0;
            }
            else
            {
                var exnetworkchurn=searchChurn.getValue(columns[8]);
            }
            if(searchChurn.getValue(columns[9])=='')
            {
                var currinterconnectionchurn=0;
            }
            else
            {
                var currinterconnectionchurn=searchChurn.getValue(columns[9]);
            }
            if(searchChurn.getValue(columns[10])=='')
            {
                var currnetworkchurn=0;
            }
            else
            {
                var currnetworkchurn=searchChurn.getValue(columns[10]);
            }
            if(searchChurn.getValue(columns[11])=='')
            {
                var currpowermrrchurn=0;
            }
            else
            {
                var currpowermrrchurn=searchChurn.getValue(columns[11]);
            }
            if(searchChurn.getValue(columns[12])=='')
            {
                var currspacerchurn=0;
            }
            else
            {
                var currspacerchurn=searchChurn.getValue(columns[12]);
            }
            if(searchChurn.getValue(columns[14])=='')
            {
                var exotherchurn=0;
            }
            else
            {
                var exotherchurn=searchChurn.getValue(columns[14]);
            }
            if(searchChurn.getValue(columns[15])=='')
            {
                var currotherchurn=0;
            }
            else
            {
                var currotherchurn=searchChurn.getValue(columns[15]);
            }
            var total=parseFloat(expowermrrchurn)+parseFloat(exspacerchurn)+parseFloat(exinterconnectionchurn)+parseFloat(exnetworkchurn)+parseFloat(exotherchurn);
            var totalcurr=parseFloat(currinterconnectionchurn)+parseFloat(searchChurn.getValue(columns[10]))+parseFloat(searchChurn.getValue(columns[11]))+parseFloat(searchChurn.getValue(columns[12]))+parseFloat(currotherchurn);

            stReturn += '\n{"number":"' + searchChurn.getValue(columns[1]) +
                '","date":"' +  searchChurn.getText(columns[2]) +
                '","internalid":' +  searchChurn.getValue(columns[1]) +
                ', "customer":"' +  searchChurn.getText(columns[3]) +
                '","customerid":' +  searchChurn.getValue(columns[3]) +
                ', "salerep":"' +  salesrep +
                '","salerepid":' +  salerepid +
                ', "total":' +  total.toFixed(2) +
                ', "totalcurr":' +  totalcurr.toFixed(2) +
                ', "expowermrrchurn":' +  parseFloat(expowermrrchurn)+
                ', "currpowermrrchurn":' +  parseFloat(currpowermrrchurn) +
                ', "exspacerchurn":' +  parseFloat(exspacerchurn) +
                ', "currspacerchurn":' +  parseFloat(currspacerchurn) +
                ', "exinterconnectionchurn":' +  parseFloat(exinterconnectionchurn) +
                ', "currinterconnectionchurn":' +  parseFloat(currinterconnectionchurn)+
                ', "exnetworkchurn":' +  parseFloat(exnetworkchurn)+
                ', "currnetworkchurn":' +  parseFloat(currnetworkchurn) +
                ', "exotherchurn":' +  parseFloat(exotherchurn)+
                ', "currotherchurn":' +  parseFloat(currotherchurn) +
                ' },';



        }
    }
    var strLen = stReturn.length;
    if(val=='1')
    {
        if (searchChurns != null){
            stReturn = stReturn.slice(0,strLen-1);
        }
    }
    stReturn += '];';
    return stReturn;
}