nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Report_ChurnsLost.js
//	Script Name:	CLGX_SL_Report_ChurnsLost
//	Script Id:		customscript_clgx_sl_chrt_churn_lost
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		01/07/2014
//	Includes:		CLGX_LIB_Global.js, moment.min.js
//-------------------------------------------------------------------------------------------------

function suitelet_charts_churn_lost(request, response){
    try {
        var market = request.getParameter('marketid');
        //var marketid = 0;
        var title='All';

        if(market != '' ){
            var objFile = nlapiLoadFile(1423927);
            var html = objFile.getValue();
            if(market!=0)
            {
                if(market=='25')
                {
                    var title = 'Montreal';
                }
                if(market=='23')
                {
                    var title = 'Toronto';
                }
                if(market=='26')
                {
                    var title = 'Vancouver';
                }
                if(market=='22')
                {
                    var title = 'Minneapolis';
                }
                if(market=='29')
                {
                    var title = 'Jacksonville';
                }
                if(market=='20')
                {
                    var title = 'Dallas';
                }
                if(market=='33')
                {
                    var title = 'Columbus';
                }
                if(market=='41')
                {
                    var title = 'Lakeland';
                }
   if(market=='52')
                {
                    var title = 'Parsippany';
                }
            }

            html = html.replace(new RegExp('{title}','g'),title);
            html = html.replace(new RegExp('{marketid}','g'), market);
            html = html.replace(new RegExp('{series}','g'), getSeries(market));
        }
        else{
            var html = 'Please select a market from the left panel.';
        }
        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function getSeries (marketid){
    var firstDate= new Date(new Date().getFullYear(), 0, 1);

    var today=new Date();
    var dayCurrMonth =new Date(new Date().getFullYear(), today.getMonth(), 1);
    var countMonths=monthDiff(firstDate, dayCurrMonth);
    var aMonth = today.getMonth();
    var months= [], i;
    var month = new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    for (i=0; i<12; i++) {
        months.push(month[aMonth]);
        aMonth--;
        if (aMonth < 0) {
            aMonth = 11;
        }
    }
    var currentDate=new Date();
    var currentYear=currentDate.getFullYear();
    var lastYear=currentDate.getFullYear() - 1;
    var currMonth=0;
    countMonths=countMonths+1;
    if(countMonths<1)
    {
        //var lastMonth=months[1]+" "+lastYear;
        var lastMonth=-1;
    }
    else
    {
        //var lastMonth=months[1]+" "+currentYear;
        var lastMonth=-1;
    }
    if(countMonths<2)
    {
        //var twoMonthsAgo=months[2]+" "+lastYear;
        var twoMonthsAgo=-2;

    }
    else
    {
        //var twoMonthsAgo=months[2]+" "+currentYear;
        var twoMonthsAgo=-2;
    }
    if(countMonths<3)
    {
        //var threeMonthsAgo=months[3]+" "+lastYear;
        var threeMonthsAgo=-3;
    }
    else
    {
        // var threeMonthsAgo=months[3]+" "+currentYear;
        var threeMonthsAgo=-3;
    }
    if(countMonths<4)
    {
        //  var fourMonthsAgo=months[4]+" "+lastYear;
        var fourMonthsAgo=-4;
    }
    else
    {
        var fourMonthsAgo=-4;
    }
    if(countMonths<5)
    {
        // var fiveMonthsAgo=months[5]+" "+lastYear;
        var fiveMonthsAgo=-5;
    }
    else
    {
        // var fiveMonthsAgo=months[5]+" "+currentYear;
        var fiveMonthsAgo=-5;
    }
    if(countMonths<6)
    {
        // var sixMonthsAgo=months[6]+" "+lastYear;
        var sixMonthsAgo=-6;
    }
    else
    {
        // var sixMonthsAgo=months[6]+" "+currentYear;
        var sixMonthsAgo=-6;
    }
    if(countMonths<7)
    {
        //  var sevenMonthsAgo=months[7]+" "+lastYear;
        var sevenMonthsAgo=-7;
    }
    else
    {
        // var sevenMonthsAgo=months[7]+" "+currentYear;
        var sevenMonthsAgo=-7;
    }
    if(countMonths<8)
    {
        //var eightMonthsAgo=months[8]+" "+lastYear;
        var eightMonthsAgo=-8;
    }
    else
    {
        //var eightMonthsAgo=months[8]+" "+currentYear;
        var eightMonthsAgo=-8;
    }
    if(countMonths<9)
    {
        // var nineMonthsAgo=months[9]+" "+lastYear;
        var nineMonthsAgo=-9;
    }
    else
    {
        // var nineMonthsAgo=months[9]+" "+currentYear;
        var nineMonthsAgo=-9;
    }
    if(countMonths<10)
    {
        //var tenMonthsAgo=months[10]+" "+lastYear;
        var tenMonthsAgo=-10;
    }
    else
    {
        // var tenMonthsAgo=months[10]+" "+currentYear;
        var tenMonthsAgo=-10;
    }
    if(countMonths<11)
    {
        // var elevenMonthsAgo=months[11]+" "+lastYear;
        var elevenMonthsAgo=-11;
    }
    else
    {
        // var elevenMonthsAgo=months[11]+" "+currentYear;
        var elevenMonthsAgo=-11;
    }


    var arrFilters = new Array();
    var arrColumns = new Array();

    if(marketid != '0'){
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_churn_location",null,"anyof",clgx_return_child_locations_of_marketid (parseInt(marketid))));
    }
    var getActualMRRs = nlapiSearchRecord('customrecord_cologix_churn', 'customsearch_clgx_churn_searchlostdal', arrFilters, arrColumns);

    var stReturn = '[';
    if(getActualMRRs!=null)
    {
        for ( var i = 0; getActualMRRs != null && i < getActualMRRs.length; i++ ) {
            var getActualMRR = getActualMRRs[i];
            var columns = getActualMRR.getAllColumns();
            stReturn += '\n{name: \'Disconnect\',type: \'column\',  data: \n[\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[11])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[11]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+elevenMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[10])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[10]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+tenMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[9])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[9]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+nineMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[8])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[8]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+eightMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[7])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[7]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+sevenMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[6])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[6]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+sixMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[5])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[5]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+fiveMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[4])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[4]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+fourMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[3])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[3]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+threeMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[2])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[2]))) : 0) + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+twoMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[1])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[1]))) : 0) + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+lastMonth+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[0])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[0]))) : 0) + ',url:\'/app/site/hosting/scriptlet.nl?script=321&deploy=1&status=1&type=Disconnect&market=' + marketid + '&month='+currMonth+'\'},\n';
            stReturn += ']\n';
            stReturn += '},\n';

            stReturn += '\n{name: \'Downgrade\',type: \'column\',  data: \n[\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[24])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[24]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+elevenMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[23])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[23]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+tenMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[22])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[22]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+nineMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[21])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[21]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+eightMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[20])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[20]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+sevenMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[19])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[19]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+sixMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[18])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[18]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+fiveMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[17])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[17]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+fourMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[16])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[16]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+threeMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[15])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[15]))) : 0) + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+twoMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[14])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[14]))) : 0) + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+lastMonth+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[13])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[13]))) : 0) + ',url:\'/app/site/hosting/scriptlet.nl?script=321&deploy=1&status=2&type=Downgrade&market=' + marketid + '&month='+currMonth+'\'},\n';
            stReturn += ']\n';
            stReturn += '},\n';
            stReturn += '\n{name: \'Decrease\',type: \'column\',  data: \n[\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[37])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[37]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+elevenMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[36])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[36]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+tenMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[35])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[35]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+nineMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[34])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[34]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+eightMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[33])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[33]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+sevenMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[32])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[32]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+sixMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[31])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[31]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+fiveMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[30])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[30]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+fourMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[29])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[29]))) : 0)  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+threeMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[28])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[28]))) : 0) + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+twoMonthsAgo+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[27])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[27]))) : 0) + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+lastMonth+'\'},\n';
            stReturn += '{y:' + ((getActualMRR.getValue(columns[26])!='') ? Math.round(parseFloat(getActualMRR.getValue(columns[26]))) : 0) + ',url:\'/app/site/hosting/scriptlet.nl?script=321&deploy=1&status=3&type=Decrease&market=' + marketid + '&month='+currMonth+'\'},\n';
        }
    }
    else
    {

        stReturn += '\n{name: \'Disconnect\',type: \'column\',  data: \n[\n';
        stReturn += '{y:' + 0 + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+elevenMonthsAgo+'\'},\n';
        stReturn += '{y:' +0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+tenMonthsAgo+'\'},\n';
        stReturn += '{y:' +0 + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+nineMonthsAgo+'\'},\n';
        stReturn += '{y:' +0 + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+eightMonthsAgo+'\'},\n';
        stReturn += '{y:' +0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+sevenMonthsAgo+'\'},\n';
        stReturn += '{y:' +0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+sixMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+fiveMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+fourMonthsAgo+'\'},\n';
        stReturn += '{y:' +0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+threeMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0 + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+twoMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0 + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=1&deploy=1&type=Disconnect&market=' + marketid + '&month='+lastMonth+'\'},\n';
        stReturn += '{y:' +0 + ',url:\'/app/site/hosting/scriptlet.nl?script=321&deploy=1&status=1&type=Disconnect&market=' + marketid + '&month='+currMonth+'\'},\n';
        stReturn += ']\n';
        stReturn += '},\n';

        stReturn += '\n{name: \'Downgrade\',type: \'column\',  data: \n[\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+elevenMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+tenMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+nineMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0 + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+eightMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+sevenMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+sixMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+fiveMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+fourMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+threeMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0 + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+twoMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0 + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=2&deploy=1&type=Downgrade&market=' + marketid + '&month='+lastMonth+'\'},\n';
        stReturn += '{y:' + 0 + ',url:\'/app/site/hosting/scriptlet.nl?script=321&deploy=1&status=2&type=Downgrade&market=' + marketid + '&month='+currMonth+'\'},\n';
        stReturn += ']\n';
        stReturn += '},\n';
        stReturn += '\n{name: \'Decrease\',type: \'column\',  data: \n[\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+elevenMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+tenMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+nineMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+eightMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+sevenMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+sixMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+fiveMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0  + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+fourMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0 + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+threeMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0 + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+twoMonthsAgo+'\'},\n';
        stReturn += '{y:' + 0 + ',url:\'/app/site/hosting/scriptlet.nl?script=321&status=3&deploy=1&type=Decrease&market=' + marketid + '&month='+lastMonth+'\'},\n';
        stReturn += '{y:' + 0 + ',url:\'/app/site/hosting/scriptlet.nl?script=321&deploy=1&status=3&type=Decrease&market=' + marketid + '&month='+currMonth+'\'}\n';

    }
    var strLen = stReturn.length;
    stReturn = stReturn.slice(0,strLen-1);

    stReturn += ']\n';
    stReturn += '},';

    var arrAverages = new Array();
    arrAverages = [0,0,0,0,0,0,0];
    var arrTotals=new Array();
    arrTotals[0]=parseFloat(((getActualMRR.getValue(columns[0])!='') ? getActualMRR.getValue(columns[0]) : 0));
    arrTotals[1]=parseFloat(((getActualMRR.getValue(columns[1])!='') ? getActualMRR.getValue(columns[1]) : 0));
    arrTotals[2]=parseFloat(((getActualMRR.getValue(columns[2])!='') ? getActualMRR.getValue(columns[2]) : 0));
    arrTotals[3]=parseFloat(((getActualMRR.getValue(columns[3])!='') ? getActualMRR.getValue(columns[3]) : 0));
    arrTotals[4]=parseFloat(((getActualMRR.getValue(columns[4])!='') ? getActualMRR.getValue(columns[4]) : 0));
    arrTotals[5]=parseFloat(((getActualMRR.getValue(columns[5])!='') ? getActualMRR.getValue(columns[5]) : 0));
    arrTotals[6]=parseFloat(((getActualMRR.getValue(columns[6])!='') ? getActualMRR.getValue(columns[6]) : 0));
    arrTotals[7]=parseFloat(((getActualMRR.getValue(columns[7])!='') ? getActualMRR.getValue(columns[7]) : 0));
    arrTotals[8]=parseFloat(((getActualMRR.getValue(columns[8])!='') ? getActualMRR.getValue(columns[8]) : 0));
    arrTotals[9]=parseFloat(((getActualMRR.getValue(columns[9])!='') ? getActualMRR.getValue(columns[9]) : 0));
    arrTotals[10]=parseFloat(((getActualMRR.getValue(columns[10])!='') ? getActualMRR.getValue(columns[10]) : 0));
    arrTotals[11]=parseFloat(((getActualMRR.getValue(columns[11])!='') ? getActualMRR.getValue(columns[11]) : 0));

    arrTotals[13]=parseFloat(((getActualMRR.getValue(columns[13])!='') ? getActualMRR.getValue(columns[13]) : 0));
    arrTotals[14]=parseFloat(((getActualMRR.getValue(columns[14])!='') ? getActualMRR.getValue(columns[14]) : 0));
    arrTotals[15]=parseFloat(((getActualMRR.getValue(columns[15])!='') ? getActualMRR.getValue(columns[15]) : 0));
    arrTotals[16]=parseFloat(((getActualMRR.getValue(columns[16])!='') ? getActualMRR.getValue(columns[16]) : 0));
    arrTotals[17]=parseFloat(((getActualMRR.getValue(columns[17])!='') ? getActualMRR.getValue(columns[17]) : 0));
    arrTotals[18]=parseFloat(((getActualMRR.getValue(columns[18])!='') ? getActualMRR.getValue(columns[18]) : 0));
    arrTotals[19]=parseFloat(((getActualMRR.getValue(columns[19])!='') ? getActualMRR.getValue(columns[19]) : 0));
    arrTotals[20]=parseFloat(((getActualMRR.getValue(columns[20])!='') ? getActualMRR.getValue(columns[20]) : 0));
    arrTotals[21]=parseFloat(((getActualMRR.getValue(columns[21])!='') ? getActualMRR.getValue(columns[21]) : 0));
    arrTotals[22]=parseFloat(((getActualMRR.getValue(columns[22])!='') ? getActualMRR.getValue(columns[22]) : 0));
    arrTotals[23]=parseFloat(((getActualMRR.getValue(columns[23])!='') ? getActualMRR.getValue(columns[23]) : 0));
    arrTotals[24]=parseFloat(((getActualMRR.getValue(columns[24])!='') ? getActualMRR.getValue(columns[24]) : 0));

    arrTotals[26]=parseFloat(((getActualMRR.getValue(columns[26])!='') ? getActualMRR.getValue(columns[26]) : 0));
    arrTotals[27]=parseFloat(((getActualMRR.getValue(columns[27])!='') ? getActualMRR.getValue(columns[27]) : 0));
    arrTotals[28]=parseFloat(((getActualMRR.getValue(columns[28])!='') ? getActualMRR.getValue(columns[28]) : 0));
    arrTotals[29]=parseFloat(((getActualMRR.getValue(columns[29])!='') ? getActualMRR.getValue(columns[29]) : 0));
    arrTotals[30]=parseFloat(((getActualMRR.getValue(columns[30])!='') ? getActualMRR.getValue(columns[30]) : 0));
    arrTotals[31]=parseFloat(((getActualMRR.getValue(columns[31])!='') ? getActualMRR.getValue(columns[31]) : 0));
    arrTotals[32]=parseFloat(((getActualMRR.getValue(columns[32])!='') ? getActualMRR.getValue(columns[32]) : 0));
    arrTotals[33]=parseFloat(((getActualMRR.getValue(columns[33])!='') ? getActualMRR.getValue(columns[33]) : 0));
    arrTotals[34]=parseFloat(((getActualMRR.getValue(columns[34])!='') ? getActualMRR.getValue(columns[34]) : 0));
    arrTotals[35]=parseFloat(((getActualMRR.getValue(columns[35])!='') ? getActualMRR.getValue(columns[35]) : 0));
    arrTotals[36]=parseFloat(((getActualMRR.getValue(columns[36])!='') ? getActualMRR.getValue(columns[36]) : 0));
    arrTotals[37]=parseFloat(((getActualMRR.getValue(columns[37])!='') ? getActualMRR.getValue(columns[37]) : 0));

    arrTotals[38]=parseFloat(((getActualMRR.getValue(columns[38])!='') ? getActualMRR.getValue(columns[38]) : 0));
    arrTotals[39]=parseFloat(((getActualMRR.getValue(columns[39])!='') ? getActualMRR.getValue(columns[39]) : 0));
    arrTotals[40]=parseFloat(((getActualMRR.getValue(columns[40])!='') ? getActualMRR.getValue(columns[40]) : 0));
    arrTotals[41]=parseFloat(((getActualMRR.getValue(columns[41])!='') ? getActualMRR.getValue(columns[41]) : 0));
    arrTotals[42]=parseFloat(((getActualMRR.getValue(columns[42])!='') ? getActualMRR.getValue(columns[42]) : 0));
    arrTotals[43]=parseFloat(((getActualMRR.getValue(columns[43])!='') ? getActualMRR.getValue(columns[43]) : 0));
    arrTotals[44]=parseFloat(((getActualMRR.getValue(columns[44])!='') ? getActualMRR.getValue(columns[44]) : 0));
    arrTotals[45]=parseFloat(((getActualMRR.getValue(columns[45])!='') ? getActualMRR.getValue(columns[45]) : 0));
    arrTotals[46]=parseFloat(((getActualMRR.getValue(columns[46])!='') ? getActualMRR.getValue(columns[46]) : 0));
    arrTotals[47]=parseFloat(((getActualMRR.getValue(columns[47])!='') ? getActualMRR.getValue(columns[47]) : 0));
    arrTotals[48]=parseFloat(((getActualMRR.getValue(columns[48])!='') ? getActualMRR.getValue(columns[48]) : 0));
    arrTotals[49]=parseFloat(((getActualMRR.getValue(columns[49])!='') ? getActualMRR.getValue(columns[49]) : 0));

    arrAverages[0] = parseFloat((arrTotals[38]+arrTotals[39] + arrTotals[40] + arrTotals[41] + arrTotals[42] + arrTotals[43] + arrTotals[44] + arrTotals[45] + arrTotals[46] + arrTotals[47] + arrTotals[48] + arrTotals[49])/12).toFixed(2);
    arrAverages[1] = parseFloat(((arrTotals[11]+arrTotals[24]+arrTotals[37])+arrTotals[38]+arrTotals[39] +arrTotals[40]+arrTotals[41]+arrTotals[42]+arrTotals[43]+arrTotals[44]+arrTotals[45]+arrTotals[46]+arrTotals[47]+arrTotals[48])/12).toFixed(2);
    arrAverages[2] = parseFloat(((arrTotals[10]+arrTotals[23]+arrTotals[36])+(arrTotals[11]+arrTotals[24] + arrTotals[37])+arrTotals[38]+arrTotals[39] +arrTotals[40]+arrTotals[41]+arrTotals[42]+arrTotals[43]+arrTotals[44]+arrTotals[45]+arrTotals[46]+arrTotals[47])/12).toFixed(2);
    arrAverages[3] = parseFloat(((arrTotals[9]+arrTotals[22]+arrTotals[35])+(arrTotals[10]+arrTotals[23] + arrTotals[36])+(arrTotals[11]+arrTotals[24] + arrTotals[37]) +arrTotals[38]+arrTotals[39] +arrTotals[40]+arrTotals[41]+arrTotals[42]+arrTotals[43]+arrTotals[44]+arrTotals[45]+arrTotals[46])/12).toFixed(2);
    arrAverages[4] = parseFloat(((arrTotals[8]+arrTotals[21]+arrTotals[34])+(arrTotals[9]+arrTotals[22] + arrTotals[35])+(arrTotals[10]+arrTotals[23] + arrTotals[36])+(arrTotals[11]+arrTotals[24] + arrTotals[37]) +arrTotals[38]+arrTotals[39] +arrTotals[40]+arrTotals[41]+arrTotals[42]+arrTotals[43]+arrTotals[44]+arrTotals[45])/12).toFixed(2);
    arrAverages[5] = parseFloat(((arrTotals[7]+arrTotals[20]+arrTotals[33])+(arrTotals[8]+arrTotals[21] + arrTotals[34])+(arrTotals[9]+arrTotals[22] + arrTotals[35])+(arrTotals[10]+arrTotals[23] + arrTotals[36])+(arrTotals[11]+arrTotals[24] + arrTotals[37]) +arrTotals[38]+arrTotals[39] +arrTotals[40]+arrTotals[41]+arrTotals[42]+arrTotals[43]+arrTotals[44])/12).toFixed(2);
    arrAverages[6] = parseFloat(((arrTotals[6]+arrTotals[19]+arrTotals[32])+(arrTotals[7]+arrTotals[20] + arrTotals[33])+(arrTotals[8]+arrTotals[21] + arrTotals[34])+(arrTotals[9]+arrTotals[22] + arrTotals[35])+(arrTotals[10]+arrTotals[23] + arrTotals[36])+(arrTotals[11]+arrTotals[24] + arrTotals[37]) +arrTotals[38]+arrTotals[39] +arrTotals[40]+arrTotals[41]+arrTotals[42]+arrTotals[43])/12).toFixed(2);
    arrAverages[7] = parseFloat(((arrTotals[5]+arrTotals[18]+arrTotals[31])+(arrTotals[6]+arrTotals[19] + arrTotals[32])+(arrTotals[7]+arrTotals[20] + arrTotals[33])+(arrTotals[8]+arrTotals[21] + arrTotals[34])+(arrTotals[9]+arrTotals[22] + arrTotals[35])+(arrTotals[10]+arrTotals[23] + arrTotals[36]) +(arrTotals[10]+arrTotals[24] + arrTotals[37])+arrTotals[38]+arrTotals[39] +arrTotals[40]+arrTotals[41]+arrTotals[42])/12).toFixed(2);
    arrAverages[8] = parseFloat(((arrTotals[4]+arrTotals[17]+arrTotals[30])+(arrTotals[5]+arrTotals[18] + arrTotals[31])+(arrTotals[6]+arrTotals[19] + arrTotals[32])+(arrTotals[7]+arrTotals[20] + arrTotals[33])+(arrTotals[8]+arrTotals[21] + arrTotals[34])+(arrTotals[9]+arrTotals[22] + arrTotals[35]) +(arrTotals[10]+arrTotals[23] + arrTotals[36])+(arrTotals[11]+arrTotals[24] + arrTotals[37])+arrTotals[38]+arrTotals[39] +arrTotals[40]+arrTotals[41])/12).toFixed(2);
    arrAverages[9] = parseFloat(((arrTotals[3]+arrTotals[16]+arrTotals[29])+(arrTotals[4]+arrTotals[17] + arrTotals[30])+(arrTotals[5]+arrTotals[18] + arrTotals[31])+(arrTotals[6]+arrTotals[19] + arrTotals[32])+(arrTotals[7]+arrTotals[20] + arrTotals[33])+(arrTotals[8]+arrTotals[21] + arrTotals[34]) +(arrTotals[9]+arrTotals[22] + arrTotals[35])+(arrTotals[10]+arrTotals[23] + arrTotals[36])+(arrTotals[11]+arrTotals[24] + arrTotals[37])+(arrTotals[11]+arrTotals[24] + arrTotals[37]) + arrTotals[38]+arrTotals[39])/12).toFixed(2);
    arrAverages[10] = parseFloat((arrTotals[2]+arrTotals[15]+arrTotals[28]+arrTotals[3]+arrTotals[16] + arrTotals[29]+arrTotals[4]+arrTotals[17] + arrTotals[30]+arrTotals[5]+arrTotals[18] + arrTotals[31]+arrTotals[6]+arrTotals[19] + arrTotals[32]+arrTotals[7]+arrTotals[20] + arrTotals[33] +arrTotals[8]+arrTotals[21] + arrTotals[34]+arrTotals[9]+arrTotals[22] + arrTotals[35]+arrTotals[10]+arrTotals[23] + arrTotals[36]+arrTotals[11]+arrTotals[24] + arrTotals[37] + arrTotals[38]+arrTotals[39])/12).toFixed(2);
    arrAverages[11] = parseFloat((arrTotals[1]+arrTotals[14]+arrTotals[27]+arrTotals[2]+arrTotals[15] + arrTotals[28]+arrTotals[3]+arrTotals[16] + arrTotals[29]+arrTotals[4]+arrTotals[17] + arrTotals[30]+arrTotals[5]+arrTotals[18] + arrTotals[31]+arrTotals[6]+arrTotals[19] + arrTotals[32] +arrTotals[7]+arrTotals[20] + arrTotals[33]+arrTotals[8]+arrTotals[21] + arrTotals[34]+arrTotals[9]+arrTotals[22] + arrTotals[35]+arrTotals[10]+arrTotals[23] + arrTotals[36]+arrTotals[11]+arrTotals[24] + arrTotals[37] + arrTotals[38])/12).toFixed(2);

    stReturn += '\n{name: \'Average\',type: \'spline\', color: \'#BC4676\',data: [\n';
    for(var z=0; z < arrAverages.length; z++){
        stReturn += '{y:' + Math.round(arrAverages[z]) + ',url:\'#\'},,';
    }
    var strLen = stReturn.length;
    stReturn = stReturn.slice(0,strLen-1);

    stReturn += ']\n';
    stReturn += '}';

    stReturn += ']';

    return stReturn;
}




function getDateRange(monthsago){

    var today = new Date();
    var date = nlapiAddMonths(today, monthsago);

    var month = parseInt(date.getMonth());
    var year = date.getFullYear();

    var stDays  = daysInMonth(parseInt(month),parseInt(year));
    var stYear  = year;

    var stMonth = parseInt(month) + 1;

    var stStartDate = stMonth + '/1/' + stYear;
    var stEndDate   = stMonth + '/' + stDays + '/' + stYear;

    var arrDateRange = new Array();
    arrDateRange[0] = stStartDate;
    arrDateRange[1] =stEndDate;

    return arrDateRange;
}


function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}
function countMonths(startDate, endDate )
{
    var stepDate = new Date();
    stepDate.time = startDate.getTime();
    var monthCount=0;

    while( stepDate.getTime() <= endDate.getTime() ) {
        stepDate.month += 1;
        monthCount += 1;
    }

    if ( stepDate != endDate ) {
        monthCount -= 1;
    }

    return monthCount;
}
function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth() + 1;
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
}