nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Churns_SalesRep_Grid.js
//	Script Name:	CLGX_SL_CHARTS_Churns_Grid
//	Script Id:		customscript_clgx_sl_chrt_churns_salesrep
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		05/21/2014
//-------------------------------------------------------------------------------------------------

function suitelet_charts_churn_details (request, response){
    try {
        var userid = nlapiGetUser();
        var roleid = nlapiGetRole();
        var market = request.getParameter('marketid');
        var statusLabel = request.getParameter('statusLabel');
        var month = request.getParameter('month');
        var status=request.getParameter('status');
        var repid=request.getParameter('repid');

        if(market!=0)
        {
            if(market=='25')
            {
                var title = 'Montreal';
            }
            if(market=='23')
            {
                var title = 'Toronto';
            }
            if(market=='26')
            {
                var title = 'Vancouver';
            }
            if(market=='22')
            {
                var title = 'Minneapolis';
            }
            if(market=='29')
            {
                var title = 'Jacksonville';
            }
            if(market=='20')
            {
                var title = 'Dallas';
            }
            if(market=='33')
            {
                var title = 'Columbus';
            }
        }


        var objFile = nlapiLoadFile(1423926);
        var html = objFile.getValue();
        html = html.replace(new RegExp('{churnsJSON}','g'), dataChurns(market, repid));
        html = html.replace(new RegExp('{title}','g'), title);

        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function dataChurns (market, repid) {


    var arrFilters = new Array();
    if(repid!=0)
    {
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_churn_salesrep",null,"anyof",repid));
    }
    if(market != '0'){
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_churn_location",null,"anyof",clgx_return_child_locations_of_marketid (parseInt(market))));
    }
    var arrColumns = new Array();
    var searchChurns = nlapiSearchRecord('customrecord_cologix_churn', 'customsearch_clgx_churn_salesrepgrid', arrFilters, arrColumns);

    var stReturn = 'var myData =  [';

    for ( var i = 0; searchChurns != null && i < searchChurns.length; i++ ) {
        var searchChurn = searchChurns[i];
        var columns = searchChurn.getAllColumns();


        if(searchChurn.getValue(columns[3])=='')
        {
            var salesrepid=0;
            var salesrep='';

        }
        else{
            var salesrep=searchChurn.getText(columns[3]);
            var salerepid=searchChurn.getValue(columns[3]);
        }
        var total=((searchChurn.getValue(columns[5])!='') ? parseFloat(searchChurn.getValue(columns[5])) : 0)+((searchChurn.getValue(columns[4])!='') ? parseFloat(searchChurn.getValue(columns[4])) : 0)+((searchChurn.getValue(columns[6])!='') ? parseFloat(searchChurn.getValue(columns[6])) : 0)+((searchChurn.getValue(columns[7])!='') ? parseFloat(searchChurn.getValue(columns[7])) : 0)+((searchChurn.getValue(columns[15])!='') ? parseFloat(searchChurn.getValue(columns[15])) : 0);
        var totalcurr=((searchChurn.getValue(columns[8])!='') ? parseFloat(searchChurn.getValue(columns[8])) : 0)+((searchChurn.getValue(columns[9])!='') ? parseFloat(searchChurn.getValue(columns[9])) : 0)+ ((searchChurn.getValue(columns[10])!='') ? parseFloat(searchChurn.getValue(columns[10])) : 0)+((searchChurn.getValue(columns[11])!='') ? parseFloat(searchChurn.getValue(columns[11])) : 0)+((searchChurn.getValue(columns[16])!='') ? parseFloat(searchChurn.getValue(columns[16])) : 0);

        stReturn += '\n["' + searchChurn.getValue(columns[0]) +
            '","' +  searchChurn.getText(columns[1]) +
            '",' +  searchChurn.getValue(columns[0]) +
            ', "' +  searchChurn.getText(columns[2]) +
            '",' +  searchChurn.getValue(columns[2]) +
            ', "' +  salesrep +
            '",' +  salerepid +
            ', ' +  total.toFixed(2) +
            ', ' +  totalcurr.toFixed(2) +
            ', ' +((searchChurn.getValue(columns[5])!='') ? parseFloat(searchChurn.getValue(columns[5])) : 0)+
            ', ' +  ((searchChurn.getValue(columns[10])!='') ? parseFloat(searchChurn.getValue(columns[10])) : 0) +
            ', ' +  ((searchChurn.getValue(columns[4])!='') ? parseFloat(searchChurn.getValue(columns[4])) : 0) +
            ', ' +  ((searchChurn.getValue(columns[11])!='') ? parseFloat(searchChurn.getValue(columns[11])) : 0) +
            ', ' +  ((searchChurn.getValue(columns[6])!='') ? parseFloat(searchChurn.getValue(columns[6])) : 0) +
            ', ' +  ((searchChurn.getValue(columns[8])!='') ? parseFloat(searchChurn.getValue(columns[8])) : 0)+
            ', ' +  ((searchChurn.getValue(columns[7])!='') ? parseFloat(searchChurn.getValue(columns[7])) : 0)+
            ', ' + ((searchChurn.getValue(columns[9])!='') ? parseFloat(searchChurn.getValue(columns[9])) : 0) +
            ', ' +  ((searchChurn.getValue(columns[15])!='') ? parseFloat(searchChurn.getValue(columns[15])) : 0)+
            ', ' + ((searchChurn.getValue(columns[16])!='') ? parseFloat(searchChurn.getValue(columns[16])) : 0) +
            ', "' + searchChurn.getText(columns[13]) +
            '", "' + searchChurn.getText(columns[14]) +
            '", "' + searchChurn.getText(columns[17]) +
            '" ],';

        var strLen = stReturn.length;

    }
    if (searchChurns != null){
        stReturn = stReturn.slice(0,strLen-1);
    }
    stReturn += '];';
    return stReturn;
}