//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Menu.js
//	Script Name:	CLGX_SL_CHARTS_Menu
//	Script Id:		customscript_clgx_sl_chrt_menu
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		07/20/2013
//	Includes:		CLGX_LIB_Global.js
//-------------------------------------------------------------------------------------------------

function suitelet_charts_menu (request, response){
    try {

        var userid = nlapiGetUser();
        var roleid = nlapiGetRole();

        var fields = ['location','department'];
        var columns = nlapiLookupField('employee', userid, fields);
        var locationid = columns.field1;
        var departmentid = columns.field2;

        var objFile = nlapiLoadFile(549375);
        var html = objFile.getValue();

        /* Roles


         // see all
         -5      Administrator
         3       Administrator
         18      Full Access
         1088	 Cologix Supreme Commander

         1006    Cologix CEO
         1007    Cologix COO
         1009    Cologix Accountant (Reviewer)
         1013    Cologix CFO
         1017    Cologix Product Manager
         1019    Cologix Sales Executive
         1027    Cologix General Counsel
         1033    Cologix Operations Vice President
         1040    Cologix Sales Team Lead
         1044    Cologix Sales Executive Admin

         // see their own churn record
         1018    Cologix Sales Person
         1021    Cologix Accounting Manager

         1031	Cologix Accountant Admin
         1045	Cologix Operations VP Admin
         1042    Cologix Sales Team Lead Admin

         1018    Cologix Sales Person
         1043    Cologix Sales Person Admin
         1047	Cologix Sales Person Test

         */

        if (roleid == -5 || roleid == 3 || roleid == 18 || roleid == 1088) {

            html = html.replace(new RegExp('{dataActivities}','g'), menuActivities('yes', 'all', locationid, userid));
            html = html.replace(new RegExp('{defaultChart}','g'),'/app/site/hosting/scriptlet.nl?script=716&deploy=1&departmentid=0&marketid=0&userid=0&period=month&periodid=0');
            html = html.replace(new RegExp('{dataSales}','g'), menuSales('all', locationid,userid));
            html = html.replace(new RegExp('{dataChurns}','g'), menuChurns('yes', 'all', locationid, userid));
            html = html.replace(new RegExp('{dataNotChurns}','g'), menuNotChurns('yes', 'all', locationid, userid));
            //html = html.replace(new RegExp('{dataMaintenance}','g'), menuMaintenance('yes', 'all', locationid, userid));
            html = html.replace(new RegExp('{dataMaintenance}','g'), 'var dataMaintenance =  {"text":".","children": []};');
            html = html.replace(new RegExp('{dataRenewals}','g'), menuRenewals('yes', 'all', locationid, userid));
            html = html.replace(new RegExp('{menuItems}','g'), 'treeSales,treeRenewals,treeChurns,treeNotChurns,treeActivities');

        }
        else if (roleid == 1007) {
            html = html.replace(new RegExp('{dataActivities}','g'),  menuActivities('yes', 'all', locationid, userid));
            html = html.replace(new RegExp('{defaultChart}','g'),'/app/site/hosting/scriptlet.nl?script=716&deploy=1&departmentid=0&marketid=0&userid=0&period=month&periodid=0');
            html = html.replace(new RegExp('{dataSales}','g'), menuSales('all', locationid,userid));
            html = html.replace(new RegExp('{dataChurns}','g'), menuChurns('yes', 'all', locationid, userid));
            html = html.replace(new RegExp('{dataNotChurns}','g'), menuNotChurns('yes', 'all', locationid, userid));
            html = html.replace(new RegExp('{dataRenewals}','g'), menuRenewals('yes', 'all', locationid, userid));
            html = html.replace(new RegExp('{menuItems}','g'), 'treeSales,treeRenewals, treeChurns,treeNotChurns,treeActivities');
            html = html.replace(new RegExp('{dataMaintenance}','g'), 'var dataMaintenance =  {"text":".","children": []};');
        }// 30/01/2015 give access  Cologix COO (Graham) to Revenue

        else if (roleid == 1006 ||roleid == 1013  || roleid == 1019 || roleid == 1027 || roleid == 1033 || roleid == 1040 || roleid == 1044) {

            html = html.replace(new RegExp('{dataActivities}','g'),  menuActivities('yes', 'all', locationid, userid));
            html = html.replace(new RegExp('{defaultChart}','g'),'/app/site/hosting/scriptlet.nl?script=716&deploy=1&departmentid=0&marketid=0&userid=0&period=month&periodid=0');
            html = html.replace(new RegExp('{dataSales}','g'), menuSales('all', locationid,userid));

            html = html.replace(new RegExp('{dataChurns}','g'), menuChurns('yes', 'all', locationid, userid));
            html = html.replace(new RegExp('{dataRenewals}','g'), menuRenewals('yes', 'all', locationid, userid));
            if((roleid != 1040) &&(roleid != 1044))
            {
                html = html.replace(new RegExp('{dataNotChurns}','g'), menuNotChurns('yes', 'all', locationid, userid));
                html = html.replace(new RegExp('{menuItems}','g'), 'treeSales,treeRenewals, treeChurns,treeNotChurns,treeActivities');
            }
            else
            {
                html = html.replace(new RegExp('{dataNotChurns}','g'), 'var dataNotChurns =  {"text":".","children": []};');
                html = html.replace(new RegExp('{menuItems}','g'), 'treeSales,treeRenewals, treeChurns,treeActivities');
            }
            html = html.replace(new RegExp('{dataMaintenance}','g'), 'var dataMaintenance =  {"text":".","children": []};');

        }//2 Sept 2014 give access  Cologix Product Manager to Revenue, requested by Val
        else if ( roleid == 1009 || roleid == 1021 || roleid == 1017) {
            html = html.replace(new RegExp('{dataActivities}','g'), menuActivities('yes', 'all', locationid, userid));
            html = html.replace(new RegExp('{defaultChart}','g'),'/app/site/hosting/scriptlet.nl?script=716&deploy=1&departmentid=0&marketid=0&userid=0&period=month&periodid=0');
            html = html.replace(new RegExp('{dataSales}','g'), menuSales('all', locationid,userid));
            html = html.replace(new RegExp('{dataChurns}','g'), menuChurns('yes', 'all', locationid, userid));
            html = html.replace(new RegExp('{dataNotChurns}','g'), menuNotChurns('yes', 'all', locationid, userid));
            html = html.replace(new RegExp('{dataRenewals}','g'), menuRenewals('yes', 'all', locationid, userid));
            html = html.replace(new RegExp('{menuItems}','g'), 'treeSales,treeRenewals,treeChurns,treeNotChurns,treeActivities');
            html = html.replace(new RegExp('{dataMaintenance}','g'), 'var dataMaintenance =  {"text":".","children": []};');

        }
        else if (roleid == 1042) {
            html = html.replace(new RegExp('{dataActivities}','g'), menuActivities('no', 'team', locationid, userid));
            //html = html.replace(new RegExp('{defaultChart}','g'),'/app/site/hosting/scriptlet.nl?script=252&deploy=1&departmentid=0&marketid=' + locationid + '&repid=0&period=month&periodid=0');
            html = html.replace(new RegExp('{defaultChart}','g'),'/app/site/hosting/scriptlet.nl?script=716&deploy=1&departmentid=0&marketid=0&userid=0&period=month&periodid=0');
            html = html.replace(new RegExp('{dataSales}','g'), menuSales('team', locationid,userid));
            html = html.replace(new RegExp('{dataChurns}','g'), menuChurns('no', 'team', locationid, userid));
            html = html.replace(new RegExp('{dataNotChurns}','g'), 'var dataNotChurns =  {"text":".","children": []};');
            html = html.replace(new RegExp('{dataRenewals}','g'), menuRenewals('no', 'team', locationid, userid));
            html = html.replace(new RegExp('{menuItems}','g'), 'treeSales,treeRenewals,treeChurns,treeActivities');
            html = html.replace(new RegExp('{dataMaintenance}','g'), 'var dataMaintenance =  {"text":".","children": []};');
        }
        // else if (roleid == 1021) {
        //   html = html.replace(new RegExp('{defaultChart}','g'),'/app/site/hosting/scriptlet.nl?script=211&deploy=1&departmentid=0&marketid=' + locationid + '&userid=0&period=month&periodid=0');
        //  html = html.replace(new RegExp('{dataChurns}','g'), menuChurns('no', 'team', locationid, userid));
        //  html = html.replace(new RegExp('{dataNotChurns}','g'), 'var dataNotChurns =  {"text":".","children": []};');
        //  html = html.replace(new RegExp('{menuItems}','g'), 'treeChurns,treeNotChurns');
        //  html = html.replace(new RegExp('{dataMaintenance}','g'), 'var dataMaintenance =  {"text":".","children": []};');
        //}
        else if (roleid == 1018 || roleid == 1043 || roleid == 1047) {
            html = html.replace(new RegExp('{dataActivities}','g'), menuActivities('no', 'rep', locationid, userid));
            html = html.replace(new RegExp('{defaultChart}','g'),'/app/site/hosting/scriptlet.nl?script=716&deploy=1&departmentid=0&marketid=0&userid=0&period=month&periodid=0');
            //html = html.replace(new RegExp('{defaultChart}','g'),'/app/site/hosting/scriptlet.nl?script=209&deploy=1&departmentid=0&marketid=' + locationid + '&userid=' + userid + '&period=month&periodid=0');
            html = html.replace(new RegExp('{dataSales}','g'), 'var dataSales =  {"text":".","children": []};');
            html = html.replace(new RegExp('{dataChurns}','g'), menuChurns('no', 'rep', locationid, userid));
            html = html.replace(new RegExp('{dataNotChurns}','g'), 'var dataNotChurns =  {"text":".","children": []};');
            html = html.replace(new RegExp('{dataRenewals}','g'), menuRenewals('no', 'rep', locationid, userid));
            html = html.replace(new RegExp('{menuItems}','g'), 'treeRenewals,treeChurns,treeActivities');
            html = html.replace(new RegExp('{dataMaintenance}','g'), 'var dataMaintenance =  {"text":".","children": []};');
        }
        else{
            html = html.replace(new RegExp('{dataActivities}','g'), 'var dataActivities =  {"text":".","children": []};');
            html = html.replace(new RegExp('{defaultChart}','g'),'');
            html = html.replace(new RegExp('{dataSales}','g'), 'var dataSales =  {"text":".","children": []};');
            html = html.replace(new RegExp('{dataChurns}','g'), 'var dataChurns =  {"text":".","children": []};');
            html = html.replace(new RegExp('{dataNotChurns}','g'), 'var dataNotChurns =  {"text":".","children": []};');
            html = html.replace(new RegExp('{dataMaintenance}','g'), 'var dataMaintenance =  {"text":".","children": []};');
            html = html.replace(new RegExp('{dataRenewals}','g'), 'var dataRenewals =  {"text":".","children": []};');
            html = html.replace(new RegExp('{menuItems}','g'), '');
            html = html.replace(new RegExp('{dataMaintenance}','g'), 'var dataMaintenance =  {"text":".","children": []};');
        }
        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function menuSales (type, locationid,userid) {

    switch(type) {


        case 'all': // --------------------------------------------------------- return all tree menu for all markets -------------------------------------------------------

            var stMenu = 'var dataSales =  {"text":".","children": [';

            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("location",null,"noneof",1));
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('location',null,'GROUP'));
            var searchMarkets = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);

            //if(userid==206211 || userid==2406 || userid==211645 || userid==-5 || userid==63 ){
                // Gross Sales Tree Menu
                stMenu += '\n{"report":"Gross Sales", "departmentid":0, "market": "All", "marketid":0, "script": 716, "iconCls":"chart_bar", "expanded":true, "leaf":false, "children":[\n';
                for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                    var searchMarket = searchMarkets[i];
                    var marketid = searchMarket.getValue('location', null, 'GROUP');
                    if(marketid!='')
                    {
                        var market = searchMarket.getText('location', null, 'GROUP');
                        stMenu += '{"report":"' + market + '", "departmentid":0, "market":"' + market + '", "marketid":' + marketid + ', "script": 716, "iconCls":"chart_bar", "leaf":true},\n';
                    }
                }
                var strLen = stMenu.length;
                stMenu = stMenu.slice(0,strLen-1);
                stMenu += ']},\n';
            //}
            // Unadjusted Gross Sales Tree Menu
            stMenu += '\n{"report":"Net Sales", "departmentid":0, "market": "All", "marketid":0, "script": 211, "iconCls":"chart_bar", "expanded":false, "leaf":false, "children":[\n';
            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');
                    stMenu += '{"report":"' + market + '", "departmentid":0, "market":"' + market + '", "marketid":' + marketid + ', "script": 211, "iconCls":"chart_bar", "leaf":true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';

            // Sales Funnel Quarterly Tree Menu
            stMenu += '\n{"report":"Sales Funnel Quarterly", "region":"", "market": "All", "marketid":0, "script": 215, "iconCls":"chart_bar", "expanded":false, "leaf":false, "children":[\n';
            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');
                    stMenu += '{"report":"' + market + '", "departmentid":0, "market":"' + market + '", "marketid":' + marketid + ', "script": 215, "iconCls":"chart_bar", "leaf":true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';

            // Departments Gross Sales Tree Menu
            var objReport = new Object();
            objReport["report"] = 'Departments Gross Sales';
            objReport["region"] = 'Departments Gross Sales';
            objReport["market"] = 'All';
            objReport["marketid"] = 0;
            objReport["department"] = 'Departments';
            objReport["departmentid"] = 9;
            objReport["script"] = 211;
            objReport["iconCls"] = 'chart_bar';
            objReport["expanded"] = false;
            objReport["leaf"] = false;
            objReport["children"] = get_menu_departments (type, 211); // build tree menu function - script 211
            stMenu += JSON.stringify(objReport);

            stMenu += ',';

            // Departments Funnel Quarterly Tree Menu
            var objReport = new Object();
            objReport["report"] = 'Departments Funnel Quarterly';
            objReport["region"] = 'Departments Funnel Quarterly';
            objReport["market"] = 'All';
            objReport["marketid"] = 0;
            objReport["department"] = 'Departments';
            objReport["departmentid"] = 9;
            objReport["script"] = 215;
            objReport["iconCls"] = 'chart_bar';
            objReport["expanded"] = false;
            objReport["leaf"] = false;
            objReport["children"] = get_menu_departments (type, 215); // build tree menu function - script 211
            stMenu += JSON.stringify(objReport);

            stMenu += ']};\n';

            break;

        case 'team': // --------------------------------------------------------- return tree menu for one market only  -------------------------------------------------------

            var stMenu = 'var dataSales =  {"text":".","children": [';
            if(userid==206211 || userid==2406 || userid==211645 || userid==-5 || userid==63){
                // Gross Sales Tree Menu
                stMenu += '{"report":"Gross Sales", "departmentid":0, "market":"' + clgx_return_market_name (locationid) + '", "marketid":' + locationid + ', "script": 716, "iconCls":"chart_bar", "leaf":true},\n';

            }
            stMenu += '{"report":"Net Sales", "departmentid":0, "market":"' + clgx_return_market_name (locationid) + '", "marketid":' + locationid + ', "script": 211, "iconCls":"chart_bar", "leaf":true},\n';
            stMenu += '{"report":"Sales Funnel Quarterly", "departmentid":0, "market":"' + clgx_return_market_name (locationid) + '", "marketid":' + locationid + ', "script": 215, "iconCls":"chart_bar", "leaf":true},\n';
            stMenu += '{"report":"Department Gross Sales", "departmentid":' + departmentid + ', "market":"", "marketid":0, "script": 211, "iconCls":"chart_bar", "leaf":true},\n';
            stMenu += '{"report":"Department Funnel Quarterly", "departmentid":' + departmentid + ', "market":"", "marketid":0, "script": 215, "iconCls":"chart_bar", "leaf":true},\n';
            stMenu += ']};\n';

            break;

        default: // --------------------------------------------------------- return tree menu for one rep only -------------------------------------------------------

            var stMenu = 'var dataSales =  {"text":".","children": []};';


        /*
         case 'all': // --------------------------------------------------------- return all tree menu for all markets -------------------------------------------------------

         var objMenu = new Object();
         objMenu["text"] = '.';

         var arrReports = new Array();

         var objReport = new Object();
         objReport["report"] = 'Net Sales';
         objReport["market"] = 'All';
         objReport["marketid"] = 0;
         objReport["script"] = 211;
         objReport["iconCls"] = 'chart_bar';
         objReport["expanded"] = true;
         objReport["leaf"] = false;
         objReport["children"] = menuRegionsMarkets (211); // build tree menu function - script 211
         arrReports.push(objReport);

         var objReport = new Object();
         objReport["report"] = 'Sales Funnel Quarterly';
         objReport["market"] = 'All';
         objReport["marketid"] = 0;
         objReport["script"] = 215;
         objReport["iconCls"] = 'chart_bar';
         objReport["expanded"] = false;
         objReport["leaf"] = false;
         objReport["children"] = menuRegionsMarkets (215); // build tree menu function - script 215
         arrReports.push(objReport);

         objMenu["children"] = arrReports;

         var stMenu = 'var dataSales =  ' + JSON.stringify(objMenu);

         break;

         case 'team': // --------------------------------------------------------- return tree menu for one market only  -------------------------------------------------------

         var stMenu = 'var dataSales =  {"text":".","children": [';
         stMenu += '{report:"Net Sales", "market":"' + clgx_return_market_name (locationid) + '", "marketid":' + locationid + ', "script": 211, "iconCls":"chart_bar", "leaf":true},\n';
         stMenu += '{report:"Sales Funnel Quarterly", "market":"' + clgx_return_market_name (locationid) + '", "marketid":' + locationid + ', "script": 215, "iconCls":"chart_bar", "leaf":true},\n';
         stMenu += ']};\n';

         break;

         default: // --------------------------------------------------------- return tree menu for one rep only -------------------------------------------------------

         var stMenu = 'var dataSales =  {"text":".","children": []};';
         */

    }

    // nlapiSendEmail(206211,206211,'menuSales','menuSales',null,null,null,null);
    return stMenu;
}



function menuChurns (admin, type, locationid, userid) {

    switch(type) {
        case 'all': // --------------------------------------------------------- return all tree menu for all markets -------------------------------------------------------

            var stMenu = 'var dataChurns =  {"text":".","children": [';

            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("location",null,"noneof",1));
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('location',null,'GROUP'));
            var searchMarkets = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);


            //stMenu += '\n{type: "market", entity:"Churn Funnel Quarterly", market: "All", marketid:0,script: 319, repid: 0, iconCls:"chart_bar", expanded:true, leaf:false, children:[\n';
            stMenu += '\n{entity:"Churn Funnel Quarterly", market: "All", marketid:0, script: 319, iconCls:"chart_bar", expanded:true, leaf:false, children:[\n';

            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');
                    stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:319, iconCls:"chart_bar", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';
            stMenu += '\n{type: "market", script:318,entity:"Markets", marketid:0, repid: 0, iconCls:"facility", expanded:false, leaf:false, children:[\n';
            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');
                    stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:318, iconCls:"facility", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';


            var arrFilters = new Array();
            var arrColumns = new Array();
            var searchReps = nlapiSearchRecord('customrecord_cologix_churn', 'customsearch_clgx_churn_salesrep', arrFilters, arrColumns);

            stMenu += '\n{type: "reps", entity:"Sales Reps", marketid:0, repid:0, iconCls:"group", expanded:false,script:318, leaf:false, children:[\n';

            for ( var i = 0; searchReps != null && i < searchReps.length; i++ ) {
                var searchRep = searchReps[i];
                var columns = searchRep.getAllColumns();
                var salesrepid = searchRep.getValue(columns[0]);
                var salesrepname = searchRep.getText(columns[0]);
                if(salesrepid!=''){
                    stMenu += '{type: "reps", entity:"' + salesrepname + '", marketid:0, repid:' + salesrepid + ', script:318, iconCls:"user_suit", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';

            // Churn Lost
            stMenu += '\n{type: "market", script:320,entity:"Churn Report - Lost", marketid:0, repid: 0, iconCls:"chart_bar", expanded:false, leaf:false, children:[\n'
            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');
                    stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:320, iconCls:"chart_bar", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']}\n';
            //Churn Lost-END

            stMenu += ']};\n';
            break;

        case 'team': // --------------------------------------------------------- return all tree menu for one market -------------------------------------------------------

            var stMenu = 'var dataChurns =  {"text":".","children": [';

            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("location",null,"anyof",locationid));
            arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",71418));
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('location',null,'GROUP'));
            var searchMarkets = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);

            stMenu += '\n{type: "market", entity:"Churn Funnel Quarterly", marketid:0, repid: 0, iconCls:"chart_bar", expanded:true, leaf:false, children:[\n';
            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');
                    stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:319, iconCls:"chart_bar", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';

            stMenu += '\n{type: "market", entity:"Markets", marketid:0, repid: 0, iconCls:"facility", expanded:false, leaf:false, children:[\n';
            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');
                    stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:319, iconCls:"facility", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';

            var arrFilters = new Array();
            //	arrFilters.push(new nlobjSearchFilter("location",null,"anyof",clgx_return_child_locations_of_marketid(locationid)));
            var arrColumns = new Array();
            var searchReps = nlapiSearchRecord('customrecord_cologix_churn', 'customsearch_clgx_churn_salesrep', arrFilters, arrColumns);

            stMenu += '\n{type: "reps", entity:"Sales Reps", marketid:0, repid:0, iconCls:"group", expanded:false, leaf:false, children:[\n';
            for ( var i = 0; searchReps != null && i < searchReps.length; i++ ) {
                var searchRep = searchReps[i];
                var columns = searchRep.getAllColumns();
                var salesrepid = searchRep.getValue(columns[0]);
                var salesrepname = searchRep.getText(columns[0]);
                if(salesrepid!=''){
                    if (userid == salesrepid){
                        stMenu += '{type: "reps", entity:"' + salesrepname + '", marketid:0, repid:' + salesrepid + ', script:318, iconCls:"user_suit", leaf:true},\n';
                    }
                    else{
                    }
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';
            // Churn Lost
            stMenu += '\n{type: "market", script:320,entity:"Churn Report - Lost", marketid:0, repid: 0, iconCls:"lost_bar", expanded:false, leaf:false, children:[\n'
            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');
                    stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:319, iconCls:"chart_bar", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']}\n';
            //Churn Lost-END
            stMenu += ']};\n';

            break;


        case 'rep': // --------------------------------------------------------- return tree menu for one rep only -------------------------------------------------------

            var stMenu = 'var dataChurns  =  {"text":".","children": [';

            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_churn_salesrep",null,"anyof",userid));
            var arrColumns = new Array();
            var searchReps = nlapiSearchRecord('customrecord_cologix_churn', 'customsearch_clgx_churn_salesrep', arrFilters, arrColumns);

            stMenu += '\n{type: "reps", entity:"Sales Reps", script:318,marketid:0, repid:'+ userid +', iconCls:"group", expanded:true, leaf:false, children:[\n';
            for ( var i = 0; searchReps != null && i < searchReps.length; i++ ) {
                var searchRep = searchReps[i];
                var columns = searchRep.getAllColumns();
                var salesrepid = searchRep.getValue(columns[0]);
                var salesrepname = searchRep.getText(columns[0]);
                if(salesrepid!=''){
                    stMenu += '{type: "reps", entity:"' + salesrepname + '", marketid:0, repid:' + salesrepid + ', script:318, iconCls:"user_suit", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']}\n';

            stMenu += ']};\n';

            break;



        default: // --------------------------------------------------------- return empty tree -------------------------------------------------------

            var stMenu = 'var dataChurns =  {"text":".","children": []};';
    }
    //   nlapiSendEmail(206211,206211,'menuChurns','menuSales',null,null,null,null);
    return stMenu;
}

function menuNotChurns (admin, type, locationid, userid) {

    switch(type) {
        case 'all': // --------------------------------------------------------- return all tree menu for all markets -------------------------------------------------------

            var stMenu = 'var dataNotChurns =  {"text":".","children": [';

            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("location",null,"noneof",1));
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('location',null,'GROUP'));
            var searchMarkets = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);


            //stMenu += '\n{type: "market", entity:"Churn Funnel Quarterly", market: "All", marketid:0,script: 319, repid: 0, iconCls:"chart_bar", expanded:true, leaf:false, children:[\n';

            //02-09-2014 Change the name of report- implemented by Catalina, requested by Val
            /* stMenu += '\n{entity:"Adjusted Gross MRR", market: "All", marketid:0, script: 445, iconCls:"chart_bar", expanded:true, leaf:false, children:[\n';
             //
             for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
             var searchMarket = searchMarkets[i];
             var marketid = searchMarket.getValue('location', null, 'GROUP');
             if(marketid!='')
             {
             var market = searchMarket.getText('location', null, 'GROUP');
             stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:445, iconCls:"chart_bar", leaf:true},\n';
             }
             }
             var strLen = stMenu.length;
             stMenu = stMenu.slice(0,strLen-1);
             stMenu += ']},\n';*/
            /* stMenu += '\n{type: "market", script:317,entity:"Markets", marketid:0, repid: 0, iconCls:"facility", expanded:true, leaf:false, children:[\n'
             for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
             var searchMarket = searchMarkets[i];
             var marketid = searchMarket.getValue('location', null, 'GROUP');
             var market = searchMarket.getText('location', null, 'GROUP');
             stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:317, iconCls:"facility", leaf:true},\n';
             }
             var strLen = stMenu.length;
             stMenu = stMenu.slice(0,strLen-1);
             stMenu += ']},\n';*/
            //Net Change in MRR
            stMenu += '\n{entity:"Net Change in MRR", market: "All", marketid:0, script: 444, iconCls:"chart_bar", expanded:true, leaf:false, children:[\n';

            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');
                    stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:444, iconCls:"chart_bar", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';
            //02-09-2014 Change the name of report- implemented by Catalina, requested by Val
            stMenu += '\n{entity:"MRR by Category", market: "All", marketid:0, script: 446, iconCls:"chart_bar", expanded:false, leaf:false, children:[\n';
            //
            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');
                    stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:446, iconCls:"chart_bar", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';


            stMenu += ']};\n';
            break;

        case 'team': // --------------------------------------------------------- return all tree menu for one market -------------------------------------------------------

            var stMenu = 'var dataNotChurns =  {"text":".","children": [';

            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("location",null,"anyof",locationid));
            arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",71418));
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('location',null,'GROUP'));
            var searchMarkets = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);

            /*   stMenu += '\n{type: "market", entity:"Adjusted Gross MRR", marketid:0, repid: 0, iconCls:"chart_bar", expanded:true, leaf:false, children:[\n';
             for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
             var searchMarket = searchMarkets[i];
             var marketid = searchMarket.getValue('location', null, 'GROUP');
             if(marketid!='')
             {
             var market = searchMarket.getText('location', null, 'GROUP');
             stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:445, iconCls:"chart_bar", leaf:true},\n';
             }
             }
             var strLen = stMenu.length;
             stMenu = stMenu.slice(0,strLen-1);
             stMenu += ']},\n';

             /* stMenu += '\n{type: "market", script:317,entity:"Markets", marketid:0, repid: 0, iconCls:"facility", expanded:true, leaf:false, children:[\n'
             for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
             var searchMarket = searchMarkets[i];
             var marketid = searchMarket.getValue('location', null, 'GROUP');
             var market = searchMarket.getText('location', null, 'GROUP');
             stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:317, iconCls:"facility", leaf:true},\n';
             }
             var strLen = stMenu.length;
             stMenu = stMenu.slice(0,strLen-1);
             stMenu += ']},\n';*/
            //Net Change in MRR
            stMenu += '\n{entity:"Net Change in MRR", market: "All", marketid:0, script: 444, iconCls:"chart_bar", expanded:true, leaf:false, children:[\n';
            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');
                    stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:444, iconCls:"chart_bar", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';
            stMenu += '\n{entity:"Revenue per Category", market: "All", marketid:0, script: 446, iconCls:"chart_bar", expanded:false, leaf:false, children:[\n';

            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');
                    stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:446, iconCls:"chart_bar", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';






            stMenu += ']};\n';
            break;
        default: // --------------------------------------------------------- return empty tree -------------------------------------------------------

            var stMenu = 'var dataNotChurns =  {"text":".","children": []};';
    }
    // nlapiSendEmail(206211,206211,'menuNotChurns','menuSales',null,null,null,null);
    return stMenu;
}
function menuMaintenance (admin, type, locationid, userid) {

    switch(type) {
        case 'all': // --------------------------------------------------------- return all tree menu for all markets -------------------------------------------------------

            var stMenu = 'var dataMaintenance =  {"text":".","children": [';
            var arrFilters = new Array();
            var arrColumns = new Array();
            var searchMarkets = nlapiSearchRecord('customrecord_cologix_facility', 'customsearch_clgx_ss_facility_main_2', arrFilters, arrColumns);
            stMenu += '\n{entity:"All Markets", market: "All", marketid:0, script: 473, repid:0, iconCls:"chart_bar", expanded:true, leaf:false, children:[\n';
            //
            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var columns = searchMarket.getAllColumns();
                var marketid = searchMarket.getValue(columns[0]);
                if(marketid!='')
                {
                    var market = searchMarket.getText(columns[0]);
                    stMenu += '{type: "market", entity:"' + market + '", marketid:0, repid:' + marketid +', script:473, iconCls:"chart_bar", leaf:false, children:[\n';
                    var arrFilters1 = new Array();
                    var arrColumns1 = new Array();
                    arrFilters1.push(new nlobjSearchFilter("custrecord_clgx_facilty_market",null,"anyof",marketid));
                    var searchMarkets1 = nlapiSearchRecord('customrecord_cologix_facility', 'customsearch_clgx_ss_facility_main', arrFilters1, arrColumns1);
                    for ( var k = 0; searchMarkets1 != null && k < searchMarkets1.length; k++ ) {
                        var searchMarket1 = searchMarkets1[k];
                        var columns1 = searchMarket1.getAllColumns();
                        var idFac=searchMarket1.getValue(columns1[0]);
                        var nameFac = searchMarket1.getValue(columns1[1]);
                        stMenu += '{type: "market", entity:"' +  nameFac + '", marketid:' + idFac + ', repid:0, script:473, iconCls:"chart_bar", leaf:true},\n';
                    }
                    stMenu +=']},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';







            stMenu += ']};\n';
            break;

        case 'team': // --------------------------------------------------------- return all tree menu for one market -------------------------------------------------------

            var stMenu = 'var dataMaintenance =  {"text":".","children": [';
            var arrFilters = new Array();
            var arrColumns = new Array();
            var searchMarkets = nlapiSearchRecord('customrecord_cologix_facility', 'customsearch_clgx_ss_facility_main_2', arrFilters, arrColumns);
            stMenu += '\n{entity:"All Markets", market: "All", marketid:0, script: 473, repid:0, iconCls:"chart_bar", expanded:true, leaf:false, children:[\n';
            //
            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var columns = searchMarket.getAllColumns();
                var marketid = searchMarket.getValue(columns[0]);
                if(marketid!='')
                {
                    var market = searchMarket.getText(columns[0]);
                    stMenu += '{type: "market", entity:"' + market + '", marketid:0, repid:' + marketid +', script:473, iconCls:"chart_bar", leaf:false, children:[\n';
                    var arrFilters1 = new Array();
                    var arrColumns1 = new Array();
                    arrFilters1.push(new nlobjSearchFilter("custrecord_clgx_facilty_market",null,"anyof",marketid));
                    var searchMarkets1 = nlapiSearchRecord('customrecord_cologix_facility', 'customsearch_clgx_ss_facility_main', arrFilters1, arrColumns1);
                    for ( var k = 0; searchMarkets1 != null && k < searchMarkets1.length; k++ ) {
                        var searchMarket1 = searchMarkets1[k];
                        var columns1 = searchMarket1.getAllColumns();
                        var idFac=searchMarket1.getValue(columns1[0]);
                        var nameFac = searchMarket1.getValue(columns1[1]);
                        stMenu += '{type: "market", entity:"' +  nameFac + '", marketid:' + idFac + ', repid:0, script:473, iconCls:"chart_bar", leaf:true},\n';
                    }
                    stMenu +=']},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';







            stMenu += ']};\n';
            break;
        default: // --------------------------------------------------------- return empty tree -------------------------------------------------------

            var stMenu = 'var dataMaintenance =  {"text":".","children": []};';
    }
    // nlapiSendEmail(206211,206211,'menuMain','menuSales',null,null,null,null);
    return stMenu;
}

function menuActivities (admin, type, locationid, userid) {

    var arrExclude = [294,279321,480345,71418,115655,2347,4274,6857,213255,323586,339721,501436,387984,394031,390070,3118,530873,1127523,600791,1251296];

    switch(type) {
        case 'all': // --------------------------------------------------------- return all tree menu for all markets -------------------------------------------------------
            var stMenu = 'var dataActivities =  {"text":".","children": [\n';
            stMenu += '{report:"All Markets", market:"All", marketid:0, userid:0, script: 208, period:"month", periodid:0, expanded:true, iconCls:"calendar", leaf:false, children:[\n' +
                '{report:"This week", market:"All", marketid:0, userid:0, script: 208, period:"week", periodid:0, iconCls:"chart_bar", leaf:true},\n' +
                '{report:"Last Week", market:"All", marketid:0, userid:0, script: 208, period:"week", periodid:1, iconCls:"chart_bar", leaf:true},\n' +
                '{report:"2 Weeks Ago", market:"All", marketid:0, userid:0, script: 208, period:"week", periodid:2, iconCls:"chart_bar", leaf:true},\n' +
                '{report:"3 Weeks Ago", market:"All", marketid:0, userid:0, script: 208, period:"week", periodid:3, iconCls:"chart_bar", leaf:true},\n' +
                '{report:"4 Weeks Ago", market:"All", marketid:0, userid:0, script: 208, period:"week", periodid:4, iconCls:"chart_bar", leaf:true}\n' +
                ']},\n';

            var arrFilters = new Array();
            if (admin != 'yes'){
                arrFilters.push(new nlobjSearchFilter("location",null,"noneof",1));
            }
            arrFilters.push(new nlobjSearchFilter("location",null,"noneof",['1','14']));
            arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",arrExclude));
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('location',null,'GROUP'));
            var searchMarkets = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);

            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');

                    stMenu += '\n{report:"' + market + '", market:"' + market + '", marketid:' + marketid + ', userid:0, script: 208, period:"month", periodid:-1, expanded:false, iconCls:"group", leaf:false, children:[\n' +
                        '{report:"Current Month", market:"' + market + '", marketid:' + marketid + ', userid:0, script: 208, period:"month", periodid:0, expanded:false, iconCls:"calendar", leaf:false, children:[\n' +
                        '{report:"This week", market:"' + market + '", marketid:' + marketid + ', userid:0, script: 208, period:"week", periodid:0, iconCls:"chart_bar", leaf:true},\n' +
                        '{report:"Last Week", market:"' + market + '", marketid:' + marketid + ', userid:0, script: 208, period:"week", periodid:1, iconCls:"chart_bar", leaf:true},\n' +
                        '{report:"2 Weeks Ago", market:"' + market + '", marketid:' + marketid + ', userid:0, script: 208, period:"week", periodid:2, iconCls:"chart_bar", leaf:true},\n' +
                        '{report:"3 Weeks Ago", market:"' + market + '", marketid:' + marketid + ', userid:0, script: 208, period:"week", periodid:3, iconCls:"chart_bar", leaf:true},\n' +
                        '{report:"4 Weeks Ago", market:"' + market + '", marketid:' + marketid + ', userid:0, script: 208, period:"week", periodid:4, iconCls:"chart_bar", leaf:true}\n' +
                        ']},\n';

                    var arrFilters = new Array();
                    arrFilters.push(new nlobjSearchFilter("location",null,"anyof",parseInt(marketid)));
                    arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
                    arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",arrExclude));
                    var arrColumns = new Array();
                    arrColumns.push(new nlobjSearchColumn('location',null,null));
                    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
                    arrColumns.push(new nlobjSearchColumn('firstname',null,null));
                    arrColumns.push(new nlobjSearchColumn('lastname',null,null));
                    var searchReps = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);


                    for ( var j = 0; searchReps != null && j < searchReps.length; j++ ) {
                        var searchRep = searchReps[j];
                        var userid = searchRep.getValue('internalid', null, null);
                        var userFirstName = searchRep.getValue('firstname', null, null);
                        var userLastName = searchRep.getValue('lastname', null, null);

                        stMenu += '\n{report:"' + userFirstName + ' ' + userLastName + '", market:"' + market + '", marketid:' + marketid + ', userid:' + userid + ', script: 209, period:"month", periodid:0, expanded:false, iconCls:"user", leaf:false, children:[\n' +
                            '{report:"This week", market:"' + market + '", marketid:' + marketid + ', userid:' + userid + ', script: 209, period:"week", periodid:0, iconCls:"chart_bar", leaf:true},\n' +
                            '{report:"Last Week", market:"' + market + '", marketid:' + marketid + ', userid:' + userid + ', script: 209, period:"week", periodid:1, iconCls:"chart_bar", leaf:true},\n' +
                            '{report:"2 Weeks Ago", market:"' + market + '", marketid:' + marketid + ', userid:' + userid + ', script: 209, period:"week", periodid:2, iconCls:"chart_bar", leaf:true},\n' +
                            '{report:"3 Weeks Ago", market:"' + market + '", marketid:' + marketid + ', userid:' + userid + ', script: 209, period:"week", periodid:3, iconCls:"chart_bar", leaf:true},\n' +
                            '{report:"4 Weeks Ago", market:"' + market + '", marketid:' + marketid + ', userid:' + userid + ', script: 209, period:"week", periodid:4, iconCls:"chart_bar", leaf:true}\n' +
                            ']},';
                    }
                    var strLen = stMenu.length;
                    stMenu = stMenu.slice(0,strLen-1);
                    stMenu += ']},';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']};\n';
            return stMenu;

            break;

        case 'team': // --------------------------------------------------------- return tree menu for one market only  -------------------------------------------------------

            var locationName = clgx_return_market_name (locationid);

            var stMenu = 'var dataActivities =  {"text":".","children": [\n';
            stMenu += '\n{report:"Current Month", market:"' + locationName + '", marketid:' + locationid + ', userid:0, script: 208, period:"month", periodid:0, expanded:true, iconCls:"calendar", leaf:false, children:[\n' +
                '{report:"This week", market:"' + locationName + '", marketid:' + locationid + ', userid:0, script: 208, period:"week", periodid:0, iconCls:"chart_bar", leaf:true},\n' +
                '{report:"Last Week", market:"' + locationName + '", marketid:' + locationid + ', userid:0, script: 208, period:"week", periodid:1, iconCls:"chart_bar", leaf:true},\n' +
                '{report:"2 Weeks Ago", market:"' + locationName + '", marketid:' + locationid + ', userid:0, script: 208, period:"week", periodid:2, iconCls:"chart_bar", leaf:true},\n' +
                '{report:"3 Weeks Ago", market:"' + locationName + '", marketid:' + locationid + ', userid:0, script: 208, period:"week", periodid:3, iconCls:"chart_bar", leaf:true},\n' +
                '{report:"4 Weeks Ago", market:"' + locationName + '", marketid:' + locationid + ', userid:0, script: 208, period:"week", periodid:4, iconCls:"chart_bar", leaf:true}\n' +
                ']},\n';

            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("location",null,"anyof",parseInt(locationid)));
            arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
            arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",arrExclude));
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('internalid',null,null));
            arrColumns.push(new nlobjSearchColumn('firstname',null,null));
            arrColumns.push(new nlobjSearchColumn('lastname',null,null));
            var searchReps = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);


            for ( var i = 0; searchReps != null && i < searchReps.length; i++ ) {
                var searchRep = searchReps[i];
                var userid = searchRep.getValue('internalid', null, null);
                var userFirstName = searchRep.getValue('firstname', null, null);
                var userLastName = searchRep.getValue('lastname', null, null);

                stMenu += '\n{report:"' + userFirstName + ' ' + userLastName + '", market:"' + locationName + '", marketid:' + locationid + ', userid:' + userid + ', script: 209, period:"month", periodid:0, expanded:false, iconCls:"user", leaf:false, children:[\n' +
                    '{report:"This week", market:"' + locationName + '", marketid:' + locationid + ', userid:' + userid + ', script: 209, period:"week", periodid:0, iconCls:"chart_bar", leaf:true},\n' +
                    '{report:"Last Week", market:"' + locationName + '", marketid:' + locationid + ', userid:' + userid + ', script: 209, period:"week", periodid:1, iconCls:"chart_bar", leaf:true},\n' +
                    '{report:"2 Weeks Ago", market:"' + locationName + '", marketid:' + locationid + ', userid:' + userid + ', script: 209, period:"week", periodid:2, iconCls:"chart_bar", leaf:true},\n' +
                    '{report:"3 Weeks Ago", market:"' + locationName + '", marketid:' + locationid + ', userid:' + userid + ', script: 209, period:"week", periodid:3, iconCls:"chart_bar", leaf:true},\n' +
                    '{report:"4 Weeks Ago", market:"' + locationName + '", marketid:' + locationid + ', userid:' + userid + ', script: 209, period:"week", periodid:4, iconCls:"chart_bar", leaf:true}\n' +
                    ']},';
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']};\n';
            return stMenu;

            break;

        default: // --------------------------------------------------------- return tree menu for one rep only -------------------------------------------------------

            var locationName = clgx_return_market_name (locationid);
            var stMenu = 'var dataActivities =  {"text":".","children": [';
            stMenu += '\n{report:"Activity (Current Month)", market:"' + locationName + '", marketid:' + locationid + ', userid:' + userid + ', script: 209, period:"month", periodid:0, expanded:true, iconCls:"calendar", leaf:false, children:[\n' +
                '{report:"This week", market:"' + locationName + '", marketid:' + locationid + ', userid:' + userid + ', script: 209, period:"week", periodid:0, iconCls:"chart_bar", leaf:true},\n' +
                '{report:"Last Week", market:"' + locationName + '", marketid:' + locationid + ', userid:' + userid + ', script: 209, period:"week", periodid:1, iconCls:"chart_bar", leaf:true},\n' +
                '{report:"2 Weeks Ago", market:"' + locationName + '", marketid:' + locationid + ', userid:' + userid + ', script: 209, period:"week", periodid:2, iconCls:"chart_bar", leaf:true},\n' +
                '{report:"3 Weeks Ago", market:"' + locationName + '", marketid:' + locationid + ', userid:' + userid + ', script: 209, period:"week", periodid:3, iconCls:"chart_bar", leaf:true},\n' +
                '{report:"4 Weeks Ago", market:"' + locationName + '", marketid:' + locationid + ', userid:' + userid + ', script: 209, period:"week", periodid:4, iconCls:"chart_bar", leaf:true}\n' +
                ']}\n' +
                ']};\n';
            //  nlapiSendEmail(206211,206211,'menuAct','menuSales',null,null,null,null);
            return stMenu;
    }
}

function menuRenewals (admin, type, locationid, userid) {

    switch(type) {
        case 'all': // --------------------------------------------------------- return all tree menu for all markets -------------------------------------------------------

            var stMenu = 'var dataRenewals =  {"text":".","children": [';

            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("location",null,"noneof",1));
            arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",71418));
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('location',null,'GROUP'));
            var searchMarkets = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);

            stMenu += '\n{type: "market", entity:"Expiring", marketid:0, repid: 0, iconCls:"chart_bar", expanded:true, leaf:false, children:[\n';
            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');
                    stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:254, iconCls:"chart_bar", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';

            stMenu += '\n{type: "market", entity:"Markets", marketid:0, repid: 0, iconCls:"facility", expanded:false, leaf:false, children:[\n'
            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');
                    stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:252, iconCls:"facility", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';

            var arrFilters = new Array();
            var arrColumns = new Array();
            var searchReps = nlapiSearchRecord('transaction', 'customsearch_clgx_chrt_renewals_reps', arrFilters, arrColumns);

            stMenu += '\n{type: "reps", entity:"Sales Reps", marketid:0, repid:0, iconCls:"group", expanded:false, leaf:false, children:[\n';

            for ( var i = 0; searchReps != null && i < searchReps.length; i++ ) {
                var searchRep = searchReps[i];
                var columns = searchRep.getAllColumns();
                var repname = searchRep.getText('salesrep', 'customerMain', 'GROUP');
                var repid = searchRep.getValue('salesrep', 'customerMain', 'GROUP');
                var sos = searchRep.getValue('internalid', null, 'COUNT');
                var sos30 = searchRep.getValue(columns[2]);
                stMenu += '{type: "reps", entity:"' + repname + '", marketid:0, repid:' + repid + ', script:252, iconCls:"user_suit", leaf:true},\n';
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']}\n';


            stMenu += ']};\n';
            break;

        case 'team': // --------------------------------------------------------- return all tree menu for one market -------------------------------------------------------

            var stMenu = 'var dataRenewals =  {"text":".","children": [';

            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("location",null,"anyof",locationid));
            arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",71418));
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('location',null,'GROUP'));
            var searchMarkets = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);

            stMenu += '\n{type: "market", entity:"Expiring", marketid:0, repid: 0, iconCls:"chart_bar", expanded:true, leaf:false, children:[\n';
            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');
                    stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:254, iconCls:"chart_bar", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';

            stMenu += '\n{type: "market", entity:"Markets", marketid:0, repid: 0, iconCls:"facility", expanded:false, leaf:false, children:[\n';
            for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
                var searchMarket = searchMarkets[i];
                var marketid = searchMarket.getValue('location', null, 'GROUP');
                if(marketid!='')
                {
                    var market = searchMarket.getText('location', null, 'GROUP');
                    stMenu += '{type: "market", entity:"' + market + '", marketid:' + marketid + ', repid:0, script:252, iconCls:"facility", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']},\n';

            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("location",null,"anyof",clgx_return_child_locations_of_marketid(locationid)));
            var arrColumns = new Array();
            var searchReps = nlapiSearchRecord('transaction', 'customsearch_clgx_chrt_renewals_reps', arrFilters, arrColumns);

            stMenu += '\n{type: "reps", entity:"Sales Reps", marketid:0, repid:0, iconCls:"group", expanded:false, leaf:false, children:[\n';
            for ( var i = 0; searchReps != null && i < searchReps.length; i++ ) {
                var searchRep = searchReps[i];
                var columns = searchRep.getAllColumns();
                var repname = searchRep.getText('salesrep', 'customerMain', 'GROUP');
                var repid = searchRep.getValue('salesrep', 'customerMain', 'GROUP');
                var sos = searchRep.getValue('internalid', null, 'COUNT');
                var sos30 = searchRep.getValue(columns[2]);
                if (userid == repid){
                    stMenu += '{type: "reps", entity:"' + repname + '", marketid:0, repid:' + repid + ', script:252, iconCls:"user_suit", leaf:true},\n';
                }
                else{
                    stMenu += '{type: "reps", entity:"' + repname + '", marketid:' + locationid + ', repid:' + repid + ', script:252, iconCls:"user_suit", leaf:true},\n';
                }
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']}\n';
            stMenu += ']};\n';

            break;


        case 'rep': // --------------------------------------------------------- return tree menu for one rep only -------------------------------------------------------

            var stMenu = 'var dataRenewals =  {"text":".","children": [';

            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("salesrep",'customerMain',"anyof",userid));
            var arrColumns = new Array();
            var searchReps = nlapiSearchRecord('transaction', 'customsearch_clgx_chrt_renewals_reps', arrFilters, arrColumns);

            stMenu += '\n{type: "reps", entity:"Sales Reps", marketid:0, repid:0, iconCls:"group", expanded:true, leaf:false, children:[\n';
            for ( var i = 0; searchReps != null && i < searchReps.length; i++ ) {
                var searchRep = searchReps[i];
                var columns = searchRep.getAllColumns();
                var repname = searchRep.getText('salesrep', 'customerMain', 'GROUP');
                var repid = searchRep.getValue('salesrep', 'customerMain', 'GROUP');
                var sos = searchRep.getValue('internalid', null, 'COUNT');
                var sos30 = searchRep.getValue(columns[2]);
                stMenu += '{type: "reps", entity:"' + repname + '", marketid:0, repid:' + repid + ', script:252, iconCls:"user_suit", leaf:true},\n';
            }
            var strLen = stMenu.length;
            stMenu = stMenu.slice(0,strLen-1);
            stMenu += ']}\n';
            stMenu += ']};\n';

            break;



        default: // --------------------------------------------------------- return empty tree -------------------------------------------------------

            var stMenu = 'var dataRenewals =  {"text":".","children": []};';
    }
    //nlapiSendEmail(206211,206211,'menuAct','menuSales',null,null,null,null);

    return stMenu;
}

function get_menu_departments (type, script, userid) {


    var arrFilters = new Array();
    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    var searchEmployees = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);
    var arrEmployees = new Array();
    for ( var i = 0; searchEmployees != null && i < searchEmployees.length; i++ ) {
        arrEmployees.push(searchEmployees[i].getValue('internalid', null, null));
    }

    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrEmployees));
    var arrColumns = new Array();
    //arrColumns.push(new nlobjSearchColumn('department',null,'GROUP'));
    //arrColumns.push(new nlobjSearchColumn('departmentnohierarchy',null,'GROUP'));
    var searchDepartments = nlapiSearchRecord('employee', 'customsearch_clgx_kpi_sales_reps_departm', arrFilters, arrColumns);

    var arrDepartments = new Array();
    for ( var i = 0; searchDepartments != null && i < searchDepartments.length; i++ ) {
        var searchDepartment = searchDepartments[i];
        var columns = searchDepartment.getAllColumns();
        var objDepartment = new Object();
        objDepartment["region"] = searchDepartment.getValue(columns[0]);
        objDepartment["department"] = searchDepartment.getValue('departmentnohierarchy', null, 'GROUP');
        objDepartment["departmentid"] = parseInt(searchDepartment.getValue('department', null, 'GROUP'));
        arrDepartments.push(objDepartment);
    }
    
    nlapiLogExecution("DEBUG", "departments", arrDepartments);

    var arrRegs = [{"region":"Canada","regionid": 23},{"region":"Cross Market","regionid": 24},{"region":"US North","regionid": 25},{"region":"US South","regionid": 26}];

    var arrRegions = new Array();
    for ( var i = 0; arrRegs != null && i < arrRegs.length; i++ ) {

        var objRegion = new Object();
        objRegion["report"] = arrRegs[i].region;
        objRegion["region"] = arrRegs[i].region;
        objRegion["regionid"] = arrRegs[i].regionid;
        objRegion["market"] = '';
        objRegion["marketid"] = 0;
        objRegion["department"] = arrRegs[i].region;
        objRegion["departmentid"] = arrRegs[i].regionid;
        objRegion["script"] = script;
        objRegion["iconCls"] = 'chart_bar';
        objRegion["expanded"] = false;
        objRegion["leaf"] = false;

        var arrRegDeparts = _.filter(arrDepartments, function(arr){
            return (arr.region == arrRegs[i].region);
        });
        var arrRegionDepartments = new Array();
        for ( var j = 0; arrRegDeparts != null && j < arrRegDeparts.length; j++ ) {
            var objRegionDepartment = new Object();
            objRegionDepartment["report"] = arrRegDeparts[j].department;
            objRegionDepartment["region"] = arrRegDeparts[j].region;
            objRegionDepartment["market"] = '';
            objRegionDepartment["marketid"] = 0;
            objRegionDepartment["department"] = arrRegDeparts[j].department;
            objRegionDepartment["departmentid"] = arrRegDeparts[j].departmentid;
            objRegionDepartment["script"] = script;
            objRegionDepartment["iconCls"] = 'chart_bar';
            objRegionDepartment["leaf"] = true;
            arrRegionDepartments.push(objRegionDepartment);
        }
        objRegion["children"] = arrRegionDepartments;

        arrRegions.push(objRegion);

    }

    return arrRegions;
}