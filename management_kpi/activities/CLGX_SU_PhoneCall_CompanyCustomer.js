/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 Sep 2017     alex.guzenski
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */

function beforeSubmit(type) {
	try {
		var organizer     = nlapiGetFieldValue("assigned");
		var attendeeCount = nlapiGetLineItemCount("contact");
		var contact       = -1;
		
		for(var a = 1; a <= attendeeCount; a++) {
			contact = nlapiGetLineItemValue("contact", "contact", a);
			
			if(organizer != contact) {
				try {
					nlapiSetFieldValue("company", nlapiGetLineItemValue("contact", "company", a));
					nlapiSetFieldValue("contact", nlapiGetLineItemValue("contact", "contact", a));
					
					break;
				} catch(ex) {
					nlapiLogExecution("ERROR", "Error #2", ex);
					continue;
				}
			}
		}
	} catch(ex) {
		nlapiLogExecution("ERROR", "Error #1", ex);
	}
}