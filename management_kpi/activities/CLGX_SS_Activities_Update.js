//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Activities_Update.js
//	Script Name:	CLGX_SS_Activities_Update
//	Script Id:		customscript_clgx_ss_activities_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_activities_update(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();

        var record = nlapiCreateRecord('customrecord_cologix_sales_activities');
	        record.setFieldValue('custrecordclgx_sales_activ_id', 1);
	        record.setFieldValue('custrecord_clgx_sales_activ_type', 'test');
	        record.setFieldValue('custrecord_clgx_sales_activ_date', 'test');
	        record.setFieldValue('custrecord_clgx_sales_activ_market_id', 1);
	        record.setFieldValue('custrecord_clgx_sales_activ_market', 'test');
	        record.setFieldValue('custrecord_clgx_sales_activ_location_id', 1);
	        record.setFieldValue('custrecord_clgx_sales_activ_location', 'test');
	        record.setFieldValue('custrecord_clgx_sales_activ_rep_id', 1);
	        record.setFieldValue('custrecord_clgx_sales_activ_rep_name', 'test');
	        record.setFieldValue('custrecord_clgx_sales_activ_customer_id', 1);
	        record.setFieldValue('custrecord_clgx_sales_activ_customer', 'test');
	        record.setFieldValue('custrecord_clgx_sales_activ_contact_id', 1);
	        record.setFieldValue('custrecord_clgx_sales_activ_contact', 'test');
	        record.setFieldValue('custrecord_clgx_sales_activ_custom_type', 'test');
	        record.setFieldValue('custrecord_clgx_sales_activ_lead_source', 'test');
        var idRec = nlapiSubmitRecord(record, true,true);


        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

