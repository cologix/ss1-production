//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Activities_All_Chart.js
//	Script Name:	CLGX_SL_CHARTS_Activities_All_Chart
//	Script Id:		customscript_clgx_sl_chrt_act_all_chrt
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		07/18/2013
//-------------------------------------------------------------------------------------------------

function suitelet_charts_activities_all_chart (request, response){
    try {

    	
    	var arrExclude = [294,279321,480345,71418,115655,2347,4274,6857,213255,323586,339721,501436,387984,394031,390070,3118,530873,1127523,600791,1251296];
    	
    	var marketid = request.getParameter('marketid');
        var market = clgx_return_market_name(marketid);
        var userid = request.getParameter('userid');
        var period = request.getParameter('period');
        var periodid = request.getParameter('periodid');

        if(marketid != '' ){
            var objFile = nlapiLoadFile(531299);
            var html = objFile.getValue();
            html = html.replace(new RegExp('{dataReps}','g'),dataReps(marketid, arrExclude));

            html = html.replace(new RegExp('{dataEmails}','g'),dataActivities('message', marketid, userid, period, periodid, '', arrExclude));
            html = html.replace(new RegExp('{dataPhones}','g'),dataActivities('phonecall', marketid, userid, period, periodid, '', arrExclude));
            //html = html.replace(new RegExp('{dataTasks}','g'),dataActivities('task', marketid, userid, period, periodid, '', arrExclude));
            html = html.replace(new RegExp('{dataVisitsCmp}','g'),dataActivities('visit', marketid, userid, period, periodid, 'Completed', arrExclude));
            html = html.replace(new RegExp('{dataVisitsPend}','g'),dataActivities('visit', marketid, userid, period, periodid, 'NotCompleted', arrExclude));
            html = html.replace(new RegExp('{dataMeetings}','g'),dataActivities('meeting', marketid, userid, period, periodid, '', arrExclude));
            //html = html.replace(new RegExp('{dataEvents}','g'),dataActivities('calendarevent', marketid, userid, period, periodid, '', arrExclude));
            html = html.replace(new RegExp('{dataTeamAverage}','g'),dataAverages('team', marketid, period, periodid, arrExclude));
            html = html.replace(new RegExp('{dataAllAverage}','g'),dataAverages('all', marketid, period, periodid, arrExclude));
            html = html.replace(new RegExp('{title}','g'), market + ' - ' + periodTitle (period, periodid));
        }
        else{
            var html = 'Please select a report from the left panel.';
        }
        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function dataReps (marketid, arrExclude) {
    var arrFilters = new Array();
    if(parseInt(marketid) != 0){
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",marketid));
    }
    arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
    arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",arrExclude));
    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    arrColumns.push(new nlobjSearchColumn('firstname',null,null));
    arrColumns.push(new nlobjSearchColumn('lastname',null,null));
    var searchReps = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);

    var stReturn = '[';
    for ( var i = 0; searchReps != null && i < searchReps.length; i++ ) {
        var searchRep = searchReps[i];
        var firstname = searchRep.getValue('firstname', null, null);
        var lastname = searchRep.getValue('lastname', null, null);
        stReturn += '\'' + firstname + ' ' + lastname + '\',';
    }
    var strLen = stReturn.length;
    if (searchReps != null){
        stReturn = stReturn.slice(0,strLen-1);
    }
    stReturn += ']';
    return stReturn;
}

function dataActivities (type, marketid, userid, period, periodid, status, arrExclude) {

    var arrFilters = new Array();
    if(parseInt(marketid) != 0){
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",marketid));
    }
    arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
    arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",arrExclude));
    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    arrColumns.push(new nlobjSearchColumn('firstname',null,null));
    arrColumns.push(new nlobjSearchColumn('lastname',null,null));
    var searchReps = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);

    var arrDates  = getDateRange(period, periodid);
    var startDate = arrDates[0];
    var endDate   = arrDates[1];

    var stReturn = '[';
    for ( var i = 0; searchReps != null && i < searchReps.length; i++ ) {
        var searchRep = searchReps[i];
        var repid = searchRep.getValue('internalid', null, null);

        var arrFilters = new Array();
        var arrColumns = new Array();
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_type",null,"is",type));
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_rep_id",null,"equalto",repid));
        arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_date',null,'within',startDate,endDate));
        arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_customer',null,'doesnotcontain','Anonymous'));
        if(type == 'visit' && status == 'Completed'){
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_status",null,"is",'Completed'));
        }
        if(type == 'visit' && status == 'NotCompleted'){
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_status",null,"isnot",'Completed'));
        }
        var searchActivities = nlapiSearchRecord('customrecord_cologix_sales_activities', null, arrFilters, arrColumns);

        if(searchActivities != null){var nbrActivities = searchActivities.length;}
        else{var nbrActivities = 0;}

        stReturn += '{y:' + nbrActivities + ',url:\'/app/site/hosting/scriptlet.nl?script=210&deploy=1&type=' + type + '&marketid=' + marketid + '&repid=' + repid + '&period=' + period + '&periodid=' + periodid + '&filter=all\'' + '},';
    }
    var strLen = stReturn.length;
    if (searchReps != null){
        stReturn = stReturn.slice(0,strLen-1);
    }
    stReturn += ']';
    return stReturn;
}

function dataAverages (type, marketid, period, periodid, arrExclude) {

    var arrDates  = getDateRange(period, periodid);
    var startDate = arrDates[0];
    var endDate   = arrDates[1];

    var stReturn = '[';
    var arrReps = new Array();
    arrReps.push(0);

    var arrFilters = new Array();
    var arrFiltersAll = new Array();
    if(marketid != '0'){
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",parseInt(marketid)));
    }
    arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
    arrFiltersAll.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
    arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",arrExclude));
    arrFiltersAll.push(new nlobjSearchFilter("internalid",null,"noneof",arrExclude));
    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    arrColumns.push(new nlobjSearchColumn('firstname',null,null));
    arrColumns.push(new nlobjSearchColumn('lastname',null,null));
    var searchTeamReps = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);
    var searchAllReps = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFiltersAll, arrColumns);

    if (type == 'all'){
        var nbrReps = searchAllReps.length;
    }
    else{
        var nbrReps = searchTeamReps.length;
    }

    if(searchTeamReps != null){
        var arrFilters = new Array();
        if (type != 'all' && marketid != '0'){
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_location_id",null,"equalto",marketid));
        }
        arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_date',null,'within',startDate,endDate));
        arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_customer',null,'doesnotcontain','Anonymous'));
        var searchActivities = nlapiSearchRecord('customrecord_cologix_sales_activities', null, arrFilters, null);
        if(searchActivities != null){var nbrActivities = searchActivities.length;}
        else{var nbrActivities = 0;}

        var averageValue = parseFloat(parseInt(nbrActivities)/parseInt(nbrReps)).toFixed(2);

        for ( var i = 0; searchTeamReps != null && i < searchTeamReps.length; i++ ) {
            stReturn += '{y:' + averageValue + ',url:\'#\'' + '},';
        }
        var strLen = stReturn.length;
        stReturn = stReturn.slice(0,strLen-1);
    }
    else{
        stReturn += '0';
    }
    stReturn += ']';
    return stReturn;
}

function getDateRange(period, periodid){
    if (period == 'week'){
        var startDate = moment().subtract('weeks', periodid).startOf('week').format('M/D/YYYY');
        var endDate = moment().subtract('weeks', periodid).endOf('week').format('M/D/YYYY');
    }
    else{
        var startDate = moment().startOf('month').format('M/D/YYYY');
        var endDate = moment().endOf('month').format('M/D/YYYY');

    }
    var arrDateRange = new Array();
    arrDateRange[0] = startDate;
    arrDateRange[1] = endDate;
    return arrDateRange;
}

/*
function getDateRange(type, period, periodid){
    if (period == 'week'){
        var startDate = moment().subtract('weeks', periodid).startOf('week').format('M/D/YYYY');
        if(type == 'task' && periodid == 0){
        	var endDate = moment().format('M/D/YYYY');
        }
        else{
        	var endDate = moment().subtract('weeks', periodid).endOf('week').format('M/D/YYYY');
        }
    }
    else{
        var startDate = moment().startOf('month').format('M/D/YYYY');
        
        if(type == 'task' && periodid == 0){
        	var endDate = moment().format('M/D/YYYY');
        }
        else{
        	var endDate = moment().endOf('month').format('M/D/YYYY');
        }
    }
    var arrDateRange = new Array();
    arrDateRange[0] = startDate;
    arrDateRange[1] = endDate;
    return arrDateRange;
}
*/
function integer(str) {
    if(str == '' || str == 'NaN' || str == null)  {
        return 0;
    }
    else {
        return parseInt(str);
    }
}

function periodTitle (period, periodid){
    if (period == 'month'){
        return 'Current Month';
    }
    else{
        switch(parseInt(periodid)) {
            case 0:
                return 'This Week';
                break;
            case 1:
                return 'Last Week';
                break;
            case 2:
                return '2 Weeks Ago';
                break;
            case 3:
                return '3 Weeks Ago';
                break;
            case 4:
                return '4 Weeks Ago';
                break;
            default:
                return '';
        }
    }
}
