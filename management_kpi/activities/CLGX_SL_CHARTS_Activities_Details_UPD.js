//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Activities_Details.js
//	Script Name:	CLGX_SL_CHARTS_Activities_Details
//	Script Id:		customscript_clgx_sl_chrt_act_details
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		07/19/2013
//-------------------------------------------------------------------------------------------------

function suitelet_charts_act_details (request, response){
	try {
		
		var type        = request.getParameter('type');
		var marketid    = request.getParameter('marketid');
		var repid       = request.getParameter('repid');
		var period      = request.getParameter('period');
		var periodid    = request.getParameter('periodid');
		var filter      = request.getParameter('filter');
		var status      = request.getParameter('status');
		var displayType = "";
		
		var objFile = nlapiLoadFile(7065661); //548975
		var html = objFile.getValue();
		
		var activitiesJSON = dataActivitiesUpd(type, status, marketid, repid, period, periodid, filter);
		
		html = html.replace(new RegExp('{activitiesJSON}','g'), activitiesJSON);
		html = html.replace(new RegExp('{activityType}','g'),getDisplayType(type));
		html = html.replace(new RegExp('{activityCount}','g'),getActivityCount(activitiesJSON));
		//html = html.replace(new RegExp('{activitiesJSON}','g'),'var dataActivities =  [];');
		
		response.write( html );
		//response.write( '| type:' + type + '| marketid:' + marketid + '| repid:' + repid + '| period:' + period + '| periodid:' + periodid );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

//Net New
function dataActivitiesUpd (type, status, marketid, repid, period, periodid, filter) {
    var arrDates   = getDateRange(period, periodid);
    var startDate  = arrDates[0];
    var endDate    = arrDates[1];
    var today      = moment().format("M/D/YYYY");
    var previousID = null;
    
    var dataArray = new Array();
    var stReturn  = "";
    
    var arrColumns = new Array();
	var arrFilters = new Array();
	
	arrColumns.push(new nlobjSearchColumn("internalid", null, null));
	
	if(type == "message") {
    	arrFilters.push(new nlobjSearchFilter("author", null, "anyof", repid));
    	arrFilters.push(new nlobjSearchFilter("messagedate", null, "within", startDate, endDate));
    	arrFilters.push(new nlobjSearchFilter("recipient", null, "noneof", repid));
    } else if(type == "phonecall") {
    	arrFilters.push(new nlobjSearchFilter("assigned", null, "anyof", repid));
    	arrFilters.push(new nlobjSearchFilter("startdate", null, "within", startDate, endDate));
    } else if(type == "visit" && status == "Completed") {
    	arrFilters = [["organizer", "anyof", repid], "and", 
	          [["location", "contains", "Data Center Tour"], "or", 
	           ["title", "contains", "Data Center Tour"]], "and", 
	          ["startdate", "within", startDate, endDate], "and",
	          ["startdate", "before", today]];
    	
    	/*arrFilters.push(new nlobjSearchFilter("organizer", null, "anyof", repid));
    	arrFilters.push(new nlobjSearchFilter("location", null, "contains", "Data Center Tour"));
    	arrFilters.push(new nlobjSearchFilter("title", null, "contains", "Data Center Tour"));
    	arrFilters.push(new nlobjSearchFilter("startdate", null, "within", startDate, endDate));
    	arrFilters.push(new nlobjSearchFilter("startdate", null, "before", today));*/
    } else if(type == "visit" && status == "NotCompleted") {
    	arrFilters = [["organizer", "anyof", repid], "and", 
    		          [["location", "contains", "Data Center Tour"], "or", 
    		           ["title", "contains", "Data Center Tour"]], "and", 
    		          ["startdate", "within", startDate, endDate], "and",
    		          ["startdate", "onorafter", today]];
    	
    	/*arrFilters.push(new nlobjSearchFilter("organizer", null, "anyof", repid));
    	arrFilters.push(new nlobjSearchFilter("location", null, "contains", "Data Center Tour"));
    	arrFilters.push(new nlobjSearchFilter("title", null, "contains", "Data Center Tour"));
    	arrFilters.push(new nlobjSearchFilter("startdate", null, "within", startDate, endDate));
    	arrFilters.push(new nlobjSearchFilter("startdate", null, "onorafter", today));*/
    } else if(type == "meeting") {
    	arrFilters = [["organizer", "anyof", repid], "and", 
    		          [["location", "doesnotcontain", "Data Center Tour"], "and", 
    		           ["title", "doesnotcontain", "Data Center Tour"]], "and", 
    		          ["startdate", "within", startDate, endDate]];
    		              	
    	/*arrFilters.push(new nlobjSearchFilter("organizer", null, "anyof", repid));
    	arrFilters.push(new nlobjSearchFilter("location", null, "doesnotcontain", "Data Center Tour"));
    	arrFilters.push(new nlobjSearchFilter("title", null, "doesnotcontain", "Data Center Tour"));
    	arrFilters.push(new nlobjSearchFilter("startdate", null, "within", startDate, endDate));*/
    	//arrFilters.push(new nlobjSearchFilter("startdate", null, "onorafter", today));
    }
	
	var searchActivities = null;
	
	if(type == "message") {
		searchActivities = nlapiSearchRecord("message", "customsearch_clgx_chrt_trrtry_act_msg", arrFilters, arrColumns);
	} else if(type == "phonecall") {
		searchActivities = nlapiSearchRecord("phonecall", "customsearch_clgx_chrt_trrtry_act_phn", arrFilters, arrColumns);
	} else if(type == "visit" || type == "meeting") {
		searchActivities = nlapiSearchRecord("calendarevent", "customsearch_clgx_chrt_trrtry_act_evt", arrFilters, arrColumns);
	}
	
	
	if(type == "message") {
		var messages = new Array();
		
		for(var m = 0; searchActivities != null && m < searchActivities.length; m++) {
			var searchActivity        = searchActivities[m];
			var columns               = searchActivities[m].getAllColumns();
			var activityID            = searchActivity.getValue(columns[1]); // Internal ID
			var customerStatus        = searchActivity.getValue("entitystatus", "customer", null);
			var customerLeadSource    = searchActivity.getValue("leadsource", "customer", null);
			
			if(previousID != activityID) {
				var includeRec = 0;
				
				if (filter == "all") {
					includeRec = 1;
				} else if (filter == "customers") {
					if(customerStatus == 13){
						includeRec = 1;
					}
				} else if (filter == "prospects") {
					if((customerStatus != 13) && (customerLeadSource != 257) && (customerLeadSource != 45337)) {
						includeRec = 1;
					}
				} else if (filter == "target") {
					if((customerStatus != 13) && (customerLeadSource == 257)) {
						includeRec = 1;
					}
				} else if (filter == "inbound") {
					if((customerStatus != 13) && (customerLeadSource == 45337)) {
						includeRec = 1;
					}
				}
				
				if(includeRec == 1) {
					messages.push({ "id": activityID, "email": searchActivity.getValue(columns[4]).toLowerCase(), "date": searchActivity.getValue("messagedate")});
				}
			}
			
			previousID = activityID;
		}
		
		var filter   = buildEmailFilter(messages);
		var contacts = getContactList(filter);
		return JSON.stringify(mergeMessageAndContacts(type, messages, contacts));
	} else {
		for ( var i = 0; searchActivities != null && i < searchActivities.length; i++ ) {
			var searchActivity = searchActivities[i];
			var columns        = searchActivities[i].getAllColumns();
			
			var activityid            = searchActivity.getValue(columns[1]); // Internal ID
			var activityDate          = -1;
			var customer              = "";
			var customerid            = -1;
			var contact               = "";
			var contactid             = -1;
			var customerStatus        = "";
			var customerLeadSource    = "";
			var customerTargetAccount = "";
			
			if(previousID != activityid) {
				//nlapiLogExecution("DEBUG", "activityid", activityid);
				
				if(type == "message") {
					/*var recipientid       = searchActivity.getValue(columns[4]); //Recipient Internal ID
					contact               = removeCompanyFromName(searchActivity.getValue(columns[13])); //Contact Name
					contactid             = searchActivity.getValue(columns[12]); //Contact Internal ID
					customer              = removeCompanyFromName(searchActivity.getValue(columns[8])); //Customer Name
					customerid            = searchActivity.getValue(columns[7]); //Customer ID
					activityDate          = searchActivity.getValue("messagedate");
					customerStatus        = searchActivity.getValue("entitystatus", "customer", null);
					customerLeadSource    = searchActivity.getValue("leadsource", "customer", null);
					customerTargetAccount = searchActivity.getValue("custentity_clgx_target_account", "customer", null);
					
					if((customer == null || customer == "") || (contact == null || contact == "")) {
						var contactObj = getContactFromEmailAddress(searchActivity.getValue(columns[6]), searchActivity.getValue(columns[14]), searchActivity.getValue(columns[15])); //params(recipient, cc, bcc)
						//nlapiLogExecution("DEBUG", "contactObj", JSON.stringify(contactObj));
						
						contact        = removeCompanyFromName(contactObj.contact.name);
						contactid      = (contactObj.contact.id.length == 0) ? -1 : contactObj.contact.id;
						customer       = removeCompanyFromName(contactObj.company.name) == "" ? "No Company Listed" : removeCompanyFromName(contactObj.company.name);
						customerid     = (contactObj.company.id.length == 0) ? -1 : contactObj.company.id;
					}*/
				} else if(type == "phonecall") {
					activityDate          = searchActivity.getValue("startdate");
					contact               = removeCompanyFromName(searchActivity.getText("contact"));
					contactid             = searchActivity.getValue("contact") == "" ? -1 : searchActivity.getValue("contact");
					customer              = removeCompanyFromName(searchActivity.getText("company"));
					customerid            = searchActivity.getValue("company") == "" ? -1 : searchActivity.getValue("company");
					customerStatus        = searchActivity.getValue("entitystatus", "companycustomer", null);
					customerLeadSource    = searchActivity.getValue("leadsource", "companycustomer", null);
					customerTargetAccount = searchActivity.getValue("custentity_clgx_target_account", "companycustomer", null);
				} else if(type == "visit" || type == "meeting") {
					activityDate          = searchActivity.getValue("startdate");
					contact               = removeCompanyFromName(searchActivity.getText("contact"));
					contactid             = searchActivity.getValue("contact") == "" ? -1 : searchActivity.getValue("contact");
					customer              = removeCompanyFromName(searchActivity.getText("company"));
					customerid            = searchActivity.getValue("company") == "" ? -1 : searchActivity.getValue("company");
					customerStatus        = searchActivity.getValue("entitystatus", "attendeecustomer", null);
					customerLeadSource    = searchActivity.getValue("leadsource", "attendeecustomer", null);
					customerTargetAccount = searchActivity.getValue("custentity_clgx_target_account", "attendeecustomer", null);
				} /*else if(type == "meeting") {
					activityDate = searchActivity.getValue("startdate");
					contact      = removeCompanyFromName(searchActivity.getText("contact"));
					contactid    = searchActivity.getValue("contact") == "" ? -1 : searchActivity.getValue("contact");
					customer     = removeCompanyFromName(searchActivity.getText("company"));
					customerid   = searchActivity.getValue("company") == "" ? -1 : searchActivity.getValue("company");
				}*/
				
				
				var includeRec = 0;
				if (filter == 'all') {
					includeRec = 1;
				} else if (filter == 'customers') {
					if(customerStatus == 13){
						includeRec = 1;
					}
				} else if (filter == 'prospects') {
					if((customerStatus != 13) && (customerLeadSource != 257) && (customerLeadSource != 45337)) {
						includeRec = 1;
					}
				} else if (filter == 'target') {
					if((customerStatus != 13) && (customerLeadSource == 257)){
						includeRec = 1;
					}
				} else if (filter == 'inbound') {
					if((customerStatus != 13) && (customerLeadSource == 45337)){
						includeRec = 1;
					}
				}
				
				
				if(includeRec == 1) {
					dataArray.push({ "type" : getDisplayType(type), 
						             "internalid" : activityid, 
						             "customer" : customer, 
						             "customerid" : customerid, 
						             "contact" : contact, 
						             "contactid" : contactid, 
						             "date" : activityDate});
					
					/*stReturn += '\n{"type":"' + getDisplayType(type) + 
									'","internalid":' +  activityid + 
									',"customer":"' +  customer + 
									'","customerid":' +  customerid + 
									',"contact":"' +  contact + 
									'","contactid":' + contactid +
									',"date":"' + activityDate + 
									'"},';*/

				}
			}
			
			previousID = activityid;
		}
	}
	
	
	/*var strLen = stReturn.length;
	if (searchActivities != null){
		stReturn = stReturn.slice(0,strLen - 1);
	}*/
	
    return JSON.stringify(dataArray);
}


/**
 * Returns whether or not the message has a contact.
 * 
 * @param {Object} message
 * @param {Object} contact
 * @return {Boolean}
 */
function messageHasContact(message, contacts) {
	if(message != null && contacts != null) {
		var contactLength = contacts.length;
		
		for(var c = 0; c < contactLength; c++) {
			if(message.email == contacts[c].contact_email) {
				return c; //Contact index.
			}
		}
	}
	
	return -1;
}


/**
 * Returns the number of activities.
 * 
 * @param {String} activitiesJSON
 * @return {Integer}
 */
function getActivityCount(activitiesJSON) {
	if(activitiesJSON != null && activitiesJSON != "") {
		var parsed = JSON.parse(activitiesJSON);
		return parsed.length;
	}
}


/**
 * Builds a NetSuite email filter from an array of emails.
 * 
 * @param {Array} messages
 * @return {Array}
 */
function buildEmailFilter(messages) {
	var finalFilters = [];
	
	if(messages != null && messages != "") {
		var ll   = messages.length;
		
		for(var l = 0; l < ll; l++) {
			if(!contains(messages[l].email, "@cologix.com")) {
				if(l != 0) { finalFilters.push("or"); }				
				finalFilters.push(["email", "contains", messages[l].email]);
			}
		}
	}
	
	return finalFilters;
}


/**
 * Returns an array of contacts.
 * 
 * @param {Array} filter
 * @return {Array}
 */
function getContactList(filter) {
	if(filter != null) {
		var finalArray = new Array();
		var results    = nlapiSearchRecord("contact", "customsearch_clgx_chrt_trrtry_act_ctct", filter, null);
		
		if(results != null) {
			var resultLength = results.length;
			for(var i = 0; i < resultLength; i++) {
				var result  = results[i];
				var columns = results[i].getAllColumns();
				
				finalArray.push({"contact_id"   : result.getValue(columns[0]),
					             "contact_name" : (result.getValue(columns[1]) + " " + result.getValue(columns[2])),
					             "company_id"   : result.getValue(columns[4]),
					             "company_name" : result.getText(columns[5]),
					             "contact_email": result.getValue(columns[3]).toLowerCase()});
			}
		}
		
		return finalArray;
	}
}


/**
 * Returns a merged list of email messages and contacts.
 * 
 * @param {Array} emails
 * @param {Array} contacts
 * @return {Array}
 */
function mergeMessageAndContacts(type, messages, contacts) {
	if(messages != null && contacts != null) {
		var mergedArray    = new Array();
		var contactLength = contacts.length;
		var messageLength = messages.length;
		
		var contactIndex = -1;
		
		for(var m = 0; m < messageLength; m++) {
			
			//nlapiLogExecution("DEBUG", "contacts", JSON.stringify(contacts));
			//nlapiLogExecution("DEBUG", "contact-emails", contacts[0].contact_email);
			//nlapiLogExecution("DEBUG", "messages", JSON.stringify(messages));
			if((contactIndex = messageHasContact(messages[m], contacts)) > -1) {
				mergedArray.push({ 
					"type": getDisplayType(type),
					"internalid": messages[m].id,
					"customer": contacts[contactIndex].company_name,
					"customerid": contacts[contactIndex].company_id,
					"contact": contacts[contactIndex].contact_name,
					"contactid": contacts[contactIndex].contact_id,
					"date": messages[m].date
				});
			} else {
				mergedArray.push({ 
					"type": getDisplayType(type),
					"internalid": messages[m].id,
					"customer": "No Record",
					"customerid": "-1",
					"contact": "No Record",
					"contactid": "-1",
					"date": messages[m].date
				});
			}
		}
		
		return mergedArray;
	}
}


function removeCompanyFromName(contactName) {
	if(contactName != null && contactName != "") {
		var nameObj = contactName.split(":");
		return nameObj[nameObj.length - 1].trim();
	}
	
	return contactName;
}

function getContactFromEmailAddress(email, cc, bcc) {
	if((email != null && email != "")) {
		var columns   = new Array();
		var filters   = getEmailFilters(email, cc, bcc);
		
		var results   = nlapiSearchRecord("contact", "customsearch_clgx_chrt_trrtry_act_ctct", filters, columns);
		var recipient = searchMessageRecipient(results);
		
		if(recipient != null) {
			return recipient;
		}
	}
	
	return {"contact" : { "id" : -1, "name" : email },
	        "company" : { "id" : -1, "name" : "No Record" }};
}


function searchMessageRecipient(results) {
	if(results != null && results != "") {
		return {"contact" : { "id" : results[0].getValue("internalid"), "name" : results[0].getValue("firstname") + " " + results[0].getValue("lastname")},
	        	"company" : { "id" : results[0].getValue("company"),    "name" : results[0].getText("company")}};
	}
	
	return null;
}


function searchMessageCC(results) {
	if(results != null && results != "") {
		return {"contact" : { "id" : results[0].getValue("internalid"), "name" : results[0].getValue("firstname") + " " + results[0].getValue("lastname")},
        		"company" : { "id" : results[0].getValue("company"),    "name" : results[0].getText("company")}};
	}
	
	return null;
}


function searchMessageBCC(results) {
	if(results != null && results != "") {
		return {"contact" : { "id" : results[0].getValue("internalid"), "name" : results[0].getValue("firstname") + " " + results[0].getValue("lastname")},
        		"company" : { "id" : results[0].getValue("company"),    "name" : results[0].getText("company")}};
	}
	
	return null;
}


function contains(value, search) {
	return (value || "").indexOf(search) > -1;
}


function getDisplayType(type) {
	if(type == "message") {
		return "Message";
	} else if(type == "phonecall") {
		return "Phone Call";
	} else if(type == "visit") {
		return "Visit";
	} else if(type == "meeting") {
		return "Meeting";
	} else {
		return "";
	}
}

function getDateRange(period, periodid){
	if (period == 'week'){
	    var startDate = moment().subtract('weeks', periodid).startOf('week').format('M/D/YYYY');
    	var endDate = moment().subtract('weeks', periodid).endOf('week').format('M/D/YYYY');
	} else if(period == "lastmonth") {
    	var startDate = moment().subtract(1, "months").startOf('month').format('M/D/YYYY');
        var endDate = moment().subtract(1, "months").endOf('month').format('M/D/YYYY');
    } else {
		var startDate = moment().startOf('month').format('M/D/YYYY');
    	var endDate = moment().endOf('month').format('M/D/YYYY');

	}
    var arrDateRange = new Array();
        arrDateRange[0] = startDate;
        arrDateRange[1] = endDate;
    return arrDateRange;
}

/*
function getDateRange(type, period, periodid){
    if (period == 'week'){
        var startDate = moment().subtract('weeks', periodid).startOf('week').format('M/D/YYYY');
        if(type == 'task' && periodid == 0){
        	var endDate = moment().format('M/D/YYYY');
        }
        else{
        	var endDate = moment().subtract('weeks', periodid).endOf('week').format('M/D/YYYY');
        }
    }
    else{
        var startDate = moment().startOf('month').format('M/D/YYYY');
        
        if(type == 'task' && periodid == 0){
        	var endDate = moment().format('M/D/YYYY');
        }
        else{
        	var endDate = moment().endOf('month').format('M/D/YYYY');
        }
    }
    var arrDateRange = new Array();
    arrDateRange[0] = startDate;
    arrDateRange[1] = endDate;
    return arrDateRange;
}
*/

function integer(str) {
	if(str == '' || str == 'NaN' || str == null)  {
		return 0;
	}
	else {
		return parseInt(str);
	}
} 
