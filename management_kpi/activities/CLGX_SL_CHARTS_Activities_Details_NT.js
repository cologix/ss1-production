//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Activities_Details.js
//	Script Name:	CLGX_SL_CHARTS_Activities_Details
//	Script Id:		customscript_clgx_sl_chrt_act_details
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		07/19/2013
//-------------------------------------------------------------------------------------------------

function suitelet_charts_act_details (request, response){
	try {
		
		var type = request.getParameter('type');
		var marketid = request.getParameter('marketid');
		var repid = request.getParameter('repid');
		var period = request.getParameter('period');
		var periodid = request.getParameter('periodid');
		var filter = request.getParameter('filter');

		var objFile = nlapiLoadFile(548975);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{activitiesJSON}','g'),dataActivities(type, marketid, repid, period, periodid, filter));
		html = html.replace(new RegExp('{activityType}','g'),type);
		//html = html.replace(new RegExp('{activitiesJSON}','g'),'var dataActivities =  [];');
		
		response.write( html );
		//response.write( '| type:' + type + '| marketid:' + marketid + '| repid:' + repid + '| period:' + period + '| periodid:' + periodid );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function dataActivities (type, marketid, repid, period, periodid, filter) {

    var arrDates  = getDateRange(period, periodid);
    var startDate = arrDates[0];
    var endDate   = arrDates[1];

	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_type",null,"is",type));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_rep_id",null,"equalto",repid));
	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_date',null,'within',startDate,endDate));
	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_customer',null,'doesnotcontain','Anonymous'));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_deleted",null,"is",'F'));
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_sales_activ_id',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_sales_activ_date',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_sales_activ_customer_id',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_sales_activ_customer',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_sales_activ_contact_id',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_sales_activ_contact',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_sales_activ_custom_type',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_sales_activ_lead_source',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_sales_activ_type',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_sales_activ_task_type',null,null));
	var searchActivities = nlapiSearchRecord('customrecord_cologix_sales_activities', null, arrFilters, arrColumns);

	var stReturn = 'var dataActivities =  [';
	for ( var i = 0; searchActivities != null && i < searchActivities.length; i++ ) {
		var searchActivity = searchActivities[i];
		
		var activityid = searchActivity.getValue('custrecord_clgx_sales_activ_id', null, null);
		var activityDate = searchActivity.getValue('custrecord_clgx_sales_activ_date', null, null);
		var contactid = searchActivity.getValue('custrecord_clgx_sales_activ_contact_id', null, null);
		var contact = searchActivity.getValue('custrecord_clgx_sales_activ_contact', null, null);
		var customerid = searchActivity.getValue('custrecord_clgx_sales_activ_customer_id', null, null);
		var customer = searchActivity.getValue('custrecord_clgx_sales_activ_customer', null, null);
		var customerStatus = searchActivity.getValue('custrecord_clgx_sales_activ_custom_type', null, null);
		var customerLeadSource = searchActivity.getValue('custrecord_clgx_sales_activ_lead_source', null, null);
		var type = searchActivity.getValue('custrecord_clgx_sales_activ_task_type', null, null);

		var includeRec = 0;
		if (filter == 'all'){
			includeRec = 1;
		}
		else if (filter == 'customers'){
			if(customerStatus == 13){
				includeRec = 1;
			}
		}
		else if (filter == 'prospects'){
			if(customerStatus != 13 && customerLeadSource != 257 && customerLeadSource != 45337){
				includeRec = 1;
			}
		}
		else if (filter == 'target'){
			if(customerStatus != 13 && customerLeadSource == 257){
				includeRec = 1;
			}
		}
		else if (filter == 'inbound'){
			if(customerStatus != 13 && customerLeadSource == 45337){
				includeRec = 1;
			}
		}
		else{}
	
		if(includeRec == 1){
			stReturn += '\n{"type":"' + type + 
							'","internalid":' +  activityid + 
							',"customer":"' +  customer + 
							'","customerid":' +  customerid + 
							',"contact":"' +  contact + 
							'","contactid":' +  contactid + 
							',"date":"' + activityDate + 
							'"},'; 
		}
	}

	var strLen = stReturn.length;
	if (searchActivities != null){
		stReturn = stReturn.slice(0,strLen-1);
	}
	stReturn += '];';
    return stReturn;
}


function getDateRange(period, periodid){
	if (period == 'week'){
	    var startDate = moment().subtract('weeks', periodid).startOf('week').format('M/D/YYYY');
    	var endDate = moment().subtract('weeks', periodid).endOf('week').format('M/D/YYYY');
	}
	else{
		var startDate = moment().startOf('month').format('M/D/YYYY');
    	var endDate = moment().endOf('month').format('M/D/YYYY');

	}
    var arrDateRange = new Array();
        arrDateRange[0] = startDate;
        arrDateRange[1] = endDate;
    return arrDateRange;
}

/*
function getDateRange(type, period, periodid){
    if (period == 'week'){
        var startDate = moment().subtract('weeks', periodid).startOf('week').format('M/D/YYYY');
        if(type == 'task' && periodid == 0){
        	var endDate = moment().format('M/D/YYYY');
        }
        else{
        	var endDate = moment().subtract('weeks', periodid).endOf('week').format('M/D/YYYY');
        }
    }
    else{
        var startDate = moment().startOf('month').format('M/D/YYYY');
        
        if(type == 'task' && periodid == 0){
        	var endDate = moment().format('M/D/YYYY');
        }
        else{
        	var endDate = moment().endOf('month').format('M/D/YYYY');
        }
    }
    var arrDateRange = new Array();
    arrDateRange[0] = startDate;
    arrDateRange[1] = endDate;
    return arrDateRange;
}
*/

function integer(str) {
	if(str == '' || str == 'NaN' || str == null)  {
		return 0;
	}
	else {
		return parseInt(str);
	}
} 
