nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Activities_Rep_Chart.js
//	Script Name:	CLGX_SL_CHARTS_Activities_Rep_Chart
//	Script Id:		customscript_clgx_sl_chrt_act_rep_chrt
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		07/18/2013
//-------------------------------------------------------------------------------------------------

function suitelet_charts_activities_rep_chart (request, response){
	try {

		var context = nlapiGetContext();
		var roleid = context.getRole();
		
		var marketid = request.getParameter('marketid');
		var userid = request.getParameter('userid');
		var period = request.getParameter('period');
		var periodid = request.getParameter('periodid');
		
		if(marketid != '' ){
			var objFile = nlapiLoadFile(525610);
			var html = objFile.getValue();
			html = html.replace(new RegExp('{dataEmails}','g'),dataActivities('message', marketid, userid, period, periodid, ''));
			html = html.replace(new RegExp('{dataPhones}','g'),dataActivities('phonecall', marketid, userid, period, periodid, ''));
			html = html.replace(new RegExp('{dataTasks}','g'),dataActivities('task', marketid, userid, period, periodid, ''));
			html = html.replace(new RegExp('{dataVisitsCmp}','g'),dataActivities('visit', marketid, userid, period, periodid, 'Completed'));
			html = html.replace(new RegExp('{dataVisitsPend}','g'),dataActivities('visit', marketid, userid, period, periodid, 'NotCompleted'));
			html = html.replace(new RegExp('{dataEvents}','g'),dataActivities('calendarevent', marketid, userid, period, periodid, ''));
			
			html = html.replace(new RegExp('{dataTeamAverage}','g'),dataAverages('team', marketid, period, periodid));
			html = html.replace(new RegExp('{dataAllAverage}','g'),dataAverages('all', marketid, period, periodid));
			
			var usage = context.getRemainingUsage();
			
			if (roleid == -5 || roleid == 3 || roleid == 18 || roleid == 1088) {
				html = html.replace(new RegExp('{title}','g'),nlapiLookupField('employee', userid, 'entityid') + ' - ' + periodTitle (period, periodid) + ' (' +usage + ')');
			}
			else{
				html = html.replace(new RegExp('{title}','g'),nlapiLookupField('employee', userid, 'entityid') + ' - ' + periodTitle (period, periodid));
				
			}
		}
		else{
			var html = 'Please select a report from the left panel.';
		}

		response.write( html );

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function dataActivities (type, marketid, userid, period, periodid, status) {

    var arrDates  = getDateRange(period, periodid);
    var startDate = arrDates[0];
    var endDate   = arrDates[1];
    
	var stReturn = '[';
	
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_type",null,"is",type));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_rep_id",null,"equalto",userid));
	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_date',null,'within',startDate,endDate));
	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_customer',null,'doesnotcontain','Anonymous'));
	if(type == 'visit' && status == 'Completed'){
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_status",null,"is",'Completed'));
	}
	if(type == 'visit' && status == 'NotCompleted'){
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_status",null,"isnot",'Completed'));
	}
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_sales_activ_custom_type',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_sales_activ_lead_source',null,null));
	var searchActivities = nlapiSearchRecord('customrecord_cologix_sales_activities', null, arrFilters, arrColumns);

	var countAll = 0;
	var countCustomers = 0;
	var countProspects = 0;
	var countTargetAcc = 0;
	var countInboundCall = 0;
	
	if(searchActivities != null){
		countAll = searchActivities.length;
	}

	for ( var i = 0; searchActivities != null && i < searchActivities.length; i++ ) {
		var searchActivity = searchActivities[i];
		var customerStatus = searchActivity.getValue('custrecord_clgx_sales_activ_custom_type', null, null);
		var customerLeadSource = searchActivity.getValue('custrecord_clgx_sales_activ_lead_source', null, null);
		
		if(customerStatus == 13){
			countCustomers +=1;
		}
		else{
			if(customerLeadSource != 257 && customerLeadSource != 45337){
				countProspects +=1;
			}
			else{
				if(customerLeadSource == 257){
					countTargetAcc +=1;
				}
				
				if(customerLeadSource == 45337){
					countInboundCall +=1;
				}
			}
		}
	}
	
	stReturn += '{y:' + integer(countAll) + ',url:\'/app/site/hosting/scriptlet.nl?script=210&deploy=1&type=' + type + '&marketid=' + marketid + '&repid=' + userid + '&period=' + period + '&periodid=' + periodid + '&filter=all\'' + '},';
	stReturn += '{y:' + integer(countCustomers) + ',url:\'/app/site/hosting/scriptlet.nl?script=210&deploy=1&type=' + type + '&marketid=' + marketid + '&repid=' + userid + '&period=' + period + '&periodid=' + periodid + '&filter=customers\'' + '},';
	stReturn += '{y:' + integer(countProspects) + ',url:\'/app/site/hosting/scriptlet.nl?script=210&deploy=1&type=' + type + '&marketid=' + marketid + '&repid=' + userid + '&period=' + period + '&periodid=' + periodid + '&filter=prospects\'' + '},';
	stReturn += '{y:' + integer(countTargetAcc) + ',url:\'/app/site/hosting/scriptlet.nl?script=210&deploy=1&type=' + type + '&marketid=' + marketid + '&repid=' + userid + '&period=' + period + '&periodid=' + periodid + '&filter=target\'' + '},';
	stReturn += '{y:' + integer(countInboundCall) + ',url:\'/app/site/hosting/scriptlet.nl?script=210&deploy=1&type=' + type + '&marketid=' + marketid + '&repid=' + userid + '&period=' + period + '&periodid=' + periodid + '&filter=inbound\'' + '},';

	stReturn += ']';
    return stReturn;
}

function dataAverages (type, marketid, period, periodid) {

    var arrDates  = getDateRange(period, periodid);
    var startDate = arrDates[0];
    var endDate   = arrDates[1];
    
	var stReturn = '[';
	var arrReps = new Array();
	arrReps.push(0);
	
	var arrFilters = new Array();
	if(parseInt(marketid) != 0){
		arrFilters.push(new nlobjSearchFilter("location",null,"anyof",marketid));
	}
	arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",[71418,115655,2347,4274,6857,8918,213255,323586,339721,390070,387984,8918,390070]));
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('firstname',null,null));
	arrColumns.push(new nlobjSearchColumn('lastname',null,null));
	var searchTeamReps = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);
	var searchAllReps = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', null, arrColumns);

	if (type == 'all'){
		var nbrReps = searchAllReps.length;
	}
	else{
		var nbrReps = searchTeamReps.length;
	}

	if(searchTeamReps != null){

		// all activities
		var arrFilters = new Array();
		if (type != 'all' && marketid != '0'){
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_location_id",null,"equalto",marketid));
		}
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_date',null,'within',startDate,endDate));
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_customer',null,'doesnotcontain','Anonymous'));
		//arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_custom_type",null,"equalto",13));
		//arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_lead_source",null,"equalto",257));
		//arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_lead_source",null,"equalto",45337));
		var searchActivities = nlapiSearchRecord('customrecord_cologix_sales_activities', null, arrFilters, null);
		if(searchActivities != null){var value = searchActivities.length;}
		else{var value = 0;}
		stReturn += '{y:' + parseFloat(parseInt(value)/parseInt(nbrReps)).toFixed(2) + ',url:\'#\'' + '},';
		
		// activities to customers
		var arrFilters = new Array();
		if (type != 'all' && marketid != '0'){
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_location_id",null,"equalto",marketid));
		}
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_date',null,'within',startDate,endDate));
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_custom_type",null,"equalto",13));
		//arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_lead_source",null,"equalto",257));
		//arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_lead_source",null,"equalto",45337));
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_customer',null,'doesnotcontain','Anonymous'));
		var searchActivities = nlapiSearchRecord('customrecord_cologix_sales_activities', null, arrFilters, null);
		if(searchActivities != null){var value = searchActivities.length;}
		else{var value = 0;}
		stReturn += '{y:' + parseFloat(parseInt(value)/parseInt(nbrReps)).toFixed(2) + ',url:\'#\'' + '},';
		
		// activities to prospects
		var arrFilters = new Array();
		if (type != 'all' && marketid != '0'){
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_location_id",null,"equalto",marketid));
		}
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_date',null,'within',startDate,endDate));
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_custom_type",null,"notequalto",13));
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_lead_source",null,"notequalto",257));
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_lead_source",null,"notequalto",45337));
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_customer',null,'doesnotcontain','Anonymous'));
		var searchActivities = nlapiSearchRecord('customrecord_cologix_sales_activities', null, arrFilters, null);
		if(searchActivities != null){var value = searchActivities.length;}
		else{var value = 0;}
		stReturn += '{y:' + parseFloat(parseInt(value)/parseInt(nbrReps)).toFixed(2) + ',url:\'#\'' + '},';
		
		// activities to targets
		var arrFilters = new Array();
		if (type != 'all' && marketid != '0'){
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_location_id",null,"equalto",marketid));
		}
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_date',null,'within',startDate,endDate));
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_custom_type",null,"notequalto",13));
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_lead_source",null,"equalto",257));
		//arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_lead_source",null,"notequalto",45337));
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_customer',null,'doesnotcontain','Anonymous'));
		var searchActivities = nlapiSearchRecord('customrecord_cologix_sales_activities', null, arrFilters, null);
		if(searchActivities != null){var value = searchActivities.length;}
		else{var value = 0;}
		stReturn += '{y:' + parseFloat(parseInt(value)/parseInt(nbrReps)).toFixed(2) + ',url:\'#\'' + '},';
		
		// activities to inbound
		var arrFilters = new Array();
		if (type != 'all' && marketid != '0'){
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_location_id",null,"equalto",marketid));
		}
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_date',null,'within',startDate,endDate));
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_custom_type",null,"notequalto",13));
		//arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_lead_source",null,"equalto",257));
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_lead_source",null,"equalto",45337));
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_customer',null,'doesnotcontain','Anonymous'));
		var searchActivities = nlapiSearchRecord('customrecord_cologix_sales_activities', null, arrFilters, null);
		if(searchActivities != null){var value = searchActivities.length;}
		else{var value = 0;}
		stReturn += '{y:' + parseFloat(parseInt(value)/parseInt(nbrReps)).toFixed(2) + ',url:\'#\'' + '},';

	}
	else{
		stReturn += '{y:0,url:\'#\'' + '},{y:0,url:\'#\'' + '},{y:0,url:\'#\'' + '},{y:0,url:\'#\'' + '},{y:0,url:\'#\'' + '}';
		
	}
	stReturn += ']';
    return stReturn;
}


function getDateRange(period, periodid){
	if (period == 'week'){
	    var startDate = moment().subtract('weeks', periodid).startOf('week').format('M/D/YYYY');
    	var endDate = moment().subtract('weeks', periodid).endOf('week').format('M/D/YYYY');
	}
	else{
		var startDate = moment().startOf('month').format('M/D/YYYY');
    	var endDate = moment().endOf('month').format('M/D/YYYY');

	}
    var arrDateRange = new Array();
        arrDateRange[0] = startDate;
        arrDateRange[1] = endDate;
    return arrDateRange;
}

/*
function getDateRange(type, period, periodid){
    if (period == 'week'){
        var startDate = moment().subtract('weeks', periodid).startOf('week').format('M/D/YYYY');
        if(type == 'task' && periodid == 0){
        	var endDate = moment().format('M/D/YYYY');
        }
        else{
        	var endDate = moment().subtract('weeks', periodid).endOf('week').format('M/D/YYYY');
        }
        
    }
    else{
        var startDate = moment().startOf('month').format('M/D/YYYY');
        
        if(type == 'task' && periodid == 0){
        	var endDate = moment().format('M/D/YYYY');
        }
        else{
        	var endDate = moment().endOf('month').format('M/D/YYYY');
        }

    }
    var arrDateRange = new Array();
    arrDateRange[0] = startDate;
    arrDateRange[1] = endDate;
    return arrDateRange;
}
*/

function integer(str) {
	if(str == '' || str == 'NaN' || str == null)  {
		return 0;
	}
	else {
		return parseInt(str);
	}
} 

function periodTitle (period, periodid){
	if (period == 'month'){
		return 'Current Month';
	}
	else{
		switch(parseInt(periodid)) {
		case 0:
			return 'This Week';
			break;
		case 1:
			return 'Last Week';
			break;
		case 2:
			return '2 Weeks Ago';
			break;
		case 3:
			return '3 Weeks Ago';
			break;
		case 4:
			return '4 Weeks Ago';
			break;
		default:
			return '';
		}
	}
}

