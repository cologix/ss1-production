nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_CHARTS_Activities_Rep_Chart.js
//	Script Name:	CLGX_RL_CHARTS_Activities_Rep_Chart
//	Script Id:		customscript_clgx_rl_chrt_act_rep_chrt
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		08/21/2013
//-------------------------------------------------------------------------------------------------

function restlet_charts_activities_rep_chart (datain){
	try {

		var context = nlapiGetContext();
		var roleid = context.getRole();

		var txtArg = datain;
		txtArg = txtArg.replace(/\"/g,"");
		txtArg = txtArg.replace(/\:/g,",");
		txtArg = txtArg.replace(/\{/g,"");
		txtArg = txtArg.replace(/\}/g,"");
		var arrArguments = [];
		var arrArguments = txtArg.split( "," );
		
		var marketid = arrArguments[1];
		var periodid = arrArguments[3];
		var userid = arrArguments[5];
		var period = arrArguments[7];

		if(marketid != '' ){
			var objFile = nlapiLoadFile(525610);
			var html = objFile.getValue();

			html = html.replace(new RegExp('{arguments}','g'),txtArg);
			
			
			html = html.replace(new RegExp('{dataEmails}','g'),dataActivities('emails', marketid, userid, period, periodid));
			html = html.replace(new RegExp('{dataPhones}','g'),dataActivities('phones', marketid, userid, period, periodid));
			html = html.replace(new RegExp('{dataTasks}','g'),dataActivities('tasks', marketid, userid, period, periodid));
			html = html.replace(new RegExp('{dataEvents}','g'),dataActivities('events', marketid, userid, period, periodid));
			html = html.replace(new RegExp('{dataTeamAverage}','g'),dataAverages('team', marketid, period, periodid));
			html = html.replace(new RegExp('{dataAllAverage}','g'),dataAverages('all', marketid, period, periodid));
			
			
			var usage = context.getRemainingUsage();
			
			if (roleid == -5 || roleid == 3 || roleid == 18) {
				//html = html.replace(new RegExp('{title}','g'),nlapiLookupField('employee', userid, 'entityid') + ' - ' + periodTitle (period, periodid) + ' (' +usage + ')');
			}
			else{
				//html = html.replace(new RegExp('{title}','g'),nlapiLookupField('employee', userid, 'entityid') + ' - ' + periodTitle (period, periodid));
				
			}
		}
		else{
			var html = 'Please select a report from the left panel.';
		}

		return html;

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function dataActivities (type, marketid, userid, period, periodid) {

    var arrDates  = getDateRange(period, periodid);
    var startDate = arrDates[0];
    var endDate   = arrDates[1];
    
	var stReturn = '[';
	var arrFilters = new Array();
	switch(type) {
	case 'emails':
		arrFilters.push(new nlobjSearchFilter("author",null,"anyof",parseInt(userid)));
		arrFilters.push(new nlobjSearchFilter('messagedate',null,'within',startDate,endDate));
		var searchActivities = nlapiSearchRecord('message', 'customsearch_clgx_chrt_act_messages_all', arrFilters, null);
		break;
	case 'phones':
		arrFilters.push(new nlobjSearchFilter("assigned",null,"anyof",parseInt(userid)));
		arrFilters.push(new nlobjSearchFilter('startdate',null,'within',startDate,endDate));
		var searchActivities = nlapiSearchRecord('phonecall', 'customsearch_clgx_chrt_act_calls_all', arrFilters, null);
		break;
	case 'tasks':
		arrFilters.push(new nlobjSearchFilter("assigned",null,"anyof",parseInt(userid)));
		arrFilters.push(new nlobjSearchFilter('completeddate',null,'within',startDate,endDate));
		var searchActivities = nlapiSearchRecord('task', 'customsearch_clgx_chrt_act_tasks_all', arrFilters, null);
		break;
	case 'events':
		arrFilters.push(new nlobjSearchFilter("owner",null,"anyof",parseInt(userid)));
		arrFilters.push(new nlobjSearchFilter('startdate',null,'within',startDate,endDate));
		var searchActivities = nlapiSearchRecord('calendarevent', 'customsearch_clgx_chrt_act_events_all', arrFilters, null);
		break;
	default:
		arrFilters.push(new nlobjSearchFilter("author",null,"anyof",parseInt(userid)));
		arrFilters.push(new nlobjSearchFilter('messagedate',null,'within',startDate,endDate));
		var searchActivities = nlapiSearchRecord('message', 'customsearch_clgx_chrt_act_messages_all', arrFilters, null);
	}

	var countAll = 0;
	var countCustomers = 0;
	var countProspects = 0;
	var countTargetAcc = 0;
	var countInboundCall = 0;
	
	if(searchActivities != null){
		countAll = searchActivities.length;
	}

	for ( var i = 0; searchActivities != null && i < searchActivities.length; i++ ) {
		var searchActivity = searchActivities[i];
		
		var columns = searchActivity.getAllColumns();

		var activityid = searchActivity.getValue(columns[0]);
		var contactid = searchActivity.getValue(columns[1]);
		var contact = searchActivity.getValue(columns[2]);
		var customerid = searchActivity.getValue(columns[3]);
		var customerDetails = nlapiLookupField('customer', customerid, ['entityid', 'status','leadsource']);
		var customerName = customerDetails.entityid;
		var customerStatus = customerDetails.status;
		var customerLeadSource = customerDetails.leadsource;
		
		if(customerStatus == 13){
			countCustomers +=1;
		}
		else{
			if(customerLeadSource != 257 && customerLeadSource != 45337){
				countProspects +=1;
			}
			else{
				if(customerLeadSource == 257){
					countTargetAcc +=1;
				}
				if(customerLeadSource == 45337){
					countInboundCall +=1;
				}
			}
		}
	}
	
	stReturn += '{y:' + integer(countAll) + ',url:\'/app/site/hosting/scriptlet.nl?script=210&deploy=1&type=' + type + '&marketid=' + marketid + '&repid=' + userid + '&period=' + period + '&periodid=' + periodid + '&filter=all\'' + '},';
	stReturn += '{y:' + integer(countCustomers) + ',url:\'/app/site/hosting/scriptlet.nl?script=210&deploy=1&type=' + type + '&marketid=' + marketid + '&repid=' + userid + '&period=' + period + '&periodid=' + periodid + '&filter=customers\'' + '},';
	stReturn += '{y:' + integer(countProspects) + ',url:\'/app/site/hosting/scriptlet.nl?script=210&deploy=1&type=' + type + '&marketid=' + marketid + '&repid=' + userid + '&period=' + period + '&periodid=' + periodid + '&filter=prospects\'' + '},';
	stReturn += '{y:' + integer(countTargetAcc) + ',url:\'/app/site/hosting/scriptlet.nl?script=210&deploy=1&type=' + type + '&marketid=' + marketid + '&repid=' + userid + '&period=' + period + '&periodid=' + periodid + '&filter=target\'' + '},';
	stReturn += '{y:' + integer(countInboundCall) + ',url:\'/app/site/hosting/scriptlet.nl?script=210&deploy=1&type=' + type + '&marketid=' + marketid + '&repid=' + userid + '&period=' + period + '&periodid=' + periodid + '&filter=inbound\'' + '},';

	stReturn += ']';
    return stReturn;
}

function dataAverages (type, marketid, period, periodid) {

    var arrDates  = getDateRange(period, periodid);
    var startDate = arrDates[0];
    var endDate   = arrDates[1];
    
	var stReturn = '[';
	var arrReps = new Array();
	arrReps.push(0);
	
	var arrFilters = new Array();
	if(parseInt(marketid) != 0){
		arrFilters.push(new nlobjSearchFilter("location",null,"anyof",marketid));
	}
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('firstname',null,null));
	arrColumns.push(new nlobjSearchColumn('lastname',null,null));
	var searchTeamReps = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);
	var searchAllReps = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', null, arrColumns);

	if (type == 'all'){
		var nbrReps = searchAllReps.length;
		for ( var i = 0; searchAllReps != null && i < searchAllReps.length; i++ ) {
			var searchAllRep = searchAllReps[i];
			arrReps.push(searchAllRep.getValue('internalid', null, null));
		}
	}
	else{
		var nbrReps = searchTeamReps.length;
		for ( var i = 0; searchTeamReps != null && i < searchTeamReps.length; i++ ) {
			var searchTeamRep = searchTeamReps[i];
			arrReps.push(searchTeamRep.getValue('internalid', null, null));
		}
	}

	if(searchTeamReps != null){
	 
	var arrFiltersEmails = new Array();
	var arrFiltersPhones = new Array();
	var arrFiltersTasks = new Array();
	var arrFiltersEvents = new Array();

	arrFiltersEmails.push(new nlobjSearchFilter("author",null,"anyof",arrReps));
	arrFiltersEmails.push(new nlobjSearchFilter('messagedate',null,'within',startDate,endDate));
	var searchEmails = nlapiSearchRecord('message', 'customsearch_clgx_chrt_act_messages_rep', arrFiltersEmails, null);
	var columnsEmails = searchEmails[0].getAllColumns();
	
	arrFiltersPhones.push(new nlobjSearchFilter("assigned",null,"anyof",arrReps));
	arrFiltersPhones.push(new nlobjSearchFilter('startdate',null,'within',startDate,endDate));
	var searchPhones = nlapiSearchRecord('phonecall', 'customsearch_clgx_chrt_act_calls_rep', arrFiltersPhones, null);
	var columnsPhones = searchPhones[0].getAllColumns();
	
	arrFiltersTasks.push(new nlobjSearchFilter("assigned",null,"anyof",arrReps));
	arrFiltersTasks.push(new nlobjSearchFilter('completeddate',null,'within',startDate,endDate));
	var searchTasks = nlapiSearchRecord('task', 'customsearch_clgx_chrt_act_tasks_rep', arrFiltersTasks, null);
	var columnsTasks = searchTasks[0].getAllColumns();
	
	arrFiltersEvents.push(new nlobjSearchFilter("owner",null,"anyof",arrReps));
	arrFiltersEvents.push(new nlobjSearchFilter('startdate',null,'within',startDate,endDate));
	var searchEvents = nlapiSearchRecord('calendarevent', 'customsearch_clgx_chrt_act_events_rep', arrFiltersEvents, null);
	var columnsEvents = searchEvents[0].getAllColumns();
	
	// all activities
	var value = parseInt(integer(searchEmails[0].getValue(columnsEmails[0])));
	value += parseInt(integer(searchPhones[0].getValue(columnsPhones[0])));
	value += parseInt(integer(searchTasks[0].getValue(columnsTasks[0])));
	value += parseInt(integer(searchEvents[0].getValue(columnsEvents[0])));
	stReturn += '{y:' + parseFloat(parseInt(value)/parseInt(nbrReps)).toFixed(2) + ',url:\'#\'' + '},';
	
	// activities to customers
	var value = parseInt(integer(searchEmails[0].getValue(columnsEmails[1])));
	value += parseInt(integer(searchPhones[0].getValue(columnsPhones[1])));
	value += parseInt(integer(searchTasks[0].getValue(columnsTasks[1])));
	value += parseInt(integer(searchEvents[0].getValue(columnsEvents[1])));
	stReturn += '{y:' + parseFloat(parseInt(value)/parseInt(nbrReps)).toFixed(2) + ',url:\'#\'' + '},';
	
	// activities to prospects
	var value = parseInt(integer(searchEmails[0].getValue(columnsEmails[2])));
	value += parseInt(integer(searchPhones[0].getValue(columnsPhones[2])));
	value += parseInt(integer(searchTasks[0].getValue(columnsTasks[2])));
	value += parseInt(integer(searchEvents[0].getValue(columnsEvents[2])));
	stReturn += '{y:' + parseFloat(parseInt(value)/parseInt(nbrReps)).toFixed(2) + ',url:\'#\'' + '},';
	
	// activities to targets
	var value = parseInt(integer(searchEmails[0].getValue(columnsEmails[3])));
	value += parseInt(integer(searchPhones[0].getValue(columnsPhones[3])));
	value += parseInt(integer(searchTasks[0].getValue(columnsTasks[3])));
	value += parseInt(integer(searchEvents[0].getValue(columnsEvents[3])));
	stReturn += '{y:' + parseFloat(parseInt(value)/parseInt(nbrReps)).toFixed(2) + ',url:\'#\'' + '},';
	
	// activities to inbound
	var value = parseInt(integer(searchEmails[0].getValue(columnsEmails[4])));
	value += parseInt(integer(searchPhones[0].getValue(columnsPhones[4])));
	value += parseInt(integer(searchTasks[0].getValue(columnsTasks[4])));
	value += parseInt(integer(searchEvents[0].getValue(columnsEvents[4])));
	stReturn += '{y:' + parseFloat(parseInt(value)/parseInt(nbrReps)).toFixed(2) + ',url:\'#\'' + '},';

	}
	else{
		stReturn += '{y:0,url:\'#\'' + '},{y:0,url:\'#\'' + '},{y:0,url:\'#\'' + '},{y:0,url:\'#\'' + '},{y:0,url:\'#\'' + '}';
		
	}
	stReturn += ']';
    return stReturn;
}

function getDateRange(period, periodid){
	if (period == 'week'){
		var now = new Date;
		now.setDate(now.getDate() - (parseInt(periodid) * 7));
		var startDate = new Date(now.setDate(now.getDate() - now.getDay()+1));
		var endDate = new Date(now.setDate(now.getDate() - now.getDay()+7));
	}
	else{
		var startDate = new Date();
		startDate.setDate(startDate.getDate()-30);
		var endDate = new Date;
	}
    var arrDateRange = new Array();
        arrDateRange[0] = getStrDate(startDate);
        arrDateRange[1] = getStrDate(endDate);
    return arrDateRange;
}

function getStrDate(date){
    var month = parseInt(date.getMonth()) + 1;
    var day = date.getDate();
    var year = date.getFullYear();
    if(parseInt(month) < 10){
        month = '0' + month;
    }
    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = month + '/' + day + '/' + year;
    return formattedDate;
}

function integer(str) {
	if(str == '' || str == 'NaN' || str == null)  {
		return 0;
	}
	else {
		return parseInt(str);
	}
} 

function periodTitle (period, periodid){
	if (period == 'month'){
		return 'Last 30 Days';
	}
	else{
		switch(parseInt(periodid)) {
		case 0:
			return 'This Week';
			break;
		case 1:
			return 'Last Week';
			break;
		case 2:
			return '2 Weeks Ago';
			break;
		case 3:
			return '3 Weeks Ago';
			break;
		case 4:
			return '4 Weeks Ago';
			break;
		default:
			return '';
		}
	}
}

