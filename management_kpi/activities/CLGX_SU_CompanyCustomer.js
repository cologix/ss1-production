/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 Sep 2017     alex.guzenski
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */

function beforeSubmit(type) {
	var organizer     = nlapiGetFieldValue("organizer");
	var attendeeCount = nlapiGetLineItemCount("attendee");
	var attendee      = -1;
	
	nlapiLogExecution("DEBUG", "organizer",     organizer);
	nlapiLogExecution("DEBUG", "attendeeCount", attendeeCount);
	
	for(var a = 1; a <= attendeeCount; a++) {
		attendee = nlapiGetLineItemValue("attendee", "attendee", a);
		
		nlapiLogExecution("DEBUG", "attendee", attendee);
		
		if(organizer != attendee) {
			try {
				var record = nlapiLoadRecord("contact", attendee);
				nlapiLogExecution("DEBUG", "customerCompany", record.getFieldValue("company"));
				
				nlapiSetFieldValue("company", record.getFieldValue("company"));
				nlapiSetFieldValue("contact", attendee);
				
				break;
			} catch(ex) {
				nlapiLogExecution("DEBUG", "Event Error", ex);
				
				continue;
			}
		}
	}
}