nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CHARTS_Activities_Update.js
//	Script Name:	CLGX_SS_CHARTS_Activities_Update
//	Script Id:		customscript_clgx_ss_chart_activ_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_ss_chrt_activities_update(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');

        var context = nlapiGetContext();

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        var searchEmployees = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', null, arrColumns);

        var arrEmployees = new Array();
        for ( var i = 0; searchEmployees != null && i < searchEmployees.length; i++ ) {
            var searchEmployee = searchEmployees[i];
            var repid = searchEmployee.getValue('internalid', null, null);
            arrEmployees.push(repid);
        }
        //var arrEmployees=[288];
        var arrNbrActivities = new Array();
        arrNbrActivities[0] = 0;
        arrNbrActivities[1] = 0;
        arrNbrActivities[2] = 0;
        arrNbrActivities[3] = 0;
        arrNbrActivities[4] = 0;
        
        var arrRecords = new Array();
        arrRecords[0] = 'message';
        arrRecords[1] = 'phonecall';
        arrRecords[2] = 'calendarevent';
        arrRecords[3] = 'task';
        arrRecords[4] = 'task';
        
        var arrSearches = new Array();
        arrSearches[0] = 'customsearch_clgx_chrt_act_messages_all';
        arrSearches[1] = 'customsearch_clgx_chrt_act_calls_all';
        arrSearches[2] = 'customsearch_clgx_chrt_act_events_all';
        arrSearches[3] = 'customsearch_clgx_chrt_act_tasks_all';
        arrSearches[4] = 'customsearch_clgx_chrt_act_visits_all';
        
        var arrDates = new Array();
        arrDates[0] = 'messagedate';
        arrDates[1] = 'startdate';
        arrDates[2] = 'startdate';
        //arrDates[3] = 'completeddate';
        arrDates[3] = 'enddate';
        arrDates[4] = 'enddate';
        
        var arrReps = new Array();
        arrReps[0] = 'author';
        arrReps[1] = 'assigned';
        arrReps[2] = 'owner';
        arrReps[3] = 'assigned';
        arrReps[4] = 'assigned';
        
        var endDate = new moment().endOf('month').format('M/D/YYYY');
        var startDate = new moment().subtract('days',35).format('M/D/YYYY');

        for ( var i = 0; i < 5; i++ ) {

        	var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter(arrDates[i],null,'within',startDate,endDate));
            arrFilters.push(new nlobjSearchFilter(arrReps[i],null,'anyof',arrEmployees));
            var searchActivities = nlapiLoadSearch(arrRecords[i], arrSearches[i]);
            searchActivities.addFilters(arrFilters);
			var resultSet = searchActivities.runSearch();
			resultSet.forEachResult(function(searchResult) {
				/*
				if ( context.getRemainingUsage() <= 1000 && (i+1) < searchActivities.length ){
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    if ( status == 'QUEUED' ) {
                        nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
                        break;
                    }
                }
				*/
				
				var taskType = '';
				var columnsActivities = searchResult.getAllColumns();
                var activityID = searchResult.getValue(columnsActivities[0]);
                
                var contactID = searchResult.getValue(columnsActivities[1]);
                
                
                //nlapiLogExecution('DEBUG','debug', '| activityID = ' + activityID + ' | contactID = ' + contactID + ' | ');
        
                if (i == 4){
                    var activityDate = searchResult.getValue(columnsActivities[7]);
                    taskType = searchResult.getText(columnsActivities[8]);
                }
                else if (i == 3){
                    var activityDate = searchResult.getValue(columnsActivities[6]);
                    taskType = searchResult.getText(columnsActivities[7]);
                }
                else{
                    var activityDate = searchResult.getValue(columnsActivities[4]);
                }
                var customerID = searchResult.getValue(columnsActivities[3]);
                var customerName = searchResult.getText(columnsActivities[3]);

                var repID = searchResult.getValue(columnsActivities[5]);
                var repName = searchResult.getText(columnsActivities[5]);
                if(i == 4){
                    var status = searchResult.getText(columnsActivities[6]);
                }
                
                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('internalid',null,null));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_sales_activ_id',null,null));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter('custrecord_clgx_sales_activ_id',null,'equalto',activityID));
                var searchCustomActivities = nlapiSearchRecord('customrecord_cologix_sales_activities', null, arrFilters, arrColumns);

                if(searchCustomActivities == null){

                    if(customerID != null && customerID != ''){
                        var arrFilters = new Array();
                        arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof',customerID));
                        var searchCustomer = nlapiSearchRecord('customer', null, arrFilters, null);
                        
                        var arrFilters = new Array();
                        arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof',customerID));
                        var searchPartner = nlapiSearchRecord('partner', null, arrFilters, null);
                    }

                    if ((searchCustomer != null && customerID != null && customerID != '') || (searchPartner != null && customerID != null && customerID != '')){

                        if(contactID != null && contactID != ''){
                            var contactDetails = nlapiLookupField('contact', contactID, ['entityid']);
                            var contactName = contactDetails.entityid;
                        }
                        else{
                            var contactID = 0;
                            var contactName = '';
                        }

                        if (searchCustomer != null){
                            var customerDetails = nlapiLookupField('customer', customerID, ['status','leadsource']);
                            var customerType = customerDetails.status;
                            var customerLeadSource = customerDetails.leadsource;
                            
                            var repDetails = nlapiLookupField('employee', repID, ['entityid', 'location']);
                            var repName = repDetails.entityid;
                            var locationID = repDetails.location;
                            var locationDetails = nlapiLookupField('location', locationID, ['name']);
                            var locationName = locationDetails.name;
                            
                            nlapiLogExecution('DEBUG','Created Activity', '| customerID = ' + customerID + ' | ');
                            
                    	}
                    	else{ // partner
                    		
                    		nlapiLogExecution('DEBUG','Created Activity', '| partnerID = ' + customerID + ' | ');
                    		
                            var customerDetails = nlapiLookupField('partner', customerID, ['custentity_clgx_sales_rep']);
                            var customerType = 13;
                            var customerLeadSource = '';
                            repID = customerDetails.custentity_clgx_sales_rep;
                            
                            var repDetails = nlapiLookupField('employee', repID, ['entityid', 'location']);
                            var repName = repDetails.entityid;
                            var locationID = repDetails.location;
                            var locationDetails = nlapiLookupField('location', locationID, ['name']);
                            var locationName = locationDetails.name;
                    	}

                        if(taskType == 'Meeting' && (searchPartner != null && customerID != null && customerID != '')){
                        	 // if meeting and is partner, don't add it
                        } else {
                        	
                            var record = nlapiCreateRecord('customrecord_cologix_sales_activities');
                            //record.setFieldValue('custrecord_clgx_sales_activ', activityID);
                            record.setFieldValue('custrecord_clgx_sales_activ_id', activityID);
                            if(i == 4){
                                record.setFieldValue('custrecord_clgx_sales_activ_type', 'visit');
                                record.setFieldValue('custrecord_clgx_sales_activ_status', status);
                            }
                            else{
                                record.setFieldValue('custrecord_clgx_sales_activ_type', arrRecords[i]);
                            }
                            
                            if(taskType == 'Meeting'){
                                record.setFieldValue('custrecord_clgx_sales_activ_type', 'meeting');
                            }
                            
                            nlapiLogExecution('DEBUG','Started Execution', ' | i = ' + i + ' |' + '| activityID = ' + activityID + ' |' + ' | activityDate = ' + activityDate + ' |');
                            record.setFieldValue('custrecord_clgx_sales_activ_date', moment(activityDate).format('M/D/YYYY'));
                            record.setFieldValue('custrecord_clgx_sales_activ_location_id', locationID);
                            record.setFieldValue('custrecord_clgx_sales_activ_location', locationName);
                            record.setFieldValue('custrecord_clgx_sales_activ_rep_id', repID);
                            record.setFieldValue('custrecord_clgx_sales_activ_rep_name', repName);
                            record.setFieldValue('custrecord_clgx_sales_activ_customer_id', customerID);
                            record.setFieldValue('custrecord_clgx_sales_activ_customer', customerName);
                            record.setFieldValue('custrecord_clgx_sales_activ_contact_id', contactID);
                            record.setFieldValue('custrecord_clgx_sales_activ_contact', contactName);
                            record.setFieldValue('custrecord_clgx_sales_activ_custom_type', customerType);
                            record.setFieldValue('custrecord_clgx_sales_activ_lead_source', customerLeadSource);
                            record.setFieldValue('custrecord_clgx_sales_activ_task_type', taskType);
                            var idRec = nlapiSubmitRecord(record, true,true);
                            arrNbrActivities[i] += 1;
                        }
                        
                    }
                    //nlapiLogExecution('DEBUG','Created Activity', '| ' + arrRecords[i] + ' | ' + searchActivities.length + ' | Remaining: ' + nlapiGetContext().getRemainingUsage() + ' |');

                }
                else{
                    if(i == 4){
                        nlapiSubmitField('customrecord_cologix_sales_activities', searchCustomActivities[0].getValue('internalid',null,null), 'custrecord_clgx_sales_activ_status', status);
                    }
                }
            
				return true;
			});
            
        }
        
        /*
        var totalActivities = parseInt(arrNbrActivities[0]) + parseInt(arrNbrActivities[1]) + parseInt(arrNbrActivities[2]) + parseInt(arrNbrActivities[3]) + parseInt(arrNbrActivities[4]);
        
         var emailSubject = totalActivities + ' activities added.';
         var emailBody = 'Emails:  ' + parseInt(arrNbrActivities[0]) + '\n';
         emailBody += 'Phones:  ' + parseInt(arrNbrActivities[1]) + '\n';
         emailBody += 'Events:  ' + parseInt(arrNbrActivities[2]) + '\n';
         emailBody += 'Tasks:  ' + parseInt(arrNbrActivities[3]) + '\n';
         emailBody += 'Visits:  ' + parseInt(arrNbrActivities[4]) + '\n\n\n\n';
         nlapiSendEmail(71418, 71418,emailSubject,emailBody,null,null,null,null); // Send email to Dan
         */

        /*
         // update status on Visits of last 30 days
         var searchResults = nlapiSearchRecord('customrecord_cologix_sales_activities', 'customsearch_clgx_kpi_visits_status');

         for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {

	         var searchResult = searchResults[i];
	         var recordID = parseInt(searchResult.getValue('internalid',null,null));
	         var activInt = parseInt(searchResult.getValue('custrecord_clgx_sales_activ_id',null,null));
	
	         var arrColumns = new Array();
	         arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	         var arrFilters = new Array();
	         arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",activInt));
	         var searchTask = nlapiSearchRecord('task', null, arrFilters, arrColumns);
	
	         if(searchTask != null){
	         var recTask = nlapiLoadRecord ('task', activInt);
	         var status = recTask.getFieldText('status');
	
	         var recVisit = nlapiLoadRecord ('customrecord_cologix_sales_activities',recordID);
	         recVisit.setFieldValue('custrecord_clgx_sales_activ_status', status);
	         nlapiSubmitRecord(recVisit, false,true);
	         }
	         else{ // this task was deleted?  - mark visit as deleted
	         nlapiSubmitField('customrecord_cologix_sales_activities', recordID, 'custrecord_clgx_sales_activ_deleted', 'T');
	         }

         }
         */
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|');
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}