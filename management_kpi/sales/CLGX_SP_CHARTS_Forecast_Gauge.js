nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SP_CHARTS_Forecast_Gauge.js
//	Script Name:	CLGX_SP_CHARTS_Forecast_Gauge
//	Script Id:		customscript_clgx_sp_chrt_forecast_gauge
//	Script Runs:	On Server
//	Script Type:	Portlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		08/08/2013
//-------------------------------------------------------------------------------------------------

function portlet_chrt_forecast_gauge (portlet, column){
	portlet.setTitle('Sales Forecast');
	var html = '<iframe name="chartsFrame" id="chartsFrame" src="/app/site/hosting/scriptlet.nl?script=218&deploy=1" height="380px" width="340px" frameborder="0" scrolling="no"></iframe>';
	portlet.setHtml(html);
}
