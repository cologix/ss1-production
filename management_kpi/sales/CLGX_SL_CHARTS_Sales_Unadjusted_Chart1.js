nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Sales_Unadjusted_Chart.js
//	Script Name:	CLGX_SL_CHARTS_Sales_Unadjusted_Chart
//	Script Id:		customscript_clgx_sl_chrt_sale_unadj_chrt
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		07/18/2013
//	Includes:		CLGX_LIB_Global.js
//	URL:			/app/site/hosting/scriptlet.nl?script=211&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_charts_sales_unadjusted_chart (request, response){
    try {

        var departmentid = request.getParameter('departmentid');
        var marketid = request.getParameter('marketid');

        if(marketid != '' ){
            if(departmentid == 0){
                var objFile = nlapiLoadFile(549675); // for markets load file with the budget
            }
            else{
                var objFile = nlapiLoadFile(2722262); // for departments load file without the budget
            }
            var html = objFile.getValue();

            html = html.replace(new RegExp('{categories}','g'),returnMonthsCategories());
            if((marketid == 0 && departmentid == 0) || (departmentid == 9 || departmentid == 23 || departmentid == 24 || departmentid == 25 || departmentid == 26)){
                html = html.replace(new RegExp('{series}','g'),returnMarketSeries(departmentid,marketid));
            }
            else{
                html = html.replace(new RegExp('{series}','g'),returnRepsSeries(departmentid,marketid));
            }

            if(parseInt(departmentid) > 0){
                html = html.replace(new RegExp('{title}','g'), 'Departments Gross Sales - ' + clgx_return_region_name(departmentid));
            }
            else{
                html = html.replace(new RegExp('{title}','g'), 'Unadjusted Gross Sales - ' + clgx_return_market_name(marketid));
            }

        }
        else{
            var html = 'Please select a report from the left panel.';
        }
        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function returnMarketSeries(departmentid,marketid){
    var stReturn = '[';

    var arrFilters = new Array();
    //arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    var searchEmployees = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);
    var arrEmployees = new Array();
    for ( var i = 0; searchEmployees != null && i < searchEmployees.length; i++ ) {
        arrEmployees.push(searchEmployees[i].getValue('internalid', null, null));
    }


    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("salesrep",null,"anyof",arrEmployees));

    if(departmentid == 9 || departmentid == 23 || departmentid == 24 || departmentid == 25 || departmentid == 26){
        arrFilters.push(new nlobjSearchFilter("department","salesRep","anyof", clgx_get_region_departments(departmentid)));
    }

    var arrColumns = new Array();
    if(departmentid > 0){ // departments tree menu
        if(departmentid == 9){ // if all departments (Sales departments)
            var searchMarkets = nlapiSearchRecord('transaction', 'customsearch_clgx_kpi_sales_reg', arrFilters, arrColumns);
            var searchMarketsIncrMRC = nlapiSearchRecord('transaction', 'customsearch_clgx_kpi_sales_reg_incr_mrc', arrFilters, arrColumns);
        }
        else{ // if regions
            var searchMarkets = nlapiSearchRecord('transaction', 'customsearch_clgx_kpi_sales_dep', arrFilters, arrColumns);
            var searchMarketsIncrMRC = nlapiSearchRecord('transaction', 'customsearch_clgx_kpi_sales_dep_incr_mrc', arrFilters, arrColumns);
        }
    }
    else{
        var searchMarkets = nlapiSearchRecord('transaction', 'customsearch_clgx_kpi_sales_all', arrFilters, arrColumns);
        var searchMarketsIncrMRC = nlapiSearchRecord('transaction', 'customsearch_clgx_kpi_sales_all_incr_mrc', arrFilters, arrColumns);
    }

    var arrTotals = new Array();
    arrTotals = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
        var searchMarket = searchMarkets[i];
        var columns = searchMarket.getAllColumns();
        var added=0;
        if(searchMarketsIncrMRC != null){
            for ( var j = 0; searchMarketsIncrMRC != null && j < searchMarketsIncrMRC.length; j++ ) {
                var searchMarket1 = searchMarketsIncrMRC[j];
                var columns1 = searchMarket1.getAllColumns();

                if(departmentid > 0){
                    var text0 = searchMarket.getValue(columns[0]);
                    var text1 = searchMarket1.getValue(columns1[0]);
                }
                else{
                    var text0 = searchMarket.getText(columns[0]);
                    var text1 = searchMarket1.getText(columns1[0]);
                }


                if(text0==text1)
                {
                    stReturn += '\n{name: \'' + text0 + '\',type: \'column\',data: [\n';
                    stReturn += '{y:' + (parseFloat(searchMarket.getValue(columns[7]))+parseFloat(searchMarket1.getValue(columns1[7]))) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-6\'},\n';
                    stReturn += '{y:' + (parseFloat(searchMarket.getValue(columns[6]))+parseFloat(searchMarket1.getValue(columns1[6]))) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-5\'},\n';
                    stReturn += '{y:' + (parseFloat(searchMarket.getValue(columns[5]))+parseFloat(searchMarket1.getValue(columns1[5]))) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-4\'},\n';
                    stReturn += '{y:' + (parseFloat(searchMarket.getValue(columns[4]))+parseFloat(searchMarket1.getValue(columns1[4]))) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-3\'},\n';
                    stReturn += '{y:' + (parseFloat(searchMarket.getValue(columns[3]))+parseFloat(searchMarket1.getValue(columns1[3]))) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-2\'},\n';
                    stReturn += '{y:' + (parseFloat(searchMarket.getValue(columns[2]))+parseFloat(searchMarket1.getValue(columns1[2]))) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-1\'},\n';
                    stReturn += '{y:' + (parseFloat(searchMarket.getValue(columns[1]))+parseFloat(searchMarket1.getValue(columns1[1]))) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=0\'}\n';
                    stReturn += ']\n';
                    stReturn += '},';
                    arrTotals[0] += (parseFloat(searchMarket.getValue(columns[2]))+parseFloat(searchMarket1.getValue(columns1[2])));
                    arrTotals[1] += (parseFloat(searchMarket.getValue(columns[3]))+parseFloat(searchMarket1.getValue(columns1[3])));
                    arrTotals[2] += (parseFloat(searchMarket.getValue(columns[4]))+parseFloat(searchMarket1.getValue(columns1[4])));
                    arrTotals[3] += (parseFloat(searchMarket.getValue(columns[5]))+parseFloat(searchMarket1.getValue(columns1[5])));
                    arrTotals[4] += (parseFloat(searchMarket.getValue(columns[6]))+parseFloat(searchMarket1.getValue(columns1[6])));
                    arrTotals[5] += (parseFloat(searchMarket.getValue(columns[7]))+parseFloat(searchMarket1.getValue(columns1[7])));
                    arrTotals[6] += (parseFloat(searchMarket.getValue(columns[8]))+parseFloat(searchMarket1.getValue(columns1[8])));
                    arrTotals[7] += (parseFloat(searchMarket.getValue(columns[9]))+parseFloat(searchMarket1.getValue(columns1[9])));
                    arrTotals[8] += (parseFloat(searchMarket.getValue(columns[10]))+parseFloat(searchMarket1.getValue(columns1[10])));
                    arrTotals[9] += (parseFloat(searchMarket.getValue(columns[11]))+parseFloat(searchMarket1.getValue(columns1[11])));
                    arrTotals[10] += (parseFloat(searchMarket.getValue(columns[12]))+parseFloat(searchMarket1.getValue(columns1[12])));
                    arrTotals[11] += (parseFloat(searchMarket.getValue(columns[13]))+parseFloat(searchMarket1.getValue(columns1[13])));
                    arrTotals[12] += (parseFloat(searchMarket.getValue(columns[14]))+parseFloat(searchMarket1.getValue(columns1[14])));
                    arrTotals[13] += (parseFloat(searchMarket.getValue(columns[15]))+parseFloat(searchMarket1.getValue(columns1[15])));
                    arrTotals[14] += (parseFloat(searchMarket.getValue(columns[16]))+parseFloat(searchMarket1.getValue(columns1[16])));
                    arrTotals[15] += (parseFloat(searchMarket.getValue(columns[17]))+parseFloat(searchMarket1.getValue(columns1[17])));
                    arrTotals[16] += (parseFloat(searchMarket.getValue(columns[18]))+parseFloat(searchMarket1.getValue(columns1[18])));
                    arrTotals[17] += (parseFloat(searchMarket.getValue(columns[19]))+parseFloat(searchMarket1.getValue(columns1[19])));
                    added=1;
                }
            }
        }
        if(added==0)
        {
            stReturn += '\n{name: \'' + text0 + '\',type: \'column\',data: [\n';
            stReturn += '{y:' + searchMarket.getValue(columns[7]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-6\'},\n';
            stReturn += '{y:' + searchMarket.getValue(columns[6]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-5\'},\n';
            stReturn += '{y:' + searchMarket.getValue(columns[5]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-4\'},\n';
            stReturn += '{y:' + searchMarket.getValue(columns[4]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-3\'},\n';
            stReturn += '{y:' + searchMarket.getValue(columns[3]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-2\'},\n';
            stReturn += '{y:' + searchMarket.getValue(columns[2]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-1\'},\n';
            stReturn += '{y:' + searchMarket.getValue(columns[1]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=0\'}\n';
            stReturn += ']\n';
            stReturn += '},';
            arrTotals[0] += parseFloat(searchMarket.getValue(columns[2]));
            arrTotals[1] += parseFloat(searchMarket.getValue(columns[3]));
            arrTotals[2] += parseFloat(searchMarket.getValue(columns[4]));
            arrTotals[3] += parseFloat(searchMarket.getValue(columns[5]));
            arrTotals[4] += parseFloat(searchMarket.getValue(columns[6]));
            arrTotals[5] += parseFloat(searchMarket.getValue(columns[7]));
            arrTotals[6] += parseFloat(searchMarket.getValue(columns[8]));
            arrTotals[7] += parseFloat(searchMarket.getValue(columns[9]));
            arrTotals[8] += parseFloat(searchMarket.getValue(columns[10]));
            arrTotals[9] += parseFloat(searchMarket.getValue(columns[11]));
            arrTotals[10] += parseFloat(searchMarket.getValue(columns[12]));
            arrTotals[11] += parseFloat(searchMarket.getValue(columns[13]));
            arrTotals[12] += parseFloat(searchMarket.getValue(columns[14]));
            arrTotals[13] += parseFloat(searchMarket.getValue(columns[15]));
            arrTotals[14] += parseFloat(searchMarket.getValue(columns[16]));
            arrTotals[15] += parseFloat(searchMarket.getValue(columns[17]));
            arrTotals[16] += parseFloat(searchMarket.getValue(columns[18]));
            arrTotals[17] += parseFloat(searchMarket.getValue(columns[19]));

        }


    }


    var arrAverages = new Array();
    arrAverages = [0,0,0,0,0,0,0];

    arrAverages[0] = parseFloat((arrTotals[17] + arrTotals[16] + arrTotals[15] + arrTotals[14] + arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6])/12).toFixed(2);
    arrAverages[1] = parseFloat((arrTotals[16] + arrTotals[15] + arrTotals[14] + arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5])/12).toFixed(2);
    arrAverages[2] = parseFloat((arrTotals[15] + arrTotals[14] + arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4])/12).toFixed(2);
    arrAverages[3] = parseFloat((arrTotals[14] + arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4] + arrTotals[3])/12).toFixed(2);
    arrAverages[4] = parseFloat((arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4] + arrTotals[3] + arrTotals[2])/12).toFixed(2);
    arrAverages[5] = parseFloat((arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4] + arrTotals[3] + arrTotals[2] + arrTotals[1])/12).toFixed(2);
    arrAverages[6] = parseFloat((arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4] + arrTotals[3] + arrTotals[2] + arrTotals[1] + arrTotals[0])/12).toFixed(2);

    stReturn += '\n{name: \'Average\',type: \'spline\', color: \'#0000CC\',data: [\n';
    for(var z=0; z < arrAverages.length; z++){
        stReturn += '{y:' + arrAverages[z] + ',url:\'#\'},';
    }
    var strLen = stReturn.length;
    stReturn = stReturn.slice(0,strLen-1);

    stReturn += ']\n';
    stReturn += '},';

    if(departmentid == 0){
        stReturn += '\n{name: \'Budget\',type: \'scatter\', color: \'#FF4DFF\',data: [\n';
        for(var j=0; j < 7; j++){

            var index = 6 - j;
            var date = new moment();
            var year = date.subtract('months',index).format('YYYY');
            var month = date.format('M');

            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_clgx_sales_budget',null,'SUM'));
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_clgx_sales_month",null,"equalto", month));
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_clgx_sales_year",null,"equalto", year));
            var searchBudgets = nlapiSearchRecord('customrecord_clgx_cologix_sales', null, arrFilters, arrColumns);

            var searchBudget = searchBudgets[0];
            var columns = searchBudget.getAllColumns();
            var monthBudget = searchBudget.getValue(columns[0]);

            if(monthBudget == null || monthBudget == ''){
                monthBudget = 0;
            }
            stReturn += '{y:' + monthBudget + ',url:\'#\'},\n';
        }

        var strLen = stReturn.length;
        stReturn = stReturn.slice(0,strLen-1);

        stReturn += ']\n';
        stReturn += '}';
    }

    stReturn += ']';

    return stReturn;
}

function returnRepsSeries(departmentid,marketid){

    var market = clgx_return_market_name(marketid);

    var stReturn = '[';

    var arrFilters = new Array();
    //arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    var searchEmployees = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);

    var arrEmployees = new Array();
    for ( var i = 0; searchEmployees != null && i < searchEmployees.length; i++ ) {
        arrEmployees.push(searchEmployees[i].getValue('internalid', null, null));
    }

    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("salesrep",null,"anyof",arrEmployees));
    //arrFilters.push(new nlobjSearchFilter("location",null,"anyof",clgx_return_child_locations_of_marketid(marketid)));
    if(departmentid == 0){
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_location",'custrecord_clgx_totals_transaction',"anyof",clgx_return_child_locations_of_marketid(marketid)));
    }
    else{
        arrFilters.push(new nlobjSearchFilter("department","salesRep","anyof", departmentid));
    }
    var searchReps = nlapiSearchRecord('transaction', 'customsearch_clgx_kpi_sales_reps', arrFilters, null);
    var searchRepsIncrMRC = nlapiSearchRecord('transaction', 'customsearch_clgx_kpi_sales_rep_incr_mrc', arrFilters, null);


    var arrTotals = new Array();
    arrTotals = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    var arrSSalesRepIds=new Array();
    for ( var i = 0; searchReps != null && i < searchReps.length; i++ ) {
        var searchRep = searchReps[i];
        var columns = searchRep.getAllColumns();
        var added=0;
        if(searchRepsIncrMRC!=null)
        {
            for ( var j = 0; searchRepsIncrMRC != null && j < searchRepsIncrMRC.length; j++ ) {
                var searchRep1 = searchRepsIncrMRC[j];
                var columns1 = searchRep1.getAllColumns();
                if(searchRep.getValue(columns[1])==searchRep1.getValue(columns1[1]))
                {
                    stReturn += '\n{name: \'' + searchRep.getValue(columns[1]) + '\',type: \'column\',data: [\n';
                    stReturn += '{y:' + (parseFloat(searchRep.getValue(columns[8]))+parseFloat(searchRep1.getValue(columns1[8])))+ ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-6\'},\n';
                    stReturn += '{y:' + (parseFloat(searchRep.getValue(columns[7]))+parseFloat(searchRep1.getValue(columns1[7]))) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-5\'},\n';
                    stReturn += '{y:' + (parseFloat(searchRep.getValue(columns[6]))+parseFloat(searchRep1.getValue(columns1[6]))) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-4\'},\n';
                    stReturn += '{y:' + (parseFloat(searchRep.getValue(columns[5]))+parseFloat(searchRep1.getValue(columns1[5]))) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-3\'},\n';
                    stReturn += '{y:' + (parseFloat(searchRep.getValue(columns[4]))+parseFloat(searchRep1.getValue(columns1[4]))) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-2\'},\n';
                    stReturn += '{y:' + (parseFloat(searchRep.getValue(columns[3]))+parseFloat(searchRep1.getValue(columns1[3]))) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-1\'},\n';
                    stReturn += '{y:' + (parseFloat(searchRep.getValue(columns[2]))+parseFloat(searchRep1.getValue(columns1[2]))) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=0\'}\n';
                    stReturn += ']\n';
                    stReturn += '},';

                    arrTotals[0] += parseFloat(searchRep.getValue(columns[3]));
                    arrTotals[1] += parseFloat(searchRep.getValue(columns[4]));
                    arrTotals[2] += parseFloat(searchRep.getValue(columns[5]));
                    arrTotals[3] += parseFloat(searchRep.getValue(columns[6]));
                    arrTotals[4] += parseFloat(searchRep.getValue(columns[7]));
                    arrTotals[5] += parseFloat(searchRep.getValue(columns[8]));
                    arrTotals[6] += parseFloat(searchRep.getValue(columns[9]));
                    arrTotals[7] += parseFloat(searchRep.getValue(columns[10]));
                    arrTotals[8] += parseFloat(searchRep.getValue(columns[11]));
                    arrTotals[9] += parseFloat(searchRep.getValue(columns[12]));
                    arrTotals[10] += parseFloat(searchRep.getValue(columns[13]));
                    arrTotals[11] += parseFloat(searchRep.getValue(columns[14]));
                    arrTotals[12] += parseFloat(searchRep.getValue(columns[15]));
                    arrTotals[13] += parseFloat(searchRep.getValue(columns[16]));
                    arrTotals[14] += parseFloat(searchRep.getValue(columns[17]));
                    arrTotals[15] += parseFloat(searchRep.getValue(columns[18]));
                    arrTotals[16] += parseFloat(searchRep.getValue(columns[19]));
                    arrTotals[17] += parseFloat(searchRep.getValue(columns[20]));
                    added=1;
                    if(!(in_array(searchRep.getValue(columns[0]),arrSSalesRepIds)))
                    {
                        arrSSalesRepIds.push(searchRep.getValue(columns[0]));
                    }
                }
            }
        }
        if(added==0)
        {
            stReturn += '\n{name: \'' + searchRep.getValue(columns[1]) + '\',type: \'column\',data: [\n';
            stReturn += '{y:' + searchRep.getValue(columns[8])+ ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-6\'},\n';
            stReturn += '{y:' + searchRep.getValue(columns[7]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-5\'},\n';
            stReturn += '{y:' +searchRep.getValue(columns[6]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-4\'},\n';
            stReturn += '{y:' + searchRep.getValue(columns[5]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-3\'},\n';
            stReturn += '{y:' + searchRep.getValue(columns[4]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-2\'},\n';
            stReturn += '{y:' + searchRep.getValue(columns[3]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-1\'},\n';
            stReturn += '{y:' + searchRep.getValue(columns[2]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=0\'}\n';
            stReturn += ']\n';
            stReturn += '},';
            arrTotals[0] += parseFloat(searchRep.getValue(columns[3]));
            arrTotals[1] += parseFloat(searchRep.getValue(columns[4]));
            arrTotals[2] += parseFloat(searchRep.getValue(columns[5]));
            arrTotals[3] += parseFloat(searchRep.getValue(columns[6]));
            arrTotals[4] += parseFloat(searchRep.getValue(columns[7]));
            arrTotals[5] += parseFloat(searchRep.getValue(columns[8]));
            arrTotals[6] += parseFloat(searchRep.getValue(columns[9]));
            arrTotals[7] += parseFloat(searchRep.getValue(columns[10]));
            arrTotals[8] += parseFloat(searchRep.getValue(columns[11]));
            arrTotals[9] += parseFloat(searchRep.getValue(columns[12]));
            arrTotals[10] += parseFloat(searchRep.getValue(columns[13]));
            arrTotals[11] += parseFloat(searchRep.getValue(columns[14]));
            arrTotals[12] += parseFloat(searchRep.getValue(columns[15]));
            arrTotals[13] += parseFloat(searchRep.getValue(columns[16]));
            arrTotals[14] += parseFloat(searchRep.getValue(columns[17]));
            arrTotals[15] += parseFloat(searchRep.getValue(columns[18]));
            arrTotals[16] += parseFloat(searchRep.getValue(columns[19]));
            arrTotals[17] += parseFloat(searchRep.getValue(columns[20]));
            if(!(in_array(searchRep.getValue(columns[0]),arrSSalesRepIds)))
            {
                arrSSalesRepIds.push(searchRep.getValue(columns[0]));
            }
        }
    }
    if((searchRepsIncrMRC!=null)&&(arrSSalesRepIds.length>0)) {
        for (var j = 0; searchRepsIncrMRC != null && j < searchRepsIncrMRC.length; j++) {
            var searchRep = searchRepsIncrMRC[j];
            var columns = searchRep.getAllColumns();
            var idSRep=searchRep.getValue(columns[0]);
            if(!in_array(idSRep,arrSSalesRepIds))
            {
                stReturn += '\n{name: \'' + searchRep.getValue(columns[1]) + '\',type: \'column\',data: [\n';
                stReturn += '{y:' + searchRep.getValue(columns[8])+ ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-6\'},\n';
                stReturn += '{y:' + searchRep.getValue(columns[7]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-5\'},\n';
                stReturn += '{y:' +searchRep.getValue(columns[6]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-4\'},\n';
                stReturn += '{y:' + searchRep.getValue(columns[5]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-3\'},\n';
                stReturn += '{y:' + searchRep.getValue(columns[4]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-2\'},\n';
                stReturn += '{y:' + searchRep.getValue(columns[3]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-1\'},\n';
                stReturn += '{y:' + searchRep.getValue(columns[2]) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=0\'}\n';
                stReturn += ']\n';
                stReturn += '},';
                arrTotals[0] += parseFloat(searchRep.getValue(columns[3]));
                arrTotals[1] += parseFloat(searchRep.getValue(columns[4]));
                arrTotals[2] += parseFloat(searchRep.getValue(columns[5]));
                arrTotals[3] += parseFloat(searchRep.getValue(columns[6]));
                arrTotals[4] += parseFloat(searchRep.getValue(columns[7]));
                arrTotals[5] += parseFloat(searchRep.getValue(columns[8]));
                arrTotals[6] += parseFloat(searchRep.getValue(columns[9]));
                arrTotals[7] += parseFloat(searchRep.getValue(columns[10]));
                arrTotals[8] += parseFloat(searchRep.getValue(columns[11]));
                arrTotals[9] += parseFloat(searchRep.getValue(columns[12]));
                arrTotals[10] += parseFloat(searchRep.getValue(columns[13]));
                arrTotals[11] += parseFloat(searchRep.getValue(columns[14]));
                arrTotals[12] += parseFloat(searchRep.getValue(columns[15]));
                arrTotals[13] += parseFloat(searchRep.getValue(columns[16]));
                arrTotals[14] += parseFloat(searchRep.getValue(columns[17]));
                arrTotals[15] += parseFloat(searchRep.getValue(columns[18]));
                arrTotals[16] += parseFloat(searchRep.getValue(columns[19]));
                arrTotals[17] += parseFloat(searchRep.getValue(columns[20]));
            }
        }
    }


    var arrAverages = new Array();
    arrAverages = [0,0,0,0,0,0,0];

    arrAverages[0] = parseFloat((arrTotals[17] + arrTotals[16] + arrTotals[15] + arrTotals[14] + arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6])/12).toFixed(2);
    arrAverages[1] = parseFloat((arrTotals[16] + arrTotals[15] + arrTotals[14] + arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5])/12).toFixed(2);
    arrAverages[2] = parseFloat((arrTotals[15] + arrTotals[14] + arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4])/12).toFixed(2);
    arrAverages[3] = parseFloat((arrTotals[14] + arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4] + arrTotals[3])/12).toFixed(2);
    arrAverages[4] = parseFloat((arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4] + arrTotals[3] + arrTotals[2])/12).toFixed(2);
    arrAverages[5] = parseFloat((arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4] + arrTotals[3] + arrTotals[2] + arrTotals[1])/12).toFixed(2);
    arrAverages[6] = parseFloat((arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4] + arrTotals[3] + arrTotals[2] + arrTotals[1] + arrTotals[0])/12).toFixed(2);

    stReturn += '\n{name: \'Average\',type: \'spline\', color: \'#0000CC\',data: [\n';
    for(var z=0; z < arrAverages.length; z++){
        stReturn += '{y:' + arrAverages[z] + ',url:\'#\'},';
    }
    var strLen = stReturn.length;
    stReturn = stReturn.slice(0,strLen-1);

    stReturn += ']\n';
    stReturn += '},';

    if(departmentid == 0){
        stReturn += '\n{name: \'Budget\',type: \'scatter\', color: \'#FF4DFF\',data: [\n';
        for(var j=0; j < 7; j++){

            var index = 6 - j;
            var date = new moment();
            var year = date.subtract('months',index).format('YYYY');
            var month = date.format('M');

            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_clgx_sales_budget',null,'SUM'));
            var arrFilters = new Array();
            if(marketid != '0'){
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_clgx_sales_market",null,"contains", clgx_return_market_name(marketid)));
            }
            //arrFilters.push(new nlobjSearchFilter("custrecord_clgx_clgx_sales_region",null,"is", clgx_return_region_name(departmentid)));
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_clgx_sales_month",null,"equalto", month));
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_clgx_sales_year",null,"equalto",year));
            var searchBudgets = nlapiSearchRecord('customrecord_clgx_cologix_sales', null, arrFilters, arrColumns);

            var searchBudget = searchBudgets[0];
            var columns = searchBudget.getAllColumns();
            var monthBudget = searchBudget.getValue(columns[0]);

            if(monthBudget == null || monthBudget == ''){
                monthBudget = 0;
            }
            stReturn += '{y:' + monthBudget + ',url:\'#\'},\n';
        }
        var strLen = stReturn.length;
        stReturn = stReturn.slice(0,strLen-1);

        stReturn += ']\n';
        stReturn += '}';
    }



    stReturn += ']';

    return stReturn;
}

function returnMonthsCategories (){
    var mom = new moment();
    var categories = '[';
    categories += '\'' + mom.add('months', -6).format('M.YYYY') + '\',';
    categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\',';
    categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\',';
    categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\',';
    categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\',';
    categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\',';
    categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\'';
    categories += ']';
    return categories;
}

function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}
