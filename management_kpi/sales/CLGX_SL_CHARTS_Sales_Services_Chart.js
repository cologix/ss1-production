nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Sales_Services_Chart.js
//	Script Name:	CLGX_SL_CHARTS_Sales_Services_Chart
//	Script Id:		customscript_clgx_sl_chrt_sale_serv_chrt
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		07/28/2013
//	Includes:		CLGX_LIB_Global.js
//-------------------------------------------------------------------------------------------------

function suitelet_charts_sales_services_chart (request, response){
	try {
		var marketid = request.getParameter('marketid');
		
		if(marketid != '' ){
			var objFile = nlapiLoadFile(563059);
			var html = objFile.getValue();
			html = html.replace(new RegExp('{categories}','g'),returnMonthsCategories());
			html = html.replace(new RegExp('{series}','g'),returnMarketSeries(marketid));
			html = html.replace(new RegExp('{title}','g'),returnMarket(marketid));
		}
		else{
			var html = 'Please select a report from the left panel.';
		}
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function returnMarketSeries(marketid){
	var stReturn = '[';  
	
	var arrFilters = new Array();
	//if(marketid != '0'){
	//	arrFilters.push(new nlobjSearchFilter("location",null,"anyof",parseInt(marketid)));
	//}
	arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",71418));
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	var searchEmployees = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);
	
	var arrEmployees = new Array();
	for ( var i = 0; searchEmployees != null && i < searchEmployees.length; i++ ) {
		var searchEmployee = searchEmployees[i];
		var repid = searchEmployee.getValue('internalid', null, null);
		arrEmployees.push(repid);
	}
	
	var arrFilters = new Array();
	var arrColumns = new Array();
	//arrFilters.push(new nlobjSearchFilter("salesrep",null,"anyof",arrEmployees));
	if(marketid != '0'){
		arrFilters.push(new nlobjSearchFilter("location",null,"anyof",returnChildLocations (parseInt(marketid))));
	}
	var searchMarkets = nlapiSearchRecord('transaction', 'customsearch_clgx_chrt_sales_services', arrFilters, arrColumns);
	
	for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
		var searchMarket = searchMarkets[i];
		var columns = searchMarket.getAllColumns();
		
		stReturn += '\n{name: \'' + searchMarket.getValue(columns[0]) + '\',type: \'column\',data: [\n\n';
		stReturn += '{y:' + searchMarket.getValue(columns[7]) + ',url:\'/app/site/hosting/scriptlet.nl?script=214&deploy=1&market=' + returnMarket(marketid) + '&service=' + searchMarket.getValue(columns[0]) + '&month=-6\'},\n';
		stReturn += '{y:' + searchMarket.getValue(columns[6]) + ',url:\'/app/site/hosting/scriptlet.nl?script=214&deploy=1&market=' + returnMarket(marketid) + '&service=' + searchMarket.getValue(columns[0]) + '&month=-5\'},\n';
		stReturn += '{y:' + searchMarket.getValue(columns[5]) + ',url:\'/app/site/hosting/scriptlet.nl?script=214&deploy=1&market=' + returnMarket(marketid) + '&service=' + searchMarket.getValue(columns[0]) + '&month=-4\'},\n';
		stReturn += '{y:' + searchMarket.getValue(columns[4]) + ',url:\'/app/site/hosting/scriptlet.nl?script=214&deploy=1&market=' + returnMarket(marketid) + '&service=' + searchMarket.getValue(columns[0]) + '&month=-3\'},\n';
		stReturn += '{y:' + searchMarket.getValue(columns[3]) + ',url:\'/app/site/hosting/scriptlet.nl?script=214&deploy=1&market=' + returnMarket(marketid) + '&service=' + searchMarket.getValue(columns[0]) + '&month=-2\'},\n';
		stReturn += '{y:' + searchMarket.getValue(columns[2]) + ',url:\'/app/site/hosting/scriptlet.nl?script=214&deploy=1&market=' + returnMarket(marketid) + '&service=' + searchMarket.getValue(columns[0]) + '&month=-1\'},\n';
		stReturn += '{y:' + searchMarket.getValue(columns[1]) + ',url:\'/app/site/hosting/scriptlet.nl?script=214&deploy=1&market=' + returnMarket(marketid) + '&service=' + searchMarket.getValue(columns[0]) + '&month=0\'}\n';
		stReturn += ']\n';
		stReturn += '},';
		
	}
	var strLen = stReturn.length;
	stReturn = stReturn.slice(0,strLen-1);
	stReturn += ']';
	
	return stReturn;
}

function returnMonthsCategories (){
	var date = new Date();
	var categories = '[';
	categories += '\'' + getMonthDate(nlapiAddMonths(date, -6)) + '\', ';
	categories += '\'' + getMonthDate(nlapiAddMonths(date, -5)) + '\', ';
	categories += '\'' + getMonthDate(nlapiAddMonths(date, -4)) + '\', ';
	categories += '\'' + getMonthDate(nlapiAddMonths(date, -3)) + '\', ';
	categories += '\'' + getMonthDate(nlapiAddMonths(date, -2)) + '\', ';
	categories += '\'' + getMonthDate(nlapiAddMonths(date, -1)) + '\', ';
	categories += '\'' + getMonthDate(date) + '\'';
	categories += ']';
    return categories;
}

function getMonthDate(date){
    var month = parseInt(date.getMonth()) + 1;
    var year = date.getFullYear();
    if(parseInt(month) < 10){
        month = '0' + month;
    }
    var formattedDate = month + '.' + year;
    return formattedDate;
}

function returnMarket(marketid){
	switch(parseInt(marketid)) {
	case 29:
		return 'Jacksonville';
		break;
	case 20:
		return 'Dallas';
		break;
	case 22:
		return 'Minneapolis';
		break;
	case 25:
		return 'Montreal';
		break;
	case 23:
		return 'Toronto';
		break;
	case 1:
		return 'Cologix HQ';
		break;
	default:
		return 'All';
	}
}


function returnChildLocations (marketid) {
	var arrLocations = new Array();
	switch(parseInt(marketid)) {
	case 29:
		arrLocations = [29,31];
		break;
	case 20: // Dallas
		arrLocations = [2,17,20];
		break;
	case 22: //Minneapolis
		arrLocations = [16,22];
		break;
	case 25: // Montreal
		arrLocations = [5,8,9,10,11,12,25,27];
		break;
	case 23: // Toronto
		arrLocations = [6,7,13,14,15,18,23,26,28];
		break;
	case 0: // All
		arrLocations = [2,17,20,16,22,5,8,9,10,11,12,25,27,6,7,13,14,15,18,23,26,28];
		break;
	default: // All Markets
		arrLocations = [2,17,20,16,22,5,8,9,10,11,12,25,27,6,7,13,14,15,18,23,26,28];
	}
	return arrLocations;
}