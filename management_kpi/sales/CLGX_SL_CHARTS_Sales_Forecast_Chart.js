nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Sales_Forecast_Chart.js
//	Script Name:	CLGX_SL_CHARTS_Sales_Forecast_Chart
//	Script Id:		customscript_clgx_sl_chrt_sale_fore_chrt
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		07/29/2013
//	Includes:		CLGX_LIB_Global.js
//	URL:			/app/site/hosting/scriptlet.nl?script=215&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_charts_forecast_chart (request, response){
    try {
    	
        var marketid = request.getParameter('marketid');
        var departmentid = request.getParameter('departmentid');

        if(marketid != '' ){
        	if(departmentid == 0){
        		var objFile = nlapiLoadFile(563159); // for markets load file with the budget
        	}
        	else{
        		var objFile = nlapiLoadFile(2722263); // for departments load file without the budget
        	}
            
            var html = objFile.getValue();
            html = html.replace(new RegExp('{categories}','g'),returnMonthsCategories());
            html = html.replace(new RegExp('{series}','g'),returnMarketSeries(departmentid,marketid));
            if(departmentid > 0){
                if(departmentid == 9 || departmentid == 23 || departmentid == 24 || departmentid == 25 || departmentid == 26){
                	var strTitle = clgx_return_region_name(departmentid);
                }
                else{
                	var strTitle = nlapiLookupField('department', departmentid, 'namenohierarchy');
                }
                html = html.replace(new RegExp('{title}','g'), 'Departments Funnel Quarterly - ' + strTitle);
            }
            else{
            	html = html.replace(new RegExp('{title}','g'), 'Sales Funnel Quarterly - ' + clgx_return_market_name(marketid));
            }
            //html = html.replace(new RegExp('{categories}','g'),returnMonthsCategories());
            //html = html.replace(new RegExp('{series}','g'),returnMarketSeries(marketid));
            //html = html.replace(new RegExp('{title}','g'),clgx_return_market_name(marketid));
        }
        else{
            var html = 'Please select a report from the left panel.';
        }
        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function returnMarketSeries(departmentid,marketid){

    var arrFilters = new Array();
    //arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    var searchEmployees = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);
    var arrEmployees = new Array();
    for ( var i = 0; searchEmployees != null && i < searchEmployees.length; i++ ) {
        arrEmployees.push(searchEmployees[i].getValue('internalid', null, null));
    }
    
    var stReturn = '[';

    var arrFilters = new Array();
    //arrFilters.push(new nlobjSearchFilter("salesrep",null,"anyof",arrEmployees));
    if(marketid > 0){
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",clgx_return_child_locations_of_marketid (parseInt(marketid))));
    }
    if(departmentid > 0){
        if(departmentid == 9 || departmentid == 23 || departmentid == 24 || departmentid == 25 || departmentid == 26){
        	arrFilters.push(new nlobjSearchFilter("department","salesRep","anyof", clgx_get_region_departments(departmentid)));
        }
        else{
        	arrFilters.push(new nlobjSearchFilter("department","salesRep","anyof", departmentid));
        }
    }
    var arrColumns = new Array();
    var searchSales = nlapiSearchRecord('transaction', 'customsearch_clgx_chrt_sale_fun_sales2', arrFilters, arrColumns);
    var searchSales1 = nlapiSearchRecord('transaction', 'customsearch_clgx_chrt_sale_fun_sales2_2', arrFilters, arrColumns);
    var searchSale = searchSales[0];
    var columns = searchSale.getAllColumns();
    var month0Sales = searchSale.getValue(columns[0]);
    var month1Sales = searchSale.getValue(columns[1]);
    var month2Sales = searchSale.getValue(columns[2]);
    if(searchSales1!=null)
    {
    var searchSale1 = searchSales1[0];
     var columns1 = searchSale1.getAllColumns();
    var month0Sales1 = searchSale1.getValue(columns1[0]);
    var month1Sales1 = searchSale1.getValue(columns1[1]);
    var month2Sales1 = searchSale1.getValue(columns1[2]);
        if(month0Sales1 == null || month0Sales1 == ''){
            month0Sales1 = 0;
        }
        if(month1Sales1 == null || month1Sales1 == ''){
            month1Sales1 = 0;
        }
        if(month2Sales1 == null || month2Sales1 == ''){
            month2Sales1 = 0;
        }
    month0Sales = parseFloat(month0Sales)+parseFloat(month0Sales1);
    month1Sales = parseFloat(month1Sales)+parseFloat(month1Sales1);
    month2Sales = parseFloat(month2Sales)+parseFloat(month2Sales1);
    }

    var arrFilters = new Array();
    if(departmentid > 0){
        if(departmentid == 9 || departmentid == 23 || departmentid == 24 || departmentid == 25 || departmentid == 26){
        	var strMarket = clgx_return_region_name(departmentid);
        	arrFilters.push(new nlobjSearchFilter("department","salesRep","anyof", clgx_get_region_departments(departmentid)));
        }
        else{
        	var strMarket = nlapiLookupField('department', departmentid, 'namenohierarchy');
        	arrFilters.push(new nlobjSearchFilter("department","salesRep","anyof", departmentid));
        }
    }
    if(marketid > 0){
    	var strMarket = clgx_return_market_name(marketid).trim();
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",clgx_return_child_locations_of_marketid (parseInt(marketid))));
    }
    if(departmentid == 0 && marketid == 0){
    	var strMarket = 'All';
    }

    var arrColumns = new Array();
    var searchForecasts = nlapiSearchRecord('opportunity', 'customsearch_clgx_chrt_sale_fun_forecast', arrFilters, arrColumns);
    var searchForecasts1 = nlapiSearchRecord('opportunity', 'customsearch_clgx_chrt_sale_fun_foreca_2', arrFilters, arrColumns);
    
  if(searchForecasts1!=null)
  {
      var searchForecast1 = searchForecasts1[0];
      var columns1 = searchForecast1.getAllColumns();
  }
    var searchForecast = searchForecasts[0];
    var columns = searchForecast.getAllColumns();


    var month0Commit = searchForecast.getValue(columns[3]);
    if(month0Commit == null || month0Commit == ''){
        month0Commit = 0;
    }
    if(searchForecasts1!=null)
    {
    var month0Commit1 = searchForecast1.getValue(columns1[3]);
    if(month0Commit1 == null || month0Commit1 == ''){
        month0Commit1 = 0;
    }
    month0Commit =parseFloat( month0Commit)+parseFloat(month0Commit1);
    }

    var month0Upside = searchForecast.getValue(columns[4]);
    if(month0Upside == null || month0Upside == ''){
        month0Upside = 0;
    }
    if(searchForecasts1!=null)
    {
    var month0Upside1 = searchForecast1.getValue(columns1[4]);
    if(month0Upside1 == null || month0Upside1 == ''){
        month0Upside1 = 0;
    }
    month0Upside =parseFloat(month0Upside)+parseFloat(month0Upside1);
    }
    var month0Pipeline = searchForecast.getValue(columns[5]);
    if(month0Pipeline == null || month0Pipeline == ''){
        month0Pipeline = 0;
    }
    if(searchForecasts1!=null)
    {
    var month0Pipeline1 = searchForecast1.getValue(columns1[5]);
    if(month0Pipeline1 == null || month0Pipeline1 == ''){
        month0Pipeline1 = 0;
    }
    month0Pipeline =parseFloat(month0Pipeline)+parseFloat(month0Pipeline1);
    }
    var month1Commit = searchForecast.getValue(columns[6]);
    if(month1Commit == null || month1Commit == ''){
        month1Commit = 0;
    }
    if(searchForecasts1!=null)
    {
    var month1Commit1 = searchForecast1.getValue(columns1[6]);
    if(month1Commit1 == null || month1Commit1 == ''){
        month1Commit1 = 0;
    }
    month1Commit =parseFloat(month1Commit)+parseFloat(month1Commit1);
    }
    var month1Upside = searchForecast.getValue(columns[7]);
    if(month1Upside == null || month1Upside == ''){
        month1Upside = 0;
    }
    if(searchForecasts1!=null)
    {
    var month1Upside1 = searchForecast1.getValue(columns1[7]);
    if(month1Upside1 == null || month1Upside1 == ''){
        month1Upside1 = 0;
    }
    month1Upside =parseFloat(month1Upside)+parseFloat(month1Upside1);
    }
    var month1Pipeline = searchForecast.getValue(columns[8]);
    if(month1Pipeline == null || month1Pipeline == ''){
        month1Pipeline = 0;
    }
    if(searchForecasts1!=null)
    {
    var month1Pipeline1 = searchForecast1.getValue(columns1[8]);
    if(month1Pipeline1 == null || month1Pipeline1 == ''){
        month1Pipeline1 = 0;
    }
    month1Pipeline =parseFloat(month1Pipeline)+parseFloat(month1Pipeline1);
    }

    var month2Commit = searchForecast.getValue(columns[9]);
    if(month2Commit == null || month2Commit == ''){
        month2Commit = 0;
    }
    if(searchForecasts1!=null)
    {
    var month2Commit1 = searchForecast1.getValue(columns1[9]);
    if(month2Commit1 == null || month2Commit1 == ''){
        month2Commit1 = 0;
    }
    month2Commit =parseFloat(month2Commit)+parseFloat(month2Commit1);
    }
    var month2Upside = searchForecast.getValue(columns[10]);
    if(month2Upside == null || month2Upside == ''){
        month2Upside = 0;
    }
    if(searchForecasts1!=null)
    {
    var month2Upside1 = searchForecast1.getValue(columns1[10]);
    if(month2Upside1 == null || month2Upside1 == ''){
        month2Upside1 = 0;
    }
    month2Upside =parseFloat(month2Upside)+parseFloat(month2Upside1);
    }
    var month2Pipeline = searchForecast.getValue(columns[11]);
    if(month2Pipeline == null || month2Pipeline == ''){
        month2Pipeline = 0;
    }
    if(searchForecasts1!=null)
    {
    var month2Pipeline1 = searchForecast1.getValue(columns1[11]);
    if(month2Pipeline1 == null || month2Pipeline1 == ''){
        month2Pipeline1 = 0;
    }
    month2Pipeline =parseFloat(month2Pipeline)+parseFloat(month2Pipeline1);
    }

//    stReturn += '\n{name: \'Pipeline\',type: \'column\',\'visible\': false, data: \n[\n';
//    stReturn += '{y:' + Math.round(parseFloat(month0Pipeline)) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=1&type=pipeline&repid=0&market=' + strMarket + '&departmentid=' + departmentid + '&month=0\'},\n';
//    stReturn += '{y:' + Math.round(parseFloat(month1Pipeline)) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=1&type=pipeline&repid=0&market=' + strMarket + '&departmentid=' + departmentid + '&month=1\'},\n';
//    stReturn += '{y:' + Math.round(parseFloat(month2Pipeline)) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=1&type=pipeline&repid=0&market=' + strMarket + '&departmentid=' + departmentid + '&month=2\'},\n';
//    stReturn += ']\n';
//    stReturn += '},\n';

    stReturn += '\n{name: \'Upside\',type: \'column\',data: \n[\n';
    stReturn += '{y:' + Math.round(parseFloat(month0Upside)) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=1&type=upside&repid=0&market=' + strMarket + '&departmentid=' + departmentid + '&month=0\'},\n';
    stReturn += '{y:' + Math.round(parseFloat(month1Upside)) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=1&type=upside&repid=0&market=' + strMarket + '&departmentid=' + departmentid + '&month=1\'},\n';
    stReturn += '{y:' + Math.round(parseFloat(month2Upside)) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=1&type=upside&repid=0&market=' + strMarket + '&departmentid=' + departmentid + '&month=2\'},\n';
    stReturn += ']\n';
    stReturn += '},\n';

    stReturn += '\n{name: \'Forecast\',type: \'column\',data: \n[\n';
    stReturn += '{y:' + Math.round(parseFloat(month0Commit)) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=1&type=commit&repid=0&market=' + strMarket + '&departmentid=' + departmentid + '&month=0\'},\n';
    stReturn += '{y:' + Math.round(parseFloat(month1Commit)) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=1&type=commit&repid=0&market=' + strMarket + '&departmentid=' + departmentid + '&month=1\'},\n';
    stReturn += '{y:' + Math.round(parseFloat(month2Commit)) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=1&type=commit&repid=0&market=' + strMarket + '&departmentid=' + departmentid + '&month=2\'},\n';
    stReturn += ']\n';
    stReturn += '},\n';

    stReturn += '\n{name: \'Sales\',type: \'column\',data: \n[\n';
    stReturn += '{y:' + Math.round(parseFloat(month0Sales)) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&type=sales&repid=0&market=' + strMarket + '&departmentid=' + departmentid + '&month=0\'},\n';
    stReturn += '{y:' + Math.round(parseFloat(month1Sales)) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&type=sales&repid=0&market=' + strMarket + '&departmentid=' + departmentid + '&month=1\'},\n';
    stReturn += '{y:' + Math.round(parseFloat(month2Sales)) + ',url:\'/app/site/hosting/scriptlet.nl?script=212&deploy=1&forecast=0&type=sales&repid=0&market=' + strMarket + '&departmentid=' + departmentid + '&month=2\'},\n';
    stReturn += ']\n';
    stReturn += '},\n';
/*
    if(departmentid == 0){
        stReturn += '\n{name: \'Budget\',type: \'scatter\', color: \'#FF4DFF\',data: [\n';

        for(var j=0; j < 3; j++){

            var mom = new moment();

            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_clgx_sales_budget',null,'SUM'));
            var arrFilters = new Array();
            if(marketid != '0'){
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_clgx_sales_market",null,"is", clgx_return_market_name(marketid)));
            }
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_clgx_sales_month",null,"equalto", mom.add('months', j).format('M')));
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_clgx_sales_year",null,"equalto", mom.add('months', j).format('YYYY')));
            var searchBudgets = nlapiSearchRecord('customrecord_clgx_cologix_sales', null, arrFilters, arrColumns);

            var searchBudget = searchBudgets[0];
            var columns = searchBudget.getAllColumns();
            var monthBudget = searchBudget.getValue(columns[0]);

            if(monthBudget == null || monthBudget == ''){
                monthBudget = 0;
            }

            stReturn += '{y:' + Math.round(parseFloat(monthBudget)) + ',url:\'#\'},\n';
        }
        var strLen = stReturn.length;
        stReturn = stReturn.slice(0,strLen-1);

        stReturn += ']\n';
        stReturn += '}';
    }
*/
    stReturn += ']\n';

    return stReturn;
}

function returnMonthsCategories (){
    var mom = new moment();
    var categories = '[';
    categories += '\'' + mom.add('months', 0).format('M.YYYY') + '\',';
    categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\',';
    categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\'';
    categories += ']';
    return categories;
}
