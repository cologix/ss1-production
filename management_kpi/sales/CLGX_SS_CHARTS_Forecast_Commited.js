nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CHARTS_Forecast_Commited.js
//	Script Name:	CLGX_SS_CHARTS_Forecast_Commited
//	Script Id:		customscript_clgx_ss_chrt_forecast_commited
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		08/19/2013
//	Includes:		CLGX_LIB_Global.js
//-------------------------------------------------------------------------------------------------

function scheduled_chrt_forecast_commited (){
	try {

		var date = new moment();
	    var year = date.format('YYYY');
	    var month = date.format('M');
	    
	    var startDate = moment().startOf('month').format('M-D-YYYY');
    	var endDate = moment().endOf('month').format('M-D-YYYY');
	    
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_clgx_sales_market',null,null));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_clgx_sales_month",null,"equalto",month));
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_clgx_sales_year",null,"equalto",year));
		var searchSales = nlapiSearchRecord('customrecord_clgx_cologix_sales', null, arrFilters, arrColumns);

		for ( var i = 0; searchSales != null && i < searchSales.length; i++ ) {
			var searchSale = searchSales[i];
			var internalid = searchSale.getValue('internalid', null, null);
			var market = searchSale.getValue('custrecord_clgx_clgx_sales_market', null, null);
			
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_oppty_location",'custrecord_clgx_totals_opportunity',"anyof",clgx_return_child_locations_of_marketname (market)));
			//arrFilters.push(new nlobjSearchFilter('expectedclosedate',null,'within',startDate,endDate));
			var arrColumns = new Array();
			var searchForecasts = nlapiSearchRecord('opportunity', 'customsearch_clgx_chrt_sale_fun_forecast', arrFilters, arrColumns);
			var searchForecasts1 = nlapiSearchRecord('opportunity', 'customsearch_clgx_chrt_sale_fun_foreca_2', arrFilters, arrColumns);
			
			var totalCommit = 0;
			var monthCommit = 0;
			var monthCommit1 = 0;
			if(searchForecasts1!=null) {
				var searchForecast1 = searchForecasts1[0];
				var columns = searchForecast1.getAllColumns();
				monthCommit1 = searchForecast1.getValue(columns[3]);
			}
			  
			if (searchForecasts != null){
				var searchForecast = searchForecasts[0];
				var columns = searchForecast.getAllColumns();
				monthCommit = searchForecast.getValue(columns[3]);
			}
			totalCommit = parseFloat(monthCommit) + parseFloat(monthCommit1);
			
		    nlapiSubmitField('customrecord_clgx_cologix_sales',internalid,'custrecord_clgx_clgx_sales_commited',totalCommit);
		}
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


