nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Sales_Gross_Details.js
//	Script Name:	CLGX_SL_CHARTS_Sales_Gross_Details
//	Script Id:		customscript_clgx_sl_chrt_sale_gross_details
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		07/19/2013
//	Includes:		CLGX_LIB_Global.js
//	URL:			/app/site/hosting/scriptlet.nl?script=212&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_charts_sale_details (request, response){
    try {
        var market = request.getParameter('market');
        var departmentid = request.getParameter('departmentid');
        var repid = request.getParameter('repid');
        var month = request.getParameter('month');
        var forecast = request.getParameter('forecast');
        var type = request.getParameter('type');
nlapiLogExecution('DEBUG','type', type);
      nlapiLogExecution('DEBUG','market_0', market);
        var arrDates  = getDateRange(month);
        var startDate = arrDates[0];
        var endDate   = arrDates[1];

        if(market != '0' && market != 'null'){
            var title = market + ' - Between ' + startDate + ' and ' + endDate;
        }
        if(departmentid > 0){
            var title = clgx_return_region_name(departmentid) + ' - Between ' + startDate + ' and ' + endDate;
        }
        if(repid != '0'){
            var repname = nlapiLookupField('employee', parseInt(repid), 'entityid');
            var title = repname + ' - Between ' + startDate + ' and ' + endDate;
        }

        if(forecast != '0'){
            var title = ' Forecast ' + type + ' - ' + market + ' - Between ' + startDate + ' and ' + endDate;
        }

        var objFile = nlapiLoadFile(4320390);
        var html = objFile.getValue();
        html = html.replace(new RegExp('{salesJSON}','g'), dataSales(forecast, type, market, repid, month, departmentid));
        html = html.replace(new RegExp('{title}','g'), title);

        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function dataSales (forecast, type, market, repid, month, departmentid) {

    market = market.trim();
    var arrDates  = getDateRange(month);
    var startDate = arrDates[0];
    var endDate   = arrDates[1];

    var arrFilters = new Array();
    var Filters = new Array();
    if(market != '0' && market != 'All' && market != 'null' && market != '' && market != 'undefined' && departmentid == 0){
        if(forecast == '0'){
            nlapiLogExecution('ERROR','market', market);
            var arrMarkets=clgx_return_child_locations_of_marketname (market);
            arrFilters.push(new nlobjSearchFilter("location",null,"anyof",arrMarkets));
            Filters.push(new nlobjSearchFilter("location","order","anyof",arrMarkets));

            nlapiLogExecution('ERROR','market', JSON.stringify(clgx_return_child_locations_of_marketname (market)));
          /*  var filters=[];
            filters =[ [['location', 'anyof', clgx_return_child_locations_of_marketname (market)],
                'or',
                ['location', 'anyof', '@NONE@'] ],
                'and', ['order.saleseffectivedate','within',startDate,endDate]];*/
        }

    }



    if(repid > 0){
        arrFilters.push(new nlobjSearchFilter('salesrep',null,'anyof',parseInt(repid)));
        Filters.push(new nlobjSearchFilter('salesrep',null,'anyof',parseInt(repid)));
       
        /*var filters=[];
        filters =[ [['location', 'anyof', clgx_return_child_locations_of_marketname (market)],
            'or',
            ['location', 'anyof', '@NONE@'] ],
            'and', ['order.saleseffectivedate','within',startDate,endDate],
            'and', ['salesrep','anyof',parseInt(repid)]
            ];*/
    }

    if(forecast == '0'){
        arrFilters.push(new nlobjSearchFilter('saleseffectivedate','order','within',startDate,endDate));
        Filters.push(new nlobjSearchFilter('saleseffectivedate','order','within',startDate,endDate));

    }
    else{
        arrFilters.push(new nlobjSearchFilter('expectedclosedate',null,'within',startDate,endDate));
    }




    var arrColumns = new Array();

    if(forecast == '0'){
        var searchSales = nlapiSearchRecord('opportunity', 'customsearch_clgx_kpi_opp_sales_all_4', arrFilters, arrColumns);
        var searchSales2 = nlapiSearchRecord('opportunity', 'customsearch_clgx_kpi_opp_sales_all_4_2', Filters, arrColumns);

    }
    else{
        var searchSales = nlapiSearchRecord('opportunity', 'customsearch_clgx_chrt_sale_fun_fore_det', arrFilters, arrColumns);
        var searchSales1 = nlapiSearchRecord('opportunity', 'customsearch_clgx_chrt_sale_fun_fore_d_2', arrFilters, arrColumns);
    }



    var stReturn = 'var dataSales =  [';

    for ( var i = 0; searchSales != null && i < searchSales.length; i++ ) {
        var searchSale = searchSales[i];

        if(forecast == '0'){
            var date = searchSale.getValue('saleseffectivedate', 'order', 'group');
            var customer = searchSale.getValue('entityid', 'customer', 'group');
            //var total = searchSale.getValue('custbody_clgx_total_recurring_month', null, null);
            var total = searchSale.getValue('custrecord_clgx_totals_oppty_total', 'custrecord_clgx_totals_opportunity', 'group');
            if(total == null || total == ''){
                total = 0;
            }
            var type = 'op';
        }
        else{
            var date = searchSale.getValue('expectedclosedate', null, 'group');
            var customer = searchSale.getText('entity', null, 'group');
            //var total = searchSale.getValue('custbody_clgx_total_recurring', null, null);
            var total = searchSale.getValue('custrecord_clgx_totals_oppty_total', 'custrecord_clgx_totals_opportunity', 'group');
            if(total == null || total == ''){
                total = 0;
            }
            var type = 'op';
        }
        stReturn += '\n{"number":"' + searchSale.getValue('number', null, 'group') +
            '","internalid":' +  searchSale.getValue('internalid', null, 'group') +
            ',"date":"' + date +
            '","customer":"' +  customer +
            //'","customerid":' +  searchSale.getValue('entityid', 'customer', null) +
            '","salerep":"' +  searchSale.getText('salesrep', null, 'group') +
            '","salerepid":' +  searchSale.getValue('salesrep', null, 'group') +
            ',"total":' + total +
            ',"type":"' + type +
            '"},';
    }
    if(forecast == '0'){

        for ( var i = 0; searchSales2 != null && i < searchSales2.length; i++ ) {
            var searchSale = searchSales2[i];
                var date = searchSale.getValue('saleseffectivedate', 'order', 'group');
                var customer = searchSale.getValue('entityid', 'customer', 'group');
                //var total = searchSale.getValue('custbody_clgx_total_recurring', null, null);
                var total = searchSale.getValue('custbody_clgx_so_incremental_mrc', 'order', 'group');
                var type = 'so';
                stReturn += '\n{"number":"' + searchSale.getValue('number', 'order', 'group') +
                    '","internalid":' + searchSale.getValue('internalid', 'order', 'group') +
                    ',"date":"' + date +
                    '","customer":"' + customer +
                    //'","customerid":' +  searchSale.getValue('entityid', 'customer', null) +
                    '","salerep":"' + searchSale.getText('salesrep', null, 'group') +
                    '","salerepid":' + searchSale.getValue('salesrep', null, 'group') +
                    ',"total":' + total +
                    ',"type":"' + type +
                    '"},';

        }
    }


    var strLen = stReturn.length;
    if (searchSales != null){
        stReturn = stReturn.slice(0,strLen-1);
    }
    stReturn += '];';
    return stReturn;
}


function getDateRange(monthsago){

    var today = new Date();
    var date = nlapiAddMonths(today, monthsago);

    var month = parseInt(date.getMonth());
    var year = date.getFullYear();

    var stDays  = daysInMonth(parseInt(month),parseInt(year));
    var stYear  = year;

    var stMonth = parseInt(month) + 1;

    var stStartDate = stMonth + '/1/' + stYear;
    var stEndDate   = stMonth + '/' + stDays + '/' + stYear;

    var arrDateRange = new Array();
    arrDateRange[0] = stStartDate;
    arrDateRange[1] =stEndDate;

    return arrDateRange;
}


function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}