nlapiLogExecution("audit","FLOStart",new Date().getTime());
    //-------------------------------------------------------------------------------------------------
    //	Script File:	CLGX_SL_CHARTS_Sales_Unadjusted_Chart.js
    //	Script Name:	CLGX_SL_CHARTS_Sales_Unadjusted_Chart
    //	Script Id:		customscript_clgx_sl_chrt_sale_unadj_chrt
    //	Script Runs:	On Server
    //	Script Type:	Suitelet
    //	@authors:		Dan Tansanu - dan.tansanu@cologix.com
    //	Created:		07/18/2013
    //	Includes:		CLGX_LIB_Global.js
    //	URL:			/app/site/hosting/scriptlet.nl?script=211&deploy=1
    //-------------------------------------------------------------------------------------------------

    function suitelet_charts_sales_unadjusted_chart (request, response){
        try {

            var departmentid = request.getParameter('departmentid');
            var marketid = request.getParameter('marketid');

            if(marketid != '' ){
                if(departmentid == 0){
                    var objFile = nlapiLoadFile(4317680); // for markets load file with the budget
                }
                else{
                    var objFile = nlapiLoadFile(4317681); // for departments load file without the budget
                }
                var html = objFile.getValue();

                html = html.replace(new RegExp('{categories}','g'),returnMonthsCategories());
                if((marketid == 0 && departmentid == 0) || (departmentid == 9 || departmentid == 23 || departmentid == 24 || departmentid == 25 || departmentid == 26)){
                    html = html.replace(new RegExp('{series}','g'),returnMarketSeries(departmentid,marketid));
                }
                else{
                    html = html.replace(new RegExp('{series}','g'),returnRepsSeries(departmentid,marketid));
                }

                if(parseInt(departmentid) > 0){
                    html = html.replace(new RegExp('{title}','g'), 'Departments Gross Sales - ' + clgx_return_region_name(departmentid));
                }
                else{
                    html = html.replace(new RegExp('{title}','g'), 'Gross Sales - ' + clgx_return_market_name(marketid));
                }

            }
            else{
                var html = 'Please select a report from the left panel.';
            }
            response.write( html );
        }
        catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
            if (error.getDetails != undefined){
                nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
                throw error;
            }
            else{
                nlapiLogExecution('ERROR','Unexpected Error', error.toString());
                throw nlapiCreateError('99999', error.toString());
            }
        } // End Catch Errors Section ------------------------------------------------------------------------------------------
    }

    function returnMarketSeries(departmentid,marketid){
        var stReturn = '[';

        var arrFilters = new Array();
        //arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        var searchEmployees = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);
        var arrEmployees = new Array();
        for ( var i = 0; searchEmployees != null && i < searchEmployees.length; i++ ) {
            arrEmployees.push(searchEmployees[i].getValue('internalid', null, null));
        }


        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("salesrep",null,"anyof",arrEmployees));


        var arrColumns = new Array();

        var searchMarkets = nlapiSearchRecord('opportunity', 'customsearch_clgx_kpi_opp_sales_all', arrFilters, arrColumns);
        var searchMarketsIncrMRC = nlapiSearchRecord('opportunity', 'customsearch_clgx_kpi_opp_sales_all_incr', arrFilters, arrColumns);


        var arrTotals = new Array();
        arrTotals = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

        //Duplicates
        var searchMarketsDuplicates = nlapiSearchRecord('opportunity', 'customsearch_clgx_kpi_opp_sales_all_2', arrFilters, arrColumns);
        var objDuplicates={};
        var objClones={};
        for ( var i = 0; searchMarketsDuplicates != null && i < searchMarketsDuplicates.length; i++ ) {
            var searchMarketD = searchMarketsDuplicates[i];
            var columns = searchMarketD.getAllColumns();
            var locationD=searchMarketD.getValue(columns[0]);
            var concatenateNOAmt=searchMarketD.getValue(columns[1]);
            var numberOfMth=searchMarketD.getValue(columns[2]);
            var arr=concatenateNOAmt.split(',');
            var cloneArr=new Array();
            var duplicates=new Array();
            for(var j = 0; j < arr.length; j++) {

                if (_.indexOf(cloneArr,arr[j])==-1) {
                    cloneArr.push(arr[j]);
                } else {
                    duplicates.push(arr[j]);
                }
            }
            var summClone=0;
            if(cloneArr.length>0)
            {
                for(var p = 0; p < cloneArr.length; p++) {
                    var arr1I=cloneArr[p].split('=');
                    summClone +=parseFloat(arr1I[1]);
                }
            }
            if(!objClones.hasOwnProperty(locationD)) {
                objClones[locationD] = [];
                objClones[locationD][numberOfMth] = summClone;
            } else {
                objClones[locationD][numberOfMth] = (typeof(objClones[locationD][numberOfMth]) !== "undefined") ? parseFloat(objClones[locationD][numberOfMth]+summClone) : summClone;
            }
            var summDuplicate=0;
            if(duplicates.length>0)
            {
                for(var p = 0; p < duplicates.length; p++) {
                    var arr1=duplicates[p].split('=');
                    summDuplicate +=parseFloat(arr1[1]);
                }
            }
            if(!objDuplicates.hasOwnProperty(locationD)) {
                objDuplicates[locationD] = [];
                objDuplicates[locationD][numberOfMth] = summDuplicate;
            } else {
                objDuplicates[locationD][numberOfMth] = summDuplicate;
            }
        }
        //nlapiLogExecution('DEBUG','Dallas_5', objDuplicates['Dallas']);
        //Duplicates Incremental--customsearch_clgx_kpi_opp_sales_all_2_2

        var searchMarketsDuplicatesI = nlapiSearchRecord('opportunity', 'customsearch_clgx_kpi_opp_sales_all_2_2', arrFilters, arrColumns);
        var objDuplicatesI={};
        var objClonesI={};
        for ( var i = 0; searchMarketsDuplicatesI != null && i < searchMarketsDuplicatesI.length; i++ ) {
            var searchMarketDI = searchMarketsDuplicatesI[i];
            var columns = searchMarketDI.getAllColumns();
            var locationDI=(searchMarketDI.getValue(columns[0])!='- None -') ? searchMarketDI.getValue(columns[0]):searchMarketDI.getValue(columns[3]);
            var concatenateNOAmtI=searchMarketDI.getValue(columns[1]);
            var numberOfMthI=searchMarketDI.getValue(columns[2]);
            var arrI=concatenateNOAmtI.split(',');
            var cloneArrI=new Array();
            var duplicatesI=new Array();
            for(var j = 0; j < arrI.length; j++) {

                if (_.indexOf(cloneArrI,arrI[j])==-1) {
                    cloneArrI.push(arrI[j]);
                } else {
                    duplicatesI.push(arrI[j]);
                }
            }
            var summCloneI=0;
            if(cloneArrI.length>0)
            {
                for(var p = 0; p < cloneArrI.length; p++) {
                    var arr1I=cloneArrI[p].split('=');
                    summCloneI +=parseFloat(arr1I[1]);
                }
            }
            if(!objClonesI.hasOwnProperty(locationDI)) {
                objClonesI[locationDI] = [];
                objClonesI[locationDI][numberOfMthI] = summCloneI;
            } else {
                objClonesI[locationDI][numberOfMthI] = (typeof(objClonesI[locationDI][numberOfMthI]) !== "undefined") ? parseFloat(objClonesI[locationDI][numberOfMthI]+summCloneI) : summCloneI;
            }

            var summDuplicateI=0;
            if(duplicatesI.length>0)
            {
                for(var p = 0; p < duplicatesI.length; p++) {
                    var arr1I=duplicatesI[p].split('=');
                    summDuplicateI +=parseFloat(arr1I[1]);
                }
            }
            if(!objDuplicatesI.hasOwnProperty(locationDI)) {
                objDuplicatesI[locationDI] = [];
                objDuplicatesI[locationDI][numberOfMthI] = summDuplicateI;
            } else {
                objDuplicatesI[locationDI][numberOfMthI] = (typeof(objDuplicatesI[locationDI][numberOfMthI]) !== "undefined") ? parseFloat(objDuplicatesI[locationDI][numberOfMthI]+summDuplicateI) : summDuplicateI;
            }
        }


        for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
            var searchMarket = searchMarkets[i];
            var columns = searchMarket.getAllColumns();
            var added=0;
            if(searchMarketsIncrMRC != null){
                for ( var j = 0; searchMarketsIncrMRC != null && j < searchMarketsIncrMRC.length; j++ ) {
                    var searchMarket1 = searchMarketsIncrMRC[j];
                    var columns1 = searchMarket1.getAllColumns();

                    if(departmentid > 0){
                        var text0 = searchMarket.getValue(columns[0]);
                        var text1 = searchMarket1.getValue(columns1[0]);
                    }
                    else{
                        var text0 = searchMarket.getValue(columns[0]);
                        var text1=searchMarket1.getValue(columns1[0]);
                    }

                    var dup0 = (typeof(objDuplicates[text0][0]) !== "undefined") ? parseFloat(objDuplicates[text0][0]) : 0;
                    var dupI0 = (typeof(objDuplicatesI[text0][0]) !== "undefined") ? parseFloat(objDuplicatesI[text0][0]) : 0;
                    var cloneI0 = (typeof(objClonesI[text0][0]) !== "undefined") ? parseFloat(objClonesI[text0][0]) : 0;
                    var dup1 = (typeof(objDuplicates[text0][1]) !== "undefined") ? parseFloat(objDuplicates[text0][1]) : 0;
                    var dupI1 = (typeof(objDuplicatesI[text0][1]) !== "undefined") ? parseFloat(objDuplicatesI[text0][1]) : 0;
                    var cloneI1 = (typeof(objClonesI[text0][1]) !== "undefined") ? parseFloat(objClonesI[text0][1]) : 0;
                    var dup2 = (typeof(objClones[text0][2]) !== "undefined") ? parseFloat(objClones[text0][2]) : 0;
                    var dupI2 = (typeof(objDuplicatesI[text0][2]) !== "undefined") ? parseFloat(objDuplicatesI[text0][2]) : 0;
                    var cloneI2 = (typeof(objClonesI[text0][2]) !== "undefined") ? parseFloat(objClonesI[text0][2]) : 0;
                    var dup3 = (typeof(objClones[text0][3]) !== "undefined") ? parseFloat(objClones[text0][3]) : 0;
                    var dupI3 = (typeof(objDuplicatesI[text0][3]) !== "undefined") ? parseFloat(objDuplicatesI[text0][3]) : 0;
                    var cloneI3 = (typeof(objClonesI[text0][3]) !== "undefined") ? parseFloat(objClonesI[text0][3]) : 0;
                    var dup4 = (typeof(objDuplicates[text0][4]) !== "undefined") ? parseFloat(objDuplicates[text0][4]) : 0;
                    var dupI4 = (typeof(objClonesI[text0][4]) !== "undefined") ? parseFloat(objClonesI[text0][4]) : 0;
                    var dup5 = (typeof(objDuplicates[text0][5]) !== "undefined") ? parseFloat(objDuplicates[text0][5]) : 0;
                    var dupI5 = (typeof(objClonesI[text0][5]) !== "undefined") ? parseFloat(objClonesI[text0][5]) : 0;
                    var dup6 = (typeof(objDuplicates[text0][6]) !== "undefined") ? parseFloat(objDuplicates[text0][6]) : 0;
                    var dupI6 = (typeof(objClonesI[text0][6]) !== "undefined") ? parseFloat(objClonesI[text0][6]) : 0;
                    var dup7 = (typeof(objDuplicates[text0][7]) !== "undefined") ? parseFloat(objDuplicates[text0][7]) : 0;
                    var dupI7 = (typeof(objClonesI[text0][7]) !== "undefined") ? parseFloat(objClonesI[text0][7]) : 0;
                    var dup8 = (typeof(objDuplicates[text0][8]) !== "undefined") ? parseFloat(objDuplicates[text0][8]) : 0;
                    var dupI8 = (typeof(objClonesI[text0][8]) !== "undefined") ? parseFloat(objClonesI[text0][8]) : 0;
                    var dup9 = (typeof(objDuplicates[text0][9]) !== "undefined") ? parseFloat(objDuplicates[text0][9]) : 0;
                    var dupI9 = (typeof(objClonesI[text0][9]) !== "undefined") ? parseFloat(objClonesI[text0][9]) : 0;
                    var dup10 = (typeof(objDuplicates[text0][10]) !== "undefined") ? parseFloat(objDuplicates[text0][10]) : 0;
                    var dupI10 = (typeof(objClonesI[text0][10]) !== "undefined") ? parseFloat(objClonesI[text0][10]) : 0;
                    var dup11 = (typeof(objDuplicates[text0][11]) !== "undefined") ? parseFloat(objDuplicates[text0][11]) : 0;
                    var dupI11 = (typeof(objClonesI[text0][11]) !== "undefined") ? parseFloat(objClonesI[text0][11]) : 0;
                    var dup12 = (typeof(objDuplicates[text0][12]) !== "undefined") ? parseFloat(objDuplicates[text0][12]) : 0;
                    var dupI12 = (typeof(objClonesI[text0][12]) !== "undefined") ? parseFloat(objClonesI[text0][12]) : 0;
                    var dup13 = (typeof(objDuplicates[text0][13]) !== "undefined") ? parseFloat(objDuplicates[text0][13]) : 0;
                    var dupI13 = (typeof(objClonesI[text0][13]) !== "undefined") ? parseFloat(objClonesI[text0][13]) : 0;
                    var dup14 = (typeof(objDuplicates[text0][14]) !== "undefined") ? parseFloat(objDuplicates[text0][14]) : 0;
                    var dupI14 = (typeof(objClonesI[text0][14]) !== "undefined") ? parseFloat(objClonesI[text0][14]) : 0;
                    var dup15 = (typeof(objDuplicates[text0][15]) !== "undefined") ? parseFloat(objDuplicates[text0][15]) : 0;
                    var dupI15 = (typeof(objClonesI[text0][15]) !== "undefined") ? parseFloat(objClonesI[text0][15]) : 0;
                    var dup16 = (typeof(objDuplicates[text0][16]) !== "undefined") ? parseFloat(objDuplicates[text0][16]) : 0;
                    var dupI16 = (typeof(objClonesI[text0][16]) !== "undefined") ? parseFloat(objClonesI[text0][16]) : 0;
                    var dup17 = (typeof(objDuplicates[text0][17]) !== "undefined") ? parseFloat(objDuplicates[text0][17]) : 0;
                    var dupI17 = (typeof(objClonesI[text0][17]) !== "undefined") ? parseFloat(objClonesI[text0][17]) : 0;
                    var dup18 = (typeof(objDuplicates[text0][18]) !== "undefined") ? parseFloat(objDuplicates[text0][18]) : 0;
                    var dupI18 = (typeof(objClonesI[text0][18]) !== "undefined") ? parseFloat(objClonesI[text0][18]) : 0;

                    if(text0==text1)
                    {
                        stReturn += '\n{name: \'' + text0 + '\',type: \'column\',data: [\n';
                        stReturn += '{y:' + ((parseFloat(searchMarket.getValue(columns[7])-parseFloat(dup6)))+(parseFloat(dupI6))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-6\'},\n';
                        stReturn += '{y:' + ((parseFloat(searchMarket.getValue(columns[6])-parseFloat(dup5))) +(parseFloat(dupI5))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-5\'},\n';
                        stReturn += '{y:' + ((parseFloat(searchMarket.getValue(columns[5])-parseFloat(dup4)))+(parseFloat(dupI4))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-4\'},\n';
                        stReturn += '{y:' + ((parseFloat(dup3))+(parseFloat(cloneI3))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-3\'},\n';
                        stReturn += '{y:' + ((parseFloat(dup2))+(parseFloat(cloneI2))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-2\'},\n';
                        stReturn += '{y:' + ((parseFloat(searchMarket.getValue(columns[2])-parseFloat(dup1)))+(parseFloat(cloneI1))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-1\'},\n';
                        stReturn += '{y:' + ((parseFloat(searchMarket.getValue(columns[1])-parseFloat(dup0)))+(parseFloat(cloneI0))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=0\'}\n';
                        stReturn += ']\n';
                        stReturn += '},';

                        arrTotals[0] += ((parseFloat(searchMarket.getValue(columns[2])-parseFloat(dup1)))+(parseFloat(cloneI1)));
                        arrTotals[1] += ((parseFloat(dup2))+(parseFloat(cloneI2)));
                        arrTotals[2] += ((parseFloat(dup3))+(parseFloat(cloneI3)));
                        arrTotals[3] += ((parseFloat(searchMarket.getValue(columns[5])-parseFloat(dup4)))+(parseFloat(dupI4)));
                        arrTotals[4] += ((parseFloat(searchMarket.getValue(columns[6])-parseFloat(dup5))) +(parseFloat(dupI5)));
                        arrTotals[5] += ((parseFloat(searchMarket.getValue(columns[7])-parseFloat(dup6)))+(parseFloat(dupI6)));
                        arrTotals[6] +=((parseFloat(searchMarket.getValue(columns[8])-parseFloat(dup7)))+(parseFloat(dupI7)));
                        arrTotals[7] +=((parseFloat(searchMarket.getValue(columns[9])-parseFloat(dup8)))+(parseFloat(dupI8)));
                        arrTotals[8] += ((parseFloat(searchMarket.getValue(columns[10])-parseFloat(dup9)))+(parseFloat(dupI9)));
                        arrTotals[9] += ((parseFloat(searchMarket.getValue(columns[11])-parseFloat(dup10)))+(parseFloat(dupI10)));
                        arrTotals[10] +=((parseFloat(searchMarket.getValue(columns[12])-parseFloat(dup11)))+(parseFloat(dupI11)));
                        arrTotals[11] += ((parseFloat(searchMarket.getValue(columns[13])-parseFloat(dup12)))+(parseFloat(dupI12)));
                        arrTotals[12] += ((parseFloat(searchMarket.getValue(columns[14])-parseFloat(dup13)))+(parseFloat(dupI13)));
                        arrTotals[13] +=((parseFloat(searchMarket.getValue(columns[15])-parseFloat(dup14)))+(parseFloat(dupI14)));
                        arrTotals[14] +=((parseFloat(searchMarket.getValue(columns[16])-parseFloat(dup15)))+(parseFloat(dupI15)));
                        arrTotals[15] +=((parseFloat(searchMarket.getValue(columns[17])-parseFloat(dup16)))+(parseFloat(dupI16)));
                        arrTotals[16] += ((parseFloat(searchMarket.getValue(columns[18])-parseFloat(dup17)))+(parseFloat(dupI17)));
                        arrTotals[17] += ((parseFloat(searchMarket.getValue(columns[19])-parseFloat(dup18)))+(parseFloat(dupI18)));
                        added=1;
                    }
                }
            }
            if(added==0)
            {
                stReturn += '\n{name: \'' + text0 + '\',type: \'column\',data: [\n';
                stReturn += '{y:' + (parseFloat(searchMarket.getValue(columns[7])-parseFloat(dup6))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-6\'},\n';
                stReturn += '{y:' + (parseFloat(searchMarket.getValue(columns[6])-parseFloat(dup5))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-5\'},\n';
                stReturn += '{y:' + (parseFloat(searchMarket.getValue(columns[5])-parseFloat(dup4))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-4\'},\n';
                stReturn += '{y:' + (parseFloat(searchMarket.getValue(columns[4])-parseFloat(dup3))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-3\'},\n';
                stReturn += '{y:' + (parseFloat(searchMarket.getValue(columns[3])-parseFloat(dup2))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-2\'},\n';
                stReturn += '{y:' + (parseFloat(searchMarket.getValue(columns[2])-parseFloat(dup1))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=-1\'},\n';
                stReturn += '{y:' + (parseFloat(searchMarket.getValue(columns[1])-parseFloat(dup0))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=0&market=' + text0 + '&departmentid=' + departmentid + '&month=0\'}\n';
                stReturn += ']\n';
                stReturn += '},';
                arrTotals[0] += (parseFloat(searchMarket.getValue(columns[2])-dup1));
                arrTotals[1] += (parseFloat(searchMarket.getValue(columns[3])-dup2));
                arrTotals[2] += (parseFloat(searchMarket.getValue(columns[4])-dup3));
                arrTotals[3] += (parseFloat(searchMarket.getValue(columns[5])-dup4));
                arrTotals[4] += (parseFloat(searchMarket.getValue(columns[6])-dup5));
                arrTotals[5] += (parseFloat(searchMarket.getValue(columns[7])-dup6));
                arrTotals[6] += (parseFloat(searchMarket.getValue(columns[8])-dup7));
                arrTotals[7] += (parseFloat(searchMarket.getValue(columns[9])-dup8));
                arrTotals[8] += (parseFloat(searchMarket.getValue(columns[10])-dup9));
                arrTotals[9] += (parseFloat(searchMarket.getValue(columns[11])-dup10));
                arrTotals[10] += (parseFloat(searchMarket.getValue(columns[12])-dup11));
                arrTotals[11] += (parseFloat(searchMarket.getValue(columns[13])-dup12));
                arrTotals[12] += (parseFloat(searchMarket.getValue(columns[14])-dup13));
                arrTotals[13] += (parseFloat(searchMarket.getValue(columns[15])-dup14));
                arrTotals[14] += (parseFloat(searchMarket.getValue(columns[16])-dup15));
                arrTotals[15] += (parseFloat(searchMarket.getValue(columns[17])-dup16));
                arrTotals[16] += (parseFloat(searchMarket.getValue(columns[18])-dup17));
                arrTotals[17] += (parseFloat(searchMarket.getValue(columns[19])-dup18));

            }


        }


        var arrAverages = new Array();
        arrAverages = [0,0,0,0,0,0,0];

        arrAverages[0] = parseFloat(139543.53).toFixed(2);
        arrAverages[1] = parseFloat((arrTotals[16] + arrTotals[15] + arrTotals[14] + arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5])/12).toFixed(2);
         arrAverages[2] = parseFloat((arrTotals[15] + arrTotals[14] + arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4])/12).toFixed(2);
         arrAverages[3] = parseFloat((arrTotals[14] + arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4] + arrTotals[3])/12).toFixed(2);
         arrAverages[4] = parseFloat((arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4] + arrTotals[3] + arrTotals[2])/12).toFixed(2);
         arrAverages[5] = parseFloat((arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4] + arrTotals[3] + arrTotals[2] + arrTotals[1])/12).toFixed(2);
         arrAverages[6] = parseFloat((arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4] + arrTotals[3] + arrTotals[2] + arrTotals[1] + arrTotals[0])/12).toFixed(2);

       // nlapiLogExecution('DEBUG','Totals 11-6', arrTotals[11]+';'+arrTotals[10]+';'+arrTotals[9]+';'+arrTotals[8]+';'+arrTotals[7]+';'+arrTotals[6]);

        /*arrAverages[2] =parseFloat(140049.63 ).toFixed(2);
        arrAverages[3] = parseFloat(143994.46).toFixed(2);
        arrAverages[4] = parseFloat(145264.55 ).toFixed(2);
        arrAverages[5] = parseFloat(144035.14).toFixed(2);
        arrAverages[6] = parseFloat(148125.33).toFixed(2);*/

        stReturn += '\n{name: \'Average\',type: \'spline\', color: \'#0000CC\',data: [\n';
        for(var z=0; z < arrAverages.length; z++){
            stReturn += '{y:' + arrAverages[z] + ',url:\'#\'},';
        }
        var strLen = stReturn.length;
        stReturn = stReturn.slice(0,strLen-1);

        stReturn += ']\n';
        stReturn += '},';



        stReturn += ']';

        return stReturn;
    }

    function returnRepsSeries(departmentid,marketid){

        var market = clgx_return_market_name(marketid);

        var stReturn = '[';

        var arrFilters = new Array();
        //arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        var searchEmployees = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);

        var arrEmployees = new Array();
        for ( var i = 0; searchEmployees != null && i < searchEmployees.length; i++ ) {
            arrEmployees.push(searchEmployees[i].getValue('internalid', null, null));
        }

        var arrFilters = new Array();
        var Filters = new Array();
        arrFilters.push(new nlobjSearchFilter("salesrep",null,"anyof",arrEmployees));
        Filters.push(new nlobjSearchFilter("salesrep",null,"anyof",arrEmployees));
        //arrFilters.push(new nlobjSearchFilter("location",null,"anyof",clgx_return_child_locations_of_marketid(marketid)));
        if(departmentid == 0){
            var arrMarkets=clgx_return_child_locations_of_marketid(marketid);
            arrFilters.push(new nlobjSearchFilter("location",null,"anyof",arrMarkets));
            Filters.push(new nlobjSearchFilter("location",'order',"anyof",arrMarkets));

            var filters=[];
            filters =[ [['location', 'anyof', clgx_return_child_locations_of_marketid(marketid)],
                'or',
                ['location', 'anyof', '@NONE@'] ],
                'and', ['salesrep',"anyof",arrEmployees]];
        }

        var searchReps = nlapiSearchRecord('opportunity', 'customsearch_clgx_kpi_opp_sales_reps', arrFilters, null);
        var searchRepsIncrMRC = nlapiSearchRecord('opportunity', 'customsearch_clgx_kpi_opp_sales_reps_inc', Filters, null);


        //Duplicates
        var arrColumns = new Array();
        var searchMarketsDuplicates = nlapiSearchRecord('opportunity', 'customsearch_clgx_kpi_opp_sales_reps_2', arrFilters, arrColumns);
        var objDuplicates={};
        for ( var i = 0; searchMarketsDuplicates != null && i < searchMarketsDuplicates.length; i++ ) {
            var searchMarketD = searchMarketsDuplicates[i];
            var columns = searchMarketD.getAllColumns();
            var locationD=searchMarketD.getValue(columns[0]);
            var concatenateNOAmt=searchMarketD.getValue(columns[1]);
            var numberOfMth=searchMarketD.getValue(columns[2]);
            var arr=concatenateNOAmt.split(',');
            var cloneArr=new Array();
            var duplicates=new Array();
            for(var j = 0; j < arr.length; j++) {

                if (_.indexOf(cloneArr,arr[j])==-1) {
                    cloneArr.push(arr[j]);
                } else {
                    duplicates.push(arr[j]);
                }
            }
            var summDuplicate=0;
            if(duplicates.length>0)
            {
                for(var p = 0; p < duplicates.length; p++) {
                    var arr1=duplicates[p].split('=');
                    summDuplicate +=parseFloat(arr1[1]);
                }
            }
            if(!objDuplicates.hasOwnProperty(locationD)) {
                objDuplicates[locationD] = [];
                objDuplicates[locationD][numberOfMth] = summDuplicate;
            } else {
                objDuplicates[locationD][numberOfMth] = summDuplicate;
            }
        }

        //Duplicates Incremental--customsearch_clgx_kpi_opp_sales_reps_i_2

        var searchMarketsDuplicatesI = nlapiSearchRecord('opportunity', 'customsearch_clgx_kpi_opp_sales_reps_i_2', Filters, arrColumns);
        var objDuplicatesI={};
        var objClonesI={};
        for ( var i = 0; searchMarketsDuplicatesI != null && i < searchMarketsDuplicatesI.length; i++ ) {
            var searchMarketDI = searchMarketsDuplicatesI[i];
            var columns = searchMarketDI.getAllColumns();
            var locationDI=(searchMarketDI.getValue(columns[0])!='- None -') ? searchMarketDI.getValue(columns[0]):searchMarketDI.getValue(columns[3]);
            var concatenateNOAmtI=searchMarketDI.getValue(columns[1]);
            var numberOfMthI=searchMarketDI.getValue(columns[2]);
            var arrI=concatenateNOAmtI.split(',');
            var cloneArrI=new Array();
            var duplicatesI=new Array();
            for(var j = 0; j < arrI.length; j++) {

                if (_.indexOf(cloneArrI,arrI[j])==-1) {
                    cloneArrI.push(arrI[j]);
                } else {
                    duplicatesI.push(arrI[j]);
                }
            }
            var summCloneI=0;
            if(cloneArrI.length>0)
            {
                for(var p = 0; p < cloneArrI.length; p++) {
                    var arr1I=cloneArrI[p].split('=');
                    summCloneI +=parseFloat(arr1I[1]);
                }
            }
            if(!objClonesI.hasOwnProperty(locationDI)) {
                objClonesI[locationDI] = [];
                objClonesI[locationDI][numberOfMthI] = summCloneI;
            } else {
                objClonesI[locationDI][numberOfMthI] = (typeof(objClonesI[locationDI][numberOfMthI]) !== "undefined") ? parseFloat(objClonesI[locationDI][numberOfMthI]+summCloneI) : summCloneI;
            }

            var summDuplicateI=0;
            if(duplicatesI.length>0)
            {
                for(var p = 0; p < duplicatesI.length; p++) {
                    var arr1I=duplicatesI[p].split('=');
                    summDuplicateI +=parseFloat(arr1I[1]);
                }
            }
            if(!objDuplicatesI.hasOwnProperty(locationDI)) {
                objDuplicatesI[locationDI] = [];
                objDuplicatesI[locationDI][numberOfMthI] = summDuplicateI;
            } else {
                objDuplicatesI[locationDI][numberOfMthI] = (typeof(objDuplicatesI[locationDI][numberOfMthI]) !== "undefined") ? parseFloat(objDuplicatesI[locationDI][numberOfMthI]+summDuplicateI) : summDuplicateI;
            }
        }


        var arrTotals = new Array();
        arrTotals = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
        var arrSSalesRepIds=new Array();
        for ( var i = 0; searchReps != null && i < searchReps.length; i++ ) {
            var searchRep = searchReps[i];
            var columns = searchRep.getAllColumns();
            var added=0;
            if(searchRepsIncrMRC!=null)
            {
                for ( var j = 0; searchRepsIncrMRC != null && j < searchRepsIncrMRC.length; j++ ) {
                    var searchRep1 = searchRepsIncrMRC[j];
                    var columns1 = searchRep1.getAllColumns();
                    var text0 = searchRep.getValue(columns[0]);


                    if(searchRep.getValue(columns[1])==searchRep1.getValue(columns1[1]))
                    {
                        var dup0 = (typeof(objDuplicates[text0][0]) !== "undefined") ? parseFloat(objDuplicates[text0][0]) : 0;
                        var dupI0 = (typeof(objClonesI[text0][0]) !== "undefined") ? parseFloat(objClonesI[text0][0]) : 0;
                        var dup1 = (typeof(objDuplicates[text0][1]) !== "undefined") ? parseFloat(objDuplicates[text0][1]) : 0;
                        var dupI1 = (typeof(objClonesI[text0][1]) !== "undefined") ? parseFloat(objClonesI[text0][1]) : 0;
                        var dup2 = (typeof(objDuplicates[text0][2]) !== "undefined") ? parseFloat(objDuplicates[text0][2]) : 0;
                        var dupI2 = (typeof(objClonesI[text0][2]) !== "undefined") ? parseFloat(objClonesI[text0][2]) : 0;
                        var dup3 = (typeof(objDuplicates[text0][3]) !== "undefined") ? parseFloat(objDuplicates[text0][3]) : 0;
                        var dupI3 = (typeof(objClonesI[text0][3]) !== "undefined") ? parseFloat(objClonesI[text0][3]) : 0;
                        var dup4 = (typeof(objDuplicates[text0][4]) !== "undefined") ? parseFloat(objDuplicates[text0][4]) : 0;
                        var dupI4 = (typeof(objClonesI[text0][4]) !== "undefined") ? parseFloat(objClonesI[text0][4]) : 0;
                        var dup5 = (typeof(objDuplicates[text0][5]) !== "undefined") ? parseFloat(objDuplicates[text0][5]) : 0;
                        var dupI5 = (typeof(objClonesI[text0][5]) !== "undefined") ? parseFloat(objClonesI[text0][5]) : 0;
                        var dup6 = (typeof(objDuplicates[text0][6]) !== "undefined") ? parseFloat(objDuplicates[text0][6]) : 0;
                        var dupI6 = (typeof(objClonesI[text0][6]) !== "undefined") ? parseFloat(objClonesI[text0][6]) : 0;
                        var dup7 = (typeof(objDuplicates[text0][7]) !== "undefined") ? parseFloat(objDuplicates[text0][7]) : 0;
                        var dupI7 = (typeof(objClonesI[text0][7]) !== "undefined") ? parseFloat(objClonesI[text0][7]) : 0;
                        var dup8 = (typeof(objDuplicates[text0][8]) !== "undefined") ? parseFloat(objDuplicates[text0][8]) : 0;
                        var dupI8 = (typeof(objClonesI[text0][8]) !== "undefined") ? parseFloat(objClonesI[text0][8]) : 0;
                        var dup9 = (typeof(objDuplicates[text0][9]) !== "undefined") ? parseFloat(objDuplicates[text0][9]) : 0;
                        var dupI9 = (typeof(objClonesI[text0][9]) !== "undefined") ? parseFloat(objClonesI[text0][9]) : 0;
                        var dup10 = (typeof(objDuplicates[text0][10]) !== "undefined") ? parseFloat(objDuplicates[text0][10]) : 0;
                        var dupI10 = (typeof(objClonesI[text0][10]) !== "undefined") ? parseFloat(objClonesI[text0][10]) : 0;
                        var dup11 = (typeof(objDuplicates[text0][11]) !== "undefined") ? parseFloat(objDuplicates[text0][11]) : 0;
                        var dupI11 = (typeof(objClonesI[text0][11]) !== "undefined") ? parseFloat(objClonesI[text0][11]) : 0;
                        var dup12 = (typeof(objDuplicates[text0][12]) !== "undefined") ? parseFloat(objDuplicates[text0][12]) : 0;
                        var dupI12 = (typeof(objClonesI[text0][12]) !== "undefined") ? parseFloat(objClonesI[text0][12]) : 0;
                        var dup13 = (typeof(objDuplicates[text0][13]) !== "undefined") ? parseFloat(objDuplicates[text0][13]) : 0;
                        var dupI13 = (typeof(objClonesI[text0][13]) !== "undefined") ? parseFloat(objClonesI[text0][13]) : 0;
                        var dup14 = (typeof(objDuplicates[text0][14]) !== "undefined") ? parseFloat(objDuplicates[text0][14]) : 0;
                        var dupI14 = (typeof(objClonesI[text0][14]) !== "undefined") ? parseFloat(objClonesI[text0][14]) : 0;
                        var dup15 = (typeof(objDuplicates[text0][15]) !== "undefined") ? parseFloat(objDuplicates[text0][15]) : 0;
                        var dupI15 = (typeof(objClonesI[text0][15]) !== "undefined") ? parseFloat(objClonesI[text0][15]) : 0;
                        var dup16 = (typeof(objDuplicates[text0][16]) !== "undefined") ? parseFloat(objDuplicates[text0][16]) : 0;
                        var dupI16 = (typeof(objClonesI[text0][16]) !== "undefined") ? parseFloat(objClonesI[text0][16]) : 0;
                        var dup17 = (typeof(objDuplicates[text0][17]) !== "undefined") ? parseFloat(objDuplicates[text0][17]) : 0;
                        var dupI17 = (typeof(objClonesI[text0][17]) !== "undefined") ? parseFloat(objClonesI[text0][17]) : 0;
                        var dup18 = (typeof(objDuplicates[text0][18]) !== "undefined") ? parseFloat(objDuplicates[text0][18]) : 0;
                        var dupI18 = (typeof(objClonesI[text0][18]) !== "undefined") ? parseFloat(objClonesI[text0][18]) : 0;

                        stReturn += '\n{name: \'' + searchRep.getValue(columns[1]) + '\',type: \'column\',data: [\n';
                        stReturn += '{y:' + (parseFloat(searchRep.getValue(columns[8])-parseFloat(dup6))+(parseFloat(dupI6)))+ ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-6\'},\n';
                        stReturn += '{y:' + (parseFloat(searchRep.getValue(columns[7])-parseFloat(dup5))+(parseFloat(dupI5))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-5\'},\n';
                        stReturn += '{y:' + (parseFloat(searchRep.getValue(columns[6])-parseFloat(dup4))+(parseFloat(dupI4))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-4\'},\n';
                        stReturn += '{y:' + (parseFloat(searchRep.getValue(columns[5])-parseFloat(dup3))+(parseFloat(dupI3))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-3\'},\n';
                        stReturn += '{y:' + (parseFloat(searchRep.getValue(columns[4])-parseFloat(dup2))+(parseFloat(dupI2))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-2\'},\n';
                        stReturn += '{y:' + (parseFloat(searchRep.getValue(columns[3])-parseFloat(dup1))+(parseFloat(dupI1))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-1\'},\n';
                        stReturn += '{y:' + (parseFloat(searchRep.getValue(columns[2])-parseFloat(dup0))+(parseFloat(dupI0))) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=0\'}\n';
                        stReturn += ']\n';
                        stReturn += '},';

                        arrTotals[0] += parseFloat(searchRep.getValue(columns[3])-parseFloat(dup1));
                        arrTotals[1] += parseFloat(searchRep.getValue(columns[4])-parseFloat(dup2));
                        arrTotals[2] += parseFloat(searchRep.getValue(columns[5])-parseFloat(dup3));
                        arrTotals[3] += parseFloat(searchRep.getValue(columns[6])-parseFloat(dup4));
                        arrTotals[4] += parseFloat(searchRep.getValue(columns[7])-parseFloat(dup5));
                        arrTotals[5] += parseFloat(searchRep.getValue(columns[8])-parseFloat(dup6));
                        arrTotals[6] += parseFloat(searchRep.getValue(columns[9])-parseFloat(dup7));
                        arrTotals[7] += parseFloat(searchRep.getValue(columns[10])-parseFloat(dup8));
                        arrTotals[8] += parseFloat(searchRep.getValue(columns[11])-parseFloat(dup9));
                        arrTotals[9] += parseFloat(searchRep.getValue(columns[12])-parseFloat(dup10));
                        arrTotals[10] += parseFloat(searchRep.getValue(columns[13])-parseFloat(dup11));
                        arrTotals[11] += parseFloat(searchRep.getValue(columns[14])-parseFloat(dup12));
                        arrTotals[12] += parseFloat(searchRep.getValue(columns[15])-parseFloat(dup13));
                        arrTotals[13] += parseFloat(searchRep.getValue(columns[16])-parseFloat(dup14));
                        arrTotals[14] += parseFloat(searchRep.getValue(columns[17])-parseFloat(dup15));
                        arrTotals[15] += parseFloat(searchRep.getValue(columns[18])-parseFloat(dup16));
                        arrTotals[16] += parseFloat(searchRep.getValue(columns[19])-parseFloat(dup17));
                        arrTotals[17] += parseFloat(searchRep.getValue(columns[20])-parseFloat(dup18));
                        added=1;
                        if(!(in_array(searchRep.getValue(columns[0]),arrSSalesRepIds)))
                        {
                            arrSSalesRepIds.push(searchRep.getValue(columns[0]));
                        }
                    }
                }
            }
            if(added==0)
            {
                var dup0 = (typeof(objDuplicates[text0][0]) !== "undefined") ? parseFloat(objDuplicates[text0][0]) : 0;
                var dup1 = (typeof(objDuplicates[text0][1]) !== "undefined") ? parseFloat(objDuplicates[text0][1]) : 0;
                var dup2 = (typeof(objDuplicates[text0][2]) !== "undefined") ? parseFloat(objDuplicates[text0][2]) : 0;
                var dup3 = (typeof(objDuplicates[text0][3]) !== "undefined") ? parseFloat(objDuplicates[text0][3]) : 0;
                var dup4 = (typeof(objDuplicates[text0][4]) !== "undefined") ? parseFloat(objDuplicates[text0][4]) : 0;
                var dup5 = (typeof(objDuplicates[text0][5]) !== "undefined") ? parseFloat(objDuplicates[text0][5]) : 0;
                var dup6 = (typeof(objDuplicates[text0][6]) !== "undefined") ? parseFloat(objDuplicates[text0][6]) : 0;
                var dup7 = (typeof(objDuplicates[text0][7]) !== "undefined") ? parseFloat(objDuplicates[text0][7]) : 0;
                var dup8 = (typeof(objDuplicates[text0][8]) !== "undefined") ? parseFloat(objDuplicates[text0][8]) : 0;
                var dup9 = (typeof(objDuplicates[text0][9]) !== "undefined") ? parseFloat(objDuplicates[text0][9]) : 0;
                var dup10 = (typeof(objDuplicates[text0][10]) !== "undefined") ? parseFloat(objDuplicates[text0][10]) : 0;
                var dup11 = (typeof(objDuplicates[text0][11]) !== "undefined") ? parseFloat(objDuplicates[text0][11]) : 0;
                var dup12 = (typeof(objDuplicates[text0][12]) !== "undefined") ? parseFloat(objDuplicates[text0][12]) : 0;
                var dup13 = (typeof(objDuplicates[text0][13]) !== "undefined") ? parseFloat(objDuplicates[text0][13]) : 0;
                var dup14 = (typeof(objDuplicates[text0][14]) !== "undefined") ? parseFloat(objDuplicates[text0][14]) : 0;
                var dup15 = (typeof(objDuplicates[text0][15]) !== "undefined") ? parseFloat(objDuplicates[text0][15]) : 0;
                var dup16 = (typeof(objDuplicates[text0][16]) !== "undefined") ? parseFloat(objDuplicates[text0][16]) : 0;
                var dup17 = (typeof(objDuplicates[text0][17]) !== "undefined") ? parseFloat(objDuplicates[text0][17]) : 0;
                var dup18 = (typeof(objDuplicates[text0][18]) !== "undefined") ? parseFloat(objDuplicates[text0][18]) : 0;

                stReturn += '\n{name: \'' + searchRep.getValue(columns[1]) + '\',type: \'column\',data: [\n';
                stReturn += '{y:' + parseFloat(searchRep.getValue(columns[8])-dup6)+ ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-6\'},\n';
                stReturn += '{y:' + parseFloat(searchRep.getValue(columns[7])-dup5) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-5\'},\n';
                stReturn += '{y:' +parseFloat(searchRep.getValue(columns[6])-dup4) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-4\'},\n';
                stReturn += '{y:' + parseFloat(searchRep.getValue(columns[5])-dup3) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-3\'},\n';
                stReturn += '{y:' + parseFloat(searchRep.getValue(columns[4])-dup2) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-2\'},\n';
                stReturn += '{y:' + parseFloat(searchRep.getValue(columns[3])-dup1) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-1\'},\n';
                stReturn += '{y:' + parseFloat(searchRep.getValue(columns[2])-dup0) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=0\'}\n';
                stReturn += ']\n';
                stReturn += '},';
                arrTotals[0] += parseFloat(searchRep.getValue(columns[3])-dup1);
                arrTotals[1] += parseFloat(searchRep.getValue(columns[4])-dup2);
                arrTotals[2] += parseFloat(searchRep.getValue(columns[5])-dup3);
                arrTotals[3] += parseFloat(searchRep.getValue(columns[6])-dup4);
                arrTotals[4] += parseFloat(searchRep.getValue(columns[7])-dup5);
                arrTotals[5] += parseFloat(searchRep.getValue(columns[8])-dup6);
                arrTotals[6] += parseFloat(searchRep.getValue(columns[9])-dup7);
                arrTotals[7] += parseFloat(searchRep.getValue(columns[10])-dup8);
                arrTotals[8] += parseFloat(searchRep.getValue(columns[11])-dup9);
                arrTotals[9] += parseFloat(searchRep.getValue(columns[12])-dup10);
                arrTotals[10] += parseFloat(searchRep.getValue(columns[13])-dup11);
                arrTotals[11] += parseFloat(searchRep.getValue(columns[14])-dup12);
                arrTotals[12] += parseFloat(searchRep.getValue(columns[15])-dup13);
                arrTotals[13] += parseFloat(searchRep.getValue(columns[16])-dup14);
                arrTotals[14] += parseFloat(searchRep.getValue(columns[17])-dup15);
                arrTotals[15] += parseFloat(searchRep.getValue(columns[18])-dup16);
                arrTotals[16] += parseFloat(searchRep.getValue(columns[19])-dup17);
                arrTotals[17] += parseFloat(searchRep.getValue(columns[20])-dup18);
                if(!(in_array(searchRep.getValue(columns[0]),arrSSalesRepIds)))
                {
                    arrSSalesRepIds.push(searchRep.getValue(columns[0]));
                }
            }
        }
        if((searchRepsIncrMRC!=null)&&(arrSSalesRepIds.length>0)) {
            for (var j = 0; searchRepsIncrMRC != null && j < searchRepsIncrMRC.length; j++) {
                var searchRep = searchRepsIncrMRC[j];
                var columns = searchRep.getAllColumns();
                var idSRep=searchRep.getValue(columns[0]);
                var text0=idSRep;
                var dupI0 = (typeof(objDuplicatesI[text0][0]) !== "undefined") ? parseFloat(objDuplicatesI[text0][0]) : 0;
                var dupI1 = (typeof(objDuplicatesI[text0][1]) !== "undefined") ? parseFloat(objDuplicatesI[text0][1]) : 0;
                var dupI2 = (typeof(objDuplicatesI[text0][2]) !== "undefined") ? parseFloat(objDuplicatesI[text0][2]) : 0;
                var dupI3 = (typeof(objDuplicatesI[text0][3]) !== "undefined") ? parseFloat(objDuplicatesI[text0][3]) : 0;
                var dupI4 = (typeof(objDuplicatesI[text0][4]) !== "undefined") ? parseFloat(objDuplicatesI[text0][4]) : 0;
                var dupI5 = (typeof(objDuplicatesI[text0][5]) !== "undefined") ? parseFloat(objDuplicatesI[text0][5]) : 0;
                var dupI6 = (typeof(objDuplicatesI[text0][6]) !== "undefined") ? parseFloat(objDuplicatesI[text0][6]) : 0;
                var dupI7 = (typeof(objDuplicatesI[text0][7]) !== "undefined") ? parseFloat(objDuplicatesI[text0][7]) : 0;
                var dupI8 = (typeof(objDuplicatesI[text0][8]) !== "undefined") ? parseFloat(objDuplicatesI[text0][8]) : 0;
                var dupI9 = (typeof(objDuplicatesI[text0][9]) !== "undefined") ? parseFloat(objDuplicatesI[text0][9]) : 0;
                var dupI10 = (typeof(objDuplicatesI[text0][10]) !== "undefined") ? parseFloat(objDuplicatesI[text0][10]) : 0;
                var dupI11 = (typeof(objDuplicatesI[text0][11]) !== "undefined") ? parseFloat(objDuplicatesI[text0][11]) : 0;
                var dupI12 = (typeof(objDuplicatesI[text0][12]) !== "undefined") ? parseFloat(objDuplicatesI[text0][12]) : 0;
                var dupI13 = (typeof(objDuplicatesI[text0][13]) !== "undefined") ? parseFloat(objDuplicatesI[text0][13]) : 0;
                var dupI14 = (typeof(objDuplicatesI[text0][14]) !== "undefined") ? parseFloat(objDuplicatesI[text0][14]) : 0;
                var dupI15 = (typeof(objDuplicatesI[text0][15]) !== "undefined") ? parseFloat(objDuplicatesI[text0][15]) : 0;
                var dupI16 = (typeof(objDuplicatesI[text0][16]) !== "undefined") ? parseFloat(objDuplicatesI[text0][16]) : 0;
                var dupI17 = (typeof(objDuplicatesI[text0][17]) !== "undefined") ? parseFloat(objDuplicatesI[text0][17]) : 0;
                var dupI18 = (typeof(objDuplicatesI[text0][18]) !== "undefined") ? parseFloat(objDuplicatesI[text0][18]) : 0;

                if(!in_array(idSRep,arrSSalesRepIds))
                {
                    stReturn += '\n{name: \'' + searchRep.getValue(columns[1]) + '\',type: \'column\',data: [\n';
                    stReturn += '{y:' + parseFloat(searchRep.getValue(columns[8])-dupI6)+ ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-6\'},\n';
                    stReturn += '{y:' + parseFloat(searchRep.getValue(columns[7])-dupI5) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-5\'},\n';
                    stReturn += '{y:' +parseFloat(searchRep.getValue(columns[6])-dupI4) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-4\'},\n';
                    stReturn += '{y:' + parseFloat(searchRep.getValue(columns[5])-dupI3) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-3\'},\n';
                    stReturn += '{y:' + parseFloat(searchRep.getValue(columns[4])-dupI2) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-2\'},\n';
                    stReturn += '{y:' + parseFloat(searchRep.getValue(columns[3])-dupI1) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=-1\'},\n';
                    stReturn += '{y:' + parseFloat(searchRep.getValue(columns[2])-dupI0) + ',url:\'/app/site/hosting/scriptlet.nl?script=717&deploy=1&forecast=0&repid=' + searchRep.getValue(columns[0]) + '&market=' + market + '&departmentid=' + departmentid + '&month=0\'}\n';
                    stReturn += ']\n';
                    stReturn += '},';
                    arrTotals[0] += parseFloat(searchRep.getValue(columns[3])-dupI1);
                    arrTotals[1] += parseFloat(searchRep.getValue(columns[4])-dupI2);
                    arrTotals[2] += parseFloat(searchRep.getValue(columns[5])-dupI3);
                    arrTotals[3] += parseFloat(searchRep.getValue(columns[6])-dupI4);
                    arrTotals[4] += parseFloat(searchRep.getValue(columns[7])-dupI5);
                    arrTotals[5] += parseFloat(searchRep.getValue(columns[8])-dupI6);
                    arrTotals[6] += parseFloat(searchRep.getValue(columns[9])-dupI7);
                    arrTotals[7] += parseFloat(searchRep.getValue(columns[10])-dupI8);
                    arrTotals[8] += parseFloat(searchRep.getValue(columns[11])-dupI9);
                    arrTotals[9] += parseFloat(searchRep.getValue(columns[12])-dupI10);
                    arrTotals[10] += parseFloat(searchRep.getValue(columns[13])-dupI11);
                    arrTotals[11] += parseFloat(searchRep.getValue(columns[14])-dupI12);
                    arrTotals[12] += parseFloat(searchRep.getValue(columns[15])-dupI13);
                    arrTotals[13] += parseFloat(searchRep.getValue(columns[16])-dupI14);
                    arrTotals[14] += parseFloat(searchRep.getValue(columns[17])-dupI15);
                    arrTotals[15] += parseFloat(searchRep.getValue(columns[18])-dupI16);
                    arrTotals[16] += parseFloat(searchRep.getValue(columns[19])-dupI17);
                    arrTotals[17] += parseFloat(searchRep.getValue(columns[20])-dupI18);
                }
            }
        }


        var arrAverages = new Array();
        arrAverages = [0,0,0,0,0,0,0];

        arrAverages[0] = parseFloat((arrTotals[17] + arrTotals[16] + arrTotals[15] + arrTotals[14] + arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6])/12).toFixed(2);
        arrAverages[1] = parseFloat((arrTotals[16] + arrTotals[15] + arrTotals[14] + arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5])/12).toFixed(2);
        arrAverages[2] = parseFloat((arrTotals[15] + arrTotals[14] + arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4])/12).toFixed(2);
        arrAverages[3] = parseFloat((arrTotals[14] + arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4] + arrTotals[3])/12).toFixed(2);
        arrAverages[4] = parseFloat((arrTotals[13] + arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4] + arrTotals[3] + arrTotals[2])/12).toFixed(2);
        arrAverages[5] = parseFloat((arrTotals[12] + arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4] + arrTotals[3] + arrTotals[2] + arrTotals[1])/12).toFixed(2);
        arrAverages[6] = parseFloat((arrTotals[11] + arrTotals[10] + arrTotals[9] + arrTotals[8] + arrTotals[7] + arrTotals[6] + arrTotals[5] + arrTotals[4] + arrTotals[3] + arrTotals[2] + arrTotals[1] + arrTotals[0])/12).toFixed(2);

        stReturn += '\n{name: \'Average\',type: \'spline\', color: \'#0000CC\',data: [\n';
        for(var z=0; z < arrAverages.length; z++){
            stReturn += '{y:' + arrAverages[z] + ',url:\'#\'},';
        }
        var strLen = stReturn.length;
        stReturn = stReturn.slice(0,strLen-1);

        stReturn += ']\n';
        stReturn += '},';




        stReturn += ']';

        return stReturn;
    }

    function returnMonthsCategories (){
        var mom = new moment();
        var categories = '[';
        categories += '\'' + mom.add('months', -6).format('M.YYYY') + '\',';
        categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\',';
        categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\',';
        categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\',';
        categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\',';
        categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\',';
        categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\'';
        categories += ']';
        return categories;
    }

    function in_array (val, arr){
        var bIsValueFound = false;
        for(var i = 0; i < arr.length; i++){
            if(val == arr[i]){
                bIsValueFound = true;
                break;
            }
        }
        return bIsValueFound;
    }
