nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Sales_Services_Details.js
//	Script Name:	CLGX_SL_CHARTS_Sales_Services_Details
//	Script Id:		customscript_clgx_sl_chrt_sale_details
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		07/29/2013
//	Includes:		CLGX_LIB_Global.js
//-------------------------------------------------------------------------------------------------

function suitelet_charts_sale_serv_details (request, response){
	try {
		
		var market = request.getParameter('market');
		var service = request.getParameter('service');
		var month = request.getParameter('month');

	    var arrDates  = getDateRange(month);
	    var startDate = arrDates[0];
	    var endDate   = arrDates[1];
	    
		if(market != '0'){
			var title = market + ' - ' + service + ' - Between ' + startDate + ' and ' + endDate;
		}
		
		var objFile = nlapiLoadFile(563259);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{salesJSON}','g'), dataSales(market, service, month));
		html = html.replace(new RegExp('{title}','g'), title);
		
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function dataSales (market, service, month) {

	var arrFilters = new Array();
	if(market != 'All'){
		arrFilters.push(new nlobjSearchFilter("location",null,"anyof",clgx_get_marketid(market)));
	}
	arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",71418));
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	var searchEmployees = nlapiSearchRecord('employee', 'customsearch_clgx_chrt_act_sale_reps', arrFilters, arrColumns);
	
	var arrEmployees = new Array();
	for ( var i = 0; searchEmployees != null && i < searchEmployees.length; i++ ) {
		var searchEmployee = searchEmployees[i];
		var repid = searchEmployee.getValue('internalid', null, null);
		arrEmployees.push(repid);
	}
	
    var arrDates  = getDateRange(month);
    var startDate = arrDates[0];
    var endDate   = arrDates[1];
    
	var arrFilters = new Array();
	if(market != '0'){
		arrFilters.push(new nlobjSearchFilter('location',null,'anyof',clgx_return_child_locations_of_marketname (market)));
	}
	if(service != '0'){
		arrFilters.push(new nlobjSearchFilter('custcol_cologix_invoice_item_category',null,'anyof',clgx_get_item_categoryid(service)));
	}
	arrFilters.push(new nlobjSearchFilter('saleseffectivedate',null,'within',startDate,endDate));
	arrFilters.push(new nlobjSearchFilter('salesrep',null,'anyof',arrEmployees));
	var arrColumns = new Array();
	var searchSales = nlapiSearchRecord('transaction', 'customsearch_clgx_chrt_sales_serv_detail', arrFilters, arrColumns);

	var stReturn = 'var dataSales =  [';
	
	for ( var i = 0; searchSales != null && i < searchSales.length; i++ ) {
		var searchSale = searchSales[i];

		stReturn += '\n{"number":"' + searchSale.getValue('tranid', null, 'GROUP') + 
						'","internalid":' +  searchSale.getValue('internalid', null, 'GROUP') + 
						',"date":"' + searchSale.getValue('saleseffectivedate', null, 'GROUP') + 
						'","customer":"' +  searchSale.getValue('entityid','customer', 'GROUP') + 
						'","customerid":0' +  
						',"salerep":"' +  searchSale.getText('salesrep', null, 'GROUP') + 
						'","salerepid":' +  searchSale.getValue('salesrep', null, 'GROUP') + 
						',"total":' + searchSale.getValue('amount', null, 'SUM') + 
						'},'; 
	}
	var strLen = stReturn.length;
	if (searchSales != null){
		stReturn = stReturn.slice(0,strLen-1);
	}
	stReturn += '];';
    return stReturn;
}


function getDateRange(monthsago){
	
	var today = new Date();
	var date = nlapiAddMonths(today, monthsago);
	
    var month = parseInt(date.getMonth());
    var year = date.getFullYear();

    var stDays  = daysInMonth(parseInt(month),parseInt(year));
    var stYear  = year;
    
    var stMonth = parseInt(month) + 1;
    
    var stStartDate = stMonth + '/1/' + stYear;
    var stEndDate   = stMonth + '/' + stDays + '/' + stYear;
    
    var arrDateRange = new Array();
        arrDateRange[0] = stStartDate;
        arrDateRange[1] =stEndDate;
    
    return arrDateRange;
}

//get the last day of the month
function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}
