nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Forecast_Gauge.js
//	Script Name:	CLGX_SL_CHARTS_Forecast_Gauge
//	Script Id:		customscript_clgx_sl_chrt_forecast_gauge
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		08/18/2013
//	URL:			/app/site/hosting/scriptlet.nl?script=218&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_chrt_forecast_gauge (request, response){
	try {
		var objFile = nlapiLoadFile(584652);
		
        var arrMonthAmounts  = returnMonthAmounts();
        var monthBudget = arrMonthAmounts[0];
        var monthCommited   = arrMonthAmounts[1];
        
        var date = new moment();
        
		var html = objFile.getValue();
		html = html.replace(new RegExp('{forecast}','g'),returnForecast()/1000);
		html = html.replace(new RegExp('{forecastK}','g'),numberWithCommas(returnForecast()));
		html = html.replace(new RegExp('{budget}','g'),monthBudget/1000);
		html = html.replace(new RegExp('{budgetK}','g'),numberWithCommas(monthBudget));
		html = html.replace(new RegExp('{startForecast}','g'),monthCommited/1000);
		html = html.replace(new RegExp('{title}','g'), date.format('M/YYYY'));
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function returnForecast(){

	var arrFilters = new Array();
	var arrColumns = new Array();
	var searchSales = nlapiSearchRecord('transaction', 'customsearch_clgx_chrt_sale_fun_sales2', arrFilters, arrColumns);
	
	var searchSale = searchSales[0];
	var columns = searchSale.getAllColumns();
	var monthSales = searchSale.getValue(columns[0]);
	

	var arrFilters = new Array();
	var arrColumns = new Array();
	var searchSalesIncrementals = nlapiSearchRecord('transaction', 'customsearch_clgx_chrt_sale_fun_sales2_2', arrFilters, arrColumns);
	
	var searchSalesIncremental = searchSalesIncrementals[0];
	var columns = searchSalesIncremental.getAllColumns();
	var monthSalesIncremental = searchSalesIncremental.getValue(columns[0]);
	
	
	var arrFilters = new Array();
	var arrColumns = new Array();
	var searchForecasts = nlapiSearchRecord('opportunity', 'customsearch_clgx_chrt_sale_fun_forecast', arrFilters, arrColumns);
	
	var searchForecast = searchForecasts[0];
	var columns = searchForecast.getAllColumns();
	var monthCommit = searchForecast.getValue(columns[3]);
	
	
	var arrFilters = new Array();
	var arrColumns = new Array();
	var searchForecastsIncrementals = nlapiSearchRecord('opportunity', 'customsearch_clgx_chrt_sale_fun_foreca_2', arrFilters, arrColumns);
	
	var searchForecastsIncremental = searchForecastsIncrementals[0];
	var columns = searchForecastsIncremental.getAllColumns();
	var monthCommitIncremental = searchForecastsIncremental.getValue(columns[3]);
	
	
	var forecast = ((parseFloat(monthSales) + parseFloat(monthSalesIncremental) + parseFloat(monthCommit) + parseFloat(monthCommitIncremental))).toFixed(0);
	
	return forecast;
}

function returnMonthAmounts(){
	
	var date = new moment();
    var year = date.format('YYYY');
    var month = date.format('M');
    
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_clgx_sales_budget',null,'SUM'));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_clgx_sales_commited',null,'SUM'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_clgx_sales_month",null,"equalto", month));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_clgx_sales_year",null,"equalto", year));
	var searchBudget = nlapiSearchRecord('customrecord_clgx_cologix_sales', null, arrFilters, arrColumns);
	
	var searchBudget = searchBudget[0];
	var columns = searchBudget.getAllColumns();
	var monthBudget = searchBudget.getValue(columns[0]);
	var monthCommited = searchBudget.getValue(columns[1]);
	
    var arrMonthAmounts = new Array();
    arrMonthAmounts[0] = parseFloat(monthBudget).toFixed(0);
    arrMonthAmounts[1] = parseFloat(monthCommited).toFixed(0);
	return arrMonthAmounts;
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


