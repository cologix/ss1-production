nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Renewals_Grid.js
//	Script Name:	CLGX_SL_CHARTS_Renewals_Grid
//	Script Id:		customscript_clgx_sl_chrt_renew_grid
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/15/2013
//-------------------------------------------------------------------------------------------------
function suitelet_renew_grid (request, response){
    try{
        //var type = request.getParameter('type');
        var marketid = request.getParameter('marketid');
		var repid = request.getParameter('repid');
		var csv = request.getParameter('csv');
		
		var xcs = request.getParameter('xcs');
		var terms = request.getParameter('terms');
		var days = request.getParameter('days');
		
		var arrRenewalsData = new Array();
		arrRenewalsData = dataRenewals(marketid, repid, xcs, terms, days);
		var returnJSON = arrRenewalsData[0];
		var returnCSV = arrRenewalsData[1];
		
		if(csv == 1){
			var attachments = new Array();
			if(returnCSV != ''){
				var fileCSV = nlapiCreateFile('sos.csv', 'CSV', returnCSV);
				attachments.push(fileCSV);
			}
			var userId = nlapiGetUser();
			emailSubject = 'Export CSVs for renewals';
			emailBody = '';
			nlapiSendEmail(userId,userId,emailSubject,emailBody,null,null,null,attachments,true);
		}
		
		if(marketid != '0'){
			var title = clgx_return_market_name (marketid);
		}
		if(repid != '0'){
			var title = nlapiLookupField('employee', parseInt(repid), 'entityid');
		}
		
		var objFile = nlapiLoadFile(938830);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{renewalsJSON}','g'), returnJSON);
		html = html.replace(new RegExp('{title}','g'), title);
		html = html.replace(new RegExp('{marketid}','g'), marketid);
		html = html.replace(new RegExp('{repid}','g'), repid);
		html = html.replace(new RegExp('{xcs}','g'), xcs);
		html = html.replace(new RegExp('{terms}','g'), terms);
		html = html.replace(new RegExp('{xcsno}','g'), return_oposite (xcs));
		html = html.replace(new RegExp('{termsno}','g'), return_oposite (terms));
		html = html.replace(new RegExp('{labelxcs}','g'), return_label (xcs));
		html = html.replace(new RegExp('{labelterms}','g'), return_label (terms));
		
		response.write( html );
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

// =======================================================================

function dataRenewals (marketid, repid, xcs, terms, days){
	
	var arrLocations = new Array();
	if(marketid > 0){
		arrLocations = clgx_return_child_locations_of_marketid (marketid);
	}
	else{
		arrLocations = [0];
	}
	var searchInactives = nlapiSearchRecord('transaction', 'customsearch_clgx_sos_with_inactives', null, null);
	var arrSOs = new Array();
	for ( var i = 0; searchInactives != null && i < searchInactives.length; i++ ) {
		var searchInactive = searchInactives[i];
		var soid = searchInactive.getValue('internalid', null, 'GROUP');
		arrSOs.push(soid);
	}
    var arrColumns = new Array();
    var arrFilters = new Array();
    if(arrLocations.length > 0 && marketid > 0){
    	//arrFilters.push(new nlobjSearchFilter("location",null,"anyof",arrLocations));
    	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_location",'custrecord_clgx_totals_transaction',"anyof",arrLocations));
    }
    if(repid > 0){
    	arrFilters.push(new nlobjSearchFilter("salesrep",'customerMain',"anyof",repid));
    }
    if(arrSOs.length > 0){
    	//arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",arrSOs));
    }
    if(xcs == 0){
    	arrFilters.push(new nlobjSearchFilter("custcol_cologix_invoice_item_category",null,"noneof",4));
    }
    if(terms == 0){
    	arrFilters.push(new nlobjSearchFilter("custbody_cologix_biling_terms",null,"isnot",'MTM'));
    }
    var searchResults = nlapiSearchRecord('transaction', 'customsearch_clgx_chrt_renewals', arrFilters, arrColumns);

	var rowsArr = new Array();
	var csvSOs = 'Rep,Action By Days,Customer,SO#,Opportunity,MMR,Initial Service Term,Current End Date,Auto Renew Start Date,Auto Renew Term,Annual Escalator,MSA\n';
	
    for ( var j = 0; searchResults != null && j < searchResults.length; j++ ) {

    	var recInclude = 0;
    	
    	var searchResult = searchResults[j];
    	var location = searchResult.getValue('locationnohierarchy',null,'GROUP');
    	var salesRep = searchResult.getText('salesrep','customerMain','GROUP');
    	var customerid = searchResult.getValue('name',null,'GROUP');
    	var customer = searchResult.getText('name',null,'GROUP');
    	var customerMSA = searchResult.getText('custentity_clgx_msa','customerMain','GROUP');
    	
    	var internalid = searchResult.getValue('internalid',null,'GROUP');
    	var discoTerms = searchResult.getText('custentity_cglx_disco_notice_terms','customerMain','GROUP');
    	var renewTerms = searchResult.getText('custentity_clgx_cust_renewal_term','customerMain','GROUP');
    	var billingTerms = searchResult.getValue('custbody_cologix_biling_terms',null,'GROUP');
    	var contractStartDate = searchResult.getValue('custbody_cologix_so_contract_start_dat',null,'GROUP');
    	var contractEndDate = searchResult.getValue('enddate',null,'GROUP');
    	var contractEndDate = searchResult.getValue('enddate',null,'GROUP');
    	var number = searchResult.getValue('tranid',null,'GROUP');
    	//var totalRecMonth = searchResult.getValue('custbody_clgx_total_recurring_month',null,'GROUP');
    	var totalRecMonth = searchResult.getValue('custrecord_clgx_totals_total','CUSTRECORD_CLGX_TOTALS_TRANSACTION','GROUP');
    	
    	var annualAccel = searchResult.getValue('custbody_cologix_annual_accelerator',null,'GROUP');
    	
    	var renewStartDate = searchResult.getValue('custbody_clgx_renewal_start_date',null,'GROUP');
    	var renewEndDate = searchResult.getValue('custbody_clgx_renewal_end_date',null,'GROUP');
    	var renewCycle = searchResult.getValue('custbody_clgx_contract_cycle',null,'GROUP');
    	
    	var renewOpptyID = searchResult.getValue('custbody_clgx_so_renewed_on_oppty',null,'GROUP');
    	var renewOppty = searchResult.getText('custbody_clgx_so_renewed_on_oppty',null,'GROUP');
    	var statusOppty = searchResult.getText('entitystatus','CUSTBODY_CLGX_SO_RENEWED_ON_OPPTY','GROUP');
    	
    	var pmflag = searchResult.getValue('custbody_clgx_so_pm_flag',null,'GROUP');
    	
    	// calculated fields
    	var renewEndDateMom = new moment(nlapiStringToDate(renewEndDate));
    	var todayMom = moment();
    	//var expiresIn = Math.ceil(todayMom.diff(renewEndDateMom, 'days', true));
    	var expiresIn = Math.ceil(renewEndDateMom.diff(todayMom, 'days', true));
    	
    	if ((billingTerms == 'MTM' || billingTerms == 'Month to Month') && (renewTerms != 'See MSA' && discoTerms != 'See MSA')){
    		actionBy = 0;
    	}
    	else if (billingTerms != 'MTM' && billingTerms != '' && billingTerms != null && (renewTerms == 'See MSA' || discoTerms == 'See MSA') && contractEndDate != ''){
    		var actionBy = expiresIn - 90; // only for See MSA
		}
    	else if(billingTerms != 'MTM' && billingTerms != '' && billingTerms != null && (renewTerms == 'Month to Month' || renewTerms == 'Original Contract Term' || renewTerms == '- None -' || renewTerms == '3 Months' || renewTerms == '12 Months' || renewTerms == '24 Months' || renewTerms == '36 Months' || renewTerms == '60 Months') && discoTerms != 'See MSA' && contractEndDate != ''){
    		var actionBy = expiresIn - get_disco_days (discoTerms);
    	}
    	else{
    		var actionBy = 0;
    	}
    
    	if(actionBy < 0){
    		actionBy = 0;
    	}

    	if(days == '' || days == null){
    		recInclude = 1;
    	}
    	else{
        	if (days == 30 && actionBy < 31){
        		recInclude = 1;
        	}
        	else if (days == 60 && actionBy > 30 && actionBy < 61){
        		recInclude = 1;
        	}
        	else if (days == 90 && actionBy > 60 && actionBy < 91){
        		recInclude = 1;
        	}
        	else if (days == 180 && actionBy > 90 && actionBy < 181){
        		recInclude = 1;
        	}
        	else{}
    	}
    	
    	if(recInclude == 1){
	    	var rowObj = new Object();
	    	rowObj['id'] = internalid;
	    	rowObj['number'] = number;
	    	if(parseInt(renewOpptyID) > 0 && statusOppty != 'Closed Lost' && statusOppty != 'Lost Customer'){
		    	rowObj['opptyid'] = renewOpptyID;
		    	rowObj['oppty'] = renewOppty;
	    	}
	    	else{
		    	rowObj['opptyid'] = 0;
		    	rowObj['oppty'] = '';
	    	}
	    	rowObj['opptystatus'] = statusOppty;
	    	rowObj['location'] = location;
	    	rowObj['salesRep'] = salesRep;
	    	rowObj['customerid'] = customerid;
	    	rowObj['customer'] = customer;
	    	rowObj['customerMSA'] = customerMSA;
	    	rowObj['totalRec'] = totalRecMonth;
	    	rowObj['annualAccel'] = annualAccel;
	    	rowObj['discoTerms'] = discoTerms;
	    	rowObj['renewTerms'] = renewTerms;
	    	rowObj['billingTerms'] = billingTerms;
	    	rowObj['contractStartDate'] = contractStartDate;
	    	rowObj['contractEndDate'] = contractEndDate;
	    	rowObj['renewCycle'] = renewCycle;
	    	rowObj['renewStartDate'] = renewStartDate;
	    	rowObj['renewEndDate'] = renewEndDate; 
	    	rowObj['expiresIn'] = expiresIn;
	    	rowObj['actionBy'] = actionBy;
	    	rowObj['pmflag'] = pmflag;
	    	rowsArr.push(rowObj);
    	}
    
    	csvSOs += 	salesRep + ',' +	
					actionBy + ',' +	
					customer.replace(/\,/g," ") + ',' +	
					number +  ',' +	
					renewOppty +  ',' +	
					totalRecMonth +  ',' +	
					billingTerms +  ',' +	
					renewEndDate +  ',' +	
					renewStartDate +  ',' +	
					renewTerms +  ',' +
					annualAccel +  ',' +
					customerMSA +  '\n';
    }
	
	var arrReturn = new Array();
	arrReturn[0] = JSON.stringify(rowsArr);
	arrReturn[1] = csvSOs;
	
    return arrReturn;
}

function get_contract_months (renewTerms, billingTerms){
	// 12 Months	 	2	 
	// 24 Months	 	8	 
	// 36 Months	 	4	 
	// Month to Month	 	1	 
	// Original Contract Term	 	3	 
	// See MSA	 	5
	var months = 1;
	switch(renewTerms) {
	case '12 Months':
		months = 12;
		break;
	case '24 Months':
		months = 24;
		break;
	case '36 Months':
		months = 36;
		break;
	case 'Month to Month':
		months = 1;
		break;
	case 'Original Contract Term':
		switch(billingTerms) {
		case 'MTM':
			months = 1;
			break;
		case '':
			months = 1;
			break;
		default:
			months = parseInt(billingTerms);
		}
	case 'See MSA':
		months = 3;
		break;
	default:
		months = 1;
	}
	return months;
}

function get_disco_days (discoTerms){
	// 30 Days	 	1	 
	// 60 Days	 	2	 
	// 90 Days	 	3	 
	// 120 Days	 	4	 
	// See MSA	 	5 - 90 Days
	var discoDays = 0;
	switch(discoTerms) {
	case '30 Days':
		discoDays = 30;
		break;
	case '60 Days':
		discoDays = 60;
		break;
	case '90 Days':
		discoDays = 90;
		break;
	case '120 Days':
		discoDays = 120;
		break;
	case 'See MSA':
		discoDays = 90;
		break;
	default:
		discoDays = 0;
	}
	return discoDays;
}

function getFormatedDate(date2format){
    
	var date = nlapiStringToDate(date2format);
    var month = parseInt(date.getMonth()) + 1;
    var day = date.getDate();
    var year = date.getFullYear();
    
    if(parseInt(month) < 10){
        month = '0' + month;
    }
    
    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = year + '-' + month + '-' + day;
    
    return formattedDate;
}

function return_oposite (value){
	if(value == 0){
		return 1;
	}
	else{
		return 0;
	}
}

function return_label (value){
	if(value == 0){
		return 'Include';
	}
	else{
		return 'Exclude';
	}
}