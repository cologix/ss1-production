nlapiLogExecution("audit","FLOStart",new Date().getTime());
/**
 * Created by catalinataran on 2014-04-22.
 */
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Renewals_Chart.js
//	Script Name:	CLGX_SL_CHARTS_Renewals_Chart
//	Script Id:		customscript_clgx_sl_chrt_renew_chrt
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/07/2014
//	Includes:		CLGX_LIB_Global.js, moment.min.js
//-------------------------------------------------------------------------------------------------

function suitelet_charts_renewals_chart (request, response){
    try {
        var marketid = request.getParameter('marketid');

        if(marketid != '' ){
            var objFile = nlapiLoadFile(938931);
            var html = objFile.getValue();
            html = html.replace(new RegExp('{series}','g'), getSeries(marketid));
            html = html.replace(new RegExp('{title}','g'),clgx_return_market_name(marketid));
        }
        else{
            var html = 'Please select a market from the left panel.';
        }
        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function getSeries (marketid){

    var arrLocations = new Array();
    if(marketid > 0){
        arrLocations = clgx_return_child_locations_of_marketid (marketid);
    }
    else{
        arrLocations = [0];
    }

    var searchInactives = nlapiSearchRecord('transaction', 'customsearch_clgx_sos_with_inactives', null, null);
    var arrSOs = new Array();
    for ( var i = 0; searchInactives != null && i < searchInactives.length; i++ ) {
        var searchInactive = searchInactives[i];
        var soid = searchInactive.getValue('internalid', null, 'GROUP');
        arrSOs.push(soid);
    }

    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("location",null,"anyof",arrLocations));
    //arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",arrSOs));
    var arrColumns = new Array();
    var searchReps = nlapiSearchRecord('transaction', 'customsearch_clgx_chrt_renewals_reps', arrFilters, arrColumns);

    var arrReps = new Array();
    for ( var i = 0; searchReps != null && i < searchReps.length; i++ ) {
        var searchRep = searchReps[i];
        var repid = searchRep.getValue('salesrep', 'customerMain', 'GROUP');
        var repname = searchRep.getText('salesrep', 'customerMain', 'GROUP');

        var arrColumns = new Array();
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("salesrep",'customerMain',"anyof",repid));
        if(arrLocations.length > 0 && marketid > 0){
            //arrFilters.push(new nlobjSearchFilter("location",null,"anyof",arrLocations));
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_location",'custrecord_clgx_totals_transaction',"anyof",arrLocations));
        }
        if(arrSOs.length > 0){
            //arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",arrSOs));
        }
        arrFilters.push(new nlobjSearchFilter("custcol_cologix_invoice_item_category",null,"noneof",4));
        arrFilters.push(new nlobjSearchFilter("custbody_cologix_biling_terms",null,"isnot",'MTM'));
        var searchResults = nlapiSearchRecord('transaction', 'customsearch_clgx_chrt_renewals', arrFilters, arrColumns);

        var arrTotals = new Array();
        var total30 = 0;
        var total60 = 0;
        var total90 = 0;
        var total180 = 0;

        for ( var j = 0; searchResults != null && j < searchResults.length; j++ ) {

            var searchResult = searchResults[j];
            var discoTerms = searchResult.getText('custentity_cglx_disco_notice_terms','customerMain','GROUP');
            var renewTerms = searchResult.getText('custentity_clgx_cust_renewal_term','customerMain','GROUP');
            var billingTerms = searchResult.getValue('custbody_cologix_biling_terms',null,'GROUP');
            //var totalRecMonth = searchResult.getValue('custbody_clgx_total_recurring_month',null,'GROUP');
            var totalRecMonth = searchResult.getValue('custrecord_clgx_totals_total','custrecord_clgx_totals_transaction','GROUP');

            var renewEndDate = searchResult.getValue('custbody_clgx_renewal_end_date',null,'GROUP');

            // calculated fields
            var renewEndDateMom = new moment(nlapiStringToDate(renewEndDate));
            var todayMom = moment();
            var expiresIn = Math.ceil(renewEndDateMom.diff(todayMom, 'days', true));

            if ((billingTerms == 'MTM' || billingTerms == 'Month to Month') && (renewTerms != 'See MSA' && discoTerms != 'See MSA')){
                actionBy = 0;
            }
            else if (billingTerms != 'MTM' && billingTerms != '' && billingTerms != null && (renewTerms == 'See MSA' || discoTerms == 'See MSA')){
                var actionBy = expiresIn - 90; // only for See MSA
            }
            else if(billingTerms != 'MTM' && billingTerms != '' && billingTerms != null && (renewTerms == 'Month to Month' || renewTerms == 'Original Contract Term' || renewTerms == '- None -' || renewTerms == '3 Months' || renewTerms == '12 Months' || renewTerms == '24 Months' || renewTerms == '36 Months' || renewTerms == '60 Months') && discoTerms != 'See MSA'){
                var actionBy = expiresIn - get_disco_days (discoTerms);
            }
            else{
                var actionBy = 0;
            }

            if(actionBy < 0){
                actionBy = 0;
            }

            if (actionBy < 31){
                total30 += parseFloat(totalRecMonth);
            }
            else if (actionBy > 30 && actionBy < 61){
                total60 += parseFloat(totalRecMonth);
            }
            else if (actionBy > 60 && actionBy < 91){
                total90 += parseFloat(totalRecMonth);
            }
            else if (actionBy > 90 && actionBy < 181){
                total180 += parseFloat(totalRecMonth);
            }
            else{}
        }

        var objT30 = new Object();
        objT30['y'] = total30;
        objT30['url'] = '/app/site/hosting/scriptlet.nl?script=252&deploy=1&repid=' + repid + '&marketid=' + marketid + '&xcs=0&terms=0&days=30';
        arrTotals.push(objT30);

        var objT60 = new Object();
        objT60['y'] = total60;
        objT60['url'] = '/app/site/hosting/scriptlet.nl?script=252&deploy=1&repid=' + repid + '&marketid=' + marketid + '&xcs=0&terms=0&days=60';
        arrTotals.push(objT60);

        var objT90 = new Object();
        objT90['y'] = total90;
        objT90['url'] = '/app/site/hosting/scriptlet.nl?script=252&deploy=1&repid=' + repid + '&marketid=' + marketid + '&xcs=0&terms=0&days=90';
        arrTotals.push(objT90);

        var objT180 = new Object();
        objT180['y'] = total180;
        objT180['url'] = '/app/site/hosting/scriptlet.nl?script=252&deploy=1&repid=' + repid + '&marketid=' + marketid + '&xcs=0&terms=0&days=180';
        arrTotals.push(objT180);

        if(total30 > 0 || total60 > 0 || total90 > 0 || total180 > 0){
            var objRep = new Object();
            objRep['name'] = repname;
            objRep['type'] = 'column';
            objRep['data'] = arrTotals;
            arrReps.push(objRep);
        }

    }

    return JSON.stringify(arrReps);
}


function returnMonthsCategories (){
    var mom = new moment();
    var categories = '[';
    categories += '\'' + mom.add('months', 0).format('M.YYYY') + '\',';
    categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\',';
    categories += '\'' + mom.add('months', 1).format('M.YYYY') + '\'';
    categories += ']';
    return categories;
}


function get_disco_days (discoTerms){
    // 30 Days	 	1
    // 60 Days	 	2
    // 90 Days	 	3
    // 120 Days	 	4
    // See MSA	 	5 - 90 Days
    var discoDays = 0;
    switch(discoTerms) {
        case '30 Days':
            discoDays = 30;
            break;
        case '60 Days':
            discoDays = 60;
            break;
        case '90 Days':
            discoDays = 90;
            break;
        case '120 Days':
            discoDays = 120;
            break;
        case 'See MSA':
            discoDays = 90;
            break;
        default:
            discoDays = 0;
    }
    return discoDays;
}
