nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SP_CHARTS_Frame.js
//	Script Name:	CLGX_SP_CHARTS_Frame
//	Script Id:		customscript_clgx_sp_chrt_frame
//	Script Runs:	On Server
//	Script Type:	Portlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		07/20/2013
//-------------------------------------------------------------------------------------------------
/*
function portlet_chrt_frame (portlet, column){
    portlet.setTitle('Management KPI');
    var html = '<iframe name="chartsFrame" id="chartsFrame" src="/app/site/hosting/scriptlet.nl?script=207&deploy=1" height="540px" width="800px" frameborder="0" scrolling="no"></iframe>';
    //html += '<iframe name="content" id="content" src="https://sso.services.box.net/sp/startSSO.ping?PartnerIdpId=https://app.onelogin.com/saml/metadata/210600&TargetResource=#target_resource#" height="1px" width="1px" frameborder="0" scrolling="no"></iframe>';
    portlet.setHtml(html);
}
*/
function portlet_chrt_frame (portlet, column){
	
	var userid = nlapiGetUser();
	var roleid = nlapiGetContext().getRole();
	var closed = 0;
	
	if ((roleid == '-5' || roleid == '3' || roleid == '18' || roleid == '1085') || closed == 0) {
    	var html = '<iframe name="chartsFrame" id="chartsFrame" src="/app/site/hosting/scriptlet.nl?script=1306&deploy=1" height="540px" width="850px" frameborder="0" scrolling="no"></iframe>';
    }
    else{
    	var html = 'This module is closed for modifications. Sorry for the inconvenience. Please come back later.';
    }
    portlet.setTitle('Management KPI');
    portlet.setHtml(html);
    
}

