nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Lost_Churns_Grid.js
//	Script Name:	CLGX_SL_Lost_Churns_Grid
//	Script Id:		customscript_clgx_sl_lost_churns_details
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		07/02/2014
//	Includes:		CLGX_LIB_Global.js
//-------------------------------------------------------------------------------------------------

function suitelet_revenue_details (request, response){
    try {

        var marketid = request.getParameter('market');
        var type = request.getParameter('type');
        var typeLabel = request.getParameter('typelabel');
        var putMonth = request.getParameter('month');
        var product=request.getParameter('product');
        var productLabel=request.getParameter('productlabel');
        var script=request.getParameter('scriptid');
        var marketName=clgx_return_market_name(marketid);
        if(product!=0)
        {
            var typeTitle=productLabel;
        }
        else
        {
            var typeTitle=typeLabel;
        }
        if(marketid != '0'){

            var title = marketName + ' - ' + typeTitle;
        }
        else
        {
            var title ='All - ' + typeTitle;
        }

        var objFile = nlapiLoadFile(2102491);
        var html = objFile.getValue();
        html = html.replace(new RegExp('{revenueJSON}','g'), dataRevenue(marketid, type, putMonth,product,script));
        html = html.replace(new RegExp('{title}','g'), title);

        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }

        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function dataRevenue (marketid, type, putMonth,product,script) {




    var stReturn = 'var dataRevenue =  [';
    if((script==1)&&((type=='newmrr')||(type=='up')||(type=='price')))
    {
        stReturn +=getNetRevenue1(marketid, type, putMonth);
    }
    if((script==2)&&((type=='newmrr')||(type=='up')||(type=='price')||(type=='disco')||(type=='down')||(type=='dec')))
    {
        stReturn +=getNetRevenue(marketid, type, putMonth);
    }
    if((script==1)&&((type=='credit')))
    {
        stReturn +=getSeriesCSVCM(marketid,  putMonth);
    }
    if((script==2)&&((type=='credit')))
    {
        stReturn +=getSeriesCSVCM(marketid,  putMonth);
    }
    if((script==1)&&(type=='us'))

    {
        if(putMonth==0)
        {
            stReturn +=getNetRevenueUsage(marketid,"customsearch_clgx_so_noinvgridpowus_c",0,-1,type);
        }
        if(putMonth==-1)
        {
            stReturn +=getNetRevenueUsage(marketid,"customsearch_clgx_so_noinvgridpowu_1",-1,-2,type);
        }
        if(putMonth==-2)
        {
            stReturn +=getNetRevenueUsage(marketid,"customsearch_clgx_so_noinvgridpowus_2",-2,-3,type);
        }
        if(putMonth==-3)
        {
            stReturn +=getNetRevenueUsage(marketid,"customsearch_clgx_so_noinvgridpowus_3",-3,-4,type);
        }
        if(putMonth==-4)
        {
            stReturn +=getNetRevenueUsage(marketid,"customsearch_clgx_so_noinvgridpowus_4",-4,-5,type);
        }
        if(putMonth==-5)
        {
            stReturn +=getNetRevenueUsage(marketid,"customsearch_clgx_so_noinvgridpowus_5",-5,-6,type);
        }


    }
    if((script==1)&&(type=='pr'))

    {
        if(putMonth==0)
        {
            stReturn +=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_c",0,-1,type);
        }
        if(putMonth==-1)
        {
            stReturn +=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_1",-1,-2,type);
        }
        if(putMonth==-2)
        {
            stReturn +=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_2",-2,-3,type);
        }
        if(putMonth==-3)
        {
            stReturn +=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_3",-3,-4,type);
        }
        if(putMonth==-4)
        {
            stReturn +=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_4",-4,-5,type);
        }
        if(putMonth==-5)
        {
            stReturn +=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_5",-5,-6,type);
        }


    }
    if((script==2)&&(type=='us'))

    {
        if(putMonth==0)
        {
            stReturn +=getNetRevenueUsage(marketid,"customsearch_clgx_so_noinvgridpowus_c",0,-1,type);
        }
        if(putMonth==-1)
        {
            stReturn +=getNetRevenueUsage(marketid,"customsearch_clgx_so_noinvgridpowu_1",-1,-2,type);
        }
        if(putMonth==-2)
        {
            stReturn +=getNetRevenueUsage(marketid,"customsearch_clgx_so_noinvgridpowus_2",-2,-3,type);
        }
        if(putMonth==-3)
        {
            stReturn +=getNetRevenueUsage(marketid,"customsearch_clgx_so_noinvgridpowus_3",-3,-4,type);
        }
        if(putMonth==-4)
        {
            stReturn +=getNetRevenueUsage(marketid,"customsearch_clgx_so_noinvgridpowus_4",-4,-5,type);
        }
        if(putMonth==-5)
        {
            stReturn +=getNetRevenueUsage(marketid,"customsearch_clgx_so_noinvgridpowus_5",-5,-6,type);
        }


    }
    if((script==2)&&(type=='pr'))

    {
        if(putMonth==0)
        {
            stReturn +=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_c",0,-1,type);
        }
        if(putMonth==-1)
        {
            stReturn +=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_1",-1,-2,type);
        }
        if(putMonth==-2)
        {
            stReturn +=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_2",-2,-3,type);
        }
        if(putMonth==-3)
        {
            stReturn +=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_3",-3,-4,type);
        }
        if(putMonth==-4)
        {
            stReturn +=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_4",-4,-5,type);
        }
        if(putMonth==-5)
        {
            stReturn +=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_5",-5,-6,type);
        }


    }
    if(script==3)
    {
        stReturn +=getProductRevenue(marketid,'customsearch_clgx_inv_revenuegr_all',putMonth,product);
    }

    var strLen = stReturn.length;
    stReturn = stReturn.slice(0,strLen-1);
    stReturn += '];';
    return stReturn;
}
//Prior Month
function getSeriesCSVPR(marketid,search,curr, bf,cat){

    var arrFilters=new Array();
    var arrColumns=new Array();
    if(marketid!=0)
    {
        if(marketid=='25')
        {
            //Montreal
            var marketidlist=["5", "8", "9", "10", "11", "12", "27"];

        }
        if(marketid=='23')
        {
            //Toronto
            var marketidlist=["6", "13", "15"];

        }
        if(marketid=='26')
        {
            //Vancouver
            var marketidlist=["28", "7"];

        }
        if(marketid=='22')
        {
            //Minneapolis
            var marketidlist=["16", "35"];
        }
        if(marketid=='29')
        {
            //Jacksonville
            var marketidlist=["31"];
        }
        if(marketid=='20')
        {
            //Dallas
            var marketidlist=["2", "17"];
        }
        if(marketid=='33')
        {
            //Columbus
            var marketidlist=["38", "34","39"];
        }
        if(marketid=='41')
        {
            //Lakeland
            var marketidlist=["42"];
        }
        if(marketid=='43')
        {
            //Jacksonville2
            var marketidlist=["40"];
        }

        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",marketidlist));


    }
    var arrDates  = getDateRange(curr);
    var arrDates1  = getDateRange(bf);
    var startDate = arrDates1[0];
    var endDate  = arrDates[1];
    var stringCSV='';
    var  stReturn='';
    var priTotalMT=0;
    arrFilters.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));

    var searchInvoices = nlapiLoadSearch('transaction',search);
    searchInvoices.addColumns(arrColumns);
    searchInvoices.addFilters(arrFilters);
    var resultINV = searchInvoices.runSearch();
    resultINV.forEachResult(function(searchResult) {
        var columns = searchResult.getAllColumns();
        var totalAmountCurr=parseFloat(searchResult.getValue(columns[2]));
        var totalAmount1MTHAgo=parseFloat(searchResult.getValue(columns[3]));
        var customerName=searchResult.getText(columns[1]);
        var customerID=searchResult.getValue(columns[1]);
        var locationInv=searchResult.getText(columns[4]);
        var currencyInv=searchResult.getValue(columns[5]);
        var productCat=searchResult.getText(columns[6]);
        var currencyInvArray=currencyInv.split(',');
        var splitdate=endDate.split('/');
        var currMTH=splitdate[0];
        var splitdate1=startDate.split('/');
        var beforeMTH=splitdate1[0];
        if((parseFloat(totalAmountCurr)!=0)||(parseFloat(totalAmount1MTHAgo)!=0))
        {
            var totalpowerUsage=parseFloat(totalAmountCurr)-parseFloat(totalAmount1MTHAgo);


            stReturn += '\n{"customer":"' + customerName +
                '","type":"' +  'Prior Month' +
                '","categoryrevenue":"' +  'Pro Rate' +
                '", "date":"' +  startDate +
                '","customerid":' +  customerID +
                ', "amount":' +parseFloat(totalpowerUsage).toFixed(2)+
                ',"market":"' +  locationInv +
                '", "product":"' +  productCat +
                '", "currency":"' +  currencyInvArray[0]+
                '" },';
        }
        return true; // return true to keep iterating

    });
    return stReturn;
}
function getSeriesCSVCM(marketid,putMonth){

    var arrFilters=new Array();
    var arrColumns=new Array();
    if(marketid!=0)
    {
        if(marketid=='25')
        {
            //Montreal
            var marketidlist=["5", "8", "9", "10", "11", "12", "27"];

        }
        if(marketid=='23')
        {
            //Toronto
            var marketidlist=["6", "13", "15"];

        }
        if(marketid=='26')
        {
            //Vancouver
            var marketidlist=["28", "7"];

        }
        if(marketid=='22')
        {
            //Minneapolis
            var marketidlist=["16", "35"];
        }
        if(marketid=='29')
        {
            //Jacksonville
            var marketidlist=["31"];
        }
        if(marketid=='20')
        {
            //Dallas
            var marketidlist=["2", "17"];
        }
        if(marketid=='33')
        {
            //Columbus
            var marketidlist=["38", "34","39"];
        }
        if(marketid=='41')
        {
            //Lakeland
            var marketidlist=["42"];
        }
        if(marketid=='43')
        {
            //Jacksonville2
            var marketidlist=["40"];
        }

        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",marketidlist));


    }
    var arrDates  = getDateRange(0);
    var arrDates1  = getDateRange(-6);
    var startDate = arrDates1[0];
    var endDate  = arrDates[1];
    arrFilters.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    var searchInvoices = nlapiSearchRecord('transaction', "customsearch_clgx_so_memogr_all", arrFilters, arrColumns);
    var stReturn='';
    if(searchInvoices!=null)
    {
        for ( var i = 0;searchInvoices  != null && i < searchInvoices.length; i++ ) {
            var searchInv = searchInvoices [i];
            var columns = searchInv.getAllColumns();
            var totalAmountCurr=parseFloat(searchInv.getValue(columns[9]));
            var totalAmountBF=parseFloat(searchInv.getValue(columns[10]));
            var totalAmount2=parseFloat(searchInv.getValue(columns[11]));
            var totalAmount3=parseFloat(searchInv.getValue(columns[12]));
            var totalAmount4=parseFloat(searchInv.getValue(columns[13]));
            var totalAmount5=parseFloat(searchInv.getValue(columns[14]));
            var totalAmount6=parseFloat(searchInv.getValue(columns[15]));
            var categoryInv=searchInv.getText(columns[16]);
            var customerName=searchInv.getValue(columns[1]);
            var customerID=searchInv.getValue(columns[0]);
            var locationInv=searchInv.getText(columns[3]);
            var internalID=searchInv.getValue(columns[4]);
            var dateInv=searchInv.getValue(columns[5]);
            var amountInv=searchInv.getValue(columns[6]);
            var currencyInv=searchInv.getValue(columns[7]);
            var itemCategory=searchInv.getValue(columns[8]);
            var internalIDArray=internalID.split(',');
            var dateInvArray=dateInv.split(',');
            var amountInvArray=amountInv.split(',');
            var currencyInvArray=currencyInv.split(',');
            var itemCategoryArray=itemCategory.split(',');
            var splitdate=endDate.split('/');
            var currMTH=splitdate[0];
            var splitdate1=startDate.split('/');
            var beforeMTH=splitdate1[0];
            var totalAmCM=parseFloat(totalAmountCurr)-parseFloat(totalAmountBF);
            var arrDates_1  = getDateRange(-1);
            var startDate_1 = arrDates_1[0];
            var totalAm1=parseFloat(totalAmountBF)-parseFloat(totalAmount2);
            var arrDates_2  = getDateRange(-2);
            var startDate_2 = arrDates_2[0];
            var totalAm2=parseFloat(totalAmount2)-parseFloat(totalAmount3);
            var arrDates_3  = getDateRange(-3);
            var startDate_3 = arrDates_3[0];
            var totalAm3=parseFloat(totalAmount3)-parseFloat(totalAmount4);
            var arrDates_4  = getDateRange(-4);
            var startDate_4 = arrDates_4[0];
            var totalAm4=parseFloat(totalAmount4)-parseFloat(totalAmount5);
            var arrDates_5  = getDateRange(-5);
            var startDate_5 = arrDates_5[0];
            var totalAm5=parseFloat(totalAmount5)-parseFloat(totalAmount6);
            var arrDates_6  = getDateRange(-6);
            var startDate_6 = arrDates_6[0];
            if(parseFloat(totalAmCM)!=0)
            {
                if(putMonth==0)
                {
                    stReturn += '\n{"customer":"' + customerName +
                        '","type":"' +  'Credit Memo' +
                        '","categoryrevenue":"' +  'Credit Memo' +
                        '", "date":"' +  startDate_1 +
                        '","customerid":' +  customerID +
                        ', "amount":' +parseFloat(totalAmCM).toFixed(2)+
                        ',"market":"' +  locationInv +
                        '", "product":"' +  categoryInv +
                        '", "currency":"' +  currencyInvArray[0]+
                        '" },';
                }
            }
            if(parseFloat(totalAm1)!=0)
            {
                if(putMonth==-1)
                {
                    stReturn += '\n{"customer":"' + customerName +
                        '","type":"' +  'Credit Memo' +
                        '","categoryrevenue":"' +  'Credit Memo' +
                        '", "date":"' +  startDate_2 +
                        '","customerid":' +  customerID +
                        ', "amount":' +parseFloat(totalAm1).toFixed(2)+
                        ',"market":"' +  locationInv +
                        '", "product":"' +  categoryInv +
                        '", "currency":"' +  currencyInvArray[0]+
                        '" },';
                }
            }
            if(parseFloat(totalAm2)!=0)
            {
                if(putMonth==-2)
                {
                    stReturn += '\n{"customer":"' + customerName +
                        '","type":"' +  'Credit Memo' +
                        '","categoryrevenue":"' +  'Credit Memo' +
                        '", "date":"' +  startDate_3 +
                        '","customerid":' +  customerID +
                        ', "amount":' +parseFloat(totalAm2).toFixed(2)+
                        ',"market":"' +  locationInv +
                        '", "product":"' +  categoryInv +
                        '", "currency":"' +  currencyInvArray[0]+
                        '" },';
                }   }
            if(parseFloat(totalAm3)!=0)
            {
                if(putMonth==-3)
                {
                    stReturn += '\n{"customer":"' + customerName +
                        '","type":"' +  'Credit Memo' +
                        '","categoryrevenue":"' +  'Credit Memo' +
                        '", "date":"' +  startDate_4 +
                        '","customerid":' +  customerID +
                        ', "amount":' +parseFloat(totalAm3).toFixed(2)+
                        ',"market":"' +  locationInv +
                        '", "product":"' +  categoryInv +
                        '", "currency":"' +  currencyInvArray[0]+
                        '" },';
                }     }
            if(parseFloat(totalAm4)!=0)
            {
                if(putMonth==-4)
                {
                    stReturn += '\n{"customer":"' + customerName +
                        '","type":"' +  'Credit Memo' +
                        '","categoryrevenue":"' +  'Credit Memo' +
                        '", "date":"' +  startDate_5 +
                        '","customerid":' +  customerID +
                        ', "amount":' +parseFloat(totalAm4).toFixed(2)+
                        ',"market":"' +  locationInv +
                        '", "product":"' +  categoryInv +
                        '", "currency":"' +  currencyInvArray[0]+
                        '" },';
                } }
            if(parseFloat(totalAm5)!=0)
            {
                if(putMonth==-5)
                {
                    stReturn += '\n{"customer":"' + customerName +
                        '","type":"' +  'Credit Memo' +
                        '","categoryrevenue":"' +  'Credit Memo' +
                        '", "date":"' +  startDate_6 +
                        '","customerid":' +  customerID +
                        ', "amount":' +parseFloat(totalAm5).toFixed(2)+
                        ',"market":"' +  locationInv +
                        '", "product":"' +  categoryInv +
                        '", "currency":"' +  currencyInvArray[0]+
                        '" },';
                }
            }
        }

    }

    return stReturn;
}

function getNetRevenue(marketid, type, putMonth){
    var arrFilters=new Array();
    var arrColumns=new Array();
    var arrFiltersNewMRR=new Array();
    var arrColumnsNewMRR=new Array();
    if(marketid!=0)
    {
        if(marketid=='25')
        {
            //Montreal
            var marketidlist=["5", "8", "9", "10", "11", "12", "27"];

        }
        if(marketid=='23')
        {
            //Toronto
            var marketidlist=["6", "13", "15"];

        }
        if(marketid=='26')
        {
            //Vancouver
            var marketidlist=["28", "7"];

        }
        if(marketid=='22')
        {
            //Minneapolis
            var marketidlist=["16", "35"];
        }
        if(marketid=='29')
        {
            //Jacksonville
            var marketidlist=["31"];
        }
        if(marketid=='20')
        {
            //Dallas
            var marketidlist=["2", "17"];
        }
        if(marketid=='33')
        {
            //Columbus
            var marketidlist=["38", "34","39"];
        }
        if(marketid=='41')
        {
            //Lakeland
            var marketidlist=["42"];
        }
        if(marketid=='43')
        {
            //Jacksonville2
            var marketidlist=["40"];
        }

        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",marketidlist));
        arrFiltersNewMRR.push(new nlobjSearchFilter("location",null,"anyof",marketidlist));


    }
    if(putMonth==0)
    {
        var arrDates  = getDateRange(0);
        var arrDates1  = getDateRange(-1);
    }
    if(putMonth==-1)
    {
        var arrDates  = getDateRange(-1);
        var arrDates1  = getDateRange(-2);
    }
    if(putMonth==-2)
    {
        var arrDates  = getDateRange(-2);
        var arrDates1  = getDateRange(-3);
    }
    if(putMonth==-3)
    {
        var arrDates  = getDateRange(-3);
        var arrDates1  = getDateRange(-4);
    }
    if(putMonth==-4)
    {
        var arrDates  = getDateRange(-4);
        var arrDates1  = getDateRange(-5);
    }
    if(putMonth==-5)
    {
        var arrDates  = getDateRange(-5);
        var arrDates1  = getDateRange(-6);
    }

    var startDate = arrDates1[0];
    var endDate  = arrDates[1];
    arrFilters.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    arrFiltersNewMRR.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    var customerMRRArray=new Array();
    var locationMRRArray=new Array();
    var totalAmountCurrMRRArray=new Array();
    var totalAmountBFMRRArray=new Array();
    var totalAmount1MRRArray=new Array();
    var totalAmount2MRRArray=new Array();
    var totalAmount3MRRArray=new Array();
    var totalAmount4MRRArray=new Array();
    var totalAmount5MRRArray=new Array();
    // if((type=='newmrr')||(type=='up'))
    // {
    var searchInvoicesNewMRR = nlapiLoadSearch('transaction', 'customsearch_clgx_inv_revenuegr_all_nwm');
    searchInvoicesNewMRR.addColumns(arrColumnsNewMRR);
    searchInvoicesNewMRR.addFilters(arrFiltersNewMRR);
    var searchInvNewMRR = searchInvoicesNewMRR.runSearch();
    searchInvNewMRR.forEachResult(function(searchResult) {
        var columnsMRR = searchResult.getAllColumns();
        var customerMRR=searchResult.getValue(columnsMRR[1]);
        var locationMRR=searchResult.getText(columnsMRR[2]);
        var totalAmountCurrMRR=parseFloat(searchResult.getValue(columnsMRR[3]));
        var totalAmountBFMRR=parseFloat(searchResult.getValue(columnsMRR[4]));
        var totalAmount1MRR=parseFloat(searchResult.getValue(columnsMRR[5]));
        var totalAmount2MRR=parseFloat(searchResult.getValue(columnsMRR[6]));
        var totalAmount3MRR=parseFloat(searchResult.getValue(columnsMRR[7]));
        var totalAmount4MRR=parseFloat(searchResult.getValue(columnsMRR[8]));
        var totalAmount5MRR=parseFloat(searchResult.getValue(columnsMRR[9]));
        customerMRRArray.push(customerMRR);
        locationMRRArray.push(customerMRR+';'+locationMRR);
        totalAmountCurrMRRArray.push(totalAmountCurrMRR);
        totalAmountBFMRRArray.push(totalAmountBFMRR);
        totalAmount1MRRArray.push(totalAmount1MRR);
        totalAmount2MRRArray.push(totalAmount2MRR);
        totalAmount3MRRArray.push(totalAmount3MRR);
        totalAmount4MRRArray.push(totalAmount4MRR);
        totalAmount5MRRArray.push(totalAmount5MRR);

        return true; // return true to keep iterating

    });
    // }
    var stReturn ='';
    var searchInvoices = nlapiLoadSearch('transaction','customsearch_clgx_inv_revenuegr_all');
    searchInvoices.addColumns(arrColumns);
    searchInvoices.addFilters(arrFilters);
    var resultINV = searchInvoices.runSearch();
    resultINV.forEachResult(function(searchResult) {
        var columns = searchResult.getAllColumns();
        var customerID=searchResult.getValue(columns[0]);
        var customerName=searchResult.getValue(columns[1]);
        var locationInv=searchResult.getText(columns[2]);
        var currMTHAmount=searchResult.getValue(columns[3]);
        var bfMTHAmount=searchResult.getValue(columns[4]);
        var MTH1AgoAmount=searchResult.getValue(columns[5]);
        var MTH2AgoAmount=searchResult.getValue(columns[6]);
        var MTH3AgoAmount=searchResult.getValue(columns[7]);
        var MTH4AgoAmount=searchResult.getValue(columns[8]);
        var MTH5AgoAmount=searchResult.getValue(columns[9]);
        var currMTHQTY=searchResult.getValue(columns[10]);
        var bfMTHQTY=searchResult.getValue(columns[11]);
        var MTH1AgoQTY=searchResult.getValue(columns[12]);
        var MTH2AgoQTY=searchResult.getValue(columns[13]);
        var MTH3AgoQTY=searchResult.getValue(columns[14]);
        var MTH4AgoQTY=searchResult.getValue(columns[15]);
        var MTH5AgoQTY=searchResult.getValue(columns[16]);
        var categoryInv1=searchResult.getText(columns[17]);
        var currencyINV1=searchResult.getText(columns[18]);
        var arrDates_1  = getDateRange(-1);
        var startDate_1 = arrDates_1[0];
        var arrDates_2  = getDateRange(-2);
        var startDate_2 = arrDates_2[0];
        var arrDates_3  = getDateRange(-3);
        var startDate_3 = arrDates_3[0];
        var arrDates_4  = getDateRange(-4);
        var startDate_4 = arrDates_4[0];
        var arrDates_5  = getDateRange(-5);
        var startDate_5 = arrDates_5[0];
        var arrDates_6  = getDateRange(-6);
        var startDate_6 = arrDates_6[0];
        var index=0;
        var test=customerName+';'+locationInv;
        index=_.indexOf(locationMRRArray, test);
        if(putMonth==0){
            if((parseFloat(currMTHAmount)!=0)&&(parseFloat(bfMTHAmount)==0))
            {
                if((type=='newmrr')||(type=='up'))
                {
                    if(index>0)
                    {
                        if((parseFloat(totalAmountCurrMRRArray[index])!=0)&&(parseFloat(totalAmountBFMRRArray[index])==0))
                        {
                            if(type=='newmrr')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        else{
                            if(type=='up')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade MRR' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
            else{
                if(parseInt(currMTHQTY)>parseInt(bfMTHQTY)){

                    var totalamInvQ=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                    if(totalamInvQ>0)
                    {
                        if(type=='up')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Upgrade MRR' +
                                '", "date":"' +  startDate_1 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';

                        }
                    }


                }
                else if(parseFloat(currMTHAmount) > parseFloat(bfMTHAmount)){


                    var totalamInvQ1=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                    if(totalamInvQ1>0)
                    {
                        if(type=='price')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Price Increase' +
                                '", "date":"' +  startDate_1 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }}
        if(putMonth==-1){
            //2  MT AGO
            if((parseFloat(bfMTHAmount)!=0)&&(parseFloat(MTH1AgoAmount)==0))
            {
                if((type=='newmrr')||(type=='up'))
                {
                    if(index>0)
                    {
                        if((parseFloat(totalAmountBFMRRArray[index])!=0)&&(parseFloat(totalAmount1MRRArray[index])==0))
                        {
                            if(type=='newmrr')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        else{
                            if(type=='up')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade MRR' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
            else{
                if(parseInt(bfMTHQTY)>parseInt(MTH1AgoQTY)){

                    var totalamInvQ=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                    if(totalamInvQ>0)
                    {
                        if(type=='up')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Upgrade MRR' +
                                '", "date":"' +  startDate_2 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }


                }
                else if(parseFloat(bfMTHAmount) > parseFloat(MTH1AgoAmount)){


                    var totalamInvQ1=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                    if(totalamInvQ1>0)
                    {
                        if(type=='price')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Price Increase' +
                                '", "date":"' +  startDate_2 +
                                '","customerid":' +  customerID +
                                ', "amount":' + parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
        }
        if(putMonth==-2){
            //3 MT AGO
            if((parseFloat(MTH1AgoAmount)!=0)&&(parseFloat(MTH2AgoAmount)==0))
            {
                if((type=='newmrr')||(type=='up'))
                {
                    if(index>0)
                    {
                        if((parseFloat(totalAmount1MRRArray[index])!=0)&&(parseFloat(totalAmount2MRRArray[index])==0))
                        {
                            if(type=='newmrr')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' + parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        else
                        {   if(type=='up')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Upgrade MRR' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' + parseFloat(MTH1AgoAmount)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                        }
                    }
                }
            }
            else{
                if(parseInt(MTH1AgoQTY)>parseInt(MTH2AgoQTY)){

                    var totalamInvQ=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                    if(totalamInvQ>0)
                    {
                        if(type=='up')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Upgrade MRR' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' + parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }


                }
                else if(parseFloat(MTH1AgoAmount) > parseFloat(MTH2AgoAmount)){


                    var totalamInvQ1=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                    if(totalamInvQ1>0)
                    {
                        if(type=='price')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Price Increase' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' + parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
        }
        if(putMonth==-3){
//4 MT AGO
            if((parseFloat(MTH2AgoAmount)!=0)&&(parseFloat(MTH3AgoAmount)==0))
            {
                if((type=='newmrr')||(type=='up'))
                {
                    if(index>0)
                    {
                        if((parseFloat(totalAmount2MRRArray[index])!=0)&&(parseFloat(totalAmount3MRRArray[index])==0))
                        {
                            if(type=='newmrr')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +  parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        else{
                            if(type=='up')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade MRR' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +  parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
            else{
                if(parseInt(MTH2AgoQTY)>parseInt(MTH3AgoQTY)){

                    var totalamInvQ=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                    if(totalamInvQ>0)
                    {
                        if(type=='up')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Upgrade MRR' +
                                '", "date":"' +  startDate_4 +
                                '","customerid":' +  customerID +
                                ', "amount":' +  parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }


                }
                else if(parseFloat(MTH2AgoAmount) > parseFloat(MTH3AgoAmount)){


                    var totalamInvQ1=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                    if(totalamInvQ1>0)
                    {
                        if(type=='price')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Price Increase' +
                                '", "date":"' +  startDate_4 +
                                '","customerid":' +  customerID +
                                ', "amount":' +  parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
        }
        if(putMonth==-4){
            //5 MT AGO
            if((parseFloat(MTH3AgoAmount)!=0)&&(parseFloat(MTH4AgoAmount)==0))
            {
                if((type=='newmrr')||(type=='up'))
                {
                    if(index>0)
                    {
                        if((parseFloat(totalAmount3MRRArray[index])!=0)&&(parseFloat(totalAmount4MRRArray[index])==0))
                        {
                            if(type=='newmrr')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +  parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        else{
                            if(type=='up')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade MRR' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +  parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
            else{
                if(parseInt(MTH3AgoQTY)>parseInt(MTH4AgoQTY)){

                    var totalamInvQ=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                    if(totalamInvQ>0)
                    {
                        if(type=='up')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Upgrade MRR' +
                                '", "date":"' +  startDate_5 +
                                '","customerid":' +  customerID +
                                ', "amount":' +  parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }


                }
                else if(parseFloat(MTH3AgoAmount) > parseFloat(MTH4AgoAmount)){


                    var totalamInvQ1=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                    if(totalamInvQ1>0)
                    {
                        if(type=='up')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Price Increase' +
                                '", "date":"' +  startDate_5 +
                                '","customerid":' +  customerID +
                                ', "amount":' +  parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
        }
        if(putMonth==-5){
            //6 MT AGO
            if((parseFloat(MTH4AgoAmount)!=0)&&(parseFloat(MTH5AgoAmount)==0))
            {
                if((type=='newmrr')||(type=='up'))
                {
                    if(index>0)
                    {
                        if((parseFloat(totalAmount4MRRArray[index])!=0)&&(parseFloat(totalAmount5MRRArray[index])==0))
                        {
                            if(type=='newmrr')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +  parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        else{
                            if(type=='up')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade MRR' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +  parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
            else{
                if(parseInt(MTH4AgoQTY)>parseInt(MTH5AgoQTY)){

                    var totalamInvQ=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                    if(totalamInvQ>0)
                    {
                        if(type=='up')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Upgrade MRR' +
                                '", "date":"' +  startDate_6 +
                                '","customerid":' +  customerID +
                                ', "amount":' +  parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }


                }
                else if(parseFloat(MTH4AgoAmount) > parseFloat(MTH5AgoAmount)){


                    var totalamInvQ1=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                    if(totalamInvQ1>0)
                    {
                        if(type=='price')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Price Increase' +
                                '", "date":"' +  startDate_6 +
                                '","customerid":' +  customerID +
                                ', "amount":' +  parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
        }
        //   return true; // return true to keep iterating

        //});
        //CHURN





        //1  MT AGO
        if(putMonth==0){
            if((parseFloat(currMTHAmount)==0)&&(parseFloat(bfMTHAmount)!=0))
            {
                if(index>0)
                {
                    if((parseFloat(totalAmountCurrMRRArray[index])==0)&&(parseFloat(totalAmountBFMRRArray[index])!=0))
                    {
                        if(type=='disco')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Disconnect' +
                                '", "date":"' +  startDate_1 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat((-1))*parseFloat(bfMTHAmount)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    else
                    {
                        if(type=='down')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Downgrade' +
                                '", "date":"' +  startDate_1 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat((-1))*parseFloat(bfMTHAmount)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
            else{
                if(parseInt(currMTHQTY)<parseInt(bfMTHQTY)){

                    var totalamInvQ=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                    if(totalamInvQ<0)
                    {
                        if(type=='down')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Downgrade' +
                                '", "date":"' +  startDate_1 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
                else if(parseFloat(currMTHAmount) < parseFloat(bfMTHAmount)){


                    var totalamInvQ1=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                    if(totalamInvQ1>0)
                    {
                        totalamInvQ1=totalamInvQ1*(-1);
                    }
                    if(type=='dec')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Churn' +
                            '","categoryrevenue":"' +  'Decrease' +
                            '", "date":"' +  startDate_1 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat(totalamInvQ1)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
            }
        }
        //2  MT AGO
        if(putMonth==-1){
            if((parseFloat(bfMTHAmount)==0)&&(parseFloat(MTH1AgoAmount)!=0))
            {
                if(index>0)
                {
                    if((parseFloat(totalAmountBFMRRArray[index])==0)&&(parseFloat(totalAmount1MRRArray[index])!=0))
                    {
                        if(type=='disco')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Disconnect' +
                                '", "date":"' +  startDate_2 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat((-1))*parseFloat(MTH1AgoAmount)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    else{
                        if(type=='down')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Downgrade' +
                                '", "date":"' +  startDate_2 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat((-1))*parseFloat(MTH1AgoAmount)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }

                    }
                }
            }
            else{
                if(parseInt(bfMTHQTY)<parseInt(MTH1AgoQTY)){

                    var totalamInvQ=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                    if(totalamInvQ<0)
                    {

                        if(type=='down')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Downgrade' +
                                '", "date":"' +  startDate_2 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
                else if(parseFloat(bfMTHAmount) < parseFloat( MTH1AgoAmount)){


                    var totalamInvQ1=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                    if(totalamInvQ1>0)
                    {
                        totalamInvQ1=totalamInvQ1*(-1);
                    }
                    if(type=='dec')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Churn' +
                            '","categoryrevenue":"' +  'Decrease' +
                            '", "date":"' +  startDate_2 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat(totalamInvQ1)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
            }
        }
        if(putMonth==-2){

            //3 MT AGO
            if((parseFloat(MTH1AgoAmount)==0)&&(parseFloat(MTH2AgoAmount)!=0))
            {
                if(index>0)
                {
                    if((parseFloat(totalAmount1MRRArray[index])==0)&&(parseFloat(totalAmount2MRRArray[index])!=0))
                    {
                        if(type=='disco')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Disconnect' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat((-1))*parseFloat(MTH2AgoAmount)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    else{
                        if(type=='down')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Downgrade' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat((-1))*parseFloat(MTH2AgoAmount)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
            else{
                if(parseInt(MTH1AgoQTY)<parseInt(MTH2AgoQTY)){

                    var totalamInvQ=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                    if(totalamInvQ<0)
                    {
                        if(type=='down')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Downgrade' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
                else if(parseFloat(MTH1AgoAmount) <parseFloat( MTH2AgoAmount)){


                    var totalamInvQ1=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                    if(totalamInvQ1>0)
                    {
                        totalamInvQ1=totalamInvQ1*(-1);
                    }
                    if(type=='dec')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Churn' +
                            '","categoryrevenue":"' +  'Decrease' +
                            '", "date":"' +  startDate_3 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat(totalamInvQ1)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
            }
        }
        if(putMonth==-3){
//4 MT AGO
            if((parseFloat(MTH2AgoAmount)==0)&&(parseFloat(MTH3AgoAmount)!=0))
            {
                if(index>0)
                {
                    if((parseFloat(totalAmount2MRRArray[index])==0)&&(parseFloat(totalAmount3MRRArray[index])!=0))
                    {
                        if(type=='disco')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Disconnect' +
                                '", "date":"' +  startDate_4 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat((-1))*parseFloat(MTH3AgoAmount)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    else{
                        if(type=='down')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Downgrade' +
                                '", "date":"' +  startDate_4 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat((-1))*parseFloat(MTH3AgoAmount)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
            else{
                if(parseInt(MTH2AgoQTY)<parseInt(MTH3AgoQTY)){

                    var totalamInvQ=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                    if(totalamInvQ<0)
                    {
                        if(type=='down')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Downgrade' +
                                '", "date":"' +  startDate_4 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
                else if(parseFloat(MTH2AgoAmount) < parseFloat(MTH3AgoAmount)){


                    var totalamInvQ1=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                    if(totalamInvQ1>0)
                    {
                        totalamInvQ1=totalamInvQ1*(-1);
                    }
                    if(type=='dec')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Churn' +
                            '","categoryrevenue":"' +  'Decrease' +
                            '", "date":"' +  startDate_4 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat(totalamInvQ1)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }  }
            }
        }
        if(putMonth==-4){
            //5 MT AGO
            if((parseFloat(MTH3AgoAmount)==0)&&(parseFloat(MTH4AgoAmount)!=0))
            {

                if(index>0)
                {
                    if((parseFloat(totalAmount3MRRArray[index])==0)&&(parseFloat(totalAmount4MRRArray[index])!=0))
                    {
                        if(type=='disco')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Disconnect' +
                                '", "date":"' +  startDate_5 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat((-1))*parseFloat(MTH4AgoAmount)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    else{
                        if(type=='down')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Downgrade' +
                                '", "date":"' +  startDate_5 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat((-1))*parseFloat(MTH4AgoAmount)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
            else{
                if(parseInt(MTH3AgoQTY)<parseInt(MTH4AgoQTY)){

                    var totalamInvQ=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                    if(totalamInvQ<0)
                    {
                        if(type=='down')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Downgrade' +
                                '", "date":"' +  startDate_5 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
                else if(parseFloat(MTH3AgoAmount) <parseFloat( MTH4AgoAmount)){


                    var totalamInvQ1=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                    if(totalamInvQ1>0)
                    {
                        totalamInvQ1=totalamInvQ1*(-1);
                    }
                    if(type=='dec')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Churn' +
                            '","categoryrevenue":"' +  'Decrease' +
                            '", "date":"' +  startDate_5 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat(totalamInvQ1)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
            }
        }
        if(putMonth==-5){
            //6 MT AGO
            if((parseFloat(MTH4AgoAmount)==0)&&(parseFloat(MTH5AgoAmount)!=0))
            {
                if(index>0)
                {
                    if((parseFloat(totalAmount4MRRArray[index])==0)&&(parseFloat(totalAmount5MRRArray[index])!=0))
                    {
                        if(type=='disco')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Disconnect' +
                                '", "date":"' +  startDate_6 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat((-1))*parseFloat(MTH5AgoAmount)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }else
                    {
                        if(type=='down')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Downgrade' +
                                '", "date":"' +  startDate_6 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat((-1))*parseFloat(MTH5AgoAmount)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
            else{
                if(parseInt(MTH4AgoQTY)<parseInt(MTH5AgoQTY)){

                    var totalamInvQ=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                    if(totalamInvQ<0)
                    {
                        if(type=='down')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Downgrade' +
                                '", "date":"' +  startDate_6 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
                else if(parseFloat(MTH4AgoAmount) < parseFloat(MTH5AgoAmount)){


                    var totalamInvQ1=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                    if(totalamInvQ1>0)
                    {
                        totalamInvQ1=totalamInvQ1*(-1);
                    }
                    if(type=='dec')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Churn' +
                            '","categoryrevenue":"' +  'Decrease' +
                            '", "date":"' +  startDate_6 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat(totalamInvQ1)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
            }
        }
        return true; // return true to keep iterating

    });

    return stReturn;
}

function getNetRevenue1(marketid, type, putMonth){
    var arrFilters=new Array();
    var arrColumns=new Array();
    var arrFiltersNewMRR=new Array();
    var arrColumnsNewMRR=new Array();
    if(marketid!=0)
    {
        if(marketid=='25')
        {
            //Montreal
            var marketidlist=["5", "8", "9", "10", "11", "12", "27"];

        }
        if(marketid=='23')
        {
            //Toronto
            var marketidlist=["6", "13", "15"];

        }
        if(marketid=='26')
        {
            //Vancouver
            var marketidlist=["28", "7"];

        }
        if(marketid=='22')
        {
            //Minneapolis
            var marketidlist=["16", "35"];
        }
        if(marketid=='29')
        {
            //Jacksonville
            var marketidlist=["31"];
        }
        if(marketid=='20')
        {
            //Dallas
            var marketidlist=["2", "17"];
        }
        if(marketid=='33')
        {
            //Columbus
            var marketidlist=["38", "34","39"];
        }
        if(marketid=='41')
        {
            //Lakeland
            var marketidlist=["42"];
        }
        if(marketid=='43')
        {
            //Jacksonville2
            var marketidlist=["40"];
        }

        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",marketidlist));
        arrFiltersNewMRR.push(new nlobjSearchFilter("location",null,"anyof",marketidlist));


    }
    if(putMonth==0)
    {
        var arrDates  = getDateRange(0);
        var arrDates1  = getDateRange(-1);
    }
    if(putMonth==-1)
    {
        var arrDates  = getDateRange(-1);
        var arrDates1  = getDateRange(-2);
    }
    if(putMonth==-2)
    {
        var arrDates  = getDateRange(-2);
        var arrDates1  = getDateRange(-3);
    }
    if(putMonth==-3)
    {
        var arrDates  = getDateRange(-3);
        var arrDates1  = getDateRange(-4);
    }
    if(putMonth==-4)
    {
        var arrDates  = getDateRange(-4);
        var arrDates1  = getDateRange(-5);
    }
    if(putMonth==-5)
    {
        var arrDates  = getDateRange(-5);
        var arrDates1  = getDateRange(-6);
    }

    var startDate = arrDates1[0];
    var endDate  = arrDates[1];
    arrFilters.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    arrFiltersNewMRR.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    var customerMRRArray=new Array();
    var locationMRRArray=new Array();
    var totalAmountCurrMRRArray=new Array();
    var totalAmountBFMRRArray=new Array();
    var totalAmount1MRRArray=new Array();
    var totalAmount2MRRArray=new Array();
    var totalAmount3MRRArray=new Array();
    var totalAmount4MRRArray=new Array();
    var totalAmount5MRRArray=new Array();
    if((type=='newmrr')||(type=='up'))
    {
        var searchInvoicesNewMRR = nlapiLoadSearch('transaction', 'customsearch_clgx_inv_revenuegr_all_nwm');
        searchInvoicesNewMRR.addColumns(arrColumnsNewMRR);
        searchInvoicesNewMRR.addFilters(arrFiltersNewMRR);
        var searchInvNewMRR = searchInvoicesNewMRR.runSearch();
        searchInvNewMRR.forEachResult(function(searchResult) {
            var columnsMRR = searchResult.getAllColumns();
            var customerMRR=searchResult.getValue(columnsMRR[1]);
            var locationMRR=searchResult.getText(columnsMRR[2]);
            var totalAmountCurrMRR=parseFloat(searchResult.getValue(columnsMRR[3]));
            var totalAmountBFMRR=parseFloat(searchResult.getValue(columnsMRR[4]));
            var totalAmount1MRR=parseFloat(searchResult.getValue(columnsMRR[5]));
            var totalAmount2MRR=parseFloat(searchResult.getValue(columnsMRR[6]));
            var totalAmount3MRR=parseFloat(searchResult.getValue(columnsMRR[7]));
            var totalAmount4MRR=parseFloat(searchResult.getValue(columnsMRR[8]));
            var totalAmount5MRR=parseFloat(searchResult.getValue(columnsMRR[9]));
            customerMRRArray.push(customerMRR);
            locationMRRArray.push(customerMRR+';'+locationMRR);
            totalAmountCurrMRRArray.push(totalAmountCurrMRR);
            totalAmountBFMRRArray.push(totalAmountBFMRR);
            totalAmount1MRRArray.push(totalAmount1MRR);
            totalAmount2MRRArray.push(totalAmount2MRR);
            totalAmount3MRRArray.push(totalAmount3MRR);
            totalAmount4MRRArray.push(totalAmount4MRR);
            totalAmount5MRRArray.push(totalAmount5MRR);

            return true; // return true to keep iterating

        });
    }
    var stReturn ='';
    var searchInvoices = nlapiLoadSearch('transaction','customsearch_clgx_inv_revenuegr_all');
    searchInvoices.addColumns(arrColumns);
    searchInvoices.addFilters(arrFilters);
    var resultINV = searchInvoices.runSearch();
    resultINV.forEachResult(function(searchResult) {
        var columns = searchResult.getAllColumns();
        var customerID=searchResult.getValue(columns[0]);
        var customerName=searchResult.getValue(columns[1]);
        var locationInv=searchResult.getText(columns[2]);
        var currMTHAmount=searchResult.getValue(columns[3]);
        var bfMTHAmount=searchResult.getValue(columns[4]);
        var MTH1AgoAmount=searchResult.getValue(columns[5]);
        var MTH2AgoAmount=searchResult.getValue(columns[6]);
        var MTH3AgoAmount=searchResult.getValue(columns[7]);
        var MTH4AgoAmount=searchResult.getValue(columns[8]);
        var MTH5AgoAmount=searchResult.getValue(columns[9]);
        var currMTHQTY=searchResult.getValue(columns[10]);
        var bfMTHQTY=searchResult.getValue(columns[11]);
        var MTH1AgoQTY=searchResult.getValue(columns[12]);
        var MTH2AgoQTY=searchResult.getValue(columns[13]);
        var MTH3AgoQTY=searchResult.getValue(columns[14]);
        var MTH4AgoQTY=searchResult.getValue(columns[15]);
        var MTH5AgoQTY=searchResult.getValue(columns[16]);
        var categoryInv1=searchResult.getText(columns[17]);
        var currencyINV1=searchResult.getText(columns[18]);
        var arrDates_1  = getDateRange(-1);
        var startDate_1 = arrDates_1[0];
        var arrDates_2  = getDateRange(-2);
        var startDate_2 = arrDates_2[0];
        var arrDates_3  = getDateRange(-3);
        var startDate_3 = arrDates_3[0];
        var arrDates_4  = getDateRange(-4);
        var startDate_4 = arrDates_4[0];
        var arrDates_5  = getDateRange(-5);
        var startDate_5 = arrDates_5[0];
        var arrDates_6  = getDateRange(-6);
        var startDate_6 = arrDates_6[0];
        var index=0;
        var test=customerName+';'+locationInv;
        index=_.indexOf(locationMRRArray, test);
        /* for ( var l = 0;customerMRRArray  != null && l < customerMRRArray.length; l++ ) {

         if(customerMRRArray[l]==customerName)
         {
         index=l;
         }
         }*/
        //1  MT AGO
        if(putMonth==0){
            if((parseFloat(currMTHAmount)!=0)&&(parseFloat(bfMTHAmount)==0))
            {
                if((type=='newmrr')||(type=='up'))
                {

                    //var emailSubject = 'July Revenue3';
                    // var emailBody="Values: "+locationMRRArray[0];
                    // nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null); // Send email to Catalina

                    if(index>0)
                    {
                        if((parseFloat(totalAmountCurrMRRArray[index])!=0)&&(parseFloat(totalAmountBFMRRArray[index])==0))
                        {
                            if(type=='newmrr')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        else{
                            if(type=='up')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade MRR' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
            else{
                if(parseInt(currMTHQTY)>parseInt(bfMTHQTY)){

                    var totalamInvQ=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                    if(totalamInvQ>0)
                    {
                        if(type=='up')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Upgrade MRR' +
                                '", "date":"' +  startDate_1 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';

                        }
                    }


                }
                else if(parseFloat(currMTHAmount) > parseFloat(bfMTHAmount)){


                    var totalamInvQ1=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                    if(totalamInvQ1>0)
                    {
                        if(type=='price')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Price Increase' +
                                '", "date":"' +  startDate_1 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }}
        if(putMonth==-1){
            //2  MT AGO
            if((parseFloat(bfMTHAmount)!=0)&&(parseFloat(MTH1AgoAmount)==0))
            {
                if((type=='newmrr')||(type=='up'))
                {
                    if(index>0)
                    {
                        if((parseFloat(totalAmountBFMRRArray[index])!=0)&&(parseFloat(totalAmount1MRRArray[index])==0))
                        {
                            if(type=='newmrr')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        else{
                            if(type=='up')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade MRR' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
            else{
                if(parseInt(bfMTHQTY)>parseInt(MTH1AgoQTY)){

                    var totalamInvQ=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                    if(totalamInvQ>0)
                    {
                        if(type=='up')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Upgrade MRR' +
                                '", "date":"' +  startDate_2 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }


                }
                else if(parseFloat(bfMTHAmount) > parseFloat(MTH1AgoAmount)){


                    var totalamInvQ1=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                    if(totalamInvQ1>0)
                    {
                        if(type=='price')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Price Increase' +
                                '", "date":"' +  startDate_2 +
                                '","customerid":' +  customerID +
                                ', "amount":' + parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
        }
        if(putMonth==-2){
            //3 MT AGO
            if((parseFloat(MTH1AgoAmount)!=0)&&(parseFloat(MTH2AgoAmount)==0))
            {
                if((type=='newmrr')||(type=='up'))
                {
                    if(index>0)
                    {
                        if((parseFloat(totalAmount1MRRArray[index])!=0)&&(parseFloat(totalAmount2MRRArray[index])==0))
                        {
                            if(type=='newmrr')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' + parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        else
                        {   if(type=='up')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Upgrade MRR' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' + parseFloat(MTH1AgoAmount)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                        }
                    }
                }
            }
            else{
                if(parseInt(MTH1AgoQTY)>parseInt(MTH2AgoQTY)){

                    var totalamInvQ=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                    if(totalamInvQ>0)
                    {
                        if(type=='up')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Upgrade MRR' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' + parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }


                }
                else if(parseFloat(MTH1AgoAmount) > parseFloat(MTH2AgoAmount)){


                    var totalamInvQ1=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                    if(totalamInvQ1>0)
                    {
                        if(type=='price')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Price Increase' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' + parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
        }
        if(putMonth==-3){
//4 MT AGO
            if((parseFloat(MTH2AgoAmount)!=0)&&(parseFloat(MTH3AgoAmount)==0))
            {
                if((type=='newmrr')||(type=='up'))
                {
                    if(index>0)
                    {
                        if((parseFloat(totalAmount2MRRArray[index])!=0)&&(parseFloat(totalAmount3MRRArray[index])==0))
                        {
                            if(type=='newmrr')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +  parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        else{
                            if(type=='up')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade MRR' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +  parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
            else{
                if(parseInt(MTH2AgoQTY)>parseInt(MTH3AgoQTY)){

                    var totalamInvQ=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                    if(totalamInvQ>0)
                    {
                        if(type=='up')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Upgrade MRR' +
                                '", "date":"' +  startDate_4 +
                                '","customerid":' +  customerID +
                                ', "amount":' +  parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }


                }
                else if(parseFloat(MTH2AgoAmount) > parseFloat(MTH3AgoAmount)){


                    var totalamInvQ1=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                    if(totalamInvQ1>0)
                    {
                        if(type=='price')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Price Increase' +
                                '", "date":"' +  startDate_4 +
                                '","customerid":' +  customerID +
                                ', "amount":' +  parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
        }
        if(putMonth==-4){
            //5 MT AGO
            if((parseFloat(MTH3AgoAmount)!=0)&&(parseFloat(MTH4AgoAmount)==0))
            {
                if((type=='newmrr')||(type=='up'))
                {
                    if(index>0)
                    {
                        if((parseFloat(totalAmount3MRRArray[index])!=0)&&(parseFloat(totalAmount4MRRArray[index])==0))
                        {
                            if(type=='newmrr')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +  parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        else{
                            if(type=='up')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade MRR' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +  parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
            else{
                if(parseInt(MTH3AgoQTY)>parseInt(MTH4AgoQTY)){

                    var totalamInvQ=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                    if(totalamInvQ>0)
                    {
                        if(type=='up')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Upgrade MRR' +
                                '", "date":"' +  startDate_5 +
                                '","customerid":' +  customerID +
                                ', "amount":' +  parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }


                }
                else if(parseFloat(MTH3AgoAmount) > parseFloat(MTH4AgoAmount)){


                    var totalamInvQ1=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                    if(totalamInvQ1>0)
                    {
                        if(type=='up')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Price Increase' +
                                '", "date":"' +  startDate_5 +
                                '","customerid":' +  customerID +
                                ', "amount":' +  parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
        }
        if(putMonth==-5){
            //6 MT AGO
            if((parseFloat(MTH4AgoAmount)!=0)&&(parseFloat(MTH5AgoAmount)==0))
            {
                if((type=='newmrr')||(type=='up'))
                {
                    if(index>0)
                    {
                        if((parseFloat(totalAmount4MRRArray[index])!=0)&&(parseFloat(totalAmount5MRRArray[index])==0))
                        {
                            if(type=='newmrr')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +  parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        else{
                            if(type=='up')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade MRR' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +  parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
            else{
                if(parseInt(MTH4AgoQTY)>parseInt(MTH5AgoQTY)){

                    var totalamInvQ=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                    if(totalamInvQ>0)
                    {
                        if(type=='up')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Upgrade MRR' +
                                '", "date":"' +  startDate_6 +
                                '","customerid":' +  customerID +
                                ', "amount":' +  parseFloat(totalamInvQ)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }


                }
                else if(parseFloat(MTH4AgoAmount) > parseFloat(MTH5AgoAmount)){


                    var totalamInvQ1=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                    if(totalamInvQ1>0)
                    {
                        if(type=='price')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Price Increase' +
                                '", "date":"' +  startDate_6 +
                                '","customerid":' +  customerID +
                                ', "amount":' +  parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
        }

        return true; // return true to keep iterating

    });

    return stReturn;
}
function getNetRevenueUsage(marketid,search,curr, bf,type){

    var arrFilters=new Array();
    var arrColumns=new Array();
    var stReturn ='';
    if(marketid!=0)
    {
        if(marketid=='25')
        {
            //Montreal
            var marketidlist=["5", "8", "9", "10", "11", "12", "27"];

        }
        if(marketid=='23')
        {
            //Toronto
            var marketidlist=["6", "13", "15"];

        }
        if(marketid=='26')
        {
            //Vancouver
            var marketidlist=["28", "7"];

        }
        if(marketid=='22')
        {
            //Minneapolis
            var marketidlist=["16", "35"];
        }
        if(marketid=='29')
        {
            //Jacksonville
            var marketidlist=["31"];
        }
        if(marketid=='20')
        {
            //Dallas
            var marketidlist=["2", "17"];
        }
        if(marketid=='33')
        {
            //Columbus
            var marketidlist=["38", "34","39"];
        }
        if(marketid=='41')
        {
            //Lakeland
            var marketidlist=["42"];
        }
        if(marketid=='43')
        {
            //Jacksonville2
            var marketidlist=["40"];
        }

        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",marketidlist));


    }
    var arrDates  = getDateRange(curr);
    var arrDates1  = getDateRange(bf);
    var startDate = arrDates1[0];
    var endDate  = arrDates[1];
    arrFilters.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    var searchInvoices = nlapiSearchRecord('transaction', search, arrFilters, arrColumns);
    var stringCSV='';
    var stringseries='';
    var powTotalMT=0;
    if(searchInvoices!=null)
    {
        for ( var i = 0;searchInvoices  != null && i < searchInvoices.length; i++ ) {
            var searchInv = searchInvoices [i];
            var columns = searchInv.getAllColumns();
            var totalAmountCurr=parseFloat(searchInv.getValue(columns[2]));
            var totalAmount1MTHAgo=parseFloat(searchInv.getValue(columns[3]));
            var totalQtyCurr=searchInv.getValue(columns[4]);
            var totalQty1MTHAgo=searchInv.getValue(columns[5]);
            var customerID=searchInv.getValue(columns[0]);
            var customerName=searchInv.getText(columns[1]);
            var locationInv=searchInv.getText(columns[6]);
            var internalID=searchInv.getValue(columns[7]);
            var dateInv=searchInv.getValue(columns[8]);
            var amountInv=searchInv.getValue(columns[9]);
            var currencyInv=searchInv.getValue(columns[10]);
            var categoryInv=searchInv.getText(columns[11]);
            var internalIDArray=internalID.split(',');
            var dateInvArray=dateInv.split(',');
            var amountInvArray=amountInv.split(',');
            var currencyInvArray=currencyInv.split(',');
            var splitdate=endDate.split('/');
            var currMTH=splitdate[0];
            var splitdate1=startDate.split('/');
            var beforeMTH=splitdate1[0];
            var totalamInv=0;
            var totalamInvB=0;
            var totalamInv1=0;
            var totalamInvB1=0;
            var totalpowerUsage=parseFloat(totalAmountCurr)-parseFloat(totalAmount1MTHAgo);
            //if(totalAmountCurr!=0)
            // {
            //   for(var j = 0;dateInvArray  != null && j < dateInvArray.length; j++)
            //  {
            //    var splitDateInv=dateInvArray[j].split('/');
            //   if((currMTH==splitDateInv[0])&&(amountInvArray[j]!=0))
            //  {
            if(parseFloat(totalpowerUsage)!=0)
            {
                stReturn += '\n{"customer":"' + customerName +
                    '","type":"' +  'Invoice' +
                    '","categoryrevenue":"' +  'Usage' +
                    '", "date":"' +  startDate +
                    '","customerid":' +  customerID +
                    ', "amount":' +parseFloat(totalpowerUsage).toFixed(2)+
                    ',"market":"' +  locationInv +
                    '", "product":"' +  categoryInv +
                    '", "currency":"' +  currencyInvArray[0]+
                    '" },';
                // }

                // }
                //}
            }
        }

    }

    return stReturn;

}
function getProductRevenue(marketid,search,putMonth,product){

    var arrFilters=new Array();
    var arrColumns=new Array();
    var arrFiltersNewMRR=new Array();
    var arrColumnsNewMRR=new Array();
    if(marketid!=0)
    {
        if(marketid=='25')
        {
            //Montreal
            var marketidlist=["5", "8", "9", "10", "11", "12", "27"];

        }
        if(marketid=='23')
        {
            //Toronto
            var marketidlist=["6", "13", "15"];

        }
        if(marketid=='26')
        {
            //Vancouver
            var marketidlist=["28", "7"];

        }
        if(marketid=='22')
        {
            //Minneapolis
            var marketidlist=["16", "35"];
        }
        if(marketid=='29')
        {
            //Jacksonville
            var marketidlist=["31"];
        }
        if(marketid=='20')
        {
            //Dallas
            var marketidlist=["2", "17"];
        }
        if(marketid=='33')
        {
            //Columbus
            var marketidlist=["38", "34","39"];
        }
        if(marketid=='41')
        {
            //Lakeland
            var marketidlist=["42"];
        }
        if(marketid=='43')
        {
            //Jacksonville2
            var marketidlist=["40"];
        }

        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",marketidlist));
        arrFiltersNewMRR.push(new nlobjSearchFilter("location",null,"anyof",marketidlist));


    }
    var  stReturn='';
    if(putMonth==0)
    {
        var arrDates  = getDateRange(0);
        var arrDates1  = getDateRange(-1);
    }
    if(putMonth==-1)
    {
        var arrDates  = getDateRange(-1);
        var arrDates1  = getDateRange(-2);
    }
    if(putMonth==-2)
    {
        var arrDates  = getDateRange(-2);
        var arrDates1  = getDateRange(-3);
    }
    if(putMonth==-3)
    {
        var arrDates  = getDateRange(-3);
        var arrDates1  = getDateRange(-4);
    }
    if(putMonth==-4)
    {
        var arrDates  = getDateRange(-4);
        var arrDates1  = getDateRange(-5);
    }
    if(putMonth==-5)
    {
        var arrDates  = getDateRange(-5);
        var arrDates1  = getDateRange(-6);
    }
    var startDate = arrDates1[0];
    var endDate  = arrDates[1];
    var productID=new Array();
    productID[0] =  4;
    productID[1] =  5;
    productID[2] =  7;
    productID[3] =  8;
    productID[4] =  9;
    productID[5] =  10;
    productID[6] =  11;
    productID[7] =  12;
    if(product=='inter'){
        var productID=[4];
    }else if(product=='pow'){
        var productID=[8];
    }
    else if(product=='net'){
        var productID=[5];
    }
    else if(product=='virtual'){
        var productID=[11];
    }
    else if(product=='other'){
        var productID=[7];
    }
    else if(product=='sp'){
        var productID=[10];
    }
    else if(product=='disaster'){
        var productID=[12];
    }
    else if(product=='remote'){
        var productID=[9];
    }


    arrFilters.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    arrFiltersNewMRR.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    arrFiltersNewMRR.push(new nlobjSearchFilter("custcol_cologix_invoice_item_category", null, "anyof",productID));
    arrFilters.push(new nlobjSearchFilter("custcol_cologix_invoice_item_category", null, "anyof",productID));


    var customerMRRArray=new Array();
    var totalAmountCurrMRRArray=new Array();
    var totalAmountBFMRRArray=new Array();
    var totalAmount1MRRArray=new Array();
    var totalAmount2MRRArray=new Array();
    var totalAmount3MRRArray=new Array();
    var totalAmount4MRRArray=new Array();
    var totalAmount5MRRArray=new Array();
    var searchInvoicesNewMRR = nlapiLoadSearch('transaction', 'customsearch_clgx_inv_revenuegr_all_nwm');
    searchInvoicesNewMRR.addColumns(arrColumnsNewMRR);
    searchInvoicesNewMRR.addFilters(arrFiltersNewMRR);
    var searchInvNewMRR = searchInvoicesNewMRR.runSearch();
    searchInvNewMRR.forEachResult(function(searchResult) {
        var columnsMRR = searchResult.getAllColumns();
        var customerMRR=searchResult.getValue(columnsMRR[1]);
        var totalAmountCurrMRR=parseFloat(searchResult.getValue(columnsMRR[3]));
        var totalAmountBFMRR=parseFloat(searchResult.getValue(columnsMRR[4]));
        var totalAmount1MRR=parseFloat(searchResult.getValue(columnsMRR[5]));
        var totalAmount2MRR=parseFloat(searchResult.getValue(columnsMRR[6]));
        var totalAmount3MRR=parseFloat(searchResult.getValue(columnsMRR[7]));
        var totalAmount4MRR=parseFloat(searchResult.getValue(columnsMRR[8]));
        var totalAmount5MRR=parseFloat(searchResult.getValue(columnsMRR[9]));
        customerMRRArray.push(customerMRR);
        totalAmountCurrMRRArray.push(totalAmountCurrMRR);
        totalAmountBFMRRArray.push(totalAmountBFMRR);
        totalAmount1MRRArray.push(totalAmount1MRR);
        totalAmount2MRRArray.push(totalAmount2MRR);
        totalAmount3MRRArray.push(totalAmount3MRR);
        totalAmount4MRRArray.push(totalAmount4MRR);
        totalAmount5MRRArray.push(totalAmount5MRR);

        return true; // return true to keep iterating

    });
    var searchInvoices = nlapiLoadSearch('transaction',search);
    searchInvoices.addColumns(arrColumns);
    searchInvoices.addFilters(arrFilters);
    var resultINV = searchInvoices.runSearch();
    resultINV.forEachResult(function(searchResult) {
        var columns = searchResult.getAllColumns();
        var customerID=searchResult.getValue(columns[0]);
        var customerName=searchResult.getValue(columns[1]);
        var locationInv=searchResult.getText(columns[2]);
        var currMTHAmount=searchResult.getValue(columns[3]);
        var bfMTHAmount=searchResult.getValue(columns[4]);
        var MTH1AgoAmount=searchResult.getValue(columns[5]);
        var MTH2AgoAmount=searchResult.getValue(columns[6]);
        var MTH3AgoAmount=searchResult.getValue(columns[7]);
        var MTH4AgoAmount=searchResult.getValue(columns[8]);
        var MTH5AgoAmount=searchResult.getValue(columns[9]);
        var currMTHQTY=searchResult.getValue(columns[10]);
        var bfMTHQTY=searchResult.getValue(columns[11]);
        var MTH1AgoQTY=searchResult.getValue(columns[12]);
        var MTH2AgoQTY=searchResult.getValue(columns[13]);
        var MTH3AgoQTY=searchResult.getValue(columns[14]);
        var MTH4AgoQTY=searchResult.getValue(columns[15]);
        var MTH5AgoQTY=searchResult.getValue(columns[16]);
        var categoryInv1=searchResult.getText(columns[17]);
        var currencyINV1=searchResult.getText(columns[18]);
        var arrDates_1  = getDateRange(-1);
        var startDate_1 = arrDates_1[0];
        var arrDates_2  = getDateRange(-2);
        var startDate_2 = arrDates_2[0];
        var arrDates_3  = getDateRange(-3);
        var startDate_3 = arrDates_3[0];
        var arrDates_4  = getDateRange(-4);
        var startDate_4 = arrDates_4[0];
        var arrDates_5  = getDateRange(-5);
        var startDate_5 = arrDates_5[0];
        var arrDates_6  = getDateRange(-6);
        var startDate_6 = arrDates_6[0];
        var index=0;
        for ( var l = 0;customerMRRArray  != null && l < customerMRRArray.length; l++ ) {

            if(customerMRRArray[l]==customerName)
            {
                index=l;
            }
        }
        //1  MT AGO
        if((parseFloat(currMTHAmount)!=0)&&(parseFloat(bfMTHAmount)==0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmountCurrMRRArray[index])!=0)&&(parseFloat(totalAmountBFMRRArray[index])==0))
                {
                    if(putMonth==0)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
                else{
                    if(putMonth==0)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(currMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
        }
        else{
            if(parseInt(currMTHQTY)>parseInt(bfMTHQTY)){

                var totalamInvQ=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                if(totalamInvQ>0)
                {
                    if(putMonth==0)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }


            }
            else if(parseFloat(currMTHAmount) > parseFloat(bfMTHAmount)){


                var totalamInvQ1=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                if(totalamInvQ1>0)
                {
                    if(putMonth==0)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
        }
        //2  MT AGO
        if((parseFloat(bfMTHAmount)!=0)&&(parseFloat(MTH1AgoAmount)==0))
        {

            if(index>0)
            {
                if((parseFloat(totalAmountBFMRRArray[index])!=0)&&(parseFloat(totalAmount1MRRArray[index])==0))
                {
                    if(putMonth==-1)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }


                }


                else{
                    if(putMonth==-1)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(bfMTHAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }

                }
            }
        }

        else{
            if(parseInt(bfMTHQTY)>parseInt(MTH1AgoQTY)){

                var totalamInvQ=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                if(totalamInvQ>0)
                {
                    if(putMonth==-1)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }

                    }
                }


            }
            else if(parseFloat(bfMTHAmount) > parseFloat(MTH1AgoAmount)){


                var totalamInvQ1=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                if(totalamInvQ1>0)
                {
                    if(putMonth==-1)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
        }

        //3 MT AGO
        if((parseFloat(MTH1AgoAmount)!=0)&&(parseFloat(MTH2AgoAmount)==0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount1MRRArray[index])!=0)&&(parseFloat(totalAmount2MRRArray[index])==0))
                {
                    if(putMonth==-2)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }

                }
                else
                {
                    if(putMonth==-2)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH1AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
        }
        else{
            if(parseInt(MTH1AgoQTY)>parseInt(MTH2AgoQTY)){

                var totalamInvQ=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                if(totalamInvQ>0)
                {
                    if(putMonth==-2)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }


            }
            else if(parseFloat(MTH1AgoAmount) > parseFloat(MTH2AgoAmount)){


                var totalamInvQ1=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                if(totalamInvQ1>0)
                {
                    if(putMonth==-2)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
        }
//4 MT AGO
        if((parseFloat(MTH2AgoAmount)!=0)&&(parseFloat(MTH3AgoAmount)==0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount2MRRArray[index])!=0)&&(parseFloat(totalAmount3MRRArray[index])==0))
                {
                    if(putMonth==-3)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
                else{
                    if(putMonth==-3)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH2AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
        }
        else{
            if(parseInt(MTH2AgoQTY)>parseInt(MTH3AgoQTY)){

                var totalamInvQ=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                if(totalamInvQ>0)
                {
                    if(putMonth==-3)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }


            }
            else if(parseFloat(MTH2AgoAmount) > parseFloat(MTH3AgoAmount)){


                var totalamInvQ1=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                if(totalamInvQ1>0)
                {
                    if(putMonth==-3)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
        }
        //5 MT AGO
        if((parseFloat(MTH3AgoAmount)!=0)&&(parseFloat(MTH4AgoAmount)==0))
        {
            if(index>0)
            {

                if((parseFloat(totalAmount3MRRArray[index])!=0)&&(parseFloat(totalAmount4MRRArray[index])==0))
                {
                    if(putMonth==-4)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
                else{
                    if(putMonth==-4)
                    {
                        if(product=='inter')
                        {

                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH3AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
        }
        else{
            if(parseInt(MTH3AgoQTY)>parseInt(MTH4AgoQTY)){

                var totalamInvQ=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                if(totalamInvQ>0)
                {
                    if(putMonth==-4)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
            else if(parseFloat(MTH3AgoAmount) > parseFloat(MTH4AgoAmount)){


                var totalamInvQ1=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                if(totalamInvQ1>0)
                {
                    if(putMonth==-4)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
        }
        //6 MT AGO
        if((parseFloat(MTH4AgoAmount)!=0)&&(parseFloat(MTH5AgoAmount)==0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount4MRRArray[index])!=0)&&(parseFloat(totalAmount5MRRArray[index])==0))
                {
                    if(putMonth==-5)
                    {

                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH34AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'New MRR' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
                else{
                    if(putMonth==-5)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH34AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(MTH4AgoAmount)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
        }
        else{
            if(parseInt(MTH4AgoQTY)>parseInt(MTH5AgoQTY)){

                var totalamInvQ=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                if(totalamInvQ>0)
                {
                    if(putMonth==-5)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Upgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }

            else if(parseFloat(MTH4AgoAmount) > parseFloat(MTH5AgoAmount)){


                var totalamInvQ1=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                if(totalamInvQ1>0)
                {
                    if(putMonth==-5)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Price Increase' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ1)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
        }

        //CHURN

        //1  MT AGO
        if((parseFloat(currMTHAmount)==0)&&(parseFloat(bfMTHAmount)!=0))
        {
            if(putMonth==0)
            {
                if(product=='inter')
                {
                    if(categoryInv1=='Interconnection')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Churn' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_1 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(bfMTHAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='other')
                {
                    if(categoryInv1=='Other Recurring')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_1 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(bfMTHAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='virtual')
                {
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_1 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(bfMTHAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='net')
                {
                    if(categoryInv1=='Network')
                    {

                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_1 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(bfMTHAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='sp')
                {
                    if(categoryInv1=='Space')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_1 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(bfMTHAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='pow')
                {
                    if(categoryInv1=='Power')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_1 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(bfMTHAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='disaster')
                {
                    if(categoryInv1=='Disaster Recovery')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_1 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(bfMTHAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='remote')
                {
                    if(categoryInv1=='Remote Hands')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_1 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(bfMTHAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
            }

        }
        else{
            if(parseInt(currMTHQTY)<parseInt(bfMTHQTY)){

                var totalamInvQ=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                if(totalamInvQ<0)
                {
                    if(putMonth==0)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Churn' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_1 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
            else if(parseFloat(currMTHAmount) < parseFloat(bfMTHAmount)){


                var totalamInvQ1=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                if(putMonth==0)
                {
                    if(product=='inter')
                    {
                        if(categoryInv1=='Interconnection')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_1 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='other')
                    {
                        if(categoryInv1=='Other Recurring')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_1 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='virtual')
                    {
                        if(categoryInv1=='Virtual Interconnection')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_1 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='net')
                    {
                        if(categoryInv1=='Network')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_1 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='sp')
                    {
                        if(categoryInv1=='Space')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_1 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='pow')
                    {
                        if(categoryInv1=='Power')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_1 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='disaster')
                    {
                        if(categoryInv1=='Disaster Recovery')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_1 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='remote')
                    {
                        if(categoryInv1=='Remote Hands')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_1 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
        }
        //2  MT AGO
        if((parseFloat(bfMTHAmount)==0)&&(parseFloat(MTH1AgoAmount)!=0))
        {
            if(putMonth==-1)
            {
                if(product=='inter')
                {
                    if(categoryInv1=='Interconnection')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Churn' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_2 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH1AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='other')
                {
                    if(categoryInv1=='Other Recurring')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_2 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH1AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='virtual')
                {
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_2 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH1AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='net')
                {
                    if(categoryInv1=='Network')
                    {

                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_2 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH1AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='sp')
                {
                    if(categoryInv1=='Space')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_2 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH1AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='pow')
                {
                    if(categoryInv1=='Power')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_2 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH1AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='disaster')
                {
                    if(categoryInv1=='Disaster Recovery')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_2 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH1AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='remote')
                {
                    if(categoryInv1=='Remote Hands')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_2 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH1AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
            }
        }
        else{
            if(parseInt(bfMTHQTY)<parseInt(MTH1AgoQTY)){

                var totalamInvQ=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                if(totalamInvQ<0)
                {
                    if(putMonth==-1)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Churn' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_2 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
            else if(parseFloat(bfMTHAmount) < parseFloat( MTH1AgoAmount)){


                var totalamInvQ1=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                if(putMonth==-1)
                {
                    if(product=='inter')
                    {
                        if(categoryInv1=='Interconnection')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_2 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='other')
                    {
                        if(categoryInv1=='Other Recurring')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_2 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='virtual')
                    {
                        if(categoryInv1=='Virtual Interconnection')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_2 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='net')
                    {
                        if(categoryInv1=='Network')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_2 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='sp')
                    {
                        if(categoryInv1=='Space')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_2 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='pow')
                    {
                        if(categoryInv1=='Power')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_2 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='disaster')
                    {
                        if(categoryInv1=='Disaster Recovery')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_2 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='remote')
                    {
                        if(categoryInv1=='Remote Hands')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_2 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
        }

        //3 MT AGO
        if((parseFloat(MTH1AgoAmount)==0)&&(parseFloat(MTH2AgoAmount)!=0))
        {
            if(putMonth==-2)
            {
                if(product=='inter')
                {
                    if(categoryInv1=='Interconnection')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Churn' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_3 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH2AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }

                if(product=='other')
                {
                    if(categoryInv1=='Other Recurring')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_3 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH2AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='virtual')
                {
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_3 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH2AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='net')
                {
                    if(categoryInv1=='Network')
                    {

                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_3 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH2AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='sp')
                {
                    if(categoryInv1=='Space')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_3 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH2AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='pow')
                {
                    if(categoryInv1=='Power')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_3 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH2AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='disaster')
                {
                    if(categoryInv1=='Disaster Recovery')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_3 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH2AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='remote')
                {
                    if(categoryInv1=='Remote Hands')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_3 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH2AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
            }

        }
        else{
            if(parseInt(MTH1AgoQTY)<parseInt(MTH2AgoQTY)){

                var totalamInvQ=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                if(totalamInvQ<0)
                {
                    if(putMonth==-2)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Churn' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_3 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
            else if(parseFloat(MTH1AgoAmount) <parseFloat( MTH2AgoAmount)){


                var totalamInvQ1=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                if(putMonth==-2)
                {
                    if(product=='inter')
                    {
                        if(categoryInv1=='Interconnection')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='other')
                    {
                        if(categoryInv1=='Other Recurring')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='virtual')
                    {
                        if(categoryInv1=='Virtual Interconnection')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='net')
                    {
                        if(categoryInv1=='Network')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='sp')
                    {
                        if(categoryInv1=='Space')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='pow')
                    {
                        if(categoryInv1=='Power')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='disaster')
                    {
                        if(categoryInv1=='Disaster Recovery')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='remote')
                    {
                        if(categoryInv1=='Remote Hands')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_3 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
        }
//4 MT AGO
        if((parseFloat(MTH2AgoAmount)==0)&&(parseFloat(MTH3AgoAmount)!=0))
        {
            if(putMonth==-3)
            {
                if(product=='inter')
                {
                    if(categoryInv1=='Interconnection')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Churn' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_4 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH3AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }

                if(product=='other')
                {
                    if(categoryInv1=='Other Recurring')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_4 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH3AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='virtual')
                {
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_4 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH3AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='net')
                {
                    if(categoryInv1=='Network')
                    {

                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_4 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH3AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='sp')
                {
                    if(categoryInv1=='Space')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_4 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH3AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='pow')
                {
                    if(categoryInv1=='Power')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_4 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH3AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='disaster')
                {
                    if(categoryInv1=='Disaster Recovery')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_4 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH3AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='remote')
                {
                    if(categoryInv1=='Remote Hands')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_4 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH3AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
            }
        }
        else{
            if(parseInt(MTH2AgoQTY)<parseInt(MTH3AgoQTY)){

                var totalamInvQ=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                if(totalamInvQ<0)
                {
                    if(putMonth==-3)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Churn' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_4 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
            else if(parseFloat(MTH2AgoAmount) < parseFloat(MTH3AgoAmount)){


                var totalamInvQ1=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                if(putMonth==-3)
                {
                    if(product=='inter')
                    {
                        if(categoryInv1=='Interconnection')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_4 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='other')
                    {
                        if(categoryInv1=='Other Recurring')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_4 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='virtual')
                    {
                        if(categoryInv1=='Virtual Interconnection')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_4 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='net')
                    {
                        if(categoryInv1=='Network')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_4 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='sp')
                    {
                        if(categoryInv1=='Space')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_4 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='pow')
                    {
                        if(categoryInv1=='Power')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_4 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='disaster')
                    {
                        if(categoryInv1=='Disaster Recovery')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_4 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='remote')
                    {
                        if(categoryInv1=='Remote Hands')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_4 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }

            }
        }
        //5 MT AGO
        if((parseFloat(MTH3AgoAmount)==0)&&(parseFloat(MTH4AgoAmount)!=0))
        {
            if(putMonth==-4)
            {
                if(product=='inter')
                {
                    if(categoryInv1=='Interconnection')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Churn' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_5 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH4AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='other')
                {
                    if(categoryInv1=='Other Recurring')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_5 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH4AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='virtual')
                {
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_5 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH4AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='net')
                {
                    if(categoryInv1=='Network')
                    {

                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_5 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH4AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='sp')
                {
                    if(categoryInv1=='Space')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_5 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH4AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='pow')
                {
                    if(categoryInv1=='Power')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_5 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH4AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='disaster')
                {
                    if(categoryInv1=='Disaster Recovery')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_5 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH4AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='remote')
                {
                    if(categoryInv1=='Remote Hands')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_5 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH4AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
            }
        }
        else{
            if(parseInt(MTH3AgoQTY)<parseInt(MTH4AgoQTY)){

                var totalamInvQ=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                if(totalamInvQ<0)
                {
                    if(putMonth==-4)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Churn' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_5 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }
            }
            else if(parseFloat(MTH3AgoAmount) <parseFloat( MTH4AgoAmount)){


                var totalamInvQ1=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                if(putMonth==-4)
                {
                    if(product=='inter')
                    {
                        if(categoryInv1=='Interconnection')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_5 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='other')
                    {
                        if(categoryInv1=='Other Recurring')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_5 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='virtual')
                    {
                        if(categoryInv1=='Virtual Interconnection')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_5 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='net')
                    {
                        if(categoryInv1=='Network')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_5 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='sp')
                    {
                        if(categoryInv1=='Space')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_5 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='pow')
                    {
                        if(categoryInv1=='Power')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_5 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='disaster')
                    {
                        if(categoryInv1=='Disaster Recovery')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_5 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='remote')
                    {
                        if(categoryInv1=='Remote Hands')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_5 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
        }
        //6 MT AGO
        if((parseFloat(MTH4AgoAmount)==0)&&(parseFloat(MTH5AgoAmount)!=0))
        {
            if(putMonth==-5)
            {
                if(product=='inter')
                {
                    if(categoryInv1=='Interconnection')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Churn' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_6 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH5AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='other')
                {
                    if(categoryInv1=='Other Recurring')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_6 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH5AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='virtual')
                {
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_6 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH5AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='net')
                {
                    if(categoryInv1=='Network')
                    {

                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_6 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH5AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='sp')
                {
                    if(categoryInv1=='Space')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_6 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH5AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='pow')
                {
                    if(categoryInv1=='Power')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_6 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH5AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='disaster')
                {
                    if(categoryInv1=='Disaster Recovery')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_6 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH5AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
                if(product=='remote')
                {
                    if(categoryInv1=='Remote Hands')
                    {
                        stReturn += '\n{"customer":"' + customerName +
                            '","type":"' +  'Invoice' +
                            '","categoryrevenue":"' +  'Disconnect' +
                            '", "date":"' +  startDate_6 +
                            '","customerid":' +  customerID +
                            ', "amount":' +parseFloat((-1))*parseFloat(MTH5AgoAmount)+
                            ',"market":"' +  locationInv +
                            '", "product":"' +  categoryInv1 +
                            '", "currency":"' +  currencyINV1+
                            '" },';
                    }
                }
            }
        }
        else{
            if(parseInt(MTH4AgoQTY)<parseInt(MTH5AgoQTY)){

                var totalamInvQ=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                if(totalamInvQ<0)
                {
                    if(putMonth==-5)
                    {
                        if(product=='inter')
                        {
                            if(categoryInv1=='Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Churn' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='other')
                        {
                            if(categoryInv1=='Other Recurring')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='virtual')
                        {
                            if(categoryInv1=='Virtual Interconnection')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='net')
                        {
                            if(categoryInv1=='Network')
                            {

                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='sp')
                        {
                            if(categoryInv1=='Space')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='pow')
                        {
                            if(categoryInv1=='Power')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='disaster')
                        {
                            if(categoryInv1=='Disaster Recovery')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                        if(product=='remote')
                        {
                            if(categoryInv1=='Remote Hands')
                            {
                                stReturn += '\n{"customer":"' + customerName +
                                    '","type":"' +  'Invoice' +
                                    '","categoryrevenue":"' +  'Downgrade' +
                                    '", "date":"' +  startDate_6 +
                                    '","customerid":' +  customerID +
                                    ', "amount":' +parseFloat(totalamInvQ)+
                                    ',"market":"' +  locationInv +
                                    '", "product":"' +  categoryInv1 +
                                    '", "currency":"' +  currencyINV1+
                                    '" },';
                            }
                        }
                    }
                }

            }
            else if(parseFloat(MTH4AgoAmount) < parseFloat(MTH5AgoAmount)){


                var totalamInvQ1=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                if(putMonth==-5)
                {
                    if(product=='inter')
                    {
                        if(categoryInv1=='Interconnection')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Churn' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_6 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='other')
                    {
                        if(categoryInv1=='Other Recurring')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_6 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='virtual')
                    {
                        if(categoryInv1=='Virtual Interconnection')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_6 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='net')
                    {
                        if(categoryInv1=='Network')
                        {

                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_6 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='sp')
                    {
                        if(categoryInv1=='Space')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_6 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='pow')
                    {
                        if(categoryInv1=='Power')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_6 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='disaster')
                    {
                        if(categoryInv1=='Disaster Recovery')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_6 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                    if(product=='remote')
                    {
                        if(categoryInv1=='Remote Hands')
                        {
                            stReturn += '\n{"customer":"' + customerName +
                                '","type":"' +  'Invoice' +
                                '","categoryrevenue":"' +  'Decrease' +
                                '", "date":"' +  startDate_6 +
                                '","customerid":' +  customerID +
                                ', "amount":' +parseFloat(totalamInvQ1)+
                                ',"market":"' +  locationInv +
                                '", "product":"' +  categoryInv1 +
                                '", "currency":"' +  currencyINV1+
                                '" },';
                        }
                    }
                }
            }
        }
        return true; // return true to keep iterating

    });

    return stReturn;

}

function getDateRange(monthsago){

    var today = new Date();
    var date = nlapiAddMonths(today, monthsago);

    var month = parseInt(date.getMonth());
    var year = date.getFullYear();

    var stDays  = daysInMonth(parseInt(month),parseInt(year));
    var stYear  = year;

    var stMonth = parseInt(month) + 1;

    var stStartDate = stMonth + '/1/' + stYear;
    var stEndDate   = stMonth + '/' + stDays + '/' + stYear;

    var arrDateRange = new Array();
    arrDateRange[0] = stStartDate;
    arrDateRange[1] =stEndDate;

    return arrDateRange;
}


function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}
