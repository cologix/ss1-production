nlapiLogExecution("audit","FLOStart",new Date().getTime());
/**
 * Created by catalinataran on 2016-03-17.
 */
function scheduled_create_json () {
    try {
        nlapiLogExecution('DEBUG','Scheduled Script - Create_json','|--------------------STARTED---------------------|');
       var arrSeries=getSeries();
        var seriesForJSON='['+arrSeries+']';
        var fileName = nlapiCreateFile('revenue.json', 'PLAINTEXT', seriesForJSON);
        fileName.setFolder(1197564);
        nlapiSubmitFile(fileName);
        nlapiLogExecution('DEBUG','Scheduled Script - Create_json','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
function getSeries() {

    var arrDates0 = getDateRange(0);
    var arrDates6 = getDateRange(-6);
    var startDate0 = arrDates0[0];
    var endDate0 = arrDates0[1];
    var startDate6 = arrDates6[0];
    endDate0 = moment(endDate0).format('l');
    startDate6 = moment(startDate6).format('l');
    var arrFilters = new Array();
    var arrColumns = new Array();
    arrFilters.push(new nlobjSearchFilter("trandate", null, "within", startDate6, endDate0));

    //Find the new MRR
    var stringseries = '';
    var searchInvoices = nlapiLoadSearch('transaction', 'customsearch_clgx_inv_revenuegr_all_13');
    searchInvoices.addColumns(arrColumns);
    searchInvoices.addFilters(arrFilters);
    var resultINV = searchInvoices.runSearch();
    resultINV.forEachResult(function (searchResult) {
        var columns = searchResult.getAllColumns();
        var customerID = searchResult.getValue(columns[0]);
        var customerName = searchResult.getValue(columns[1]);
        var locationInv = searchResult.getText(columns[2]);
        var currMTHAmount = searchResult.getValue(columns[3]);
        var bfMTHAmount = searchResult.getValue(columns[4]);
        var MTH1AgoAmount = searchResult.getValue(columns[5]);
        var MTH2AgoAmount = searchResult.getValue(columns[6]);
        var MTH3AgoAmount = searchResult.getValue(columns[7]);
        var MTH4AgoAmount = searchResult.getValue(columns[8]);
        var MTH5AgoAmount = searchResult.getValue(columns[9]);
        var currMTHQTY = searchResult.getValue(columns[10]);
        var bfMTHQTY = searchResult.getValue(columns[11]);
        var MTH1AgoQTY = searchResult.getValue(columns[12]);
        var MTH2AgoQTY = searchResult.getValue(columns[13]);
        var MTH3AgoQTY = searchResult.getValue(columns[14]);
        var MTH4AgoQTY = searchResult.getValue(columns[15]);
        var MTH5AgoQTY = searchResult.getValue(columns[16]);
        var categoryInv1 = searchResult.getText(columns[17]);
        var currencyINV1 = searchResult.getText(columns[18]);

        stringseries +=  '{"Customer":"' + customerName + '","CustomerID":' + customerID + ', "Amount1":"' + parseFloat(currMTHAmount).toFixed(2) + '", "Amount2":"' + parseFloat(bfMTHAmount).toFixed(2) + '", "Amount3":"' + parseFloat(MTH1AgoAmount).toFixed(2) + '", "Amount4":"' + parseFloat(MTH2AgoAmount).toFixed(2) + '","Amount5":"' + parseFloat(MTH3AgoAmount).toFixed(2) + '","Amount6":"' + parseFloat(MTH4AgoAmount).toFixed(2) + '","Amount7":"' + parseFloat(MTH5AgoAmount).toFixed(2) + '","QTY1":"' + parseFloat(currMTHQTY).toFixed(2) + '", "QTY2":"' + parseFloat(bfMTHQTY).toFixed(2) + '","QTY3":"' + parseFloat(MTH1AgoQTY).toFixed(2) + '","QTY4":"' + parseFloat(MTH2AgoQTY).toFixed(2) + '","QTY5":"' + parseFloat(MTH3AgoQTY).toFixed(2) + '","QTY6":"' + parseFloat(MTH4AgoQTY).toFixed(2) + '","QTY7":"' + parseFloat(MTH5AgoQTY).toFixed(2) + '","Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'


        return true; // return true to keep iterating

    });
    var searchInvoices = nlapiLoadSearch('transaction', 'customsearch_clgx_inv_revenuegr_all_13_2');
    searchInvoices.addColumns(arrColumns);
    searchInvoices.addFilters(arrFilters);
    var resultINV = searchInvoices.runSearch();
    resultINV.forEachResult(function (searchResult) {
        var columns = searchResult.getAllColumns();
        var customerID = searchResult.getValue(columns[0]);
        var customerName = searchResult.getValue(columns[1]);
        var locationInv = searchResult.getText(columns[2]);
        var currMTHAmount = searchResult.getValue(columns[3]);
        var bfMTHAmount = searchResult.getValue(columns[4]);
        var MTH1AgoAmount = searchResult.getValue(columns[5]);
        var MTH2AgoAmount = searchResult.getValue(columns[6]);
        var MTH3AgoAmount = searchResult.getValue(columns[7]);
        var MTH4AgoAmount = searchResult.getValue(columns[8]);
        var MTH5AgoAmount = searchResult.getValue(columns[9]);
        var currMTHQTY = searchResult.getValue(columns[10]);
        var bfMTHQTY = searchResult.getValue(columns[11]);
        var MTH1AgoQTY = searchResult.getValue(columns[12]);
        var MTH2AgoQTY = searchResult.getValue(columns[13]);
        var MTH3AgoQTY = searchResult.getValue(columns[14]);
        var MTH4AgoQTY = searchResult.getValue(columns[15]);
        var MTH5AgoQTY = searchResult.getValue(columns[16]);
        var categoryInv1 = searchResult.getText(columns[17]);
        var currencyINV1 = searchResult.getText(columns[18]);

        stringseries +=  '{"Customer":"' + customerName + '","CustomerID":' + customerID + ', "Amount1":"' + parseFloat(currMTHAmount).toFixed(2) + '", "Amount2":"' + parseFloat(bfMTHAmount).toFixed(2) + '", "Amount3":"' + parseFloat(MTH1AgoAmount).toFixed(2) + '", "Amount4":"' + parseFloat(MTH2AgoAmount).toFixed(2) + '","Amount5":"' + parseFloat(MTH3AgoAmount).toFixed(2) + '","Amount6":"' + parseFloat(MTH4AgoAmount).toFixed(2) + '","Amount7":"' + parseFloat(MTH5AgoAmount).toFixed(2) + '","QTY1":"' + parseFloat(currMTHQTY).toFixed(2) + '", "QTY2":"' + parseFloat(bfMTHQTY).toFixed(2) + '","QTY3":"' + parseFloat(MTH1AgoQTY).toFixed(2) + '","QTY4":"' + parseFloat(MTH2AgoQTY).toFixed(2) + '","QTY5":"' + parseFloat(MTH3AgoQTY).toFixed(2) + '","QTY6":"' + parseFloat(MTH4AgoQTY).toFixed(2) + '","QTY7":"' + parseFloat(MTH5AgoQTY).toFixed(2) + '","Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'


        return true; // return true to keep iterating

    });
    var strLen = stringseries.length;

    stringseries = stringseries.slice(0,strLen-1);

    return stringseries;

}
function getDateRange(monthsago){

    var today = new Date();
    var date = nlapiAddMonths(today, monthsago);

    var month = parseInt(date.getMonth());
    var year = date.getFullYear();

    var stDays  = daysInMonth(parseInt(month),parseInt(year));
    var stYear  = year;

    var stMonth = parseInt(month) + 1;
    if(stMonth<10){
        stMonth='0'+stMonth;
    }

    var stStartDate = stYear+'-' + stMonth+ '-01';
    var stEndDate   = stYear  + '-' + stMonth + '-' + stDays;

    var arrDateRange = new Array();
    arrDateRange[0] = stStartDate;
    arrDateRange[1] =stEndDate;

    return arrDateRange;
}


function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 500 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}