nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Revenue_Chart.js
//	Script Name:	CLGX_SL_CHARTS_Revenue_Chart
//	Script Id:		customscript_clgx_sl_chrt_revenue
// 	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		15/07/2014
//	Includes:		CLGX_LIB_Global.js, moment.min.js
//-------------------------------------------------------------------------------------------------

function suitelet_charts_revenueManagement_chart(datain){
    try {
        // var marketid=0;
        var txtArg = datain;
        // var emailSubject = 'July Revenue';
        //  var emailBody="Values: "+datain;
        //  nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null); // Send email to Catalina
        txtArg = txtArg.replace(/\"/g,"");
        txtArg = txtArg.replace(/\:/g,",");
        txtArg = txtArg.replace(/\{/g,"");
        txtArg = txtArg.replace(/\}/g,"");
        var arrArguments = new Array();
        arrArguments = txtArg.split( "," );
        var marketid = arrArguments[3];
        if(marketid != '' ){
            var objFile = nlapiLoadFile(1513949);
            var html = objFile.getValue();
            html = html.replace(new RegExp('{title}','g'),clgx_return_market_name(marketid));
            html = html.replace(new RegExp('{marketid}','g'), marketid);
            html = html.replace(new RegExp('{location}','g'), clgx_return_market_name(marketid));
            if(marketid==0)
            {
                var min='min: {-250000},'
            }
            else{
                var min='';
            }
            html = html.replace(new RegExp('{min}','g'), min);
            var series=getSeriesCSV(marketid);
            html = html.replace(new RegExp('{series}','g'), series[1]);
            html = html.replace(new RegExp('{seriesCSV}','g'), series[0]);


        }
        else{
            var html = 'Please select a market from the left panel.';
        }
        return html;
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
function getSeriesCSV(marketid){
    var stringReturnCSV='';
    //Network
    //   var netCurrString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridnet_n",1, 0,'NetWork');
    var net1MTHString=getSeriesCSV1(marketid,"customsearch_clgx_inv_revenuegr_all",0,-6,'All');
    var powus1MTHString=getSeriesCSVPU(marketid,"customsearch_clgx_so_noinvgridpowus_c",0,-1,'Power Usage');

   /*var net2MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridnet_1",-1,-2,'NetWork');
     var net3MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridnet_2",-2,-3,'NetWork');
     var net4MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridnet_3",-3,-4,'NetWork');
     var net5MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridnet_4",-4,-5,'NetWork');
     var net6MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridnet_5",-5,-6,'NetWork');
     //Space
     // var spCurrString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridsp_n",1, 0,'Space');
     var sp1MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridsp_c",0,-1,'Space');
     var sp2MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridsp_1",-1,-2,'Space');
     var sp3MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridsp_2",-2,-3,'Space');
     var sp4MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridsp_3",-3,-4,'Space');
     var sp5MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridsp_4",-4,-5,'Space');
     var sp6MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridsp_5",-5,-6,'Space');
     //Power
     // var powCurrString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridpow_n",1, 0,'Power');
     var pow1MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridpow_c",0,-1,'Power');
     var pow2MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridpow_1",-1,-2,'Power');
     var pow3MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridpow_2",-2,-3,'Power');
     var pow4MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridpow_3",-3,-4,'Power');
     var pow5MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridpow_4",-4,-5,'Power');
     var pow6MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridpow_5",-5,-6,'Power');
     //Interconnection
     //var interCurrString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridinter_n",1, 0,'Interconnection');
     var inter1MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridinter_c",0,-1,'Interconnection');
     var inter2MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridinter_1",-1,-2,'Interconnection');
     var inter3MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridinter_2",-2,-3,'Interconnection');
     var inter4MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridinter_3",-3,-4,'Interconnection');
     var inter5MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridinter_4",-4,-5,'Interconnection');
     var inter6MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridinter_5",-5,-6,'Interconnection');
     //Other customsearch_clgx_so_noinvgridpro_5
     //  var otCurrString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridot_n",1, 0,'Other');
     var ot1MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridot_c",0,-1,'Other');
     var ot2MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridot_1",-1,-2,'Other');
     var ot3MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridot_2",-2,-3,'Other');
     var ot4MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridot_3",-3,-4,'Other');
     var ot5MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridot_4",-4,-5,'Other');
     var ot6MTHString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridot_5",-5,-6,'Other');
     */
    //Power Usage
    // var powusCurrString=getSeriesCSVPU(marketid,"customsearch_clgx_so_noinvgridpowus_n",1, 0,'Power Usage');
    var powus2MTHString=getSeriesCSVPU(marketid,"customsearch_clgx_so_noinvgridpowu_1",-1,-2,'Power Usage');
    var powus3MTHString=getSeriesCSVPU(marketid,"customsearch_clgx_so_noinvgridpowus_2",-2,-3,'Power Usage');
    var powus4MTHString=getSeriesCSVPU(marketid,"customsearch_clgx_so_noinvgridpowus_3",-3,-4,'Power Usage');
    var powus5MTHString=getSeriesCSVPU(marketid,"customsearch_clgx_so_noinvgridpowus_4",-4,-5,'Power Usage');
    var powus6MTHString=getSeriesCSVPU(marketid,"customsearch_clgx_so_noinvgridpowus_5",-5,-6,'Power Usage');
    //Prior Month
    var prius2MTHString=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_1",-1,-2,'Power Usage');
    var prius1MTHString=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_c",0,-1,'Power Usage');
    var prius3MTHString=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_2",-2,-3,'Power Usage');
    var prius4MTHString=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_3",-3,-4,'Power Usage');
    var prius5MTHString=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_4",-4,-5,'Power Usage');
    var prius6MTHString=getSeriesCSVPR(marketid,"customsearch_clgx_so_noinvgridpro_5",-5,-6,'Power Usage');
    //Credit Memo
    //Network
    // var cmnetCurrString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogr_n",1, 0,'NetWork');
    var cmnet1MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogr_all",0,-6,'All');
    // var cmnet2MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogr_1",-1,-2,'NetWork');
    //var cmnet3MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogr_2",-2,-3,'NetWork');
    //var cmnet4MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogr_3",-3,-4,'NetWork');
    //var cmnet5MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogr_4",-4,-5,'NetWork');
    //var cmnet6MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogr_5",-5,-6,'NetWork');

    //Space
    // var cmspCurrString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrsp_n",1, 0,'Space');
    //var cmsp1MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrsp_c",0,-1,'Space');
    //var cmsp2MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrsp_1",-1,-2,'Space');
    //var cmsp3MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrsp_2",-2,-3,'Space');
    //var cmsp4MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrsp_3",-3,-4,'Space');
    //var cmsp5MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrsp_4",-4,-5,'Space');
    //var cmsp6MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrsp_5",-5,-6,'Space');
    //Power
    // var cmpowCurrString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrpow_n",1, 0,'Power');
    // var cmpow1MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrpow_c",0,-1,'Power');
    // var cmpow2MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrpow_1",-1,-2,'Power');
    // var cmpow3MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrpow_2",-2,-3,'Power');
    // var cmpow4MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrpow_3",-3,-4,'Power');
    // var cmpow5MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrpow_4",-4,-5,'Power');
    //var cmpow6MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrpow_5",-5,-6,'Power');
    //Interconnection
    //var cminterCurrString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrint_n",1, 0,'Interconnection');
    //  var cminter1MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrint_c",0,-1,'Interconnection');
    // var cminter2MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrint_1",-1,-2,'Interconnection');
    // var cminter3MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrint_2",-2,-3,'Interconnection');
    // var cminter4MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrint_3",-3,-4,'Interconnection');
    // var cminter5MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrint_4",-4,-5,'Interconnection');
    // var cminter6MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrint_5",-5,-6,'Interconnection');
    //Other
    // var cmotCurrString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrot_n",1, 0,'Other');
    //  var cmot1MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrot_c",0,-1,'Other');
    // var cmot2MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrot_1",-1,-2,'Other');
    // var cmot3MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrot_2",-2,-3,'Other');
    // var cmot4MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrot_3",-3,-4,'Other');
    // var cmot5MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrot_4",-4,-5,'Other');
    //var cmot6MTHString=getSeriesCSVCM(marketid,"customsearch_clgx_so_memogrot_5",-5,-6,'Other');
    //Churn
    // var churnCurrString=getSeriesCSVChurn(marketid,"customsearch_clgx_churn_searchlostnet",-6, 1,'');

    stringReturnCSV=net1MTHString[0]+
        powus1MTHString[0]+powus2MTHString[0]+powus3MTHString[0]+powus4MTHString[0]+powus5MTHString[0]+powus6MTHString[0]+
        prius1MTHString[0]+prius2MTHString[0]+prius3MTHString[0]+prius4MTHString[0]+prius5MTHString[0]+prius6MTHString[0]+
        cmnet1MTHString[0];


    var  stringseries= '\n{name: \'Usage\',type: \'column\',  data: \n[\n';
    stringseries += '{y:' + parseFloat(powus6MTHString[1]).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=us&typelabel=Usage&month=-5&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(powus5MTHString[1]).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=us&typelabel=Usage&month=-4&scriptid=2&product=0&productlabel=na'+'\'},\n';
    stringseries += '{y:' + parseFloat(powus4MTHString[1]).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=us&typelabel=Usage&month=-3&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(powus3MTHString[1]).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=us&typelabel=Usage&month=-2&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(powus2MTHString[1]).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=us&typelabel=Usage&month=-1&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(powus1MTHString[1]).toFixed(2) +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=us&typelabel=Usage&month=0&scriptid=2&product=0&productlabel=na'+'\'}';
    stringseries += ']\n';
    stringseries += '},\n';
    var  stringseries2= '\n{name: \'Pro Rate\',type: \'column\',visible: false,  data: \n[\n';
    stringseries2 += '{y:' + parseFloat(prius6MTHString[1]).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=pr&typelabel=Pro Rate&month=-5&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries2 += '{y:' + parseFloat(prius5MTHString[1]).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=pr&typelabel=Pro Rate&month=-4&scriptid=2&product=0&productlabel=na'+'\'},\n';
    stringseries2 += '{y:' + parseFloat(prius4MTHString[1]).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=pr&typelabel=Pro Rate&month=-3&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries2 += '{y:' + parseFloat(prius3MTHString[1]).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=pr&typelabel=Pro Rate&month=-2&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries2 += '{y:' + parseFloat(prius2MTHString[1]).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=pr&typelabel=Pro Rate&month=-1&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries2 += '{y:' + parseFloat(prius1MTHString[1]).toFixed(2) +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=pr&typelabel=Pro Rate&month=0&scriptid=2&product=0&productlabel=na'+'\'}';
    stringseries2 += ']\n';
    stringseries2 += '}\n';
    //  var emailSubject = 'July Revenue';
    // var emailBody="Values: "+net1MTHString[2] + ', '+powus6MTHString[1]+','+prius6MTHString[1]+','+cmnet1MTHString[2];
    // nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null); // Send email to Catalina
    var v1=net1MTHString[2];
    var v2=powus6MTHString[1];
    var v3=prius6MTHString[1];
    var v4=cmnet1MTHString[2];
    v1=parseFloat(v1).toFixed(2);
    v2=parseFloat(v2).toFixed(2);
    v3=parseFloat(v3).toFixed(2);
    v4=parseFloat(v4).toFixed(2);
    // var total1=(parseFloat(v1)+parseFloat(v2)+parseFloat(v3)+parseFloat(v4));
    var total1=(parseFloat(v1)+parseFloat(v2));
    var v1_1=net1MTHString[3];
    var v2_1=powus5MTHString[1];
    var v3_1=prius5MTHString[1];
    var v4_1=cmnet1MTHString[3];
    v1_1=parseFloat(v1_1).toFixed(2);
    v2_1=parseFloat(v2_1).toFixed(2);
    v3_1=parseFloat(v3_1).toFixed(2);
    v4_1=parseFloat(v4_1).toFixed(2);
    //var total2=(parseFloat(v1_1)+parseFloat(v2_1)+parseFloat(v3_1)+parseFloat(v4_1));
    var total2=(parseFloat(v1_1)+parseFloat(v2_1));
    //3
    var v1_2=net1MTHString[4];
    var v2_2=powus4MTHString[1];
    var v3_2=prius4MTHString[1];
    var v4_2=cmnet1MTHString[4];
    v1_2=parseFloat(v1_2).toFixed(2);
    v2_2=parseFloat(v2_2).toFixed(2);
    v3_2=parseFloat(v3_2).toFixed(2);
    v4_2=parseFloat(v4_2).toFixed(2);
    //var total3=(parseFloat(v1_2)+parseFloat(v2_2)+parseFloat(v3_2)+parseFloat(v4_2));
    var total3=(parseFloat(v1_2)+parseFloat(v2_2));
    //4
    var v1_3=net1MTHString[5];
    var v2_3=powus3MTHString[1];
    var v3_3=prius3MTHString[1];
    var v4_3=cmnet1MTHString[5];
    v1_3=parseFloat(v1_3).toFixed(2);
    v2_3=parseFloat(v2_3).toFixed(2);
    v3_3=parseFloat(v3_3).toFixed(2);
    v4_3=parseFloat(v4_3).toFixed(2);
    //  var total4=(parseFloat(v1_3)+parseFloat(v2_3)+parseFloat(v3_3)+parseFloat(v4_3));
    var total4=(parseFloat(v1_3)+parseFloat(v2_3));
    //5
    var v1_4=net1MTHString[6];
    var v2_4=powus2MTHString[1];
    var v3_4=prius2MTHString[1];
    var v4_4=cmnet1MTHString[6];
    v1_4=parseFloat(v1_4).toFixed(2);
    v2_4=parseFloat(v2_4).toFixed(2);
    v3_4=parseFloat(v3_4).toFixed(2);
    v4_4=parseFloat(v4_4).toFixed(2);
    //  var total5=(parseFloat(v1_4)+parseFloat(v2_4)+parseFloat(v3_4)+parseFloat(v4_4));
    var total5=(parseFloat(v1_4)+parseFloat(v2_4));
    //6
    var v1_5=net1MTHString[7];
    var v2_5=powus1MTHString[1];
    var v3_5=prius1MTHString[1];
    var v4_5=cmnet1MTHString[7];
    v1_5=parseFloat(v1_5).toFixed(2);
    v2_5=parseFloat(v2_5).toFixed(2);
    v3_5=parseFloat(v3_5).toFixed(2);
    v4_5=parseFloat(v4_5).toFixed(2);
    //var total6=(parseFloat(v1_5)+parseFloat(v2_5)+parseFloat(v3_5)+parseFloat(v4_5));
    var total6=(parseFloat(v1_5)+parseFloat(v2_5));
    var stringseries1=",{name: 'Net Total',type: 'spline',visible: true,color: '#BC4676',data: ["+
        parseFloat(total1).toFixed(2)+',' +
        parseFloat(total2).toFixed(2)+','+
        parseFloat(total3).toFixed(2)+','+
        parseFloat(total4).toFixed(2)+','+
        parseFloat(total5).toFixed(2)+','+
        parseFloat(total6).toFixed(2)+','+"]}";
    var returnArr=[stringReturnCSV,net1MTHString[1]+stringseries+cmnet1MTHString[1]+stringseries2+stringseries1];
    return returnArr;



}

function getSeriesCSV1(marketid,search,curr, bf,cat){

    var arrFilters=new Array();
    var arrColumns=new Array();
    var arrFiltersNewMRR=new Array();
    var arrFiltersNewMRR1=new Array();
    var arrColumnsNewMRR=new Array();
    var arrColumnsNewMRR1=new Array();
    if(marketid!=0)
    {
        if(marketid=='25')
        {
            //Montreal
            var marketidlist=["5", "8", "9", "10", "11", "12", "27"];

        }
        if(marketid=='23')
        {
            //Toronto
            var marketidlist=["6", "13", "15"];

        }
        if(marketid=='26')
        {
            //Vancouver
            var marketidlist=["28", "7"];

        }
        if(marketid=='22')
        {
            //Minneapolis
            var marketidlist=["16", "35"];
        }
        if(marketid=='29')
        {
            //Jacksonville
            var marketidlist=["31"];
        }
        if(marketid=='20')
        {
            //Dallas
            var marketidlist=["2", "17"];
        }
        if(marketid=='33')
        {
            //Columbus
            var marketidlist=["38", "34","39"];
        }
        if(marketid=='41')
        {
            //Lakeland
            var marketidlist=["42"];
        }
        if(marketid=='43')
        {
            //Jacksonville2
            var marketidlist=["40"];
        }
        if(marketid=='52')
        {
            //Parsippany
            var marketidlist=["53","54","55","56"];
        }

        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",marketidlist));
        arrFiltersNewMRR.push(new nlobjSearchFilter("location",null,"anyof",marketidlist));


    } else if(marketid==0){
        var arrNJ=[53,54,55,56,42];
        arrFilters.push(new nlobjSearchFilter("location",null,"noneof",arrNJ));


    }
    var stringCSV='';
    var stringseries='';
    var newMRR1MTTotal=0;
    var newMRR2MTTotal=0;
    var newMRR3MTTotal=0;
    var newMRR4MTTotal=0;
    var newMRR5MTTotal=0;
    var newMRR6MTTotal=0;
    var prMRR1MTTotal=0;
    var prMRR2MTTotal=0;
    var prMRR3MTTotal=0;
    var prMRR4MTTotal=0;
    var prMRR5MTTotal=0;
    var prMRR6MTTotal=0;
    var upMRR1MTTotal=0;
    var upMRR2MTTotal=0;
    var upMRR3MTTotal=0;
    var upMRR4MTTotal=0;
    var upMRR5MTTotal=0;
    var upMRR6MTTotal=0;
    var disco1MTTotal=0;
    var disco2MTTotal=0;
    var disco3MTTotal=0;
    var disco4MTTotal=0;
    var disco5MTTotal=0;
    var disco6MTTotal=0;
    var dec1MTTotal=0;
    var dec2MTTotal=0;
    var dec3MTTotal=0;
    var dec4MTTotal=0;
    var dec5MTTotal=0;
    var dec6MTTotal=0;
    var don1MTTotal=0;
    var don2MTTotal=0;
    var don3MTTotal=0;
    var don4MTTotal=0;
    var don5MTTotal=0;
    var don6MTTotal=0;
    var arrDates  = getDateRange(curr);
    var arrDates1  = getDateRange(bf);
    var startDate = arrDates1[0];
    var endDate  = arrDates[1];
    arrFilters.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    arrFiltersNewMRR.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    arrFiltersNewMRR1.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    var customerMRRArray=new Array();
    var locationMRRArray=new Array();
    var totalAmountCurrMRRArray=new Array();
    var totalAmountBFMRRArray=new Array();
    var totalAmount1MRRArray=new Array();
    var totalAmount2MRRArray=new Array();
    var totalAmount3MRRArray=new Array();
    var totalAmount4MRRArray=new Array();
    var totalAmount5MRRArray=new Array();
    var searchInvoicesNewMRR = nlapiLoadSearch('transaction', 'customsearch_clgx_inv_revenuegr_all_nwm');
    searchInvoicesNewMRR.addColumns(arrColumnsNewMRR);
    searchInvoicesNewMRR.addFilters(arrFiltersNewMRR);
    var searchInvNewMRR = searchInvoicesNewMRR.runSearch();
    searchInvNewMRR.forEachResult(function(searchResult) {
        var columnsMRR = searchResult.getAllColumns();
        var customerMRR=searchResult.getValue(columnsMRR[1]);
        var locationMRR=searchResult.getText(columnsMRR[2]);
        var totalAmountCurrMRR=parseFloat(searchResult.getValue(columnsMRR[3]));
        var totalAmountBFMRR=parseFloat(searchResult.getValue(columnsMRR[4]));
        var totalAmount1MRR=parseFloat(searchResult.getValue(columnsMRR[5]));
        var totalAmount2MRR=parseFloat(searchResult.getValue(columnsMRR[6]));
        var totalAmount3MRR=parseFloat(searchResult.getValue(columnsMRR[7]));
        var totalAmount4MRR=parseFloat(searchResult.getValue(columnsMRR[8]));
        var totalAmount5MRR=parseFloat(searchResult.getValue(columnsMRR[9]));
        customerMRRArray.push(customerMRR);
        locationMRRArray.push(customerMRR+';'+locationMRR);
        totalAmountCurrMRRArray.push(totalAmountCurrMRR);
        totalAmountBFMRRArray.push(totalAmountBFMRR);
        totalAmount1MRRArray.push(totalAmount1MRR);
        totalAmount2MRRArray.push(totalAmount2MRR);
        totalAmount3MRRArray.push(totalAmount3MRR);
        totalAmount4MRRArray.push(totalAmount4MRR);
        totalAmount5MRRArray.push(totalAmount5MRR);

        return true; // return true to keep iterating

    });

    var searchInvoices = nlapiLoadSearch('transaction',search);
    searchInvoices.addColumns(arrColumns);
    searchInvoices.addFilters(arrFilters);
    var resultINV = searchInvoices.runSearch();
    resultINV.forEachResult(function(searchResult) {
        var columns = searchResult.getAllColumns();
        var customerID=searchResult.getValue(columns[0]);
        var customerName=searchResult.getValue(columns[1]);
        var locationInv=searchResult.getText(columns[2]);
        var currMTHAmount=searchResult.getValue(columns[3]);
        var bfMTHAmount=searchResult.getValue(columns[4]);
        var MTH1AgoAmount=searchResult.getValue(columns[5]);
        var MTH2AgoAmount=searchResult.getValue(columns[6]);
        var MTH3AgoAmount=searchResult.getValue(columns[7]);
        var MTH4AgoAmount=searchResult.getValue(columns[8]);
        var MTH5AgoAmount=searchResult.getValue(columns[9]);
        var currMTHQTY=searchResult.getValue(columns[10]);
        var bfMTHQTY=searchResult.getValue(columns[11]);
        var MTH1AgoQTY=searchResult.getValue(columns[12]);
        var MTH2AgoQTY=searchResult.getValue(columns[13]);
        var MTH3AgoQTY=searchResult.getValue(columns[14]);
        var MTH4AgoQTY=searchResult.getValue(columns[15]);
        var MTH5AgoQTY=searchResult.getValue(columns[16]);
        var categoryInv1=searchResult.getText(columns[17]);
        var currencyINV1=searchResult.getText(columns[18]);
        var arrDates_1  = getDateRange(-1);
        var startDate_1 = arrDates_1[0];
        var arrDates_2  = getDateRange(-2);
        var startDate_2 = arrDates_2[0];
        var arrDates_3  = getDateRange(-3);
        var startDate_3 = arrDates_3[0];
        var arrDates_4  = getDateRange(-4);
        var startDate_4 = arrDates_4[0];
        var arrDates_5  = getDateRange(-5);
        var startDate_5 = arrDates_5[0];
        var arrDates_6  = getDateRange(-6);
        var startDate_6 = arrDates_6[0];
        var index=0;
        var test=customerName+';'+locationInv;
        index=_.indexOf(locationMRRArray, test);
        /* for ( var l = 0;customerMRRArray  != null && l < customerMRRArray.length; l++ ) {

         if(customerMRRArray[l]==customerName)
         {
         index=l;
         }
         }*/
        //1  MT AGO
        if((parseFloat(currMTHAmount)!=0)&&(parseFloat(bfMTHAmount)==0))
        {

            //var emailSubject = 'July Revenue3';
            // var emailBody="Values: "+locationMRRArray[0];
            // nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null); // Send email to Catalina

            if(index>0)
            {
                if((parseFloat(totalAmountCurrMRRArray[index])!=0)&&(parseFloat(totalAmountBFMRRArray[index])==0))
                {
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    newMRR1MTTotal=parseFloat(newMRR1MTTotal)+parseFloat(currMTHAmount);
                }
                else{
                    upMRR1MTTotal=parseFloat(upMRR1MTTotal)+parseFloat(currMTHAmount);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
            }
        }
        else{
            if(parseInt(currMTHQTY)>parseInt(bfMTHQTY)){

                var totalamInvQ=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                if(totalamInvQ>0)
                {
                    upMRR1MTTotal=parseFloat(upMRR1MTTotal)+parseFloat(totalamInvQ);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }


            }
            else if((parseFloat(currMTHAmount) > parseFloat(bfMTHAmount))&&(parseInt(currMTHQTY)==parseInt(bfMTHQTY))){


                var totalamInvQ1=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                if(totalamInvQ1>0)
                {
                    prMRR1MTTotal=parseFloat(prMRR1MTTotal)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
            }
        }
        //2  MT AGO
        if((parseFloat(bfMTHAmount)!=0)&&(parseFloat(MTH1AgoAmount)==0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmountBFMRRArray[index])!=0)&&(parseFloat(totalAmount1MRRArray[index])==0))
                {

                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    newMRR2MTTotal=parseFloat(newMRR2MTTotal)+parseFloat(bfMTHAmount);
                }
                else{
                    upMRR2MTTotal=parseFloat(upMRR2MTTotal)+parseFloat(bfMTHAmount);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
            }
        }
        else{
            if(parseInt(bfMTHQTY)>parseInt(MTH1AgoQTY)){

                var totalamInvQ=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                if(totalamInvQ>0)
                {
                    upMRR2MTTotal=parseFloat(upMRR2MTTotal)+parseFloat(totalamInvQ);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }


            }
            else if(parseFloat(bfMTHAmount) > parseFloat(MTH1AgoAmount)){


                var totalamInvQ1=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                if(totalamInvQ1>0)
                {
                    prMRR2MTTotal=parseFloat(prMRR2MTTotal)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
            }
        }

        //3 MT AGO
        if((parseFloat(MTH1AgoAmount)!=0)&&(parseFloat(MTH2AgoAmount)==0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount1MRRArray[index])!=0)&&(parseFloat(totalAmount2MRRArray[index])==0))
                {
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    newMRR3MTTotal=parseFloat(newMRR3MTTotal)+parseFloat(MTH1AgoAmount);
                }
                else
                {
                    upMRR3MTTotal=parseFloat(upMRR3MTTotal)+parseFloat(MTH1AgoAmount);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
            }
        }
        else{
            if(parseInt(MTH1AgoQTY)>parseInt(MTH2AgoQTY)){

                var totalamInvQ=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                if(totalamInvQ>0)
                {
                    upMRR3MTTotal=parseFloat(upMRR3MTTotal)+parseFloat(totalamInvQ);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }


            }
            else if(parseFloat(MTH1AgoAmount) > parseFloat(MTH2AgoAmount)){


                var totalamInvQ1=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                if(totalamInvQ1>0)
                {
                    prMRR3MTTotal=parseFloat(prMRR3MTTotal)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
            }
        }
//4 MT AGO
        if((parseFloat(MTH2AgoAmount)!=0)&&(parseFloat(MTH3AgoAmount)==0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount2MRRArray[index])!=0)&&(parseFloat(totalAmount3MRRArray[index])==0))
                {
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    newMRR4MTTotal=parseFloat(newMRR4MTTotal)+parseFloat(MTH2AgoAmount);
                }
                else{
                    upMRR4MTTotal=parseFloat(upMRR4MTTotal)+parseFloat(MTH2AgoAmount);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
            }
        }
        else{
            if(parseInt(MTH2AgoQTY)>parseInt(MTH3AgoQTY)){

                var totalamInvQ=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                if(totalamInvQ>0)
                {
                    upMRR4MTTotal=parseFloat(upMRR4MTTotal)+parseFloat(totalamInvQ);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }


            }
            else if(parseFloat(MTH2AgoAmount) > parseFloat(MTH3AgoAmount)){


                var totalamInvQ1=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                if(totalamInvQ1>0)
                {
                    prMRR4MTTotal=parseFloat(prMRR4MTTotal)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
            }
        }
        //5 MT AGO
        if((parseFloat(MTH3AgoAmount)!=0)&&(parseFloat(MTH4AgoAmount)==0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount3MRRArray[index])!=0)&&(parseFloat(totalAmount4MRRArray[index])==0))
                {
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    newMRR5MTTotal=parseFloat(newMRR5MTTotal)+parseFloat(MTH3AgoAmount);
                }
                else{
                    upMRR5MTTotal=parseFloat(upMRR5MTTotal)+parseFloat(MTH3AgoAmount);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
            }
        }
        else{
            if(parseInt(MTH3AgoQTY)>parseInt(MTH4AgoQTY)){

                var totalamInvQ=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                if(totalamInvQ>0)
                {
                    upMRR5MTTotal=parseFloat(upMRR5MTTotal)+parseFloat(totalamInvQ);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }


            }
            else if(parseFloat(MTH3AgoAmount) > parseFloat(MTH4AgoAmount)){


                var totalamInvQ1=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                if(totalamInvQ1>0)
                {
                    prMRR5MTTotal=parseFloat(prMRR5MTTotal)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
            }
        }
        //6 MT AGO
        if((parseFloat(MTH4AgoAmount)!=0)&&(parseFloat(MTH5AgoAmount)==0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount4MRRArray[index])!=0)&&(parseFloat(totalAmount5MRRArray[index])==0))
                {

                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    newMRR6MTTotal=parseFloat(newMRR6MTTotal)+parseFloat(MTH4AgoAmount);
                }
                else{
                    upMRR6MTTotal=parseFloat(upMRR6MTTotal)+parseFloat(MTH4AgoAmount);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
            }
        }
        else{
            if(parseInt(MTH4AgoQTY)>parseInt(MTH5AgoQTY)){

                var totalamInvQ=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                if(totalamInvQ>0)
                {
                    upMRR6MTTotal=parseFloat(upMRR6MTTotal)+parseFloat(totalamInvQ);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }


            }
            else if(parseFloat(MTH4AgoAmount) > parseFloat(MTH5AgoAmount)){


                var totalamInvQ1=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                if(totalamInvQ1>0)
                {
                    prMRR6MTTotal=parseFloat(prMRR6MTTotal)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
            }
        }

        //CHURN

        //1  MT AGO
        if((parseFloat(currMTHAmount)==0)&&(parseFloat(bfMTHAmount)!=0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmountCurrMRRArray[index])==0)&&(parseFloat(totalAmountBFMRRArray[index])!=0))
                {
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    disco1MTTotal=parseFloat(disco1MTTotal)+parseFloat((-1))*parseFloat(bfMTHAmount);
                }
                else
                {
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    don1MTTotal=parseFloat(don1MTTotal)+parseFloat((-1))*parseFloat(bfMTHAmount);
                }
            }
        }
        else{
            if(parseInt(currMTHQTY)<parseInt(bfMTHQTY)){

                var totalamInvQ=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                if(totalamInvQ<0)
                {
                    don1MTTotal=parseFloat(don1MTTotal)+parseFloat(totalamInvQ);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
            }
            else if(parseFloat(currMTHAmount) < parseFloat(bfMTHAmount)){


                var totalamInvQ1=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                dec1MTTotal=parseFloat(dec1MTTotal)+parseFloat(totalamInvQ1);
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

            }
        }
        //2  MT AGO
        if((parseFloat(bfMTHAmount)==0)&&(parseFloat(MTH1AgoAmount)!=0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmountBFMRRArray[index])==0)&&(parseFloat(totalAmount1MRRArray[index])!=0))
                {
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    disco2MTTotal=parseFloat(disco2MTTotal)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                }
                else{
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    don2MTTotal=parseFloat(don2MTTotal)+parseFloat((-1))*parseFloat(MTH1AgoAmount);

                }
            }

        }
        else{
            if(parseInt(bfMTHQTY)<parseInt(MTH1AgoQTY)){

                var totalamInvQ=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                if(totalamInvQ<0)
                {
                    don2MTTotal=parseFloat(don2MTTotal)+parseFloat(totalamInvQ);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
            }
            else if(parseFloat(bfMTHAmount) < parseFloat( MTH1AgoAmount)){


                var totalamInvQ1=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                dec2MTTotal=parseFloat(dec2MTTotal)+parseFloat(totalamInvQ1);
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
            }
        }

        //3 MT AGO
        if((parseFloat(MTH1AgoAmount)==0)&&(parseFloat(MTH2AgoAmount)!=0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount1MRRArray[index])==0)&&(parseFloat(totalAmount2MRRArray[index])!=0))
                {

                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    disco3MTTotal=parseFloat(disco3MTTotal)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                }
                else{
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    don3MTTotal=parseFloat(don3MTTotal)+parseFloat((-1))*parseFloat(MTH2AgoAmount);

                }
            }
        }
        else{
            if(parseInt(MTH1AgoQTY)<parseInt(MTH2AgoQTY)){

                var totalamInvQ=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                if(totalamInvQ<0)
                {
                    don3MTTotal=parseFloat(don3MTTotal)+parseFloat(totalamInvQ);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
            }
            else if(parseFloat(MTH1AgoAmount) <parseFloat( MTH2AgoAmount)){


                var totalamInvQ1=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                dec3MTTotal=parseFloat(dec3MTTotal)+parseFloat(totalamInvQ1);
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
            }
        }
//4 MT AGO
        if((parseFloat(MTH2AgoAmount)==0)&&(parseFloat(MTH3AgoAmount)!=0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount2MRRArray[index])==0)&&(parseFloat(totalAmount3MRRArray[index])!=0))
                {
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    disco4MTTotal=parseFloat(disco4MTTotal)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                }
                else{
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    don4MTTotal=parseFloat(don4MTTotal)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                }
            }
        }
        else{
            if(parseInt(MTH2AgoQTY)<parseInt(MTH3AgoQTY)){

                var totalamInvQ=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                if(totalamInvQ<0)
                {
                    don4MTTotal=parseFloat(don4MTTotal)+parseFloat(totalamInvQ);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
            }
            else if(parseFloat(MTH2AgoAmount) < parseFloat(MTH3AgoAmount)){


                var totalamInvQ1=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                dec4MTTotal=parseFloat(dec4MTTotal)+parseFloat(totalamInvQ1);
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
            }
        }
        //5 MT AGO
        if((parseFloat(MTH3AgoAmount)==0)&&(parseFloat(MTH4AgoAmount)!=0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount3MRRArray[index])==0)&&(parseFloat(totalAmount4MRRArray[index])!=0))
                {
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    disco5MTTotal=parseFloat(disco5MTTotal)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                }
                else{
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    don5MTTotal=parseFloat(don5MTTotal)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                }
            }
        }
        else{
            if(parseInt(MTH3AgoQTY)<parseInt(MTH4AgoQTY)){

                var totalamInvQ=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                if(totalamInvQ<0)
                {
                    don5MTTotal=parseFloat(don5MTTotal)+parseFloat(totalamInvQ);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
            }
            else if(parseFloat(MTH3AgoAmount) <parseFloat( MTH4AgoAmount)){


                var totalamInvQ1=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                dec5MTTotal=parseFloat(dec5MTTotal)+parseFloat(totalamInvQ1);
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
            }
        }
        //6 MT AGO
        if((parseFloat(MTH4AgoAmount)==0)&&(parseFloat(MTH5AgoAmount)!=0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount4MRRArray[index])==0)&&(parseFloat(totalAmount5MRRArray[index])!=0))
                {
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    disco6MTTotal=parseFloat(disco6MTTotal)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                }
                else{
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    don6MTTotal=parseFloat(don6MTTotal)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                }
            }
        }
        else{
            if(parseInt(MTH4AgoQTY)<parseInt(MTH5AgoQTY)){

                var totalamInvQ=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                if(totalamInvQ<0)
                {
                    don6MTTotal=parseFloat(don6MTTotal)+parseFloat(totalamInvQ);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
            }
            else if(parseFloat(MTH4AgoAmount) < parseFloat(MTH5AgoAmount)){


                var totalamInvQ1=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                dec6MTTotal=parseFloat(dec6MTTotal)+parseFloat(totalamInvQ1);
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
            }
        }
        return true; // return true to keep iterating

    });
    if(marketid==0) {
//NNJ
        var searchInvoices = nlapiLoadSearch('transaction', 'customsearch_clgx_inv_revenuegr_all_4');
        searchInvoices.addColumns(arrColumnsNewMRR1);
        searchInvoices.addFilters(arrFiltersNewMRR1);
        var resultINV = searchInvoices.runSearch();
        resultINV.forEachResult(function (searchResult) {
            var columns = searchResult.getAllColumns();
            var customerID = searchResult.getValue(columns[0]);
            var customerName = searchResult.getValue(columns[1]);
            var locationInv = searchResult.getText(columns[2]);
            var currMTHAmount = searchResult.getValue(columns[3]);
            var bfMTHAmount = searchResult.getValue(columns[4]);
            var MTH1AgoAmount = searchResult.getValue(columns[5]);
            var MTH2AgoAmount = searchResult.getValue(columns[6]);
            var MTH3AgoAmount = searchResult.getValue(columns[7]);
            var MTH4AgoAmount = searchResult.getValue(columns[8]);
            var MTH5AgoAmount = searchResult.getValue(columns[9]);
            var currMTHQTY = searchResult.getValue(columns[10]);
            var bfMTHQTY = searchResult.getValue(columns[11]);
            var MTH1AgoQTY = searchResult.getValue(columns[12]);
            var MTH2AgoQTY = searchResult.getValue(columns[13]);
            var MTH3AgoQTY = searchResult.getValue(columns[14]);
            var MTH4AgoQTY = searchResult.getValue(columns[15]);
            var MTH5AgoQTY = searchResult.getValue(columns[16]);
            var categoryInv1 = searchResult.getText(columns[17]);
            var currencyINV1 = searchResult.getText(columns[18]);
            var arrDates_1 = getDateRange(-1);
            var startDate_1 = arrDates_1[0];
            var arrDates_2 = getDateRange(-2);
            var startDate_2 = arrDates_2[0];
            var arrDates_3 = getDateRange(-3);
            var startDate_3 = arrDates_3[0];
            var arrDates_4 = getDateRange(-4);
            var startDate_4 = arrDates_4[0];
            var arrDates_5 = getDateRange(-5);
            var startDate_5 = arrDates_5[0];
            var arrDates_6 = getDateRange(-6);
            var startDate_6 = arrDates_6[0];
            var index = 0;
            var test = customerName + ';' + locationInv;
            index = _.indexOf(locationMRRArray, test);
            /* for ( var l = 0;customerMRRArray  != null && l < customerMRRArray.length; l++ ) {

             if(customerMRRArray[l]==customerName)
             {
             index=l;
             }
             }*/
            //1  MT AGO
            if ((parseFloat(currMTHAmount) != 0) && (parseFloat(bfMTHAmount) == 0)) {

                //var emailSubject = 'July Revenue3';
                // var emailBody="Values: "+locationMRRArray[0];
                // nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null); // Send email to Catalina

                if (index > 0) {
                    if ((parseFloat(totalAmountCurrMRRArray[index]) != 0) && (parseFloat(totalAmountBFMRRArray[index]) == 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"New MRR","Date":"' + startDate_1 + '", "Amount":"' + parseFloat(currMTHAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        newMRR1MTTotal = parseFloat(newMRR1MTTotal) + parseFloat(currMTHAmount);
                    }
                    else {
                        upMRR1MTTotal = parseFloat(upMRR1MTTotal) + parseFloat(currMTHAmount);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_1 + '", "Amount":"' + parseFloat(currMTHAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }
            else {
                if (parseInt(currMTHQTY) > parseInt(bfMTHQTY)) {

                    var totalamInvQ = parseFloat(currMTHAmount) - parseFloat(bfMTHAmount);
                    if (totalamInvQ > 0) {
                        upMRR1MTTotal = parseFloat(upMRR1MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_1 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                    }


                }
                else if((parseFloat(currMTHAmount) > parseFloat(bfMTHAmount))&&(parseInt(currMTHQTY)==parseInt(bfMTHQTY))){


                    var totalamInvQ1 = parseFloat(currMTHAmount) - parseFloat(bfMTHAmount);
                    if (totalamInvQ1 > 0) {
                        prMRR1MTTotal = parseFloat(prMRR1MTTotal) + parseFloat(totalamInvQ1);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"' + startDate_1 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }
            //2  MT AGO
            if ((parseFloat(bfMTHAmount) != 0) && (parseFloat(MTH1AgoAmount) == 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmountBFMRRArray[index]) != 0) && (parseFloat(totalAmount1MRRArray[index]) == 0)) {

                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"New MRR","Date":"' + startDate_2 + '", "Amount":"' + parseFloat(bfMTHAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        newMRR2MTTotal = parseFloat(newMRR2MTTotal) + parseFloat(bfMTHAmount);
                    }
                    else {
                        upMRR2MTTotal = parseFloat(upMRR2MTTotal) + parseFloat(bfMTHAmount);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_2 + '", "Amount":"' + parseFloat(bfMTHAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }
            else {
                if (parseInt(bfMTHQTY) > parseInt(MTH1AgoQTY)) {

                    var totalamInvQ = parseFloat(bfMTHAmount) - parseFloat(MTH1AgoAmount);
                    if (totalamInvQ > 0) {
                        upMRR2MTTotal = parseFloat(upMRR2MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_2 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                    }


                }
                else if (parseFloat(bfMTHAmount) > parseFloat(MTH1AgoAmount)) {


                    var totalamInvQ1 = parseFloat(bfMTHAmount) - parseFloat(MTH1AgoAmount);
                    if (totalamInvQ1 > 0) {
                        prMRR2MTTotal = parseFloat(prMRR2MTTotal) + parseFloat(totalamInvQ1);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"' + startDate_2 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }

            //3 MT AGO
            if ((parseFloat(MTH1AgoAmount) != 0) && (parseFloat(MTH2AgoAmount) == 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmount1MRRArray[index]) != 0) && (parseFloat(totalAmount2MRRArray[index]) == 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"New MRR","Date":"' + startDate_3 + '", "Amount":"' + parseFloat(MTH1AgoAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        newMRR3MTTotal = parseFloat(newMRR3MTTotal) + parseFloat(MTH1AgoAmount);
                    }
                    else {
                        upMRR3MTTotal = parseFloat(upMRR3MTTotal) + parseFloat(MTH1AgoAmount);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_3 + '", "Amount":"' + parseFloat(MTH1AgoAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }
            else {
                if (parseInt(MTH1AgoQTY) > parseInt(MTH2AgoQTY)) {

                    var totalamInvQ = parseFloat(MTH1AgoAmount) - parseFloat(MTH2AgoAmount);
                    if (totalamInvQ > 0) {
                        upMRR3MTTotal = parseFloat(upMRR3MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_3 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                    }


                }
                else if (parseFloat(MTH1AgoAmount) > parseFloat(MTH2AgoAmount)) {


                    var totalamInvQ1 = parseFloat(MTH1AgoAmount) - parseFloat(MTH2AgoAmount);
                    if (totalamInvQ1 > 0) {
                        prMRR3MTTotal = parseFloat(prMRR3MTTotal) + parseFloat(totalamInvQ1);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"' + startDate_3 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }
//4 MT AGO
            if ((parseFloat(MTH2AgoAmount) != 0) && (parseFloat(MTH3AgoAmount) == 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmount2MRRArray[index]) != 0) && (parseFloat(totalAmount3MRRArray[index]) == 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"New MRR","Date":"' + startDate_4 + '", "Amount":"' + parseFloat(MTH2AgoAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        newMRR4MTTotal = parseFloat(newMRR4MTTotal) + parseFloat(MTH2AgoAmount);
                    }
                    else {
                        upMRR4MTTotal = parseFloat(upMRR4MTTotal) + parseFloat(MTH2AgoAmount);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_4 + '", "Amount":"' + parseFloat(MTH2AgoAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                    }
                }
            }
            else {
                if (parseInt(MTH2AgoQTY) > parseInt(MTH3AgoQTY)) {

                    var totalamInvQ = parseFloat(MTH2AgoAmount) - parseFloat(MTH3AgoAmount);
                    if (totalamInvQ > 0) {
                        upMRR4MTTotal = parseFloat(upMRR4MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_4 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                    }


                }
                else if (parseFloat(MTH2AgoAmount) > parseFloat(MTH3AgoAmount)) {


                    var totalamInvQ1 = parseFloat(MTH2AgoAmount) - parseFloat(MTH3AgoAmount);
                    if (totalamInvQ1 > 0) {
                        prMRR4MTTotal = parseFloat(prMRR4MTTotal) + parseFloat(totalamInvQ1);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"' + startDate_4 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }
            //5 MT AGO
            if ((parseFloat(MTH3AgoAmount) != 0) && (parseFloat(MTH4AgoAmount) == 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmount3MRRArray[index]) != 0) && (parseFloat(totalAmount4MRRArray[index]) == 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"New MRR","Date":"' + startDate_5 + '", "Amount":"' + parseFloat(MTH3AgoAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        newMRR5MTTotal = parseFloat(newMRR5MTTotal) + parseFloat(MTH3AgoAmount);
                    }
                    else {
                        upMRR5MTTotal = parseFloat(upMRR5MTTotal) + parseFloat(MTH3AgoAmount);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_5 + '", "Amount":"' + parseFloat(MTH3AgoAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }
            else {
                if (parseInt(MTH3AgoQTY) > parseInt(MTH4AgoQTY)) {

                    var totalamInvQ = parseFloat(MTH3AgoAmount) - parseFloat(MTH4AgoAmount);
                    if (totalamInvQ > 0) {
                        upMRR5MTTotal = parseFloat(upMRR5MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_5 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                    }


                }
                else if (parseFloat(MTH3AgoAmount) > parseFloat(MTH4AgoAmount)) {


                    var totalamInvQ1 = parseFloat(MTH3AgoAmount) - parseFloat(MTH4AgoAmount);
                    if (totalamInvQ1 > 0) {
                        prMRR5MTTotal = parseFloat(prMRR5MTTotal) + parseFloat(totalamInvQ1);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"' + startDate_5 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }
            //6 MT AGO
            if ((parseFloat(MTH4AgoAmount) != 0) && (parseFloat(MTH5AgoAmount) == 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmount4MRRArray[index]) != 0) && (parseFloat(totalAmount5MRRArray[index]) == 0)) {

                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"New MRR","Date":"' + startDate_6 + '", "Amount":"' + parseFloat(MTH4AgoAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        newMRR6MTTotal = parseFloat(newMRR6MTTotal) + parseFloat(MTH4AgoAmount);
                    }
                    else {
                        upMRR6MTTotal = parseFloat(upMRR6MTTotal) + parseFloat(MTH4AgoAmount);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_6 + '", "Amount":"' + parseFloat(MTH4AgoAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                    }
                }
            }
            else {
                if (parseInt(MTH4AgoQTY) > parseInt(MTH5AgoQTY)) {

                    var totalamInvQ = parseFloat(MTH4AgoAmount) - parseFloat(MTH5AgoAmount);
                    if (totalamInvQ > 0) {
                        upMRR6MTTotal = parseFloat(upMRR6MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_6 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                    }


                }
                else if (parseFloat(MTH4AgoAmount) > parseFloat(MTH5AgoAmount)) {


                    var totalamInvQ1 = parseFloat(MTH4AgoAmount) - parseFloat(MTH5AgoAmount);
                    if (totalamInvQ1 > 0) {
                        prMRR6MTTotal = parseFloat(prMRR6MTTotal) + parseFloat(totalamInvQ1);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"' + startDate_6 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }

            //CHURN

            //1  MT AGO
            if ((parseFloat(currMTHAmount) == 0) && (parseFloat(bfMTHAmount) != 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmountCurrMRRArray[index]) == 0) && (parseFloat(totalAmountBFMRRArray[index]) != 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Disconnect","Date":"' + startDate_1 + '", "Amount":"' + parseFloat(((-1) * bfMTHAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                        disco1MTTotal = parseFloat(disco1MTTotal) + parseFloat((-1)) * parseFloat(bfMTHAmount);
                    }
                    else {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_1 + '", "Amount":"' + parseFloat(((-1) * bfMTHAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        don1MTTotal = parseFloat(don1MTTotal) + parseFloat((-1)) * parseFloat(bfMTHAmount);
                    }
                }
            }
            else {
                if (parseInt(currMTHQTY) < parseInt(bfMTHQTY)) {

                    var totalamInvQ = parseFloat(currMTHAmount) - parseFloat(bfMTHAmount);
                    if (totalamInvQ < 0) {
                        don1MTTotal = parseFloat(don1MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_1 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
                else if (parseFloat(currMTHAmount) < parseFloat(bfMTHAmount)) {


                    var totalamInvQ1 = parseFloat(currMTHAmount) - parseFloat(bfMTHAmount);
                    if (totalamInvQ1 > 0) {
                        totalamInvQ1 = totalamInvQ1 * (-1);
                    }
                    dec1MTTotal = parseFloat(dec1MTTotal) + parseFloat(totalamInvQ1);
                    stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Decrease","Date":"' + startDate_1 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                }
            }
            //2  MT AGO
            if ((parseFloat(bfMTHAmount) == 0) && (parseFloat(MTH1AgoAmount) != 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmountBFMRRArray[index]) == 0) && (parseFloat(totalAmount1MRRArray[index]) != 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Disconnect","Date":"' + startDate_2 + '", "Amount":"' + parseFloat(((-1) * MTH1AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        disco2MTTotal = parseFloat(disco2MTTotal) + parseFloat((-1)) * parseFloat(MTH1AgoAmount);
                    }
                    else {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_2 + '", "Amount":"' + parseFloat(((-1) * MTH1AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        don2MTTotal = parseFloat(don2MTTotal) + parseFloat((-1)) * parseFloat(MTH1AgoAmount);

                    }
                }

            }
            else {
                if (parseInt(bfMTHQTY) < parseInt(MTH1AgoQTY)) {

                    var totalamInvQ = parseFloat(bfMTHAmount) - parseFloat(MTH1AgoAmount);
                    if (totalamInvQ < 0) {
                        don2MTTotal = parseFloat(don2MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_2 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
                else if (parseFloat(bfMTHAmount) < parseFloat(MTH1AgoAmount)) {


                    var totalamInvQ1 = parseFloat(bfMTHAmount) - parseFloat(MTH1AgoAmount);
                    if (totalamInvQ1 > 0) {
                        totalamInvQ1 = totalamInvQ1 * (-1);
                    }
                    dec2MTTotal = parseFloat(dec2MTTotal) + parseFloat(totalamInvQ1);
                    stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Decrease","Date":"' + startDate_2 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                }
            }

            //3 MT AGO
            if ((parseFloat(MTH1AgoAmount) == 0) && (parseFloat(MTH2AgoAmount) != 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmount1MRRArray[index]) == 0) && (parseFloat(totalAmount2MRRArray[index]) != 0)) {

                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Disconnect","Date":"' + startDate_3 + '", "Amount":"' + parseFloat(((-1) * MTH2AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        disco3MTTotal = parseFloat(disco3MTTotal) + parseFloat((-1)) * parseFloat(MTH2AgoAmount);
                    }
                    else {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_3 + '", "Amount":"' + parseFloat(((-1) * MTH2AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        don3MTTotal = parseFloat(don3MTTotal) + parseFloat((-1)) * parseFloat(MTH2AgoAmount);

                    }
                }
            }
            else {
                if (parseInt(MTH1AgoQTY) < parseInt(MTH2AgoQTY)) {

                    var totalamInvQ = parseFloat(MTH1AgoAmount) - parseFloat(MTH2AgoAmount);
                    if (totalamInvQ < 0) {
                        don3MTTotal = parseFloat(don3MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_3 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
                else if (parseFloat(MTH1AgoAmount) < parseFloat(MTH2AgoAmount)) {


                    var totalamInvQ1 = parseFloat(MTH1AgoAmount) - parseFloat(MTH2AgoAmount);
                    if (totalamInvQ1 > 0) {
                        totalamInvQ1 = totalamInvQ1 * (-1);
                    }
                    dec3MTTotal = parseFloat(dec3MTTotal) + parseFloat(totalamInvQ1);
                    stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Decrease","Date":"' + startDate_3 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                }
            }
//4 MT AGO
            if ((parseFloat(MTH2AgoAmount) == 0) && (parseFloat(MTH3AgoAmount) != 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmount2MRRArray[index]) == 0) && (parseFloat(totalAmount3MRRArray[index]) != 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Disconnect","Date":"' + startDate_4 + '", "Amount":"' + parseFloat(((-1) * MTH3AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        disco4MTTotal = parseFloat(disco4MTTotal) + parseFloat((-1)) * parseFloat(MTH3AgoAmount);
                    }
                    else {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_4 + '", "Amount":"' + parseFloat(((-1) * MTH3AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        don4MTTotal = parseFloat(don4MTTotal) + parseFloat((-1)) * parseFloat(MTH3AgoAmount);
                    }
                }
            }
            else {
                if (parseInt(MTH2AgoQTY) < parseInt(MTH3AgoQTY)) {

                    var totalamInvQ = parseFloat(MTH2AgoAmount) - parseFloat(MTH3AgoAmount);
                    if (totalamInvQ < 0) {
                        don4MTTotal = parseFloat(don4MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_4 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
                else if (parseFloat(MTH2AgoAmount) < parseFloat(MTH3AgoAmount)) {


                    var totalamInvQ1 = parseFloat(MTH2AgoAmount) - parseFloat(MTH3AgoAmount);
                    if (totalamInvQ1 > 0) {
                        totalamInvQ1 = totalamInvQ1 * (-1);
                    }
                    dec4MTTotal = parseFloat(dec4MTTotal) + parseFloat(totalamInvQ1);
                    stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Decrease","Date":"' + startDate_4 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                }
            }
            //5 MT AGO
            if ((parseFloat(MTH3AgoAmount) == 0) && (parseFloat(MTH4AgoAmount) != 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmount3MRRArray[index]) == 0) && (parseFloat(totalAmount4MRRArray[index]) != 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Disconnect","Date":"' + startDate_5 + '", "Amount":"' + parseFloat(((-1) * MTH4AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        disco5MTTotal = parseFloat(disco5MTTotal) + parseFloat((-1)) * parseFloat(MTH4AgoAmount);
                    }
                    else {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_5 + '", "Amount":"' + parseFloat(((-1) * MTH4AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        don5MTTotal = parseFloat(don5MTTotal) + parseFloat((-1)) * parseFloat(MTH4AgoAmount);
                    }
                }
            }
            else {
                if (parseInt(MTH3AgoQTY) < parseInt(MTH4AgoQTY)) {

                    var totalamInvQ = parseFloat(MTH3AgoAmount) - parseFloat(MTH4AgoAmount);
                    if (totalamInvQ < 0) {
                        don5MTTotal = parseFloat(don5MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_5 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
                else if (parseFloat(MTH3AgoAmount) < parseFloat(MTH4AgoAmount)) {


                    var totalamInvQ1 = parseFloat(MTH3AgoAmount) - parseFloat(MTH4AgoAmount);
                    if (totalamInvQ1 > 0) {
                        totalamInvQ1 = totalamInvQ1 * (-1);
                    }
                    dec5MTTotal = parseFloat(dec5MTTotal) + parseFloat(totalamInvQ1);
                    stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Decrease","Date":"' + startDate_5 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                }
            }
            //6 MT AGO
            if ((parseFloat(MTH4AgoAmount) == 0) && (parseFloat(MTH5AgoAmount) != 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmount4MRRArray[index]) == 0) && (parseFloat(totalAmount5MRRArray[index]) != 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Disconnect","Date":"' + startDate_6 + '", "Amount":"' + parseFloat(((-1) * MTH5AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        disco6MTTotal = parseFloat(disco6MTTotal) + parseFloat((-1)) * parseFloat(MTH5AgoAmount);
                    }
                    else {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_6 + '", "Amount":"' + parseFloat(((-1) * MTH5AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        don6MTTotal = parseFloat(don6MTTotal) + parseFloat((-1)) * parseFloat(MTH5AgoAmount);
                    }
                }
            }
            else {
                if (parseInt(MTH4AgoQTY) < parseInt(MTH5AgoQTY)) {

                    var totalamInvQ = parseFloat(MTH4AgoAmount) - parseFloat(MTH5AgoAmount);
                    if (totalamInvQ < 0) {
                        don6MTTotal = parseFloat(don6MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_6 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
                else if (parseFloat(MTH4AgoAmount) < parseFloat(MTH5AgoAmount)) {


                    var totalamInvQ1 = parseFloat(MTH4AgoAmount) - parseFloat(MTH5AgoAmount);
                    if (totalamInvQ1 > 0) {
                        totalamInvQ1 = totalamInvQ1 * (-1);
                    }
                    dec6MTTotal = parseFloat(dec6MTTotal) + parseFloat(totalamInvQ1);
                    stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Decrease","Date":"' + startDate_6 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                }
            }
            return true; // return true to keep iterating

        });
    }
    stringseries += '\n{name: \'New MRR\',type: \'column\',  data: \n[\n';
    stringseries += '{y:' + parseFloat(newMRR6MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=newmrr&typelabel=New MRR&month=-5&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(newMRR5MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=newmrr&typelabel=New MRR&month=-4&scriptid=2&product=0&productlabel=na'+'\'},\n';
    stringseries += '{y:' + parseFloat(newMRR4MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=newmrr&typelabel=New MRR&month=-3&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(newMRR3MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=newmrr&typelabel=New MRR&month=-2&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(newMRR2MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=newmrr&typelabel=New MRR&month=-1&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(newMRR1MTTotal).toFixed(2) +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=newmrr&typelabel=New MRR&month=0&scriptid=2&product=0&productlabel=na'+'\'}';

    stringseries += ']\n';
    stringseries += '},\n';

    stringseries += '\n{name: \'Disconnect\',type: \'column\',  data: \n[\n';
    stringseries += '{y:' + parseFloat(disco6MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=disco&typelabel=Disconnect&month=-5&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(disco5MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=disco&typelabel=Disconnect&month=-4&scriptid=2&product=0&productlabel=na'+'\'},\n';
    stringseries += '{y:' + parseFloat(disco4MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=disco&typelabel=Disconnect&month=-3&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(disco3MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=disco&typelabel=Disconnect&month=-2&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(disco2MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=disco&typelabel=Disconnect&month=-1&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(disco1MTTotal).toFixed(2) +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=disco&typelabel=Disconnect&month=0&scriptid=2&product=0&productlabel=na'+'\'}';
    stringseries += ']\n';
    stringseries += '},\n';

    stringseries += '\n{name: \'Decrease\',type: \'column\',  data: \n[\n';
    stringseries += '{y:' + parseFloat(dec6MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=dec&typelabel=Decrease&month=-5&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(dec5MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=dec&typelabel=Decrease&month=-4&scriptid=2&product=0&productlabel=na'+'\'},\n';
    stringseries += '{y:' + parseFloat(dec4MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=dec&typelabel=Decrease&month=-3&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(dec3MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=dec&typelabel=Decrease&month=-2&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(dec2MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=dec&typelabel=Decrease&month=-1&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(dec1MTTotal).toFixed(2) +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=dec&typelabel=Decrease&month=0&scriptid=2&product=0&productlabel=na'+'\'}';
    stringseries += ']\n';
    stringseries += '},\n';

    stringseries += '\n{name: \'Downgrade\',type: \'column\',  data: \n[\n';
    stringseries += '{y:' + parseFloat(don6MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=down&typelabel=Downgrade&month=-5&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(don5MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=down&typelabel=Downgrade&month=-4&scriptid=2&product=0&productlabel=na'+'\'},\n';
    stringseries += '{y:' + parseFloat(don4MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=down&typelabel=Downgrade&month=-3&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(don3MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=down&typelabel=Downgrade&month=-2&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(don2MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=down&typelabel=Downgrade&month=-1&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(don1MTTotal).toFixed(2) +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=down&typelabel=Downgrade&month=0&scriptid=2&product=0&productlabel=na'+'\'}';
    stringseries += ']\n';
    stringseries += '},\n';
    stringseries += '\n{name: \'Price Increase\',type: \'column\',  data: \n[\n';
    stringseries += '{y:' + parseFloat(prMRR6MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=price&typelabel=Price Increase&month=-5&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(prMRR5MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=price&typelabel=Price Increase&month=-4&scriptid=2&product=0&productlabel=na'+'\'},\n';
    stringseries += '{y:' + parseFloat(prMRR4MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=price&typelabel=Price Increase&month=-3&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(prMRR3MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=price&typelabel=Price Increase&month=-2&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(prMRR2MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=price&typelabel=Price Increase&month=-1&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(prMRR1MTTotal).toFixed(2) +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=price&typelabel=Price Increase&month=0&scriptid=2&product=0&productlabel=na'+'\'}';
    stringseries += ']\n';
    stringseries += '},\n';
    stringseries += '\n{name: \'Upgrade\',type: \'column\',  data: \n[\n';
    stringseries += '{y:' + parseFloat(upMRR6MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=up&typelabel=Upgrade&month=-5&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(upMRR5MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=up&typelabel=Upgrade&month=-4&scriptid=2&product=0&productlabel=na'+'\'},\n';
    stringseries += '{y:' + parseFloat(upMRR4MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=up&typelabel=Upgrade&month=-3&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(upMRR3MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=up&typelabel=Upgrade&month=-2&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(upMRR2MTTotal).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=up&typelabel=Upgrade&month=-1&scriptid=2&product=0&productlabel=na' +'\'},\n';
    stringseries += '{y:' + parseFloat(upMRR1MTTotal).toFixed(2) +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=up&typelabel=Upgrade&month=0&scriptid=2&product=0&productlabel=na'+'\'}';
    stringseries += ']\n';
    stringseries += '},\n';
    newMRR6MTTotal=parseFloat(newMRR6MTTotal).toFixed(2);
    disco6MTTotal=parseFloat(disco6MTTotal).toFixed(2);
    dec6MTTotal=parseFloat(dec6MTTotal).toFixed(2);
    don6MTTotal=parseFloat(don6MTTotal).toFixed(2);
    prMRR6MTTotal=parseFloat(prMRR6MTTotal).toFixed(2);
    upMRR6MTTotal=parseFloat(upMRR6MTTotal).toFixed(2);
    var total6MT=(parseFloat(newMRR6MTTotal)+parseFloat(disco6MTTotal)+parseFloat(dec6MTTotal)+parseFloat(don6MTTotal)+parseFloat(prMRR6MTTotal)+parseFloat(upMRR6MTTotal));
    newMRR5MTTotal=parseFloat(newMRR5MTTotal).toFixed(2);
    disco5MTTotal=parseFloat(disco5MTTotal).toFixed(2);
    dec5MTTotal=parseFloat(dec5MTTotal).toFixed(2);
    don5MTTotal=parseFloat(don5MTTotal).toFixed(2);
    prMRR5MTTotal=parseFloat(prMRR5MTTotal).toFixed(2);
    upMRR5MTTotal=parseFloat(upMRR5MTTotal).toFixed(2);
    var total5MT=(parseFloat(newMRR5MTTotal)+parseFloat(disco5MTTotal)+parseFloat(dec5MTTotal)+parseFloat(don5MTTotal)+parseFloat(prMRR5MTTotal)+parseFloat(upMRR5MTTotal));
    newMRR4MTTotal=parseFloat(newMRR4MTTotal).toFixed(2);
    disco4MTTotal=parseFloat(disco4MTTotal).toFixed(2);
    dec4MTTotal=parseFloat(dec4MTTotal).toFixed(2);
    don4MTTotal=parseFloat(don4MTTotal).toFixed(2);
    prMRR4MTTotal=parseFloat(prMRR4MTTotal).toFixed(2);
    upMRR4MTTotal=parseFloat(upMRR4MTTotal).toFixed(2);
    var total4MT=(parseFloat(newMRR4MTTotal)+parseFloat(disco4MTTotal)+parseFloat(dec4MTTotal)+parseFloat(don4MTTotal)+parseFloat(prMRR4MTTotal)+parseFloat(upMRR4MTTotal));
    newMRR3MTTotal=parseFloat(newMRR3MTTotal).toFixed(2);
    disco3MTTotal=parseFloat(disco3MTTotal).toFixed(2);
    dec3MTTotal=parseFloat(dec3MTTotal).toFixed(2);
    don3MTTotal=parseFloat(don3MTTotal).toFixed(2);
    prMRR3MTTotal=parseFloat(prMRR3MTTotal).toFixed(2);
    upMRR3MTTotal=parseFloat(upMRR3MTTotal).toFixed(2);
    var total3MT=(parseFloat(newMRR3MTTotal)+parseFloat(disco3MTTotal)+parseFloat(dec3MTTotal)+parseFloat(don3MTTotal)+parseFloat(prMRR3MTTotal)+parseFloat(upMRR3MTTotal));
    newMRR2MTTotal=parseFloat(newMRR2MTTotal).toFixed(2);
    disco2MTTotal=parseFloat(disco2MTTotal).toFixed(2);
    dec2MTTotal=parseFloat(dec2MTTotal).toFixed(2);
    don2MTTotal=parseFloat(don2MTTotal).toFixed(2);
    prMRR2MTTotal=parseFloat(prMRR2MTTotal).toFixed(2);
    upMRR2MTTotal=parseFloat(upMRR2MTTotal).toFixed(2);
    var total2MT=(parseFloat(newMRR2MTTotal)+parseFloat(disco2MTTotal)+parseFloat(dec2MTTotal)+parseFloat(don2MTTotal)+parseFloat(prMRR2MTTotal)+parseFloat(upMRR2MTTotal));
    newMRR1MTTotal=parseFloat(newMRR1MTTotal).toFixed(2);
    disco1MTTotal=parseFloat(disco1MTTotal).toFixed(2);
    dec1MTTotal=parseFloat(dec1MTTotal).toFixed(2);
    don1MTTotal=parseFloat(don1MTTotal).toFixed(2);
    prMRR1MTTotal=parseFloat(prMRR1MTTotal).toFixed(2);
    upMRR1MTTotal=parseFloat(upMRR1MTTotal).toFixed(2);
    var total1MT=(parseFloat(newMRR1MTTotal)+parseFloat(disco1MTTotal)+parseFloat(dec1MTTotal)+parseFloat(don1MTTotal)+parseFloat(prMRR1MTTotal)+parseFloat(upMRR1MTTotal));
    // var emailSubject = 'July Revenue';
    // var emailBody="Values: "+total6MT;
    // nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null); // Send email to Catalina

    var returnArray=[stringCSV,stringseries,total6MT,total5MT,total4MT,total3MT,total2MT,total1MT];
    return returnArray;

}

function getSeriesCSVPU(marketid,search,curr, bf,cat){

    var arrFilters=new Array();
    var arrColumns=new Array();
    if(marketid!=0)
    {
        if(marketid=='25')
        {
            //Montreal
            var marketidlist=["5", "8", "9", "10", "11", "12", "27"];

        }
        if(marketid=='23')
        {
            //Toronto
            var marketidlist=["6", "13", "15"];

        }
        if(marketid=='26')
        {
            //Vancouver
            var marketidlist=["28", "7"];

        }
        if(marketid=='22')
        {
            //Minneapolis
            var marketidlist=["16", "35"];
        }
        if(marketid=='29')
        {
            //Jacksonville
            var marketidlist=["31"];
        }
        if(marketid=='20')
        {
            //Dallas
            var marketidlist=["2", "17"];
        }
        if(marketid=='33')
        {
            //Columbus
            var marketidlist=["38", "34","39"];
        }
        if(marketid=='41')
        {
            //Lakeland
            var marketidlist=["42"];
        }
        if(marketid=='43')
        {
            //Jacksonville2
            var marketidlist=["40"];
        }
        if(marketid=='52')
        {
            //Parsippany
            var marketidlist=["53","54","55","56"];
        }
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",marketidlist));


    }
    var arrDates  = getDateRange(curr);
    var arrDates1  = getDateRange(bf);
    var startDate = arrDates1[0];
    var endDate  = arrDates[1];
    arrFilters.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    var searchInvoices = nlapiSearchRecord('transaction', search, arrFilters, arrColumns);
    var stringCSV='';
    var stringseries='';
    var powTotalMT=0;
    if(searchInvoices!=null)
    {
        for ( var i = 0;searchInvoices  != null && i < searchInvoices.length; i++ ) {
            var searchInv = searchInvoices [i];
            var columns = searchInv.getAllColumns();
            var totalAmountCurr=parseFloat(searchInv.getValue(columns[2]));
            var totalAmount1MTHAgo=parseFloat(searchInv.getValue(columns[3]));
            var totalQtyCurr=searchInv.getValue(columns[4]);
            var totalQty1MTHAgo=searchInv.getValue(columns[5]);
            var customerID=searchInv.getValue(columns[0]);
            var customerName=searchInv.getText(columns[1]);
            var locationInv=searchInv.getText(columns[6]);
            var internalID=searchInv.getValue(columns[7]);
            var dateInv=searchInv.getValue(columns[8]);
            var amountInv=searchInv.getValue(columns[9]);
            var currencyInv=searchInv.getValue(columns[10]);
            var categoryInv=searchInv.getText(columns[11]);
            var internalIDArray=internalID.split(',');
            var dateInvArray=dateInv.split(',');
            var amountInvArray=amountInv.split(',');
            var currencyInvArray=currencyInv.split(',');
            var splitdate=endDate.split('/');
            var currMTH=splitdate[0];
            var splitdate1=startDate.split('/');
            var beforeMTH=splitdate1[0];
            var totalamInv=0;
            var totalamInvB=0;
            var totalamInv1=0;
            var totalamInvB1=0;
            var totalpowerUsage=parseFloat(totalAmountCurr)-parseFloat(totalAmount1MTHAgo);
            //if(totalAmountCurr!=0)
            // {
            //   for(var j = 0;dateInvArray  != null && j < dateInvArray.length; j++)
            //  {
            //    var splitDateInv=dateInvArray[j].split('/');
            //   if((currMTH==splitDateInv[0])&&(amountInvArray[j]!=0))
            //  {
            if(parseFloat(totalpowerUsage)!=0)
            {
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Usage","Date":"'+startDate+'", "Amount":"'+parseFloat(totalpowerUsage).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv+'","Currency":"'+currencyInvArray[0]+'"},'
                // }

                // }
                //}
                powTotalMT=parseFloat(powTotalMT)+parseFloat(totalpowerUsage);
            }
        }

    }

    var arrayReturn=[stringCSV,powTotalMT];
    return arrayReturn;

}
//Prior Month
function getSeriesCSVPR(marketid,search,curr, bf,cat){

    var arrFilters=new Array();
    var arrColumns=new Array();
    if(marketid!=0)
    {
        if(marketid=='25')
        {
            //Montreal
            var marketidlist=["5", "8", "9", "10", "11", "12", "27"];

        }
        if(marketid=='23')
        {
            //Toronto
            var marketidlist=["6", "13", "15"];

        }
        if(marketid=='26')
        {
            //Vancouver
            var marketidlist=["28", "7"];

        }
        if(marketid=='22')
        {
            //Minneapolis
            var marketidlist=["16", "35"];
        }
        if(marketid=='29')
        {
            //Jacksonville
            var marketidlist=["31"];
        }
        if(marketid=='20')
        {
            //Dallas
            var marketidlist=["2", "17"];
        }
        if(marketid=='33')
        {
            //Columbus
            var marketidlist=["38", "34","39"];
        }
        if(marketid=='41')
        {
            //Lakeland
            var marketidlist=["42"];
        }
        if(marketid=='43')
        {
            //Jacksonville2
            var marketidlist=["40"];
        }
        if(marketid=='52')
        {
            //Parsippany
            var marketidlist=["53","54","55","56"];
        }

        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",marketidlist));


    }
    var arrDates  = getDateRange(curr);
    var arrDates1  = getDateRange(bf);
    //  if(curr==0)
    // {

    //    var startDate = arrDates1[0];
    //    var endDate  = arrDates1[1];
    // }
    // else
    // {
    var startDate = arrDates1[0];
    var endDate  = arrDates[1];
    // }

    var stringCSV='';
    var stringseries='';
    var priTotalMT=0;
    arrFilters.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    var searchInvoices = nlapiLoadSearch('transaction',search);
    searchInvoices.addColumns(arrColumns);
    searchInvoices.addFilters(arrFilters);
    var resultINV = searchInvoices.runSearch();
    resultINV.forEachResult(function(searchResult) {
        var columns = searchResult.getAllColumns();
        var totalAmountCurr=parseFloat(searchResult.getValue(columns[2]));
        var totalAmount1MTHAgo=parseFloat(searchResult.getValue(columns[3]));
        var customerName=searchResult.getText(columns[1]);
        var customerID=searchResult.getValue(columns[1]);
        var locationInv=searchResult.getText(columns[4]);
        var currencyInv=searchResult.getValue(columns[5]);
        var productCat=searchResult.getText(columns[6]);
        var currencyInvArray=new Array();
        if(currencyInv!=null && currencyInv!='')
        {
            currencyInvArray=currencyInv.split(',');
        }
        else
        {
            currencyInvArray[0]='US Dollar';
        }
        var splitdate=endDate.split('/');
        var currMTH=splitdate[0];
        var splitdate1=startDate.split('/');
        var beforeMTH=splitdate1[0];
        if((parseFloat(totalAmountCurr)!=0)||(parseFloat(totalAmount1MTHAgo)!=0))
        {
            var totalpowerUsage=parseFloat(totalAmountCurr)-parseFloat(totalAmount1MTHAgo);
            stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Prior Month", "Category Revenue":"Pro Rate","Date":"'+startDate+'", "Amount":"'+parseFloat(totalpowerUsage).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+productCat+'","Currency":"'+currencyInvArray[0]+'"},'
            // }

            // }
            //}
            priTotalMT=parseFloat(priTotalMT)+parseFloat(totalpowerUsage);
        }
        return true; // return true to keep iterating

    });

    var arrayReturn=[stringCSV,priTotalMT];
    return arrayReturn;
}
function getSeriesCSVCM(marketid,search,curr, bf,cat){

    var arrFilters=new Array();
    var arrColumns=new Array();
    if(marketid!=0)
    {
        if(marketid=='25')
        {
            //Montreal
            var marketidlist=["5", "8", "9", "10", "11", "12", "27"];

        }
        if(marketid=='23')
        {
            //Toronto
            var marketidlist=["6", "13", "15"];

        }
        if(marketid=='26')
        {
            //Vancouver
            var marketidlist=["28", "7"];

        }
        if(marketid=='22')
        {
            //Minneapolis
            var marketidlist=["16", "35"];
        }
        if(marketid=='29')
        {
            //Jacksonville
            var marketidlist=["31"];
        }
        if(marketid=='20')
        {
            //Dallas
            var marketidlist=["2", "17"];
        }
        if(marketid=='33')
        {
            //Columbus
            var marketidlist=["38", "34","39"];
        }
        if(marketid=='41')
        {
            //Lakeland
            var marketidlist=["42"];
        }
        if(marketid=='43')
        {
            //Jacksonville2
            var marketidlist=["40"];
        }
        if(marketid=='52')
        {
            //Parsippany
            var marketidlist=["53","54","55","56"];
        }

        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",marketidlist));


    }
    var arrDates  = getDateRange(curr);
    var arrDates1  = getDateRange(bf);
    var startDate = arrDates1[0];
    var endDate  = arrDates[1];
    arrFilters.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    var searchInvoices = nlapiSearchRecord('transaction', search, arrFilters, arrColumns);
    var stringCSV='';
    var stringseries='';
    var totalAm5T=0;
    var totalAm4T=0;
    var totalAm3T=0;
    var totalAm2T=0;
    var totalAm1T=0;
    var totalAmCMT=0;
    if(searchInvoices!=null)
    {
        for ( var i = 0;searchInvoices  != null && i < searchInvoices.length; i++ ) {
            var searchInv = searchInvoices [i];
            var columns = searchInv.getAllColumns();
            var totalAmountCurr=parseFloat(searchInv.getValue(columns[9]));
            var totalAmountBF=parseFloat(searchInv.getValue(columns[10]));
            var totalAmount2=parseFloat(searchInv.getValue(columns[11]));
            var totalAmount3=parseFloat(searchInv.getValue(columns[12]));
            var totalAmount4=parseFloat(searchInv.getValue(columns[13]));
            var totalAmount5=parseFloat(searchInv.getValue(columns[14]));
            var totalAmount6=parseFloat(searchInv.getValue(columns[15]));
            var categoryInv=searchInv.getText(columns[16]);
            var customerName=searchInv.getValue(columns[1]);
            var locationInv=searchInv.getText(columns[3]);
            var internalID=searchInv.getValue(columns[4]);
            var dateInv=searchInv.getValue(columns[5]);
            var amountInv=searchInv.getValue(columns[6]);
            var currencyInv=searchInv.getValue(columns[7]);
            var itemCategory=searchInv.getValue(columns[8]);
            var internalIDArray=internalID.split(',');
            var dateInvArray=dateInv.split(',');
            var amountInvArray=amountInv.split(',');
            var currencyInvArray=currencyInv.split(',');
            var itemCategoryArray=itemCategory.split(',');
            var splitdate=endDate.split('/');
            var currMTH=splitdate[0];
            var splitdate1=startDate.split('/');
            var beforeMTH=splitdate1[0];
            var totalamInv=0;
            var totalamInvB=0;
            var totalamInv1=0;
            var totalamInvB1=0;
            var totalAmCM=parseFloat(totalAmountCurr)-parseFloat(totalAmountBF);
            var arrDates_1  = getDateRange(-1);
            var startDate_1 = arrDates_1[0];
            var totalAm1=parseFloat(totalAmountBF)-parseFloat(totalAmount2);
            var arrDates_2  = getDateRange(-2);
            var startDate_2 = arrDates_2[0];
            var totalAm2=parseFloat(totalAmount2)-parseFloat(totalAmount3);
            var arrDates_3  = getDateRange(-3);
            var startDate_3 = arrDates_3[0];
            var totalAm3=parseFloat(totalAmount3)-parseFloat(totalAmount4);
            var arrDates_4  = getDateRange(-4);
            var startDate_4 = arrDates_4[0];
            var totalAm4=parseFloat(totalAmount4)-parseFloat(totalAmount5);
            var arrDates_5  = getDateRange(-5);
            var startDate_5 = arrDates_5[0];
            var totalAm5=parseFloat(totalAmount5)-parseFloat(totalAmount6);
            var arrDates_6  = getDateRange(-6);
            var startDate_6 = arrDates_6[0];
            if(parseFloat(totalAmCM)!=0)
            {
                totalAmCMT=parseFloat(totalAmCMT)+parseFloat(totalAmCM);
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Credit Memo", "Category Revenue":"Credit Memo","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalAmCM).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv+'","Currency":"'+currencyInvArray[0]+'"},'
            }
            if(parseFloat(totalAm1)!=0)
            {
                totalAm1T=parseFloat(totalAm1T)+parseFloat(totalAm1);
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Credit Memo", "Category Revenue":"Credit Memo","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalAm1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv+'","Currency":"'+currencyInvArray[0]+'"},'
            }
            if(parseFloat(totalAm2)!=0)
            {
                totalAm2T=parseFloat(totalAm2T)+parseFloat(totalAm2);
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Credit Memo", "Category Revenue":"Credit Memo","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalAm2).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv+'","Currency":"'+currencyInvArray[0]+'"},'
            }
            if(parseFloat(totalAm3)!=0)
            {
                totalAm3T=parseFloat(totalAm3T)+parseFloat(totalAm3);
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Credit Memo", "Category Revenue":"Credit Memo","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalAm3).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv+'","Currency":"'+currencyInvArray[0]+'"},'
            }
            if(parseFloat(totalAm4)!=0)
            {
                totalAm4T=parseFloat(totalAm4T)+parseFloat(totalAm4);
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Credit Memo", "Category Revenue":"Credit Memo","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalAm4).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv+'","Currency":"'+currencyInvArray[0]+'"},'
            }
            if(parseFloat(totalAm5)!=0)
            {
                totalAm5T=parseFloat(totalAm5T)+parseFloat(totalAm5);
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Credit Memo", "Category Revenue":"Credit Memo","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalAm5).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv+'","Currency":"'+currencyInvArray[0]+'"},'
            }
        }

        stringseries += '\n{name: \'Credit Memo\',type: \'column\', visible: false,  data: \n[\n';
        stringseries += '{y:' + parseFloat(totalAm5T).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=credit&typelabel=Credit Memo&month=-5&scriptid=2&product=0&productlabel=na' +'\'},\n';
        stringseries += '{y:' + parseFloat(totalAm4T).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=credit&typelabel=Credit Memo&month=-4&scriptid=2&product=0&productlabel=na'+'\'},\n';
        stringseries += '{y:' + parseFloat(totalAm3T).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=credit&typelabel=Credit Memo&month=-3&scriptid=2&product=0&productlabel=na' +'\'},\n';
        stringseries += '{y:' + parseFloat(totalAm2T).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=credit&typelabel=Credit Memo&month=-2&scriptid=2&product=0&productlabel=na' +'\'},\n';
        stringseries += '{y:' + parseFloat(totalAm1T).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=credit&typelabel=Credit Memo&month=-1&scriptid=2&product=0&productlabel=na' +'\'},\n';
        stringseries += '{y:' + parseFloat(totalAmCMT).toFixed(2) +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=credit&typelabel=Credit Memo&month=0&scriptid=2&product=0&productlabel=na'+'\'}';
        stringseries += ']\n';
        stringseries += '},\n';
    }

    var arrayReturn=[stringCSV,stringseries,totalAm5T,totalAm4T,totalAm3T,totalAm2T,totalAm1T,totalAmCMT]
    return arrayReturn;

}

function getSeriesCSVChurn(marketid,search,curr, bf,cat){

    var arrFilters=new Array();
    var arrColumns=new Array();
    if(marketid!=0)
    {
        if(marketid=='25')
        {
            //Montreal
            var marketidlist=["5", "8", "9", "10", "11", "12", "27"];

        }
        if(marketid=='23')
        {
            //Toronto
            var marketidlist=["6", "13", "15"];

        }
        if(marketid=='26')
        {
            //Vancouver
            var marketidlist=["28", "7"];

        }
        if(marketid=='22')
        {
            //Minneapolis
            var marketidlist=["16", "35"];
        }
        if(marketid=='29')
        {
            //Jacksonville
            var marketidlist=["31"];
        }
        if(marketid=='20')
        {
            //Dallas
            var marketidlist=["2", "17"];
        }
        if(marketid=='33')
        {
            //Columbus
            var marketidlist=["38", "34","39"];
        }
        if(marketid=='41')
        {
            //Lakeland
            var marketidlist=["42"];
        }
        if(marketid=='43')
        {
            //Jacksonville2
            var marketidlist=["40"];
        }
        if(marketid=='52')
        {
            //Parsippany
            var marketidlist=["53","54","55","56"];
        }

        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_churn_location",null,"anyof",marketidlist));


    }
    var arrDates  = getDateRange(curr);
    var arrDates1  = getDateRange(bf);
    var startDate = arrDates[0];
    var endDate  = arrDates1[1];
    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_churn_closeddate", null, "within",startDate,endDate));
    var searchInvoices = nlapiSearchRecord('customrecord_cologix_churn', search, arrFilters, arrColumns);
    var stringCSV='';
    if(searchInvoices!=null)
    {
        for ( var i = 0;searchInvoices  != null && i < searchInvoices.length; i++ ) {
            var searchInv = searchInvoices [i];
            var columns = searchInv.getAllColumns();
            var customerName=searchInv.getText(columns[0]);
            var locationInv=searchInv.getText(columns[2]);
            var internalID=searchInv.getValue(columns[8]);
            var dateInv=searchInv.getValue(columns[1]);
            var totalInter=parseFloat(searchInv.getValue(columns[3]));
            var totalNet=parseFloat(searchInv.getValue(columns[4]));
            var totalOt=parseFloat(searchInv.getValue(columns[5]));
            var totalPow=parseFloat(searchInv.getValue(columns[6]));
            var totalSp=parseFloat(searchInv.getValue(columns[7]));
            if(totalInter!=0)
            {
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Churn","Date":"'+dateInv+'", "Amount":"-'+Math.round(totalInter)+'", "Market":"'+locationInv+'","Product":"Interconnection","Currency":"","Link":"https://system.netsuite.com/app/common/custom/custrecordentry.nl?rectype=237&id='+internalID+' "},';
            }
            if(totalNet!=0)
            {
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Churn","Date":"'+dateInv+'", "Amount":"-'+Math.round(totalNet)+'", "Market":"'+locationInv+'","Product":"Network","Currency":"","Link":"https://system.netsuite.com/app/common/custom/custrecordentry.nl?rectype=237&id='+internalID+' "},';
            }
            if(totalOt!=0)
            {
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Churn","Date":"'+dateInv+'", "Amount":"-'+Math.round(totalOt)+'", "Market":"'+locationInv+'","Product":"Network","Currency":"","Link":"https://system.netsuite.com/app/common/custom/custrecordentry.nl?rectype=237&id='+internalID+' "},';
            }
            if(totalPow!=0)
            {
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Churn","Date":"'+dateInv+'", "Amount":"-'+Math.round(totalPow)+'", "Market":"'+locationInv+'","Product":"Network","Currency":"","Link":"https://system.netsuite.com/app/common/custom/custrecordentry.nl?rectype=237&id='+internalID+' "},';
            }
            if(totalSp!=0)
            {
                stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Churn","Date":"'+dateInv+'", "Amount":"-'+Math.round(totalSp)+'", "Market":"'+locationInv+'","Product":"Network","Currency":"","Link":"https://system.netsuite.com/app/common/custom/custrecordentry.nl?rectype=237&id='+internalID+' "},';
            }



        }
    }


    return stringCSV;

}

function getSeries (marketid){
    return null;
}




function getDateRange(monthsago){

    var today = new Date();
    var date = nlapiAddMonths(today, monthsago);

    var month = parseInt(date.getMonth());
    var year = date.getFullYear();

    var stDays  = daysInMonth(parseInt(month),parseInt(year));
    var stYear  = year;

    var stMonth = parseInt(month) + 1;

    var stStartDate = stMonth + '/1/' + stYear;
    var stEndDate   = stMonth + '/' + stDays + '/' + stYear;

    var arrDateRange = new Array();
    arrDateRange[0] = stStartDate;
    arrDateRange[1] =stEndDate;

    return arrDateRange;
}


function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}

//check if a value is in the array
function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}