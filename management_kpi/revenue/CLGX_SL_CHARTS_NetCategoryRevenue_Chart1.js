nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_NetCategoryRevenue_Chart.js
//	Script Name:	CLGX_RL_CHARTS_NetCategoryRevenue_Chart
//	Script Id:		customscript_clgx_sl_chrt_revenue
// 	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		15/07/2014
//	Includes:		CLGX_LIB_Global.js, moment.min.js
//-------------------------------------------------------------------------------------------------
function suitelet_charts_revenueManagement_chart(datain){
    try {
        // var marketid=0;
        var txtArg = datain;
        // var emailSubject = 'July Revenue';
        //  var emailBody="Values: "+datain;
        //  nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null); // Send email to Catalina
        txtArg = txtArg.replace(/\"/g,"");
        txtArg = txtArg.replace(/\:/g,",");
        txtArg = txtArg.replace(/\{/g,"");
        txtArg = txtArg.replace(/\}/g,"");
        var arrArguments = new Array();
        arrArguments = txtArg.split( "," );
        var marketid = arrArguments[3];
        if(marketid != '' ){
            var objFile = nlapiLoadFile(1524072);
            var html = objFile.getValue();
            html = html.replace(new RegExp('{title}','g'),clgx_return_market_name(marketid));
            html = html.replace(new RegExp('{marketid}','g'), marketid);
            html = html.replace(new RegExp('{location}','g'), clgx_return_market_name(marketid));
            if(marketid==0)
            {
                var min='min: {-250000},'
            }
            else{
                var min='';
            }
            html = html.replace(new RegExp('{min}','g'), min);
            var series=getSeriesCSV(marketid);
            html = html.replace(new RegExp('{series}','g'), series[1]);
            html = html.replace(new RegExp('{seriesCSV}','g'), series[0]);


        }
        else{
            var html = 'Please select a market from the left panel.';
        }
        return html;
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
function getSeriesCSV(marketid){
    var stringReturnCSV='';
    //Network
    //   var netCurrString=getSeriesCSV1(marketid,"customsearch_clgx_so_noinvgridnet_n",1, 0,'NetWork');
    var net1MTHString=getSeriesCSV1(marketid,"customsearch_clgx_inv_revenuegr_all_3",0,-6,'All');

    stringReturnCSV=net1MTHString[0];
    /*  var v1=net1MTHString[2];
     v1=parseFloat(v1).toFixed(2);
     var total1=(parseFloat(v1));
     var v1_1=net1MTHString[3];
     v1_1=parseFloat(v1_1).toFixed(2);
     var total2=(parseFloat(v1_1));
     //3
     var v1_2=net1MTHString[4];
     v1_2=parseFloat(v1_2).toFixed(2);
     var total3=(parseFloat(v1_2));
     //4
     var v1_3=net1MTHString[5];
     v1_3=parseFloat(v1_3).toFixed(2);
     var total4=(parseFloat(v1_3));
     //5
     var v1_4=net1MTHString[6];
     v1_4=parseFloat(v1_4).toFixed(2);
     var total5=(parseFloat(v1_4));
     //6
     var v1_5=net1MTHString[7];
     v1_5=parseFloat(v1_5).toFixed(2);
     var total6=(parseFloat(v1_5));*/
    /*var stringseries1=",{name: 'Net Total',type: 'spline',visible: true,color: '#BC4676',data: ["+
     parseFloat(total1).toFixed(2)+',' +
     parseFloat(total2).toFixed(2)+','+
     parseFloat(total3).toFixed(2)+','+
     parseFloat(total4).toFixed(2)+','+
     parseFloat(total5).toFixed(2)+','+
     parseFloat(total6).toFixed(2)+','+"]}";*/

    var returnArr=[stringReturnCSV,net1MTHString[1]];
    return returnArr;



}

function getSeriesCSV1(marketid,search,curr, bf,cat){

    var arrFilters=new Array();
    var arrColumns=new Array();
    var arrFiltersNewMRR=new Array();
    var arrColumnsNewMRR=new Array();
    var arrFiltersNewMRR1=new Array();
    var arrColumnsNewMRR1=new Array();
    if(marketid!=0)
    {
        if(marketid=='25')
        {
            //Montreal
            var marketidlist=["5", "8", "9", "10", "11", "12", "27"];

        }
        if(marketid=='23')
        {
            //Toronto
            var marketidlist=["6", "13", "15"];

        }
        if(marketid=='26')
        {
            //Vancouver
            var marketidlist=["28", "7"];

        }
        if(marketid=='22')
        {
            //Minneapolis
            var marketidlist=["16", "35"];
        }
        if(marketid=='29')
        {
            //Jacksonville
            var marketidlist=["31"];
        }
        if(marketid=='20')
        {
            //Dallas
            var marketidlist=["2", "17"];
        }
        if(marketid=='33')
        {
            //Columbus
            var marketidlist=["38", "34","39"];
        }
        if(marketid=='41')
        {
            //Lakeland
            var marketidlist=["42"];
        }
        if(marketid=='43')
        {
            //Jacksonville2
            var marketidlist=["40"];
        }
        if(marketid=='52')
        {
            //Parsippany
            var marketidlist=["53","54","55","56"];
        }

        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",marketidlist));
        arrFiltersNewMRR.push(new nlobjSearchFilter("location",null,"anyof",marketidlist));


    }
    else if(marketid==0){
        var arrNJ=[53,54,55,56];
        arrFilters.push(new nlobjSearchFilter("location",null,"noneof",arrNJ));


    }
    var stringCSV='';
    var stringseries='';
    var newMRR1MTTotal=0;
    var newMRR2MTTotal=0;
    var newMRR3MTTotal=0;
    var newMRR4MTTotal=0;
    var newMRR5MTTotal=0;
    var newMRR6MTTotal=0;
    var prMRR1MTTotal=0;
    var prMRR2MTTotal=0;
    var prMRR3MTTotal=0;
    var prMRR4MTTotal=0;
    var prMRR5MTTotal=0;
    var prMRR6MTTotal=0;
    var upMRR1MTTotal=0;
    var upMRR2MTTotal=0;
    var upMRR3MTTotal=0;
    var upMRR4MTTotal=0;
    var upMRR5MTTotal=0;
    var upMRR6MTTotal=0;
    var disco1MTTotal=0;
    var disco2MTTotal=0;
    var disco3MTTotal=0;
    var disco4MTTotal=0;
    var disco5MTTotal=0;
    var disco6MTTotal=0;
    var dec1MTTotal=0;
    var dec2MTTotal=0;
    var dec3MTTotal=0;
    var dec4MTTotal=0;
    var dec5MTTotal=0;
    var dec6MTTotal=0;
    var don1MTTotal=0;
    var don2MTTotal=0;
    var don3MTTotal=0;
    var don4MTTotal=0;
    var don5MTTotal=0;
    var don6MTTotal=0;
    var interconnectionMRR=0;
    var networkMRR=0
    var otherRecurringMRR=0;
    var virtualInterconnectionMRR=0;
    var spaceMRR=0;
    var powerMRR=0;
    var disasterRecoveryMRR=0;
    var remoteHandsMRR=0;
    //1
    var interconnectionMRR1=0;
    var networkMRR1=0
    var otherRecurringMRR1=0;
    var virtualInterconnectionMRR1=0;
    var spaceMRR1=0;
    var powerMRR1=0;
    var disasterRecoveryMRR1=0;
    var remoteHandsMRR1=0;
    //2
    var interconnectionMRR2=0;
    var networkMRR2=0
    var otherRecurringMRR2=0;
    var virtualInterconnectionMRR2=0;
    var spaceMRR2=0;
    var powerMRR2=0;
    var disasterRecoveryMRR2=0;
    var remoteHandsMRR2=0;
    //3
    var interconnectionMRR3=0;
    var networkMRR3=0
    var otherRecurringMRR3=0;
    var virtualInterconnectionMRR3=0;
    var spaceMRR3=0;
    var powerMRR3=0;
    var disasterRecoveryMRR3=0;
    var remoteHandsMRR3=0;
    //4
    var interconnectionMRR4=0;
    var networkMRR4=0
    var otherRecurringMRR4=0;
    var virtualInterconnectionMRR4=0;
    var spaceMRR4=0;
    var powerMRR4=0;
    var disasterRecoveryMRR4=0;
    var remoteHandsMRR4=0;
    //5
    var interconnectionMRR5=0;
    var networkMRR5=0
    var otherRecurringMRR5=0;
    var virtualInterconnectionMRR5=0;
    var spaceMRR5=0;
    var powerMRR5=0;
    var disasterRecoveryMRR5=0;
    var remoteHandsMRR5=0;
    //6
    var interconnectionMRR6=0;
    var networkMRR6=0
    var otherRecurringMRR6=0;
    var virtualInterconnectionMRR6=0;
    var spaceMRR6=0;
    var powerMRR6=0;

    var arrDates  = getDateRange(curr);
    var arrDates1  = getDateRange(bf);
    var startDate = arrDates1[0];
    var endDate  = arrDates[1];
    arrFilters.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    arrFiltersNewMRR.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    arrFiltersNewMRR1.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));

    var customerMRRArray=new Array();
    var totalAmountCurrMRRArray=new Array();
    var totalAmountBFMRRArray=new Array();
    var totalAmount1MRRArray=new Array();
    var totalAmount2MRRArray=new Array();
    var totalAmount3MRRArray=new Array();
    var totalAmount4MRRArray=new Array();
    var totalAmount5MRRArray=new Array();
    arrFilters.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    arrFiltersNewMRR.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
    var customerMRRArray=new Array();
    var locationMRRArray=new Array();
    var totalAmountCurrMRRArray=new Array();
    var totalAmountBFMRRArray=new Array();
    var totalAmount1MRRArray=new Array();
    var totalAmount2MRRArray=new Array();
    var totalAmount3MRRArray=new Array();
    var totalAmount4MRRArray=new Array();
    var totalAmount5MRRArray=new Array();
    var searchInvoicesNewMRR = nlapiLoadSearch('transaction', 'customsearch_clgx_inv_revenuegr_all_nwm');
    searchInvoicesNewMRR.addColumns(arrColumnsNewMRR);
    searchInvoicesNewMRR.addFilters(arrFiltersNewMRR);
    var searchInvNewMRR = searchInvoicesNewMRR.runSearch();
    searchInvNewMRR.forEachResult(function(searchResult) {
        var columnsMRR = searchResult.getAllColumns();
        var customerMRR=searchResult.getValue(columnsMRR[1]);
        var locationMRR=searchResult.getText(columnsMRR[2]);
        var totalAmountCurrMRR=parseFloat(searchResult.getValue(columnsMRR[3]));
        var totalAmountBFMRR=parseFloat(searchResult.getValue(columnsMRR[4]));
        var totalAmount1MRR=parseFloat(searchResult.getValue(columnsMRR[5]));
        var totalAmount2MRR=parseFloat(searchResult.getValue(columnsMRR[6]));
        var totalAmount3MRR=parseFloat(searchResult.getValue(columnsMRR[7]));
        var totalAmount4MRR=parseFloat(searchResult.getValue(columnsMRR[8]));
        var totalAmount5MRR=parseFloat(searchResult.getValue(columnsMRR[9]));
        customerMRRArray.push(customerMRR);
        locationMRRArray.push(customerMRR+';'+locationMRR);
        totalAmountCurrMRRArray.push(totalAmountCurrMRR);
        totalAmountBFMRRArray.push(totalAmountBFMRR);
        totalAmount1MRRArray.push(totalAmount1MRR);
        totalAmount2MRRArray.push(totalAmount2MRR);
        totalAmount3MRRArray.push(totalAmount3MRR);
        totalAmount4MRRArray.push(totalAmount4MRR);
        totalAmount5MRRArray.push(totalAmount5MRR);

        return true; // return true to keep iterating

    });

    var searchInvoices = nlapiLoadSearch('transaction',search);
    searchInvoices.addColumns(arrColumns);
    searchInvoices.addFilters(arrFilters);
    var resultINV = searchInvoices.runSearch();
    resultINV.forEachResult(function(searchResult) {
        var columns = searchResult.getAllColumns();
        var customerID=searchResult.getValue(columns[0]);
        var customerName=searchResult.getValue(columns[1]);
        var locationInv=searchResult.getText(columns[2]);
        var currMTHAmount=searchResult.getValue(columns[3]);
        var bfMTHAmount=searchResult.getValue(columns[4]);
        var MTH1AgoAmount=searchResult.getValue(columns[5]);
        var MTH2AgoAmount=searchResult.getValue(columns[6]);
        var MTH3AgoAmount=searchResult.getValue(columns[7]);
        var MTH4AgoAmount=searchResult.getValue(columns[8]);
        var MTH5AgoAmount=searchResult.getValue(columns[9]);
        var currMTHQTY=searchResult.getValue(columns[10]);
        var bfMTHQTY=searchResult.getValue(columns[11]);
        var MTH1AgoQTY=searchResult.getValue(columns[12]);
        var MTH2AgoQTY=searchResult.getValue(columns[13]);
        var MTH3AgoQTY=searchResult.getValue(columns[14]);
        var MTH4AgoQTY=searchResult.getValue(columns[15]);
        var MTH5AgoQTY=searchResult.getValue(columns[16]);
        var categoryInv1=searchResult.getText(columns[17]);
        var currencyINV1=searchResult.getText(columns[18]);
        var arrDates_1  = getDateRange(-1);
        var startDate_1 = arrDates_1[0];
        var arrDates_2  = getDateRange(-2);
        var startDate_2 = arrDates_2[0];
        var arrDates_3  = getDateRange(-3);
        var startDate_3 = arrDates_3[0];
        var arrDates_4  = getDateRange(-4);
        var startDate_4 = arrDates_4[0];
        var arrDates_5  = getDateRange(-5);
        var startDate_5 = arrDates_5[0];
        var arrDates_6  = getDateRange(-6);
        var startDate_6 = arrDates_6[0];
        var index=0;
        var test=customerName+';'+locationInv;
        index=_.indexOf(locationMRRArray, test);
        //1  MT AGO
        if((parseFloat(currMTHAmount)!=0)&&(parseFloat(bfMTHAmount)==0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmountCurrMRRArray[index])!=0)&&(parseFloat(totalAmountBFMRRArray[index])==0))
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR=parseFloat(interconnectionMRR)+parseFloat(currMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR=parseFloat(otherRecurringMRR)+parseFloat(currMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR=parseFloat(virtualInterconnectionMRR)+parseFloat(currMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR=parseFloat(networkMRR)+parseFloat(currMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR=parseFloat(spaceMRR)+parseFloat(currMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR=parseFloat(powerMRR)+parseFloat(currMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }

                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR=parseFloat(disasterRecoveryMRR)+parseFloat(currMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR=parseFloat(remoteHandsMRR)+parseFloat(currMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
                else{
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR=parseFloat(interconnectionMRR)+parseFloat(currMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR=parseFloat(otherRecurringMRR)+parseFloat(currMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR=parseFloat(virtualInterconnectionMRR)+parseFloat(currMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR=parseFloat(networkMRR)+parseFloat(currMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR=parseFloat(spaceMRR)+parseFloat(currMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR=parseFloat(powerMRR)+parseFloat(currMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR=parseFloat(disasterRecoveryMRR)+parseFloat(currMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR=parseFloat(remoteHandsMRR)+parseFloat(currMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(currMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
            }
        }
        else{
            if(parseInt(currMTHQTY)>parseInt(bfMTHQTY)){

                var totalamInvQ=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                if(totalamInvQ>0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR=parseFloat(interconnectionMRR)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR=parseFloat(otherRecurringMRR)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR=parseFloat(virtualInterconnectionMRR)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR=parseFloat(networkMRR)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR=parseFloat(spaceMRR)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR=parseFloat(powerMRR)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR=parseFloat(disasterRecoveryMRR)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR=parseFloat(remoteHandsMRR)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }


            }
            else if(parseFloat(currMTHAmount) > parseFloat(bfMTHAmount)){


                var totalamInvQ1=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                if(totalamInvQ1>0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR=parseFloat(interconnectionMRR)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR=parseFloat(otherRecurringMRR)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR=parseFloat(virtualInterconnectionMRR)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR=parseFloat(networkMRR)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR=parseFloat(spaceMRR)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR=parseFloat(powerMRR)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR=parseFloat(disasterRecoveryMRR)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR=parseFloat(remoteHandsMRR)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
            }
        }
        //2  MT AGO
        if((parseFloat(bfMTHAmount)!=0)&&(parseFloat(MTH1AgoAmount)==0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmountBFMRRArray[index])!=0)&&(parseFloat(totalAmount1MRRArray[index])==0))
                {


                    if(categoryInv1=='Interconnection')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        interconnectionMRR1=parseFloat(interconnectionMRR1)+parseFloat(bfMTHAmount);
                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        otherRecurringMRR1=parseFloat(otherRecurringMRR1)+parseFloat(bfMTHAmount);
                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        virtualInterconnectionMRR1=parseFloat(virtualInterconnectionMRR1)+parseFloat(bfMTHAmount);
                    }
                    if(categoryInv1=='Network')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        networkMRR1=parseFloat(networkMRR1)+parseFloat(bfMTHAmount);
                    }
                    if(categoryInv1=='Space')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        spaceMRR1=parseFloat(spaceMRR1)+parseFloat(bfMTHAmount);
                    }
                    if(categoryInv1=='Power')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        powerMRR1=parseFloat(powerMRR1)+parseFloat(bfMTHAmount);
                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        disasterRecoveryMRR1=parseFloat(disasterRecoveryMRR1)+parseFloat(bfMTHAmount);
                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        remoteHandsMRR1=parseFloat(remoteHandsMRR1)+parseFloat(bfMTHAmount);
                    }
                }
                else{
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR1=parseFloat(interconnectionMRR1)+parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR1=parseFloat(otherRecurringMRR1)+parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR1=parseFloat(virtualInterconnectionMRR1)+parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR1=parseFloat(networkMRR1)+parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR1=parseFloat(spaceMRR1)+parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR1=parseFloat(powerMRR1)+parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR1=parseFloat(disasterRecoveryMRR1)+parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR1=parseFloat(remoteHandsMRR1)+parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(bfMTHAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                }
            }
        }

        else{
            if(parseInt(bfMTHQTY)>parseInt(MTH1AgoQTY)){

                var totalamInvQ=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                if(totalamInvQ>0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR1=parseFloat(interconnectionMRR1)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR1=parseFloat(otherRecurringMRR1)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR1=parseFloat(virtualInterconnectionMRR1)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR1=parseFloat(networkMRR1)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR1=parseFloat(spaceMRR1)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR1=parseFloat(powerMRR1)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR1=parseFloat(disasterRecoveryMRR1)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR1=parseFloat(remoteHandsMRR1)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }


            }
            else if(parseFloat(bfMTHAmount) > parseFloat(MTH1AgoAmount)){


                var totalamInvQ1=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                if(totalamInvQ1>0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR1=parseFloat(interconnectionMRR1)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR1=parseFloat(otherRecurringMRR1)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR1=parseFloat(virtualInterconnectionMRR1)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR1=parseFloat(networkMRR1)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR1=parseFloat(spaceMRR1)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR1=parseFloat(powerMRR1)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR1=parseFloat(disasterRecoveryMRR1)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR1=parseFloat(remoteHandsMRR1)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                }
            }
        }

        //3 MT AGO
        if((parseFloat(MTH1AgoAmount)!=0)&&(parseFloat(MTH2AgoAmount)==0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount1MRRArray[index])!=0)&&(parseFloat(totalAmount2MRRArray[index])==0))
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR2=parseFloat(interconnectionMRR2)+parseFloat(MTH1AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR2=parseFloat(otherRecurringMRR2)+parseFloat(MTH1AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR2=parseFloat(virtualInterconnectionMRR2)+parseFloat(MTH1AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR2=parseFloat(networkMRR2)+parseFloat(MTH1AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR2=parseFloat(spaceMRR2)+parseFloat(MTH1AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR2=parseFloat(powerMRR2)+parseFloat(MTH1AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR2=parseFloat(disasterRecoveryMRR2)+parseFloat(MTH1AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR2=parseFloat(remoteHandsMRR2)+parseFloat(MTH1AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
                else
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR2=parseFloat(interconnectionMRR2)+parseFloat(MTH1AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR2=parseFloat(otherRecurringMRR2)+parseFloat(MTH1AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR2=parseFloat(virtualInterconnectionMRR2)+parseFloat(MTH1AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR2=parseFloat(networkMRR2)+parseFloat(MTH1AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR2=parseFloat(spaceMRR2)+parseFloat(MTH1AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR2=parseFloat(powerMRR2)+parseFloat(MTH1AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR2=parseFloat(disasterRecoveryMRR2)+parseFloat(MTH1AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR2=parseFloat(remoteHandsMRR2)+parseFloat(MTH1AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(MTH1AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
            }
        }
        else{
            if(parseInt(MTH1AgoQTY)>parseInt(MTH2AgoQTY)){

                var totalamInvQ=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                if(totalamInvQ>0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR2=parseFloat(interconnectionMRR2)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR2=parseFloat(otherRecurringMRR2)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR2=parseFloat(virtualInterconnectionMRR2)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR2=parseFloat(networkMRR2)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR2=parseFloat(spaceMRR2)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR2=parseFloat(powerMRR2)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR2=parseFloat(disasterRecoveryMRR2)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR2=parseFloat(remoteHandsMRR2)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }


            }
            else if(parseFloat(MTH1AgoAmount) > parseFloat(MTH2AgoAmount)){


                var totalamInvQ1=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                if(totalamInvQ1>0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR2=parseFloat(interconnectionMRR2)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR2=parseFloat(otherRecurringMRR2)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR2=parseFloat(virtualInterconnectionMRR2)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR2=parseFloat(networkMRR2)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR2=parseFloat(spaceMRR2)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR2=parseFloat(powerMRR2)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR2=parseFloat(disasterRecoveryMRR2)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR2=parseFloat(remoteHandsMRR2)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                }
            }
        }
//4 MT AGO
        if((parseFloat(MTH2AgoAmount)!=0)&&(parseFloat(MTH3AgoAmount)==0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount2MRRArray[index])!=0)&&(parseFloat(totalAmount3MRRArray[index])==0))
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR3=parseFloat(interconnectionMRR3)+parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR3=parseFloat(otherRecurringMRR3)+parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR3=parseFloat(virtualInterconnectionMRR3)+parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR3=parseFloat(networkMRR3)+parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR3=parseFloat(spaceMRR3)+parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR3=parseFloat(powerMRR3)+parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR3=parseFloat(disasterRecoveryMRR3)+parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR3=parseFloat(remoteHandsMRR3)+parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
                else{
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR3=parseFloat(interconnectionMRR3)+parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR3=parseFloat(otherRecurringMRR3)+parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR3=parseFloat(virtualInterconnectionMRR3)+parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR3=parseFloat(networkMRR3)+parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR3=parseFloat(spaceMRR3)+parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR3=parseFloat(powerMRR3)+parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR3=parseFloat(disasterRecoveryMRR3)+parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR3=parseFloat(remoteHandsMRR3)+parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(MTH2AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
            }
        }
        else{
            if(parseInt(MTH2AgoQTY)>parseInt(MTH3AgoQTY)){

                var totalamInvQ=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                if(totalamInvQ>0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR3=parseFloat(interconnectionMRR3)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR3=parseFloat(otherRecurringMRR3)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR3=parseFloat(virtualInterconnectionMRR3)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR3=parseFloat(networkMRR3)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR3=parseFloat(spaceMRR3)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR3=parseFloat(powerMRR3)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR3=parseFloat(disasterRecoveryMRR3)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR3=parseFloat(remoteHandsMRR3)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }


            }
            else if(parseFloat(MTH2AgoAmount) > parseFloat(MTH3AgoAmount)){


                var totalamInvQ1=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                if(totalamInvQ1>0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR3=parseFloat(interconnectionMRR3)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR3=parseFloat(otherRecurringMRR3)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR3=parseFloat(virtualInterconnectionMRR3)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR3=parseFloat(networkMRR3)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR3=parseFloat(spaceMRR3)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR3=parseFloat(powerMRR3)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR3=parseFloat(disasterRecoveryMRR3)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR3=parseFloat(remoteHandsMRR3)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
            }
        }
        //5 MT AGO
        if((parseFloat(MTH3AgoAmount)!=0)&&(parseFloat(MTH4AgoAmount)==0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount3MRRArray[index])!=0)&&(parseFloat(totalAmount4MRRArray[index])==0))
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR4=parseFloat(interconnectionMRR4)+parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR4=parseFloat(otherRecurringMRR4)+parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR4=parseFloat(virtualInterconnectionMRR4)+parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR4=parseFloat(networkMRR4)+parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR4=parseFloat(spaceMRR4)+parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR4=parseFloat(powerMRR4)+parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR4=parseFloat(disasterRecoveryMRR4)+parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR4=parseFloat(remoteHandsMRR4)+parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
                else{
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR4=parseFloat(interconnectionMRR4)+parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR4=parseFloat(otherRecurringMRR4)+parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR4=parseFloat(virtualInterconnectionMRR4)+parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR4=parseFloat(networkMRR4)+parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR4=parseFloat(spaceMRR4)+parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR4=parseFloat(powerMRR4)+parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR4=parseFloat(disasterRecoveryMRR4)+parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR4=parseFloat(remoteHandsMRR4)+parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(MTH3AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
            }
        }
        else{
            if(parseInt(MTH3AgoQTY)>parseInt(MTH4AgoQTY)){

                var totalamInvQ=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                if(totalamInvQ>0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR4=parseFloat(interconnectionMRR4)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR4=parseFloat(otherRecurringMRR4)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR4=parseFloat(virtualInterconnectionMRR4)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR4=parseFloat(networkMRR4)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR4=parseFloat(spaceMRR4)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR4=parseFloat(powerMRR4)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR4=parseFloat(disasterRecoveryMRR4)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR4=parseFloat(remoteHandsMRR4)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }


            }
            else if(parseFloat(MTH3AgoAmount) > parseFloat(MTH4AgoAmount)){


                var totalamInvQ1=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                if(totalamInvQ1>0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR4=parseFloat(interconnectionMRR4)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR4=parseFloat(otherRecurringMRR4)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR4=parseFloat(virtualInterconnectionMRR4)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR4=parseFloat(networkMRR4)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR4=parseFloat(spaceMRR4)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR4=parseFloat(powerMRR4)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR4=parseFloat(disasterRecoveryMRR4)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR4=parseFloat(remoteHandsMRR4)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
            }
        }
        //6 MT AGO
        if((parseFloat(MTH4AgoAmount)!=0)&&(parseFloat(MTH5AgoAmount)==0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount4MRRArray[index])!=0)&&(parseFloat(totalAmount5MRRArray[index])==0))
                {

                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR5=parseFloat(interconnectionMRR5)+parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR5=parseFloat(otherRecurringMRR5)+parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR5=parseFloat(virtualInterconnectionMRR5)+parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR5=parseFloat(networkMRR5)+parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR5=parseFloat(spaceMRR5)+parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR5=parseFloat(powerMRR5)+parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR5=parseFloat(disasterRecoveryMRR5)+parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR5=parseFloat(remoteHandsMRR5)+parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"New MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
                else{
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR5=parseFloat(interconnectionMRR5)+parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR5=parseFloat(otherRecurringMRR5)+parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR5=parseFloat(virtualInterconnectionMRR5)+parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR5=parseFloat(networkMRR5)+parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR5=parseFloat(spaceMRR5)+parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR5=parseFloat(powerMRR5)+parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR5=parseFloat(disasterRecoveryMRR5)+parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR5=parseFloat(remoteHandsMRR5)+parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(MTH4AgoAmount).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
            }
        }
        else{
            if(parseInt(MTH4AgoQTY)>parseInt(MTH5AgoQTY)){

                var totalamInvQ=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                if(totalamInvQ>0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR5=parseFloat(interconnectionMRR5)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR5=parseFloat(otherRecurringMRR5)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR5=parseFloat(virtualInterconnectionMRR5)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR5=parseFloat(networkMRR5)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR5=parseFloat(spaceMRR5)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR5=parseFloat(powerMRR5)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR5=parseFloat(disasterRecoveryMRR5)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR5=parseFloat(remoteHandsMRR5)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }


            }
            else if(parseFloat(MTH4AgoAmount) > parseFloat(MTH5AgoAmount)){


                var totalamInvQ1=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                if(totalamInvQ1>0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR5=parseFloat(interconnectionMRR5)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR5=parseFloat(otherRecurringMRR5)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR5=parseFloat(virtualInterconnectionMRR5)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR5=parseFloat(networkMRR5)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR5=parseFloat(spaceMRR5)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR5=parseFloat(powerMRR5)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR5=parseFloat(disasterRecoveryMRR5)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR5=parseFloat(remoteHandsMRR5)+parseFloat(totalamInvQ1);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                }
            }
        }

        //CHURN

        //1  MT AGO
        if((parseFloat(currMTHAmount)==0)&&(parseFloat(bfMTHAmount)!=0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmountCurrMRRArray[index])==0)&&(parseFloat(totalAmountBFMRRArray[index])!=0))
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR=parseFloat(interconnectionMRR)+parseFloat((-1))*parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR=parseFloat(otherRecurringMRR)+parseFloat((-1))*parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR=parseFloat(virtualInterconnectionMRR)+parseFloat((-1))*parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR=parseFloat(networkMRR)+parseFloat((-1))*parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR=parseFloat(spaceMRR)+parseFloat((-1))*parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR=parseFloat(powerMRR)+parseFloat((-1))*parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR=parseFloat(disasterRecoveryMRR)+parseFloat((-1))*parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR=parseFloat(remoteHandsMRR)+parseFloat((-1))*parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
                else{
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR=parseFloat(interconnectionMRR)+parseFloat((-1))*parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR=parseFloat(otherRecurringMRR)+parseFloat((-1))*parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR=parseFloat(virtualInterconnectionMRR)+parseFloat((-1))*parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR=parseFloat(networkMRR)+parseFloat((-1))*parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR=parseFloat(spaceMRR)+parseFloat((-1))*parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR=parseFloat(powerMRR)+parseFloat((-1))*parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR=parseFloat(disasterRecoveryMRR)+parseFloat((-1))*parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR=parseFloat(remoteHandsMRR)+parseFloat((-1))*parseFloat(bfMTHAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(((-1)*bfMTHAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }

                }
            }
        }
        else{
            if(parseInt(currMTHQTY)<parseInt(bfMTHQTY)){

                var totalamInvQ=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                if(totalamInvQ<0)
                {

                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR=parseFloat(interconnectionMRR)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR=parseFloat(otherRecurringMRR)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR=parseFloat(virtualInterconnectionMRR)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR=parseFloat(networkMRR)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR=parseFloat(spaceMRR)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR=parseFloat(powerMRR)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR=parseFloat(disasterRecoveryMRR)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR=parseFloat(remoteHandsMRR)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
            }
            else if(parseFloat(currMTHAmount) < parseFloat(bfMTHAmount)){


                var totalamInvQ1=parseFloat(currMTHAmount)-parseFloat(bfMTHAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                if(categoryInv1=='Interconnection')
                {
                    interconnectionMRR=parseFloat(interconnectionMRR)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Other Recurring')
                {
                    otherRecurringMRR=parseFloat(otherRecurringMRR)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Virtual Interconnection')
                {
                    virtualInterconnectionMRR=parseFloat(virtualInterconnectionMRR)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Network')
                {
                    networkMRR=parseFloat(networkMRR)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Space')
                {
                    spaceMRR=parseFloat(spaceMRR)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Power')
                {
                    powerMRR=parseFloat(powerMRR)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Disaster Recovery')
                {
                    disasterRecoveryMRR=parseFloat(disasterRecoveryMRR)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Remote Hands')
                {
                    remoteHandsMRR=parseFloat(remoteHandsMRR)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_1+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
            }
        }
        //2  MT AGO
        if((parseFloat(bfMTHAmount)==0)&&(parseFloat(MTH1AgoAmount)!=0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmountBFMRRArray[index])==0)&&(parseFloat(totalAmount1MRRArray[index])!=0))
                {
                    if(categoryInv1=='Interconnection')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        interconnectionMRR1=parseFloat(interconnectionMRR1)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        otherRecurringMRR1=parseFloat(otherRecurringMRR1)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        virtualInterconnectionMRR1=parseFloat(virtualInterconnectionMRR1)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                    }
                    if(categoryInv1=='Network')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        networkMRR1=parseFloat(networkMRR1)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                    }
                    if(categoryInv1=='Space')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        spaceMRR1=parseFloat(spaceMRR1)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                    }
                    if(categoryInv1=='Power')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        powerMRR1=parseFloat(powerMRR1)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        disasterRecoveryMRR1=parseFloat(disasterRecoveryMRR1)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        remoteHandsMRR1=parseFloat(remoteHandsMRR1)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                    }
                }
                else{
                    if(categoryInv1=='Interconnection')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        interconnectionMRR1=parseFloat(interconnectionMRR1)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        otherRecurringMRR1=parseFloat(otherRecurringMRR1)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        virtualInterconnectionMRR1=parseFloat(virtualInterconnectionMRR1)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                    }
                    if(categoryInv1=='Network')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        networkMRR1=parseFloat(networkMRR1)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                    }
                    if(categoryInv1=='Space')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        spaceMRR1=parseFloat(spaceMRR1)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                    }
                    if(categoryInv1=='Power')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        powerMRR1=parseFloat(powerMRR1)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        disasterRecoveryMRR1=parseFloat(disasterRecoveryMRR1)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(((-1)*MTH1AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        remoteHandsMRR1=parseFloat(remoteHandsMRR1)+parseFloat((-1))*parseFloat(MTH1AgoAmount);
                    }
                }

            }
        }
        else{
            if(parseInt(bfMTHQTY)<parseInt(MTH1AgoQTY)){

                var totalamInvQ=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                if(totalamInvQ<0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR1=parseFloat(interconnectionMRR1)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR1=parseFloat(otherRecurringMRR1)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR1=parseFloat(virtualInterconnectionMRR1)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR1=parseFloat(networkMRR1)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR1=parseFloat(spaceMRR1)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR1=parseFloat(powerMRR1)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR1=parseFloat(disasterRecoveryMRR1)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR1=parseFloat(remoteHandsMRR1)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
            }
            else if(parseFloat(bfMTHAmount) < parseFloat( MTH1AgoAmount)){


                var totalamInvQ1=parseFloat(bfMTHAmount)-parseFloat(MTH1AgoAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                if(categoryInv1=='Interconnection')
                {
                    interconnectionMRR1=parseFloat(interconnectionMRR1)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Other Recurring')
                {
                    otherRecurringMRR1=parseFloat(otherRecurringMRR1)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Virtual Interconnection')
                {
                    virtualInterconnectionMRR1=parseFloat(virtualInterconnectionMRR1)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Network')
                {
                    networkMRR1=parseFloat(networkMRR1)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Space')
                {
                    spaceMRR1=parseFloat(spaceMRR1)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Power')
                {
                    powerMRR1=parseFloat(powerMRR1)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Disaster Recovery')
                {
                    disasterRecoveryMRR1=parseFloat(disasterRecoveryMRR1)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Remote Hands')
                {
                    remoteHandsMRR1=parseFloat(remoteHandsMRR1)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_2+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
            }
        }

        //3 MT AGO
        if((parseFloat(MTH1AgoAmount)==0)&&(parseFloat(MTH2AgoAmount)!=0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount1MRRArray[index])==0)&&(parseFloat(totalAmount2MRRArray[index])!=0))
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR2=parseFloat(interconnectionMRR2)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR2=parseFloat(otherRecurringMRR2)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR2=parseFloat(virtualInterconnectionMRR2)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR2=parseFloat(networkMRR2)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR2=parseFloat(spaceMRR2)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR2=parseFloat(powerMRR2)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR2=parseFloat(disasterRecoveryMRR2)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR2=parseFloat(remoteHandsMRR2)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
                else{
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR2=parseFloat(interconnectionMRR2)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR2=parseFloat(otherRecurringMRR2)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        virtualInterconnectionMRR2=parseFloat(virtualInterconnectionMRR2)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                    }
                    if(categoryInv1=='Network')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        networkMRR2=parseFloat(networkMRR2)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                    }
                    if(categoryInv1=='Space')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        spaceMRR2=parseFloat(spaceMRR2)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                    }
                    if(categoryInv1=='Power')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        powerMRR2=parseFloat(powerMRR2)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        disasterRecoveryMRR2=parseFloat(disasterRecoveryMRR2)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(((-1)*MTH2AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                        remoteHandsMRR2=parseFloat(remoteHandsMRR2)+parseFloat((-1))*parseFloat(MTH2AgoAmount);
                    }
                }
            }
        }
        else{
            if(parseInt(MTH1AgoQTY)<parseInt(MTH2AgoQTY)){

                var totalamInvQ=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                if(totalamInvQ<0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR2=parseFloat(interconnectionMRR2)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR2=parseFloat(otherRecurringMRR2)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR2=parseFloat(virtualInterconnectionMRR2)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR2=parseFloat(networkMRR2)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR2=parseFloat(spaceMRR2)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR2=parseFloat(powerMRR2)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR2=parseFloat(disasterRecoveryMRR2)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR2=parseFloat(remoteHandsMRR2)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
            }
            else if(parseFloat(MTH1AgoAmount) <parseFloat( MTH2AgoAmount)){


                var totalamInvQ1=parseFloat(MTH1AgoAmount)-parseFloat(MTH2AgoAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                if(categoryInv1=='Interconnection')
                {
                    interconnectionMRR2=parseFloat(interconnectionMRR2)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Other Recurring')
                {
                    otherRecurringMRR2=parseFloat(otherRecurringMRR2)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Virtual Interconnection')
                {
                    virtualInterconnectionMRR2=parseFloat(virtualInterconnectionMRR2)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Network')
                {
                    networkMRR2=parseFloat(networkMRR2)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Space')
                {
                    spaceMRR2=parseFloat(spaceMRR2)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Power')
                {
                    powerMRR2=parseFloat(powerMRR2)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Disaster Recovery')
                {
                    disasterRecoveryMRR2=parseFloat(disasterRecoveryMRR2)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Remote Hands')
                {
                    remoteHandsMRR2=parseFloat(remoteHandsMRR2)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_3+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
            }
        }
//4 MT AGO
        if((parseFloat(MTH2AgoAmount)==0)&&(parseFloat(MTH3AgoAmount)!=0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount2MRRArray[index])==0)&&(parseFloat(totalAmount3MRRArray[index])!=0))
                {

                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR3=parseFloat(interconnectionMRR3)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR3=parseFloat(otherRecurringMRR3)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR3=parseFloat(virtualInterconnectionMRR3)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR3=parseFloat(networkMRR3)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR3=parseFloat(spaceMRR3)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR3=parseFloat(powerMRR3)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR3=parseFloat(disasterRecoveryMRR3)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR3=parseFloat(remoteHandsMRR3)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
                else{
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR3=parseFloat(interconnectionMRR3)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR3=parseFloat(otherRecurringMRR3)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR3=parseFloat(virtualInterconnectionMRR3)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR3=parseFloat(networkMRR3)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR3=parseFloat(spaceMRR3)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR3=parseFloat(powerMRR3)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR3=parseFloat(disasterRecoveryMRR3)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR3=parseFloat(remoteHandsMRR3)+parseFloat((-1))*parseFloat(MTH3AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(((-1)*MTH3AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
            }
        }
        else{
            if(parseInt(MTH2AgoQTY)<parseInt(MTH3AgoQTY)){

                var totalamInvQ=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                if(totalamInvQ<0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR3=parseFloat(interconnectionMRR3)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR3=parseFloat(otherRecurringMRR3)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR3=parseFloat(virtualInterconnectionMRR3)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR3=parseFloat(networkMRR3)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR3=parseFloat(spaceMRR3)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR3=parseFloat(powerMRR3)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR3=parseFloat(disasterRecoveryMRR3)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR3=parseFloat(remoteHandsMRR3)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'


                    }
                }
            }
            else if(parseFloat(MTH2AgoAmount) < parseFloat(MTH3AgoAmount)){


                var totalamInvQ1=parseFloat(MTH2AgoAmount)-parseFloat(MTH3AgoAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                if(categoryInv1=='Interconnection')
                {
                    interconnectionMRR3=parseFloat(interconnectionMRR3)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Other Recurring')
                {
                    otherRecurringMRR3=parseFloat(otherRecurringMRR3)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'
                }
                if(categoryInv1=='Virtual Interconnection')
                {
                    virtualInterconnectionMRR3=parseFloat(virtualInterconnectionMRR3)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Network')
                {
                    networkMRR3=parseFloat(networkMRR3)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Space')
                {
                    spaceMRR3=parseFloat(spaceMRR3)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Power')
                {
                    powerMRR3=parseFloat(powerMRR3)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Disaster Recovery')
                {
                    disasterRecoveryMRR3=parseFloat(disasterRecoveryMRR3)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Remote Hands')
                {
                    remoteHandsMRR3=parseFloat(remoteHandsMRR3)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_4+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
            }
        }
        //5 MT AGO
        if((parseFloat(MTH3AgoAmount)==0)&&(parseFloat(MTH4AgoAmount)!=0))
        {

            if(index>0)
            {
                if((parseFloat(totalAmount3MRRArray[index])==0)&&(parseFloat(totalAmount4MRRArray[index])!=0))
                {
                    if(categoryInv1=='Interconnection')
                    {

                        interconnectionMRR4=parseFloat(interconnectionMRR4)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'


                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR4=parseFloat(otherRecurringMRR4)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR4=parseFloat(virtualInterconnectionMRR4)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR4=parseFloat(networkMRR4)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR4=parseFloat(spaceMRR4)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR4=parseFloat(powerMRR4)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR4=parseFloat(disasterRecoveryMRR4)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR4=parseFloat(remoteHandsMRR4)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }}
                else
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR4=parseFloat(interconnectionMRR4)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'


                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR4=parseFloat(otherRecurringMRR4)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR4=parseFloat(virtualInterconnectionMRR4)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR4=parseFloat(networkMRR4)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR4=parseFloat(spaceMRR4)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR4=parseFloat(powerMRR4)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR4=parseFloat(disasterRecoveryMRR4)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR4=parseFloat(remoteHandsMRR4)+parseFloat((-1))*parseFloat(MTH4AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(((-1)*MTH4AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
            }

        }
        else{
            if(parseInt(MTH3AgoQTY)<parseInt(MTH4AgoQTY)){

                var totalamInvQ=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                if(totalamInvQ<0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR4=parseFloat(interconnectionMRR4)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR4=parseFloat(otherRecurringMRR4)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR4=parseFloat(virtualInterconnectionMRR4)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR4=parseFloat(networkMRR4)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR4=parseFloat(spaceMRR4)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR4=parseFloat(powerMRR4)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR4=parseFloat(disasterRecoveryMRR4)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR4=parseFloat(remoteHandsMRR4)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
            }
            else if(parseFloat(MTH3AgoAmount) <parseFloat( MTH4AgoAmount)){


                var totalamInvQ1=parseFloat(MTH3AgoAmount)-parseFloat(MTH4AgoAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                if(categoryInv1=='Interconnection')
                {
                    interconnectionMRR4=parseFloat(interconnectionMRR4)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Other Recurring')
                {
                    otherRecurringMRR4=parseFloat(otherRecurringMRR4)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Virtual Interconnection')
                {
                    virtualInterconnectionMRR4=parseFloat(virtualInterconnectionMRR4)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Network')
                {
                    networkMRR4=parseFloat(networkMRR4)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Space')
                {
                    spaceMRR4=parseFloat(spaceMRR4)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Power')
                {
                    powerMRR4=parseFloat(powerMRR4)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Disaster Recovery')
                {
                    disasterRecoveryMRR4=parseFloat(disasterRecoveryMRR4)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Remote Hands')
                {
                    remoteHandsMRR4=parseFloat(remoteHandsMRR4)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_5+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
            }
        }
        //6 MT AGO
        if((parseFloat(MTH4AgoAmount)==0)&&(parseFloat(MTH5AgoAmount)!=0))
        {
            if(index>0)
            {
                if((parseFloat(totalAmount4MRRArray[index])==0)&&(parseFloat(totalAmount5MRRArray[index])!=0))
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR5=parseFloat(interconnectionMRR5)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR5=parseFloat(otherRecurringMRR5)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR5=parseFloat(virtualInterconnectionMRR5)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR5=parseFloat(networkMRR5)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR5=parseFloat(spaceMRR5)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR5=parseFloat(powerMRR5)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR5=parseFloat(disasterRecoveryMRR5)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR5=parseFloat(remoteHandsMRR5)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Disconnect","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
                else{
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR5=parseFloat(interconnectionMRR5)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR5=parseFloat(otherRecurringMRR5)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR5=parseFloat(virtualInterconnectionMRR5)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR5=parseFloat(networkMRR5)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR5=parseFloat(spaceMRR5)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR5=parseFloat(powerMRR5)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR5=parseFloat(disasterRecoveryMRR5)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR5=parseFloat(remoteHandsMRR5)+parseFloat((-1))*parseFloat(MTH5AgoAmount);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(((-1)*MTH5AgoAmount)).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
            }
        }
        else{
            if(parseInt(MTH4AgoQTY)<parseInt(MTH5AgoQTY)){

                var totalamInvQ=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                if(totalamInvQ<0)
                {
                    if(categoryInv1=='Interconnection')
                    {
                        interconnectionMRR5=parseFloat(interconnectionMRR5)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Other Recurring')
                    {
                        otherRecurringMRR5=parseFloat(otherRecurringMRR5)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Virtual Interconnection')
                    {
                        virtualInterconnectionMRR5=parseFloat(virtualInterconnectionMRR5)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Network')
                    {
                        networkMRR5=parseFloat(networkMRR5)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Space')
                    {
                        spaceMRR5=parseFloat(spaceMRR5)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Power')
                    {
                        powerMRR5=parseFloat(powerMRR5)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Disaster Recovery')
                    {
                        disasterRecoveryMRR5=parseFloat(disasterRecoveryMRR5)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                    if(categoryInv1=='Remote Hands')
                    {
                        remoteHandsMRR5=parseFloat(remoteHandsMRR5)+parseFloat(totalamInvQ);
                        stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Downgrade","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                    }
                }
            }
            else if(parseFloat(MTH4AgoAmount) < parseFloat(MTH5AgoAmount)){


                var totalamInvQ1=parseFloat(MTH4AgoAmount)-parseFloat(MTH5AgoAmount);
                if(totalamInvQ1>0)
                {
                    totalamInvQ1=totalamInvQ1*(-1);
                }
                if(categoryInv1=='Interconnection')
                {
                    interconnectionMRR5=parseFloat(interconnectionMRR5)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Other Recurring')
                {
                    otherRecurringMRR5=parseFloat(otherRecurringMRR5)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Virtual Interconnection')
                {
                    virtualInterconnectionMRR5=parseFloat(virtualInterconnectionMRR5)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Network')
                {
                    networkMRR5=parseFloat(networkMRR5)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Space')
                {
                    spaceMRR5=parseFloat(spaceMRR5)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Power')
                {
                    powerMRR5=parseFloat(powerMRR5)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Disaster Recovery')
                {
                    disasterRecoveryMRR5=parseFloat(disasterRecoveryMRR5)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
                if(categoryInv1=='Remote Hands')
                {
                    remoteHandsMRR5=parseFloat(remoteHandsMRR5)+parseFloat(totalamInvQ1);
                    stringCSV=stringCSV+'{"Customer":"'+customerName+'", "Type":"Churn", "Category Revenue":"Decrease","Date":"'+startDate_6+'", "Amount":"'+parseFloat(totalamInvQ1).toFixed(2)+'", "Market":"'+locationInv+'","Product":"'+categoryInv1+'","Currency":"'+currencyINV1+'"},'

                }
            }
        }
        return true; // return true to keep iterating

    });
    if(marketid==0) {
//NNJ
        var searchInvoices = nlapiLoadSearch('transaction', 'customsearch_clgx_inv_revenuegr_all_4');
        searchInvoices.addColumns(arrColumnsNewMRR1);
        searchInvoices.addFilters(arrFiltersNewMRR1);
        var resultINV = searchInvoices.runSearch();
        resultINV.forEachResult(function (searchResult) {
            var columns = searchResult.getAllColumns();
            var customerID = searchResult.getValue(columns[0]);
            var customerName = searchResult.getValue(columns[1]);
            var locationInv = searchResult.getText(columns[2]);
            var currMTHAmount = searchResult.getValue(columns[3]);
            var bfMTHAmount = searchResult.getValue(columns[4]);
            var MTH1AgoAmount = searchResult.getValue(columns[5]);
            var MTH2AgoAmount = searchResult.getValue(columns[6]);
            var MTH3AgoAmount = searchResult.getValue(columns[7]);
            var MTH4AgoAmount = searchResult.getValue(columns[8]);
            var MTH5AgoAmount = searchResult.getValue(columns[9]);
            var currMTHQTY = searchResult.getValue(columns[10]);
            var bfMTHQTY = searchResult.getValue(columns[11]);
            var MTH1AgoQTY = searchResult.getValue(columns[12]);
            var MTH2AgoQTY = searchResult.getValue(columns[13]);
            var MTH3AgoQTY = searchResult.getValue(columns[14]);
            var MTH4AgoQTY = searchResult.getValue(columns[15]);
            var MTH5AgoQTY = searchResult.getValue(columns[16]);
            var categoryInv1 = searchResult.getText(columns[17]);
            var currencyINV1 = searchResult.getText(columns[18]);
            var arrDates_1 = getDateRange(-1);
            var startDate_1 = arrDates_1[0];
            var arrDates_2 = getDateRange(-2);
            var startDate_2 = arrDates_2[0];
            var arrDates_3 = getDateRange(-3);
            var startDate_3 = arrDates_3[0];
            var arrDates_4 = getDateRange(-4);
            var startDate_4 = arrDates_4[0];
            var arrDates_5 = getDateRange(-5);
            var startDate_5 = arrDates_5[0];
            var arrDates_6 = getDateRange(-6);
            var startDate_6 = arrDates_6[0];
            var index = 0;
            var test = customerName + ';' + locationInv;
            index = _.indexOf(locationMRRArray, test);
            /* for ( var l = 0;customerMRRArray  != null && l < customerMRRArray.length; l++ ) {

             if(customerMRRArray[l]==customerName)
             {
             index=l;
             }
             }*/
            //1  MT AGO
            if ((parseFloat(currMTHAmount) != 0) && (parseFloat(bfMTHAmount) == 0)) {

                //var emailSubject = 'July Revenue3';
                // var emailBody="Values: "+locationMRRArray[0];
                // nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null); // Send email to Catalina

                if (index > 0) {
                    if ((parseFloat(totalAmountCurrMRRArray[index]) != 0) && (parseFloat(totalAmountBFMRRArray[index]) == 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"New MRR","Date":"' + startDate_1 + '", "Amount":"' + parseFloat(currMTHAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        newMRR1MTTotal = parseFloat(newMRR1MTTotal) + parseFloat(currMTHAmount);
                    }
                    else {
                        upMRR1MTTotal = parseFloat(upMRR1MTTotal) + parseFloat(currMTHAmount);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_1 + '", "Amount":"' + parseFloat(currMTHAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }
            else {
                if (parseInt(currMTHQTY) > parseInt(bfMTHQTY)) {

                    var totalamInvQ = parseFloat(currMTHAmount) - parseFloat(bfMTHAmount);
                    if (totalamInvQ > 0) {
                        upMRR1MTTotal = parseFloat(upMRR1MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_1 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                    }


                }
                else if (parseFloat(currMTHAmount) > parseFloat(bfMTHAmount)) {


                    var totalamInvQ1 = parseFloat(currMTHAmount) - parseFloat(bfMTHAmount);
                    if (totalamInvQ1 > 0) {
                        prMRR1MTTotal = parseFloat(prMRR1MTTotal) + parseFloat(totalamInvQ1);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"' + startDate_1 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }
            //2  MT AGO
            if ((parseFloat(bfMTHAmount) != 0) && (parseFloat(MTH1AgoAmount) == 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmountBFMRRArray[index]) != 0) && (parseFloat(totalAmount1MRRArray[index]) == 0)) {

                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"New MRR","Date":"' + startDate_2 + '", "Amount":"' + parseFloat(bfMTHAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        newMRR2MTTotal = parseFloat(newMRR2MTTotal) + parseFloat(bfMTHAmount);
                    }
                    else {
                        upMRR2MTTotal = parseFloat(upMRR2MTTotal) + parseFloat(bfMTHAmount);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_2 + '", "Amount":"' + parseFloat(bfMTHAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }
            else {
                if (parseInt(bfMTHQTY) > parseInt(MTH1AgoQTY)) {

                    var totalamInvQ = parseFloat(bfMTHAmount) - parseFloat(MTH1AgoAmount);
                    if (totalamInvQ > 0) {
                        upMRR2MTTotal = parseFloat(upMRR2MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_2 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                    }


                }
                else if (parseFloat(bfMTHAmount) > parseFloat(MTH1AgoAmount)) {


                    var totalamInvQ1 = parseFloat(bfMTHAmount) - parseFloat(MTH1AgoAmount);
                    if (totalamInvQ1 > 0) {
                        prMRR2MTTotal = parseFloat(prMRR2MTTotal) + parseFloat(totalamInvQ1);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"' + startDate_2 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }

            //3 MT AGO
            if ((parseFloat(MTH1AgoAmount) != 0) && (parseFloat(MTH2AgoAmount) == 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmount1MRRArray[index]) != 0) && (parseFloat(totalAmount2MRRArray[index]) == 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"New MRR","Date":"' + startDate_3 + '", "Amount":"' + parseFloat(MTH1AgoAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        newMRR3MTTotal = parseFloat(newMRR3MTTotal) + parseFloat(MTH1AgoAmount);
                    }
                    else {
                        upMRR3MTTotal = parseFloat(upMRR3MTTotal) + parseFloat(MTH1AgoAmount);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_3 + '", "Amount":"' + parseFloat(MTH1AgoAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }
            else {
                if (parseInt(MTH1AgoQTY) > parseInt(MTH2AgoQTY)) {

                    var totalamInvQ = parseFloat(MTH1AgoAmount) - parseFloat(MTH2AgoAmount);
                    if (totalamInvQ > 0) {
                        upMRR3MTTotal = parseFloat(upMRR3MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_3 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                    }


                }
                else if (parseFloat(MTH1AgoAmount) > parseFloat(MTH2AgoAmount)) {


                    var totalamInvQ1 = parseFloat(MTH1AgoAmount) - parseFloat(MTH2AgoAmount);
                    if (totalamInvQ1 > 0) {
                        prMRR3MTTotal = parseFloat(prMRR3MTTotal) + parseFloat(totalamInvQ1);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"' + startDate_3 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }
//4 MT AGO
            if ((parseFloat(MTH2AgoAmount) != 0) && (parseFloat(MTH3AgoAmount) == 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmount2MRRArray[index]) != 0) && (parseFloat(totalAmount3MRRArray[index]) == 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"New MRR","Date":"' + startDate_4 + '", "Amount":"' + parseFloat(MTH2AgoAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        newMRR4MTTotal = parseFloat(newMRR4MTTotal) + parseFloat(MTH2AgoAmount);
                    }
                    else {
                        upMRR4MTTotal = parseFloat(upMRR4MTTotal) + parseFloat(MTH2AgoAmount);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_4 + '", "Amount":"' + parseFloat(MTH2AgoAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                    }
                }
            }
            else {
                if (parseInt(MTH2AgoQTY) > parseInt(MTH3AgoQTY)) {

                    var totalamInvQ = parseFloat(MTH2AgoAmount) - parseFloat(MTH3AgoAmount);
                    if (totalamInvQ > 0) {
                        upMRR4MTTotal = parseFloat(upMRR4MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_4 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                    }


                }
                else if (parseFloat(MTH2AgoAmount) > parseFloat(MTH3AgoAmount)) {


                    var totalamInvQ1 = parseFloat(MTH2AgoAmount) - parseFloat(MTH3AgoAmount);
                    if (totalamInvQ1 > 0) {
                        prMRR4MTTotal = parseFloat(prMRR4MTTotal) + parseFloat(totalamInvQ1);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"' + startDate_4 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }
            //5 MT AGO
            if ((parseFloat(MTH3AgoAmount) != 0) && (parseFloat(MTH4AgoAmount) == 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmount3MRRArray[index]) != 0) && (parseFloat(totalAmount4MRRArray[index]) == 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"New MRR","Date":"' + startDate_5 + '", "Amount":"' + parseFloat(MTH3AgoAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        newMRR5MTTotal = parseFloat(newMRR5MTTotal) + parseFloat(MTH3AgoAmount);
                    }
                    else {
                        upMRR5MTTotal = parseFloat(upMRR5MTTotal) + parseFloat(MTH3AgoAmount);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_5 + '", "Amount":"' + parseFloat(MTH3AgoAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }
            else {
                if (parseInt(MTH3AgoQTY) > parseInt(MTH4AgoQTY)) {

                    var totalamInvQ = parseFloat(MTH3AgoAmount) - parseFloat(MTH4AgoAmount);
                    if (totalamInvQ > 0) {
                        upMRR5MTTotal = parseFloat(upMRR5MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_5 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                    }


                }
                else if (parseFloat(MTH3AgoAmount) > parseFloat(MTH4AgoAmount)) {


                    var totalamInvQ1 = parseFloat(MTH3AgoAmount) - parseFloat(MTH4AgoAmount);
                    if (totalamInvQ1 > 0) {
                        prMRR5MTTotal = parseFloat(prMRR5MTTotal) + parseFloat(totalamInvQ1);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"' + startDate_5 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }
            //6 MT AGO
            if ((parseFloat(MTH4AgoAmount) != 0) && (parseFloat(MTH5AgoAmount) == 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmount4MRRArray[index]) != 0) && (parseFloat(totalAmount5MRRArray[index]) == 0)) {

                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"New MRR","Date":"' + startDate_6 + '", "Amount":"' + parseFloat(MTH4AgoAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        newMRR6MTTotal = parseFloat(newMRR6MTTotal) + parseFloat(MTH4AgoAmount);
                    }
                    else {
                        upMRR6MTTotal = parseFloat(upMRR6MTTotal) + parseFloat(MTH4AgoAmount);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_6 + '", "Amount":"' + parseFloat(MTH4AgoAmount).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                    }
                }
            }
            else {
                if (parseInt(MTH4AgoQTY) > parseInt(MTH5AgoQTY)) {

                    var totalamInvQ = parseFloat(MTH4AgoAmount) - parseFloat(MTH5AgoAmount);
                    if (totalamInvQ > 0) {
                        upMRR6MTTotal = parseFloat(upMRR6MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Upgrade MRR","Date":"' + startDate_6 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                    }


                }
                else if (parseFloat(MTH4AgoAmount) > parseFloat(MTH5AgoAmount)) {


                    var totalamInvQ1 = parseFloat(MTH4AgoAmount) - parseFloat(MTH5AgoAmount);
                    if (totalamInvQ1 > 0) {
                        prMRR6MTTotal = parseFloat(prMRR6MTTotal) + parseFloat(totalamInvQ1);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Invoice", "Category Revenue":"Price Increase","Date":"' + startDate_6 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
            }

            //CHURN

            //1  MT AGO
            if ((parseFloat(currMTHAmount) == 0) && (parseFloat(bfMTHAmount) != 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmountCurrMRRArray[index]) == 0) && (parseFloat(totalAmountBFMRRArray[index]) != 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Disconnect","Date":"' + startDate_1 + '", "Amount":"' + parseFloat(((-1) * bfMTHAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                        disco1MTTotal = parseFloat(disco1MTTotal) + parseFloat((-1)) * parseFloat(bfMTHAmount);
                    }
                    else {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_1 + '", "Amount":"' + parseFloat(((-1) * bfMTHAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        don1MTTotal = parseFloat(don1MTTotal) + parseFloat((-1)) * parseFloat(bfMTHAmount);
                    }
                }
            }
            else {
                if (parseInt(currMTHQTY) < parseInt(bfMTHQTY)) {

                    var totalamInvQ = parseFloat(currMTHAmount) - parseFloat(bfMTHAmount);
                    if (totalamInvQ < 0) {
                        don1MTTotal = parseFloat(don1MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_1 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
                else if (parseFloat(currMTHAmount) < parseFloat(bfMTHAmount)) {


                    var totalamInvQ1 = parseFloat(currMTHAmount) - parseFloat(bfMTHAmount);
                    if (totalamInvQ1 > 0) {
                        totalamInvQ1 = totalamInvQ1 * (-1);
                    }
                    dec1MTTotal = parseFloat(dec1MTTotal) + parseFloat(totalamInvQ1);
                    stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Decrease","Date":"' + startDate_1 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'

                }
            }
            //2  MT AGO
            if ((parseFloat(bfMTHAmount) == 0) && (parseFloat(MTH1AgoAmount) != 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmountBFMRRArray[index]) == 0) && (parseFloat(totalAmount1MRRArray[index]) != 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Disconnect","Date":"' + startDate_2 + '", "Amount":"' + parseFloat(((-1) * MTH1AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        disco2MTTotal = parseFloat(disco2MTTotal) + parseFloat((-1)) * parseFloat(MTH1AgoAmount);
                    }
                    else {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_2 + '", "Amount":"' + parseFloat(((-1) * MTH1AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        don2MTTotal = parseFloat(don2MTTotal) + parseFloat((-1)) * parseFloat(MTH1AgoAmount);

                    }
                }

            }
            else {
                if (parseInt(bfMTHQTY) < parseInt(MTH1AgoQTY)) {

                    var totalamInvQ = parseFloat(bfMTHAmount) - parseFloat(MTH1AgoAmount);
                    if (totalamInvQ < 0) {
                        don2MTTotal = parseFloat(don2MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_2 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
                else if (parseFloat(bfMTHAmount) < parseFloat(MTH1AgoAmount)) {


                    var totalamInvQ1 = parseFloat(bfMTHAmount) - parseFloat(MTH1AgoAmount);
                    if (totalamInvQ1 > 0) {
                        totalamInvQ1 = totalamInvQ1 * (-1);
                    }
                    dec2MTTotal = parseFloat(dec2MTTotal) + parseFloat(totalamInvQ1);
                    stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Decrease","Date":"' + startDate_2 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                }
            }

            //3 MT AGO
            if ((parseFloat(MTH1AgoAmount) == 0) && (parseFloat(MTH2AgoAmount) != 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmount1MRRArray[index]) == 0) && (parseFloat(totalAmount2MRRArray[index]) != 0)) {

                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Disconnect","Date":"' + startDate_3 + '", "Amount":"' + parseFloat(((-1) * MTH2AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        disco3MTTotal = parseFloat(disco3MTTotal) + parseFloat((-1)) * parseFloat(MTH2AgoAmount);
                    }
                    else {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_3 + '", "Amount":"' + parseFloat(((-1) * MTH2AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        don3MTTotal = parseFloat(don3MTTotal) + parseFloat((-1)) * parseFloat(MTH2AgoAmount);

                    }
                }
            }
            else {
                if (parseInt(MTH1AgoQTY) < parseInt(MTH2AgoQTY)) {

                    var totalamInvQ = parseFloat(MTH1AgoAmount) - parseFloat(MTH2AgoAmount);
                    if (totalamInvQ < 0) {
                        don3MTTotal = parseFloat(don3MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_3 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
                else if (parseFloat(MTH1AgoAmount) < parseFloat(MTH2AgoAmount)) {


                    var totalamInvQ1 = parseFloat(MTH1AgoAmount) - parseFloat(MTH2AgoAmount);
                    if (totalamInvQ1 > 0) {
                        totalamInvQ1 = totalamInvQ1 * (-1);
                    }
                    dec3MTTotal = parseFloat(dec3MTTotal) + parseFloat(totalamInvQ1);
                    stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Decrease","Date":"' + startDate_3 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                }
            }
//4 MT AGO
            if ((parseFloat(MTH2AgoAmount) == 0) && (parseFloat(MTH3AgoAmount) != 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmount2MRRArray[index]) == 0) && (parseFloat(totalAmount3MRRArray[index]) != 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Disconnect","Date":"' + startDate_4 + '", "Amount":"' + parseFloat(((-1) * MTH3AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        disco4MTTotal = parseFloat(disco4MTTotal) + parseFloat((-1)) * parseFloat(MTH3AgoAmount);
                    }
                    else {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_4 + '", "Amount":"' + parseFloat(((-1) * MTH3AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        don4MTTotal = parseFloat(don4MTTotal) + parseFloat((-1)) * parseFloat(MTH3AgoAmount);
                    }
                }
            }
            else {
                if (parseInt(MTH2AgoQTY) < parseInt(MTH3AgoQTY)) {

                    var totalamInvQ = parseFloat(MTH2AgoAmount) - parseFloat(MTH3AgoAmount);
                    if (totalamInvQ < 0) {
                        don4MTTotal = parseFloat(don4MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_4 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
                else if (parseFloat(MTH2AgoAmount) < parseFloat(MTH3AgoAmount)) {


                    var totalamInvQ1 = parseFloat(MTH2AgoAmount) - parseFloat(MTH3AgoAmount);
                    if (totalamInvQ1 > 0) {
                        totalamInvQ1 = totalamInvQ1 * (-1);
                    }
                    dec4MTTotal = parseFloat(dec4MTTotal) + parseFloat(totalamInvQ1);
                    stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Decrease","Date":"' + startDate_4 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                }
            }
            //5 MT AGO
            if ((parseFloat(MTH3AgoAmount) == 0) && (parseFloat(MTH4AgoAmount) != 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmount3MRRArray[index]) == 0) && (parseFloat(totalAmount4MRRArray[index]) != 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Disconnect","Date":"' + startDate_5 + '", "Amount":"' + parseFloat(((-1) * MTH4AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        disco5MTTotal = parseFloat(disco5MTTotal) + parseFloat((-1)) * parseFloat(MTH4AgoAmount);
                    }
                    else {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_5 + '", "Amount":"' + parseFloat(((-1) * MTH4AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        don5MTTotal = parseFloat(don5MTTotal) + parseFloat((-1)) * parseFloat(MTH4AgoAmount);
                    }
                }
            }
            else {
                if (parseInt(MTH3AgoQTY) < parseInt(MTH4AgoQTY)) {

                    var totalamInvQ = parseFloat(MTH3AgoAmount) - parseFloat(MTH4AgoAmount);
                    if (totalamInvQ < 0) {
                        don5MTTotal = parseFloat(don5MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_5 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
                else if (parseFloat(MTH3AgoAmount) < parseFloat(MTH4AgoAmount)) {


                    var totalamInvQ1 = parseFloat(MTH3AgoAmount) - parseFloat(MTH4AgoAmount);
                    if (totalamInvQ1 > 0) {
                        totalamInvQ1 = totalamInvQ1 * (-1);
                    }
                    dec5MTTotal = parseFloat(dec5MTTotal) + parseFloat(totalamInvQ1);
                    stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Decrease","Date":"' + startDate_5 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                }
            }
            //6 MT AGO
            if ((parseFloat(MTH4AgoAmount) == 0) && (parseFloat(MTH5AgoAmount) != 0)) {
                if (index > 0) {
                    if ((parseFloat(totalAmount4MRRArray[index]) == 0) && (parseFloat(totalAmount5MRRArray[index]) != 0)) {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Disconnect","Date":"' + startDate_6 + '", "Amount":"' + parseFloat(((-1) * MTH5AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        disco6MTTotal = parseFloat(disco6MTTotal) + parseFloat((-1)) * parseFloat(MTH5AgoAmount);
                    }
                    else {
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_6 + '", "Amount":"' + parseFloat(((-1) * MTH5AgoAmount)).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                        don6MTTotal = parseFloat(don6MTTotal) + parseFloat((-1)) * parseFloat(MTH5AgoAmount);
                    }
                }
            }
            else {
                if (parseInt(MTH4AgoQTY) < parseInt(MTH5AgoQTY)) {

                    var totalamInvQ = parseFloat(MTH4AgoAmount) - parseFloat(MTH5AgoAmount);
                    if (totalamInvQ < 0) {
                        don6MTTotal = parseFloat(don6MTTotal) + parseFloat(totalamInvQ);
                        stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Downgrade","Date":"' + startDate_6 + '", "Amount":"' + parseFloat(totalamInvQ).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                    }
                }
                else if (parseFloat(MTH4AgoAmount) < parseFloat(MTH5AgoAmount)) {


                    var totalamInvQ1 = parseFloat(MTH4AgoAmount) - parseFloat(MTH5AgoAmount);
                    if (totalamInvQ1 > 0) {
                        totalamInvQ1 = totalamInvQ1 * (-1);
                    }
                    dec6MTTotal = parseFloat(dec6MTTotal) + parseFloat(totalamInvQ1);
                    stringCSV = stringCSV + '{"Customer":"' + customerName + '", "Type":"Churn", "Category Revenue":"Decrease","Date":"' + startDate_6 + '", "Amount":"' + parseFloat(totalamInvQ1).toFixed(2) + '", "Market":"' + locationInv + '","Product":"' + categoryInv1 + '","Currency":"' + currencyINV1 + '"},'
                }
            }
            return true; // return true to keep iterating

        });
    }
    stringseries += '\n{name: \'Interconnection\',type: \'column\',  data: \n[\n';
    stringseries += '{y:' + parseFloat(interconnectionMRR5).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-5&scriptid=3&product=inter&productlabel=Interconnection ' +'\'},\n';
    stringseries += '{y:' + parseFloat(interconnectionMRR4).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-4&scriptid=3&product=inter&productlabel=Interconnection '+'\'},\n';
    stringseries += '{y:' + parseFloat(interconnectionMRR3).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-3&scriptid=3&product=inter&productlabel=Interconnection ' +'\'},\n';
    stringseries += '{y:' + parseFloat(interconnectionMRR2).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-2&scriptid=3&product=inter&productlabel=Interconnection ' +'\'},\n';
    stringseries += '{y:' + parseFloat(interconnectionMRR1).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-1&scriptid=3&product=inter&productlabel=Interconnection ' +'\'},\n';
    stringseries += '{y:' + parseFloat(interconnectionMRR).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=0&scriptid=3&product=inter&productlabel=Interconnection '+'\'}';

    stringseries += ']\n';
    stringseries += '},\n';
    stringseries += '\n{name: \'Power\',type: \'column\',  data: \n[\n';
    stringseries += '{y:' + parseFloat(powerMRR5).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-5&scriptid=3&product=pow&productlabel=Power' +'\'},\n';
    stringseries += '{y:' + parseFloat(powerMRR4).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-4&scriptid=3&product=pow&productlabel=Power'+'\'},\n';
    stringseries += '{y:' + parseFloat(powerMRR3).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-3&scriptid=3&product=pow&productlabel=Power' +'\'},\n';
    stringseries += '{y:' + parseFloat(powerMRR2).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-2&scriptid=3&product=pow&productlabel=Power' +'\'},\n';
    stringseries += '{y:' + parseFloat(powerMRR1).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-1&scriptid=3&product=pow&productlabel=Power' +'\'},\n';
    stringseries += '{y:' + parseFloat(powerMRR).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=0&scriptid=3&product=pow&productlabel=Power'+'\'}';

    stringseries += ']\n';
    stringseries += '},\n';

    stringseries += '\n{name: \'Network\',type: \'column\',  data: \n[\n';
    stringseries += '{y:' + parseFloat(networkMRR5).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-5&scriptid=3&product=net&productlabel=Network' +'\'},\n';
    stringseries += '{y:' + parseFloat(networkMRR4).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-4&scriptid=3&product=net&productlabel=Network'+'\'},\n';
    stringseries += '{y:' + parseFloat(networkMRR3).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-3&scriptid=3&product=net&productlabel=Network' +'\'},\n';
    stringseries += '{y:' + parseFloat(networkMRR2).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-2&scriptid=3&product=net&productlabel=Network' +'\'},\n';
    stringseries += '{y:' + parseFloat(networkMRR1).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-1&scriptid=3&product=net&productlabel=Network' +'\'},\n';
    stringseries += '{y:' + parseFloat(networkMRR).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=0&scriptid=3&product=net&productlabel=Network'+'\'}';

    stringseries += ']\n';
    stringseries += '},\n';
    stringseries += '\n{name: \'Virtual Interconnection\',type: \'column\',  data: \n[\n';
    stringseries += '{y:' + parseFloat(virtualInterconnectionMRR5).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-5&scriptid=3&product=virtual&productlabel=Virtual Interconnection' +'\'},\n';
    stringseries += '{y:' + parseFloat(virtualInterconnectionMRR4).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-4&scriptid=3&product=virtual&productlabel=Virtual Interconnection'+'\'},\n';
    stringseries += '{y:' + parseFloat(virtualInterconnectionMRR3).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-3&scriptid=3&product=virtual&productlabel=Virtual Interconnection' +'\'},\n';
    stringseries += '{y:' + parseFloat(virtualInterconnectionMRR2).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-2&scriptid=3&product=virtual&productlabel=Virtual Interconnection' +'\'},\n';
    stringseries += '{y:' + parseFloat(virtualInterconnectionMRR1).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-1&scriptid=3&product=virtual&productlabel=Virtual Interconnection' +'\'},\n';
    stringseries += '{y:' + parseFloat(virtualInterconnectionMRR).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=0&scriptid=3&product=virtual&productlabel=Virtual Interconnection'+'\'}';

    stringseries += ']\n';
    stringseries += '},\n';
    stringseries += '\n{name: \'Other Recurring\',type: \'column\',  data: \n[\n';
    stringseries += '{y:' + parseFloat(otherRecurringMRR5).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-5&scriptid=3&product=other&productlabel=Other Recurring' +'\'},\n';
    stringseries += '{y:' + parseFloat(otherRecurringMRR4).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-4&scriptid=3&product=other&productlabel=Other Recurring'+'\'},\n';
    stringseries += '{y:' + parseFloat(otherRecurringMRR3).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-3&scriptid=3&product=other&productlabel=Other Recurring' +'\'},\n';
    stringseries += '{y:' + parseFloat(otherRecurringMRR2).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-2&scriptid=3&product=other&productlabel=Other Recurring' +'\'},\n';
    stringseries += '{y:' + parseFloat(otherRecurringMRR1).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-1&scriptid=3&product=other&productlabel=Other Recurring' +'\'},\n';
    stringseries += '{y:' + parseFloat(otherRecurringMRR).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=0&scriptid=3&product=other&productlabel=Other Recurring'+'\'}';
    stringseries += ']\n';
    stringseries += '},\n';
    stringseries += '\n{name: \'Space\',type: \'column\',  data: \n[\n';
    stringseries += '{y:' + parseFloat(spaceMRR5).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-5&scriptid=3&product=sp&productlabel=Space' +'\'},\n';
    stringseries += '{y:' + parseFloat(spaceMRR4).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-4&scriptid=3&product=sp&productlabel=Space'+'\'},\n';
    stringseries += '{y:' + parseFloat(spaceMRR3).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-3&scriptid=3&product=sp&productlabel=Space' +'\'},\n';
    stringseries += '{y:' + parseFloat(spaceMRR2).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-2&scriptid=3&product=sp&productlabel=Space' +'\'},\n';
    stringseries += '{y:' + parseFloat(spaceMRR1).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-1&scriptid=3&product=sp&productlabel=Space' +'\'},\n';
    stringseries += '{y:' + parseFloat(spaceMRR).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=0&scriptid=3&product=sp&productlabel=Space'+'\'}';
    stringseries += ']\n';
    stringseries += '},\n';
    stringseries += '\n{name: \'Disaster Recovery\',type: \'column\',  data: \n[\n';
    stringseries += '{y:' + parseFloat(disasterRecoveryMRR5).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-5&scriptid=3&product=disaster&productlabel=Disaster Recovery' +'\'},\n';
    stringseries += '{y:' + parseFloat(disasterRecoveryMRR4).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-4&scriptid=3&product=disaster&productlabel=Disaster Recovery'+'\'},\n';
    stringseries += '{y:' + parseFloat(disasterRecoveryMRR3).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-3&scriptid=3&product=disaster&productlabel=Disaster Recovery' +'\'},\n';
    stringseries += '{y:' + parseFloat(disasterRecoveryMRR2).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-2&scriptid=3&product=disaster&productlabel=Disaster Recovery' +'\'},\n';
    stringseries += '{y:' + parseFloat(disasterRecoveryMRR1).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-1&scriptid=3&product=disaster&productlabel=Disaster Recovery' +'\'},\n';
    stringseries += '{y:' + parseFloat(disasterRecoveryMRR).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=0&scriptid=3&product=disaster&productlabel=Disaster Recovery'+'\'}';
    stringseries += ']\n';
    stringseries += '},\n';
    stringseries += '\n{name: \'Remote Hands\',type: \'column\',  data: \n[\n';
    stringseries += '{y:' + parseFloat(remoteHandsMRR5).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-5&scriptid=3&product=remote&productlabel=Remote Hands' +'\'},\n';
    stringseries += '{y:' + parseFloat(remoteHandsMRR4).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-4&scriptid=3&product=remote&productlabel=Remote Hands'+'\'},\n';
    stringseries += '{y:' + parseFloat(remoteHandsMRR3).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-3&scriptid=3&product=remote&productlabel=Remote Hands' +'\'},\n';
    stringseries += '{y:' + parseFloat(remoteHandsMRR2).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-2&scriptid=3&product=remote&productlabel=Remote Hands' +'\'},\n';
    stringseries += '{y:' + parseFloat(remoteHandsMRR1).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=-1&scriptid=3&product=remote&productlabel=Remote Hands' +'\'},\n';
    stringseries += '{y:' + parseFloat(remoteHandsMRR).toFixed(2)  +',url:\'/app/site/hosting/scriptlet.nl?script=322&status=1&deploy=1&market=' + marketid + '&type=all&typelabel=all&month=0&scriptid=3&product=remote&productlabel=Remote Hands'+'\'}';
    stringseries += ']\n';
    stringseries += '},\n';
    //  stringseries="{name: 'Interconnection',data: ["+parseFloat(interconnectionMRR5).toFixed(2)+','+parseFloat(interconnectionMRR4).toFixed(2)+','+ parseFloat(interconnectionMRR3).toFixed(2)+','+parseFloat(interconnectionMRR2).toFixed(2)+','+parseFloat(interconnectionMRR1).toFixed(2)+','+parseFloat(interconnectionMRR).toFixed(2)+"],stack: 'Invoice'},";
    // stringseries=stringseries+"{name: 'Power',data: ["+parseFloat(powerMRR5).toFixed(2)+','+parseFloat(powerMRR4).toFixed(2)+','+ parseFloat(powerMRR3).toFixed(2)+','+parseFloat(powerMRR2).toFixed(2)+','+parseFloat(powerMRR1).toFixed(2)+','+parseFloat(powerMRR).toFixed(2)+"],stack: 'Invoice'},";
    // stringseries=stringseries+"{name: 'Network',data: ["+parseFloat(networkMRR5).toFixed(2)+','+parseFloat(networkMRR4).toFixed(2)+','+ parseFloat(networkMRR3).toFixed(2)+','+parseFloat(networkMRR2).toFixed(2)+','+parseFloat(networkMRR1).toFixed(2)+','+parseFloat(networkMRR).toFixed(2)+"],stack: 'Invoice'},";
    // stringseries=stringseries+"{name: 'Virtual Interconnection',data: ["+parseFloat(virtualInterconnectionMRR5).toFixed(2)+','+parseFloat(virtualInterconnectionMRR4).toFixed(2)+','+ parseFloat(virtualInterconnectionMRR3).toFixed(2)+','+parseFloat(virtualInterconnectionMRR2).toFixed(2)+','+parseFloat(virtualInterconnectionMRR1).toFixed(2)+','+parseFloat(virtualInterconnectionMRR).toFixed(2)+"],stack: 'Invoice'},";
    // stringseries=stringseries+"{name: 'Other Recurring',data: ["+parseFloat(otherRecurringMRR5).toFixed(2)+','+parseFloat(otherRecurringMRR4).toFixed(2)+','+ parseFloat(otherRecurringMRR3).toFixed(2)+','+parseFloat(otherRecurringMRR2).toFixed(2)+','+parseFloat(otherRecurringMRR1).toFixed(2)+','+parseFloat(otherRecurringMRR).toFixed(2)+"],stack: 'Invoice'},";
    // stringseries=stringseries+"{name: 'Space',data: ["+parseFloat(spaceMRR5).toFixed(2)+','+parseFloat(spaceMRR4).toFixed(2)+','+ parseFloat(spaceMRR3).toFixed(2)+','+parseFloat(spaceMRR2).toFixed(2)+','+parseFloat(spaceMRR1).toFixed(2)+','+parseFloat(spaceMRR).toFixed(2)+"],stack: 'Invoice'},";
    // stringseries=stringseries+"{name: 'Disaster Recovery',data: ["+parseFloat(disasterRecoveryMRR5).toFixed(2)+','+parseFloat(disasterRecoveryMRR4).toFixed(2)+','+ parseFloat(disasterRecoveryMRR3).toFixed(2)+','+parseFloat(disasterRecoveryMRR2).toFixed(2)+','+parseFloat(disasterRecoveryMRR1).toFixed(2)+','+parseFloat(disasterRecoveryMRR).toFixed(2)+"],stack: 'Invoice'},";
    // stringseries=stringseries+"{name: 'Remote Hands',data: ["+parseFloat(remoteHandsMRR5).toFixed(2)+','+parseFloat(remoteHandsMRR4).toFixed(2)+','+ parseFloat(remoteHandsMRR3).toFixed(2)+','+parseFloat(remoteHandsMRR2).toFixed(2)+','+parseFloat(remoteHandsMRR1).toFixed(2)+','+parseFloat(remoteHandsMRR).toFixed(2)+"],stack: 'Invoice'}";
    var grTotalI=parseFloat(parseFloat(interconnectionMRR5)+parseFloat(interconnectionMRR4)+ parseFloat(interconnectionMRR3)+parseFloat(interconnectionMRR2)+parseFloat(interconnectionMRR1)+parseFloat(interconnectionMRR)).toFixed(2);
    var grTotalPow=parseFloat(parseFloat(powerMRR5)+parseFloat(powerMRR4)+ parseFloat(powerMRR3)+parseFloat(powerMRR2)+parseFloat(powerMRR1)+parseFloat(powerMRR)).toFixed(2);
    var grTotalNet=parseFloat(parseFloat(networkMRR5)+parseFloat(networkMRR4)+ parseFloat(networkMRR3)+parseFloat(networkMRR2)+parseFloat(powerMRR1)+parseFloat(networkMRR)).toFixed(2);
    var grTotalVirtual=parseFloat(parseFloat(virtualInterconnectionMRR5)+parseFloat(virtualInterconnectionMRR4)+ parseFloat(virtualInterconnectionMRR3)+parseFloat(virtualInterconnectionMRR2)+parseFloat(virtualInterconnectionMRR1)+parseFloat(virtualInterconnectionMRR)).toFixed(2);
    var grTotalOt=parseFloat(parseFloat(otherRecurringMRR5)+parseFloat(otherRecurringMRR4)+ parseFloat(otherRecurringMRR3)+parseFloat(otherRecurringMRR2)+parseFloat(otherRecurringMRR1)+parseFloat(otherRecurringMRR)).toFixed(2);
    var grTotalSp=parseFloat(parseFloat(spaceMRR5)+parseFloat(spaceMRR4)+ parseFloat(spaceMRR3)+parseFloat(spaceMRR2)+parseFloat(spaceMRR1)+parseFloat(spaceMRR)).toFixed(2);
    var grTotalDisaster=parseFloat(parseFloat(disasterRecoveryMRR5)+parseFloat(disasterRecoveryMRR4)+ parseFloat(disasterRecoveryMRR3)+parseFloat(disasterRecoveryMRR2)+parseFloat(disasterRecoveryMRR1)+parseFloat(disasterRecoveryMRR)).toFixed(2);
    var grTotalRemote=parseFloat(parseFloat(remoteHandsMRR5)+parseFloat(remoteHandsMRR4)+ parseFloat(remoteHandsMRR3)+parseFloat(remoteHandsMRR2)+parseFloat(remoteHandsMRR1)+parseFloat(remoteHandsMRR)).toFixed(2);
    var grTotal1=parseFloat(parseFloat(interconnectionMRR5)+parseFloat(powerMRR5)+ parseFloat(networkMRR5)+parseFloat(virtualInterconnectionMRR5)+parseFloat(otherRecurringMRR5)+parseFloat(spaceMRR5)+parseFloat(disasterRecoveryMRR5)+parseFloat(remoteHandsMRR5)).toFixed(2);
    var grTotal2=parseFloat(parseFloat(interconnectionMRR4)+parseFloat(powerMRR4)+ parseFloat(networkMRR4)+parseFloat(virtualInterconnectionMRR4)+parseFloat(otherRecurringMRR4)+parseFloat(spaceMRR4)+parseFloat(disasterRecoveryMRR4)+parseFloat(remoteHandsMRR4)).toFixed(2);
    var grTotal3=parseFloat(parseFloat(interconnectionMRR3)+parseFloat(powerMRR3)+ parseFloat(networkMRR3)+parseFloat(virtualInterconnectionMRR3)+parseFloat(otherRecurringMRR3)+parseFloat(spaceMRR3)+parseFloat(disasterRecoveryMRR3)+parseFloat(remoteHandsMRR3)).toFixed(2);
    var grTotal4=parseFloat(parseFloat(interconnectionMRR2)+parseFloat(powerMRR2)+ parseFloat(networkMRR2)+parseFloat(virtualInterconnectionMRR2)+parseFloat(otherRecurringMRR2)+parseFloat(spaceMRR2)+parseFloat(disasterRecoveryMRR2)+parseFloat(remoteHandsMRR2)).toFixed(2);
    var grTotal5=parseFloat(parseFloat(interconnectionMRR1)+parseFloat(powerMRR1)+ parseFloat(networkMRR1)+parseFloat(virtualInterconnectionMRR1)+parseFloat(otherRecurringMRR1)+parseFloat(spaceMRR1)+parseFloat(disasterRecoveryMRR1)+parseFloat(remoteHandsMRR1)).toFixed(2);
    var grTotal6=parseFloat(parseFloat(interconnectionMRR)+parseFloat(powerMRR)+ parseFloat(networkMRR)+parseFloat(virtualInterconnectionMRR)+parseFloat(otherRecurringMRR)+parseFloat(spaceMRR)+parseFloat(disasterRecoveryMRR)+parseFloat(remoteHandsMRR)).toFixed(2);
    var grTotal7=parseFloat(parseFloat(grTotalI)+parseFloat(grTotalPow)+ parseFloat(grTotalNet)+parseFloat(grTotalVirtual)+parseFloat(grTotalOt)+parseFloat(grTotalSp)+parseFloat(grTotalDisaster)+parseFloat(grTotalRemote)).toFixed(2);

    // stringCSV=stringCSV+'{"Row Labels":"'+"Interconnection"+'", "8/1/2014":"'+parseFloat(interconnectionMRR5).toFixed(2)+'", "9/1/2014":"'+parseFloat(interconnectionMRR4).toFixed(2)+'", "10/1/2014":"'+parseFloat(interconnectionMRR3).toFixed(2)+'", "11/1/2014":"'+parseFloat(interconnectionMRR2).toFixed(2)+'", "12/1/2014":"'+parseFloat(interconnectionMRR1).toFixed(2)+'", "1/1/2015":"'+parseFloat(interconnectionMRR).toFixed(2)+'", "Grand Total":"'+grTotalI+'"},';
    // stringCSV=stringCSV+'{"Row Labels":"'+"Power"+'", "8/1/2014":"'+parseFloat(powerMRR5).toFixed(2)+'", "9/1/2014":"'+parseFloat(powerMRR4).toFixed(2)+'", "10/1/2014":"'+parseFloat(powerMRR3).toFixed(2)+'", "11/1/2014":"'+parseFloat(powerMRR2).toFixed(2)+'", "12/1/2014":"'+parseFloat(powerMRR1).toFixed(2)+'", "1/1/2015":"'+parseFloat(powerMRR).toFixed(2)+'", "Grand Total":"'+grTotalPow+'"},';
    // stringCSV=stringCSV+'{"Row Labels":"'+"Network"+'", "8/1/2014":"'+parseFloat(networkMRR5).toFixed(2)+'", "9/1/2014":"'+parseFloat(networkMRR4).toFixed(2)+'", "10/1/2014":"'+parseFloat(networkMRR3).toFixed(2)+'", "11/1/2014":"'+parseFloat(networkMRR2).toFixed(2)+'", "12/1/2014":"'+parseFloat(networkMRR1).toFixed(2)+'", "1/1/2015":"'+parseFloat(networkMRR).toFixed(2)+'", "Grand Total":"'+grTotalNet+'"},';
    // stringCSV=stringCSV+'{"Row Labels":"'+"Virtual Interconnection"+'", "8/1/2014":"'+parseFloat(virtualInterconnectionMRR5).toFixed(2)+'", "9/1/2014":"'+parseFloat(virtualInterconnectionMRR4).toFixed(2)+'", "10/1/2014":"'+parseFloat(virtualInterconnectionMRR3).toFixed(2)+'", "11/1/2014":"'+parseFloat(virtualInterconnectionMRR2).toFixed(2)+'", "12/1/2014":"'+parseFloat(virtualInterconnectionMRR1).toFixed(2)+'", "1/1/2015":"'+parseFloat(virtualInterconnectionMRR).toFixed(2)+'", "Grand Total":"'+grTotalVirtual+'"},';
    // stringCSV=stringCSV+'{"Row Labels":"'+"Other Recurring"+'", "8/1/2014":"'+parseFloat(otherRecurringMRR5).toFixed(2)+'", "9/1/2014":"'+parseFloat(otherRecurringMRR4).toFixed(2)+'", "10/1/2014":"'+parseFloat(otherRecurringMRR3).toFixed(2)+'", "11/1/2014":"'+parseFloat(otherRecurringMRR2).toFixed(2)+'", "12/1/2014":"'+parseFloat(otherRecurringMRR1).toFixed(2)+'", "1/1/2015":"'+parseFloat(otherRecurringMRR).toFixed(2)+'", "Grand Total":"'+grTotalOt+'"},';
    // stringCSV=stringCSV+'{"Row Labels":"'+"Space"+'", "8/1/2014":"'+parseFloat(spaceMRR5).toFixed(2)+'", "9/1/2014":"'+parseFloat(spaceMRR4).toFixed(2)+'", "10/1/2014":"'+parseFloat(spaceMRR3).toFixed(2)+'", "11/1/2014":"'+parseFloat(spaceMRR2).toFixed(2)+'", "12/1/2014":"'+parseFloat(spaceMRR1).toFixed(2)+'", "1/1/2015":"'+parseFloat(spaceMRR).toFixed(2)+'", "Grand Total":"'+grTotalSp+'"},';
    // stringCSV=stringCSV+'{"Row Labels":"'+"Disaster Recovery"+'", "8/1/2014":"'+parseFloat(disasterRecoveryMRR5).toFixed(2)+'", "9/1/2014":"'+parseFloat(disasterRecoveryMRR4).toFixed(2)+'", "10/1/2014":"'+parseFloat(disasterRecoveryMRR3).toFixed(2)+'", "11/1/2014":"'+parseFloat(disasterRecoveryMRR2).toFixed(2)+'", "12/1/2014":"'+parseFloat(disasterRecoveryMRR1).toFixed(2)+'", "1/1/2015":"'+parseFloat(disasterRecoveryMRR).toFixed(2)+'", "Grand Total":"'+grTotalDisaster+'"},';
    // stringCSV=stringCSV+'{"Row Labels":"'+"Remote Hands"+'", "8/1/2014":"'+parseFloat(remoteHandsMRR5).toFixed(2)+'", "9/1/2014":"'+parseFloat(remoteHandsMRR4).toFixed(2)+'", "10/1/2014":"'+parseFloat(remoteHandsMRR3).toFixed(2)+'", "11/1/2014":"'+parseFloat(remoteHandsMRR2).toFixed(2)+'", "12/1/2014":"'+parseFloat(remoteHandsMRR1).toFixed(2)+'", "1/1/2015":"'+parseFloat(remoteHandsMRR).toFixed(2)+'", "Grand Total":"'+grTotalRemote+'"},';
    // stringCSV=stringCSV+'{"Row Labels":"'+"Grand Total"+'", "8/1/2014":"'+grTotal1+'", "9/1/2014":"'+grTotal2+'", "10/1/2014":"'+grTotal3+'", "11/1/2014":"'+grTotal4+'", "12/1/2014":"'+grTotal5+'", "1/1/2015":"'+grTotal6+'", "Grand Total":"'+grTotal7+'"}';



    var returnArray=[stringCSV,stringseries];
    return returnArray;

}




function getDateRange(monthsago){

    var today = new Date();
    var date = nlapiAddMonths(today, monthsago);

    var month = parseInt(date.getMonth());
    var year = date.getFullYear();

    var stDays  = daysInMonth(parseInt(month),parseInt(year));
    var stYear  = year;

    var stMonth = parseInt(month) + 1;

    var stStartDate = stMonth + '/1/' + stYear;
    var stEndDate   = stMonth + '/' + stDays + '/' + stYear;

    var arrDateRange = new Array();
    arrDateRange[0] = stStartDate;
    arrDateRange[1] =stEndDate;

    return arrDateRange;
}


function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}

//check if a value is in the array
function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}