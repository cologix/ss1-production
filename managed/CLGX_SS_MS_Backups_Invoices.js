//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_MS_Backups_Invoices.js
//	Script Name:	CLGX_SS_MS_Backups_Invoices
//	Script Id:		customscript_clgx_ss_ms_backups_invoices
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		07/22/2018
//-------------------------------------------------------------------------------------------------

function scheduled__ms_backups_invoices (){
    try{
    	
    	var context = nlapiGetContext();
        var environment = context.getEnvironment();
        
        if(environment == 'PRODUCTION'){
        	

    		// managed services from netsuite
    		var services = [];
    		var managed = [];
    		var search = nlapiSearchRecord('customrecord_clgx_managed_services', 'customsearch_clgx_ms_backup_usage');
    		for ( var i = 0; search != null && i < search.length; i++ ) {
    			var columns = search[i].getAllColumns();
    			var service_id = parseInt(search[i].getValue(columns[1]));
    			managed.push({
    					"ms_id"        : parseInt(search[i].getValue(columns[0])),
    					"service_id"   : service_id,
    					"vaultname"         : search[i].getValue(columns[2]),
    					"equipment_id" : parseInt(search[i].getValue(columns[3]))
    				});
    			services.push(service_id);
    	    }
    		
    		// quantities from netsuite SOs lines
    		var sos = [];
    		var columns = [];
    		var filters = [];
    		filters.push(new nlobjSearchFilter("custcol_clgx_so_col_service_id",null,"anyof",services));
    		var search = nlapiSearchRecord('transaction', 'customsearch_clgx_ms_backup_usage_serv', filters, columns);
    		for ( var i = 0; search != null && i < search.length; i++ ) {
    			var columns = search[i].getAllColumns();
    			var service_id = parseInt(search[i].getValue(columns[6]));
    			var objMS = _.find(managed, function(arr){ return (arr.service_id == service_id) ; });
    			if(objMS){
    				sos.push({
    					"so_id"         : parseInt(search[i].getValue(columns[0])),
    					"customer_id"   : parseInt(search[i].getValue(columns[1])),
    					"subsidiary_id" : parseInt(search[i].getValue(columns[2])),
    					"clocation_id"  : parseInt(search[i].getValue(columns[3])),
    					"location_id"   : parseInt(search[i].getValue(columns[4])),
    					"addressee"     : search[i].getValue(columns[5]),
    					"ms_id"         : objMS.ms_id,
    					"service_id"    : objMS.service_id,
    					"vaultname"     : objMS.vaultname,
    					"equipment_id"  : objMS.equipment_id,
    					"item_id"       : parseInt(search[i].getValue(columns[7])) || 0,
    					"class_id"      : parseInt(search[i].getValue(columns[8])) || 0,
    					"quantity"      : parseInt(search[i].getValue(columns[9])) || 0,
    					"rate"          : parseFloat(search[i].getValue(columns[10])) || 0
    				});
    			}
    	    }
    		
    		// request backups usage from vaults via clgx_cmd
    		var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/dw/backup/get_backup_overage.cfm');
    		var vaults= JSON.parse( requestURL.body );
    		
    		var billing_period = moment().startOf('month').add('month', 1).format('MM/DD/YYYY');
    		var occured = moment().startOf('month').format('MMMM YYYY');
    		
    		// usage netsuite with vaults data
    		var usage = [];
    		for ( var i = 0; i < vaults.length; i++ ) {
    			var objNS = _.find(sos, function(arr){ return (arr.vaultname == vaults[i].Customer && arr.equipment_id == vaults[i].Equipment) ; });
    			if(objNS){
    				usage.push({
    					"so_id"           : objNS.so_id,
    					"customer_id"     : objNS.customer_id,
    					"subsidiary_id"   : objNS.subsidiary_id,
    					"clocation_id"    : objNS.clocation_id,
    					"location_id"     : objNS.location_id,
    					"addressee"       : objNS.addressee,
    					"ms_id"           : objNS.ms_id,
    					"equipment_id"    : objNS.equipment_id,
    					"service_id"      : objNS.service_id,
    					"vaultname"       : objNS.vaultname,
    					"item_id"         : objNS.item_id,
    					"class_id"        : objNS.class_id,
    					"quantity"        : objNS.quantity,
    					"rate"            : objNS.rate,
    					"usage"           : vaults[i].UsedDiskSpace,
    					"billing_period"  : billing_period
    				});
    			}
    	    }
    		
    		var invoices = [];
    		for ( var i = 0; i < usage.length; i++ ) {
    			if(usage[i].usage > usage[i].quantity){
    				var taxcode = -8;
    				if(usage[i].subsidiary_id == 6){
    					taxcode = 11;
    				}
    				var overage = parseFloat(round(usage[i].usage - usage[i].quantity, 4));
    				var amount =  parseFloat(round(usage[i].rate * overage, 4));
    				var memo = 'E-Vault Overage ' + overage + ' GB (' + occured + ')'
    				
    				invoices.push({
    					"customer_id"     : usage[i].customer_id,
    					"subsidiary_id"   : usage[i].subsidiary_id,
    					"clocation_id"    : usage[i].clocation_id,
    					"addressee"       : usage[i].addressee,
    					"trandate"        : usage[i].billing_period,
    					"startdate"       : usage[i].billing_period,
    					"usage_invoice_so": usage[i].so_id,
    					"items"           :[{
    						"service_id"      : usage[i].service_id,
    						"item_id"         : 762,
    						"class_id"        : usage[i].class_id,
    						"taxcode"         : taxcode,
    						"quantity"        : overage,
    						"rate"            : usage[i].rate,
    						"amount"          : amount,
    						"memo"            : memo
    					}]
    				});
    			}
    		}
    		
    		var abjALL = {
    				"invoices"   : invoices,
    				"usage"      : usage,
    				"netsuite"   : sos,
    				"vaults"     : vaults,
    				//"quantities" : quantities,
    				//"services"   : services
    		}
    		
    		var str = JSON.stringify(match);
			var file = nlapiCreateFile('backups_usage_' + billing_period + '.json', 'PLAINTEXT', str);
			file.setFolder(8778305);
			nlapiSubmitFile(file);
			
			for ( var i = 0; i < usage.length; i++ ) {
				
				var record = nlapiCreateRecord('customrecord_clgx_backup_usage');
				record.setFieldValue('custrecord_clgx_backup_ms', usage[i].ms_id);
				record.setFieldValue('custrecord_clgx_backup_billing_period', billing_period);
				record.setFieldValue('custrecord_clgx_backup_quota', usage[i].quantity);
				record.setFieldValue('custrecord_clgx_backup_usage', usage[i].usage);
				record.setFieldValue('custrecord_clgx_backup_overage', usage[i].rate);
				record.setFieldValue('custrecord_clgx_backup_vault', usage[i].equipment_id);
				var idRec = nlapiSubmitRecord(record, false,true);

	    		var usage = 10000 - parseInt(context.getRemainingUsage());
	            nlapiLogExecution('DEBUG','Results ', '| idRec = ' + idRec + ' | ' + i + ' / ' + usage.length + ' | usage = '+ usage  + '  |');
	            
			}
        	
        }
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
