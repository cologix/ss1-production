//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Managed.js
//	Script Name:	CLGX_SU_Managed
//	Script Id:		customscript_clgx_su_managed
//	Script Runs:	On Server
//	Script Type:	User Event
//	Deployments:	Managed Services
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		8/6/2018
//-------------------------------------------------------------------------------------------------


function beforeSubmit(type){
    try{
    	
    	var currentContext = nlapiGetContext();
		if (currentContext.getExecutionContext() == 'userinterface' && (type == 'create' || type == 'edit')) {
			
			var type = parseInt(nlapiGetFieldValue('custrecord_clgx_managed_services_type'));
			var name = nlapiGetFieldValue('custrecord_clgx_ms_hostname');
			
			var so = parseInt(nlapiGetFieldValue('custrecord_clgx_ms_service_order'));
			var so_status = null;
			if(so > 0){
				so_status = nlapiLookupField('salesorder', so, 'status');
			}
			
	    	var equipment = parseInt(nlapiGetFieldValue('custrecord_clgx_ms_equipment'));
	    	var vault = null;
			if(equipment > 0){
				vault = (nlapiLookupField('customrecord_cologix_equipment', equipment, 'custrecord_clgx_equipment_name')).toUpperCase();
				
				if(vault == "cvault4.nnj3.cologix.net".toUpperCase()) {
					vault = "207.99.60.100\\EVAULT_DB_V800";
				} else if(vault == "cvault5.nnj3.cologix.net".toUpperCase()) {
					vault = "207.99.60.101\\EVAULT_DB_V800";
				} else if(vault == "cvault6.nnj3.cologix.net".toUpperCase()) {
					vault = "207.99.60.103\\EVAULT_DB_V800";
				} else if(vault == "cvault7.nnj2.cologix.net".toUpperCase()) {
					vault = "64.21.33.133\\EVAULT_DB_V800";
				}
				
				//if(vault == 'cvault8.nnj3.cologix.net'){
				//	vault = 'tcp:cvault8.nnj3.cologix.net,49163';
				//}
			}
			
	    	if(so_status != 'pendingBilling' && so_status != 'pendingApproval' && so_status != "fullyBilled"){
                throw nlapiCreateError('99999', 'The status of the Service Order is not Pending Billing or Pending Approval.');
	    	}
	    	else if(type == 1){
	    		var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/dw/backup/get_shortname_from_vault.cfm?vault=' + vault + '&name=' + name);
				var count= JSON.parse( requestURL.body );
				if(count == 0){
	                throw nlapiCreateError('99999', 'The name ' + name + ' does not exist on ' + vault);
				}
				else if(count > 1){
	                throw nlapiCreateError('99999', 'The name ' + name + ' exist more tham once on ' + vault);
				}
				else{}
	    	}
		}
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
