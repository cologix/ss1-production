nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_Managed.js
//	Script Name:	CLGX_CR_Managed
//	Script Id:		customscript_clgx_cr_managed
//	Script Runs:	Server
//	Script Type:	User Event Script
//	Deployments:	Case
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		06/11/2018
//-------------------------------------------------------------------------------------------------


function fieldChanged(type, name, linenum){
    try {
//------------- Begin Section 1 -------------------------------------------------------------------

        if (name == 'custrecord_clgx_managed_services_type') {
        	
        	/*
            ========================
            Equip Func Type
            ========================
            Server	 	1
            Firewall	2	 
            Switch	 	3	 
            Router	 	4	 
            Vault	 	6
            =======================================================
            Managed Services Type   ->   Equip Func Type
            =======================================================
			Backup              1   ->   6     Vault
			Licensing           2   ->   ?     ?
			Load Balancer       3   ->   1     Server
			Router (switch)     4   ->   3     Switch
			Security            5   ->   2     Firewall
			Server              6   ->   1     Server
			Storage             7   ->   1     Server
			Virtual DC          8   ->   ?     ?
			Virtual Server      9   ->   ?     ?
			Router             10   ->   4     Router
			=======================================================
            */
            
        	nlapiSetFieldValue('custrecord_clgx_ms_equipment_function', '');
            var ms_equipment_function = null;
            var ms_services_type = parseInt(nlapiGetFieldValue('custrecord_clgx_managed_services_type'));
            if(ms_services_type){

                switch(ms_services_type) {
                    case 1: // case Backup
                        ms_equipment_function = 6; // then Vault
                        break;
                    case 2: // case Licensing
                        ms_equipment_function = ''; // then null
                        break;
                    case 3: // case Load Balancer
                        ms_equipment_function = 1; // then Server
                        break;
                    case 4: // case Router
                        ms_equipment_function = 3; // then Switch
                        break;
                    case 5: // case Security
                        ms_equipment_function = 2; // then Firewall
                        break;
                    case 6: // case Server
                        ms_equipment_function = 1; // then Server
                        break;
                    case 7: // case Storage
                        ms_equipment_function = 1; // then Server
                        break;
                    case 8: // case Virtual DC
                        ms_equipment_function = ''; // then null
                        break;
                    case 9: // case Virtual Server
                        ms_equipment_function = ''; // then null
                        break;
                    case 11: // case Router
                        ms_equipment_function = 4; // then Router
                        break;
                    default:
                        ms_equipment_function = '';
                }
            }
            nlapiSetFieldValue('custrecord_clgx_ms_equipment_function', ms_equipment_function);
        }
//------------- End Section 1 -------------------------------------------------------------------
    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
