nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_MS_Backups_Usage.js
//	Script Name:	CLGX_SS_MS_Backups_Usage
//	Script Id:		customscript_clgx_ss_ms_backups_usage
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		07/05/2018
//-------------------------------------------------------------------------------------------------

function scheduled__ms_backups_usage (){
    try{
    	
    	var context = nlapiGetContext();
        var environment = context.getEnvironment();
        
        if(environment == 'PRODUCTION'){
        	
        	// managed services from netsuite
    		var services = [];
    		var managed = [];
    		var search = nlapiSearchRecord('customrecord_clgx_managed_services', 'customsearch_clgx_ms_backup_usage');
    		for ( var i = 0; search != null && i < search.length; i++ ) {
    			var columns = search[i].getAllColumns();
    			var service_id = parseInt(search[i].getValue(columns[1]));
    			managed.push({
    					"ms_id"        : parseInt(search[i].getValue(columns[0])),
    					"service_id"   : service_id,
    					"vaultname"         : search[i].getValue(columns[2]),
    					"equipment_id" : parseInt(search[i].getValue(columns[3]))
    				});
    			services.push(service_id);
    	    }
    		
    		// quantities from netsuite SOs lines
    		var sos = [];
    		var columns = [];
    		var filters = [];
    		filters.push(new nlobjSearchFilter("custcol_clgx_so_col_service_id",null,"anyof",services));
    		var search = nlapiSearchRecord('transaction', 'customsearch_clgx_ms_backup_usage_serv', filters, columns);
    		for ( var i = 0; search != null && i < search.length; i++ ) {
    			var columns = search[i].getAllColumns();
    			var service_id = parseInt(search[i].getValue(columns[6]));
    			var objMS = _.find(managed, function(arr){ return (arr.service_id == service_id) ; });
    			if(objMS){
    				sos.push({
    					"so_id"         : parseInt(search[i].getValue(columns[0])),
    					"customer_id"   : parseInt(search[i].getValue(columns[1])),
    					"subsidiary_id" : parseInt(search[i].getValue(columns[2])),
    					"clocation_id"  : parseInt(search[i].getValue(columns[3])),
    					"location_id"   : parseInt(search[i].getValue(columns[4])),
    					"addressee"     : search[i].getValue(columns[5]),
    					"ms_id"         : objMS.ms_id,
    					"service_id"    : objMS.service_id,
    					"vaultname"     : objMS.vaultname,
    					"equipment_id"  : objMS.equipment_id,
    					"item_id"       : parseInt(search[i].getValue(columns[7])) || 0,
    					"class_id"      : parseInt(search[i].getValue(columns[8])) || 0,
    					"quantity"      : parseInt(search[i].getValue(columns[9])) || 0,
    					"rate"          : parseFloat(search[i].getValue(columns[10])) || 0
    				});
    			}
    	    }
    		
    		// request backups usages from vaults via clgx_cmd
    		var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/dw/backup/get_backup_overage.cfm');
    		var vaults= JSON.parse( requestURL.body );
    		
    		nlapiLogExecution('DEBUG','vaults', JSON.stringify(vaults, null, 4));
    		
    		var billing_period = moment().startOf('month').add('month', 1).format('MM/DD/YYYY');
    		var billing_period_str = moment().startOf('month').add('month', 1).format('MM_DD_YYYY');
    		var occured = moment().startOf('month').format('MMMM YYYY');
    		
    		var str = JSON.stringify(vaults);
    		var file = nlapiCreateFile('backups_usage_' + billing_period_str + '_vaults_raw.json', 'PLAINTEXT', str);
    		file.setFolder(8778305);
    		nlapiSubmitFile(file);

    		// usages netsuite with vaults data
    		var usages = [];
    		for ( var i = 0; i < vaults.length; i++ ) {
    			var objNS = _.find(sos, function(arr){ return (arr.vaultname == vaults[i].Customer && arr.equipment_id == vaults[i].Equipment) ; });
    			if(objNS){
    				usages.push({
    					"usage_id"        : null,
    					"invoice_id"      : null,
    					"ms_id"           : objNS.ms_id,
    					"so_id"           : objNS.so_id,
    					"customer_id"     : objNS.customer_id,
    					"vaultname"       : objNS.vaultname,
    					"subsidiary_id"   : objNS.subsidiary_id,
    					"clocation_id"    : objNS.clocation_id,
    					"location_id"     : objNS.location_id,
    					"addressee"       : objNS.addressee,
    					"equipment_id"    : objNS.equipment_id,
    					"service_id"      : objNS.service_id,
    					"vaultname"       : objNS.vaultname,
    					"item_id"         : objNS.item_id,
    					"class_id"        : objNS.class_id,
    					"quantity"        : objNS.quantity,
    					"rate"            : objNS.rate,
    					"usage"           : vaults[i].UsedDiskSpace,
    					"billing_period"  : billing_period
    				});
    			}
    	    }
    		
    		var invoices = [];
    		for ( var i = 0; i < usages.length; i++ ) {
    			if(usages[i].usage > usages[i].quantity){
    				var taxcode = -8;
    				if(usages[i].subsidiary_id == 6){
    					taxcode = 11;
    				}
    				var overage = parseFloat(round(usages[i].usage - usages[i].quantity, 4));
    				var amount =  parseFloat(round(usages[i].rate * overage, 4));
    				var description = usages[i].vaultname + ' - E-Vault Overage ' + overage + ' GB (' + occured + ')'
    				
    				invoices.push({
    					"invoice_id"      : null,
    					"ms_id"           : usages[i].ms_id,
    					"equipment_id"    : usages[i].equipment_id,
    					"customer_id"     : usages[i].customer_id,
    					"vaultname"       : usages[i].vaultname,
    					"subsidiary_id"   : usages[i].subsidiary_id,
    					"clocation_id"    : usages[i].clocation_id,
    					"location_id"     : usages[i].location_id,
    					"addressee"       : usages[i].addressee,
    					"trandate"        : usages[i].billing_period,
    					"startdate"       : usages[i].billing_period,
    					"usage_invoice_so": usages[i].so_id,
    					"items"           :[{
    						"item_id"         : 762,
    						"class_id"        : usages[i].class_id,
    						"taxcode"         : taxcode,
    						"quantity"        : overage,
    						"rate"            : usages[i].rate,
    						"amount"          : amount,
    						"description"     : description
    					}]
    				});
    			}
    		}

    		//var invoices = _.filter(invoices, function(arr){
    		//	return (arr.equipment_id == 1027);
    		//});
    		// create invoice records
    		var invoices_ids = [];
    		for ( var i = 0; i < invoices.length; i++ ) {
				
				var recInvoice = nlapiCreateRecord('invoice');
    			recInvoice.setFieldValue('entity', invoices[i].customer_id);
    			recInvoice.setFieldValue('subsidiary', invoices[i].subsidiary_id);
    			recInvoice.setFieldValue('custbody_clgx_consolidate_locations', invoices[i].clocation_id);
    			recInvoice.setFieldValue('location', invoices[i].location_id);
    			recInvoice.setFieldValue('billaddressee', invoices[i].addressee);
    			recInvoice.setFieldValue('trandate', invoices[i].trandate);
    			recInvoice.setFieldValue('startdate', invoices[i].startdate);
    			recInvoice.setFieldValue('custbody_cologix_legacy_so', invoices[i].usage_invoice_so);
    			
    			recInvoice.selectNewLineItem('item');
    			recInvoice.setCurrentLineItemValue('item','item', invoices[i].items[0].item_id);
    			recInvoice.setCurrentLineItemValue('item','class', invoices[i].items[0].class_id);
    			recInvoice.setCurrentLineItemValue('item','taxcode', invoices[i].items[0].taxcode);
    			recInvoice.setCurrentLineItemValue('item','location', invoices[i].location_id);
    			recInvoice.setCurrentLineItemValue('item','quantity', invoices[i].items[0].quantity);
    			recInvoice.setCurrentLineItemValue('item','rate', invoices[i].items[0].rate);
    			recInvoice.setCurrentLineItemValue('item','amount', invoices[i].items[0].amount);
    			recInvoice.setCurrentLineItemValue('item','description', invoices[i].items[0].description);
				recInvoice.commitLineItem('item');
				
				var idRec = nlapiSubmitRecord(recInvoice, false,true);
				
				invoices[i].invoice_id = parseInt(idRec);
				invoices_ids.push(parseInt(idRec));
				
				var usage_script = 10000 - parseInt(context.getRemainingUsage());
	            nlapiLogExecution('DEBUG','Invoices Records ', '| invoice_id = ' + idRec + ' | ' + i + ' / ' + invoices.length + ' | usage_script = '+ usage_script  + '  |');
			}
			
    		//var usages = _.filter(usages, function(arr){
    		//	return (arr.equipment_id == 1027);
    		//});
			// create usages records
    		var usages_ids = [];
    		for ( var i = 0; i < usages.length; i++ ) {
				
				var record = nlapiCreateRecord('customrecord_clgx_backup_usage');
				record.setFieldValue('custrecord_clgx_backup_ms', usages[i].ms_id);
				record.setFieldValue('custrecord_clgx_backup_billing_period', billing_period);
				record.setFieldValue('custrecord_clgx_backup_quota', usages[i].quantity);
				record.setFieldValue('custrecord_clgx_backup_usage', usages[i].usage);
				record.setFieldValue('custrecord_clgx_backup_overage', usages[i].rate);
				record.setFieldValue('custrecord_clgx_backup_vault', usages[i].equipment_id);
				var idRec = nlapiSubmitRecord(record, false,true);
				usages[i].usage_id = parseInt(idRec);
				usages_ids.push(parseInt(idRec));
				
				var obj = _.find(invoices, function(arr){ return (arr.ms_id == usages[i].ms_id) ; });
				if(obj){
					usages[i].invoice_id = parseInt(obj.invoice_id);
				}
	    		
	    		var usage_script = 10000 - parseInt(context.getRemainingUsage());
	            nlapiLogExecution('DEBUG','Results ', '| usage_id = ' + idRec + ' | ' + i + ' / ' + usages.length + ' | usage_script = '+ usage_script  + '  |');
			}
        	
    		var abjALL = {
    				"invoices"       : invoices,
    				"invoices_ids"   : invoices_ids,
    				"usages"         : usages,
    				"usages_ids"     : usages_ids,
    				"netsuite"       : sos,
    				"vaults"         : vaults
    		}
    		
    		var str = JSON.stringify(abjALL);
			var file = nlapiCreateFile('backups_usage_' + billing_period_str + '.json', 'PLAINTEXT', str);
			file.setFolder(8778305);
			nlapiSubmitFile(file);
			
        }
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function round(num,decimals){
    var sign = num >= 0 ? 1 : -1;
    return (Math.round((num*Math.pow(10,decimals))+(sign*0.001))/Math.pow(10,decimals)).toFixed(decimals);
}