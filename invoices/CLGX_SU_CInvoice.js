nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_CInvoice.js
//	Script Name:	CLGX_SU_CInvoice.js
//	Script Id:		customscript_clgx_su_cinvoice
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Invoice
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		02/19/2016
//-------------------------------------------------------------------------------------------------

function afterSubmit(type){
	try {
		
//------------- Begin Section 1 -------------------------------------------------------------------
// Created:	02/19/2016
// Details:	Ping NAC
//-------------------------------------------------------------------------------------------------
		
        var context = nlapiGetContext();
        var environment = context.getEnvironment();
        var userid = nlapiGetUser();
        var user = nlapiLookupField('employee', userid, 'entityid');
        var ciid = nlapiGetRecordId();
        
        var rec = nlapiLoadRecord('customrecord_clgx_consolidated_invoices', ciid);
        var outputid = rec.getFieldValue('custrecord_clgx_consol_inv_output');
        var location = rec.getFieldText('custrecord_clgx_consol_inv_clocation');

        /*
        if (environment == 'PRODUCTION' && outputid == 1 && location == 'NNJ') {
	         var row = nlapiLoadRecord('customrecord_clgx_consolidated_invoices', ciid);
	         flexapi('POST','/netsuite/update', {
		         'type': 'customrecord_clgx_consolidated_invoices',
                 'id': ciid,
                 'action': 'update',
                 'userid': userid,
                 'user': user,
                 'data': json_serialize(row)
	         });
	         nlapiLogExecution('DEBUG', 'flexapi request: ', ciid);
        }
		*/
//---------- End Section 1 ------------------------------------------------------------------------------------------------

	} 
	catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	}
}