nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Invoices_Queue_Show.js
//	Script Name:	CLGX_SL_Invoices_Queue_Show
//	Script Id:		customscript_clgx_sl_inv_queue_show
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/13/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=587&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_inv_queue_show (request, response){
	try {
		
		var arrMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"];
		
		var arrCInvoices = new Array();
		var searchCInvoices = nlapiLoadSearch('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_inv_ci_queue');
		var resultSet = searchCInvoices.runSearch();
		
		resultSet.forEachResult(function(searchResult) {
			var objCInvoice = new Object();

			objCInvoice["internalid"] = parseInt(searchResult.getValue('internalid',null,null));
			objCInvoice["invoice"] = searchResult.getValue('custrecord_clgx_consol_inv_batch',null,null);
			objCInvoice["output"] = searchResult.getText('custrecord_clgx_consol_inv_output',null,null)
			
			objCInvoice["customerid"] = parseInt(searchResult.getValue('custrecord_clgx_consol_inv_customer',null,null));
			objCInvoice["customer"] = searchResult.getText('custrecord_clgx_consol_inv_customer',null,null);
			objCInvoice["location"] = searchResult.getValue('custrecord_clgx_consol_inv_market',null,null);
			objCInvoice["currency"] = searchResult.getValue('custrecord_clgx_consol_inv_currency',null,null);
			
			objCInvoice["cyear"] = parseInt(searchResult.getValue('custrecord_clgx_consol_inv_year',null,null));
			var cmonth = parseInt(searchResult.getValue('custrecord_clgx_consol_inv_month',null,null));
			objCInvoice["cmonth"] = arrMonths[cmonth - 1];
			objCInvoice["dyear"] = parseInt(searchResult.getValue('custrecord_clgx_consol_inv_year_display',null,null));
			var dmonth = parseInt(searchResult.getValue('custrecord_clgx_consol_inv_month_display',null,null));
			objCInvoice["dmonth"] = arrMonths[dmonth - 1];
			
			objCInvoice["date"] = searchResult.getValue('custrecord_clgx_consol_inv_date',null,null);
			objCInvoice["user"] = searchResult.getText('custrecord_clgx_consol_inv_user',null,null);
			objCInvoice["queue"] = parseInt(searchResult.getValue('custrecord_clgx_consol_inv_queue',null,null)) || 0;
			
			
			var strInvoices = searchResult.getValue('custrecord_clgx_consol_inv_invoices',null,null);
			var arrInvoices = _.map(strInvoices.split(";"), Number);
			
			
			if(strInvoices == null || strInvoices == ''){
				objCInvoice["invoices"] = 0;
			}
			else{
				objCInvoice["invoices"] = arrInvoices.length;
			}
			
			arrCInvoices.push(objCInvoice);
			return true;
		});

		response.write(JSON.stringify(arrCInvoices));
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

