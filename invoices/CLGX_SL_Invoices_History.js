nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Invoices_History.js
//	Script Name:	CLGX_SL_Invoices_History
//	Script Id:		customscript_clgx_sl_inv_history
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/12/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=584&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_inv_history (request, response){
    try {
    	
    	var locationid = request.getParameter('locationid');
        var location = request.getParameter('location');
        var customerid = request.getParameter('customerid');
        var customer = request.getParameter('customer');
        var invoiceid = request.getParameter('invoiceid');
        var invoice = request.getParameter('invoice');
        var cyear = request.getParameter('cyear');
        var cmonth = request.getParameter('cmonth');
        var strmonth = request.getParameter('strmonth');
        
    	var start = moment(cmonth + '/1/' + cyear).format('M/D/YYYY');
    	var end = moment(cmonth + '/1/' + cyear).endOf('month').format('M/D/YYYY');
    	
    	if(locationid > 0){
        	
        	var objFile = nlapiLoadFile(3184098);
            var html = objFile.getValue();
            
            var objParams = new Object();
    		objParams["locationid"] = locationid;
    		objParams["location"] = location;
    		objParams["customerid"] = customerid;
    		objParams["customer"] = customer;
    		objParams["invoiceid"] = invoiceid;
    		objParams["invoice"] = invoice;
    		objParams["start"] = start;
    		objParams["end"] = end;
    		objParams["cmonth"] = cmonth;
    		objParams["cyear"] = cyear;
    		objParams["strmonth"] = strmonth;
    		
    		html = html.replace(new RegExp('{params}','g'), JSON.stringify(objParams));
    		html = html.replace(new RegExp('{history}','g'), JSON.stringify(get_history (customerid,cyear,cmonth)));
    		
        }
        else{
            var html = 'Please select a customer or an invoice from the left panel.';
        }
    	
    	var usageConsumtion = 1000 - parseInt(nlapiGetContext().getRemainingUsage());
    	nlapiLogExecution('DEBUG','usageConsumtion', '| Customer - ' + customer + '| CustomerID - ' + customerid + ' | All Usage - ' + usageConsumtion) + ' |'; 
    	
        response.write( html );
        
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function get_history (customerid,cyear,cmonth){
	
    var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_customer",null,"anyof",customerid));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_year",null,"is",cyear.toString()));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_month",null,"anyof",cmonth));
	//arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_market",null,"anyof",location));
	var searchCInvoices = nlapiSearchRecord('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_inv_cinvoices', arrFilters, arrColumns);
	var arrCInvoices = new Array();
	for ( var i = 0; searchCInvoices != null && i < searchCInvoices.length; i++ ) {
		var objCInvoice = new Object();
		objCInvoice["node"] = searchCInvoices[i].getValue('custrecord_clgx_consol_inv_batch',null,null);
		objCInvoice["nodetype"] = 'cinvoice';
		objCInvoice["customerid"] = customerid;
		objCInvoice["location"] = searchCInvoices[i].getValue('custrecord_clgx_consol_inv_market',null,null);
		objCInvoice["currency"] = searchCInvoices[i].getValue('custrecord_clgx_consol_inv_currency',null,null);
		objCInvoice["cinvoiceid"] = parseInt(searchCInvoices[i].getValue('internalid',null,null));
		objCInvoice["cinvoice"] = searchCInvoices[i].getValue('custrecord_clgx_consol_inv_batch',null,null);
		objCInvoice["invoiceid"] = 0;
		objCInvoice["contactid"] = 0;
		
		var outputid =  parseInt(searchCInvoices[i].getValue('custrecord_clgx_consol_inv_output',null,null));
		objCInvoice["outputid"] = outputid;
		objCInvoice["output"] = searchCInvoices[i].getText('custrecord_clgx_consol_inv_output',null,null);
		objCInvoice["cyear"] = cyear;
		objCInvoice["cmonth"] = cmonth;
		objCInvoice["dyear"] = parseInt(searchCInvoices[i].getValue('custrecord_clgx_consol_inv_year_display',null,null));
		objCInvoice["dmonth"] = parseInt(searchCInvoices[i].getValue('custrecord_clgx_consol_inv_month_display',null,null));
		
		var stotal = parseFloat(searchCInvoices[i].getValue('custrecord_clgx_consol_inv_subtotal',null,null));
		var total = parseFloat(searchCInvoices[i].getValue('custrecord_clgx_consol_inv_total',null,null));
		objCInvoice["stotal"] = stotal;
		objCInvoice["total"] = total;
		objCInvoice["taxes"] = parseFloat((total - stotal).toFixed(2));
		objCInvoice["tax1"] = parseFloat(searchCInvoices[i].getValue('custrecord_clgx_consol_inv_tax_total',null,null));
		objCInvoice["tax2"] = parseFloat(searchCInvoices[i].getValue('custrecord_clgx_consol_inv_tax2_total',null,null));
		
		var fileid = searchCInvoices[i].getValue('custrecord_clgx_consol_inv_pdf_file_id', null, null);
		try {
        	var objFile = nlapiLoadFile(fileid);
            objCInvoice["fileid"] = parseInt(fileid);
            objCInvoice["filename"] = objFile.getName();
            objCInvoice["fileurl"] = objFile.getURL();
            
        }
        catch (e) {
            var str = String(e);
            if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
                var fileid = 0;
                var filename = 'unknown';
                var fileurl = '';
	            objCInvoice["fileid"] = parseInt(fileid);
	            objCInvoice["filename"] = 'unknown';
	            objCInvoice["fileurl"] = '';
            }
        }
        
        var strInvoices = searchCInvoices[i].getValue('custrecord_clgx_consol_inv_invoices',null,null);
		var arrInv = _.map(strInvoices.split(";"), Number);
		
		var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
        arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP').setSort(false));
        arrColumns.push(new nlobjSearchColumn('createdfrom',null,'GROUP'));
        var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrInv));
		var searchInvoices = nlapiSearchRecord('invoice', null, arrFilters, arrColumns);
		
		var arrInvoices = new Array();
		for ( var j = 0; searchInvoices != null && j < searchInvoices.length; j++ ) {
			var objInvoice = new Object();
			objInvoice["node"] = searchInvoices[j].getValue('tranid',null,'GROUP');
			objInvoice["nodetype"] = 'invoice';
			objInvoice["invoiceid"] = parseInt(searchInvoices[j].getValue('internalid',null,'GROUP'));
			objInvoice["invoice"] = searchInvoices[j].getValue('tranid',null,'GROUP');
			objInvoice["contactid"] = 0;
			objInvoice["soid"] = parseInt(searchInvoices[j].getValue('createdfrom',null,'GROUP'));
			objInvoice["so"] = (searchInvoices[j].getText('createdfrom',null,'GROUP')).replace(/\Service Order #/g,"");
			if(outputid == 1){
				objInvoice["faicon"] = 'fa fa-list-ul Silver1';
			}
			else{
				objInvoice["faicon"] = 'fa fa-list-ul Green1';
			}
			objInvoice["leaf"] = true;
			arrInvoices.push(objInvoice);
		}
		
		var strContacts = searchCInvoices[i].getValue('custrecord_clgx_consol_inv_emailed_to',null,null);
		var arrCont = _.map(strContacts.split(";"), Number);
		
		var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        arrColumns.push(new nlobjSearchColumn('entityid',null,null));
        var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrCont));
		var searchContacts = nlapiSearchRecord('contact', null, arrFilters, arrColumns);
		
		var arrContacts = new Array();
		for ( var j = 0; searchContacts != null && j < searchContacts.length; j++ ) {
			var objContact = new Object();
			objContact["node"] = searchContacts[j].getValue('entityid',null,null);
			objContact["nodetype"] = 'contact';
			objContact["contactid"] = parseInt(searchContacts[j].getValue('internalid',null,null));
			objContact["contact"] = searchContacts[j].getValue('entityid',null,null);
			objContact["invoiceid"] = 0;
			objContact["faicon"] = 'fa fa-user Silver1';
			objContact["leaf"] = true;
			arrContacts.push(objContact);
		}
		
		
		var arrChildren = new Array();
		
		var objChild = new Object();
		objChild["node"] = 'Included invoices';
		objChild["nodetype"] = 'invoices';
		objChild["children"] = arrInvoices;
		objChild["expanded"] = true;
		if(outputid == 1){
			objChild["faicon"] = 'fa fa-list-ul Green1';
		}
		else{
			objChild["faicon"] = 'fa fa-list-ul SteelBlue2';
		}
		objChild["leaf"] = false;
		arrChildren.push(objChild);
		
		var objChild = new Object();
		objChild["node"] = 'Emailed to';
		objChild["nodetype"] = 'contacts';
		objChild["children"] = arrContacts;
		objChild["expanded"] = true;
		objChild["faicon"] = 'fa fa-at SteelBlue1';
		objChild["leaf"] = false;
		arrChildren.push(objChild);
		
		objCInvoice["children"] = arrChildren;
		objCInvoice["expanded"] = true;
		objCInvoice["faicon"] = 'fa fa-list-ul SteelBlue2';
		
		if(outputid == 1){
			objCInvoice["faicon"] = 'fa fa-list-ul Green1';
		}
		else{
			objCInvoice["faicon"] = 'fa fa-list-ul SteelBlue2';
		}
		objCInvoice["leaf"] = false;
		
		arrCInvoices.push(objCInvoice);
	}
	
	var objTree = new Object();
	objTree["text"] = '.';
	objTree["children"] = arrCInvoices;
	
	return objTree;
}

