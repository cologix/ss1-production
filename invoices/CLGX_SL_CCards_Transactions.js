nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CCards_Transactions.js
//	Script Name:	CLGX_SL_CCards_Transactions
//	Script Id:		customscript_clgx_sl_ccards_transactions
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/24/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=579&deploy=1
//-------------------------------------------------------------------------------------------------
var TXN_TYPE_SALE   = 1;
var TXN_TYPE_REFUND = 2;
var TXN_TYPE_VOID   = 3;

var SUBMIT_SOURCE_AUTOMATED = 1;
var SUBMIT_SOURCE_MANUAL    = 2;
var SUBMIT_SOURCE_WEB       = 3;

var STATUS_PENDING_SUBMIT 				= 1;
var STATUS_PENDING_APPROVE 				= 2;
var STATUS_PENDING_DECLINE 				= 3;
var STATUS_SUBMITTED 					= 4;
var STATUS_SUCCESS 						= 5;
var STATUS_VOIDED 						= 6;
var STATUS_ERROR_TOKEN 					= 7;
var STATUS_ERROR_NOCARD 				= 8;
var STATUS_ERROR_BALANCE_ZERO 			= 9;
var STATUS_ERROR_CC_NOT_ENABLED			= 10;
var STATUS_ERROR_UNABLE_TO_LOAD_COMPANY = 11;
var STATUS_ERROR_API_ERROR				= 12;
function suitelet_ccards_transactions (request, response){
    try {

        var id = request.getParameter('id');
        var refundAuth=request.getParameter('refundAuth')||0;
        var arrtr=request.getParameter('arrtr')||'';
        var tranid=request.getParameter('tranid')||'';
        var total=request.getParameter('total')||0;
        var rerun=request.getParameter('rerun')||0;
        var proc=request.getParameter('proc')||0;

        nlapiLogExecution('DEBUG','1', id);
        nlapiLogExecution('DEBUG','refund', refundAuth);
        nlapiLogExecution('DEBUG','3', arrtr);
        nlapiLogExecution('DEBUG','4', tranid);
        nlapiLogExecution('DEBUG','5', total);
        nlapiLogExecution('DEBUG','6', rerun);
        nlapiLogExecution('DEBUG','7', proc);




        if(id > 0){

            var objFile = nlapiLoadFile(4210180);

            var html = objFile.getValue();
            if(refundAuth>0)
            {
                if(arrtr!='')
                {
                    var arrPay=arrtr.split(';');
                    var payIds=new Array();
                    var amounts=new Array();
                    for (var i = 0;arrPay != null && i < arrPay.length; i++) {
                        var splitPay=arrPay[i].split('=');
                        payIds.push(splitPay[0]);
                        amounts.push(splitPay[1]);
                    }

                }
                // nlapiLogExecution('DEBUG','arrtr', arrtr[0]);
                // nlapiSendEmail(206211, 206211,'Test Refund',amounts[0],null,null,null,null,true);//Send email to Cat

                html = html.replace(new RegExp('{respRefund}','g'), refundCharge(id,total,tranid,payIds,amounts));
            }
            else   if(rerun>0){

                html = html.replace(new RegExp('{respRefund}','g'), reRun(tranid));

            }

            else{
                html = html.replace(new RegExp('{respRefund}','g'), '');
            }

            html = html.replace(new RegExp('{transactions}','g'), get_transactions(id));

        }
        else{
            if(proc>0){
                var objFile = nlapiLoadFile(5698160);

                var html = objFile.getValue();
                html = html.replace(new RegExp('{transactions}','g'), getCustomers());
            }
            else {
                var html = 'Please select a customer from the left panel.';
            }
        }
        response.write( html );

    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function get_transactions(id){

    var columns = new Array();
    columns.push(new nlobjSearchColumn('internalid',null,null));
    columns.push(new nlobjSearchColumn('custrecord_clgx_ccards_refunded_amt',null,null));
    columns.push(new nlobjSearchColumn('custrecord_clgx_ccards_amount',null,null));
    columns.push(new nlobjSearchColumn('custrecord_clgx_ccards_payment_id',null,null));
    columns.push(new nlobjSearchColumn('custrecord_clgx_ccards_transactionid',null,null));
    columns.push(new nlobjSearchColumn('custrecord_clgx_ccards_customer',null,null));
    columns.push(new nlobjSearchColumn('custrecord_clgx_ccards_response_code',null,null));
    columns.push(new nlobjSearchColumn('custrecord_clgx_ccards_response_text',null,null));
    columns.push(new nlobjSearchColumn('custrecord_clgx_ccards_txn_type',null,null));
    columns.push(new nlobjSearchColumn('custrecord_clgx_ccards_status',null,null));
    columns.push(new nlobjSearchColumn('custrecord_clgx_ccards_amountcharged',null,null));
    columns.push(new nlobjSearchColumn('created',null,null));
    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_clgx_ccards_customer",null,"anyof",id));
    var search = nlapiSearchRecord('customrecord_clgx_ccards_processing', null, filters, columns);
    var arr = new Array();
    for ( var i = 0; search != null && i < search.length; i++ ) {
        var pay=search[i].getText('custrecord_clgx_ccards_payment_id',null,null);
        var payArr=pay.split('#');

        var obj = {
            "id": parseInt(search[i].getValue('internalid',null,null)),
            "amountref": search[i].getValue('custrecord_clgx_ccards_refunded_amt',null,null),
            "customerid": search[i].getValue('custrecord_clgx_ccards_customer',null,null),
            "amount": search[i].getValue('custrecord_clgx_ccards_amount',null,null),
            "amountcharged": search[i].getValue('custrecord_clgx_ccards_amountcharged',null,null),
            "paymentid": search[i].getValue('custrecord_clgx_ccards_payment_id',null,null),
            "payment": payArr[1],
            "tranid": search[i].getValue('custrecord_clgx_ccards_transactionid',null,null),
            "code": search[i].getValue('custrecord_clgx_ccards_response_code',null,null),
            "response": search[i].getValue('custrecord_clgx_ccards_response_text',null,null),
            "type": search[i].getText('custrecord_clgx_ccards_txn_type',null,null),
            "status": search[i].getText('custrecord_clgx_ccards_status',null,null),
            "created": search[i].getValue('created',null,null)
        };
        arr.push(obj);
    }
    return JSON.stringify(arr);
}

function refundCharge(id,total,tranid,payIds,amounts) {
    nlapiLogExecution('DEBUG', 'UserEvent_RefundCharge [' + tranid + ']');
    try {
        // You can never refund greater then Unapplied
        // On return, set paymentitem.amount = paymentitem.amount
        var context = nlapiGetContext();
        var environment = context.getEnvironment();
        if (environment == 'PRODUCTION') {
            var recQueue = nlapiLoadRecord('customrecord_clgx_ccards_processing', tranid);
            var customer_id = recQueue.getFieldValue('custrecord_clgx_ccards_customer');
            //  var recCompany = nlapiLoadRecord('customer', customer_id);
            var status = recQueue.getFieldValue('custrecord_clgx_ccards_status');
            var type = recQueue.getFieldValue('custrecord_clgx_ccards_txn_type');
            var merchant_id = recQueue.getFieldValue('custrecord_clgx_ccards_merchant_id');
            var ccNumber = recQueue.getFieldValue('custrecord_clgx_ccards_accountnumber');
            var tranID = recQueue.getFieldValue('custrecord_clgx_ccards_transactionid');
            var refAmount = recQueue.getFieldValue('custrecord_clgx_ccards_refunded_amt') || 0;
            var refIDS = recQueue.getFieldValue('custrecordclgx_ccards_refundids') || '';

            var ccNbr = ccNumber.substr(ccNumber.length - 4);
            var recordAuth = nlapiLoadRecord('customrecord_clgx_authorize_net', merchant_id);
            var apiID = recordAuth.getFieldValue('custrecord_clgx_auth_net_api_id');
            var apiKEY = recordAuth.getFieldValue('custrecord_clgx_auth_net_transaction_key');


            //Send the transaction to Authorize;
            var data = {
                "createTransactionRequest": {
                    "merchantAuthentication": {
                        "name": apiID,
                        "transactionKey": apiKEY
                    },
                    "refId": "",
                    "transactionRequest": {
                        "transactionType": "refundTransaction",
                        "amount": total,
                        "payment": {
                            "creditCard": {
                                "cardNumber": ccNbr,
                                "expirationDate": "XXXX"
                            }
                        },
                        "refTransId": tranID
                    }
                }
            };
            var req = nlapiRequestURL('https://api.authorize.net/xml/v1/request.api', JSON.stringify(data), {'Content-type': 'application/json'}, null, 'POST');
            var str = req.body;
            //   nlapiSendEmail(206211, 206211, 'str', str, null, null, null, null,true);
            var resp = JSON.parse(str.slice(1));
            var resp_code = resp.messages.resultCode;
            var message = resp.messages.message[0];
            var messagesStatus = resp.transactionResponse.responseCode;
            // 1 = Approved
            // 2 = Declined
            // 3 = Error
            // 4 = Held for Review
            //record.setFieldValue('custrecord_clgx_ccards_status', 0);
            recQueue.setFieldValue('custrecord_clgx_ccards_lstrres', str);
            recQueue.setFieldValue('custrecord_clgx_ccards_txn_type', TXN_TYPE_REFUND);
            recQueue.setFieldValue('custrecord_clgx_ccards_submit_source', SUBMIT_SOURCE_AUTOMATED);
            recQueue.setFieldValue('custrecord_clgx_ccards_response_code', resp_code);
            recQueue.setFieldValue('custrecord_clgx_ccards_response_text', message.text);
            // 1 = Approved
            // 2 = Declined
            // 3 = Error
            // 4 = Held for Review
            var returnMessage = '';
            if (messagesStatus == 1) { //Success
                returnMessage = 'Success. The refund has been approved.';
                recQueue.setFieldValue('custrecord_clgx_ccards_status', 5);
                recQueue.setFieldValue('custrecord_clgx_ccards_refunded_amt', (parseFloat(total) + parseFloat(refAmount)).toFixed(2));

            }
            else if (messagesStatus == 2) {
                returnMessage = 'Error. The refund has been declined.';
                //  recQueue.setFieldValue('custrecord_clgx_ccards_status', 3);
            }
            else if (messagesStatus == 3) {
                returnMessage = 'Error. The refund has been declined.';
                //    recQueue.setFieldValue('custrecord_clgx_ccards_status', 7);
            }
            else if (messagesStatus == 4) {
                returnMessage = 'Pending Approved. The refund is in pending.';
                recQueue.setFieldValue('custrecord_clgx_ccards_status', 2);
            }
            if (messagesStatus == 1) { //Success
                recQueue.setFieldValue('custrecord_clgx_ccards_reftransid', resp.transactionResponse.refTransID);
                recQueue.setFieldValue('custrecord_clgx_ccards_avs', resp.transactionResponse.avsResultCode);
                recQueue.setFieldValue('custrecord_clgx_ccards_ccv', resp.transactionResponse.cvvResultCode);
                recQueue.setFieldValue('custrecord_clgx_ccards_cavv', resp.transactionResponse.cavvResultCode);
                var stringRefIDs = refIDS + resp.transactionResponse.transId + ';';
                recQueue.setFieldValue('custrecordclgx_ccards_refundids', stringRefIDs);
                recQueue.setFieldValue('custrecord_clgx_ccards_accountnumber', resp.transactionResponse.accountNumber);
                recQueue.setFieldValue('custrecord_clgx_ccards_accounttype', resp.transactionResponse.accountType);

                nlapiSubmitRecord(recQueue, false, true);
                var arrC = new Array();
                var arrF = new Array();
                arrF.push(new nlobjSearchFilter("name", null, "anyof", customer_id));

                var searchLocations = nlapiSearchRecord('transaction', 'customsearch_clgx_ss_ccards_in', arrF, arrC);

                var searchLoc = searchLocations[0];
                var columnsC = searchLoc.getAllColumns();
                var location = searchLoc.getValue(columnsC[0]);


                var ref = nlapiCreateRecord('customerrefund', {entity: customer_id});
                ref.setFieldValue('location', location);
                ref.setFieldValue('class', '1');
                ref.setFieldValue('aracct', 122);
                ref.setFieldValue('account', 116);
                ref.setFieldValue('department', 6);
                ref.setFieldValue('custbody_clgx_ccards_reftran', tranid);
                for (var j = 0; payIds != null && j < payIds.length; j++) {
                    for (var i = ref.getLineItemCount('apply'); i > 0; i--) {
                        if (payIds[j] != '' && payIds[j] == ref.getLineItemValue('apply', 'doc', i)) {
                            ref.setLineItemValue('apply', 'apply', i, 'T');
                            ref.setLineItemValue('apply', 'amount', i, amounts[j]);
                        }
                    }
                }
                var id = nlapiSubmitRecord(ref, false, true);
                var balance = clgx_update_balances(customer_id);

            }


            return returnMessage;
        }

    } catch (error) { // Start Catch Errors Section
        if (error.getDetails != undefined) {
            nlapiLogExecution('ERROR', 'Process Error UserEvent_RefundCharge', error.getCode() + ': ' + error.getDetails() );
            alert(error.toString());
            throw error;
        } else {
            nlapiLogExecution('ERROR', 'Unexpected Error UserEvent_RefundCharge', error.toString());
            alert(error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function reRun(tranid) {
    nlapiLogExecution('DEBUG', 'UserEvent_Rerun [' + tranid + ']');
    try {
        // You can never refund greater then Unapplied
        // On return, set paymentitem.amount = paymentitem.amount
        var context = nlapiGetContext();
        var environment = context.getEnvironment();
        if (environment == 'PRODUCTION') {
            var recQueue = nlapiLoadRecord('customrecord_clgx_ccards_processing', tranid);
            var customer_id = recQueue.getFieldValue('custrecord_clgx_ccards_customer');
            var merchant_id = recQueue.getFieldValue('custrecord_clgx_ccards_merchant_id');
            var amount = recQueue.getFieldValue('custrecord_clgx_ccards_amount') || 0;
            var refIDS = recQueue.getFieldValue('custrecordclgx_ccards_refundids') || '';
            var recordAuth = nlapiLoadRecord('customrecord_clgx_authorize_net', merchant_id);
            var apiID = recordAuth.getFieldValue('custrecord_clgx_auth_net_api_id');
            var apiKEY = recordAuth.getFieldValue('custrecord_clgx_auth_net_transaction_key');
            var customerProfile = nlapiLookupField('customer', customer_id, 'custentity_clgx_customer_authorize_id') || 0;
            var customer = nlapiLookupField('customer', customer_id, 'entityid') || '';
            var subsidiary = nlapiLookupField('customer', customer_id, 'subsidiary') || '';
            //  nlapiSubmitField('customer', customer_id, ['custentity_clgx_cc_paused'], 'F');
            if (merchant_id == 2) {
                var currency = 1;
            }
            else if (merchant_id == 4) {
                var currency = 3;
            }
            var arrC = new Array();
            var arrF = new Array();
            arrF.push(new nlobjSearchFilter("custrecord_token_customer", null, "anyof", customer_id));

            var defaultCard = nlapiSearchRecord('customrecord_clgx_credit_cards', 'customsearch_clgx_ss_ccards_fortr', arrF, arrC);
            if (defaultCard == null) {
                //send an email to ...
                /*  var recordTransaction = nlapiCreateRecord('customrecord_clgx_ccards_processing');
                 recordTransaction.setFieldValue('custrecord_clgx_ccards_status', 7);
                 recordTransaction.setFieldValue('custrecord_clgx_ccards_response_text', 'Error. No Default Card');
                 recordTransaction.setFieldValue('name', 'Automatic Credit Card Charge: '+customerArr.customer);
                 //record.setFieldValue('custrecord_clgx_ccards_status', 0);
                 recordTransaction.setFieldValue('custrecord_clgx_ccards_txn_type', TXN_TYPE_SALE);
                 recordTransaction.setFieldValue('custrecord_clgx_ccards_submit_source', SUBMIT_SOURCE_AUTOMATED);
                 recordTransaction.setFieldValue('custrecord_clgx_ccards_submit_date', sDate);
                 recordTransaction.setFieldValue('custrecord_clgx_ccards_merchant_id', merchant_id);
                 recordTransaction.setFieldValue('custrecord_clgx_ccards_amount',customerArr.tocharge);
                 var id = nlapiSubmitRecord(recordTransaction, false, true);
                 return;*/
            }
            else {
                if (amount > 0) {
                    var searchCard = defaultCard[0];
                    var columnsC = searchCard.getAllColumns();
                    var paymentPID = searchCard.getValue(columnsC[0]);

                    //Send the transaction to Authorize;
                    var data = {
                        "createTransactionRequest": {
                            "merchantAuthentication": {
                                "name": apiID,
                                "transactionKey": apiKEY
                            },
                            "refId": "",
                            "transactionRequest": {
                                "transactionType": "authCaptureTransaction",
                                "amount": amount,
                                "profile": {
                                    "customerProfileId": customerProfile,
                                    "paymentProfile": {"paymentProfileId": paymentPID}
                                }
                            }
                        }
                    };
                    var req = nlapiRequestURL('https://api.authorize.net/xml/v1/request.api', JSON.stringify(data), {'Content-type': 'application/json'}, null, 'POST');
                    var str = req.body;
                    //   nlapiSendEmail(206211, 206211, 'str', str, null, null, null, null,true);
                    var resp = JSON.parse(str.slice(1));
                    var resp_code = resp.messages.resultCode;
                    var message = resp.messages.message[0];
                    var messagesStatus = resp.transactionResponse.messages;
                    if (messagesStatus === undefined && (resp.transactionResponse.responseCode == 2 || resp.transactionResponse.responseCode == 3)) {
                        var errorObj = resp.transactionResponse.errors[0];
                        var errorCode = errorObj.errorcode;
                        var errorText = errorObj.errorText;
                        var mess_text = errorText;
                    }
                    else {
                        var mess_text = message.text;
                    }
                    var sDate = moment().format('MM/DD/YYYY');
                    recQueue.setFieldValue('custrecord_clgx_ccards_submit_date', sDate);
                    recQueue.setFieldValue('custrecord_clgx_ccards_lstrres', str);
                    recQueue.setFieldValue('custrecord_clgx_ccards_response_code', resp_code);
                    recQueue.setFieldValue('custrecord_clgx_ccards_response_text', mess_text);
                    // 1 = Approved
                    // 2 = Declined
                    // 3 = Error
                    // 4 = Held for Review

                    var body = '';
                    body = body + 'Customer ID: ' + customer_id + '\n';
                    body = body + 'Company Name: ' + customer + '\n';
                    body = body + 'Transaction Type: ' + TXN_TYPE_SALE + '\n';
                    body = body + 'Transaction Amount: ' + amount + '\n';
                    body = body + 'Transaction Date: ' + sDate + '\n';
                    body = body + 'Result: Error\n';
                    body = body + 'Response: ' + mess_text + '\n';
                    body = body + 'Transaction ID: ' + resp.transactionResponse.transId + '\n';
                    var subject = 'Credit Card Decline Detail - ' + customer
                    var returnMessage = '';


                    if (messagesStatus !== undefined && messagesStatus[0].code == 1) { //Success
                        returnMessage = 'Success. The transaction has been approved.';
                        recQueue.setFieldValue('custrecord_clgx_ccards_status', 5);
                        if (resp.transactionResponse["splitTenderPayments"] !== undefined) {
                            var splitTenderPayments = resp.transactionResponse["splitTenderPayments"];
                            recQueue.setFieldValue('custrecord_clgx_ccards_amountcharged', splitTenderPayments.splitTenderPayment.approvedAmount);
                        }
                        else {
                            recQueue.setFieldValue('custrecord_clgx_ccards_amountcharged', amount);

                        }
                        recQueue.setFieldValue('custrecord_clgx_ccards_authorization', resp.transactionResponse.authCode);

                    }
                    else if (resp.transactionResponse.responseCode == 2) {
                        recQueue.setFieldValue('custrecord_clgx_ccards_status', 3);
                        var record = nlapiCreateRecord('supportcase');
                        record.setFieldValue('company', customer_id);
                        record.setFieldValue('title', subject);
                        record.setFieldValue('category', 11);
                        record.setFieldValue('custevent_cologix_sub_case_type', 54);
                        record.setFieldValue('incomingmessage', body);
                        record.setFieldValue('priority', 2);
                        record.setFieldValue('origin', 1);
                        var idCase = nlapiSubmitRecord(record, false, true);
                        recQueue.setFieldValue('custrecord_clgx_ccards_case_id', idCase);
                        recQueue.setFieldValue('custrecord_clgx_ccards_amountcharged', 0);
                        returnMessage = 'Error. The transaction has been declined.';
                        nlapiSubmitField('customer', customer_id, ['custentity_clgx_cc_paused'], 'T');


                    }
                    else if (resp.transactionResponse.responseCode == 3 || resp_code == 'Error') {
                        customer_id.setFieldValue('custrecord_clgx_ccards_status', 7);
                        var record = nlapiCreateRecord('supportcase');
                        record.setFieldValue('company', customer_id);
                        record.setFieldValue('title', subject);
                        record.setFieldValue('category', 11);
                        record.setFieldValue('custevent_cologix_sub_case_type', 54);
                        record.setFieldValue('incomingmessage', body);
                        record.setFieldValue('priority', 2);
                        record.setFieldValue('origin', 1);
                        var idCase = nlapiSubmitRecord(record, false, true);
                        recQueue.setFieldValue('custrecord_clgx_ccards_case_id', idCase);
                        returnMessage = 'Error. The transaction has been declined.';
                        recQueue.setFieldValue('custrecord_clgx_ccards_amountcharged', 0);

                        nlapiSubmitField('customer', customer_id, ['custentity_clgx_cc_paused'], 'T');

                    }
                    else if (messagesStatus[0].code == 4) {
                        returnMessage = 'Pending Approved. The transaction is in pending.';
                        recQueue.setFieldValue('custrecord_clgx_ccards_status', 2);
                    }
                    recQueue.setFieldValue('custrecord_clgx_ccards_transactionid', resp.transactionResponse.transId);
                    recQueue.setFieldValue('custrecord_clgx_ccards_avs', resp.transactionResponse.avsResultCode);
                    recQueue.setFieldValue('custrecord_clgx_ccards_ccv', resp.transactionResponse.cvvResultCode);
                    recQueue.setFieldValue('custrecord_clgx_ccards_cavv', resp.transactionResponse.cavvResultCode);
                    recQueue.setFieldValue('custrecord_clgx_ccards_reftransid', resp.transactionResponse.refTransID);
                    recQueue.setFieldValue('custrecord_clgx_ccards_accountnumber', resp.transactionResponse.accountNumber);
                    recQueue.setFieldValue('custrecord_clgx_ccards_accounttype', resp.transactionResponse.accountType);
                    if (resp.transactionResponse["splitTenderPayments"] !== undefined) {
                        var splitTenderPayments = resp.transactionResponse["splitTenderPayments"];
                        recQueue.setFieldValue('custrecord_clgx_ccards_splittenderpaymen', 'Yes');
                        recQueue.setFieldValue('custrecord_clgx_ccards_requestedamount', splitTenderPayments.splitTenderPayment.requestedAmount);
                        recQueue.setFieldValue('custrecord_clgx_ccards_approvedamount', splitTenderPayments.splitTenderPayment.approvedAmount);
                        recQueue.setFieldValue('custrecord_clgx_ccards_balanceoncard', splitTenderPayments.splitTenderPayment.balanceOnCard);
                        var amount = parseFloat(splitTenderPayments.splitTenderPayment.approvedAmount).toFixed(2);
                    }
                    else {
                        recQueue.setFieldValue('custrecord_clgx_ccards_splittenderpaymen', 'No');
                    }

                    var id = nlapiSubmitRecord(recQueue, false, true);
                    if (messagesStatus !== undefined && messagesStatus[0].code == 1) {
                        var typeCard = resp.transactionResponse.accountType.charAt(0);
                        nlapiSubmitField('customrecord_clgx_ccards_processing', id, ['custrecord_clgx_ccards_payment_id'], createPayment(currency, amount, resp.transactionResponse.transId, subsidiary, id, customer_id, typeCard));

                    }

                }
                else {
                    var returnMessage = 'Error. You are unable to charge $0.'
                }
            }


            return returnMessage;
        }

    } catch (error) { // Start Catch Errors Section
        if (error.getDetails != undefined) {
            nlapiLogExecution('ERROR', 'Process Error UserEvent_RefundCharge', error.getCode() + ': ' + error.getDetails() );
            alert(error.toString());
            throw error;
        } else {
            nlapiLogExecution('ERROR', 'Unexpected Error UserEvent_RefundCharge', error.toString());
            alert(error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function createPayment(currency,amount,transId,subsidiary,id,customer,typeCard){
    try {
        nlapiLogExecution('DEBUG', 'Start_CC_ProcessReturn - [' + id + ']');
        // Create Payment Record
        var arrC = new Array();
        var arrF = new Array();
        arrF.push(new nlobjSearchFilter("name", null, "anyof",customer ));

        var searchLocations = nlapiSearchRecord('transaction','customsearch_clgx_ss_ccards_in', arrF, arrC);

        var searchLoc = searchLocations[0];
        var columnsC = searchLoc.getAllColumns();
        var location = searchLoc.getValue(columnsC[0]);
        var record = nlapiCreateRecord('customerpayment');
        record.setFieldValue('autoapply', 'T');
        record.setFieldValue('currency', currency);
        record.setFieldValue('exchangerate', 1);
        record.setFieldValue('location', location);
        record.setFieldValue('customer', customer);
        record.setFieldValue('payment', amount);
        record.setFieldValue('trandate', moment().format('MM/DD/YYYY'));
        record.setFieldValue('tranid', "cc auth "+typeCard+' '+transId);
        record.setFieldValue('checknum', "cc auth "+typeCard+' '+transId);
        record.setFieldValue('custbody_clgx_credit_card_tr',id);
        record.setFieldValue('memo','Credit card authorization #'+ transId+' received from Authorized.net.')
        record.setFieldValue('aracct', 122);
        record.setFieldValue('subsidiary', subsidiary);
        record.setFieldValue('undepfunds', 'T');
        record.setFieldValue('custbody_clgx_credit_card_trans', id);

        var newid = nlapiSubmitRecord(record, false, true);
        nlapiLogExecution('DEBUG', 'CC_ProcessReturn - [' + newid + ']');

        // Update Customer Balance- It's done automatically when a Payment is created
        //  var recCompany = nlapiLoadRecord('customer', customer);
        //   var amountB=recCompany.getFieldValue('custentity_clgx_customer_balance');
        //   var balance=parseFloat(amountB)-parseFloat(amount);
        //  recCompany.setFieldValue('custentity_clgx_customer_balance',balance);
        // nlapiSubmitRecord(recCompany, false, true);
        var balance = clgx_update_balances (customer);
        var customerBalance = parseFloat(nlapiLookupField('customer', customer, 'custentity_clgx_customer_balance') || 0);
        if(customerBalance>0){

            nlapiSubmitField('customer', customer, ['custentity_clgx_cc_paused'], 'T');

        }
        else if(customerBalance==0){

            nlapiSubmitField('customer', customer, ['custentity_clgx_cc_paused'], 'F');

        }
        return newid;

    } catch (error) { // Start Catch Errors Section
        if (error.getDetails != undefined) {
            nlapiLogExecution('ERROR', 'Process Error CC_ProcessReturn', error.getCode() + ': ' + error.getDetails());
            throw error;
        } else {
            nlapiLogExecution('ERROR', 'Unexpected Error CC_ProcessReturn', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
        return false;
    }
}

function getCustomers(){
    try {


// ================================================= build dynamic queue =================================================

        var customers = [];
        var search = nlapiLoadSearch('customer', 'customsearch_clgx_ccards_queue_customers');
        var resultSet = search.runSearch();
        resultSet.forEachResult(function(searchResult) {
            customers.push({
                "customerid": parseInt(searchResult.getValue('internalid',null,null)),
                "customer": searchResult.getValue('entityid',null,null),
                "subsidiaryid": parseInt(searchResult.getValue('subsidiary',null,null)),
                "balance": parseFloat(searchResult.getValue('custentity_clgx_customer_balance',null,null)) || 0,
                "tocharge":  parseFloat(searchResult.getValue('custentity_clgx_customer_balance',null,null)) || 0
            });
            return true;
        });

        var ids = _.pluck(customers, 'customerid');
        if(customers.length>0) {
            var invoices = [];
            var filters = [];
            filters.push(new nlobjSearchFilter('custrecord_clgx_consol_inv_customer', null, 'anyof', ids));
            var search = nlapiLoadSearch('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_ccards_queue_invoices');
            search.addFilters(filters);
            var resultSet = search.runSearch();
            resultSet.forEachResult(function (searchResult) {
                invoices.push({
                    "customerid": parseInt(searchResult.getValue('custrecord_clgx_consol_inv_customer', null, 'GROUP')),
                    "total": parseFloat(searchResult.getValue('custrecord_clgx_consol_inv_total', null, 'SUM')) || 0
                });
                return true;
            });

            for (var i = 0; i < customers.length; i++) {
                var invoice = _.find(invoices, function (arr) {
                    return (arr.customerid == customers[i].customerid);
                });
                if (invoice) {
                    customers[i].tocharge = (customers[i].balance - invoice.total).toFixed(2);
                }
            }
        }
        return (JSON.stringify(customers));



    }

    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}