nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Invoices_Preview.js
//	Script Name:	CLGX_SL_Invoices_Preview
//	Script Id:		customscript_clgx_sl_inv_preview
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/11/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=583&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_inv_preview (request, response){
    try {
    	
    	var location = request.getParameter('location');
        var locationid = request.getParameter('locationid');
        var customerid = request.getParameter('customerid');
        var customer = request.getParameter('customer');
        var outputid = request.getParameter('outputid');
        var cyear = request.getParameter('cyear');
        var cmonth = request.getParameter('cmonth');
        var dyear = request.getParameter('dyear');
        var dmonth = request.getParameter('dmonth');
        var strmonth = request.getParameter('strmonth');
        
    	var ids = request.getParameter('ids');
    	var arrStr = ids.split(',');
    	var arrIDs = new Array();
    	for ( var i = 0; i < arrStr.length; i++ ) {
    		arrIDs.push(parseInt(arrStr[i]));
    	}
    	
    	var start = moment(cmonth + '/1/' + cyear).format('M/D/YYYY');
    	var end = moment(cmonth + '/1/' + cyear).endOf('month').format('M/D/YYYY');
    	
    	if(locationid > 0){
        	
        	var objFile = nlapiLoadFile(3183474);
            var html = objFile.getValue();
            
            var objParams = new Object();
            objParams["locationid"] = locationid;
    		objParams["location"] = location;
    		objParams["customerid"] = customerid;
    		objParams["customer"] = customer;
    		objParams["outputid"] = outputid;
    		objParams["start"] = start;
    		objParams["end"] = end;
    		objParams["cmonth"] = cmonth;
    		objParams["cyear"] = cyear;
    		objParams["dmonth"] = dmonth;
    		objParams["dyear"] = dyear;
    		objParams["strmonth"] = strmonth;
    		objParams["ids"] = arrIDs;
    		
    		html = html.replace(new RegExp('{params}','g'), JSON.stringify(objParams));
    		html = html.replace(new RegExp('{cinvoices}','g'), JSON.stringify(clgx_inv_get_cinvoices (locationid,customerid,cyear,cmonth,start,end,arrIDs)));
        }
        else{
            var html = 'Please select a customer or an invoice from the left panel.';
        }
    	
    	var usageConsumtion = 1000 - parseInt(nlapiGetContext().getRemainingUsage());
    	nlapiLogExecution('DEBUG','usageConsumtion', '| Customer - ' + customer + '| CustomerID - ' + customerid + ' | All Usage - ' + usageConsumtion) + ' |'; 
    	
        response.write( html );
        
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

