nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Invoices_Invoice_JSON.js
//	Script Name:	CLGX_SL_Invoices_Invoice_JSON
//	Script Id:		customscript_clgx_sl_inv_invoice_json
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		02/09/2016
//-------------------------------------------------------------------------------------------------

function suitelet_inv_invoice_json (request, response){
    try {

    	var location = request.getParameter('location');
        var locationid = request.getParameter('locationid');
        var customer = request.getParameter('customer');
        var customerid = request.getParameter('customerid');
        var invoice = request.getParameter('invoice');
        var invoiceid = request.getParameter('invoiceid');
        var cyear = request.getParameter('cyear');
        var cmonth = request.getParameter('cmonth');
        var strmonth = request.getParameter('strmonth');
        
    	var ids = request.getParameter('ids');
    	var arrStr = ids.split(',');
    	var arrIDs = new Array();
    	for ( var i = 0; i < arrStr.length; i++ ) {
    		arrIDs.push(parseInt(arrStr[i]));
    	}
    	
    	var start = moment(cmonth + '/1/' + cyear).format('M/D/YYYY');
    	var end = moment(cmonth + '/1/' + cyear).endOf('month').format('M/D/YYYY');
    	
    	var objCInvoices = clgx_inv_get_cinvoices (locationid,customerid,cyear,cmonth,start,end,arrIDs);
    	var objCInvoice = objCInvoices.children[0];
    	
    	response.write(JSON.stringify(objCInvoices, null, 4));

    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

