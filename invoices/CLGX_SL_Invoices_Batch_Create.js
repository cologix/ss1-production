nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Invoices_Batch_Create.js
//	Script Name:	CLGX_SL_Invoices_Batch_Create
//	Script Id:		customscript_clgx_sl_inv_batch_create
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/12/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=585&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_inv_batch_create (request, response){
    try {

    	var userid = nlapiGetUser();
    	var today = moment().format('M/D/YYYY');
    	
    	var type = parseInt(request.getParameter('type'));
    	var locationid = parseInt(request.getParameter('locationid'));
        var customerid = parseInt(request.getParameter('customerid'));
        var outputid = parseInt(request.getParameter('outputid'));
        var cyear = parseInt(request.getParameter('cyear'));
        var cmonth = parseInt(request.getParameter('cmonth'));
        var dyear = parseInt(request.getParameter('dyear'));
        var dmonth = parseInt(request.getParameter('dmonth'));
    	
    	var ids = request.getParameter('ids');
    	var arrStr = ids.split(',');
    	var arrIDs = new Array();
    	for ( var i = 0; i < arrStr.length; i++ ) {
    		arrIDs.push(parseInt(arrStr[i]));
    	}
    	
    	var recBatch = nlapiCreateRecord('customrecord_clgx_queue_json');
    	recBatch.setFieldValue('custrecord_clgx_queue_json_type', type);
    	recBatch.setFieldValue('custrecord_clgx_queue_json_processed', 'F');
    	recBatch.setFieldValue('custrecord_clgx_queue_json_user', userid);
    	recBatch.setFieldValue('custrecord_clgx_queue_json_date', today);
		var batchid = nlapiSubmitRecord(recBatch, false,true);
    	
		var objBatch = new Object();
		objBatch["batchid"] = parseInt(batchid);
    	objBatch["userid"] = parseInt(userid);
    	objBatch["type"] = parseInt(request.getParameter('type'));
		objBatch["locationid"] = parseInt(request.getParameter('locationid'));
		objBatch["customerid"] = parseInt(request.getParameter('customerid'));
		objBatch["outputid"] = parseInt(request.getParameter('outputid'));
		objBatch["cmonth"] = parseInt(request.getParameter('cmonth'));
		objBatch["cyear"] = parseInt(request.getParameter('cyear'));
		objBatch["dmonth"] = parseInt(request.getParameter('dmonth'));
		objBatch["dyear"] = parseInt(request.getParameter('dyear'));
		objBatch["fileid"] = 0;
		objBatch["ids"] = arrIDs;
		objBatch["queue"] = arrIDs;
		objBatch["ciids"] = [];
		
		var fileName = 'batch_' + batchid + '_' + cyear + '-' + cmonth + '_' + outputid;
		
        var environment = nlapiGetContext().getEnvironment();
    	var folder = clgx_inv_get_folder(outputid,cyear,cmonth,1);
    	
    	var file = nlapiCreateFile(fileName + '.json', 'PLAINTEXT', JSON.stringify(objBatch));
    	file.setFolder(folder);
		var fileid = nlapiSubmitFile(file);
    	
		nlapiSubmitField('customrecord_clgx_queue_json', batchid, 'custrecord_clgx_queue_json_fileid',fileid);
		
		// trigger script to process batch - depending on type - send fileQueID
		var arrParam = new Array();
        arrParam['custscript_clgx_queue_json_fileid'] = fileid;
        arrParam['custscript_clgx_queue_json_batchid'] = batchid;
        nlapiScheduleScript('customscript_clgx_ss_inv_batch_process', 'customdeploy_clgx_ss_inv_batch_process' ,arrParam);       

		response.write('The consolidation batch was created and will be processed soon.');

		
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
