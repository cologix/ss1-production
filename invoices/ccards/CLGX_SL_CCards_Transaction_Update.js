nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CCards_Transaction_Update.js
//	Script Name:	CLGX_SL_CCards_Transaction_Update
//	Script Id:		customscript_clgx_SL_CCards_Transaction_Update
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Catalina Taran - catalina.taran@cologix.com

//-------------------------------------------------------------------------------------------------

function suitelet_updatetransaction (request, response){
    try {
        var tranid= request.getParameter('tranid');
        var customerid= request.getParameter('customerid');
        var amounttr=request.getParameter('amounttr');
        var amountref=request.getParameter('amountref');
        var reruncard=request.getParameter('reruncard')||0;



        if(amountref!='')
        {
            var amunapplied=parseFloat(amounttr)-parseFloat(amountref);
        }
        else{
            var amunapplied=parseFloat(amounttr);
        }

        if(tranid > 0){

            if(reruncard>0){
                var objFile = nlapiLoadFile(5699872);
                var html = objFile.getValue();
                html = html.replace(new RegExp('{tranRec}','g'), reRunCard(tranid));
            }
            else{
                var objFile = nlapiLoadFile(5699871);
                var html = objFile.getValue();
                html = html.replace(new RegExp('{data}','g'), getListOfPaymentsandCreditMemos (customerid, tranid,amunapplied));


            }
        }
        else{
            var html = 'Please select a transaction';
        }
        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function getListOfPaymentsandCreditMemos (customerid, tranid,amunapplied){


    var filters = new Array();
    var columns = new Array();
    filters.push(new nlobjSearchFilter("name", null, "anyof", customerid));

    var rows = nlapiSearchRecord('transaction', 'customsearch_clgx_ccards_payandcredit', filters, columns);
    var arr = new Array();

    for (var i = 0; rows != null && i < rows.length; i++) {
        // categories
        var row = rows[i];
        var columns = row.getAllColumns();
        var id = row.getValue(columns[0]);
        var refno = row.getValue(columns[3]);
        var recType=row.getText(columns[1]);
        var  amRemaining= row.getValue(columns[2]) || 0;
        var objData = new Object();
        objData['tranid']=tranid;
        objData['payid']=id;
        objData['refno']=refno;
        objData['recType']=recType;
        objData['amount']=parseFloat(0).toFixed(2);
        objData['total']=parseFloat(0).toFixed(2);
        objData['amRemaining']=amRemaining;
        objData['amunapplied']=amunapplied;
        objData['customer']=customerid;

        arr.push(objData);


    }
    return JSON.stringify(arr)
}
function reRunCard(id){
    var transaction = nlapiLoadRecord('customrecord_clgx_ccards_processing', id);
    var obj = {
        "id": id,
        "amount": transaction.getFieldValue('custrecord_clgx_ccards_amount'),
        "date": transaction.getFieldValue('custrecord_clgx_ccards_submit_date'),
        "status": transaction.getFieldText('custrecord_clgx_ccards_status'),
        "customer": transaction.getFieldValue('custrecord_clgx_ccards_customer'),
        "reason": transaction.getFieldValue('custrecord_clgx_ccards_response_text')
    };

    return JSON.stringify(obj);
}

