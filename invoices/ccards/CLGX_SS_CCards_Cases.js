nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CCards_Cases.js
//	Script Name:	CLGX_SS_CCards_Cases
//	Script Id:		customscript_clgx_ss_ccards_cases
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		28/04/2017
//-------------------------------------------------------------------------------------------------

function sc_dailyCases() {
    nlapiLogExecution('DEBUG', 'Email_Daily_Digest');
    try {
        var arrColumns = new Array();

        var arrFilters = new Array();

        var rows = nlapiSearchRecord('customrecord_clgx_ccards_processing', 'customsearch_clgx_ccards_tr_crcases', arrFilters, arrColumns);

        if (rows!= null)
        {

            var dat = moment().format('MM/DD/YYYY');
            var subject = 'Credit Card Log US- ' + dat;
            nlapiLogExecution('DEBUG', 'Credit Card Log US-Begin', 'Credit Card Log US- ' + dat);

            var hbody = '<HTML><table border = 1 cellspacing=7 cellpadding=7><tr><td> Customer ID </td><td> Company Name </td><td> CC Type </td><td> Transaction Type </td><td> Transaction Date </td><td> Amount</td><td> Transaction ID </td></tr>';
            for (var i = 0; rows != null && i < rows.length; i++) {
                var row = rows[i];
                var id = row.getValue('internalid');
                var columns = row.getAllColumns();
                var id = row.getValue(columns[0]);
                var customer_id = row.getValue(columns[1]);
                var company_name = row.getText(columns[1]);
                var type_name = row.getText(columns[2]);
                var c_type = row.getValue(columns[3]);
                var date = row.getValue(columns[4]);
                var amount = row.getValue(columns[5]);
                var transaction_id = row.getValue(columns[6]);

                hbody = hbody + '<TR>';
                hbody = hbody + '<TD><A HREF="https://1337135.app.netsuite.com/app/common/entity/custjob.nl?id=' + customer_id + '">' + customer_id + '</A></TD>';
                hbody = hbody + '<TD>' + company_name + '</TD>';
                hbody = hbody + '<TD>' + c_type + '</TD>';
                hbody = hbody + '<TD>' + type_name + '</TD>';
                hbody = hbody + '<TD>' + date + '</TD>';
                hbody = hbody + '<TD>' + amount + '</TD>';
                hbody = hbody + '<TD>' + transaction_id + '</TD>';
                hbody = hbody + '</TR>';
                //recQueue.setFieldValue('custrecord_clgx_ccards_reported', 'T');
                //nlapiSubmitRecord(recQueue, false, true);
            }
            hbody = hbody + '</table></HTML>'
            var record = nlapiCreateRecord('supportcase');
            record.setFieldValue('company', 12827);
            record.setFieldValue('custevent_cologix_facility', 16);
            record.setFieldValue('email', 'donotreply@cologix.com');
            record.setFieldValue('title', subject);
            record.setFieldValue('category', 11);
            record.setFieldValue('custevent_cologix_sub_case_type', 54);
            record.setFieldValue('outgoingmessage', hbody);
            record.setFieldValue('priority', 2);
            record.setFieldValue('origin', 1);
            //record.setFieldValue('assigned', iAssignToGroup);

            try {
                var idRec = nlapiSubmitRecord(record, false, true);
               // return idRec;
            } catch (error) {
                nlapiLogExecution('ERROR', 'nlapiSubmitRecord Error', error.getCode() + ': ' + error.getDetails());
            }
        }

        var rows = nlapiSearchRecord('customrecord_clgx_ccards_processing', 'customsearch_clgx_ccards_tr_crcases_2', arrFilters, arrColumns);

        if (rows == null) { return; }

        var dat = moment().format('MM/DD/YYYY');
        var subject = 'Credit Card Log CAD- ' + dat;
        nlapiLogExecution('DEBUG','Credit Card Log CAD-Begin',  'Credit Card Log CAD- ' + dat);

        var hbody = '<HTML><table border = 1 cellspacing=7 cellpadding=7><tr><td> Customer ID </td><td> Company Name </td><td> CC Type </td><td> Transaction Type </td><td> Transaction Date </td><td> Amount</td><td> Transaction ID </td></tr>';
        for (var i = 0; rows != null && i < rows.length; i++) {
            var row = rows[i];
            var id = row.getValue('internalid');
            var columns= row.getAllColumns();
            var id = row.getValue(columns[0]);
            var customer_id = row.getValue(columns[1]);
            var company_name = row.getText(columns[1]);
            var type_name = row.getText(columns[2]);
            var c_type = row.getValue(columns[3]);
            var date = row.getValue(columns[4]);
            var amount = row.getValue(columns[5]);
            var transaction_id = row.getValue(columns[6]);

            hbody = hbody + '<TR>';
            hbody = hbody + '<TD><A HREF="https://1337135.app.netsuite.com/app/common/entity/custjob.nl?id=' + customer_id + '">' + customer_id + '</A></TD>';
            hbody = hbody + '<TD>' + company_name + '</TD>';
            hbody = hbody + '<TD>' + c_type+ '</TD>';
            hbody = hbody + '<TD>' + type_name + '</TD>';
            hbody = hbody + '<TD>' + date + '</TD>';
            hbody = hbody + '<TD>' + amount + '</TD>';
            hbody = hbody + '<TD>' + transaction_id + '</TD>';
            hbody = hbody + '</TR>';
            //recQueue.setFieldValue('custrecord_clgx_ccards_reported', 'T');
            //nlapiSubmitRecord(recQueue, false, true);
        }
        hbody = hbody + '</table></HTML>'
        var record = nlapiCreateRecord('supportcase');
        record.setFieldValue('company', 12827);
        record.setFieldValue('custevent_cologix_facility', 16);
        record.setFieldValue('email', 'donotreply@cologix.com');
        record.setFieldValue('title', subject);
        record.setFieldValue('category', 11);
        record.setFieldValue('custevent_cologix_sub_case_type', 54);
        record.setFieldValue('outgoingmessage', hbody);
        record.setFieldValue('priority', 2);
        record.setFieldValue('origin', 1);
        //record.setFieldValue('assigned', iAssignToGroup);

        try {
            var idRec = nlapiSubmitRecord(record, false, true);
          //  return idRec;
        } catch (error) {
            nlapiLogExecution('ERROR','nlapiSubmitRecord Error', error.getCode() + ': ' + error.getDetails());
        }

    } catch (error) { // Start Catch Errors Section
        if (error.getDetails != undefined) {
            nlapiLogExecution('ERROR', 'Process Error Email_Declined', error.getCode() + ': ' + error.getDetails());
            alert(error.getDetails());
            throw error;
        } else {
            nlapiLogExecution('ERROR', 'Unexpected Error Email_Declined', error.toString());
            alert(error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}