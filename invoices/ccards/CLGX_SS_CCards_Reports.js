nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CCards_Reports.js
//	Script Name:	CLGX_SS_CCards_Reports
//	Script Id:		customscript_clgx_ss_ccards_reports
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		28/04/2017
//-------------------------------------------------------------------------------------------------

function sc_mthReports() {
    nlapiLogExecution('DEBUG', 'Email_Montly_Digest');
    try {
        var arrColumns = new Array();

        var arrFilters = new Array();

        var rows = nlapiSearchRecord('customrecord_clgx_ccards_processing', 'customsearch_clgx_ss_ccards_trmth', arrFilters, arrColumns);
        if (rows!= null)
        {
            var arrObj=new Array();
            var total=0;
            var totalType=0;
            for (var i = 0; rows != null && i < rows.length; i++) {
                var row = rows[i];
                var columns = row.getAllColumns();
                var market = row.getValue(columns[0]);
                var type = row.getValue(columns[2]);
                var percent = row.getValue(columns[3]);
                var amount = row.getValue(columns[1]);

                if(i==0){
                    var obj=new Object();
                    var objCards=new Object();
                    obj.type=type;
                    objCards={name:market,amount:amount,percent:percent};
                    obj['cards']=new Array();
                    obj['cards'].push(objCards);
                    total +=parseFloat(amount);
                    totalType+=parseFloat(amount);
                    var x=0;
                }
                if(i>0){
                    var row1 = rows[i-1];
                    var columns1 = row1.getAllColumns();
                    var type1 = row1.getValue(columns1[2]);
                    if(type1==type){
                        objCards={name:market,amount:amount,percent:percent};
                        obj['cards'].push(objCards);
                        total +=parseFloat(amount);
                        totalType+=parseFloat(amount);
                    }
                    else {
                        obj.totalType=totalType;
                        arrObj.push(obj);
                        totalType=0;
                        var obj=new Object();
                        var objCards=new Object();
                        obj.type=type;
                        objCards={name:market,amount:amount,percent:percent};
                        obj['cards']=new Array();
                        obj['cards'].push(objCards);
                        total +=parseFloat(amount);
                        totalType +=parseFloat(amount);
                    }
                }
                if(i ==(rows.length-1)){
                    obj.totalType=totalType;
                    arrObj.push(obj);
                }
            }
            var dat = moment().format('MM/DD/YYYY');
            var subject = 'Monthly Card Report US- ' + dat;
            //nlapiLogExecution('DEBUG', 'Credit Card Log US-Begin', 'Credit Card Log US- ' + dat);
            var totalamount=0;
            var hbody = '<HTML><table border = 1 cellspacing=3 cellpadding=3 style="border-collapse: collapse; border-color:#D3D3D3;text-align: left;padding: 30px; width:350px; font-size:13px">';
            for (var j = 0; arrObj != null && j < arrObj.length; j++) {
                var objCC=arrObj[j].cards;
                hbody += '<tr style=" border:1px solid #D3D3D3;"><td style="font-weight: bold; ">'+arrObj[j].type+' </td><td>  </td><td style="font-weight: bold; ">$'+parseFloat(arrObj[j].totalType).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+' </td></tr>';
                for (var k = 0; objCC != null && k < objCC.length; k++) {
                    var percarray=objCC[k].percent.split('%');
                    var per=parseFloat(percarray[0]).toFixed(2);
                    hbody += '<tr style=" border:1px solid #D3D3D3;"><td>'+objCC[k].name+' </td><td>'+per+'% </td><td>$'+parseFloat(objCC[k].amount).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'  </td></tr>';

                }

            }
            hbody += '<tr style="border:1px solid #D3D3D3;border-top:1px solid #111111 !important;"><td>  </td><td> Grand Total</td><td>$'+parseFloat(total).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+' </td></tr>';
            hbody = hbody + '</table></HTML>'
            var record = nlapiCreateRecord('supportcase');
            record.setFieldValue('company', 12827);
            record.setFieldValue('custevent_cologix_facility', 16);
            record.setFieldValue('email', 'donotreply@cologix.com');
            record.setFieldValue('title', subject);
            record.setFieldValue('category', 11);
            record.setFieldValue('custevent_cologix_sub_case_type', 54);
            record.setFieldValue('outgoingmessage', hbody);
            record.setFieldValue('priority', 2);
            record.setFieldValue('origin', 1);
            //record.setFieldValue('assigned', iAssignToGroup);

            try {
                var idRec = nlapiSubmitRecord(record, false, true);
                // return idRec;
            } catch (error) {
                nlapiLogExecution('ERROR', 'nlapiSubmitRecord Error', error.getCode() + ': ' + error.getDetails());
            }
        }
//Canada

            var arrColumns = new Array();

            var arrFilters = new Array();

            var rows = nlapiSearchRecord('customrecord_clgx_ccards_processing', 'customsearch_clgx_ss_ccards_trmth_2', arrFilters, arrColumns);
            if (rows!= null)
            {
                var arrObj=new Array();
                var total=0;
                var totalType=0;
                for (var i = 0; rows != null && i < rows.length; i++) {
                    var row = rows[i];
                    var columns = row.getAllColumns();
                    var market = row.getValue(columns[0]);
                    var type = row.getValue(columns[2]);
                    var percent = row.getValue(columns[3]);
                    var amount = row.getValue(columns[1]);

                    if(i==0){
                        var obj=new Object();
                        var objCards=new Object();
                        obj.type=type;
                        objCards={name:market,amount:amount,percent:percent};
                        obj['cards']=new Array();
                        obj['cards'].push(objCards);
                        total +=parseFloat(amount);
                        totalType+=parseFloat(amount);
                        var x=0;
                    }
                    if(i>0){
                        var row1 = rows[i-1];
                        var columns1 = row1.getAllColumns();
                        var type1 = row1.getValue(columns1[2]);
                        if(type1==type){
                            objCards={name:market,amount:amount,percent:percent};
                            obj['cards'].push(objCards);
                            total +=parseFloat(amount);
                            totalType+=parseFloat(amount);
                        }
                        else {
                            obj.totalType=totalType;
                            arrObj.push(obj);
                            totalType=0;
                            var obj=new Object();
                            var objCards=new Object();
                            obj.type=type;
                            objCards={name:market,amount:amount,percent:percent};
                            obj['cards']=new Array();
                            obj['cards'].push(objCards);
                            total +=parseFloat(amount);
                            totalType +=parseFloat(amount);
                        }
                    }
                    if(i ==(rows.length-1)){
                        obj.totalType=totalType;
                        arrObj.push(obj);
                    }
                }
                var dat = moment().format('MM/DD/YYYY');
                var subject = 'Monthly Card Report CA- ' + dat;
                //nlapiLogExecution('DEBUG', 'Credit Card Log US-Begin', 'Credit Card Log US- ' + dat);
                var totalamount=0;
                var hbody = '<HTML><table border = 1 cellspacing=3 cellpadding=3 style="border-collapse: collapse; border-color:#D3D3D3;text-align: left;padding: 30px; width:350px; font-size:13px">';
                for (var j = 0; arrObj != null && j < arrObj.length; j++) {
                    var objCC=arrObj[j].cards;
                    hbody += '<tr style=" border:1px solid #D3D3D3;"><td style="font-weight: bold; ">'+arrObj[j].type+' </td><td>  </td><td style="font-weight: bold; ">$'+parseFloat(arrObj[j].totalType).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+' </td></tr>';
                    for (var k = 0; objCC != null && k < objCC.length; k++) {
                        var percarray=objCC[k].percent.split('%');
                        var per=parseFloat(percarray[0]).toFixed(2);
                        hbody += '<tr style=" border:1px solid #D3D3D3;"><td>'+objCC[k].name+' </td><td>'+per+'% </td><td>$'+parseFloat(objCC[k].amount).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'  </td></tr>';

                    }

                }
                hbody += '<tr style="border:1px solid #D3D3D3;border-top:1px solid #111111 !important;"><td>  </td><td> Grand Total</td><td>$'+parseFloat(total).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+' </td></tr>';
                hbody = hbody + '</table></HTML>'
                var record = nlapiCreateRecord('supportcase');
                record.setFieldValue('company', 12827);
                record.setFieldValue('custevent_cologix_facility', 16);
                record.setFieldValue('email', 'donotreply@cologix.com');
                record.setFieldValue('title', subject);
                record.setFieldValue('category', 11);
                record.setFieldValue('custevent_cologix_sub_case_type', 54);
                record.setFieldValue('outgoingmessage', hbody);
                record.setFieldValue('priority', 2);
                record.setFieldValue('origin', 1);
                //record.setFieldValue('assigned', iAssignToGroup);

                try {
                    var idRec = nlapiSubmitRecord(record, false, true);
                    // return idRec;
                } catch (error) {
                    nlapiLogExecution('ERROR', 'nlapiSubmitRecord Error', error.getCode() + ': ' + error.getDetails());
                }
            }





    } catch (error) { // Start Catch Errors Section
        if (error.getDetails != undefined) {
            nlapiLogExecution('ERROR', 'Process Error Email_Declined', error.getCode() + ': ' + error.getDetails());
            alert(error.getDetails());
            throw error;
        } else {
            nlapiLogExecution('ERROR', 'Unexpected Error Email_Declined', error.toString());
            alert(error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}