//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Customers_CCards.js
//	Script Name:	CLGX_SL_Customers_CCards
//	Script Id:		customscript_clgx_sl_customer_ccards
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/24/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=579&deploy=1
//------------------------------------------------------------------------------------------------- 

function suitelet_customer_ccards (request, response){
    try {
    	
        var id = request.getParameter('id');
        
        if(id > 0){
        	/*
        	var objTree = get_customers(locationid,location,start,end);
            var objFile = nlapiLoadFile(3146734);
            var html = objFile.getValue();
            
            var objParams = new Object();
    		objParams["locationid"] = locationid;
    		objParams["location"] = location;
    		objParams["start"] = start;
    		objParams["end"] = end;
    		objParams["cmonth"] = cmonth;
    		objParams["cyear"] = cyear;
    		objParams["strmonth"] = strmonth;
    		objParams["customers"] = (objTree.records).toString();
    		objParams["invoices"] = (objTree.invoices).toString();
    		
    		html = html.replace(new RegExp('{customers}','g'), JSON.stringify(objTree));
            html = html.replace(new RegExp('{params}','g'), JSON.stringify(objParams));
            */
        }
        else{
            var html = 'Please select a customer from the left panel.';
        }
        response.write( html );

    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


