nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Invoices_Invoice_PDF.js
//	Script Name:	CLGX_SL_Invoices_Invoice_PDF
//	Script Id:		customscript_clgx_sl_inv_invoice_pdf
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/30/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=581&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_inv_invoice_pdf (request, response){
	try {

		var fileid = request.getParameter('fileid');
		var width = request.getParameter('width');
		var height = request.getParameter('height');
		var scale = request.getParameter('scale');
		
		try {
			var pdfFile = nlapiLoadFile(fileid);
			var urlFile = pdfFile.getURL();
			var nameFile = pdfFile.getName();

        	var objFile = nlapiLoadFile(3166988);
			var html = objFile.getValue();
			//html = html.replace(new RegExp('{params}','g'),JSON.parse( objParams ));
			
			html = html.replace(new RegExp('{width}','g'),width);
			html = html.replace(new RegExp('{height}','g'),height);
			html = html.replace(new RegExp('{scale}','g'),scale);
			html = html.replace(new RegExp('{name}','g'),nameFile);
			html = html.replace(new RegExp('{url}','g'),urlFile);
            
        }
        catch (e) {
            var str = String(e);
            if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
            	html = 'The requested file does not exist.';
            }
        }
        
        response.write( html );
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}



