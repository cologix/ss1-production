nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_getAUTH.js
//	ScriptID:	customscript_CLGX_RL_CP_getAUTH
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=675&deploy=1
//	@authors:	Catalina Taran- catalina.taran@cologix.com
//	Created:	27/03/2016
//------------------------------------------------------

function post(datain) {

    //if(srights.ccards > 0){

    var subsidiary = datain['subsidiary'] || "0";
    var environment = datain['environment'] || "";
    var type=1;
    if(environment=='prod')
    {
        type=2;
    }
    var obj=new Object();
    nlapiLogExecution('DEBUG','subsidiary', subsidiary);
    nlapiLogExecution('DEBUG','environment', environment);
    if(subsidiary>0) {
        // define search filters
        var filters = new Array();

        filters[0] = new nlobjSearchFilter( 'custrecord_clgx_auth_net_subsidiary', null, 'anyof', subsidiary);
        filters[1] = new nlobjSearchFilter( 'custrecord_clgx_auth_net_type', null, 'anyof', type);

// return keys
        var columns = new Array();
        columns[0] = new nlobjSearchColumn( 'custrecord_clgx_auth_net_api_id' );
        columns[1] = new nlobjSearchColumn( 'custrecord_clgx_auth_net_client_key' );
        columns[2] = new nlobjSearchColumn( 'custrecord_clgx_auth_net_transaction_key' );

// execute the customrecord_clgx_authorize_net search, passing all filters and return columns
        var searchresults = nlapiSearchRecord( 'customrecord_clgx_authorize_net', null, filters, columns );

// loop through the results
        for ( var i = 0; searchresults != null && i < searchresults.length; i++ )
        {
            // get result values
            var searchresult = searchresults[ i ];
            obj["name"] =searchresult.getValue( 'custrecord_clgx_auth_net_api_id' );
            obj["key"] =searchresult.getValue( 'custrecord_clgx_auth_net_client_key' );
            obj["keyT"] =searchresult.getValue( 'custrecord_clgx_auth_net_transaction_key' );
        }

    }

    obj.code = 'SUCCESS';
    obj.msg = 'You are managing cards.';



    return obj;

}
