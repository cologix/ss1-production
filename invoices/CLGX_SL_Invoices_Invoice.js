//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Invoices_Invoice.js
//	Script Name:	CLGX_SL_Invoices_Invoice
//	Script Id:		customscript_clgx_sl_inv_invoice
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/30/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=581&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_inv_invoice (request, response){
    try {
    	
        var location = request.getParameter('location');
        var locationid = request.getParameter('locationid');
        var customer = request.getParameter('customer');
        var customerid = request.getParameter('customerid');
        var invoice = request.getParameter('invoice');
        var invoiceid = request.getParameter('invoiceid');
        var year = request.getParameter('cyear');
        var month = request.getParameter('cmonth');
        
    	var start = moment(month + '/1/' + year).format('M/D/YYYY');
    	var end = moment(month + '/1/' + year).endOf('month').format('M/D/YYYY');
    	var cmonth = moment(month + '/1/' + year).format('MMMM');
    	
        if(locationid > 0){
        	
        	var objTree = get_customers(locationid,start,end);
            var objFile = nlapiLoadFile(3154150);
            var html = objFile.getValue();
            
            var objParams = new Object();
    		objParams["locationid"] = locationid;
    		objParams["location"] = location;
    		objParams["start"] = start;
    		objParams["end"] = end;
    		objParams["month"] = month;
    		objParams["year"] = year;
    		objParams["cmonth"] = cmonth;
    		objParams["cyear"] = year;
    		objParams["customers"] = (objTree.records).toString();
    		objParams["invoices"] = (objTree.invoices).toString();
    		
    		html = html.replace(new RegExp('{customers}','g'), JSON.stringify(objTree));
            html = html.replace(new RegExp('{params}','g'), JSON.stringify(objParams));
        }
        else{
            var html = 'Please select a location from the left panel.';
        }
        response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function get_customers (locationid,start,end){

	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('trandate',null,'within', start,end));
	arrFilters.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', locationid));
	var arrCustomers = new Array();
    var searchCustomers = nlapiLoadSearch('invoice', 'customsearch_clgx_inv_customers');
    searchCustomers.addFilters(arrFilters);
	var resultSet = searchCustomers.runSearch();
	resultSet.forEachResult(function(searchResult) {
		var objCustomer = new Object();
		objCustomer["customerid"] = parseInt(searchResult.getValue('entity',null,'GROUP'));
		objCustomer["customer"] = _.last((searchResult.getText('entity',null,'GROUP')).split(":")).trim();
		objCustomer["invoices"] = searchResult.getText('internalid',null,'COUNT');
		arrCustomers.push(objCustomer);
		return true;
	});
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('trandate',null,'within', start,end));
	arrFilters.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', locationid));
	var arrInvoices = new Array();
    var searchCustomers = nlapiLoadSearch('invoice', 'customsearch_clgx_inv_invoices');
    searchCustomers.addFilters(arrFilters);
	var resultSet = searchCustomers.runSearch();
	resultSet.forEachResult(function(searchResult) {
		var objInvoice = new Object();
		objInvoice["customerid"] = parseInt(searchResult.getValue('entity',null,'GROUP'));
		objInvoice["invoiceid"] = parseInt(searchResult.getValue('internalid',null,'GROUP'));
		objInvoice["invoice"] = searchResult.getValue('tranid',null,'GROUP');
		objInvoice["cinvoice"] = searchResult.getValue('custbody_consolidate_inv_nbr',null,'GROUP');
		objInvoice["batch"] = searchResult.getValue('custbody_consolidate_inv_id',null,'GROUP');
		arrInvoices.push(objInvoice);
		return true;
	});

	var objTree = new Object();
	objTree["text"] = '.';
	var arrC = new Array();
	var nbrInv = 0;
	for ( var i = 0; i < arrCustomers.length; i++ ) {
		
		objC = new Object();
		objC["entityid"] = arrCustomers[i].customerid;
		objC["entity"] = arrCustomers[i].customer;
		
		var arrCustomerInvoices = _.filter(arrInvoices, function(arr){
	        return arr.customerid === arrCustomers[i].customerid;
		});
		var arrI = new Array();
		for ( var j = 0; j < arrCustomerInvoices.length; j++ ) {
			
			objI = new Object();
			objI["entityid"] = arrCustomerInvoices[j].invoiceid;
			objI["entity"] = arrCustomerInvoices[j].invoice;
			objI["cinvoice"] = arrCustomerInvoices[j].cinvoice;
			objI["batch"] = arrCustomerInvoices[j].batch;
			objI["customerid"] = arrCustomers[i].customerid;
			objI["customer"] = arrCustomers[i].customer;
			objI["checked"] = true;
			objI["iconCls"] = 'invoice';
			objI["leaf"] = true;
			arrI.push(objI);
		}
		objC["records"] = arrCustomerInvoices.length;
		nbrInv += arrCustomerInvoices.length;
		objC["children"] = arrI;
		objC["checked"] = true;
		objC["expanded"] = true;
		objC["iconCls"] = 'customer';
		objC["leaf"] = false;
		arrC.push(objC);
	}
	objTree["invoices"] = nbrInv;
	objTree["records"] = arrCustomers.length;
	objTree["children"] = arrC;
	
	return objTree;
	
}


