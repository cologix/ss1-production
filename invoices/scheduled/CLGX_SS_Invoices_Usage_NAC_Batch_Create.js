nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Invoices_Usage_NAC_Batch_Create.js
//	Script Name:	CLGX_SS_Invoices_Usage_NAC_Batch_Create
//	Script Id:		customscript_clgx_ss_inv_usg_nac_btch_cr
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/13/2015
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_inv_usg_nac_btch_cr (){
    try {

    	var context = nlapiGetContext();
    	var environment = context.getEnvironment();
    	
    	if(environment == 'PRODUCTION'){
    		
    		nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
            
    		var today = moment().format('MM/D/YYYY');
	    	var year = moment().format('YYYY');
	    	var month = moment().format('M');
	    	//var today = moment(month + '/20/' + year).format('MM/D/YYYY');
	    	
// NAC raw usage ========================================================================================================
    	
		var requestURL = nlapiRequestURL(
    			  'https://nsapi1.dev.nac.net/v1.0/netsuite/usage/?per_page=0',
    			  null,
    			  {
    			    'Content-type': 'application/json',
    			    'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
    			  },
    			  null,
    			  'GET');
      	var objRequest = JSON.parse( requestURL.body );
	  	
      	nlapiSendEmail(71418,71418,'Start NAC Usage - ' + objRequest.total ,'',null,null,null,null);
      	
        var file = nlapiCreateFile('nac_usage_raw.json', 'PLAINTEXT', JSON.stringify(objRequest));
    	file.setFolder(2627008);
		var fileid = nlapiSubmitFile(file);
		
        var file = nlapiCreateFile('nac_usage_raw_' + today + '.json', 'PLAINTEXT', JSON.stringify(objRequest));
    	file.setFolder(2627008);
		var fileid = nlapiSubmitFile(file);
		
		
    	var arr = objRequest.data;
		var arr = _.filter(arr, function(arr){
			//return (arr.netsuite_item_id == 574 || arr.line.indexOf("Real Power, kW") > -1);
			return (arr.netsuite_item_id == 574);
		});
		var newbatch = {
				"data": arr
		};
        var file = nlapiCreateFile('nac_usage_network.json', 'PLAINTEXT', JSON.stringify(newbatch));
    	file.setFolder(2627008);
		var fileid = nlapiSubmitFile(file);
		
		var file = nlapiCreateFile('nac_usage_network_' + today + '.json', 'PLAINTEXT', JSON.stringify(newbatch));
    	file.setFolder(2627008);
		var fileid = nlapiSubmitFile(file);
		
	// Netsuite customers with legacy ids ========================================================================================================
	    	
			var arrCustomers = new Array();
	    	var searchCustomers = nlapiLoadSearch('customer', 'customsearch_clgx_nac_customers');
			var resultSet = searchCustomers.runSearch();
			resultSet.forEachResult(function(searchResult) {
				var objCustomer = new Object();
				objCustomer["customerid"] = parseInt(searchResult.getValue('internalid',null,null));
				objCustomer["customer"] = searchResult.getValue('entityid',null,null);
				objCustomer["subsidiaryid"] = parseInt(searchResult.getValue('subsidiary',null,null));
				objCustomer["subsidiary"] = searchResult.getText('subsidiary',null,null);
				objCustomer["nacid"] = parseInt(searchResult.getValue('custentity_clgx_matrix_entity_id',null,null));
				arrCustomers.push(objCustomer);
				return true;
			});
			var file = nlapiCreateFile('nac_customers_netsuite.json', 'PLAINTEXT', JSON.stringify(arrCustomers));
	    	file.setFolder(2627008);
			var fileid = nlapiSubmitFile(file);
					
					
	// NAC / Netsuite Customers ========================================================================================================
			
	    	var objFile = nlapiLoadFile(3206018);
	    	var arrNSCustomers = JSON.parse(objFile.getValue());
	    	
	// NAC Usage Network Raw ========================================================================================================
	    	
	    	var objFile = nlapiLoadFile(5585492);
	    	var objBatchAll = JSON.parse(objFile.getValue());
	    	
	// Netsuite Items ========================================================================================================
	    	
	  		var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null));
			arrColumns.push(new nlobjSearchColumn('name',null,null));
			arrColumns.push(new nlobjSearchColumn('class',null,null));
			arrColumns.push(new nlobjSearchColumn('isinactive',null,null));
			var arrFilters = new Array();
			//arrFilters.push(new nlobjSearchFilter("inactif",null,"is",'F'));
			var arrItems = new Array();
			var searchItems = nlapiSearchRecord('item', null, arrFilters, arrColumns);
			for ( var i = 0; searchItems != null && i < searchItems.length; i++ ) {
				var objItem = new Object();
				objItem["itemid"] = parseInt(searchItems[i].getValue('internalid',null,null));
				objItem["item"] = searchItems[i].getValue('name',null,null);
				objItem["classid"] = parseInt(searchItems[i].getValue('class',null,null));
				objItem["classname"] = searchItems[i].getText('class',null,null);
				objItem["inactive"] = searchItems[i].getValue('isinactive',null,null);
				arrItems.push(objItem);
			}	
			
	// Netsuite Legacy SOs  ========================================================================================================
			
			var searchSOs = nlapiSearchRecord('transaction', 'customsearch_clgx_inv_legacy_so_ids');
	    	var arrSOs = new Array();
			for ( var s = 0; s < searchSOs.length; s++ ) {
				var objSO = new Object();
				objSO["customerid"] = parseInt(searchSOs[s].getValue('internalid','customerMain','GROUP'));
				objSO["soid"] = parseInt(searchSOs[s].getValue('internalid',null,'GROUP'));
				objSO["legacyid"] = searchSOs[s].getValue('custbody_cologix_legacy_so',null,'GROUP');
				objSO["partnerid"] = searchSOs[s].getValue('partner',null,'GROUP');
				arrSOs.push(objSO);
			}
			
	// ==================================================================================================================================================
	    	
	    	var arrBatchAll = objBatchAll.data;
	    	var arrBatch = _.filter(arrBatchAll, function(arr){
	    		return (arr.line_total > 0);
		    });
	    	
	    	var arrNSLocations = ['NNJ1','NNJ2','NNJ3','NNJ4'];
	    	var arrNSLocationsIDs = ['53','54','55','56'];
	    	
	    	var arrSOsFound = new Array();
	    	var arrLines = new Array();
	    	for ( var i = 0; arrBatch != null && i < arrBatch.length; i++ ) {
	    		var objLine = new Object();
	    		
	    		var naccustomerid = parseInt(arrBatch[i].client_id);
	    		objLine["naccustomerid"] = naccustomerid;
	    		objLine["naccustomer"] = arrBatch[i].client_display_name;
	    		
				var objCustNS = _.find(arrNSCustomers, function(arr){ return (arr.nacid == naccustomerid) ; });
				if(objCustNS != null){
					objLine["customerid"] = objCustNS.customerid;
					objLine["customer"] = objCustNS.customer;
					objLine["subsidiaryid"] = objCustNS.subsidiaryid;
					objLine["subsidiary"] = objCustNS.subsidiary;
	    		}
	    		else{
	    			objLine["customerid"] = 0;
	    			objLine["customer"] = '';
	    			objLine["subsidiaryid"] = 0;
	    			objLine["subsidiary"] = '';
	    		}
	    		
	    		objLine["facilityid"] = parseInt(arrBatch[i].facility_id);
	    		objLine["facility"] = arrBatch[i].facility_name;
	    		objLine["locationid"] = parseInt(arrNSLocationsIDs [arrNSLocations.indexOf(arrBatch[i].facility_market_name)]);
	    		objLine["location"] = arrBatch[i].facility_market_name;
	    		
	    		objLine["serviceid"] = arrBatch[i].service_id;
	    		objLine["service_instance_id"] = arrBatch[i].service_instance_id;
	    		objLine["sub_object_id"] = arrBatch[i].sub_object_id;
	    		objLine["lineid"] = parseInt(arrBatch[i].id);
	    		objLine["legacyid"] = (arrBatch[i].service_instance_id).toString();
	    		
    			objLine["soid"] = 0;
    			objLine["partnerid"] = 0;
    			objLine["iaddressee"] = '';
    			objLine["matrixid"] = parseInt(((arrBatch[i].service_id).split("."))[0]);

    			var arrColumns = new Array();
				var arrFilters = new Array();
				arrFilters.push(new nlobjSearchFilter("internalid",'CUSTCOL_CLGX_SO_COL_SERVICE_ID',"anyof", objLine.matrixid));
				var searchSOs = nlapiSearchRecord('transaction', 'customsearch_clgx_inv_legacy_so_ids_all', arrFilters, arrColumns);
				
				if(searchSOs){
					var soid = parseInt(searchSOs[0].getValue('internalid',null,'GROUP')) || 0;
	    			objLine.soid = soid;
	    			objLine.partnerid = parseInt(searchSOs[0].getValue('partner',null,'GROUP')) || 0;
	    			objLine.iaddressee = searchSOs[0].getValue('billaddressee',null,'GROUP') || '';
	    			arrSOsFound.push(soid);
				}
				
				var itemid = parseInt(arrBatch[i].netsuite_item_id);
	    		objLine["itemid"] = itemid;
	    		var objItem = _.find(arrItems, function(arr){ return (arr.itemid == itemid) ; });
				if(objItem != null){
					objLine["itemid"] = objItem.itemid;
					objLine["item"] = objItem.item;
					objLine["classid"] = objItem.classid;
					objLine["classname"] = objItem.classname;
					objLine["inactive"] = objItem.inactive;
	    		}
	    		else{
	    			objLine["itemid"] = 0;
					objLine["item"] = '';
					objLine["classid"] = 0;
					objLine["classname"] = '';
					objLine["inactive"] = 'T';
	    		}
				
				objLine["start"] = moment(arrBatch[i].for_service_from).format('M/D/YYYY');
				objLine["end"] = moment(arrBatch[i].for_service_to).format('M/D/YYYY');
				
				//objLine["description"] = arrBatch[i].description;
	    		objLine["description"] = arrBatch[i].line;
	    		objLine["quantity"] = parseFloat(arrBatch[i].quantity);
	    		objLine["rate"] = parseFloat(arrBatch[i].rate);
	    		objLine["amount"] = parseFloat(arrBatch[i].amount);
	    		
	    		arrLines.push(objLine);
	    	}
	    	var arrCustomersIDs = _.compact(_.uniq(_.pluck(arrLines, 'customerid')));
	    	
	        //var file = nlapiCreateFile('usage_test_found_sos.json', 'PLAINTEXT', JSON.stringify(arrSOsFound));
	    	//file.setFolder(2627008);
			//var fileQueid = nlapiSubmitFile(file);
	    	
	    	var arrCustomers = new Array();
	    	for ( var c = 0; arrCustomersIDs != null && c < arrCustomersIDs.length; c++ ) {
	    		var objCustomer = new Object();
	
	    		var objCustNAC = _.find(arrLines, function(arr){ return (arr.customerid == arrCustomersIDs[c]) ; });
	    		
				objCustomer["node"] = objCustNAC.customer;
				objCustomer["nodetype"] = 'customer';
				objCustomer["naccustomerid"] = arrCustomersIDs[c];
				objCustomer["naccustomer"] = objCustNAC.customer;
				
				objCustomer["customerid"] = objCustNAC.customerid;
				objCustomer["customer"] = objCustNAC.customer;
				objCustomer["subsidiaryid"] = objCustNAC.subsidiaryid;
				objCustomer["subsidiary"] = objCustNAC.subsidiary;
				
				objCustomer["processed"] = 0;
				objCustomer["invoices"] = 0;
				
				var arrCustomerInvoices = _.filter(arrLines, function(arr){
	    			return (arr.customerid == arrCustomersIDs[c]);
		    	});
	    		var arrInvoices = new Array();
	    		for ( var i = 0; arrCustomerInvoices != null && i < arrCustomerInvoices.length; i++ ) {
	        		var objInvoice = new Object();
	        		
	        		objInvoice["node"] = arrCustomerInvoices[i].item;
	        		objInvoice["nodetype"] = 'invoice';
					
	        		objInvoice["serviceid"] = arrCustomerInvoices[i].serviceid;
	        		objInvoice["service_instance_id"] = arrCustomerInvoices[i].service_instance_id;
	        		objInvoice["sub_object_id"] = arrCustomerInvoices[i].sub_object_id;
	        		objInvoice["lineid"] = arrCustomerInvoices[i].lineid;
	        		objInvoice["legacyid"] = arrCustomerInvoices[i].legacyid;
	        		objInvoice["soid"] = arrCustomerInvoices[i].soid;
	        		objInvoice["partnerid"] = arrCustomerInvoices[i].partnerid;
	        		objInvoice["iaddressee"] = arrCustomerInvoices[i].iaddressee;
	        		
	        		objInvoice["invoiceid"] = 0;
	        		objInvoice["invoice"] = '';
	        		
	                objInvoice["start"] = arrCustomerInvoices[i].start;
	        		objInvoice["end"] = arrCustomerInvoices[i].end;
	            	
	            	objInvoice["itemid"] = arrCustomerInvoices[i].itemid;
	        		objInvoice["item"] = arrCustomerInvoices[i].item;
	        		
	            	objInvoice["classid"] = arrCustomerInvoices[i].classid;
	        		objInvoice["classname"] = arrCustomerInvoices[i].classname;
	        		objInvoice["inactive"] = arrCustomerInvoices[i].inactive;
	        		
	        		objInvoice["description"] = arrCustomerInvoices[i].description;
	    			objInvoice["clocationid"] = 29;
	    			objInvoice["clocation"] = 'NNJ';
	    			objInvoice["locationid"] = arrCustomerInvoices[i].locationid;
	    			objInvoice["location"] = arrCustomerInvoices[i].location;
	    			
	    			objInvoice["quantity"] = arrCustomerInvoices[i].quantity;
	        		objInvoice["rate"] = arrCustomerInvoices[i].rate;
	        		objInvoice["amount"] = arrCustomerInvoices[i].amount;
	        		
	        		objInvoice["processed"] = 0;
	        		
	        		//arrInvoices["expanded"] = false;
	        		objInvoice["faicon"] = 'fa fa-list-ul Silver1';
	        		objInvoice["leaf"] = true;
	        		
	        		arrInvoices.push(objInvoice);
	    		}
	    		objCustomer["children"] = arrInvoices;
	      		objCustomer["expanded"] = false;
	      		objCustomer["faicon"] = 'fa fa-user SteelBlue1';
	      		objCustomer["leaf"] = false;
	    		arrCustomers.push(objCustomer);
	    	}
	    	arrCustomers = _.sortBy(arrCustomers, function(obj){ return obj.node;});
	    	var arrCustomers = _.filter(arrCustomers, function(arr){
	    		return (arr.subsidiaryid == 5);
	    	});

	    	var recBatch = nlapiCreateRecord('customrecord_clgx_queue_json');
	    	recBatch.setFieldValue('custrecord_clgx_queue_json_type', 5);
	    	recBatch.setFieldValue('custrecord_clgx_queue_json_processed', 'F');
	    	recBatch.setFieldValue('custrecord_clgx_queue_json_date', today);
			var batchid = nlapiSubmitRecord(recBatch, false,true);
			
	    	var objBatch = new Object();
	    	objBatch["node"] = batchid.toString();
	    	objBatch["nodetype"] = "batch";
	    	objBatch["batchid"] = batchid;
	    	objBatch["processed"] = "F";
	    	objBatch["date"] = today;
	    	objBatch["children"] = arrCustomers;
	    	objBatch["expanded"] = false;
	    	objBatch["faicon"] = "fa fa-th SteelBlue2";
	    	objBatch["leaf"] = false;
	    	
	    	var folder = clgx_inv_get_folder(2,year,month,1);
	    	
	    	var fileName = 'usage_nac_' + today + '.json';
	    	var file = nlapiCreateFile(fileName, 'PLAINTEXT', JSON.stringify(objBatch));
	    	file.setFolder(folder);
			var fileid = nlapiSubmitFile(file);
			
			nlapiSubmitField('customrecord_clgx_queue_json', batchid, 'custrecord_clgx_queue_json_fileid',fileid);
			
			//nlapiScheduleScript('customscript_clgx_ss_inv_usg_nac_btch_pr', 'customdeploy_clgx_ss_inv_usg_nac_btch_pr');   
			//nlapiScheduleScript('customscript_clgx_ss_inv_usg_nac_btch_vd', 'customdeploy_clgx_ss_inv_usg_nac_btch_vd');   
    	}
	}
// --------------------------------------------------------------------------

    catch (error) { 
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
