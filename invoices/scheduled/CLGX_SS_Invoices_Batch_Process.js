nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Invoices_Batch_Process.js
//	Script Name:	CLGX_SS_Invoices_Batch_Process
//	Script Id:		customscript_clgx_ss_inv_batch_process
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/13/2015
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_inv_batch_process (request, response){
    try {
    	
    	var context = nlapiGetContext();
    	var fileid = context.getSetting('SCRIPT','custscript_clgx_queue_json_fileid');
    	var batchid = context.getSetting('SCRIPT','custscript_clgx_queue_json_batchid');
    	nlapiLogExecution('DEBUG','fileid', fileid);
       nlapiLogExecution('DEBUG','batchid', batchid);
    	try {
        	
        	var objFile = nlapiLoadFile(fileid);
        	var folder = objFile.getFolder();
        	var filename = objFile.getName();
        	var objBatch = JSON.parse(objFile.getValue());

        	var location = nlapiLookupField('customrecord_clgx_consolidate_locations', objBatch.locationid, 'name');
        	var start = moment(objBatch.cmonth + '/1/' + objBatch.cyear).format('M/D/YYYY');
	    	var end = moment(objBatch.cmonth + '/1/' + objBatch.cyear).endOf('month').format('M/D/YYYY');
	    	
	    	var objBatchNew = process_batch (objBatch,location,start,end,fileid);
	    	
	    	var fileQue = nlapiCreateFile(filename, 'PLAINTEXT', JSON.stringify(objBatchNew));
			fileQue.setFolder(folder);
			var fileQueID = nlapiSubmitFile(fileQue);
	    	
			if((objBatchNew.queue).length > 0){
				var arrParam = new Array();
		        arrParam['custscript_clgx_queue_json_fileid'] = fileid;
		        arrParam['custscript_clgx_queue_json_batchid'] = batchid;
		        nlapiScheduleScript('customscript_clgx_ss_inv_batch_process', 'customdeploy_clgx_ss_inv_batch_process' ,arrParam);  
			}
			else{
				nlapiSubmitField('customrecord_clgx_queue_json', batchid, 'custrecord_clgx_queue_json_processed','T');
				nlapiScheduleScript('customscript_clgx_ss_inv_queue_process', 'customdeploy_clgx_ss_inv_queue_process1');  
				nlapiScheduleScript('customscript_clgx_ss_inv_queue_process', 'customdeploy_clgx_ss_inv_queue_process2');
			}
        }
        catch (e) {
            var str = String(e);
            if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
                var emailAdminSubject = 'Error loading  batch ' + batchid + ' / file ' + fileid + ' / ' + e.getCode() + ': ' + e.getDetails();
                var emailAdminBody = '';
            	nlapiSendEmail(71418,71418,emailAdminSubject,emailAdminBody,null,null,null,null,true);
            }
        }
    }
    
// Start Catch Errors Section --------------------------------------------------------------------------

    catch (error) { 
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function process_batch (objBatch,location,start,end,fileid){
	
	// build new queue file  ----------------------------------------------------------------------------------------------
		
		var objBatchNew = new Object();
		objBatchNew["batchid"] = objBatch.batchid;
		objBatchNew["userid"] = objBatch.userid;
		objBatchNew["type"] = objBatch.type;
		objBatchNew["locationid"] = objBatch.locationid;
		objBatchNew["customerid"] = objBatch.customerid;
		objBatchNew["outputid"] = objBatch.outputid;
		objBatchNew["cmonth"] = objBatch.cmonth;
		objBatchNew["cyear"] = objBatch.cyear;
		objBatchNew["dmonth"] = objBatch.dmonth;
		objBatchNew["dyear"] = objBatch.dyear;
		objBatchNew["fileid"] = fileid;
		objBatchNew["ids"] = objBatch.ids;
		objBatchNew["ciids"] = objBatch.ciids;
		
		if(objBatch.type == 2){ // process only one customer
			var customerid = objBatch.customerid;
			var arrInvs = objBatch.queue; // ids are invoices
			objBatchNew["queue"] = []; // all invoices processed, empty array
		}
		else{
			var customerid = objBatch.queue[0]; // queue are customers
			var arrInvs = []; // include all invoices
			
			var arrCustomersNew = new Array();
			arrCustomersNew = _.reject(objBatch.queue, function(num){
		        return (num == objBatch.queue[0]);
			});
	    	objBatchNew["queue"] = arrCustomersNew; // only one customer at a time, exclude first customer from array
		}

		
	// unique consolidated invoices ----------------------------------------------------------------------------------------------
		
		var caddressee = nlapiLookupField('customer', customerid, 'billaddressee');

		var arrColumns = new Array();
		if(caddressee == '' || caddressee == null){
			arrColumns.push(new nlobjSearchColumn('billaddressee',null,'GROUP'));
		}
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter('trandate',null,'within', start,end));
		arrFilters.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', objBatch.locationid));
		arrFilters.push(new nlobjSearchFilter('entity',null,'anyof', customerid));
		if(arrInvs.length > 0){
			arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof', arrInvs));
		}
		if(caddressee == '' || caddressee == null){
			arrFilters.push(new nlobjSearchFilter('billaddressee',null,'isnot',''));
		}
		var searchCInvoices = nlapiSearchRecord('invoice', 'customsearch_clgx_inv_invoices_uniques', arrFilters, arrColumns);
		
		
		var arrCInvoices = new Array();
		for ( var i = 0; searchCInvoices != null && i < searchCInvoices.length; i++ ) {
			
			var currency = searchCInvoices[i].getText('currency',null,'GROUP');
			if(caddressee == '' || caddressee == null){
				var iaddressee = searchCInvoices[i].getValue('billaddressee',null,'GROUP');
			}
			else{
				var iaddressee = caddressee;
			}
			var addressid = get_addressid (customerid,iaddressee);
			var invoiceNbr = (objBatch.batchid).toString() + '.' + addressid.toString();
			
	// invoices on consolidated invoice ----------------------------------------------------------------------------------------------
			
			var arrInvoices = new Array();
			
			var arrColumns = new Array();
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter('trandate',null,'within', start,end));
			arrFilters.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', objBatch.locationid));
			arrFilters.push(new nlobjSearchFilter('entity',null,'anyof', customerid));
			if(arrInvs.length > 0){
				arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof', arrInvs));
			}
			if(caddressee == '' || caddressee == null){
				arrFilters.push(new nlobjSearchFilter('billaddressee',null,'is', iaddressee));
			}
			var searchInvoices = nlapiSearchRecord('invoice', 'customsearch_clgx_inv_invoices', arrFilters, arrColumns);
			
			for ( var j = 0; searchInvoices != null && j < searchInvoices.length; j++ ) {
				arrInvoices.push(searchInvoices[j].getValue('internalid',null,'GROUP'));
			}
			
			var arrColumns = new Array();
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter('trandate',null,'within', start,end));
			arrFilters.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', objBatch.locationid));
			arrFilters.push(new nlobjSearchFilter('entity',null,'anyof', customerid));
			if(arrInvs.length > 0){
				arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof', arrInvs));
			}
			if(caddressee == '' || caddressee == null){
				arrFilters.push(new nlobjSearchFilter('billaddressee',null,'is', iaddressee));
			}
			var searchInvoices = nlapiSearchRecord('invoice', 'customsearch_clgx_inv_invoices_2', arrFilters, arrColumns);
			
			for ( var j = 0; searchInvoices != null && j < searchInvoices.length; j++ ) {
				arrInvoices.push(searchInvoices[j].getValue('internalid',null,'GROUP'));
			}
			
	// billing contacts ----------------------------------------------------------------------------------------------
			
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid','contact',null));
			arrColumns.push(new nlobjSearchColumn('entityid', 'contact',null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter('internalid',null,'is',customerid));
			arrFilters.push(new nlobjSearchFilter('contactrole','contact','is',1));
			arrFilters.push(new nlobjSearchFilter('email','contact','isnotempty'));
			arrFilters.push(new nlobjSearchFilter('isinactive','contact','is','F'));
			var searchContacts = nlapiSearchRecord('customer', null, arrFilters, arrColumns);
			
			var arrContacts = new Array();
			for ( var c = 0; searchContacts != null && c < searchContacts.length; c++ ) {
				arrContacts.push(searchContacts[c].getValue('internalid','contact',null));
			}
			
			
	// create cinvoice record ----------------------------------------------------------------------------------------------
			
			var scriptqueue = getRandom(4, 5);
			
	        var recCI = nlapiCreateRecord('customrecord_clgx_consolidated_invoices');

	        recCI.setFieldValue('custrecord_clgx_consol_inv_customer', customerid);
	        recCI.setFieldValue('custrecord_clgx_consol_inv_date', start);
	        recCI.setFieldValue('custrecord_clgx_consol_inv_batch', invoiceNbr);
	        recCI.setFieldValue('custrecord_clgx_consol_inv_invs', arrInvoices);
	    	recCI.setFieldValue('custrecord_clgx_consol_inv_invoices', arrInvoices.join(';'));
	        recCI.setFieldValue('custrecord_clgx_consol_inv_month', objBatch.cmonth);
	        recCI.setFieldValue('custrecord_clgx_consol_inv_year', (objBatch.cyear).toFixed(0));
	        recCI.setFieldValue('custrecord_clgx_consol_inv_month_display', objBatch.dmonth);
	        recCI.setFieldValue('custrecord_clgx_consol_inv_year_display', (objBatch.dyear).toFixed(0));
	        recCI.setFieldValue('custrecord_clgx_consol_inv_output', objBatch.outputid);
	        recCI.setFieldValue('custrecord_clgx_consol_inv_contacts', arrContacts);
	        recCI.setFieldValue('custrecord_clgx_consol_inv_emailed_to', arrContacts.join(';'));
	        recCI.setFieldValue('custrecord_clgx_consol_inv_currency', clgx_inv_get_currency (currency));
	        recCI.setFieldValue('custrecord_clgx_consol_inv_cc_enabled', 'F');
	        recCI.setFieldValue('custrecord_clgx_consol_inv_market', location);
	        recCI.setFieldValue('custrecord_clgx_consol_inv_clocation', objBatch.locationid);
	        recCI.setFieldValue('custrecord_clgx_consol_inv_churn', 'T');
	        recCI.setFieldValue('custrecord_clgx_consol_inv_processed', 'F');
	        recCI.setFieldValue('custrecord_clgx_consol_inv_user', objBatch.userid);
	        recCI.setFieldValue('custrecord_clgx_consol_inv_queue', scriptqueue);
	        
	        var ciid = nlapiSubmitRecord(recCI, true);
	        
	        (objBatchNew.ciids).push(parseInt(ciid));
		}
		return objBatchNew;
}


function get_addressid (customerid,iaddressee){

	var addressid = 0;
	
	var recCustomer = nlapiLoadRecord('customer', customerid);
	var caddressee = recCustomer.getFieldValue('billaddressee');
	var nbrAddresses = recCustomer.getLineItemCount('addressbook');
	
	if(caddressee == '' || caddressee == null){
		
		var countAddress = 0;
		for (var a = 0; a < nbrAddresses; a++) {
			var aaddressee = recCustomer.getLineItemValue('addressbook', 'addressee', a + 1);
			if (aaddressee == iaddressee){
				addressid = recCustomer.getLineItemValue('addressbook', 'id', a + 1);
				countAddress = 1;
			}
		}
		if(countAddress == 0){ // no address was found or is differet
			var emailSubjectAddressee = 'ALERT - Customer - ' + customerid + ' - invoice(s) without an address ( different addressee on invoice then on customer record.)';
			var emailBodyAddressee = '';
			
			clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_ci_batch_address", emailSubjectAddressee, emailBodyAddressee);
			
			//nlapiSendEmail(432742, 12073,emailSubjectAddressee,emailBodyAddressee,null,null,null,null); // Kristen
			//nlapiSendEmail(432742, 689258,emailSubjectAddressee,emailBodyAddressee,null,null,null,null); // Wanny
            /*nlapiSendEmail(432742, 713181,emailSubjectAddressee,emailBodyAddressee,null,null,null,null); // Chad
			nlapiSendEmail(432742, 336456,emailSubjectAddressee,emailBodyAddressee,null,null,null,null); // Glenda
            nlapiSendEmail(432742, 2406,emailSubjectAddressee,emailBodyAddressee,null,null,null,null); // Lisa
            nlapiSendEmail(432742, 71418,emailSubjectAddressee,emailBodyAddressee,null,null,null,null);  // Dan
            nlapiSendEmail(432742, 1349020,emailSubjectAddressee,emailBodyAddressee,null,null,null,null);*/  // Alex
		}
	}
	else{
		// loop the addresses of the customer and look the default billing address
		for (var a = 0; a < nbrAddresses; a++) {
			var isDefaultBilling = recCustomer.getLineItemValue('addressbook', 'defaultbilling', a + 1);
			if (isDefaultBilling == 'T'){
				addressid = parseInt(recCustomer.getLineItemValue('addressbook', 'id', a + 1));
			}
		}
	}
	
	return addressid;
}

function getRandom(min, max) {
    return min + Math.floor(Math.random() * (max - min + 1));
}
