nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Invoices_Usage_NAC_Batch_Process.js
//	Script Name:	CLGX_SS_Invoices_Usage_NAC_Batch_Process
//	Script Id:		customscript_clgx_ss_inv_usg_nac_btch_pr
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/13/2015
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_inv_usg_nac_btch_pr (){
    try {
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
    	
	    	var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_queue_json_fileid',null,null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_queue_json_type",null,"anyof",5));
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_queue_json_processed",null,"is",'F'));
			var searchBatch = nlapiSearchRecord('customrecord_clgx_queue_json', null, arrFilters, arrColumns);
			
			if(searchBatch != null){
				
				var startdate = moment().startOf('month').add('months', 1).format('M/D/YYYY');
				
				var batchid = parseInt(searchBatch[0].getValue('internalid',null,null));
				var fileid = parseInt(searchBatch[0].getValue('custrecord_clgx_queue_json_fileid',null,null));
				
				var objFile = nlapiLoadFile(fileid);
				var fileName = objFile.getName();
				var fileFolder = objFile.getFolder();
				
				var objBatch = JSON.parse(objFile.getValue());
		    	
				for ( var c = 0; objBatch.children != null && c < objBatch.children.length; c++ ) {
					
		    		var customerid = objBatch.children[c].customerid;
					var naccustomerid = objBatch.children[c].naccustomerid;
					var invoices = objBatch.children[c].invoices;
					
					var reschedule = 0;
					
					if (customerid > 0 && invoices == 0){
						
						var hasinactive = 0;
						
						for ( var i = 0; objBatch.children[c].children != null && i < objBatch.children[c].children.length; i++ ) {
							
							var inactive = objBatch.children[c].children[i].inactive;
							var locationid = objBatch.children[c].children[i].locationid;
							
							if(inactive == 'F' && locationid != '' && locationid != null){
								var recInvoice = nlapiCreateRecord('invoice');
				    			recInvoice.setFieldValue('entity', customerid);
				    			//recInvoice.setFieldValue('externalid', objBatch.children[c].children[i].lineid);
				    			recInvoice.setFieldValue('subsidiary', 5);
				    			recInvoice.setFieldValue('custbody_clgx_consolidate_locations', objBatch.children[c].children[i].clocationid);
				    			recInvoice.setFieldValue('trandate', startdate);
				    			recInvoice.setFieldValue('startdate', startdate);
				    			recInvoice.setFieldValue('location', objBatch.children[c].children[i].locationid);
				    			
				    			if(objBatch.children[c].children[i].soid > 0){
				    				recInvoice.setFieldValue('custbody_cologix_legacy_so', ((objBatch.children[c].children[i].soid).toFixed(0)).toString());
				    			}
				    			
				    			if(objBatch.children[c].children[i].partnerid > 0){
				    				recInvoice.setFieldValue('partner', objBatch.children[c].children[i].partnerid);
				    			}
				    			
				    			if(objBatch.children[c].children[i].iaddressee == ''){
						    		var arrColumns = new Array();
									var arrFilters = new Array();
									arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
									arrFilters.push(new nlobjSearchFilter("custbody_cologix_legacy_so",null,"is",objBatch.children[c].children[i].legacyid));
									var searchSOs = nlapiSearchRecord('transaction', 'customsearch_clgx_inv_legacy_so_ids_all', arrFilters, arrColumns);
									if(searchSOs){
						    			recInvoice.setFieldValue('billaddressee', searchSOs[0].getValue('billaddressee',null,'GROUP') || '');
									}
				    			} else {
				    				recInvoice.setFieldValue('billaddressee', objBatch.children[c].children[i].iaddressee);
				    			}
				    		
				    			recInvoice.selectNewLineItem('item');
			    				recInvoice.setCurrentLineItemValue('item','item', objBatch.children[c].children[i].itemid);
			    				recInvoice.setCurrentLineItemValue('item','location',objBatch.children[c].children[i].locationid);
			    				recInvoice.setCurrentLineItemValue('item','quantity', objBatch.children[c].children[i].quantity);
			    				recInvoice.setCurrentLineItemValue('item','rate', objBatch.children[c].children[i].rate);
			    				recInvoice.setCurrentLineItemValue('item','amount', objBatch.children[c].children[i].amount);
			    				recInvoice.setCurrentLineItemValue('item','class', objBatch.children[c].children[i].classid);
			    				recInvoice.setCurrentLineItemValue('item','description', objBatch.children[c].children[i].description);
			    				recInvoice.commitLineItem('item');
			    				
			    				var recid = nlapiSubmitRecord(recInvoice, false,true);
			    				var lineid = objBatch.children[c].children[i].lineid;
			    				
			    				// mark the line as billed
			    		    	nlapiRequestURL(
			    		    			  'https://nsapi1.dev.nac.net/v1.0/netsuite/usage/',
			    		    			  '{"lines":{"' + lineid + '": ' + recid + '}}',
			    		    			  {
			    		    			    'Content-type': 'application/json',
			    		    			    'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
			    		    			  },
			    		    			  null, 'POST');
			    				
			    				objBatch.children[c].children[i].invoiceid = parseInt(recid);
							}
							else{
								objBatch.children[c].children[i].invoiceid = -1;
							}

		    				var usage  = 10000 - nlapiGetContext().getRemainingUsage();
		    				nlapiLogExecution('DEBUG','Process', '| c = ' + c + ' | i = ' + i + '| Invoice = ' + recid + ' | Usage = ' + usage + ' |');
		    	            
						}
						objBatch.children[c].invoices = 1;
					}
					
    				if(usage > 7000){
    	            	
    	            	reschedule = 1;
						
    			    	var file = nlapiCreateFile(fileName, 'PLAINTEXT', JSON.stringify(objBatch));
    			    	file.setFolder(fileFolder);
    					var fileid = nlapiSubmitFile(file);
    					
    	            	var status = nlapiScheduleScript(nlapiGetContext().getScriptId(), nlapiGetContext().getDeploymentId());
    	                if(status == 'QUEUED'){
    	                    nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
    	    			    break;
    	                }
    	            }
					
				}
		    	
				if(reschedule == 0){
					
					var file = nlapiCreateFile(fileName, 'PLAINTEXT', JSON.stringify(objBatch));
			    	file.setFolder(fileFolder);
					var fileid = nlapiSubmitFile(file);
					
					nlapiSubmitField('customrecord_clgx_queue_json', batchid, ['custrecord_clgx_queue_json_processed'], ['T']);
					
					
			    	
					var requestURL = nlapiRequestURL(
			    			  'https://nsapi1.dev.nac.net/v1.0/netsuite/usage/?per_page=0',
			    			  null,
			    			  {
			    			    'Content-type': 'application/json',
			    			    'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
			    			  },
			    			  null,
			    			  'GET');
			      	var objRequest = JSON.parse( requestURL.body );
				  	
			      	clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_nac_usage", "Stop NAC Usage - " + objRequest.total, "");
			      	
			      	//nlapiSendEmail(71418,71418,'Stop NAC Usage - ' + objRequest.total ,'',null,null,null,null);

				}

			}
			else{
				nlapiLogExecution('DEBUG','Finish', 'No batch to process');
				nlapiScheduleScript('customscript_clgx_ss_inv_usg_nac_btch_vd', 'customdeploy_clgx_ss_inv_usg_nac_btch_vd');   
				// email no batch
			}
        	
        	
        	//nlapiLogExecution('DEBUG','Usage', 10000 - nlapiGetContext().getRemainingUsage());
        	
        }
    
// --------------------------------------------------------------------------

    catch (error) { 
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
