nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CI_Usage_Batch_Create.js
//	Script Name:	CLGX_SS_CI_Usage_Batch_Create
//	Script Id:		customscript_clgx_ss_ci_usgage_btch_cr
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/13/2015
//-------------------------------------------------------------------------------------------------

function scheduled_ci_usage_btch_cr (request, response){
    try {
    	
    	var context = nlapiGetContext();
    	var environment = context.getEnvironment();
    	var userid = nlapiGetUser();
    	
    	if(environment == 'PRODUCTION'){
	    		
	    	var today = moment().format('MM/D/YYYY');
	    	var year = moment().format('YYYY');
	    	var month = moment().format('M');
	    	
	    	var end = moment(month + '/20/' + year).format('MM/D/YYYY');
	    	var start = moment(end).subtract(1, 'months').add(1, 'day').format('MM/D/YYYY');
	    	
	    	var arrLines = new Array();

// ========================================================== No Commit - one invoice by SO ==========================================================
	    	
	    	var arrColumns = new Array();
			var arrFilters = new Array();
			var searchSOs = nlapiSearchRecord('salesorder', 'customsearch_clgx_inv_usg_kwh_no_commit', arrFilters, arrColumns);
	    	for ( var i = 0; searchSOs != null && i < searchSOs.length; i++ ) {
				var objLine = new Object();
				objLine["invoiceid"] = 0;
				objLine["invoice"] = '';
				objLine["soid"] = parseInt(searchSOs[i].getValue('internalid',null,'GROUP'));
				objLine["subsidiaryid"] = parseInt(searchSOs[i].getValue('subsidiary',null,'GROUP'));
				objLine["customerid"] = parseInt(searchSOs[i].getValue('entity',null,'GROUP'));
				objLine["customer"] = searchSOs[i].getText('entity',null,'GROUP');
				objLine["currencyid"] = parseInt(searchSOs[i].getValue('currency',null,'GROUP'));
				objLine["currency"] = searchSOs[i].getText('currency',null,'GROUP');
				objLine["clocationid"] = parseInt(searchSOs[i].getValue('custbody_clgx_consolidate_locations',null,'GROUP'));
				objLine["clocation"] = searchSOs[i].getText('custbody_clgx_consolidate_locations',null,'GROUP');
				objLine["iaddressee"] = searchSOs[i].getValue('billaddressee',null,'GROUP');
				objLine["utilrate"] = parseFloat(searchSOs[i].getValue('custbody_clgx_pwusg_util_rate',null,'GROUP'));
				objLine["commitkwh"] = parseFloat(searchSOs[i].getValue('custbody_clgx_pwusg_commit_util_kwh',null,'GROUP'));
				objLine["commitrate"] = parseFloat(searchSOs[i].getValue('custbody_clgx_pwusg_commit_util_rate',null,'GROUP'));
				objLine["locationid"] = parseInt(searchSOs[i].getValue('location',null,'GROUP'));
				objLine["location"] = searchSOs[i].getValue('locationnohierarchy',null,'GROUP');
				
	    		var arrColumns = new Array();
	        	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_kw_adj',null,'SUM'));
	    		var arrFilters = new Array();
	        	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_points_day_date',null,'within', start,end));
	        	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_points_day_so',null,'anyof', objLine.soid));
	        	var searchUsage = nlapiSearchRecord('customrecord_clgx_dcim_points_day', null, arrFilters, arrColumns);
	
	        	var kwhadj = 0;
	        	if(searchUsage){
	        		kwhadj = parseFloat(searchUsage[0].getValue('custrecord_clgx_dcim_points_day_kw_adj',null,'SUM'));
	        	}
	        	objLine["kwhadj"] = kwhadj;
	        	
				var outage = 0;
	    		if (objLine.commitkwh > 0 && objLine.commitrate > 0){
	    			outage = kwhadj - objLine.commitkwh;
	    			if (outage > 0){
	        			var rate = objLine.commitrate;
	        			var quantity = outage;
	        			var amount = (outage * objLine.commitrate);
	    			}
	    			else{
	        			var rate = objLine.utilrate;
	        			var quantity = 0;
	        			var amount = 0;
	    			}
	    		}
	    		else{
	    			var rate = objLine.utilrate;
	    			var quantity = kwhadj;
	    			var amount = (objLine.utilrate * kwhadj);
	    		}
				
	    		objLine["rate"] = rate;
	    		objLine["quantity"] = quantity;
	    		objLine["amount"] = amount;
	    		
	    		objLine["itemid"] = 210;
	    		objLine["item"] = 'Power Usage';
	    		objLine["classid"] = 4;
	    		objLine["classname"] = 'Recurring : Recurring - Power';
	    		objLine["description"] = 'From ' + start + ' to ' + end;
				
				if(objLine.amount > 0){
					arrLines.push(objLine);
				}
	    	}

// ========================================================== Commit - one invoice by customer ==========================================================
	    	
	    	var arrColumns = new Array();
			var arrFilters = new Array();
			var searchSOs = nlapiSearchRecord('salesorder', 'customsearch_clgx_inv_usg_kwh_commit', arrFilters, arrColumns);
	    	for ( var i = 0; searchSOs != null && i < searchSOs.length; i++ ) {
				var objLine = new Object();
				objLine["invoiceid"] = 0;
				objLine["invoice"] = '';
				objLine["soid"] = 0;
				objLine["subsidiaryid"] = parseInt(searchSOs[i].getValue('subsidiary',null,'GROUP'));
				objLine["customerid"] = parseInt(searchSOs[i].getValue('entity',null,'GROUP'));
				objLine["customer"] = searchSOs[i].getText('entity',null,'GROUP');
				objLine["currencyid"] = parseInt(searchSOs[i].getValue('currency',null,'GROUP'));
				objLine["currency"] = searchSOs[i].getText('currency',null,'GROUP');
				objLine["clocationid"] = parseInt(searchSOs[i].getValue('custbody_clgx_consolidate_locations',null,'GROUP'));
				objLine["clocation"] = searchSOs[i].getText('custbody_clgx_consolidate_locations',null,'GROUP');
				objLine["iaddressee"] = '';
				objLine["utilrate"] = parseFloat(searchSOs[i].getValue('custbody_clgx_pwusg_util_rate',null,'GROUP'));
				objLine["commitkwh"] = parseFloat(searchSOs[i].getValue('custbody_clgx_pwusg_commit_util_kwh',null,'GROUP'));
				objLine["commitrate"] = parseFloat(searchSOs[i].getValue('custbody_clgx_pwusg_commit_util_rate',null,'GROUP'));
				objLine["locationid"] = parseInt(searchSOs[i].getValue('location',null,'GROUP'));
				objLine["location"] = searchSOs[i].getValue('locationnohierarchy',null,'GROUP');
				
				var arrColumns = new Array();
				var arrFilters = new Array();
				arrFilters.push(new nlobjSearchFilter("subsidiary",null,"anyof",objLine.subsidiaryid));
				arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",objLine.customerid));
				arrFilters.push(new nlobjSearchFilter("currency",null,"anyof",objLine.currencyid));
				arrFilters.push(new nlobjSearchFilter("custbody_clgx_consolidate_locations",null,"anyof",objLine.clocationid));
				arrFilters.push(new nlobjSearchFilter("location",null,"anyof",objLine.locationid));
				arrFilters.push(new nlobjSearchFilter("custbody_clgx_pwusg_util_rate",null,"is",objLine.utilrate));
				arrFilters.push(new nlobjSearchFilter("custbody_clgx_pwusg_commit_util_kwh",null,"is",objLine.commitkwh));
				arrFilters.push(new nlobjSearchFilter("custbody_clgx_pwusg_commit_util_rate",null,"is",objLine.commitrate));
				var searchSOIDs = nlapiSearchRecord('salesorder', 'customsearch_clgx_inv_usg_kwh_commit_sos', arrFilters, arrColumns);
		    	var arrSOIDs = new Array();
		    	for ( var j = 0; searchSOIDs != null && j < searchSOIDs.length; j++ ) {
		    		arrSOIDs.push(parseInt(searchSOIDs[j].getValue('internalid',null,'GROUP')));
		    	}
		    	objLine["sos"] = arrSOIDs;
		    	
	    		var arrColumns = new Array();
	        	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_kw_adj',null,'SUM'));
	    		var arrFilters = new Array();
	        	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_points_day_date',null,'within', start,end));
	        	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_points_day_so',null,'anyof', arrSOIDs));
	        	var searchUsage = nlapiSearchRecord('customrecord_clgx_dcim_points_day', null, arrFilters, arrColumns);
	
	        	var kwhadj = 0;
	        	if(searchUsage){
	        		kwhadj = parseFloat(searchUsage[0].getValue('custrecord_clgx_dcim_points_day_kw_adj',null,'SUM'));
	        	}
	        	objLine["kwhadj"] = kwhadj;
	        	
				var outage = 0;
	    		if (objLine.commitkwh > 0 && objLine.commitrate > 0){
	    			outage = kwhadj - objLine.commitkwh;
	    			if (outage > 0){
	        			var rate = objLine.commitrate;
	        			var quantity = outage;
	        			var amount = (outage * objLine.commitrate);
	    			}
	    			else{
	        			var rate = objLine.utilrate;
	        			var quantity = 0;
	        			var amount = 0;
	    			}
	    		}
	    		else{
	    			var rate = objLine.utilrate;
	    			var quantity = kwhadj;
	    			var amount = (objLine.utilrate * kwhadj);
	    		}
				
	    		objLine["rate"] = rate;
	    		objLine["quantity"] = quantity;
	    		objLine["amount"] = amount;
	    		
	    		objLine["itemid"] = 210;
	    		objLine["item"] = 'Power Usage';
	    		objLine["classid"] = 4;
	    		objLine["classname"] = 'Recurring : Recurring - Power';
	    		objLine["description"] = 'From ' + start + ' to ' + end;
				
				if(objLine.amount > 0){
					arrLines.push(objLine);
				}
	    	}

// ========================================================== Create invoices batch ==========================================================
	    	
	    	var recBatch = nlapiCreateRecord('customrecord_clgx_queue_json');
	    	recBatch.setFieldValue('custrecord_clgx_queue_json_type', 4);
	    	recBatch.setFieldValue('custrecord_clgx_queue_json_processed', 'F');
	    	recBatch.setFieldValue('custrecord_clgx_queue_json_date', today);
			var batchid = nlapiSubmitRecord(recBatch, false,true);
	    	
			var objBatch = new Object();
			objBatch["batchid"] = batchid;
			objBatch["type"] = 4;
			objBatch["start"] = start;
			objBatch["end"] = end;
			objBatch["lines"] = arrLines;
			
			var folder = clgx_inv_get_folder(2,year,month,1);
			
	    	var fileName = 'usage_ns_' + today + '.json';
	    	var file = nlapiCreateFile(fileName, 'PLAINTEXT', JSON.stringify(objBatch));
	    	file.setFolder(folder);
			var fileid = nlapiSubmitFile(file);
			
			nlapiSubmitField('customrecord_clgx_queue_json', batchid, 'custrecord_clgx_queue_json_fileid',fileid);

	    	nlapiScheduleScript('customscript_clgx_ss_ci_usage_btch_pr', 'customdeploy_clgx_ss_ci_usage_btch_pr');   
	    	
			var usage  = 10000 - nlapiGetContext().getRemainingUsage();
			nlapiLogExecution('DEBUG','Process', '| Usage = ' + usage + ' |');
			
    	}
    }
// --------------------------------------------------------------------------

    catch (error) { 
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
