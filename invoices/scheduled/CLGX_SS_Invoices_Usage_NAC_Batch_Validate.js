//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Invoices_Usage_NAC_Batch_Validat.js
//	Script Name:	CLGX_SS_Invoices_Usage_NAC_Batch_Validat
//	Script Id:		customscript_clgx_ss_inv_usg_nac_btch_vd
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/27/2017
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_inv_usg_nac_btch_vd(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();

		var requestURL = nlapiRequestURL(
	  			  'https://nsapi1.dev.nac.net/v1.0/netsuite/usage/?per_page=0',
	  			  null,
	  			  {
	  			    'Content-type': 'application/json',
	  			    'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
	  			  },
	  			  null,
	  			  'GET');
	    	var objRequest = JSON.parse( requestURL.body );
		  	
	    	
	    for ( var i = 0; i < objRequest.data.length; i++ ) {
	    	
	    	var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            var index = i + 1;
        
    		try {
		    	nlapiRequestURL(
	    			  'https://nsapi1.dev.nac.net/v1.0/netsuite/usage/',
	    			  '{"lines":{"' + objRequest.data[i].id + '": ' + index + '}}',
	    			  {
	    			    'Content-type': 'application/json',
	    			    'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
	    			  },
	    			  null, 'POST');
		    	
		    	nlapiLogExecution('DEBUG','success ', ' | Index = ' + index + ' of ' + objRequest.data.length + ' | id - '+ objRequest.data[i].id + ' | Usage - '+ usageConsumtion + '  |');
				}
			catch (error) {

	            nlapiLogExecution('DEBUG','error ', ' | Index = ' + index + ' of ' + objRequest.data.length + ' | id - '+ objRequest.data[i].id + ' | Usage - '+ usageConsumtion + '  |');
			}
        }
        
        
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
