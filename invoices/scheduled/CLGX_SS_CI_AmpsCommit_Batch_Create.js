nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CI_AmpsCommit_Batch_Create.js
//	Script Name:	CLGX_SS_CI_AmpsCommit_Batch_Create
//	Script Id:		customscript_clgx_ss_ci_amps_com_btch_cr
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/27/2017
//-------------------------------------------------------------------------------------------------

function scheduled_ci_amps_com_btch_cr (request, response){
    try {
    	
    	var startScript = moment();
    	var context = nlapiGetContext();
    	var environment = context.getEnvironment();
    	var userid = nlapiGetUser();
    	
    	if(environment == 'PRODUCTION'){
    		
    		nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
            
	    	var today = moment().format('MM/D/YYYY');
	    	var year = moment().format('YYYY');
	    	var month = moment().format('M');
	    	var end = moment(month + '/20/' + year).format('MM/D/YYYY');
	    	var start = moment(end).subtract(1, 'months').add(1, 'day').format('MM/D/YYYY');
	    	//var end = moment('02/20/2017').format('MM/D/YYYY');
        	//var start = moment(end).subtract(1, 'months').add(1, 'day').format('MM/D/YYYY');
        	
        	var sos = [];
        	var search = nlapiLoadSearch('transaction', 'customsearch_clgx_ci_commited_amps_sos');
    		var result = search.runSearch();
    		result.forEachResult(function(row) {
    			sos.push({
    				"soid": parseInt(row.getValue('internalid',null,'GROUP')) || 0,
    				"so": row.getValue('transactionnumber',null,'GROUP') || 0,
    				"subsidiaryid": parseInt(row.getValue('subsidiary',null,'GROUP')) || 0,
    				"customerid": parseInt(row.getValue('mainname',null,'GROUP')) || 0,
    				"customer": row.getText('mainname',null,'GROUP') || '',
    				"currencyid": parseInt(row.getValue('currency',null,'GROUP')) || 0,
    				"addressee": row.getValue('billaddressee',null,'GROUP') || '',
    				"clocationid": parseInt(row.getValue('custbody_clgx_consolidate_locations',null,'GROUP')) || 0,
    				"clocation": row.getText('custbody_clgx_consolidate_locations',null,'GROUP') || '',					
    				"locationid": parseInt(row.getValue('location',null,'GROUP')) || 0,
    				"location": row.getText('location',null,'GROUP') || '',
    				"usagerate": round(parseFloat(row.getValue('custbody_clgx_pwusg_util_rate',null,'GROUP'))) || 0,
    				"commited": round(parseFloat(row.getValue('custbody_clgx_pwusg_commit_util_kwh',null,'GROUP'))) || 0,
    				"overagerate": round(parseFloat(row.getValue('custbody_clgx_pwusg_commit_util_rate',null,'GROUP'))) || 0
    			});
    			return true;
    		});
        	
        	var ids = _.compact(_.uniq(_.pluck(sos, 'soid')));

        	var columns = [];
        	var filters = [];
    		filters[0] = new nlobjSearchFilter("custrecord_power_circuit_service_order", null, "anyof", ids);
    		var powers = [];
        	while (true) {
        		search = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_ci_commited_amps_pwrs', filters, columns);
        		if (!search) {
        			break;
        		}
        		for (var i in search) {
        			
        			var powerid = parseInt(search[i].getValue('internalid',null,null)) || 0;
        			var power = search[i].getValue('name',null,null) || '';
        			var soid = parseInt(search[i].getValue('custrecord_power_circuit_service_order',null,null)) || 0;
        			var panel = search[i].getText('custrecord_clgx_dcim_device',null,null) || '';
        			
        			var poles = [];
    				var breakers = (search[i].getText('custrecord_clgx_dcim_points_current',null,null)).split(",");
    				for ( var j = 0; breakers != null && j < breakers.length; j++ ) {
    					//poles.push(parseInt(breakers[j].substr(breakers[j].length - 3), 10));
    					poles.push(parseInt(breakers[j].substr(breakers[j].length - 7, 3), 10));
    				}
    				var strPoles = poles.join('-');
    				var nbrPoles = breakers.length;
    				
        			var obj = _.find(sos, function(arr){ return (arr.soid == soid) ; });
    				if(obj){
    	    			powers.push({
    	    				"node": power,
    	    				"type": 'power',
    	    				"invoiceid": 0,
    	    				"invoice": '',
    	    				"powerid": powerid,
    	    				"power": power,
    	    				"soid": soid,
    	    				"so": search[i].getText('custrecord_power_circuit_service_order',null,null) || '',
    	    				"serviceid": parseInt(search[i].getValue('custrecord_cologix_power_service',null,null)) || 0,
    	    				"service": search[i].getText('custrecord_cologix_power_service',null,null) || '',
    	    				"spaceid": parseInt(search[i].getValue('custrecord_cologix_power_space',null,null)) || 0,
    	    				"space": search[i].getText('custrecord_cologix_power_space',null,null) || '',
    	    				"panelid": parseInt(search[i].getValue('custrecord_clgx_dcim_device',null,null)) || 0,
    	    				"panel": panel,
    	    				"amps": search[i].getText('custrecord_cologix_power_amps',null,null) || '',
    	    				"strPoles": strPoles,
    	    				"nbrPoles": nbrPoles,
    	    				
    	    				"subsidiaryid": obj.subsidiaryid,
    	    				"customerid": obj.customerid,
    	    				"customer": obj.customer,
    	    				"currencyid": obj.currencyid,
    	    				"addressee": obj.addressee,
    	    				"clocationid": obj.clocationid,
    	    				"clocation": obj.clocation,			
    	    				"locationid": obj.locationid,
    	    				"location": obj.location,
    	    				"usagerate": obj.usagerate,
    	    				"commited": obj.commited,
    	    				"overagerate": obj.overagerate,
    	    				
    	    				"rate": 0,
    	    				"quantity": 0,
    	    				"amount": 0,
    	    				"itemid": 210,
    	    				"item": 'Power Usage',
    	    				"description": '',
    	    				"classid": 4,
    	    				"classname": 'Recurring : Recurring - Power',
    	    				"faicon": "fa fa-plug",
    	    				"leaf": true,
    	    				"breakers": [],
    	    			});
        			}
        		}
        		if (search.length < 1000) {
        			break;
        		}
        		filters[1] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", powerid);
        	}

        	
        	var total = 0;
        	for ( var i = 0;  i < powers.length; i++ ) {
                
        		var filters = new Array();
        		filters.push(new nlobjSearchFilter('custrecord_clgx_dcim_points_day_power',null,'anyof', powers[i].powerid));
    	    	filters.push(new nlobjSearchFilter('custrecord_clgx_dcim_points_day_date',null,'within', start,end));
    	    	var search = nlapiSearchRecord('customrecord_clgx_dcim_points_day', 'customsearch_clgx_ci_commited_amps_avg', filters);
    	    	var breakers = [];
    	    	var quantity = 0;
    	    	var amount = 0;
    	    	var sum = 0;
    			for ( var j = 0; search != null && j < search.length; j++ ) {
    				
    	        	var breaker = parseInt(search[j].getValue('custrecord_clgx_dcim_points_day_breaker',null,'GROUP')) || 0;
    	        	var avg = round(parseFloat(search[j].getValue('custrecord_clgx_dcim_points_day_amps_avg',null,'AVG'))) || 0;
    	        	
    	        	sum += avg;
    	        	breakers.push({
    	        		"breaker": breaker,
    					"avg": avg
    				});
    			}
    			
    			if(search){
    				var avgs = _.pluck(breakers, 'avg');
    				var max = _.max(avgs);
    				if(max > powers[i].commited){
    					quantity += Math.ceil(max - powers[i].commited);
    					amount = powers[i].overagerate * quantity;
    					total += amount;
    				}
    				
    				powers[i].rate = powers[i].overagerate;
    				powers[i].quantity = quantity;
    				powers[i].amount = powers[i].overagerate * quantity;
    				powers[i].description = '(Overage) ' + powers[i].quantity + ' amperes @ $' + powers[i].rate + '; ' + start + ' to ' + end + ' : ' + powers[i].power + ' - ' + powers[i].panel + ' - ' + powers[i].strPoles + ' - (' + powers[i].nbrPoles + ' poles, ' +  powers[i].amps + ') ' + powers[i].space;
    				powers[i].breakers = breakers;
    			}
    		}

        	var invoices = _.filter(powers, function(arr){
    			return (arr.amount > 0);
    		});
        	
	    	var recBatch = nlapiCreateRecord('customrecord_clgx_queue_json');
	    	recBatch.setFieldValue('custrecord_clgx_queue_json_type', 6);
	    	recBatch.setFieldValue('custrecord_clgx_queue_json_processed', 'F');
	    	recBatch.setFieldValue('custrecord_clgx_queue_json_date', today);
			var batchid = nlapiSubmitRecord(recBatch, false,true);
	    	
			var batch = {
        			"batchid": batchid,
    				"type": 'Commited Amps',
    				"start": start,
    				"end": end,
    				"total": round(total),
    				"count": invoices.length,
    				"children": invoices
        	};
    		
			var folder = clgx_inv_get_folder(2,year,month,1);
			
	    	var fileName = 'usage_commit_amps_' + today + '.json';
	    	var file = nlapiCreateFile(fileName, 'PLAINTEXT', JSON.stringify(batch));
	    	file.setFolder(folder);
			var fileid = nlapiSubmitFile(file);
			
			nlapiSubmitField('customrecord_clgx_queue_json', batchid, 'custrecord_clgx_queue_json_fileid',fileid);

	    	nlapiScheduleScript('customscript_clgx_ss_ci_amps_com_btch_pr', 'customdeploy_clgx_ss_ci_amps_com_btch_pr');   
	    	
			var usage  = 10000 - nlapiGetContext().getRemainingUsage();
			nlapiLogExecution('DEBUG','Process', '| Usage = ' + usage + ' |');
			
    	}
    }
// --------------------------------------------------------------------------

    catch (error) { 
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
