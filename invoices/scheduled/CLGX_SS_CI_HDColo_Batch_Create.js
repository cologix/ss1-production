nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CI_HDColo_Batch_Create.js
//	Script Name:	CLGX_SS_CI_HDColo_Batch_Create
//	Script Id:		customscript_clgx_ss_ci_hdcolo_btch_cr
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/27/2017
//-------------------------------------------------------------------------------------------------

function scheduled_ci_hdcolo_btch_cr (request, response){
    try {
    	
    	var startScript = moment();
    	var context = nlapiGetContext();
    	var environment = context.getEnvironment();
    	var userid = nlapiGetUser();
    	
    	if(environment == 'PRODUCTION'){
    		
    		nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
            
	    	var today = moment().format('MM/D/YYYY');
	    	var year = moment().format('YYYY');
	    	var month = moment().format('M');
	    	var end = moment(month + '/20/' + year).format('MM/D/YYYY');
	    	var start = moment(end).subtract(1, 'months').add(1, 'day').format('MM/D/YYYY');
	    	
        	var sos = [];
        	var search = nlapiLoadSearch('salesorder', 'customsearch_clgx_ci_hdcolo_sos');
    		var result = search.runSearch();
    		result.forEachResult(function(row) {
    			sos.push({
    					"soid": parseInt(row.getValue('internalid',null,null)) || 0,
    					"serviceid": parseInt(row.getValue('custcol_clgx_so_col_service_id',null,null)) || 0,
    					"rate": parseFloat(row.getValue('rate',null,null)) || 0
    			});
    			return true;
    		});


    		var total = 0;
    		var invoices = [];
        	var search = nlapiLoadSearch('customrecord_cologix_space', 'customsearch_clgx_ci_hdcolo_spaces');
    		var result = search.runSearch();
    		result.forEachResult(function(row) {
    			
    			var spaceid = parseInt(row.getValue('internalid',null,'GROUP')) || 0;
    			var space = row.getValue('name',null,'GROUP') || '';
    			var soid = parseInt(row.getValue('internalid','custrecord_space_service_order','GROUP')) || 0;
    			var serviceid = parseInt(row.getValue('custrecord_cologix_space_project',null,'GROUP')) || 0;
    			var service = row.getText('custrecord_cologix_space_project',null,'GROUP') || '';
    			
    			var sorate = 0;
    			var obj = _.find(sos, function(arr){ return (arr.soid == soid && arr.serviceid == serviceid) ; });
    			if (obj){
    				sorate = obj.rate;
    			}
    			
    			var powers = [];
    			var columns = new Array();
    			var filters = new Array();
    			filters.push(new nlobjSearchFilter("custrecord_cologix_power_space",null,"anyof",spaceid));
    			var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_ci_hdcolo_powers', filters, columns);
    			for ( var i = 0; searchPowers != null && i < searchPowers.length; i++ ) {
    				powers.push({
    					"powerid": parseInt(searchPowers[i].getValue('internalid',null,null)) || 0,
    					"power": searchPowers[i].getValue('name',null,null) || '',
    					"avg": round(parseFloat(searchPowers[i].getValue('custrecord_clgx_dcim_sum_day_kw_a',null,null))) || 0,
    					"nacid": parseInt(searchPowers[i].getValue('custrecord_clgx_power_nac_id',null,null)) || 0
    				});
    			}
    			
    			var sum = 0;
    			var avg = 0;
    			if(powers.length > 0){
    				
    				var powersids = _.pluck(powers, 'powerid');
    				
    				var days = [];
    				var columns = new Array();
    				var filters = new Array();
    				filters.push(new nlobjSearchFilter('custrecord_clgx_dcim_points_day_date',null,'within', start,end));
    		    	filters.push(new nlobjSearchFilter('custrecord_clgx_dcim_points_day_power',null,'anyof', powersids));
    		    	var searchKW = nlapiSearchRecord('customrecord_clgx_dcim_points_day', 'customsearch_clgx_ci_hdcolo_kw', filters, columns);
    				for ( var i = 0; searchKW != null && i < searchKW.length; i++ ) {
    					days.push({
    						"day": searchKW[i].getValue('custrecord_clgx_dcim_points_day_date',null,'GROUP') || '',
    						"kw": round(parseFloat(searchKW[i].getValue('custrecord_clgx_dcim_points_day_kw_avg',null,'SUM'))) || 0
    					});
    				}
    				var kws = _.pluck(days, 'kw');
    				if(kws){
    					for( var i = 0; i < kws.length; i++ ){
    					    sum += kws[i]; //don't forget to add the base
    					}
    					avg = round(sum/kws.length);
    				}
    			}

    			var lhigh = round(parseFloat(row.getValue('custbody_clgx_legacy_utilization','custrecord_space_service_order','GROUP'))) || 0;
    			var lhrate = round(parseFloat(row.getValue('custbody_clgx_legay_rate','custrecord_space_service_order','GROUP'))) || 0;
    			
    			var high = round(parseFloat(row.getValue('custbody_clgx_hd_high_utilization','custrecord_space_service_order','GROUP'))) || 0;
    			var hrate = round(parseFloat(row.getValue('custbody_clgx_hd_high_rate','custrecord_space_service_order','GROUP'))) || 0;
    			
    			var uhigh = round(parseFloat(row.getValue('custbody_clgx_hd_ultra_high_utz','custrecord_space_service_order','GROUP'))) || 0;
    			var uhrate = round(parseFloat(row.getValue('custbody_clgx_hd_ultra_high_rate','custrecord_space_service_order','GROUP'))) || 0;
    			
    			var rate = 0;
    			var quantity = 0;
    			var amount = 0;

    			if(avg > lhigh && sorate > 0){
    				rate = lhrate - sorate;
    				quantity = 1;
    				amount = lhrate - sorate;
    				var tier = 'Standard';
    			}
    			if(avg > high && sorate > 0){
    				rate = hrate - sorate;
    				quantity = 1;
    				amount = hrate - sorate;
    				var tier = 'High';
    			}
    			if(avg > uhigh && sorate > 0){
    				rate = uhrate - sorate;
    				quantity = 1;
    				amount = uhrate - sorate;
    				var tier = 'UltraHigh';
    			}
    			
    			if(amount > 0){
    				invoices.push({
    						"node": space,
    						"type": "space",
    						"invoiceid": 0,
    						"invoice": '',
    						"subsidiaryid": parseInt(row.getValue('subsidiary','custrecord_space_service_order','GROUP')) || 0,
    						"customerid": parseInt(row.getValue('entity','custrecord_space_service_order','GROUP')) || 0,
    						"customer": row.getText('entity','custrecord_space_service_order','GROUP') || '',
    						"currencyid": parseInt(row.getValue('currency','custrecord_space_service_order','GROUP')) || 0,
    						"currency": row.getText('currency','custrecord_space_service_order','GROUP') || '',
    						"addressee": row.getValue('billaddressee','custrecord_space_service_order','GROUP') || '',
    						"clocationid": parseInt(row.getValue('custbody_clgx_consolidate_locations','custrecord_space_service_order','GROUP')) || 0,
    						"clocation": row.getText('custbody_clgx_consolidate_locations','custrecord_space_service_order','GROUP') || '',					
    						"locationid": parseInt(row.getValue('location','custrecord_space_service_order','GROUP')) || 0,
    						"location": row.getValue('locationnohierarchy','custrecord_space_service_order','GROUP') || '',
    						"soid": soid,
    						"so": row.getValue('transactionnumber','custrecord_space_service_order','GROUP') || '',
    						"serviceid": serviceid,
    						"service": service,
    						"spaceid": spaceid,
    						"space": space,
    	
    						"sorate": sorate,
    						"ligh": lhigh,
    						"lrate": lhrate,
    						"high": high,
    						"hrate": hrate,
    						"uhigh": uhigh,
    						"uhrate": uhrate,
    						"avg": avg,
    						
    						"rate": rate,
    						"quantity": quantity,
    						"amount": amount,
    						
    						"itemid": 758,
    						"item": 'High Density Usage Premium',
    						"classid": 4,
    						"classname": 'Recurring : Recurring - Power',
    						"description": '(Overage) Location [' + space + '] From ' + start + ' to ' + end + ' Kw:' + avg + ' Tier:' + tier,
    						
    						"faicon": "fa fa-map",
    	    				"leaf": true,
    	    				
    						"kws": kws,
    						"days": days,
    						"powers": powers
    						
    				});
    				total += amount;
    				
    			}
    			return true;
    		});

    		var recBatch = nlapiCreateRecord('customrecord_clgx_queue_json');
	    	recBatch.setFieldValue('custrecord_clgx_queue_json_type', 7);
	    	recBatch.setFieldValue('custrecord_clgx_queue_json_processed', 'F');
	    	recBatch.setFieldValue('custrecord_clgx_queue_json_date', today);
			var batchid = nlapiSubmitRecord(recBatch, false,true);
	    	
			var batch = {
        			"batchid": batchid,
    				"type": 'HD Colo KW',
    				"start": start,
    				"end": end,
    				"total": round(total),
    				"count": invoices.length,
    				"children": invoices
        	};
    		
			var folder = clgx_inv_get_folder(2,year,month,1);
			
	    	var fileName = 'usage_hdcolo_' + today + '.json';
	    	var file = nlapiCreateFile(fileName, 'PLAINTEXT', JSON.stringify(batch));
	    	file.setFolder(folder);
			var fileid = nlapiSubmitFile(file);
			
			nlapiSubmitField('customrecord_clgx_queue_json', batchid, 'custrecord_clgx_queue_json_fileid',fileid);

	    	nlapiScheduleScript('customscript_clgx_ss_ci_hdcolo_btch_pr', 'customdeploy_clgx_ss_ci_hdcolo_btch_pr');   
	    	
			var usage  = 10000 - nlapiGetContext().getRemainingUsage();
			nlapiLogExecution('DEBUG','Process', '| Usage = ' + usage + ' |');
			
        }
    }
// --------------------------------------------------------------------------

    catch (error) { 
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
