nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CCards_Process.js
//	Script Name:	CLGX_SS_CCards_Process
//	Script Id:		customscript_clgx_ss_ccards_process
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		27/02/2017
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_ccards_process (){
    try {

        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');

        var context = nlapiGetContext();
        var initialTime = moment();
        var environment = context.getEnvironment();
        if (environment == 'PRODUCTION') {

// ================================================= build dynamic queue =================================================

            var customers = [];
            var search = nlapiLoadSearch('customer', 'customsearch_clgx_ccards_queue_customers');
            var resultSet = search.runSearch();
            var start = 1;
            resultSet.forEachResult(function (searchResult) {
                //  if(start<11) {
                customers.push({
                    "customerid": parseInt(searchResult.getValue('internalid', null, null)),
                    "customer": searchResult.getValue('entityid', null, null),
                    "subsidiaryid": parseInt(searchResult.getValue('subsidiary', null, null)),
                    "balance": parseFloat(searchResult.getValue('custentity_clgx_customer_balance', null, null)) || 0,
                    "tocharge": parseFloat(searchResult.getValue('custentity_clgx_customer_balance', null, null)) || 0
                });
                // }
                // start++;
                return true;

            });

            var ids = _.pluck(customers, 'customerid');

            var invoices = [];
            var filters = [];
            filters.push(new nlobjSearchFilter('custrecord_clgx_consol_inv_customer', null, 'anyof', ids));
            var search = nlapiLoadSearch('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_ccards_queue_invoices');
            search.addFilters(filters);
            var resultSet = search.runSearch();
            resultSet.forEachResult(function (searchResult) {
                invoices.push({
                    "customerid": parseInt(searchResult.getValue('custrecord_clgx_consol_inv_customer', null, 'GROUP')),
                    "total": parseFloat(searchResult.getValue('custrecord_clgx_consol_inv_total', null, 'SUM')) || 0
                });
                return true;
            });

            for (var i = 0; i < customers.length; i++) {
                var invoice = _.find(invoices, function (arr) {
                    return (arr.customerid == customers[i].customerid);
                });
                if (invoice) {
                    customers[i].tocharge = (customers[i].balance - invoice.total).toFixed(2);
                }
            }

// =================================================  process credit cards =================================================

            for (var i = 0; i < customers.length; i++) {
                var startExecTime = moment();
                var totalMinutes = (startExecTime.diff(initialTime) / 60000).toFixed(1);
                if (context.getRemainingUsage() <= 1000 || totalMinutes > 50 || i > 990) {
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    if (status == 'QUEUED') {
                        nlapiLogExecution('DEBUG', 'End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                        break;
                    }
                }


                // process credit card on Authorize.net
                // is success, create payment record - that should also update the balance on customer (if not trigger here) - important for customer to not be included in the next script run, if any
                // if no success, check 'Credit Card Pause' on Customer - important for customer to not be included in the next script run, if any
                // create ccard transaction record
                //return true;
                try {
                    CC_Process(customers[i]);
                }
                catch (error) {

                }


                var usage = 10000 - parseInt(context.getRemainingUsage());
                var index = i + 1;
                nlapiLogExecution('DEBUG', 'Results ', ' | Index = ' + index + ' of ' + customers.length + ' | Usage - ' + usage + '  |');

            }


// =================================================  send email report =================================================


            nlapiLogExecution('DEBUG', 'Started Execution', '|--------------------------Finish Scheduled Script--------------------------|');
        }
    }

    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}