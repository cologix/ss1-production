nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CI_Usage_Batch_Process.js
//	Script Name:	CLGX_SS_CI_Usage_Batch_Process
//	Script Id:		customscript_clgx_ss_ci_usage_btch_pr
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/13/2015
//-------------------------------------------------------------------------------------------------

function scheduled_ci_usage_btch_pr (request, response){
    try {
    	
    	var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,null).setSort(true));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_queue_json_fileid',null,null));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_queue_json_type",null,"anyof",4));
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_queue_json_processed",null,"is",'F'));
		var searchBatch = nlapiSearchRecord('customrecord_clgx_queue_json', null, arrFilters, arrColumns);
		
		if(searchBatch != null){
			
			var startdate = moment().startOf('month').add('months', 1).format('M/D/YYYY');
			
			var batchid = parseInt(searchBatch[0].getValue('internalid',null,null));
			var fileid = parseInt(searchBatch[0].getValue('custrecord_clgx_queue_json_fileid',null,null));
			
			var objFile = nlapiLoadFile(fileid);
			var fileName = objFile.getName();
			var fileFolder = objFile.getFolder();
			
			var objBatch = JSON.parse(objFile.getValue());
	    	
			nlapiLogExecution('DEBUG','debug', '| batchid = ' + batchid + ' | fileid = ' + fileid + ' | objBatch.lines.length = ' + objBatch.lines.length + ' | ');
			//nlapiLogExecution('DEBUG','debug', '| obj = ' + JSON.stringify( obj ) + ' | ');
			
			for ( var i = 0; objBatch.lines != null && i < objBatch.lines.length; i++ ) {
				
				var recInvoice = nlapiCreateRecord('invoice');
				recInvoice.setFieldValue('entity', objBatch.lines[i].customerid);
				recInvoice.setFieldValue('subsidiary', objBatch.lines[i].subsidiaryid);
				recInvoice.setFieldValue('custbody_clgx_consolidate_locations', objBatch.lines[i].clocationid);
				recInvoice.setFieldValue('billaddressee', objBatch.lines[i].iaddressee);
				recInvoice.setFieldValue('trandate', startdate);
				recInvoice.setFieldValue('startdate', startdate);
				recInvoice.setFieldValue('location', objBatch.lines[i].locationid);
				
				recInvoice.selectNewLineItem('item');
				recInvoice.setCurrentLineItemValue('item','item', objBatch.lines[i].itemid);
				recInvoice.setCurrentLineItemValue('item','location',objBatch.lines[i].locationid);
				recInvoice.setCurrentLineItemValue('item','quantity', objBatch.lines[i].quantity);
				recInvoice.setCurrentLineItemValue('item','rate', objBatch.lines[i].rate);
				recInvoice.setCurrentLineItemValue('item','amount', objBatch.lines[i].amount);
				recInvoice.setCurrentLineItemValue('item','class', objBatch.lines[i].classid);
				if(objBatch.lines[i].subsidiaryid == 6){
					recInvoice.setCurrentLineItemValue('item','taxcode', 11);
				} else {
					if(objBatch.lines[i].clocationid == 25 || objBatch.lines[i].clocationid == 27){ //  JAX
						recInvoice.setCurrentLineItemValue('item','taxcode', 498);
					} else {
						recInvoice.setCurrentLineItemValue('item','taxcode', -8);
					}
				}
				recInvoice.setCurrentLineItemValue('item','description', objBatch.lines[i].description);
				recInvoice.commitLineItem('item');
				
				var recid = nlapiSubmitRecord(recInvoice, false,true);
				
				objBatch.lines[i].invoiceid = parseInt(recid);
				
				if(objBatch.lines[i].soid >0){
					try {
						nlapiSubmitField('invoice', parseInt(recid), 'custbody_clgx_usage_invoice_so', objBatch.lines[i].soid);
					}
					catch (error) {
						nlapiSendEmail(71418,71418,'missing SO','soid = ' + lines[i].soid + ' invid = ' + recid,null,null,null,null);
					}
				}

				var usage  = 10000 - nlapiGetContext().getRemainingUsage();
				nlapiLogExecution('DEBUG','Process', '| Line ' + i + ' of ' + objBatch.lines.length + '| Invoice = ' + recid + ' | Usage = ' + usage + ' |');
			
		    	
				var file = nlapiCreateFile(fileName, 'PLAINTEXT', JSON.stringify(objBatch));
		    	file.setFolder(fileFolder);
				nlapiSubmitFile(file);
				
			}

			nlapiSubmitField('customrecord_clgx_queue_json', batchid, 'custrecord_clgx_queue_json_processed', 'T');
		}
		else{
			nlapiLogExecution('DEBUG','Finish', 'No batch to process');
			// email no batch
		}
    	
    	nlapiLogExecution('DEBUG','Usage', 10000 - nlapiGetContext().getRemainingUsage());
    	
    }
    
// --------------------------------------------------------------------------

    catch (error) { 
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
