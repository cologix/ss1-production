nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Invoices_Queue_Process.js
//	Script Name:	CLGX_SS_Invoices_Queue_Process
//	Script Id:		customscript_clgx_ss_inv_queue_process
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/13/2015
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_inv_queue_process (){
    try {

        var context = nlapiGetContext();
        var environment = context.getEnvironment();

        var deployment = context.getDeploymentId();
        if(deployment == 'customdeploy_clgx_ss_inv_queue_process1'){
            var queue = 4;
        }
        if(deployment == 'customdeploy_clgx_ss_inv_queue_process2'){
            var queue = 5;
        }

        var arrColumns = new Array();
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_invoices",null,"isnot",''));
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_queue",null,"equalto", queue));
        var searchQueue = nlapiSearchRecord('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_inv_ci_queue', arrFilters, arrColumns);

        if(searchQueue != null){

// process first record from queue  -------------------------------------------------------------------------------------------------------------------

            var ciid = parseInt(searchQueue[0].getValue('internalid',null,null));

            nlapiLogExecution('DEBUG','debug', '|------------------------- process first record from queue - ciid = ' + ciid + ' ------------------------- | ');

            var invoiceNbr = searchQueue[0].getValue('custrecord_clgx_consol_inv_batch',null,null);
            var outputid = parseInt(searchQueue[0].getValue('custrecord_clgx_consol_inv_output',null,null));
            var output = searchQueue[0].getText('custrecord_clgx_consol_inv_output',null,null)
            var customerid = parseInt(searchQueue[0].getValue('custrecord_clgx_consol_inv_customer',null,null));
            var customer = searchQueue[0].getText('custrecord_clgx_consol_inv_customer',null,null);
            var location = searchQueue[0].getValue('custrecord_clgx_consol_inv_market',null,null);
            var locationid = clgx_inv_get_locationid (location);
            var currency = searchQueue[0].getValue('custrecord_clgx_consol_inv_currency',null,null);
            var cyear = parseInt(searchQueue[0].getValue('custrecord_clgx_consol_inv_year',null,null));
            var cmonth = parseInt(searchQueue[0].getValue('custrecord_clgx_consol_inv_month',null,null));
            var dyear = parseInt(searchQueue[0].getValue('custrecord_clgx_consol_inv_year_display',null,null));
            var dmonth = parseInt(searchQueue[0].getValue('custrecord_clgx_consol_inv_month_display',null,null));
            var date = searchQueue[0].getValue('custrecord_clgx_consol_inv_date',null,null);
            var userid = searchQueue[0].getValue('custrecord_clgx_consol_inv_user',null,null);

            var strInvoices = searchQueue[0].getValue('custrecord_clgx_consol_inv_invoices',null,null);
            var arrIDs = _.map(strInvoices.split(";"), Number);

            var strContacts = searchQueue[0].getValue('custrecord_clgx_consol_inv_emailed_to',null,null);
            var arrContacts = _.map(strContacts.split(";"), Number);

            var start = moment(cmonth + '/1/' + cyear).format('M/D/YYYY');
            var end = moment(cmonth + '/1/' + cyear).endOf('month').format('M/D/YYYY');
            var invdate = (moment(dmonth + '/1/' + dyear).format('D-MMM-YYYY')).toUpperCase();
            
            var objCInvoices = clgx_inv_get_cinvoices (locationid,customerid,cyear,cmonth,start,end,arrIDs);
            var objCInvoice = objCInvoices.children[0];

            var objTotals = _.find(objCInvoice.children, function(arr){ return (arr.nodetype == 'totals') ; });
            var objSubtotal =  _.find(objTotals.children, function(arr){ return (arr.nodetype == 'subtotal') ; });
            var objTotal =  _.find(objTotals.children, function(arr){ return (arr.nodetype == 'total') ; });


// create JSON File -------------------------------------------------------------------------------------------------------------------

            nlapiLogExecution('DEBUG','debug', '------------------------- create JSON File ');
            
            var jsonFileName = 'cinvoice_' + invoiceNbr + '_' + cyear + '-' + cmonth + '_' + outputid + '_' + customerid + '.json';
            var jsonFileFolder = clgx_inv_get_folder (outputid,cyear,cmonth,1);

            var jsonFileString = JSON.stringify(objCInvoice);
            var jsonFile = nlapiCreateFile(jsonFileName, 'PLAINTEXT', jsonFileString);
            jsonFile.setFolder(jsonFileFolder);
            var jsonFileID = nlapiSubmitFile(jsonFile);

            //nlapiLogExecution('DEBUG','debug', '| jsonFileID = ' + jsonFileID + ' | ');


// create PDF File -------------------------------------------------------------------------------------------------------------------

            nlapiLogExecution('DEBUG','debug', '------------------------- create PDF File ');
            
            var arrHtml = clgx_inv_get_invoice_html (objCInvoice, start);

            var templateFile = nlapiLoadFile(objCInvoice.templateid);
            var mainHTML = templateFile.getValue();
            mainHTML = mainHTML.replace(new RegExp('billAddress','g'),arrHtml[0]);
            //mainHTML = mainHTML.replace(new RegExp('{invoicedate}','g'),objCInvoice.date);
            mainHTML = mainHTML.replace(new RegExp('{invoicedate}','g'),invdate);
            
            mainHTML = mainHTML.replace(new RegExp('{invoiceno}','g'),invoiceNbr);
            mainHTML = mainHTML.replace(new RegExp('{totalcharges}','g'),clgx_inv_get_currency (currency) + ' $' + addCommas((objTotal.amount).toFixed(2)));

            mainHTML = mainHTML.replace(new RegExp('{paymentDue}','g'),objCInvoice.due);
            mainHTML = mainHTML.replace(new RegExp('{billFrom}','g'),objCInvoice.from);
            mainHTML = mainHTML.replace(new RegExp('{billTo}','g'),objCInvoice.to);
            mainHTML = mainHTML.replace(new RegExp('{companyName}','g'),cleanStr(objCInvoice.company));
            mainHTML = mainHTML.replace(new RegExp('{accountNumber}','g'),objCInvoice.customerid);
            mainHTML = mainHTML.replace(new RegExp('ServicesCategoryTable','g'),arrHtml[2]);
            mainHTML = mainHTML.replace(new RegExp('CPITable','g'),arrHtml[1]);
            if(objCInvoice.creditcard == 'T' && objCInvoice.ccpaused=='F') {
                mainHTML = mainHTML.replace(new RegExp('{creditcard}','g'),objCInvoice.labels[20]);
            }
            else{
                mainHTML = mainHTML.replace(new RegExp('{creditcard}','g'),'');

            }

            var today = moment().format('M/D/YYYY');
            var pdfFileName = objCInvoice.customer + '_' + today+ '_' + invoiceNbr + '_' + clgx_inv_get_currency (currency) + '_'+ location + clgx_inv_get_file_sufix (outputid,arrContacts.length);
            var pdfFileFolder = clgx_inv_get_folder(outputid,cyear,cmonth,0);

            var pdfFile = nlapiXMLToPDF(mainHTML);
            pdfFile.setName(pdfFileName);
            pdfFile.setFolder(pdfFileFolder);
            var pdfFileID = nlapiSubmitFile(pdfFile);

            //nlapiLogExecution('DEBUG','debug', '| pdfFileID = ' + pdfFileID + ' | ');

// send email to customers if final invoice -------------------------------------------------------------------------------------------------------------------

            
            var objContacts = _.find(objCInvoice.children, function(arr){ return (arr.nodetype == 'contacts') ; });
            var arrContacts = objContacts.children;

            nlapiLogExecution("DEBUG", "objContacts", JSON.stringify(objContacts));
            nlapiLogExecution("DEBUG", "arrContacts", JSON.stringify(arrContacts));
            nlapiLogExecution("DEBUG", "arrContacts.length", arrContacts.length);
            nlapiLogExecution("DEBUG", "outputid", outputid);
            
            
            if (arrContacts != null && arrContacts.length > 0 && outputid == 1) {
                if(objCInvoice.language == 'en_US' || objCInvoice.language == 'en'){

                    var emailSubject = 'Cologix Invoice - ' + invoiceNbr;
                    var emailBody = 'Dear ' + objCInvoice.company + ',\n\n' +
                        'A copy of your invoice is attached for your records. Please remit payment as soon as possible.\n\n' +
                        'If you have questions regarding your invoice, please send an email to billing@cologix.com.\n\n' +
                        'Thank you for your business.\n\n' +
                        'Sincerely,\n' +
                        'Cologix\n' +
                        '1-855-IX-BILLS (492-4557)\n';
                }
                else{
                    var emailSubject = 'Facture Cologix - ' + invoiceNbr;
                    //emailSubjectConfirm = 'Cologix Invoice sent to ' + custCompName;
                    var emailBody = 'Cher ' + objCInvoice.company + ',\n\n<br><br>' +
                        'Vous trouverez en pi&egrave;ce jointe une copie de votre facture pour vos dossiers. Veuillez s\'il-vous-pla&icirc;t envoyer votre paiement d&egrave;s que possible.\n\n<br><br>' +
                        'Pour toute question concernant votre facture, veuillez envoyer un courriel &agrave; billing@cologix.com.\n\n<br><br>' +
                        'Merci de votre confiance.\n\n<br><br>' +
                        'Cordialement,\n<br>' +
                        'Cologix\n<br>' +
                        '1-855-IX-BILLS (492-4557)\n\n<br><br>' +
                        'AVIS DE CONFIDENTIALIT&Eacute;: L\'information contenue dans le pr&eacute;sent message ainsi que dans toute pi&egrave;ce jointe est priv&eacute;e, confidentielle et est la propri&eacute;t&eacute; de Cologix, Inc. L\'information contenue dans le pr&eacute;sent message ainsi que dans toute pi&egrave;ce jointe est &eacute;galement privil&eacute;gi&eacute;e et &agrave; l\'usage exclusif du destinataire ci-dessus. Toute autre personne est par les pr&eacute;sentes avis&eacute;e qu\'il lui est strictement interdit de prendre quelque action que ce soit en se basant sur l\'information contenue dans le pr&eacute;sent message ainsi que dans toute pi&egrave;ce jointe, tout comme il lui est interdit de divulguer, reproduire ou distribuer cette m&ecirc;me information sans autorisation. Si vous avez re&ccedil;u le pr&eacute;sent message par erreur, veuillez en informer l\'exp&eacute;diteur par t&eacute;l&eacute;phone ou par courriel et veuillez par la suite d&eacute;truire imm&eacute;diatement ce message et toute copie de celui-ci. Merci.\n\n<br><br>';
                }

                nlapiLogExecution('DEBUG','debug', '------------------------- send email to customers if final invoice ');
                
                for (var k = 0; arrContacts != null && k < arrContacts.length; k++) {
                    nlapiSendEmail(12827, arrContacts[k].contactid, emailSubject, emailBody,null,null,null,pdfFile, true);
                    //nlapiSendEmail(author, recipient, subject, body, cc, bcc, records, attachments, notifySenderOnBounce, internalOnly, replyTo)
                }
            } else if(arrContacts != null && arrContacts.length <= 0) {
            	var emailSubject = "Cologix Missing Billing Contact";
            	var emailBody    = "The invoice " + invoiceNbr + " failed to send to " + objCInvoice.company + " because no billing contact exists.";
            	clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_no_billing_contact", emailSubject, emailBody);
            }

// update processed invoices -------------------------------------------------------------------------------------------------------------------

            var objInvoices = _.find(objCInvoice.children, function(arr){ return (arr.nodetype == 'invoices') ; });
            var arrInvoices = objInvoices.children;
            
            nlapiLogExecution('DEBUG','debug', '------------------------- update processed invoices ');
            
            for(var i = 0; i < arrInvoices.length; i++){

                var oldoutputid = nlapiLookupField('invoice', arrInvoices[i].invoiceid, 'custbody_consolidate_inv_output');
                if(oldoutputid === '' || oldoutputid === null){
                    oldoutputid = 0;
                }
                if(oldoutputid !== 1 || outputid === 1){
                    nlapiSubmitField('invoice', arrInvoices[i].invoiceid, ['custbody_consolidate_inv_cinvoice','custbody_consolidate_inv_output','custbody_consolidate_inv_nbr'], [ciid,outputid,invoiceNbr]);
                }
            }

// update consolidated invoice record -------------------------------------------------------------------------------------------------------------------

            nlapiLogExecution('DEBUG','debug', '------------------------- update consolidated invoice record ');
            
            var recQueue = nlapiLoadRecord('customrecord_clgx_consolidated_invoices', ciid);
            recQueue.setFieldValue('name', invoiceNbr);
            recQueue.setFieldValue('custrecord_clgx_consol_inv_json_file_id', jsonFileID);
            recQueue.setFieldValue('custrecord_clgx_consol_inv_pdf_file_id', pdfFileID);
            recQueue.setFieldValue('custrecord_clgx_consol_inv_subtotal', objSubtotal.amount);
            recQueue.setFieldValue('custrecord_clgx_consol_inv_total', objTotal.amount);
            recQueue.setFieldValue('custrecord_clgx_consol_inv_balance', objTotal.amount);
            recQueue.setFieldValue('custrecord_clgx_consol_inv_cc_enabled', objCInvoice.creditcard);
            recQueue.setFieldValue('custrecord_clgx_consol_inv_processed', 'T');
            
            nlapiLogExecution('DEBUG','debug', '| ciid = ' + ciid + ' | jsonFileID = ' + jsonFileID + ' | pdfFileID = ' + pdfFileID + ' | invoiceNbr = ' + invoiceNbr + ' | subtotal = ' + objSubtotal.amount +' | total = ' + objTotal.amount +' | creditcard = ' + objCInvoice.creditcard + ' | ');
            
            var idRec = nlapiSubmitRecord(recQueue, false,true);

// update balances if final invoice -------------------------------------------------------------------------------------------------------------------

            if (outputid == 1) {
            	
            	nlapiLogExecution('DEBUG','debug', '------------------------- update balances if final invoice ');
            	
                var balance = clgx_update_balances (customerid);
            }

// create Credit Card Queue Record -------------------------------------------------------------------------------------------------------------------
            /*
             if (outputid == 1 && objCInvoice.creditcard == 'T') {
             // Add a queue entry for CC processing.
             CC_AddtoQueue(customerid, ciid, 1, date, currency);
             }
             */

// ping NAC if final invoice -------------------------------------------------------------------------------------------------------------------
            /*
             if (environment == 'PRODUCTION' && outputid == 1 && location == 'NNJ') {

             var row = nlapiLoadRecord('customrecord_clgx_consolidated_invoices', ciid);
             try {

             flexapi('POST','/netsuite/update', {
             'type': 'customrecord_clgx_consolidated_invoices',
             'id': ciid,
             'action': 'create',
             'userid': 0,
             'user': 'system',
             'data': json_serialize(row)
             });
             nlapiLogExecution('DEBUG', 'flexapi request: ', ciid);
             }
             catch (error) {
             var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
             record.setFieldValue('custrecord_clgx_queue_ping_record_id', ciid);
             record.setFieldValue('custrecord_clgx_queue_ping_record_type', 4);
             record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
             var idRec = nlapiSubmitRecord(record, false,true);
             }
             }
             */
// re-schedule script -------------------------------------------------------------------------------------------------------------------

            var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());

            //nlapiLogExecution('DEBUG','Usage', 10000 - nlapiGetContext().getRemainingUsage());
        }
        else{
            // queue empty
        }
    }

// Start Catch Errors Section --------------------------------------------------------------------------

    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function clgx_inv_get_invoice_html (objCInvoice, start){

// Address  ------------------------------------------------------------------------------------------------------------------------

    var htmlAddress = '<table cellpadding="0" border="0" table-layout="fixed" class="full">';
    htmlAddress += 	'<tr>';
    htmlAddress += 		'<td class="address">';
    htmlAddress += 			'<b>' + nlapiEscapeXML(cleanStr(objCInvoice.company)) + '</b><br/>';
    if (objCInvoice.aaddressee != null && objCInvoice.aaddressee != '' && objCInvoice.aaddressee != '- None -') {
        htmlAddress += nlapiEscapeXML(cleanStr(objCInvoice.aaddressee)) + '<br/>';
    }
    if (objCInvoice.addresspon != null && objCInvoice.addresspon != '' && objCInvoice.addresspon != '- None -') {
        htmlAddress += nlapiEscapeXML(cleanStr(objCInvoice.addresspon)) + '<br/>';
    }
    if (objCInvoice.attention != null && objCInvoice.attention != '' && objCInvoice.attention != '- None -') {
        htmlAddress += nlapiEscapeXML(objCInvoice.attention) + '<br/>';
    }
    htmlAddress += nlapiEscapeXML(objCInvoice.address1) + '<br/>';
    if (objCInvoice.address2 != null && objCInvoice.address2 != '' && objCInvoice.address2 != '- None -') {
        htmlAddress += nlapiEscapeXML(objCInvoice.address2) + '<br/>';
    }
    if (objCInvoice.address3 != null && objCInvoice.address3 != '' && objCInvoice.address3 != '- None -') {
        htmlAddress += nlapiEscapeXML(objCInvoice.address3) + '<br/>';
    }
    if (objCInvoice.city != null && objCInvoice.city != '' && objCInvoice.city != '- None -') {
        htmlAddress += nlapiEscapeXML(objCInvoice.city);
    }
    if (objCInvoice.state != null && objCInvoice.state != '' && objCInvoice.state != '- None -') {
        htmlAddress += ', ' + nlapiEscapeXML(objCInvoice.state);
    }
    if (objCInvoice.zip != null && objCInvoice.zip != '' && objCInvoice.zip != '- None -') {
        htmlAddress += ' ' + nlapiEscapeXML(objCInvoice.zip);
    }
    htmlAddress += ' <br/>';
    if (objCInvoice.country != null && objCInvoice.country != '' && objCInvoice.country != '- None -') {
        htmlAddress += nlapiEscapeXML(objCInvoice.country);
    }
    htmlAddress += 		'</td>';
    htmlAddress += 	'</tr>';
    htmlAddress += '</table>';


// CPI  ------------------------------------------------------------------------------------------------------------------------

    var objCPI = _.find(objCInvoice.children, function(arr){ return (arr.nodetype == 'cpis') ; });

    var htmlCPI =  '<table class="profile" align="left" table-layout="fixed" width="100%">';
    htmlCPI += 	'<tr>';
    htmlCPI += 		'<td colspan="2"><h4>' + objCInvoice.labels[12] + objCInvoice.from + '</h4></td>';
    htmlCPI += 	'</tr>';

    if(objCPI.children != null){
        for ( var i = 0; i < (objCPI.children).length; i++ ) {

            htmlCPI += '<tr>';
            if (i == 0) {
                htmlCPI += '<td class="profile t-border">&nbsp;' + objCPI.children[i].so + '</td>';
                htmlCPI += '<td class="profile t-border">&nbsp;' + objCPI.children[i].cpi + '%</td>';
            }
            else {
                htmlCPI += '<td>&nbsp;' + objCPI.children[i].so + '</td>';
                htmlCPI += '<td>&nbsp;' + objCPI.children[i].cpi + '%</td>';
            }
            htmlCPI += '</tr>';
        }
    }
    htmlCPI += '</table>';
    htmlCPI += '<br/>';


// Categories  ------------------------------------------------------------------------------------------------------------------------

    var html = '';

    var arrCategories= _.filter(objCInvoice.children, function(arr){
        return (arr.nodetype == 'category' || arr.nodetype == 'discount');
    });

    var html = '';
    html =  '<table cellpadding="1" class="profile" align="center" table-layout="fixed">';
    html += '<tr>';
    html +=     '<td width="7%">&nbsp;</td>';
    html +=     '<td width="10%">&nbsp;</td>';
    html +=     '<td width="25%">&nbsp;</td>';
    html +=     '<td width="28%">&nbsp;</td>';
    html +=     '<td width="10%">&nbsp;</td>';
    html +=     '<td width="10%">&nbsp;</td>';
    html +=     '<td width="10%">&nbsp;</td>';
    html += '</tr>';


    html += '<tr>';
    html +=        '<td colspan="7"><h2>' + objCInvoice.labels[1] + '</h2></td>';
    html += '</tr>';
    html += '<tr><td colspan="7" style="color:white;">.</td></tr>';

    for(var c = 0; arrCategories != null && c < arrCategories.length; c++) {
    	
    	html += '<tr><td colspan="7" style="color:white;">.</td></tr>';
        html += '<tr>';
        html +=     '<td colspan="7">' + arrCategories[c].node + '</td>';
        html += '</tr>';
        html += '<tr><td colspan="7" style="color:white;">.</td></tr>';
        html += '<tr>';
        html +=     '<td class="cool"><b>Ref#</b></td>';
        html +=     '<td class="cool"><b>' + objCInvoice.labels[3] + '#</b></td>';
        html +=     '<td class="cool"><b>' + objCInvoice.labels[4] + '</b></td>';
        html +=     '<td class="cool" align="left"><b>' + objCInvoice.labels[5] + '</b></td>';
        html +=     '<td class="cool" align="right"><b>' + objCInvoice.labels[6] + '</b></td>';
        html +=     '<td class="cool" align="right"><b>' + objCInvoice.labels[7] + '</b></td>';
        html +=     '<td class="cool" align="right"><b>' + objCInvoice.labels[8] + '</b></td>';
        html += '</tr>';

// Items  ------------------------------------------------------------------------------------------------------------------------

        var arrItems = arrCategories[c].children;

        for(var i = 0; arrItems != null && i < arrItems.length; i++){

            // add item on invoice
            html += '<tr>';
            html +=     '<td>'+ nlapiEscapeXML(arrItems[i].invoice) +'</td>';
            html +=     '<td>'+ nlapiEscapeXML(arrItems[i].so) +'</td>';
            if(objCInvoice.language == 'en_US' || objCInvoice.language == 'en'){
                html +=     '<td align="left">'+ nlapiEscapeXML(arrItems[i].ditem) +'</td>';
            }
            else{
                var recItem = nlapiLoadRecord('serviceitem', arrItems[i].itemid);
                var frenchName = recItem.getLineItemValue('translations', 'displayname', 3);
                if (frenchName == null || frenchName == ''){
                    html +=     '<td align="left">'+ nlapiEscapeXML(arrItems[i].ditem) +'</td>';
                }
                else{
                    html +=     '<td align="left">'+ nlapiEscapeXML(frenchName) +'</td>';
                }
            }
            var strQty = '';
            if(arrItems[i].quantity > 0){
                strQty = addCommas((arrItems[i].quantity).toFixed(2));
            }

            html +=     '<td>'+ cleanStrDescr(arrItems[i].memo) +'</td>';
            html +=     '<td align="right">'+ strQty +'</td>';
            html +=     '<td align="right">$'+ addCommas((arrItems[i].rate).toFixed(2)) +'</td>';
            html +=     '<td align="right">$'+ addCommas((arrItems[i].amount).toFixed(2)) +'</td>';
            html += '</tr>';
        }
        html += '<tr><td colspan="7">&nbsp;</td></tr>';
        html += '<tr>';
        html +=     '<td colspan="5" class="profile t-border"><b>' + objCInvoice.labels[9] + arrCategories[c].category + '</b></td>';
        html +=     '<td colspan="2" align="right" class="profile t-border"><b>$' + addCommas((arrCategories[c].subtotal).toFixed(2)) + '</b></td>';
        html += '</tr>';
        html += '<tr><td colspan="7">&nbsp;</td></tr>';
    }


// Totals and Taxes  ------------------------------------------------------------------------------------------------------------------------

    var objTotals = _.find(objCInvoice.children, function(arr){ return (arr.nodetype == 'totals') ; });
    var objTotal =  _.find(objTotals.children, function(arr){ return (arr.nodetype == 'total') ; });

    var sumTaxes = 0;
    for(var t = 0; objTotals.children != null && t < (objTotals.children).length; t++){
        if(objTotals.children[t].nodetype == 'tax'){
            sumTaxes += objTotals.children[t].amount;
        }
    }

    html += '<tr><td colspan="7" style="color:white;">.</td></tr>';
    html += '<tr><td colspan="7">';
    html +=  '<table class="profile" align="center" table-layout="fixed"><tr>';
    html +=  '<td width="60%" class="profile t-border"><h4>' + objCInvoice.labels[16] + '</h4></td>';

    if (sumTaxes > 0) {

        html +=  '<td width="40%" class="profile t-border" align="right"><b>';

        if (objTotals != null && (objTotals.children).length > 0) {

            html +=  '<table class="profile" align="right" width="100%">';

            var tdClass = '';
            var preCurr = '';
            for(var t = 0; objTotals.children != null && t < (objTotals.children).length; t++){

                if (objTotals.children[t].nodetype == 'total'){
                    tdClass = ' class="profile t-border"';
                    preCurr = clgx_inv_get_currency (objCInvoice.currency) + ' ';
                }

                html += '<tr>';
                html += '<td'+ tdClass + '><b>&nbsp;' + objTotals.children[t].node + '</b></td>';
                html += '<td'+ tdClass + ' align="right"><b>'+ preCurr + '$' + addCommas((objTotals.children[t].amount).toFixed(2)) + '</b></td>';
                html += '</tr>';
            }

            html += '</table>';
        }
        html +=  '</b></td>';
    }
    else{
        var preCurr = clgx_inv_get_currency (objCInvoice.currency) + ' ';
        html += '<td width="40%" class="profile t-border" align="right"><b>'+ preCurr + '$' + addCommas((objTotal.amount).toFixed(2)) + '</b></td>';
    }

    html +=  '</tr></table>';
    html += '</td></tr>';

    html += '</table>';
    html += '<br/>';

    var arrHtml = new Array();
    arrHtml.push(htmlAddress);
    arrHtml.push(htmlCPI);
    arrHtml.push(html);

    return arrHtml;
}