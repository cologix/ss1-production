nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CI_HDColo_Batch_Process.js
//	Script Name:	CLGX_SS_CI_HDColo_Batch_Process
//	Script Id:		customscript_clgx_ss_ci_hdcolo_btch_pr
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/27/2017
//-------------------------------------------------------------------------------------------------

function scheduled_ci_hdcolo_btch_pr (request, response){
    try {
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
    	var context = nlapiGetContext();
    	
    	var arrColumns = new Array();
    	arrColumns.push(new nlobjSearchColumn('internalid',null,null).setSort(true));
    	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_queue_json_fileid',null,null));
    	var arrFilters = new Array();
    	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_queue_json_type",null,"anyof",7));
    	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_queue_json_processed",null,"is",'F'));
    	var searchBatch = nlapiSearchRecord('customrecord_clgx_queue_json', null, arrFilters, arrColumns);
    	
    	if(searchBatch != null){
    		
    		var startdate = moment().startOf('month').add('months', 1).format('M/D/YYYY');
    		
    		var batchid = parseInt(searchBatch[0].getValue('internalid',null,null));
    		var fileid = parseInt(searchBatch[0].getValue('custrecord_clgx_queue_json_fileid',null,null));
    		
    		var objFile = nlapiLoadFile(fileid);
    		var fileName = objFile.getName();
    		var fileFolder = objFile.getFolder();
    		
    		var objBatch = JSON.parse(objFile.getValue());
        	
    		var lines = _.filter(objBatch.children, function(arr){
    			return (arr.invoiceid == 0);
    		});
    		
    		if(lines.length > 0){
    			
    	    	var batch = 200;
    	    	var loop = lines.length;
    	    	if(lines.length > batch){
    		        loop = batch;
    			}
    			
    	    	for ( var i = 0; i < loop; i++ ) {
    				
    				var recInvoice = nlapiCreateRecord('invoice');
    				recInvoice.setFieldValue('entity', lines[i].customerid);
    				recInvoice.setFieldValue('subsidiary', lines[i].subsidiaryid);
    				recInvoice.setFieldValue('custbody_clgx_consolidate_locations', lines[i].clocationid);
    				recInvoice.setFieldValue('billaddressee', lines[i].addressee);
    				recInvoice.setFieldValue('trandate', startdate);
    				recInvoice.setFieldValue('startdate', startdate);
    				recInvoice.setFieldValue('location', lines[i].locationid);
    				//recInvoice.setFieldValue('custbody_clgx_usage_invoice_so', lines[i].soid);
    				
    				recInvoice.selectNewLineItem('item');
    				recInvoice.setCurrentLineItemValue('item','item', lines[i].itemid);
    				recInvoice.setCurrentLineItemValue('item','location',lines[i].locationid);
    				recInvoice.setCurrentLineItemValue('item','quantity', lines[i].quantity);
    				recInvoice.setCurrentLineItemValue('item','rate', lines[i].rate);
    				recInvoice.setCurrentLineItemValue('item','amount', lines[i].amount);
    				recInvoice.setCurrentLineItemValue('item','class', lines[i].classid);
    				if(lines[i].subsidiaryid == 6){
    					recInvoice.setCurrentLineItemValue('item','taxcode', 11);
    				}
    				else{
    					recInvoice.setCurrentLineItemValue('item','taxcode', -8);
    				}
    				recInvoice.setCurrentLineItemValue('item','description', lines[i].description);
    				recInvoice.commitLineItem('item');
    				
    				var recid = nlapiSubmitRecord(recInvoice, false,true);
    				
    				lines[i].invoiceid = parseInt(recid);
    				
    				try {
    					nlapiSubmitField('invoice', parseInt(recid), 'custbody_clgx_usage_invoice_so', lines[i].soid);
    				}
    				catch (error) {
    					nlapiSendEmail(71418,71418,'missing SO','soid = ' + lines[i].soid + ' invid = ' + recid,null,null,null,null);
    				}
    				
    				var usage  = 10000 - nlapiGetContext().getRemainingUsage();
    				nlapiLogExecution('DEBUG','Process', '| Line ' + i + ' of ' + lines.length + '| Invoice = ' + recid + ' | Usage = ' + usage + ' |');
    			}
    	    	
    			var file = nlapiCreateFile(fileName, 'PLAINTEXT', JSON.stringify(objBatch));
    	    	file.setFolder(fileFolder);
    			var fileid = nlapiSubmitFile(file);
    		
    			status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
    		}
        	else{
        		
        		nlapiLogExecution('DEBUG','Debug', 'No more invoices to create'); 
        		nlapiSubmitField('customrecord_clgx_queue_json', batchid, 'custrecord_clgx_queue_json_processed', 'T');
        	}
    	}
    	else{
    		nlapiLogExecution('DEBUG','Finish', 'No batch to process');
    		// email no batch
    	}
    	
    	
    	nlapiLogExecution('DEBUG','Usage', 10000 - nlapiGetContext().getRemainingUsage());
    	
}
    
// --------------------------------------------------------------------------

    catch (error) { 
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
