//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_LIB_Invoices_Functions.js
//	Script Name:	CLGX_LIB_Invoices_Functions
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/11/2015
//-------------------------------------------------------------------------------------------------


function clgx_inv_get_cinvoices (locationid,customerid,cyear,cmonth,start,end,arrIDs){

    var caddressee = nlapiLookupField('customer', customerid, 'billaddressee');

//----------------------- UNIQUE INVOICES QUERY --------------------------------------------------------------------------------------------

    var arrColumns = new Array();
    if(caddressee == '' || caddressee == null){
        arrColumns.push(new nlobjSearchColumn('billaddressee',null,'GROUP'));
    }
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('trandate',null,'within', start,end));
    arrFilters.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', locationid));
    arrFilters.push(new nlobjSearchFilter('entity',null,'anyof', customerid));
    arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof', arrIDs));
    if(caddressee == '' || caddressee == null){
        arrFilters.push(new nlobjSearchFilter('billaddressee',null,'isnot',''));
    }
    var searchCInvoices = nlapiSearchRecord('invoice', 'customsearch_clgx_inv_invoices_uniques', arrFilters, arrColumns);
    var arrInvoices = new Array();
    for ( var i = 0; searchCInvoices != null && i < searchCInvoices.length; i++ ) {

        var arrCPIs = new Array();
        var arrSOs = new Array();
        var arrIncludedInvoices = new Array();
        var arrIncludedInvoicesIDs = new Array();

        var objInvoice = new Object();
        var customer = searchCInvoices[i].getText('entity',null,'GROUP');
        var iaddressee = searchCInvoices[i].getValue('billaddressee',null,'GROUP') || '';

        if(caddressee == '' || caddressee == null){
            var node = iaddressee;
        }
        else{
            var node = caddressee;
        }

        var objCustomer = clgx_inv_get_customer (customerid,iaddressee,start);
        var arrLabels = clgx_inv_get_labels (locationid, objCustomer.language);

        objInvoice["node"] = node;
        objInvoice["nodetype"] = 'cinvoice';

        objInvoice["start"] = start;
        objInvoice["end"] = end;
        objInvoice["from"] = getDateFormated(start,objCustomer.language);
        objInvoice["to"] = getDateFormated(end,objCustomer.language);
        objInvoice["date"] = getDateFormated(start,objCustomer.language);

        objInvoice["customerid"] = customerid;
        objInvoice["customer"] = customer;
        var location = searchCInvoices[i].getText('custbody_clgx_consolidate_locations',null,'GROUP');
        objInvoice["locationid"] = locationid;
        objInvoice["location"] = location;
        var currencyid = parseInt(searchCInvoices[i].getValue('currency',null,'GROUP'));
        objInvoice["currencyid"] = currencyid;
        objInvoice["currency"] = searchCInvoices[i].getText('currency',null,'GROUP');
        objInvoice["iaddressee"] = iaddressee;
        //objInvoice["outputid"] = outputid;
        //objInvoice["output"] = output;
        objInvoice["cyear"] = cyear;
        objInvoice["cmonth"] = cmonth;
        //objInvoice["dyear"] = dyear;
        //objInvoice["dmonth"] = dmonth;

        objInvoice["company"] = objCustomer.company;
        objInvoice["account"] = objCustomer.account;
        objInvoice["language"] = objCustomer.language;
        var subsidiaryid = objCustomer.subsidiaryid;
        objInvoice["subsidiaryid"] = subsidiaryid;
        objInvoice["subsidiary"] = objCustomer.subsidiary;
        objInvoice["terms"] = objCustomer.terms;
        objInvoice["creditcard"] = objCustomer.creditcard;
        objInvoice["ccpaused"] = objCustomer.ccpaused;
        objInvoice["due"] = objCustomer.due;
        objInvoice["addressid"] = objCustomer.addressid;
        objInvoice["address1"] = objCustomer.address1;
        objInvoice["address2"] = objCustomer.address2;
        objInvoice["address3"] = objCustomer.address3;
        objInvoice["caddressee"] = objCustomer.caddressee;
        objInvoice["aaddressee"] = objCustomer.aaddressee;
        objInvoice["addresspon"] = objCustomer.addresspon;
        objInvoice["attention"] = objCustomer.attention;
        objInvoice["city"] = objCustomer.city;
        objInvoice["state"] = objCustomer.state;
        objInvoice["zip"] = objCustomer.zip;
        objInvoice["country"] = objCustomer.country;
        if(objCustomer.creditcard == 'T'  && objCustomer.ccpaused=='F') {
            objInvoice["creditE"]=arrLabels[20];
        }
        else{
            objInvoice["creditE"]='';
        }

        var objTemplate = clgx_inv_get_template (locationid,objCustomer.language);
        objInvoice["templateid"] = objTemplate.templateid;
        objInvoice["template"] = objTemplate.template;


//----------------------- ALL DISCOUNTS QUERY --------------------------------------------------------------------------------------------

        var arrColumns = new Array();
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter('trandate',null,'within', start,end));
        arrFilters.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', locationid));
        arrFilters.push(new nlobjSearchFilter('entity',null,'anyof', customerid));
        arrFilters.push(new nlobjSearchFilter('currency',null,'anyof',currencyid));
        if(caddressee == '' || caddressee == null){
            arrFilters.push(new nlobjSearchFilter('billaddressee',null,'is', objInvoice.iaddressee));
            arrFilters.push(new nlobjSearchFilter('billaddressee',null,'isnot',''));
        }
        arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof', arrIDs));
        var searchDiscounts = nlapiSearchRecord('invoice','customsearch_clgx_inv_discounts',arrFilters,arrColumns);
        var arrDiscounts = new Array();
        for ( var d = 0; searchDiscounts != null && d < searchDiscounts.length; d++ ) {
            var objDiscount = new Object();
            objDiscount["invoiceid"] = parseInt(searchDiscounts[d].getValue('internalid',null,null));
            objDiscount["invoice"] = searchDiscounts[d].getValue('tranid',null,null);
            objDiscount["itemid"] = parseInt(searchDiscounts[d].getValue('item',null,null));
            objDiscount["item"] = searchDiscounts[d].getText('item',null,null);
            objDiscount["ditem"] = searchDiscounts[d].getValue('displayname', 'item',null);

            objDiscount["soid"] = parseInt(searchDiscounts[d].getValue('createdfrom',null,null));
            var so = (searchDiscounts[d].getText('createdfrom',null,null)).replace(/\Service Order #/g,"");
            var lso = searchDiscounts[d].getValue('custbody_cologix_legacy_so',null,null);
            if(lso != null && lso != ''){
                objDiscount["so"] = lso;
            }
            else{
                objDiscount["so"] = so;
            }

            objDiscount["memo"] = (searchDiscounts[d].getValue('memo',null,null)).replace(/"/g, '');
            objDiscount["quantity"] = parseFloat(searchDiscounts[d].getValue('quantity',null,null));
            objDiscount["rate"] = parseFloat(searchDiscounts[d].getValue('rate',null,null));

            var amount = parseFloat(searchDiscounts[d].getValue('amount',null,null));
            amount = Math.round((Math.round(amount*1000)/1000)*100)/100;
            var fxamount = parseFloat(searchDiscounts[d].getValue('amount',null,null));
            fxamount = Math.round((Math.round(fxamount*1000)/1000)*100)/100;

            objDiscount["amount"] = parseFloat(amount);
            objDiscount["fxamount"] = parseFloat(fxamount);
            arrDiscounts.push(objDiscount);
        }

//----------------------- ITEMS QUERY --------------------------------------------------------------------------------------------

        var arrColumns = new Array();
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter('trandate',null,'within', start,end));
        arrFilters.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', locationid));
        arrFilters.push(new nlobjSearchFilter('entity',null,'anyof', customerid));
        arrFilters.push(new nlobjSearchFilter('currency',null,'anyof',currencyid));
        if(caddressee == '' || caddressee == null){
            arrFilters.push(new nlobjSearchFilter('billaddressee',null,'is', objInvoice.iaddressee));
            arrFilters.push(new nlobjSearchFilter('billaddressee',null,'isnot',''));
        }
        arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof', arrIDs));
        var arrItems = new Array();
        var searchItems = nlapiLoadSearch('invoice', 'customsearch_clgx_inv_items');
        searchItems.addColumns(arrColumns);
        searchItems.addFilters(arrFilters);
        var resultSet = searchItems.runSearch();
        resultSet.forEachResult(function(searchResult) {
            var columns = searchResult.getAllColumns();
            var objItem = new Object();
            var invid = parseInt(searchResult.getValue('internalid',null,null));
            objItem["invoiceid"] = invid;
            var inv = searchResult.getValue('tranid',null,null);
            objItem["invoice"] = inv;
            objItem["itemid"] = parseInt(searchResult.getValue('item',null,null));
            objItem["item"] = searchResult.getText('item',null,null);
            objItem["ditem"] = searchResult.getValue('displayname', 'item',null);

            var soid = parseInt(searchResult.getValue('createdfrom',null,null));
            objItem["soid"] = soid;

            var so = '';
            var name = (searchResult.getText('createdfrom', null, null)) || '';
            if (name){
                name = name.split("#");
                so = (name[name.length-1]).trim();
            }

            lso = searchResult.getValue('custbody_cologix_legacy_so',null,null).trim();

            var usageso = '';
            var name = (searchResult.getText('custbody_clgx_usage_invoice_so', null, null)) || '';
            if (name){
                name = name.split("#");
                usageso = (name[name.length-1]).trim();
            }

            //var so = (searchResult.getText('createdfrom',null,null)).replace(/\Service Order #/g,"");
            //var lso = searchResult.getValue('custbody_cologix_legacy_so',null,null);
            //var usageso = (searchResult.getText('custbody_clgx_usage_invoice_so',null,null)).replace(/\Service Order #/g,"") || '';

            if(lso != null && lso != ''){
                objItem["so"] = lso;
            }
            else{
                if(usageso != null && usageso != ''){
                    objItem["so"] = usageso;
                } else {
                    objItem["so"] = so;
                }
            }

//----------------------- BUILD CPIs & SOs ARRAY --------------------------------------------------------------------------------------------

            var cpirate = parseFloat(searchResult.getValue('custbody_cologix_annual_accelerator',null,null));
            var nextaadate = searchResult.getValue('custbody_clgx_next_aa_date',null,null);
            if (nextaadate != '' && nextaadate != null){
                var aamonth = parseInt(moment(nextaadate).format('M'));
            }
            else{
                var aamonth = 0;
            }
            objItem["cpi"] = cpirate;
            objItem["nextaadate"] = nextaadate;
            objItem["aamonth"] = aamonth;

            var contractDate = '';
            var difMonths = 0;
            if(soid > 0){
                contractDate = searchResult.getValue('custbody_cologix_so_contract_start_dat','createdfrom',null);
            }
            if (contractDate != '' && contractDate != null){
                var startDateMom = new moment(start);
                var contractDateMom = new moment(contractDate).startOf('month');
                difMonths = Math.ceil(startDateMom.diff(contractDateMom, 'months', true));
            }
            objItem["difMonths"] = difMonths;
            objItem["contractDate"] = contractDate;

            if ((_.indexOf(arrSOs, so) == -1) && cpirate > 0 && aamonth == cmonth && difMonths > 11){
                objCPI = new Object();
                objCPI["node"] = so + ' (' + cpirate + '%)';
                objCPI["nodetype"] = 'cpi';
                objCPI["so"] = so;
                objCPI["soid"] = soid;
                objCPI["cpi"] = cpirate;
                objCPI["faicon"] = 'fa fa-list-ul Silver1';
                objCPI["leaf"] = true;
                arrCPIs.push(objCPI);
                arrSOs.push(so);
            }

//----------------------- BUILD INCLUDED INVOICES ARRAY --------------------------------------------------------------------------------------------

            if (_.indexOf(arrIncludedInvoicesIDs, invid) == -1){
                arrIncludedInvoicesIDs.push(invid);

                objIncludedInvoice = new Object();
                objIncludedInvoice["node"] = inv;
                objIncludedInvoice["nodetype"] = 'invoice';
                objIncludedInvoice["invoiceid"] = invid;
                objIncludedInvoice["invoice"] = inv;
                objIncludedInvoice["faicon"] = 'fa fa-list-ul Silver1';
                objIncludedInvoice["leaf"] = true;
                arrIncludedInvoices.push(objIncludedInvoice);
            }

//----------------------- FINISH BUILD ARRAYS --------------------------------------------------------------------------------------------

            var memo = searchResult.getValue('memo',null,null);
            memo = memo.replace(/"/g, '');
            memo = memo.replace(/&/g, ' and ');

            objItem["memo"] = memo;
            objItem["categoryid"] = parseInt(searchResult.getValue('custcol_cologix_invoice_item_category',null,null));
            objItem["category"] = searchResult.getText('custcol_cologix_invoice_item_category',null,null);
            objItem["quantity"] = parseFloat(searchResult.getValue('quantity',null,null));
            objItem["rate"] = parseFloat(searchResult.getValue('rate',null,null));
            objItem["crate"] = parseFloat(searchResult.getValue(columns[20]));

            var amount = parseFloat(searchResult.getValue('amount',null,null));
            amount = Math.round((Math.round(amount*1000)/1000)*100)/100;
            var fxamount = parseFloat(searchResult.getValue('fxamount',null,null));
            fxamount = Math.round((Math.round(fxamount*1000)/1000)*100)/100;

            objItem["amount"] = parseFloat(amount);
            objItem["fxamount"] = parseFloat(fxamount);
            //objItem["amount"] = parseFloat(Math.round((Math.round((searchResult.getValue('amount',null,null))*1000)/1000)*100)/100);
            //objItem["fxamount"] = parseFloat(Math.round((Math.round((searchResult.getValue('fxamount',null,null))*1000)/1000)*100)/100);

            objItem["exchangerate"] = parseFloat(searchResult.getValue('exchangerate',null,null));
            arrItems.push(objItem);
            return true;
        });

        var arrInvoicesIDs = _.compact(_.uniq(_.pluck(arrItems, 'invoiceid')));


//----------------------- CATEGORIES LOOP --------------------------------------------------------------------------------------------

        var arrCategories = _.compact(_.uniq(_.pluck(arrItems, 'category')));
        arrCategories = _.sortBy(arrCategories, function(obj){ return obj;});

        var arrNodes = new Array();
        var subtotal = 0;
        for ( var c = 0; c < arrCategories.length; c++ ) {

            var objCategory = _.find(arrItems, function(arr){ return (arr.category == arrCategories[c]) ; });

            if(objCustomer.language == 'en_US' || objCustomer.language == 'en'){
                var category = objCategory.category;
            }
            else{
                var category = clgx_inv_get_french_category (objCategory.categoryid, objCategory.category);
            }

            objNode = new Object();
            objNode["node"] = (arrLabels[2] + category).toUpperCase();
            objNode["nodetype"] = 'category';

            objNode["categoryid"] = objCategory.categoryid;
            objNode["category"] = category.toUpperCase();

//----------------------- ITEMS LOOP --------------------------------------------------------------------------------------------

            var arrCategoryItems = _.filter(arrItems, function(arr){
                return arr.category === arrCategories[c];
            });
            var arrI = new Array();
            var sumCateg = 0;
            for ( var j = 0; j < arrCategoryItems.length; j++ ) {

                objI = new Object();
                objI["node"] = arrCategoryItems[j].ditem;
                objI["nodetype"] = 'item';

                objI["itemid"] = arrCategoryItems[j].itemid;
                objI["item"] = arrCategoryItems[j].item;
                objI["ditem"] = arrCategoryItems[j].ditem;
                objI["invoiceid"] = arrCategoryItems[j].invoiceid;
                objI["invoice"] = arrCategoryItems[j].invoice;
                //objI["soid"] = arrCategoryItems[j].soid;
                objI["so"] = arrCategoryItems[j].so;
                objI["memo"] = arrCategoryItems[j].memo; // to clean up of " ?

                var quantity = arrCategoryItems[j].quantity;
                objI["quantity"] = quantity;

                if(subsidiaryid == 6 && currencyid == 1){ // Canada subsidiary and $US - use itemCalcRate instead of itemRate

                    objI["rate"] = arrCategoryItems[j].rate / arrCategoryItems[j].exchangerate;
                    var amount =  arrCategoryItems[j].quantity * (arrCategoryItems[j].rate / arrCategoryItems[j].exchangerate);
                    amount = Math.round((Math.round(amount*1000)/1000)*100)/100;

                    objI["amount"] = amount;
                    sumCateg += amount;
                    subtotal += amount;
                }
                else{

                    objI["rate"] = arrCategoryItems[j].rate;
                    var amount =  arrCategoryItems[j].quantity * arrCategoryItems[j].rate;
                    amount = Math.round((Math.round(amount*1000)/1000)*100)/100;
                    objI["amount"] = amount;
                    sumCateg += amount;
                    subtotal += amount;
                }

                objI["cpi"] = arrCategoryItems[j].cpirate;
                objI["contractDate"] = arrCategoryItems[j].contractDate;
                objI["nextaadate"] = arrCategoryItems[j].nextaadate;
                objI["aamonth"] = arrCategoryItems[j].aamonth;
                objI["difMonths"] = arrCategoryItems[j].difMonths;

                objI["faicon"] = 'fa fa-cog Silver1';
                objI["faicon"] = 'fa fa-cog Silver1';
                objI["leaf"] = true;
                arrI.push(objI);
            }

//----------------------- CATEGORY DISCOUNTS LOOP --------------------------------------------------------------------------------------------

            var arrDiscountsCategs = new Array();
            if(objCategory.categoryid == 8 || objCategory.categoryid == 10){ // if category is Power or Space

                if(objCategory.categoryid == 8){
                    arrDiscountsCategs = _.filter(arrDiscounts, function(arr){
                        return (arr.itemid == 534);
                    });
                }
                else{
                    arrDiscountsCategs = _.filter(arrDiscounts, function(arr){
                        return (arr.itemid == 535 || arr.itemid == 551);
                    });
                }

                if (arrDiscountsCategs != null && arrDiscountsCategs.length > 0){

                    for ( var d = 0; d < arrDiscountsCategs.length; d++ ) {
                        objI = new Object();
                        objI["node"] = arrDiscountsCategs[d].ditem;
                        objI["nodetype"] = 'item';

                        objI["itemid"] = arrDiscountsCategs[d].itemid;
                        objI["item"] = arrDiscountsCategs[d].item;

                        objI["ditem"] = arrDiscountsCategs[d].ditem;
                        objI["invoiceid"] = arrDiscountsCategs[d].invoiceid;
                        objI["invoice"] = arrDiscountsCategs[d].invoice;
                        //objI["soid"] = arrDiscountsCategs[d].soid;
                        objI["so"] = arrDiscountsCategs[d].so;
                        objI["memo"] = arrDiscountsCategs[d].memo; // to clean up of " ?

                        var amount =  arrDiscountsCategs[d].fxamount;
                        amount = Math.round((Math.round(amount*1000)/1000)*100)/100;
                        objI["quantity"] = '';
                        objI["rate"] = amount;
                        objI["amount"] = amount;

                        sumCateg += amount;
                        subtotal += amount;

                        objI["faicon"] = 'fa fa-cog Silver1';
                        objI["leaf"] = true;
                        if(amount != 0){
                            arrI.push(objI);
                        }
                    }
                }
            }



            objNode["children"] = arrI;
            objNode["subtotal"] = sumCateg;
            objNode["expanded"] = false;
            objNode["faicon"] = 'fa fa-cogs SteelBlue1';
            objNode["leaf"] = false;

            arrNodes.push(objNode);
        }


//----------------------- REFERENCE DISCOUNTS LOOP --------------------------------------------------------------------------------------------

        arrDiscountsRef = _.filter(arrDiscounts, function(arr){
            return (arr.itemid == 533);
        });

        if (arrDiscountsRef != null && arrDiscountsRef.length > 0){

            objNode = new Object();
            objNode["node"] = (arrLabels[2] + arrLabels[18]).toUpperCase();
            objNode["nodetype"] = 'discount';
            objNode["categoryid"] = 0;
            objNode["category"] = 'DISCOUNTS';

            var sumCateg = 0;
            var arrI = new Array();
            for ( var d = 0; d < arrDiscountsRef.length; d++ ) {
                objI = new Object();
                objI["node"] = arrDiscountsRef[d].ditem;
                objI["nodetype"] = 'item';

                objI["itemid"] = arrDiscountsRef[d].itemid;
                objI["item"] = arrDiscountsRef[d].item;
                objI["ditem"] = arrDiscountsRef[d].ditem;

                objI["invoiceid"] = arrDiscountsRef[d].invoiceid;
                objI["invoice"] = arrDiscountsRef[d].invoice;
                //objI["soid"] = arrDiscountsRef[d].soid;
                objI["so"] = arrDiscountsRef[d].so;
                objI["memo"] = arrDiscountsRef[d].memo; // to clean up of " ?

                var amount =  arrDiscountsRef[d].fxamount;
                amount = Math.round((Math.round(amount*1000)/1000)*100)/100;
                objI["quantity"] = '';
                objI["rate"] = amount;
                objI["amount"] = amount;

                sumCateg += amount;
                subtotal += amount;

                objI["faicon"] = 'fa fa-cog Silver1';
                objI["leaf"] = true;
                if(amount != 0){
                    arrI.push(objI);
                }
            }
            objNode["children"] = arrI;
            objNode["subtotal"] = sumCateg;
            objNode["expanded"] = false;
            objNode["faicon"] = 'fa fa-cogs SteelBlue1';
            objNode["leaf"] = false;
            if(sumCateg != 0){
                arrNodes.push(objNode);
            }
        }

//----------------------- REFERENCE FIRST MONTH FREE LOOP --------------------------------------------------------------------------------------------

        arrDiscountsFMF= _.filter(arrDiscounts, function(arr){
            return (arr.itemid == 549);
        });

        if (arrDiscountsFMF != null && arrDiscountsFMF.length > 0){

            objNode = new Object();

            objNode["node"] = (arrLabels[2] + arrLabels[19]).toUpperCase();
            objNode["nodetype"] = 'discount';
            objNode["categoryid"] = 0;
            objNode["category"] = 'DISCOUNTS';

            var sumCateg = 0;
            var arrI = new Array();
            for ( var d = 0; d < arrDiscountsFMF.length; d++ ) {
                objI = new Object();
                objI["node"] = arrDiscountsFMF[d].ditem;
                objI["nodetype"] = 'item';

                objI["itemid"] = arrDiscountsFMF[d].itemid;
                objI["item"] = arrDiscountsFMF[d].item;
                objI["ditem"] = arrDiscountsFMF[d].ditem;

                objI["invoiceid"] = arrDiscountsFMF[d].invoiceid;
                objI["invoice"] = arrDiscountsFMF[d].invoice;
                //objI["soid"] = arrDiscountsFMF[d].soid;
                objI["so"] = arrDiscountsFMF[d].so;
                objI["memo"] = arrDiscountsFMF[d].memo; // to clean up of " ?

                var amount =  arrDiscountsFMF[d].fxamount;
                amount = Math.round((Math.round(amount*1000)/1000)*100)/100;
                objI["quantity"] = '';
                objI["rate"] = amount;
                objI["amount"] = amount;

                sumCateg += amount;
                subtotal += amount;

                objI["faicon"] = 'fa fa-cog Silver1';
                objI["leaf"] = true;
                if(amount != 0){
                    arrI.push(objI);
                }
            }
            objNode["children"] = arrI;
            objNode["subtotal"] = sumCateg;
            objNode["expanded"] = false;
            objNode["faicon"] = 'fa fa-cogs SteelBlue1';
            objNode["leaf"] = false;
            if(sumCateg != 0){
                arrNodes.push(objNode);
            }
        }

//----------------------- FINISH DISCOUNTS --------------------------------------------------------------------------------------------

        objInvoice["children"] = arrNodes;

//----------------------- FINISH CATEGORIES --------------------------------------------------------------------------------------------

        var objTotals = clgx_inv_get_totals (start,end,locationid,customerid,currencyid,objInvoice.caddressee,objInvoice.iaddressee,arrLabels,subtotal,arrIDs);
        arrNodes.push(objTotals);

        objNode = new Object();
        objNode["node"] = 'Payments';
        objNode["nodetype"] = 'payments';
        var objTotal = _.find(objTotals.children, function(arr){ return (arr.nodetype == "total") ; });
        objNode["paid"] = 0;
        objNode["balance"] = objTotal.amount;
        objNode["children"] = [];
        objNode["expanded"] = false;
        objNode["faicon"] = 'fa fa-list-ul SteelBlue1';
        objNode["leaf"] = false;
        arrNodes.push(objNode);

        objNode = new Object();
        objNode["node"] = 'SOs with a new CPI Rate ' + getDateFormated(start,objCustomer.language);
        objNode["nodetype"] = 'cpis';
        objNode["children"] = arrCPIs;
        objNode["expanded"] = false;
        objNode["faicon"] = 'fa fa-list-ul SteelBlue1';
        objNode["leaf"] = false;
        arrNodes.push(objNode);

        arrNodes.push(clgx_inv_get_contacts (customerid,iaddressee));


        objNode = new Object();
        objNode["node"] = 'Included invoices';
        objNode["nodetype"] = 'invoices';
        objNode["children"] = arrIncludedInvoices;
        objNode["expanded"] = false;
        objNode["faicon"] = 'fa fa-list-ul SteelBlue1';
        objNode["leaf"] = false;
        arrNodes.push(objNode);


        objInvoice["expanded"] = true;
        objInvoice["faicon"] = 'fa fa-list-ul SteelBlue2';
        objInvoice["leaf"] = false;

        objInvoice["labels"] = arrLabels;
        objInvoice["usage"] = 1000 - nlapiGetContext().getRemainingUsage();

        arrInvoices.push(objInvoice);
    }

    var objTree = new Object();
    objTree["text"] = '.';
    objTree["customerid"] = customerid;
    objTree["customer"] = customer;
    objTree["locationid"] = locationid;
    objTree["location"] = location;
    //objTree["outputid"] = outputid;
    //objTree["output"] = output;
    objTree["cyear"] = cyear;
    objTree["cmonth"] = cmonth;
    //objTree["dyear"] = dyear;
    //objTree["dmonth"] = dmonth;
    objTree["children"] = arrInvoices;

    return objTree;

}

function clgx_inv_get_customer (customerid,iaddressee,start){

    var objCustomer = new Object();
    objCustomer["text"] = '.';

    var recCustomer = nlapiLoadRecord('customer', customerid);
    var nbrAddresses = recCustomer.getLineItemCount('addressbook');

    var customer = recCustomer.getFieldValue('entityid');
    var company = recCustomer.getFieldValue('companyname');
    var terms = parseInt(recCustomer.getFieldValue('terms'));
    var account = recCustomer.getFieldValue('accountnumber');
    var language = recCustomer.getFieldValue('language');
    var subsidiaryid = parseInt(recCustomer.getFieldValue('subsidiary'));
    var subsidiary = recCustomer.getFieldText('subsidiary');
    var caddressee = recCustomer.getFieldValue('billaddressee');
    var ccenabled=recCustomer.getFieldValue('custentity_clgx_cc_enabled');
    var ccpaused=recCustomer.getFieldValue('custentity_clgx_cc_paused');

    objCustomer["customerid"] = customerid;
    objCustomer["customer"] = customer;
    objCustomer["company"] = company;
    objCustomer["account"] = account;
    objCustomer["language"] = language;
    objCustomer["subsidiaryid"] = subsidiaryid;
    objCustomer["subsidiary"] = subsidiary;
    objCustomer["caddressee"] = caddressee;
    objCustomer["terms"] = terms;
    objCustomer["creditcard"]=ccenabled;
    objCustomer["creditE"]=ccenabled;
    objCustomer["ccpaused"]=ccpaused;
    objCustomer["due"] = clgx_inv_get_date_due (terms,start,language);

    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",customerid));
    var searchAddressees = nlapiSearchRecord('customer', 'customsearch_clgx_invs_cust_addressees', arrFilters, arrColumns);

    //nlapiLogExecution('DEBUG','debug', '| caddressee = ' + caddressee + '| iaddressee = ' + iaddressee + ' | ');

    if(caddressee == '' || caddressee == null){

        // loop the addresses of the customer and look for the right addressee and get the address
        var countAddress = 0;
        for ( var a = 0; searchAddressees != null && a < searchAddressees.length; a++ ) {
            var aaddressee = searchAddressees[a].getValue('addressee','address',null);
            if (aaddressee == iaddressee){
                var addressid = parseInt(searchAddressees[a].getValue('addressinternalid','address',null));
                var address1 = searchAddressees[a].getValue('address1','address',null);
                var address2 = searchAddressees[a].getValue('address2','address',null);
                var address3 = searchAddressees[a].getValue('address3','address',null);
                var addressee = searchAddressees[a].getValue('addressee','address',null);
                var addresspon = searchAddressees[a].getValue('custrecord_clgx_address_pon','address',null);
                var attention = searchAddressees[a].getValue('attention','address',null);
                var city = searchAddressees[a].getValue('city','address',null);
                var state = searchAddressees[a].getValue('state','address',null);
                var zip = searchAddressees[a].getValue('zipcode','address',null);
                var country = searchAddressees[a].getText('country','address',null);
                countAddress = 1;
                break;
            }
        }

        if(countAddress == 0){ // no address was found or is differet
            var emailSubjectAddressee = 'ALERT - Customer - ' + customer + ' - invoice(s) without an address ( different addressee on invoice then on customer record.)';
            var emailBodyAddressee = '';
            //nlapiSendEmail(2406, 12073,emailSubjectAddressee,emailBodyAddressee,null,null,null,null); // send to Kristen
            //nlapiSendEmail(12827,206211,emailSubjectAddressee,emailBodyAddressee,null,null,null,null);
            //nlapiSendEmail(12827,336456,emailSubjectAddressee,emailBodyAddressee,null,null,null,null);
            //nlapiSendEmail(2406, 71418,emailSubjectAddressee,emailBodyAddressee,null,null,null,null);  // send to Dan

            var addressid = '0';
            var address1 = '';
            var address2 = '';
            var address3 = '';
            var addressee = '';
            var addresspon = '';
            var attention = '';
            var city = '';
            var state = '';
            var zip = '';
            var country = '';
        }
    }
    else{
        // loop the addresses of the customer and look the default billing address

        for ( var a = 0; searchAddressees != null && a < searchAddressees.length; a++ ) {
            var isDefaultBilling = searchAddressees[a].getValue('isdefaultbilling','address',null);
            if (isDefaultBilling == 'T'){
                var addressid = parseInt(searchAddressees[a].getValue('addressinternalid','address',null));
                var address1 = searchAddressees[a].getValue('address1','address',null);
                var address2 = searchAddressees[a].getValue('address2','address',null);
                var address3 = searchAddressees[a].getValue('address3','address',null);
                var addressee = searchAddressees[a].getValue('addressee','address',null);
                var addresspon = searchAddressees[a].getValue('custrecord_clgx_address_pon','address',null);
                var attention = searchAddressees[a].getValue('attention','address',null);
                var city = searchAddressees[a].getValue('city','address',null);
                var state = searchAddressees[a].getValue('state','address',null);
                var zip = searchAddressees[a].getValue('zipcode','address',null);
                var country = searchAddressees[a].getText('country','address',null);
                countAddress = 1;
                break;
            }
        }

    }

    objCustomer["addressid"] = addressid;
    objCustomer["address1"] = address1;
    objCustomer["address2"] = address2;
    objCustomer["address3"] = address3;
    objCustomer["aaddressee"] = addressee;
    objCustomer["addresspon"] = addresspon;
    objCustomer["attention"] = attention;
    objCustomer["city"] = city;
    objCustomer["state"] = state;
    objCustomer["zip"] = zip;
    objCustomer["country"] = country;

    return objCustomer;
}

function clgx_inv_get_contacts (customerid,iaddressee){

    // search all billing contacts of this customer
    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid','contact',null));
    arrColumns.push(new nlobjSearchColumn('entityid', 'contact',null));
    arrColumns.push(new nlobjSearchColumn('custentity_clgx_addressee_exception', 'contact',null));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('internalid',null,'is',customerid));
    arrFilters.push(new nlobjSearchFilter('contactrole','contact','is',1));
    arrFilters.push(new nlobjSearchFilter('email','contact','isnotempty'));
    arrFilters.push(new nlobjSearchFilter('isinactive','contact','is','F'));
    var searchContacts = nlapiSearchRecord('customer', null, arrFilters, arrColumns);

    var children = [];
    for ( var i = 0; searchContacts != null && i < searchContacts.length; i++ ) {
        var contactid = parseInt(searchContacts[i].getValue('internalid','contact',null)) || 0;
        var contact = searchContacts[i].getValue('entityid','contact',null) || '';
        var exception = searchContacts[i].getValue('custentity_clgx_addressee_exception','contact',null) || 'F';
        var add = 0;
        if(exception == 'T'){
            var rec = nlapiLoadRecord('contact', contactid);
            var nbr = rec.getLineItemCount('addressbook');
            for (var j = 0; j < nbr; j++) {
                var addressee = rec.getLineItemValue('addressbook', 'addressee', j+1) || '';
                if(addressee == iaddressee){
                    add = 1;
                    //nlapiLogExecution('DEBUG','debug', '| add = ' + contact + ' | ');
                }
            }
        } else {add = 1;}
        if(add){
            children.push({
                "node": contact,
                "nodetype": 'contact',
                "contactid": contactid,
                "contact": contact,
                "exception": exception,
                "faicon": 'fa fa-user Silver1',
                "leaf": true,
            });
        }
    }
    return {
        "node": 'Will be emailed to',
        "nodetype": 'contacts',
        "children": children,
        "expanded": false,
        "faicon": 'fa fa-at SteelBlue1',
        "leaf": false,
    };

}

function clgx_inv_get_date_due (terms, start, language){

    switch(terms) {
        case 1:
            var datedue = moment(start).add(15, 'days').format('M/D/YYYY');
            break;
        case 2:
            var datedue = moment(start).add(30, 'days').format('M/D/YYYY');
            break;
        case 3:
            var datedue = moment(start).add(60, 'days').format('M/D/YYYY');
            break;
        case 7:
            var datedue = moment(start).add(45, 'days').format('M/D/YYYY');
            break;
        case 8:
            var datedue = moment(start).add(40, 'days').format('M/D/YYYY');
            break;
        default:
            var datedue = start;
    }
    return getDateFormated(datedue,language);
}

function clgx_inv_get_template (locationid,language){

    if (locationid == 18){
        var template = 'DAL';
        var templateid = 48396;
    }
    else if (locationid == 25 || locationid == 27 || locationid == 28){
        var template = 'JAX';
        var templateid = 705744;
    }
    else if (locationid == 19 || locationid == 21){
        var template = 'TOR';
        var templateid = 48397;
    }
    else if (locationid == 20){
        var template = 'VAN';
        var templateid = 363975;
    }
    else if (locationid == 22){
        var template = 'MIN';
        var templateid = 88014;
    }
    else if (locationid == 26){
        var template = 'COL';
        var templateid = 1016053;
    }
    else if (locationid == 29){
        var template = 'NNJ';
        var templateid = 3131310;
    }
    else if (locationid == 23){
        var template = 'CON';
        var templateid = 89155;
    }
    else if (locationid == 24){
        if(language == 'en_US' || language == 'en'){
            var template = 'MTL_EN';
            var templateid = 89153;
        }
        else{
            var template = 'MTL_FR';
            var templateid = 89154;
        }
    }
    else{
        var template = 'DAL';
        var templateid = 48396;
    }
    objTemplate = new Object();
    objTemplate["templateid"] = templateid;
    objTemplate["template"] = template;

    return objTemplate;
}

function clgx_inv_get_labels (locationid, language){

    var arrLabels = new Array();

    if(language == 'en_US' || language == 'en'){
        arrLabels.push('English');
        arrLabels.push('Services Details');
        arrLabels.push('CATEGORY : ');
        arrLabels.push('Order');
        arrLabels.push('Item');
        arrLabels.push('Description');
        arrLabels.push('Qty');
        arrLabels.push('Rate');
        arrLabels.push('Amount');
        arrLabels.push('Subtotal : ');
        arrLabels.push('Subtotal');
        arrLabels.push('Total');
        arrLabels.push('Orders with a new CPI Rate effective ');
        if(locationid == 20){ // If Vancouver different label
            arrLabels.push('GST');
        }
        else if(locationid == 25 || locationid == 27 || locationid == 28){ // if Jacksonville
            arrLabels.push('Tax');
        }
        else if(locationid == 26 || locationid == 29){ // if Columbus or NNJ
            arrLabels.push('Sales Tax');
        }
        else{
            arrLabels.push('GST/HST');
        }
        if(locationid == 24){ // Montreal
            arrLabels.push('QST');
        }
        else{
            arrLabels.push('PST');
        }
        arrLabels.push('Account Number');
        arrLabels.push('Total Services');
        arrLabels.push('TOTAL SERVICES');
        arrLabels.push('REFERENCE DISCOUNTS');
        arrLabels.push('Service Discount');
        arrLabels.push('Cologix will apply a surcharge of approximately 2% on all payments made by credit card. This invoice is scheduled for auto payment via your credit card.');
    }
    else{
        arrLabels.push('French');
        arrLabels.push('Détails des Services');
        arrLabels.push('CATÉGORIE : ');
        arrLabels.push('Commande');
        arrLabels.push('Produit');
        arrLabels.push('Description');
        arrLabels.push('Qté');
        arrLabels.push('Prix');
        arrLabels.push('Total');
        arrLabels.push('Sous-total : ');
        arrLabels.push('Sous-total');
        arrLabels.push('Total');
        arrLabels.push('Contrat de service avec nouveau taux (IPC) en vigueur en date du ');
        arrLabels.push('TPS/TVH');
        arrLabels.push('TVQ');
        arrLabels.push('No du compte');
        arrLabels.push('Total Services');
        arrLabels.push('TOTAL SERVICES');
        arrLabels.push('REFERENCE DISCOUNTS');
        arrLabels.push('Remise sur les Services');
        arrLabels.push("Cologix appliquera un supplément d'environ 2% sur tous les paiements effectués par carte de crédit. This invoice is scheduled for auto payment via your credit card.");
    }
    return arrLabels;
}

function clgx_inv_get_totals (start,end,locationid,customerid,currencyid,caddressee,iaddressee,arrLabels,subtotal,arrIDs){

    var objTotals = new Object();
    objTotals["node"] = 'Totals';
    objTotals["nodetype"] = 'totals';

    var arrTotals = new Array();

    var objTotal = new Object();
    objTotal["node"] = arrLabels[10];
    objTotal["nodetype"] = 'subtotal';
    objTotal["subtotal"] = arrLabels[10];
    objTotal["amount"] = subtotal;
    objTotal["faicon"] = 'fa fa-usd DarkSlateGray1';
    objTotal["leaf"] = true;
    arrTotals.push(objTotal);

    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('trandate',null,'within', start,end));
    arrFilters.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', locationid));
    arrFilters.push(new nlobjSearchFilter('entity',null,'anyof', customerid));
    arrFilters.push(new nlobjSearchFilter('currency',null,'anyof',currencyid));
    if(caddressee == '' || caddressee == null){
        arrFilters.push(new nlobjSearchFilter('billaddressee',null,'is', iaddressee));
        arrFilters.push(new nlobjSearchFilter('billaddressee',null,'isnot',''));
    }
    arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof', arrIDs));
    var searchTaxes = nlapiSearchRecord('invoice','customsearch_clgx_inv_taxes',arrFilters,arrColumns);

    var sumTaxes = 0;
    var arrTaxes = new Array();
    for ( var t = 0; searchTaxes != null && t < searchTaxes.length; t++ ) {
        var objTax = new Object();
        objTax["tax"] = searchTaxes[t].getValue('memo',null,'GROUP');
        var amount = parseFloat(searchTaxes[t].getValue('fxamount',null,'SUM'));
        sumTaxes += amount;
        objTax["amount"] = amount;
        arrTaxes.push(objTax);
    }

    if(locationid != 24 && locationid != 20){ // If other than Montreal and Vancouver only one tax - total of taxes
        if(sumTaxes > 0){
            var taxname = arrLabels[13];
        }
        else{
            var taxname = 'No Tax';
        }
        var objTax = new Object();
        objTax["node"] = taxname;
        objTax["nodetype"] = 'tax';
        objTax["amount"] = sumTaxes;
        objTax["faicon"] = 'fa fa-usd DarkSlateGray1';
        objTax["leaf"] = true;
        arrTotals.push(objTax);
    }
    else{ // If Montreal or Vancouver 2 taxes
        for ( var t = 0; arrTaxes != null && t < arrTaxes.length; t++ ) {
            if(arrTaxes[t].tax == 'GST'){
                var taxname = arrLabels[13];
            }
            else if (arrTaxes[t].tax == 'PST'){
                var taxname = arrLabels[14];
            }
            else{
                var taxname = arrLabels[13];
            }
            var objTax = new Object();
            objTax["node"] = taxname;
            objTax["nodetype"] = 'tax';
            objTax["amount"] = arrTaxes[t].amount;
            objTax["faicon"] = 'fa fa-usd DarkSlateGray1';
            objTax["leaf"] = true;
            arrTotals.push(objTax);
        }
    }

    var objTotal = new Object();
    objTotal["node"] = 'Total';
    objTotal["nodetype"] = 'total';
    objTotal["amount"] = parseFloat(subtotal + sumTaxes);
    objTotal["faicon"] = 'fa fa-usd DarkSlateGray1';
    objTotal["leaf"] = true;
    arrTotals.push(objTotal);

    objTotals["children"] = arrTotals;
    objTotals["expanded"] = true;
    objTotals["faicon"] = 'fa fa-usd DarkSlateGray1';
    objTotals["leaf"] = false;

    return objTotals;
}


function clgx_inv_get_french_category (categoryid, category){

    var frenchCategory = '';
    switch(categoryid) {
        case 1:
            frenchCategory = 'Location d\'équipement';
            break;
        case 2:
            frenchCategory = 'Vente d\'équipement';
            break;
        case 3:
            frenchCategory = 'Service d\'installation';
            break;
        case 4:
            frenchCategory = 'Interconnexion';
            break;
        case 5:
            frenchCategory = 'Réseau';
            break;
        case 6:
            frenchCategory = 'Autre non Récurent';
            break;
        case 7:
            frenchCategory = 'Autre Récurent';
            break;
        case 8:
            frenchCategory = 'Électricité';
            break;
        case 9:
            frenchCategory = 'Support Technique';
            break;
        case 10:
            frenchCategory = 'Espace';
            break;
        case 11:
            frenchCategory = 'Interconnexion virtuelle';
            break;
        default:
            frenchCategory = category;
    }
    return frenchCategory;
}


function clgx_inv_get_folder (outputid,cyear,cmonth,json){

    var folder  = 2659874; // 2015 JSON folder

    var arr2020Final = [15261086,15261089,15261090,15261091,15261092,15261093,15261094,15261095,15261096,15261097,15261098,15261099,15261100];
    var arr2020JSON  = [15261087,15261101,15261102,15261103,15261104,15261105,15261106,15261107,15261108,15261109,15261110,15261111,15261112];
    var arr2020PrFrm = [15261088,15261113,15261114,15261115,15261116,15261117,15261118,15261119,15261120,15261121,15261122,15261123,15261124];

    var arr2019Final = [10662489,10662693,10662694,10662795,10662796,10662797,10662799,10662801,10662802,10662803,10662804,10662805,10662806];
    var arr2019JSON  = [10662490,10662807,10662808,10662809,10662810,10662811,10662812,10662813,10662814,10662815,10662816,10662817,10662818];
    var arr2019PrFrm = [10662491,10662819,10662820,10662821,10662822,10662823,10662824,10662825,10662826,10662827,10662828,10662829,10662830];

    var arr2018Final = [6336571,6336575,6336576,6336577,6336578,6336579,6336580,6336581,6336582,6336583,6336584,6336585,6336586];
    var arr2018JSON  = [6336572,6336687,6336688,6336689,6336690,6336691,6336692,6336693,6336694,6336695,6336696,6336697,6336698];
    var arr2018PrFrm = [6336574,6336799,6336800,6336801,6336802,6336803,6336804,6336805,6336806,6336807,6336808,6336809,6336810];

    var arr2017Final = [3821108,3821111,3821112,3821113,3821114,3821115,3821116,3821117,3821118,3821119,3821120,3821121,3821122];
    var arr2017JSON = [3821110,3821123,3821124,3821125,3821126,3821127,3821128,3821129,3821130,3821131,3821132,3821133,3821134];
    var arr2017PrFrm = [3821109,3821135,3821136,3821137,3821138,3821139,3821141,3821142,3821143,3821144,3821145,3821146,3821147];

    var arr2016Final = [2659871,2659835,2659836,2659837,2659838,2659839,2659840,2659841,2659842,2659843,2659844,2659845,2659846];
    var arr2016JSON = [2659871,2659859,2659860,2659861,2659862,2659863,2659864,2659865,2659866,2659867,2659868,2659869,2659870];
    var arr2016PrFrm = [2659871,2659847,2659848,2659849,2659850,2659851,2659852,2659853,2659854,2659855,2659856,2659857,2659858];

    var arr2015Final = [1594298,1594299,1594300,1594301,1594302,1594303,1594304,1594305,1594306,1594307,1594308,1594309];
    var arr2015PrFrm = [1594410,1594411,1594412,1594413,1594414,1594415,1594416,1594417,1594518,1594519,1594520,1594521];

    if(cyear == 2020) {
        if(json == 1) {
            folder = arr2020JSON[cmonth];
        }
        else {
            if(outputid == 1) {
                folder = arr2020Final[cmonth];
            }
            else {
                folder = arr2020PrFrm[cmonth];
            }
        }
    }
    else if(cyear == 2019) {
        if(json == 1) {
            folder = arr2019JSON[cmonth];
        }
        else {
            if(outputid == 1) {
                folder = arr2019Final[cmonth];
            }
            else {
                folder = arr2019PrFrm[cmonth];
            }
        }
    }
    else if(cyear == 2018) {
        if(json == 1) {
            folder = arr2018JSON[cmonth];
        }
        else {
            if(outputid == 1) {
                folder = arr2018Final[cmonth];
            }
            else {
                folder = arr2018PrFrm[cmonth];
            }
        }
    }
    else if(cyear == 2017){
        if(json == 1){
            folder = arr2017JSON[cmonth];
        }
        else{
            if(outputid == 1){
                folder = arr2017Final[cmonth];
            }
            else{
                folder = arr2017PrFrm[cmonth];
            }
        }
    }
    else if(cyear == 2016){
        if(json == 1){
            folder = arr2016JSON[cmonth];
        }
        else{
            if(outputid == 1){
                folder = arr2016Final[cmonth];
            }
            else{
                folder = arr2016PrFrm[cmonth];
            }
        }
    } else { // 2015
        if(json == 1){
            folder = 2659874;
        }
        else{
            if(outputid == 1){
                folder = arr2015Final[cmonth];
            }
            else{
                folder = arr2015PrFrm[cmonth];
            }
        }
    }

    return folder;
}


function clgx_inv_get_locationid (location){

    var locationid = 0;
    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("name",null,"is",location));
    var searchLocation = nlapiSearchRecord('customrecord_clgx_consolidate_locations', null, arrFilters, arrColumns);

    if (searchLocation != null){
        locationid = parseInt(searchLocation[0].getValue('internalid',null,null));
    }

    return locationid;
}

function clgx_inv_get_currency (currency){
    var currencyid = 'USD';
    if (currency == 'Canadian Dollar' || currency == 'CDN'){
        currencyid = 'CDN';
    }
    return currencyid;
}

function clgx_inv_get_file_sufix (outputid,contacts){

    if (outputid == 1){
        if(contacts > 0){
            var fileSufix = '_&@.pdf';
        }
        else{
            var fileSufix = '.pdf';
        }
    }
    else{
        var fileSufix = '_pro_forma.pdf'
    }
    return fileSufix;
}


function getDateFormated(date,language){

    var arrMonthEN = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'];
    var arrMonthFR = ['JAN','FÉV','MAR','AVR','MAI','JUN','JUL','AOÛ','SEP','OCT','NOV','DÉC'];

    var fdate = moment(date).format('D-MMM-YYYY').toUpperCase();
    var fmonth = moment(date).format('MMM').toUpperCase();
    var xmonth = _.indexOf(arrMonthEN, fmonth);

    if(language != 'en_US' && language != 'en'){
        var fdate = fdate.replace(fmonth, arrMonthFR[xmonth]);
    }

    return fdate;
}


function round(value) {
    return Number(Math.round(value+'e'+2)+'e-'+2);
}


function addCommas(nStr){
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}


function encodeHTML(str){
    var aStr = str.split(''),
        i = aStr.length,
        aRet = [];

    while (i--) {
        var iC = aStr[i].charCodeAt();
        if (iC < 65 || iC > 127 || (iC>90 && iC<97)) {
            aRet.push('&#'+iC+';');
        } else {
            aRet.push(aStr[i]);
        }
    }
    return aRet.reverse().join('');
}



function cleanStr(str){
    str = str.replace(/\#/g,"");
    return str;
}

function cleanStrDescr(str){
    str = str.replace(/\#/g,"");
    str = str.replace(/\ & /g," and ");
    return str;
}