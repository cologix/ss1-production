nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Invoices_Customers.js
//	Script Name:	CLGX_SL_Invoices_Customers
//	Script Id:		customscript_clgx_sl_inv_customers
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/24/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=579&deploy=1
//------------------------------------------------------------------------------------------------- 

function suitelet_inv_customers (request, response){
    try {
    	
        var location = request.getParameter('location');
        var locationid = request.getParameter('locationid');
        var cyear = request.getParameter('cyear');
        var cmonth = request.getParameter('cmonth');
        
    	var start = moment(cmonth + '/1/' + cyear).format('M/D/YYYY');
    	var end = moment(cmonth + '/1/' + cyear).endOf('month').format('M/D/YYYY');
    	var strmonth = moment(cmonth + '/1/' + cyear).format('MMMM');
    	
        if(locationid > 0){
        	
        	var objTree = get_customers(locationid,location,start,end);
            var objFile = nlapiLoadFile(3146734);
            var html = objFile.getValue();
            
            var objParams = new Object();
    		objParams["locationid"] = locationid;
    		objParams["location"] = location;
    		objParams["start"] = start;
    		objParams["end"] = end;
    		objParams["cmonth"] = cmonth;
    		objParams["cyear"] = cyear;
    		objParams["strmonth"] = strmonth;
    		objParams["customers"] = (objTree.records).toString();
    		objParams["invoices"] = (objTree.invoices).toString();
    		
    		html = html.replace(new RegExp('{customers}','g'), JSON.stringify(objTree));
            html = html.replace(new RegExp('{params}','g'), JSON.stringify(objParams));
        }
        else{
            var html = 'Please select an year / month / location from the left panel.';
        }
        response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function get_customers (locationid,location,start,end){

	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('trandate',null,'within', start,end));
	arrFilters.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', locationid));
	var arrCustomers = new Array();
    var searchCustomers = nlapiLoadSearch('invoice', 'customsearch_clgx_inv_customers');
    searchCustomers.addFilters(arrFilters);
	var resultSet = searchCustomers.runSearch();
	resultSet.forEachResult(function(searchResult) {
		var columns = searchResult.getAllColumns();
		var objCustomer = new Object();
		objCustomer["customerid"] = parseInt(searchResult.getValue(columns[1]));
		//objCustomer["customer"] = _.last((searchResult.getText(columns[1])).split(":")).trim();
		objCustomer["customer"] = searchResult.getText(columns[1]);
		objCustomer["invoices"] = parseInt(searchResult.getValue(columns[3]));
		var caddressee = searchResult.getValue(columns[2]);
		if(caddressee != null && caddressee != '' && caddressee != '- None -'){
			objCustomer["caddressee"] = caddressee;
		}
		else{
			objCustomer["caddressee"] = '';
		}
		arrCustomers.push(objCustomer);
		return true;
	});
	
	var arrInvoices = new Array();
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('trandate',null,'within', start,end));
	arrFilters.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', locationid));
	var searchCustomers = nlapiLoadSearch('invoice', 'customsearch_clgx_inv_invoices');
    searchCustomers.addFilters(arrFilters);
	var resultSet = searchCustomers.runSearch();
	resultSet.forEachResult(function(searchResult) {
		var objInvoice = new Object();
		objInvoice["customerid"] = parseInt(searchResult.getValue('entity',null,'GROUP'));
		objInvoice["invoiceid"] = parseInt(searchResult.getValue('internalid',null,'GROUP'));
		objInvoice["invoice"] = searchResult.getValue('tranid',null,'GROUP');
		objInvoice["soid"] = parseInt(searchResult.getValue('createdfrom',null,'GROUP'));
		objInvoice["so"] = searchResult.getText('createdfrom',null,'GROUP');
		objInvoice["batch"] = searchResult.getValue('custbody_consolidate_inv_id',null,'GROUP');
		var iaddressee = searchResult.getValue('billaddressee',null,'GROUP');
		if(iaddressee != null && iaddressee != '' && iaddressee != '- None -'){
			objInvoice["iaddressee"] = iaddressee;
		} else{
			objInvoice["iaddressee"] = '';
		}
		var source = searchResult.getText('source',null,'GROUP');
		if(source == '' || source == null || source == '- None -'){
			objInvoice["source"] = '';
		} else{
			objInvoice["source"] = source;
		}
		objInvoice["batch"] = searchResult.getValue('custbody_consolidate_inv_id',null,'GROUP');
		objInvoice["cinvoiceid"] = parseInt(searchResult.getValue('custbody_consolidate_inv_cinvoice',null,'GROUP'));
		objInvoice["cinvoice"] = searchResult.getValue('custbody_consolidate_inv_nbr',null,'GROUP');
		objInvoice["outputid"] = parseInt(searchResult.getValue('custbody_consolidate_inv_output',null,'GROUP'));
		var output = searchResult.getText('custbody_consolidate_inv_output',null,'GROUP');
		if(output == '' || output == null || output == '- None -'){
			objInvoice["output"] = '';
		}
		else{
			objInvoice["output"] = output;
		}
		arrInvoices.push(objInvoice);
		return true;
	});

	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('trandate',null,'within', start,end));
	arrFilters.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', locationid));
	var searchCustomers = nlapiLoadSearch('invoice', 'customsearch_clgx_inv_invoices_2');
    searchCustomers.addFilters(arrFilters);
	var resultSet = searchCustomers.runSearch();
	resultSet.forEachResult(function(searchResult) {
		var objInvoice = new Object();
		objInvoice["customerid"] = parseInt(searchResult.getValue('entity',null,'GROUP'));
		objInvoice["invoiceid"] = parseInt(searchResult.getValue('internalid',null,'GROUP'));
		objInvoice["invoice"] = searchResult.getValue('tranid',null,'GROUP');
		objInvoice["soid"] = parseInt(searchResult.getValue('createdfrom',null,'GROUP'));
		objInvoice["so"] = searchResult.getText('createdfrom',null,'GROUP');
		objInvoice["batch"] = searchResult.getValue('custbody_consolidate_inv_id',null,'GROUP');
		var iaddressee = searchResult.getValue('billaddressee',null,'GROUP');
		if(iaddressee != null && iaddressee != '' && iaddressee != '- None -'){
			objInvoice["iaddressee"] = iaddressee;
		} else{
			objInvoice["iaddressee"] = '';
		}
		var source = searchResult.getText('source',null,'GROUP');
		if(source == '' || source == null || source == '- None -'){
			objInvoice["source"] = '';
		} else{
			objInvoice["source"] = source;
		}
		objInvoice["batch"] = searchResult.getValue('custbody_consolidate_inv_id',null,'GROUP');
		objInvoice["cinvoiceid"] = parseInt(searchResult.getValue('custbody_consolidate_inv_cinvoice',null,'GROUP'));
		objInvoice["cinvoice"] = searchResult.getValue('custbody_consolidate_inv_nbr',null,'GROUP');
		objInvoice["outputid"] = parseInt(searchResult.getValue('custbody_consolidate_inv_output',null,'GROUP'));
		var output = searchResult.getText('custbody_consolidate_inv_output',null,'GROUP');
		if(output == '' || output == null || output == '- None -'){
			objInvoice["output"] = '';
		}
		else{
			objInvoice["output"] = output;
		}
		arrInvoices.push(objInvoice);
		return true;
	});
	
	
	
	var objTree = new Object();
	objTree["text"] = '.';
	var arrC = new Array();
	var nbrInv = 0;
	for ( var i = 0; i < arrCustomers.length; i++ ) {
		
		objC = new Object();
		objC["node"] = arrCustomers[i].customer;
		objC["nodetype"] = 'customer';
		objC["locationid"] = locationid;
		objC["location"] = location;
		objC["customerid"] = arrCustomers[i].customerid;
		objC["customer"] = (arrCustomers[i].customer).replace(/\#/g,"");
		objC["invoices"] = arrCustomers[i].invoices;
		objC["caddressee"] = arrCustomers[i].caddressee;
		objC["cinvoice"] = arrCustomers[i].invoices;
		objC["so"] = '';
		objC["soid"] = 0;
		objC["invoiceid"] = 0;
		objC["invoice"] = '';
		
		var arrCustomerInvoices = _.filter(arrInvoices, function(arr){
	        return arr.customerid === arrCustomers[i].customerid;
		});
		
		var hasOrphan = 0;
		
		var hasBlank = 0;
		var hasProForma = 0;
		var hasFinal = 0;
		
		var arrI = new Array();
		for ( var j = 0; j < arrCustomerInvoices.length; j++ ) {
			
			objI = new Object();
			objI["node"] = arrCustomerInvoices[j].invoice;
			objI["nodetype"] = 'invoice';
			objI["locationid"] = locationid;
			objI["location"] = location;
			objI["customerid"] = arrCustomers[i].customerid;
			objI["customer"] = (arrCustomers[i].customer).replace(/\#/g,"");
			objI["invoiceid"] = arrCustomerInvoices[j].invoiceid;
			objI["invoice"] = arrCustomerInvoices[j].invoice;
			objI["soid"] = arrCustomerInvoices[j].soid;
			objI["so"] = arrCustomerInvoices[j].so;
			objI["cinvoice"] = arrCustomerInvoices[j].cinvoice;
			objI["cinvoiceid"] = arrCustomerInvoices[j].cinvoiceid;
			objI["batch"] = arrCustomerInvoices[j].batch;
			objI["iaddressee"] = arrCustomerInvoices[j].iaddressee;
			objI["source"] = arrCustomerInvoices[j].source;
			objI["qtip"] = 'Created from ' + arrCustomerInvoices[j].so;
			
			objI["outputid"] = arrCustomerInvoices[j].outputid;
			objI["output"] = arrCustomerInvoices[j].output;
			
			var faicon = 'fa fa-list-ul ';
			var facolor = 'Silver1';
			
			if(arrCustomerInvoices[j].iaddressee == ''){
				faicon = 'fa fa-exclamation-triangle ';
				hasOrphan = 1;
			}
			
			if(arrCustomerInvoices[j].output == 'Pro Forma'){
				facolor = 'SteelBlue1';
				hasProForma = 1;
			}
			else if(arrCustomerInvoices[j].output == 'Final Invoice'){
				facolor = 'Green1';
				hasFinal = 1;
			}
			else{
				facolor = 'Maroon1';
				hasBlank = 1;
			}
			
			if(arrCustomers[i].caddressee == '' && arrCustomerInvoices[j].iaddressee == ''){
				facolor = 'Crimson1';
			}
			
			objI["faicon"] = faicon + facolor;
			objI["leaf"] = true;
			arrI.push(objI);
		}
		
		objC["records"] = arrCustomerInvoices.length;
		nbrInv += arrCustomerInvoices.length;
		objC["children"] = arrI;
		objC["qtip"] = arrCustomers[i].customer,
		objC["checked"] = false;
		objC["expanded"] = false;
		
		var faicon = 'fa fa-user ';
		var facolor = 'Silver1';
		
		if(arrCustomers[i].caddressee == ''){
			faicon = 'fa fa-users ';
		}
		
		if(hasBlank == 0 && hasProForma == 0 && hasFinal == 1){ // all invoices are finals
			facolor = 'Green1';
		}
		else if(hasBlank == 0 && hasProForma == 1){ // some or all pro formas
			facolor = 'SteelBlue2';
		}
		else if(hasBlank == 1){ // some invoices not consolidated
			facolor = 'Maroon1';
		}
		
		if(hasOrphan == 1 && arrCustomers[i].caddressee == ''){
			facolor = 'Crimson1';
		}
		
		
		/*
		if(hasProForma == 0 && hasFinal == 0 && hasOrphan == 0){
			facolor = 'SteelBlue2';
		}
		else if(hasOutput == 1 && hasOrphan == 0){
			facolor = 'Green1';
		}
		else if(hasOutput == 0 && hasOrphan == 0){
			facolor = 'Maroon1';
		}
		else if(hasOrphan == 1 && arrCustomers[i].caddressee == ''){
			facolor = 'Crimson1';
		}
		else{
			facolor = 'SteelBlue2';
		}
		*/
		
		objC["faicon"] = faicon + facolor;
		
		
		objC["leaf"] = false;
		arrC.push(objC);
	}
	objTree["invoices"] = nbrInv;
	objTree["records"] = arrCustomers.length;
	objTree["children"] = arrC;
	
	return objTree;
	
}


