nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Invoices_Consolidate.js
//	Script Name:	CLGX_SL_Invoices_Consolidate
//	Script Id:		customscript_clgx_sl_inv_consolidate
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/23/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=578&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_inv_consolidate (request, response){
    try {

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        arrColumns.push(new nlobjSearchColumn('name',null,null).setSort(false));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter('isinactive',null,'is', 'F'));
        arrFilters.push(new nlobjSearchFilter('internalid',null,'noneof', [19,23]));
        var searchLocations = nlapiSearchRecord('customrecord_clgx_consolidate_locations', null, arrFilters, arrColumns);

        var arrLocations = new Array();
        for ( var i = 0; searchLocations != null && i < searchLocations.length; i++ ) {
            var objLocation = new Object();
            objLocation["locationid"] = parseInt(searchLocations[i].getValue('internalid',null,null));
            objLocation["location"] = searchLocations[i].getValue('name',null,null);
            arrLocations.push(objLocation);
        }

        var objFile = nlapiLoadFile(5698058);
        var html = objFile.getValue();
        html = html.replace(new RegExp('{locations}','g'), get_locations(arrLocations));
        html = html.replace(new RegExp('{queue}','g'), get_queue());
        html = html.replace(new RegExp('{CCCustomers}','g'), get_ccards_customers());
        response.write( html );
    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function get_locations (arrLocations){

    var start = moment().startOf('month').subtract(6, 'months').format('M/D/YYYY');

    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('trandate',null,'after', start));
    var searchLocations = nlapiSearchRecord('invoice', 'customsearch_clgx_inv_months_locations', arrFilters, arrColumns);
    var arrLocations = new Array();
    for ( var i = 0; searchLocations != null && i < searchLocations.length; i++ ) {

        var searchLocation = searchLocations[i];
        var columns = searchLocation.getAllColumns();

        var objLocation = new Object();
        var cyear =  searchLocation.getValue(columns[0]);
        objLocation["cyear"] = parseInt(cyear);
        var arrMonth = (searchLocation.getValue(columns[1])).split('-');
        var cmonth = arrMonth[1].replace(/^0+/, '');
        objLocation["cmonth"] = parseInt(cmonth);
        objLocation["locationid"] = parseInt(searchLocation.getValue(columns[2]));
        objLocation["location"] = searchLocation.getText(columns[2]);
        objLocation["customers"] = parseInt(searchLocation.getValue(columns[3]));
        objLocation["invoices"] = parseInt(searchLocation.getValue(columns[4]));
        arrLocations.push(objLocation);
    }
    var arrYears = _.compact(_.uniq(_.pluck(arrLocations, 'cyear')));
    arrYears = _.sortBy(arrYears, function(num){ return num; }).reverse();

    var objTree = new Object();
    objTree["text"] = '.';
    var arrY = new Array();
    for ( var y = 0; y < arrYears.length; y++ ) {

        objY = new Object();
        objY["node"] = arrYears[y].toString();
        objY["nodetype"] = 'year';
        objY["locationid"] = 0;
        objY["location"] = '';
        objY["cyear"] = arrYears[y];
        objY["cmonth"] = 0;

        var arrYearMonths = _.filter(arrLocations, function(arr){
            return arr.cyear === arrYears[y];
        });
        var arrMonths = _.compact(_.uniq(_.pluck(arrYearMonths, 'cmonth')));
        arrMonths = _.sortBy(arrMonths, function(num){ return num; }).reverse();

        var arrM = new Array();
        for ( var m = 0; m < arrMonths.length; m++ ) {

            objM = new Object();
            objM["node"] = moment(arrMonths[m].toString() + '/1/' + arrYears[y].toString()).format('MMMM');
            objM["nodetype"] = 'month';
            objM["locationid"] = 0;
            objM["location"] = '';
            objM["cyear"] = arrYears[y];
            objM["cmonth"] = arrMonths[m];

            var arrMonthLocations = _.filter(arrLocations, function(arr){
                return (arr.cyear == arrYears[y]) && arr.cmonth == arrMonths[m];
            });
            arrMonthLocations = _.sortBy(arrMonthLocations, function(obj){ return obj.location;});

            var arrL = new Array();
            for ( var l = 0; l < arrMonthLocations.length; l++ ) {
                objL = new Object();
                objL["node"] = arrMonthLocations[l].location;
                objL["nodetype"] = 'location';
                objL["locationid"] = arrMonthLocations[l].locationid;
                objL["location"] = arrMonthLocations[l].location;
                objL["cyear"] = arrYears[y];
                objL["cmonth"] = arrMonths[m];
                objL["customers"] = arrMonthLocations[l].customers;
                objL["invoices"] = arrMonthLocations[l].invoices;
                objL["qtip"] = 'Market ' + arrMonthLocations[l].location + ' has ' + arrMonthLocations[l].customers + ' customers and ' + arrMonthLocations[l].invoices + ' invoices.',
                    //objL["iconCls"] = 'location';
                    objL["faicon"] = 'fa fa-building-o SteelBlue';
                objL["leaf"] = true;
                arrL.push(objL);
            }
            objM["children"] = arrL;
            if(m == 0){
                objM["expanded"] = true;
            }
            else{
                objM["expanded"] = false;
            }
            //objM["iconCls"] = 'month';
            objM["faicon"] = 'fa fa-calendar-o SteelBlue1';
            objM["leaf"] = false;
            arrM.push(objM);
        }
        //arrM = _.sortBy(arrM, function(obj){ return obj.entityid;}).reverse();
        objY["children"] = arrM;
        if(y == 0){
            objY["expanded"] = true;
        }
        else{
            objY["expanded"] = false;
        }
        //objY["iconCls"] = 'year';
        objY["leaf"] = false;
        objY["faicon"] = 'fa fa-calendar SteelBlue2';
        arrY.push(objY);
    }
    //arrY = _.sortBy(arrY, function(obj){ return obj.entityid;}).reverse();
    objTree["children"] = arrY;

    return JSON.stringify(objTree);
}


function get_queue (){

    //var arrOutput = ["Final Invoice","Pro Forma"];
    var arrMonths = ["January","February","March","April","May","June","July","August","September","October","November","December"];

    var arrCInvoices = new Array();
    var searchCInvoices = nlapiLoadSearch('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_inv_ci_queue');
    var resultSet = searchCInvoices.runSearch();
    resultSet.forEachResult(function(searchResult) {
        var objCInvoice = new Object();

        objCInvoice["invoice"] = searchResult.getValue('custrecord_clgx_consol_inv_batch',null,null);
        objCInvoice["output"] = searchResult.getText('custrecord_clgx_consol_inv_output',null,null)

        objCInvoice["customerid"] = parseInt(searchResult.getValue('custrecord_clgx_consol_inv_customer',null,null));
        objCInvoice["customer"] = searchResult.getText('custrecord_clgx_consol_inv_customer',null,null);
        objCInvoice["location"] = searchResult.getValue('custrecord_clgx_consol_inv_market',null,null);
        objCInvoice["currency"] = searchResult.getValue('custrecord_clgx_consol_inv_currency',null,null);

        objCInvoice["cyear"] = parseInt(searchResult.getValue('custrecord_clgx_consol_inv_year',null,null));
        var cmonth = parseInt(searchResult.getValue('custrecord_clgx_consol_inv_month',null,null));
        objCInvoice["cmonth"] = arrMonths[cmonth + 1];
        objCInvoice["dyear"] = parseInt(searchResult.getValue('custrecord_clgx_consol_inv_year_display',null,null));
        var dmonth = parseInt(searchResult.getValue('custrecord_clgx_consol_inv_month_display',null,null));
        objCInvoice["dmonth"] = arrMonths[dmonth + 1];

        arrCInvoices.push(objCInvoice);
        return true;
    });

    return JSON.stringify(arrCInvoices);

}

function get_ccards_customers (){

    var columns = new Array();
    columns.push(new nlobjSearchColumn('custrecord_clgx_ccards_customer',null,'GROUP'));
    var filters = new Array();
    var obj = new Object();
    obj["text"] = '.';
    filters.push(new nlobjSearchFilter('custrecord_clgx_ccards_customer',null,'noneof', '@NONE@'));
    var search = nlapiSearchRecord('customrecord_clgx_ccards_processing', null, filters, columns);
    var arr = new Array();

    var objB =new Object();
    objB["node"] = 'Customers';
    objB["nodetype"] = 'parent';
    for ( var i = 0; search != null && i < search.length; i++ ) {
        var objC =new Object();
        objC["node"]=search[i].getText('custrecord_clgx_ccards_customer',null,'GROUP');
        objC["idC"]=parseInt(search[i].getValue('custrecord_clgx_ccards_customer',null,'GROUP'));
        objC["customerC"]=search[i].getText('custrecord_clgx_ccards_customer',null,'GROUP');
        objC["nodetype"] = 'child';
        objC["leaf"]="true";

        arr.push(objC);
    }

    objB["children"] = arr;
    var arrB=new Array();
    arrB.push(objB);
    var objB =new Object();
    objB["node"] = 'CC Processing Queue';
    objB["nodetype"] = 'proc';
    objB["leaf"]="true";
    objB["children"] = [];
    arrB.push(objB);
    obj["children"] = arrB;
    obj["expanded"]="false";
    obj["leaf"]="false";
    obj["faicon"]="fa fa-calendar SteelBlue2";
    return JSON.stringify(obj);
}

/*
 function get_customers (arrLocations){

 var arrCustomers = new Array();
 var searchCustomers = nlapiLoadSearch('invoice', 'customsearch_clgx_inv_customers');
 var resultSet = searchCustomers.runSearch();
 resultSet.forEachResult(function(searchResult) {
 var objCustomer = new Object();
 objCustomer["locationid"] = parseInt(searchResult.getValue('custbody_clgx_consolidate_locations',null,'GROUP'));
 objCustomer["location"] = searchResult.getText('custbody_clgx_consolidate_locations',null,'GROUP');
 objCustomer["customerid"] = parseInt(searchResult.getValue('entity',null,'GROUP'));
 objCustomer["customer"] = searchResult.getText('entity',null,'GROUP');
 arrCustomers.push(objCustomer);
 return true;
 });

 var objTree = new Object();
 objTree["text"] = '.';
 var arrL = new Array();
 for ( var i = 0; i < arrLocations.length; i++ ) {

 objL = new Object();
 objL["node"] = arrLocations[i].location;
 objL["nodetype"] = 'location';

 var arrLocationCustomers = _.filter(arrCustomers, function(arr){
 return arr.locationid === arrLocations[i].locationid;
 });
 var arrC = new Array();
 for ( var j = 0; j < arrLocationCustomers.length; j++ ) {

 objC = new Object();
 objC["node"] = _.last((arrLocationCustomers[j].customer).split(":")).trim();
 objC["nodetype"] = 'customer';
 objC["locationid"] = arrLocations[i].locationid;
 objC["location"] = arrLocations[i].location;
 objC["iconCls"] = 'customer';
 objC["leaf"] = true;
 arrC.push(objC);
 }
 objL["records"] = arrLocationCustomers.length;
 objL["children"] = arrC;
 objL["expanded"] = false;
 objL["iconCls"] = 'location';
 objL["leaf"] = false;
 arrL.push(objL);
 }
 objTree["records"] = arrLocations.length;
 objTree["children"] = arrL;

 return JSON.stringify(objTree);
 }
 */
