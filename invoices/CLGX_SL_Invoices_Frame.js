nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Invoices_Frame.js
//	Script Name:	CLGX_SL_Invoices_Frame
//	Script Id:		customscript_clgx_sl_inv_frame
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/23/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=577&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_so_inv_frame (request, response){
	try {
		var formFrame = nlapiCreateForm('Consolidate Invoices');
		var fieldFrame = formFrame.addField('custpage_clgx_frame_html','inlinehtml', null, null, null);
		var frameHTML = '<iframe name="ci1" id="ci1" src="/app/site/hosting/scriptlet.nl?script=578&deploy=1" height="600px" width="1235px" frameborder="0" scrolling="no"></iframe>';
		fieldFrame.setDefaultValue(frameHTML);
		response.writePage( formFrame );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}