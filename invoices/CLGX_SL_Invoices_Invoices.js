nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Invoices_Invoices.js
//	Script Name:	CLGX_SL_Invoices_Invoices
//	Script Id:		customscript_clgx_sl_inv_invoices
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/30/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=580&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_inv_invoices (request, response){
    try {
    	
    	var location = request.getParameter('location');
        var locationid = request.getParameter('locationid');
        var customer = request.getParameter('customer');
        var customerid = request.getParameter('customerid');
        var invoice = request.getParameter('invoice');
        var invoiceid = request.getParameter('invoiceid');
        var cyear = request.getParameter('cyear');
        var cmonth = request.getParameter('cmonth');
        var strmonth = request.getParameter('strmonth');
        
    	var start = moment(cmonth + '/1/' + cyear).format('M/D/YYYY');
    	var end = moment(cmonth + '/1/' + cyear).endOf('month').format('M/D/YYYY');
    	
    	if(locationid > 0){
        	
        	var objFile = nlapiLoadFile(3160381);
            var html = objFile.getValue();
            
            var objParams = new Object();
    		objParams["locationid"] = locationid;
    		objParams["location"] = location;
    		objParams["customerid"] = customerid;
    		objParams["invoice"] = invoice;
    		objParams["invoiceid"] = invoiceid;
    		objParams["customer"] = customer;
    		objParams["start"] = start;
    		objParams["end"] = end;
    		objParams["cmonth"] = cmonth;
    		objParams["cyear"] = cyear;
    		objParams["strmonth"] = strmonth;
    		
    		html = html.replace(new RegExp('{params}','g'), JSON.stringify(objParams));
    		html = html.replace(new RegExp('{invoices}','g'), JSON.stringify(get_invoices (locationid,location,customerid,customer,invoiceid,start,end)));
    		//html = html.replace(new RegExp('{cinvoices}','g'), JSON.stringify(get_cinvoices (locationid,customerid,invoiceid,cyear,cmonth,start,end)));
    		//html = html.replace(new RegExp('{history}','g'), JSON.stringify(get_history (customerid,cyear,cmonth)));
    		
        }
        else{
            var html = 'Please select a customer or an invoice from the left panel.';
        }
    	
    	var usageConsumtion = 1000 - parseInt(nlapiGetContext().getRemainingUsage());
    	nlapiLogExecution('DEBUG','usageConsumtion', '| Customer - ' + customer + '| CustomerID - ' + customerid + ' | All Usage - ' + usageConsumtion) + ' |'; 
    	
        response.write( html );
        
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}



function get_invoices (locationid,location,customerid,customer,invoiceid,start,end){
	
	var caddressee = nlapiLookupField('customer', customerid, 'billaddressee');
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('trandate',null,'within', start,end));
	arrFilters.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', locationid));
	arrFilters.push(new nlobjSearchFilter('entity',null,'anyof', customerid));
	if(invoiceid > 0){
		arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof', invoiceid));
	}
	if(caddressee == '' || caddressee == null){
		arrFilters.push(new nlobjSearchFilter('billaddressee',null,'isnot',''));
	}
	var arrItems = new Array();
    var searchItems = nlapiLoadSearch('invoice', 'customsearch_clgx_inv_items');
    searchItems.addColumns(arrColumns);
    searchItems.addFilters(arrFilters);
	var resultSet = searchItems.runSearch();
	resultSet.forEachResult(function(searchResult) {
		var objItem = new Object();
		objItem["invoiceid"] = parseInt(searchResult.getValue('internalid',null,null));
		objItem["invoice"] = searchResult.getValue('tranid',null,null);
		objItem["itemid"] = parseInt(searchResult.getValue('item',null,null));
		objItem["item"] = searchResult.getText('item',null,null);
		objItem["soid"] = parseInt(searchResult.getValue('createdfrom',null,null));
		objItem["so"] = (searchResult.getText('createdfrom',null,null)).replace(/\Service Order #/g,"");
		objItem["memo"] = searchResult.getValue('memo',null,null);
		objItem["categoryid"] = parseInt(searchResult.getValue('custcol_cologix_invoice_item_category',null,null));
		objItem["category"] = searchResult.getText('custcol_cologix_invoice_item_category',null,null);
		objItem["quantity"] = parseFloat(searchResult.getValue('quantity',null,null));
		objItem["rate"] = parseFloat(searchResult.getValue('rate',null,null));
		objItem["amount"] = parseFloat(searchResult.getValue('amount',null,null));
		var source = searchResult.getText('source',null,null);
		if(source == '' || source == null || source == '- None -'){
			objItem["source"] = '';
		}
		else{
			objItem["source"] = source;
		}
		
		objItem["cinvoiceid"] = parseInt(searchResult.getValue('custbody_consolidate_inv_cinvoice',null,null));
		objItem["cinvoice"] = searchResult.getValue('custbody_consolidate_inv_nbr',null,null);
		objItem["outputid"] = parseInt(searchResult.getValue('custbody_consolidate_inv_output',null,null));
		var output = searchResult.getText('custbody_consolidate_inv_output',null,null);
		if(output == '' || output == null || output == '- None -'){
			objItem["output"] = '';
		}
		else{
			objItem["output"] = output;
		}
		
		arrItems.push(objItem);
		return true;
	});

	var arrInvoices = _.compact(_.uniq(_.pluck(arrItems, 'invoiceid')));
	
	var objTree = new Object();
	objTree["text"] = '.';
	var arrInv = new Array();
	for ( var i = 0; i < arrInvoices.length; i++ ) {
		
		var objInvoice = _.find(arrItems, function(arr){ return (arr.invoiceid == arrInvoices[i]) ; });
		
		objInv = new Object();
		objInv["node"] = objInvoice.invoice;
		objInv["nodetype"] = 'invoice';
		objInv["invoiceid"] = objInvoice.invoiceid;
		objInv["invoice"] = objInvoice.invoice;
		objInv["soid"] = objInvoice.soid;
		objInv["so"] = objInvoice.so;
		objInv["cinvoice"] = objInvoice.cinvoice;
		objInv["cinvoiceid"] = objInvoice.cinvoiceid;
		objInv["outputid"] = objInvoice.outputid;
		objInv["output"] = objInvoice.output;

		objInv["batch"] = objInvoice.batch;
		objInv["source"] = objInvoice.source;
		objInv["itemid"] = '';
		objInv["item"] = 0;
		
		var arrInvoiceItems = _.filter(arrItems, function(arr){
	        return arr.invoiceid === objInvoice.invoiceid;
		});
		var arrI = new Array();
		for ( var j = 0; j < arrInvoiceItems.length; j++ ) {
			
			objI = new Object();
			objI["node"] = arrInvoiceItems[j].item;
			objI["nodetype"] = 'item';
			objI["invoiceid"] = objInvoice.invoiceid;
			objI["invoice"] = objInvoice.invoice;
			objI["soid"] = objInvoice.soid;
			objI["so"] = objInvoice.so;
			objI["itemid"] = arrInvoiceItems[j].itemid;
			objI["item"] = arrInvoiceItems[j].item;
			objI["memo"] = arrInvoiceItems[j].memo; // to clean up of " ?
			objI["categoryid"] = arrInvoiceItems[j].categoryid;
			objI["category"] = arrInvoiceItems[j].category;
			objI["quantity"] = arrInvoiceItems[j].quantity;
			objI["rate"] = arrInvoiceItems[j].rate;
			objI["amount"] = arrInvoiceItems[j].amount;
			objI["faicon"] = 'fa fa-cog Silver1';
			objI["leaf"] = true;
			arrI.push(objI);
		}
		objInv["children"] = arrI;
		objInv["checked"] = true;
		objInv["expanded"] = false;
		
		if(objInvoice.output == 'Pro Forma'){
			objInv["faicon"] = 'fa fa-list-ul SteelBlue1';
		}
		else if(objInvoice.output == 'Final Invoice'){
			objInv["faicon"] = 'fa fa-list-ul Green1';
		}
		else{
			objInv["faicon"] = 'fa fa-list-ul Crimson1';
		}
		
		objInv["leaf"] = false;
		arrInv.push(objInv);
	}
	objTree["children"] = arrInv;
	
	return objTree;

}

