nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Invoices_Compare.js
//	Script Name:	CLGX_SL_Invoices_Compare
//	Script Id:		customscript_clgx_sl_inv_compare
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		02/09/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=619&deploy=1
//-------------------------------------------------------------------------------------------------
function suitelet_inv_compare (request, response){
    try {

        var location = request.getParameter('location');
        var periodA = request.getParameter('periodA');
        var periodB = request.getParameter('periodB');
        var view = request.getParameter('view');

        var start = moment().endOf('month').subtract(17, 'months').format('M/D/YYYY');
        var arrPeriods = get_periods (start);
        var arrLocations = get_locations (start);

        var objFile = nlapiLoadFile(3311742);
        if(view == 'print') objFile =  nlapiLoadFile(3517220);
        var html = objFile.getValue();
        html = html.replace(new RegExp('{periods}','g'), JSON.stringify(arrPeriods));
        html = html.replace(new RegExp('{locations}','g'), JSON.stringify(arrLocations));
        html = html.replace(new RegExp('{view}','g'), view);

        if(location > 0 && periodA > 0 && periodB > 0){
            var objTree = get_customers (periodA, periodB, location);
        }
        else{
            var objTree = new Object();
            objTree["text"] = '.';
            objTree["location"] = 0;
            objTree["periodA"] = 0;
            objTree["periodB"] = 0;
            objTree["children"] = [];
        }
        html = html.replace(new RegExp('{customers}','g'), JSON.stringify(objTree));
        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function get_locations (start){

    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("trandate",null,"after",start));
    var searchCLocations = nlapiSearchRecord('invoice', 'customsearch_clgx_cinvs_stare_clocations', arrFilters, arrColumns);
    var arrCLocations = new Array();
    for ( var l = 0; searchCLocations != null && l < searchCLocations.length; l++ ) {
        var objCLocation = new Object();
        objCLocation["value"] = parseInt(searchCLocations[l].getValue('custbody_clgx_consolidate_locations',null,'GROUP'));
        objCLocation["text"] = searchCLocations[l].getText('custbody_clgx_consolidate_locations',null,'GROUP');
        arrCLocations.push(objCLocation);
    }
    return arrCLocations;
}

function get_periods (start){

    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("trandate",null,"after",start));
    var searchPeriods = nlapiSearchRecord('invoice', 'customsearch_clgx_cinvs_stare_periods', arrFilters, arrColumns);
    var arrPeriods = new Array();
    for ( var p = 0; searchPeriods != null && p < searchPeriods.length; p++ ) {
        var objPeriod = new Object();
        objPeriod["value"] = parseInt(searchPeriods[p].getValue('postingperiod',null,'GROUP'));
        objPeriod["text"] = searchPeriods[p].getText('postingperiod',null,'GROUP');
        arrPeriods.push(objPeriod);
    }
    return arrPeriods;
}

function get_customers (periodA, periodB, location){


// Get Consolidated invoices Period A ======================================================================================================================================================

    var arrCInvoices = new Array();
    var arrInvoices = new Array();

    //var arrFilters = [['custbody_clgx_consolidate_locations', 'anyof', location], 'and', [ [ 'postingperiod', 'anyof', periodA ],'or',[ 'postingperiod', 'anyof', periodB ]]];
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("custbody_clgx_consolidate_locations",null,"anyof",location));
    arrFilters.push(new nlobjSearchFilter("postingperiod",null,"anyof",periodA));
    var searchCInvoices = nlapiLoadSearch('invoice', 'customsearch_clgx_cinvs_stare_cinvs');
    searchCInvoices.addFilters(arrFilters);
    var resultSet = searchCInvoices.runSearch();
    resultSet.forEachResult(function(searchResult) {
        var columns = searchResult.getAllColumns();
        var objCInvoice = new Object();
        objCInvoice["customerid"] = parseInt(searchResult.getValue('entity',null,'GROUP'));
        objCInvoice["customer"] = searchResult.getText('entity',null,'GROUP');
        //objCInvoice["addressee"] = searchResult.getValue('billaddressee',null,'GROUP');
        objCInvoice["addressee"] = searchResult.getValue(columns[1]);
        objCInvoice["cinvoiceid"] = parseInt(searchResult.getValue('custbody_consolidate_inv_cinvoice',null,'GROUP'));
        objCInvoice["cinvoice"] = searchResult.getValue('custbody_consolidate_inv_nbr',null,'GROUP');
        objCInvoice["clocationid"] = parseInt(searchResult.getValue('custbody_clgx_consolidate_locations',null,'GROUP'));
        objCInvoice["clocation"] = searchResult.getText('custbody_clgx_consolidate_locations',null,'GROUP');
        objCInvoice["currencyid"] = parseInt(searchResult.getValue('currency',null,'GROUP'));
        objCInvoice["currency"] = searchResult.getText('currency',null,'GROUP');
        objCInvoice["periodid"] = parseInt(searchResult.getValue('postingperiod',null,'GROUP'));
        objCInvoice["period"] = searchResult.getText('postingperiod',null,'GROUP');
        objCInvoice["periodname"] = 'A';
        objCInvoice["total"] = parseFloat(searchResult.getValue(columns[7]));
        objCInvoice["power_usage"] = parseFloat(searchResult.getValue(columns[8]));
        objCInvoice["banwidth_burst"] = parseFloat(searchResult.getValue(columns[9]));
        objCInvoice["equipment_rental"] = parseFloat(searchResult.getValue(columns[10]));
        objCInvoice["equipment_sales"] = parseFloat(searchResult.getValue(columns[11]));
        objCInvoice["install"] = parseFloat(searchResult.getValue(columns[12]));
        objCInvoice["xc"] = parseFloat(searchResult.getValue(columns[13]));
        objCInvoice["network"] = parseFloat(searchResult.getValue(columns[14]));
        objCInvoice["other_non_recurring"] = parseFloat(searchResult.getValue(columns[15]));
        objCInvoice["other_recurring"] = parseFloat(searchResult.getValue(columns[16]));
        objCInvoice["power"] = parseFloat(searchResult.getValue(columns[17]));
        objCInvoice["remote_hands"] = parseFloat(searchResult.getValue(columns[18]));
        objCInvoice["space"] = parseFloat(searchResult.getValue(columns[19]));
        objCInvoice["vxc"] = parseFloat(searchResult.getValue(columns[20]));
        objCInvoice["disaster"] = parseFloat(searchResult.getValue(columns[21]));
        objCInvoice["managed"] = parseFloat(searchResult.getValue(columns[22]));
        objCInvoice["managed_services_usage"] = parseFloat(searchResult.getValue(columns[23]));
        objCInvoice["high_density_usage_premium"] = parseFloat(searchResult.getValue(columns[24]));
        arrCInvoices.push(objCInvoice);
        return true;
    });

// Get Consolidated invoices Period B ======================================================================================================================================================

    //var arrCInvoicesB = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("custbody_clgx_consolidate_locations",null,"anyof",location));
    arrFilters.push(new nlobjSearchFilter("postingperiod",null,"anyof",periodB));
    var searchCInvoices = nlapiLoadSearch('invoice', 'customsearch_clgx_cinvs_stare_cinvs');
    searchCInvoices.addFilters(arrFilters);
    var resultSet = searchCInvoices.runSearch();
    resultSet.forEachResult(function(searchResult) {
        var columns = searchResult.getAllColumns();
        var objCInvoice = new Object();
        objCInvoice["customerid"] = parseInt(searchResult.getValue('entity',null,'GROUP'));
        objCInvoice["customer"] = searchResult.getText('entity',null,'GROUP');
        //objCInvoice["addressee"] = searchResult.getValue('billaddressee',null,'GROUP');
        objCInvoice["addressee"] = searchResult.getValue(columns[1]);
        objCInvoice["cinvoiceid"] = parseInt(searchResult.getValue('custbody_consolidate_inv_cinvoice',null,'GROUP'));
        objCInvoice["cinvoice"] = searchResult.getValue('custbody_consolidate_inv_nbr',null,'GROUP');
        objCInvoice["clocationid"] = parseInt(searchResult.getValue('custbody_clgx_consolidate_locations',null,'GROUP'));
        objCInvoice["clocation"] = searchResult.getText('custbody_clgx_consolidate_locations',null,'GROUP');
        objCInvoice["currencyid"] = parseInt(searchResult.getValue('currency',null,'GROUP'));
        objCInvoice["currency"] = searchResult.getText('currency',null,'GROUP');
        objCInvoice["periodid"] = parseInt(searchResult.getValue('postingperiod',null,'GROUP'));
        objCInvoice["period"] = searchResult.getText('postingperiod',null,'GROUP');
        objCInvoice["periodname"] = 'B';
        objCInvoice["total"] = parseFloat(searchResult.getValue(columns[7]));
        objCInvoice["power_usage"] = parseFloat(searchResult.getValue(columns[8]));
        objCInvoice["banwidth_burst"] = parseFloat(searchResult.getValue(columns[9]));
        objCInvoice["equipment_rental"] = parseFloat(searchResult.getValue(columns[10]));
        objCInvoice["equipment_sales"] = parseFloat(searchResult.getValue(columns[11]));
        objCInvoice["install"] = parseFloat(searchResult.getValue(columns[12]));
        objCInvoice["xc"] = parseFloat(searchResult.getValue(columns[13]));
        objCInvoice["network"] = parseFloat(searchResult.getValue(columns[14]));
        objCInvoice["other_non_recurring"] = parseFloat(searchResult.getValue(columns[15]));
        objCInvoice["other_recurring"] = parseFloat(searchResult.getValue(columns[16]));
        objCInvoice["power"] = parseFloat(searchResult.getValue(columns[17]));
        objCInvoice["remote_hands"] = parseFloat(searchResult.getValue(columns[18]));
        objCInvoice["space"] = parseFloat(searchResult.getValue(columns[19]));
        objCInvoice["vxc"] = parseFloat(searchResult.getValue(columns[20]));
        objCInvoice["disaster"] = parseFloat(searchResult.getValue(columns[21]));
        objCInvoice["managed"] = parseFloat(searchResult.getValue(columns[22]));
        objCInvoice["managed_services_usage"] = parseFloat(searchResult.getValue(columns[23]));
        objCInvoice["high_density_usage_premium"] = parseFloat(searchResult.getValue(columns[24]));
        arrCInvoices.push(objCInvoice);
        return true;
    });


// Get All invoices Period A ======================================================================================================================================================

    var arrColumns = new Array();

    var arrFilters = new Array();
    arrFilters[0] = new nlobjSearchFilter("custbody_clgx_consolidate_locations",null,"anyof",location);
    arrFilters[1] = new nlobjSearchFilter("postingperiod",null,"anyof",periodA);
    while (true) {
        searchInvoices = nlapiSearchRecord('invoice', 'customsearch_clgx_cinvs_stare_invs', arrFilters, arrColumns);
        if (!searchInvoices) {
            break;
        }
        for (var i in searchInvoices) {
            var objInvoice = new Object();
            //objInvoice["cinvoiceid"] = parseInt(searchInvoices[i].getValue('custbody_consolidate_inv_id',null,'GROUP'));
            objInvoice["cinvoice"] = searchInvoices[i].getValue('custbody_consolidate_inv_nbr',null,null);
            var invoiceid = parseInt(searchInvoices[i].getValue('internalid',null,null));
            objInvoice["invoiceid"] = invoiceid;
            objInvoice["invoice"] = searchInvoices[i].getValue('transactionnumber',null,null);
            objInvoice["status"] = searchInvoices[i].getValue('status',null,null);
            objInvoice["total"] = parseFloat(searchInvoices[i].getValue('fxamount',null,null));

            arrInvoices.push(objInvoice);
        }
        if (searchInvoices.length < 1000) {
            break;
        }
        arrFilters[2] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", invoiceid);
    }

// Get All invoices Period B ======================================================================================================================================================

    var arrFilters = new Array();
    arrFilters[0] = new nlobjSearchFilter("custbody_clgx_consolidate_locations",null,"anyof",location);
    arrFilters[1] = new nlobjSearchFilter("postingperiod",null,"anyof",periodB);
    while (true) {
        searchInvoices = nlapiSearchRecord('invoice', 'customsearch_clgx_cinvs_stare_invs', arrFilters, arrColumns);
        if (!searchInvoices) {
            break;
        }
        for (var i in searchInvoices) {
            var objInvoice = new Object();
            //objInvoice["cinvoiceid"] = parseInt(searchInvoices[i].getValue('custbody_consolidate_inv_id',null,'GROUP'));
            objInvoice["cinvoice"] = searchInvoices[i].getValue('custbody_consolidate_inv_nbr',null,null);
            var invoiceid = parseInt(searchInvoices[i].getValue('internalid',null,null));
            objInvoice["invoiceid"] = invoiceid;
            objInvoice["invoice"] = searchInvoices[i].getValue('transactionnumber',null,null);
            objInvoice["status"] = searchInvoices[i].getValue('status',null,null);
            objInvoice["total"] = parseFloat(searchInvoices[i].getValue('fxamount',null,null));
            arrInvoices.push(objInvoice);
        }
        if (searchInvoices.length < 1000) {
            break;
        }
        arrFilters[2] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", invoiceid);
    }

// Get All Consolidated Invoices Records ======================================================================================================================================================

    var arrCInvRecIDs = _.compact(_.uniq(_.pluck(arrCInvoices, 'cinvoiceid')));
    var arrCInvRec = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrCInvRecIDs));
    //  arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_output",null,"anyof",1));
    var searchCInvRec = nlapiLoadSearch('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_cinvs_stare_cinvs_rec');
    searchCInvRec.addFilters(arrFilters);
    var resultSet = searchCInvRec.runSearch();
    resultSet.forEachResult(function(searchResult) {
        var columns = searchResult.getAllColumns();
        var objCInvRec = new Object();
        objCInvRec["cinvoiceid"] = parseInt(searchResult.getValue('internalid',null,null));
        objCInvRec["cinvoice"] = searchResult.getValue('custrecord_clgx_consol_inv_batch',null,null);
        objCInvRec["jsonid"] = parseInt(searchResult.getValue('custrecord_clgx_consol_inv_json_file_id',null,null));
        objCInvRec["pdfid"] = parseInt(searchResult.getValue('custrecord_clgx_consol_inv_pdf_file_id',null,null));
        objCInvRec["csubtotal"] = parseFloat(searchResult.getValue('custrecord_clgx_consol_inv_subtotal',null,null));
        objCInvRec["ctotal"] = parseFloat(searchResult.getValue('custrecord_clgx_consol_inv_total',null,null));
        //var strInvoices = searchResult.getValue('custrecord_clgx_consol_inv_invoices',null,null);
        //var arrInvoices = strInvoices.split(";");
        //objCInvRec["invoices"] = arrInvoices;
        arrCInvRec.push(objCInvRec);
        return true;
    });

// Build tree ======================================================================================================================================================

    var arrCustomersIDs = _.compact(_.uniq(_.pluck(arrCInvoices, 'customerid')));
    var arrCustomers = new Array();
    for ( var c = 0; arrCustomersIDs != null && c < arrCustomersIDs.length; c++ ) {

        var arrCustAddressees = _.filter(arrCInvoices, function(arr){
            return (arr.customerid == arrCustomersIDs[c]);
        });
        var arrAddressees = _.compact(_.uniq(_.pluck(arrCustAddressees, 'addressee')));

        for ( var a = 0; arrAddressees != null && a < arrAddressees.length; a++ ) {

            var arrCustPeriods = _.filter(arrCInvoices, function(arr){
                return (arr.customerid == arrCustomersIDs[c] && arr.addressee == arrAddressees[a]);
            });
            arrCustPeriods = _.sortBy(arrCustPeriods, function(obj){ return obj.periodid;});

            var objCustomer = new Object();
            objCustomer["node"] = arrCustPeriods[0].customer;
            objCustomer["nodetype"] = 'customer';
            objCustomer["customerid"] = arrCustPeriods[0].customerid;
            objCustomer["customer"] = arrCustPeriods[0].customer;
            objCustomer["addressee"] = arrCustPeriods[0].addressee;

            var arrPeriods = new Array();
            for ( var p = 0; arrCustPeriods != null && p < arrCustPeriods.length; p++ ) {
                var objPeriod = new Object();
                objPeriod["node"] = arrCustPeriods[p].cinvoice;
                objPeriod["nodetype"] = 'cinvoice';
                objPeriod["cinvoiceid"] = arrCustPeriods[p].cinvoiceid;
                objPeriod["cinvoice"] = arrCustPeriods[p].cinvoice;
                objPeriod["clocationid"] = arrCustPeriods[p].clocationid;
                objPeriod["clocation"] = arrCustPeriods[p].clocation;
                objPeriod["currencyid"] = arrCustPeriods[p].currencyid;
                objPeriod["currency"] = arrCustPeriods[p].currency;
                objPeriod["periodid"] = arrCustPeriods[p].periodid;
                objPeriod["period"] = arrCustPeriods[p].period;
                objPeriod["periodname"] = arrCustPeriods[p].periodname;
                objPeriod["total"] = (arrCustPeriods[p].total).toFixed(2);
                objPeriod["power_usage"] = (arrCustPeriods[p].power_usage).toFixed(2);
                objPeriod["banwidth_burst"] = (arrCustPeriods[p].banwidth_burst).toFixed(2);
                objPeriod["equipment_rental"] = (arrCustPeriods[p].equipment_rental).toFixed(2);
                objPeriod["equipment_sales"] = (arrCustPeriods[p].equipment_sales).toFixed(2);
                objPeriod["install"] = (arrCustPeriods[p].install).toFixed(2);
                objPeriod["xc"] = (arrCustPeriods[p].xc).toFixed(2);
                objPeriod["network"] = (arrCustPeriods[p].network).toFixed(2);
                objPeriod["other_non_recurring"] = (arrCustPeriods[p].other_non_recurring).toFixed(2);
                objPeriod["other_recurring"] = (arrCustPeriods[p].other_recurring).toFixed(2);
                objPeriod["power"] = (arrCustPeriods[p].power).toFixed(2);
                objPeriod["remote_hands"] = (arrCustPeriods[p].remote_hands).toFixed(2);
                objPeriod["space"] = (arrCustPeriods[p].space).toFixed(2);
                objPeriod["vxc"] = (arrCustPeriods[p].vxc).toFixed(2);
                objPeriod["disaster"] = (arrCustPeriods[p].disaster).toFixed(2);
                objPeriod["managed"] = (arrCustPeriods[p].managed).toFixed(2);
                objPeriod["managed_services_usage"] = (arrCustPeriods[p].managed_services_usage).toFixed(2);
                objPeriod["high_density_usage_premium"] = (arrCustPeriods[p].high_density_usage_premium).toFixed(2);

                var objCInvRec = _.find(arrCInvRec, function(arr){ return (arr.cinvoice == arrCustPeriods[p].cinvoice) ; });
                if(objCInvRec != null){
                    objPeriod["jsonid"] = objCInvRec.jsonid;
                    objPeriod["pdfid"] = objCInvRec.pdfid;
                    objPeriod["ctotal"] = objCInvRec.ctotal;
                    //objPeriod["invoices"] = objCInvRec.invoices;
                }
                else{
                    objPeriod["jsonid"] = 0;
                    objPeriod["pdfid"] = 0;
                    objPeriod["ctotal"] = 0;
                    //objPeriod["invoices"] = [];
                }
                objPeriod["expanded"] = false;
                objPeriod["faicon"] = 'fa fa-list SteelBlue2';
                objPeriod["leaf"] = false;
                arrPeriods.push(objPeriod);
            }

            var objPeriodA = _.find(arrPeriods, function(arr){ return (arr.periodname == 'A') ; });
            var objPeriodB = _.find(arrPeriods, function(arr){ return (arr.periodname == 'B') ; });

            if(objPeriodA && objPeriodB){
                objCustomer["total"] = (objPeriodA.total - objPeriodB.total).toFixed(2);
                objCustomer["ctotal"] = (objPeriodA.ctotal - objPeriodB.ctotal).toFixed(2);
                objCustomer["power_usage"] = (objPeriodA.power_usage - objPeriodB.power_usage).toFixed(2);
                objCustomer["banwidth_burst"] = (objPeriodA.banwidth_burst - objPeriodB.banwidth_burst).toFixed(2);
                objCustomer["equipment_rental"] = (objPeriodA.equipment_rental - objPeriodB.equipment_rental).toFixed(2);
                objCustomer["equipment_sales"] = (objPeriodA.equipment_sales - objPeriodB.equipment_sales).toFixed(2);
                objCustomer["install"] = (objPeriodA.install - objPeriodB.install).toFixed(2);
                objCustomer["xc"] = (objPeriodA.xc - objPeriodB.xc).toFixed(2);
                objCustomer["network"] = (objPeriodA.network - objPeriodB.network).toFixed(2);
                objCustomer["other_non_recurring"] = (objPeriodA.other_non_recurring - objPeriodB.other_non_recurring).toFixed(2);
                objCustomer["other_recurring"] = (objPeriodA.other_recurring - objPeriodB.other_recurring).toFixed(2);
                objCustomer["power"] = (objPeriodA.power - objPeriodB.power).toFixed(2);
                objCustomer["remote_hands"] = (objPeriodA.remote_hands - objPeriodB.remote_hands).toFixed(2);
                objCustomer["space"] = (objPeriodA.space - objPeriodB.space).toFixed(2);
                objCustomer["vxc"] = (objPeriodA.vxc - objPeriodB.vxc).toFixed(2);
                objCustomer["disaster"] = (objPeriodA.disaster - objPeriodB.disaster).toFixed(2);
                objCustomer["managed"] = (objPeriodA.managed - objPeriodB.managed).toFixed(2);
                objCustomer["managed_services_usage"] = (objPeriodA.managed_services_usage - objPeriodB.managed_services_usage).toFixed(2);
                objCustomer["high_density_usage_premium"] = (objPeriodA.high_density_usage_premium - objPeriodB.high_density_usage_premium).toFixed(2);

                objCustomer["children"] = arrPeriods;
                objCustomer["expanded"] = true;
                objCustomer["faicon"] = 'fa fa-user SteelBlue2';
                objCustomer["leaf"] = false;

                if(location != 29 && (objCustomer.ctotal != 0 || objCustomer.power_usage != 0 || objCustomer.banwidth_burst != 0 || objCustomer.equipment_rental != 0 || objCustomer.equipment_sales != 0 || objCustomer.install != 0 || objCustomer.xc != 0 || objCustomer.network != 0 || objCustomer.other_non_recurring != 0 || objCustomer.other_recurring != 0 || objCustomer.power != 0 || objCustomer.remote_hands != 0 || objCustomer.space != 0 || objCustomer.vxc != 0 || objCustomer.disaster != 0 || objCustomer.managed != 0)){
                    for ( var p = 0; objCustomer.children != null && p < objCustomer.children.length; p++ ) {
                        var arrCInvInvs = _.filter(arrInvoices, function(arr){
                            return (arr.cinvoice == objCustomer.children[p].cinvoice);
                        });
                        var hasunpaid = 0;
                        var arrInvs = new Array();
                        for ( var i = 0; arrCInvInvs != null && i < arrCInvInvs.length; i++ ) {
                            var objInv = new Object();
                            objInv["node"] = arrCInvInvs[i].invoice;
                            objInv["nodetype"] = 'invoice';
                            objInv["invoiceid"] = arrCInvInvs[i].invoiceid;
                            objInv["invoice"] = arrCInvInvs[i].invoice;
                            objInv["status"] = arrCInvInvs[i].status;
                            objInv["total"] = (arrCInvInvs[i].total).toFixed(2);
                            objInv["ctotal"] = (arrCInvInvs[i].total).toFixed(2);
                            if(arrCInvInvs[i].status == 'paidInFull'){
                                objInv["faicon"] = 'fa fa-list-ul Green1';
                            }
                            else{
                                objInv["faicon"] = 'fa fa-list-ul Crimson1';
                                hasunpaid = 1;
                            }
                            objInv["leaf"] = true;
                            arrInvs.push(objInv);
                        }
                        objCustomer.children[p]["children"] = arrInvs;
                        if(hasunpaid == 1){
                            objCustomer.children[p].faicon = 'fa fa-list Crimson1';
                        }
                        else{
                            objCustomer.children[p].faicon = 'fa fa-list Green1';
                        }

                    }
                    arrCustomers.push(objCustomer);
                }
                else if(location == 29 && (objCustomer.ctotal != 0 || objCustomer.power_usage != 0 || objCustomer.banwidth_burst != 0 || objCustomer.equipment_rental != 0 || objCustomer.equipment_sales != 0 || objCustomer.install != 0 || objCustomer.xc != 0 || objCustomer.network != 0 || objCustomer.other_non_recurring != 0 || objCustomer.other_recurring != 0 || objCustomer.power != 0 || objCustomer.remote_hands != 0 || objCustomer.space != 0 || objCustomer.vxc != 0 || objCustomer.disaster != 0 || objCustomer.managed != 0)){
                    arrCustomers.push(objCustomer);
                    //arrCustomers.children[p].faicon = 'fa fa-list SteelBlue2';
                }
                else{
                }
                //arrCustomers.push(objCustomer);
            } else  if(objPeriodA && !objPeriodB){
                objCustomer["total"] = objPeriodA.total;
                objCustomer["ctotal"] = objPeriodA.ctotal;
                objCustomer["power_usage"] = objPeriodA.power_usage;
                objCustomer["banwidth_burst"] = objPeriodA.banwidth_burst;
                objCustomer["equipment_rental"] = objPeriodA.equipment_rental;
                objCustomer["equipment_sales"] = objPeriodA.equipment_sales;
                objCustomer["install"] = objPeriodA.install;
                objCustomer["xc"] = objPeriodA.xc;
                objCustomer["network"] = objPeriodA.network;
                objCustomer["other_non_recurring"] = objPeriodA.other_non_recurring;
                objCustomer["other_recurring"] = objPeriodA.other_recurring;
                objCustomer["power"] = objPeriodA.power;
                objCustomer["remote_hands"] = objPeriodA.remote_hands;
                objCustomer["space"] = objPeriodA.space;
                objCustomer["vxc"] = objPeriodA.vxc;
                objCustomer["disaster"] = objPeriodA.disaster;
                objCustomer["managed"] = objPeriodA.managed;
                objCustomer["managed_services_usage"] = objPeriodA.managed_services_usage;
                objCustomer["high_density_usage_premium"] = objPeriodA.high_density_usage_premium;

                objCustomer["children"] = arrPeriods;
                objCustomer["expanded"] = true;
                objCustomer["faicon"] = 'fa fa-user SteelBlue2';
                objCustomer["leaf"] = false;

                if(location != 29 && (objCustomer.ctotal != 0 || objCustomer.power_usage != 0 || objCustomer.banwidth_burst != 0 || objCustomer.equipment_rental != 0 || objCustomer.equipment_sales != 0 || objCustomer.install != 0 || objCustomer.xc != 0 || objCustomer.network != 0 || objCustomer.other_non_recurring != 0 || objCustomer.other_recurring != 0 || objCustomer.power != 0 || objCustomer.remote_hands != 0 || objCustomer.space != 0 || objCustomer.vxc != 0 || objCustomer.disaster != 0 || objCustomer.managed != 0)){
                    for ( var p = 0; objCustomer.children != null && p < objCustomer.children.length; p++ ) {
                        var arrCInvInvs = _.filter(arrInvoices, function(arr){
                            return (arr.cinvoice == objCustomer.children[p].cinvoice);
                        });
                        var hasunpaid = 0;
                        var arrInvs = new Array();
                        for ( var i = 0; arrCInvInvs != null && i < arrCInvInvs.length; i++ ) {
                            var objInv = new Object();
                            objInv["node"] = arrCInvInvs[i].invoice;
                            objInv["nodetype"] = 'invoice';
                            objInv["invoiceid"] = arrCInvInvs[i].invoiceid;
                            objInv["invoice"] = arrCInvInvs[i].invoice;
                            objInv["status"] = arrCInvInvs[i].status;
                            objInv["total"] = (arrCInvInvs[i].total).toFixed(2);
                            objInv["ctotal"] = (arrCInvInvs[i].total).toFixed(2);
                            if(arrCInvInvs[i].status == 'paidInFull'){
                                objInv["faicon"] = 'fa fa-list-ul Green1';
                            }
                            else{
                                objInv["faicon"] = 'fa fa-list-ul Crimson1';
                                hasunpaid = 1;
                            }
                            objInv["leaf"] = true;
                            arrInvs.push(objInv);
                        }
                        objCustomer.children[p]["children"] = arrInvs;
                        if(hasunpaid == 1){
                            objCustomer.children[p].faicon = 'fa fa-list Crimson1';
                        }
                        else{
                            objCustomer.children[p].faicon = 'fa fa-list Green1';
                        }

                    }
                    arrCustomers.push(objCustomer);
                }
                else if(location == 29 && (objCustomer.ctotal != 0 || objCustomer.power_usage != 0 || objCustomer.banwidth_burst != 0 || objCustomer.equipment_rental != 0 || objCustomer.equipment_sales != 0 || objCustomer.install != 0 || objCustomer.xc != 0 || objCustomer.network != 0 || objCustomer.other_non_recurring != 0 || objCustomer.other_recurring != 0 || objCustomer.power != 0 || objCustomer.remote_hands != 0 || objCustomer.space != 0 || objCustomer.vxc != 0 || objCustomer.disaster != 0 || objCustomer.managed != 0)){
                    arrCustomers.push(objCustomer);
                    //arrCustomers.children[p].faicon = 'fa fa-list SteelBlue2';
                }
                else{
                }
                //arrCustomers.push(objCustomer);
            }
            else  if(!objPeriodA && objPeriodB){
                objCustomer["total"] = objPeriodB.total;
                objCustomer["ctotal"] = objPeriodB.ctotal;
                objCustomer["power_usage"] = objPeriodB.power_usage;
                objCustomer["banwidth_burst"] = objPeriodB.banwidth_burst;
                objCustomer["equipment_rental"] = objPeriodB.equipment_rental;
                objCustomer["equipment_sales"] = objPeriodB.equipment_sales;
                objCustomer["install"] = objPeriodB.install;
                objCustomer["xc"] = objPeriodB.xc;
                objCustomer["network"] = objPeriodB.network;
                objCustomer["other_non_recurring"] = objPeriodB.other_non_recurring;
                objCustomer["other_recurring"] = objPeriodB.other_recurring;
                objCustomer["power"] = objPeriodB.power;
                objCustomer["remote_hands"] = objPeriodB.remote_hands;
                objCustomer["space"] = objPeriodB.space;
                objCustomer["vxc"] = objPeriodB.vxc;
                objCustomer["disaster"] = objPeriodB.disaster;
                objCustomer["managed"] = objPeriodB.managed;
                objCustomer["managed_services_usage"] = objPeriodB.managed_services_usage;
                objCustomer["high_density_usage_premium"] = objPeriodB.high_density_usage_premium;

                objCustomer["children"] = arrPeriods;
                objCustomer["expanded"] = true;
                objCustomer["faicon"] = 'fa fa-user SteelBlue2';
                objCustomer["leaf"] = false;

                if(location != 29 && (objCustomer.ctotal != 0 || objCustomer.power_usage != 0 || objCustomer.banwidth_burst != 0 || objCustomer.equipment_rental != 0 || objCustomer.equipment_sales != 0 || objCustomer.install != 0 || objCustomer.xc != 0 || objCustomer.network != 0 || objCustomer.other_non_recurring != 0 || objCustomer.other_recurring != 0 || objCustomer.power != 0 || objCustomer.remote_hands != 0 || objCustomer.space != 0 || objCustomer.vxc != 0 || objCustomer.disaster != 0 || objCustomer.managed != 0)){
                    for ( var p = 0; objCustomer.children != null && p < objCustomer.children.length; p++ ) {
                        var arrCInvInvs = _.filter(arrInvoices, function(arr){
                            return (arr.cinvoice == objCustomer.children[p].cinvoice);
                        });
                        var hasunpaid = 0;
                        var arrInvs = new Array();
                        for ( var i = 0; arrCInvInvs != null && i < arrCInvInvs.length; i++ ) {
                            var objInv = new Object();
                            objInv["node"] = arrCInvInvs[i].invoice;
                            objInv["nodetype"] = 'invoice';
                            objInv["invoiceid"] = arrCInvInvs[i].invoiceid;
                            objInv["invoice"] = arrCInvInvs[i].invoice;
                            objInv["status"] = arrCInvInvs[i].status;
                            objInv["total"] = (arrCInvInvs[i].total).toFixed(2);
                            objInv["ctotal"] = (arrCInvInvs[i].total).toFixed(2);
                            if(arrCInvInvs[i].status == 'paidInFull'){
                                objInv["faicon"] = 'fa fa-list-ul Green1';
                            }
                            else{
                                objInv["faicon"] = 'fa fa-list-ul Crimson1';
                                hasunpaid = 1;
                            }
                            objInv["leaf"] = true;
                            arrInvs.push(objInv);
                        }
                        objCustomer.children[p]["children"] = arrInvs;
                        if(hasunpaid == 1){
                            objCustomer.children[p].faicon = 'fa fa-list Crimson1';
                        }
                        else{
                            objCustomer.children[p].faicon = 'fa fa-list Green1';
                        }

                    }
                    arrCustomers.push(objCustomer);
                }
                else if(location == 29 && (objCustomer.ctotal != 0 || objCustomer.power_usage != 0 || objCustomer.banwidth_burst != 0 || objCustomer.equipment_rental != 0 || objCustomer.equipment_sales != 0 || objCustomer.install != 0 || objCustomer.xc != 0 || objCustomer.network != 0 || objCustomer.other_non_recurring != 0 || objCustomer.other_recurring != 0 || objCustomer.power != 0 || objCustomer.remote_hands != 0 || objCustomer.space != 0 || objCustomer.vxc != 0 || objCustomer.disaster != 0 || objCustomer.managed != 0)){
                    arrCustomers.push(objCustomer);
                    //arrCustomers.children[p].faicon = 'fa fa-list SteelBlue2';
                }
                else{
                }
                //arrCustomers.push(objCustomer);
            }

        }
    }
    arrCustomers = _.sortBy(arrCustomers, function(obj){ return obj.node.toLowerCase();});

    var objTree = new Object();
    objTree["text"] = '.';
    objTree["location"] = location;
    objTree["periodA"] = periodA;
    objTree["periodB"] = periodB;
    objTree["children"] = arrCustomers;

    return objTree;
}