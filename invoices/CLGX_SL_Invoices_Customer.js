//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Invoices_Customer.js
//	Script Name:	CLGX_SL_Invoices_Customer
//	Script Id:		customscript_clgx_sl_inv_customer
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/30/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=580&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_inv_customer (request, response){
    try {
    	
    	var location = request.getParameter('location');
        var locationid = request.getParameter('locationid');
        var customer = request.getParameter('customer');
        var customerid = request.getParameter('customerid');
        var year = request.getParameter('year');
        var month = request.getParameter('month');
        
    	var start = moment(month + '/1/' + year).format('M/D/YYYY');
    	var end = moment(month + '/1/' + year).endOf('month').format('M/D/YYYY');
    	var cmonth = moment(month + '/1/' + year).format('MMMM');
    	
    	
        if(locationid > 0){
        	
        	var arrTree = get_items (locationid,location,customerid,start,end);
        	var objTree1 = arrTree[0];
        	var objTree2 = arrTree[1];
        	
            var objFile = nlapiLoadFile(3154659);
            var html = objFile.getValue();
            
            var objParams = new Object();
    		objParams["locationid"] = locationid;
    		objParams["location"] = location;
    		objParams["customerid"] = customerid;
    		objParams["customer"] = customer;
    		objParams["start"] = start;
    		objParams["end"] = end;
    		objParams["month"] = month;
    		objParams["year"] = year;
    		objParams["cmonth"] = cmonth;
    		objParams["cyear"] = year;
    		
    		html = html.replace(new RegExp('{params}','g'), JSON.stringify(objParams));
    		html = html.replace(new RegExp('{invoices}','g'), JSON.stringify(objTree1));
    		html = html.replace(new RegExp('{categories}','g'), JSON.stringify(objTree2));
        }
        else{
            var html = 'Please select a customer or an invoice from the left panel.';
        }
        response.write( html );
        

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function get_items (locationid,location,customerid,start,end){
	
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('trandate',null,'within', start,end));
	arrFilters.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', locationid));
	arrFilters.push(new nlobjSearchFilter('entity',null,'anyof', customerid));
	var arrInvoices = new Array();
    var searchInvoices = nlapiLoadSearch('invoice', 'customsearch_clgx_inv_invoices');
    searchInvoices.addColumns(arrColumns);
    searchInvoices.addFilters(arrFilters);
	var resultSet = searchInvoices.runSearch();
	resultSet.forEachResult(function(searchResult) {
		var objInvoice = new Object();
		objInvoice["locationid"] = locationid;
		objInvoice["location"] = location;
		objInvoice["customerid"] = parseInt(searchResult.getValue('entity',null,'GROUP'));
		objInvoice["customer"] = _.last((searchResult.getText('entity',null,'GROUP')).split(":")).trim();
		objInvoice["invoiceid"] = parseInt(searchResult.getValue('internalid',null,'GROUP'));
		objInvoice["invoice"] = searchResult.getValue('tranid',null,'GROUP');
		objInvoice["cinvoice"] = searchResult.getValue('custbody_consolidate_inv_nbr',null,'GROUP');
		objInvoice["batch"] = searchResult.getValue('custbody_consolidate_inv_id',null,'GROUP');
		arrInvoices.push(objInvoice);
		return true;
	});
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('trandate',null,'within', start,end));
	arrFilters.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', locationid));
	arrFilters.push(new nlobjSearchFilter('entity',null,'anyof', customerid));
	var arrItems = new Array();
    var searchItems = nlapiLoadSearch('invoice', 'customsearch_clgx_inv_items');
    searchItems.addColumns(arrColumns);
    searchItems.addFilters(arrFilters);
	var resultSet = searchItems.runSearch();
	resultSet.forEachResult(function(searchResult) {
		var objItem = new Object();
		objItem["invoiceid"] = parseInt(searchResult.getValue('internalid',null,null));
		objItem["invoice"] = searchResult.getValue('tranid',null,null);
		objItem["itemid"] = parseInt(searchResult.getValue('item',null,null));
		objItem["item"] = searchResult.getText('item',null,null);
		objItem["categoryid"] = parseInt(searchResult.getValue('custcol_cologix_invoice_item_category',null,null));
		objItem["category"] = searchResult.getText('custcol_cologix_invoice_item_category',null,null);
		objItem["quantity"] = parseFloat(searchResult.getValue('quantity',null,null));
		objItem["qty2print"] = parseFloat(searchResult.getValue('custcol_clgx_qty2print',null,null));
		objItem["rate"] = parseFloat(searchResult.getValue('rate',null,null));
		objItem["amount"] = parseFloat(searchResult.getValue('amount',null,null));
		objItem["fxamount"] = parseFloat(searchResult.getValue('fxamount',null,null));
		arrItems.push(objItem);
		return true;
	});


	var objTree1 = new Object();
	objTree1["text"] = '.';
	var arrInv = new Array();
	for ( var i = 0; i < arrInvoices.length; i++ ) {
		
		objInv = new Object();
		objInv["entityid"] = arrInvoices[i].invoiceid;
		objInv["entity"] = arrInvoices[i].invoice;
		objInv["locationid"] = locationid;
		objInv["location"] = location;
		objInv["customerid"] = arrInvoices[i].customerid;
		objInv["customer"] = arrInvoices[i].customer;
		objInv["invoiceid"] = arrInvoices[i].invoiceid;
		objInv["invoice"] = arrInvoices[i].invoice;
		objInv["cinvoice"] = arrInvoices[i].cinvoice;
		objInv["batch"] = arrInvoices[i].batch;
		objInv["itemid"] = '';
		objInv["item"] = 0;
		
		var arrInvoiceItems = _.filter(arrItems, function(arr){
	        return arr.invoiceid === arrInvoices[i].invoiceid;
		});
		var arrI = new Array();
		for ( var j = 0; j < arrInvoiceItems.length; j++ ) {
			
			objI = new Object();
			objI["entityid"] = arrInvoiceItems[j].itemid;
			objI["entity"] = arrInvoiceItems[j].item;
			objI["locationid"] = locationid;
			objI["location"] = location;
			objI["customerid"] = arrInvoices[i].customerid;
			objI["customer"] = arrInvoices[i].customer;
			objI["invoiceid"] = arrInvoices[i].invoiceid;
			objI["invoice"] = arrInvoices[i].invoice;
			objI["itemid"] = arrInvoiceItems[j].itemid;
			objI["item"] = arrInvoiceItems[j].item;
			
			objI["categoryid"] = arrInvoiceItems[j].categoryid;
			objI["category"] = arrInvoiceItems[j].category;
			objI["quantity"] = arrInvoiceItems[j].quantity;
			objI["qty2print"] = arrInvoiceItems[j].qty2print;
			objI["rate"] = arrInvoiceItems[j].rate;
			objI["amount"] = arrInvoiceItems[j].amount;
			objI["fxamount"] = arrInvoiceItems[j].fxamount;
			
			//objI["iconCls"] = 'svg_item';
			objI["leaf"] = true;
			arrI.push(objI);
		}
		objInv["children"] = arrI;
		objInv["checked"] = true;
		objInv["expanded"] = true;
		//objInv["iconCls"] = 'svg_invoice';
		
		objInv["leaf"] = false;
		arrInv.push(objInv);
	}
	objTree1["children"] = arrInv;
	
	
	
	var arrCategories = _.compact(_.uniq(_.pluck(arrItems, 'category')));
	var arrInvoicesIDs = _.compact(_.uniq(_.pluck(arrItems, 'invoiceid')));
	
	var objTree2 = new Object();
	objTree2["text"] = '.';
	var arrCateg = new Array();
	var sumAll = 0;
	for ( var i = 0; i < arrCategories.length; i++ ) {
		
		var objCategory = _.find(arrItems, function(arr){ return (arr.category == arrCategories[i]) ; });
		
		if(objCategory != null){
			
			
			objCateg = new Object();
			
			objCateg["entityid"] = objCategory.categoryid;
			objCateg["entity"] = objCategory.category;
			
			objCateg["locationid"] = locationid;
			objCateg["location"] = location;
			objCateg["customerid"] = objCategory.customerid;
			objCateg["customer"] = objCategory.customer;
			objCateg["invoiceid"] = objCategory.invoiceid;
			objCateg["invoice"] = objCategory.invoice;
			objCateg["cinvoice"] = objCategory.cinvoice;
			objCateg["batch"] = objCategory.batch;
			objCateg["categoryid"] = objCategory.categoryid;
			objCateg["category"] = objCategory.category;
			objCateg["itemid"] = '';
			objCateg["item"] = 0;
			
			var arrCategoryItems = _.filter(arrItems, function(arr){
		        return arr.category === arrCategories[i];
			});
			var arrI = new Array();
			var sumCateg = 0;
			for ( var j = 0; j < arrCategoryItems.length; j++ ) {
				
				objI = new Object();
				objI["entityid"] = arrCategoryItems[j].itemid;
				objI["entity"] = arrCategoryItems[j].item;
				
				objI["locationid"] = locationid;
				objI["location"] = location;
				objI["customerid"] = objCategory.customerid;
				objI["customer"] = objCategory.customer;
				objI["invoiceid"] = objCategory.invoiceid;
				objI["invoice"] = objCategory.invoice;
				
				
				objI["itemid"] = arrCategoryItems[j].itemid;
				objI["item"] = arrCategoryItems[j].item;
				objI["categoryid"] = arrCategoryItems[j].categoryid;
				objI["category"] = arrCategoryItems[j].category;
				objI["quantity"] = arrCategoryItems[j].quantity;
				objI["qty2print"] = arrCategoryItems[j].qty2print;
				objI["rate"] = arrCategoryItems[j].rate;
				var amount =  arrCategoryItems[j].amount;
				objI["amount"] = arrCategoryItems[j].amount;
				objI["fxamount"] = arrCategoryItems[j].fxamount;
				
				sumCateg += amount;
				sumAll += amount;
				//objI["iconCls"] = 'svg_item';
				objI["leaf"] = true;
				arrI.push(objI);
			}
			objCateg["amount"] = sumCateg;
			objCateg["children"] = arrI;
			objCateg["expanded"] = true;
			//objCateg["iconCls"] = 'svg_invoice';
			
			objCateg["leaf"] = false;
			arrCateg.push(objCateg);
		}

	objTree2["children"] = arrCateg;
	}
	
	objTotal = new Object();
	objTotal["entityid"] = 0;
	objTotal["entity"] = 'Total';
	objTotal["amount"] = sumAll;
	objTotal["leaf"] = false;
	objTree2.children.push(objTotal);
	
	objTree2["customerid"] = customerid;
	objTree2["locationid"] = locationid;
	objTree2["subtotal"] = sumAll;
	objTree2["tax1"] = 0;
	objTree2["tax2"] = 0;
	objTree2["total"] = sumAll;
	
	objTree2["invoices"] = arrInvoicesIDs;
	
	
	var arrTrees = new Array();
	arrTrees.push(objTree1);
	arrTrees.push(objTree2);
	
	return arrTrees;
	
}


