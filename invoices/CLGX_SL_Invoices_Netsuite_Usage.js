nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Invoices_Netsuite_Usage.js
//	Script Name:	CLGX_SL_Invoices_History
//	Script Id:		customscript_clgx_sl_inv_ns_usage
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/16/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=596&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_inv_ns_usage (request, response){
    try {

        var after = moment().startOf('month').subtract(3, 'months').format('M/D/YYYY');

        var objTree = new Object();
        objTree["text"] = '.';

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_queue_json_fileid',null,null));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_queue_json_processed',null,null));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_queue_json_type',null,null));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_queue_json_date',null,null).setSort(true));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_queue_json_type",null,"anyof",[4,6,7,8]));
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_queue_json_date",null,"after",after));
        //arrFilters.push(new nlobjSearchFilter("custrecord_clgx_queue_json_processed",null,"is",'F'));
        var searchQueues = nlapiSearchRecord('customrecord_clgx_queue_json', null, arrFilters, arrColumns);
        var arrQueues = new Array();

        var objQueue29 = new Object();
        objQueue29["node"] = '21/04/2020';
        objQueue29["nodetype"] = 'batch';
        objQueue29["batchid"] = '21/04/2020';
        objQueue29["processed"] = 'T';
        objQueue29["date"] = '21/04/2020';

        objQueue29["typeid"] = '6';
        objQueue29["type"] = 'NetSuite Bandwidth Overage';
        objQueue29["leaf"] = false;
        objQueue29["start"] = '03/21/2020';
        objQueue29["end"] = '04/20/2020';
        var arrInvoices=new Array();
        var arrFil=new Array();
        var arrCol=new Array();



        var search = nlapiSearchRecord('invoice','customsearch8244', arrFil, arrCol);
        if(search!=null) {

            for (var i = 0; search != null && i < search.length; i++) {

                var searchR = search[i];
                var columns = searchR.getAllColumns();
                var objInvoice = new Object();
                objInvoice["node"] = searchR.getText(columns[6]);
                objInvoice["nodetype"] = 'invoice';
                objInvoice["customerid"] = searchR.getValue(columns[6]);
                objInvoice["customer"] = searchR.getText(columns[6]);


                objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                objInvoice["invoice"] = searchR.getValue(columns[0]);

                objInvoice["clocationid"] = searchR.getValue(columns[1]);
                objInvoice["clocation"] = searchR.getText(columns[1]);
                objInvoice["locationid"] = searchR.getValue(columns[2]);
                objInvoice["location"] = searchR.getText(columns[2]);

                objInvoice["quantity"] = searchR.getValue(columns[4]);
                objInvoice["rate"] = searchR.getValue(columns[3]);
                objInvoice["amount"] = searchR.getValue(columns[5]);
                objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                objInvoice["leaf"] = true;

                arrInvoices.push(objInvoice);

            }
        }
        objQueue29["children"] = arrInvoices;
        objQueue29["expanded"] = false;

        objQueue29["faicon"] = 'fa fa-th SteelBlue2';
        arrQueues.push(objQueue29);
        

        var objQueue28 = new Object();
        objQueue28["node"] = '21/03/2020';
        objQueue28["nodetype"] = 'batch';
        objQueue28["batchid"] = '21/03/2020';
        objQueue28["processed"] = 'T';
        objQueue28["date"] = '21/03/2020';

        objQueue28["typeid"] = '6';
        objQueue28["type"] = 'NetSuite Bandwidth Overage';
        objQueue28["leaf"] = false;
        objQueue28["start"] = '02/21/2020';
        objQueue28["end"] = '03/20/2020';
        var arrInvoices=new Array();
        var arrFil=new Array();
        var arrCol=new Array();



        var search = nlapiSearchRecord('invoice','customsearch8030', arrFil, arrCol);
        if(search!=null) {

            for (var i = 0; search != null && i < search.length; i++) {

                var searchR = search[i];
                var columns = searchR.getAllColumns();
                var objInvoice = new Object();
                objInvoice["node"] = searchR.getText(columns[6]);
                objInvoice["nodetype"] = 'invoice';
                objInvoice["customerid"] = searchR.getValue(columns[6]);
                objInvoice["customer"] = searchR.getText(columns[6]);


                objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                objInvoice["invoice"] = searchR.getValue(columns[0]);

                objInvoice["clocationid"] = searchR.getValue(columns[1]);
                objInvoice["clocation"] = searchR.getText(columns[1]);
                objInvoice["locationid"] = searchR.getValue(columns[2]);
                objInvoice["location"] = searchR.getText(columns[2]);

                objInvoice["quantity"] = searchR.getValue(columns[4]);
                objInvoice["rate"] = searchR.getValue(columns[3]);
                objInvoice["amount"] = searchR.getValue(columns[5]);
                objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                objInvoice["leaf"] = true;

                arrInvoices.push(objInvoice);

            }
        }
        objQueue28["children"] = arrInvoices;
        objQueue28["expanded"] = false;

        objQueue28["faicon"] = 'fa fa-th SteelBlue2';
        arrQueues.push(objQueue28);

        var objQueue27 = new Object();
        objQueue27["node"] = '21/02/2020';
        objQueue27["nodetype"] = 'batch';
        objQueue27["batchid"] = '21/02/2020';
        objQueue27["processed"] = 'T';
        objQueue27["date"] = '21/02/2020';

        objQueue27["typeid"] = '6';
        objQueue27["type"] = 'NetSuite Bandwidth Overage';
        objQueue27["leaf"] = false;
        objQueue27["start"] = '01/21/2020';
        objQueue27["end"] = '02/20/2020';
        var arrInvoices=new Array();
        var arrFil=new Array();
        var arrCol=new Array();



        var search = nlapiSearchRecord('invoice','customsearch7915', arrFil, arrCol);
        if(search!=null) {

            for (var i = 0; search != null && i < search.length; i++) {

                var searchR = search[i];
                var columns = searchR.getAllColumns();
                var objInvoice = new Object();
                objInvoice["node"] = searchR.getText(columns[6]);
                objInvoice["nodetype"] = 'invoice';
                objInvoice["customerid"] = searchR.getValue(columns[6]);
                objInvoice["customer"] = searchR.getText(columns[6]);


                objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                objInvoice["invoice"] = searchR.getValue(columns[0]);

                objInvoice["clocationid"] = searchR.getValue(columns[1]);
                objInvoice["clocation"] = searchR.getText(columns[1]);
                objInvoice["locationid"] = searchR.getValue(columns[2]);
                objInvoice["location"] = searchR.getText(columns[2]);

                objInvoice["quantity"] = searchR.getValue(columns[4]);
                objInvoice["rate"] = searchR.getValue(columns[3]);
                objInvoice["amount"] = searchR.getValue(columns[5]);
                objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                objInvoice["leaf"] = true;

                arrInvoices.push(objInvoice);

            }
        }
        objQueue27["children"] = arrInvoices;
        objQueue27["expanded"] = false;

        objQueue27["faicon"] = 'fa fa-th SteelBlue2';
        arrQueues.push(objQueue27);

        var objQueue26 = new Object();
        objQueue26["node"] = '21/01/2020';
        objQueue26["nodetype"] = 'batch';
        objQueue26["batchid"] = '21/01/2020';
        objQueue26["processed"] = 'T';
        objQueue26["date"] = '21/01/2020';

        objQueue26["typeid"] = '6';
        objQueue26["type"] = 'NetSuite Bandwidth Overage';
        objQueue26["leaf"] = false;
        objQueue26["start"] = '12/21/2019';
        objQueue26["end"] = '01/20/2020';
        var arrInvoices=new Array();
        var arrFil=new Array();
        var arrCol=new Array();



        var search = nlapiSearchRecord('invoice','customsearch7825', arrFil, arrCol);
        if(search!=null) {

            for (var i = 0; search != null && i < search.length; i++) {

                var searchR = search[i];
                var columns = searchR.getAllColumns();
                var objInvoice = new Object();
                objInvoice["node"] = searchR.getText(columns[6]);
                objInvoice["nodetype"] = 'invoice';
                objInvoice["customerid"] = searchR.getValue(columns[6]);
                objInvoice["customer"] = searchR.getText(columns[6]);


                objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                objInvoice["invoice"] = searchR.getValue(columns[0]);

                objInvoice["clocationid"] = searchR.getValue(columns[1]);
                objInvoice["clocation"] = searchR.getText(columns[1]);
                objInvoice["locationid"] = searchR.getValue(columns[2]);
                objInvoice["location"] = searchR.getText(columns[2]);

                objInvoice["quantity"] = searchR.getValue(columns[4]);
                objInvoice["rate"] = searchR.getValue(columns[3]);
                objInvoice["amount"] = searchR.getValue(columns[5]);
                objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                objInvoice["leaf"] = true;

                arrInvoices.push(objInvoice);

            }
        }
        objQueue26["children"] = arrInvoices;
        objQueue26["expanded"] = false;

        objQueue26["faicon"] = 'fa fa-th SteelBlue2';
        arrQueues.push(objQueue26);



        var objQueue25 = new Object();
        objQueue25["node"] = '21/12/2019';
        objQueue25["nodetype"] = 'batch';
        objQueue25["batchid"] = '21/12/2019';
        objQueue25["processed"] = 'T';
        objQueue25["date"] = '21/12/2019';

        objQueue25["typeid"] = '6';
        objQueue25["type"] = 'NetSuite Bandwidth Overage';
        objQueue25["leaf"] = false;
        objQueue25["start"] = '11/21/2019';
        objQueue25["end"] = '12/20/2019';
        var arrInvoices=new Array();
        var arrFil=new Array();
        var arrCol=new Array();



        var search = nlapiSearchRecord('invoice','customsearch7771', arrFil, arrCol);
        if(search!=null) {

            for (var i = 0; search != null && i < search.length; i++) {

                var searchR = search[i];
                var columns = searchR.getAllColumns();
                var objInvoice = new Object();
                objInvoice["node"] = searchR.getText(columns[6]);
                objInvoice["nodetype"] = 'invoice';
                objInvoice["customerid"] = searchR.getValue(columns[6]);
                objInvoice["customer"] = searchR.getText(columns[6]);


                objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                objInvoice["invoice"] = searchR.getValue(columns[0]);

                objInvoice["clocationid"] = searchR.getValue(columns[1]);
                objInvoice["clocation"] = searchR.getText(columns[1]);
                objInvoice["locationid"] = searchR.getValue(columns[2]);
                objInvoice["location"] = searchR.getText(columns[2]);

                objInvoice["quantity"] = searchR.getValue(columns[4]);
                objInvoice["rate"] = searchR.getValue(columns[3]);
                objInvoice["amount"] = searchR.getValue(columns[5]);
                objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                objInvoice["leaf"] = true;

                arrInvoices.push(objInvoice);

            }
        }
        objQueue25["children"] = arrInvoices;
        objQueue25["expanded"] = false;

        objQueue25["faicon"] = 'fa fa-th SteelBlue2';
        arrQueues.push(objQueue25);

        var objQueue24 = new Object();
        objQueue24["node"] = '21/11/2019';
        objQueue24["nodetype"] = 'batch';
        objQueue24["batchid"] = '21/11/2019';
        objQueue24["processed"] = 'T';
        objQueue24["date"] = '21/11/2019';

        objQueue24["typeid"] = '6';
        objQueue24["type"] = 'NetSuite Bandwidth Overage';
        objQueue24["leaf"] = false;
        objQueue24["start"] = '10/21/2019';
        objQueue24["end"] = '11/20/2019';
        var arrInvoices=new Array();
        var arrFil=new Array();
        var arrCol=new Array();



        var search = nlapiSearchRecord('invoice','customsearch7706', arrFil, arrCol);
        if(search!=null) {

            for (var i = 0; search != null && i < search.length; i++) {

                var searchR = search[i];
                var columns = searchR.getAllColumns();
                var objInvoice = new Object();
                objInvoice["node"] = searchR.getText(columns[6]);
                objInvoice["nodetype"] = 'invoice';
                objInvoice["customerid"] = searchR.getValue(columns[6]);
                objInvoice["customer"] = searchR.getText(columns[6]);


                objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                objInvoice["invoice"] = searchR.getValue(columns[0]);

                objInvoice["clocationid"] = searchR.getValue(columns[1]);
                objInvoice["clocation"] = searchR.getText(columns[1]);
                objInvoice["locationid"] = searchR.getValue(columns[2]);
                objInvoice["location"] = searchR.getText(columns[2]);

                objInvoice["quantity"] = searchR.getValue(columns[4]);
                objInvoice["rate"] = searchR.getValue(columns[3]);
                objInvoice["amount"] = searchR.getValue(columns[5]);
                objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                objInvoice["leaf"] = true;

                arrInvoices.push(objInvoice);

            }
        }
        objQueue24["children"] = arrInvoices;
        objQueue24["expanded"] = false;

        objQueue24["faicon"] = 'fa fa-th SteelBlue2';
        arrQueues.push(objQueue24);

        var objQueue23 = new Object();
        objQueue23["node"] = '21/10/2019';
        objQueue23["nodetype"] = 'batch';
        objQueue23["batchid"] = '21/10/2019';
        objQueue23["processed"] = 'T';
        objQueue23["date"] = '21/10/2019';

        objQueue23["typeid"] = '6';
        objQueue23["type"] = 'NetSuite Bandwidth Overage';
        objQueue23["leaf"] = false;
        objQueue23["start"] = '09/21/2019';
        objQueue23["end"] = '10/20/2019';
        var arrInvoices=new Array();
        var arrFil=new Array();
        var arrCol=new Array();



        var search = nlapiSearchRecord('invoice','customsearch7615', arrFil, arrCol);
        if(search!=null) {

            for (var i = 0; search != null && i < search.length; i++) {

                var searchR = search[i];
                var columns = searchR.getAllColumns();
                var objInvoice = new Object();
                objInvoice["node"] = searchR.getText(columns[6]);
                objInvoice["nodetype"] = 'invoice';
                objInvoice["customerid"] = searchR.getValue(columns[6]);
                objInvoice["customer"] = searchR.getText(columns[6]);


                objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                objInvoice["invoice"] = searchR.getValue(columns[0]);

                objInvoice["clocationid"] = searchR.getValue(columns[1]);
                objInvoice["clocation"] = searchR.getText(columns[1]);
                objInvoice["locationid"] = searchR.getValue(columns[2]);
                objInvoice["location"] = searchR.getText(columns[2]);

                objInvoice["quantity"] = searchR.getValue(columns[4]);
                objInvoice["rate"] = searchR.getValue(columns[3]);
                objInvoice["amount"] = searchR.getValue(columns[5]);
                objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                objInvoice["leaf"] = true;

                arrInvoices.push(objInvoice);

            }
        }
        objQueue23["children"] = arrInvoices;
        objQueue23["expanded"] = false;

        objQueue23["faicon"] = 'fa fa-th SteelBlue2';
        arrQueues.push(objQueue23);

        var objQueue22 = new Object();
        objQueue22["node"] = '21/09/2019';
        objQueue22["nodetype"] = 'batch';
        objQueue22["batchid"] = '21/09/2019';
        objQueue22["processed"] = 'T';
        objQueue22["date"] = '21/09/2019';

        objQueue22["typeid"] = '6';
        objQueue22["type"] = 'NetSuite Bandwidth Overage';
        objQueue22["leaf"] = false;
        objQueue22["start"] = '08/21/2019';
        objQueue22["end"] = '09/20/2019';
        var arrInvoices=new Array();
        var arrFil=new Array();
        var arrCol=new Array();



        var search = nlapiSearchRecord('invoice','customsearch7532', arrFil, arrCol);
        if(search!=null) {

            for (var i = 0; search != null && i < search.length; i++) {

                var searchR = search[i];
                var columns = searchR.getAllColumns();
                var objInvoice = new Object();
                objInvoice["node"] = searchR.getText(columns[6]);
                objInvoice["nodetype"] = 'invoice';
                objInvoice["customerid"] = searchR.getValue(columns[6]);
                objInvoice["customer"] = searchR.getText(columns[6]);


                objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                objInvoice["invoice"] = searchR.getValue(columns[0]);

                objInvoice["clocationid"] = searchR.getValue(columns[1]);
                objInvoice["clocation"] = searchR.getText(columns[1]);
                objInvoice["locationid"] = searchR.getValue(columns[2]);
                objInvoice["location"] = searchR.getText(columns[2]);

                objInvoice["quantity"] = searchR.getValue(columns[4]);
                objInvoice["rate"] = searchR.getValue(columns[3]);
                objInvoice["amount"] = searchR.getValue(columns[5]);
                objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                objInvoice["leaf"] = true;

                arrInvoices.push(objInvoice);

            }
        }
        objQueue22["children"] = arrInvoices;
        objQueue22["expanded"] = false;

        objQueue22["faicon"] = 'fa fa-th SteelBlue2';
        arrQueues.push(objQueue22);




        var objQueue21 = new Object();
        objQueue21["node"] = '21/08/2019';
        objQueue21["nodetype"] = 'batch';
        objQueue21["batchid"] = '21/08/2019';
        objQueue21["processed"] = 'T';
        objQueue21["date"] = '21/08/2019';

        objQueue21["typeid"] = '6';
        objQueue21["type"] = 'NetSuite Bandwidth Overage';
        objQueue21["leaf"] = false;
        objQueue21["start"] = '07/21/2019';
        objQueue21["end"] = '08/20/2019';
        var arrInvoices=new Array();
        var arrFil=new Array();
        var arrCol=new Array();



        var search = nlapiSearchRecord('invoice','customsearch7450', arrFil, arrCol);
        if(search!=null) {

            for (var i = 0; search != null && i < search.length; i++) {

                var searchR = search[i];
                var columns = searchR.getAllColumns();
                var objInvoice = new Object();
                objInvoice["node"] = searchR.getText(columns[6]);
                objInvoice["nodetype"] = 'invoice';
                objInvoice["customerid"] = searchR.getValue(columns[6]);
                objInvoice["customer"] = searchR.getText(columns[6]);


                objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                objInvoice["invoice"] = searchR.getValue(columns[0]);

                objInvoice["clocationid"] = searchR.getValue(columns[1]);
                objInvoice["clocation"] = searchR.getText(columns[1]);
                objInvoice["locationid"] = searchR.getValue(columns[2]);
                objInvoice["location"] = searchR.getText(columns[2]);

                objInvoice["quantity"] = searchR.getValue(columns[4]);
                objInvoice["rate"] = searchR.getValue(columns[3]);
                objInvoice["amount"] = searchR.getValue(columns[5]);
                objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                objInvoice["leaf"] = true;

                arrInvoices.push(objInvoice);

            }
        }
        objQueue21["children"] = arrInvoices;
        objQueue21["expanded"] = false;

        objQueue21["faicon"] = 'fa fa-th SteelBlue2';
        arrQueues.push(objQueue21);


        var objQueue20 = new Object();
        objQueue20["node"] = '21/07/2019';
        objQueue20["nodetype"] = 'batch';
        objQueue20["batchid"] = '21/07/2019';
        objQueue20["processed"] = 'T';
        objQueue20["date"] = '21/07/2019';

        objQueue20["typeid"] = '6';
        objQueue20["type"] = 'NetSuite Bandwidth Overage';
        objQueue20["leaf"] = false;
        objQueue20["start"] = '06/21/2019';
        objQueue20["end"] = '07/20/2019';
        var arrInvoices=new Array();
        var arrFil=new Array();
        var arrCol=new Array();



        var search = nlapiSearchRecord('invoice','customsearch7369', arrFil, arrCol);
        if(search!=null) {

            for (var i = 0; search != null && i < search.length; i++) {

                var searchR = search[i];
                var columns = searchR.getAllColumns();
                var objInvoice = new Object();
                objInvoice["node"] = searchR.getText(columns[6]);
                objInvoice["nodetype"] = 'invoice';
                objInvoice["customerid"] = searchR.getValue(columns[6]);
                objInvoice["customer"] = searchR.getText(columns[6]);


                objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                objInvoice["invoice"] = searchR.getValue(columns[0]);

                objInvoice["clocationid"] = searchR.getValue(columns[1]);
                objInvoice["clocation"] = searchR.getText(columns[1]);
                objInvoice["locationid"] = searchR.getValue(columns[2]);
                objInvoice["location"] = searchR.getText(columns[2]);

                objInvoice["quantity"] = searchR.getValue(columns[4]);
                objInvoice["rate"] = searchR.getValue(columns[3]);
                objInvoice["amount"] = searchR.getValue(columns[5]);
                objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                objInvoice["leaf"] = true;

                arrInvoices.push(objInvoice);

            }
        }
        objQueue20["children"] = arrInvoices;
        objQueue20["expanded"] = false;

        objQueue20["faicon"] = 'fa fa-th SteelBlue2';
        arrQueues.push(objQueue20);

        var objQueue19 = new Object();
        objQueue19["node"] = '21/06/2019';
        objQueue19["nodetype"] = 'batch';
        objQueue19["batchid"] = '21/06/2019';
        objQueue19["processed"] = 'T';
        objQueue19["date"] = '21/06/2019';

        objQueue19["typeid"] = '6';
        objQueue19["type"] = 'NetSuite Bandwidth Overage';
        objQueue19["leaf"] = false;
        objQueue19["start"] = '05/21/2019';
        objQueue19["end"] = '06/20/2019';
        var arrInvoices=new Array();
        var arrFil=new Array();
        var arrCol=new Array();



        var search = nlapiSearchRecord('invoice','customsearch7324', arrFil, arrCol);
        if(search!=null) {

            for (var i = 0; search != null && i < search.length; i++) {

                var searchR = search[i];
                var columns = searchR.getAllColumns();
                var objInvoice = new Object();
                objInvoice["node"] = searchR.getText(columns[6]);
                objInvoice["nodetype"] = 'invoice';
                objInvoice["customerid"] = searchR.getValue(columns[6]);
                objInvoice["customer"] = searchR.getText(columns[6]);


                objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                objInvoice["invoice"] = searchR.getValue(columns[0]);

                objInvoice["clocationid"] = searchR.getValue(columns[1]);
                objInvoice["clocation"] = searchR.getText(columns[1]);
                objInvoice["locationid"] = searchR.getValue(columns[2]);
                objInvoice["location"] = searchR.getText(columns[2]);

                objInvoice["quantity"] = searchR.getValue(columns[4]);
                objInvoice["rate"] = searchR.getValue(columns[3]);
                objInvoice["amount"] = searchR.getValue(columns[5]);
                objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                objInvoice["leaf"] = true;

                arrInvoices.push(objInvoice);

            }
        }
        objQueue19["children"] = arrInvoices;
        objQueue19["expanded"] = false;

        objQueue19["faicon"] = 'fa fa-th SteelBlue2';
        arrQueues.push(objQueue19);



        var objQueue18 = new Object();
        objQueue18["node"] = '21/05/2019';
        objQueue18["nodetype"] = 'batch';
        objQueue18["batchid"] = '21/05/2019';
        objQueue18["processed"] = 'T';
        objQueue18["date"] = '21/05/2019';

        objQueue18["typeid"] = '6';
        objQueue18["type"] = 'NetSuite Bandwidth Overage';
        objQueue18["leaf"] = false;
        objQueue18["start"] = '04/21/2019';
        objQueue18["end"] = '05/20/2019';
        var arrInvoices=new Array();
        var arrFil=new Array();
        var arrCol=new Array();



        var search = nlapiSearchRecord('invoice','customsearch7255', arrFil, arrCol);
        if(search!=null) {

            for (var i = 0; search != null && i < search.length; i++) {

                var searchR = search[i];
                var columns = searchR.getAllColumns();
                var objInvoice = new Object();
                objInvoice["node"] = searchR.getText(columns[6]);
                objInvoice["nodetype"] = 'invoice';
                objInvoice["customerid"] = searchR.getValue(columns[6]);
                objInvoice["customer"] = searchR.getText(columns[6]);


                objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                objInvoice["invoice"] = searchR.getValue(columns[0]);

                objInvoice["clocationid"] = searchR.getValue(columns[1]);
                objInvoice["clocation"] = searchR.getText(columns[1]);
                objInvoice["locationid"] = searchR.getValue(columns[2]);
                objInvoice["location"] = searchR.getText(columns[2]);

                objInvoice["quantity"] = searchR.getValue(columns[4]);
                objInvoice["rate"] = searchR.getValue(columns[3]);
                objInvoice["amount"] = searchR.getValue(columns[5]);
                objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                objInvoice["leaf"] = true;

                arrInvoices.push(objInvoice);

            }
        }
        objQueue18["children"] = arrInvoices;
        objQueue18["expanded"] = false;

        objQueue18["faicon"] = 'fa fa-th SteelBlue2';
        arrQueues.push(objQueue18);
        /*
        var objQueue17 = new Object();
        objQueue17["node"] = '21/04/2019';
        objQueue17["nodetype"] = 'batch';
        objQueue17["batchid"] = '21/04/2019';
        objQueue17["processed"] = 'T';
        objQueue17["date"] = '21/04/2019';

        objQueue17["typeid"] = '6';
        objQueue17["type"] = 'NetSuite Bandwidth Overage';
        objQueue17["leaf"] = false;
        objQueue17["start"] = '03/21/2019';
        objQueue17["end"] = '04/20/2019';
        var arrInvoices=new Array();
        var arrFil=new Array();
        var arrCol=new Array();



        var search = nlapiSearchRecord('invoice','customsearch7183', arrFil, arrCol);
        if(search!=null) {

            for (var i = 0; search != null && i < search.length; i++) {

                var searchR = search[i];
                var columns = searchR.getAllColumns();
                var objInvoice = new Object();
                objInvoice["node"] = searchR.getText(columns[6]);
                objInvoice["nodetype"] = 'invoice';
                objInvoice["customerid"] = searchR.getValue(columns[6]);
                objInvoice["customer"] = searchR.getText(columns[6]);


                objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                objInvoice["invoice"] = searchR.getValue(columns[0]);

                objInvoice["clocationid"] = searchR.getValue(columns[1]);
                objInvoice["clocation"] = searchR.getText(columns[1]);
                objInvoice["locationid"] = searchR.getValue(columns[2]);
                objInvoice["location"] = searchR.getText(columns[2]);

                objInvoice["quantity"] = searchR.getValue(columns[4]);
                objInvoice["rate"] = searchR.getValue(columns[3]);
                objInvoice["amount"] = searchR.getValue(columns[5]);
                objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                objInvoice["leaf"] = true;

                arrInvoices.push(objInvoice);

            }
        }
        objQueue17["children"] = arrInvoices;
        objQueue17["expanded"] = false;

        objQueue17["faicon"] = 'fa fa-th SteelBlue2';
        arrQueues.push(objQueue17);


        var objQueue16 = new Object();
        objQueue16["node"] = '21/03/2019';
        objQueue16["nodetype"] = 'batch';
        objQueue16["batchid"] = '21/03/2019';
        objQueue16["processed"] = 'T';
        objQueue16["date"] = '21/03/2019';

        objQueue16["typeid"] = '6';
        objQueue16["type"] = 'NetSuite Bandwidth Overage';
        objQueue16["leaf"] = false;
        objQueue16["start"] = '02/21/2019';
        objQueue16["end"] = '03/20/2019';
        var arrInvoices=new Array();
        var arrFil=new Array();
        var arrCol=new Array();



        var search = nlapiSearchRecord('invoice','customsearch7117', arrFil, arrCol);
        if(search!=null) {

            for (var i = 0; search != null && i < search.length; i++) {

                var searchR = search[i];
                var columns = searchR.getAllColumns();
                var objInvoice = new Object();
                objInvoice["node"] = searchR.getText(columns[6]);
                objInvoice["nodetype"] = 'invoice';
                objInvoice["customerid"] = searchR.getValue(columns[6]);
                objInvoice["customer"] = searchR.getText(columns[6]);


                objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                objInvoice["invoice"] = searchR.getValue(columns[0]);

                objInvoice["clocationid"] = searchR.getValue(columns[1]);
                objInvoice["clocation"] = searchR.getText(columns[1]);
                objInvoice["locationid"] = searchR.getValue(columns[2]);
                objInvoice["location"] = searchR.getText(columns[2]);

                objInvoice["quantity"] = searchR.getValue(columns[4]);
                objInvoice["rate"] = searchR.getValue(columns[3]);
                objInvoice["amount"] = searchR.getValue(columns[5]);
                objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                objInvoice["leaf"] = true;

                arrInvoices.push(objInvoice);

            }
        }
        objQueue16["children"] = arrInvoices;
        objQueue16["expanded"] = false;

        objQueue16["faicon"] = 'fa fa-th SteelBlue2';
        arrQueues.push(objQueue16);



           var objQueue15 = new Object();
           objQueue15["node"] = '21/02/2019';
           objQueue15["nodetype"] = 'batch';
           objQueue15["batchid"] = '21/02/2019';
           objQueue15["processed"] = 'T';
           objQueue15["date"] = '21/02/2019';

           objQueue15["typeid"] = '6';
           objQueue15["type"] = 'NetSuite Bandwidth Overage';
           objQueue15["leaf"] = false;
           objQueue15["start"] = '01/21/2019';
           objQueue15["end"] = '02/20/2019';
           var arrInvoices=new Array();
           var arrFil=new Array();
           var arrCol=new Array();



           var search = nlapiSearchRecord('invoice','customsearch7056', arrFil, arrCol);
           if(search!=null) {

               for (var i = 0; search != null && i < search.length; i++) {

                   var searchR = search[i];
                   var columns = searchR.getAllColumns();
                   var objInvoice = new Object();
                   objInvoice["node"] = searchR.getText(columns[6]);
                   objInvoice["nodetype"] = 'invoice';
                   objInvoice["customerid"] = searchR.getValue(columns[6]);
                   objInvoice["customer"] = searchR.getText(columns[6]);


                   objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                   objInvoice["invoice"] = searchR.getValue(columns[0]);

                   objInvoice["clocationid"] = searchR.getValue(columns[1]);
                   objInvoice["clocation"] = searchR.getText(columns[1]);
                   objInvoice["locationid"] = searchR.getValue(columns[2]);
                   objInvoice["location"] = searchR.getText(columns[2]);

                   objInvoice["quantity"] = searchR.getValue(columns[4]);
                   objInvoice["rate"] = searchR.getValue(columns[3]);
                   objInvoice["amount"] = searchR.getValue(columns[5]);
                   objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                   objInvoice["leaf"] = true;

                   arrInvoices.push(objInvoice);

               }
           }
           objQueue15["children"] = arrInvoices;
           objQueue15["expanded"] = false;

           objQueue15["faicon"] = 'fa fa-th SteelBlue2';
           arrQueues.push(objQueue15);

           var objQueue14 = new Object();
           objQueue14["node"] = '21/01/2019';
           objQueue14["nodetype"] = 'batch';
           objQueue14["batchid"] = '21/01/2019';
           objQueue14["processed"] = 'T';
           objQueue14["date"] = '21/01/2019';

           objQueue14["typeid"] = '6';
           objQueue14["type"] = 'NetSuite Bandwidth Overage';
           objQueue14["leaf"] = false;
           objQueue14["start"] = '12/21/2018';
           objQueue14["end"] = '01/20/2019';
           var arrInvoices=new Array();
           var arrFil=new Array();
           var arrCol=new Array();



           var search = nlapiSearchRecord('invoice','customsearch6950', arrFil, arrCol);
           if(search!=null) {

               for (var i = 0; search != null && i < search.length; i++) {

                   var searchR = search[i];
                   var columns = searchR.getAllColumns();
                   var objInvoice = new Object();
                   objInvoice["node"] = searchR.getText(columns[6]);
                   objInvoice["nodetype"] = 'invoice';
                   objInvoice["customerid"] = searchR.getValue(columns[6]);
                   objInvoice["customer"] = searchR.getText(columns[6]);


                   objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                   objInvoice["invoice"] = searchR.getValue(columns[0]);

                   objInvoice["clocationid"] = searchR.getValue(columns[1]);
                   objInvoice["clocation"] = searchR.getText(columns[1]);
                   objInvoice["locationid"] = searchR.getValue(columns[2]);
                   objInvoice["location"] = searchR.getText(columns[2]);

                   objInvoice["quantity"] = searchR.getValue(columns[4]);
                   objInvoice["rate"] = searchR.getValue(columns[3]);
                   objInvoice["amount"] = searchR.getValue(columns[5]);
                   objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                   objInvoice["leaf"] = true;

                   arrInvoices.push(objInvoice);

               }
           }
           objQueue14["children"] = arrInvoices;
           objQueue14["expanded"] = false;

           objQueue14["faicon"] = 'fa fa-th SteelBlue2';
           arrQueues.push(objQueue14);

                 var objQueue13 = new Object();
                 objQueue13["node"] = '21/12/2018';
                 objQueue13["nodetype"] = 'batch';
                 objQueue13["batchid"] = '21/12/2018';
                 objQueue13["processed"] = 'T';
                 objQueue13["date"] = '21/12/2018';

                 objQueue13["typeid"] = '6';
                 objQueue13["type"] = 'NetSuite Bandwidth Overage';
                 objQueue13["leaf"] = false;
                 objQueue13["start"] = '11/21/2018';
                 objQueue13["end"] = '12/20/2018';
                 var arrInvoices=new Array();
                 var arrFil=new Array();
                 var arrCol=new Array();



                 var search = nlapiSearchRecord('invoice','customsearch6882', arrFil, arrCol);
                 if(search!=null) {

                     for (var i = 0; search != null && i < search.length; i++) {

                         var searchR = search[i];
                         var columns = searchR.getAllColumns();
                         var objInvoice = new Object();
                         objInvoice["node"] = searchR.getText(columns[6]);
                         objInvoice["nodetype"] = 'invoice';
                         objInvoice["customerid"] = searchR.getValue(columns[6]);
                         objInvoice["customer"] = searchR.getText(columns[6]);


                         objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                         objInvoice["invoice"] = searchR.getValue(columns[0]);

                         objInvoice["clocationid"] = searchR.getValue(columns[1]);
                         objInvoice["clocation"] = searchR.getText(columns[1]);
                         objInvoice["locationid"] = searchR.getValue(columns[2]);
                         objInvoice["location"] = searchR.getText(columns[2]);

                         objInvoice["quantity"] = searchR.getValue(columns[4]);
                         objInvoice["rate"] = searchR.getValue(columns[3]);
                         objInvoice["amount"] = searchR.getValue(columns[5]);
                         objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                         objInvoice["leaf"] = true;

                         arrInvoices.push(objInvoice);

                     }
                 }
                 objQueue13["children"] = arrInvoices;
                 objQueue13["expanded"] = false;

                 objQueue13["faicon"] = 'fa fa-th SteelBlue2';
                 arrQueues.push(objQueue13);





                 var objQueue12 = new Object();
                 objQueue12["node"] = '21/11/2018';
                 objQueue12["nodetype"] = 'batch';
                 objQueue12["batchid"] = '21/11/2018';
                 objQueue12["processed"] = 'T';
                 objQueue12["date"] = '21/11/2018';

                 objQueue12["typeid"] = '6';
                 objQueue12["type"] = 'NetSuite Bandwidth Overage';
                 objQueue12["leaf"] = false;
                 objQueue12["start"] = '10/21/2018';
                 objQueue12["end"] = '11/20/2018';
                 var arrInvoices=new Array();
                 var arrFil=new Array();
                 var arrCol=new Array();



                 var search = nlapiSearchRecord('invoice','customsearch6807', arrFil, arrCol);
                 if(search!=null) {

                     for (var i = 0; search != null && i < search.length; i++) {

                         var searchR = search[i];
                         var columns = searchR.getAllColumns();
                         var objInvoice = new Object();
                         objInvoice["node"] = searchR.getText(columns[6]);
                         objInvoice["nodetype"] = 'invoice';
                         objInvoice["customerid"] = searchR.getValue(columns[6]);
                         objInvoice["customer"] = searchR.getText(columns[6]);


                         objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                         objInvoice["invoice"] = searchR.getValue(columns[0]);

                         objInvoice["clocationid"] = searchR.getValue(columns[1]);
                         objInvoice["clocation"] = searchR.getText(columns[1]);
                         objInvoice["locationid"] = searchR.getValue(columns[2]);
                         objInvoice["location"] = searchR.getText(columns[2]);

                         objInvoice["quantity"] = searchR.getValue(columns[4]);
                         objInvoice["rate"] = searchR.getValue(columns[3]);
                         objInvoice["amount"] = searchR.getValue(columns[5]);
                         objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                         objInvoice["leaf"] = true;

                         arrInvoices.push(objInvoice);

                     }
                 }
                 objQueue12["children"] = arrInvoices;
                 objQueue12["expanded"] = false;

                 objQueue12["faicon"] = 'fa fa-th SteelBlue2';
                 arrQueues.push(objQueue12);

               var objQueue11 = new Object();
                   objQueue11["node"] = '21/10/2018';
                   objQueue11["nodetype"] = 'batch';
                   objQueue11["batchid"] = '21/10/2018';
                   objQueue11["processed"] = 'T';
                   objQueue11["date"] = '21/10/2018';

                   objQueue11["typeid"] = '6';
                   objQueue11["type"] = 'NetSuite Bandwidth Overage';
                   objQueue11["leaf"] = false;
                   objQueue11["start"] = '09/21/2018';
                   objQueue11["end"] = '10/20/2018';
                   var arrInvoices=new Array();
                   var arrFil=new Array();
                   var arrCol=new Array();



                   var search = nlapiSearchRecord('invoice','customsearch6443', arrFil, arrCol);
                   if(search!=null) {

                       for (var i = 0; search != null && i < search.length; i++) {

                           var searchR = search[i];
                           var columns = searchR.getAllColumns();
                           var objInvoice = new Object();
                           objInvoice["node"] = searchR.getText(columns[6]);
                           objInvoice["nodetype"] = 'invoice';
                           objInvoice["customerid"] = searchR.getValue(columns[6]);
                           objInvoice["customer"] = searchR.getText(columns[6]);


                           objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                           objInvoice["invoice"] = searchR.getValue(columns[0]);

                           objInvoice["clocationid"] = searchR.getValue(columns[1]);
                           objInvoice["clocation"] = searchR.getText(columns[1]);
                           objInvoice["locationid"] = searchR.getValue(columns[2]);
                           objInvoice["location"] = searchR.getText(columns[2]);

                           objInvoice["quantity"] = searchR.getValue(columns[4]);
                           objInvoice["rate"] = searchR.getValue(columns[3]);
                           objInvoice["amount"] = searchR.getValue(columns[5]);
                           objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                           objInvoice["leaf"] = true;

                           arrInvoices.push(objInvoice);

                       }
                   }
                   objQueue11["children"] = arrInvoices;
                   objQueue11["expanded"] = false;

                   objQueue11["faicon"] = 'fa fa-th SteelBlue2';
                   arrQueues.push(objQueue11);
                   /*
                       var objQueue10 = new Object();
                       objQueue10["node"] = '21/09/2018';
                       objQueue10["nodetype"] = 'batch';
                       objQueue10["batchid"] = '21/09/2018';
                       objQueue10["processed"] = 'T';
                       objQueue10["date"] = '21/09/2018';

                       objQueue10["typeid"] = '6';
                       objQueue10["type"] = 'NetSuite Bandwidth Overage';
                       objQueue10["leaf"] = false;
                       objQueue10["start"] = '08/21/2018';
                       objQueue10["end"] = '09/20/2018';
                       var arrInvoices=new Array();
                       var arrFil=new Array();
                       var arrCol=new Array();



                       var search = nlapiSearchRecord('invoice','customsearch6349', arrFil, arrCol);
                       if(search!=null) {

                           for (var i = 0; search != null && i < search.length; i++) {

                               var searchR = search[i];
                               var columns = searchR.getAllColumns();
                               var objInvoice = new Object();
                               objInvoice["node"] = searchR.getText(columns[6]);
                               objInvoice["nodetype"] = 'invoice';
                               objInvoice["customerid"] = searchR.getValue(columns[6]);
                               objInvoice["customer"] = searchR.getText(columns[6]);


                               objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                               objInvoice["invoice"] = searchR.getValue(columns[0]);

                               objInvoice["clocationid"] = searchR.getValue(columns[1]);
                               objInvoice["clocation"] = searchR.getText(columns[1]);
                               objInvoice["locationid"] = searchR.getValue(columns[2]);
                               objInvoice["location"] = searchR.getText(columns[2]);

                               objInvoice["quantity"] = searchR.getValue(columns[4]);
                               objInvoice["rate"] = searchR.getValue(columns[3]);
                               objInvoice["amount"] = searchR.getValue(columns[5]);
                               objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                               objInvoice["leaf"] = true;

                               arrInvoices.push(objInvoice);

                           }
                       }
                       objQueue10["children"] = arrInvoices;
                       objQueue10["expanded"] = false;

                       objQueue10["faicon"] = 'fa fa-th SteelBlue2';
                       arrQueues.push(objQueue10);



                       var objQueue9 = new Object();
                       objQueue9["node"] = '21/08/2018';
                       objQueue9["nodetype"] = 'batch';
                       objQueue9["batchid"] = '21/08/2018';
                       objQueue9["processed"] = 'T';
                       objQueue9["date"] = '21/08/2018';

                       objQueue9["typeid"] = '6';
                       objQueue9["type"] = 'NetSuite Bandwidth Overage';
                       objQueue9["leaf"] = false;
                       objQueue9["start"] = '07/21/2018';
                       objQueue9["end"] = '08/20/2018';
                       var arrInvoices=new Array();
                       var arrFil=new Array();
                       var arrCol=new Array();



                       var search = nlapiSearchRecord('invoice','customsearch6233', arrFil, arrCol);
                       if(search!=null) {

                           for (var i = 0; search != null && i < search.length; i++) {

                               var searchR = search[i];
                               var columns = searchR.getAllColumns();
                               var objInvoice = new Object();
                               objInvoice["node"] = searchR.getText(columns[6]);
                               objInvoice["nodetype"] = 'invoice';
                               objInvoice["customerid"] = searchR.getValue(columns[6]);
                               objInvoice["customer"] = searchR.getText(columns[6]);


                               objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                               objInvoice["invoice"] = searchR.getValue(columns[0]);

                               objInvoice["clocationid"] = searchR.getValue(columns[1]);
                               objInvoice["clocation"] = searchR.getText(columns[1]);
                               objInvoice["locationid"] = searchR.getValue(columns[2]);
                               objInvoice["location"] = searchR.getText(columns[2]);

                               objInvoice["quantity"] = searchR.getValue(columns[4]);
                               objInvoice["rate"] = searchR.getValue(columns[3]);
                               objInvoice["amount"] = searchR.getValue(columns[5]);
                               objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                               objInvoice["leaf"] = true;

                               arrInvoices.push(objInvoice);

                           }
                       }
                       objQueue9["children"] = arrInvoices;
                       objQueue9["expanded"] = false;

                       objQueue9["faicon"] = 'fa fa-th SteelBlue2';
                       arrQueues.push(objQueue9);


                       var objQueue8 = new Object();
                       objQueue8["node"] = '21/07/2018';
                       objQueue8["nodetype"] = 'batch';
                       objQueue8["batchid"] = '21/07/2018';
                       objQueue8["processed"] = 'T';
                       objQueue8["date"] = '21/07/2018';

                       objQueue8["typeid"] = '6';
                       objQueue8["type"] = 'NetSuite Bandwidth Overage';
                       objQueue8["leaf"] = false;
                       objQueue8["start"] = '06/21/2018';
                       objQueue8["end"] = '07/20/2018';
                       var arrInvoices=new Array();
                       var arrFil=new Array();
                       var arrCol=new Array();



                       var search = nlapiSearchRecord('invoice','customsearch6145', arrFil, arrCol);
                       if(search!=null) {

                           for (var i = 0; search != null && i < search.length; i++) {

                               var searchR = search[i];
                               var columns = searchR.getAllColumns();
                               var objInvoice = new Object();
                               objInvoice["node"] = searchR.getText(columns[6]);
                               objInvoice["nodetype"] = 'invoice';
                               objInvoice["customerid"] = searchR.getValue(columns[6]);
                               objInvoice["customer"] = searchR.getText(columns[6]);


                               objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                               objInvoice["invoice"] = searchR.getValue(columns[0]);

                               objInvoice["clocationid"] = searchR.getValue(columns[1]);
                               objInvoice["clocation"] = searchR.getText(columns[1]);
                               objInvoice["locationid"] = searchR.getValue(columns[2]);
                               objInvoice["location"] = searchR.getText(columns[2]);

                               objInvoice["quantity"] = searchR.getValue(columns[4]);
                               objInvoice["rate"] = searchR.getValue(columns[3]);
                               objInvoice["amount"] = searchR.getValue(columns[5]);
                               objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                               objInvoice["leaf"] = true;

                               arrInvoices.push(objInvoice);

                           }
                       }
                       objQueue8["children"] = arrInvoices;
                       objQueue8["expanded"] = false;

                       objQueue8["faicon"] = 'fa fa-th SteelBlue2';
                       arrQueues.push(objQueue8);



                             var objQueue7 = new Object();
                             objQueue7["node"] = '21/06/2018';
                             objQueue7["nodetype"] = 'batch';
                             objQueue7["batchid"] = '21/06/2018';
                             objQueue7["processed"] = 'T';
                             objQueue7["date"] = '21/06/2018';

                             objQueue7["typeid"] = '6';
                             objQueue7["type"] = 'NetSuite Bandwidth Overage';
                             objQueue7["leaf"] = false;
                             objQueue7["start"] = '05/21/2018';
                             objQueue7["end"] = '06/20/2018';
                             var arrInvoices=new Array();
                             var arrFil=new Array();
                             var arrCol=new Array();



                             var search = nlapiSearchRecord('invoice','customsearch6056', arrFil, arrCol);
                             if(search!=null) {

                                 for (var i = 0; search != null && i < search.length; i++) {

                                     var searchR = search[i];
                                     var columns = searchR.getAllColumns();
                                     var objInvoice = new Object();
                                     objInvoice["node"] = searchR.getText(columns[6]);
                                     objInvoice["nodetype"] = 'invoice';
                                     objInvoice["customerid"] = searchR.getValue(columns[6]);
                                     objInvoice["customer"] = searchR.getText(columns[6]);


                                     objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                                     objInvoice["invoice"] = searchR.getValue(columns[0]);

                                     objInvoice["clocationid"] = searchR.getValue(columns[1]);
                                     objInvoice["clocation"] = searchR.getText(columns[1]);
                                     objInvoice["locationid"] = searchR.getValue(columns[2]);
                                     objInvoice["location"] = searchR.getText(columns[2]);

                                     objInvoice["quantity"] = searchR.getValue(columns[4]);
                                     objInvoice["rate"] = searchR.getValue(columns[3]);
                                     objInvoice["amount"] = searchR.getValue(columns[5]);
                                     objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                                     objInvoice["leaf"] = true;

                                     arrInvoices.push(objInvoice);

                                 }
                             }
                             objQueue7["children"] = arrInvoices;
                             objQueue7["expanded"] = false;

                             objQueue7["faicon"] = 'fa fa-th SteelBlue2';
                             arrQueues.push(objQueue7);


                             var objQueue6 = new Object();
                             objQueue6["node"] = '21/05/2018';
                             objQueue6["nodetype"] = 'batch';
                             objQueue6["batchid"] = '21/05/2018';
                             objQueue6["processed"] = 'T';
                             objQueue6["date"] = '21/05/2018';

                             objQueue6["typeid"] = '6';
                             objQueue6["type"] = 'NetSuite Bandwidth Overage';
                             objQueue6["leaf"] = false;
                             objQueue6["start"] = '04/21/2018';
                             objQueue6["end"] = '05/20/2018';
                             var arrInvoices=new Array();
                             var arrFil=new Array();
                             var arrCol=new Array();



                             var search = nlapiSearchRecord('invoice','customsearch5917', arrFil, arrCol);
                             if(search!=null) {

                                 for (var i = 0; search != null && i < search.length; i++) {

                                     var searchR = search[i];
                                     var columns = searchR.getAllColumns();
                                     var objInvoice = new Object();
                                     objInvoice["node"] = searchR.getText(columns[6]);
                                     objInvoice["nodetype"] = 'invoice';
                                     objInvoice["customerid"] = searchR.getValue(columns[6]);
                                     objInvoice["customer"] = searchR.getText(columns[6]);


                                     objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                                     objInvoice["invoice"] = searchR.getValue(columns[0]);

                                     objInvoice["clocationid"] = searchR.getValue(columns[1]);
                                     objInvoice["clocation"] = searchR.getText(columns[1]);
                                     objInvoice["locationid"] = searchR.getValue(columns[2]);
                                     objInvoice["location"] = searchR.getText(columns[2]);

                                     objInvoice["quantity"] = searchR.getValue(columns[4]);
                                     objInvoice["rate"] = searchR.getValue(columns[3]);
                                     objInvoice["amount"] = searchR.getValue(columns[5]);
                                     objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                                     objInvoice["leaf"] = true;

                                     arrInvoices.push(objInvoice);

                                 }
                             }
                             objQueue6["children"] = arrInvoices;
                             objQueue6["expanded"] = false;

                             objQueue6["faicon"] = 'fa fa-th SteelBlue2';
                             arrQueues.push(objQueue6);





                             var objQueue5 = new Object();
                             objQueue5["node"] = '21/04/2018';
                             objQueue5["nodetype"] = 'batch';
                             objQueue5["batchid"] = '21/04/2018';
                             objQueue5["processed"] = 'T';
                             objQueue5["date"] = '21/04/2018';

                             objQueue5["typeid"] = '6';
                             objQueue5["type"] = 'NetSuite Bandwidth Overage';
                             objQueue5["leaf"] = false;
                             objQueue5["start"] = '03/21/2018';
                             objQueue5["end"] = '04/20/2018';
                             var arrInvoices=new Array();
                             var arrFil=new Array();
                             var arrCol=new Array();



                             var search = nlapiSearchRecord('invoice','customsearch5861', arrFil, arrCol);
                             if(search!=null) {

                                 for (var i = 0; search != null && i < search.length; i++) {

                                     var searchR = search[i];
                                     var columns = searchR.getAllColumns();
                                     var objInvoice = new Object();
                                     objInvoice["node"] = searchR.getText(columns[6]);
                                     objInvoice["nodetype"] = 'invoice';
                                     objInvoice["customerid"] = searchR.getValue(columns[6]);
                                     objInvoice["customer"] = searchR.getText(columns[6]);


                                     objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                                     objInvoice["invoice"] = searchR.getValue(columns[0]);

                                     objInvoice["clocationid"] = searchR.getValue(columns[1]);
                                     objInvoice["clocation"] = searchR.getText(columns[1]);
                                     objInvoice["locationid"] = searchR.getValue(columns[2]);
                                     objInvoice["location"] = searchR.getText(columns[2]);

                                     objInvoice["quantity"] = searchR.getValue(columns[4]);
                                     objInvoice["rate"] = searchR.getValue(columns[3]);
                                     objInvoice["amount"] = searchR.getValue(columns[5]);
                                     objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                                     objInvoice["leaf"] = true;

                                     arrInvoices.push(objInvoice);

                                 }
                             }
                             objQueue5["children"] = arrInvoices;
                             objQueue5["expanded"] = false;

                             objQueue5["faicon"] = 'fa fa-th SteelBlue2';
                             arrQueues.push(objQueue5);

                             var objQueue4 = new Object();
                             objQueue4["node"] = '21/03/2018';
                             objQueue4["nodetype"] = 'batch';
                             objQueue4["batchid"] = '21/03/2018';
                             objQueue4["processed"] = 'T';
                             objQueue4["date"] = '21/03/2018';

                             objQueue4["typeid"] = '6';
                             objQueue4["type"] = 'NetSuite Bandwidth Overage';
                             objQueue4["leaf"] = false;
                             objQueue4["start"] = '02/21/2018';
                             objQueue4["end"] = '03/20/2018';
                             var arrInvoices=new Array();
                             var arrFil=new Array();
                             var arrCol=new Array();



                             var search = nlapiSearchRecord('invoice','customsearch5795', arrFil, arrCol);
                             if(search!=null) {

                                 for (var i = 0; search != null && i < search.length; i++) {

                                     var searchR = search[i];
                                     var columns = searchR.getAllColumns();
                                     var objInvoice = new Object();
                                     objInvoice["node"] = searchR.getText(columns[6]);
                                     objInvoice["nodetype"] = 'invoice';
                                     objInvoice["customerid"] = searchR.getValue(columns[6]);
                                     objInvoice["customer"] = searchR.getText(columns[6]);


                                     objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                                     objInvoice["invoice"] = searchR.getValue(columns[0]);

                                     objInvoice["clocationid"] = searchR.getValue(columns[1]);
                                     objInvoice["clocation"] = searchR.getText(columns[1]);
                                     objInvoice["locationid"] = searchR.getValue(columns[2]);
                                     objInvoice["location"] = searchR.getText(columns[2]);

                                     objInvoice["quantity"] = searchR.getValue(columns[4]);
                                     objInvoice["rate"] = searchR.getValue(columns[3]);
                                     objInvoice["amount"] = searchR.getValue(columns[5]);
                                     objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                                     objInvoice["leaf"] = true;

                                     arrInvoices.push(objInvoice);

                                 }
                             }
                             objQueue4["children"] = arrInvoices;
                             objQueue4["expanded"] = false;

                             objQueue4["faicon"] = 'fa fa-th SteelBlue2';
                             arrQueues.push(objQueue4);



                                               var objQueue3 = new Object();
                                               objQueue3["node"] = '21/02/2018';
                                               objQueue3["nodetype"] = 'batch';
                                               objQueue3["batchid"] = '21/02/2018';
                                               objQueue3["processed"] = 'T';
                                               objQueue3["date"] = '21/02/2018';

                                               objQueue3["typeid"] = '6';
                                               objQueue3["type"] = 'NetSuite Bandwidth Overage';
                                               objQueue3["leaf"] = false;
                                               objQueue3["start"] = '01/21/2018';
                                               objQueue3["end"] = '02/20/2018';
                                               var arrInvoices=new Array();
                                               var arrFil=new Array();
                                               var arrCol=new Array();

                                               var search = nlapiSearchRecord('invoice','customsearch5671', arrFil, arrCol);
                                               if(search!=null) {

                                                   for (var i = 0; search != null && i < search.length; i++) {

                                                       var searchR = search[i];
                                                       var columns = searchR.getAllColumns();
                                                       var objInvoice = new Object();
                                                       objInvoice["node"] = searchR.getText(columns[6]);
                                                       objInvoice["nodetype"] = 'invoice';
                                                       objInvoice["customerid"] = searchR.getValue(columns[6]);
                                                       objInvoice["customer"] = searchR.getText(columns[6]);


                                                       objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                                                       objInvoice["invoice"] = searchR.getValue(columns[0]);

                                                       objInvoice["clocationid"] = searchR.getValue(columns[1]);
                                                       objInvoice["clocation"] = searchR.getText(columns[1]);
                                                       objInvoice["locationid"] = searchR.getValue(columns[2]);
                                                       objInvoice["location"] = searchR.getText(columns[2]);

                                                       objInvoice["quantity"] = searchR.getValue(columns[4]);
                                                       objInvoice["rate"] = searchR.getValue(columns[3]);
                                                       objInvoice["amount"] = searchR.getValue(columns[5]);
                                                       objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                                                       objInvoice["leaf"] = true;

                                                       arrInvoices.push(objInvoice);

                                                   }
                                               }
                                               objQueue3["children"] = arrInvoices;
                                               objQueue3["expanded"] = false;

                                               objQueue3["faicon"] = 'fa fa-th SteelBlue2';
                                               arrQueues.push(objQueue3);


                                               var objQueue2 = new Object();
                                               objQueue2["node"] = '21/01/2018';
                                               objQueue2["nodetype"] = 'batch';
                                               objQueue2["batchid"] = '21/01/2018';
                                               objQueue2["processed"] = 'T';
                                               objQueue2["date"] = '21/01/2018';

                                               objQueue2["typeid"] = '6';
                                               objQueue2["type"] = 'NetSuite Bandwidth Overage';
                                               objQueue2["leaf"] = false;
                                               objQueue2["start"] = '12/21/2017';
                                               objQueue2["end"] = '01/20/2018';
                                               var arrInvoices=new Array();
                                               var arrFil=new Array();
                                               var arrCol=new Array();

                                               var search = nlapiSearchRecord('invoice','customsearch5514', arrFil, arrCol);
                                               if(search!=null) {

                                                   for (var i = 0; search != null && i < search.length; i++) {

                                                       var searchR = search[i];
                                                       var columns = searchR.getAllColumns();
                                                       var objInvoice = new Object();
                                                       objInvoice["node"] = searchR.getText(columns[6]);
                                                       objInvoice["nodetype"] = 'invoice';
                                                       objInvoice["customerid"] = searchR.getValue(columns[6]);
                                                       objInvoice["customer"] = searchR.getText(columns[6]);


                                                       objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                                                       objInvoice["invoice"] = searchR.getValue(columns[0]);

                                                       objInvoice["clocationid"] = searchR.getValue(columns[1]);
                                                       objInvoice["clocation"] = searchR.getText(columns[1]);
                                                       objInvoice["locationid"] = searchR.getValue(columns[2]);
                                                       objInvoice["location"] = searchR.getText(columns[2]);

                                                       objInvoice["quantity"] = searchR.getValue(columns[4]);
                                                       objInvoice["rate"] = searchR.getValue(columns[3]);
                                                       objInvoice["amount"] = searchR.getValue(columns[5]);
                                                       objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                                                       objInvoice["leaf"] = true;

                                                       arrInvoices.push(objInvoice);

                                                   }
                                               }
                                               objQueue2["children"] = arrInvoices;
                                               objQueue2["expanded"] = false;

                                               objQueue2["faicon"] = 'fa fa-th SteelBlue2';
                                               arrQueues.push(objQueue2);





                                               var objQueue1 = new Object();
                                               objQueue1["node"] = '21/12/2017';
                                               objQueue1["nodetype"] = 'batch';
                                               objQueue1["batchid"] = '21/12/2017';
                                               objQueue1["processed"] = 'T';
                                               objQueue1["date"] = '12/22/2017';

                                               objQueue1["typeid"] = '6';
                                               objQueue1["type"] = 'NetSuite Bandwidth Overage';
                                               objQueue1["leaf"] = false;
                                               objQueue1["start"] = '11/21/2017';
                                               objQueue1["end"] = '12/20/2017';
                                               var arrInvoices=new Array();
                                               var arrFil=new Array();
                                               var arrCol=new Array();

                                               var search = nlapiSearchRecord('invoice','customsearch5457', arrFil, arrCol);
                                               if(search!=null) {

                                                   for (var i = 0; search != null && i < search.length; i++) {

                                                       var searchR = search[i];
                                                       var columns = searchR.getAllColumns();
                                                       var objInvoice = new Object();
                                                       objInvoice["node"] = searchR.getText(columns[6]);
                                                       objInvoice["nodetype"] = 'invoice';
                                                       objInvoice["customerid"] = searchR.getValue(columns[6]);
                                                       objInvoice["customer"] = searchR.getText(columns[6]);


                                                       objInvoice["invoiceid"] = searchR.getValue(columns[0]);
                                                       objInvoice["invoice"] = searchR.getValue(columns[0]);

                                                       objInvoice["clocationid"] = searchR.getValue(columns[1]);
                                                       objInvoice["clocation"] = searchR.getText(columns[1]);
                                                       objInvoice["locationid"] = searchR.getValue(columns[2]);
                                                       objInvoice["location"] = searchR.getText(columns[2]);

                                                       objInvoice["quantity"] = searchR.getValue(columns[4]);
                                                       objInvoice["rate"] = searchR.getValue(columns[3]);
                                                       objInvoice["amount"] = searchR.getValue(columns[5]);
                                                       objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                                                       objInvoice["leaf"] = true;

                                                       arrInvoices.push(objInvoice);

                                                   }
                                               }
                                               objQueue1["children"] = arrInvoices;
                                               objQueue1["expanded"] = false;

                                               objQueue1["faicon"] = 'fa fa-th SteelBlue2';
                                               arrQueues.push(objQueue1);*/
        for ( var q = 0; searchQueues != null && q < searchQueues.length; q++ ) {

            var objQueue = new Object();
            objQueue["node"] = searchQueues[q].getValue('internalid',null,null);
            objQueue["nodetype"] = 'batch';
            objQueue["batchid"] = parseInt(searchQueues[q].getValue('internalid',null,null));
            var fileid = parseInt(searchQueues[q].getValue('custrecord_clgx_queue_json_fileid',null,null));
            objQueue["fileid"] = fileid;
            var processed = searchQueues[q].getValue('custrecord_clgx_queue_json_processed',null,null);
            objQueue["processed"] = searchQueues[q].getValue('custrecord_clgx_queue_json_processed',null,null);
            objQueue["date"] = searchQueues[q].getValue('custrecord_clgx_queue_json_date',null,null);

            objQueue["typeid"] = searchQueues[q].getValue('custrecord_clgx_queue_json_type',null,null);
            objQueue["type"] = searchQueues[q].getText('custrecord_clgx_queue_json_type',null,null);


            try {
                var objFile = nlapiLoadFile(fileid);
                var fileName = objFile.getName();
                var objBatch = JSON.parse(objFile.getValue());

                objQueue["start"] = objBatch.start;
                objQueue["end"] = objBatch.end;

                var arrInvoices = new Array();

                if(objQueue.typeid == 4){
                    var invoices = objBatch.lines;
                } else {
                    var invoices = objBatch.children;
                }



                for ( var i = 0; invoices != null && i < invoices.length; i++ ) {

                    var objInvoice = new Object();

                    objInvoice["node"] = invoices[i].customer;
                    objInvoice["nodetype"] = 'invoice';
                    objInvoice["customerid"] = invoices[i].customerid;
                    objInvoice["customer"] = invoices[i].customer;

                    objInvoice["iaddressee"] = invoices[i].iaddressee;
                    objInvoice["currencyid"] = invoices[i].currencyid;
                    objInvoice["currency"] = invoices[i].currency;
                    objInvoice["iaddressee"] = invoices[i].iaddressee;
                    objInvoice["invoiceid"] = invoices[i].invoiceid;
                    objInvoice["invoice"] = invoices[i].invoice;
                    objInvoice["customerid"] = invoices[i].customerid;
                    objInvoice["customer"] = _.last((invoices[i].customer).split(":")).trim();
                    objInvoice["clocationid"] = invoices[i].clocationid;
                    objInvoice["clocation"] = invoices[i].clocation;
                    objInvoice["locationid"] = invoices[i].locationid;
                    objInvoice["location"] = invoices[i].location;

                    objInvoice["utilrate"] = invoices[i].utilrate;
                    objInvoice["commitrate"] = invoices[i].commitrate;
                    objInvoice["commitkwh"] = invoices[i].commitkwh;
                    objInvoice["kwhadj"] = invoices[i].kwhadj;

                    objInvoice["outage"] = invoices[i].outage;
                    objInvoice["itemid"] = invoices[i].itemid;
                    objInvoice["description"] = invoices[i].description;
                    objInvoice["quantity"] = invoices[i].quantity;
                    objInvoice["rate"] = invoices[i].rate;
                    objInvoice["amount"] = invoices[i].amount;

                    if(invoices[i].invoiceid > 0){
                        objInvoice["faicon"] = 'fa fa-list-ul SteelBlue1';
                    }
                    else{
                        objInvoice["faicon"] = 'fa fa-list-ul Crimson1';
                    }

                    objInvoice["leaf"] = true;

                    arrInvoices.push(objInvoice);
                }
                arrInvoices = _.sortBy(arrInvoices, function(obj){ return obj.customer;});

                objQueue["children"] = arrInvoices;
                objQueue["expanded"] = false;
                if(processed == 'T'){
                    objQueue["faicon"] = 'fa fa-th SteelBlue2';
                }
                else{
                    objQueue["faicon"] = 'fa fa-th Crimson1';
                }

                objQueue["leaf"] = false;


                arrQueues.push(objQueue);


            }
            catch (e) {
                var str = String(e);
                if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
                    html = 'The requested file does not exist.';
                }
            }
        }




        objTree["children"] = arrQueues;


        var searchSOs = nlapiSearchRecord('transaction', 'customsearch_clgx_inv_legacy_so_ids');
        var arrSOs = new Array();
        for ( var s = 0; s < searchSOs.length; s++ ) {
            var objSO = new Object();
            objSO["customerid"] = parseInt(searchSOs[s].getValue('internalid','customerMain','GROUP'));
            objSO["soid"] = parseInt(searchSOs[s].getValue('internalid',null,'GROUP'));
            objSO["legacyid"] = searchSOs[s].getValue('custbody_cologix_legacy_so',null,'GROUP');
            objSO["partnerid"] = searchSOs[s].getValue('partner',null,'GROUP');
            arrSOs.push(objSO);
        }

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null).setSort(true));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_queue_json_fileid',null,null));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_queue_json_type",null,"anyof",5));
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_queue_json_date",null,"after",after));
        var searchBatches = nlapiSearchRecord('customrecord_clgx_queue_json', null, arrFilters, arrColumns);
        var arrBatches = new Array();
        for ( var q = 0; searchBatches != null && q < searchBatches.length; q++ ) {

            var batchid = parseInt(searchBatches[q].getValue('internalid',null,null));
            var fileid = parseInt(searchBatches[q].getValue('custrecord_clgx_queue_json_fileid',null,null));

            var objFile = nlapiLoadFile(fileid);
            var fileName = objFile.getName();
            var fileFolder = objFile.getFolder();

            var objBatch = JSON.parse(objFile.getValue());

            for ( var c = 0; c < objBatch.children.length; c++ ) {

                for ( var i = 0; i < objBatch.children[c].children.length; i++ ) {

                    var serviceid = (objBatch.children[c].children[i].serviceid).toString();
                    var legacyid = (parseInt(serviceid.substr(serviceid.length - 5), 10)).toString();
                    objBatch.children[c].children[i]["legacyid"] = legacyid;

                    var arrFindSOs = _.filter(arrSOs, function(arr){
                        return (arr.legacyid == legacyid);
                    });

                    if(arrFindSOs.length > 0){
                        objBatch.children[c].children[i]["soid"] = arrFindSOs[0].soid;
                        //objBatch.children[c].children[i]["nbrsos"] = arrFindSOs.length;
                    }
                    else{
                        objBatch.children[c].children[i]["soid"] = 0;
                        //objBatch.children[c].children[i]["nbrsos"] = 0;
                    }
                }
            }
            arrBatches.push(objBatch);
        }

        var objTree2 = new Object();
        objTree2["text"] = '.';
        objTree2["children"] = arrBatches;

        var objFile = nlapiLoadFile(3192226);
        var html = objFile.getValue();
        html = html.replace(new RegExp('{usage}','g'), JSON.stringify(objTree));
        html = html.replace(new RegExp('{usageNAC}','g'), JSON.stringify(objTree2));
        response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}