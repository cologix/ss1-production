nlapiLogExecution("audit","FLOStart",new Date().getTime());
function BillStoredShipments() {
	try {
		nlapiLogExecution('DEBUG', 'BillStoredShipments Begin');
		// Do Billings
		DoShipmentBillings();
		nlapiLogExecution('DEBUG', 'BillStoredShipments End');
	} catch (error) {
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error BillStoredShipments', error.getCode() + ': ' + error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR', 'Unexpected Error BillStoredShipments', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}