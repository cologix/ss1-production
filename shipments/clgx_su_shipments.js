nlapiLogExecution("audit","FLOStart",new Date().getTime());
function beforeLoad(type, form) {
	try {
		var currentContext = nlapiGetContext();
		if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'view' || type == 'edit')) {
			// Add buttons and Links
			var id = nlapiGetRecordId();
			var record = nlapiLoadRecord('customrecord_clgx_shipments', id);
			var status = record.getFieldValue('custrecord_clgx_ship_status');
			
			// 7 : Waiting -> Mark received (1)
			// 1 : Received -> Mark Claimed (3)
			// 2 : Stored -> Mark Claimed (3)
			
			if (status == 7) {
				form.setScript('customscript_clgx_lib_shipments');
				form.addButton('custpage_add_markreceived_button', 'Mark Received', 'UserEvent_MarkReceived()');
			} else if (status == 1 || status == 2) {
				form.setScript('customscript_clgx_lib_shipments');
				form.addButton('custpage_add_markclaimed_button', 'Mark Claimed', 'UserEvent_MarkClaimed()');
				form.addButton('custpage_add_marklost_button', 'Mark Shipment Lost', 'UserEvent_MarkLost()');
				form.addButton('custpage_add_dobilling', 'DEBUG: Do Billings', 'UserEvent_DoBilling()');
			}
			
			if (status != 5 && status != 4) {
				if (currentContext.getExecutionContext() == 'userinterface'){
					var createNewReqLink = form.addField('custpage_new_req_link','inlinehtml', null, null, null);
					var company_id = record.getFieldValue('custrecord_clgx_ship_tocustomer');
					var recCompany = nlapiLoadRecord('customer', company_id);
					var company_name = recCompany.getFieldValue('companyname');
	 
					var to_name = record.getFieldValue('custrecord_clgx_ship_description');
					var trackingnum = record.getFieldValue('custrecord_clgx_ship_trackingnumber');
					var date_delivered = record.getFieldValue('custrecord_clgx_ship_delivereddate');
					var linkURL = 'https://tools.nac.net/pdfgen/nspackagelabel.php?package_id=' + id + '&from=' + to_name + '&to_name=' + company_name + '&trackingnum=' + trackingnum + '&date=' + date_delivered;
					createNewReqLink.setDefaultValue('<BR><BR><B>Click <A HREF="' + linkURL + '" target="_blank">here</A> to print a shipping label</B>');
				}
			}
			
			var tracking_no = record.getFieldValue('custrecord_clgx_ship_trackingnumber');
			if ((currentContext.getExecutionContext() == 'userinterface') && (tracking_no != null)) {
				var carrier_id = record.getFieldValue('custrecord_clgx_ship_carrier');
				var recCarrier = nlapiLoadRecord('customrecord_clgx_shipment_carriers', carrier_id);
				var trackurl = recCarrier.getFieldValue('custrecord_clgx_sc_trackingurl');
				trackurl = trackurl.replace('%TrackingNo%', tracking_no);
				var createNewTrackLink = form.addField('custpage_new_track_link','inlinehtml', null, null, null);
				createNewTrackLink.setDefaultValue('<BR><BR><B>Click <A HREF="' + trackurl + '" target="_blank">here</A> for shipment tracking</B>');
			}
		}
	} catch (error) { // Start Catch Errors Section
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error Shipments_BeforeLoad', error.getCode() + ': ' + error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR', 'Unexpected Error Shipments_BeforeLoad', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}

function afterSubmit (type) {
	try {
		var currentContext = nlapiGetContext();
		if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'create' || type == 'edit')) {
			var id = nlapiGetRecordId();
			VerifyOneBoxOrMore(id);
		}
	} catch (error) { // Start Catch Errors Section
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error Shipments_afterSubmit', error.getCode() + ': ' + error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR', 'Unexpected Error Shipments_afterSubmit', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}