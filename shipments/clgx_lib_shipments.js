nlapiLogExecution("audit","FLOStart",new Date().getTime());
function UserEvent_MarkReceived() {
	if (confirm('Are you sure you wish to mark this shipment as received?') == false) {
		return;
	}
	try {
		var id = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'UserEvent_MarkReceived [' + id + ']');

		var record = nlapiLoadRecord('customrecord_clgx_shipments', id);
		record.setFieldValue('custrecord_clgx_ship_status', 1);
		record.setFieldValue('custrecord_clgx_ship_signedby', nlapiGetUser());
		record.setFieldValue('custrecord_clgx_ship_delivereddate', moment().format('MM/DD/YYYY'));
		record.setFieldValue('custrecord_clgx_ship_lastnotify', moment().format('MM/DD/YYYY'));
		nlapiSubmitRecord(record, false, true);

		// Send notifications
		SendShipmentReceivedNotification(id)

		location.reload();
	} catch (error) {
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error UserEvent_MarkReceived', error.getCode() + ': ' + error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR', 'Unexpected Error UserEvent_MarkReceived', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}

function UserEvent_MarkClaimed() {
	if (confirm('Are you sure you wish to mark this shipment as claimed?') == false) {
		return;
	}
	try {
		var id = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'UserEvent_MarkClaimed [' + id + ']');

		var record = nlapiLoadRecord('customrecord_clgx_shipments', id);
		record.setFieldValue('custrecord_clgx_ship_receivedby', nlapiGetUser());
		record.setFieldValue('custrecord_clgx_ship_claimeddate', moment().format('MM/DD/YYYY'));
		record.setFieldValue('custrecord_clgx_ship_status', 3);
		
		nlapiSubmitRecord(record, false, true);
		
		location.reload();
	} catch (error) {
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error UserEvent_MarkClaimed', error.getCode() + ': ' + error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR', 'Unexpected Error UserEvent_MarkClaimed', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}

function UserEvent_MarkLost() {
	if (confirm('Are you sure you wish to mark this shipment as lost?') == false) {
		return;
	}
	try {
		var id = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'UserEvent_MarkLost [' + id + ']');

		var record = nlapiLoadRecord('customrecord_clgx_shipments', id);
		record.setFieldValue('custrecord_clgx_ship_status', 4);
		record.setFieldValue('custrecord_clgx_ship_receivedby', nlapiGetUser());
		record.setFieldValue('custrecord_clgx_ship_claimeddate', moment().format('MM/DD/YYYY'));
		
		nlapiSubmitRecord(record, false, true);
		
		location.reload();
	} catch (error) {
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error UserEvent_MarkLost', error.getCode() + ': ' + error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR', 'Unexpected Error UserEvent_MarkLost', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}

function DoShipmentBillings() {
	try {

		var arrColumns = new Array();
		arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_ship_status', null, 'anyof', [1, 2]));
		arrFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_ship_tocontact', null, 'isnotempty'));
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_ship_status', null, 'noneof', [2763]));
		var rows = nlapiSearchRecord('customrecord_clgx_shipments', null, arrFilters, arrColumns);
		for (var i = 0; rows != null && i < rows.length; i++) {
			var row = rows[i];
			var id = row.getValue('internalid');
			DoShipmentBilling(id);
		}
	} catch (error) {
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error DoShipmentBillings', error.getCode() + ': ' + error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR', 'Unexpected Error DoShipmentBillings', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}

function UserEvent_DoBilling() {
	try {
		var id = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'UserEvent_DoBilling [' + id + ']');
		
		var today_date = moment().format('MM/DD/YYYY');
		
		var record = nlapiLoadRecord('customrecord_clgx_shipments', id);
		var status = record.getFieldValue('custrecord_clgx_ship_status', 1);
		var company_id = record.getFieldValue('custrecord_clgx_ship_tocustomer');
		if ((status == 1 || status == 2) && ( company_id != null) && (company_id != 2763)) {
			
			var delivered_date = record.getFieldValue('custrecord_clgx_ship_delivereddate');
			var billedto_date = record.getFieldValue('custrecord_clgx_ship_billedto');
			var shipmenttype = record.getFieldValue('custrecord_clgx_ship_type');
			var shiptype = nlapiLoadRecord('customrecord_clgx_shipment_types', shipmenttype);
			var billafter = shiptype.getFieldValue('custrecord_clgx_st_billafterdays');
	
			if (billedto_date == null) {
				var newDT = moment(delivered_date).add(billafter, 'days');
				billedto_date = newDT;
			} else {
				var newDT = moment(billedto_date).add(billafter, 'days');
			}
		
			var bill_days = moment(today_date).diff(moment(billedto_date), 'days');
			if (bill_days <= 0) {
				alert('No days of billing required.')
				return;
			}
			
			var msg = 'Are you sure you wish to run billings for [' + bill_days + '] days from [' + moment(billedto_date).format('MM/DD/YYYY') + '] to [' + moment(today_date).format('MM/DD/YYYY') + ']?';
			if (confirm(msg) == false) {
				return;
			}
			
			DoShipmentBilling(id);
			
			location.reload();
		} else {			
			alert('Billing not applicable.')
			return;
		}
	} catch (error) {
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error UserEvent_MarkReceived', error.getCode() + ': ' + error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR', 'Unexpected Error UserEvent_MarkReceived', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}

function DoShipmentBilling(id) {
	try {
		var today_date = moment().format('MM/DD/YYYY');
		var record = nlapiLoadRecord('customrecord_clgx_shipments', id);
		var delivered_date = record.getFieldValue('custrecord_clgx_ship_delivereddate');
		var billedto_date = record.getFieldValue('custrecord_clgx_ship_billedto');
		var claimed_date = record.getFieldValue('custrecord_clgx_ship_claimeddate');
		var shipmenttype = record.getFieldValue('custrecord_clgx_ship_type');
		var shiptype = nlapiLoadRecord('customrecord_clgx_shipment_types', shipmenttype);
		var dailyrate = shiptype.getFieldValue('custrecord_clgx_st_dailyrate');
		var billafter = shiptype.getFieldValue('custrecord_clgx_st_billafterdays');
		var fromcompany = record.getFieldValue('custrecord_clgx_ship_description');
		var comments = record.getFieldValue('custrecord_clgx_ship_comments');
	
		var company_id = record.getFieldValue('custrecord_clgx_ship_tocustomer');
		var recCompany = nlapiLoadRecord('customer', company_id);
		var company = recCompany.getFieldValue('companyname');
	
		var carrier_id = record.getFieldValue('custrecord_clgx_ship_carrier');
		var recCarrier = nlapiLoadRecord('customrecord_clgx_shipment_carriers', carrier_id);
		var carrier = recCarrier.getFieldValue('name');
	
		var location_id = record.getFieldValue('custrecord_clgx_ship_location');
		var recLocation = nlapiLoadRecord('customrecord_clgx_shipment_mailroom', location_id);
		var location = recLocation.getFieldValue('name');
	
		var facility_id = recLocation.getFieldValue('location');
		var recFacility = nlapiLoadRecord('customer', location_id);
		var facility = recFacility.getFieldValue('name');
	
		var staff_id = record.getFieldValue('custrecord_clgx_ship_signedby');
		var staff = nlapiLoadRecord('employee', staff_id);
		var staff_fname = staff.getFieldValue('firstname');
		var staff_lname = staff.getFieldValue('lastname');
		var staff_name = staff_fname + ' ' + staff_lname;
	
		if (billedto_date == null) {
			var newDT = moment(delivered_date).add(billafter, 'days');
			billedto_date = newDT;
		} else {
			var newDT = moment(billedto_date).add(billafter, 'days');
		}
	
		var bill_days = moment(today_date).diff(moment(billedto_date), 'days');
		nlapiLogExecution('DEBUG', 'DoShipmentBillings [' + id + ']', '[' + delivered_date + '] [' + moment(billedto_date).format('MM/DD/YYYY') + ']-[' + moment(today_date).format('MM/DD/YYYY') + '] [' + billafter + '] [' + moment(newDT).format('MM/DD/YYYY') + ']<[' + moment(today_date).format('MM/DD/YYYY') + '] [' + bill_days + ']');
		//alert('DoShipmentBillings [' + id + '] [' + delivered_date + '] [' + moment(billedto_date).format('MM/DD/YYYY') + ']-[' + moment(today_date).format('MM/DD/YYYY') + '] [' + billafter + '] [' + moment(newDT).format('MM/DD/YYYY') + ']<[' + moment(today_date).format('MM/DD/YYYY') + '] [' + bill_days + ']');
	
		if (bill_days > 0) {
			//alert('bill it [' + bill_days + ']');
			nlapiLogExecution('DEBUG', 'DoShipmentBillings [' + id + '] days=' + bill_days);
			var recCase = nlapiCreateRecord('supportcase');
			recCase.setFieldValue('title', 'Packagetracker - Please Invoice Storage Fees');
	
			recCase.setFieldValue('category', 11); // Finance
			recCase.setFieldValue('custevent_cologix_facility', facility_id);
			recCase.setFieldValue('custevent_cologix_sub_case_type', 110);
			recCase.setFieldValue('status', 1);
			recCase.setFieldValue('priority', 2);
			recCase.setFieldValue('origin', 7);
			recCase.setFieldValue('profile', 22);
			recCase.setFieldValue('assigned', 713181);
			recCase.setFieldValue('email', 'chad.cummins@cologix.com');
	
			case_body = '';
			case_body += 'Please Invoice for billable packages left on the loading dock.\n';
			case_body += '\n';
			case_body += '*****************************************************\n';
			case_body += 'Invoice Line Text: Physical Storage Fees for Shipment #' + id + '\n';
			case_body += '\n';
			case_body += '*** Shipment Details\n';
			case_body += 'NetSuiteID: ' + company_id + '\n';
			case_body += 'ShipmentID: ' + id + '\n';
			case_body += '\n';
	
			case_body += 'From Company: ' + fromcompany + '\n';
			case_body += 'Shipping Method: ' + carrier + '\n';
			case_body += 'Location: ' + location + ' ' + facility + '\n';
			case_body += 'Comments Private: ' + comments + '\n';
			case_body += '\n';
	
			var arrBoxColumns = new Array();
			arrBoxColumns[0] = new nlobjSearchColumn('internalid', null, null);
			var arrBoxFilters = new Array();
			arrBoxFilters.push(new nlobjSearchFilter('custrecord_clgx_sb_shipment', null, 'anyof', id));
			arrBoxFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
			var boxrows = nlapiSearchRecord('customrecord_clgx_shipment_boxes', null, arrBoxFilters, arrBoxColumns);
	
			case_body += '  Box Count: (' + boxrows.length + ')\n';
			case_body += '\n';
	
			grand_total = 0;
	
			fromdate = self.signed_in_date;
			if (billedto_date != null) {
				from_date = billedto_date;
			}
			if (claimed_date != null && claimed_date < today_date) {
				to_date = claimed_date;
			} else {
				to_date = today_date;
			}
	
			for (var i = 0; rows != null && i < boxrows.length; i++) {
				var boxrow = boxrows[i];
				var boxid = boxrow.getValue('internalid');
				var box = nlapiLoadRecord('customrecord_clgx_shipment_boxes', boxid);
				var tracking = box.getFieldValue('custrecord_clgx_sb_trackingnumber');
	
				case_body += '  ----- Box Details ------\n';
				case_body += '  Carrier Tracking Number: ' + tracking + '\n';
				case_body += '  Signed By Cologix Employee: ' + staff_name + ' - ' + claimed_date + '\n';
				case_body += '  As of: ' + moment(today_date).format('MM/DD/YYYY') + '\n';
				case_body += '  Rate: $' + dailyrate + ' for ' + bill_days + ' days.  (billable after ' + billafter + ' days)\n';
	
				case_body += '  Billing From ' + moment(from_date).format('MM/DD/YYYY') + ' to ' + moment(to_date).format('MM/DD/YYYY') + '\n';
	
				sub_total = dailyrate * bill_days;
				case_body += '  Subtotal for this Box: $' + sub_total + '\n\n';
				grand_total += sub_total;
			}
			case_body += '\n';
			case_body += '-====== Total charge for ALL Shipments: $' + grand_total + ' =====-';
	
			recCase.setFieldValue('incomingmessage', case_body);
	
			record.setFieldValue('custrecord_clgx_ship_billedto', today_date);
	
			try {
				var caseid = nlapiSubmitRecord(recCase, false, true);
			} catch (error) {
				if (error.getDetails != undefined) {
					nlapiLogExecution('ERROR', 'Process Error DoShipmentBilling [' + id + '] Update BilledToDate', error.getCode() + ': ' + error.getDetails());
					throw error;
				} else {
					nlapiLogExecution('ERROR', 'Unexpected Error DoShipmentBilling [' + id + '] Update BilledToDate', error.toString());
					throw nlapiCreateError('99999', error.toString());
				}
			}
	
			var invoiceline = 'Physical Storage Fees for Shipment #' + id + '; ' + moment(from_date).format('MM/DD/YYYY') + ' to ' + moment(to_date).format('MM/DD/YYYY');
			var recInvoice = nlapiCreateRecord('invoice');
			recInvoice.setFieldValue('entity', company_id);
			recInvoice.setFieldValue('subsidiary', 5);
			recInvoice.setFieldValue('trandate', today_date);
			recInvoice.setFieldValue('startdate', today_date);
			recInvoice.setFieldValue('location', facility_id);
	
			recInvoice.setLineItemValue('item', 'item', 1, 586);
			recInvoice.setLineItemValue('item', 'quantity', 1, 1);
			recInvoice.setLineItemValue('item', 'rate', 1, grand_total);
			recInvoice.setLineItemValue('item', 'location', 1, facility_id);
			recInvoice.setLineItemValue('item', 'amount', 1, grand_total);
			recInvoice.setLineItemValue('item', 'description', 1, invoiceline);
			recInvoice.setLineItemValue('item','custcol_clgx_rh_invoice_item_case', 1, caseid);
	
			try {
				nlapiSubmitRecord(recInvoice, false, true);
			} catch (error) {
				if (error.getDetails != undefined) {
					nlapiLogExecution('ERROR', 'Process Error DoShipmentBilling [' + id + '] Submit Invoice', error.getCode() + ': ' + error.getDetails());
					throw error;
				} else {
					nlapiLogExecution('ERROR', 'Unexpected Error DoShipmentBilling [' + id + '] Submit Invoice', error.toString());
					throw nlapiCreateError('99999', error.toString());
				}
			}
			try {
				nlapiSubmitRecord(recCase, false, true);
				// nlapiSendEmail(nlapiGetUser(), 797630, 'case test', case_body, null, null, null, null);
	
			} catch (error) {
				if (error.getDetails != undefined) {
					nlapiLogExecution('ERROR', 'Process Error DoShipmentBilling [' + id + '] Submit Case', error.getCode() + ': ' + error.getDetails());
					throw error;
				} else {
					nlapiLogExecution('ERROR', 'Unexpected Error DoShipmentBilling [' + id + '] Submit Case', error.toString());
					throw nlapiCreateError('99999', error.toString());
				}
			}
		}	
	} catch (error) {
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error DoShipmentBilling [' + id + ']', error.getCode() + ': ' + error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR', 'Unexpected Error DoShipmentBilling [' + id + ']', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}	
}

function SendShipmentReceivedNotification(id) {
	try {
		var today_date = moment().format('MM/DD/YYYY');

		var id = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'SendShipmentReceivedNotification [' + id + ']');
		var record = nlapiLoadRecord('customrecord_clgx_shipments', id);
		var to_contactid = record.getFieldValue('custrecord_clgx_ship_tocontact');
		
		var delivered_date = record.getFieldValue('custrecord_clgx_ship_delivereddate');
		var staff_id = record.getFieldValue('custrecord_clgx_ship_signedby');
		var staff = nlapiLoadRecord('employee', staff_id);
		var staff_fname = staff.getFieldValue('firstname');
		var staff_lname = staff.getFieldValue('lastname');
		var staff_name = staff_fname + ' ' + staff_lname;
		var from_company = record.getFieldValue('custrecord_clgx_ship_description');

		var dt_diff = moment(delivered_date).diff(today_date, 'days');

		var emailSubject = 'Shipment ' + id + ' has arrived.';
		emailBody = '';
		emailBody += 'A Shipment is Waiting to be claimed at Cologix\n';
		emailBody += '\n';
		emailBody += 'If your package is not picked up after (7) days, Cologix may charge a storage fee for any packages left ';
		emailBody += 'unclaimed.  Please contact us to arrange for pickup or storage of your shipment. ';
		emailBody += 'Call us at (MMU) 973-590-5016 or (WBR) 973-590-5183 and please refer to your Shipment ID. \n';
		emailBody += 'Daily storage fees are $10.00 per package and $15.00 per pallet.\n';
		emailBody += '\n';
		emailBody += '\n';
		emailBody += 'Shipment ID: ' + id + '\n';
		emailBody += 'From Company: ' + from_company + '\n';
		emailBody += 'Signed By Cologix: ' + staff_name + '\n';
		emailBody += '\n';
		emailBody += '\n';
		emailBody += 'Sent from Cologix Package Tracker. \n\n';
		emailBody += 'Track your shipments to us using Cologix Customer Portal https://my.cologix.com/portal \n';
		emailBody += 'Call (NNJ2) 973-590-5016 or (NNJ3) 973-590-5183 to contact the loading dock. \n';

		try {
			if (to_contactid != null) {
					nlapiSendEmail(nlapiGetUser(),to_contactid,emailSubject,emailBody,null,null,null,null);
			}
		} catch (error) {
			if (error.getDetails != undefined) {
				nlapiLogExecution('ERROR', 'Process Error SendShipmentReceivedNotification Send Email', error.getCode() + ': ' + error.getDetails());
				throw error;
			} else {
				nlapiLogExecution('ERROR', 'Unexpected Error SendShipmentReceivedNotification Send Email', error.toString());
				throw nlapiCreateError('99999', error.toString());
			}
		}

		var notify_contacts = record.getFieldValues('custrecord_clgx_ship_contacts');
		var test = notify_contacts[0];
		if (test.length == 1) {
			var contactID = notify_contacts;
			try {
				nlapiSendEmail(nlapiGetUser(),contactID,emailSubject,emailBody,null,null,null,null);
			} catch (error) {
				if (error.getDetails != undefined) {
					nlapiLogExecution('ERROR', 'Process Error SendShipmentReceivedNotification Send Email addtl', error.getCode() + ': ' + error.getDetails());
					throw error;
				} else {
					nlapiLogExecution('ERROR', 'Unexpected Error SendShipmentReceivedNotification Send Email addtl', error.toString());
					throw nlapiCreateError('99999', error.toString());
				}
			}
		} else {
			for (var i = 0; notify_contacts != null && i < notify_contacts.length; i++) {
				var contactID = notify_contacts[i];
				try {
					nlapiSendEmail(nlapiGetUser(),contactID,emailSubject,emailBody,null,null,null,null);
				} catch (error) {
					if (error.getDetails != undefined) {
						nlapiLogExecution('ERROR', 'Process Error SendShipmentReceivedNotification Send Email addtl', error.getCode() + ': ' + error.getDetails());
						throw error;
					} else {
						nlapiLogExecution('ERROR', 'Unexpected Error SendShipmentReceivedNotification Send Email addtl', error.toString());
						throw nlapiCreateError('99999', error.toString());
					}
				}
			}
		}
	} catch (error) {
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error SendShipmentReceivedNotification', error.getCode() + ': ' + error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR', 'Unexpected Error SendShipmentReceivedNotification', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}

function SendShipmentUnclaimedNotifications() {
	try {
		var today_date = moment().format('MM/DD/YYYY');

		var arrColumns = new Array();
		arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_ship_status', null, 'anyof', [ 1, 2 ]));
		arrFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_ship_tocontact', null, 'isnotempty'));
		var rows = nlapiSearchRecord('customrecord_clgx_shipments', null, arrFilters, arrColumns);
		for (var i = 0; rows != null && i < rows.length; i++) {
			var row = rows[i];
			var id = row.getValue('internalid');
			var record = nlapiLoadRecord('customrecord_clgx_shipments', id);
			var delivered_date = record.getFieldValue('custrecord_clgx_ship_delivereddate');
			var last_notify = record.getFieldValue('custrecord_clgx_ship_lastnotify');
			if (last_notify == null) {
				last_notify = delivered_date;
			}

			var newDT = moment(last_notify).add(3, 'days');
			if ((newDT < today_date) == true) {
				SendShipmentUnclaimedNotification(id);
			}
		}
	} catch (error) {
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error SendShipmentUnclaimedNotifications', error.getCode() + ': ' + error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR', 'Unexpected Error SendShipmentUnclaimedNotifications', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}

function SendShipmentUnclaimedNotification(id) {
	try {
		var today_date = moment().format('MM/DD/YYYY');

		var record = nlapiLoadRecord('customrecord_clgx_shipments', id);

		var id = nlapiGetRecordId();
		nlapiLogExecution('DEBUG', 'SendShipmentUnclaimedNotification [' + id + ']');
		var record = nlapiLoadRecord('customrecord_clgx_shipments', id);
		var contactID = record.getFieldValue('custrecord_clgx_ship_tocontact');
		var delivered_date = record.getFieldValue('custrecord_clgx_ship_delivereddate');
		var staff_id = record.getFieldValue('custrecord_clgx_ship_signedby');
		var staff = nlapiLoadRecord('employee', staff_id);
		var staff_fname = staff.getFieldValue('firstname');
		var staff_lname = staff.getFieldValue('lastname');
		var staff_name = staff_fname + ' ' + staff_lname;
		var from_company = record.getFieldValue('custrecord_clgx_ship_description');

		var dt_diff = moment(delivered_date).diff(today_date, 'days');

		var emailSubject = 'Shipment ' + id + ' is waiting to be Claimed for ' + dt_diff + ' days at Cologix';
		emailBody = '';
		emailBody += 'A Shipment is Waiting to be claimed at Cologix\n';
		emailBody += '\n';
		emailBody += 'If your package is not picked up after (7) days, Cologix may charge a storage fee for any packages left ';
		emailBody += 'unclaimed.  Please contact us to arrange for pickup or storage of your shipment. ';
		emailBody += 'Call us at (MMU) 973-590-5016 or (WBR) 973-590-5183 and please refer to your Shipment ID. \n';
		emailBody += 'Daily storage fees are $10.00 per package and $15.00 per pallet.\n';
		emailBody += '\n';
		emailBody += 'This notification will be sent every (3) days until the package is claimed.\n';

		emailBody += '\n';
		emailBody += 'Shipment ID: ' + id + '\n';
		emailBody += 'From Company: ' + from_company + '\n';
		emailBody += 'Signed By Cologix: ' + staff_name + '\n';
		emailBody += 'Waiting at NAC ' + dt_diff + ' days.\n';
		emailBody += '\n';
		emailBody += '\n';
		emailBody += 'Sent from Cologix Package Tracker. \n\n';
		emailBody += 'Track your shipments to us using Cologix Customer Portal https://my.cologix.com/portal \n';
		emailBody += 'Call (NNJ2) 973-590-5016 or (NNJ3) 973-590-5183 to contact the loading dock. \n';

		if (contact_id != null) {
			nlapiSendEmail(nlapiGetUser(),contactID,emailSubject,emailBody,null,null,null,null);
		}
		
		var notify_contacts = record.getFieldValues('custrecord_clgx_ship_contacts');
		var test = notify_contacts[0];
		if (test.length == 1) {
			var contactID = notify_contacts;
			try {
				nlapiSendEmail(nlapiGetUser(),contactID,emailSubject,emailBody,null,null,null,null);
			} catch (error) {
				if (error.getDetails != undefined) {
					nlapiLogExecution('ERROR', 'Process Error SendShipmentReceivedNotification Send Email addtl', error.getCode() + ': ' + error.getDetails());
					throw error;
				} else {
					nlapiLogExecution('ERROR', 'Unexpected Error SendShipmentReceivedNotification Send Email addtl', error.toString());
					throw nlapiCreateError('99999', error.toString());
				}
			}
		} else {
			for (var i = 0; notify_contacts != null && i < notify_contacts.length; i++) {
				var contactID = notify_contacts[i];
				try {
					nlapiSendEmail(nlapiGetUser(),contactID,emailSubject,emailBody,null,null,null,null);
				} catch (error) {
					if (error.getDetails != undefined) {
						nlapiLogExecution('ERROR', 'Process Error SendShipmentReceivedNotification Send Email addtl', error.getCode() + ': ' + error.getDetails());
						throw error;
					} else {
						nlapiLogExecution('ERROR', 'Unexpected Error SendShipmentReceivedNotification Send Email addtl', error.toString());
						throw nlapiCreateError('99999', error.toString());
					}
				}
			}
		}

		record.setFieldValue('custrecord_clgx_ship_lastnotify', moment().format('MM/DD/YYYY'));
		nlapiSubmitRecord(record, false, true);

	} catch (error) {
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error SendShipmentUnclaimedNotification', error.getCode() + ': ' + error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR','Unexpected Error SendShipmentUnclaimedNotification', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}

function VerifyOneBoxOrMore(id) {
	try {
		var record = nlapiLoadRecord('customrecord_clgx_shipments', id);
		record.setFieldValue('custrecord_clgx_ship_status', 1);
		var tracking_no = record.getFieldValue('custrecord_clgx_ship_trackingnumber');

		var arrBoxColumns = new Array();
		arrBoxColumns[0] = new nlobjSearchColumn('internalid', null, null);
		var arrBoxFilters = new Array();
		arrBoxFilters.push(new nlobjSearchFilter('custrecord_clgx_sb_shipment', null, 'anyof', id));
		arrBoxFilters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
		var boxrows = nlapiSearchRecord('customrecord_clgx_shipment_boxes', null, arrBoxFilters, arrBoxColumns);
		if (boxrows == null) {
			nlapiLogExecution('DEBUG', 'VerifyOneBoxOrMore add one [' + id + ']');
			var recBox = nlapiCreateRecord('customrecord_clgx_shipment_boxes');
			recBox.setFieldValue('custrecord_clgx_sb_shipment', id);
			recBox.setFieldValue('custrecord_clgx_sb_trackingnumber', tracking_no);
			try {
				nlapiSubmitRecord(recBox, false, true);
			} catch (error) {
				if (error.getDetails != undefined) {
					nlapiLogExecution('ERROR', 'Process Error VerifyOneBoxOrMore add box', error.getCode() + ': ' + error.getDetails());
					throw error;
				} else {
					nlapiLogExecution('ERROR', 'Unexpected Error VerifyOneBoxOrMore add box', error.toString());
					throw nlapiCreateError('99999', error.toString());
				}
			}
		}
	} catch (error) {
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error VerifyOneBoxOrMore', error.getCode() + ': ' + error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR', 'Unexpected Error VerifyOneBoxOrMore', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}