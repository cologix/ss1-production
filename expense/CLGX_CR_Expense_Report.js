nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_Expense_Report.js
//	Script Name:	CLGX_CR_Expense_Report
//	Script Id:		customscript_clgx_cr_expense_report
//	Script Runs:	Client
//	Script Type:	Client Script
//	Deployments:	Expense Report
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		1/20/2012
//-------------------------------------------------------------------------------------------------

function lineInit(type) {
    try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 1/20/2012
// Details:	Defaults the amount to 0 on an expense line and disables mileage fields.
//-------------------------------------------------------------------------------------------------
        if (type=='expense'){
            nlapiSetCurrentLineItemValue('expense', 'amount', 0, true, false);
            nlapiSetCurrentLineItemValue('expense', 'receipt', 'T', true, false);
            nlapiDisableLineItemField('expense', 'custcol_clgx_mileage', true);
            nlapiDisableLineItemField('expense', 'custcol_clgx_mileage_type', true);
        }
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function fieldChanged(type, name, linenum) {
    try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 1/20/2012
// Details:	Calculates the mileage amount on an expense line depending on user input of miles or km.
//-------------------------------------------------------------------------------------------------

        if (type == 'expense') {

            //var perMile = 0.535;
            //var perKm = 0.54;
            var perMile = 0.575;
            var perKm =  0.59;

            if (name == 'category') {

                nlapiSetCurrentLineItemValue('expense', 'receipt', 'T');

                var selectedCategory = nlapiGetCurrentLineItemText('expense', 'category');
                if (selectedCategory != 'Mileage') {
                    nlapiDisableLineItemField('expense', 'custcol_clgx_mileage', true);
                    nlapiDisableLineItemField('expense', 'custcol_clgx_mileage_type', true);
                    nlapiSetCurrentLineItemValue('expense', 'custcol_clgx_mileage', 0);
                }
                else{
                    nlapiDisableLineItemField('expense', 'custcol_clgx_mileage', false);
                    nlapiDisableLineItemField('expense', 'custcol_clgx_mileage_type', false);
                }
            }
            if (name == 'custcol_clgx_mileage_type' || name == 'custcol_clgx_mileage') {
                var selectedCategory = nlapiGetCurrentLineItemText('expense', 'category');
                if (selectedCategory == 'Mileage') {
                    var milesType = nlapiGetCurrentLineItemText('expense', 'custcol_clgx_mileage_type');
                    var milesNbr = parseFloat(nlapiGetCurrentLineItemValue('expense', 'custcol_clgx_mileage'));

                    if (milesType == 'Km') {
                        nlapiSetCurrentLineItemValue('expense', 'amount', perKm * milesNbr);
                    }
                    else {
                        nlapiSetCurrentLineItemValue('expense', 'amount', perMile * milesNbr);
                    }
                }
            }
        }
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function validateLine(type) {
    try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 1/20/2012
// Details:	If category is Mileage, Mileage type can't be empty and do mileage amount math.
//-------------------------------------------------------------------------------------------------

        if (type == 'expense') {
            var perMile = 0.575;
            var perKm =  0.59;
            var selectedCategory = nlapiGetCurrentLineItemText('expense', 'category');

            if (selectedCategory == 'Mileage') {
                var milesType = nlapiGetCurrentLineItemText('expense', 'custcol_clgx_mileage_type');
                var milesNbr = parseFloat(nlapiGetCurrentLineItemValue('expense', 'custcol_clgx_mileage'));

                if (milesType == 'Km') {
                    nlapiSetCurrentLineItemValue('expense', 'amount', perKm * milesNbr);
                }
                else if (milesType == 'Miles') {
                    nlapiSetCurrentLineItemValue('expense', 'amount', perMile * milesNbr);
                }
                else {
                    alert('Mileage type can not be empty');
                    return false;
                }
            }
        }
        return true;
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

    return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function saveRecord() {
    try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 1/20/2012
// Details:	Gives alerts to attach receipts on expense reports if user or approver.
//-------------------------------------------------------------------------------------------------
        var stEmployee = nlapiGetFieldValue('entity');
        var stUser = nlapiGetUser();

        if (stEmployee == stUser){
            var response = confirm('Please make sure you have attached all necessary receipts to your expense report before clicking <Save>.');
            if (response) {return true;}
            else {return false;}
        }
        else{
            var response = confirm('Please make sure your employee attached all necessary receipts to their expense report before approving it.');
            if (response) {return true;}
            else {return false;}
        }
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////