nlapiLogExecution("audit", "FLOStart", new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Case.js
//	Script Name:	CLGX_SS_Case
//	Script Id:		customscript_clgx_ss_case
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Case
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		04/12/2014
//-------------------------------------------------------------------------------------------------

function scheduled_case() {
    try {
        //------------- Begin Section 1 -------------------------------------------------------------------
        //	Details:	Create subcases for Maintenances or Incidents.
        //	Created:	04/12/2014
        //-------------------------------------------------------------------------------------------------
        nlapiLogExecution('DEBUG', 'Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');

        var scriptStart = moment();

        //retrieve script parameters
        var userid = nlapiGetContext().getSetting('SCRIPT', 'custscript_ss_case_userid');
        var caseid = nlapiGetContext().getSetting('SCRIPT', 'custscript_ss_case_caseid');
        var sendto = nlapiGetContext().getSetting('SCRIPT', 'custscript_ss_case_sendto');
        var message = nlapiGetContext().getSetting('SCRIPT', 'custscript_ss_case_message');
        if (message != null && message != '') {
            message = message.replace(/\&lt;/g, '<');
            message = message.replace(/\gt;/g, '>');
        }

        var username = nlapiLookupField('employee', userid, 'entityid');

        var recParentCase = nlapiLoadRecord('supportcase', caseid);

        var casenumber = recParentCase.getFieldValue('casenumber');
        var customer = recParentCase.getFieldValue('company');
        var title = recParentCase.getFieldValue('title');
        var assigned = recParentCase.getFieldText('assigned');

        var subsidiary = recParentCase.getFieldValue('subsidiary');
        var facility = recParentCase.getFieldValue('custevent_cologix_facility');
        var facilityname = recParentCase.getFieldText('custevent_cologix_facility');
        var profile = recParentCase.getFieldValue('profile');

        var casetype = recParentCase.getFieldValue('category');
        var casetypename = recParentCase.getFieldText('category');
        var subcasetype = recParentCase.getFieldValue('custevent_cologix_sub_case_type');

        var priority = recParentCase.getFieldText('priority');
        var status = recParentCase.getFieldText('status');

        var equipment = recParentCase.getFieldValue('custevent_cologix_equipment');
        var notify = recParentCase.getFieldValue('custevent_clgx_customer_notification');

        var followup = recParentCase.getFieldValue('custevent_cologix_case_sched_followup');
        var starttime = recParentCase.getFieldValue('custevent_cologix_sched_start_time');
        /*
        if(casetype == 'Maintenance'){
            var subcasestatus = 'Not Started';
            var priority = 'Medium';
        }
        else{
            var subcasestatus = 'Escalated';
            var priority = 'Urgent';
        }
        */
        if (status == 'Escalated') {
            var subcasestatus = 'In Progress';
        }
        else {
            subcasestatus = status;
        }
        if (status == 'CAB Approved') {
            if (facilityname.indexOf("MTL") > -1) {
                assigned = 'Customer Care-MTL';
                nlapiSubmitField('supportcase', caseid, 'assigned', 317218);
            }
            else {
                assigned = 'Customer Care-Non MTL';
                nlapiSubmitField('supportcase', caseid, 'assigned', 317219);
            }
        }


        if (priority == 'Urgent') {
            priority = 'Medium';
        }

        // start email report -----------------------------------------------------------------------------------------------------------------------------
        var startScript = moment();
        var emailAdminSubject = 'Sub Cases created for case ' + casenumber + ' by ' + username;
        var emailAdminBody = '';
        emailAdminBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');
        emailAdminBody += '<h2>Sub Cases created for case ' + casenumber + '</h2>';
        emailAdminBody += '<table border="1" cellpadding="5">';
        emailAdminBody += '<tr><td>Location</td><td>Case Number</td><td>Time</td><td>Minutes</td><td>Usage</td></tr>';

        var emailUserSubject = 'Sub Cases created for case ' + casenumber + ' by ' + username;
        var emailUserBody = '';
        emailUserBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');
        emailUserBody += '<h2>Sub Cases created for case ' + casenumber + '</h2>';
        emailUserBody += '<table border="1" cellpadding="5">';
        emailUserBody += '<tr><td>Location</td><td>Case Number</td></tr>';

        // find all customers / contacts ----------------------------------------------------------------------------------------------------------------------------------

        // find all children(FAMs) of the configured equipment(FAM)
        var arrFAMs = new Array();
        arrFAMs.push(equipment);
        for (var i = 0; i < 15; i++) {
            var arrChildren = get_fam_children(arrFAMs);
            arrFAMs = _.union(arrFAMs, arrChildren);
        }

        var arrServices = new Array();
        // Search for Power Circuits linked to this FAM record
        var filterExpression = [['isinactive', 'is', 'F'], 'and', ['custrecord_cologix_power_service', 'noneof', '@NONE@'], 'and', [['custrecord_cologix_power_ups_rect', 'anyof', arrFAMs], 'or', ['custrecord_clgx_power_panel_pdpm', 'anyof', arrFAMs], 'or', ['custrecord_clgx_power_generator', 'anyof', arrFAMs]]];
        var searchColumns = new Array();
        searchColumns.push(new nlobjSearchColumn('custrecord_cologix_power_service', null, 'GROUP'));
        var searchPowers = nlapiCreateSearch('customrecord_clgx_power_circuit', filterExpression, searchColumns);
        var resultSet = searchPowers.runSearch();
        resultSet.forEachResult(function (result) {
            var serviceid = result.getValue('custrecord_cologix_power_service', null, 'GROUP');
            if (!inArray(serviceid, arrServices)) {
                arrServices.push(serviceid);
            }
            return true;
        });

        // Search for Spaces linked to this FAM record
        var searchColumns = new Array();
        searchColumns.push(new nlobjSearchColumn('custrecord_cologix_space_project', null, 'GROUP'));
        var searchFilter = new Array();
        searchFilter.push(new nlobjSearchFilter('custrecord_cologix_space_fams', null, 'anyof', arrFAMs));
        searchFilter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
        var searchSpaces = nlapiSearchRecord('customrecord_cologix_space', null, searchFilter, searchColumns);
        for (var i = 0; searchSpaces != null && i < searchSpaces.length; i++) {
            var searchSpace = searchSpaces[i];
            var serviceid = searchSpace.getValue('custrecord_cologix_space_project', null, 'GROUP');
            if (!inArray(serviceid, arrServices)) {
                arrServices.push(serviceid);
            }
        }
        // Search for Services linked to this FAM record and add them to services array
        var searchColumns = new Array();
        searchColumns.push(new nlobjSearchColumn('internalid', null, 'GROUP'));
        var searchFilter = new Array();
        searchFilter.push(new nlobjSearchFilter('custentity_clgx_service_fams', null, 'anyof', arrFAMs));
        searchFilter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
        searchFilter.push(new nlobjSearchFilter('isinactive', 'customer', 'is', 'F'));
        var searchServices = nlapiSearchRecord('job', null, searchFilter, searchColumns);
        for (var i = 0; searchServices != null && i < searchServices.length; i++) {
            var searchService = searchServices[i];
            var serviceid = searchService.getValue('internalid', null, 'GROUP');
            if (!inArray(serviceid, arrServices)) {
                arrServices.push(serviceid);
            }
        }

        var arrCustomers = new Array();
        // Search all customers of all found services
        var searchColumns = new Array();
        searchColumns.push(new nlobjSearchColumn('customer', null, 'GROUP'));
        var searchFilter = new Array();
        if (arrServices != null) {
            searchFilter.push(new nlobjSearchFilter('internalid', null, 'anyof', arrServices));
        }
        else {
            searchFilter.push(new nlobjSearchFilter('internalid', null, 'anyof', 0));
        }
        searchFilter.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
        searchFilter.push(new nlobjSearchFilter('isinactive', 'customer', 'is', 'F'));
        searchFilter.push(new nlobjSearchFilter('status', 'customer', 'noneof', 16));
        var searchCustomers = nlapiSearchRecord('job', null, searchFilter, searchColumns);

        // create all sub cases ----------------------------------------------------------------------------------------------------------------------------------
        for (var i = 0; searchCustomers != null && i < searchCustomers.length; i++) {

            var startExec = moment(); // time when each loop is starting
            var execMinutes = (startExec.diff(startScript) / 60000).toFixed(1); // execution time in minutes

            var searchCustomer = searchCustomers[i];
            var customer = searchCustomer.getValue('customer', null, 'GROUP');

            if (!inArray(customer, arrCustomers)) {
                arrCustomers.push(customer);
            }

            var arrEmail = new Array();
            var listEmails = '';

            var searchColumns = new Array();
            searchColumns.push(new nlobjSearchColumn('email', 'contact', 'GROUP').setSort(false));
            var searchFilter = new Array();
            searchFilter.push(new nlobjSearchFilter('internalid', null, 'anyof', customer));
            searchFilter.push(new nlobjSearchFilter('custentity_clgx_ops_outage_notification', 'contact', 'is', 'T'));
            searchFilter.push(new nlobjSearchFilter('email', 'contact', 'isnot', '@NONE@'));
            searchFilter.push(new nlobjSearchFilter('isinactive', 'contact', 'is', 'F'));
            var searchOutage = nlapiSearchRecord('customer', null, searchFilter, searchColumns);

            if (searchOutage != null) {

                for (var j = 0; j < searchOutage.length; j++) {
                    var email = searchOutage[j].getValue('email', 'contact', 'GROUP');
                    //var email = searchOutage[j].getValue('email',null,'GROUP');
                    if (email.indexOf("None") < 0 && !inArray(email, arrEmail)) {
                        arrEmail.push(email);
                    }
                }
            }
            else {
                var searchColumns = new Array();
                //searchColumns.push(new nlobjSearchColumn('internalid','contact','GROUP'));
                searchColumns.push(new nlobjSearchColumn('email', 'contact', 'GROUP').setSort(false));
                var searchFilter = new Array();
                searchFilter.push(new nlobjSearchFilter('internalid', null, 'anyof', customer));
                //searchFilter.push(new nlobjSearchFilter('role', 'contact', 'anyof', -10)); // Primary Contact
                //searchFilter.push(new nlobjSearchFilter('custentity_clgx_ops_outage_notification','contact','is', 'T'));
                searchFilter.push(new nlobjSearchFilter('email', 'contact', 'isnot', '@NONE@'));
                searchFilter.push(new nlobjSearchFilter('isinactive', 'contact', 'is', 'F'));
                var searchPrimary = nlapiSearchRecord('customer', null, searchFilter, searchColumns);

                if (searchPrimary != null) {
                    for (var j = 0; j < searchPrimary.length; j++) {
                        //assigned = searchPrimary[j].getValue('internalid','contact','GROUP');
                        var email = searchPrimary[j].getValue('email', 'contact', 'GROUP');
                        //var email = searchPrimary[j].getValue('email',null,'GROUP');
                        if (email.indexOf("None") < 0 && !inArray(email, arrEmail)) {
                            arrEmail.push(email);
                        }
                    }
                }
            }
            // MOVED THIS TO LINE 269 - 285
            // var listEmails = '';
            // if(arrEmail != null){
            //     listEmails = arrEmail.join();
            //       //-----ADDED THIS FOR TESTING-------//
            //     record.setFieldValue('custevent_acs_custom_email', listEmails)
            //     nlapiLogExecution('DEBUG','---List Emails not Greater than 300---',listEmails);
            //      //----------------------------------//
            // }
            // if(listEmails.length > 300){
            //     while (listEmails.length > 300) {
            //         //-----ADDED THIS FOR TESTING-------//
            //         nlapiLogExecution('DEBUG','---List Emails Greaer than 300---',listEmails);
            //         //----------------------------------//
            //         arrEmail.pop();
            //         listEmails = arrEmail.join();
            //     }
            // }


            var casesubsidiary = nlapiLookupField('customer', customer, 'subsidiary');

            var record = nlapiCreateRecord('supportcase');

            record.setFieldValue('title', 'Childcase of ' + casenumber + ' - ' + casetypename + ' at ' + facilityname);
            record.setFieldValue('company', customer);
            record.setFieldValue('custevent_cologix_facility', facility);
            record.setFieldValue('subsidiary', casesubsidiary);
            if (assigned != null && assigned != '') {
                record.setFieldText('assigned', assigned);
            }

            nlapiLogExecution('DEBUG', 'customer', customer);

            var listEmails = '';
            // var listAllEmails = '';
            // var temp_list = new Array();
            // var spillEmails = '';
            if (arrEmail != null) {

                //-------------- DO NOT REMOVE THIS SNIPPET------
                listEmails = arrEmail.join();
                record.setFieldValue('custevent_acs_custom_email', listEmails);
                //-------------- DO NOT REMOVE THIS SNIPPET------


            }
            if (listEmails.length > 300) {
                while (listEmails.length > 300) {
                    arrEmail.pop();
                    nlapiLogExecution('DEBUG','--- arrEmail.pop();---', arrEmail.pop())
                    listEmails = arrEmail.join();
                    // temp_list.push(arrEmail.pop());
                    // listAllEmails = temp_list.join();
                }
            }



            record.setFieldValue('email', listEmails); // use all emails if multiple contacts
            //record.setFieldValue('custevent_acs_custom_email', listAllEmails);


            var get_email = record.getFieldValue('email');
            //----------------------------------//
            var get_email_spillover = record.getFieldValue('custevent_acs_custom_email');
            if(get_email_spillover != null){


                var get_email_array = new Array();
                var get_email_spillover_array = new Array();
                get_email_array = get_email.split(',');
                get_email_spillover_array = get_email_spillover.split(',');

                nlapiLogExecution('DEBUG','--- get_email_spillover---', get_email_spillover)
                if(get_email_array.length == get_email_spillover_array.length){
                    nlapiLogExecution('DEBUG','--- THERE IS NO SPILL OVER EMAIL ---')
                    record.setFieldValue('custevent_acs_custom_email', ''); // use all emails if multiple contacts
                }else{
                    nlapiLogExecution('DEBUG','--- THERE IS SPILL OVER EMAIL ---')
                    get_email_spillover_array = get_email_spillover_array.filter(function(val) {
                        return get_email_array.indexOf(val) == -1;
                    });
                    // for (var i = get_email_spillover_array.length - 1; i >= 0; i--) {
                    //     for (var j = 0; j < get_email_array.length; j++) {
                    //       if (get_email_spillover_array[i] === get_email_array[j]) {
                    //         get_email_spillover_array.splice(i, 1);
                    //         }
                    //       }
                    //     }
                    record.setFieldValue('custevent_acs_custom_email', get_email_spillover_array.join()); // use all emails if multiple contacts

                    //----------------------------------//
                }


            }



            record.setFieldText('status', subcasestatus);
            record.setFieldText('priority', priority);

            record.setFieldValue('category', casetype);
            record.setFieldValue('custevent_cologix_sub_case_type', subcasetype);

            record.setFieldValue('custevent_cologix_case_sched_followup', followup);
            record.setFieldValue('custevent_cologix_sched_start_time', starttime);

            record.setFieldValue('custevent_clgx_parent_id', caseid);
            record.setFieldValue('custevent_cologix_equipment', equipment);
            record.setFieldValue('origin', 6);

            var idRec = nlapiSubmitRecord(record, false, true);

            numRec = nlapiLookupField('supportcase', idRec, 'casenumber');

            var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
            emailAdminBody += '<tr><td>' + facilityname + '</td><td><a href="https://1337135.app.netsuite.com/app/crm/support/supportcase.nl?id=' + idRec + '">' + numRec + '</a></td><td>' + startExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + execMinutes + '</td><td>' + usageConsumtion + '</td></tr>';
            emailUserBody += '<tr><td>' + facilityname + '</td><td><a href="https://1337135.app.netsuite.com/app/crm/support/supportcase.nl?id=' + idRec + '">' + numRec + '</a></td></tr>';

            // //-----------------------------------------------------------------
            // var new_array_by_10_length = new_array_by_10.length;
            // for (var count = 1; count < new_array_by_10_length; count++) {
            //     //nlapiSendEmail(userid, userid, "TEST EMAIL-----SS CASE" + subject, message + new_array_by_10[count], null, null, null, null, true)
            // }
            // //-----------------------------------------------------------------
        }
        nlapiLogExecution('DEBUG', 'finishloop', caseid);
        nlapiSubmitField('supportcase', caseid, 'custevent_clgx_locked_case', 'F');

        // inform sale reps ----------------------------------------------------------------------------------------------------------------------------------------

        // Search all sales reps of all found customers
        var searchColumns = new Array();
        searchColumns.push(new nlobjSearchColumn('salesrep', null, 'GROUP'));
        var searchFilter = new Array();
        if (arrCustomers != null) {
            searchFilter.push(new nlobjSearchFilter('internalid', null, 'anyof', arrCustomers));
        }
        else {
            searchFilter.push(new nlobjSearchFilter('internalid', null, 'anyof', 0));
        }
        searchFilter.push(new nlobjSearchFilter('salesrep', null, 'noneof', '@NONE@'));
        var searchSaleReps = nlapiSearchRecord('customer', null, searchFilter, searchColumns);

        var emailRepSubject = 'Incident / maintenance case ' + casenumber + ' - created by ' + username;

        var emailJayBody = '';
        emailJayBody += '<br>Incident / maintenance case <a href="https://1337135.app.netsuite.com/app/crm/support/supportcase.nl?id=' + caseid + '">' + casenumber + '</a> - created by ' + username + '<br><br>';
        emailJayBody += 'List of affected customers : <br><br>';

        for (var i = 0; searchSaleReps != null && i < searchSaleReps.length; i++) {
            var searchSaleRep = searchSaleReps[i];
            var salerepid = searchSaleRep.getValue('salesrep', null, 'GROUP');
            var salerepname = nlapiLookupField('employee', salerepid, 'entityid');

            var emailRepBody = '';
            emailRepBody += '<br>Incident / maintenance case <a href="https://1337135.app.netsuite.com/app/crm/support/supportcase.nl?id=' + caseid + '">' + casenumber + '</a> - created by ' + username + '<br><br>';
            emailRepBody += 'List of affected customers : <br><br>';

            // Search all customers of the sale rep
            var searchColumns = new Array();
            searchColumns.push(new nlobjSearchColumn('entityid', null, 'GROUP'));
            var searchFilter = new Array();
            searchFilter.push(new nlobjSearchFilter('internalid', null, 'anyof', arrCustomers));
            searchFilter.push(new nlobjSearchFilter('salesrep', null, 'anyof', salerepid));
            var searchRepCustomers = nlapiSearchRecord('customer', null, searchFilter, searchColumns);

            for (var j = 0; searchRepCustomers != null && j < searchRepCustomers.length; j++) {
                var searchRepCustomer = searchRepCustomers[j];
                var repcustname = searchRepCustomer.getValue('entityid', null, 'GROUP');
                emailRepBody += repcustname + '<br>';
                emailJayBody += repcustname + '<br>';
            }
            emailRepBody += '<br><br>';
            nlapiSendEmail(userid, salerepid, emailRepSubject, emailRepBody, null, null, null, null, true);
            nlapiLogExecution('DEBUG', 'salerepid',salerepid);
        }
        emailJayBody += '<br><br>';
        if (searchSaleReps != null) {
            //nlapiSendEmail(userid, 115655, emailRepSubject, emailJayBody, null, null, null, null, true);
            nlapiSendEmail(userid, userid, emailRepSubject, emailJayBody, null, null, null, null, true);
        }

        emailAdminBody += '</table>';
        emailUserBody += '</table>';

        var endScript = moment();
        emailAdminBody += '</table><br>';
        emailAdminBody += 'End : ' + endScript.format('M/D/YYYY h:mm:ss a') + '<br>';
        emailAdminBody += 'Total execution minutes : ' + (endScript.diff(startScript) / 60000).toFixed(1) + '<br>';
        var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
        emailAdminBody += 'Total usage : ' + usageConsumtion;
        emailAdminBody += '<br><br>';

        //emailUserBody += '</table><br>';
        emailUserBody += 'End : ' + endScript.format('M/D/YYYY h:mm:ss a') + '<br>';
        emailUserBody += 'Total execution minutes : ' + (endScript.diff(startScript) / 60000).toFixed(1);
        emailUserBody += '<br><br>';

        /*nlapiSendEmail(userid,71418,emailAdminSubject,emailAdminBody,null,null,null,null,true);
        nlapiSendEmail(userid,206211,emailAdminSubject,emailAdminBody,null,null,null,null,true);*/

        clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_case_admin", emailAdminSubject, emailAdminBody);

        nlapiSendEmail(userid, userid, emailUserSubject, emailUserBody, null, null, null, null, true);

        nlapiLogExecution('DEBUG', 'Finished Execution', '|-------------------------- Finished Scheduled Script --------------------------|');
    }
    catch (error) {

        nlapiSubmitField('supportcase', caseid, 'custevent_clgx_locked_case', 'F');

        clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_case_admin", "Error processing sub cases for case id: " + caseid + " - PLEASE CONTACT SUPPORT!", "PLEASE CONTACT SUPPORT!\n\n");

        //nlapiSendEmail(userid,71418,'Error processing sub cases for case id ' + caseid + ' - PLEASE CONTACT SUPPORT!', 'PLEASE CONTACT SUPPORT!\n\n',null,null,null,null,true);
        nlapiSendEmail(userid, userid, 'Error processing sub cases for case id ' + caseid + ' - PLEASE CONTACT SUPPORT!', 'PLEASE CONTACT SUPPORT!\n\n', null, null, null, null, true);

        if (error.getDetails != undefined) {
            nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else {
            nlapiLogExecution('ERROR', 'Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

//check if value is in the array
function inArray(val, arr) {
    var bIsValueFound = false;
    for (var i = 0; i < arr.length; i++) {
        if (val == arr[i]) {
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}


function get_fam_children(arrFAMs) {
    var arrChildren = new Array();
    var searchColumns = new Array();
    searchColumns.push(new nlobjSearchColumn('internalid', null, null));
    var searchFilter = new Array();
    searchFilter.push(new nlobjSearchFilter('custrecord_assetparent', null, 'anyof', arrFAMs));
    var searchChildren = nlapiSearchRecord('customrecord_ncfar_asset', null, searchFilter, searchColumns);
    for (var i = 0; searchChildren != null && i < searchChildren.length; i++) {
        arrChildren.push(parseInt(searchChildren[i].getValue('internalid', null, null)));
    }
    return arrChildren;
}