nlapiLogExecution("audit","FLOStart",new Date().getTime());
/**
 * Created by catalinataran on 2014-10-03.
 */


//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Create_Boss_Media_Equipment_Audit.js
//	Script Name:	CLGX_SS_Create_Boss_Media_Equipment_Audit
//	Script Id:		customscript_clgx_create_boss_media_equipment_audit
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Case
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//-------------------------------------------------------------------------------------------------

function scheduled_create_boss_media_equipment_a() {
    try {
        nlapiLogExecution('DEBUG','Scheduled Script - Create_Audit_Cases','|--------------------STARTED---------------------|');
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.1 - 2/8/2012
// Details:	Creates Boss Media Equipment Audit case every Friday morning.
//-----------------------------------------------------------------------------------------------------------------

        var date = new Date();
        var month = parseInt(date.getMonth()) + 1;
        var year = date.getFullYear();
        var record = nlapiCreateRecord('supportcase');
        record.setFieldValue('title', 'Boss Media Equipment Audit');
        record.setFieldValue('company',1617);
        record.setFieldValue('custevent_cologix_facility', 8);//TOR1
        record.setFieldValue('category', 1); //Remote Hands
        record.setFieldValue('custevent_cologix_sub_case_type', 6); //Scheduled
        record.setFieldValue('assigned', 218515);
        // test sandbox   //
        // record.setFieldValue('assigned', 2797);
        record.setFieldValue('email', 'cristian.brolin@gtech.com');
        record.setFieldValue('contact',5853);
        record.setFieldValue('custevent_clgx_co_sec_id',477550);
        record.setFieldValue('phone','4670.1911.293');
        record.setFieldValue('status',2);
        record.setFieldValue('priority',2);
        //record.setFieldValue('startdate', nlapiDateToString(date));
        var idRec = nlapiSubmitRecord(record, false, true);
        var emailSubject = 'Boss Media equipment audit,' +  date.getDate() +"/"+month+ '/' + year;
        var emailBody = 'Boss Media equipment audit case has been created: '+ idRec+'.\n\n\n';
        
        clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_case_admin", emailSubject, emailBody);
        
        /*nlapiSendEmail(432742,71418,emailSubject,emailBody,null,null,null,null,true);//send email to Dan
        nlapiSendEmail(432742,206211,emailSubject,emailBody,null,null,null,null,true);//send email to Catalina
        nlapiSendEmail(432742,1349020,emailSubject,emailBody,null,null,null,null,true);*///send email to Alex
        nlapiLogExecution('DEBUG','Friday case for Boss Media','Boss Media equipment audit - '  +date.getDate() +"/"+month+ '/' + year);


//---------- End Section 1 ------------------------------------------------------------------------------------------------


        nlapiLogExecution('DEBUG','Scheduled Script - Create_Audit_Cases','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}




