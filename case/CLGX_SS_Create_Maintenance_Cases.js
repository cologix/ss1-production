nlapiLogExecution("audit","FLOStart",new Date().getTime());
/**
 * Created by catalinataran on 2015-05-20.
 */
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Create_Maintenance_Cases.js
//	Script Name:	CLGX_SS_Create_Maintenance_Cases
//	Script Id:		customscript_clgx_create_maintenance_cases
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Case
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		05/20/2015
//-------------------------------------------------------------------------------------------------

function scheduled_create_maintenance_cases () {
    try {
        nlapiLogExecution('DEBUG','Scheduled Script - Create_Audit_Cases','|--------------------STARTED---------------------|');
        //------------- Begin Section 1 -----------------------------------------------------------------------------------
        // Version:	1.1 - 2/8/2012
        // Details:	Creates 4 types of audit cases on the 1st of every month for each facility
        //-----------------------------------------------------------------------------------------------------------------

        var currentTime = new Date();
        var currentTimeF = new Date();
        currentTime.setDate(currentTime.getDate()+42);
        var month=currentTime.getMonth()+parseInt(1);
        var day=currentTime.getDate();
        var year=currentTime.getFullYear();
        var monthF=currentTimeF.getMonth()+parseInt(1);
        var dayF=currentTimeF.getDate();
        var yearF=currentTimeF.getFullYear();
        var from=monthF+'/'+dayF+'/'+yearF;
        var datetoFind=month+'/'+day+'/'+year;
        var arrColumns = new Array();
        var arrFilters = new Array();
        arrFilters[0] = new nlobjSearchFilter('custrecord_assetmaintnextdate', null, 'within',from,datetoFind);
        var searchfams = nlapiSearchRecord('customrecord_ncfar_asset','customsearch_clgx_fam_mt_2', arrFilters, arrColumns);
        if(searchfams!=null){
            for ( var i = 0; searchfams != null && i < searchfams.length; i++ ) { // loop fams
                var searchfam = searchfams[i];
                var columns = searchfam.getAllColumns();
                var id=searchfam.getValue(columns[0]);
                var name=searchfam.getValue(columns[1]);
                var fac=searchfam.getValue(columns[2]);
                var title=searchfam.getText(columns[2]);
                var inspectiondate=searchfam.getValue(columns[3]);
                var repair=searchfam.getText(columns[4]);
                var checkCases=searchfam.getValue(columns[5]);
                var returnCompany=clgx_return_company (fac);
                var returnAssignment=clgx_return_assigned(fac);
                var inspectiondateMom=moment(inspectiondate);
                //var arrColumnsC = new Array();
                // var arrFiltersC = new Array();
                // arrFiltersC[0] = new nlobjSearchFilter('custevent_cologix_equipment', null, 'anyof',id);
                // var searchCases = nlapiSearchRecord('supportcase','customsearch_clgx_ss_crea_maintenance_ca', arrFiltersC, arrColumnsC);
                if((fac!=null)&&(fac!=''))
                {
                    if(checkCases==0)
                    {
                        if(returnCompany!=0)
                        {

                            if((title.indexOf("&") > -1))
                            {
                                title=title.replace("&", " and ");
                            }
                            var arrFiltersM = new Array();
                            var arrColumnsM = new Array();

                            arrFiltersM.push(new nlobjSearchFilter("custrecord_clgx_mor_facility",null,'is',title));
                            arrFiltersM.push(new nlobjSearchFilter("custrecord_clgx_mor_start_date",null, 'within',from,datetoFind));
                            arrFiltersM.push(new nlobjSearchFilter("custrecord_clgx_mor_end_date",null, 'notwithin',from,datetoFind));
                            var newFilterExpression =[ [ 'custrecord_clgx_mor_facility', 'anyof', fac ],
                                'and',
                                [ [ 'custrecord_clgx_mor_start_date', 'within', from,datetoFind ] ,'or',[ 'custrecord_clgx_mor_end_date', 'within', from,datetoFind ] ]];

                            var mySearch = nlapiSearchRecord('customrecord_clgx_moratorium','customsearch_clgx_moratorium_ops_search',newFilterExpression,arrColumnsM);


                            for ( var k = 0; mySearch != null && k < mySearch.length; k++ ) {
                                var searchResult = mySearch[k];
                                var columns1 =  searchResult.getAllColumns();

                                var sdate = searchResult.getValue(columns1[0]);
                                var edate = searchResult.getValue(columns1[1]);
                                var sdateMom=moment(sdate);
                                var edateMom=moment(edate);
                                if(inspectiondateMom.isBetween(sdateMom, edateMom))
                                {
                                    inspectiondate= moment(edate).add(1, 'day').format('L');
                                }


                            }



                            var record = nlapiCreateRecord('supportcase');
                            //  record.setFieldValue('title', name+ ' Maintenance ' + inspectiondate);
                            record.setFieldValue('title', name);
                            record.setFieldValue('company', returnCompany);
                            record.setFieldValue('custevent_cologix_facility', fac);
                            record.setFieldValue('category', 3); // Case is only type Maintenance
                            //   record.setFieldValue('profile', 5);
                            //  record.setFieldValue('subsidiary', 5);
                            //   if(returnAssignment!=0)
                            // {
                            record.setFieldValue('assigned', clgx_return_contact1(fac,repair));
                            // }
                            if((repair!=null)&&(repair!=''))
                            {
                                record.setFieldText('custevent_cologix_sub_case_type', repair);
                            }
                            record.setFieldValue('contact', clgx_return_contact(fac,repair));
                            // record.setFieldValue('custevent_clgx_audit_mops_instructions', stCaseInstructions);
                            record.setFieldValue('custevent_cologix_case_sched_followup', inspectiondate);
                            record.setFieldValue('status', 1);
                            record.setFieldValue('priority', 2);
                            record.setFieldValue('custevent_cologix_equipment', id);
                            // record.setFieldValue('startdate', nlapiDateToString(date));
                            var idRec = nlapiSubmitRecord(record, false, true);
                        }
                    }
                }
            }
        }
        nlapiLogExecution('DEBUG','Monthly Report','Monthly Report - Audit Cases - ' +  year + '.' + month);


        //---------- End Section 1 ------------------------------------------------------------------------------------------------


        nlapiLogExecution('DEBUG','Scheduled Script - Create_Audit_Cases','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
function clgx_return_company (locationid){

    var company = 0;
    var assigned=0;
    switch(parseInt(locationid)) {
        case 3:
            company = 2790; // DAL
            assigned=317219; // Dallas
            break;
        case 18:
            company = 2790; // DAL
            assigned=317219; // Dallas
            break;
        case 17:
            company = 2790;
            assigned=317219; // Minneapolis
            break;
        case 25:
            company = 2790;//Minneapolis3
            break;
        case 8:
            company = 2789; // TOR
            assigned=317219; // TOR1
            break;
        case 15:
            company = 2789; // TOR
            assigned=317219; // TOR2
            break;
        case 21:
            company = 2790;
            assigned=317219; //JAX1
            break;
        case 27:
            company = 2790;//JAX2
            break;
        case 28:
            company = 2790;//LAK1
            break;
        case 14:
            company = 2789; // TOR
            assigned=317219; //VAN1
            break;
        case 20:
            company = 2789; // TOR
            assigned=317219; //VAN2
            break;
        case 2:
            //MTL1
            company = 2789; // MTL
            assigned=317218;
            break;
        case 5:
            //MTL2
            company = 2789; // MTL
            assigned=317218;
            break;
        case 4:
            //MTL3
            company = 2789; // MTL
            assigned=317218;
            break;
        case 9:
            //MTL4
            company = 2789; // MTL
            assigned=317218;
            break;
        case 6:
            //MTL5
            company = 2789; // MTL
            assigned=317218;
            break;
        case 7:
            //MTL6
            company = 2789; // MTL
            assigned=317218;
            break;
        case 19:
            //MTL7
            company = 2789; // MTL
            assigned=317218;
            break;
        case 24:
            //COL1&2
            company = 2790;
            assigned=317219;
            break;
        case 29:
            //COL1&2
            company = 2790;
            assigned=317219;
            break;
        //NNJs
        case 33:
            company = 2790;
            break;
        case 34:
            company = 2790;
            break;
        case 35:
            company = 2790;
            break;
        case 36:
            company = 2790;
            break;
        default:
            company = 0;
            assigned=0;
    }
    return company;
}

function clgx_return_contact (locationid,type){

    var contact = 0;
    if(locationid==2||locationid==5||locationid==4||locationid==9||locationid==6||locationid==7||locationid==19)
    {

        if(type=="HVACs" || type=="Chillers" || type=="Condensers/Dry-cooler/Towers"|| type=="Pumps/motors/drives"|| type=="All other AHU" || type=="Compressors & Air Systems"||type=="Fire Systems"
            || type=="Extinguisher" ||type=="Electrical Distribution")
        {
            contact = 72660;
        }
        if(type=="UPS" || type=="PDU" || type=="STS"|| type=="UPS Battery"|| type=="DC Plant Battery" || type=="Generator"||type=="ATS"
            || type=="Electrical Distribution")
        {
            contact = 2497;
        }

    }
    if(locationid==8)
    {
        contact = 2418; // TOR1--Steve Ducroix
    }
    if(locationid==15)
    {
        contact = 2418; // TOR2--Steve Ducroix
    }

    if(locationid==14||locationid==20)
    {
        contact = 82114; // VAN--Anand Mohammed
    }
    if(locationid==3||locationid==18)
    {
        contact = 18405; // DAL--David Ramirez
    }

    if(locationid==24)
    {
        //COL1&2
        contact = 277775;//Kevin Barber
    }
    if(locationid==29)
    {
        //COL1&2
        contact = 277775;//Kevin Barber
    }
    if(locationid==21)
    {
        //contact = 303111; // JAX1--Roy Stoutenborough
    	contact = 1976210; //JAX1--David Staier
    }
    if(locationid==27)
    {
        contact = 375070; // JAX2--Twann Davis
        //contact = 1976210; // JAX2--David Staier
    }
    if(locationid==28)
    {
        contact = 1006667;//LAK1--Brennen Bogle

    }
    if(locationid==17||locationid==25)
    {
        contact = 347047;//Minn-Brian Zitzow
    }
    //NNJs
    if(locationid==34||locationid==36 || locationid==33|| locationid==35)
    {
        if(type=="HVACs" || type=="Chillers" || type=="Condensers/Dry-cooler/Towers"|| type=="Pumps/motors/drives"|| type=="All other AHU" || type=="Compressors & Air Systems" ||type=="Electrical Distribution")
        {
            contact = 1143631;
        }
        if(type=="UPS" || type=="PDU" || type=="STS"|| type=="UPS Battery"|| type=="DC Plant Battery" || type=="ATS"
            || type=="Electrical Distribution")
        {
            contact = 1662603;
        }
        
        if(type=="Extinguisher" || type=="Fire Systems" || type=="Generator") {
        	contact = 1935982;
        }
    }
    /*   if(locationid==33)
       {
           contact = 600333;//Ben Cooper
       }
       if(locationid==35)
       {
           contact = 600333;//Ben Cooper
       }
   */
    return contact;
}

function clgx_return_contact1 (locationid,type){

    var contact = 0;
    //  Louis Paquette for HVAC and fire assets
    //  Pierre Chapdelaine for Power Chain assets.  2497 -8286

    /*   HVACs – HVAC and fire assets

     Chillers – HVAC and fire assets

     Condensers/Dry-cooler/Towers – HVAC and fire assets

     Pumps/motors/drives – HVAC and fire assets

     All other AHU – HVAC and fire assets

     UPS – Power Chain assets

     PDU – Power Chain assets

     STS – Power Chain assets

     UPS Battery – Power Chain assets

     DC Plant Battery – Power Chain assets

     Generator – Power Chain assets

     ATS – Power Chain assets

     Compressors & Air Systems – HVAC and fire assets

     Fire Systems – HVAC and fire assets

     Extinguisher – HVAC and fire assets

     Electrical Distribution – Power Chain assets*/
    //MTL1
    if(locationid==2||locationid==5||locationid==4||locationid==9||locationid==6||locationid==7||locationid==19)
    {

        if(type=="HVACs" || type=="Chillers" || type=="Condensers/Dry-cooler/Towers"|| type=="Pumps/motors/drives"|| type=="All other AHU" || type=="Compressors & Air Systems"||type=="Fire Systems"
            || type=="Extinguisher" ||type=="Electrical Distribution")
        {
            contact = 72660;
        }
        if(type=="UPS" || type=="PDU" || type=="STS"|| type=="UPS Battery"|| type=="DC Plant Battery" || type=="Generator"||type=="ATS"
            || type=="Electrical Distribution")
        {
            contact = 2497;
        }

    }
    if(locationid==8)
    {
        contact = 2418; // TOR1--Steve Ducroix
    }
    if(locationid==15)
    {
        contact = 2418; // TOR2--Steve Ducroix
    }

    if(locationid==14||locationid==20)
    {
        contact = 82114; // VAN--Anand Mohammed
    }
    if(locationid==3||locationid==18)
    {
        contact = 18405; // DAL--David Ramirez
    }

    //COL1&2 && COL3
    if(locationid==24  || locationid==29)
    {
        nlapiLogExecution("DEBUG", "type", type);

        if(type=="HVACs" ||   type=="Chillers" || type=="Condensers/Dry-cooler/Towers"|| type=="Pumps/motors/drives"|| type=="All other AHU" || type=="Compressors & Air Systems"||type=="Fire Systems"
            || type=="Extinguisher" ||type=="Electrical Distribution")
        {
            contact = 277775;//Kevin Barber
        }
        if(type=="UPS" || type=="Busway" || type=="PDU" || type=="STS"|| type=="UPS Battery"|| type=="DC Plant Battery" || type=="Generator"||type=="ATS"
            || type=="Electrical Distribution" || type == "DC Plant")
        {
            contact = 1981392; //David Mullen
        }
    }
    /*if(locationid==24  || locationid==29)
    {
        //COL1&2
        contact = 277775;//Kevin Barber
    }*/
    if(locationid==21)
    {
        //contact = 303111; // JAX1--Roy Stoutenborough
    	contact = 1976210; //JAX1--David Staier
    }
    if(locationid==27)
    {
        contact = 375070; // JAX2--Twann Davis
        //contact = 1976210; // JAX2--David Staier
    }
    if(locationid==28)
    {
        contact = 1006667;//LAK1--Brennen Bogle
    }
    if(locationid==17||locationid==25)
    {
        contact = 347047;//Minn-Brian Zitzow
    }
    //NNJs
    //NNJs
    if(locationid==34||locationid==36 || locationid==33|| locationid==35)
    {
        if(type=="HVACs" || type=="Chillers" || type=="Condensers/Dry-cooler/Towers"|| type=="Pumps/motors/drives"|| type=="All other AHU" || type=="Compressors & Air Systems" || type=="Electrical Distribution")
        {
            contact = 1143631;
        }
        if(type=="UPS" || type=="PDU" || type=="STS"|| type=="UPS Battery"|| type=="DC Plant Battery" ||type=="ATS" || type=="Electrical Distribution")
        {
            contact = 1662603;
        }
        
        if(type=="Extinguisher" || type=="Fire Systems" || type=="Generator") {
        	contact = 1935982;
        }
    }
    return contact;
}
function clgx_return_assigned (locationid){


    var assigned=0;
    switch(parseInt(locationid)) {
        case 3:

            assigned=317219; // Dallas
            break;
        case 18:

            assigned=317219; // Dallas
            break;
        case 17:

            assigned=317219; // Minneapolis
            break;
        case 25:
            assigned=317219;//Minneapolis3
            break;
        case 8:

            assigned=317219; // TOR1
            break;
        case 15:

            assigned=317219; // TOR2
            break;
        case 21:

            assigned=317219; //JAX1
            break;
        case 27:
            assigned=317219;//JAX2
            break;
        case 28:
            assigned=317219;//LAK1
            break;
        case 14:

            assigned=317219; //VAN1
            break;
        case 20:

            assigned=317219; //VAN2
            break;
        case 2:
            //MTL1

            assigned=317218;
            break;
        case 5:
            //MTL2

            assigned=317218;
            break;
        case 4:
            //MTL3

            assigned=317218;
            break;
        case 9:
            //MTL4

            assigned=317218;
            break;
        case 6:
            //MTL5

            assigned=317218;
            break;
        case 7:
            //MTL6

            assigned=317218;
            break;
        case 19:
            //MTL7

            assigned=317218;
            break;
        case 24:
            //COL1&2

            assigned=317219;
            break;
        case 29:
            //COL1&2

            assigned=317219;
            break;
        //NNJs
        case 33:
            //NNJ2

            assigned=317219;
            break;
        case 34:
            //NNJ1

            assigned=317219;
            break;
        case 35:
            //NNJ3

            assigned=317219;
            break;
        case 36:
            //NNJ4

            assigned=317219;
            break;
        default:

            assigned=0;
    }
    return assigned;
}