nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Create_Aizan_Scan_Report_Cases.js
//	Script Name:	CLGX_SS_Create_Aizan_Scan_Report_Cases
//	Script Id:		customscript_clgx_create_aizan_cases
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Case
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Released:		04/24/2014
//-------------------------------------------------------------------------------------------------

function scheduled_create_aizan_scan_report_cases () {
    try {
        nlapiLogExecution('DEBUG','Scheduled Script - Create_Audit_Cases','|--------------------STARTED---------------------|');
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.1 - 2/8/2012
// Details:	Creates 4 types of audit cases on the 1st of every month for each facility
//-----------------------------------------------------------------------------------------------------------------

        var date = new Date();
        var month = parseInt(date.getMonth()) + 1;
        var year = date.getFullYear();
        var record = nlapiCreateRecord('supportcase');
        record.setFieldValue('title', 'Aizan Monthly report every last Friday of the month');
        record.setFieldValue('company',3154);
        record.setFieldValue('custevent_cologix_facility', 8);
        record.setFieldValue('category', 1); //Remote Hands
        record.setFieldValue('custevent_cologix_sub_case_type', 6); //Scheduled
        record.setFieldValue('assigned', 218515);
       // test sandbox   //
       // record.setFieldValue('assigned', 2797);
        record.setFieldValue('email', 'apros@aizan.com');
        record.setFieldValue('contact',11438);
        record.setFieldValue('startdate', nlapiDateToString(date));
        var idRec = nlapiSubmitRecord(record, false, true);
        var emailSubject = 'Aizan Monthly Key Scan report ' +  year + '.' + month;
        var emailBody = 'Aizan Monthly Key Scan report case has been created.\n\n\n';
        
        /*nlapiSendEmail(432742,71418,emailSubject,emailBody,null,null,null,null,true);
        nlapiSendEmail(432742,206211,emailSubject,emailBody,null,null,null,null,true);
        nlapiSendEmail(432742, 1349020,emailSubject,emailBody,null,null,null,null,true);*/
        
        clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_case_admin", emailSubject, emailBody);

        nlapiLogExecution('DEBUG','Monthly Report','Monthly Report - Audit Cases - ' +  year + '.' + month);


//---------- End Section 1 ------------------------------------------------------------------------------------------------


        nlapiLogExecution('DEBUG','Scheduled Script - Create_Audit_Cases','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}




