nlapiLogExecution("audit","FLOStart",new Date().getTime());
/**
 * Created by catalinataran on 2014-10-14.
 */

    //-------------------------------------------------------------------------------------------------
    //	Script File:	CLGX_SS_Invoices_Cases_Remote_Hands.js
    //	Script Name:	CLGX_SS_Invoices_Cases_Remote_Hands
    //	Script Id:	     customscript_clgx_ss_invoices_cases_rh
    //	Script Runs:	On Server
    //	Script Type:	Scheduled Script
    //	Deployments:	Case
    //	@authors:	Catalina Taran - catalina.taran@cologix.com
    //	Created:	10/14/2014
    //-------------------------------------------------------------------------------------------------
function scheduled_create_invoice_fromclosedcases(){
    try{
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 2/2/2011
// Details:	Consolidate invoices.
//-------------------------------------------------------------------------------------------------
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');
        var startScript = moment();
        var context = nlapiGetContext();
        var initialTime = moment();
        var arrDates  = getDateRange(-1);
        var startDate = arrDates[0];
        var endDate  = arrDates[1];
        var arrDates1  = getDateRange(1);
        var startDate1 = arrDates1[0];
        var endDate1  = arrDates1[1];
        var date=new Date();
        var searchColumn = new Array();
        var searchFilter=new Array();
        searchColumn.push(new nlobjSearchColumn('custcol_clgx_rh_invoice_item_case',null,'GROUP'));
        searchFilter.push(new nlobjSearchFilter('trandate',null,'within',startDate1,endDate1));
        searchFilter.push(new nlobjSearchFilter('custcol_clgx_rh_invoice_item_case',null,'noneof','@NONE@'));
        var searchCases = nlapiSearchRecord('invoice',null,searchFilter,searchColumn);
        var arrCases = new Array();
        for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
            var caseid = searchCases[i].getValue('custcol_clgx_rh_invoice_item_case',null,'GROUP');
            if(!in_array (caseid, arrCases)){
                arrCases.push(caseid);
            }
        }

        var arrColumns = new Array();
        var arrFilters = new Array();
        if(arrCases.length>0)
        {
            arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof", arrCases));
        }




        var emailBody='';
        // arrFilters.push(new nlobjSearchFilter("custrecord_clgx_rh_from_date",null,"on",startDate));
        //  arrFilters.push(new nlobjSearchFilter("custrecord_clgx_rh_to_date",null,"on",endDate));

        var searchRemote = nlapiSearchRecord('supportcase',   'customsearch_clgx_search_caserhinv', arrFilters, arrColumns);
        if(searchRemote!=null)
        {
            for ( var i = 0; searchRemote != null && i < searchRemote.length; i++ ){
                var startExecTime = moment();
                var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);

                if ( (context.getRemainingUsage() <= 300 || totalMinutes > 50) && (i+1) < searchRemote.length ){
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    if ( status == 'QUEUED' ) {
                        nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                        break;
                    }
                }
                var searchRem = searchRemote[i];
                var columns = searchRem.getAllColumns();
                var casenumber=searchRem.getValue(columns[0]);
                var email=searchRem.getValue(columns[1]);
                var customer=searchRem.getValue(columns[2]);
                var location=searchRem.getValue(columns[3]);
                var billing=searchRem.getValue(columns[4]);
                var subsidiary=searchRem.getValue(columns[5]);
                var internalID=searchRem.getValue(columns[6]);
                var busRate=searchRem.getValue(columns[7]);
                var afterRate=searchRem.getValue(columns[8]);
                if(busRate=='')
                {
                    busRate=0;
                }
                if(afterRate=='')
                {
                    afterRate=0;
                }
                var newInvoice=nlapiCreateRecord('invoice');
                newInvoice.setFieldValue('location', location);
                var consolidateLocation=clgx_return_consolidate_location(location);
                newInvoice.setFieldValue('custbody_clgx_consolidate_locations', clgx_return_consolidate_location(location));
                newInvoice.setFieldValue('subsidiary', subsidiary);
                var ctr=1;
                //get the business hours
                var arrColumnsB = new Array();
                var arrFiltersB = new Array();
                var hoursB=0;
                arrFiltersB.push(new nlobjSearchFilter("internalid",null,"anyof",internalID));
                var searchRemoteB = nlapiSearchRecord('supportcase',   'customsearch_clgx_search_caserhib', arrFiltersB, arrColumnsB);
                for ( var j = 0; searchRemoteB != null && j < searchRemoteB.length; j++ ) {
                    var searchRemB = searchRemoteB[j];
                    var columnsB = searchRemB.getAllColumns();
                    hoursB=searchRemB.getValue(columnsB[0]);

                }

                var busH=hoursB;
                if(busH>0)
                {
                    var hoursDecimal=busH;
                    hoursDecimal=parseFloat(hoursDecimal);
                    hoursDecimal=hoursDecimal.toFixed(2);
                    var splitBusinessTimeinHoursperCase=hoursDecimal.split('.');
                    var splitBusinessTimeinHoursperCase0=hoursDecimal.split('.');
                    if(splitBusinessTimeinHoursperCase[1]==0)
                    {
                        splitBusinessTimeinHoursperCase[1]=0;
                        splitBusinessTimeinHoursperCase0[1]=0;
                    }
                    if((splitBusinessTimeinHoursperCase[1]>0)&&(splitBusinessTimeinHoursperCase[1]<=25))
                    {
                        splitBusinessTimeinHoursperCase[1]=15;
                        splitBusinessTimeinHoursperCase0[1]=25;
                    }
                    if((splitBusinessTimeinHoursperCase[1]>25)&&(splitBusinessTimeinHoursperCase[1]<=50))
                    {
                        splitBusinessTimeinHoursperCase[1]=30;
                        splitBusinessTimeinHoursperCase0[1]=50;
                    }
                    if((splitBusinessTimeinHoursperCase[1]>50)&&(splitBusinessTimeinHoursperCase[1]<=75))
                    {
                        splitBusinessTimeinHoursperCase[1]=45;
                        splitBusinessTimeinHoursperCase0[1]=75;
                    }
                    if((splitBusinessTimeinHoursperCase[1]>75)&&(splitBusinessTimeinHoursperCase[1]<99))
                    {
                        splitBusinessTimeinHoursperCase[1]=0;
                        splitBusinessTimeinHoursperCase0[1]=0;
                        splitBusinessTimeinHoursperCase[0]=parseInt(splitBusinessTimeinHoursperCase[0])+parseInt(1);
                        splitBusinessTimeinHoursperCase0[0]=parseInt(splitBusinessTimeinHoursperCase0[0])+parseInt(1);

                    }
                    var timeBus= splitBusinessTimeinHoursperCase[0]+'.'+ splitBusinessTimeinHoursperCase[1];
                    timeBus=parseFloat(timeBus);
                    timeBus=timeBus.toFixed(2);
                    var timeBus0= splitBusinessTimeinHoursperCase0[0]+'.'+ splitBusinessTimeinHoursperCase0[1];
                    timeBus0=parseFloat(timeBus0);
                    timeBus0=timeBus0.toFixed(2);
                    newInvoice.setLineItemValue('item', 'item', ctr, 544);
                    newInvoice.setLineItemValue('item', 'quantity', ctr, timeBus0);
                    newInvoice.setLineItemValue('item', 'rate',ctr, busRate);
                    newInvoice.setLineItemValue('item', 'location',ctr, location);
                    newInvoice.setLineItemValue('item', 'amount',ctr, timeBus0*busRate);
                    //custentity_clgx_tax_item_col
                    //custentity_clgx_tax_item_dal
                    //custentity_clgx_tax_item_jax
                    //custentity_clgx_tax_item_min
                    //Canada
                    //custentity_clgx_tax_item_mtl
                    ////custentity_clgx_tax_item_tor
                    ////custentity_clgx_tax_item_van
                    var returntax=clgx_return_tax(location,customer);
                    if(returntax!='')
                    {
                        newInvoice.setLineItemValue('item', 'taxcode',ctr, returntax[0]);
                        newInvoice.setLineItemValue('item', 'taxcode_display',ctr, returntax[1]);

                    }
                    newInvoice.setLineItemValue('item', 'description',ctr, 'Case\r\n #'+casenumber+ '\r\nRequested by '+email);
                    newInvoice.setLineItemValue('item', 'custcol_clgx_rh_invoice_item_case',ctr, internalID);

                    ctr++;
                }
                //get after hours
                var arrColumnsA = new Array();
                var arrFiltersA = new Array();
                var hoursA=0;
                arrFiltersA.push(new nlobjSearchFilter("internalid",null,"anyof",internalID));
                var searchRemoteA = nlapiSearchRecord('supportcase',   'customsearch_clgx_search_caserhia', arrFiltersA, arrColumnsA);
                for ( var t = 0; searchRemoteA != null && t < searchRemoteA.length; t++ ) {
                    var searchRemA = searchRemoteA[t];
                    var columnsA = searchRemA.getAllColumns();
                    hoursA=searchRemA.getValue(columnsA[0]);
                }
                var afterH=hoursA;
                if(afterH>0)
                {
                    var hoursDecimal=afterH;
                    hoursDecimal=parseFloat(hoursDecimal);
                    hoursDecimal=hoursDecimal.toFixed(2);
                    var splitAfterTimeinHoursperCase=hoursDecimal.split('.');
                    var splitAfterTimeinHoursperCase0=hoursDecimal.split('.');
                    if(splitAfterTimeinHoursperCase[0]==0)
                    {
                        splitAfterTimeinHoursperCase[0]=1;
                        splitAfterTimeinHoursperCase[1]=0;
                        splitAfterTimeinHoursperCase0[0]=1;
                        splitAfterTimeinHoursperCase0[1]=0;
                    }
                    if((splitAfterTimeinHoursperCase[1]==0)&&(splitAfterTimeinHoursperCase[0]>0))
                    {

                        splitAfterTimeinHoursperCase[1]=0;
                        splitAfterTimeinHoursperCase0[1]=0;
                    }
                    if((splitAfterTimeinHoursperCase[1]>0)&&(splitAfterTimeinHoursperCase[1]<=25))
                    {
                        splitAfterTimeinHoursperCase[1]=15;
                        splitAfterTimeinHoursperCase0[1]=25;
                    }
                    if((splitAfterTimeinHoursperCase[1]>25)&&(splitAfterTimeinHoursperCase[1]<=50))
                    {
                        splitAfterTimeinHoursperCase[1]=30;
                        splitAfterTimeinHoursperCase0[1]=50;
                    }
                    if((splitAfterTimeinHoursperCase[1]>50)&&(splitAfterTimeinHoursperCase[1]<=75))
                    {
                        splitAfterTimeinHoursperCase[1]=45;
                        splitAfterTimeinHoursperCase0[1]=75;
                    }
                    if((splitAfterTimeinHoursperCase[1]>75)&&(splitAfterTimeinHoursperCase[1]<99))
                    {
                        splitAfterTimeinHoursperCase[1]=0;
                        splitAfterTimeinHoursperCase[0]=parseInt(splitAfterTimeinHoursperCase[0])+parseInt(1);
                        splitAfterTimeinHoursperCase0[1]=0;
                        splitAfterTimeinHoursperCase0[0]=parseInt(splitAfterTimeinHoursperCase0[0])+parseInt(1);

                    }
                    var timeAfter= splitAfterTimeinHoursperCase[0]+'.'+ splitAfterTimeinHoursperCase[1];
                    timeAfter=parseFloat(timeAfter);
                    timeAfter=timeAfter.toFixed(2);
                    var timeAfter0= splitAfterTimeinHoursperCase0[0]+'.'+ splitAfterTimeinHoursperCase0[1];
                    timeAfter0=parseFloat(timeAfter0);
                    timeAfter0=timeAfter0.toFixed(2);
                    newInvoice.setLineItemValue('item', 'item', ctr, 545);
                    newInvoice.setLineItemValue('item', 'quantity', ctr, timeAfter0);
                    newInvoice.setLineItemValue('item', 'rate',ctr, afterRate);
                    newInvoice.setLineItemValue('item', 'location',ctr, location);
                    newInvoice.setLineItemValue('item', 'amount',ctr, timeAfter0*afterRate);
                    newInvoice.setLineItemValue('item', 'custcol_clgx_rh_invoice_item_case',ctr, internalID);
                    var returntax=clgx_return_tax(location,customer);
                    if(returntax!='')
                    {
                        newInvoice.setLineItemValue('item', 'taxcode',ctr, returntax[0]);
                        newInvoice.setLineItemValue('item', 'taxcode_display',ctr, returntax[1]);

                    }
                    newInvoice.setLineItemValue('item', 'description',ctr, 'Case\r\n #'+casenumber+ '\r\nRequested by '+email);
                    ctr++;
                }
                newInvoice.setFieldValue('entity', customer); //set customer ID
                var billingid='';
                if(billing=='- None -')
                {
                    var billingid=clgx_return_billing (location, customer)

                }
                if(billingid!='')
                {
                    var custRecord=nlapiLoadRecord('customer',customer);
                    var NRABs = custRecord.getLineItemCount('addressbook');
                    for ( var j = 1; j <= NRABs; j++ ) {
                        var internalid = custRecord.getLineItemValue('addressbook', 'internalid', j);
                        if(internalid== billingid)
                        {
                            var billaddress=custRecord.getLineItemValue('addressbook', 'addressbookaddress_text', j);
                            var billaddresslist=custRecord.getLineItemValue('addressbook', 'addressid', j);
                            var billingaddress_key=custRecord.getLineItemValue('addressbook', 'addressbookaddress_key', j);
                            var billingaddress_text=custRecord.getLineItemValue('addressbook', 'addressbookaddress_text', j);
                            var billingaddress_type=custRecord.getLineItemValue('addressbook', 'addressbookaddress_type', j);
                            var billisresidential=custRecord.getLineItemValue('addressbook', 'isresidential', j);
                            var addressee = custRecord.getLineItemValue('addressbook', 'addressee', j);

                        }


                    }
                    newInvoice.setFieldValue('billaddress', billaddress);
                    newInvoice.setFieldValue('billaddressee', addressee);
                    newInvoice.setFieldValue('billaddresslist', billaddresslist);
                    newInvoice.setFieldValue('billingaddress_key', billingaddress_key);
                    newInvoice.setFieldValue('billingaddress_text', billingaddress_text);
                    newInvoice.setFieldValue('billingaddress_type', billingaddress_type);
                    newInvoice.setFieldValue('billisresidential', 'F');
                } else{
                    newInvoice.setFieldValue('billaddress', billing);
                    //  newInvoice.setFieldValue('billaddressee', addressee);
                }


                //newInvoice.setFieldValue('name', customer);
                newInvoice.setFieldValue('trandate', startDate1);
                newInvoice.setFieldValue('saleseffectivedate', startDate1);

               if((busH>0)||(afterH>0))
                {
                    var invoiceId =nlapiSubmitRecord(newInvoice, false,true);
                }
                 
              

                emailBody = emailBody+'Please check this invoice and make sure everyting is ok!!!!' +invoiceId+'\r\n';

                //Update the case field billed
                // var caseRecord=nlapiLoadRecord('supportcase',internalID);
                // caseRecord.setFieldValue('custevent_clgx_case_invoiced','T');
                // nlapiSubmitRecord(caseRecord, false,true);

            }
            var emailSubject = 'Dear Catalina,';
            
            //nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null,true); // Send email to Catalina
            
            clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_case_admin", emailSubject, emailBody);
            
            var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            var index = i + 1;
            nlapiLogExecution('DEBUG','SO ', ' | consolidateinvid =  ' + invoiceId + ' | Index = ' + index + ' of ' + searchRemote.length + ' | Usage - '+ usageConsumtion + '  |');
        }
        var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
        nlapiLogExecution('DEBUG','Finished Execution', 'Total Remaining Usage - ' + context.getRemainingUsage() + 'Total Usage-' + usageConsumtion + '-------------------------- Finished Scheduled Script --------------------------|');

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function checkFixedRem(customer,so)
{
    var arrDates1  = getDateRange(0);
    var startDate1 = arrDates1[0];
    var endDate1  = arrDates1[1];
    var date=new Date();
    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_rh_customer",null,"anyof",customer));
    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_rh_from_date",null,"on",startDate1));
    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_rh_to_date",null,"on",endDate1));
    var searchRemote = nlapiSearchRecord('customrecord_clgx_remote_hands',   'customsearch_clgx_search_rhfinvoices1', arrFilters, arrColumns);
    var test=0;
    for ( var i = 0; searchRemote != null && i < searchRemote.length; i++ ) {
        var searchRem = searchRemote[i];
        var columns = searchRem.getAllColumns();
        var location=searchRem.getValue(columns[0]);
        var customer=searchRem.getValue(columns[1]);
        var casenumber=searchRem.getValue(columns[2]);
        var email=searchRem.getValue(columns[3]);
        var so1=searchRem.getValue(columns[4]);
        var soLoc=searchRem.getValue(columns[5]);
        var itemrate=searchRem.getValue(columns[6]);
        var prepaidTime=searchRem.getValue(columns[7]);
        var busH=searchRem.getValue(columns[8]);
        var busRate=searchRem.getValue(columns[9]);
        var afterH=searchRem.getValue(columns[10]);
        var afterRate=searchRem.getValue(columns[11]);
        var package=searchRem.getValue(columns[12]);
        var packageH=searchRem.getValue(columns[13]);
    }
    if(so==so1)
    {
        if(prepaidTime>0)
        {
            test=0;

        }
        else{
            test=1;
        }
    }
    return test;
}

function getLocation(location)
{
//COL1&2
    if(location==24)
    {
        var loc=34;
    }
    //TOR 156 Front
    if(location== 23)
    {
        var loc=13;
    }
    //Cologix HQ
    if(location== 16)
    {
        var loc=0;
    }
    //DAL1
    if(location==3)
    {
        var loc=2;
    }
    //DAL2
    if(location==18)
    {
        var loc=17;
    }
    //JAX1
    if(location==21)
    {
        var loc=31;
    }
    //JAX2
    if(location==27)
    {
        var loc=40;
    }
    //MIN1&2
    if(location==17)
    {
        var loc=16;
    }
    //MIN3
    if(location==25)
    {
        var loc=35;
    }
    //MTL1
    if(location==2)
    {
        var loc=5;
    }
    //MTL2
    if(location==5)
    {
        var loc=8;
    }
    //MTL3
    if(location==4)
    {
        var loc=9;
    }
    //MTL4
    if(location==9)
    {
        var loc=10;
    }
    //MTL5
    if(location==6)
    {
        var loc=11;
    }
    //MTL6
    if(location==7)
    {
        var loc=12;
    }
    //MTL7
    if(location==19)
    {
        var loc=27;
    }
    //TOR1
    if(location==8)
    {
        var loc=6;
    }
    //TOR2
    if(location==15)
    {
        var loc=15;
    }
    //VAN1
    if(location==14)
    {
        var loc=28;
    }
    //VAN2
    if(location==20)
    {
        var loc=7;
    }
    return loc;
}

function getDateRange(monthsago){

    var today = new Date();
    var date = nlapiAddMonths(today, monthsago);

    var month = parseInt(date.getMonth());
    var year = date.getFullYear();

    var stDays  = daysInMonth(parseInt(month),parseInt(year));
    var stYear  = year;

    var stMonth = parseInt(month) + 1;

    var stStartDate = stMonth + '/1/' + stYear;
    var stEndDate   = stMonth + '/' + stDays + '/' + stYear;

    var arrDateRange = new Array();
    arrDateRange[0] = stStartDate;
    arrDateRange[1] =stEndDate;

    return arrDateRange;
}

function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}

//check if a value is in the array
function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}

function roundMinBusiness(totbusinessH){
    totbusinessH=totbusinessH.toFixed(2);
    totbusinessH=totbusinessH.toString();
    var hb=totbusinessH.split(".");
    if((hb[1]>0)&&(hb[1]<=15))
    {
        hb[1]=15;
    }
    if((hb[1]>15)&&(hb[1]<=30))
    {
        hb[1]=30;
    }
    if((hb[1]>30)&&(hb[1]<=45))
    {
        hb[1]=45;
    }
    if((hb[1]>45)&&(hb[1]<=59))
    {
        hb[1]=0;
        hb[0]=parseInt(hb[0])+parseInt(1);
    }
    var totalTimeB=hb[0]+'.'+hb[1];
    if(parseInt(hb[0])*parseInt(60)>0)
    {
        totalTimeB=parseInt(hb[0])*parseInt(60)+parseInt(hb[1]);
    }
    else
    {
        totalTimeB=parseInt(hb[1]);
    }

    return totalTimeB;

}

function roundMinAfter(totafterH){
    totafterH=totafterH.toFixed(2);
    totafterH=totafterH.toString();
    var ha=totafterH.split(".");
    if((ha[0]==0)&&(ha[1]==0))
    {
        var totalTimeA=0;
    }
//after hours
    if((ha[0]<1)&&(ha[1]>0))
    {
        var totalTimeA=1;
    }
    else
    {
        if((ha[1]>0)&&(ha[1]<=15))
        {
            ha[1]=15;
        }
        if((ha[1]>15)&&(ha[1]<=30))
        {
            ha[1]=30;
        }
        if((ha[1]>30)&&(ha[1]<=45))
        {
            ha[1]=45;
        }
        if((ha[1]>45)&&(ha[1]<=59))
        {
            ha[1]=0;
            ha[0]=parseInt(ha[0])+parseInt(1);
        }
        var totalTimeA=ha[0]+'.'+ha[1];
    }
    if(parseInt(ha[0])*parseInt(60)>0)
    {
        totalTimeA=parseInt(ha[0])*parseInt(60)+parseInt(ha[1]);
    }
    else
    {
        totalTimeA=parseInt(ha[1]);
    }
    return totalTimeA;
}

//return tax
function clgx_return_tax (locationid, customerid){

    var returntax = '';
    var returntaxText = '';
    var customertax=nlapiLoadRecord('customer', customerid);

    switch(parseInt(locationid)) {
        case 2: // Dallas Infomart
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_dal'); // DAL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_dal'); // DAL
            break;
        case 5: // MTL1
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 6: // 151 Front Street West
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_tor'); // TOR
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_tor'); // TOR
            break;
        case 7: // Harbour Centre
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_van'); // VAN
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_van'); // VAN
            break;
        case 8: // MTL2
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 9: // MTL3
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 10: // MTL4
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 11: // MTL5
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 12: // MTL6
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 13: // 156 Front Street West
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_tor');// TOR
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_tor');// TOR
            break;
        case 14: // Barrie Office
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_tor'); // TOR
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_tor'); // TOR
            break;
        case 15: // 905 King Street West
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_tor'); // TOR
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_tor'); // TOR
            break;
        case 16: // MIN1&2
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_min'); // MIN
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_min'); // MIN
            break;
        case 17: // Dallas Infomart - Ste 2010
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_dal'); // DAL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_dal'); // DAL
            break;
        case 18: // 151 Front Street West - Ste 822
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_tor'); // 151 Front Street West - Ste 822
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_tor'); // 151 Front Street West - Ste 822
            break;
        case 27: // MTL7
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 28: // 1050 Pender
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_van'); // VAN
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_van'); // VAN
            break;
        case 29: // JAX1
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_jax'); // JAX1
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_jax'); // JAX1
            break;
        case 31: // JAX1
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_jax'); // JAX1
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_jax'); // JAX1
            break;
        case 33: // Columbus
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_col'); // COL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_col'); // COL
            break;
        case 34: // COL1&2
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_col'); // COL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_col'); // COL
            break;
        case 35: // MIN3
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_min'); // MIN
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_min'); // MIN
            break;
        case 39: // COL3
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_min'); // COL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_min'); // COL
            break;
        case 40: // JAX2
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_jax'); // JAX2
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_jax'); // JAX2
            break;
        case 42: // LAK1
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_jax'); // Jacksonville
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_jax'); // Jacksonville
            break;
        default:
            returntax = '';

    }
    var returntaxArray=[returntax,returntaxText]
    return returntaxArray;
}

//return billing address
//return tax
function clgx_return_billing (locationid, customerid){

    var returnbilling = '';
//Bell Canada        -          MTL1              -               1114
//                   -          MTL2              -               24133
//                   -          MTL3              -               24159
    if(customerid==2340)
    {
        if(locationid==5)
        {
            returnbilling=1114;
        }
        if(locationid==8)
        {
            returnbilling=24133;
        }
        if(locationid==9)
        {
            returnbilling=24159;
        }
    }

//    Fibrenoire           -           MTL3              -              24130
//                         -           MTL4              -              11411
//                         -           MTL5              -              15044

    if(customerid==1720)
    {
        if(locationid==9)
        {
            returnbilling=24130;
        }
        if(locationid==11)
        {
            returnbilling=11411;
        }
        if(locationid==11)
        {
            returnbilling=15044;
        }
    }
//    Hurricane Electric CA -     TOR1 or TOR2    -           6232
//                       -          VAN1 OR VAN2  -           15313
//            -   MTL1, MTL2, MTL3, MTL4, MTL5, MTL6, MTL7  - 15314
    if(customerid==6501)
    {
        if((locationid==14)||(locationid==15))
        {
            returnbilling=6232;
        }
        if((locationid==7)||(locationid==28))
        {
            returnbilling=15313;
        }
        if((locationid==5)||(locationid==8)||(locationid==9)||(locationid==10)||(locationid==11)||(locationid==12)||(locationid==27))
        {
            returnbilling=15314;
        }
    }
//    Hurricane Electric US -   MIN1&2, MIN3      -           6084
//        -    DAL1 & DAL2       -          15315
//        -    COL1&2, COL3    -           23627

    if(customerid==9519)
    {
        if((locationid==16)||(locationid==35))
        {
            returnbilling=6084;
        }
        if((locationid==2)||(locationid==17))
        {
            returnbilling=15315;
        }
        if((locationid==34)||(locationid==39))
        {
            returnbilling=23627;
        }
    }

//    Hydro One Telecom Inc - TOR1 or TOR2    -           1032
//        -   MTL1, MTL2, MTL3, MTL4, MTL5, MTL6, MTL7  - 12949
    if(customerid==1764)
    {
        if((locationid==14)||(locationid==15))
        {
            returnbilling=1032;
        }
        if((locationid==5)||(locationid==8)||(locationid==9)||(locationid==10)||(locationid==11)||(locationid==12)||(locationid==27))
        {
            returnbilling=12949;
        }
    }
//    Netelligent Hosting      -          MTL1               -         958
//        -          MTL2               -         11243
//        -          MTL3               -         11244
//        -          MTL4               -         11245
//        -          MTL5               -         11246
//        -      TOR1 or TOR2          -           11372
    if(customerid==2191)
    {
        if((locationid==14)||(locationid==15))
        {
            returnbilling=11372;
        }
        if(locationid==5)
        {
            returnbilling=958;
        }
        if(locationid==8)
        {
            returnbilling=11243;
        }
        if(locationid==9)
        {
            returnbilling=11244;
        }
        if(locationid==10)
        {
            returnbilling=11245;
        }
        if(locationid==11)
        {
            returnbilling=11246;
        }


    }
    return returnbilling;
}