nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_Case.js
//	Script Name:	CLGX_CR_Case
//	Script Id:		customscript_clgx_cr_case
//	Script Runs:	Server
//	Script Type:	User Event Script
//	Deployments:	Case
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/23/2012
//-------------------------------------------------------------------------------------------------

function pageInit(type){
    try {
        nlapiLogExecution('DEBUG','Client/Record - PageInit','--------------------STARTED---------------------|');
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 2/6/2012
// Details:	Hides or not field MOP depending on case type and sub type.
//-------------------------------------------------------------------------------------------------

        var currentContext = nlapiGetContext();
        stRole = currentContext.getRole();
        if (currentContext.getExecutionContext() == 'userinterface') {

            var stCaseType = nlapiGetFieldValue('category');

            var stSubCaseType = nlapiGetFieldValue('custevent_cologix_sub_case_type');

            if ((stCaseType == 9) && ((stSubCaseType == 37) || (stSubCaseType == 38) || (stSubCaseType == 39) || (stSubCaseType == 40))){
                nlapiSetFieldDisplay('custevent_clgx_audit_mops', true);
                //var stMOP = nlapiGetField(fldnam);
                //stMOP.setDisplayType(type);

            }
        }

//---------- End Section 1 ------------------------------------------------------------------------------------------------
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function fieldChanged(type, name, linenum){
    try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 12/23/2011
// Details:	Set the value for Service Item on 'Time Tracking' sublist to 'Remote Hands : Billable Time' when 'Remote Hands' is choosen on 'Case Type' field
//-------------------------------------------------------------------------------------------------
        if (type == 'timeitem') {
            var stCaseType = nlapiGetFieldText('category');
            if (stCaseType == 'Remote Hands ($$)') {
                var stServiceItem = nlapiGetCurrentLineItemText('timeitem', 'item').substring(0,6);
                if (stServiceItem != 'Remote') {
                    nlapiSetCurrentLineItemText('timeitem', 'item', 'Remote Hands : Billable Time Customer Support Time');
                }
            }
        }
//------------- End Section 1 -------------------------------------------------------------------
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function lineInit(type) {
    try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 12/23/2011
// Details:	Set the value for Service Item on 'Time Tracking' sublist to 'Remote Hands : Billable Time' when 'Remote Hands' is choosen on 'Case Type' field
//-------------------------------------------------------------------------------------------------
        if (type == 'timeitem') {
            nlapiDisableLineItemField('timeitem', 'isbillable', true);
            nlapiDisableLineItemField('timeitem', 'rate', true);

            var stCaseType = nlapiGetFieldText('category');
            if (stCaseType == 'Remote Hands ($$)') {
                nlapiSetCurrentLineItemText('timeitem', 'item', 'Remote Hands : Billable Time Customer Support Time');
            }
        }
//------------- End Section 1 -------------------------------------------------------------------
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function saveRecord(){
    try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 4/4/2013
// Details:	If Status is is Closed-Lost, Loss reason is mandatory
//-------------------------------------------------------------------------------------------------
        var currentContext = nlapiGetContext();
        var usrRole = currentContext.getRole();

        var allowSave = 1;
        var alertMsg = '';
        //Don't allow to save a case on there is a blockout date
        var companyid=nlapiGetFieldValue("company");
        var type=nlapiGetFieldValue("category");
        var facility=nlapiGetFieldValue('custevent_cologix_facility');
        var facilityText=nlapiGetFieldText('custevent_cologix_facility');
        var followup=nlapiGetFieldValue('custevent_cologix_case_sched_followup');
        var stimefollowup=nlapiGetFieldValue('custevent_cologix_sched_start_time');
        var endDate=nlapiGetFieldValue('custevent_clgx_case_end_date');
        var endTime=nlapiGetFieldValue('custevent_clgx_case_end_time');
        /* Roles


         // see all
         -5      Administrator
         3       Administrator
         18      Full Access
         //Moratorium
         */
        if(usrRole!=-5 && usrRole!=3 && usrRole!=18) {

            if ((companyid == 2789 || companyid == 2790 || companyid == 2763) && (type == 3)) {

                var arrFiltersM = new Array();
                var arrColumnsM = new Array();
                if(followup!=null && followup!='') {
                    if (endDate != null && endDate!='') {
                        var filterExpression = [
                            ['custrecord_clgx_mor_facility', 'anyof', facility], 'and',
                            [[['custrecord_clgx_mor_start_date', 'onOrBefore', followup],'and',['custrecord_clgx_mor_end_date', 'onOrAfter', followup]],
                                'or',
                                [['custrecord_clgx_mor_start_date', 'onOrAfter', followup],'and',['custrecord_clgx_mor_end_date', 'onOrAfter', followup]],
                                'or',
                                [[['custrecord_clgx_mor_start_date', 'onOrBefore', endDate],'and',['custrecord_clgx_mor_end_date', 'onOrAfter', endDate]],
                                    'or',
                                    [['custrecord_clgx_mor_start_date', 'onOrAfter', endDate],'and',['custrecord_clgx_mor_end_date', 'onOrAfter', endDate]]]]];
                    } else {
                        var filterExpression = [
                            ['custrecord_clgx_mor_facility', 'anyof', facility], 'and',
                            [[['custrecord_clgx_mor_start_date', 'onOrBefore', followup],'and',['custrecord_clgx_mor_end_date', 'onOrAfter', followup]],
                                'or',
                                [['custrecord_clgx_mor_start_date', 'onOrAfter', followup],'and',['custrecord_clgx_mor_end_date', 'onOrAfter', followup]]]
                        ];
                    }
                    var searchM = nlapiSearchRecord('customrecord_clgx_moratorium', 'customsearch_clgx_moratorium_ops_search', filterExpression, arrColumnsM);
                    if (searchM != null) {

                        for (var k = 0; searchM != null && k < searchM.length; k++) {
                            var searchCaseM = searchM[k];
                            var columns1 = searchCaseM.getAllColumns();
                            var sdate = searchCaseM.getValue(columns1[0]);
                            var edate = searchCaseM.getValue(columns1[1]);
                            var etime = searchCaseM.getValue(columns1[3]);
                            var stime = searchCaseM.getValue(columns1[4]);


                            var startDateStr = sdate + ' ' + stime;
                            var endDateStr = edate + ' ' + etime;
                            var followupStr = followup + ' ' + stimefollowup;
                            if(endDate!=null && endDate!='') {
                                var endCaseStr = endDate + ' ' + endTime;
                            }
                            var x = moment(followupStr).format("YYYY-MM-DD HH:mm:ss  A");
                            var x1 = moment(startDateStr).format("YYYY-MM-DD HH:mm:ss  A");
                            var followupTest = (moment(followupStr)).isBetween(moment(startDateStr), moment(endDateStr));
                            if(endDate!=null && endDate!='') {
                                var endDateTest = (moment(endCaseStr)).isBetween(moment(startDateStr), moment(endDateStr));
                            }
                            if(followupTest===true && allowSave==1){
                                var endMoment = moment(endDateStr);

                                var inspectiondate = moment(endMoment).add(1, 'minutes').format('MM/DD/YYYY, h:mm a');
                                var allowSave = 0;
                                var alertMsg = 'We have a blackout date between ' + sdate + ' and ' + edate + '. The next available date is on ' + inspectiondate;
                                break;
                            }
                            if(endDate!=null && endDate!='') {
                                if (endDateTest === true && allowSave==1) {
                                    var endMoment = moment(endDateStr);
                                    var inspectiondate = moment(endMoment).add(1, 'minutes').format('MM/DD/YYYY, h:mm a');
                                    var allowSave = 0;
                                    var alertMsg = 'We have a blackout date between ' + sdate + ' and ' + edate + '. The next available date is on ' + inspectiondate;
                                    break;
                                }
                            }
                            if(endDate!=null && endDate!='' && allowSave==1) {
                                if (moment(startDateStr).isSameOrAfter(moment(followupStr)) && moment(startDateStr).isSameOrBefore(moment(endCaseStr)) && moment(endDateStr).isSameOrAfter(moment(followupStr)) && moment(endDateStr).isSameOrBefore(moment(endCaseStr))) {
                                    var endMoment = moment(endDateStr);
                                    var inspectiondate = moment(endMoment).add(1, 'minutes').format('MM/DD/YYYY, h:mm a');
                                    var allowSave = 0;
                                    var alertMsg = 'We have a blackout date between ' + sdate + ' and ' + edate + '. The next available date is on ' + inspectiondate;
                                    break;
                                }
                            }

                        }
                    }
                }

            }
        }
        // Catalina Remote Hands
        var company=nlapiGetFieldValue('company');
        if((company!=2789)&&(company!=2790)) {
            var category = nlapiGetFieldValue('category');


            if (category == 1) {
                var searchColumnsCustomer = new Array();
                var searchFilterCustomer = new Array();
                searchFilterCustomer.push(new nlobjSearchFilter('internalid', null, 'anyof', company));
                var searchServices = nlapiSearchRecord('customer', 'customsearch_ss_create_rh_services', searchFilterCustomer, searchColumnsCustomer);
                if (searchServices != null) {
                    var allowSave = 0;
                    var alertMsg = "You have selected a service instead of a customer, please correct the 'Customer' field";
                }


                if ((usrRole == 18 || usrRole == 1054 || usrRole == 1003 || usrRole == 1004) && (searchServices == null)) {
                    var searchPartener = nlapiSearchRecord('partner', 'customsearch_clgx_ss_partener_rh', searchFilterCustomer, searchColumnsCustomer);
                    var searchCustomer = nlapiSearchRecord('customer', 'customsearch_clgx_ss_customer_rh', searchFilterCustomer, searchColumnsCustomer);
                    if (searchPartener != null && searchCustomer==null) {
                        var allowSave = 0;
                        var alertMsg = "  You have selected a Partner in the Customer : Service field, please change this to a Customer";
                    }
                    var searchCustomer = nlapiSearchRecord('customer', null, searchFilterCustomer, searchColumnsCustomer);
                    if (searchCustomer != '' && searchCustomer != null) {
                        var customerRec = nlapiLoadRecord('customer', company);
                        var subsidiary = customerRec.getFieldValue('subsidiary');
                        var statusCust = customerRec.getFieldValue('entitystatus');
                        var subcasetype = nlapiGetFieldValue('custevent_cologix_sub_case_type');
                        //  var location = clgx_return_locationid(facility);
                        var searchColumnsSO = new Array();
                        var searchFilterSO = new Array();
                        searchFilterSO.push(new nlobjSearchFilter('customer', null, 'anyof', company));
                        searchFilterSO.push(new nlobjSearchFilter('custentity_cologix_facility', null, 'anyof', facility));
                        var searchSOs = nlapiSearchRecord('job', 'customsearch_clgx_ss_so_fac1', searchFilterSO, searchColumnsSO);
                        if ((statusCust != 13) && ((subcasetype == 5) || (subcasetype == 6) || (subcasetype == 85))) {

                            var allowSave = 0;
                            var alertMsg = "You have selected a Company that does not have active services with Cologix. Please lookup the Service Order and select the correct Customer for this remote hands request.";

                        } else if (subsidiary == 9) {
                            var allowSave = 0;
                            var alertMsg = "You have selected a Parent company, and Remote Hands cannot be processed for Parent companies. Please lookup the Service Order and select the correct Customer for this remote hands request.";
                        }


                        else if ((searchSOs == null) && ((subcasetype == 5) || (subcasetype == 6) || (subcasetype == 85))) {
                            var allowSave = 0;
                            var alertMsg = "The Customer you have selected does not have services in " + facilityText + ", please select the correct customer.";

                        }
                    }
                }
            }
        }
//Catalina
        var allowSaveCase=nlapiGetFieldValue("custpage_clgx_case_allowsave");
        if(allowSaveCase==0)
        {
            var allowSave = 0;
            var alertMsg = 'This case already has been billed. Please add another case.';
        }
//Catalina END RH


        var category = nlapiGetFieldText('category');
        var equipment = nlapiGetFieldValue('custevent_cologix_equipment');
        var notify = nlapiGetFieldValue('custevent_clgx_customer_notification');
        var status = nlapiGetFieldText('status');
        var origin = nlapiGetFieldText('origin');

        if(category == 'Maintenance' && notify == 'T' && origin != 'Command' && (equipment == '' || equipment == null)) {
            var allowSave = 0;
            var alertMsg = 'When case type is "Maintenance", you must choose an equipment.';
        }
        if(category == 'Incident' && notify == 'T'  && origin != 'Command' && (equipment == '' || equipment == null)) {
            var allowSave = 0;
            var alertMsg = 'When case type is "Incident" and "Customer Notification Required " is checked, you must choose an equipment.';
        }

        if (allowSave == 0) {
            alert(alertMsg);
            return false;
        }
        else{

            if (equipment != '' && equipment != null && notify == 'T'){

                var caseid = nlapiGetRecordId();
                if(caseid != null && caseid != ''){ // this is an existing case
                    // Search cases with this parent
                    var searchColumns = new Array();
                    searchColumns.push(new nlobjSearchColumn('internalid',null,null));
                    var searchFilter = new Array();
                    searchFilter.push(new nlobjSearchFilter('custevent_clgx_parent_id',null,'anyof', caseid));
                    var searchCases = nlapiSearchRecord('supportcase',null,searchFilter,searchColumns);

                    if(searchCases == null){ // no sub cases have been created yet
                        var response = confirm('Please make sure you have selected the right equipment before clicking <Save>. You will be informed by email when all sub cases have been created. Please wait for this email before editing this case again.');
                        if (response) {return true;}
                        else {return false;}
                    }
                    else{ // sub cases have been created
                        return true;
                    }
                }
                else{ // this is a new case
                    var response = confirm('Please make sure you have selected the right equipment before clicking <Save>. You will be informed by email when all sub cases have been created. Please wait for this email before editing this case again.');
                    if (response) {return true;}
                    else {return false;}
                }
            }
            else{
                return true;
            }
        }
//-------------------------------------------------------------------------------------------------

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
function fieldChange(type, name)
{
//RH
    if (name == 'company') {
        var company = nlapiGetFieldValue('company');
        if ((company != 2789) && (company != 2790)) {
            var category = nlapiGetFieldValue('category');

            if (category == 1) {
                var searchColumnsCustomer = new Array();
                var searchFilterCustomer = new Array();
                searchFilterCustomer.push(new nlobjSearchFilter('internalid', null, 'anyof', company));
                var searchServices = nlapiSearchRecord('customer', 'customsearch_ss_create_rh_services', searchFilterCustomer, searchColumnsCustomer);
                if (searchServices != null) {
                    var allowSave = 0;
                    alert("You have selected a service instead of a customer, please correct the 'Customer' field");
                    return false;
                }
                var currentContext = nlapiGetContext();
                var usrRole = currentContext.getRole();
                if (usrRole == 18 || usrRole == 1054 || usrRole == 1003 || usrRole == 1004) {
                    var companyText = nlapiGetFieldText('company');
                    var searchColumnsCustomer1 = new Array();
                    var searchFilterCustomer1 = new Array();
                    searchFilterCustomer1.push(new nlobjSearchFilter('internalid', null, 'anyof', company));
                    var searchServices1 = nlapiSearchRecord('customer', 'customsearch_clgx_ss_pr_rhcases', searchFilterCustomer1, searchColumnsCustomer1);
                    if (searchServices1 != null) {
                        var allowSave = 0;
                        alert("You have selected a Project, please select a Company or Employee in the Customer : Service field.");
                        return false;
                    }

                    var substring = "Anonymous";
                    if (companyText.indexOf(substring) > -1) {
                        alert("You have selected an anonymous customer, please select the correct customer in the Customer : Service field.");
                        return false;
                    }

                }
            }
        }
    }
    return true;
}
function fieldChangedChurn(type, name)
{
//RH
    if (name == 'custevent_cologix_sub_case_type')
    {

        if ( (nlapiGetFieldValue('custevent_cologix_sub_case_type') == '50') &&
            (nlapiGetFieldValue('category')=='11') )
        {
            //   var churnstatus = nlapiGetField('custevent_clgx_case_churnstatus');
            //  churnstatus.setDisplayType('normal');
            return true;
        }
    }
    return true;
}