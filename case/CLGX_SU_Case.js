nlapiLogExecution("audit", "FLOStart", new Date().getTime());
// 
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Case.js
//	Script Name:	CLGX_SU_Case
//	Script Id:		customscript_clgx_su_case
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Sales Order - Sandbox | Production
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/14/2011
//-------------------------------------------------------------------------------------------------

function beforeLoad(type, form) {
    try {
        var start = moment();
        var body = '| ';
        nlapiLogExecution('DEBUG', 'User Event - Before Load', '|-------------STARTED--------------|');

        //------------- Begin Section 1 -----------------------------------------------------------------------------------
        // Details:	Verify if case is processed to create sub cases - if yes, don't allow editing.
        // Created:	07/14/2014
        //-----------------------------------------------------------------------------------------------------------------
        var caseid = nlapiGetRecordId();
        var currentContext = nlapiGetContext();
        var roleid = currentContext.getRole();
        nlapiLogExecution('DEBUG', 'Context Type', type);
        if ((currentContext.getExecutionContext() == 'userinterface') && ((type == 'edit') || (type == 'view'))) {
            if ((caseid != null) && (caseid != '')) {
                var subCaseType = nlapiGetFieldValue('custevent_cologix_sub_case_type');
                if (subCaseType == 112 && (roleid != -5 && roleid != 3 && roleid != 18 && roleid != 1004 && roleid != 1054)) {
                    form.getField('priority').setDisplayType('inline');
                }

                //-5      Administrator
                // 3       Administrator
                //18      Full Access
                if (subCaseType == 112) {
                    var title = nlapiGetFieldValue('title');
                    var startdate = nlapiGetFieldValue('startdate');

                    var message = 'Below is an update for services ' + title + '.<br><br>Incident Start Time: ' + startdate + '<br><br>Description of Problem:<br><br>Current Status of Resolution:<br><br>Estimated Time of Resolution:';
                    nlapiSetFieldValue('outgoingmessage', message);
                    // form.getField('outgoingmessage').setDisplayType('inline');
                    //form.getField('insertsolution').setDisplayType('inline');

                    //alert('Please select the 1st message');
                }
                ///------------------------ ACS 
                var customrecord_ns_acs_in_progressSearch = nlapiSearchRecord("customrecord_ns_acs_in_progress", null,
                        [
                            new nlobjSearchFilter('custrecord_ns_acs_parent_case', null, 'is', caseid)
                        ],
                        [
                            new nlobjSearchColumn("id").setSort(false),
                            new nlobjSearchColumn("scriptid")
                        ]
                    );
        
                    if (customrecord_ns_acs_in_progressSearch != null) {
                        nlapiLogExecution('DEBUG', 'has Custom Record Field', "LOcked");
                        var custom_in_progress_count = customrecord_ns_acs_in_progressSearch.length
                        if (custom_in_progress_count > 0) {
                            nlapiSubmitField('supportcase', caseid, 'custevent_clgx_locked_case', 'T')
                            var locked = nlapiGetFieldValue('custevent_clgx_locked_case');
                            nlapiLogExecution('DEBUG', 'before log locked', locked);
        
                            var arrParam = new Array();
                            arrParam['custscript_internal_message'] = 'The sub cases for this parent case are still being created or updated. Please wait until you have received the confirmation email.';
        
                            if (nlapiGetRole() != 1088) {
                                nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
                            }
        
                        }
                    }
                var locked = nlapiGetFieldValue('custevent_clgx_locked_case');
                if (locked == 'T') {
                    nlapiLogExecution('DEBUG', 'Locked Field -- 1', "LOcked");
                    var locked = nlapiGetFieldValue('custevent_clgx_locked_case');
                    var arrParam = new Array();
                    arrParam['custscript_internal_message'] = 'The sub cases for this parent case are still being created or updated. Please wait until you have received the confirmation email.';

                    if (nlapiGetRole() != 1088) {
                        nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
                    }
                }

                
                    
                    
                /////-------------------------------- ACS

                /*var searchColumn = new Array();
                var searchFilter=new Array();
                searchColumn.push(new nlobjSearchColumn('custcol_clgx_rh_invoice_item_case',null,'GROUP'));
                searchFilter.push(new nlobjSearchFilter('custcol_clgx_rh_invoice_item_case',null,'anyof',caseid));
                var searchInvoices = nlapiSearchRecord('invoice',null,searchFilter,searchColumn);
                if(searchInvoices!=null)
                {
                    if(roleid==1003)
                    {
                        var arrParam = new Array();
                        arrParam['custscript_internal_message'] = 'This Remote Hands Case has already been billed, Please create a new case';
                        nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);


                    }

                }*/


            }
        }
        var locked = nlapiGetFieldValue('custevent_clgx_locked_case');
        if (locked == 'T') {
            var locked = nlapiGetFieldValue('custevent_clgx_locked_case');
            var arrParam = new Array();
            arrParam['custscript_internal_message'] = 'The sub cases for this parent case are still being created or updated. Please wait until you have received the confirmation email.';

            if (nlapiGetRole() != 1088) {
                nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
            }
        }
        if ((caseid != null) && (caseid != '')) {
            var customrecord_ns_acs_in_progressSearch = nlapiSearchRecord("customrecord_ns_acs_in_progress", null,
                [
                    new nlobjSearchFilter('custrecord_ns_acs_parent_case', null, 'is', caseid)
                ],
                [
                    new nlobjSearchColumn("id").setSort(false),
                    new nlobjSearchColumn("scriptid")
                ]
            );

            if (customrecord_ns_acs_in_progressSearch != null) {
                var custom_in_progress_count = customrecord_ns_acs_in_progressSearch.length
                if (custom_in_progress_count > 0) {
                    nlapiSubmitField('supportcase', caseid, 'custevent_clgx_locked_case', 'T')
                    var locked = nlapiGetFieldValue('custevent_clgx_locked_case');
                    //nlapiLogExecution('DEBUG', 'before log locked', locked);

                    var arrParam = new Array();
                    arrParam['custscript_internal_message'] = 'The sub cases for this parent case are still being created or updated. Please wait until you have received the confirmation email.';

                    if (nlapiGetRole() != 1088) {
                        nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
                    }

                }
            }
        }


        body += '01 - ' + moment().diff(start) + ' | ';
        //------------- Begin Section 2 -----------------------------------------------------------------------------------
        // Details:	Disable Equipment field if Notify Customer is checked.
        // Created:	07/11/2014
        //-----------------------------------------------------------------------------------------------------------------

        var famid = nlapiGetFieldValue('custevent_cologix_equipment');
        var notify = nlapiGetFieldValue('custevent_clgx_customer_notification');
        if (notify == 'T' && famid != null && famid != '') {
            form.getField('custevent_cologix_equipment').setDisplayType('inline');
            form.getField('custevent_clgx_customer_notification').setDisplayType('inline');
        }

        body += '02 - ' + moment().diff(start) + ' | ';
        //------------- Begin Section 3 -----------------------------------------------------------------------------------
        // Details:	Hides or not field MOP depending on case type and sub type.
        // Created:	2/6/2012
        //-----------------------------------------------------------------------------------------------------------------

        var currentContext = nlapiGetContext();
        var stUserRole = currentContext.getRole();
        if (currentContext.getExecutionContext() == 'userinterface') {

            // if this case is Audit type, hide MOP instructions field
            var stCaseType = nlapiGetFieldValue('category');
            var stSubCaseType = nlapiGetFieldValue('custevent_cologix_sub_case_type');
            if (stSubCaseType != 37 && stSubCaseType != 38 && stSubCaseType != 39 && stSubCaseType != 40) {
                if (nlapiGetField('custevent_clgx_audit_mops_instructions') != null && nlapiGetField('custevent_clgx_audit_mops_instructions') != '') {
                    form.getField('custevent_clgx_audit_mops_instructions').setDisplayType('hidden');
                }
            }

            // hide credit remittance if there is no parent case
            var parent = nlapiGetFieldValue('custevent_clgx_parent_id');
            if (parent == null || parent == '') {
                if (nlapiGetField('custevent_clgx_credit_remittance') != null && nlapiGetField('custevent_clgx_credit_remittance') != '') {
                    form.getField('custevent_clgx_credit_remittance').setDisplayType('hidden');
                }
            }
        }
        //---------- End Section 1 ------------------------------------------------------------------------------------------------
        body += '03 - ' + moment().diff(start) + ' | ';
        //Catalina
        //------------- Begin Section 4 -----------------------------------------------------------------------------------
        //add temporary fields for old time items and new time items
        //-----------------------------------------------------------------------------------------------------------------
        if (type == 'edit') {
            var oldtime = form.addField('custpage_clgx_case_oldtime', 'currency', 'OltTime');
            form.getField('custpage_clgx_case_oldtime').setDisplayType('hidden');
            var newtime = form.addField('custpage_clgx_case_newtime', 'currency', 'NewTime');
            form.getField('custpage_clgx_case_newtime').setDisplayType('hidden');

            var caseid = nlapiGetRecordId();
            var oldcase = nlapiLoadRecord("supportcase", caseid);
            var oldtimes = oldcase.getLineItemCount('timeitem');
            nlapiSetFieldValue("custpage_clgx_case_oldtime", oldtimes);
            var oldtimeDecimal = form.addField('custpage_clgx_case_oldtimedecimal', 'currency', 'OltTimeDecimal');
            form.getField('custpage_clgx_case_oldtimedecimal').setDisplayType('hidden');
            var arrColumns = new Array();
            var arrFilters = new Array();
            var hoursB = 0;
            arrFilters.push(new nlobjSearchFilter("internalid", null, "anyof", caseid));
            var searchRemoteB = nlapiSearchRecord('supportcase', 'customsearch_clgx_savedsearchtimebcs', arrFilters, arrColumns);
            for (var j = 0; searchRemoteB != null && j < searchRemoteB.length; j++) {
                var searchRemB = searchRemoteB[j];
                var columnsB = searchRemB.getAllColumns();
                hoursB = searchRemB.getValue(columnsB[0]);
            }
            var oldStatus = oldcase.getFieldValue('status');
            var oldStatusField = form.addField('custpage_clgx_case_oldstatus', 'text', 'OltStatus');
            form.getField('custpage_clgx_case_oldstatus').setDisplayType('hidden');
            nlapiSetFieldValue("custpage_clgx_case_oldstatus", oldStatus);
            nlapiSetFieldValue("custpage_clgx_case_oldtimedecimal", hoursB);
            //add hidden field for allow save
            var allowSave = form.addField('custpage_clgx_case_allowsave', 'integer', 'AllowSave');
            form.getField('custpage_clgx_case_allowsave').setDisplayType('hidden');
            nlapiSetFieldValue("custpage_clgx_case_allowsave", 1);
        }
        //---------- End Section 4 ------------------------------------------------------------------------------------------------
        body += '04 - ' + moment().diff(start) + ' | ';
        var record = nlapiCreateRecord('customrecord_clgx_script_exec_time');
        record.setFieldValue('custrecord_clgx_script_exec_time_user', nlapiGetUser());
        record.setFieldValue('custrecord_clgx_script_exec_time_rec', "case");
        record.setFieldValue('custrecord_clgx_script_exec_time_id', nlapiGetRecordId());
        record.setFieldValue('custrecord_clgx_script_exec_time_context', 1);
        record.setFieldValue('custrecord_clgx_script_exec_time', moment().diff(start));
        record.setFieldValue('custrecord_clgx_script_exec_time_details', body);
        try {
            nlapiSubmitRecord(record, false, true);
        }
        catch (error) {
        }
        nlapiLogExecution('DEBUG', 'User Event - Befor Load', '|-------------FINISHED--------------|');

        // nlapiSendEmail(71418,71418,'Case beforeLoad Processing Time - ' + caseid, body,null,null,null,null);

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined) {
            nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else {
            nlapiLogExecution('ERROR', 'Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

    nlapiLogExecution("DEBUG", "CLGX beforeLoad - Case #2892137", nlapiGetFieldValue("status"));
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function beforeSubmit(type) {
    nlapiLogExecution("DEBUG", "CLGX beforeSubmit - Case #2892137", nlapiGetFieldValue("status"));

    try {
        var start = moment();
        var body = '';
        var caseid = nlapiGetRecordId();
        nlapiLogExecution('DEBUG', 'User Event - Before Submit', '|-------------STARTED--------------|');

        if (type == 'edit') {
            //Catalina
            var oldTime = nlapiGetFieldValue("custpage_clgx_case_oldtime");
            var newTime = nlapiGetLineItemCount('timeitem');
            var oldtimeDecimal = nlapiGetFieldValue('custpage_clgx_case_oldtimedecimal');
            var arrColumns = new Array();
            var arrFilters = new Array();
            var hoursB = 0;


            for (var t = 1; t <= newTime; t++) {
                var hours = nlapiGetLineItemValue('timeitem', 'hours', t);
                var item = nlapiGetLineItemValue('timeitem', 'item', t);
                if ((item == 544) || (item == 545)) {
                    var hourssplit = hours.split(':');
                    hourssplit[1] = hourssplit[1] / 60;
                    var hoursDec = parseFloat(hourssplit[0]) + parseFloat(hourssplit[1]);
                    hoursB = parseFloat(hoursB) + parseFloat(hoursDec);
                }
            }
            oldtimeDecimal = parseFloat(oldtimeDecimal);
            oldtimeDecimal = oldtimeDecimal.toFixed(2);
            hoursB = hoursB.toFixed(2);
            if ((caseid != null) && (caseid != '')) {
                var searchColumn = new Array();
                var searchFilter = new Array();
                searchColumn.push(new nlobjSearchColumn('custcol_clgx_rh_invoice_item_case', null, 'GROUP'));
                searchFilter.push(new nlobjSearchFilter('custcol_clgx_rh_invoice_item_case', null, 'anyof', caseid));
                var searchInvoices = nlapiSearchRecord('invoice', null, searchFilter, searchColumn);
                if (searchInvoices != null) {
                    if ((oldTime != null) && (oldTime != '')) {
                        if (oldTime != newTime) {
                            nlapiSetFieldValue("custpage_clgx_case_allowsave", 0);
                        }

                    }

                    if (oldtimeDecimal != hoursB) {
                        nlapiSetFieldValue("custpage_clgx_case_allowsave", 0);


                    }


                }
            }
            //Catalina END RH

            body += '01 - ' + moment().diff(start) + ' | ';

            nlapiLogExecution('DEBUG', 'User Event - Befor Submit', '|-------------FINISHED--------------|');
        }
        //SECTION5 UPDATE Resolved date Field
        var newStatus = nlapiGetFieldValue("status");
        var resolveddate = nlapiGetFieldValue("custevent_clgx_date_resolved");
        if (newStatus == 7) {
            if (resolveddate == '' || resolveddate == null) {
                var searchColumn = new Array();
                var searchFilter = new Array();
                searchFilter.push(new nlobjSearchFilter('internalid', null, 'anyof', caseid));
                var searchCSs = nlapiSearchRecord('supportcase', 'customsearch_clgx_ss_cscraetedate', searchFilter, searchColumn);
                var searchCS = searchCSs[0];
                var columns = searchCS.getAllColumns();
                var createddate = searchCS.getValue(columns[0]);
                var newdate = createddate.replace('-', ':');
                // nlapiSendEmail(206211, 206211,'date','body:'+newdate,null,null,null,null); // Send email to Catalina
                var now = moment();
                var mid = moment(newdate);
                //nlapiSendEmail(206211, 206211,'date',mid,null,null,null,null); // Send email to Catalina

                var diffMinutes = now.diff(mid, 'minutes');
                nlapiSetFieldValue("custevent_clgx_date_resolved", diffMinutes);
                nlapiSetFieldValue("custevent_clgx_case", moment().format('MM/DD/YYYY'));

                //  nlapiSendEmail(206211, 206211,'date',moment().format('MM/DD/YYYY h:mm:ss a'),null,null,null,null); // Send email to Catalina

            }
        }
        body += '02 - ' + moment().diff(start) + ' | ';
        var record = nlapiCreateRecord('customrecord_clgx_script_exec_time');
        record.setFieldValue('custrecord_clgx_script_exec_time_user', nlapiGetUser());
        record.setFieldValue('custrecord_clgx_script_exec_time_rec', "case");
        record.setFieldValue('custrecord_clgx_script_exec_time_id', nlapiGetRecordId());
        record.setFieldValue('custrecord_clgx_script_exec_time_context', 2);
        record.setFieldValue('custrecord_clgx_script_exec_time', moment().diff(start));
        record.setFieldValue('custrecord_clgx_script_exec_time_details', body);
        //nlapiSubmitRecord(record, false,true);
        try {
            nlapiSubmitRecord(record, false, true);
        }
        catch (error) {
        }
        // nlapiSendEmail(71418,71418,'Case beforeSubmit Processing Time - ' + caseid, body,null,null,null,null);

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined) {
            nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else {
            nlapiLogExecution('ERROR', 'Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

    nlapiLogExecution("DEBUG", "CLGX beforeSubmit - Case #2892137", nlapiGetFieldValue("status"));
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function afterSubmit(type) {
    nlapiLogExecution("DEBUG", "CLGX afterSubmit - Case #2892137", nlapiGetFieldValue("status"));

    try {
        var start = moment();
        var body = '';
        var caseid = nlapiGetRecordId();
        var userid = nlapiGetUser();
        var subcasetype = nlapiGetFieldValue('custevent_cologix_sub_case_type');
        if (type == 'create' && subcasetype == 112) {
            var arrColumns1 = new Array();
            var arrFilters1 = new Array();
            arrFilters1.push(new nlobjSearchFilter("internalid", null, "anyof", caseid));

            var searchCS = nlapiSearchRecord('supportcase', 'customsearch_clgx_ss_cases_neo_exclude', arrFilters1, arrColumns1);
            if (searchCS != null) {
                var customer = nlapiGetFieldValue('company');
                var record = nlapiCreateRecord('customrecord_clgx_queue_cases_neo_15min');
                record.setFieldValue('custrecord_clgx_cases_neo_15min_cust', customer);
                record.setFieldValue('custrecord_clgx_queue_neo_15min_cas', caseid);
                var momcr = moment().format('MM/DD/YYYY h:mm:ss a');
                record.setFieldValue('custrecord_clgx_queue_neo_15min_cr', momcr);
                record.setFieldValue('custrecord_clgx_queue_neo_15min_1h', momcr);
                record.setFieldValue('custrecord_clgx_queue_neo_15min_2h', momcr);
                record.setFieldValue('custrecord_clgx_queue_neo_15min_4h', momcr);
                var idRec = nlapiSubmitRecord(record, false, true);
                var caseRecord = nlapiLoadRecord('supportcase', caseid);
                if (customer == 137226) {
                    var contactNeo = 990299;//JAX1
                    caseRecord.setFieldValue('email', 'sciencelogic-alarms@amazon.com,colo-service-alerts@amazon.com');
                }
                else if (customer == 791328) {
                    var contactNeo = 990300; //MIN3
                    caseRecord.setFieldValue('email', 'sciencelogic-alarms@amazon.com,colo-service-alerts@amazon.com');
                }
                else if (customer == 238400) {
                    //var contactNeo = 990294; //MTL3
                    var contactNeo = 0; //MTL3
                    caseRecord.setFieldValue('email', 'sciencelogic-alarms@amazon.com,yul-dceo@amazon.com,foc-ops-edge@amazon.com');

                }
                else if (customer == 922786) {
                    //var contactNeo = 990294; //VAN2
                    var contactNeo = 0; //VAN2
                    caseRecord.setFieldValue('email', 'sciencelogic-alarms@amazon.com,colo-service-alerts@amazon.com');

                }
                else if (customer == 922783) {
                    //var contactNeo = 990294; //COL2
                    var contactNeo = 0; //COL2
                    caseRecord.setFieldValue('email', 'sciencelogic-alarms@amazon.com,foc-ops-edge@amazon.com');

                }
                else if (customer == 1825070) {
                    //var contactNeo = 990294; //COL3
                    var contactNeo = 0; //COL3
                    caseRecord.setFieldValue('email', 'sciencelogic-alarms@amazon.com,colo-service-alerts@amazon.com');

                }

                else if (customer == 946880) {
                    var contactNeo = 990295; //MTL4
                    caseRecord.setFieldValue('email', 'sciencelogic-alarms@amazon.com,colo-service-alerts@amazon.com');
                }
                else if (customer == 586663) {
                    var contactNeo = 990296; //TOR2
                    caseRecord.setFieldValue('email', 'sciencelogic-alarms@amazon.com,colo-service-alerts@amazon.com');
                }

                else if (customer == 789277) {
                    // var contactNeo = 990297; //MTL1
                    //var contactNeo = 990294; //MTL1
                    var contactNeo = 0; //MTL3
                    caseRecord.setFieldValue('email', 'sciencelogic-alarms@amazon.com,yul-dceo@amazon.com,foc-ops-edge@amazon.com');
                }
                // nlapiLogExecution('DEBUG','Contact', contactNeo);

                /* if(contactNeo!=0) {
                     caseRecord.setFieldValue('contact', contactNeo);
                 }*/
                //caseRecord.setFieldValue('email','catalina.taran@cologix.com');
                nlapiSubmitRecord(caseRecord, false, true);
            }
            if (searchCS == null) {
                nlapiSubmitField('supportcase', caseid, 'status', 5);


            }

        }
        body += '01 - ' + moment().diff(start) + ' | ';
        var currentContext = nlapiGetContext();
        if (currentContext.getExecutionContext() == 'csvimport') { // csvimport / userinterface

            var credit = nlapiGetFieldValue('custevent_clgx_credit_remittance');
            if (parseInt(credit) > 0) {

                // Search cases with this parent
                var searchColumns = new Array();
                searchColumns.push(new nlobjSearchColumn('internalid', null, null));
                var searchFilter = new Array();
                searchFilter.push(new nlobjSearchFilter('custevent_clgx_parent_id', null, 'anyof', caseid));
                var searchCases = nlapiSearchRecord('supportcase', null, searchFilter, searchColumns);

                if (searchCases == null) { // there is no grandchild case, so create one

                    var customer = nlapiGetFieldValue('company');

                    var searchColumns = new Array();
                    searchColumns.push(new nlobjSearchColumn('internalid', 'contact', 'GROUP'));
                    searchColumns.push(new nlobjSearchColumn('email', 'contact', 'GROUP').setSort(false));
                    var searchFilter = new Array();
                    searchFilter.push(new nlobjSearchFilter('internalid', null, 'anyof', customer));
                    searchFilter.push(new nlobjSearchFilter('role', 'contact', 'anyof', 1));
                    searchFilter.push(new nlobjSearchFilter('email', 'contact', 'isnot', '@NONE@'));
                    searchFilter.push(new nlobjSearchFilter('isinactive', 'contact', 'is', 'F'));
                    var searchBillingContacts = nlapiSearchRecord('customer', null, searchFilter, searchColumns);

                    var arrEmail = new Array();
                    var contact = 0;
                    if (searchBillingContacts != null) {
                        contact = searchBillingContacts[0].getValue('internalid', 'contact', 'GROUP'); // take only the first billing contact as a case contact
                        for (var j = 0; j < searchBillingContacts.length; j++) {
                            var email = searchBillingContacts[j].getValue('email', 'contact', 'GROUP');
                            if (!inArray(email, arrEmail)) {
                                arrEmail.push(email);
                            }
                        }
                    }
                    var listEmails = '';
                    if (arrEmail != null) {
                        listEmails = arrEmail.join();
                    }

                    var casenumber = nlapiGetFieldValue('casenumber');
                    var customer = nlapiGetFieldValue('company');
                    var subsidiary = nlapiGetFieldValue('subsidiary');
                    var facility = nlapiGetFieldValue('custevent_cologix_facility');
                    var profile = nlapiGetFieldValue('profile');
                    var casetype = nlapiGetFieldValue('category');
                    var subcasetype = nlapiGetFieldValue('custevent_cologix_sub_case_type');
                    var status = nlapiGetFieldValue('status');
                    var profile = nlapiGetFieldValue('profile');
                    var followup = nlapiGetFieldValue('custevent_cologix_case_sched_followup');
                    var starttime = nlapiGetFieldValue('custevent_cologix_sched_start_time');
                    var equipment = nlapiGetFieldValue('custevent_cologix_equipment');

                    var record = nlapiCreateRecord('supportcase');
                    record.setFieldValue('title', 'Credit Memo of ' + casenumber);
                    record.setFieldValue('company', customer);

                    record.setFieldValue('subsidiary', subsidiary);
                    record.setFieldValue('custevent_cologix_facility', facility);
                    //record.setFieldValue('profile', profile);
                    record.setFieldValue('assigned', 4260); // Billing@Cologix
                    if (searchBillingContacts != null) {
                        record.setFieldValue('contact', contact); // billing contact
                        record.setFieldValue('email', listEmails); // emails of all billing contacts
                    }
                    record.setFieldText('status', 'In Progress');
                    record.setFieldText('priority', 'Medium');
                    record.setFieldText('category', 'Finance Helpdesk');
                    record.setFieldText('custevent_cologix_sub_case_type', 'Dispute');

                    record.setFieldValue('custevent_cologix_case_sched_followup', followup);
                    record.setFieldValue('custevent_cologix_sched_start_time', starttime);
                    record.setFieldValue('custevent_cologix_equipment', equipment);

                    record.setFieldValue('custevent_clgx_parent_id', caseid);
                    record.setFieldValue('custevent_clgx_credit_remittance', parseInt(credit));
                    record.setFieldValue('origin', 6);

                    var idRec = nlapiSubmitRecord(record, false, true);
                    contact = 0;
                    listEmails = '';
                }
            }
        }
        body += '02 - ' + moment().diff(start) + ' | ';
        if (currentContext.getExecutionContext() == 'userinterface') {

            //------------- Begin Section 1 -------------------------------------------------------------------
            // Details:	If case is parent and have message, send the message to any child case if any - if not, ceated them.
            // Created:	8/16/2012
            //-------------------------------------------------------------------------------------------------

            if (type == 'create' || type == 'edit' || type == 'view') { // added type view
                //// inserted the locked record here
                var locked = nlapiGetFieldValue('custevent_clgx_locked_case');
                nlapiLogExecution('DEBUG', 'before log locked', locked);
                if (locked == 'T') {
                    var arrParam = new Array();
                    arrParam['custscript_internal_message'] = 'The sub cases for this parent case are still being created or updated. Please wait until you have received the confirmation email.';

                    if (nlapiGetUser() != 1349020) {
                        nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
                    }
                }
                //// inserted the locked record here
                var casetype = nlapiGetFieldText('category');
                var equipment = nlapiGetFieldValue('custevent_cologix_equipment');
                var notify = nlapiGetFieldValue('custevent_clgx_customer_notification');
                var status = nlapiGetFieldText('status');
                var company = nlapiGetFieldText('company');
                var majorMinor = nlapiGetFieldValue("custevent_clgx_custcasefield_majorminor");

                var sendto = nlapiGetFieldValue('emailform');
                var message = nlapiGetFieldValue('outgoingmessage');
                var origin = nlapiGetFieldText('origin');

                var createCases = 0;
                if (company.indexOf('Cologix') > -1) {
                    createCases = 1;
                }

                // Search cases with this parent
                var searchColumns = new Array();
                searchColumns.push(new nlobjSearchColumn('internalid', null, null));
                var searchFilter = new Array();
                searchFilter.push(new nlobjSearchFilter('custevent_clgx_parent_id', null, 'anyof', caseid));
                var searchCases = nlapiSearchRecord('supportcase', null, searchFilter, searchColumns);

                // if no child cases and parent case from Command
                if (searchCases == null && createCases == 1 && origin == 'Command') {
                    if ((casetype == 'Maintenance' && majorMinor != 3 && status == 'CAB Approved' && notify == 'T')) {
                        var arrParam = new Array();
                        arrParam['custscript_cmd_mops_parent_userid'] = userid;
                        arrParam['custscript_cmd_mops_parent_caseid'] = caseid;

                        nlapiLogExecution("DEBUG", "CLGX_SU_Case - afterSubmit - Line #517", "customscript_clgx_ss_cmd_mops_subcases");
                        nlapiSubmitField('supportcase', caseid, 'custevent_clgx_locked_case', 'T');
                        var locked = nlapiGetFieldValue('custevent_clgx_locked_case');
                        nlapiLogExecution('DEBUG', 'After Lock 1 locked', locked);
                        var status = nlapiScheduleScript('customscript_clgx_ss_cmd_mops_subcases', null, arrParam);

                    }
                }

                // if no child cases and FAM is configured and company is Cologix
                if ((equipment != null || equipment != '') && searchCases == null && createCases == 1 && origin != 'Command') {
                    if ((casetype == 'Maintenance' && status == 'CAB Approved' && notify == 'T') || (casetype == 'Incident' && notify == 'T')) {
                        var arrParam = new Array();
                        arrParam['custscript_ss_case_userid'] = userid;
                        arrParam['custscript_ss_case_caseid'] = caseid;
                        arrParam['custscript_ss_case_sendto'] = sendto;
                        arrParam['custscript_ss_case_message'] = message;

                        nlapiLogExecution("DEBUG", "CLGX_SU_Case - afterSubmit - Line #532", "customscript_clgx_ss_case");
                        nlapiSubmitField('supportcase', caseid, 'custevent_clgx_locked_case', 'T');
                        var locked = nlapiGetFieldValue('custevent_clgx_locked_case');
                        nlapiLogExecution('DEBUG', 'After Lock 2 locked', locked);
                        var status = nlapiScheduleScript('customscript_clgx_ss_case', null, arrParam);
                        
                    }
                }

                // if child cases exist and type maintenance or Incident, send same message to child cases
                if (searchCases != null && (casetype == 'Maintenance' || casetype == 'Incident') && notify == 'T') {
                    var arrParam = new Array();
                    arrParam['custscript_ss_case_message_userid'] = userid;
                    arrParam['custscript_ss_case_message_caseid'] = caseid;
                    arrParam['custscript_ss_case_message_sendto'] = 'F';
                    arrParam['custscript_ss_case_message_message'] = '';
                    arrParam['custscript_ss_case_message_messageid'] = 0;

                   
                    nlapiLogExecution("DEBUG", "CLGX_SU_Case - afterSubmit - Line #547", "customscript_clgx_ss_case_message");
                    /** UNCOMMENT THIS FOR LATER */
                    //var status = nlapiScheduleScript('customscript_clgx_ss_case_message', null ,arrParam);
                    // Send Spill Spill Over Email moved the support case to lock first before sending the emails-----------
                    nlapiSubmitField('supportcase', caseid, 'custevent_clgx_locked_case', 'T');
                    var locked = nlapiGetFieldValue('custevent_clgx_locked_case');
                    nlapiLogExecution('DEBUG', 'After Lock 3 locked', locked);
                    var status = nlapiScheduleScript('customscript_clgx_ss_case_message', null, arrParam);
                    nlapiLogExecution("DEBUG", "status", status);
                  
                    //---------------------------------------
                    //nlapiSubmitField('supportcase', caseid, 'custevent_clgx_locked_case', 'T');
                    /** ---------------------------------------------------------------------------------- */
                }

                // verify if credit remittance and if yes and no child sub cases, create a Credit remittance case
                var credit = nlapiGetFieldValue('custevent_clgx_credit_remittance');
                if(/*casetype == 'Incident' &&*/parseInt(credit) > 0 && searchCases == null){
                    //if(searchCases == null){
                    var casenumber = nlapiGetFieldValue('casenumber');
                    var customer = nlapiGetFieldValue('company');
                    var subsidiary = nlapiGetFieldValue('subsidiary');
                    var facility = nlapiGetFieldValue('custevent_cologix_facility');
                    var casetype = nlapiGetFieldValue('category');
                    var subcasetype = nlapiGetFieldValue('custevent_cologix_sub_case_type');
                    var status = nlapiGetFieldValue('status');
                    var equipment = nlapiGetFieldValue('custevent_cologix_equipment');
                    var notify = nlapiGetFieldValue('custevent_clgx_customer_notification');
                    var profile = nlapiGetFieldValue('profile');
                    var followup = nlapiGetFieldValue('custevent_cologix_case_sched_followup');
                    var starttime = nlapiGetFieldValue('custevent_cologix_sched_start_time');

                    var searchColumns = new Array();
                    searchColumns.push(new nlobjSearchColumn('internalid', 'contact', 'GROUP'));
                    searchColumns.push(new nlobjSearchColumn('email', 'contact', 'GROUP').setSort(false));
                    var searchFilter = new Array();
                    searchFilter.push(new nlobjSearchFilter('internalid', null, 'anyof', customer));
                    searchFilter.push(new nlobjSearchFilter('role', 'contact', 'anyof', 1));
                    searchFilter.push(new nlobjSearchFilter('email', 'contact', 'isnot', '@NONE@'));
                    searchFilter.push(new nlobjSearchFilter('isinactive', 'contact', 'is', 'F'));
                    var searchBillingContacts = nlapiSearchRecord('customer', null, searchFilter, searchColumns);

                    var arrEmail = new Array();
                    var contact = 0;
                    if (searchBillingContacts != null) {
                        contact = searchBillingContacts[0].getValue('internalid', 'contact', 'GROUP'); // take only the first billing contact as a case contact
                        for (var j = 0; j < searchBillingContacts.length; j++) {
                            var email = searchBillingContacts[j].getValue('email', 'contact', 'GROUP');
                            if (!inArray(email, arrEmail)) {
                                arrEmail.push(email);
                            }
                        }
                    }
                    var listEmails = '';
                    if (arrEmail != null) {
                        listEmails = arrEmail.join();
                    }

                    var record = nlapiCreateRecord('supportcase');
                    record.setFieldValue('title', 'Credit Memo of ' + casenumber);
                    record.setFieldValue('company', customer);

                    record.setFieldValue('subsidiary', subsidiary);
                    record.setFieldValue('custevent_cologix_facility', facility);
                    //record.setFieldValue('profile', profile);
                    record.setFieldValue('assigned', 4260); // Billing@Cologix
                    if (searchBillingContacts != null) {
                        record.setFieldValue('contact', contact); // billing contact
                        record.setFieldValue('email', listEmails); // emails of all billing contacts
                    }
                    record.setFieldText('status', 'In Progress');
                    record.setFieldText('priority', 'Medium');
                    record.setFieldText('category', 'Finance Helpdesk');
                    record.setFieldText('custevent_cologix_sub_case_type', 'Dispute');

                    record.setFieldValue('custevent_cologix_case_sched_followup', followup);
                    record.setFieldValue('custevent_cologix_sched_start_time', starttime);
                    record.setFieldValue('custevent_cologix_equipment', equipment);

                    record.setFieldValue('custevent_clgx_parent_id', caseid);
                    record.setFieldValue('custevent_clgx_credit_remittance',credit ); //parseInt(credit)
                    record.setFieldValue('origin', 6);

                    var idRec = nlapiSubmitRecord(record, false, true);
                    //}
                }
            }
            body += '03 - ' + moment().diff(start) + ' | ';
            //------------- Begin Section 2 -------------------------------------------------------------------
            // Details:	Create Churn Records
            // When a Disco Case with sub-case type = ‘Disconnect – Customer Requested’ is created and if there are no active (At-risk or Expected) Churn record already created for this customer, a new Churn Record should be created and auto-populate the fields.
            // Created:	6/3/2014
            //-------------------------------------------------------------------------------------------------

            var caseSubType = nlapiGetFieldValue('custevent_cologix_sub_case_type');
            var churnType = nlapiGetFieldValue('custevent_clgx_churn_type_case');
            soId = nlapiGetFieldValue('custevent_clgx_so_case');
            var parentCase = nlapiGetFieldValue('custevent_clgx_parent_id');
            var actualNetwork = 0;
            var actualInterconnection = 0;
            var actualPower = 0;
            var actualSpace = 0;
            var actualOther = 0;
            var actualNetwork1 = 0;
            var actualInterconnection1 = 0;
            var actualPower1 = 0;
            var actualSpace1 = 0;
            var actualOther1 = 0;
            var actualNetworkT = 0;
            var actualInterconnectionT = 0;
            var actualPowerT = 0;
            var actualSpaceT = 0;
            var actualOtherT = 0;
            //update the SO in Parent Case Churn Record
            if (((type == 'create') || (type == 'edit')) && (soId != null)) {
                if (parentCase != '') {
                    var arrFil = new Array();
                    var arrCol = new Array();
                    arrFil.push(new nlobjSearchFilter("custrecord_clgx_case_num", null, "anyof", parentCase));

                    var searchChurnstoUp = nlapiSearchRecord('customrecord_cologix_churn', 'customsearch_clgx_search_churncasesparen', arrFil, arrCol);

                    if (searchChurnstoUp != null) {
                        for (var i = 0; searchChurnstoUp != null && i < searchChurnstoUp.length; i++) {
                            var searchChurnUp = searchChurnstoUp[i];
                            var columnsUp = searchChurnUp.getAllColumns();
                            var churnID = searchChurnUp.getValue(columnsUp[0]);
                        }
                        //search for invoices created from soID this month
                        var arrFilInv = new Array();
                        var arrColInv = new Array();
                        arrFilInv.push(new nlobjSearchFilter("createdfrom", null, "anyof", soId));
                        var searchInvCurrM = nlapiSearchRecord('transaction', 'customsearch_clgs_search_invoices_soch', arrFilInv, arrColInv);
                        //search for invoices created from soID next month
                        var arrFilInv1 = new Array();
                        var arrColInv1 = new Array();
                        arrFilInv1.push(new nlobjSearchFilter("createdfrom", null, "anyof", soId));
                        var searchInvCurrNM = nlapiSearchRecord('transaction', 'customsearch_clgs_search_invoices_sochnm', arrFilInv1, arrColInv1);
                        var invoicesIDs = new Array();
                        var invoicesID1s = new Array();
                        if (searchInvCurrM != null) {
                            for (var i = 0; searchInvCurrM != null && i < searchInvCurrM.length; i++) {
                                var searchInvoiceCurrM = searchInvCurrM[i];
                                var columnsM = searchInvoiceCurrM.getAllColumns();
                                var invoiceidM = searchInvoiceCurrM.getValue(columnsM[0]);
                                invoicesIDs[i] = invoiceidM;
                            }
                            if (invoicesIDs.length > 0) {
                                var arrFInvoice = new Array();
                                var arrColInvoice = new Array();
                                arrFInvoice[0] = new nlobjSearchFilter('internalid', null, 'anyof', invoicesIDs);
                                var searchInvoices = nlapiSearchRecord('transaction', 'customsearch_clgx_invoicesmrrs', arrFInvoice, arrColInvoice);
                                if (searchInvoices != null) {
                                    for (var z = 0; searchInvoices != null && z < searchInvoices.length; z++) {
                                        var searchInvoiceActualMRRs = searchInvoices[z];
                                        var columns = searchInvoiceActualMRRs.getAllColumns();
                                        actualNetwork = parseFloat(actualNetwork) + parseFloat(searchInvoiceActualMRRs.getValue(columns[0]));
                                        actualInterconnection = parseFloat(actualInterconnection) + parseFloat(searchInvoiceActualMRRs.getValue(columns[1]));
                                        actualSpace = parseFloat(actualSpace) + parseFloat(searchInvoiceActualMRRs.getValue(columns[2]));
                                        actualPower = parseFloat(actualPower) + parseFloat(searchInvoiceActualMRRs.getValue(columns[3]));
                                        actualOther = parseFloat(searchInvoiceActualMRRs.getValue(columns[4])) + parseFloat(searchInvoiceActualMRRs.getValue(columns[5])) + parseFloat(searchInvoiceActualMRRs.getValue(columns[6]));
                                        //      var loc=searchInvoiceActualMRRs.getValue(columns[14]);
                                    }
                                }
                            }


                        }
                        if (searchInvCurrNM != null) {
                            for (var k = 0; searchInvCurrNM != null && k < searchInvCurrNM.length; k++) {
                                var searchInvoiceCurrNM = searchInvCurrNM[k];
                                var columnsNM = searchInvoiceCurrNM.getAllColumns();
                                var invoiceidNM = searchInvoiceCurrNM.getValue(columnsNM[0]);
                                invoicesID1s[k] = invoiceidNM;
                            }
                            if (invoicesID1s.length > 0) {
                                var arrFInvoice1 = new Array();
                                var arrColInvoice1 = new Array();
                                arrFInvoice1[0] = new nlobjSearchFilter('internalid', null, 'anyof', invoicesID1s);
                                var searchInvoices1 = nlapiSearchRecord('transaction', 'customsearch_clgx_invoicesmrrs', arrFInvoice1, arrColInvoice1);
                                if (searchInvoices1 != null) {
                                    for (var z = 0; searchInvoices1 != null && z < searchInvoices1.length; z++) {
                                        var searchInvoiceActualMRRs = searchInvoices[z];
                                        var columns = searchInvoiceActualMRRs.getAllColumns();
                                        actualNetwork1 = parseFloat(actualNetwork1) + parseFloat(searchInvoiceActualMRRs.getValue(columns[0]));
                                        actualInterconnection1 = parseFloat(actualInterconnection1) + parseFloat(searchInvoiceActualMRRs.getValue(columns[1]));
                                        actualSpace1 = parseFloat(actualSpace1) + parseFloat(searchInvoiceActualMRRs.getValue(columns[2]));
                                        actualPower1 = parseFloat(actualPower1) + parseFloat(searchInvoiceActualMRRs.getValue(columns[3]));
                                        actualOther1 = parseFloat(actualOther1) + parseFloat(searchInvoiceActualMRRs.getValue(columns[4])) + parseFloat(searchInvoiceActualMRRs.getValue(columns[5])) + parseFloat(searchInvoiceActualMRRs.getValue(columns[6]));
                                        //      var loc=searchInvoiceActualMRRs.getValue(columns[14]);
                                    }
                                }
                            }
                        }
                        var actualNetworkT = actualNetwork1 - actualNetwork;
                        var actualInterconnectionT = actualInterconnection1 - actualInterconnection;
                        var actualPowerT = actualPower1 - actualPower;
                        var actualSpaceT = actualSpace1 - actualSpace;
                        var actualOtherT = actualOther1 - actualOther;
                        var churnToUpdate = nlapiLoadRecord("customrecord_cologix_churn", churnID);
                        var oldSOs = churnToUpdate.getFieldValues('custrecord_clgx_churn_so');
                        var newSOs = new Array();
                        if (oldSOs != null) {
                            var lenghtSos = oldSOs.length;
                            for (var t = 0; t < lenghtSos; t++) {
                                newSOs[t] = oldSOs[t];
                            }
                            var lenghtNSos = newSOs.length;
                            newSOs[lenghtNSos] = float2int(soId);
                            churnToUpdate.setFieldValues('custrecord_clgx_churn_so', newSOs);
                        }
                        else {
                            churnToUpdate.setFieldValue('custrecord_clgx_churn_so', soId);

                        }
                        var oldPower = churnToUpdate.getFieldValue('custrecord_clgx_churn_act_power');
                        var oldSpace = churnToUpdate.getFieldValue('custrecord_clgx_churn_act_space');
                        var oldNetwork = churnToUpdate.getFieldValue('custrecord_clgx_churn_act_network');
                        var oldInter = churnToUpdate.getFieldValue('custrecord_clgx_churn_act_xconn');
                        var oldOther = churnToUpdate.getFieldValue('custrecord_clgx_churn_act_other');
                        if (actualPowerT < 0) {
                            actualPowerT = actualPowerT * (-1);

                        }
                        if (actualOtherT < 0) {
                            actualOtherT = actualOtherT * (-1);

                        }
                        if (actualSpaceT < 0) {

                            actualSpaceT = actualSpaceT * (-1);

                        }
                        if (actualNetworkT < 0) {

                            actualNetworkT = actualNetworkT * (-1);
                        }
                        if (actualInterconnectionT < 0) {

                            actualInterconnectionT = actualInterconnectionT * (-1);

                        }
                        if (oldPower != null) {
                            actualPowerT = actualPowerT + oldPower;
                        }
                        if (oldSpace != null) {
                            actualSpaceT = actualSpaceT + oldSpace;
                        }
                        if (oldInter != null) {
                            actualInterconnectionT = actualInterconnectionT + oldInter;
                        }
                        if (oldNetwork != null) {
                            actualNetworkT = actualNetworkT + oldNetwork;
                        }
                        if (oldOther != null) {
                            actualOtherT = actualOtherT + oldOther;
                        }

                        churnToUpdate.setFieldValue('custrecord_clgx_churn_act_power', actualPowerT);
                        churnToUpdate.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnectionT);
                        churnToUpdate.setFieldValue('custrecord_clgx_churn_act_space', actualSpaceT);
                        churnToUpdate.setFieldValue('custrecord_clgx_churn_act_network', actualNetworkT);
                        churnToUpdate.setFieldValue('custrecord_clgx_churn_act_other', actualOtherT);
                        if (churnType != '') {
                            churnToUpdate.setFieldValue('custrecord_clgx_churn_type_churn', churnType);
                        }




                        nlapiSubmitRecord(churnToUpdate, false, true);


                    }

                }

            }
            body += '04 - ' + moment().diff(start) + ' | ';
            if (caseSubType == '50') {
                // var statustask=nlapiGetFieldValue('status');
                var company = null;
                var currNetwork = 0;
                var currInterconnection = 0;
                var churnst = nlapiGetFieldValue('custevent_clgx_case_churnst');
                var statusCase = nlapiGetFieldValue('status');
                var currPower = 0;
                var currSpace = 0;
                var currOther = 0;
                var exInter = nlapiGetFieldValue('custevent_clgx_case_churnintert');
                var exSpace = nlapiGetFieldValue('custevent_clgx_case_churnsp');
                var exNetwork = nlapiGetFieldValue('custevent_clgx_case_churnnet');
                var exPower = nlapiGetFieldValue('custevent_clgx_case_churnpow');
                var exOther = nlapiGetFieldValue('custevent_clgx_case_churnother');
                var fac = 1;
                var caseExist = 0;
                if ((nlapiGetFieldValue('company') != '')) {
                    var company = nlapiGetFieldValue('company');
                }

                var facility = nlapiGetFieldValue('custevent_cologix_facility');
                switch (facility) {
                    case '4':
                        //MTL3
                        fac = 9;
                        break;
                    case '23':
                        //TOR3
                        fac = 13;
                        break;
                    case '8':
                        //TOR1
                        fac = 6;
                        break;
                    case '15':
                        //TOR2
                        fac = 15;
                        break;
                    case '14':
                        //VAN1
                        fac = 28;
                        break;
                    case '20':
                        //VAN2
                        fac = 7;
                        break;
                    case '19':
                        //MTL7
                        fac = 27;
                        break;
                    case '7':
                        //MTL6
                        fac = 12;
                        break;
                    case '6':
                        //MTL5
                        fac = 11;
                        break;
                    case '9':
                        //MTL4
                        fac = 10;
                        break;
                    case '5':
                        //MTL2
                        fac = 8;
                        break;
                    case '2':
                        //MTL1
                        fac = 5;
                        break;
                    case '25':
                        //MIN3
                        fac = 35;
                        break;
                    case '17':
                        //MIN1&2
                        fac = 16;
                        break;
                    case '21':
                        //JAX
                        fac = 31;
                        break;
                    case '27':
                        //JAX2
                        fac = 40;
                        break;
                    case '18':
                        //DAl2
                        fac = 17;
                        break;
                    case '3':
                        //DAL1
                        fac = 2;
                        break;
                    case '16':
                        //COL HQ
                        fac = 1;
                        break;
                    case '24':
                        //COL1&2
                        fac = 34;
                        break;
                    case '28':
                        //LAK
                        fac = 42;
                        break;
                    default:
                        fac = 1;
                }
                var duedate = nlapiGetFieldValue('custevent_clgx_exp_churn_dte');
                var casenum = caseid;
                var soId = nlapiGetFieldValue('custevent_clgx_so_case');
                var invoicesIDCs = new Array();
                if (company != null) {

                    if (((type == 'create') || (type == 'edit')) && (statusCase == 2)) {
                        var customer = nlapiLoadRecord('customer', company);
                        var salesrep = null;
                        var salesrepId = null;
                        if (customer.getFieldText('salesrep').length != 0) {
                            var salesrep = customer.getFieldText('salesrep');
                            var salesrepId = customer.getFieldValue('salesrep');
                        }
                        var emp = nlapiLoadRecord('employee', salesrepId);
                        var isinac = emp.getFieldValue('isinactive');
                        if (isinac == 'T') {
                            var salesrepId = '';
                        }
                        if (company != null) {
                            var arrFilters = new Array();
                            arrFilters.push(new nlobjSearchFilter("name", null, "anyof", company));
                            var arrColumns = new Array();
                            var searchChurns = nlapiSearchRecord('invoice', 'customsearch_clg_invchurn_2', arrFilters, arrColumns);
                            for (var i = 0; searchChurns != null && i < searchChurns.length; i++) {
                                var searchChurn = searchChurns[i];
                                var columns = searchChurn.getAllColumns();
                                var invoiceid = searchChurn.getValue(columns[0]);

                                invoicesIDCs[i] = invoiceid;
                            }
                            if (invoicesIDCs.length > 0) {
                                var arrFInvoiceC = new Array();
                                var arrColInvoiceC = new Array();
                                arrFInvoiceC[0] = new nlobjSearchFilter('internalid', null, 'anyof', invoicesIDCs);
                                var searchInvoicesC = nlapiSearchRecord('transaction', 'customsearch_clgx_invoicesmrrs', arrFInvoiceC, arrColInvoiceC);
                                if (searchInvoicesC != null) {
                                    for (var z = 0; searchInvoicesC != null && z < searchInvoicesC.length; z++) {
                                        var searchInvoiceActualMRRs = searchInvoicesC[z];
                                        var columns = searchInvoiceActualMRRs.getAllColumns();
                                        currNetwork = parseFloat(currNetwork) + parseFloat(searchInvoiceActualMRRs.getValue(columns[0]));
                                        currInterconnection = parseFloat(currInterconnection) + parseFloat(searchInvoiceActualMRRs.getValue(columns[1]));
                                        currPower = parseFloat(currPower) + parseFloat(searchInvoiceActualMRRs.getValue(columns[3]));
                                        currSpace = parseFloat(currSpace) + parseFloat(searchInvoiceActualMRRs.getValue(columns[2]));
                                        currOther = parseFloat(currOther) + parseFloat(searchInvoiceActualMRRs.getValue(columns[4])) + parseFloat(searchInvoiceActualMRRs.getValue(columns[5])) + parseFloat(searchInvoiceActualMRRs.getValue(columns[6]));
                                        //      var loc=searchInvoiceActualMRRs.getValue(columns[14]);
                                    }
                                }
                            }


                        }
                        var stsactive = ["1", "2", "3"];
                        var wasCreated = 0;
                        var arrColumns = new Array();
                        var arrLocation = new Array();
                        var arrLoc = new Array();

                        arrColumns.push(new nlobjSearchColumn('internalid', null, null));
                        //check if already exist a churn for the case customer and case market
                        //JAX
                        arrLocation[0] = ['31', '40'];
                        //COL
                        arrLocation[1] = ['34', '38'];
                        //DAL
                        arrLocation[2] = ['2', '17'];
                        //TOR
                        arrLocation[3] = ['6', '13', '15'];
                        //MTL
                        arrLocation[4] = ['5', '8', '9', '10', '11', '12', '27'];
                        //VAN
                        arrLocation[5] = ['28', '7'];
                        //Min
                        arrLocation[6] = ['16', '35'];
                        //LAK
                        arrLocation[7] = ['42'];
                        for (var j = 0; j < arrLocation.length; j++) {
                            if (inArray(fac, arrLocation[j])) {
                                arrLoc = arrLocation[j].slice(0)

                            }

                        }

                        //edit the case and update the churn record

                        if (type == 'edit') {
                            var customer = nlapiLoadRecord('customer', company);
                            var salesrep = null;
                            var salesrepId = null;
                            if (customer.getFieldText('salesrep').length != 0) {
                                var salesrep = customer.getFieldText('salesrep');
                                var salesrepId = customer.getFieldValue('salesrep');
                            }
                            var emp = nlapiLoadRecord('employee', salesrepId);
                            var isinac = emp.getFieldValue('isinactive');
                            if (isinac == 'T') {
                                var salesrepId = '';
                            }
                            var arrColumns = new Array();
                            arrColumns.push(new nlobjSearchColumn('internalid', null, null));

                            var arrFilters = new Array();
                            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_case_num", null, "anyof", casenum));
                            var searchChurns = nlapiSearchRecord('customrecord_cologix_churn', null, arrFilters, arrColumns);
                            for (var i = 0; searchChurns != null && i < searchChurns.length; i++) {
                                var searchChurn = searchChurns[i];
                                var internalid = searchChurn.getValue('internalid');
                                var churn = nlapiLoadRecord('customrecord_cologix_churn', internalid);
                                churn.setFieldValue('custrecord_clgx_churn_customer', company);
                                churn.setFieldValue('custrecord_clgx_churn_location', fac);
                                if (statusCase != 5) {
                                    churn.setFieldValue('custrecord_clgx_churn_status', churnst);
                                }
                                churn.setFieldValue('custrecord_clgx_churn_expecteddate', duedate);
                                churn.setFieldValue('custrecord_clgx_churn_salesrep', salesrepId);
                                churn.setFieldValue('custrecord_clgx_churn_so', soId);
                                if (exNetwork != '') {
                                    churn.setFieldValue('custrecord_clgx_churn_network', exNetwork);
                                }
                                if (exPower != '') {
                                    churn.setFieldValue('custrecord_clgx_churn_powermrr', exPower);
                                }
                                if (exSpace != '') {
                                    churn.setFieldValue('custrecord_clgx_churn_expspace', exSpace);
                                }
                                if (exInter != '') {
                                    churn.setFieldValue('custrecord_clgx_churn_expcross', exInter);
                                }
                                if (exOther != '') {
                                    churn.setFieldValue('custrecord_clgx_churn_expother', exOther);
                                }
                                churn.setFieldValue('custrecord_clgx_churn_currentnet', currNetwork);
                                churn.setFieldValue('custrecord_clgx_churn_currentint', currInterconnection);
                                churn.setFieldValue('custrecord_clgx_churn_currentpow', currPower);
                                churn.setFieldValue('custrecord_clgx_churn_currentsp', currSpace);
                                churn.setFieldValue('custrecord_clgx_churn_currentother', currOther);
                                if (churnType != '') {
                                    churn.setFieldValue('custrecord_clgx_churn_type_churn', churnType);
                                }
                                nlapiSubmitRecord(churn, false, true);
                            }


                        }
                        //edit the case and update the churn record

                        var arrFilters = new Array();
                        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_churn_customer", null, "anyof", company));
                        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_churn_status", null, "anyof", stsactive));
                        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_churn_location", null, "anyof", fac));
                        //arrFilters.push(new nlobjSearchFilter("custrecord_clgx_case_num",null,"anyof",casenum));
                        var searchChurns = nlapiSearchRecord('customrecord_cologix_churn', null, arrFilters, arrColumns);
                        var arrColumns1 = new Array();
                        arrColumns1.push(new nlobjSearchColumn('internalid', null, null));
                        arrColumns1.push(new nlobjSearchColumn('custrecord_clgx_churn_customer', null, null));
                        arrColumns1.push(new nlobjSearchColumn('custrecord_clgx_churn_status', null, null));
                        arrColumns1.push(new nlobjSearchColumn('custrecord_clgx_churn_expcross', null, null));
                        arrColumns1.push(new nlobjSearchColumn('custrecord_clgx_churn_expspace', null, null));
                        arrColumns1.push(new nlobjSearchColumn('custrecord_clgx_churn_powermrr', null, null));
                        arrColumns1.push(new nlobjSearchColumn('custrecord_clgx_churn_network', null, null));
                        arrColumns1.push(new nlobjSearchColumn('custrecord_clgx_churn_currentother', null, null));


                        var arrFilters1 = new Array();
                        arrFilters1.push(new nlobjSearchFilter("custrecord_clgx_churn_customer", null, "anyof", company));
                        //arrFilters1.push(new nlobjSearchFilter("custrecord_clgx_churn_status",null,"anyof",stsactive));
                        arrFilters1.push(new nlobjSearchFilter("custrecord_clgx_case_num", null, "anyof", casenum));
                        var searchChurns1 = nlapiSearchRecord('customrecord_cologix_churn', null, arrFilters1, arrColumns1);
                        if (searchChurns == null) {
                            if (searchChurns1 == null) {
                                var newRecord = nlapiCreateRecord('customrecord_cologix_churn');
                                newRecord.setFieldValue('custrecord_clgx_churn_customer', company);
                                newRecord.setFieldValue('custrecord_clgx_churn_location', fac);
                                newRecord.setFieldValue('custrecord_clgx_churn_status', churnst);
                                newRecord.setFieldValue('custrecord_clgx_churn_expecteddate', duedate);
                                newRecord.setFieldValue('custrecord_clgx_churn_salesrep', salesrepId);
                                newRecord.setFieldValue('custrecord_clgx_churn_so', soId);
                                if (exNetwork != '') {
                                    newRecord.setFieldValue('custrecord_clgx_churn_network', exNetwork);
                                }
                                if (exPower != '') {
                                    newRecord.setFieldValue('custrecord_clgx_churn_powermrr', exPower);
                                }
                                if (exSpace != '') {
                                    newRecord.setFieldValue('custrecord_clgx_churn_expspace', exSpace);
                                }
                                if (exInter != '') {
                                    newRecord.setFieldValue('custrecord_clgx_churn_expcross', exInter);
                                }
                                if (exOther != '') {
                                    newRecord.setFieldValue('custrecord_clgx_churn_expother', exOther);
                                }
                                newRecord.setFieldValue('custrecord_clgx_churn_currentnet', currNetwork);
                                newRecord.setFieldValue('custrecord_clgx_churn_currentint', currInterconnection);
                                newRecord.setFieldValue('custrecord_clgx_churn_currentpow', currPower);
                                newRecord.setFieldValue('custrecord_clgx_churn_currentsp', currSpace);
                                newRecord.setFieldValue('custrecord_clgx_churn_currentother', currOther);
                                newRecord.setFieldValue('custrecord_clgx_case_num', casenum);
                                if (churnType != '') {
                                    newRecord.setFieldValue('custrecord_clgx_churn_type_churn', churnType);
                                }
                                nlapiSubmitRecord(newRecord, false, true);
                            }

                        }
                        else {
                            if (searchChurns1 == null) {
                                for (var i = 0; searchChurns != null && i < searchChurns.length; i++) {
                                    //update the chrn record for this custmer and market
                                    var searchChurn = searchChurns[i];
                                    var columns = searchChurn.getAllColumns();
                                    var internalid = searchChurn.getValue('internalid');
                                    var churn = nlapiLoadRecord('customrecord_cologix_churn', internalid);
                                    if (exNetwork != '') {
                                        churn.setFieldValue('custrecord_clgx_churn_network', exNetwork);
                                    }
                                    if (exPower != '') {
                                        churn.setFieldValue('custrecord_clgx_churn_powermrr', exPower);
                                    }
                                    if (exSpace != '') {
                                        churn.setFieldValue('custrecord_clgx_churn_expspace', exSpace);
                                    }
                                    if (exInter != '') {
                                        churn.setFieldValue('custrecord_clgx_churn_expcross', exInter);
                                    }
                                    if (exOther != '') {
                                        churn.setFieldValue('custrecord_clgx_churn_expother', exOther);
                                    }
                                    churn.setFieldValue('custrecord_clgx_churn_expecteddate', duedate);
                                    churn.setFieldValue('custrecord_clgx_churn_status', churnst);
                                    churn.setFieldValue('custrecord_clgx_case_num', casenum);
                                    churn.setFieldValue('custrecord_clgx_churn_createdby', userid);
                                    churn.setFieldValue('custrecord_clgx_churn_salesrep', salesrepId);
                                    churn.setFieldValue('custrecord_clgx_churn_so', soId);
                                    churn.setFieldValue('custrecord_clgx_churn_currentnet', currNetwork);
                                    churn.setFieldValue('custrecord_clgx_churn_currentother', currOther);
                                    churn.setFieldValue('custrecord_clgx_churn_currentint', currInterconnection);
                                    churn.setFieldValue('custrecord_clgx_churn_currentpow', currPower);
                                    churn.setFieldValue('custrecord_clgx_churn_currentsp', currSpace);
                                    churn.setFieldValue('custrecord_clgx_churn_location', fac);
                                    if (churnType != '') {
                                        churn.setFieldValue('custrecord_clgx_churn_type_churn', churnType);
                                    }

                                    nlapiSubmitRecord(churn, false, true);

                                }


                            }
                        }

                    }
                    //update the status
                    if (((type == 'create') || (type == 'edit')) && (statusCase == 5)) {
                        var customer = nlapiLoadRecord('customer', company);
                        var salesrep = null;
                        var salesrepId = null;
                        if (customer.getFieldText('salesrep').length != 0) {
                            var salesrep = customer.getFieldText('salesrep');
                            var salesrepId = customer.getFieldValue('salesrep');
                        }
                        var emp = nlapiLoadRecord('employee', salesrepId);
                        var isinac = emp.getFieldValue('isinactive');
                        if (isinac == 'T') {
                            var salesrepId = '';
                        }
                        var caseChurnStatus = nlapiGetFieldValue('custevent_clgx_case_churnst');

                        var arrColumns = new Array();
                        arrColumns.push(new nlobjSearchColumn('internalid', null, null));
                        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_churn_status', null, null));

                        var arrFilters = new Array();
                        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_case_num", null, "anyof", casenum));
                        var searchChurns = nlapiSearchRecord('customrecord_cologix_churn', null, arrFilters, arrColumns);
                        for (var i = 0; searchChurns != null && i < searchChurns.length; i++) {
                            var searchChurn = searchChurns[i];
                            var internalid = searchChurn.getValue('internalid');
                            var statusChurn = searchChurn.getValue('custrecord_clgx_churn_status');
                            if (statusChurn != 5) {
                                var caseCreated = nlapiLoadRecord('supportcase', casenum);
                                var enddate = caseCreated.getFieldValue('enddate');
                                var dateParts = enddate.split(" ");
                                var churn = nlapiLoadRecord('customrecord_cologix_churn', internalid);

                                churn.setFieldValue('custrecord_clgx_churn_status', 4);

                                // churn.setFieldValue('custrecord_clgx_churn_closeddate', dateParts[0]);
                                nlapiSubmitRecord(churn, false, true);
                                //update the case churn status
                                nlapiSubmitField('supportcase', casenum, 'custevent_clgx_case_churnst', 4, true);
                            }
                            if (caseChurnStatus == 5) {
                                var churn = nlapiLoadRecord('customrecord_cologix_churn', internalid);
                                churn.setFieldValue('custrecord_clgx_churn_status', 5);
                                //upadate the churn close date if case staus is closed
                                nlapiSubmitRecord(churn, false, true);
                                nlapiSubmitField('supportcase', casenum, 'custevent_clgx_case_churnst', 5, true);
                            }
                        }

                    }

                }




            }
            body += '05 - ' + moment().diff(start) + ' | ';
            // End Section 2
            /**
                          *
                          *------------- Begin Section 3 -------------------------------------------------------------------
                          * Details:	Update/Create Remote Hands
                          * When a Case has a closed date a Scheduled Script will be launched. The Scheduled Script  wil create a  New Remote Hands will be created/updated. The Remote Hands will be added info about time tracking, rates...etc.
                          * Catalina  8 Sept 2014
                          */
            //-------------------------------------------------------------------------------------------------
            /**
                          *
                          * get the closed date && CreatedDate
                          */
            var caseid = nlapiGetRecordId();
            var acCase = nlapiLoadRecord('supportcase', caseid);
            var closeddate = '';
            closeddate = acCase.getFieldValue('enddate');
            var category = acCase.getFieldValue('category');
            var subcasetype = acCase.getFieldValue('custevent_cologix_sub_case_type');
            // if(category==1)
            var arrColumns = new Array();
            var arrFilters = new Array();
            var hoursB = 0;
            arrFilters.push(new nlobjSearchFilter("internalid", null, "anyof", caseid));
            var searchRemoteB = nlapiSearchRecord('supportcase', 'customsearch_clgx_savedsearchtimebcs', arrFilters, arrColumns);
            for (var j = 0; searchRemoteB != null && j < searchRemoteB.length; j++) {
                var searchRemB = searchRemoteB[j];
                var columnsB = searchRemB.getAllColumns();
                hoursB = searchRemB.getValue(columnsB[0]);
            }
            var oldtimedecimal = nlapiGetFieldValue("custpage_clgx_case_oldtimedecimal");
            var oldStatus = nlapiGetFieldValue("custpage_clgx_case_oldstatus");
            var newStatus = nlapiGetFieldValue("status");
            if ((oldtimedecimal != hoursB) || ((oldStatus != 5) && (newStatus == 5))) {
                if ((category == 1) && ((subcasetype == 5) || (subcasetype == 6) || (subcasetype == 85))) {
                    // var status=acCase.getFieldValue('status');
                    if (closeddate != null) {
                        closeddate = new Date(closeddate);
                        var year = closeddate.getFullYear();
                        var month = closeddate.getMonth();
                        var createddate = acCase.getFieldValue('createddate');
                        createddate = new Date(createddate);

                        /**
                                          * get the SO from case
                                          *
                                          */

                        /**
                                          * get the total businessH, total rates and company and location
                                          */
                        var company = acCase.getFieldValue('companyid');
                        var location = acCase.getFieldValue('custevent_cologix_facility');
                        var casesnbr = acCase.getFieldValue('casenumber');

                        /**
                                          *
                                          * schedule the script execution and define script parameter values
                                          */
                        var params = {
                            custscript_ss_remote_hands_cus: company,
                            custscript_ss_remote_hands_customerlocat: location,
                            custscript_ss_remote_hands_casecloseddat: closeddate,
                            custscript_ss_remote_hands_caseclosedmon: month,
                            custscript_ss_remote_hands_caseclosedyr: year,
                            custscript_ss_remote_hands_caseid: caseid,
                            //custscript_ss_remote_hands_totbusinessH:totalTimeB,
                            //custscript_ss_remote_hands_totafterH:totalTimeA,
                            // custscript_ss_remote_hands_totratebus:0,
                            // custscript_ss_remote_hands_totrateafter:0,
                            custscript_ss_remote_hands_casecreateddt: createddate,
                            custscript_ss_remote_hands_cas: casesnbr,
                            custscript_ss_remote_hands_userid: userid,
                            custscript_ss_remote_hands_modifiedcase: 1
                            //,
                            //custscript_ss_remote_hands_casetotalhour: totalTimeHoursBandA
                        };
                        //hit the scheduled script for remote hands

                        nlapiLogExecution("DEBUG", "CLGX_SU_Case - afterSubmit - Line #1379", "customscript_clgx_ss_remote_hands");
                        nlapiScheduleScript('customscript_clgx_ss_remote_hands', null, params);

                    }
                }
            }
            body += '06 - ' + moment().diff(start) + ' | ';
            //END SECTION 3
            //SECTION4 UPDATE FAM RECORD for Closed Maintenance Cases
            var newStatus = nlapiGetFieldValue("status");
            var subStatus = nlapiGetFieldValue("custevent_clgx_custcasefield_majorminor");

            if (newStatus == 5 && subStatus != 3) {
                var enddate = new Date(closeddate);

                var arrFilters = new Array();
                var arrColumns = new Array();
                var caseid = nlapiGetRecordId();
                arrFilters.push(new nlobjSearchFilter("internalid", "custevent_cologix_equipment", "anyof", caseid));
                var searchFAM = nlapiSearchRecord('customrecord_ncfar_asset', 'customsearch_search_famcasesmain', arrFilters, arrColumns);
                if (searchFAM != null) {
                    for (var j = 0; searchFAM != null && j < searchFAM.length; j++) {
                        var searchF = searchFAM[j];
                        var columns = searchF.getAllColumns();
                        var idFAM = searchF.getValue(columns[0]);
                        var interval = searchF.getValue(columns[1]);
                        var lastins = searchF.getValue(columns[2]);
                        var nextins = searchF.getValue(columns[3]);
                    }

                    var FAM = nlapiLoadRecord('customrecord_ncfar_asset', idFAM);
                    FAM.setFieldValue('custrecord_assetmaintlastdate', moment(nextins).format('l'));
                    if (interval > 0) {
                        enddate = new Date(nextins);
                        var nextdate = nlapiAddMonths(enddate, interval); // moment(add months);
                        FAM.setFieldValue('custrecord_assetmaintnextdate', moment(nextdate).format('l'));
                    }
                    //upadate the churn close date if case staus is closed
                    nlapiSubmitRecord(FAM, false, true);
                }
            }
        }
        //---------- End Sections ------------------------------------------------------------------------------------------------

        var usage = 1000 - parseInt(nlapiGetContext().getRemainingUsage());
        nlapiLogExecution('DEBUG', 'usage', usage);
        body += '07 - ' + moment().diff(start) + ' | ';
        var record = nlapiCreateRecord('customrecord_clgx_script_exec_time');
        record.setFieldValue('custrecord_clgx_script_exec_time_user', nlapiGetUser());
        record.setFieldValue('custrecord_clgx_script_exec_time_rec', "case");
        record.setFieldValue('custrecord_clgx_script_exec_time_id', nlapiGetRecordId());
        record.setFieldValue('custrecord_clgx_script_exec_time_context', 3);
        record.setFieldValue('custrecord_clgx_script_exec_time', moment().diff(start));
        record.setFieldValue('custrecord_clgx_script_exec_time_details', body);
        try {
            nlapiSubmitRecord(record, false, true);
        }
        catch (error) {
        }

        // nlapiSendEmail(71418,71418,'Case afterSubmit Processing Time - ' + caseid, body,null,null,null,null);


    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined) {
            nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else {
            nlapiLogExecution('ERROR', 'Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

    nlapiLogExecution("DEBUG", "CLGX afterSubmit - Case #2892137", nlapiGetFieldValue("status"));
}


//check if value is in the array
function inArray(val, arr) {
    var bIsValueFound = false;
    for (var i = 0; i < arr.length; i++) {
        if (val == arr[i]) {
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}