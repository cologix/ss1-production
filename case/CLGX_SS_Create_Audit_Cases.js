nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Create_Audit_Cases.js
//	Script Name:	CLGX_SS_Create_Audit_Cases
//	Script Id:		customscript_clgx_create_audit_cases
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Case
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		1/31/2012
//-------------------------------------------------------------------------------------------------

function scheduled_create_audit_cases () {
    try {
        nlapiLogExecution('DEBUG','Scheduled Script - Create_Audit_Cases','|--------------------STARTED---------------------|');
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.1 - 2/8/2012
// Details:	Creates 4 types of audit cases on the 1st of every month for each facility
//-----------------------------------------------------------------------------------------------------------------

        var date = new Date();
        var month = parseInt(date.getMonth()) + 1;
        var year = date.getFullYear();

        var arrColumnsBLDG = new Array();
        var arrFiltersBLDG = new Array();
        arrColumnsBLDG[0] = new nlobjSearchColumn('internalid', null, null);
        arrFiltersBLDG[0] = new nlobjSearchFilter('internalid', null, 'anyof',['3','17','15', '21', '14', '8','2', '5', '4',
            '9', '6','7','19', '20', '18', '24','27','28','33', '34','35','36']);
        //arrFiltersBLDG[0] = new nlobjSearchFilter('internalid', null, 'anyof',['8','15','14','20']);
        var searchBLDGs = nlapiSearchRecord('customrecord_cologix_facility', null, arrFiltersBLDG, arrColumnsBLDG);

        var arrColumnsMOPs = new Array();
        var arrFiltersMOPs = new Array();
        arrColumnsMOPs[0] = new nlobjSearchColumn('internalid', null, null);
        var searchMOPs = nlapiSearchRecord('customrecord_clgx_audit_mops', null, arrFiltersMOPs, arrColumnsMOPs);

        for ( var i = 0; searchBLDGs != null && i < searchBLDGs.length; i++ ) { // loop facilities

            var searchBLDG = searchBLDGs[i];
            var stBLDGId = searchBLDG.getValue('internalid');

            var recBLDG = nlapiLoadRecord('customrecord_cologix_facility', stBLDGId);
            var stBLDGName =  recBLDG.getFieldValue('name');
            var stBLDGAltName =  recBLDG.getFieldValue('altname');
            var stBLDGCompanyId =  recBLDG.getFieldValue('custrecord_cologix_location_subsidiary');

            for ( var j = 0; searchMOPs != null && j < searchMOPs.length; j++ ) { // loop sub categories

                var searchMOP = searchMOPs[j];
                var stMOPId = searchMOP.getValue('internalid');

                if((stBLDGId == 14 || stBLDGId == 21) &&( stMOPId == 1)){
                    // do not create Visitors Log for Minneapolis, JAX1 and VAN1
                }
                else if((stBLDGId == 24 || stBLDGId == 29|| stBLDGId == 33 || stBLDGId == 35) &&( stMOPId == 7 || stMOPId == 8)){
                    // do not create Day Pass and No Day Pass for COL
                }
                else{
                    if(stBLDGId != 14)
                    {
                        var recMOP = nlapiLoadRecord('customrecord_clgx_audit_mops', stMOPId);
                        var stCaseSubject =  recMOP.getFieldValue('custrecord_clgx_audit_mops_title');
                        var stCaseSubCategory =  recMOP.getFieldValue('custrecord_clgx_audit_mops_sub_category');
                        var stCaseInstructions =  recMOP.getFieldValue('custrecord_clgx_audit_mops_body');

                        var record = nlapiCreateRecord('supportcase');
                        record.setFieldValue('title', year + '.' + month + '.' + stBLDGName + '.' + stCaseSubject);
                        record.setFieldValue('company', stBLDGCompanyId);
                        record.setFieldValue('custevent_cologix_facility', stBLDGId);
                        record.setFieldValue('category', 9); // Case is only type Audit
                        record.setFieldValue('custevent_cologix_sub_case_type', stCaseSubCategory);
                        record.setFieldValue('custevent_clgx_audit_mops_instructions', stCaseInstructions);
                        record.setFieldValue('custevent_cologix_case_sched_followup', nlapiDateToString(nlapiAddDays(date, 15)));
                        record.setFieldValue('assigned', returnGroup(stBLDGId));
                        record.setFieldValue('email', returnEmail(stBLDGId));
                        record.setFieldValue('startdate', nlapiDateToString(date));
                        var idRec = nlapiSubmitRecord(record, false, true);
                    }
                }
            }
        }

        var emailSubject = 'Monthly Report - Audit Cases - ' +  year + '.' + month;
        var emailBody = 'All audit cases have been created.\n\n\n';
        
        /*nlapiSendEmail(432742,71418,emailSubject,emailBody,null,null,null,null,true);
        nlapiSendEmail(432742,206211,emailSubject,emailBody,null,null,null,null,true);
        nlapiSendEmail(432742,1349020,emailSubject,emailBody,null,null,null,null,true);*/

        clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_case_admin", emailSubject, emailBody);
        
        
        nlapiLogExecution('DEBUG','Monthly Report','Monthly Report - Audit Cases - ' +  year + '.' + month);


//---------- End Section 1 ------------------------------------------------------------------------------------------------


        nlapiLogExecution('DEBUG','Scheduled Script - Create_Audit_Cases','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function returnGroup(facility){
    var groupid = 0;
    switch(parseInt(facility)) {
        case 3:
            groupid = 2883; // Dallas
            break;
        case 17:
            groupid = 10505; // Minneapolis
            break;
        case 8:
            groupid=12883; //TOR1
            break;
        case 15:
            groupid = 12883; // TOR2
            break;
        case 21:
            groupid =231735;//JAX1
            break;
        case 27:
            //JAX2
            groupid=406253;
            break;
        case 14:
            groupid =12884; //VAN1
            break;
        case 2:
            //MTL1
            groupid=5241;
            break;
        case 5:
            //MTL2
            groupid=5393;
            break;
        case 4:
            //MTL3
            groupid =5394;
            break;
        case 9:
            //MTL4
            groupid=5395;
            break;
        case 6:
            //MTL5
            groupid=5396;
            break;
        case 7:
            //MTL6
            groupid=5400;
            break;
        case 19:
            //MTL7
            groupid=179538;
            break;
        case 20:
            //VAN2
            groupid=12884;
            break;
        case 18:
            //DAL2
            groupid=2883;
            break;
        case 24:
            //COL1&2
            groupid=232971;
            break;
        case 28:
            //LAK1
            groupid=380823;
            break;
        case 33:
            //NNJ2
            groupid=600770;
            break;
        case 34:
        	//NNJ1
        	groupid=600770;
        	break;
        case 35:
            //NNJ3
            groupid=600770;
            break;
        case 36:
        	//NNJ4
        	groupid=600770;
        	break;
        default:
            groupid = 0;
    }
    return groupid;
}

function returnEmail(facility){
    var email = '';
    switch(parseInt(facility)) {
        case 3:
            email = 'dallas@cologix.com'; // Dallas
            break;
        case 18:
            email = 'dallas@cologix.com'; // Dallas
            break;
        case 17:
            email = 'mpls.support@cologix.com'; // Minneapolis
            break;
        case 8:
            email = 'torontosupport@cologix.com'; // TOR1
            break;
        case 15:
            email = 'torontosupport@cologix.com'; // TOR2
            break;
        case 21:
            email ='jax.support@cologix.com'; //JAX1
            break;
        case 27:
            //JAX2
            email ='jax.support@cologix.com';
            break;
        case 14:
            email ='van.support@cologix.com'; //VAN1
            break;
        case 20:
            email ='van.support@cologix.com'; //VAN2
            break;
        case 2:
            //MTL1
            email='mtl.c1@cologix.com';
            break;
        case 5:
            //MTL2
            email='mtl.c2@cologix.com';
            break;
        case 4:
            //MTL3
            email ='mtl.c3@cologix.com';
            break;
        case 9:
            //MTL4
            email='mtl.c4@cologix.com';
            break;
        case 6:
            //MTL5
            email='mtl.c5@cologix.com';
            break;
        case 7:
            //MTL6
            email='mtl.c6@cologix.com';
            break;
        case 19:
            //MTL7
            email='mtl.c7@cologix.com';
            break;
        case 24:
            //COL1&2
            email='col.support@columbus.com';
            break;
        case 28:
            //LAK1
            email='lak.support@cologix.com';
            break;
        case 33:
            //NNJ2
            email='marcus.dockery@cologix.com';
            break;
        case 34:
        	email = "marcus.dockery@cologix.com";
        	break;
        case 35:
            //NNJ3
            email='marcus.dockery@cologix.com';
            break;
        case 36:
        	//NNJ4
        	email = "marcus.dockery@cologix.com";
        	break;
        default:
            email = 0;
    }
    return email;
}