function test() {
    try {
        // All Powers Usage
        var text = 'id,name,type,internalid\n';
        var x=[ {"boxid":	4531029062	,"name":"Ventech - SBIR"},
            {"boxid":	1837044539	,"name":"Ventech Solutions"},
            {"boxid":	886359562	,"name":"Venture Computers of Canada - STD"},
            {"boxid":	2793301649	,"name":"Venture Pointe Solutions"},
            {"boxid":	271962381	,"name":"Verizon Canada Ltd"},
            {"boxid":	116399334	,"name":"Vermont Telephone Company"},
            {"boxid":	1797121371	,"name":"Vertex Systems"},
            {"boxid":	10941948021	,"name":"Vertical Capital"},
            {"boxid":	7775251357	,"name":"Vertican Technologies"},
            {"boxid":	8977924338	,"name":"Verus Claims Services"},
            {"boxid":	3538455061	,"name":"Verus Corp"},
            {"boxid":	14053392531	,"name":"Vested Heritage"},
            {"boxid":	11675822349	,"name":"VGI Technology"},
            {"boxid":	116399174	,"name":"Viagenie"},
            {"boxid":	349879069	,"name":"Vianet Internet Solutions - STD"},
            {"boxid":	11875268196	,"name":"VianetTV"},
            {"boxid":	590828310	,"name":"Videoconference Solutions Inc"},
            {"boxid":	577707184	,"name":"Videotron"},
            {"boxid":	75146365938	,"name":"Videotron Ltee - CD1"},
            {"boxid":	116399108	,"name":"Videotron Ltee - Cross Connects"},
            {"boxid":	2342543729	,"name":"Videotron S.E.N.C. - C6"},
            {"boxid":	1396960252	,"name":"Videotron S.E.N.C. - MTL1"},
            {"boxid":	14479205030	,"name":"Videotron S.E.N.C. - MTL3"},
            {"boxid":	7452105525	,"name":"Videotron S.E.N.C. - MTL7"},
            {"boxid":	8707002417	,"name":"Vidvita - Canada"},
            {"boxid":	116400300	,"name":"VIF Internet"},
            {"boxid":	52049308218	,"name":"Vigilant Global"},
            {"boxid":	75145436949	,"name":"Ville de Brossard - CD1"},
            {"boxid":	50278780385	,"name":"Vineyard Partners"},
            {"boxid":	277152391	,"name":"Virtela Communications Inc - STD"},
            {"boxid":	3646708749	,"name":"Virtual Armor Security Solutions - INC."},
            {"boxid":	36757621026	,"name":"Virtual Business Management Solutions"},
            {"boxid":	9363687136	,"name":"Virtual Forum Inc."},
            {"boxid":	1958221966	,"name":"Virtual Office Systems"},
            {"boxid":	530682952	,"name":"Virtual Radiologic"},
            {"boxid":	66465195888	,"name":"Virtual Space Solutions"},
            {"boxid":	3145704641	,"name":"Virtualization Advisors"},
            {"boxid":	93497562079	,"name":"Vision CTS LLC"},
            {"boxid":	909518140	,"name":"Visionary Communications dba Mammoth Networks"},
            {"boxid":	909827302	,"name":"Visionary Communications"},
            {"boxid":	52050360393	,"name":"VisionNetwork"},
            {"boxid":	204434838	,"name":"Vista Print"},
            {"boxid":	9219023473	,"name":"Visual Graphic Systems"},
            {"boxid":	3909288383	,"name":"Vivint Internet"},
            {"boxid":	78094926526	,"name":"Vivo Gaming"},
            {"boxid":	9086526565	,"name":"Vling E Business LTD"},
            {"boxid":	431356668	,"name":"VMedia Inc."},
            {"boxid":	116398806	,"name":"Vocalspace LLC"},
            {"boxid":	457941741	,"name":"Voice Networks Canada"},
            {"boxid":	1837059471	,"name":"VoiceNet"},
            {"boxid":	42883625263	,"name":"Voip1000 Inc"},
            {"boxid":	7322961885	,"name":"Voipwell"},
            {"boxid":	772708174	,"name":"Vonage Business - USA"},
            {"boxid":	69057859577	,"name":"Votuvo Corporation"},
            {"boxid":	1318491961	,"name":"Voxcentric"},
            {"boxid":	1343242764	,"name":"VOXCO Canada"},
            {"boxid":	7050280313	,"name":"VOXO"},
            {"boxid":	350070207	,"name":"Voyant Communications"},
            {"boxid":	2588410019	,"name":"VRAD"},
            {"boxid":	831518052	,"name":"Vubiquity"},
            {"boxid":	5815916533	,"name":"Wabtec"},
            {"boxid":	2624237123	,"name":"Wabtec Corp."},
            {"boxid":	6315539665	,"name":"Waiting Room Solutions"},
            {"boxid":	735604102	,"name":"Wajam"},
            {"boxid":	592770778	,"name":"Wajam Internet Technologies Inc_"},
            {"boxid":	9048067158	,"name":"Wall Street Webcasting"},
            {"boxid":	508797050	,"name":"Walleye Trading Advisors"},
            {"boxid":	3365518010	,"name":"Wallick Communities"},
            {"boxid":	352342939	,"name":"Walser Automotive Group LLC"},
            {"boxid":	846727798	,"name":"Walter Investment Management Corp_"},
            {"boxid":	350193845	,"name":"Warner Connect"},
            {"boxid":	1834513923	,"name":"Watch TV Company DBA Watch Communications"},
            {"boxid":	2793337181	,"name":"Watson Mortgage Corporation"},
            {"boxid":	2912918259	,"name":"Watson Realty Corp."},
            {"boxid":	83421809309	,"name":"Wayne Automatic Fire Sprinklers"},
            {"boxid":	2793354727	,"name":"We Insure Florida"},
            {"boxid":	2793580883	,"name":"Web.com Group"},
            {"boxid":	2593338315	,"name":"Web.com Group"},
            {"boxid":	116396424	,"name":"Webair Internet Development"},
            {"boxid":	34022469458	,"name":"WebLearning _ WL Technology Solutions"},
            {"boxid":	116396696	,"name":"WebMinds"},
            {"boxid":	243689673	,"name":"Websdepot Technology Partners Inc"},
            {"boxid":	43864110097	,"name":"WebTech House"},
            {"boxid":	52051337908	,"name":"Weiner Law Group LLP"},
            {"boxid":	34005748090	,"name":"Wellington Management Company LLP"},
            {"boxid":	52049080727	,"name":"Wells Fargo Accounts Payable"},
            {"boxid":	941892785	,"name":"West Central Transport Group"},
            {"boxid":	352319739	,"name":"West Safety Services"},
            {"boxid":	51844970816	,"name":"Westchester Jewish Community Services"},
            {"boxid":	6303208777	,"name":"Westelcom Companies"},
            {"boxid":	9143599837	,"name":"Western Technologies Group"},
            {"boxid":	76976991250	,"name":"Westfield Imaging"},
            {"boxid":	418201718	,"name":"Westhill Trading"},
            {"boxid":	466477994	,"name":"Westman Communication - STD"},
            {"boxid":	123607129	,"name":"WGAL"},
            {"boxid":	6027139281	,"name":"Whipcord"},
            {"boxid":	19586596137	,"name":"Whiplash Technologies Limited"},
            {"boxid":	3288945702	,"name":"Whistler Blackcomb"},
            {"boxid":	5754000573	,"name":"White Castle"},
            {"boxid":	1541941329	,"name":"White Falcon communications"},
            {"boxid":	6252656293	,"name":"White Wings Consulting"},
            {"boxid":	29693736092	,"name":"WiBand"},
            {"boxid":	1901656979	,"name":"Widestream Solutions"},
            {"boxid":	11640243289	,"name":"Wikstrom Telephone Company"},
            {"boxid":	52050549985	,"name":"Wiley Malehorn Sirota"},
            {"boxid":	1826070519	,"name":"Wilke Global"},
            {"boxid":	6066658525	,"name":"WilliamMild"},
            {"boxid":	116399112	,"name":"WilTel Communications"},
            {"boxid":	82574213699	,"name":"Windstream (PARENT)"},
            {"boxid":	11310411088	,"name":"Windstream - COL"},
            {"boxid":	352305073	,"name":"Windstream - MIN"},
            {"boxid":	58147848053	,"name":"Windstream - MTL"},
            {"boxid":	2054913934	,"name":"Windstream Communications"},
            {"boxid":	2793669845	,"name":"Windstream Communications"},
            {"boxid":	6317268509	,"name":"Windstream Dallas"},
            {"boxid":	6082088886	,"name":"WinMill Software"},
            {"boxid":	487609068	,"name":"WireIE.com"},
            {"boxid":	3932747057	,"name":"Wirestar Networks"},
            {"boxid":	2507038563	,"name":"WISCNET"},
            {"boxid":	352314325	,"name":"Wisconsin Independent  Networks"},
            {"boxid":	1729442499	,"name":"Wisconsin Independent Network (Parent)"},
            {"boxid":	15091280456	,"name":"Witbe"},
            {"boxid":	2553102723	,"name":"WJXT"},
            {"boxid":	2142418378	,"name":"Wolfe.net"},
            {"boxid":	9469369034	,"name":"Wolfe.net - CAN"},
            {"boxid":	3697258841	,"name":"Wolfe.net - COL"},
            {"boxid":	432806268	,"name":"World Anti-Doping Agency"},
            {"boxid":	116396166	,"name":"Worldpay Canada"},
            {"boxid":	2793735415	,"name":"Wounded Warrior Project"},
            {"boxid":	53491402050	,"name":"Wow Unlimited Media Inc."},
            {"boxid":	1837065615	,"name":"WOW! Business"},
            {"boxid":	15908051787	,"name":"WSB & Associates"},
            {"boxid":	52049291868	,"name":"WSPC (World Scientific Publishing Col"},
            {"boxid":	8174019141	,"name":"WT Squared Inc."},
            {"boxid":	47977900419	,"name":"WTC Communications"},
            {"boxid":	89580683344	,"name":"WWE (Parent)"},
            {"boxid":	11236709291	,"name":"WWE - Web Division - World Wrestling Entertainment"},
            {"boxid":	8883943486	,"name":"WWE - World Wrestling Entertainment"},
            {"boxid":	37760194545	,"name":"Xcel Energy"},
            {"boxid":	2099829348	,"name":"Xconnect #1 Earthlink"},
            {"boxid":	1374228380	,"name":"Xigence.com Inc."},
            {"boxid":	52249548194	,"name":"Xinuos"},
            {"boxid":	52114565564	,"name":"XLEducation_DigitalPublishing (Parent)"},
            {"boxid":	8637347137	,"name":"XLEducation_DigitalPublishing-SpireVision"},
            {"boxid":	8863251058	,"name":"XLEducation_DigitalPublishing-Sterling Marketing"},
            {"boxid":	52114218882	,"name":"XLEducation_DigitalPublishing-WardMedia"},
            {"boxid":	116399166	,"name":"XO Communications"},
            {"boxid":	914798542	,"name":"XO Communications (Parent)"},
            {"boxid":	2151220398	,"name":"XO Communications - COL"},
            {"boxid":	450554335	,"name":"XO Communications - MIN - Cross Connect"},
            {"boxid":	1288572272	,"name":"XO Communications - MIN - Dark Fiber"},
            {"boxid":	247979385	,"name":"XO Communications Toronto"},
            {"boxid":	260617946	,"name":"Xplornet Communications"},
            {"boxid":	424875648	,"name":"Xplornet communications inc"},
            {"boxid":	7771830121	,"name":"Xybion"},
            {"boxid":	5989001041	,"name":"XYZ"},
            {"boxid":	2619244429	,"name":"XZ Backup LLC"},
            {"boxid":	97493540656	,"name":"Yelp"},
            {"boxid":	243777815	,"name":"YesUp Ecommerce Solutions Inc"},
            {"boxid":	2793760171	,"name":"YIG Administration"},
            {"boxid":	93323635062	,"name":"YNAP Corporation"},
            {"boxid":	29545441988	,"name":"Yomura Corporation"},
            {"boxid":	1837070063	,"name":"Yost Network Solutions"},
            {"boxid":	2793778675	,"name":"Your IT Resource"},
            {"boxid":	3056226383	,"name":"Your nucleus"},
            {"boxid":	116395160	,"name":"Z-4184122 C"},
            {"boxid":	30434162120	,"name":"Zadara Storage"},
            {"boxid":	61431200172	,"name":"Zadara Storage Canada"},
            {"boxid":	11250743616	,"name":"Zaphyr Technologies"},
            {"boxid":	3404983404	,"name":"Zayo (fka AboveNet) - JAX"},
            {"boxid":	243711097	,"name":"Zayo - CAN"},
            {"boxid":	1837073077	,"name":"Zayo - COL"},
            {"boxid":	317046387	,"name":"Zayo Bandwidth"},
            {"boxid":	2968934373	,"name":"Zayo Canada Inc. (GCNet)"},
            {"boxid":	123347517	,"name":"Zayo Canada Inc. (MAS)"},
            {"boxid":	1179934453	,"name":"Zayo Group"},
            {"boxid":	75213815678	,"name":"Zayo Group LLC - CD1:CD2"},
            {"boxid":	365365961	,"name":"Zayo Group"},
            {"boxid":	2265055369	,"name":"Zayo Professional Services"},
            {"boxid":	11983692759	,"name":"Zazeen inc."},
            {"boxid":	52114368124	,"name":"ZCTS_JaziNET"},
            {"boxid":	9204895437	,"name":"ZD Media"},
            {"boxid":	7305404677	,"name":"Zelis Payments"},
            {"boxid":	52014046750	,"name":"Zeta Interactive Corporation"},
            {"boxid":	7892249181	,"name":"Ziff Brothers Investments"},
            {"boxid":	250689461	,"name":"Zip Telecom"},
            {"boxid":	31105700856	,"name":"Zirro"},
            {"boxid":	1050771365	,"name":"Zito Texas-Washington"},
            {"boxid":	390686959	,"name":"Zone R√É¬©seau"},
            {"boxid":	3248927298	,"name":"Zscaler Canada"},
            {"boxid":	5428800374	,"name":"ZTE"},
            {"boxid":	6743992941	,"name":"Zylog Systems Ltd DBA ZSLInc"},
            {"boxid":	90765749141	,"name":"[24]7.ai Inc."}];

        for ( var j = 0; x != null && j < x.length; j++ ) {
            var columns = new Array();
            var filters = new Array();
            filters.push(new nlobjSearchFilter("entityid", null, "is", x[j].name));
            var records = nlapiSearchRecord('lead','customsearch8113', filters, columns);
            if(records!=null){

            for (var i = 0; records != null && i < records.length; i++) {

                text += x[j].boxid + ',';
                text += x[j].name + ',';
                text += 'lead,';
                text += records[i].getValue('internalid', null, null) +',';
                text += '\n';
            }
            }
        }
        var fileName = nlapiCreateFile('box_lead18Apr.csv', 'PLAINTEXT', text);
        fileName.setFolder(9114791);
        var fileid=nlapiSubmitFile(fileName);

        var x=0

    }
    catch (error){
        var context = nlapiGetContext();
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function buildComparisonObject(serviceOrderID) {
    var serviceOrders = buildServiceOrderObject(serviceOrderID);
    var services      = buildServiceObject(serviceOrderID, _.map(serviceOrders, "sid"));
    var diff=_.differenceBy( services,serviceOrders, "soid");
    return JSON.stringify(_.differenceBy( services,serviceOrders, "soid"));
}


function buildServiceOrderObject(serviceOrderID) {
    var serviceOrderJson    = new Array();
    var serviceOrderFilters = new Array();
    serviceOrderFilters.push(new nlobjSearchFilter("type", null, "anyof", "SalesOrd"));
    serviceOrderFilters.push(new nlobjSearchFilter("internalid", null, "anyof", serviceOrderID));
    serviceOrderFilters.push(new nlobjSearchFilter("custcol_clgx_so_col_service_id", null, "noneof", "@NONE@"));

    var serviceOrderColumns = new Array();
    serviceOrderColumns.push(new nlobjSearchColumn("internalid"));
    serviceOrderColumns.push(new nlobjSearchColumn("custcol_clgx_so_col_service_id"));

    var serviceOrderSearchObject = nlapiSearchRecord("transaction", null, serviceOrderFilters, serviceOrderColumns);

    if(serviceOrderSearchObject != null && serviceOrderSearchObject != "") {
        for(var soso = 0; soso < serviceOrderSearchObject.length; soso++) {
            serviceOrderJson.push({ soid: parseInt(serviceOrderSearchObject[soso].getValue("internalid")), sid: parseInt(serviceOrderSearchObject[soso].getValue("custcol_clgx_so_col_service_id")) });
        }
    }

    return serviceOrderJson;
}


function buildServiceObject(serviceOrderID, serviceOrderArray) {
    var serviceJson    = new Array();

    if(serviceOrderArray && serviceOrderArray.length > 0) {
        var serviceFilters = new Array();

        serviceFilters.push(new nlobjSearchFilter("internalid", null, "anyof", serviceOrderArray));
        serviceFilters.push(new nlobjSearchFilter("isinactive", null, "is", "F"));

        var serviceColumns = new Array();
        serviceColumns.push(new nlobjSearchColumn("internalid"));
        serviceColumns.push(new nlobjSearchColumn("custentity_cologix_service_order"));

        var serviceSearchObject = nlapiSearchRecord("job", null, serviceFilters, serviceColumns);

        if(serviceSearchObject != null && serviceSearchObject != "") {
            for(var sso = 0; sso < serviceSearchObject.length; sso++) {
                serviceJson.push({ sid: parseInt(serviceSearchObject[sso].getValue("internalid")), soid: parseInt(serviceSearchObject[sso].getValue("custentity_cologix_service_order")) });
            }
        }
    }

    return serviceJson;
}
function clgx_update_balances (customerid){

    var balance = 0;
    var unapplied = 0;
    var sumremaining = 0;

// Get IDs of all consolidate invoices with open invoices ====================================================================================

    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
    var searchOpenCInvoices = nlapiSearchRecord('transaction', 'customsearch_clgx_inv_balance_open', arrFilters, arrColumns);

    if(searchOpenCInvoices){

        var arrOpenCInvoicesIDs = new Array();
        for ( var i = 0; searchOpenCInvoices != null && i < searchOpenCInvoices.length; i++ ) {
            arrOpenCInvoicesIDs.push(parseInt(searchOpenCInvoices[i].getValue('custbody_consolidate_inv_cinvoice',null,'GROUP')));
        }
        var arrOpenCInvoicesIDs = _.compact(_.uniq(arrOpenCInvoicesIDs));

// Get Sums of all consolidate invoices with open invoices ====================================================================================

        var arrColumns = new Array();
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
        arrFilters.push(new nlobjSearchFilter("custbody_consolidate_inv_cinvoice",null,"anyof",arrOpenCInvoicesIDs));
        var searchCInvoices = nlapiSearchRecord('transaction', 'customsearch_clgx_inv_balance_cinvoice', arrFilters, arrColumns);
        var arrCInvoices = new Array();
        for ( var i = 0; searchCInvoices != null && i < searchCInvoices.length; i++ ) {
            var cinvoice = parseInt(searchCInvoices[i].getValue('custbody_consolidate_inv_cinvoice',null,'GROUP'));
            var remaining = parseFloat(searchCInvoices[i].getValue('fxamountremaining',null,'SUM'));
            sumremaining += remaining;
            nlapiSubmitField('customrecord_clgx_consolidated_invoices', cinvoice, 'custrecord_clgx_consol_inv_balance', remaining);
        }
    }

// Get Sums of unapplied payments and credit memos  ====================================================================================

    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
    var searchUnapplied = nlapiSearchRecord('transaction', 'customsearch_clgx_inv_balance_unapplied', arrFilters, arrColumns);
    if(searchUnapplied){
        var fxamountremaining = searchUnapplied[0].getValue('fxamountremaining',null,'SUM');
        if(fxamountremaining !== '' && fxamountremaining !== null){
            unapplied = parseFloat(fxamountremaining);
        }
    }

    var balance = sumremaining - unapplied;
    nlapiSubmitField('customer', customerid, 'custentity_clgx_customer_balance', balance);

    return balance;
}