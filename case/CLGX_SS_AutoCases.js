nlapiLogExecution("audit","FLOStart",new Date().getTime());

//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_AutoCases.js
//	Script Name:	CLGX_SS_AutoCases.js
//	Script Id:		customscript_clgx_ss_autocases
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		3/22/2017
//-------------------------------------------------------------------------------------------------

function scheduled_auto_cases (OnlyID) {
    try {
        nlapiLogExecution('DEBUG','Auto Cases','|--------------------STARTED---------------------| ');

        var context = nlapiGetContext();

        //var schedid = 1320;
        var columns = [];
        var filters = [];
        //filters.push(new nlobjSearchFilter("internalid",null,"anyof",schedid));
        var search = nlapiSearchRecord('customrecord_clgx_auto_cases', 'customsearch_clgx_run_auto_cases', filters, columns);
        var schedules = [];
        for ( var i = 0; search != null && i < search.length; i++ ) {

            var schedid = parseInt(search[i].getValue('internalid'));

            var rec = nlapiLoadRecord('customrecord_clgx_auto_cases', schedid);

            var facilities = [];
            var strfac = rec.getFieldValues('custrecord_facility') || [];
            for ( var j = 0; j < strfac.length; j++ ) {
                facilities.push(parseInt(strfac[j]));
            }
            facilities = _.compact(_.sortBy(facilities, function(num){ return num; }));

            var now = parseInt(moment().format('H'));
            var hour = (parseInt(rec.getFieldValue('custrecord_clgx_start_time')) - 1) || 1;
            var fhour = get_facility_hour (parseInt(moment().format('H')), facilities[0]);

            if(fhour == hour || (now > 3 && fhour >= hour)){
                var schedule = get_schedule(schedid);
                schedules.push(schedule);
                //nlapiLogExecution('DEBUG','Results ', ' | SchedID = ' + schedid + ' will create a case |');
            }
        }

        for ( var i = 0; i < schedules.length; i++ ) {

            // create cases
            for ( var c = 0;  c < schedules[i].cases.length; c++ ) {

                try {
                	
                    var record = nlapiCreateRecord('supportcase');
                    record.setFieldValue('company', schedules[i].cases[c].customer);
                    record.setFieldValue('title', schedules[i].cases[c].subject);
                    record.setFieldValue('custevent_cologix_facility', schedules[i].cases[c].facility);
                    
                    record.setFieldValue('category', schedules[i].cases[c].type); // Case is only type Remote Hands
                    record.setFieldValue('custevent_cologix_sub_case_type', schedules[i].cases[c].subtype); // Scheduled
		            //record.setFieldValue('category', 1); // Case is only type Remote Hands
		            //record.setFieldValue('custevent_cologix_sub_case_type', 6); // Scheduled
	            	
		            record.setFieldValue('custevent_clgx_audit_mops_instructions', schedules[i].cases[c].body);
                    record.setFieldValue('incomingmessage', schedules[i].cases[c].body);
                    record.setFieldValue('startdate', schedules[i].nextrun);
                    record.setFieldValue('priority', schedules[i].cases[c].priority);
                    record.setFieldValue('origin', 4);
                    if (schedules[i].cases[c].employee > 0) {
                        record.setFieldValue('assigned', schedules[i].cases[c].employee);
                    } else if (schedules[i].cases[c].group > 0) {
                        record.setFieldValue('assigned', schedules[i].cases[c].group);
                    }

                    if (schedules[i].cases[c].contact > 0) {
                        var recContact = nlapiLoadRecord ('contact', schedules[i].cases[c].contact);
                        if (recContact != null) {
                            var sEmail = recContact.getFieldValue('email');
                            if (sEmail != null) {
                                record.setFieldValue('email',sEmail);
                            }
                        }
                    }
                    record.setFieldValue('custevent_clgx_autocase', schedules[i].id);

                    var caseID = nlapiSubmitRecord(record, false, true);

                    var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
                    nlapiLogExecution('DEBUG','Results ', ' | SchedID = ' + schedules[i].id + ' | CaseID = ' + caseID + ' | Usage - '+ usageConsumtion + '  |');

                    // update dates on auto case record
                    var rec = nlapiLoadRecord('customrecord_clgx_auto_cases', schedules[i].id);
                    if(schedules[i].newlastrun){
                        rec.setFieldValue('custrecord_clgx_last_run', schedules[i].newlastrun);
                    }
                    if(schedules[i].newnextrun){
                        rec.setFieldValue('custrecord_clgx_next_run', schedules[i].newnextrun);
                    }
                    nlapiSubmitRecord(rec, false,true);

                }
                catch (error) { 
                    if (error.getDetails != undefined){
                        nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
                        //throw error;
                        var error = error.getCode() + ': ' + error.getDetails();
                    } else {
                        nlapiLogExecution('ERROR','Unexpected Error', error.toString());
                        //throw nlapiCreateError('99999', error.toString());
                        var error = error.toString();
                    }

                    var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
                    nlapiLogExecution('DEBUG','Results ', ' | SchedID = ' + schedules[i].id + ' | error = ' + error + ' | Usage - '+ usageConsumtion + '  |');

                    clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_case_admin", 'error autocases', error);
                }
            }
        }

    }
    catch (error) { 
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        } else {
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } 
 
}

function AutoCaseLog(auto_case_id, log_type, log_message, log_details) {
    nlapiLogExecution(log_type, 'error', log_message, log_details);
    try {
        var record = nlapiCreateRecord('customrecord_clgx_auto_case_logevents');
        record.setFieldValue('custrecord_clgx_autocaselog_autocaseid', auto_case_id);
        record.setFieldValue('custrecord_clgx_autocaselog_type', log_type);
        record.setFieldValue('custrecord_clgx_autocaselog_text', log_message + ' - ' + log_details);

        var idRec = nlapiSubmitRecord(record, false, true);
    } catch (error) {
        nlapiLogExecution('ERROR','[' + auto_case_id + '] nlapiSubmitRecord AutoCaseLog Error', error.getCode() + ': ' + error.getDetails());
        //	//throw error;
    }
}


function returnGroup(facility){
    var groupid = 0;
    switch(parseInt(facility)) {
        case 3: // DAL
            groupid = 2883;
            break;
        case 17: // MIN
            groupid = 10505;
            break;
        case 8: //TOR1
            groupid=12883;
            break;
        case 15: // TOR2
            groupid = 12883;
            break;
        case 21: //JAX1
            groupid =231735;
            break;
        case 27: //JAX2
            groupid=406253;
            break;
        case 14: //VAN1
            groupid =12884;
            break;
        case 2: //MTL1
            groupid=5241;
            break;
        case 5: //MTL2
            groupid=5393;
            break;
        case 4: //MTL3
            groupid =5394;
            break;
        case 9: //MTL4
            groupid=5395;
            break;
        case 6: //MTL5
            groupid=5396;
            break;
        case 7: //MTL6
            groupid=5400;
            break;
        case 19: //MTL7
            groupid=179538;
            break;
        case 20: //VAN2
            groupid=12884;
            break;
        case 18: //DAL2
            groupid=2883;
            break;
        case 24: //COL1&2
            groupid=232971;
            break;
        case 28: //LAK1
            groupid=380823;
            break;
        case 33: // NNJ2
            groupid=677920;
            break;
        case 34: // NNJ1
            groupid=677920;
            break;
        case 35: // NNJ3
            groupid=677920;
            break;
        case 36: // NNJ4
            groupid=677920;
            break;

        default:
            groupid = 0;
    }
    return groupid;
}

function get (datain) {
    try {
        nlapiLogExecution('DEBUG','Scheduled Script - DO GET','');
        if (datain['id'] ) {
            return scheduled_auto_cases(datain['id']);
        } else {
            return scheduled_auto_cases();
        }

        nlapiLogExecution('DEBUG','Scheduled Script - END GET','');
    } catch (error) { // Start Catch Errors Section
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        } else {
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function timestamp(asof) {
    var str = "";

    var currentTime = new Date(asof);
    var hours = currentTime.getHours();
    var minutes = currentTime.getMinutes();
    var seconds = currentTime.getSeconds();
    var meridian = "";
    if (hours > 12) {
        meridian += "pm";
    } else {
        meridian += "am";
    }
    if (hours > 12) {

        hours = hours - 12;
    }
    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    str += hours + ":" + minutes + ":" + seconds + " ";

    return str + meridian;
}
