nlapiLogExecution("audit", "FLOStart", new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Case_Message.js
//	Script Name:	CLGX_SS_Case_Message
//	Script Id:		customscript_clgx_ss_case_message
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Case
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		05/14/2014
//-------------------------------------------------------------------------------------------------

function scheduled_case_message() {
    try {
        //------------- Begin Section 1 -------------------------------------------------------------------
        //	Details:	Update message on sub cases
        //	Created:	04/12/2014

        var scriptStart = moment();

        //retrieve script parameters
        var userid = nlapiGetContext().getSetting('SCRIPT', 'custscript_ss_case_message_userid');
        var caseid = nlapiGetContext().getSetting('SCRIPT', 'custscript_ss_case_message_caseid');
        var sendto = nlapiGetContext().getSetting('SCRIPT', 'custscript_ss_case_message_sendto');
        var messageid = nlapiGetContext().getSetting('SCRIPT', 'custscript_ss_case_message_messageid');

        var message = '';
        var subject = '';

        nlapiLogExecution('DEBUG', 'userid', userid);
        nlapiLogExecution('DEBUG', 'caseid', caseid);
        nlapiLogExecution('DEBUG', 'sendto', sendto);
        nlapiLogExecution('DEBUG', 'messageid', messageid);

        //-------------------------------------------------------------------------------------------------
        nlapiLogExecution('DEBUG', 'Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');
        //Set the parent to lock while sending in the emails to the child cases
        var in_progress = nlapiCreateRecord('customrecord_ns_acs_in_progress')
        in_progress.setFieldValue('custrecord_ns_acs_parent_case', caseid)
        var in_progress_record = nlapiSubmitRecord(in_progress, true);
        //check parent case
        var parent_case = nlapiLoadRecord('supportcase', caseid);
        nlapiLogExecution('DEBUG', 'parent_case', parent_case);
        nlapiLogExecution('DEBUG', 'in_progress_record', in_progress_record);
        var parent_locked = parent_case.getFieldValue('custevent_clgx_locked_case');
        if (in_progress_record != null || parent_locked == true) {
            nlapiLogExecution('DEBUG', 'in_progress_record is not null', in_progress_record);
            var arrParam = new Array();
            arrParam['custscript_internal_message'] = 'The sub cases for this parent case are still being created or updated. Please wait until you have received the confirmation email.';
            if (nlapiGetRole() != 1088) {
                nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
            }
        }
       
        //-----------------------------------------------------------------------
        if (messageid > 0) {
            var recMessage = nlapiLoadRecord('message', messageid);
            message = recMessage.getFieldValue('message');
            subject = recMessage.getFieldValue('subject');
            var d = new moment();
            if (message != '' && message != null) {
                message = message.replace(new RegExp('{date}', 'g'), d.format('MMMM DD, YYYY'));
            }
        }
      

        // Search cases with this parent
        var searchColumns = new Array();
        searchColumns.push(new nlobjSearchColumn('internalid', null, null));
        var searchFilter = new Array();
        searchFilter.push(new nlobjSearchFilter('custevent_clgx_parent_id', null, 'anyof', caseid));
        //remove this after
        //searchFilter.push(new nlobjSearchFilter('internalid', null, 'anyof', 14274271));
        //searchFilter.push(new nlobjSearchFilter('internalid', null, 'anyof', 14274272));
        var searchCases = nlapiSearchRecord('supportcase', null, searchFilter, searchColumns);

        var recParentCase = nlapiLoadRecord('supportcase', caseid);
        nlapiLogExecution('DEBUG', 'caseid', caseid);
        var casenumber = recParentCase.getFieldValue('casenumber');
        nlapiLogExecution('DEBUG', 'casenumber', casenumber);
        var casetype = recParentCase.getFieldValue('category');
        nlapiLogExecution('DEBUG', 'casetype', casetype);
        var subcasetype = recParentCase.getFieldValue('custevent_cologix_sub_case_type');
        nlapiLogExecution('DEBUG', 'subcasetype', subcasetype);
        var status = recParentCase.getFieldText('status');
        nlapiLogExecution('DEBUG', 'status', status);
        var priority = recParentCase.getFieldText('priority');
        nlapiLogExecution('DEBUG', 'priority', priority);
        var assigned = recParentCase.getFieldText('assigned');
        nlapiLogExecution('DEBUG', 'assigned', assigned);
        var facilityname = recParentCase.getFieldText('custevent_cologix_facility');
        nlapiLogExecution('DEBUG', 'facilityname', facilityname);

        

        var followup = recParentCase.getFieldValue('custevent_cologix_case_sched_followup');
        var starttime = recParentCase.getFieldValue('custevent_cologix_sched_start_time');
        
        var enddate = recParentCase.getFieldValue('custevent_clgx_case_end_date');
        var endtime = recParentCase.getFieldValue('custevent_clgx_case_end_time');
        
        var equipment = recParentCase.getFieldText('custevent_cologix_equipment');
        
        nlapiLogExecution('DEBUG', 'followup', followup);
        nlapiLogExecution('DEBUG', 'starttime', starttime);
        if (status == 'Escalated') {
            var subcasestatus = 'In Progress';
        }
        else {
            subcasestatus = status;
        }
        if (status == 'CAB Approved') {
            if (facilityname.indexOf("MTL") > -1) {
                assigned = 'Customer Care-MTL';
                var assignedto1 = nlapiSubmitField('supportcase', caseid, 'assigned', 317218);
                //var assignedto1 = nlapiSubmitField('supportcase', caseid, 'assigned', 2338653);
                nlapiLogExecution('DEBUG', 'assignedto1', assignedto1);
            }
            else {
                assigned = 'Customer Care-Non MTL';
                var assignedto2 =   nlapiSubmitField('supportcase', caseid, 'assigned', 317219);
                //var assignedto2 = nlapiSubmitField('supportcase', caseid, 'assigned', 2392996);
                nlapiLogExecution('DEBUG', 'assignedto1', assignedto2);
            }
        }

        if (priority == 'Urgent') {
            priority = 'Medium';
        }


        for (var i = 0; searchCases != null && i < searchCases.length; i++) {
            nlapiLogExecution('DEBUG', 'START LOOP', "----------------------Start LOOP ----------------------");

            var searchCase = searchCases[i];
            var id = searchCase.getValue('internalid', null, null);

            var recCase = nlapiLoadRecord('supportcase', id);
            var email = recCase.getFieldValue('email');
            var email_custom = recCase.getFieldValue('custevent_acs_custom_email');
        
            if (email != null && email != '' && sendto == 'T' && message != '' && message != null) {
                nlapiLogExecution('DEBUG', 'Entered Here', "----------------------ENTER HERE ----------------------");
                var customer = recCase.getFieldValue('company');
                var companyname = nlapiLookupField('customer', customer, 'companyname');
                var facilityname = recCase.getFieldText('custevent_cologix_facility');
                var followup = recCase.getFieldValue('custevent_cologix_case_sched_followup');
				var startdate = recCase.getFieldValue('startdate');
				
                var outgoingmessage = message;
                var outgoingmessage = outgoingmessage.replace(new RegExp('{customer}', 'g'), companyname);
                outgoingmessage = outgoingmessage.replace(new RegExp('{facility}', 'g'), facilityname);
                outgoingmessage = outgoingmessage.replace(new RegExp('{date}', 'g'), new moment().format('M/D/YYYY'));
                
                var d1 = new moment(followup);
                outgoingmessage = outgoingmessage.replace(new RegExp('{followup}', 'g'), d1.format('MMMM DD, YYYY'));

				var timeZoneSuffix = getFacilityTimeSuffix(facilityname);
                
                var startEventTimeObject = new moment((followup + " " + starttime));
                var endEventTimeObject   = new moment((followup + " " + starttime));
                
                var eventStart    = startEventTimeObject.format("M/D/YYYY hh:mm a ").concat(timeZoneSuffix);
                var eventStartICS = startEventTimeObject.format("YYYYMMDDTHHmmss");
                
                var eventEndObject = endEventTimeObject.add(1, "hour");
                var eventEnd       = eventEndObject.format("M/D/YYYY hh:mm a ").concat(timeZoneSuffix);
                var eventEndICS    = eventEndObject.format("YYYYMMDDTHHmmss");
                
                if(enddate && endtime) {
                	endEventTimeObject = new moment((enddate + " " + endtime));
                	eventEnd = endEventTimeObject.format("M/D/YYYY hh:mm a ").concat(timeZoneSuffix);
                	eventEndICS = endEventTimeObject.format("YYYYMMDDTHHmmss");
                }
                
                outgoingmessage = outgoingmessage.replace(new RegExp('{end}', 'g'), eventEnd);
                
                outgoingmessage = outgoingmessage.replace(new RegExp('{equipment}', 'g'), equipment);
                outgoingmessage = outgoingmessage.replace(new RegExp('{start}', 'g'), eventStart);
                
                outgoingmessage = outgoingmessage.replace(new RegExp('{addicstocalendar}', 'g'), buildICSCalendarLink({ title: subject, details: "", location: facilityname, begin: eventStartICS, end: eventEndICS }));
                outgoingmessage = outgoingmessage.replace(new RegExp('{addtogooglecalendar}', 'g'), buildGoogleCalendarLink({ title: subject, details: "", location: facilityname, begin: eventStartICS, end: eventEndICS }));

                recCase.setFieldValue('internalonly', 'F');
                recCase.setFieldValue('emailform', 'T');
                recCase.setFieldValue('htmlmessage', 'T');
                recCase.setFieldValue('outgoingmessage', outgoingmessage);

                //nlapiLogExecution('DEBUG', 'outgoingmessage', outgoingmessage);
                //nlapiLogExecution('DEBUG', 'd1', d1);

                if (email_custom != null && email_custom != '') {
                    var new_array_by_10 = new Array();
                    var email_arr;

                    nlapiLogExecution('DEBUG', 'email_custom >>', email_custom);

                    email_arr = email_custom.split(',');
                    nlapiLogExecution('DEBUG', 'Emails Custom All >> ', email_arr);

                    for (var x = 0; x < email_arr.length; x = x + 10) {
                        nlapiLogExecution('DEBUG', 'push spillover email by 10', "---START LOOP ---");
                        new_array_by_10.push(email_arr.slice(x, x + 10));
                        nlapiLogExecution('DEBUG', 'new_array_by_10', new_array_by_10);

                    }
                    nlapiLogExecution('DEBUG', 'END PUSH', "---END PUSH LOOP---");

                    var new_array_by_10_length = new_array_by_10.length;
                    var count;
                    for (count = 0; count <= new_array_by_10_length - 1; count++) {
                        nlapiLogExecution('DEBUG', 'EMAIL USER', "---EMAIL USER START---");
                        nlapiLogExecution('DEBUG', "count", count)
                        nlapiLogExecution('DEBUG', 'email message', count.toString() + subject + "\n" + outgoingmessage);
                        var records = new Object();
                        records['activity'] = id;
                        nlapiSendEmail(userid, new_array_by_10[count], subject, outgoingmessage, null, null, records, null, true);
                        //nlapiSendEmail(userid, userid, nlapiLogExecution('DEBUG', + subject, message + new_array_by_10[count], null, null, null, null, true)
                    }
                    nlapiLogExecution('DEBUG', 'EMAIL END', "---EMAIL USER END---");

                }
            }
                recCase.setFieldValue('category', casetype);
                recCase.setFieldValue('custevent_cologix_sub_case_type', subcasetype);
                recCase.setFieldText('status', subcasestatus);
                nlapiLogExecution('DEBUG', 'subcasestatus', subcasestatus);
                recCase.setFieldValue('custevent_cologix_case_sched_followup', followup);
                recCase.setFieldValue('custevent_cologix_sched_start_time', starttime);

                if (assigned != null && assigned != '') {
                    recCase.setFieldText('assigned', assigned);
                }
                recCase.setFieldText('priority', priority);
                nlapiSubmitRecord(recCase, false, true);
                nlapiLogExecution('DEBUG', 'Search Length', searchCases.length);
                nlapiLogExecution('DEBUG', 'I', i);

                nlapiLogExecution('DEBUG', 'END LOOP', "----------------------END LOOP ----------------------");

            
        }
            var parent_case = nlapiLoadRecord('supportcase', caseid);
            //nlapiLogExecution('DEBUG', 'parent_case', parent_case);
            
           
            //nlapiSubmitField('supportcase', caseid, 'custevent_clgx_locked_case', 'F');
            nlapiLogExecution('DEBUG', 'caseid', caseid);
            var emailUserSubject = 'Update for incident / maintenance case ' + casenumber;
            var emailUserBody = 'All sub cases have been updated.';

            nlapiSendEmail(userid, userid, emailUserSubject, emailUserBody, null, null, null, null, true);
            nlapiLogExecution('DEBUG', 'EMAIL SENT---', "----------EMAIL SENT------------");

            var id = nlapiDeleteRecord('customrecord_ns_acs_in_progress', in_progress_record);
            nlapiLogExecution('DEBUG', 'Deleted Custom Record---', id);

            parent_case.setFieldValue('custevent_clgx_locked_case', 'F')
            //nlapiLogExecution('DEBUG', 'userid', userid);
            //nlapiLogExecution('DEBUG', 'parent_case', JSON.stringify(parent_case));
            var locked = parent_case.getFieldValue('custevent_clgx_locked_case');
            nlapiLogExecution('DEBUG', 'Unlock Record 1', locked);
            var submitted = nlapiSubmitRecord(parent_case)
            //nlapiLogExecution('DEBUG', 'caseid---', submitted);
            
            nlapiLogExecution('DEBUG', 'Finished Execution', '|-------------------------- Finished Scheduled Script --------------------------|');
        
    }
    catch (error) {
        nlapiLogExecution('DEBUG', 'INSIDE CATCH', '|-------------------------- CONTACT NETSUITE SUPPORT --------------------------|');
        nlapiLogExecution('DEBUG', 'INSIDE CATCH', JSON.stringify(error));
       
        //nlapiSubmitField('supportcase', caseid, 'custevent_clgx_locked_case', 'F');

        clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_case_admin", "Error processing sub cases for case id: " + caseid, "Please contact support");

        //nlapiSendEmail(userid,71418,'Error processing sub cases for case id ' + caseid, 'Please contact support',null,null,null,null,true);
        nlapiSendEmail(userid, userid, 'Error processing sub cases for case id ' + caseid, 'Please contact support', null, null, null, null, true);
        var parent_case = nlapiLoadRecord('supportcase', caseid);
        parent_case.setFieldValue('custevent_clgx_locked_case', 'F')
        nlapiSubmitRecord(parent_case)
        nlapiLogExecution('DEBUG', 'Parent CAse----', JSON.stringify(parent_case));
        if (error.getDetails != undefined) {
            nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else {
            nlapiLogExecution('ERROR', 'Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function getFacilityTimeOffset(facilityName) {
	var timeOffset = "";
	
	if(facilityName) {
		if((facilityName.indexOf("ASH") > -1) || (facilityName.indexOf("COL") > -1) 
				|| (facilityName.indexOf("JAX") > -1) || (facilityName.indexOf("MTL") > -1) 
				|| (facilityName.indexOf("NNJ") > -1) || (facilityName.indexOf("TOR") > -1)) {
			timeOffset = 2;
		} else if((facilityName.indexOf("DAL") > -1) || (facilityName.indexOf("MIN") > -1)) {
			timeOffset = 1;
		} else if(facilityName.indexOf("VAN") > -1) {
			timeOffset = -1;
		} else if(facilityName.indexOf("HQ") > -1) {
			timeOffset = 0;
		}
	} else {
		timeOffset = 0; //Denver time
	}
	
	return timeOffset;
}

function getFacilityTimeSuffix(facilityName) {
	var suffix = "";
	
	if(facilityName) {
		if((facilityName.indexOf("ASH") > -1) || (facilityName.indexOf("COL") > -1) 
				|| (facilityName.indexOf("JAX") > -1) || (facilityName.indexOf("MTL") > -1) 
				|| (facilityName.indexOf("NNJ") > -1) || (facilityName.indexOf("TOR") > -1)) {
			suffix = "(EST)";
		} else if((facilityName.indexOf("DAL") > -1) || (facilityName.indexOf("MIN") > -1)) {
			suffix = "(CST)";
		} else if(facilityName.indexOf("VAN") > -1) {
			suffix = "(PST)";
		} else if(facilityName.indexOf("HQ") > -1) {
			suffix = "(MST)";
		}
	} else {
		suffix = "(MST)"; //Denver time
	}
	
	return suffix;
}

function buildICSCalendarLink(paramObject) {
	if(paramObject) {
		var encryptionKey = nlapiGetContext().getSetting("SCRIPT", "custscript_clgx2_gmics_encryption_key").toString();
		var icsBaseURL       = "https://1337135.extforms.netsuite.com/app/site/hosting/scriptlet.nl?script=2063&deploy=1&compid=1337135&h=807ea89f6e8161eeda25&event=";
		
		var eventObject         = {};
		eventObject["title"]    = paramObject.title.toString();
		eventObject["details"]  = paramObject.details.toString();
		eventObject["location"] = paramObject.location.toString();
		eventObject["begin"]    = paramObject.begin.toString();
		eventObject["end"]      = paramObject.end.toString();
		
		return (icsBaseURL + encodeURIComponent(CryptoJS.AES.encrypt(JSON.stringify(eventObject), encryptionKey).toString()));
	}
}


function buildGoogleCalendarLink(paramObject) {
	if(paramObject) {
		var baseURL       = "http://www.google.com/calendar/event?action=TEMPLATE";
		
		var eventObject         = {};
		eventObject["title"]    = paramObject.title.toString();
		eventObject["details"]  = paramObject.details.toString();
		eventObject["location"] = paramObject.location.toString();
		eventObject["begin"]    = paramObject.begin.toString();
		eventObject["end"]      = paramObject.end.toString();
		
		return (baseURL + "&dates=" + encodeURIComponent(eventObject.begin + "/" + eventObject.end) 
				+ "&text=" + encodeURIComponent(eventObject.title) 
				+ "&location=" + encodeURIComponent(eventObject.location) 
				+ "&details=" + encodeURIComponent(eventObject.details));
	}
}