/**
 * @date 9/1/2017
 * @author NetSuite 
 */
function beforeSubmit(type, form)
{
	nlapiLogExecution("DEBUG", "beforeSubmit", JSON.stringify(type));
	nlapiLogExecution("DEBUG", "beforeSubmit", nlapiGetRecordId());
        //set the custom field which tags the record if it is a customer record or not
	
    nlapiSetFieldValue('custevent_is_cust','F');
    
    //created a try catch statement for the company field
    //the company field can be a partner or a project record
    //this will prevent errors from a mismatch load of record type
    try {
       //retrieves the company field and loads the record
       var newRec  = nlapiGetNewRecord();
       var custid  = newRec.getFieldValue('company');
       var custRec = nlapiLoadRecord('customer', custid);

       nlapiLogExecution("DEBUG", "beforeSubmit", "custid: " + custid);
       //nlapiLogExecution("ERROR", "beforeSubmit", "custrec: " + JSON.stringify(custRec));
       
       nlapiSetFieldValue('custevent_is_cust', 'T');
               
       if (custRec != null && custRec != '') {
          //checks the email value on the customer record 
          //checks the checkbox if it's blank else it wlll be unchecked
          var email = custRec.getFieldValue('email');
          
          nlapiLogExecution("DEBUG", "beforeSubmit", "email: " + email);
          
          if (email != '' && email != null) {
        	 nlapiLogExecution("DEBUG", "beforeSubmit", "email processed");
             nlapiSetFieldValue('custevent_cust_email_blank', 'F');
          } else {
        	  nlapiLogExecution("DEBUG", "beforeSubmit", "email not processed");
             nlapiSetFieldValue('custevent_cust_email_blank', 'T');
          }
       }
    } catch (e) {
       // check if the record error is a mismatch on the load
       // log the id of the record and company field for audit purposes
       // else it will throw an error in the UI with the actual error and log it
       if (e.code == 'SSS_RECORD_TYPE_MISMATCH') {
          nlapiLogExecution('AUDIT', 'Company is not a customer record', 'Case ID: ' + nlapiGetRecordId() + ' Company ID: ' + custid);
       } else {
          nlapiLogExecution('ERROR', 'Error Details', e);
          //throw e.code + ' : ' + e.details;
       }
    }
}


function afterSubmit(type, form)
{
        //retrieves the new record and checks if it's a customer type of company
        var newRec = nlapiGetNewRecord();
        
        nlapiLogExecution("DEBUG", "afterSubmit", "is cust: " + newRec.getFieldValue('custevent_is_cust'));
        
        if (newRec.getFieldValue('custevent_is_cust') == 'T') {
           //retrieves the id of the company field for the submission of the field
           var custid = newRec.getFieldValue('company');
        
           nlapiLogExecution("DEBUG", "afterSubmit", "custid: " + custid);
           
           //checks the tag if the email is true 
           //then it will update the email field on the customer record as blank
           nlapiLogExecution("DEBUG", "afterSubmit", "email blank: " + nlapiGetFieldValue('custevent_cust_email_blank'));
           
           if (nlapiGetFieldValue('custevent_cust_email_blank') == 'F') {
        	   nlapiLogExecution("DEBUG", "afterSubmit", "email blank");
               nlapiSubmitField('customer',custid,'email','');	   
           }
        }
}