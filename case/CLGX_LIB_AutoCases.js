//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_LIB_AutoCases.js
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		3/22/2017
//-------------------------------------------------------------------------------------------------

function get_facility_hour (hour,facilityid){
	
	var marketid =  parseInt(nlapiLookupField('customrecord_cologix_facility', facilityid, 'custrecord_clgx_facilty_market'));

	switch(marketid) {
    case 7: // Vancouver
    	var fhour =  hour;
        break;
    case 1: // Cologix HQ
    	var fhour =  hour + 1;
        break;
    case 2: // Dallas
    	var fhour =  hour + 2;
        break;
    default:
    	var fhour =  hour + 3;
	}
	
	if(fhour > 24){
		fhour = fhour - 24;
	}
	return fhour;
}

function get_schedule (schedid){

	var rec = nlapiLoadRecord('customrecord_clgx_auto_cases', schedid);
	
	var facilities = [];
	var strfac = rec.getFieldValues('custrecord_facility') || [];
	for ( var j = 0; j < strfac.length; j++ ) {
		facilities.push(parseInt(strfac[j]));
	}
	facilities = _.compact(_.sortBy(facilities, function(num){ return num; }));
	
	var hour = (parseInt(rec.getFieldValue('custrecord_clgx_start_time')) - 1) || 1;
	var fhour = get_facility_hour (parseInt(moment().format('H')), facilities[0]);
	
	var months = [];
	var strmonths = rec.getFieldValues('custrecord_clgx_scheduled_months') || [];
	for ( var j = 0; j < strmonths.length; j++ ) {
		months.push(parseInt(strmonths[j]));
	}
	months = _.compact(_.sortBy(months, function(num){ return num; }));
	
	var dates = [];
	var strdates = rec.getFieldValues('custrecord_clgx_scheduled_dates') || [];
	for ( var j = 0; j < strdates.length; j++ ) {
		dates.push(parseInt(strdates[j]));
	}
	dates = _.compact(_.sortBy(dates, function(num){ return num; }));
	
	var days = [];
	var strdays = rec.getFieldValues('custrecord_clgx_scheduled_days') || [];
	for ( var j = 0; j < strdays.length; j++ ) {
		days.push(parseInt(strdays[j]));
	}
	days = _.compact(_.sortBy(days, function(num){ return num; }));
	
	var weeks = [];
	var strweeks = rec.getFieldValues('custrecord_clgx_scheduled_weekdays') || [];
	for ( var j = 0; j < strweeks.length; j++ ) {
		weeks.push(parseInt(strweeks[j]));
	}
	weeks = _.compact(_.sortBy(weeks, function(num){ return num; }));
	
	var schedule = ({
		"id": parseInt(schedid) || 0,
		"today":{
			"date": moment().format('M/D/YYYY'),
			"year": parseInt(moment().format('YYYY')),
	    	"month": moment().format('M'),
	    	"day": moment().format('D'),
	    	"hour": parseInt(moment().format('H')),
	    	"fhour": fhour
		},
		"hour": hour,
		"lastrun": '',
		"nextrun": '',
		"newlastrun": '',
		"newnextrun": '',
		"alldates": [],
		"cases": [],
		
		"facilities": facilities,
		"months": months,
		"dates": dates,
		"days": days,
		"weeks": weeks,
		
		"weekday": rec.getFieldValue('custrecord_clgx_weekday') || 'F',
		"start": rec.getFieldValue('custrecord_start_date') || '',
		"end": rec.getFieldValue('custrecord_clgx_end_date') || '',
		"last": rec.getFieldValue('custrecord_clgx_last_run') || '',
		"next": rec.getFieldValue('custrecord_clgx_next_run') || ''
	});
	
	// find days to run for this year & next
	var mdates = [];
	if(schedule.last){
		mdates.push(moment(schedule.last)); // add the last run  date  - trick to manage the once a year run case
	}
	
	
	for ( var j = 0;  j < 2; j++ ) {
		for ( var m = 0;  m < schedule.months.length; m++ ) {
			
			var first = schedule.months[m] + '/1/' + (schedule.today.year + j);
			
			if(schedule.dates.length > 0){ // specific dates of each month for this year and next
				for ( var d = 0;  d < schedule.dates.length; d++ ) {
					var result = moment(schedule.months[m] + '/' + schedule.dates[d] + '/' + (schedule.today.year + j));
					if(moment(schedule.months[m] + '/' + schedule.dates[d] + '/' + (schedule.today.year + j), 'M/D/YYYY', true).isValid()){
						mdates.push(result);
					}
				}
			} else {
				
				for ( var w = 0;  w < schedule.weeks.length; w++ ) {
					
					if(schedule.weekday == 'T'){ // 1st, 2nd, 3rd, 4th, last weekday of each month for this year and next

						if(schedule.weeks[w] == 5){ //last weekday of the month
							result = moment(first).endOf('month');
							while (result.isoWeekday() > 5) {
							    result.subtract(1, 'days');
							}
						} else {
							result = moment(first);
							while (result.isoWeekday() > 5) {
							    result.add(1, 'days');
							}
							result.add((schedule.weeks[w] - 1), 'days');
						}
						mdates.push(result);
						
					} else { // M,T,W,T,F,S,S of each month for this year and next
						
						for ( var d = 0;  d < schedule.days.length; d++ ) {
							
							// if Sunday change 7 to 0
							var day = schedule.days[d];
							if(day == 7){
								day = 0;
							}
							
							if(schedule.weeks[w] < 5){
								// start from the first day of the month and find the first configured day M-F
								result = moment(first);
								while (result.day() != day) {
								    result.add(1, 'days');
								}
								// add weeks to the founded day
								result.add((schedule.weeks[w] - 1), 'weeks');
							} else { // == 5  - last specified day (M-F) of the week
								result = moment(first).endOf('month');
								// find the day going back from the last day of the month
								while (result.day() != day) {
								    result.subtract(1, 'days');
								}
							}
							// verify if date is still on the right month and year
							var resultyear = parseInt(result.format('YYYY'));
							var resultmonth = parseInt(result.format('M'));
							if(resultyear == (schedule.today.year + j) && resultmonth == schedule.months[m]){
								mdates.push(result);
							}
						}
					}
				}
			}
		}
	}
	mdates = _.sortBy(mdates, function(num){ return num; });
	
	var alldates = [];
	for ( var d = 0;  d < mdates.length; d++ ) {
		var mday = mdates[d].format('M/D/YYYY');
		alldates.push(mday);
	}
	alldates = _.uniq(alldates);
	
	var ndx = -1;
	var mtoday = moment(schedule.today.date);
	for ( var d = 0;  d < alldates.length; d++ ) {
		//nlapiLogExecution('DEBUG','debug', '| alldates = ' + alldates[d] + ' | ');
		if(mtoday.isAfter(moment(alldates[d]))){
			ndx = d; 
			//break;
		}
	}
	if(ndx > -1){
		schedule.lastrun = alldates[ndx];
		schedule.nextrun = alldates[ndx+1];
		schedule.newlastrun = alldates[ndx+1];
		schedule.newnextrun = alldates[ndx+2];
	} else {
		schedule.lastrun = null;
		schedule.nextrun = alldates[0];
		schedule.newlastrun = null;
		schedule.newnextrun = alldates[0];
	}
	schedule.alldates = alldates;
	
	// cases to be created
	for ( var f = 0;  f < schedule.facilities.length; f++ ) {
		var obj = {
				"time": (parseInt(rec.getFieldValue('custrecord_clgx_start_time')) - 1) || 1,
		    	"customer": parseInt(rec.getFieldValue('custrecord_customer')) || 0,
		    	"contact": parseInt(rec.getFieldValue('custrecord_contact')) || 0,
		    	"priority": parseInt(rec.getFieldValue('custrecord_priority')) || 3,
	            "type": parseInt(rec.getFieldValue('custrecord_clgx_case_type')) || 1,
	            "subtype": parseInt(rec.getFieldValue('custrecord_clgx_subcase_type')) || 6,
		    	"subject": rec.getFieldValue('custrecord_case_subject') || 0,
		    	"body": rec.getFieldValue('custrecord_case_body') || 0,
		    	"group": parseInt(rec.getFieldValue('custrecord_assigned_to_group')) || 0,
		    	"employee": parseInt(rec.getFieldValue('custrecord_assigned_to_employee')) || 0,
		    	"facility": schedule.facilities[f]
		};
		schedule.cases.push(obj);
	}
	
	return schedule;

}
