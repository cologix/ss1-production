nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_AutoCase.js
//	Script Name:	CLGX_SU_AutoCase.js
//	Script Id:		customscript_clgx_su_autocase
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Invoice
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		03/20/2017
//-------------------------------------------------------------------------------------------------

function beforeSubmit(type, form) {
	try {

		
	}
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }

}

function afterSubmit(type, form) {
	try {
		
		var currentContext = nlapiGetContext();
		
		if (currentContext.getExecutionContext() == 'userinterface' && (type=='edit' || type == "create")){
			
			var schedid = nlapiGetRecordId();
			var schedule = get_schedule(schedid);
			
			if(schedule.lastrun == null){
				schedule.lastrun = schedule.nextrun;
			}
			
			var rec = nlapiLoadRecord('customrecord_clgx_auto_cases', schedid);
			rec.setFieldValue('custrecord_clgx_last_run', schedule.lastrun); 
			rec.setFieldValue('custrecord_clgx_next_run', schedule.nextrun);
			
			var strdates = schedule.alldates.join(',');
			var trimmed = strdates.substring(0, 3999)
			rec.setFieldValue('custrecord_clgx_all_run_dates', trimmed); 
			
			nlapiSubmitRecord(rec, false,true);
			
		 }
		
	}
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
