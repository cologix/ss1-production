nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_Create_Notifications_A100_120.js
//	Script Name:	CLGX_RL_Create_Notifications_A100_120
//	Script Id:		customscript_clgx_create_not_a100_120
//	Script Runs:	On Server
//	Script Type:	RESTLET
//	Deployments:	Case
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Released:		11/18/2016
//-------------------------------------------------------------------------------------------------

function put(datain) {
    try {
        nlapiLogExecution('DEBUG','Scheduled Script','|--------------------STARTED---------------------|');
//------------- Begin Section 1 -----------------------------------------------------------------------------------
//Find the case to send notifications A100 -15 mins
//-----------------------------------------------------------------------------------------------------------------

        var arrColumns = new Array();
        var arrFilters = new Array();

        var searchCases = nlapiSearchRecord('customrecord_clgx_queue_cases_neo_15min', 'customsearch_clgx_ss_cases_neo_120', arrFilters, arrColumns);
        if(searchCases!=null) {
            for (var i = 0; searchCases != null && i < searchCases.length; i++) {

                var searchCase = searchCases [i];
                var columns = searchCase.getAllColumns();
                var datecreated = searchCase.getValue(columns[0]);
                var momcreate = new moment(datecreated);
                var internalid = searchCase.getValue(columns[1]);
                var caseId=searchCase.getValue(columns[2]);
                var end=new moment();
                var minutes =end.diff(momcreate, 'minutes');
                var casenumber=searchCase.getValue(columns[3]);
                var contact=searchCase.getValue(columns[4]);
                if (minutes >= 120 && minutes <= 122) {

                    // nlapiSendEmail(206211, 2406, 'Please check this case. 2H Notification', 'Case ID: '+caseId, null, null, null, null);
                    var recordCase=nlapiLoadRecord('supportcase',caseId);
                    // nlapiSendEmail(206211, 2406, 'Case #'+searchMessage.getValue(columns1[2])+' - '+searchMessage.getValue(columns1[3]), searchMessage.getValue(columns1[1]), null, null, null, null);
                    recordCase.setFieldValue('messagenew', 'T');
                    recordCase.setFieldValue('internalonly', 'F');
                    recordCase.setFieldValue('emailform', 'T');
                    recordCase.setFieldValue('htmlmessage', 'T');
                    recordCase.setFieldValue('outgoingmessage', 'Project Neo 2 Hours Update Reminder: Case #'+casenumber+ ' needs an update sent to the customer per contractual requirements.');
                    recordCase.setFieldValue('subject', 'Case #'+casenumber+' : Project Neo 1 Hour Update Reminder');
                    recordCase.setFieldValue('recipient', contact);
                    if(contact!=null && contact!='') {
                        var email = nlapiLookupField('contact', contact, 'email') || '';
                    }
                    else{
                        var email='';
                    }
                    recordCase.setFieldValue('recipientemail', email);
                    nlapiSubmitRecord(recordCase, true, true);
                    var momcr=moment().format('MM/DD/YYYY h:mm:ss a');
                    nlapiSubmitField('customrecord_clgx_queue_cases_neo_15min', internalid,'custrecord_clgx_queue_neo_15min_2h', momcr);

                }



            }
        }


//---------- End Section 1 ------------------------------------------------------------------------------------------------


        nlapiLogExecution('DEBUG','Scheduled Script','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            //throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
          //  throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}