//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_Realtionship.js
//	Script Name:	CLGX_CR_Realtionship
//	Script Id:		customscript_clgx_cr_relationship
//	Script Runs:	Client
//	Script Type:	Client Script
//	Deployments:	Relationship
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		04/20/2016
//-------------------------------------------------------------------------------------------------

function fieldChanged(type, name, linenum){
	try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 12/23/2011
// Details:	Make email mandatory when role is billing
//-------------------------------------------------------------------------------------------------
		
		if (name == 'contactrole'){
			var roleID = nlapiGetFieldValue('contactrole');
			if (roleID == '1'){
				nlapiSetFieldValue('custentity_clgx_is_billing_contact','T');
			}
			else{
				nlapiSetFieldValue('custentity_clgx_is_billing_contact','F');
			}
		}
		
		if (name == 'custentity_clgx_contact_reset_password'){
			
			nlapiSetFieldValue('custentity_clgx_portal_password',generatePassword(8,false));
		}

//------------- End Section 1 -------------------------------------------------------------------	

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
