//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Relationship.js
//	Script Name:	CLGX_SU_Relationship
//	Script Id:		customscript_clgx_su_relationship
//	Script Type:	User Event Script
//	Deployments:	Relationship
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		04/16/2016
//-------------------------------------------------------------------------------------------------

function afterSubmit (type) {
	try {
		
		var context = nlapiGetContext();
		
		var entity = nlapiGetFieldValue('custrecord_clgx_relationship_entity');
		var contact = nlapiGetFieldValue('custrecord_clgx_relationship_contact');
		var role = nlapiGetFieldValue('custrecord_clgx_relationship_role');
		
		var rectype = nlapiLookupField('entity', entity, 'type', true);
		rectype = rectype.toLowerCase();
		
		if ((context.getExecutionContext() == 'userinterface') && (type == 'create' || type == 'edit')) {
			if(role){
				nlapiAttachRecord('contact', contact, rectype, entity, {"role": role});
			}
			else{
				nlapiAttachRecord('contact', contact, rectype, entity);
			}
		}
		if ((context.getExecutionContext() == 'userinterface') && (type == 'delete')) {
			nlapiDetachRecord('contact', contact, rectype, entity);
			nlapiSetRedirectURL('RECORD', 'contact', contact);
		}
		
	} 
	catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	}
}
