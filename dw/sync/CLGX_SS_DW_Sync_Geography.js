//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DW_Sync_Geography.js
//	Script Name:	CLGX_SS_DW_Sync_Geography
//	Script Id:		customscript_clgx_ss_dw_sync_geography
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------

function scheduled_dw_sync_geography(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
    	
    	var clocation = [];
    	var search = nlapiSearchRecord('customrecord_clgx_consolidate_locations', 'customsearch_clgx_dw_clocation');
		for ( var i = 0; search != null && i < search.length; i++ ) {
			var columns = search[i].getAllColumns();
			clocation.push({
				"clocation_id":parseInt(search[i].getValue(columns[0])),
				"clocation":search[i].getValue(columns[1]),
				"inactive":search[i].getValue(columns[2]),
				"external_id":search[i].getValue(columns[3])
			});
		}
		
		var subsidiary = [];
    	var search = nlapiSearchRecord('subsidiary', 'customsearch_clgx_dw_subsidiary');
		for ( var i = 0; search != null && i < search.length; i++ ) {
			var columns = search[i].getAllColumns();
			subsidiary.push({
				"subsidiary_id":parseInt(search[i].getValue(columns[0])),
				"subsidiary":search[i].getValue(columns[1]),
				"inactive":search[i].getValue(columns[2]),
				"external_id":search[i].getValue(columns[3]),
				"parent_id":parseInt(search[i].getValue(columns[4])),
				"currency_id":parseInt(search[i].getValue(columns[5])),
				"address1":search[i].getValue(columns[6]),
				"address2":search[i].getValue(columns[7]),
				"address3":search[i].getValue(columns[8]),
				"city":search[i].getValue(columns[9]),
				"zip":search[i].getValue(columns[10]),
				//"state_id":search[i].getValue(columns[11]),
				"state":search[i].getText(columns[12]),
				//"country_id":search[i].getValue(columns[13]),
				"country":search[i].getText(columns[14]),
				"elimination":search[i].getValue(columns[15])
			});
		}
		/*
		var state_ids = _.compact(_.uniq(_.pluck(subsidiary, 'state_id')));
		var native_state = [];
		for ( var i = 0; i < state_ids.length; i++ ) {
			var obj = _.find(subsidiary, function(arr){ return (arr.state_id == state_ids[i]) ; });
			native_state.push({
				"state_id":obj.state_id,
				"state":obj.state
			})
		}
		var country_ids = _.compact(_.uniq(_.pluck(subsidiary, 'country_id')));
		var native_country = [];
		for ( var i = 0; i < country_ids.length; i++ ) {
			var obj = _.find(subsidiary, function(arr){ return (arr.country_id == country_ids[i]) ; });
			native_country.push({
				"country_id":obj.country_id,
				"country":obj.country
			})
		}
		subsidiary = _.map(subsidiary, function(obj) { return _.omit(obj, ['state', 'country']);});
		*/
		
		var location = [];
    	var search = nlapiSearchRecord('location', 'customsearch_clgx_dw_location');
		for ( var i = 0; search != null && i < search.length; i++ ) {
			var columns = search[i].getAllColumns();
			var location_id = parseInt(search[i].getValue(columns[0]));
			var rec = nlapiLoadRecord('location', location_id);
			var subsidiray_id = parseInt(rec.getFieldValue('subsidiary'));
			location.push({
				"location_id":location_id,
				"location":search[i].getValue(columns[1]),
				"inactive":search[i].getValue(columns[2]),
				"external_id":search[i].getValue(columns[3]),
				"subsidiary_id":subsidiray_id,
				"rlocation_id":parseInt(search[i].getValue(columns[4])),
				"address1":search[i].getValue(columns[5]),
				"address2":search[i].getValue(columns[6]),
				"address3":search[i].getValue(columns[7]),
				"city":search[i].getValue(columns[8]),
				"zip":search[i].getValue(columns[9]),
				//"state_id":search[i].getValue(columns[10]),
				"state":search[i].getText(columns[10]),
				//"country_id":search[i].getValue(columns[11]),
				"country":search[i].getText(columns[11]),
				"phone":search[i].getValue(columns[12])
			});
		}

		var location_junction_clocation = [];
    	var search = nlapiSearchRecord('customrecord_clgx_consolidate_locations', 'customsearch_clgx_dw_loc_junction_cloc');
		for ( var i = 0; search != null && i < search.length; i++ ) {
			var columns = search[i].getAllColumns();
			location_junction_clocation.push({
				"clocation_id":parseInt(search[i].getValue(columns[0])),
				"location_id":search[i].getValue(columns[1])
			});
		}
		
		var facility = [];
    	var search = nlapiSearchRecord('customrecord_cologix_facility', 'customsearch_clgx_dw_facility');
		for ( var i = 0; search != null && i < search.length; i++ ) {
			var columns = search[i].getAllColumns();
			facility.push({
				"facility_id":parseInt(search[i].getValue(columns[0])),
				"facility":search[i].getValue(columns[1]),
				"inactive":search[i].getValue(columns[2]),
				"external_id":search[i].getValue(columns[3]),
				"subsidiary_id":0,
				"customer_id":parseInt(search[i].getValue(columns[5])),
				"market_id":parseInt(search[i].getValue(columns[6])),
				"location_id":parseInt(search[i].getValue(columns[7])),
				"building_clli_id":parseInt(search[i].getValue(columns[8])),
				"building_number_id":parseInt(search[i].getValue(columns[9])),
				"address1":search[i].getValue(columns[10]),
				"address2":search[i].getValue(columns[11]),
				"city":search[i].getValue(columns[12]),
				"zip":search[i].getValue(columns[13]),
				//"state_id":parseInt(search[i].getValue(columns[14])),
				"state":search[i].getText(columns[15]),
				//"country_id":parseInt(search[i].getValue(columns[16])),
				"country":search[i].getText(columns[17])
			});
		}
		for ( var i = 0; i < facility.length; i++ ) {
			var obj = _.find(location, function(arr){ return (arr.location_id == facility[i].location_id) ; });
			if(obj){
				facility[i].subsidiary_id = obj.subsidiary_id;
			}
		}
		
		/*
		var state_ids = _.compact(_.uniq(_.pluck(facility, 'state_id')));
		var state = [];
		for ( var i = 0; i < state_ids.length; i++ ) {
			var obj = _.find(facility, function(arr){ return (arr.state_id == state_ids[i]) ; });
			state.push({
				"state_id":obj.state_id,
				"state":obj.state
			})
		}
		var country_ids = _.compact(_.uniq(_.pluck(facility, 'country_id')));
		var country = [];
		for ( var i = 0; i < country_ids.length; i++ ) {
			var obj = _.find(facility, function(arr){ return (arr.country_id == country_ids[i]) ; });
			country.push({
				"country_id":obj.country_id,
				"country":obj.country
			})
		}
		facility = _.map(facility, function(obj) { return _.omit(obj, ['state', 'country']);});
		*/

		var department = [];
    	var search = nlapiSearchRecord('department', 'customsearch_clgx_dw_department');
		for ( var i = 0; search != null && i < search.length; i++ ) {
			var columns = search[i].getAllColumns();
			var department_id = parseInt(search[i].getValue(columns[0]));
			var rec = nlapiLoadRecord('department', department_id);
			var parent_id = parseInt(rec.getFieldValue('parent'));
			var dep_subs = rec.getFieldValue('subsidiary');
			department.push({
				"department_id":department_id,
				"department":search[i].getValue(columns[1]),
				"inactive":search[i].getValue(columns[2]),
				"external_id":search[i].getValue(columns[3]),
				"parent_id":parent_id,
				"subsidiary_id":parseInt(dep_subs[0])
			});
		}
		
		var records = {
				"proc": 'CLGX_DW_SYNC_GEOGRAPHY',
				"enviro": 'dev',
				"clocation": clocation,
    			//"native_state": native_state,
    			//"native_country": native_country,
    			"subsidiary": subsidiary,
    			"market": get_list('customlist_clgx_market'),
    			"rlocation": get_list('customlist_clgx_reporting_location'),
    			"location": location,
    			"location_junction_clocation": location_junction_clocation,
    			"building_number": get_list('customlist_cologix_facility_bldgnumber'),
    			"building_clli": get_list('customlist_cologix_facility_clli_lst'),
    			//"state": state,
    			//"country": country,
    			"facility": facility,
    			"department": department
    	};
		var strJSON = JSON.stringify(records);
		
		try {
			var url = 'https://lucee-nnj2.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm';
			var requestURL = nlapiRequestURL(url, strJSON, {'Content-type': 'application/json'}, null, 'POST');
			if(requestURL.body == 0){
				nlapiSendEmail(71418,71418,'Updated - ' + records.proc + ' - ' + records.enviro, strJSON ,null,null,null,null);
			} else {
				nlapiSendEmail(71418,71418,'Error - ' + records.proc + ' - ' + records.enviro, requestURL.body ,null,null,null,null);
			}
		}
		catch (error) {
			nlapiSendEmail(71418,71418,'Error - ' + error.getCode() + ' - ' + records.proc + ' - ' + records.enviro, strJSON ,null,null,null,null);
		}
		
        
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function get_list(list) {
	var arr = [];
	var columns = new Array();
	columns.push(new nlobjSearchColumn('internalid',null,null).setSort(false));
	columns.push(new nlobjSearchColumn('name',null,null));
	var filters = new Array();
	var search = nlapiSearchRecord(list, null, filters, columns);
	for ( var i = 0; search != null && i < search.length; i++ ) {
		arr.push({
			"id": parseInt(search[i].getValue('internalid',null,null)),
			"name": search[i].getValue('name',null,null)
		})
	}
	return arr;
}

