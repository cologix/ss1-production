//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DW_Sync_Entities.js
//	Script Name:	CLGX_SS_DW_Sync_Entities
//	Script Id:		customscript_clgx_ss_dw_sync_entities
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		9/1/2017
//-------------------------------------------------------------------------------------------------

function scheduled_dw_sync_entities(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        

		var columns = [];
    	columns.push(new nlobjSearchColumn('status',null,'GROUP').setSort(false));
		var filters = [];
		filters.push(new nlobjSearchFilter("status",null,"noneof",'@NONE@'));
		var search = nlapiSearchRecord('customer', null, filters, columns);
		var customer_status = [];
		for ( var i = 0; search != null && i < search.length; i++ ) {
			customer_status.push({
				'status_id':parseInt(search[i].getValue('status',null,'GROUP')),
				'status':search[i].getText('status',null,'GROUP')
			});
		}
    	
		var columns = [];
    	columns.push(new nlobjSearchColumn('category',null,'GROUP').setSort(false));
		var filters = [];
		filters.push(new nlobjSearchFilter("category",null,"noneof",'@NONE@'));
		var search = nlapiSearchRecord('customer', null, filters, columns);
		var customer_segment = [];
		for ( var i = 0; search != null && i < search.length; i++ ) {
			customer_segment.push({
				'segment_id':parseInt(search[i].getValue('category',null,'GROUP')),
				'segment':search[i].getText('category',null,'GROUP')
			});
		}
    	
		var columns = [];
    	columns.push(new nlobjSearchColumn('language',null,'GROUP').setSort(false));
		var filters = [];
		filters.push(new nlobjSearchFilter("language",null,"noneof",'@NONE@'));
		var search = nlapiSearchRecord('customer', null, filters, columns);
		var language = [];
		for ( var i = 0; search != null && i < search.length; i++ ) {
			language.push({
				'language_id':search[i].getValue('language',null,'GROUP'),
				'language':search[i].getText('language',null,'GROUP')
			});
		}
		
		var columns = [];
    	columns.push(new nlobjSearchColumn('category',null,'GROUP').setSort(false));
		var filters = [];
		filters.push(new nlobjSearchFilter("category",null,"noneof",'@NONE@'));
		var search = nlapiSearchRecord('vendor', null, filters, columns);
		var vendor_category = [];
		for ( var i = 0; search != null && i < search.length; i++ ) {
			vendor_category.push({
				'category_id':parseInt(search[i].getValue('category',null,'GROUP')),
				'category':search[i].getText('category',null,'GROUP')
			});
		}


    	var employee = [];
    	var search = nlapiSearchRecord('employee', 'customsearch_clgx_dw_employee');
		for ( var i = 0; search != null && i < search.length; i++ ) {
			var columns = search[i].getAllColumns();
			employee.push({
				"employee_id":parseInt(search[i].getValue(columns[0])),
				"employee":search[i].getValue(columns[1]),
				"inactive":search[i].getValue(columns[2]),
				"external_id":search[i].getValue(columns[3]),
				"first_name":search[i].getValue(columns[4]),
				"last_name":search[i].getValue(columns[5]),
				"title":search[i].getValue(columns[6]),
				"subsidiary_id":parseInt(search[i].getValue(columns[7])),
				"department_id":parseInt(search[i].getValue(columns[8])),
				"location_id":parseInt(search[i].getValue(columns[9])),
				"facility_id":parseInt(search[i].getValue(columns[10])),
				"supervisor_id":parseInt(search[i].getValue(columns[11])),
				"hired_by_id":parseInt(search[i].getValue(columns[12])),
				"support_rep":search[i].getValue(columns[13]),
				"sales_rep":search[i].getValue(columns[14]),
				"email":search[i].getValue(columns[15]),
				"phone":search[i].getValue(columns[16]),
				"mobile":search[i].getValue(columns[17]),
				"skype":search[i].getValue(columns[18])
			});
		}

		var records = {
				"proc": 'CLGX_DW_SYNC_ENTITIES',
				"enviro": 'dev',
				"customer_status": customer_status,
				"customer_segment": customer_segment,
				"language": language,
				"vendor_category": vendor_category,
				"vendor_payment_type": get_list('customlist_clgx_pymnt_types'),
				"contact_role": get_list('contactrole'),
				"contact_secondary_role": get_list('customlist_clgx_secondary_role'),
				"employee": employee
				
    	};
    	var strJSON = JSON.stringify(records);
		
		try {
			var url = 'https://lucee-nnj2.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm';
			var requestURL = nlapiRequestURL(url, strJSON, {'Content-type': 'application/json'}, null, 'POST');
			if(requestURL.body == 0){
				nlapiSendEmail(71418,71418,'Updated - ' + records.proc + ' - ' + records.enviro, strJSON ,null,null,null,null);
			} else {
				nlapiSendEmail(71418,71418,'Error - ' + records.proc + ' - ' + records.enviro, requestURL.body ,null,null,null,null);
			}
		}
		catch (error) {
			nlapiSendEmail(71418,71418,'Error - ' + error.getCode() + ' - ' + records.proc + ' - ' + records.enviro, strJSON ,null,null,null,null);
		}
		
        
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function get_list(list) {
	var arr = [];
	var columns = new Array();
	columns.push(new nlobjSearchColumn('internalid',null,null).setSort(false));
	columns.push(new nlobjSearchColumn('name',null,null));
	var filters = new Array();
	var search = nlapiSearchRecord(list, null, filters, columns);
	for ( var i = 0; search != null && i < search.length; i++ ) {
		arr.push({
			"id": parseInt(search[i].getValue('internalid',null,null)),
			"name": search[i].getValue('name',null,null)
		})
	}
	return arr;
}

