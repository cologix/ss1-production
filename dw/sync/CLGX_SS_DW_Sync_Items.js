//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DW_Sync_Items.js
//	Script Name:	CLGX_SS_DW_Sync_Items
//	Script Id:		customscript_clgx_ss_dw_sync_items
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		9/1/2017
//-------------------------------------------------------------------------------------------------

function scheduled_dw_sync_items(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        

		var classification = [];
    	var search = nlapiSearchRecord('classification', 'customsearch_clgx_dw_item_class');
		for ( var i = 0; search != null && i < search.length; i++ ) {
			var columns = search[i].getAllColumns();
			var class_id = parseInt(search[i].getValue(columns[0]));
			var rec = nlapiLoadRecord('classification', class_id);
			var parent_id = parseInt(rec.getFieldValue('parent'));
			var dep_subs = rec.getFieldValue('subsidiary');
			classification.push({
				"class_id":class_id,
				"class":search[i].getValue(columns[1]),
				"inactive":search[i].getValue(columns[2]),
				"external_id":search[i].getValue(columns[3]),
				"parent_id":parent_id,
				"subsidiary_id":parseInt(dep_subs[0])
			});
		}
		
    	var item = [];
    	var search = nlapiSearchRecord('item', 'customsearch_clgx_dw_item');
		for ( var i = 0; search != null && i < search.length; i++ ) {
			var columns = search[i].getAllColumns();
			item.push({
				"item_id":parseInt(search[i].getValue(columns[0])),
				"item":search[i].getValue(columns[1]),
				"inactive":search[i].getValue(columns[2]),
				"external_id":search[i].getValue(columns[3]),
				"item":search[i].getValue(columns[4]),
				"class_id":parseInt(search[i].getValue(columns[5])),
				"category_id":parseInt(search[i].getValue(columns[6])),
				"amps_id":parseInt(search[i].getValue(columns[7])),
				"volts_id":parseInt(search[i].getValue(columns[8])),
				"item_standard_id":parseInt(search[i].getValue(columns[9]))
			});
		}

		var records = {
				"proc": 'CLGX_DW_SYNC_ITEMS',
				"enviro": 'dev',
    			"item_standard": get_list('customlist_clgx_list_standard'),
    			"item_category": get_list('customlist_cologix_item_category_list'),
    			"volts": get_list('customlist_cologix_power_volts_lst'),
    			"amps": get_list('customlist_cologix_power_amps_lst'),
    			"item_class": classification,
    			"item": item
    	};
    	var strJSON = JSON.stringify(records);
		
		try {
			var url = 'https://lucee-nnj2.dev.nac.net/dw/sync/clgx_ns2dw_sync.cfm';
			var requestURL = nlapiRequestURL(url, strJSON, {'Content-type': 'application/json'}, null, 'POST');
			if(requestURL.body == 0){
				nlapiSendEmail(71418,71418,'Updated - ' + records.proc + ' - ' + records.enviro, strJSON ,null,null,null,null);
			} else {
				nlapiSendEmail(71418,71418,'Error - ' + records.proc + ' - ' + records.enviro, requestURL.body ,null,null,null,null);
			}
		}
		catch (error) {
			nlapiSendEmail(71418,71418,'Error - ' + error.getCode() + ' - ' + records.proc + ' - ' + records.enviro, strJSON ,null,null,null,null);
		}
		
        
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function get_list(list) {
	var arr = [];
	var columns = new Array();
	columns.push(new nlobjSearchColumn('internalid',null,null).setSort(false));
	columns.push(new nlobjSearchColumn('name',null,null));
	var filters = new Array();
	var search = nlapiSearchRecord(list, null, filters, columns);
	for ( var i = 0; search != null && i < search.length; i++ ) {
		arr.push({
			"id": parseInt(search[i].getValue('internalid',null,null)),
			"name": search[i].getValue('name',null,null)
		})
	}
	return arr;
}

