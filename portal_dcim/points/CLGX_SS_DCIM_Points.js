nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_Points.js
//	Script Name:	CLGX_SS_DCIM_Points
//	Script Id:		customscript_clgx_ss_dcim_points
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		6/20/2014
//-------------------------------------------------------------------------------------------------
function scheduled_dcim_points(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Date:	6/20/2014
// Details:	Replicate Points
//-----------------------------------------------------------------------------------------------------------------

        var context = nlapiGetContext();
        var initialTime = moment();
	        
		// pull out update devices queue
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,null));
		arrColumns.push(new nlobjSearchColumn('name',null,null));
		arrColumns.push(new nlobjSearchColumn('externalid',null,null));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_device_updated",null,"is",'T'));
		arrFilters.push(new nlobjSearchFilter("externalid",null,"noneof",'@NONE@'));
		arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
		var searchDevices = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);

		var arrDevicesInternalIds = new Array();
		var arrDevicesNames = new Array();
		var arrDevicesExternalIds = new Array();
		for ( var i = 0; searchDevices != null && i < searchDevices.length; i++ ) {
			arrDevicesInternalIds.push(searchDevices[i].getValue('internalid',null,null));
			arrDevicesNames.push(searchDevices[i].getValue('name',null,null));
			arrDevicesExternalIds.push(searchDevices[i].getValue('externalid',null,null));
		}

		for ( var i = 0; arrDevicesInternalIds != null && i < arrDevicesInternalIds.length; i++ ) {
			
	    	var startExecTime = moment();
	    	var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);
	        if ( (context.getRemainingUsage() <= 5000 || totalMinutes > 50) && (i+1) < arrDevicesInternalIds.length ){
                var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                if ( status == 'QUEUED' ) {
             	   nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                    break; 
                }
	        }
	        
	    	// request new points from OpenData each device at a time
			var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/devices/get_points_update.cfm?deviceid=' + arrDevicesExternalIds[i]);
			//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/getpointsupdate.cfm?deviceid=' + arrDevicesExternalIds[i]);
			var pointsJSON = requestURL.body;
			var arrNewPoints= JSON.parse( pointsJSON );
			
			// build new points arrays
			var arrNewExternalIds = new Array();
			var arrNewNames = new Array();
			var arrNewDayAVGs = new Array();
			for ( var j = 0; arrNewPoints != null && j < arrNewPoints.length; j++ ) {
				arrNewExternalIds.push(parseInt(arrNewPoints[j].POINTID));
				arrNewNames.push(arrNewPoints[j].NAME);
				arrNewDayAVGs.push(arrNewPoints[j].DAVG);
			}
	
			// pull out old points from Netsuite for the same device
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null));
			arrColumns.push(new nlobjSearchColumn('externalid',null,null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_device",null,"anyof",arrDevicesInternalIds[i]));
			arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
			var searchPoints = nlapiSearchRecord('customrecord_clgx_dcim_points', null, arrFilters, arrColumns);
			
			// build old points arrays
			var arrOldInternalIds = new Array();
			var arrOldExternalIds = new Array();
			for ( var j = 0; searchPoints != null && j < searchPoints.length; j++ ) {
		        arrOldInternalIds.push(parseInt(searchPoints[j].getValue('internalid',null,null)));
		        arrOldExternalIds.push(parseInt(searchPoints[j].getValue('externalid',null,null)));
			}
			
			var arrAdd = _.difference(arrNewExternalIds, arrOldExternalIds);
			var arrDelete = _.difference(arrOldExternalIds, arrNewExternalIds);
			var arrUpdate = _.intersection(arrNewExternalIds, arrOldExternalIds);
			
			nlapiLogExecution('DEBUG','Add/Delete/Update', '| Device - ' + arrDevicesNames[i] + ' / ' + arrDevicesInternalIds[i] + ' | Add - ' + arrAdd.length + ' | Delete - ' + arrDelete.length + ' | Update - ' + arrUpdate.length + ' |');
			
			
			for ( var j = 0; arrDelete != null && j < arrDelete.length; j++ ) {
				var internalid = arrOldInternalIds[_.indexOf(arrOldExternalIds, arrDelete[j])];
				var oldname = nlapiLookupField('customrecord_clgx_dcim_points', internalid, 'name');
				var delname = (oldname + '-DEL').substring(0,299);
				nlapiSubmitField('customrecord_clgx_dcim_points', internalid, ['name','custrecord_clgx_dcim_points_deleted'], [delname,'T']);
			}
			
			for ( var j = 0; arrUpdate != null && j < arrUpdate.length; j++ ) {
				
				//var newPoint = _.find(arrNewPoints, function(arr){ return (arr.POINTID == arrUpdate[j]);});
				var newname = arrNewNames[_.indexOf(arrNewExternalIds, arrUpdate[j])];
				if(newname != null && newname != ''){
					var newname = newname.substring(0,299);
				}
				var newavg = arrNewDayAVGs[_.indexOf(arrNewExternalIds, arrUpdate[j])];
				var internalid = arrOldInternalIds[_.indexOf(arrOldExternalIds, arrUpdate[j])];
				
				var fields = ['name', 'custrecord_clgx_dcim_points_day_avg'];
				var values = [newname, newavg];
				nlapiSubmitField('customrecord_clgx_dcim_points', internalid, fields, values);
				
			}

			
			for ( var j = 0; arrAdd != null && j < arrAdd.length; j++ ) {
				
				var newname = arrNewNames[_.indexOf(arrNewExternalIds, arrAdd[j])];
				if(newname != null && newname != ''){
					var newname = newname.substring(0,299);
				}
				var newavg = arrNewDayAVGs[_.indexOf(arrNewExternalIds, arrAdd[j])];
				//var newPoint = _.find(arrNewPoints, function(arr){ return (arr.POINTID == arrAdd[j]);});
				
				var arrColumns = new Array();
				arrColumns.push(new nlobjSearchColumn('internalid',null,null));
				var arrFilters = new Array();
				arrFilters.push(new nlobjSearchFilter("name",null,"is",newname));
				arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_device",null,"anyof",arrDevicesInternalIds[i]));
				arrFilters.push(new nlobjSearchFilter("isinactive",null,"noneof",'@NONE@'));
				var searchRecords = nlapiSearchRecord('customrecord_clgx_dcim_points', null, arrFilters, arrColumns);
				
				if(searchRecords == null){
		            var record = nlapiCreateRecord('customrecord_clgx_dcim_points');
		            record.setFieldValue('name', newname);
		            record.setFieldValue('externalid', Number(arrAdd[j]).toString());
		            record.setFieldValue('custrecord_clgx_dcim_points_device', arrDevicesInternalIds[i]);
		            record.setFieldValue('custrecord_clgx_dcim_points_day_avg', parseFloat(newavg));
		            record.setFieldValue('custrecord_clgx_dcim_points_deleted', 'F');
		            var idRec = nlapiSubmitRecord(record, false, true);
				}
			}

			nlapiSubmitField('customrecord_clgx_dcim_devices', arrDevicesInternalIds[i], 'custrecord_clgx_dcim_device_updated', 'F');
			
            var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            var index = i + 1;
            nlapiLogExecution('DEBUG','SO ', ' | recordID =  ' + arrDevicesExternalIds[i] + ' | Index = ' + index + ' of ' + arrDevicesInternalIds.length + ' | Usage - '+ usageConsumtion + '  |');
		}

//-----------------------------------------------------------------------------------------------------------------
		nlapiLogExecution('DEBUG','Usage', context.getRemainingUsage()); 
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

