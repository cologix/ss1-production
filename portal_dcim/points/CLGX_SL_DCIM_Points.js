nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_Points.js
//	Script Name:	CLGX_SL_DCIM_Points
//	Script Id:		customscript_clgx_sl_dcim_points
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/14/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=300&deploy=1  
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_points (request, response){
	try {
		var deviceid = request.getParameter('deviceid');
		var powerid = request.getParameter('powerid');
		var html = '';
		
		if(deviceid > 0){
			
			var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/devices/get_points.cfm?deviceid=' + deviceid);
			//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/getpoints.cfm?deviceid=' + deviceid);
			var pointsJSON = requestURL.body;
			/*
			if(powerid > 0){
				
				var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', powerid);
				var pointConsumtion = recPower.getFieldValue('custrecord_clgx_dcim_points_consumption');
				var pointDemand = recPower.getFieldValue('custrecord_clgx_dcim_points_demand');
				var arrPointsCurrent = recPower.getFieldValues('custrecord_clgx_dcim_points_current');
				
				var arrPoints = new Array();
				if(arrPointsCurrent.length > 0){
					for ( var i = 0; arrPointsCurrent != null && i < arrPointsCurrent.length; i++ ) {
						if(_.indexOf(arrPoints, arrPointsCurrent[i]) < 0){
							arrPoints.push(arrPointsCurrent[i]);
						}
					}
				}
				if(pointConsumtion != null && pointConsumtion != '' && _.indexOf(arrPoints, pointConsumtion) < 0){
					arrPoints.push(pointConsumtion);
				}
				if(pointDemand != null && pointDemand != '' && _.indexOf(arrPoints, pointDemand) < 0){
					arrPoints.push(pointDemand);
				}
				
				//nlapiSendEmail(432742,71418,'arrPoints',JSON.stringify(arrPoints),null,null,null,null);
				
				var arrExternalIDs = new Array();
				for ( var i = 0; arrPoints != null && i < arrPoints.length; i++ ) {
					var externalid = nlapiLookupField('customrecord_clgx_dcim_points', arrPoints[i], 'externalid');
					arrExternalIDs.push(externalid);
				}
				
				var arrDevicePoints = JSON.parse( pointsJSON );
				var arrPowerPoints =  new Array();
				
				for ( var i = 0; arrDevicePoints != null && i < arrDevicePoints.length; i++ ) {
					for ( var j = 0; arrExternalIDs != null && j < arrExternalIDs.length; j++ ) {
						if(arrDevicePoints[i].ID == arrExternalIDs[j]){
							arrPowerPoints.push(arrDevicePoints[i]);
						}
					}
				}
				pointsJSON = JSON.stringify(arrPowerPoints);
			}
			*/
			var objFile = nlapiLoadFile(1296745);
			html = objFile.getValue();
			html = html.replace(new RegExp('{pointsJSON}','g'), pointsJSON);
		}

		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
