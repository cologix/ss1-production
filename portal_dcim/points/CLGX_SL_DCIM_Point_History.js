nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_Point_History.js
//	Script Name:	CLGX_SL_DCIM_Point_History
//	Script Id:		customscript_clgx_sl_dcim_point_history
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		08/20/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=333&deploy=1  
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_point_history (request, response){
	try {
		
		var pointid = request.getParameter('pointid');
		var html = '';
		
		if(pointid > 0){
			
			var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/devices/get_point_history_values.cfm?pointid=' + pointid);
			//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/get_point_history_values.cfm?pointid=' + pointid);
			var strJSON = requestURL.body;
			var arrValues = JSON.parse(strJSON);
			
			var arrMinutes = arrValues[0];
			var arrHours = arrValues[1];
			var arrDays = arrValues[2];
			var arrMonths = arrValues[3];
			
			var arrMin = new Array();
			for ( var i = 0; arrMinutes != null && i < arrMinutes.length; i++ ) {
				var objMin = new Object();
				objMin["hour"] = moment(arrMinutes[i].STIME).format('YYYY-MM-DD HH');
				objMin["minute"] = moment(arrMinutes[i].STIME).format('YYYY-MM-DD HH:mm');
				objMin["val"] = arrMinutes[i].VAL;
				arrMin.push(objMin);
			}
			
			var objFile = nlapiLoadFile(1500405);
			html = objFile.getValue();
			html = html.replace(new RegExp('{minutesJSON}','g'), JSON.stringify(arrMin));
			html = html.replace(new RegExp('{hoursJSON}','g'), JSON.stringify(arrHours));
			html = html.replace(new RegExp('{daysJSON}','g'), JSON.stringify(arrDays));
			html = html.replace(new RegExp('{monthsJSON}','g'), JSON.stringify(arrMonths));
		}

		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
