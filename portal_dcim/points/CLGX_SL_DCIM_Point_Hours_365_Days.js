nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_Point_Hours_365_Days.js
//	Script Name:	CLGX_SL_DCIM_Point_Hours_365_Days
//	Script Id:		customscript_clgx_sl_dcim_point_hrs_365
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/14/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=528&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_point_hours_365_days (request, response){
	try {
		
		var pointid = request.getParameter('pointid');
		var html = '';
		
		var start = moment().subtract('days', 365).format('YYYY-MM-DD');
    	var end = moment().format('YYYY-MM-DD');
    	
		if(pointid > 0){
			
			var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/devices/get_point_hours_365_days.cfm?pointid=' + pointid + '&start=' + start + '&end=' + end);
			//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/get_point_hours_365_days.cfm?pointid=' + pointid + '&start=' + start + '&end=' + end);
			var strJSON = requestURL.body;
			var arrValues = JSON.parse(strJSON);
			
			var objFile = nlapiLoadFile(2920771);
			html = objFile.getValue();
			html = html.replace(new RegExp('{hoursJSON}','g'), JSON.stringify(arrValues));
		}

		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
