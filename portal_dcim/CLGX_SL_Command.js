nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Command.js
//	Script Name:	CLGX_SL_Command
//	Script Id:		customscript_clgx_sl_command
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/15/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=724&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_clgx_command (request, response){
	try {

		var objFile = nlapiLoadFile(4410899);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{strMenu}','g'), get_menus());
		response.write( html );
	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}

function get_menus(){
	
	var bar = ['<img valign=bottom width=96px height=30px src=/core/media/media.nl?id=2&c=1337135&h=120db2ec4539ba4117d6&whence= border=0/>',{
		"text": "Customers",
		"xtype": 'splitbutton',
		"iconCls": 'fa fa-users fa-lg blue1',
        "menu": [{
    		"text": "By Name",
    		"iconCls": 'fa fa-user-secret fa-lg blue2',
	        "handler": 'menu'
	    },{
    		"text": "By Location",
    		"iconCls": 'fa fa-building-o fa-lg blue2',
	        "handler": 'menu'
	    }]
	},{
		"text": "DCIM",
		"xtype": 'splitbutton',
		"iconCls": 'fa fa-cogs fa-lg blue1',
        "menu": [{
    		"text": "FAMs",
    		//"xtype": 'splitbutton',
    		"iconCls": 'fa fa-cog fa-lg blue1',
            "menu": get_menu_fams()
    	},{
    		"text": "Power Chain",
    		//"xtype": 'splitbutton',
    		"iconCls": 'fa fa-sitemap fa-lg blue1',
            "menu": get_menu_pwr_trees(307)
    	},{
    		"text": "BCM Usage",
    		//"xtype": 'splitbutton',
    		"iconCls": 'fa fa-bolt fa-lg blue1',
            "menu": get_menu_pwr_trees(439)
    	},{
    		"text": "Power Reports",
    		//"xtype": 'splitbutton',
    		"iconCls": 'fa fa-plug fa-lg blue1',
            "menu": [{
        		"text": "Draw Exceeded (Amps)",
        		"iconCls": 'fa fa-plug fa-lg blue2',
    	        "handler": 'menu'
    	    },{
        		"text": "Peak Exceeded (kW)",
        		"iconCls": 'fa fa-plug fa-lg blue2',
    	        "handler": 'menu'
    	    }]
    	}]
	},{
		"text": "Services",
		//"xtype": 'splitbutton',
		"iconCls": 'fa fa-th fa-lg blue1',
        //"menu": []
	}];
	
	var strBar = JSON.stringify(bar);
	strBar = strBar.replace(new RegExp('"menu"','g'),'menu');
	
	return strBar;
	
}


function get_menu_fams(){
	
	var columns = [];
	columns.push(new nlobjSearchColumn('custrecord_cologix_location_subsidiary','custrecord_assetfacility','GROUP').setSort(false));
	columns.push(new nlobjSearchColumn('custrecord_clgx_facilty_market','custrecord_assetfacility','GROUP').setSort(false));
	var filters = [];
	var searchMarkets = nlapiSearchRecord('customrecord_ncfar_asset', null, filters, columns);
	
	var markets = [];
	for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
		
		var subsidiaryid = parseInt(searchMarkets[i].getValue('custrecord_cologix_location_subsidiary','custrecord_assetfacility','GROUP'));
		var marketid = parseInt(searchMarkets[i].getValue('custrecord_clgx_facilty_market','custrecord_assetfacility','GROUP'));
		var market = searchMarkets[i].getText('custrecord_clgx_facilty_market','custrecord_assetfacility','GROUP');
		
		var columns = [];
		columns.push(new nlobjSearchColumn('internalid','custrecord_assetfacility','GROUP'));
		columns.push(new nlobjSearchColumn('name','custrecord_assetfacility','GROUP').setSort(false));
		var filters = [];
		filters.push(new nlobjSearchFilter('custrecord_clgx_facilty_market','custrecord_assetfacility',"anyof",marketid));
		var searchFacilities = nlapiSearchRecord('customrecord_ncfar_asset', null, filters, columns);
		
		var facilities = new Array();
		for ( var j = 0; searchFacilities != null && j < searchFacilities.length; j++ ) {
			
			var facilityid = searchFacilities[j].getValue('internalid','custrecord_assetfacility','GROUP');
			var facility = searchFacilities[j].getValue('name','custrecord_assetfacility','GROUP');
			
			facilities.push({
				"text": facility,
				"node": parseInt(searchFacilities[j].getValue('internalid','custrecord_assetfacility','GROUP')),
				"bread": '/DCIM/FAMs/' + market + '/',
				"script": 308,
				"root": searchFacilities[j].getValue('name','custrecord_assetfacility','GROUP'),
				"iconCls": 'fa fa-building-o fa-lg blue2',
		        "handler": 'menu'
			});
		}
		if(subsidiaryid == 2789){
			var faicon = 'flag-icon flag-icon-ca';
		} else {
			var faicon = 'flag-icon flag-icon-us';
		}
		markets.push({
			"text": market,
			"iconCls": faicon,
	        "menu": facilities
		});
	}
	return markets;
}


function get_menu_pwr_trees(script){
	
	if(script == 307){
		var bread = '/DCIM/Power Chain/';
	} else {
		var bread = '/DCIM/BCM Usage/';
	}
	
	var columns = [];
	columns.push(new nlobjSearchColumn('custrecord_cologix_location_subsidiary','custrecord_clgx_dcim_device_facility','GROUP').setSort(false));
	columns.push(new nlobjSearchColumn('custrecord_clgx_facilty_market','custrecord_clgx_dcim_device_facility','GROUP').setSort(false));
	var filters = [];
	var searchMarkets = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, filters, columns);
	
	var markets = [];
	for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
		
		var subsidiaryid = parseInt(searchMarkets[i].getValue('custrecord_cologix_location_subsidiary','custrecord_clgx_dcim_device_facility','GROUP'));
		var marketid = parseInt(searchMarkets[i].getValue('custrecord_clgx_facilty_market','custrecord_clgx_dcim_device_facility','GROUP'));
		var market = searchMarkets[i].getText('custrecord_clgx_facilty_market','custrecord_clgx_dcim_device_facility','GROUP');
		
		var columns = [];
		columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_pwr_tree',null,'GROUP').setSort(false));
		var filters = [];
		filters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device_pwr_tree',null,"noneof",'@NONE@'));
		filters.push(new nlobjSearchFilter('custrecord_clgx_facilty_market','custrecord_clgx_dcim_device_facility',"anyof",marketid));
		var searchFacilities = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, filters, columns);
		
		var facilities = new Array();
		for ( var j = 0; searchFacilities != null && j < searchFacilities.length; j++ ) {
			
			var treeid = parseInt(searchFacilities[j].getValue('custrecord_clgx_dcim_device_pwr_tree',null,'GROUP'));
			
			var columns = [];
			columns.push(new nlobjSearchColumn('internalid',null,null).setSort(false));
			var filters = [];
			filters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device_root',null,"is",'T'));
			filters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device_pwr_tree',null,"anyof",treeid));
			filters.push(new nlobjSearchFilter('custrecord_clgx_facilty_market','custrecord_clgx_dcim_device_facility',"anyof",marketid));
			var searchRoots = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, filters, columns);
			var roots = [];
			for ( var k = 0; searchRoots != null && k < searchRoots.length; k++ ) {
				roots.push(parseInt(searchRoots[k].getValue('internalid',null,null)));
			}
			facilities.push({
				"text": searchFacilities[j].getText('custrecord_clgx_dcim_device_pwr_tree',null,'GROUP'),
				"treeid": treeid,
				"bread": bread + market + '/',
				"script": script,
				"root": roots.join(),
				"iconCls": 'fa fa-building-o blue2',
				"handler": 'menu'
			});
		}
		
		if(subsidiaryid == 2789){
			var faicon = 'flag-icon flag-icon-ca';
		} else {
			var faicon = 'flag-icon flag-icon-us';
		}
		markets.push({
				"text": market,
				"iconCls": faicon,
		        "menu": facilities
		});
	}
	return markets;
}