nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_Alarms.js
//	Script Name:	CLGX_SL_DCIM_Alarms
//	Script Id:		customscript_clgx_sl_dcim_alarms
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		06/24/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=306&deploy=1  
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_alarms (request, response){
    try {

    	//var requestURL = nlapiRequestURL('https://lucee-nnj2.dev.nac.net/odins/alarms/get_alarms.cfm');
    	var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/alarms/get_alarms.cfm');
        //var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/getalarms.cfm');
        //var requestURL = nlapiRequestURL('https://command1.nnj2.cologix.net:10313/alarms',null,{'Content-type': 'application/json'},null,'GET');
        var jsonAlarms = requestURL.body;
        var arrAlarms = JSON.parse( jsonAlarms );

        response.write( getAlarmsTreeJSON(arrAlarms));
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function getAlarmsTreeJSON(arrAlarms){
    var userid = nlapiGetUser();
    var arrAlarmsFAMs = new Array();
    for ( var i = 0; arrAlarms != null && i < arrAlarms.length; i++ ) {
        var objAlarmsFAM = new Object();

        
        
        objAlarmsFAM["deviceid"] = parseInt(arrAlarms[i].DEVICE_ID);
        objAlarmsFAM["devicename"] = arrAlarms[i].DEVICE_NAME;
        objAlarmsFAM["devicecode"] = arrAlarms[i].DEVICE_CODE;

        objAlarmsFAM["pointid"] = parseInt(arrAlarms[i].POINT_ID);
        objAlarmsFAM["pointname"] = arrAlarms[i].POINT_NAME;
        objAlarmsFAM["pointcode"] = arrAlarms[i].POINT_CODE;

        objAlarmsFAM["eventid"] = parseInt(arrAlarms[i].EVENT_ID);
        objAlarmsFAM["eventname"] = arrAlarms[i].EVENT_NAME;
        objAlarmsFAM["eventcode"] = arrAlarms[i].EVENT_CODE;
        objAlarmsFAM["eventresp"] = arrAlarms[i].RESPONSE;

        objAlarmsFAM["date"] = arrAlarms[i].DATE;
        objAlarmsFAM["severity"] = parseInt(arrAlarms[i].SEVERITY);

        objAlarmsFAM["famid"] = Number(arrAlarms[i].EXTERNAL_ID.substring(3));
        objAlarmsFAM["fam"] = arrAlarms[i].EXTERNAL_ID;
        objAlarmsFAM["userid"]=userid;
         
        /*
        objAlarmsFAM["deviceid"] = parseInt(arrAlarms[i].device_id);
        objAlarmsFAM["devicename"] = arrAlarms[i].device_name;
        objAlarmsFAM["devicecode"] = arrAlarms[i].device_code;

        objAlarmsFAM["pointid"] = parseInt(arrAlarms[i].point_id);
        objAlarmsFAM["pointname"] = arrAlarms[i].point_name;
        objAlarmsFAM["pointcode"] = arrAlarms[i].point_code;

        objAlarmsFAM["eventid"] = parseInt(arrAlarms[i].event_id);
        objAlarmsFAM["eventname"] = arrAlarms[i].event_name;
        objAlarmsFAM["eventcode"] = arrAlarms[i].event_code;
        objAlarmsFAM["eventresp"] = arrAlarms[i].response;

        objAlarmsFAM["date"] = arrAlarms[i].date;
        objAlarmsFAM["severity"] = parseInt(arrAlarms[i].severity);

        if(arrAlarms[i].external_id){
            objAlarmsFAM["famid"] = Number(arrAlarms[i].external_id.substring(3));
            objAlarmsFAM["fam"] = arrAlarms[i].external_id;
        }
        else{
            objAlarmsFAM["famid"] = 0;
            objAlarmsFAM["fam"] = '';
        }
        objAlarmsFAM["userid"]=userid;
        */
        
        
        arrAlarmsFAMs.push(objAlarmsFAM);
    }
    return JSON.stringify(arrAlarmsFAMs);
}
