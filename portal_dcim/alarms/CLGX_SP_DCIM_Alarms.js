nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SP_DCIM_Alarms.js
//	Script Name:	CLGX_SP_DCIM_Alarms
//	Script Id:		customscript_clgx_sp_dcim_alarms
//	Script Runs:	On Server
//	Script Type:	Portlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		7/7/2014
//-------------------------------------------------------------------------------------------------

function portlet_cpi_frame (portlet, column){
	portlet.setTitle('Command Alarms');
	var html = '<iframe name="alarms" id="alarms" src="/core/media/media.nl?id=1569532&c=1337135&h=c5e0be5de0e4a765a922&_xt=.html" height="300px" width="100%" frameborder="0" scrolling="no"></iframe>';
	//var html = '<iframe name="alarms" id="alarms" src="" height="300px" width="100%" frameborder="0" scrolling="no"></iframe>';
	portlet.setHtml(html);
}
