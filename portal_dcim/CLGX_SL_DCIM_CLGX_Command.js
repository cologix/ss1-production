//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_CLGX_Command.js
//	Script Name:	CLGX_SL_DCIM_CLGX_Command
//	Script Id:		customscript_clgx_sl_dcim_clgx_cmd
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		06/24/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=264&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_clgx_cmd (request, response){
	try {

		var objFile = nlapiLoadFile(1322278);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{strMenu}','g'), get_menus());
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function get_menus(){
	
	var arrFams = get_menu_fams();
	var arrChains = get_menu_pwr_trees(307);
	var arrUsages = get_menu_pwr_trees(465);
	
	var objBar = new Object();
	objBar["xtype"] = 'toolbar';
	
	var arrBarItems = new Array();
	
	// add menu FAMs =============================
	var objBarItem = new Object();
	objBarItem["text"] = '<i class="fa fa-cogs fa-lg SteelBlue2"></i> FAMs';
	//objBarItem["iconCls"] = 'fam';
	
		var objBarItemMenu = new Object();
		objBarItemMenu["xtype"] = 'menu';
		objBarItemMenu["items"] = arrFams;
	
	objBarItem["menu"] = objBarItemMenu;
	arrBarItems.push(objBarItem);
	
	// add menu Power Chain =============================
	var objBarItem = new Object();
	objBarItem["text"] = '<i class="fa fa-sitemap fa-lg SteelBlue2"></i> Power Chain';
	
		var objBarItemMenu = new Object();
		objBarItemMenu["xtype"] = 'menu';
		objBarItemMenu["items"] = arrChains;

	objBarItem["menu"] = objBarItemMenu;
	arrBarItems.push(objBarItem);
	
	// add menu BCM Usage =============================
	var objBarItem = new Object();
	objBarItem["text"] = '<i class="fa fa-bolt fa-lg SteelBlue2"></i> BCM Usage';
	//objBarItem["iconCls"] = 'capacity';
	
		var objBarItemMenu = new Object();
		objBarItemMenu["xtype"] = 'menu';
		objBarItemMenu["items"] = arrUsages;

	objBarItem["menu"] = objBarItemMenu;
	arrBarItems.push(objBarItem);

	// add menu Power Reports =============================
	var objBarItem = new Object();
	objBarItem["text"] = '<i class="fa fa-plug fa-lg SteelBlue2"></i> Power Reports';
	//objBarItem["iconCls"] = 'dcim';
	
	var objBarItemMenu = new Object();
	objBarItemMenu["xtype"] = 'menu';
	
	var arrReports = new Array();
	
	var objReport = new Object();
	objReport["text"] = '<i class="fa fa-plug SteelBlue1"></i> Draw Exceeded (Amps)';
	//objReport["iconCls"] = 'dcim';
	objReport["handler"] = 'openAmpsOverageReport';
	arrReports.push(objReport);
	
	var objReport = new Object();
	objReport["text"] = '<i class="fa fa-plug SteelBlue1"></i> Peak Exceeded (kW)';
	//objReport["iconCls"] = 'dcim';
	objReport["handler"] = 'openKWOverageReport';
	arrReports.push(objReport);
	
	objBarItemMenu["items"] = arrReports;
	
	objBarItem["menu"] = objBarItemMenu;
	arrBarItems.push(objBarItem);
	
	
	// add menu Services =============================
	var objBarItem = new Object();
	objBarItem["text"] = '<i class="fa fa-th fa-lg SteelBlue2"></i> Services';
	
	var objBarItemMenu = new Object();
	objBarItemMenu["xtype"] = 'menu';
	
	var arrServices = new Array();
	
	var objServices = new Object();
	objServices["text"] = '<i class="fa fa-user-secret SteelBlue1"></i> By Customer (All)';
	objServices["handler"] = 'openServicesByCustomersAll';
	arrServices.push(objServices);
	
	objBarItemMenu["items"] = arrServices;
	
	objBarItem["menu"] = objBarItemMenu;
	arrBarItems.push(objBarItem);
	
	
	// =============================
	
	objBar["items"] = arrBarItems;
	
	
	var strBar = JSON.stringify(objBar);
	strBar = strBar.replace(new RegExp('"openFAMs"','g'),'openFAMs');
	strBar = strBar.replace(new RegExp('"openTree"','g'),'openTree');
	strBar = strBar.replace(new RegExp('"openAmpsOverageReport"','g'),'openAmpsOverageReport');
	strBar = strBar.replace(new RegExp('"openKWOverageReport"','g'),'openKWOverageReport');
	strBar = strBar.replace(new RegExp('"openServicesByCustomersAll"','g'),'openServicesByCustomersAll');
	
	return strBar;
	
}


function get_menu_fams(){
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_location_subsidiary','custrecord_assetfacility','GROUP').setSort(false));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_facilty_market','custrecord_assetfacility','GROUP').setSort(false));
	var arrFilters = new Array();
	var searchMarkets = nlapiSearchRecord('customrecord_ncfar_asset', null, arrFilters, arrColumns);
	
	var arrMarkets = new Array();
	for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
		var subsidiaryid = parseInt(searchMarkets[i].getValue('custrecord_cologix_location_subsidiary','custrecord_assetfacility','GROUP'));
		var marketid = parseInt(searchMarkets[i].getValue('custrecord_clgx_facilty_market','custrecord_assetfacility','GROUP'));
		var market = searchMarkets[i].getText('custrecord_clgx_facilty_market','custrecord_assetfacility','GROUP');
		
		if(subsidiaryid == 2789){
			var faicon = 'flag-icon flag-icon-ca';
		}
		else{
			var faicon = 'flag-icon flag-icon-us';
		}
		var objMarket = new Object();
		objMarket["text"] = '<span class="' + faicon + '"></span> ' + market;

		var objMarketMenu = new Object();
		objMarketMenu["xtype"] = 'menu';
		
		
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid','custrecord_assetfacility','GROUP'));
		arrColumns.push(new nlobjSearchColumn('name','custrecord_assetfacility','GROUP').setSort(false));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_facilty_market','custrecord_assetfacility',"anyof",marketid));
		var searchFacilities = nlapiSearchRecord('customrecord_ncfar_asset', null, arrFilters, arrColumns);
		
		var arrFacilities = new Array();
		for ( var j = 0; searchFacilities != null && j < searchFacilities.length; j++ ) {
			var facilityid = searchFacilities[j].getValue('internalid','custrecord_assetfacility','GROUP');
			var facility = searchFacilities[j].getValue('name','custrecord_assetfacility','GROUP');
			
			var objFacility = new Object();
			objFacility["text"] = '<i class="fa fa-building-o SteelBlue1"></i> '  + facility;
			objFacility["node"] = facility;
			objFacility["root"] = facilityid;
			//objFacility["iconCls"] = 'fa fa-building-o SteelBlue1';
			objFacility["handler"] = 'openFAMs';
			arrFacilities.push(objFacility);
		}
		objMarketMenu["items"] = arrFacilities;
		objMarket["menu"] = objMarketMenu;
		arrMarkets.push(objMarket);
	}
	return arrMarkets;
}


function get_menu_pwr_trees(script){
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_location_subsidiary','custrecord_clgx_dcim_device_facility','GROUP').setSort(false));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_facilty_market','custrecord_clgx_dcim_device_facility','GROUP').setSort(false));
	var arrFilters = new Array();
	var searchMarkets = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
	
	var arrMarkets = new Array();
	for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {
		var subsidiaryid = parseInt(searchMarkets[i].getValue('custrecord_cologix_location_subsidiary','custrecord_clgx_dcim_device_facility','GROUP'));
		var marketid = parseInt(searchMarkets[i].getValue('custrecord_clgx_facilty_market','custrecord_clgx_dcim_device_facility','GROUP'));
		var market = searchMarkets[i].getText('custrecord_clgx_facilty_market','custrecord_clgx_dcim_device_facility','GROUP');
		
		if(subsidiaryid == 2789){
			var faicon = 'flag-icon flag-icon-ca';
		}
		else{
			var faicon = 'flag-icon flag-icon-us';
		}
		var objMarket = new Object();
		objMarket["text"] = '<span class="' + faicon + '"></span> ' + market;
		
		var objMarketMenu = new Object();
		objMarketMenu["xtype"] = 'menu';
		
		
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_pwr_tree',null,'GROUP').setSort(false));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device_pwr_tree',null,"noneof",'@NONE@'));
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_facilty_market','custrecord_clgx_dcim_device_facility',"anyof",marketid));
		var searchFacilities = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
		
		var arrFacilities = new Array();
		for ( var j = 0; searchFacilities != null && j < searchFacilities.length; j++ ) {
			var treeid = parseInt(searchFacilities[j].getValue('custrecord_clgx_dcim_device_pwr_tree',null,'GROUP'));
			var tree = searchFacilities[j].getText('custrecord_clgx_dcim_device_pwr_tree',null,'GROUP');
			
			var objFacility = new Object();
			objFacility["treeid"] = treeid;
			//objFacility["text"] = tree;
			objFacility["text"] = '<i class="fa fa-building-o SteelBlue1"></i> ' + tree;
			objFacility["node"] = tree;
			
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null).setSort(false));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device_root',null,"is",'T'));
			arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device_pwr_tree',null,"anyof",treeid));
			arrFilters.push(new nlobjSearchFilter('custrecord_clgx_facilty_market','custrecord_clgx_dcim_device_facility',"anyof",marketid));
			var searchRoots = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
			var arrRoots = new Array();
			for ( var k = 0; searchRoots != null && k < searchRoots.length; k++ ) {
				arrRoots.push(parseInt(searchRoots[k].getValue('internalid',null,null)));
			}
			
			objFacility["root"] = arrRoots.join();
			
			//objFacility["iconCls"] = 'location';
			objFacility["script"] = script;
			objFacility["handler"] = 'openTree';
			arrFacilities.push(objFacility);
			
		}
		objMarketMenu["items"] = arrFacilities;
		
		objMarket["menu"] = objMarketMenu;
		
		arrMarkets.push(objMarket);
	}
	return arrMarkets;
	
	
	
}