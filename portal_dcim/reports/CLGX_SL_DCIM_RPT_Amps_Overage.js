nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_RPT_Amps_Overage.js
//	Script Name:	CLGX_SL_Test
//	Script Id:		customscript_clgx_sl_dcim_rpt_amps_over
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		02/24/2015
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_rpt_amps_over (request, response){
    try {

    	var powers = [];
    	var search = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_powers_usg_out');
    	var result = search.runSearch();
		result.forEachResult(function(row) {
			
			var name = (row.getText('mainname', 'CUSTRECORD_POWER_CIRCUIT_SERVICE_ORDER', null)).split(":") || '';
			var customer = (name[name.length-1]).trim();
			
			var name = (row.getText('custrecord_cologix_power_service', null, null)).split(":") || '';
			var service = (name[name.length-1]).trim();
			
			powers.push({
					"customerid": parseInt(row.getValue('mainname', 'CUSTRECORD_POWER_CIRCUIT_SERVICE_ORDER', null)) || 0,
					"customer": customer,
					"nodeid": parseInt(row.getValue('internalid', null, null)) || 0,
					"node": row.getValue('name', null, null) || '',
					"nodetype": 'power',
					"soid": parseInt(row.getValue('custrecord_power_circuit_service_order', null, null)) || 0,
					"so": row.getText('custrecord_power_circuit_service_order', null, null) || '',
					"serviceid": parseInt(row.getValue('custrecord_cologix_power_service', null, null)) || 0,
					"service": service,
					"spaceid": parseInt(row.getValue('custrecord_cologix_power_space', null, null)) || 0,
					"space": row.getText('custrecord_cologix_power_space', null, null) || '',
					"facilityid": parseInt(row.getValue('custrecord_cologix_power_facility', null, null)) || 0,
					"facility": row.getText('custrecord_cologix_power_facility', null, null) || '',
					"deviceid": parseInt(row.getValue('custrecord_clgx_dcim_device', null, null)) || 0,
					"device": row.getText('custrecord_clgx_dcim_device', null, null) || '',
					"volts": row.getText('custrecord_cologix_power_volts', null, null) || 0,
					"circuit": row.getText('custrecord_cologix_power_amps', null, null) || '',
					"amps": parseFloat(row.getValue('custrecord_clgx_dcim_sum_day_amp_a_max', null, null)) || 0,
					
					"pairid": parseInt(row.getValue('custrecord_clgx_dcim_pair_power', null, null)) || 0,
					"pair": row.getText('custrecord_clgx_dcim_pair_power', null, null) || '',
					"pspaceid": parseInt(row.getValue('custrecord_cologix_power_space', 'CUSTRECORD_CLGX_DCIM_PAIR_POWER', null)) || 0,
					"pspace": row.getText('custrecord_cologix_power_space', 'CUSTRECORD_CLGX_DCIM_PAIR_POWER', null) || '',
					"pdeviceid": parseInt(row.getValue('custrecord_clgx_dcim_device', 'CUSTRECORD_CLGX_DCIM_PAIR_POWER', null)) || 0,
					"pdevice": row.getText('custrecord_clgx_dcim_device', 'CUSTRECORD_CLGX_DCIM_PAIR_POWER', null) || '',
					"pvolts": row.getText('custrecord_cologix_power_volts', 'CUSTRECORD_CLGX_DCIM_PAIR_POWER', null) || 0,
					"pcircuit": row.getText('custrecord_cologix_power_amps', 'CUSTRECORD_CLGX_DCIM_PAIR_POWER', null) || '',
					"pamps": parseFloat(row.getValue('custrecord_clgx_dcim_sum_day_amp_a_max', 'CUSTRECORD_CLGX_DCIM_PAIR_POWER', null)) || 0,
					
					"sum": parseFloat(row.getValue('custrecord_clgx_dcim_sum_day_amp_ab_max', null, null)) || 0,
					"usage": parseFloat(row.getValue('custrecord_clgx_dcim_sum_day_amp_ab_usg', null, null)) || 0,
					"days": parseInt(row.getValue('custrecord_clgx_dcim_points_occurrences', null, null)) || 0,
					"email": row.getValue('custrecord_clgx_dcim_points_email_date', null, null) || 0,
					"faicon": "plug",
	                "leaf": true
			});
			return true;
		});
		var customerids = _.uniq(_.pluck(powers, 'customerid'));
    	
		var customers = [];
		for ( var i = 0; customerids != null && i < customerids.length; i++ ) {
			
			var pwr = _.find(powers, function(arr){ return (arr.customerid == customerids[i]);});
			var pwrs = _.filter(powers, function(arr){
				return (arr.customerid == customerids[i]);
			});
			pwrs = _.map(pwrs, function(obj) { return _.omit(obj, ['customerid','customer']); });
			
			customers.push({
				"nodeid": pwr.customerid,
				"node": pwr.customer,
				"nodetype": 'customer',
				"faicon": "user-secret",
				"expanded": true,
                "leaf": false,
                "children": pwrs
			});

		}
		customers = _.sortBy(customers, function(obj){ return obj.node;});

		var tree = {
				"text": ".",
				"children": customers
		};
		
		
    	var file = nlapiLoadFile(2169804);
		var html = file.getValue();
		html = html.replace(new RegExp('{jsonTree}','g'), JSON.stringify(tree));
		response.write( html );

	    
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
