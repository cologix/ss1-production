nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_Power_Reports.js
//	Script Name:	CLGX_SL_DCIM_Power_Reports
//	Script Id:		customscript_clgx_sl_dcim_pwr_rpt
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/14/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=299&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_pwr_rpts (request, response){
	try {

		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,null));
		arrColumns.push(new nlobjSearchColumn('name',null,null));
		var arrFilters = new Array();
		var searchReports = nlapiSearchRecord('customrecord_clgx_dcim_rpt_cust_pwr', null, arrFilters, arrColumns);
		var arrRPTs = new Array();
		var arrRPTsIDs = new Array();
		for ( var i = 0; searchReports != null && i < searchReports.length; i++ ) {
			var reportid = parseInt(searchReports[i].getValue('internalid',null,null));
			var report = searchReports[i].getValue('name',null,null);
			var objRPT = new Object();
			objRPT["reportid"] = reportid;
			objRPT["report"] = report;
			arrRPTs.push(objRPT);
			arrRPTsIDs.push(reportid);
		}
		
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_report',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_customer',null,null));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrRPTsIDs));
		var searchCustomers = nlapiSearchRecord('customrecord_clgx_dcim_rpt_cust_customer', null, arrFilters, arrColumns);
		var arrCUSTs = new Array();
		var arrCUSTsIDs = new Array();
		for ( var i = 0; searchCustomers != null && i < searchCustomers.length; i++ ) {
			var reportid = parseInt(searchCustomers[i].getValue('custrecord_clgx_dcim_rpt_cust_report',null,null));
			var customerid = parseInt(searchCustomers[i].getValue('custrecord_clgx_dcim_rpt_cust_customer',null,null));
			var customer = searchCustomers[i].getText('custrecord_clgx_dcim_rpt_cust_customer',null,null);
			//var customer = (_.last((searchCustomers[i].getText('custrecord_clgx_dcim_rpt_cust_customer',null,null)).split(':'))).trim();
			var objCUST = new Object();
			objCUST["reportid"] = reportid;
			objCUST["customerid"] = customerid;
			objCUST["customer"] = customer;
			arrCUSTs.push(objCUST);
			arrCUSTsIDs.push(customerid);
		}
		_.uniq(arrCUSTsIDs);
		
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_service',null,'GROUP'));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("parent",'custrecord_cologix_power_service',"anyof",arrCUSTsIDs));
		var searchServices = nlapiSearchRecord('customrecord_clgx_power_circuit', null, arrFilters, arrColumns);
		var arrSERVs = new Array();
		var arrSERVsIDs = new Array();
		for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
			var serviceid = parseInt(searchServices[i].getValue('custrecord_cologix_power_service',null,'GROUP'));
			var service = (_.last((searchServices[i].getText('custrecord_cologix_power_service',null,'GROUP')).split(':'))).trim();
			var customerid = nlapiLookupField('job', serviceid, 'customer');
			var objSERV = new Object();
			objSERV["customerid"] = customerid;
			objSERV["serviceid"] = serviceid;
			objSERV["service"] = service;
			arrSERVs.push(objSERV);
			arrSERVsIDs.push(serviceid);
		}
		_.uniq(arrSERVsIDs);
		
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_service',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_power_circuit_service_order',null,null));
		arrColumns.push(new nlobjSearchColumn('internalid',null,null));
		arrColumns.push(new nlobjSearchColumn('name',null,null));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_service",null,"anyof",arrSERVsIDs));
		var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', null, arrFilters, arrColumns);
		var arrPWRs = new Array();
		var arrPWRsIDs = new Array();
		for ( var i = 0; searchPowers != null && i < searchPowers.length; i++ ) {
			var serviceid = parseInt(searchPowers[i].getValue('custrecord_cologix_power_service',null,null));
			var soid = parseInt(searchPowers[i].getValue('custrecord_power_circuit_service_order',null,null));
			//var so = searchPowers[i].getText('custrecord_power_circuit_service_order',null,null);
			var so = (_.last((searchPowers[i].getText('custrecord_power_circuit_service_order',null,null)).split('#'))).trim();
			var powerid = parseInt(searchPowers[i].getValue('internalid',null,null));
			var power = searchPowers[i].getValue('name',null,null);
			var objPWR = new Object();
			objPWR["serviceid"] = serviceid;
			objPWR["soid"] = soid;
			objPWR["so"] = so;
			objPWR["powerid"] = powerid;
			objPWR["power"] = power;
			arrPWRs.push(objPWR);
			arrPWRsIDs.push(powerid);
		}
		_.uniq(arrPWRsIDs);
		
		
		var objTree = new Object();
		objTree["text"] = '.';
		var arrReports = new Array();
		for ( var i = 0; arrRPTs != null && i < arrRPTs.length; i++ ) {
			var objReport = new Object();
			objReport["node"] = arrRPTs[i].report;
			objReport["nodeid"] = arrRPTs[i].reportid;
			
			var arrREPsCUSTs = _.filter(arrCUSTs, function(arr){
		        return (arr.reportid == arrRPTs[i].reportid);
			});
			var arrCustomers = new Array();
			for ( var j = 0; arrREPsCUSTs != null && j < arrREPsCUSTs.length; j++ ) {
				var objCustomer = new Object();
				objCustomer["node"] = arrREPsCUSTs[j].customer;
				objCustomer["nodeid"] = arrREPsCUSTs[j].customerid;
				
				var arrCUSTsSERVs = _.filter(arrSERVs, function(arr){
			        return (arr.customerid == arrREPsCUSTs[j].customerid);
				});
				var arrServices = new Array();
				for ( var k = 0; arrCUSTsSERVs != null && k < arrCUSTsSERVs.length; k++ ) {
					var objService = new Object();
					objService["node"] = arrCUSTsSERVs[k].service;
					objService["nodeid"] = arrCUSTsSERVs[k].serviceid;
					
					var arrSERVsPWRs = _.filter(arrPWRs, function(arr){
				        return (arr.serviceid == arrCUSTsSERVs[k].serviceid);
					});
					var arrPowers = new Array();
					for ( var l = 0; arrSERVsPWRs != null && l < arrSERVsPWRs.length; l++ ) {
						var objPower = new Object();
						objPower["node"] = arrSERVsPWRs[l].power;
						objPower["nodeid"] = arrSERVsPWRs[l].powerid;
						objPower["so"] = arrSERVsPWRs[l].so;
						objPower["soid"] = arrSERVsPWRs[l].soid;
						objPower["iconCls"] = 'power';
						objPower["leaf"] = true;
						arrPowers.push(objPower);
					}
					objService["children"] = arrPowers;
					objService["expanded"] = false;
					objService["iconCls"] = 'service';
					objService["leaf"] = false;
					arrServices.push(objService);
				}
				objCustomer["children"] = arrServices;
				objCustomer["expanded"] = false;
				objCustomer["iconCls"] = 'customer';
				objCustomer["leaf"] = false;
				arrCustomers.push(objCustomer);
			}
			objReport["children"] = arrCustomers;
			objReport["expanded"] = true;
			objReport["iconCls"] = 'report';
			objReport["leaf"] = false;
			arrReports.push(objReport);
		}
		objTree["children"] = arrReports;

		response.write( JSON.stringify(objTree) );
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

