nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_Day_kW.js
//	Script Name:	CLGX_SL_DCIM_Day_kW
//	Script Id:		customscript_clgx_sl_dcim_day_kw
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		02/27/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=469&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_day_kw (request, response){
	try {
		
		var file = request.getParameter('file');
		var objFile = nlapiLoadFile(file);
		var csv = objFile.getValue();
		var json = JSON.parse(csvJSON(csv));
		
		var objFile = nlapiLoadFile(2184724);
		html = objFile.getValue();
		html = html.replace(new RegExp('{jsonKW}','g'), JSON.stringify(json));
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}



//var csv is the CSV file with headers
function csvJSON(csv){
 
  var lines=csv.split("\n");
 
  var result = [];
 
  var headers=lines[0].split(",");
 
  for(var i=1;i<lines.length;i++){
 
	  var obj = {};
	  var currentline=lines[i].split(",");
 
	  for(var j=0;j<headers.length;j++){
		  obj[headers[j]] = currentline[j];
	  }
 
	  result.push(obj);
 
  }
  
  //return result; //JavaScript object
  return JSON.stringify(result); //JSON
}