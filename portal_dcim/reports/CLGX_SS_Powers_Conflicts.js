nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Powers_Conflicts.js
//	Script Name:	CLGX_SS_Powers_Conflicts
//	Script Id:		customscript_clgx_ss_powers_conflicts
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_powers_conflicts (){
    try{

    	var context = nlapiGetContext();
    	
    	var collecting = 0;
    	var collectmsg = '';
		if(collecting == 1){
			collectmsg = '| OpenData ';
		}
    	
    	var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,null));
		arrColumns.push(new nlobjSearchColumn('name',null,null).setSort(false));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",16));
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_facilty_mu',null,'is','F'));
		var searchFacilities = nlapiSearchRecord('customrecord_cologix_facility', null, arrFilters, arrColumns);
		
		if (searchFacilities != null) {
			
        	var searchFacility = searchFacilities[0];
        	var facilityid = searchFacility.getValue('internalid',null,null);
        	var facility = searchFacility.getValue('name',null,null);
        	
			var arrPowers = new Array();
			var arrPowers2 = new Array();
			
// All Others ===============================================================================================================================

			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_facility",null,"anyof",facilityid));
			var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pwrs_dups_others');
			searchPowers.addFilters(arrFilters);
			var resultSet = searchPowers.runSearch();
			resultSet.forEachResult(function(searchResult) {
				var columns = searchResult.getAllColumns();
				
				var voltage = searchResult.getText(columns[4]);
				var breaker = parseInt(searchResult.getValue(columns[5]));
				
				// these voltages  - only one breaker
				if(voltage == '120V Single Phase' || voltage == '240V Single Phase'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["breaker"] = breaker;
					arrPowers.push(objPower);
					arrPowers2.push(objPower);
				}
				// this voltages - 2 breakers
				else if (voltage == '208V Single Phase'){
					for ( var i = 0; i < 2; i++ ) {
						var objPower = new Object();
						objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
						objPower["fam"] = searchResult.getText(columns[0]);
						objPower["famname"] = searchResult.getValue(columns[1]);
						objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
						objPower["power"] = searchResult.getValue(columns[3]);
						objPower["breaker"] = breaker + i*2;
						arrPowers.push(objPower);
						arrPowers2.push(objPower);
					}
				}
				// these voltages - 3 breakers
				else if (voltage == '208V Three Phase' || voltage == '240V Three Phase' || voltage == '600V Three Phase'){
					for ( var i = 0;  i < 3; i++ ) {
						var objPower = new Object();
						objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
						objPower["fam"] = searchResult.getText(columns[0]);
						objPower["famname"] = searchResult.getValue(columns[1]);
						objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
						objPower["power"] = searchResult.getValue(columns[3]);
						objPower["breaker"] = breaker + i*2;
						arrPowers.push(objPower);
						arrPowers2.push(objPower);
					}
				}
				else{
				}
				return true;
			});
			

// VERTICAL ===============================================================================================================================
                 	
         			var arrFilters = new Array();
         			arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_facility",null,"anyof",facilityid));
         			var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pwrs_dups_vertic');
         			searchPowers.addFilters(arrFilters);
         			var resultSet = searchPowers.runSearch();
         			resultSet.forEachResult(function(searchResult) {
         				var columns = searchResult.getAllColumns();
         				
         				var voltage = searchResult.getText(columns[4]);
         				var breaker = parseInt(searchResult.getValue(columns[5]));
         				
         				// these voltages  - only one breaker
         				if(voltage == '120V Single Phase' || voltage == '240V Single Phase'){
         					var objPower = new Object();
         					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
         					objPower["fam"] = searchResult.getText(columns[0]);
         					objPower["famname"] = searchResult.getValue(columns[1]);
         					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
         					objPower["power"] = searchResult.getValue(columns[3]);
         					objPower["breaker"] = breaker;
         					arrPowers.push(objPower);
         					arrPowers2.push(objPower);
         				}
         				// this voltages - 2 breakers
         				else if (voltage == '208V Single Phase'){
         					for ( var i = 0; i < 2; i++ ) {
         						var objPower = new Object();
         						objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
         						objPower["fam"] = searchResult.getText(columns[0]);
         						objPower["famname"] = searchResult.getValue(columns[1]);
         						objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
         						objPower["power"] = searchResult.getValue(columns[3]);
         						objPower["breaker"] = breaker + i;
         						arrPowers.push(objPower);
         						arrPowers2.push(objPower);
         					}
         				}
         				// these voltages - 3 breakers
         				else if (voltage == '208V Three Phase' || voltage == '240V Three Phase'){
         					for ( var i = 0;  i < 3; i++ ) {
         						var objPower = new Object();
         						objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
         						objPower["fam"] = searchResult.getText(columns[0]);
         						objPower["famname"] = searchResult.getValue(columns[1]);
         						objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
         						objPower["power"] = searchResult.getValue(columns[3]);
         						objPower["breaker"] = breaker + i;
         						arrPowers.push(objPower);
         						arrPowers2.push(objPower);
         					}
         				}
         				else{
         				}
         				return true;
         			});
        			
        			var arrDuplicates = new Array();
        			var arrDuplicatesIDs = new Array();
        			for ( var i = 0; arrPowers != null && i < arrPowers.length; i++ ) {
        				var arrDups = new Array();
        				var arrDupsIDsUnsorted = new Array();
        				var arrDupsIDs = new Array();
        				var arrPowerDups = _.filter(arrPowers2, function(arr){
        			        return (arr.famid == arrPowers[i].famid && arr.breaker == arrPowers[i].breaker && arr.powerid != arrPowers[i].powerid);
        				});
        				if(arrPowerDups.length > 0){
        					var arrPowerDupsIDs = _.pluck(arrPowerDups, 'powerid');
        					arrDups.push(arrPowers[i]);
        					arrDupsIDsUnsorted.push(arrPowers[i].powerid);
        					for ( var j = 0; arrPowerDups != null && j < arrPowerDups.length; j++ ) {
        						arrDups.push(arrPowerDups[j]);
        						arrDupsIDsUnsorted.push(arrPowerDups[j].powerid);
        					}
        					arrDups = _.sortBy(arrDups, function(obj){ return obj.powerid;});
        					var firstid = arrDups[0].powerid;
        					
        					if(_.indexOf(arrDuplicatesIDs, firstid) == -1){
        						arrDuplicatesIDs.push(firstid);
        						arrDuplicates.push(arrDups);
        					}
        				}
        			}
        			
        			var count = 0;
        			var html = '';
        			html += '<table border="1" cellpadding="5">';
        			for ( var i = 0; arrDuplicates != null && i < arrDuplicates.length; i++ ) {
        				html += '<tr><td>';
        				html += ' | ';
        				var arrDup = arrDuplicates[i];
        				count += arrDup.length;
        				for ( var j = 0; arrDup != null && j < arrDup.length; j++ ) {
        					//html += arrDup[j].power + ' | ';
        					html += '<a href="https://1337135.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=17&id=' + arrDup[j].powerid + '">' + arrDup[j].power + '</a> | ';
        				}
        				html += '</td></tr>';
        			}
        			html += '</table><br><br><br>';
        			
        			if(arrDuplicates.length > 0){
        				var emailSubject = collectmsg + '| Power Dups (' + count + ') - ALL OTHERS - ' + facility;
        				
        				clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_alerts", emailSubject, html);
        				/*nlapiSendEmail(432742,71418,emailSubject,html,null,null,null,null,true);
        				nlapiSendEmail(432742,211645,emailSubject,html,null,null,null,null,true);
        				nlapiSendEmail(432742,1349020,emailSubject,html,null,null,null,null,true);*/
        			}

        			var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
                    nlapiLogExecution('DEBUG','Results ', ' | OTHERS / '+ facility + ' | '  + ' | DUPS = '+ count + '  |'+ ' | Usage - '+ usageConsumtion + '  |');

// PDPM ===============================================================================================================================
			
            
			var arrPowers = new Array();
			var arrPowers2 = new Array();
			
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_facility",null,"anyof",facilityid));
			var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pwrs_dups_pdpm');
			
			searchPowers.addFilters(arrFilters);
			var resultSet = searchPowers.runSearch();
			resultSet.forEachResult(function(searchResult) {
				var columns = searchResult.getAllColumns();
				
				var voltage = searchResult.getText(columns[4]);
				var module = parseInt(searchResult.getText(columns[5]));
				var lines = searchResult.getText(columns[6]);

				if(lines == 'Line 1'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 1;
					arrPowers.push(objPower);
					arrPowers2.push(objPower);
				}
				if(lines == 'Line 2'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 2;
					arrPowers.push(objPower);
					arrPowers2.push(objPower);
				}
				if(lines == 'Line 3'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 3;
					arrPowers.push(objPower);
					arrPowers2.push(objPower);
				}
				if(lines == 'Lines 1, 2'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 1;
					arrPowers.push(objPower);
					arrPowers2.push(objPower);
					
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 2;
					arrPowers.push(objPower);
					arrPowers2.push(objPower);
				}
				if(lines == 'Lines 2, 3'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 2;
					arrPowers.push(objPower);
					arrPowers2.push(objPower);
					
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 3;
					arrPowers.push(objPower);
					arrPowers2.push(objPower);
				}
				if(lines == 'Lines 1, 3'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 1;
					arrPowers.push(objPower);
					arrPowers2.push(objPower);
					
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 3;
					arrPowers.push(objPower);
					arrPowers2.push(objPower);
				}
				if(lines == 'Lines 1, 2, 3'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 1;
					arrPowers.push(objPower);
					arrPowers2.push(objPower);
					
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 2;
					arrPowers.push(objPower);
					arrPowers2.push(objPower);
					
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 3;
					arrPowers.push(objPower);
					arrPowers2.push(objPower);
				}

				return true;
			});
			
			var arrDuplicates = new Array();
			var arrDuplicatesIDs = new Array();
			for ( var i = 0; arrPowers != null && i < arrPowers.length; i++ ) {
				var arrDups = new Array();
				var arrDupsIDsUnsorted = new Array();
				var arrDupsIDs = new Array();
				var arrPowerDups = _.filter(arrPowers2, function(arr){
			        return (arr.famid == arrPowers[i].famid && arr.module == arrPowers[i].module  && arr.breaker == arrPowers[i].breaker && arr.powerid != arrPowers[i].powerid);
				});
				if(arrPowerDups.length > 0){
					var arrPowerDupsIDs = _.pluck(arrPowerDups, 'powerid');
					arrDups.push(arrPowers[i]);
					arrDupsIDsUnsorted.push(arrPowers[i].powerid);
					for ( var j = 0; arrPowerDups != null && j < arrPowerDups.length; j++ ) {
						arrDups.push(arrPowerDups[j]);
						arrDupsIDsUnsorted.push(arrPowerDups[j].powerid);
					}
					arrDups = _.sortBy(arrDups, function(obj){ return obj.powerid;});
					var firstid = arrDups[0].powerid;
					
					if(_.indexOf(arrDuplicatesIDs, firstid) == -1){
						arrDuplicatesIDs.push(firstid);
						arrDuplicates.push(arrDups);
					}
				}
			}
			
			var count = 0;
			var html = '';
			html += '<table border="1" cellpadding="5">';
			for ( var i = 0; arrDuplicates != null && i < arrDuplicates.length; i++ ) {
				html += '<tr><td>';
				html += ' | ';
				var arrDup = arrDuplicates[i];
				count += arrDup.length;
				for ( var j = 0; arrDup != null && j < arrDup.length; j++ ) {
					//html += arrDup[j].power + ' | ';
					html += '<a href="https://1337135.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=17&id=' + arrDup[j].powerid + '">' + arrDup[j].power + '</a> | ';
					
				}
				html += '</td></tr>';
			}
			html += '</table><br><br><br>';
			
			if(arrDuplicates.length > 0){
				var emailSubject = collectmsg + '| Power Dups (' + count + ') - PDPM - ' + facility;
				
				clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_alerts", emailSubject, html);
				
				/*nlapiSendEmail(432742,71418,emailSubject,html,null,null,null,null,true);
				nlapiSendEmail(432742,211645,emailSubject,html,null,null,null,null,true);
				nlapiSendEmail(432742,1349020,emailSubject,html,null,null,null,null,true);*/
			}
			
			var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            nlapiLogExecution('DEBUG','Results ', ' | PDPM / '+ facility + ' | '  + ' | DUPS = '+ count + '  |'+ ' | Usage - '+ usageConsumtion + '  |');
            
            nlapiSubmitField('customrecord_cologix_facility', facilityid, 'custrecord_clgx_facilty_mu', 'T');
            var status = nlapiScheduleScript('customscript_clgx_ss_powers_conflicts', 'customdeploy_clgx_ss_powers_conflicts' ,null);

		}
		else{
			
	    	var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",16));
			var searchFacilities = nlapiSearchRecord('customrecord_cologix_facility', null, arrFilters, arrColumns);
			
			for ( var i = 0; searchFacilities != null && i < searchFacilities.length; i++ ) {
	        	var searchFacility = searchFacilities[i];
	        	var facilityid = searchFacility.getValue('internalid',null,null);
	        	nlapiSubmitField('customrecord_cologix_facility', facilityid, 'custrecord_clgx_facilty_mu', 'F');
			}
			
			clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_alerts", "Finish Power Duplicates", "");
			
			/*nlapiSendEmail(432742,71418,'Finish Power Duplicates','',null,null,null,null,true);
			nlapiSendEmail(432742,211645,'Finish Power Duplicates','',null,null,null,null,true);
			nlapiSendEmail(432742,1349020,'Finish Power Duplicates','',null,null,null,null,true);*/
			
		}

  
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
