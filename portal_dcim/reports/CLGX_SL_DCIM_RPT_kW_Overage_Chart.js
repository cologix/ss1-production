nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_RPT_kW_Overage_Chart.js
//	Script Name:	CLGX_SL_DCIM_RPT_kW_Overage_Chart
//	Script Id:		customscript_clgx_sl_dcim_rpt_kw_over_ch
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		02/27/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=471&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_rpt_kw_over_ch (request, response){
    try {

    	var customerid = request.getParameter('customerid');
    	var facilityid = request.getParameter('facilityid');
    	var contract = request.getParameter('contract');

    	var customer = nlapiLookupField('customer', customerid, 'entityid');
    	var after = moment().subtract('days', 60).format('MM/DD/YYYY');
    	
    	var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_day_date',null,null).setSort(false));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_day_kw',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_day_file',null,null));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_day_customer",null,"anyof",customerid));
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_day_facilty",null,"anyof",facilityid));
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_day_date",null,"after",after));
		var searchKWs = nlapiSearchRecord('customrecord_clgx_dcim_customer_peak_day', null, arrFilters, arrColumns);

		var arrKW = new Array();
		for ( var i = 0; searchKWs != null && i < searchKWs.length; i++ ) {
			var searchKW = searchKWs[i];
			var objKW = new Object();
			objKW["day"] = searchKW.getValue('custrecord_clgx_dcim_peak_day_date',null,null);
			objKW["peak"] = parseFloat(searchKW.getValue('custrecord_clgx_dcim_peak_day_kw',null,null));	
			objKW["color"] = '#000000';	
			arrKW.push(objKW);
		}
    	
    	var objFile = nlapiLoadFile(2208352);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{data}','g'), JSON.stringify(arrKW));
		html = html.replace(new RegExp('{contract}','g'), contract);
		html = html.replace(new RegExp('{minimum}','g'), contract * 0.1);
		html = html.replace(new RegExp('{maximum}','g'), contract * 1.1);
		html = html.replace(new RegExp('{chartitle}','g'), customer + ' - Daily Power Peaks / Last 60 days');
		
		response.write( html );


    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
