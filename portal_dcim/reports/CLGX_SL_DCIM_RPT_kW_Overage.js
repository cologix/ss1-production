nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_RPT_kW_Overage.js
//	Script Name:	CLGX_SL_DCIM_RPT_kW_Overage
//	Script Id:		customscript_clgx_sl_dcim_rpt_kw_over
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		02/24/2015
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_rpt_kw_over (request, response){
    try {

		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,null).setSort(false));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_customer',null,null).setSort(false));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_facility',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_kw_max',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_kw',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_date',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_update',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_update',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_file',null,null));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
		var searchCustomers = nlapiSearchRecord('customrecord_clgx_dcim_customer_peak', null, arrFilters, arrColumns);
		var arrCustomers = new Array();
		for ( var i = 0; searchCustomers != null && i < searchCustomers.length; i++ ) {
	    	var searchCustomer = searchCustomers[i];
			var objCustomer = new Object();
			objCustomer["id"] = parseInt(searchCustomer.getValue('internalid',null,null));
			objCustomer["customerid"] = parseInt(searchCustomer.getValue('custrecord_clgx_dcim_peak_customer',null,null));
			objCustomer["customer"] = searchCustomer.getText('custrecord_clgx_dcim_peak_customer',null,null);
			objCustomer["facilityid"] = parseInt(searchCustomer.getValue('custrecord_clgx_dcim_peak_facility',null,null));
			objCustomer["facility"] = searchCustomer.getText('custrecord_clgx_dcim_peak_facility',null,null);
			objCustomer["facilityid"] = parseInt(searchCustomer.getValue('custrecord_clgx_dcim_peak_facility',null,null));
			objCustomer["contract"] = parseFloat(searchCustomer.getValue('custrecord_clgx_dcim_peak_kw_max',null,null));
			objCustomer["peak"] = parseFloat(searchCustomer.getValue('custrecord_clgx_dcim_peak_kw',null,null));
			objCustomer["date"] = searchCustomer.getValue('custrecord_clgx_dcim_peak_date',null,null);
			objCustomer["update"] = searchCustomer.getValue('custrecord_clgx_dcim_peak_update',null,null);
			var file = parseInt(searchCustomer.getValue('custrecord_clgx_dcim_peak_file',null,null));
			if(file > 0){
				objCustomer["file"] = file;
				var objFile = nlapiLoadFile(file);
				var fileurl = objFile.getURL();
				objCustomer["fileurl"] = fileurl;
			}
			else{
				objCustomer["file"] = 0;
				objCustomer["fileurl"] = '#';
			}	
			arrCustomers.push(objCustomer);
		}

    	var objFile = nlapiLoadFile(2170322);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{jsonGrid}','g'), JSON.stringify(arrCustomers));
		response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
