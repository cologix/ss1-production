nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_DCIM_RPT_Customer_Power.js
//	Script Name:	CLGX_SU_DCIM_RPT_Customer_Power
//	Script Id:		customscript_clgx_su_dcim_rpt_cust_pwr
//	Script Type:	User Event Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Date:			08/27/2014
//-------------------------------------------------------------------------------------------------


function beforeLoad (type, form) {
	try {
		
		var currentContext = nlapiGetContext();
		if (currentContext.getExecutionContext() == 'userinterface'){

		var roleid = nlapiGetRole();
		if (roleid == '-5' || roleid == '3' || roleid == '18' || roleid == '1052') {
			form.getField('custrecord_clgx_dcim_rpt_cust_rpt_enviro').setDisplayType('normal');
			form.getField('custrecord_clgx_dcim_rpt_cust_rpt_amps').setDisplayType('normal');
			form.getField('custrecord_clgx_dcim_rpt_cust_rpt_months').setDisplayType('normal');
		}
		
//------------- Begin Section 1 -------------------------------------------------------------------
// Created:	08/27/2014
// Details:	Display download links for Report and make pdf report checkbox available only to admin
//-------------------------------------------------------------------------------------------------
		
		var currentContext = nlapiGetContext();
		var userid = nlapiGetUser();
		var roleid = nlapiGetRole();
		
		if (type == 'view' || type == 'edit'){
			
			var fileID = nlapiGetFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_file');
			if(fileID != null && fileID != ''){
				nlapiSetFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_icon', '<a href="/app/common/media/mediaitem.nl?id=' + fileID + '" target="_blank"><img src="//www.cologix.com/images/icon/application_pdf.png" alt="PDF" height="16" width="16"></a>');
			}
		}

//---------- End Sections ------------------------------------------------------------------------------------------------
		}
		nlapiLogExecution('DEBUG','User Event - Before Load','|-------------FINISHED--------------|');  
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function beforeSubmit (type) {
	try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Date:	08/27/2014
// Details:	Generate Customer Power Report
//-----------------------------------------------------------------------------------------------------------------
		
		var currentContext = nlapiGetContext();
		if(currentContext.getExecutionContext() == 'userinterface' && type == 'edit'){
			
			var userid = nlapiGetUser();
			//var roleid = nlapiGetRole();

			var generatePDF = nlapiGetFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_pdf');
			if(generatePDF == 'T'){
				
				var reportid = nlapiGetRecordId();
				var rptname = nlapiGetFieldValue('name');
				var rpttitle = nlapiGetFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_title');
				var nbrmonths = nlapiGetFieldText('custrecord_clgx_dcim_rpt_cust_rpt_months');
				var byemail = nlapiGetFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_email');
				var rptype = nlapiGetFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_type');
				
				var arrColumns = new Array();
				arrColumns.push(new nlobjSearchColumn('internalid',null,null));
				arrColumns.push(new nlobjSearchColumn('name',null,null));
				arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_rpt_title',null,null));
				arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_rpt_months',null,null));
				arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_rpt_email',null,null));
				arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_rpt_type',null,null));
				var arrFilters = new Array();
				arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",reportid));
				var searchReports = nlapiSearchRecord('customrecord_clgx_dcim_rpt_cust_pwr', null, arrFilters, arrColumns);
				
				var arrReports = new Array();
				for ( var i = 0; searchReports != null && i < searchReports.length; i++ ) {
				        	var searchReport = searchReports[i];
				        	var objReport = new Object();
				        	objReport["reportid"] = reportid;
				        	objReport["report"] = rptname;
				        	objReport["title"] = rpttitle;
				        	objReport["email"] = byemail;
				        	objReport["rptype"] = parseInt(rptype);
				        	
				        	var nbrmonths = parseInt(searchReport.getText('custrecord_clgx_dcim_rpt_cust_rpt_months',null,null));
				        	objReport["nbrmonths"] = nbrmonths;
				        	var arrMonths = new Array();
				        	var m = moment().startOf('month').subtract('months', nbrmonths);
				        	for ( var h = 0; h < nbrmonths; h++ ) {
				        		var objMonth = new Object();
				        		objMonth["monthid"] = h;
				        		objMonth["month"] = m.add('months', 1).format('YYYY-MM');
				        		objMonth["ckwh"] = 0;
				        		arrMonths.push(objMonth);
				        	}
				        	objReport["months"] = arrMonths;
				        	
				        	// look for the facilities (grouped) on the report
				    		var arrColumns = new Array();
				    		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_facility',null,'GROUP').setSort(false));
				    		var arrFilters = new Array();
				    		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_cust_report",null,"anyof",reportid));
				    		var searchFacilities = nlapiSearchRecord('customrecord_clgx_dcim_rpt_cust_customer', null, arrFilters, arrColumns);
				    		var arrFacilities = new Array();
				    		for ( var j = 0; searchFacilities != null && j < searchFacilities.length; j++ ) {
				    		        	var searchFacility = searchFacilities[j];
				    		        	var objFacility = new Object();
				    		        	var facilityid = parseInt(searchFacility.getValue('custrecord_clgx_dcim_rpt_cust_facility',null,'GROUP'));
				    		        	objFacility["facilityid"] = facilityid;
				    		        	objFacility["facility"] = searchFacility.getText('custrecord_clgx_dcim_rpt_cust_facility',null,'GROUP');
			
				    		        	// look for customers on that facility
				    		    		var arrColumns = new Array();
				    		    		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_customer',null,null).setSort(false));
				    		    		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_contacts',null,null));
				    		    		var arrFilters = new Array();
				    		    		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_cust_report",null,"anyof",reportid));
				    		    		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_cust_facility",null,"anyof",facilityid));
				    		    		var searchCustomers = nlapiSearchRecord('customrecord_clgx_dcim_rpt_cust_customer', null, arrFilters, arrColumns);
				    		    		var arrCustomers = new Array();
				    		        	for ( var k = 0; searchCustomers != null && k < searchCustomers.length; k++ ) {
				    			        	var searchCustomer = searchCustomers[k];
				    			        	var objCustomer = new Object();
				    			        	var customerid = parseInt(searchCustomer.getValue('custrecord_clgx_dcim_rpt_cust_customer',null,null));
				    			        	objCustomer["customerid"] = customerid;
				    			        	objCustomer["customer"] = searchCustomer.getText('custrecord_clgx_dcim_rpt_cust_customer',null,null);
				    			        	
				    			        	var lstContactsIDs = searchCustomer.getValue('custrecord_clgx_dcim_rpt_cust_contacts',null,null);
				    			        	var lstContactsNames = searchCustomer.getText('custrecord_clgx_dcim_rpt_cust_contacts',null,null);
				    			        	
				    			        	var arrContactsIDs = lstContactsIDs.split(",");
				    			        	var arrContactsNames = lstContactsNames.split(",");
				    			        	
				    			        	//nlapiSendEmail(71418,71418,'arrPoints',JSON.stringify(arrContactsIDs),null,null,null,null);
				    			        	
				    			        	if(arrContactsIDs.length > 0){
				    			        		var arrContacts = new Array();
				    			        		for ( var l = 0; arrContactsIDs != null && l < arrContactsIDs.length; l++ ) {
				    			        			var contactid = parseInt(arrContactsIDs[l]);
				    			        			if(contactid > 0){
					    			        			var objContact = new Object();
					    			        			objContact["contactid"] = parseInt(arrContactsIDs[l]);
					    			        			objContact["contact"] = arrContactsNames[l];
					    			        			objContact["email"] = nlapiLookupField('contact', arrContactsIDs[l], 'email');
					    			        			arrContacts.push(objContact);
				    			        			}
				    			        		}
				    			        		objCustomer["contacts"] = arrContacts;
				    			        	}
				    			        	objCustomer["panels"] = get_powers(reportid,facilityid,customerid);
				    			        	objCustomer["devices"] = get_devices(reportid,facilityid);
				    			        	arrCustomers.push(objCustomer);
				    		    		}
				    		    		objFacility["customers"] = arrCustomers;
				    		    		arrFacilities.push(objFacility);
				    		}
				    		objReport["facilities"] = arrFacilities;
				        	arrReports.push(objReport);
				}
	
				var arrReports = set_historical_points(arrReports);
				
				
				var arrMonthsNames = arrReports[0].months;
				var arrMonthTotals = new Array();
				var arrGrandTotals = new Array();
				for ( var mt = 0; arrMonthsNames != null && mt < arrMonthsNames.length; mt++ ) {
					arrMonthTotals.push(0);
					arrGrandTotals.push(0);
				}
				
				var colspan = arrMonthsNames.length * 2 + 11;
				var colspan2 = arrMonthsNames.length + 2;
				
				var reporthtml = '';
				reporthtml += '<table>';
				reporthtml += '<tr>';
				reporthtml += '<td class="title" align="left"><img width="151px" height="48px" src="/core/media/media.nl?id=2&amp;c=1337135&amp;h=120db2ec4539ba4117d6" border="0"/></td>';
				//reporthtml += '<td class="title" align="center"><h1>' + arrReports[0].title + '</h1></td>';
				if(arrReports[0].rptype == 1){
					reporthtml += '<td class="title" align="right"><h1>Power Consumption<br/>and Environmentals</h1></td>';
					var decimals = 0;
				}
				else{
					reporthtml += '<td class="title" align="right"><h1>Power Demand<br/>and Environmentals</h1></td>';
					var decimals = 2;
				}
				
				
				reporthtml += '</tr>';
				reporthtml += '<tr><td colspan="2" class="space"> </td></tr>'; 
				reporthtml += '</table>';
				
				var arrFacilities = arrReports[0].facilities;
				for ( var j = 0; arrFacilities != null && j < arrFacilities.length; j++ ) {
					
					//reporthtml += '<tr><td colspan="' + colspan + '" class="title"><h1>' + arrFacilities[j].facility + '</h1></td></tr>';
					//reporthtml += '<tr><td colspan="' + colspan + '" class="title"> </td></tr>';
					var arrCustomers = arrFacilities[j].customers;
					for ( var k = 0; arrCustomers != null && k < arrCustomers.length; k++ ) {
						
						reporthtml += '<table>';
						reporthtml += '<tr><td colspan="' + colspan + '" class="title"><h1>' + arrFacilities[j].facility + ' - ' + arrCustomers[k].customer + '</h1></td></tr>';
						//reporthtml += '<tr><td colspan="' + colspan + '" class="title"><h2>' + arrFacilities[j].facility + ' - Anonymous</h2></td></tr>';
						reporthtml += '<tr><td colspan="' + colspan + '" class="title"> </td></tr>';
						
						//nlapiSendEmail(71418,71418,'arrReports',JSON.stringify(arrReports),null,null,null,null);
						
						if(arrReports[0].rptype == 1){
							reporthtml += '<tr><td colspan="' + colspan + '" class="title"><h2>Power consumption - all values are kWh</h2></td></tr>';
						}
						else{
							reporthtml += '<tr><td colspan="' + colspan + '" class="title"><h2>Power demand - all values are kW</h2></td></tr>';
						}
						reporthtml += '<tr><td colspan="' + colspan + '" class="title"> </td></tr>'; 
					
						var arrPanels = arrCustomers[k].panels;
						
						for ( var l = 0; arrPanels != null && l < arrPanels.length; l++ ) {
							
							var arrMonthTotals = new Array();
							for ( var mt = 0; arrMonthsNames != null && mt < arrMonthsNames.length; mt++ ) {
								arrMonthTotals.push(0);
							}
							
							reporthtml += '<tr><td colspan="' + colspan + '" class="td6"><h4>' + arrPanels[l].panel + '</h4></td></tr>';
							
							reporthtml += '<tr>';
							reporthtml += '<td align="left" class="td5">Module</td>';
							reporthtml += '<td align="left" class="td5">Breaker</td>';
							reporthtml += '<td align="left" class="td5">Power</td>';
							reporthtml += '<td align="left" class="td5">Pair</td>';
							reporthtml += '<td align="left" class="td5">Space</td>';
							reporthtml += '<td align="left" class="td5">Order</td>';
							reporthtml += '<td align="left" class="td5">Service</td>';
							reporthtml += '<td align="left" class="td5">Voltage</td>';
							reporthtml += '<td align="left" class="td5">Circuit</td>';
							
							for ( var o = 0; arrMonthsNames != null && o < arrMonthsNames.length; o++ ) {
								reporthtml += '<td  align="center" colspan="2" class="td5">' + arrMonthsNames[o].month + '</td>';
							}
							reporthtml += '<td align="center" colspan="2" class="td10">Totals</td>';
							reporthtml += '</tr>';
							
							var arrModules = arrPanels[l].modules;
							
							for ( var m = 0; arrModules != null && m < arrModules.length; m++ ) {
								var moduletotal = 0;
								var arrMMonths = arrModules[m].months;
								
								//nlapiSendEmail(71418,71418,'arrMMonths',JSON.stringify(arrMMonths),null,null,null,null);
								
								var arrBreakers = arrModules[m].breakers;
								var mtotal = 0;
								for ( var mb = 0; arrBreakers != null && mb < arrBreakers.length; mb++ ) {
									
									var tdclass = mb%3;
									
									reporthtml += '<tr>';
									if(mb == 0){
										reporthtml += '<td align="left" rowspan="' + arrBreakers.length + '" class="td4">' + arrBreakers[mb].current.substring(0, 10) + '</td>';
										//reporthtml += '<td align="left">' + arrBreakers[mb].current.substring(0, 10) + '</td>';
									}
										//reporthtml += '<td align="left" class="head">' + arrBreakers[mb].breaker + '<br/>' + arrBreakers[mb].currentextid + '</td>';
										reporthtml += '<td align="left" class="td3">' + arrBreakers[mb].breaker + '</td>';
										reporthtml += '<td align="left" class="td' + tdclass + '">' + arrBreakers[mb].power + '</td>';
										reporthtml += '<td align="left" class="td' + tdclass + '">' + arrBreakers[mb].power2 + '</td>';
										reporthtml += '<td align="left" class="td' + tdclass + '">' + arrBreakers[mb].space + '</td>';
										reporthtml += '<td align="left" class="td' + tdclass + '">' + arrBreakers[mb].so + '</td>';
										reporthtml += '<td align="left" class="td' + tdclass + '">' + arrBreakers[mb].service + '</td>';
										reporthtml += '<td align="left" class="td' + tdclass + '">' + arrBreakers[mb].voltage + '</td>';
										reporthtml += '<td align="left" class="td' + tdclass + '">' + arrBreakers[mb].circuit + '</td>';
										
									var total = 0;
									var arrBMonths = arrBreakers[mb].months;
									for ( var bm = 0; arrMonthsNames != null && bm < arrMonthsNames.length; bm++ ) {
	
										var objBMonth = _.find(arrBMonths, function(arr){ return (arr.month == arrMonthsNames[bm].month); });
										if(objBMonth != null){
											//reporthtml += '<td align="right">' + objBMonth.amps + '<br/>' + objBMonth.kwh + '<br/>' + objBMonth.hours + '<br/>' + objBMonth.mkwh + '</td>';
											reporthtml += '<td align="right" class="td' + tdclass + '">' + objBMonth.mkwh.toFixed(decimals) + '</td>';
											
											total += parseFloat((objBMonth.mkwh));
											arrMonthTotals[bm] += parseFloat((objBMonth.mkwh));
										}
										else{
											reporthtml += '<td align="right" class="td' + tdclass + '">0</td>';
										}
										if(mb == 0){
											//var objMMonth = _.find(arrMMonths, function(arr){ return (arr.month == arrMonthsNames[bm].month); });
											//if(objMMonth != null){
												//reporthtml += '<td align="right" rowspan="' + arrBreakers.length + '" class="td7">' + objMMonth.kwh + '-<br/>' + objMMonth.kwhpre + '=<br/>' + objMMonth.kwhdiff + '</td>';
												//reporthtml += '<td align="right" rowspan="' + arrBreakers.length + '"  class="td7">' + objMMonth.kwhdiff + '</td>';
												reporthtml += '<td align="right" rowspan="' + arrBreakers.length + '"  class="td7">' + arrModules[m].months[bm].toFixed(decimals) + '</td>';
												moduletotal += arrModules[m].months[bm];
											//}
											//else{
											//	reporthtml += '<td align="right" rowspan="' + arrBreakers.length + '" class="td7">0</td>';
											//}
										}
									}
									//reporthtml += '<td align="right" rowspan="' + arrBreakers.length + '">' + total.toFixed(decimals) + '</td>';
									reporthtml += '<td align="right" class="td8">' + total.toFixed(decimals) + '</td>';
									if(mb == 0){
										reporthtml += '<td align="right" rowspan="' + arrBreakers.length + '" class="td9">' + moduletotal.toFixed(decimals) + '</td>';
									}
									
									
									reporthtml += '</tr>';
								}
							}
							
							//nlapiSendEmail(71418,71418,'arrMonthTotals',JSON.stringify(arrMonthTotals),null,null,null,null);
							
							var grandTotal = 0;
							reporthtml += '<tr>';
							reporthtml += '<td align="left" colspan="9" class="td10">Totals</td>';
							
							for ( var o = 0; arrMonthTotals != null && o < arrMonthTotals.length; o++ ) {
								reporthtml += '<td align="right" colspan="2" class="td8">' + arrMonthTotals[o].toFixed(decimals) + '</td>';
								grandTotal += parseFloat(arrMonthTotals[o]);
								arrGrandTotals[o] += arrMonthTotals[o];
							}
							reporthtml += '<td align="right" colspan="2" class="td10">' + grandTotal.toFixed(decimals) + '</td>';
							reporthtml += '</tr>';
							reporthtml += '<tr><td colspan="' + colspan + '" class="space"> </td></tr>'; 
							
						}
						
						reporthtml += '<tr><td colspan="' + colspan + '" class="space"> </td></tr>'; 
						reporthtml += '<tr>';
						reporthtml += '<td align="left" colspan="9" class="td11">Grand Totals</td>';
						
						var totalAll = 0;
						for ( var o = 0; arrGrandTotals != null && o < arrGrandTotals.length; o++ ) {
							reporthtml += '<td align="right" colspan="2" class="td9">' + arrGrandTotals[o].toFixed(decimals) + '</td>';
							totalAll += parseFloat(arrGrandTotals[o]);
						}
						reporthtml += '<td align="right" colspan="2" class="td11">' + totalAll.toFixed(decimals) + '</td>';
						reporthtml += '</tr>';
						
						reporthtml += '</table>';
						
						
						
						var arrDevices = arrCustomers[j].devices;
						if(arrDevices.length > 0){
							// page break for environmentals section if it exist
							reporthtml += '<div page-break-before="always"></div>';
							
							reporthtml += '<table>';
							reporthtml += '<tr><td colspan="' + colspan2 + '" class="title"> </td></tr>';
							reporthtml += '<tr><td colspan="' + colspan2 + '" class="title"><h2>Environmentals</h2></td></tr>';
							reporthtml += '<tr><td colspan="' + colspan2 + '" class="title"> </td></tr>'; 
							
							var arrDevices = arrCustomers[j].devices;
							for ( var l = 0; arrDevices != null && l < arrDevices.length; l++ ) {
								reporthtml += '<tr><td  colspan="' + colspan2 + '" class="td6"><h4>' + arrDevices[l].device + '</h4></td></tr>';
								reporthtml += '<tr><td align="left" colspan="2" class="td5">Sensor</td>';
								for ( var o = 0; arrMonthsNames != null && o < arrMonthsNames.length; o++ ) {
									reporthtml += '<td  align="right" class="td5">' + arrMonthsNames[o].month + '</td>';
								}
								reporthtml += '</tr>';
								
								var arrSensors = arrDevices[l].sensors;
								for ( var m = 0; arrSensors != null && m < arrSensors.length; m++ ) {
									
									var tdclass = m%2;
									
									reporthtml += '<tr>';
									reporthtml += '<td align="left" colspan="2" class="td4">' + arrSensors[m].sensor + '</td>';
									
									var arrSMonths = arrSensors[m].months;
									for ( var n = 0; arrMonthsNames != null && n < arrMonthsNames.length; n++ ) {
										var objMonth = _.find(arrSMonths, function(arr){ return (arr.month == arrMonthsNames[n].month); });
										if(objMonth != null){
											reporthtml += '<td align="right" class="td' + tdclass + '">' + objMonth.value.toFixed(0) + '</td>';
										}
										else{
											reporthtml += '<td align="right" class="td' + tdclass + '">0</td>';
										}
									}
									reporthtml += '</tr>';
								}
								reporthtml += '<tr><td colspan="' + colspan2 + '" class="space"> </td></tr>'; 
							}
							reporthtml += '</table>';
						}
	
					}
					// page break for each facility except the last one
					if(j < (arrFacilities.length - 1)){
						reporthtml += '<div page-break-before="always"></div>';
					}

				}
				
				//nlapiSendEmail(71418,71418,'arrReports',JSON.stringify(arrReports),null,null,null,null);
				
				var objFile = nlapiLoadFile(1520031);
				var html = objFile.getValue();
				html = html.replace(new RegExp('{report}', 'g'), reporthtml);
				var filePDF = nlapiXMLToPDF(html);
				
				var reportdate = moment().format('YYYY-M-D');
				var filename = objReport.report + '_' + reportdate;
				
				if(objReport.rptype == 1){
					filename += '_KWH';
				}
				else{
					filename += '_KW';
				}
				if(objReport.email == 'T'){
					filename += '_&@';
				}
				filename += '.pdf';
				
				filePDF.setName(filename);

				// find folder id depending od year / month
				//filePDF.setName('Anonymous_08/20/2014_&@.pdf');
				//filePDF.setFolder(1267033);
				filePDF.setFolder(get_file_folder());
				var fileId = nlapiSubmitFile(filePDF);
				
				nlapiSetFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_file', fileId);
				nlapiSetFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_pdf', 'F');
				nlapiSetFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_email', 'F');
			}
		}

//---------- End Sections ------------------------------------------------------------------------------------------------
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}




function get_powers (reportid, facilityid, customerid){
	// search customer services
	var arrServicesIDs = new Array();
	
	var arrPanels = new Array();
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("customer",null,"anyof",customerid));
	var searchServices = nlapiSearchRecord('job', null, arrFilters, arrColumns);
	
	for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
		arrServicesIDs.push(searchServices[i].getValue('internalid',null,null));
	}
	
	// Build Powers Array ------------------------------------------------------------------------------------------------------------------------------------------------
	var arrPowers = new Array();
	
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_service",null,"anyof",arrServicesIDs));
	var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_panels_pwr_points');
	searchPowers.addFilters(arrFilters);
	var resultSet = searchPowers.runSearch();
	resultSet.forEachResult(function(searchResult) {
		var columns = searchResult.getAllColumns();
		var objPower = new Object();
		
		objPower["panelid"] = parseInt(searchResult.getValue('custrecord_clgx_od_fam_device','CUSTRECORD_CLGX_POWER_PANEL_PDPM',null));
		objPower["panel"] = searchResult.getText('custrecord_clgx_od_fam_device','CUSTRECORD_CLGX_POWER_PANEL_PDPM',null);
		
		objPower["module"] = parseInt(searchResult.getValue(columns[9]));
		objPower["breaker"] = parseInt(searchResult.getValue(columns[14]));
		
		objPower["powerid"] = parseInt(searchResult.getValue('internalid',null,null));
		objPower["power"] = searchResult.getValue('name',null,null);
		
		objPower["power2id"] = parseInt(searchResult.getValue('custrecord_clgx_dcim_pair_power',null,null));
		objPower["power2"] = searchResult.getText('custrecord_clgx_dcim_pair_power',null,null);
		
		objPower["spaceid"] = parseInt(searchResult.getValue('custrecord_cologix_power_space',null,null));
		objPower["space"] = searchResult.getText('custrecord_cologix_power_space',null,null);
		
		objPower["serviceid"] = parseInt(searchResult.getValue('custrecord_cologix_power_service',null,null));
		var service = searchResult.getText('custrecord_cologix_power_service',null,null);
		var arrService = service.split(":");
		objPower["service"] = _.last(arrService).trim();
		
		objPower["soid"] = parseInt(searchResult.getValue('custrecord_power_circuit_service_order',null,null));
		var so = searchResult.getText('custrecord_power_circuit_service_order',null,null);
		var arrSO = so.split("#");
		objPower["so"] = _.last(arrSO).trim();

		var voltage = searchResult.getText('custrecord_cologix_power_volts',null,null);
		objPower["voltage"] = voltage;
		objPower["volts"] = parseInt(voltage.substring(0, 3));
		
		var circuit = searchResult.getText('custrecord_cologix_power_amps',null,null);
		var soldamps = circuit.substring(0, circuit.length - 1);
		if(circuit != null && circuit != ''){
			objPower["circuit"] = circuit;
			
			if(voltage == '120V Single Phase' || voltage == '240V Single Phase'){
				objPower["soldamps"] = parseFloat(parseFloat(soldamps) * 0.8);
			}
			else if(voltage == '208V Single Phase'){
				objPower["soldamps"] = parseFloat(parseFloat(soldamps) * 0.8 / 2);
			}
			else if(voltage == '208V Three Phase' || voltage == '240V Three Phase' || voltage == '480V Three Phase'){
				objPower["soldamps"] = parseFloat(parseFloat(soldamps) * 0.8 / 3);
			}
			else{
				objPower["soldamps"] = parseFloat(parseFloat(soldamps) * 0.8);
			}
		}
		else{
			objPower["circuit"] = '';
			objPower["soldamps"] = 0;
		}

		objPower["energyid"] = parseInt(searchResult.getValue('internalid','CUSTRECORD_CLGX_DCIM_POINTS_CONSUMPTION',null));
		objPower["energyextid"] = parseInt(searchResult.getValue('externalid','CUSTRECORD_CLGX_DCIM_POINTS_CONSUMPTION',null));
		objPower["energy"] = searchResult.getValue('name','CUSTRECORD_CLGX_DCIM_POINTS_CONSUMPTION',null);
		objPower["kwhavg"] = parseFloat(searchResult.getValue('custrecord_clgx_dcim_points_day_avg','CUSTRECORD_CLGX_DCIM_POINTS_CONSUMPTION',null));
		

		objPower["currentid"] = parseInt(searchResult.getValue('internalid','CUSTRECORD_CLGX_DCIM_POINTS_CURRENT',null));
		objPower["currentextid"] = parseInt(searchResult.getValue('externalid','CUSTRECORD_CLGX_DCIM_POINTS_CURRENT',null));
		objPower["current"] = searchResult.getValue('name','CUSTRECORD_CLGX_DCIM_POINTS_CURRENT',null);
		objPower["currentavg"] = parseFloat(searchResult.getValue('custrecord_clgx_dcim_points_day_avg','CUSTRECORD_CLGX_DCIM_POINTS_CURRENT',null));
		
		arrPowers.push(objPower);
		return true;
	});

	var arrPanelsIDs = _.uniq(_.pluck(arrPowers, 'panelid'));
	var arrPanelsNames = _.uniq(_.pluck(arrPowers, 'panel'));
	var arrPanels = new Array();
	for ( var i = 0; arrPanelsIDs != null && i < arrPanelsIDs.length; i++ ) {
		var objPanel = new Object();
    	objPanel["panelid"] = parseInt(arrPanelsIDs[i]);
    	objPanel["panel"] = arrPanelsNames[i];
    	
    	var arrPanelModules = _.filter(arrPowers, function(arr){
	        return (arr.panelid == arrPanelsIDs[i]);
		});
    	var arrPanelModulesIDs = _.uniq(_.pluck(arrPanelModules, 'module'));
    	var arrModules = new Array();
    	for ( var j = 0; arrPanelModulesIDs != null && j < arrPanelModulesIDs.length; j++ ) {
    		
    		var objPower = _.find(arrPowers, function(arr){ return (arr.panelid == arrPanelsIDs[i] && arr.module == arrPanelModulesIDs[j] ); });
    		
    		var objModule = new Object();
    		objModule["moduleid"] = parseInt(objPower.module);
    		objModule["module"] = 'M' + objPower.module;
    		objModule["energyid"] = objPower.energyid;
    		objModule["energyextid"] = objPower.energyextid;
    		objModule["energy"] = objPower.energy;
    		objModule["kwhavg"] = objPower.kwhavg;
    		objModule["months"] = [];
    		
    		var arrModuleBreakers = _.filter(arrPanelModules, function(arr){
    	        return (arr.panelid == arrPanelsIDs[i] && arr.module == arrPanelModulesIDs[j] );
    		});
        	var arrModulesBreakersIDs = _.uniq(_.pluck(arrModuleBreakers, 'breaker'));
        	var arrBreakers = new Array();
        	for ( var k = 0; arrModulesBreakersIDs != null && k < arrModulesBreakersIDs.length; k++ ) {
        		
        		var objPower = _.find(arrPowers, function(arr){ return (arr.panelid == arrPanelsIDs[i] && arr.module == arrPanelModulesIDs[j] && arr.breaker == arrModulesBreakersIDs[k]); });
        		
        		var objBreaker = new Object();
        		objBreaker["breakerid"] = parseInt(objPower.breaker);
        		objBreaker["breaker"] = 'B' + objPower.breaker;
        		
        		objBreaker["powerid"] = objPower.powerid;
        		objBreaker["power"] = objPower.power;
        		
        		objBreaker["power2id"] = objPower.power2id;
        		objBreaker["power2"] = objPower.power2;
        		
        		objBreaker["spaceid"] = objPower.spaceid;
        		objBreaker["space"] = objPower.space;
        		
        		objBreaker["serviceid"] = objPower.serviceid;
        		objBreaker["service"] = objPower.service;
        		
        		objBreaker["soid"] = objPower.soid;
        		objBreaker["so"] = objPower.so;
        		
        		objBreaker["voltage"] = objPower.voltage;
        		objBreaker["volts"] = objPower.volts;
        		

    			objBreaker["circuit"] = objPower.circuit;
    			objBreaker["soldamps"] = objPower.soldamps;

    			objBreaker["currentid"] = objPower.currentid;
        		objBreaker["currentextid"] = objPower.currentextid;
        		objBreaker["current"] = objPower.current;
        		objBreaker["currentavg"] = objPower.currentavg;
        		objBreaker["months"] = [];
        		
        		arrBreakers.push(objBreaker);
        	}
        	objModule["breakers"] = arrBreakers;
        	arrModules.push(objModule);
    	}
    	objPanel["modules"] = arrModules;
    	arrPanels.push(objPanel);
	}
	return arrPanels;
}


function get_panels (reportid, facilityid){
	
	var arrPanels = new Array();
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_pnl_device',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_pnl_points',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_cust_pnl_rpt",null,"anyof",reportid));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_cust_pnl_fac",null,"anyof",facilityid));
	var searchPanels = nlapiSearchRecord('customrecord_clgx_dcim_rpt_cust_pnl', null, arrFilters, arrColumns);
	var arrPanels = new Array();
	for ( var i = 0; searchPanels != null && i < searchPanels.length; i++ ) {
		
		var searchPanel = searchPanels[i];
    	var objPanel = new Object();
    	objPanel["panelid"] = parseInt(searchPanel.getValue('custrecord_clgx_dcim_rpt_cust_pnl_device',null,null));
    	objPanel["panel"] = searchPanel.getText('custrecord_clgx_dcim_rpt_cust_pnl_device',null,null);
    	
    	var lstPointsIDs = searchPanel.getValue('custrecord_clgx_dcim_rpt_cust_pnl_points',null,null);
    	var lstPointsNames = searchPanel.getText('custrecord_clgx_dcim_rpt_cust_pnl_points',null,null);
    	
    	var arrPointsIDs = lstPointsIDs.split(",");
    	var arrPointsNames = lstPointsNames.split(",");
    	if(arrPointsIDs.length > 0){
    		var arrPoints = new Array();
    		for ( var j = 0; arrPointsIDs != null && j < arrPointsIDs.length; j++ ) {
    			var objPoint = new Object();
    			objPoint["pointid"] = parseInt(arrPointsIDs[j]);
    			objPoint["point"] = arrPointsNames[j];
    			objPoint["months"] = [];
    			arrPoints.push(objPoint);
    		}
    		objPanel["points"] = arrPoints;
    	}
    	arrPanels.push(objPanel);
	}
	return arrPanels;
}



function get_devices (reportid, facilityid){
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_cust_dev_rpt",null,"anyof",reportid));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_cust_dev_fac",null,"anyof",facilityid));
	var searchDevices = nlapiSearchRecord('customrecord_clgx_dcim_rpt_cust_dev', 'customsearch_clgx_dcim_rpt_cust_devices', arrFilters, arrColumns);
	var arrDevicesGrid = new Array();
	for ( var i = 0; searchDevices != null && i < searchDevices.length; i++ ) {
		var searchDevice = searchDevices[i];
    	var objDevice = new Object();
    	objDevice["deviceid"] = parseInt(searchDevice.getValue('custrecord_clgx_dcim_rpt_cust_dev_device',null,null));
    	objDevice["device"] = searchDevice.getText('custrecord_clgx_dcim_rpt_cust_dev_device',null,null);
    	objDevice["sensorid"] = parseInt(searchDevice.getValue('internalid','CUSTRECORD_CLGX_DCIM_RPT_CUST_DEV_POINTS',null));
    	objDevice["sensorextid"] = parseInt(searchDevice.getValue('externalid','CUSTRECORD_CLGX_DCIM_RPT_CUST_DEV_POINTS',null));
    	objDevice["sensor"] = searchDevice.getValue('name','CUSTRECORD_CLGX_DCIM_RPT_CUST_DEV_POINTS',null);
    	objDevice["sensoravg"] = parseFloat(searchDevice.getValue('custrecord_clgx_dcim_points_day_avg','CUSTRECORD_CLGX_DCIM_RPT_CUST_DEV_POINTS',null));
    	objDevice["months"] = [];
    	arrDevicesGrid.push(objDevice);
	}
	
	var arrDevicesIDs = _.uniq(_.pluck(arrDevicesGrid, 'deviceid'));
	var arrDevicesNames = _.uniq(_.pluck(arrDevicesGrid, 'device'));
	var arrDevices = new Array();
	for ( var i = 0; arrDevicesIDs != null && i < arrDevicesIDs.length; i++ ) {
		var objDevice = new Object();
    	objDevice["deviceid"] = arrDevicesIDs[i];
    	objDevice["device"] = arrDevicesNames[i];
		var arrDeviceSensors = _.filter(arrDevicesGrid, function(arr){
	        return (arr.deviceid === arrDevicesIDs[i]);
		});
		var arrSensors = new Array();
		for ( var j = 0; arrDeviceSensors != null && j < arrDeviceSensors.length; j++ ) {
			var objSensor = new Object();
			objSensor["sensorid"] = arrDeviceSensors[j].sensorid;
			objSensor["sensorextid"] = arrDeviceSensors[j].sensorextid;
			objSensor["sensor"] = arrDeviceSensors[j].sensor;
			objSensor["sensoravg"] = arrDeviceSensors[j].sensoravg;
			arrSensors.push(objSensor);
		}
		objDevice["sensors"] = arrSensors;
		arrDevices.push(objDevice);
	}
	return arrDevices;
}


function set_historical_points (arrReports){
	//nlapiSendEmail(71418,71418,'arrReports',JSON.stringify(arrReports),null,null,null,null);
	
	var arrModulesEnergyIDs = new Array();
	var arrEnvironmentalIDs = new Array();
	var arrBreakersCurrentIDs = new Array();
	
	for ( var i = 0; arrReports != null && i < arrReports.length; i++ ) {
		var arrFacilities = arrReports[i].facilities;
		for ( var j = 0; arrFacilities != null && j < arrFacilities.length; j++ ) {
			var arrCustomers = arrFacilities[j].customers;
			for ( var k = 0; arrCustomers != null && k < arrCustomers.length; k++ ) {
				var arrPanels = arrCustomers[k].panels;
				for ( var l = 0; arrPanels != null && l < arrPanels.length; l++ ) {
					var arrModules = arrPanels[l].modules;
					for ( var m = 0; arrModules != null && m < arrModules.length; m++ ) {
						arrModulesEnergyIDs.push(arrModules[m].energyextid);
						var arrBreakers = arrModules[m].breakers;
						for ( var n = 0; arrBreakers != null && n < arrBreakers.length; n++ ) {
							arrBreakersCurrentIDs.push(arrBreakers[n].currentextid);
						}
					}
				}
				var arrDevices = arrCustomers[k].devices;
				for ( var o = 0; arrDevices != null && o < arrDevices.length; o++ ) {
					var arrSensors = arrDevices[o].sensors;
					for ( var p = 0; arrSensors != null && p < arrSensors.length; p++ ) {
						arrEnvironmentalIDs.push(arrSensors[p].sensorextid);
					}
				}
			}
		}
	}
	
	var start = moment().startOf('month').subtract('months', arrReports[0].nbrmonths - 1).format('YYYY-M-D');
	var end = moment().startOf('month').add('months', 1).format('YYYY-M-D');
	
	var url = '';
	url += 'https://lucee-nnj3.dev.nac.net/odins/powers/get_rpt_customer_points.cfm';
	//url += 'https://command1.cologix.com:10313/netsuite/reports/get_rpt_customer_points.cfm';
	url += '?start=' + start;
	url += '&end=' + end;
	url += '&e=' + arrModulesEnergyIDs.join();
	url += '&p=' + arrEnvironmentalIDs.join();
	url += '&c=' + arrBreakersCurrentIDs.join();
	
	var requestURL = nlapiRequestURL(url);
	var valuesJSON = requestURL.body;
	var arrValues= JSON.parse( valuesJSON );
	
	
	var arrModulesEnergyVals = arrValues[0];
	var arrEnvironmentalVals = arrValues[1];
	var arrBreakersCurrentVals = arrValues[2];
	var arrBreakersAmpsVals = arrValues[3];
	
	var arrModulesEnergy = get_points_values(arrModulesEnergyIDs, arrModulesEnergyVals,'energy');
	var arrEnvironmental = get_points_values(arrEnvironmentalIDs, arrEnvironmentalVals,'enviro');
	if(arrReports[0].rptype == 1){
		var arrBreakersCurrent = get_points_values(arrBreakersCurrentIDs, arrBreakersCurrentVals,'current');
	}
	else{
		var arrBreakersCurrent = get_points_values(arrBreakersCurrentIDs, arrBreakersAmpsVals,'current');
	}
	
	var arrMonthsNames = arrReports[0].months;
	var arrMonthTotals = new Array();
	var arrModuleTotals = new Array();
	for ( var mt = 0; arrMonthsNames != null && mt < arrMonthsNames.length; mt++ ) {
		arrMonthTotals.push(0);
		arrModuleTotals.push(0);
	}
	
	for ( var i = 0; arrReports != null && i < arrReports.length; i++ ) {
		var arrFacilities = arrReports[i].facilities;
		for ( var j = 0; arrFacilities != null && j < arrFacilities.length; j++ ) {
			var arrCustomers = arrFacilities[j].customers;
			for ( var k = 0; arrCustomers != null && k < arrCustomers.length; k++ ) {
				var arrPanels = arrCustomers[k].panels;
				for ( var l = 0; arrPanels != null && l < arrPanels.length; l++ ) {
					var arrModules = arrPanels[l].modules;
					for ( var m = 0; arrModules != null && m < arrModules.length; m++ ) {
						//for ( var s = 0; arrModuleTotals != null && s < arrModuleTotals.length; s++ ) {
						//	arrModuleTotals[s] = 0;
						//}
						
						var arrModuleTotals = new Array();
						for ( var mt = 0; arrMonthsNames != null && mt < arrMonthsNames.length; mt++ ) {
							arrModuleTotals.push(0);
						}
						
						/*
						var arrModuleMonths = arrModules[m].months;
						for ( var s = 0; arrModuleMonths != null && s < arrModuleMonths.length; s++ ) {
							var objMPoint = _.find(arrModulesEnergy, function(arr){ return (arr.pointid == arrModules[m].energyextid && arr.months[s].month == arrModuleMonths[s].month); });
							if(objMPoint != null){
								arrReports[i].facilities[j].customers[k].panels[l].modules[m].months[s] = objMPoint.months[s];
							}							
							arrModuleTotals[s] = 0;
						}
						*/
						var arrBreakers = arrModules[m].breakers;
						for ( var n = 0; arrBreakers != null && n < arrBreakers.length; n++ ) {
							var voltage = arrBreakers[n].voltage;
							var volts = arrBreakers[n].volts;
							
							var objBreakerPoint = _.find(arrBreakersCurrent, function(arr){ return (arr.pointid == arrBreakers[n].currentextid); });
							
							if(objBreakerPoint != null){
								var arrBreakerMonths = objBreakerPoint.months;
								
								for ( var r = 0; arrBreakerMonths != null && r < arrBreakerMonths.length; r++ ) {

									
									if(voltage == '120V Single Phase' || voltage == '240V Single Phase' || voltage == '240V Three Phase'){
										var kwh = parseFloat(arrBreakerMonths[r].amps * volts / 1000);
									}
									else if(voltage == '208V Single Phase'){
										var kwh = parseFloat(arrBreakerMonths[r].amps * volts / 2 / 1000);
									}
									else if(voltage == '208V Three Phase'){
										var kwh = parseFloat(arrBreakerMonths[r].amps * Math.sqrt(3) * volts / 3 / 1000);
									}
									else{}
									
									if(arrReports[i].rptype == 1){
										var mkwh = arrBreakerMonths[r].mhours * kwh / arrBreakerMonths[r].hours;
									}
									else{
										var mkwh = kwh;
									}
									
									
									objBreakerPoint.months[r].mkwh = parseFloat(mkwh);
									arrModuleTotals[r] += parseFloat(mkwh);
									
								}
								arrReports[i].facilities[j].customers[k].panels[l].modules[m].breakers[n].months = objBreakerPoint.months;
							}
						}
						
						
						//var arrM = arrMonthsNames;
						//for ( var s = 0; arrM != null && s < arrM.length; s++ ) {
						//	arrM[s].ckwh = parseFloat(arrModuleTotals[s]);
						//}
						//nlapiSendEmail(71418,71418,'arrM',JSON.stringify(arrM),null,null,null,null);
						arrReports[i].facilities[j].customers[k].panels[l].modules[m].months = arrModuleTotals;
						
					}
				}
				var arrDevices = arrCustomers[k].devices;
				for ( var o = 0; arrDevices != null && o < arrDevices.length; o++ ) {
					var arrSensors = arrDevices[o].sensors;
					for ( var p = 0; arrSensors != null && p < arrSensors.length; p++ ) {
						
						var objPoint = _.find(arrEnvironmental, function(arr){ return (arr.pointid == arrSensors[p].sensorextid); });
						if(objPoint != null){
							arrReports[i].facilities[j].customers[k].devices[o].sensors[p].months = objPoint.months;
						}
					}
				}
			}
		}
	}

	return arrReports;
}

function get_points_values (arrPoints, arrValues, type){
	
	var arrPointsValues = new Array(); 
	for ( var i = 0; arrPoints != null && i < arrPoints.length; i++ ) {
		var objPoint = new Object();
		objPoint["pointid"] = arrPoints[i];
		var arrPValues = _.filter(arrValues, function(arr){
	        return (arr.POINTID === arrPoints[i]);
		});
		var arrMonths = new Array();
		for ( var j = 0; arrPValues != null && j < arrPValues.length; j++ ) {
			var objMonth = new Object();
			objMonth["monthid"] = j;
			objMonth["month"] = arrPValues[j].MONTH;
			
			var value = arrPValues[j].VAL;
			if(value < 0){
				value = 0;
			}
			if(j == 0){
				var prevj = 0;
			}
			else{
				var prevj = j - 1;
			}
			var prevalue = arrPValues[prevj].VAL;
			if(prevalue <  0){
				prevalue = 0;
			}
			var difference = value - prevalue;
			
			if(type == 'energy'){
				objMonth["kwh"] = parseFloat(value);
				objMonth["ckwh"] = 0;
				objMonth["kwhpre"] = parseFloat(prevalue);
				objMonth["kwhdiff"] = parseFloat(difference);
			}
			if(type == 'enviro'){
				objMonth["value"] = parseFloat(value);
			}
			if(type == 'current'){
				objMonth["hours"] = arrPValues[j].COUNT;
				objMonth["amps"] = parseFloat(value);
				objMonth["kwh"] = 0;
				
				var month = arrPValues[j].MONTH;
				var start = moment(month + '-01');
				var end = moment(month + '-01').endOf('month');
				var mhours = end.diff(start, 'hours') +1;
				
				objMonth["mhours"] = mhours;
				objMonth["mkwh"] = 0;
			}
			arrMonths.push(objMonth);
		}
		objPoint["months"] = arrMonths;
		arrPointsValues.push(objPoint);
	}
	return arrPointsValues;
}



function get_file_folder(){
	
	var d = new Date();
	var m = d.getMonth();
	var y = d.getFullYear();

	if(parseInt(y) == 2014){
		switch(parseInt(m)) {
			case 0:
				fileFolder = 1266920;
				break;
			case 1:
				fileFolder = 1266922;
			  break;
			case 2:
				fileFolder = 1267023;
			  break;
			case 3:
				fileFolder = 1267024;
				break;
			case 4:
				fileFolder = 1267025;
			  break;
			case 5:
				fileFolder = 1267026;
			  break;  
			case 6:
				fileFolder = 1267027;
				break;
			case 7:
				fileFolder = 1267028;
			  break;
			case 8:
				fileFolder = 1267029;
			  break;
			case 9:
				fileFolder = 1267030;
			  break;
			case 10:
				fileFolder = 1267031;
			  break;
			case 11:
				fileFolder = 1267032;
			  break;
			default:
				fileFolder = 1267033;
		}
	}
	else{
		fileFolder = 1267033;
	}
	return fileFolder;
}