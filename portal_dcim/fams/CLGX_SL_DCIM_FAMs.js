nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_FAMs.js
//	Script Name:	CLGX_SL_DCIM_FAMs
//	Script Id:		customscript_clgx_sl_dcim_fams
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		07/01/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=308&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_fams (request, response){
	try {

		var facilityid = request.getParameter('root')
		
		var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/alarms/get_alarms.cfm');
		//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/getalarms.cfm');
		var jsonAlarms = requestURL.body;
		var arrAlarms = JSON.parse( jsonAlarms );

		// pull out legacy services linked to a FAM -------------------------------------------------------------------------------------------------------------
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custentity_cologix_facility",null,"anyof",facilityid));
		var searchServices = nlapiLoadSearch('job', 'customsearch_clgx_dcim_services');
		searchServices.addColumns(arrColumns);
		searchServices.addFilters(arrFilters);
		var resultSet = searchServices.runSearch();
		var arrServicesFAMs = new Array();
		resultSet.forEachResult(function(searchResult) {
			var serviceid = searchResult.getValue('internalid',null,null);
			var service = searchResult.getValue('entityid',null,null);
			var arrMultiFAMsIDs = searchResult.getValue('custentity_clgx_service_fams',null,null).split(",");
			var arrMultiFAMs = searchResult.getText('custentity_clgx_service_fams',null,null).split(",");
			for ( var j = 0; arrMultiFAMsIDs != null && j < arrMultiFAMsIDs.length; j++ ) {
				var objFAM = new Object();
				objFAM["serviceid"] = parseInt(serviceid);
				objFAM["service"] = service;
				objFAM["famid"] = parseInt(arrMultiFAMsIDs[j]);
				objFAM["fam"] = arrMultiFAMs[j];
				arrServicesFAMs.push(objFAM);
			}
			return true; // return true to keep iterating
		});
		
		// pull out power circuits linked to a FAM -------------------------------------------------------------------------------------------------------------
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_facility",null,"anyof",facilityid));
		var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_powers');
		searchPowers.addColumns(arrColumns);
		searchPowers.addFilters(arrFilters);
		var resultSet = searchPowers.runSearch();
		var arrPowersFAMs = new Array();
		resultSet.forEachResult(function(searchResult) {
			var objPower = new Object();
			objPower["powers"] = parseInt(searchResult.getValue('internalid',null,'COUNT'));
			objPower["famid"] = parseInt(searchResult.getValue('custrecord_clgx_power_panel_pdpm',null,'GROUP'));
			objPower["fam"] = searchResult.getText('custrecord_clgx_power_panel_pdpm',null,'GROUP');
			arrPowersFAMs.push(objPower);
			return true; // return true to keep iterating
		});
		
		// pull out spaces linked to a FAM -------------------------------------------------------------------------------------------------------------
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custrecord_cologix_space_location",null,"anyof",facilityid));
		var searchSpaces = nlapiLoadSearch('customrecord_cologix_space', 'customsearch_clgx_dcim_spaces');
		searchSpaces.addColumns(arrColumns);
		searchSpaces.addFilters(arrFilters);
		var resultSet = searchSpaces.runSearch();
		var arrSpacesFAMs = new Array();
		resultSet.forEachResult(function(searchResult) {
			var spaceid = searchResult.getValue('internalid',null,null);
			var space = searchResult.getValue('entityid',null,null);
			var arrMultiFAMsIDs = searchResult.getValue('custrecord_cologix_space_fams',null,null).split(",");
			var arrMultiFAMs = searchResult.getText('custrecord_cologix_space_fams',null,null).split(",");
			for ( var j = 0; arrMultiFAMsIDs != null && j < arrMultiFAMsIDs.length; j++ ) {
				var objFAM = new Object();
				objFAM["spaceid"] = parseInt(spaceid);
				objFAM["space"] = space;
				objFAM["famid"] = parseInt(arrMultiFAMsIDs[j]);
				objFAM["fam"] = arrMultiFAMs[j];
				arrSpacesFAMs.push(objFAM);
			}
			return true; // return true to keep iterating
		});
		
		// build FAMs array -------------------------------------------------------------------------------------------------------------
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custrecord_assetfacility",null,"anyof",facilityid));
		var searchFAMs = nlapiLoadSearch('customrecord_ncfar_asset', 'customsearch_clgx_dcim_fams_all');
		searchFAMs.addColumns(arrColumns);
		searchFAMs.addFilters(arrFilters);
		var resultSet = searchFAMs.runSearch();
		var arrFAMs = new Array();
		var strFAMsCSV = 'FAM,Services,Spaces,Powers\n';
		var arrMarketsIDs = new Array();
		var arrMarkets = new Array();
		var arrMarketsFacilitiesIDs = new Array();
		var arrFacilitiesIDs = new Array();
		var arrFacilities = new Array();
		
		resultSet.forEachResult(function(searchResult) {
			
			var marketid = parseInt(searchResult.getValue('custrecord_clgx_facilty_market', 'CUSTRECORD_ASSETFACILITY', null));
			var market = searchResult.getText('custrecord_clgx_facilty_market', 'CUSTRECORD_ASSETFACILITY', null);
			var facilityid = parseInt(searchResult.getValue('custrecord_assetfacility', null, null));
			var facility = searchResult.getText('custrecord_assetfacility', null, null);
			var parentid = parseInt(searchResult.getValue('custrecord_assetparent', null, null));
			var parent = searchResult.getText('custrecord_assetparent', null, null);
			
			var objFAM = new Object();
			objFAM["marketid"] = parseInt(marketid);
			objFAM["market"] = market;
			objFAM["facilityid"] = parseInt(facilityid);
			objFAM["facility"] = facility;
			//if(parent != '' && parent != null){
				objFAM["parentid"] = parseInt(parentid);
				objFAM["parent"] = parent;
			//}
			//else{
			//	objFAM["parentid"] = parseInt(facilityid);
			//	objFAM["parent"] = facility;
			//}
			objFAM["nodeid"] = parseInt(searchResult.getValue('internalid', null, null));
			objFAM["node"] = searchResult.getValue('name', null, null);
			strFAMsCSV += searchResult.getValue('name', null, null) + ',';
			objFAM["name"] = searchResult.getValue('altname', null, null);
			objFAM["descr"] = searchResult.getValue('custrecord_assetdescr', null, null);
			objFAM["deviceid"] = parseInt(searchResult.getValue('internalid', 'CUSTRECORD_CLGX_OD_FAM_DEVICE', null));
			objFAM["device"] = searchResult.getValue('name', 'CUSTRECORD_CLGX_OD_FAM_DEVICE', null);
			objFAM["externalid"] = parseInt(searchResult.getValue('externalid', 'CUSTRECORD_CLGX_OD_FAM_DEVICE', null));
	    	var abjAlarm = _.find(arrAlarms, function(num){ return num.EXTERNAL_ID == searchResult.getValue('name', null, null); });
	    	//alarms
			if(abjAlarm != undefined){
				objFAM["alarm"] = abjAlarm.SEVERITY;
			}
			else{
				objFAM["alarm"] = 0;
			}
			// services
			var arrFAMServices = _.filter(arrServicesFAMs, function(arr){
		        return (arr.famid === objFAM.nodeid);
			});
			if(arrFAMServices != null){
				if(arrFAMServices.length > 0){
					objFAM["services"] = arrFAMServices.length;
					strFAMsCSV += arrFAMServices.length + ',';
				}
				else{
					objFAM["services"] = '';
					strFAMsCSV += ',';
				}
			}
			else{
				objFAM["services"] = '';
				strFAMsCSV += ',';
			}
			// spaces 
			var arrFAMSpaces = _.filter(arrSpacesFAMs, function(arr){
		        return (arr.famid === objFAM.nodeid);
			});
			if(arrFAMSpaces != null){
				if(arrFAMSpaces.length > 0){
					objFAM["spaces"] = arrFAMSpaces.length;
					strFAMsCSV += arrFAMSpaces.length + ',';
				}
				else{
					objFAM["spaces"] = '';
					strFAMsCSV += ',';
				}
			}
			else{
				objFAM["spaces"] = '';
				strFAMsCSV += ',';
			}
			// powers
			var arrFAMPowers = _.filter(arrPowersFAMs, function(arr){
		        return (arr.famid === objFAM.nodeid);
			});
			if(arrFAMPowers != null){
				if(arrFAMPowers.length > 0){
					objFAM["powers"] = arrFAMPowers[0].powers;
					strFAMsCSV += arrFAMPowers[0].powers + '\n';
				}
				else{
					objFAM["powers"] = '';
					strFAMsCSV += '\n';
				}
			}
			else{
				objFAM["powers"] = '';
				strFAMsCSV += '\n';
			}
			objFAM["expanded"] = true;
			objFAM["iconCls"] = 'fam';
			objFAM["leaf"] = false;
			objFAM["children"] = [];

			arrFAMs.push(objFAM);
			return true;
		});


		var arrMarketsIDs = _.uniq(_.pluck(arrFAMs, 'marketid'));
		var arrMarkets = _.uniq(_.pluck(arrFAMs, 'market'));
		
		var objTree = new Object();
		objTree["text"] = '.';
		
		var arrNodesMarkets = new Array();
		for ( var i = 0; arrMarketsIDs != null && i < arrMarketsIDs.length; i++ ) {
	    	var objMarket = new Object();
	    	objMarket["parentid"] = 0;
	    	objMarket["parent"] = '';
	    	objMarket["nodeid"] = parseInt(arrMarketsIDs[i]);
	    	objMarket["node"] = arrMarkets[i];
	    	objMarket["expanded"] = true;
	    	objMarket["iconCls"] = getMarketIcon(arrMarkets[i]);
	    	//objMarket["leaf"] = false;
	    	//objMarket["children"] = [];
	    	//arrFAMs.push(objMarket);
	    	
	    	// extract facilities of this market -------------------------------------------------------------------------------------------------------------
	    	var arrMarketFacilities = _.filter(arrFAMs, function(arr){
		        return (arr.marketid === objMarket.nodeid);
			});
			var arrFacilitiesIDs = _.uniq(_.pluck(arrMarketFacilities, 'facilityid'));
			var arrFacilities = _.uniq(_.pluck(arrMarketFacilities, 'facility'));
	    	
			var arrNodesFacilities = new Array();
			for ( var j = 0; arrFacilitiesIDs != null && j < arrFacilitiesIDs.length; j++ ) {
		    	var objFacility = new Object();
		    	objFacility["parentid"] = parseInt(objMarket.nodeid);
		    	objFacility["parent"] = objMarket.node;
		    	objFacility["nodeid"] = parseInt(arrFacilitiesIDs[j]);
		    	objFacility["node"] = arrFacilities[j];
		    	objFacility["expanded"] = true;
		    	objFacility["iconCls"] = 'location';
		    	//objFacility["leaf"] = false;
		    	//objFacility["children"] = [];
		    	//arrFAMs.push(objFacility);
		    	
		    	// extract fams of this facility -------------------------------------------------------------------------------------------------------------
		    	var arrFacilityFAMs = _.filter(arrFAMs, function(arr){
			        return (arr.facilityid === objFacility.nodeid);
				});
		    
		    	// change grid into a tree -------------------------------------------------------------------------------------------------------------
		    	var buildTree = function(tree, item) {
				    if (item) { // if item then have parent
				        for (var k=0; k<tree.length; k++) { // parses the entire tree in order to find the parent
				            if (tree[k].nodeid === item.parentid) { // bingo!
				                tree[k].children.push(item); // add the child to his parent
				                break;
				            }
				            else buildTree(tree[k].children, item); // if item doesn't match but tree have childs then parses childs again to find item parent
				        }
				    }
				    else { // if no item then is a root item, multiple root items are supported
				        var idx = 0;
				        while (idx < tree.length)
				            if (tree[idx].parentid > 0) buildTree(tree, tree.splice(idx, 1)[0]) // if have parent then remove it from the array to relocate it to the right place
				            else idx++; // if doesn't have parent then is root and move it to the next object
				    }
				}
				buildTree(arrFacilityFAMs);
				
		    	objFacility["children"] = arrFacilityFAMs;
				arrNodesFacilities.push(objFacility);
			}
			objMarket["children"] = arrNodesFacilities;
			arrNodesMarkets.push(objMarket);
		}
		
		objTree["children"] = arrNodesMarkets;
		
		
		var objFile = nlapiLoadFile(1569742);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{treeJSON}','g'),JSON.stringify(objTree));
		response.write( html );
		
		/*
		//var strFAMsCSV = JSON.stringify(arrFAMsCSV);
		var csvFAMs = nlapiCreateFile('fams.csv', 'PLAINTEXT', strFAMsCSV);
		csvFAMs.setFolder(1080872);
		nlapiSubmitFile(csvFAMs);
		*/
		
		response.write( JSON.stringify(objTree) );
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function getMarketIcon(market){
	if(market == 'Columbus' || market == 'Dallas' || market == 'Jacksonville' || market == 'Minneapolis' || market == 'Cologix HQ'){
		return 'us';
	}
	else if (market == ''){
		return 'alarm';
	}
	else{
		return 'ca';
	}
}

//check if value is in the array
function inArray(val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}
