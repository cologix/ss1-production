//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_Customers.js
//	Script Name:	CLGX_SL_DCIM_Customers
//	Script Id:		customscript_clgx_sl_dcim_customers
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		07/07/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=308&deploy=1
//-------------------------------------------------------------------------------------------------


var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP').setSort(false));
var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",internalid));
	arrFilters.push(new nlobjSearchFilter("isinactive",null,"noneof",'@NONE@'));
var searchRecords = nlapiSearchRecord('job', 'customsearch_clgx_dcim_services', null, null);

for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
        	var searchResult = searchResults[i];
        	var customerid = searchResult.getValue('internalid',null,'GROUP');
}







function suitelet_dcim_customers (request, response){
	try {
		
		var famid = request.getParameter('famid');
		
		// load all FAMs in arrFAMs
		var arrFAM = new Array();
		
		// look for all children of this FAM and push them to arrFAMChildren
		
		
		
		
		var arrFAMChildren = new Array();
		
		
		
		if(famid > 0){
			
			var port = 10313; // railo
			//var port = 10811; // coldfusion
			
			var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/alarms/get_alarms.cfm');
			//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/getalarms.cfm');
			var jsonAlarms = requestURL.body;
			var arrAlarms = JSON.parse( jsonAlarms );
			arrFAMChildren.push(famid);
		}
		else{
			var arrAlarms = [];
		}

		// pull out all legacy services linked to a FAM -------------------------------------------------------------------------------------------------------------
		var searchServices = nlapiLoadSearch('job', 'customsearch_clgx_dcim_services');
		if(famid > 0){
			searchServices.addFilter(new nlobjSearchFilter("custentity_clgx_service_fams",null,"anyof",arrFAMChildren));
		}
		var resultSet = searchServices.runSearch();
		var arrServicesFAMs = new Array();
		resultSet.forEachResult(function(searchResult) {
			var columnsCustomer = searchResult.getAllColumns();
			
			var arrMultiFAMsIDs = searchResult.getValue('custentity_clgx_service_fams',null,null).split(",");
			var arrMultiFAMs = searchResult.getText('custentity_clgx_service_fams',null,null).split(",");
			
			for ( var j = 0; arrMultiFAMsIDs != null && j < arrMultiFAMsIDs.length; j++ ) {
				var objFAM = new Object();
				objFAM["customerid"] = parseInt(searchResult.getValue('customer',null,null));
				objFAM["customer"] = searchResult.getText('customer',null,null);
				objFAM["serviceid"] = parseInt(searchResult.getValue('internalid',null,null));
				objFAM["service"] = searchResult.getValue('entityid',null,null);
				objFAM["famid"] = parseInt(arrMultiFAMsIDs[j]);
				objFAM["fam"] = arrMultiFAMs[j];
				arrServicesFAMs.push(objFAM);
			}
			return true; // return true to keep iterating
		});
		
		// pull out all power circuits linked to a FAM -------------------------------------------------------------------------------------------------------------
		var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_powers_all');
		if(famid > 0){
			searchPowers.addFilter(new nlobjSearchFilter("custrecord_clgx_power_panel_pdpm",null,"anyof",arrFAMChildren));
		}
		var resultSet = searchPowers.runSearch();
		var arrPowersFAMs = new Array();
		resultSet.forEachResult(function(searchResult) {
			var columnsPowers = searchResult.getAllColumns();
			var objPower = new Object();
			objPower["customerid"] = parseInt(searchResult.getValue(columnsPowers[4]));
			objPower["customer"] = searchResult.getValue(columnsPowers[5]);
			objPower["serviceid"] = parseInt(searchResult.getValue('custrecord_cologix_power_service',null,null));
			objPower["service"] = searchResult.getText('custrecord_cologix_power_service',null,null);
			objPower["powerid"] = parseInt(searchResult.getValue('internalid',null,null));
			objPower["power"] = searchResult.getValue('entityid',null,null);
			objPower["famid"] = parseInt(searchResult.getValue('custrecord_clgx_power_panel_pdpm',null,null));
			objPower["fam"] = searchResult.getText('custrecord_clgx_power_panel_pdpm',null,null);
			arrPowersFAMs.push(objPower);
			return true; // return true to keep iterating
		});
		

		// pull out all spaces linked to a FAM -------------------------------------------------------------------------------------------------------------
		var searchSpaces = nlapiLoadSearch('customrecord_cologix_space', 'customsearch_clgx_dcim_spaces');
		if(famid > 0){
			searchSpaces.addFilter(new nlobjSearchFilter("custrecord_cologix_space_fams",null,"anyof",arrFAMChildren));
		}
		var resultSet = searchSpaces.runSearch();
		var arrSpacesFAMs = new Array();
		resultSet.forEachResult(function(searchResult) {
			var columnsSpaces = searchResult.getAllColumns();
			
			var arrMultiFAMsIDs = searchResult.getValue('custrecord_cologix_space_fams',null,null).split(",");
			var arrMultiFAMs = searchResult.getText('custrecord_cologix_space_fams',null,null).split(",");
			
			for ( var j = 0; arrMultiFAMsIDs != null && j < arrMultiFAMsIDs.length; j++ ) {
				var objSpace = new Object();
				objSpace["customerid"] = parseInt(searchResult.getValue(columnsSpaces[4]));
				objSpace["customer"] = searchResult.getValue(columnsSpaces[5]);
				objSpace["serviceid"] = parseInt(searchResult.getValue('custrecord_cologix_space_project',null,null));
				objSpace["service"] = searchResult.getValue('custrecord_cologix_space_project',null,null);
				objSpace["spaceid"] = parseInt(searchResult.getValue('internalid',null,null));
				objSpace["space"] = searchResult.getValue('entityid',null,null);
				objSpace["famid"] = parseInt(arrMultiFAMsIDs[j]);
				objSpace["fam"] = arrMultiFAMs[j];
				arrSpacesFAMs.push(objSpace);
			}
			return true; // return true to keep iterating
		});
		
		var arrServices = _.union(_.pluck(arrServicesFAMs, 'serviceid'), _.pluck(arrPowersFAMs, 'serviceid'), _.pluck(arrSpacesFAMs, 'serviceid'));
		
		// Search all customers of all found services -------------------------------------------------------------------------------------------------------------
		var searchCustomers = nlapiLoadSearch('job', 'customsearch_clgx_dcim_customers');
		searchCustomers.addFilter(new nlobjSearchFilter("internalid",null,"anyof",arrServices));
		var resultSet = searchCustomers.runSearch();
		var arrCustomers = new Array();
		resultSet.forEachResult(function(searchResult) {
			
			var objCustomer = new Object();
			objCustomer["marketid"] = parseInt(searchResult.getValue('custrecord_clgx_facilty_market','CUSTENTITY_COLOGIX_FACILITY','GROUP'));
			objCustomer["market"] = searchResult.getText('custrecord_clgx_facilty_market','CUSTENTITY_COLOGIX_FACILITY','GROUP');
			objCustomer["facilityid"] = parseInt(searchResult.getValue('custentity_cologix_facility',null,'GROUP'));
			objCustomer["facility"] = searchResult.getText('custentity_cologix_facility',null,'GROUP');
			objCustomer["customerid"] = parseInt(searchResult.getValue('parent',null,'GROUP'));
			objCustomer["customer"] = searchResult.getText('parent',null,'GROUP');
			arrCustomers.push(objCustomer);

			return true; // return true to keep iterating
		});
		
		var arrMarkets = _.uniq(_.pluck(arrCustomers, 'marketid'));
		
		var searchRecords = nlapiSearchRecord('job', 'customsearch_clgx_dcim_customers', null, null);
	
		
		
		// build FAMs array -------------------------------------------------------------------------------------------------------------
		var searchFAMs = nlapiLoadSearch('customrecord_ncfar_asset', 'customsearch_clgx_dcim_fams_all');
		var resultSet = searchFAMs.runSearch();
		var arrFAMs = new Array();
		var arrMarketsIDs = new Array();
		var arrMarkets = new Array();
		var arrMarketsFacilitiesIDs = new Array();
		var arrFacilitiesIDs = new Array();
		var arrFacilities = new Array();
		resultSet.forEachResult(function(searchResult) {
			
			var marketid = parseInt(searchResult.getValue('custrecord_clgx_facilty_market', 'CUSTRECORD_ASSETFACILITY', null));
			var market = searchResult.getText('custrecord_clgx_facilty_market', 'CUSTRECORD_ASSETFACILITY', null);
			var facilityid = parseInt(searchResult.getValue('custrecord_assetfacility', null, null));
			var facility = searchResult.getText('custrecord_assetfacility', null, null);

			var objFAM = new Object();
			objFAM["marketid"] = parseInt(marketid);
			objFAM["market"] = market;
			objFAM["facilityid"] = parseInt(facilityid);
			objFAM["facility"] = facility;
			objFAM["parentid"] = parseInt(searchResult.getValue('custrecord_assetparent', null, null));
			objFAM["parent"] = searchResult.getText('custrecord_assetparent', null, null);
			objFAM["nodeid"] = parseInt(searchResult.getValue('internalid', null, null));
			objFAM["node"] = searchResult.getValue('name', null, null);
			objFAM["name"] = searchResult.getValue('altname', null, null);
			objFAM["descr"] = searchResult.getValue('custrecord_assetdescr', null, null);
			objFAM["deviceid"] = parseInt(searchResult.getValue('internalid', 'CUSTRECORD_CLGX_OD_FAM_DEVICE', null));
			objFAM["device"] = searchResult.getValue('name', 'CUSTRECORD_CLGX_OD_FAM_DEVICE', null);
			objFAM["externalid"] = parseInt(searchResult.getValue('externalid', 'CUSTRECORD_CLGX_OD_FAM_DEVICE', null));
	    	var abjAlarm = _.find(arrAlarms, function(num){ return num.EXTERNAL_ID == searchResult.getValue('name', null, null); });
	    	//alarms
			if(abjAlarm != undefined){
				objFAM["alarm"] = abjAlarm.SEVERITY;
			}
			else{
				objFAM["alarm"] = 0;
			}
			// services
			var arrFAMServices = _.filter(arrServicesFAMs, function(arr){
		        return (arr.famid === objFAM.nodeid);
			});
			if(arrFAMServices != null){
				if(arrFAMServices.length > 0){
					objFAM["services"] = arrFAMServices.length;
				}
				else{
					objFAM["services"] = '';
				}
			}
			else{
				objFAM["services"] = '';
			}
			// spaces 
			var arrFAMSpaces = _.filter(arrSpacesFAMs, function(arr){
		        return (arr.famid === objFAM.nodeid);
			});
			if(arrFAMSpaces != null){
				if(arrFAMSpaces.length > 0){
					objFAM["spaces"] = arrFAMSpaces.length;
				}
				else{
					objFAM["spaces"] = '';
				}
			}
			else{
				objFAM["spaces"] = '';
			}
			// powers
			var arrFAMPowers = _.filter(arrPowersFAMs, function(arr){
		        return (arr.famid === objFAM.nodeid);
			});
			if(arrFAMPowers != null){
				if(arrFAMPowers.length > 0){
					objFAM["powers"] = arrFAMPowers[0].powers;
				}
				else{
					objFAM["powers"] = '';
				}
			}
			else{
				objFAM["powers"] = '';
			}
			objFAM["expanded"] = true;
			objFAM["iconCls"] = 'fam';
			objFAM["leaf"] = false;
			objFAM["children"] = [];

			arrFAMs.push(objFAM);
			return true;
			});

		var arrMarketsIDs = _.uniq(_.pluck(arrFAMs, 'marketid'));
		var arrMarkets = _.uniq(_.pluck(arrFAMs, 'market'));
		
		var objTree = new Object();
		objTree["text"] = '.';
		
		var arrNodesMarkets = new Array();
		for ( var i = 0; arrMarketsIDs != null && i < arrMarketsIDs.length; i++ ) {
	    	var objMarket = new Object();
	    	objMarket["nodeid"] = parseInt(arrMarketsIDs[i]);
	    	objMarket["node"] = arrMarkets[i];
	    	objMarket["expanded"] = false;
	    	objMarket["iconCls"] = getMarketIcon(arrMarkets[i]);
	    	objMarket["leaf"] = false;

	    	// extract facilities of this market -------------------------------------------------------------------------------------------------------------
	    	var arrMarketFacilities = _.filter(arrFAMs, function(arr){
		        return (arr.marketid === objMarket.nodeid);
			});
			var arrFacilitiesIDs = _.uniq(_.pluck(arrMarketFacilities, 'facilityid'));
			var arrFacilities = _.uniq(_.pluck(arrMarketFacilities, 'facility'));
	    	
			var arrNodesFacilities = new Array();
			for ( var j = 0; arrFacilitiesIDs != null && j < arrFacilitiesIDs.length; j++ ) {
		    	var objFacility = new Object();
		    	objFacility["nodeid"] = parseInt(arrFacilitiesIDs[j]);
		    	objFacility["node"] = arrFacilities[j];
		    	objFacility["expanded"] = false;
		    	objFacility["iconCls"] = 'location';
		    	objFacility["leaf"] = false;
		    	
		    	// extract fams of this facility -------------------------------------------------------------------------------------------------------------
		    	var arrFacilityFAMs = _.filter(arrFAMs, function(arr){
			        return (arr.marketid === objMarket.nodeid && arr.facilityid === objFacility.nodeid);
				});
		    	
		    	// change grid into a tree -------------------------------------------------------------------------------------------------------------
		    	var buildTree = function(tree, item) {
				    if (item) { // if item then have parent
				        for (var k=0; k<tree.length; k++) { // parses the entire tree in order to find the parent
				            if (tree[k].nodeid === item.parentid) { // bingo!
				                tree[k].children.push(item); // add the child to his parent
				                break;
				            }
				            else buildTree(tree[k].children, item); // if item doesn't match but tree have childs then parses childs again to find item parent
				        }
				    }
				    else { // if no item then is a root item, multiple root items are supported
				        var idx = 0;
				        while (idx < tree.length)
				            if (tree[idx].parentid > 0) buildTree(tree, tree.splice(idx, 1)[0]) // if have parent then remove it from the array to relocate it to the right place
				            else idx++; // if doesn't have parent then is root and move it to the next object
				    }
				}
				buildTree(arrFacilityFAMs);

				objFacility["children"] = arrFacilityFAMs;
				arrNodesFacilities.push(objFacility);
			}
			objMarket["children"] = arrNodesFacilities;
			arrNodesMarkets.push(objMarket);
		}
		
		objTree["children"] = arrNodesMarkets;
		response.write( JSON.stringify(objTree) );

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function getMarketIcon(market){
	if(market == 'Columbus' || market == 'Dallas' || market == 'Jacksonville' || market == 'Minneapolis' || market == 'Cologix HQ'){
		return 'us';
	}
	else if (market == ''){
		return 'alarm';
	}
	else{
		return 'ca';
	}
}

//check if value is in the array
function inArray(val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}
