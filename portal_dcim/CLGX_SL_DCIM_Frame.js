//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_Frame.js
//	Script Name:	CLGX_SL_DCIM_Frame
//	Script Id:		customscript_clgx_sl_dcim_frame
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/22/2014
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_frame (request, response){
	try {
		var roleid = nlapiGetRole();
		var closed = 0;
		if(closed == 1 && (roleid != -5 && roleid != 3 && roleid != 18)){ // if module is closed any not admin using it
			var arrParam = new Array();
			arrParam['custscript_internal_message'] = 'This module is closed for modifications. Sorry for the inconvenience. Please come back later.';
			nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
		}
		
		var formFrame = nlapiCreateForm('Cologix Command');
		var fieldFrame = formFrame.addField('custpage_clgx_frame_html','inlinehtml', null, null, null);
		var frameHTML = '<iframe name="dcim" id="dcim" src="/app/site/hosting/scriptlet.nl?script=305&deploy=1" height="600px" width="1235px" frameborder="0" scrolling="no"></iframe>';
		fieldFrame.setDefaultValue(frameHTML);
		response.writePage( formFrame );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}