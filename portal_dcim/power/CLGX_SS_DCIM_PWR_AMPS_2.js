nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_AMPS_2.js
//	Script Name:	CLGX_SS_DCIM_PWR_AMPS_2
//	Script Id:		customscript_clgx_ss_dcim_pwr_amps_2
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/30/2014
//-------------------------------------------------------------------------------------------------

function scheduled_dcim_pwr_amps_2(){
	
    try{

    	// the search returns > 4000 so split it by Amps
    	var arrBatches = new Array();
    	var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_amps',null,'GROUP').setSort(false));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_volts',null,'GROUP').setSort(false));
		var arrFilters = new Array();
		var searchAmps = nlapiSearchRecord('customrecord_clgx_dcim_points_day', null, arrFilters, arrColumns);
		for ( var i = 0; searchAmps != null && i < searchAmps.length; i++ ) {
			//arrBatches.push(parseInt(searchAmps[i].getValue('custrecord_clgx_dcim_points_day_amps',null,'GROUP')));
			arrBatches.push({
				"amps": parseInt(searchAmps[i].getValue('custrecord_clgx_dcim_points_day_amps',null,'GROUP')) || 0,
				"volts": parseInt(searchAmps[i].getValue('custrecord_clgx_dcim_points_day_volts',null,'GROUP')) || 0
			});
		}
    	
		var arrPowers = new Array();
    	var arrPowersIDs = new Array();
    	
    	for ( var i = 0; arrBatches != null && i < arrBatches.length; i++ ) {

			var arrColumns = new Array();
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_amps",null,"equalto",arrBatches[i].amps));
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_volts",null,"equalto",arrBatches[i].volts));
			var searchPoints = nlapiLoadSearch('customrecord_clgx_dcim_points_day', 'customsearch_clgx_dcim_pwr_util_outage_2');
			searchPoints.addColumns(arrColumns);
			searchPoints.addFilters(arrFilters);
			var resultSet = searchPoints.runSearch();
			resultSet.forEachResult(function(searchResult) {
				var columns = searchResult.getAllColumns();
				var objPower = new Object();
				objPower["customerid"] = parseInt(searchResult.getValue(columns[0]));
				objPower["customer"] = searchResult.getText(columns[0]);
				objPower["emailsent"] = searchResult.getValue(columns[7]);
				
				var oldcount = parseInt(searchResult.getValue(columns[10]));
				if(oldcount > 0){
					objPower["oldcount"] = oldcount;
					objPower["newcount"] = oldcount;
				}
				else{
					objPower["oldcount"] = 0;
					objPower["newcount"] = 0;
				}
				
				objPower["facilityid"] = parseInt(searchResult.getValue(columns[6]));
				objPower["facility"] = searchResult.getText(columns[6]);
				objPower["serviceid"] = parseInt(searchResult.getValue(columns[1]));
				objPower["service"] = searchResult.getText(columns[1]);
				
				
				var powerid = parseInt(searchResult.getValue(columns[2]));
				objPower["powerid"] = powerid;
				objPower["power"] = searchResult.getText(columns[2]);
				
				var pairid = parseInt(searchResult.getValue(columns[3]));
				if(pairid > 0){
					objPower["pairid"] = parseInt(searchResult.getValue(columns[3]));
					objPower["pair"] = searchResult.getText(columns[3]);
				}
				else{
					objPower["pairid"] = 0;
					objPower["pair"] = '';
				}
				
				arrPowersIDs.push(powerid);
				arrPowersIDs.push(pairid);
				
				var panelid = parseInt(searchResult.getValue(columns[12]));
				objPower["panelid"] = panelid;
				objPower["panel"] = searchResult.getText(columns[12]);

				//objPower["breaker"] = parseInt(searchResult.getValue(columns[4]));
				
				objPower["circuit"] = parseInt(searchResult.getValue(columns[4]));
				objPower["space"] = searchResult.getText(columns[9]);
				objPower["volts"] = searchResult.getText(columns[8]);
				objPower["amps"] = parseFloat(searchResult.getValue(columns[5]));
				objPower["ampspair"] = 0;
				objPower["circuitpair"] = 0;
				objPower["absum"] = 0;
				objPower["usage"] = 0;
				
				arrPowers.push(objPower);
				return true;
			});
    	}

    	var arrPowersUniqIDs = _.compact(_.uniq(arrPowersIDs));
    	
    	for ( var p = 0; arrPowers != null && p < arrPowers.length; p++ ) {

			var objPair = _.find(arrPowers, function(arr){ return (arr.powerid == arrPowers[p].pairid); });
			if(objPair != null){
				circuitpair = objPair.circuit;
				arrPowers[p].ampspair = objPair.amps;
				arrPowers[p].circuitpair = objPair.circuit;
				var absum = arrPowers[p].amps + objPair.amps;
			}
			else{
				var absum = arrPowers[p].amps
			}
    		
			var usage = parseFloat(((absum * 100) / arrPowers[p].circuit).toFixed(2));
    		
			arrPowers[p].absum = absum;
			arrPowers[p].usage = usage;
    		
			if(usage >= 80){
				arrPowers[p].newcount = arrPowers[p].oldcount + 1;
			}
    		
    	}
    	
    	var strBreakers = JSON.stringify(arrPowers);
		var jsonUsageAll = nlapiCreateFile('powers_usage_all2.json', 'PLAINTEXT', strBreakers);
		jsonUsageAll.setFolder(1294374);
		nlapiSubmitFile(jsonUsageAll);
		
		var arrPowersUniqIDs = _.compact(_.uniq(arrPowersIDs));
		var jsonPowersIDs = nlapiCreateFile('powers_ids2.json', 'PLAINTEXT', JSON.stringify(arrPowersUniqIDs));
		jsonPowersIDs.setFolder(1294374);
		nlapiSubmitFile(jsonPowersIDs);
		
		var jsonPowersIDs = nlapiCreateFile('powers_ids_queue2.json', 'PLAINTEXT', JSON.stringify(arrPowersUniqIDs));
		jsonPowersIDs.setFolder(1294374);
		nlapiSubmitFile(jsonPowersIDs);
		
		var arrOutage = _.filter(arrPowers, function(arr){
	        //return (arr.usage > 80 || arr.oldcount > 0);
	        return (arr.usage > 80);
		});
		
		// keep only the A part of A+B Powers
		var arrUniqIDs = new Array();
		var arrOutageA = new Array();
		for ( var i = 0; arrOutage != null && i < arrOutage.length; i++ ) {
			if(_.indexOf(arrUniqIDs, arrOutage[i].powerid) == -1){
				arrOutageA.push(arrOutage[i]);
				arrUniqIDs.push(arrOutage[i].powerid);
				if(arrOutage[i].pairid != '- None -'){
					arrUniqIDs.push(arrOutage[i].pairid);
				}
			}
		}
		
		arrOutageA = _.sortBy(arrOutageA, function(obj){ return obj.customer;});
		
		for ( var i = 0; arrOutageA != null && i < arrOutageA.length; i++ ) {
			
			var oldcount = arrOutageA[i].oldcount;
			var newcount = arrOutageA[i].newcount;
			
			if(newcount > oldcount){
				if(newcount == 1){
					arrOutageA[i]["email"] = 1;
				}
				else if(newcount == 5){
					arrOutageA[i]["email"] = 2;
				}
				else{
					arrOutageA[i]["email"] = 0;
				}
				nlapiSubmitField('customrecord_clgx_power_circuit', arrOutageA[i].powerid, 'custrecord_clgx_dcim_points_occurrences', arrOutageA[i].newcount);
				if(arrOutageA[i].pairid > 0){
					nlapiSubmitField('customrecord_clgx_power_circuit', arrOutageA[i].pairid, 'custrecord_clgx_dcim_points_occurrences', arrOutageA[i].newcount);
				}
			}
			else{
				if(oldcount < 5){
					nlapiSubmitField('customrecord_clgx_power_circuit', arrOutageA[i].powerid, 'custrecord_clgx_dcim_points_occurrences', 0);
					if(arrOutageA[i].pairid > 0){
						nlapiSubmitField('customrecord_clgx_power_circuit', arrOutageA[i].pairid, 'custrecord_clgx_dcim_points_occurrences', 0);
					}
				}
				arrOutageA[i]["email"] = 0;
			}
		}
		
		var strUsage = JSON.stringify(arrOutageA);
		var jsonUsage = nlapiCreateFile('powers_usage2.json', 'PLAINTEXT', strUsage);
		jsonUsage.setFolder(1294374);
		nlapiSubmitFile(jsonUsage);

		var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
        nlapiLogExecution('DEBUG', 'Usage', '| Usage = ' + usageConsumtion + ' |');
        
		var status = nlapiScheduleScript('customscript_clgx_ss_dcim_pwr_amps_3', 'customdeploy_clgx_ss_dcim_pwr_amps_3' ,null);

	}
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function get_max_logic (panel){
	
	var tor12 = 0;
	var tor1 = panel.indexOf("TOR1");
	var tor1205 = panel.indexOf("205");
	var tor2 = panel.indexOf("TOR2");
	
	if((tor2 > -1) || ((tor1 > -1) && (tor1205 > -1))){
		tor12 = 1;
		}
	return tor12;
}