//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_LIB_DCIM_PWR_Day_Test.js
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		10/12/2016
//-------------------------------------------------------------------------------------------------

function get_power (powerid){
    try {
    	var power = {
			"power": 0,
			"pair": 0,
			"count": 0,
			"amp_a": 0,
			"amp_b": 0,
			"amp_c": 0,
			"amp_max": 0,
			"amp_usg": 0,
			"kw": 0,
			"primary": get_od_power (powerid, true),
			"secondary": get_od_power (powerid, false)
		};
    	
		power.power = power.primary.power;
		power.pair = power.secondary.power;
		power.count = power.primary.count;
		
		power.amp_a = round(power.primary.amp_a + power.secondary.amp_a);
		power.amp_b = round(power.primary.amp_b + power.secondary.amp_b);
		power.amp_c = round(power.primary.amp_c + power.secondary.amp_c);
		
		power.amp_max = round(_.max([power.amp_a, power.amp_b, power.amp_c]));
		power.amp_usg = round(power.amp_max * 100 / power.primary.amps);
		power.kw = round(power.primary.kw + power.secondary.kw);

		power.primary.amp_max = round(_.max([power.primary.amp_a, power.primary.amp_b, power.primary.amp_c]));
		power.primary.amp_usg = round(power.primary.amp_max * 100 / power.primary.amps);
		
		power.secondary.amp_max = round(_.max([power.secondary.amp_a, power.secondary.amp_b, power.secondary.amp_c]));
		power.secondary.amp_usg = round(power.secondary.amp_max * 100 / power.secondary.amps);
		
		return power;
    }
    catch (error) {
    	return false;
    }
}

function get_od_power (powerid, primary){
	
	var factor = 0.9;
	if(!primary){
		var pairid = parseInt(nlapiLookupField('customrecord_clgx_power_circuit', powerid, 'custrecord_clgx_dcim_pair_power')) || 0;
		powerid = pairid;
	}
	var power = get_empty_power ();

	if(powerid){
		
		var rec = nlapiLoadRecord('customrecord_clgx_power_circuit', powerid);
		
		var voltage = rec.getFieldText('custrecord_cologix_power_volts');
		var volts = parseInt(voltage.substring(0, 3));
		
		var amperage = rec.getFieldText('custrecord_cologix_power_amps') || '0A';
		var amps = parseInt(amperage.slice(0, -1));
		
		var last = rec.getFieldValue('custrecord_clgx_dcim_date_last_replicate');
		var nstart = moment(last).add('days', 1).format('M/D/YYYY');
		var start = moment(last).add('days', 1).format('YYYY-MM-DD');
		var end = moment(last).add('days', 2).format('YYYY-MM-DD');

		var count = parseInt(rec.getFieldValue('custrecord_clgx_dcim_points_occurrences')) || 0;
		
		var phases = 1;
		if(voltage == '208V Single Phase'){
			phases = 2;
		}
		if(voltage == '208V Three Phase' || voltage == '240V Three Phase' || voltage == '600V Three Phase'){
			phases = 3;
		}
		if(phases == 1 || phases == 3){
			var kv = round(volts * factor / 1000);
		} else {
			var kv = round(volts * factor / 2000);
		}
		
		var so = parseInt(rec.getFieldValue('custrecord_power_circuit_service_order')) || 0;
		var service = parseInt(rec.getFieldValue('custrecord_cologix_power_service')) || 0;
		var customer = parseInt(nlapiLookupField('salesorder', so, 'entity')) || 0;
		
		var facility = parseInt(rec.getFieldValue('custrecord_cologix_power_facility')) || 0;
		var space = parseInt(rec.getFieldValue('custrecord_cologix_power_space')) || 0;
		var module = parseInt(rec.getFieldText('custrecord_cologix_power_upsbreaker')) || 0;
		var breaker = parseInt(rec.getFieldText('custrecord_cologix_power_circuitbreaker')) || 0;
		
		var device = parseInt(rec.getFieldValue('custrecord_clgx_dcim_device')) || 0;
		var panel = parseInt(rec.getFieldValue('custrecord_clgx_power_panel_pdpm')) || 0;
		var type = parseInt(nlapiLookupField('customrecord_ncfar_asset', panel, 'custrecord_clgx_panel_type')) || 0;
		
		var cfurl = '';
		if(type == 4){ // Cyberex
			cfurl = '_cyberex';
		}
		else if(type == 5 || type == 6){ // Traditional - Amps || xx.xxx
			cfurl = '_amps';
		}
		else{ // all others
		}
		
		power.customer = customer;
		power.so = so;
		power.service = service;
		power.power = powerid;
		power.facility = facility;
		power.space = space;
		power.device = device;
		power.panel = panel;
		power.factor = factor;
		power.nstart = nstart;
		power.start = start;
		power.end = end;
		power.type = type;
		power.module = module;
		power.breaker = breaker;
		power.volts = volts;
		power.kv = kv;
		power.phases = phases;
		power.amps = amps;
		power.count = count;

		var arrAmps = get_points (powerid, 'customsearch_clgx_dcim_pwr_day_amps', 0) || [];
		for ( var i = 0; arrAmps != null && i < arrAmps.length; i++ ) {
			power.breakers[i].breaker = arrAmps[i].breaker;
			power.breakers[i].phase = get_breaker_phase(arrAmps[i].breaker, type),
			power.breakers[i].amps.nsid = arrAmps[i].nsid;
			power.breakers[i].amps.odid = arrAmps[i].odid;
		}
		power.brks = arrAmps.length;
		
		var arrKws = get_points (powerid, 'customsearch_clgx_dcim_pwr_day_kws', 0) || [];
		for ( var i = 0; arrKws != null && i < arrKws.length; i++ ) {
			power.breakers[i].kws.nsid = arrKws[i].nsid;
			power.breakers[i].kws.odid = arrKws[i].odid;
		}
		var arrKwsh = get_points (powerid, 'customsearch_clgx_dcim_pwr_day_kwsh', 0) || [];
		for ( var i = 0; arrKwsh != null && i < arrKwsh.length; i++ ) {
			power.breakers[i].kwsh.nsid = arrKwsh[i].nsid;
			power.breakers[i].kwsh.odid = arrKwsh[i].odid;
		}
		var arrVolts = get_points (powerid, 'customsearch_clgx_dcim_pwr_day_volts', breaker) || [];
		for ( var i = 0; arrVolts != null && i < arrVolts.length; i++ ) {
			power.breakers[i].volts.nsid = arrVolts[i].nsid;
			power.breakers[i].volts.odid = arrVolts[i].odid;
		}
		var arrFactors = get_points (powerid, 'customsearch_clgx_dcim_pwr_day_factors', 0) || [];
		for ( var i = 0; arrFactors != null && i < arrFactors.length; i++ ) {
			power.breakers[i].factors.nsid = arrFactors[i].nsid;
			power.breakers[i].factors.odid = arrFactors[i].odid;
		}

		var url = 'https://lucee-nnj3.dev.nac.net/odins/powers/get_power_day_values' + cfurl + '.cfm';
		var requestURL = nlapiRequestURL(url, JSON.stringify(power), {'Content-type': 'application/json'}, null, 'POST');
		var valuesJSON = requestURL.body;
		var response = JSON.parse( valuesJSON );
		
		for ( var i = 0; response.breakers != null && i < response.breakers.length; i++ ) {
			if(response.breakers[i].phase == 'A'){
				response.amp_a = round(response.breakers[i].amps.avg);
			}
			if(response.breakers[i].phase == 'B'){
				response.amp_b = round(response.breakers[i].amps.avg);
			}
			if(response.breakers[i].phase == 'C'){
				response.amp_c = round(response.breakers[i].amps.avg);
			}
			if(response.breakers[i].phase == 'ABC'){
				response.amp_a = round(response.breakers[0].amps.avg);
			}
			response.kw += round(response.breakers[i].kws.avg);
		}
		
	} else {
		var response = power;
	}
	return response;
}

function get_points (id, ss, breaker){
	
	var filters = [];
	filters.push(new nlobjSearchFilter("internalid",null,"anyof",id));
	var search = nlapiSearchRecord('customrecord_clgx_power_circuit', ss, filters);
	var arr = [];
	for ( var i = 0; search != null && i < search.length; i++ ) {
		var row = search[i];
		var columns = row.getAllColumns();
		var obj = {};
		if(breaker){
			obj["breaker"] = breaker + i*2;
		} else {
			obj["breaker"] = parseInt(row.getValue(columns[0])) || 0;
		}
		obj["nsid"] = parseInt(row.getValue(columns[1]));
		obj["odid"] = parseInt(row.getValue(columns[2]));
		arr.push(obj);
	}
	return arr;
}


function get_breaker_phase (breaker, paneltype){
	
	var mod3 = breaker % 3;
	var mod6 = breaker % 6;
	var phase = '';
	if(paneltype == 1){ // Traditional
		if(mod6 == 1 || mod6 == 2){
			phase = 'A';
		}
		if(mod6 == 3 || mod6 == 4){
			phase = 'B';
		}
		if(mod6 == 5 || mod6 == 0){
			phase = 'C';
		}
	}
	else if(paneltype > 1){ // PDPM or Vertical PDU or Cyberex
		if(mod3 == 1){
			phase = 'A';
		}
		if(mod3 == 2){
			phase = 'B';
		}
		if(mod3 == 0){
			phase = 'C';
		}
	}
	else{
		phase = 'ABC';
	}
	return phase;
}

function get_empty_power (){
	return {
			"customer": 0,"so": 0,"service": 0,"device": 0,"panel": 0,"type": 0,"power": 0,
			"facility": 0,"space": 0,"module": 0,"breaker": 0,
			"volts": 0,"brks": 0,"factor": 0,"kv": 0,"phases": 0,
			"amps": 0,"amp_a": 0,"amp_b": 0,"amp_c": 0,"amp_max": 0,"amp_usg": 0,"count": 0,"kw": 0,
			"nstart": "","start": "","end": "",
			"breakers": [{
				"breaker": 0,"phase": "",
				"amps": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
				"kws": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
				"kwsh": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0,"cal":0},
				"volts": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
				"factors": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
				"kw15min": []
			},{
				"breaker": 0,"phase": "",
				"amps": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
				"kws": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
				"kwsh": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0,"cal":0},
				"volts": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
				"factors": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
				"kw15min": []
			},{
				"breaker": 0,"phase": "",
				"amps": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
				"kws": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
				"kwsh": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0,"cal":0},
				"volts": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
				"factors": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
				"kw15min": []
			}]
		};
}

function get_power_day_values (power,breaker,nstart){
	
		var last = {"search": 0,"kwadj": 0,"kwhavg": 0,"day": nstart};
		
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_kw_cum',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_kwh_avg',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_date',null,null).setSort(true));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_power",null,"anyof",power));
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_breaker",null,"equalto",breaker));
		var search = nlapiSearchRecord('customrecord_clgx_dcim_points_day', null, arrFilters, arrColumns);
		if(search){
			last.search = 1;
			last.kwadj = parseFloat(search[0].getValue('custrecord_clgx_dcim_points_day_kw_cum',null,null)) || 0;
			last.kwhavg = parseFloat(search[0].getValue('custrecord_clgx_dcim_points_day_kwh_avg',null,null)) || 0;
			last.day = search[0].getValue('custrecord_clgx_dcim_points_day_date',null,null) || '';
		}
		return last;
}

function create_power_day_values (power,last,i){

		var record = nlapiCreateRecord('customrecord_clgx_dcim_points_day');
		record.setFieldValue('custrecord_clgx_dcim_points_day_date', power.nstart);
		record.setFieldValue('custrecord_clgx_dcim_points_day_customer', power.customer);
		record.setFieldValue('custrecord_clgx_dcim_points_day_so', power.so);
		record.setFieldValue('custrecord_clgx_dcim_points_day_service', power.service);
		record.setFieldValue('custrecord_clgx_dcim_points_day_power', power.power);
		record.setFieldValue('custrecord_clgx_dcim_points_day_breaker', power.breakers[i].breaker);
		
		if(power.device){
			record.setFieldValue('custrecord_clgx_dcim_points_day_pnl', power.device);
		}
		if(power.panel && power.type){
			record.setFieldValue('custrecord_clgx_dcim_points_day_pnl_type', power.type);
		}
		record.setFieldValue('custrecord_clgx_dcim_points_day_volts', power.volts);
		record.setFieldValue('custrecord_clgx_dcim_points_day_amps', power.amps);
		record.setFieldValue('custrecord_clgx_dcim_points_day_phase', power.breakers[i].phase);
		record.setFieldValue('custrecord_clgx_dcim_points_day_phases', power.phases);
		
		if(power.breakers[i].amps.nsid){
			record.setFieldValue('custrecord_clgx_dcim_points_day_amps_pnt', power.breakers[i].amps.nsid);
		}
		record.setFieldValue('custrecord_clgx_dcim_points_day_amps_avg', power.breakers[i].amps.avg);
		record.setFieldValue('custrecord_clgx_dcim_points_day_amps_min', power.breakers[i].amps.min);
		record.setFieldValue('custrecord_clgx_dcim_points_day_amps_max', power.breakers[i].amps.max);
		record.setFieldValue('custrecord_clgx_dcim_points_day_amps_sum', power.breakers[i].amps.sum);
		record.setFieldValue('custrecord_clgx_dcim_points_day_amps_hrs', power.breakers[i].amps.hrs);
		record.setFieldValue('custrecord_clgx_dcim_points_day_amps_adj', power.breakers[i].amps.adj);
		
		if(power.breakers[i].kws.nsid){
			record.setFieldValue('custrecord_clgx_dcim_points_day_kw_pnt', power.breakers[i].kws.nsid);
		}
		record.setFieldValue('custrecord_clgx_dcim_points_day_kw_avg', power.breakers[i].kws.avg);
		record.setFieldValue('custrecord_clgx_dcim_points_day_kw_min', power.breakers[i].kws.min);
		record.setFieldValue('custrecord_clgx_dcim_points_day_kw_max', power.breakers[i].kws.max);
		record.setFieldValue('custrecord_clgx_dcim_points_day_kw_sum', power.breakers[i].kws.sum);
		record.setFieldValue('custrecord_clgx_dcim_points_day_kw_hrs', power.breakers[i].kws.hrs);
		record.setFieldValue('custrecord_clgx_dcim_points_day_kw_adj', power.breakers[i].kws.adj);
		record.setFieldValue('custrecord_clgx_dcim_points_day_kw_cum', parseFloat((power.breakers[i].kws.adj + last.kwadj).toFixed(2)));

		if(power.breakers[i].kwsh.nsid > 0){
			record.setFieldValue('custrecord_clgx_dcim_points_day_kwh_pnt', power.breakers[i].kwsh.nsid);
		}
		record.setFieldValue('custrecord_clgx_dcim_points_day_kwh_avg', power.breakers[i].kwsh.cal);
		if(last.search = 1){
			record.setFieldValue('custrecord_clgx_dcim_points_day_kwh_net', parseFloat((power.breakers[i].kwsh.cal - last.kwhavg).toFixed(2)));
		} else{
			record.setFieldValue('custrecord_clgx_dcim_points_day_kwh_net', 0);
		}
		
		if(power.breakers[i].volts.nsid > 0){
			record.setFieldValue('custrecord_clgx_dcim_points_day_volt_pnt', power.breakers[i].volts.nsid);
		}
		if(power.breakers[i].factors.nsid > 0){
			record.setFieldValue('custrecord_clgx_dcim_points_day_fact_pnt', power.breakers[i].factors.nsid);
		}
		record.setFieldValue('custrecord_clgx_dcim_points_day_kw15min', (power.breakers[i].kw15min).join());
		//var idRec = nlapiSubmitRecord(record, false,true);
		
		return record;
}

function update_power (power, prim, email){
	
	if(prim){
		var obj = power.primary;
	} else {
		var obj = power.secondary;
	}
	
	var rec = nlapiLoadRecord('customrecord_clgx_power_circuit', obj.power);
	
	rec.setFieldValue('custrecord_clgx_dcim_sum_day_amp_a_a', obj.amp_a);
	rec.setFieldValue('custrecord_clgx_dcim_sum_day_amp_a_b', obj.amp_b);
	rec.setFieldValue('custrecord_clgx_dcim_sum_day_amp_a_c', obj.amp_c);
	
	rec.setFieldValue('custrecord_clgx_dcim_sum_day_amp_ab_a', power.amp_a);
	rec.setFieldValue('custrecord_clgx_dcim_sum_day_amp_ab_b', power.amp_b);
	rec.setFieldValue('custrecord_clgx_dcim_sum_day_amp_ab_c', power.amp_c);
	
	rec.setFieldValue('custrecord_clgx_dcim_sum_day_kw_a', obj.kw);
	rec.setFieldValue('custrecord_clgx_dcim_sum_day_kw_ab', power.kw);
	
	rec.setFieldValue('custrecord_clgx_dcim_sum_day_amp_a_max', obj.amp_max);
	rec.setFieldValue('custrecord_clgx_dcim_sum_day_amp_ab_max', power.amp_max);
	
	rec.setFieldValue('custrecord_clgx_dcim_sum_day_amp_a_usg', obj.amp_usg);
	rec.setFieldValue('custrecord_clgx_dcim_sum_day_amp_ab_usg', power.amp_usg);
	
	rec.setFieldValue('custrecord_clgx_dcim_points_occurrences', power.count);
	if(email){
		rec.setFieldValue('custrecord_clgx_dcim_points_email', 'T');
		rec.setFieldValue('custrecord_clgx_dcim_points_email_date', moment().format('M/D/YYYY'));
	}
	rec.setFieldValue('custrecord_clgx_dcim_date_last_replicate', obj.nstart);
	
	//nlapiSubmitRecord(rec, false,true);
	return true;
}

function round(value) {
	  return Number(Math.round(value+'e'+2)+'e-'+2);
}

// usage email ===========================================================================================


function send_usage_email (power){
	
	var yesterday = moment().subtract('days',1).format('M/D/YYYY');
    var fromid = 432742;
	var names = {
			"primary": {
				"customer": nlapiLookupField('customer', power.primary.customer, 'entityid') || "",
				"so": nlapiLookupField('salesorder', power.primary.so, 'tranid'),
				"space": nlapiLookupField('customrecord_cologix_space', power.primary.space, 'name') || "",
				"power": nlapiLookupField('customrecord_clgx_power_circuit', power.primary.power, 'name') || "",
				"panel": nlapiLookupField('customrecord_clgx_dcim_devices', power.primary.device, 'name') || ""
			},
			"secondary": {
				"customer": nlapiLookupField('customer', power.secondary.customer, 'entityid') || "",
				"so": nlapiLookupField('salesorder', power.secondary.so, 'tranid') || "",
				"space": nlapiLookupField('customrecord_cologix_space', power.secondary.space, 'name') || "",
				"power": nlapiLookupField('customrecord_clgx_power_circuit', power.secondary.power, 'name') || "",
				"panel": nlapiLookupField('customrecord_clgx_dcim_devices', power.secondary.device, 'name') || ""
			}
	};
	
    if(power.primary.facility == 2 || power.primary.facility == 4 || power.primary.facility == 5 || power.primary.facility == 6 || power.primary.facility == 7 || power.primary.facility == 9 || power.primary.facility == 19){
    	var mtl = 1;
    	var subject = 'Cologix - Avis de consommation électrique excédée pour / Power Notice - Draw Exceeded for - ' + names.primary.customer + ' - ' + yesterday;
    }
    else{
    	var mtl = 0;
    	var subject = 'Cologix Power Notice - Draw Exceeded for - ' + names.primary.customer + ' - ' + yesterday;
    }
    
    var html_en = '';
    var html_fr = '';
    
    html_en += '<table width="560" cellpadding="5" cellspacing="0" border="1" style="border-style: solid;border-color: #CCCCCC;">';

    html_en += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
    html_en += 'Service Order';
    html_en += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
    html_en += names.primary.so;
    html_en += '</td></tr>';

    html_en += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
    html_en += 'Space';
    html_en += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
    html_en += names.primary.space;
    html_en += '</td></tr>';

    html_en += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
    html_en += 'Primary Power';
    html_en += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
    html_en += names.primary.power;
    html_en += '</td></tr>';

    html_en += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
    html_en += 'Primary Panel';
    html_en += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
    html_en += names.primary.panel;
    html_en += '</td></tr>';

    html_en += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
    html_en += 'Primary Rating';
    html_en += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
    html_en += power.primary.amps + ' Amps';
    html_en += '</td></tr>';

    html_en += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
    html_en += 'Primary Draw';
    html_en += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
    html_en += power.primary.amp_max + ' Amps';
    html_en += '</td></tr>';

    if(power.pair){
        html_en += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_en += 'Redundant Power';
        html_en += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_en += names.secondary.power;
        html_en += '</td></tr>';

        html_en += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_en += 'Redundant Panel';
        html_en += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_en += names.secondary.panel;
        html_en += '</td></tr>';

        html_en += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_en += 'Redundant Rating';
        html_en += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_en += power.secondary.amps + ' Amps';
        html_en += '</td></tr>';

        html_en += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_en += 'Redundant Draw';
        html_en += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_en += power.secondary.amp_max + ' Amps';
        html_en += '</td></tr>';

        html_en += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_en += 'Total Draw';
        html_en += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_en += power.amp_max + ' Amps';
        html_en += '</td></tr>';
    }
    html_en += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
    html_en += 'Utilization';
    html_en += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
    html_en += power.amp_usg + ' %';
    html_en += '</td></tr>';
    html_en += '</table><br/>';


    if(mtl == 1){
		html_fr += '<table width="560" cellpadding="5" cellspacing="0" border="1" style="border-style: solid;border-color: #CCCCCC;">';

        html_fr += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_fr += 'Commande de service';
        html_fr += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_fr += names.primary.so;
        html_fr += '</td></tr>';

        html_fr += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_fr += 'Espace (cabinet/cage)';
        html_fr += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_fr += names.primary.space;
        html_fr += '</td></tr>';

        html_fr += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_fr += 'Alimentation primaire';
        html_fr += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_fr += names.primary.power;
        html_fr += '</td></tr>';

        html_fr += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_fr += 'Paneaux primaire';
        html_fr += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_fr += names.primary.panel;
        html_fr += '</td></tr>';

        html_fr += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_fr += 'Capacit&eacute; nominale du circuit primaire';
        html_fr += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_fr += power.primary.amps + ' Amps';
        html_fr += '</td></tr>';

        html_fr += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_fr += 'Consommation du circuit primaire';
        html_fr += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_fr += power.primary.amp_max + ' Amps';
        html_fr += '</td></tr>';

        if(power.pair){
            html_fr += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
            html_fr += 'Alimentation redondante';
            html_fr += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
            html_fr += names.secondary.power;
            html_fr += '</td></tr>';

            html_fr += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
            html_fr += 'Paneaux redondant';
            html_fr += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
            html_fr += names.secondary.panel;
            html_fr += '</td></tr>';

            html_fr += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
            html_fr += 'Capacit&eacute; nominale du circuit redondant';
            html_fr += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
            html_fr += power.secondary.amps + ' Amps';
            html_fr += '</td></tr>';

            html_fr += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
            html_fr += 'Consommation du circuit redondant';
            html_fr += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
            html_fr += power.secondary.amp_max + ' Amps';
            html_fr += '</td></tr>';

            html_fr += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
            html_fr += 'Consommation totale';
            html_fr += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
            html_fr += power.amp_max + ' Amps';
            html_fr += '</td></tr>';
        }
        html_fr += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_fr += 'Utilisation';
        html_fr += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
        html_fr += power.amp_usg + ' %';
        html_fr += '</td></tr>';
        html_fr += '</table><br/>';
    }
    
    if(mtl == 1){
    	if(power.count == 1){
    		var file = nlapiLoadFile(2163606);
    	} else {
    		var file = nlapiLoadFile(3085802);
    	}
    } else {
    	if(power.count == 1){
    		var file = nlapiLoadFile(2153544);
    	} else {
    		var file = nlapiLoadFile(3085903);
    	}
    }
    var body = file.getValue();

    var customer = get_customer (power.primary.customer);
    var opsreps = get_ops_reps(power.primary.facility);
    
    body = body.replace(new RegExp('{powers}','g'), html_en);
    if(mtl == 1){
        body = body.replace(new RegExp('{powersFR}','g'), html_fr);
    }
    body = body.replace(new RegExp('{salesrep}','g'), customer.salesrep + ' (' + customer.salesrepemail + ')');

    if(power.primary.type != 5 && power.primary.type != 6){
        for ( var i = 0; customer.contacts != null && i < customer.contacts.length; i++ ) {
            nlapiSendEmail(fromid, customer.contacts[i], subject, body, null, null, null, null,true);
        }
        for ( var i = 0; opsreps != null && i < opsreps.length; i++ ) {
            nlapiSendEmail(fromid, opsreps[i].opid, subject, body, null, null, null, null,true);
        }
        nlapiSendEmail(fromid, customer.salesrepid, subject, body, null, null, null, null,true);
    }
    nlapiSendEmail(fromid, 71418, subject, body, null, null, null, null,true); // Dan
    nlapiSendEmail(fromid, 211645, subject, body, null, null, null, null,true); // Charles
    
    return customer;
}


function get_customer (customer){
	
    var rec = nlapiLoadRecord('customer', customer);
    var obj = new Object();
    obj["customer"] = rec.getFieldValue('entityid');
    
    obj["salesrepid"] = parseInt(rec.getFieldValue('salesrep'));
    obj["salesrep"] = rec.getFieldText('salesrep');
    obj["salesrepemail"] = nlapiLookupField('employee', parseInt(rec.getFieldValue('salesrep')), 'email');
    
    var nbrContacts = rec.getLineItemCount('contactroles');
    var arrContacts = new Array();
    var arrEmails = new Array();
    var arrFiltersContacts=new Array();
    var arrColumnsContacts=new Array();
    arrFiltersContacts.push(new nlobjSearchFilter("internalid", "company", "anyof",customer));
    var searchTechContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_tecrole", arrFiltersContacts, arrColumnsContacts);
    var searchPrimaryContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_prrole", arrFiltersContacts, arrColumnsContacts);
    var searchSecondaryContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_secrole", arrFiltersContacts, arrColumnsContacts);
    var searchDecisionMaker = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_dmrole", arrFiltersContacts, arrColumnsContacts);

    if((searchTechContact!=null)||(searchPrimaryContact!=null)||(searchSecondaryContact!=null)||(searchDecisionMaker!=null)) {

        for ( var m = 0;searchTechContact != null && m < searchTechContact.length; m++ ) {
            var searchTCnt = searchTechContact[m];
            var columns = searchTCnt.getAllColumns();
            var contactid = parseInt(searchTCnt.getValue(columns[0]));
            var email = searchTCnt.getValue(columns[3]);
            if((_.indexOf(arrEmails, email) == -1) && (email != '' )&& (email != null)){
                arrContacts.push(contactid);
                arrEmails.push(email);
            }
        }
        for ( var m = 0;searchPrimaryContact != null && m < searchPrimaryContact.length; m++ ) {
            var searchPCnt = searchPrimaryContact[m];
            var columns = searchPCnt.getAllColumns();
            var contactid = parseInt(searchPCnt.getValue(columns[0]));
            var email = searchPCnt.getValue(columns[3]);
            if((_.indexOf(arrEmails, email) == -1) && (email != '' )&& (email != null)){
                arrContacts.push(contactid);
                arrEmails.push(email);
            }
        }
        for ( var m = 0;searchSecondaryContact != null && m < searchSecondaryContact.length; m++ ) {
            var searchSCnt = searchSecondaryContact[m];
            var columns = searchSCnt.getAllColumns();
            var contactid = parseInt(searchSCnt.getValue(columns[0]));
            var email = searchSCnt.getValue(columns[3]);
            if((_.indexOf(arrEmails, email) == -1) && (email != '' )&& (email != null)){
                arrContacts.push(contactid);
                arrEmails.push(email);
            }
        }
        for ( var m = 0;searchDecisionMaker!= null && m <searchDecisionMaker.length; m++ ) {
            var searchDCnt = searchDecisionMaker[m];
            var columns = searchDCnt.getAllColumns();
            var contactid = parseInt(searchDCnt.getValue(columns[0]));
            var email = searchDCnt.getValue(columns[3]);
            if((_.indexOf(arrEmails, email) == -1) && (email != '') && (email != null)){
                arrContacts.push(contactid);
                arrEmails.push(email);
            }
        }

    } else {
	    for ( var j = 0; j < nbrContacts; j++ ) {
	        var contactid = parseInt(rec.getLineItemValue('contactroles', 'contact', j + 1));
	        var email = rec.getLineItemValue('contactroles', 'email', j + 1);
	        if((_.indexOf(arrEmails, email) == -1) && (email != '') && (email != null)){
	            arrContacts.push(contactid);
	            arrEmails.push(email);
	        }
	    }
    }
    obj["contacts"] = arrContacts;
    
   return obj;
}


function get_ops_reps (facility){

    var arr = new Array();
    if(facility == 2 || facility == 4 || facility == 5 || facility == 6 || facility == 7 || facility == 9 || facility == 19){
        var obj = new Object();
        obj["opid"] = 168983;
        obj["op"] = 'Donato Di Rienzo';
        arr.push(obj);

        var obj = new Object();
        obj["opid"] = 3118;
        obj["op"] = 'Sorin Arion';
        arr.push(obj);
    } else {
        var obj = new Object();
        obj["opid"] = 212518;
        obj["op"] = 'Amy J Connell';
        arr.push(obj);

        var obj = new Object();
        obj["opid"] = 9735;
        obj["op"] = 'Pauli Guild';
        arr.push(obj);

        var obj = new Object();
        obj["opid"] = 8795;
        obj["op"] = 'Mario Novello';
        arr.push(obj);
        }
    return arr;
}
