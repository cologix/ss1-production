nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_Day_Cyberex.js
//	Script Name:	CLGX_SS_DCIM_PWR_Day_Cyberex
//	Script Id:		customscript_clgx_ss_dcim_pwr_day_cyberx
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		06/13/2016
//-------------------------------------------------------------------------------------------------

function scheduled_dcim_pwr_day_cyberex(){
    try{
        var context = nlapiGetContext();
        var environment = context.getEnvironment();
        var startScript = moment();
        
        if(environment == 'PRODUCTION'){
    		
	        var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('parent','custrecord_cologix_power_service',null));
			var arrFilters = new Array();
			var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pwrs_day_cyberex', arrFilters, arrColumns);
	
			for ( var k = 0; searchPowers != null && k < searchPowers.length; k++ ) {
	 			
	 			var startExec = moment();
	         	var execMinutes = (startExec.diff(startScript)/60000).toFixed(1);
	            if (context.getRemainingUsage() < 100 || execMinutes > 50 || k > 990 ){
	             	var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
	             	nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
	             	break; 
	            }
	            
	            var powerid = parseInt(searchPowers[k].getValue('internalid',null,null));
	    		var customerid = parseInt(searchPowers[k].getValue('parent','custrecord_cologix_power_service',null));
				var soid = parseInt(searchPowers[k].getValue('custrecord_power_circuit_service_order',null,null));
				var serviceid = parseInt(searchPowers[k].getValue('custrecord_cologix_power_service',null,null));
				
				var voltage = searchPowers[k].getText('custrecord_cologix_power_volts',null,null);
				var soldamps = searchPowers[k].getText('custrecord_cologix_power_amps',null,null);
				var lastDate = searchPowers[k].getValue('custrecord_clgx_dcim_date_last_replicate',null,null);
				var breaker = parseInt(searchPowers[k].getText('custrecord_cologix_power_circuitbreaker',null,null));
				
				var deviceid = searchPowers[k].getValue('custrecord_clgx_dcim_device',null,null);
				var paneltype = parseInt(searchPowers[k].getValue('custrecord_clgx_panel_type','custrecord_clgx_power_panel_pdpm',null));
				
				var volts = parseInt(voltage.substring(0, 3));
				var circuit = soldamps.substring(0, soldamps.length - 1);
				var phases = 1;
		 		if(voltage == '208V Single Phase'){
		 			phases = 2;
		 		}
		 		if(voltage == '208V Three Phase' || voltage == '240V Three Phase' || voltage == '600V Three Phase'){
		 			phases = 3;
		 		}

		 		var start = moment(lastDate).add('days', 1).format('YYYY-MM-DD');
	 			var end = moment(lastDate).add('days', 2).format('YYYY-MM-DD');
	 			var startN = moment(lastDate).add('days', 1).format('M/D/YYYY');
	 			
	 			var objPower = get_obj();
	 			objPower.panel = paneltype;
				objPower.power = powerid;
	 			objPower.volts = volts;
	 			objPower.factor = 0;
	 			objPower.phases = phases;
	 			objPower.start = start;
	 			objPower.end = end;
	 			objPower.kv = 0;
	 			
	 			var arrAmps = get_points (powerid, 'customsearch_clgx_dcim_pwr_day_amps', 0);
	 			for ( var i = 0; arrAmps != null && i < arrAmps.length; i++ ) {
	 				objPower.breakers[i].breaker = arrAmps[i].breaker;
	 				objPower.breakers[i].phase = get_breaker_phase(arrAmps[i].breaker,paneltype);
					objPower.breakers[i].amps.nsid = arrAmps[i].nsid;
	 				objPower.breakers[i].amps.odid = arrAmps[i].odid;
	 			}
	 			objPower.brks = arrAmps.length;

 				var arrVolts = get_points (powerid, 'customsearch_clgx_dcim_pwr_day_volts', breaker);
 				for ( var i = 0; arrVolts != null && i < arrVolts.length; i++ ) {
 					objPower.breakers[i].volts.nsid = arrVolts[i].nsid;
 					objPower.breakers[i].volts.odid = arrVolts[i].odid;
 				}

 				var arrFactors = get_points (powerid, 'customsearch_clgx_dcim_pwr_day_factors', 0);
 				for ( var i = 0; arrFactors != null && i < arrFactors.length; i++ ) {
 					objPower.breakers[i].factors.nsid = arrFactors[i].nsid;
 					objPower.breakers[i].factors.odid = arrFactors[i].odid;
 				}
	 			
 				var url = 'https://lucee-nnj3.dev.nac.net/odins/powers/get_power_day_values_cyberex.cfm';
	 			var requestURL = nlapiRequestURL(url, JSON.stringify(objPower), {'Content-type': 'application/json'}, null, 'POST');
	 			var valuesJSON = requestURL.body;
	 			var powerBack = JSON.parse( valuesJSON );
	 			
 				for ( var i = 0; i < powerBack.brks; i++ ) {
	 				
	 				// get last values for a power/ breaker of date, kW sum and kWh to calculate the cumulative kW and the kWh net
	 				var lastkwadj = 0;
	 				var lastkwhavg = 0;
	 				var lastday = startN;
	 				var arrColumns = new Array();
					arrColumns.push(new nlobjSearchColumn('internalid',null,null));
					arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_kw_cum',null,null));
					arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_kwh_avg',null,null));
					arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_date',null,null).setSort(true));
					var arrFilters = new Array();
					arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_power",null,"anyof",powerid));
					arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_breaker",null,"equalto",powerBack.breakers[i].breaker));
					var searchLastDay = nlapiSearchRecord('customrecord_clgx_dcim_points_day', null, arrFilters, arrColumns);
					if(searchLastDay != null){
						lastkwadj = parseFloat(searchLastDay[0].getValue('custrecord_clgx_dcim_points_day_kw_cum',null,null));
						lastkwhavg = parseFloat(searchLastDay[0].getValue('custrecord_clgx_dcim_points_day_kwh_avg',null,null));
						lastday = searchLastDay[0].getValue('custrecord_clgx_dcim_points_day_date',null,null);
					}
	 				
	 				// if record does not exist already and if returning positive values from OpenData, create record
	 				if((lastday != startN || searchLastDay == null) && (powerBack.breakers[i].amps.avg > 0 || powerBack.breakers[i].kws.avg > 0 || powerBack.breakers[i].kwsh.cal > 0)){
	 					
	 					var record = nlapiCreateRecord('customrecord_clgx_dcim_points_day');
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_date', startN);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_customer', customerid);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_so', soid);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_service', serviceid);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_power', powerid);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_pnl', deviceid);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_volts', volts);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_phases', phases);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_breaker', powerBack.breakers[i].breaker);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_amps', circuit);
	 					
	 					//record.setFieldValue('custrecord_clgx_dcim_points_day_pnl_type', paneltype);
	 					//record.setFieldValue('custrecord_clgx_dcim_points_day_phase', get_breaker_phase(powerBack.breakers[i].breaker,paneltype));
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_pnl_type', powerBack.panel);
 						record.setFieldValue('custrecord_clgx_dcim_points_day_phase', powerBack.breakers[i].phase);
 						
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_amps_pnt', powerBack.breakers[i].amps.nsid);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_amps_avg', powerBack.breakers[i].amps.avg);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_amps_min', powerBack.breakers[i].amps.min);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_amps_max', powerBack.breakers[i].amps.max);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_amps_sum', powerBack.breakers[i].amps.sum);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_amps_hrs', powerBack.breakers[i].amps.hrs);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_amps_adj', powerBack.breakers[i].amps.adj);
	 					
	 					if(powerBack.breakers[i].volts.nsid > 0){
	 						record.setFieldValue('custrecord_clgx_dcim_points_day_volt_pnt', powerBack.breakers[i].volts.nsid);
	 					}
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_kw_avg', powerBack.breakers[i].kws.avg);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_kw_min', powerBack.breakers[i].kws.min);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_kw_max', powerBack.breakers[i].kws.max);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_kw_sum', powerBack.breakers[i].kws.sum);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_kw_hrs', powerBack.breakers[i].kws.hrs);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_kw_adj', powerBack.breakers[i].kws.adj);
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_kw_cum', parseFloat((powerBack.breakers[i].kws.adj + lastkwadj).toFixed(2)));
	
	 					if(powerBack.breakers[i].factors.nsid > 0){
	 						record.setFieldValue('custrecord_clgx_dcim_points_day_fact_pnt', powerBack.breakers[i].factors.nsid);
	 					}
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_kwh_avg', powerBack.breakers[i].kwsh.cal);
	 					
	 					if(searchLastDay != null){
	 						record.setFieldValue('custrecord_clgx_dcim_points_day_kwh_net', parseFloat((powerBack.breakers[i].kwsh.cal - lastkwhavg).toFixed(2)));
	 					}
	 					else{
	 						record.setFieldValue('custrecord_clgx_dcim_points_day_kwh_net', 0);
	 					}
	 					record.setFieldValue('custrecord_clgx_dcim_points_day_kw15min', (powerBack.breakers[i].kw15min).join());
	 					
	 					var idRec = nlapiSubmitRecord(record, false,true);
	 				}
	 			}
	 			nlapiSubmitField('customrecord_clgx_power_circuit', powerid, 'custrecord_clgx_dcim_date_last_replicate', startN);
	 			
	            var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
	            var index = k + 1;
	            nlapiLogExecution('DEBUG','Results ', '| powerid = ' + powerid + ' | ID = ' + index + ' of ' + searchPowers.length + ' | Usage = '+ usageConsumtion + '  |');
	        }
		    if(searchPowers != null){
				var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
	        }
	        else{
	        	clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_admin", "OD 1.0 - Finishing Powers Updates - Cyberex", "No more powers to update");
	        	
	        	/*nlapiSendEmail(432742,71418,'OD 1.0 - Finishing Powers Updates - Cyberex','No more powers to update',null,null,null,null);
	        	nlapiSendEmail(432742,1349020,'OD 1.0 - Finishing Powers Updates - Cyberex','No more powers to update',null,null,null,null);*/
	        	//var status = nlapiScheduleScript('customscript_clgx_ss_dcim_pwr_day_amps', 'customdeploy_clgx_ss_dcim_pwr_day_amps' ,null);
	        	
	        }
	    }
    }
    catch (error){
    	
    	var str = String(error);
        if (str.match('SSS_UNKNOWN_HOST') || str.match('SSS_INVALID_HOST_CERT') || str.match('SSS_CONNECTION_CLOSED') || str.match('SSS_REQUEST_TIME_EXCEEDED') || str.match('SSS_CONNECTION_TIME_OUT') || str.match('UNEXPECTED_ERROR') || str.match('Unexpected token')) {
        	var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
        	nlapiLogExecution('DEBUG','End', 'Finished powers processing because of communication error and reschedule it.');
        }
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function get_breaker_phase(breaker, paneltype){
	var mod3 = breaker % 3;
	var mod6 = breaker % 6;
	var phase = '';
	if(paneltype == 1){ // Traditional
		if(mod6 == 1 || mod6 == 2){
			phase = 'A';
		}
		if(mod6 == 3 || mod6 == 4){
			phase = 'B';
		}
		if(mod6 == 5 || mod6 == 0){
			phase = 'C';
		}
	}
	else if(paneltype > 1){ // PDPM or Vertical PDU or Cyberex
		if(mod3 == 1){
			phase = 'A';
		}
		if(mod3 == 2){
			phase = 'B';
		}
		if(mod3 == 0){
			phase = 'C';
		}
	}
	else{
		phase = '';
	}
	return phase;
}

function get_points (id, ss, breaker){
	var filters = [];
	filters.push(new nlobjSearchFilter("internalid",null,"anyof",id));
	var search = nlapiSearchRecord('customrecord_clgx_power_circuit', ss, filters);
	var arr = [];
	for ( var i = 0; search != null && i < search.length; i++ ) {
		var row = search[i];
		var columns = row.getAllColumns();
		var obj = {};
		if(breaker){
			obj["breaker"] = breaker + i*2;
		} else {
			obj["breaker"] = parseInt(row.getValue(columns[0])) || 0;
		}
		obj["nsid"] = parseInt(row.getValue(columns[1]));
		obj["odid"] = parseInt(row.getValue(columns[2]));
		arr.push(obj);
	}
	return arr;
}

function get_obj (){
	var obj = {"panel": 0,"power": 0,"volts": 0,"factor": 0,"phases": 0,"kv": 0,"start": "","end": "",
		"breakers": [{
			"breaker": 0,"phase": "",
			"amps": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
			"kws": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
			"kwsh": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0,"cal":0},
			"volts": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
			"factors": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
			"kw15min": []
		},{
			"breaker": 0,"phase": "",
			"amps": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
			"kws": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
			"kwsh": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0,"cal":0},
			"volts": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
			"factors": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
			"kw15min": []
		},{
			"breaker": 0,"phase": "",
			"amps": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
			"kws": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
			"kwsh": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0,"cal":0},
			"volts": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
			"factors": {"nsid": 0,"odid": 0,"avg":0,"min":0,"max":0,"sum":0,"hrs":0,"adj":0},
			"kw15min": []
		}]
	}
	return obj;
}
