nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_KW.js
//	Script Name:	CLGX_SS_DCIM_PWR_KW
//	Script Id:		customscript_clgx_ss_dcim_pwr_kw
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		9/30/2014
//-------------------------------------------------------------------------------------------------

function scheduled_dcim_pwr_kw(){

    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Date:		9/30/2014
// Details:		Calculate KW peak of the day for one each customer
//-----------------------------------------------------------------------------------------------------------------

        var context = nlapiGetContext();
        var reschedule = 0;

        var date = new Date();
        var startScript = moment();
        var emailAdminSubject = 'OD Powers Demand - Peaks (kW) ' + startScript.format('M/D/YYYY');
        var emailAdminBody = '';
        emailAdminBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');
        emailAdminBody += '<h2>Customers processed</h2>';
        emailAdminBody += '<table border="1" cellpadding="5">';
        emailAdminBody += '<tr><td>Customer</td><td>Facility</td><td>Date</td><td>Peak</td><td>kW</td></tr>';


        // Customers that have powers with collected data
        var arrCustomersIDs = new Array();
        var searchResults = nlapiLoadSearch('customrecord_clgx_dcim_points_day', 'customsearch_clgx_dcim_so_collected');
        var resultSet = searchResults.runSearch();
        resultSet.forEachResult(function(searchResult) {
            var columns = searchResult.getAllColumns();
            var customerid = parseInt(searchResult.getValue(columns[0]));
            arrCustomersIDs.push(customerid);
            return true;
        });

        // Service Orders with kilowats reservation or deployment
        var arrColumns = new Array();
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",arrCustomersIDs)); // filter only customers with collected data
        var searchServiceOrders = nlapiSearchRecord('transaction', 'customsearch_clgx_dcim_peaks_sos', arrFilters, arrColumns);
        var arrServiceOrders = new Array();
        for ( var i = 0; searchServiceOrders != null && i < searchServiceOrders.length; i++ ) {
            var searchServiceOrder = searchServiceOrders[i];
            var columns = searchServiceOrder.getAllColumns();
            var objServiceOrder = new Object();
            objServiceOrder["customerid"] = parseInt(searchServiceOrder.getValue(columns[0]));
            objServiceOrder["customer"] = searchServiceOrder.getText(columns[0]);
            var facilityid = parseInt(clgx_return_facilityid (searchServiceOrder.getValue(columns[1])));
            objServiceOrder["facilityid"] = facilityid;
            objServiceOrder["facility"] = nlapiLookupField('customrecord_cologix_facility', facilityid, 'name');
            objServiceOrder["soid"] = parseInt(searchServiceOrder.getValue(columns[2]));
            objServiceOrder["so"] = searchServiceOrder.getValue(columns[3]);
            objServiceOrder["kwcontract"] = parseInt(searchServiceOrder.getValue(columns[4]));
            arrServiceOrders.push(objServiceOrder);
        }

        var arrCustomersIDs = _.uniq(_.pluck(arrServiceOrders, 'customerid'));
        var arrCustomers = new Array();

        for ( var i = 0; arrCustomersIDs != null && i < arrCustomersIDs.length; i++ ) {

            var objFirstSO = _.find(arrServiceOrders, function(arr){ return (arr.customerid  == arrCustomersIDs[i]); });

            var objCustomer = new Object();
            objCustomer["customerid"] = objFirstSO.customerid;
            objCustomer["customer"] = objFirstSO.customer;
            objCustomer["facilityid"] = objFirstSO.facilityid;
            objCustomer["facility"] = objFirstSO.facility;
            var arrCustomerSOs = _.filter(arrServiceOrders, function(arr){
                return (arr.customerid == arrCustomersIDs[i]);
            });

            var totalkwcontract = 0;
            var arrSOs = new Array();
            for ( var j = 0; arrCustomerSOs != null && j < arrCustomerSOs.length; j++ ) {
                var objSO = new Object();
                objSO["soid"] = arrCustomerSOs[j].soid;
                objSO["so"] = arrCustomerSOs[j].so;
                objSO["kwcontract"] = arrCustomerSOs[j].kwcontract;
                totalkwcontract += arrCustomerSOs[j].kwcontract;
                arrSOs.push(objSO);
            }
            objCustomer["kwcontract"] = totalkwcontract;
            objCustomer["sos"] = arrSOs;


            var recCustomer = nlapiLoadRecord('customer',objFirstSO.customerid);
            var nbrContacts = recCustomer.getLineItemCount('contactroles');
            var arrContacts = new Array();
            var arrEmails = new Array();
            var arrFiltersContacts=new Array();
            var arrColumnsContacts=new Array();
            arrFiltersContacts.push(new nlobjSearchFilter("internalid", "company", "anyof",arrCustomersIDs[i]));
            var searchTechContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_tecrole", arrFiltersContacts, arrColumnsContacts);
            var searchPrimaryContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_prrole", arrFiltersContacts, arrColumnsContacts);
            var searchSecondaryContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_secrole", arrFiltersContacts, arrColumnsContacts);
            var searchDecisionMaker = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_dmrole", arrFiltersContacts, arrColumnsContacts);
            //  var searchOpsOutage = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_ops", arrFiltersContacts, arrColumnsContacts);
            //search for Primary/Secondary/Tech Contacts
            if((searchTechContact!=null)||(searchPrimaryContact!=null)||(searchSecondaryContact!=null)||(searchDecisionMaker!=null))
            {

                for ( var m = 0;searchTechContact != null && m < searchTechContact.length; m++ ) {
                    var searchTCnt = searchTechContact[m];
                    var columns = searchTCnt.getAllColumns();
                    var contactid = parseInt(searchTCnt.getValue(columns[0]));
                    var contact = searchTCnt.getText(columns[0]);
                    var email = searchTCnt.getValue(columns[3]);
                    if((_.indexOf(arrEmails, email) == -1) && (email != '' )&& (email != null)){
                        var objContact = new Object();
                        objContact["contactid"] = contactid;
                        objContact["contact"] = contact;
                        objContact["email"] = email;
                        arrContacts.push(objContact);
                        arrEmails.push(email);
                    }
                }
                for ( var m = 0;searchPrimaryContact != null && m < searchPrimaryContact.length; m++ ) {
                    var searchPCnt = searchPrimaryContact[m];
                    var columns = searchPCnt.getAllColumns();
                    var contactid = parseInt(searchPCnt.getValue(columns[0]));
                    var contact = searchPCnt.getText(columns[0]);
                    var email = searchPCnt.getValue(columns[3]);
                    if((_.indexOf(arrEmails, email) == -1) && (email != '' )&& (email != null)){
                        var objContact = new Object();
                        objContact["contactid"] = contactid;
                        objContact["contact"] = contact;
                        objContact["email"] = email;
                        arrContacts.push(objContact);
                        arrEmails.push(email);
                    }
                }
                for ( var m = 0;searchSecondaryContact != null && m < searchSecondaryContact.length; m++ ) {
                    var searchSCnt = searchSecondaryContact[m];
                    var columns = searchSCnt.getAllColumns();
                    var contactid = parseInt(searchSCnt.getValue(columns[0]));
                    var contact = searchSCnt.getText(columns[0]);
                    var email = searchSCnt.getValue(columns[3]);
                    if((_.indexOf(arrEmails, email) == -1) && (email != '' )&& (email != null)){
                        var objContact = new Object();
                        objContact["contactid"] = contactid;
                        objContact["contact"] = contact;
                        objContact["email"] = email;
                        arrContacts.push(objContact);
                        arrEmails.push(email);
                    }
                }
                for ( var m = 0;searchDecisionMaker!= null && m <searchDecisionMaker.length; m++ ) {
                    var searchDCnt = searchDecisionMaker[m];
                    var columns = searchDCnt.getAllColumns();
                    var contactid = parseInt(searchDCnt.getValue(columns[0]));
                    var contact = searchDCnt.getText(columns[0]);
                    var email = searchDCnt.getValue(columns[3]);
                    if((_.indexOf(arrEmails, email) == -1) && (email != '') && (email != null)){
                        var objContact = new Object();
                        objContact["contactid"] = contactid;
                        objContact["contact"] = contact;
                        objContact["email"] = email;
                        arrContacts.push(objContact);
                        arrEmails.push(email);
                    }
                }

            }
            else
            {
                for ( var j = 0; j < nbrContacts; j++ ) {
                    var contactid = parseInt(recCustomer.getLineItemValue('contactroles', 'contact', j + 1));
                    var contact = recCustomer.getLineItemValue('contactroles', 'contactname', j + 1);
                    var email = recCustomer.getLineItemValue('contactroles', 'email', j + 1);
                    if((_.indexOf(arrEmails, email) == -1) && (email != '') && (email != null)){
                        var objContact = new Object();
                        objContact["contactid"] = contactid;
                        objContact["contact"] = contact;
                        objContact["email"] = email;
                        arrContacts.push(objContact);
                        arrEmails.push(email);
                    }
                }
            }

            objCustomer["contacts"] = arrContacts;

            arrCustomers.push(objCustomer);
        }

      /*  for ( var i = 0; arrCustomers != null && i < arrCustomers.length; i++ ) {

            var startExec = moment(); // time when each day loop is starting
            var execMinutes = (startExec.diff(startScript)/60000).toFixed(1); // execution time in minutes

            var day = '';
            var alltimespeak = 0;
            var ignore = '';

            // verify what was the last update date for this customer / facility
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('internalid',null,null).setSort(true));
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_update',null,null));
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_kw',null,null));
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_customer",null,"anyof",arrCustomers[i].customerid));
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_facility",null,"anyof",arrCustomers[i].facilityid));
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_update",null,"before","yesterday"));
            var searchLastDay = nlapiSearchRecord('customrecord_clgx_dcim_customer_peak', null, arrFilters, arrColumns);

            var x = 0;
            if (searchLastDay != null){ // there is a record older than yesterday
                peakid = parseInt(searchLastDay[0].getValue('internalid',null,null));
                update = searchLastDay[0].getValue('custrecord_clgx_dcim_peak_update',null,null);
                alltimespeak = parseFloat(searchLastDay[0].getValue('custrecord_clgx_dcim_peak_kw',null,null));
                // add one day to the last update date
                day = moment(update).add('days', 1).format('MM/DD/YYYY');
                reschedule = 1; // in case there are other days to process
            }
            else{

                // verify if there is any record at all for this customer / facility
                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('internalid',null,null));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_customer",null,"anyof",arrCustomers[i].customerid));
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_facility",null,"anyof",arrCustomers[i].facilityid));
                var searchRecords = nlapiSearchRecord('customrecord_clgx_dcim_customer_peak', null, arrFilters, arrColumns);

                if(searchRecords == null){

                    // there is no customer / facility record yet, so look for first day of available collected data for powers
                    var arrColumns = new Array();
                    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_date',null,null).setSort(false));
                    var arrFilters = new Array();
                    arrFilters.push(new nlobjSearchFilter("parent",'custrecord_clgx_dcim_points_day_service',"anyof",arrCustomers[i].customerid));
                    arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_facility","custrecord_clgx_dcim_points_day_power","anyof",arrCustomers[i].facilityid));
                    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_kw_avg",null,"greaterthan",0));
                    var searchFirstDay = nlapiSearchRecord('customrecord_clgx_dcim_points_day', null, arrFilters, arrColumns);

                    if(searchFirstDay != null){ // there is collected data
                        day = searchFirstDay[0].getValue('custrecord_clgx_dcim_points_day_date',null,null);
                        reschedule = 1;
                    }
                }
            }


            if(day != ''){

                // pull out data for all powers/breakers for this customer/facility/day
                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_power',null,null).setSort(false));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_breaker',null,null).setSort(false));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_kw15min',null,null));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_points_day_date',null,'on',day));
                arrFilters.push(new nlobjSearchFilter("parent",'custrecord_clgx_dcim_points_day_service',"anyof",arrCustomers[i].customerid));
                arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_facility","custrecord_clgx_dcim_points_day_power","anyof",arrCustomers[i].facilityid));
                var searchPeaks = nlapiSearchRecord('customrecord_clgx_dcim_points_day', null, arrFilters, arrColumns);

                // construct CSV file header columns
                var csv = 'Power,' + 'Breaker,';
                for ( var j = 0; j < 24; j++ ) {
                    for ( var k = 0; k < 4; k++ ) {
                        csv += ('0' + j).slice(-2) + ':' + ('0' + (k*15)).slice(-2) + ',';
                    }
                }
                csv += '\n';


                var arrPeaksIDs = new Array();
                var arr15MinPeaks = new Array();

                // construct totals array
                var arrTotals = new Array();
                for ( var j = 0; j < 96; j++ ) {
                    arrTotals[j] = 0;
                }
                // loop all powers/breakers and calculate totals by 15 minutes and continue constructing CSV file
                for ( var j = 0; searchPeaks != null && j < searchPeaks.length; j++ ) {
                    var power = searchPeaks[j].getText('custrecord_clgx_dcim_points_day_power',null,null);
                    var breaker = searchPeaks[j].getValue('custrecord_clgx_dcim_points_day_breaker',null,null);
                    var lst15min = searchPeaks[j].getValue('custrecord_clgx_dcim_points_day_kw15min',null,null);
                    csv += power + ',' + breaker + ',' + lst15min + '\n';
                    var arr15min = lst15min.split(',');
                    for ( var k = 0; arr15min != null && k < arr15min.length; k++ ) {
                        arrTotals[k] += parseFloat(arr15min[k]);
                    }
                }
                // add last totals and peak lines to CSV file
                csv += 'Totals,,' + arrTotals.join() + '\n';
                var peak = parseFloat(_.max(arrTotals).toFixed(2));
                csv += 'Peak ' + day + ','+ peak;


                // create CSV file on file cabinet for the customer/facility/day
                var month = moment(day).format('M');
                var year = moment(day).format('YYYY');
                var filename = arrCustomers[i].customer + '_' + arrCustomers[i].facility + '_kW_Peak_' + day + '.csv';
                var csvFile = nlapiCreateFile(filename, 'PLAINTEXT', csv);
                csvFile.setFolder(get_file_folder(year,month));
                var fileID = nlapiSubmitFile(csvFile);


                if (searchLastDay !== null || searchRecords != null){ // this customer/facility record exist
                    if(peak > alltimespeak){ // if peak value > than the existing peak on the record - update value, date, last update date and file
                        var fields = ['custrecord_clgx_dcim_peak_date','custrecord_clgx_dcim_peak_kw_max','custrecord_clgx_dcim_peak_kw','custrecord_clgx_dcim_peak_update','custrecord_clgx_dcim_peak_file'];
                        var values = [day,arrCustomers[i].kwcontract,peak,day,fileID];
                        nlapiSubmitField('customrecord_clgx_dcim_customer_peak', peakid, fields, values);

                        // inform Kristen that a new peak have been reached
                        var emailSubject = 'There is a new peak (kW) of ' + peak + 'kW for ' + arrCustomers[i].customer + ' that occured on ' + day;
                        var emailBody = 'There is a new peak (kW) of ' + peak + 'kW for ' + arrCustomers[i].customer + ' that occured on ' + day;
                       // nlapiSendEmail(71418,71418,emailSubject,emailBody,null,null,null,null); // Dan
                        //nlapiSendEmail(71418,12073,emailSubject,emailBody,null,null,null,null); // Kristen
                    }
                    else{ // if peak value <= than the existing peak on the record - update only last update date
                        var fields = ['custrecord_clgx_dcim_peak_update','custrecord_clgx_dcim_peak_kw_max'];
                        var values = [day,arrCustomers[i].kwcontract];
                        nlapiSubmitField('customrecord_clgx_dcim_customer_peak', peakid, fields, values);
                    }
                }
                else{ // this customer/facility record does not exist or has to be ignored - create it
                    var record = nlapiCreateRecord('customrecord_clgx_dcim_customer_peak');
                    record.setFieldValue('custrecord_clgx_dcim_peak_customer', arrCustomers[i].customerid);
                    record.setFieldValue('custrecord_clgx_dcim_peak_facility', arrCustomers[i].facilityid);
                    record.setFieldValue('custrecord_clgx_dcim_peak_date', day);
                    record.setFieldValue('custrecord_clgx_dcim_peak_kw_max', arrCustomers[i].kwcontract);
                    record.setFieldValue('custrecord_clgx_dcim_peak_kw', peak);
                    record.setFieldValue('custrecord_clgx_dcim_peak_update', day);
                    record.setFieldValue('custrecord_clgx_dcim_peak_file', fileID);
                    var idRec = nlapiSubmitRecord(record, false,true);
                }


                // verify if a record for this customer/facility/day exist
                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('internalid',null,null).setSort(false));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_peak_day_date',null,'on',day));
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_day_customer",null,"anyof",arrCustomers[i].customerid));
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_day_facilty",null,"anyof",arrCustomers[i].facilityid));
                var searchDayPeak = nlapiSearchRecord('customrecord_clgx_dcim_customer_peak_day', null, arrFilters, arrColumns);

                // if it does not exist - create it
                if(searchDayPeak == null){

                    var so = '';
                    var kwcontract = '';
                    var objSO = _.find(arrSOs, function(arr){ return (arr.customerid == arrCustomers[i].customerid && arr.facilityid == arrCustomers[i].facilityid); });
                    if(objSO != null){
                        so = objSO.so;
                        kwcontract = parseFloat(objSO.kwcontract);
                        var style = 'background-color:#ffffff;';
                        if(peak > kwcontract){
                            style = 'background-color:#ffcccc;';
                        }
                    }

                    var record = nlapiCreateRecord('customrecord_clgx_dcim_customer_peak_day');
                    record.setFieldValue('custrecord_clgx_dcim_peak_day_customer', arrCustomers[i].customerid);
                    record.setFieldValue('custrecord_clgx_dcim_peak_day_facilty', arrCustomers[i].facilityid);
                    record.setFieldValue('custrecord_clgx_dcim_peak_day_date', day);
                    record.setFieldValue('custrecord_clgx_dcim_peak_day_kw', peak);
                    record.setFieldValue('custrecord_clgx_dcim_peak_day_file', fileID);
                    var idRec = nlapiSubmitRecord(record, false,true);

                    var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
                    emailAdminBody += '<tr style="' + style + '"><td>' + arrCustomers[i].customer + '<td>' + arrCustomers[i].facility + '</td><td>' + day + '</td><td>' + peak + '</td><td>' + arrCustomers[i].kwcontract + '</td></tr>';
                    nlapiLogExecution('DEBUG', 'Value', '| Customer = ' + arrCustomers[i].customer + ' | (' + i + ' of ' + arrCustomers.length+ ') | Facility = ' + arrCustomers[i].facility + ' | Date = ' + day + ' | Usage = '+ usageConsumtion + ' | Minute = ' + execMinutes + ' |');

                }
            }
        }

*/

        var strUsage = JSON.stringify(arrCustomers);
        var jsonUsage = nlapiCreateFile('powers_demand1.json', 'PLAINTEXT', strUsage);
        jsonUsage.setFolder(1294374);
        nlapiSubmitFile(jsonUsage);


    /*    emailAdminBody += '</table>';
        var endScript = moment();
        emailAdminBody += '<br><br>';
        emailAdminBody += 'End : ' + endScript.format('M/D/YYYY h:mm:ss a') + '<br>';
        emailAdminBody += 'Total execution minutes : ' + (endScript.diff(startScript)/60000).toFixed(1) + '<br>';
        var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
        emailAdminBody += 'Total usage : ' + usageConsumtion;
        emailAdminBody += '<br><br>';


        if(reschedule == 1){
          //  nlapiSendEmail(71418,71418,emailAdminSubject,emailAdminBody,null,null,null,null);
            //nlapiSendEmail(71418,12073,emailSubject,emailBody,null,null,null,null); // Kristen
            var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
            nlapiLogExecution('DEBUG','End', 'Finished powers for one day and re-schedule it.');
        }
        else{
            //nlapiSendEmail(71418,71418,emailAdminSubject,emailAdminBody,null,null,null,null);
         //   nlapiSendEmail(71418,71418,'Finishing Processing Powers Peaks','No more powers to process',null,null,null,null);
            var status1 = nlapiScheduleScript('customscript_clgx_ss_dcim_pwr_kw_cust', 'customdeploy_clgx_ss_dcim_pwr_kw_cust' ,null);
            var status2 = nlapiScheduleScript('customscript_clgx_ss_dcim_pwr_amps', 'customdeploy_clgx_ss_dcim_pwr_amps' ,null);
            nlapiLogExecution('DEBUG','End', 'No more powers to process.');
        }
*/

//-----------------------------------------------------------------------------------------------------------------
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|');
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function get_file_folder(rptyear, rptmonth){

    if(rptyear == 2015){
        switch(parseInt(rptmonth)) {
            case 1:
                fileFolder = 1594284;
                break;
            case 2:
                fileFolder = 1594285;
                break;
            case 3:
                fileFolder = 1594286;
                break;
            case 4:
                fileFolder = 1594287;
                break;
            case 5:
                fileFolder = 1594288;
                break;
            case 6:
                fileFolder = 1594289;
                break;
            case 7:
                fileFolder = 1594290;
                break;
            case 8:
                fileFolder = 1594291;
                break;
            case 9:
                fileFolder = 1594292;
                break;
            case 10:
                fileFolder = 1594293;
                break;
            case 11:
                fileFolder = 1594294;
                break;
            case 12:
                fileFolder = 1594295;
                break;
            default:
                fileFolder = 1267033;
        }
    }
    else{ // 2014
        switch(parseInt(rptmonth)) {
            case 1:
                fileFolder = 1266920;
                break;
            case 2:
                fileFolder = 1266922;
                break;
            case 3:
                fileFolder = 1267023;
                break;
            case 4:
                fileFolder = 1267024;
                break;
            case 5:
                fileFolder = 1267025;
                break;
            case 6:
                fileFolder = 1267026;
                break;
            case 7:
                fileFolder = 1267027;
                break;
            case 8:
                fileFolder = 1267028;
                break;
            case 9:
                fileFolder = 1267029;
                break;
            case 10:
                fileFolder = 1267030;
                break;
            case 11:
                fileFolder = 1267031;
                break;
            case 12:
                fileFolder = 1267032;
                break;
            default:
                fileFolder = 1267033;
        }
    }

    return fileFolder;
}
