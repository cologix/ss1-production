nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_Day_Capacity_1.js
//	Script Name:	CLGX_SS_DCIM_PWR_Day_Capacity_1
//	Script Id:		customscript_clgx_ss_dcim_pwr_day_capacity_1
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		06/12/2018
//-------------------------------------------------------------------------------------------------

function scheduled_dcim_pwr_day(){
    try{
    	var context = nlapiGetContext();
        var environment = context.getEnvironment();
        
        if(environment == 'PRODUCTION'){
        	
        	
    		// CLGX_SS_DCIM_PWR_Day_Capacity_1
    		update_devices_capacity_sums_amps ('panel');
    		var usage = 10000 - parseInt(context.getRemainingUsage());
            nlapiLogExecution('DEBUG','Results ', ' | update_devices_capacity_sums_amps = ' + usage  + '  |');
            
    		//update_devices_capacity_sums_amps ('ups');
    		//update_devices_capacity_sums_amps ('generator');
    		
            // CLGX_SS_DCIM_PWR_Day_Capacity_2
    		update_devices_capacity_sums_kw ('panel');
    		var usage = 10000 - parseInt(context.getRemainingUsage());
            nlapiLogExecution('DEBUG','Results ', ' | update_devices_capacity_sums_kw_panel = ' + usage  + '  |');
            
            // CLGX_SS_DCIM_PWR_Day_Capacity_3
    		update_devices_capacity_sums_kw ('ups');
    		var usage = 10000 - parseInt(context.getRemainingUsage());
            nlapiLogExecution('DEBUG','Results ', ' | update_devices_capacity_sums_kw_ups = ' + usage  + '  |');
            
    		//update_devices_capacity_sums_kw ('generator');
    		
            // CLGX_SS_DCIM_PWR_Day_Capacity_4
    		update_devices_capacity_vals();
    		var usage = 10000 - parseInt(context.getRemainingUsage());
            nlapiLogExecution('DEBUG','Results ', ' | update_devices_capacity_vals = ' + usage  + '  |');
    		
    		clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_admin", "OD 2.3 - Finishing Powers Capacities", "");
    		
    		var status = nlapiScheduleScript('customscript_clgx_ss_dcim_pwr_kw', 'customdeploy_clgx_ss_dcim_pwr_kw' ,null);
        	
        }
    }
    catch (error){
    	
    	var str = String(error);
        if (str.match('SSS_UNKNOWN_HOST') || str.match('SSS_INVALID_HOST_CERT') || str.match('SSS_CONNECTION_CLOSED') || str.match('SSS_REQUEST_TIME_EXCEEDED') || str.match('SSS_CONNECTION_TIME_OUT') || str.match('UNEXPECTED_ERROR') || str.match('Unexpected token')) {
        	var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
        	nlapiLogExecution('DEBUG','End', 'Finished powers processing because of error and reschedule it.');
        }
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
        	if (str.match('TypeError: Cannot read property "phases" from undefined')) {
        		var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
        		//nlapiLogExecution('ERROR','Unexpected Error', error.toString());
                //throw nlapiCreateError('99999', error.toString());
        	} else {
                nlapiLogExecution('ERROR','Unexpected Error', error.toString());
                throw nlapiCreateError('99999', error.toString());
        	}
        }
    }
}
