nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_AMPS_1.js
//	Script Name:	CLGX_SS_DCIM_PWR_AMPS_1
//	Script Id:		customscript_clgx_ss_dcim_pwr_amps_1
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/30/2014
//-------------------------------------------------------------------------------------------------

function scheduled_dcim_pwr_amps_1(){
	
    try{

    	// the search returns > 4000 so split it by Amps
    	var arrAmps = new Array();
    	var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_amps',null,'GROUP').setSort(false));
		var arrFilters = new Array();
		var searchAmps = nlapiSearchRecord('customrecord_clgx_dcim_points_day', null, arrFilters, arrColumns);
		for ( var i = 0; searchAmps != null && i < searchAmps.length; i++ ) {
			arrAmps.push(parseInt(searchAmps[i].getValue('custrecord_clgx_dcim_points_day_amps',null,'GROUP')));
		}
    	
		var previousid = 0;
		var index = 0;
    	var arrBreakers = new Array();
    	var arrPowersIDs = new Array();
    	
    	for ( var i = 0; arrAmps != null && i < arrAmps.length; i++ ) {

			var arrColumns = new Array();
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_amps",null,"equalto",arrAmps[i]));
			var searchPoints = nlapiLoadSearch('customrecord_clgx_dcim_points_day', 'customsearch_clgx_dcim_pwr_util_outage_1');
			searchPoints.addColumns(arrColumns);
			searchPoints.addFilters(arrFilters);
			var resultSet = searchPoints.runSearch();
			resultSet.forEachResult(function(searchResult) {
				var columns = searchResult.getAllColumns();
				var objBreaker = new Object();
				objBreaker["customerid"] = parseInt(searchResult.getValue(columns[0]));
				objBreaker["customer"] = searchResult.getText(columns[0]);
				objBreaker["emailsent"] = searchResult.getValue(columns[8]);
				
				var oldcount = parseInt(searchResult.getValue(columns[11]));
				if(oldcount > 0){
					objBreaker["oldcount"] = oldcount;
					objBreaker["newcount"] = oldcount;
				}
				else{
					objBreaker["oldcount"] = 0;
					objBreaker["newcount"] = 0;
				}
				
				objBreaker["facilityid"] = parseInt(searchResult.getValue(columns[7]));
				objBreaker["facility"] = searchResult.getText(columns[7]);
				objBreaker["serviceid"] = parseInt(searchResult.getValue(columns[1]));
				objBreaker["service"] = searchResult.getText(columns[1]);
				
				
				var powerid = parseInt(searchResult.getValue(columns[2]));
				objBreaker["powerid"] = powerid;
				objBreaker["power"] = searchResult.getText(columns[2]);
				
				var pairid = parseInt(searchResult.getValue(columns[3]));
				if(pairid > 0){
					objBreaker["pairid"] = parseInt(searchResult.getValue(columns[3]));
					objBreaker["pair"] = searchResult.getText(columns[3]);
				}
				else{
					objBreaker["pairid"] = 0;
					objBreaker["pair"] = '';
				}
				
				arrPowersIDs.push(powerid);
				arrPowersIDs.push(pairid);
				
				var panelid = parseInt(searchResult.getValue(columns[12]));
				objBreaker["panelid"] = panelid;
				objBreaker["panel"] = searchResult.getText(columns[12]);

				objBreaker["breaker"] = parseInt(searchResult.getValue(columns[4]));
				objBreaker["circuit"] = parseInt(searchResult.getValue(columns[5]));
				objBreaker["space"] = searchResult.getText(columns[10]);
				objBreaker["volts"] = searchResult.getText(columns[9]);
				var amps = parseFloat(searchResult.getValue(columns[6]));
				objBreaker["amps"] = amps;
				
				if(powerid != previousid){
					index = 1;
				}
				else{
					index = index + 1;
				}
				
				objBreaker["index"] = index;
				previousid = powerid;
				
				arrBreakers.push(objBreaker);
				return true;
			});
    	}

    	var arrPowersUniqIDs = _.compact(_.uniq(arrPowersIDs));
    	
    	var arrPowers = new Array();
    	for ( var p = 0; arrPowersUniqIDs != null && p < arrPowersUniqIDs.length; p++ ) {
    		
    		var objPwrData = _.find(arrBreakers, function(arr){ return (arr.powerid == arrPowersUniqIDs[p]);});
    		if(objPwrData != null){
	    		
	    		var objPower = new Object();
	    		objPower["customerid"] = objPwrData.customerid;
	    		objPower["customer"] = objPwrData.customer;
	    		objPower["emailsent"] = objPwrData.emailsent;
	    		objPower["oldcount"] = objPwrData.oldcount;
	    		objPower["newcount"] = objPwrData.newcount;
	    		objPower["facilityid"] = objPwrData.facilityid;
	    		objPower["facility"] = objPwrData.facility;
	    		objPower["serviceid"] = objPwrData.serviceid;
	    		objPower["service"] = objPwrData.service;
	    		objPower["powerid"] = objPwrData.powerid;
	    		objPower["power"] = objPwrData.power;
	    		objPower["pairid"] = objPwrData.pairid;
	    		objPower["pair"] = objPwrData.pair;
	    		objPower["panelid"] = objPwrData.panelid;
	    		objPower["panel"] = objPwrData.panel;
	    		objPower["space"] = objPwrData.space;
	    		objPower["volts"] = objPwrData.volts;
	    		objPower["circuit"] = objPwrData.circuit;
	    		
	    		var arrPowerBreakers = _.filter(arrBreakers, function(arr){
	    			return (arr.powerid == arrPowersUniqIDs[p]);
		    	});
	            
				var max = 0;
				var maxab = 0;
				var circuitpair = 0;
				
				var arrB = new Array();
	    		for ( var i = 0; arrPowerBreakers != null && i < arrPowerBreakers.length; i++ ) {
	    			
	    			var objB = new Object();
	    			objB["index"] = arrPowerBreakers[i].index;
	    			objB["breaker"] = arrPowerBreakers[i].breaker;
	    			var amps = arrPowerBreakers[i].amps;
	    			objB["amps"] = amps;
	    			
	    			var objPairBreaker = _.find(arrBreakers, function(arr){ return (arr.powerid == arrPowerBreakers[i].pairid && arr.index == arrPowerBreakers[i].index ); });
	    			if(objPairBreaker != null){
	    				circuitpair = objPairBreaker.circuit;
	    				objB["ampspair"] = objPairBreaker.amps;
	    				var absum = amps + objPairBreaker.amps;
	    			}
	    			else{
	    				objB["ampspair"] = 0;
	    				var absum = amps;
	    			}
	    			objB["absum"] = absum;
	    			
	    			if(absum > maxab){
	    				maxab = absum;
	    			}
	    			
	    			if(amps > max){
	    				max = amps;
	    			}
	    			
	    			var usage = parseFloat(((absum * 100) / arrBreakers[i].circuit).toFixed(2));
	    			objB["usage"] = usage;
	
	    			arrB.push(objB);
	    		}
	    		
	    		if(objPwrData.pairid > 0){ // if pair exist, look for max on pair
	    			
	        		var arrPairBreakers = _.filter(arrBreakers, function(arr){
	        			return (arr.powerid == objPwrData.pairid);
	    	    	});
	        		var maxpair = 0;
	        		for ( var j = 0; arrPairBreakers != null && j < arrPairBreakers.length; j++ ) {
	        			
	        			if(arrPairBreakers[j].amps > maxpair){
	        				maxpair = arrPairBreakers[j].amps;
	        			}
	        		}
	    		}
	    		objPower["circuitpair"] = circuitpair;
	    		objPower["amps"] = max;
	    		objPower["ampspair"] = maxpair;
	    	
	    		var usage = parseFloat(((maxab * 100) / objPwrData.circuit).toFixed(2));
	    		
	    		objPower["absum"] = maxab;
	        	objPower["usage"] = usage;
	
				if(usage >= 80){
					objPower.newcount = objPower.oldcount + 1;
				}
	    		
	    		//objPower["breakers"] = arrB;
	    		arrPowers.push(objPower);
    		}
    	}
    	
		
		var strPowers = JSON.stringify(arrPowers);
		var jsonUsageAll = nlapiCreateFile('powers_usage_all1.json', 'PLAINTEXT', strPowers);
		jsonUsageAll.setFolder(1294374);
		nlapiSubmitFile(jsonUsageAll);
		
		var arrPowersUniqIDs = _.compact(_.uniq(arrPowersIDs));
		var jsonPowersIDs = nlapiCreateFile('powers_ids1.json', 'PLAINTEXT', JSON.stringify(arrPowersUniqIDs));
		jsonPowersIDs.setFolder(1294374);
		nlapiSubmitFile(jsonPowersIDs);
		
		var jsonPowersIDs = nlapiCreateFile('powers_ids_queue1.json', 'PLAINTEXT', JSON.stringify(arrPowersUniqIDs));
		jsonPowersIDs.setFolder(1294374);
		nlapiSubmitFile(jsonPowersIDs);
		
		var arrOutage = _.filter(arrPowers, function(arr){
	        //return (arr.usage > 80 || arr.oldcount > 0);
	        return (arr.usage > 80);
		});
		
		// keep only the A part of A+B Powers
		var arrUniqIDs = new Array();
		var arrOutageA = new Array();
		for ( var i = 0; arrOutage != null && i < arrOutage.length; i++ ) {
			if(_.indexOf(arrUniqIDs, arrOutage[i].powerid) == -1){
				arrOutageA.push(arrOutage[i]);
				arrUniqIDs.push(arrOutage[i].powerid);
				if(arrOutage[i].pairid != '- None -'){
					arrUniqIDs.push(arrOutage[i].pairid);
				}
			}
		}
		
		arrOutageA = _.sortBy(arrOutageA, function(obj){ return obj.customer;});
		
		for ( var i = 0; arrOutageA != null && i < arrOutageA.length; i++ ) {
			
			var oldcount = arrOutageA[i].oldcount;
			var newcount = arrOutageA[i].newcount;
			
			if(newcount > oldcount){
				if(newcount == 1){
					arrOutageA[i]["email"] = 1;
				}
				else if(newcount == 5){
					arrOutageA[i]["email"] = 2;
				}
				else{
					arrOutageA[i]["email"] = 0;
				}
				nlapiSubmitField('customrecord_clgx_power_circuit', arrOutageA[i].powerid, 'custrecord_clgx_dcim_points_occurrences', arrOutageA[i].newcount);
				if(arrOutageA[i].pairid > 0){
					nlapiSubmitField('customrecord_clgx_power_circuit', arrOutageA[i].pairid, 'custrecord_clgx_dcim_points_occurrences', arrOutageA[i].newcount);
				}
			}
			else{
				if(oldcount < 5){
					nlapiSubmitField('customrecord_clgx_power_circuit', arrOutageA[i].powerid, 'custrecord_clgx_dcim_points_occurrences', 0);
					if(arrOutageA[i].pairid > 0){
						nlapiSubmitField('customrecord_clgx_power_circuit', arrOutageA[i].pairid, 'custrecord_clgx_dcim_points_occurrences', 0);
					}
				}
				arrOutageA[i]["email"] = 0;
			}
		}
		
		var strUsage = JSON.stringify(arrOutageA);
		var jsonUsage = nlapiCreateFile('powers_usage1.json', 'PLAINTEXT', strUsage);
		jsonUsage.setFolder(1294374);
		nlapiSubmitFile(jsonUsage);

        var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
        nlapiLogExecution('DEBUG', 'Usage', '| Usage = ' + usageConsumtion + ' |');

		var status = nlapiScheduleScript('customscript_clgx_ss_dcim_pwr_amps_2', 'customdeploy_clgx_ss_dcim_pwr_amps_2' ,null);
	}
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function get_max_logic (panel){
	
	var tor12 = 0;
	var tor1 = panel.indexOf("TOR1");
	var tor1205 = panel.indexOf("205");
	var tor2 = panel.indexOf("TOR2");
	
	if((tor2 > -1) || ((tor1 > -1) && (tor1205 > -1))){
		tor12 = 1;
		}
	return tor12;
}