nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_Power_Minutes.js
//	Script Name:	CLGX_SL_DCIM_Power_Minutes
//	Script Id:		customscript_clgx_sl_dcim_power_minutes
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		09/02/2014
//	URL:			app/site/hosting/scriptlet.nl?script=367&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_power_minutes (request, response){
	try {
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
    	
		var powerid = parseInt(request.getParameter('powerid'));
		var interval = parseInt(request.getParameter('interval'));
		var intervals = 60 / interval; // how many intervals in an hour
		var point = parseInt(request.getParameter('point'));
		var date = request.getParameter('date');
		
		var recPower = nlapiLoadRecord ('customrecord_clgx_power_circuit',powerid);
		var id = recPower.getFieldValue('id');
		var name = recPower.getFieldValue('name');
		var circuit = recPower.getFieldText('custrecord_cologix_power_amps');
		var voltage = recPower.getFieldText('custrecord_cologix_power_volts');
		var volts = voltage.substring(0, 3);
		
		var arrExternalIDs = new Array();
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",powerid));
		arrFilters.push(new nlobjSearchFilter("internalid",'CUSTRECORD_CLGX_DCIM_POINTS_CURRENT',"anyof",point));
		var searchPointsAmps = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_panels_pwr_amps', arrFilters, arrColumns);
		var arrPointsAmps = new Array();
		for ( var i = 0; searchPointsAmps != null && i < searchPointsAmps.length; i++ ) {
			var searchPoint = searchPointsAmps[i];
			var columns = searchPoint.getAllColumns();
			var objPoint = new Object();
			objPoint["breaker"] = searchPoint.getValue(columns[0]);
			objPoint["breakerid"] = parseInt(searchPoint.getValue(columns[1]));
			objPoint["index"] = parseInt(i+1);
			objPoint["ampsintid"] = parseInt(searchPoint.getValue(columns[2]));
			objPoint["ampsextid"] = parseInt(searchPoint.getValue(columns[3]));
			//arrExternalIDs.push(parseInt(searchPoint.getValue(columns[3])));
			objPoint["ampsname"] = parseInt(searchPoint.getValue(columns[4]));
			//objPoint["avg"] = parseFloat(searchPoint.getValue(columns[5]));
			arrPointsAmps.push(objPoint);
		}
		
		var arrExternalIDs = new Array();
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",powerid));
		//arrFilters.push(new nlobjSearchFilter("internalid",'CUSTRECORD_CLGX_DCIM_POINTS_CURRENT',"anyof",point));
		var searchPointsKW = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_panels_pwr_kw', arrFilters, arrColumns);
		var arrPointsKW = new Array();
		for ( var i = 0; searchPointsKW != null && i < searchPointsKW.length; i++ ) {
			var searchPoint = searchPointsKW[i];
			var columns = searchPoint.getAllColumns();
			var objPoint = new Object();
			objPoint["breaker"] = searchPoint.getValue(columns[0]);
			objPoint["breakerid"] = parseInt(searchPoint.getValue(columns[1]));
			objPoint["index"] = parseInt(i+1);
			objPoint["kwintid"] = parseInt(searchPoint.getValue(columns[2]));
			objPoint["kwextid"] = parseInt(searchPoint.getValue(columns[3]));
			//arrExternalIDs.push(parseInt(searchPoint.getValue(columns[3])));
			objPoint["kwname"] = parseInt(searchPoint.getValue(columns[4]));
			//objPoint["avg"] = parseFloat(searchPoint.getValue(columns[5]));
			arrPointsKW.push(objPoint);
		}
		for ( var i = 0; arrPointsAmps != null && i < arrPointsAmps.length; i++ ) {
			var objPointKW = _.find(arrPointsKW, function(arr){ return (arr.breakerid == arrPointsAmps[i].breakerid); });
			if(objPointKW != null){
				arrPointsAmps[i]["kwintid"] = objPointKW.kwintid;
				arrPointsAmps[i]["kwextid"] = objPointKW.kwextid;
				arrPointsAmps[i]["kwname"] = objPointKW.kwname;
				//arrExternalIDs.push(objPoint.kwextid);
			}
			else{
				arrPointsAmps[i]["kwintid"] = 0;
				arrPointsAmps[i]["kwextid"] = 0;
				arrPointsAmps[i]["kwname"] = '';
			}
		}
		for ( var i = 0; arrPointsAmps != null && i < arrPointsAmps.length; i++ ) {
			arrExternalIDs.push(arrPointsAmps[i].ampsextid);
			arrExternalIDs.push(arrPointsAmps[i].kwextid);
		}
		
		
		var start = moment(date).format('YYYY-MM-DD');
		var end = moment(date).add('days', 1).format('YYYY-MM-DD');
		
		var url = '';
		url += 'https://lucee-nnj3.dev.nac.net/odins/powers/get_power_history_minutes.cfm';
		//url += 'https://command1.cologix.com:10313/netsuite/power/get_power_history_minutes.cfm';
		url += '?points=' + arrExternalIDs.join();
		url += '&start=' + start;
		url += '&end=' + end;
		
		
		var requestURL = nlapiRequestURL(url);
		var valuesJSON = requestURL.body;
		var arrMinutes= JSON.parse( valuesJSON );
		
		var arrMin = new Array();
		for ( var i = 0; arrMinutes != null && i < arrMinutes.length; i++ ) {
			var objMin = new Object();
			objMin["pointid"] = arrMinutes[i].POINTID;
			objMin["hour"] = moment(arrMinutes[i].STIME).format('YYYY-MM-DD HH');
			objMin["minute"] = moment(arrMinutes[i].STIME).format('YYYY-MM-DD HH:mm');
			objMin["val"] = parseFloat(Math.max(0,arrMinutes[i].VAL));
			arrMin.push(objMin);
		}
		
		var lastIntervalAmps = 0;
		var lastIntervalKW = 0;
		var objTree = new Object();
		objTree["text"] = '.';
		objTree["power"] = name;
		var arrHours = new Array();
		for ( var i = 0; i < 24; i++ ) {
			var objHour = new Object();
			objHour["node"] = date + ' ' + ('0' + i).slice(-2);
			objHour["expanded"] = true;
			objHour["iconCls"] = 'hour';
			objHour["leaf"] = false;
			
			var countHourAmps = 0;
			var countHourKW = 0;
			var arrInterval = new Array();
			for ( var j = 0; j < intervals; j++ ) {
				var objInterval = new Object();
				objInterval["node"] = date + ' ' + ('0' + i).slice(-2) + ':' + ('0' + (j*interval)).slice(-2);
				
				var arrTotalIntervalAmps = new Array();
				var totalIntervalAmps = 0;
				var maxIntervalAmps = 0;
				var countIntervalAmps = 0;
				
				var arrTotalIntervalKW = new Array();
				var totalIntervalKW = 0;
				var maxIntervalKW = 0;
				var countIntervalKW = 0;
				
				var arrMinutes = new Array();
				for ( var k = 0; k < interval; k++ ) {
					var objMinute = new Object();
					
					var min = date + ' ' + ('0' + i).slice(-2) + ':' + ('0' + (j*interval + k)).slice(-2);
					objMinute["node"] = min;
					
					var amps = 0;
					var kw = 0;
					
					var objMAmps = _.find(arrMin, function(arr){ return (arr.minute == min && arr.pointid == arrPointsAmps[0].ampsextid); });
					if(objMAmps != null){
						amps = (objMAmps.val).toFixed(2);
						objMinute["amps"] = amps;
						arrTotalIntervalAmps.push(parseFloat(amps));
						totalIntervalAmps += parseFloat(amps);
						if(amps > maxIntervalAmps){
							maxIntervalAmps = amps;
						}
						if(amps > 0){
							lastIntervalAmps = amps;
						}
						objMinute["vamps"] = true;
						objMinute["countamps"] = 1;
						countIntervalAmps += 1;
						countHourAmps += 1;
					}
					else{
						objMinute["amps"] = parseFloat(lastIntervalAmps);
						if(lastIntervalAmps > maxIntervalAmps){
							maxIntervalAmps = lastIntervalAmps;
						}
						objMinute["vamps"] = false;
					}
					
					var objMKW = _.find(arrMin, function(arr){ return (arr.minute == min && arr.pointid == arrPointsAmps[0].kwextid); });
					if(objMKW != null){
						kw = (objMKW.val).toFixed(2);
						objMinute["kw"] = kw;
						arrTotalIntervalKW.push(parseFloat(kw));
						totalIntervalKW += parseFloat(kw);
						if(kw > maxIntervalKW){
							maxIntervalKW = kw;
						}
						if(kw > 0){
							lastIntervalKW = kw;
						}
						objMinute["vkw"] = true;
						objMinute["countkw"] = 1;
						countIntervalKW += 1;
						countHourKW += 1;
					}
					else{
						objMinute["kw"] = parseFloat(lastIntervalKW);
						if(lastIntervalKW > maxIntervalKW){
							maxIntervalKW = lastIntervalKW;
						}
						objMinute["vkw"] = false;
					}
				
					
					if(objMAmps != null){
						objMinute["iconCls"] = 'min';
					}
					else{
						objMinute["iconCls"] = 'vmin';
					}
					if(objMKW != null){
						objMinute["iconCls"] = 'min';
					}
					else{
						objMinute["iconCls"] = 'vmin';
					}
					
					objMinute["leaf"] = true;
					arrMinutes.push(objMinute);

				}
				objInterval["amps"] = parseFloat(maxIntervalAmps);
				objInterval["kw"] = parseFloat(maxIntervalKW);
				objInterval["countamps"] = parseInt(countIntervalAmps);
				objInterval["countkw"] = parseInt(countIntervalKW);
				objInterval["iconCls"] = 'intmin';

				var ckw = 0;
				if(voltage == '120V Single Phase' || voltage == '240V Single Phase' || voltage == '240V Three Phase'){
					ckw = parseFloat(parseFloat(objInterval.amps) * volts / 1000);
				}
				if(voltage == '208V Single Phase'){
					ckw = parseFloat(parseFloat(objInterval.amps) * volts / 2 / 1000);
				}
				if(voltage == '208V Three Phase'){
					ckw = parseFloat(parseFloat(objInterval.amps) * Math.sqrt(3) * volts / 3 / 1000);
				}
				objInterval["ckw"] = ckw.toFixed(2);
				
				objInterval["expanded"] = false;
				objInterval["leaf"] = false;
				
				objInterval["children"] = arrMinutes;
				arrInterval.push(objInterval);
			}
			objHour["countamps"] = parseInt(countHourAmps);
			objHour["countkw"] = parseInt(countHourKW);
			objHour["children"] = arrInterval;
			arrHours.push(objHour);
		}
		objTree["children"] = arrHours;
		
		
		var objFile = nlapiLoadFile(1537167);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{minutesJSON}','g'), JSON.stringify(objTree));
		response.write( html );

        var usageConsumtion = parseInt(nlapiGetContext().getRemainingUsage());
        nlapiLogExecution('DEBUG', 'RemainingUsage', '| RemainingUsage = ' + usageConsumtion + ' |');
        
		nlapiLogExecution('DEBUG','Finished Execution', '|-------------------------- Finished Scheduled Script --------------------------|');
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
