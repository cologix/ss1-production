nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_RPT.js
//	Script Name:	CLGX_SS_DCIM_PWR_RPT
//	Script Id:		customscript_clgx_ss_dcim_pwr_rpt
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		10/20/2014
//-------------------------------------------------------------------------------------------------

function scheduled_dcim_pwr_rpt(){
	try{
		var context = nlapiGetContext();
		
		var exclude = [];
		//var exclude = [27,44,45,46,47];
		
		var rptid = 0; // produce only one report - if 0 all are produced
		var thismonth = 0; // 1 - include this month
		
		var arrMonths = new Array();
		//var arrDays = new Array();
		
		if(thismonth == 0){
			var rptDate = moment().startOf('month').subtract('months',1).format('M/D/YYYY');
		}
		else{
			var rptDate = moment().startOf('month').format('M/D/YYYY');
		}
    	
    	// see what reports exist for last month
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_pwr_month_rpt',null,null));
		var filters = new Array();
		filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_pwr_month_date",null,"on",rptDate));
		filters.push(new nlobjSearchFilter('isinactive',null,'is', 'F'));
		var searchRptMonths = nlapiSearchRecord('customrecord_clgx_dcim_rpt_pwr_months', null, filters, columns);
		var arrRptMonthsIDs = new Array();
		for ( var i = 0; searchRptMonths != null && i < searchRptMonths.length; i++ ) {
			arrRptMonthsIDs.push(searchRptMonths[i].getValue('custrecord_clgx_dcim_rpt_pwr_month_rpt',null,null));
		}
		_.uniq(arrRptMonthsIDs);
    	
		// search reports excluding existing ones
		var columns = new Array();
		columns.push(new nlobjSearchColumn('internalid',null,null).setSort(false));
		var filters = new Array();
		if(rptid > 0){
			filters.push(new nlobjSearchFilter("internalid",null,"anyof",rptid));
		}
		if(arrRptMonthsIDs.length > 0 && rptid == 0){
			filters.push(new nlobjSearchFilter("internalid",null,"noneof",arrRptMonthsIDs));
			if(exclude.length > 0){
				filters.push(new nlobjSearchFilter("internalid",null,"noneof",exclude));
			}
		}
		//filters.push(new nlobjSearchFilter("internalid",null,"noneof",8));
		filters.push(new nlobjSearchFilter('isinactive',null,'is', 'F'));
		var searchReports = nlapiSearchRecord('customrecord_clgx_dcim_rpt_cust_pwr', null, filters, columns);
		
		if(searchReports != null){
			
			var reportid = parseInt(searchReports[0].getValue('internalid',null,null));
			var recReport = nlapiLoadRecord ('customrecord_clgx_dcim_rpt_cust_pwr',reportid);
			var rptname = recReport.getFieldValue('name').replace(/\&/g, '&amp;');
			var nbrmonths = parseInt(recReport.getFieldText('custrecord_clgx_dcim_rpt_cust_rpt_months'));
			var byemail = recReport.getFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_email');
			//var rptypeid = parseInt(recReport.getFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_type'));
			//var rptype = recReport.getFieldText('custrecord_clgx_dcim_rpt_cust_rpt_type');
			var rptkwh = recReport.getFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_kwh');
			var cumulative = recReport.getFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_cumul');
			var rptkw = recReport.getFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_kw');
			var rptamps = recReport.getFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_amps');
			var rptenviro = recReport.getFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_enviro');
			
			var arrEmployeesIDs = recReport.getFieldValues('custrecord_clgx_dcim_rpt_cust_rpt_copyto');
	    	//var arrEmployeesNames = recReport.getFieldTexts('custrecord_clgx_dcim_rpt_cust_rpt_copyto');

			nlapiLogExecution('DEBUG', 'rptname', rptname);
			nlapiLogExecution('DEBUG','debug', '| start = ' + reportid + ' | ');
			
			
			arrMonths = get_arr_months (nbrmonths);
			//arrDays = get_arr_days ();
			
			if(thismonth == 0){
		        var start = moment().subtract('months', nbrmonths).startOf('month').format('M/D/YYYY');
		        var end = moment().subtract('months', 1).endOf('month').format('M/D/YYYY');
		        var startOD = moment().subtract('months', nbrmonths).startOf('month').format('YYYY-M-D');
		        var endOD = moment().subtract('months', 1).endOf('month').format('YYYY-M-D');
			}
			else{
		        var start = moment().subtract('months', (nbrmonths-1)).startOf('month').format('M/D/YYYY');
		        var end = moment().endOf('month').format('M/D/YYYY');
		        var startOD = moment().subtract('months', (nbrmonths-1)).startOf('month').format('YYYY-M-D');
		        var endOD = moment().endOf('month').format('YYYY-M-D');
			}

	        var arrReportColumns = arrMonths;
	        var colspan = arrReportColumns.length * 2 + 11;
			var colspan2 = arrReportColumns.length + 2;
	        //var arrReportColumns = arrDays;
	        //var colspan = arrReportColumns.length + 8;
			//var colspan2 = arrReportColumns.length + 2;

			var html = '';
			
			// report header ----------------------------------------------------------------------------------------------------------------
			html += '<table>';
			html += '<tr>';
			html += '<td class="title" align="left"><img width="151px" height="48px" src="/core/media/media.nl?id=2&amp;c=1337135&amp;h=120db2ec4539ba4117d6" border="0"/></td>';
			html += '<td class="title" align="right"><h1>Power Reports</h1></td>';
			html += '</tr>';
			html += '<tr><td colspan="2" class="space"> </td></tr>'; 
			html += '</table>';
			// report header ----------------------------------------------------------------------------------------------------------------
			
			
			var arrContactsIDs = new Array();
			
	    	// look for the facilities (grouped) of the customers on the report
			var columns = new Array();
			columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_facility',null,'GROUP').setSort(false));
			var filters = new Array();
			filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_cust_report",null,"anyof",reportid));
			var searchFacilities = nlapiSearchRecord('customrecord_clgx_dcim_rpt_cust_customer', null, filters, columns);
			var arrFacilities = new Array();

			for ( var i = 0; searchFacilities != null && i < searchFacilities.length; i++ ) {
	
	        	var searchFacility = searchFacilities[i];
	        	var objFacility = new Object();
	        	var facilityid = parseInt(searchFacility.getValue('custrecord_clgx_dcim_rpt_cust_facility',null,'GROUP'));
	        	var facility = searchFacility.getText('custrecord_clgx_dcim_rpt_cust_facility',null,'GROUP');
	        	facility = facility.replace(/\&/g,'&amp;');
	        	
	        	// look for customers on that facility
	    		var columns = new Array();
	    		columns.push(new nlobjSearchColumn('internalid',null,null));
	    		columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_customer',null,null).setSort(false));
	    		//columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_contacts',null,null));
	    		var filters = new Array();
	    		filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_cust_report",null,"anyof",reportid));
	    		filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_cust_facility",null,"anyof",facilityid));
	    		var searchCustomers = nlapiSearchRecord('customrecord_clgx_dcim_rpt_cust_customer', null, filters, columns);
	    		var arrCustomers = new Array();
	    		
	    		for ( var j = 0; searchCustomers != null && j < searchCustomers.length; j++ ) {
	        		
		        	var searchCustomer = searchCustomers[j];
		        	var objCustomer = new Object();
		        	var repcustid = parseInt(searchCustomer.getValue('internalid',null,null));
		        	var customerid = parseInt(searchCustomer.getValue('custrecord_clgx_dcim_rpt_cust_customer',null,null));
		        	var customer = searchCustomer.getText('custrecord_clgx_dcim_rpt_cust_customer',null,null).replace(/\&/g, '&amp;');
		        	
					var recReportCustomer = nlapiLoadRecord ('customrecord_clgx_dcim_rpt_cust_customer',repcustid);
					var arrContacts = recReportCustomer.getFieldValues('custrecord_clgx_dcim_rpt_cust_contacts');

		        	
		        	// customer/facility header ----------------------------------------------------------------------------------------------------------------
		        	html += '<table>';
					html += '<tr><td class="title"><h1>' + facility + ' - ' + customer + '</h1></td></tr>';
					html += '<tr><td class="title"> </td></tr>';
					html += '<tr><td class="title">From ' + start + ' To ' + end + '</td></tr>'; 
					html += '<tr><td class="title"> </td></tr>'; 
					html += '</table>';
					// customer/facility header ----------------------------------------------------------------------------------------------------------------

					
					//if(searchData != null){
// kWh section ----------------------------------------------------------------------------------------------------------------
						if(rptkwh == 'T'){
							html += get_kwh_html(customerid, customer, facilityid, facility, colspan, start, end, arrReportColumns, nbrmonths, cumulative);
						}
						//nlapiLogExecution('DEBUG','debug', '| kWh section | ');
						
// kW section ----------------------------------------------------------------------------------------------------------------
						if(rptkw == 'T'){
							//html += '<div page-break-before="always"></div>';
							html += get_kw_html(customerid, customer, facilityid, facility, start, end);
						}
						//nlapiLogExecution('DEBUG','debug', '| kW section | ');
// amps section ----------------------------------------------------------------------------------------------------------------
						if(rptamps == 'T'){
							html += get_amps_html(customerid, customer, facilityid, facility, start, end, arrMonths);
						}
						nlapiLogExecution('DEBUG','debug', '| amps section | ');
// end sections  ----------------------------------------------------------------------------------------------------------------
						// page break for each facility/customer except the last one
						if(j < (searchCustomers.length - 1)){
							html += '<div page-break-before="always"></div>';
						}
					//}
					
	    		}
// facility enviro section ----------------------------------------------------------------------------------------------------------------
				if(rptenviro == 'T'){
					html += get_enviro_html (reportid, facilityid, facility, startOD, endOD, arrReportColumns, colspan2);
				}
			}
			
// pdf report  ----------------------------------------------------------------------------------------------------------------
			
			var objFile = nlapiLoadFile(1520031);
			var reporthtml = objFile.getValue();
			reporthtml = reporthtml.replace(new RegExp('{report}', 'g'), html);
			
			var testFile = nlapiCreateFile('test.html', 'HTMLDOC', reporthtml);
			testFile.setFolder(5029801);
			nlapiSubmitFile(testFile);
			
			var filePDF = nlapiXMLToPDF(reporthtml);
			
			if(thismonth == 0){
				var rptdate = moment().subtract('months', 1).format('YYYY-M');
				var rptmonth = moment().subtract('months', 1).format('M');
				var rptyear = moment().subtract('months', 1).format('YYYY');
			}
			else{
				var rptdate = moment().format('YYYY-M');
				var rptmonth = moment().format('M');
				var rptyear = moment().format('YYYY');
			}
			
			var filename = rptname + '_' + rptdate;
			
			if(byemail == 'T'){
				filename += '_&@';
			}
			filename += '.pdf';
			
			filePDF.setName(filename);
	
			filePDF.setFolder(get_file_folder(rptmonth,rptyear));
			//filePDF.setFolder(1267033); // demo folder
			var fileId = nlapiSubmitFile(filePDF);
			
			if(rptid == 0){
				// verify if the report record (historical) exist for the month, if not create it
				var columns = new Array();
				columns.push(new nlobjSearchColumn('internalid',null,null).setSort(false));
				var filters = new Array();
				filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_pwr_month_rpt",null,"anyof",reportid));
				filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_pwr_month_date",null,"on",rptDate));
				var searchRecords = nlapiSearchRecord('customrecord_clgx_dcim_rpt_pwr_months', null, filters, columns);
				if(searchRecords == null){
					var record = nlapiCreateRecord('customrecord_clgx_dcim_rpt_pwr_months');
					record.setFieldValue('custrecord_clgx_dcim_rpt_pwr_month_rpt', reportid);
					record.setFieldValue('custrecord_clgx_dcim_rpt_pwr_month_date', rptDate);
					record.setFieldValue('custrecord_clgx_dcim_rpt_pwr_month_file', fileId);
					var idRec = nlapiSubmitRecord(record, false,true);
				}
			}

			// update the last pdf report file on the report record 
			var fields = ['custrecord_clgx_dcim_rpt_cust_rpt_file','custrecord_clgx_dcim_rpt_cust_rpt_pdf'];
			var values = [fileId,'F'];
			nlapiSubmitField('customrecord_clgx_dcim_rpt_cust_pwr', reportid, fields, values);
			
			//recReport.setFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_file', fileId);
			//recReport.setFieldValue('custrecord_clgx_dcim_rpt_cust_rpt_pdf', 'F');
	    	//nlapiSubmitRecord(recReport, true, true);
	    	
			var emailSubject = 'Cologix Power Report - ' +  rptname;
			var emailBody = 'Hello,\n\n' +
						'A copy of your power report is attached for your records.\n\n' +
						'If you have questions regarding this report, please send an email to billing@cologix.com.\n\n' + 
						'Thank you for your business.\n\n' +
						'Sincerely,\n' +
						'Cologix\n' +
						'1-855-IX-BILLS (492-4557)\n\n\n';
			
				
			// send emails to employees
			for ( var i = 0; arrEmployeesIDs != null && i < arrEmployeesIDs.length; i++ ) {
				nlapiSendEmail(12827,arrEmployeesIDs[i],emailSubject,emailBody,null,null,null,filePDF,true);
			}
			// send emails to customers contacts if there is data
			if(byemail == 'T'){
				for ( var i = 0; arrContacts != null && i < arrContacts.length; i++ ) {
					nlapiSendEmail(12827,arrContacts[i],emailSubject,emailBody,null,null,null,filePDF,true);
				}
			}
			if(rptid == 0){
				var status = nlapiScheduleScript('customscript_clgx_ss_dcim_pwr_rpt', 'customdeploy_clgx_ss_dcim_pwr_rpt');
			}
			
			clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_admin", "Finishing Report - " + reportid, "Finishing Report - " + reportid);
			
			/*nlapiSendEmail(432742,71418,'Finishing Report - ' + reportid, 'Finishing Report - ' + reportid, null,null,null,null,true);
			nlapiSendEmail(432742,1349020,'Finishing Report - ' + reportid, 'Finishing Report - ' + reportid, null,null,null,null,true);*/
			nlapiLogExecution('DEBUG','debug', '| end = ' + reportid + ' | ');
	       
		}
        else{
        	clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_admin", "Finishing Reports", "No more reports to process.");
        	
        	/*nlapiSendEmail(432742,71418,'Finishing Reports','No more reports to process.',null,null,null,null,true);
        	nlapiSendEmail(432742,1349020,'Finishing Reports','No more reports to process.',null,null,null,null,true);*/
        	nlapiLogExecution('DEBUG','End', 'No more powers to update.');
        }

		
//---------- End Sections ------------------------------------------------------------------------------------------------
	    
    }
    catch (error){
    	nlapiLogExecution('ERROR', 'error', JSON.stringify(error));
        var str = String(error);
        if (str.match('SSS_REQUEST_TIME_EXCEEDED') || str.match('SSS_CONNECTION_TIME_OUT') || str.match('UNEXPECTED_ERROR')) {
        	//nlapiSendEmail(432742,71418, str + ' - Finished powers processing because of communication error and reschedule it','',null,null,null,null,true);
        	//var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
        	//nlapiLogExecution('DEBUG','End', 'Finished powers processing because of communication error and reschedule it.');
        }
        
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}



function get_kwh_html (customerid, customer, facilityid, facility, colspan, start, end, months, nbrmonths, cumulative){
	
	var json = get_kwh_json (customerid, facilityid, start, end, nbrmonths, cumulative);
	
	var html = '<table><tr><td class="title"><h2>' + json.title + '</h2></td></tr></table><br/><br/>';
	html += '<table>';
	
	for ( var p = 0; p < json.panels.length; p++ ) {
		
		// panel header ----------------------------------------------------------------------------------------------------------------
		html += '<tr><td colspan="' + colspan + '" class="td6"><h4>' + json.panels[p].panel + '</h4></td></tr>';
		html += '<tr>';
		html += '<td align="left" class="td5">Module</td>';
		html += '<td align="left" class="td5">Breaker</td>';
		html += '<td align="left" class="td5">Power</td>';
		html += '<td align="left" class="td5">Pair Power</td>';
		html += '<td align="left" class="td5">Space</td>';
		html += '<td align="left" class="td5">Order</td>';
		html += '<td align="left" class="td5">Service</td>';
		html += '<td align="left" class="td5">Voltage</td>';
		html += '<td align="left" class="td5">Amps</td>';
		for ( var m = 0; m < json.panels[p].months.length; m++ ) {
			html += '<td  align="center" colspan="2" class="td5">' + json.panels[p].months[m].name + '</td>';
		}
		html += '</tr>';
		
		// panel body  ----------------------------------------------------------------------------------------------------------------
		for ( var md = 0; md < json.panels[p].modules.length; md++ ) {

			for ( var b = 0; b < json.panels[p].modules[md].breakers.length; b++ ) {
				
				var tdclass = b % 3;
				html += '<tr>';
				if(b == 0){
					html += '<td align="left" rowspan="' + json.panels[p].modules[md].breakers.length + '" class="td4">' + json.panels[p].modules[md].module + '</td>';
				}
				html += '<td align="left" class="td3">' + json.panels[p].modules[md].breakers[b].breaker + '</td>';
				html += '<td align="left" class="td' + tdclass + '">' + json.panels[p].modules[md].breakers[b].power + '</td>';
				html += '<td align="left" class="td' + tdclass + '">' + json.panels[p].modules[md].breakers[b].pair + '</td>';
				html += '<td align="left" class="td' + tdclass + '">' + json.panels[p].modules[md].breakers[b].space + '</td>';
				html += '<td align="left" class="td' + tdclass + '">' + json.panels[p].modules[md].breakers[b].so + '</td>';
				html += '<td align="left" class="td' + tdclass + '">' + json.panels[p].modules[md].breakers[b].service + '</td>';
				html += '<td align="left" class="td' + tdclass + '">' + json.panels[p].modules[md].breakers[b].voltage + '</td>';
				html += '<td align="left" class="td' + tdclass + '">' + json.panels[p].modules[md].breakers[b].amps + '</td>';
				
				for ( var m = 0; m < json.panels[p].modules[md].breakers[b].months.length; m++ ) {
					if(b == 0){
						html += '<td align="right" rowspan="' + json.panels[p].modules[md].breakers.length + '"  class="td7">' +  (json.panels[p].modules[md].months[m].value).toFixed(2) + '</td>';
					}
					html += '<td  align="right" class="td' + tdclass + '">' + (json.panels[p].modules[md].breakers[b].months[m].value).toFixed(2) + '</td>';
				}
				html += '</tr>';
			}
		}

		// panel footer ----------------------------------------------------------------------------------------------------------------
		html += '<tr>';
		html += '<td align="left" colspan="9" class="td10">Panel Totals</td>';
	
		for ( var m = 0; m < json.panels[p].months.length; m++ ) {
			html += '<td align="right" colspan="2" class="td8">' + (json.panels[p].months[m].value).toFixed(2) + '</td>';
		}
		html += '</tr>';
		html += '<tr><td colspan="' + colspan + '" class="space"> </td></tr>'; 
	}
	
	html += '<tr><td colspan="' + colspan + '" class="space"> </td></tr>'; 
	html += '<tr><td colspan="' + colspan + '" class="space"> </td></tr>'; 
	
	// facility footer ----------------------------------------------------------------------------------------------------------------
	html += '<tr>';
	html += '<td align="left" colspan="9" class="td11">ALL ' + facility + ' PANELS TOTALS</td>';
	
	for ( var m = 0; m < json.months.length; m++ ) {
		html += '<td align="right" colspan="2" class="td9">' + (json.months[m].value).toFixed(2) + '</td>';
	}
	html += '</tr>';
	html += '</table>';

	return html;
}


function get_kwh_json (customerid, facilityid, start, end, nbrmonths, cumulative){
	
	var facilityMonths = get_arr_months (nbrmonths, 'F');
	
	var title = 'Power consumption - all values are kWh and represent the consumption for each breaker of each power circuit - the ';
	if(cumulative == 'T'){
		title +=  'cumulative per month';
	}
	else{
		title +=  'delta per month';
	}
	
	// customer services ids
	var columns = [];
	columns.push(new nlobjSearchColumn('internalid',null,null));
	var filters = [];
	filters.push(new nlobjSearchFilter("customer",null,"anyof",customerid));
	filters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	var searchServices = nlapiSearchRecord('job', null, filters, columns);
	var ids = [];
	for ( var b = 0; searchServices != null && b < searchServices.length; b++ ) {
		ids.push(searchServices[b].getValue('internalid',null,null));
	}
	
	// panels
	var columns = [];
	columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device',null,'GROUP').setSort(false));
	columns.push(new nlobjSearchColumn('custrecord_clgx_panel_type','custrecord_clgx_power_panel_pdpm','GROUP'));
	var filters = [];
	filters.push(new nlobjSearchFilter("custrecord_cologix_power_facility",null,"anyof",facilityid));
	filters.push(new nlobjSearchFilter("custrecord_cologix_power_service",null,"anyof",ids));
	filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_device",null,"noneof",'@NONE@'));
	filters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	var searchPanels = nlapiSearchRecord('customrecord_clgx_power_circuit', null, filters, columns);
	
	var panels = [];
	for ( var p = 0; searchPanels != null && p < searchPanels.length; p++ ) {
		
		var panelMonths = get_arr_months (nbrmonths, 'F');
		var panelid = searchPanels[p].getValue('custrecord_clgx_dcim_device',null,'GROUP');
		var panel = searchPanels[p].getText('custrecord_clgx_dcim_device',null,'GROUP');
		var type = parseInt(searchPanels[p].getValue('custrecord_clgx_panel_type','custrecord_clgx_power_panel_pdpm','GROUP')) || 1;

		// modules
		var columns = [];
		var filters = [];
		filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_device",null,"anyof",panelid));
		filters.push(new nlobjSearchFilter("custrecord_cologix_power_facility",null,"anyof",facilityid));
		filters.push(new nlobjSearchFilter("custrecord_cologix_power_service",null,"anyof",ids));
		filters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
		var searchModules = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_rpt_pnl_modules', filters, columns);
		
		var modules = [];
		for ( var md = 0; searchModules != null && md < searchModules.length; md++ ) {
			
			var moduleMonths = get_arr_months (nbrmonths, 'F');
			var columns = searchModules[md].getAllColumns();
			if(type == 2 || type == 12){
				var moduleid = parseInt(searchModules[md].getText(columns[1]));
			} else {
				var moduleid = 1;
			}

			var columns = [];
			var filters = [];
			filters.push(new nlobjSearchFilter("custrecord_cologix_power_service",null,"anyof",ids));
			filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_device",null,"anyof",panelid));
			if(type == 2 || type == 12){
				filters.push(new nlobjSearchFilter("custrecord_cologix_power_upsbreaker",null,"anyof",moduleid));
			}
			filters.push(new nlobjSearchFilter("custrecord_cologix_power_facility",null,"anyof",facilityid));
			filters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
			var searchBreakers = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_rpt_pnl_breakers', filters, columns);
			
			var breakers = [];
			for ( var b = 0; searchBreakers != null && b < searchBreakers.length; b++ ) {
				
				var columns = searchBreakers[b].getAllColumns();
				var circuit = searchBreakers[b].getText('custrecord_cologix_power_amps',null,null);
				var powerid = parseInt(searchBreakers[b].getValue('internalid',null,null));
				var breaker = parseInt(searchBreakers[b].getValue(columns[10])) || 0;
				
				// breaker months values
				var columns = [];
				var filters = [];
				filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_power",null,"anyof",powerid));
				filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_breaker",null,"equalto",breaker));
				filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_device",'custrecord_clgx_dcim_points_day_power',"anyof",panelid));
				if(type == 2 || type == 12){
					filters.push(new nlobjSearchFilter("custrecord_cologix_power_upsbreaker",'custrecord_clgx_dcim_points_day_power',"anyof",moduleid));
				}
				filters.push(new nlobjSearchFilter('custrecord_clgx_dcim_points_day_date',null,'within',start,end));
				filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_service",null,"anyof",ids));
				filters.push(new nlobjSearchFilter("custrecord_cologix_power_facility",'custrecord_clgx_dcim_points_day_power',"anyof",facilityid));
				var searchBreakersMonths = nlapiSearchRecord('customrecord_clgx_dcim_points_day', 'customsearch_clgx_dcim_rpt_pwrs_days_kwh', filters, columns);
				
				var arr = [];
				for ( var bm = 0; searchBreakersMonths != null && bm < searchBreakersMonths.length; bm++ ) {
					var columns = searchBreakersMonths[bm].getAllColumns();
	    			arr.push({
						"name": searchBreakersMonths[bm].getValue(columns[2]),
						"value": parseFloat(searchBreakersMonths[bm].getValue(columns[3])),
						"cumul": parseFloat(searchBreakersMonths[bm].getValue(columns[4]))
	    			});
				}
			
				var breakerMonths = get_arr_months (nbrmonths, 'F');
				for ( var m = 0; breakerMonths != null && m < breakerMonths.length; m++ ) {
					var obj = _.find(arr, function(arr){ return (arr.name == breakerMonths[m].name); });
					if(obj != null){

						breakerMonths[m].value = obj.value;
						breakerMonths[m].cumul = obj.cumul;
						
						moduleMonths[m].value += obj.value;
						moduleMonths[m].cumul += obj.cumul;
						
						panelMonths[m].value += obj.value;
						panelMonths[m].cumul += obj.cumul;
						
						facilityMonths[m].value += obj.value;
						facilityMonths[m].cumul += obj.cumul;
					}
				}
				
				var objBreaker = {
						//"module": parseInt(searchBreakers[b].getValue(columns[9])),
						"breaker": breaker,
						//"powerid": powerid,
						"power": searchBreakers[b].getValue('name',null,null),
						"pair": searchBreakers[b].getText('custrecord_clgx_dcim_pair_power',null,null),
						"space": (searchBreakers[b].getText('custrecord_cologix_power_space',null,null)).replace(/\&/g,'&amp;'),
						"so": _.last((searchBreakers[b].getText('custrecord_power_circuit_service_order',null,null)).split("#")).trim(),
						"service": _.last((searchBreakers[b].getText('custrecord_cologix_power_service',null,null)).split(":")).trim(),
						"voltage": searchBreakers[b].getText('custrecord_cologix_power_volts',null,null),
						//"volts": parseInt((searchBreakers[b].getText('custrecord_cologix_power_volts',null,null)).substring(0, 3)),
						//"circuit": circuit,
						"amps": (circuit).substring(0, circuit.length - 1),
						"months": breakerMonths,
						"arr": arr
				};
				breakers.push(objBreaker);
			}
			
			modules.push({
				"module": moduleid,
				"months": moduleMonths,
				"breakers": breakers
			});
		}
		panels.push({
			"panel": panel,
			"months": panelMonths,
			"modules": modules
		});
	}

	var json = {
			"title":title,
			"cumulative":cumulative,
			"months": facilityMonths,
			"panels": panels
	};
	
	//nlapiSendEmail(71418,71418,'json',JSON.stringify( json ),null,null,null,true);
	
	return json;
}


function get_kw_html (customerid, customer, facilityid, facility, start, end){
	
	var html = '';
	
	var columns = new Array();
	columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_kw',null,null));
	columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_date',null,null));
	var filters = new Array();
	filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_facility",null,"anyof",facilityid));
	filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_customer",null,"anyof",customerid));
	var searchMaxPeak = nlapiSearchRecord('customrecord_clgx_dcim_customer_peak', null, filters, columns);
	
	if (searchMaxPeak != null){

		var maxpeak = parseFloat(searchMaxPeak[0].getValue('custrecord_clgx_dcim_peak_kw',null,null)).toFixed(2);
		var maxpeakdate = searchMaxPeak[0].getValue('custrecord_clgx_dcim_peak_date',null,null);
		
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_day_date',null,null));
		columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_day_kw',null,null));
		var filters = new Array();
		filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_day_facilty",null,"anyof",facilityid));
		filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_day_customer",null,"anyof",customerid));
		filters.push(new nlobjSearchFilter('custrecord_clgx_dcim_peak_day_date',null,'within',start,end));
		var searchPeaks = nlapiSearchRecord('customrecord_clgx_dcim_customer_peak_day', null, filters, columns);
		
		var arrPeaks = new Array();
		for ( var i = 0; searchPeaks != null && i < searchPeaks.length; i++ ) {
			var searchPeak = searchPeaks[i];
			var peak = searchPeak.getValue('custrecord_clgx_dcim_peak_day_kw',null,null);
			var peakdate = searchPeak.getValue('custrecord_clgx_dcim_peak_day_date',null,null);
			var peakmonth = moment(peakdate).format('YYYY-MM');
			var peakday = moment(peakdate).format('D');
			
			var objPeak = new Object();
			objPeak["month"] = peakmonth;
			objPeak["day"] = parseInt(peakday);
			objPeak["peak"] = parseFloat(peak);
			arrPeaks.push(objPeak);
		}
		var arrMonths = _.uniq(_.pluck(arrPeaks, 'month'));
		
		html += '<table>';
		html += '<tr><td class="title"> </td></tr>';
		html += '<tr><td class="title"><h2>Power demand - all values are kW and represent the peak at ' + facility + ' for all power circuits per day (details by 15 minutes for each day can be provided on request)</h2></td></tr>';
		html += '<tr><td class="title">All times peak is ' + maxpeak + ' kW and it occurred on ' + maxpeakdate + '.</td></tr>';
		html += '</table>';
		
		html += '<table>';
		html += '<tr>';
		html += '<td class="td5">Month</td>';
		for ( var i = 1; i < 32; i++ ) {
			html += '<td class="td5" align="right">' + i + '</td>';
		}
		html += '<td class="td9" align="right">Peak</td>';
		html += '</tr>';
		
		
		for ( var i = 0; arrMonths != null && i < arrMonths.length; i++ ) {
			var tdclass = i % 3;
			html += '<tr>';
			html += '<td class="td4" >' + arrMonths[i] + '</td>';
			var max = 0;
			for ( var j = 1; j < 32; j++ ) {
				
				var objPeak = _.find(arrPeaks, function(arr){ return (arr.month == arrMonths[i] && arr.day == j); });
				if(objPeak != null){
					if(objPeak.peak > max){
						max = objPeak.peak;
					}
					var peak = (objPeak.peak).toFixed(2);
					
					html += '<td class="td' + tdclass + '" align="right">' + peak + '</td>';
				}
				else{
					html += '<td class="td' + tdclass + '"></td>';
				}
				
			}
			max = max.toFixed(2);
			html += '<td class="td8" align="right">' + max + '</td>';
			html += '</tr>';
		}
	
		
		html += '</table>';
	}
	return html;
}


function get_amps_html (customerid, customer, facilityid, facility, start, end, arrMonths){
	
	nlapiLogExecution("DEBUG", "customerid", customerid);
	nlapiLogExecution("DEBUG", "customer", JSON.stringify(customer));
	nlapiLogExecution("DEBUG", "facilityid", facilityid);
	nlapiLogExecution("DEBUG", "facility", JSON.stringify(facility));
	nlapiLogExecution("DEBUG", "start", start);
	nlapiLogExecution("DEBUG", "end", end);
	nlapiLogExecution("DEBUG", "arrMonths", JSON.stringify(arrMonths));
	
	
	var html = '<table>';
	html += '<tr><td class="title"> </td></tr>';
	html += '<tr><td class="title"><h2>Power utilization - all values are Amps and represent the averages values and usage (%) of each breaker of a power circuit per day (when usage exceeds 80% values are red)</h2></td></tr>';
	html += '</table>';
	
	var startM = moment(start);
	var endM = moment(start).endOf('month');
	
	for ( var m = 0; arrMonths != null && m < arrMonths.length; m++ ) {

		var arrAmps = new Array();
		var columns = new Array();
		var filters = new Array();
		filters.push(new nlobjSearchFilter("parent","custrecord_clgx_dcim_points_day_service","anyof",customerid));
		filters.push(new nlobjSearchFilter('custrecord_clgx_dcim_points_day_date',null,'within',startM.format('M/D/YYYY'),endM.format('M/D/YYYY')));
		var searchPoints = nlapiLoadSearch('customrecord_clgx_dcim_points_day', 'customsearch_clgx_rpt_pwrs_days_amps');
		searchPoints.addColumns(columns);
		searchPoints.addFilters(filters);
		var resultSet = searchPoints.runSearch();
		
		resultSet.forEachResult(function(searchResult) {
			
			var ampsdate = searchResult.getValue('custrecord_clgx_dcim_points_day_date',null,null);
			var ampsmonth = moment(ampsdate).format('YYYY-MM');
			var ampsday = moment(ampsdate).format('D');
			var powerid = parseInt(searchResult.getValue('custrecord_clgx_dcim_points_day_power',null,null));
			var power = searchResult.getText('custrecord_clgx_dcim_points_day_power',null,null);
			var pairid = parseInt(searchResult.getValue('custrecord_clgx_dcim_pair_power','custrecord_clgx_dcim_points_day_power',null));
			var pair = searchResult.getText('custrecord_clgx_dcim_pair_power','custrecord_clgx_dcim_points_day_power',null);
			var breaker = parseInt(searchResult.getValue('custrecord_clgx_dcim_points_day_breaker',null,null));
			var volts = parseInt(searchResult.getValue('custrecord_clgx_dcim_points_day_volts',null,null));
			var phases = parseInt(searchResult.getValue('custrecord_clgx_dcim_points_day_phases',null,null));
			var amps = parseInt(searchResult.getValue('custrecord_clgx_dcim_points_day_amps',null,null));
			var avg = parseFloat(searchResult.getValue('custrecord_clgx_dcim_points_day_amps_avg',null,null));
			
			var objAmps = new Object();
			objAmps["month"] = ampsmonth;
			objAmps["day"] = parseInt(ampsday);
			objAmps["powerid"] = powerid;
			objAmps["power"] = power;
			objAmps["pairid"] = pairid;
			objAmps["pair"] = pair;
			objAmps["breaker"] = breaker;
			objAmps["volts"] = volts;
			objAmps["phases"] = phases;
			objAmps["amps"] = amps;
			objAmps["avg"] = avg.toFixed(2);
			objAmps["avg2"] = avg.toFixed(2);
			
			if(amps > 0){
				var usage = parseInt(((avg*100) / amps).toFixed(2));
				objAmps["usage"] = usage;
			}
			else{
				objAmps["usage"] = 0;
			}
			if(usage > 80){
				objAmps["outage"] = 1;
			}
			else{
				objAmps["outage"] = 0;
			}
			
			arrAmps.push(objAmps);
	
			return true;
		});
		var arrPowersIDs = _.uniq(_.pluck(arrAmps, 'powerid'));

		html += '<table>';
		//var tdclass = i % 3;
		html += '<tr>';
		html += '<td class="td5" >' + arrMonths[m].name + '</td>';
		html += '<td class="td5" >Breaker</td>';
		html += '<td class="td5" >Volts</td>';
		html += '<td class="td5" >Phases</td>';
		html += '<td class="td5" >Amps</td>';
		for ( var i = 1; i < 32; i++ ) {
			html += '<td class="td5" align="right">' + i + '</td>';
		}
		html += '</tr>';
		
		for ( var p = 0; arrPowersIDs != null && p < arrPowersIDs.length; p++ ) {
			
			var pairid = 0;
			var objPair = _.find(arrAmps, function(arr){ return (arr.powerid == arrPowersIDs[p]); });
			if(objPair != null){
				pairid = objPair.pairid;
			}
			
			
			var arrBreakers = _.filter(arrAmps, function(arr){
				return (arr.powerid == arrPowersIDs[p]);
			});
			var arrBreakersIDsUnsorted = _.uniq(_.pluck(arrBreakers, 'breaker'));
			var arrBreakersIDs = _.sortBy(arrBreakersIDsUnsorted, function(num){ return num; });
			
			for ( var b = 0; arrBreakersIDs != null && b < arrBreakersIDs.length; b++ ) {
			
				var tdclass = b % 3;
				var objBreaker = _.find(arrBreakers, function(arr){ return (arr.powerid == arrPowersIDs[p] && arr.breaker == arrBreakersIDs[b]); });
				
				if(objBreaker != null){
					html += '<tr>';
					if(b == 0){
						html += '<td class="td4" rowspan="' + arrBreakersIDs.length + '">' + objBreaker.power + '</td>';
					}
					html += '<td class="td3" align="right">' + objBreaker.breaker + '</td>';
					html += '<td class="td3" align="right">' + objBreaker.volts + '</td>';
					html += '<td class="td3" align="right">' + objBreaker.phases + '</td>';
					html += '<td class="td3" align="right">' + objBreaker.amps + '</td>';
					for ( var i = 1; i < 32; i++ ) {
						
						var objBreakerDay = _.find(arrAmps, function(arr){ return (arr.powerid == arrPowersIDs[p] && arr.breaker == arrBreakersIDs[b] && arr.day == i); });
			
						if(objBreakerDay != null){
							
							var average = parseFloat(objBreakerDay.avg);
							var amps = objBreakerDay.amps;
							
							var arrPairDay = _.filter(arrAmps, function(arr){
						        return (arr.powerid == pairid && arr.day == i);
							});
							if(arrPairDay != null){
								var objPairDay = arrPairDay[b];
								if(objPairDay != null){
									average += parseFloat(objPairDay.avg);
								}
							}
							
							if(amps > 0){
								var usage = parseInt(((average*100) / amps).toFixed(2));
							}
							else{
								var usage = 0;
							}
							if(usage > 80){
								var outage = 1;
							}
							else{
								var outage  = 0;
							}
							
							html += '<td class="td' + tdclass + '" align="right">' + objBreakerDay.avg + '<br/><span class="outage' + outage + '">' + average.toFixed(2) + '</span><br/><span class="outage' + outage + '">' + usage + '%</span></td>';
						}
						else{
							html += '<td class="td' + tdclass + '" align="right"></td>';
						}
					}
					html += '</tr>';
				}
			}
		}
		html += '<tr><td colspan="36" class="title"> </td></tr>';
		html += '</table>';
		
		startM.add('months', 1);
		endM.startOf('month').add('months', 1).endOf('month');

	}
	
	return html;
}

function get_enviro_html (reportid, facilityid, facility, startOD, endOD, months, colspan2){
	
	var html = '';
	
	var arrPointsExtIDs = new Array();
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_cust_dev_rpt",null,"anyof",reportid));
	filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_cust_dev_fac",null,"anyof",facilityid));
	var searchDevices = nlapiSearchRecord('customrecord_clgx_dcim_rpt_cust_dev', 'customsearch_clgx_dcim_rpt_cust_devices', filters, columns);
	var arrDevicesGrid = new Array();
	for ( var i = 0; searchDevices != null && i < searchDevices.length; i++ ) {
		var searchDevice = searchDevices[i];
    	var objDevice = new Object();
    	objDevice["deviceid"] = parseInt(searchDevice.getValue('custrecord_clgx_dcim_rpt_cust_dev_device',null,null));
    	objDevice["device"] = searchDevice.getText('custrecord_clgx_dcim_rpt_cust_dev_device',null,null);
    	objDevice["sensorid"] = parseInt(searchDevice.getValue('internalid','CUSTRECORD_CLGX_DCIM_RPT_CUST_DEV_POINTS',null));
    	objDevice["sensorextid"] = parseInt(searchDevice.getValue('externalid','CUSTRECORD_CLGX_DCIM_RPT_CUST_DEV_POINTS',null));
    	arrPointsExtIDs.push(parseInt(searchDevice.getValue('externalid','CUSTRECORD_CLGX_DCIM_RPT_CUST_DEV_POINTS',null)));
    	objDevice["sensor"] = searchDevice.getValue('name','CUSTRECORD_CLGX_DCIM_RPT_CUST_DEV_POINTS',null);
    	//objDevice["sensoravg"] = parseFloat(searchDevice.getValue('custrecord_clgx_dcim_points_day_avg','CUSTRECORD_CLGX_DCIM_RPT_CUST_DEV_POINTS',null));
    	objDevice["months"] = [];
    	arrDevicesGrid.push(objDevice);
	}
	
	var arrDevicesIDs = _.uniq(_.pluck(arrDevicesGrid, 'deviceid'));
	var arrDevicesNames = _.uniq(_.pluck(arrDevicesGrid, 'device'));
	var arrDevices = new Array();
	for ( var i = 0; arrDevicesIDs != null && i < arrDevicesIDs.length; i++ ) {
		var objDevice = new Object();
    	objDevice["deviceid"] = arrDevicesIDs[i];
    	objDevice["device"] = arrDevicesNames[i];
		var arrDeviceSensors = _.filter(arrDevicesGrid, function(arr){
	        return (arr.deviceid === arrDevicesIDs[i]);
		});
		var arrSensors = new Array();
		for ( var j = 0; arrDeviceSensors != null && j < arrDeviceSensors.length; j++ ) {
			var objSensor = new Object();
			objSensor["sensorid"] = arrDeviceSensors[j].sensorid;
			objSensor["sensorextid"] = arrDeviceSensors[j].sensorextid;
			objSensor["sensor"] = arrDeviceSensors[j].sensor;
			//objSensor["sensoravg"] = arrDeviceSensors[j].sensoravg;
			arrSensors.push(objSensor);
		}
		objDevice["sensors"] = arrSensors;
		arrDevices.push(objDevice);
	}
	
	var url = '';
	url += 'https://lucee-nnj3.dev.nac.net/odins/powers/get_rpt_customer_points.cfm';
	//url += 'https://command1.cologix.com:10313/netsuite/reports/get_rpt_customer_points.cfm';
	url += '?start=' + startOD;
	url += '&end=' + endOD;
	url += '&e=0';
	url += '&p=' + arrPointsExtIDs.join();
	url += '&c=0';
	
	var requestURL = nlapiRequestURL(url);
	var valuesJSON = requestURL.body;
	var arrValues= JSON.parse( valuesJSON );
	var arrEnvironmentalVals = arrValues[1];
	var arrEnvironmental = get_points_values(arrPointsExtIDs, arrEnvironmentalVals,'enviro');
	
	for ( var o = 0; arrDevices != null && o < arrDevices.length; o++ ) {
		var arrSensors = arrDevices[o].sensors;
		for ( var p = 0; arrSensors != null && p < arrSensors.length; p++ ) {
			
			var objPoint = _.find(arrEnvironmental, function(arr){ return (arr.pointid == arrSensors[p].sensorextid); });
			if(objPoint != null){
				arrDevices[o].sensors[p].months = objPoint.months;
			}
		}
	}
	
	if(arrDevices.length > 0){
		// page break for environmentals section if it exist
		html += '<div page-break-before="always"></div>';
		
		html += '<table>';
		html += '<tr><td colspan="' + colspan2 + '" class="title"> </td></tr>';
		html += '<tr><td colspan="' + colspan2 + '" class="title"><h2>Environmentals for ' + facility + '</h2></td></tr>';
		html += '<tr><td colspan="' + colspan2 + '" class="title"> </td></tr>'; 
		
		for ( var dv = 0; arrDevices != null && dv < arrDevices.length; dv++ ) {
			html += '<tr><td  colspan="' + colspan2 + '" class="td6"><h4>' + arrDevices[dv].device + '</h4></td></tr>';
			html += '<tr><td align="left" colspan="2" class="td5">Sensor</td>';
			for ( var rc = 0; months != null && rc < months.length; rc++ ) {
				html += '<td  align="center" class="td5">' + months[rc].name + '</td>';
			}
			html += '</tr>';
			
			var arrSensors = arrDevices[dv].sensors;
			for ( var sn = 0; arrSensors != null && sn < arrSensors.length; sn++ ) {
				
				var tdclass = sn % 2;
				html += '<tr>';
				html += '<td align="left" colspan="2" class="td4">' + arrSensors[sn].sensor + '</td>';
				
				var arrSMonths = arrSensors[sn].months;
				for ( var rc = 0; months != null && rc < months.length; rc++ ) {
					var objMonth = _.find(arrSMonths, function(arr){ return (arr.month == months[rc].name); });
					if(objMonth != null){
						html += '<td align="right" class="td' + tdclass + '">' + (objMonth.value).toFixed(2) + '</td>';
					}
					else{
						html += '<td align="right" class="td' + tdclass + '">0</td>';
					}
				}
				html += '</tr>';
			}
			html += '<tr><td colspan="' + colspan2 + '" class="space"> </td></tr>'; 
		}
		html += '</table>';
	}
	
	return html;
}


function get_points_values (arrPoints, arrValues, type){
	
	var arrPointsValues = new Array(); 
	for ( var i = 0; arrPoints != null && i < arrPoints.length; i++ ) {
		var objPoint = new Object();
		objPoint["pointid"] = arrPoints[i];
		var arrPValues = _.filter(arrValues, function(arr){
	        return (arr.POINTID === arrPoints[i]);
		});
		var arrMonths = new Array();
		for ( var j = 0; arrPValues != null && j < arrPValues.length; j++ ) {
			var objMonth = new Object();
			objMonth["monthid"] = j;
			objMonth["month"] = arrPValues[j].MONTH;
			
			var value = arrPValues[j].VAL;
			if(value < 0){
				value = 0;
			}
			if(j == 0){
				var prevj = 0;
			}
			else{
				var prevj = j - 1;
			}
			var prevalue = arrPValues[prevj].VAL;
			if(prevalue <  0){
				prevalue = 0;
			}
			var difference = value - prevalue;
			
			if(type == 'energy'){
				objMonth["kwh"] = parseFloat(value);
				objMonth["ckwh"] = 0;
				objMonth["kwhpre"] = parseFloat(prevalue);
				objMonth["kwhdiff"] = parseFloat(difference);
			}
			if(type == 'enviro'){
				objMonth["value"] = parseFloat(value);
			}
			if(type == 'current'){
				objMonth["hours"] = arrPValues[j].COUNT;
				objMonth["amps"] = parseFloat(value);
				objMonth["kwh"] = 0;
				
				var month = arrPValues[j].MONTH;
				var start = moment(month + '-01');
				var end = moment(month + '-01').endOf('month');
				var mhours = end.diff(start, 'hours') +1;
				
				objMonth["mhours"] = mhours;
				objMonth["mkwh"] = 0;
			}
			arrMonths.push(objMonth);
		}
		objPoint["months"] = arrMonths;
		arrPointsValues.push(objPoint);
	}
	return arrPointsValues;
}

function get_arr_months (nbrmonths, thismonth){
	
	var arrMonths = new Array();
	if(thismonth == 'T'){
		var m = moment().startOf('month').subtract('months', nbrmonths);
	}
	else{
		var m = moment().startOf('month').subtract('months', nbrmonths+1);
	}

	for ( var i = 0; i < nbrmonths; i++ ) {
		var objMonth = new Object();
		objMonth["id"] = i;
		objMonth["name"] = m.add('months', 1).format('YYYY-MM');
		objMonth["value"] = 0;
		objMonth["cumul"] = 0;
		arrMonths.push(objMonth);
	}
	return arrMonths;
}


function get_file_folder(rptmonth,rptyear){

	var fileFolder = 1266919;
	
	var arr2015 = [1594284,1594285,1594286,1594287,1594288,1594289,1594290,1594291,1594292,1594293,1594294,1594295];
	var arr2016 = [2664254,2664255,2664256,2664257,2664258,2664259,2664260,2664261,2664262,2664263,2664264,2664265];
	var arr2017 = [3820982,3820983,3820984,3820985,3820986,3820987,3820988,3820989,3820990,3820991,3820992,3820993];
	var arr2018 = [6337112,6337213,6337214,6337215,6337316,6337317,6337318,6337319,6337320,6337421,6337422,6337423];
	var arr2019 = [11990793,11990794,11990795,11990796,11990797,11990798,11990799,11990800,11990801,11990802,11990803,11990904];
	var arr2020 = [16871816,16871817,16871818,16871819,16871820,16871821,16871822,16871823,16871824,16871825,16871826,16871827];
	
	if(rptyear == 2020) {
		fileFolder = arr2020[(rptmonth-1)];
	}
	else if(rptyear == 2019) {
		fileFolder = arr2019[(rptmonth-1)];
	}
	else if(rptyear == 2018) {
		fileFolder = arr2018[(rptmonth-1)];
	}
	else if(rptyear == 2017){
		fileFolder = arr2017[(rptmonth-1)];
	}
	else{
		fileFolder = arr2016[(rptmonth-1)];
	}

	return fileFolder;
}
