nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_Power.js
//	Script Name:	CLGX_SL_DCIM_Power
//	Script Id:		customscript_clgx_sl_dcim_power
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/14/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=366&deploy=1  
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_power (request, response){
	try {
		
		var powerid = request.getParameter('powerid');
		var module = 1;
		
		var recPower = nlapiLoadRecord ('customrecord_clgx_power_circuit',powerid);
		var id = recPower.getFieldValue('id');
		var name = recPower.getFieldValue('name');
		var created = moment(nlapiStringToDate(recPower.getFieldValue('created'))).startOf('month').subtract('days', 1).format('YYYY-M-D');
		var circuit = recPower.getFieldText('custrecord_cologix_power_amps');
		var voltage = recPower.getFieldText('custrecord_cologix_power_volts');
		var volts = voltage.substring(0, 3);
		
		var strAmps = circuit.substring(0, 3);
		if(strAmps.substr(strAmps.length - 1) == 'A'){
			var strAmps = strAmps.substring(0, 2);
		}
		var amps = parseInt(strAmps);
		
		var isPDPM = 'F';
		var panelid = recPower.getFieldValue('custrecord_clgx_power_panel_pdpm');
		if(panelid != null && panelid != ''){
			var pdpm = nlapiLookupField('customrecord_ncfar_asset', panelid, 'custrecord_clgx_panel_type');
			if(pdpm == 2){
				isPDPM = 'T';
				module = parseInt(recPower.getFieldText('custrecord_cologix_power_upsbreaker'));
			}
		}
		
		var arrExternalIDs = new Array();
		
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",powerid));
		var searchPointsAmps = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_panels_pwr_amps', arrFilters, arrColumns);
		var arrPointsAmps = new Array();
		for ( var i = 0; searchPointsAmps != null && i < searchPointsAmps.length; i++ ) {
			var searchPoint = searchPointsAmps[i];
			var columns = searchPoint.getAllColumns();
			var objPoint = new Object();
			objPoint["breaker"] = searchPoint.getValue(columns[0]);
			objPoint["breakerid"] = parseInt(searchPoint.getValue(columns[1]));
			objPoint["intid"] = parseInt(searchPoint.getValue(columns[2]));
			objPoint["extid"] = parseInt(searchPoint.getValue(columns[3]));
			arrExternalIDs.push(parseInt(searchPoint.getValue(columns[3])));
			objPoint["name"] = parseInt(searchPoint.getValue(columns[4]));
			//objPoint["avg"] = parseFloat(searchPoint.getValue(columns[5]));
			arrPointsAmps.push(objPoint);
		}
		
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",powerid));
		var searchPointsKW = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_panels_pwr_kw', arrFilters, arrColumns);
		var arrPointsKW = new Array();
		for ( var i = 0; searchPointsKW != null && i < searchPointsKW.length; i++ ) {
			var searchPoint = searchPointsKW[i];
			var columns = searchPoint.getAllColumns();
			var objPoint = new Object();
			objPoint["breaker"] = searchPoint.getValue(columns[0]);
			objPoint["breakerid"] = parseInt(searchPoint.getValue(columns[1]));
			objPoint["intid"] = parseInt(searchPoint.getValue(columns[2]));
			objPoint["extid"] = parseInt(searchPoint.getValue(columns[3]));
			arrExternalIDs.push(parseInt(searchPoint.getValue(columns[3])));
			objPoint["name"] = parseInt(searchPoint.getValue(columns[4]));
			//objPoint["avg"] = parseFloat(searchPoint.getValue(columns[5]));
			arrPointsKW.push(objPoint);
		}
		
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",powerid));
		var searchPointsKWH = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_panels_pwr_kwh', arrFilters, arrColumns);
		var arrPointsKWH = new Array();
		for ( var i = 0; searchPointsKWH != null && i < searchPointsKWH.length; i++ ) {
			var searchPoint = searchPointsKWH[i];
			var columns = searchPoint.getAllColumns();
			var objPoint = new Object();
			objPoint["breaker"] = searchPoint.getValue(columns[0]);
			objPoint["breakerid"] = parseInt(searchPoint.getValue(columns[1]));
			objPoint["intid"] = parseInt(searchPoint.getValue(columns[2]));
			objPoint["extid"] = parseInt(searchPoint.getValue(columns[3]));
			arrExternalIDs.push(parseInt(searchPoint.getValue(columns[3])));
			objPoint["name"] = parseInt(searchPoint.getValue(columns[4]));
			//objPoint["avg"] = parseFloat(searchPoint.getValue(columns[5]));
			arrPointsKWH.push(objPoint);
		}
		
		// get extids values from OpenData
		var end = moment().add('days', 1).format('YYYY-M-D');
		
		var url = '';
		url += 'https://lucee-nnj3.dev.nac.net/odins/powers/get_power_history_days.cfm';
		//url += 'https://command1.cologix.com:10313/netsuite/power/get_power_history_days.cfm';
		url += '?start=' + created;
		url += '&end=' + end;
		url += '&points=' + arrExternalIDs.join();
		var requestURL = nlapiRequestURL(url);
		var valuesJSON = requestURL.body;
		var arrValues= JSON.parse( valuesJSON );
		
		var arrMonthsValues = arrValues[0];
		var arrDaysValues = arrValues[1];
		var arrDaysHoursValues = arrValues[2];
		
		var objPower = new Object();
		objPower["text"] = '.';
		objPower["powerid"] = parseInt(id);
		objPower["power"] = name;
		objPower["created"] = created;
		objPower["voltage"] = voltage;
		objPower["volts"] = parseInt(volts);
		objPower["circuit"] = circuit;
		objPower["amps"] = parseInt(amps);
		objPower["pdpm"] = isPDPM;
		objPower["module"] = parseInt(module);
		
		var arrBreakers = new Array();
		for ( var i = 0; arrPointsAmps != null && i < arrPointsAmps.length; i++ ) {
			var objBreaker = new Object();
			objBreaker["node"] = 'Breaker ' + arrPointsAmps[i].breakerid;
			objBreaker["breaker"] = arrPointsAmps[i].breakerid;
			objBreaker["ampsintid"] = arrPointsAmps[i].intid;
			objBreaker["ampsextid"] = arrPointsAmps[i].extid;
			
			var objBreakerKW = _.find(arrPointsKW, function(arr){ return (arr.breakerid == arrPointsAmps[i].breakerid); });
			if(objBreakerKW != null){
				objBreaker["kwintid"] = objBreakerKW.intid;
				objBreaker["kwextid"] = objBreakerKW.extid;
			}
			else{
				objBreaker["kwintid"] = 0;
				objBreaker["kwextid"] = 0;
			}
			var objBreakerKWH = _.find(arrPointsKWH, function(arr){ return (arr.breakerid == arrPointsAmps[i].breakerid || arr.breakerid == 0); });
			if(objBreakerKWH != null){
				objBreaker["kwhintid"] = objBreakerKWH.intid;
				objBreaker["kwhextid"] = objBreakerKWH.extid;
			}
			else{
				objBreaker["kwhintid"] = 0;
				objBreaker["kwhextid"] = 0;
			}
			objBreaker["expanded"] = true;
			objBreaker["iconCls"] = 'power';
			objBreaker["leaf"] = false;
			
			var arrMonths = new Array();
			
			var arrMonthsAmps = _.filter(arrMonthsValues, function(arr){
		        return (arr.POINTID == arrPointsAmps[i].extid);
			});
			var previouskwhmax = 0;
			var previouskwhmean = 0;
			for ( var j = 0; arrMonthsAmps != null && j < arrMonthsAmps.length; j++ ) {
				var objMonth = new Object();
				objMonth["node"] = arrMonthsAmps[j].MONTH;
				objMonth["month"] = arrMonthsAmps[j].MONTH;
				
				objMonth["ampsintid"] = objBreaker.ampsintid;
				objMonth["ampsextid"] = objBreaker.ampsextid;
				objMonth["kwextid"] = objBreaker.kwextid;
				objMonth["kwhextid"] = objBreaker.kwhextid;
				
				objMonth["ampsmax"] = Math.max(0,arrMonthsAmps[j].MAX);
				objMonth["ampsmean"] = Math.max(0,arrMonthsAmps[j].MEAN);
				objMonth["ampsminutes"] = arrMonthsAmps[j].MINUTES;
				objMonth["ampsacumul"] = Math.max(0,arrMonthsAmps[j].ACUMUL);
				
				var objMonthKW = _.find(arrMonthsValues, function(arr){ return (arr.POINTID == objBreaker.kwextid && arr.MONTH == arrMonthsAmps[j].MONTH); });
				//nlapiSendEmail(71418,71418,'objMonthKW',JSON.stringify(objMonthKW),null,null,null,null);
				if(objMonthKW != null){
					objMonth["kwmax"] = Math.max(0,objMonthKW.MAX);
					objMonth["kwmean"] = Math.max(0,objMonthKW.MEAN);
					objMonth["kwminutes"] = objMonthKW.MINUTES;
					objMonth["kwacumul"] = Math.max(0,objMonthKW.ACUMUL);
				}
				else{
					objMonth["kwmax"] = 0;
					objMonth["kwmean"] = 0;
					objMonth["kwminutes"] = 0;
					objMonth["kwacumul"] = 0;
				}
				var objMonthKWH = _.find(arrMonthsValues, function(arr){ return (arr.POINTID == objBreaker.kwhextid && arr.MONTH == arrMonthsAmps[j].MONTH); });
				if(objMonthKWH != null){
					objMonth["kwhmax"] = Math.max(0,objMonthKWH.MAX);
					objMonth["kwhmean"] = Math.max(0,objMonthKWH.MEAN);
					objMonth["kwhminutes"] = objMonthKWH.MINUTES;
					objMonth["kwhacumul"] = Math.max(0,objMonthKWH.ACUMUL);
				}
				else{
					objMonth["kwhmax"] = 0;
					objMonth["kwhmean"] = 0;
					objMonth["kwhminutes"] = 0;
					objMonth["kwhacumul"] = 0;
				}
				objMonth["expanded"] = false;
				objMonth["iconCls"] = 'month';
				objMonth["leaf"] = false;
				
				var arrMonthDaysAmps = _.filter(arrDaysValues, function(arr){
			        return (arr.POINTID == objBreaker.ampsextid && arr.MONTH == arrMonthsAmps[j].MONTH);
				});
				var arrMonthDaysHoursAmps = _.filter(arrDaysHoursValues, function(arr){
			        return (arr.POINTID == objBreaker.ampsextid && arr.MONTH == arrMonthsAmps[j].MONTH);
				});
				
				var arrMonthDaysKW = _.filter(arrDaysValues, function(arr){
			        return (arr.POINTID == objBreaker.kwextid && arr.MONTH == arrMonthsAmps[j].MONTH);
				});
				var arrMonthDaysHoursKW = _.filter(arrDaysHoursValues, function(arr){
			        return (arr.POINTID == objBreaker.kwextid && arr.MONTH == arrMonthsAmps[j].MONTH);
				});
				
				var arrMonthDaysKWH = _.filter(arrDaysValues, function(arr){
			        return (arr.POINTID == objBreaker.kwhextid && arr.MONTH == arrMonthsAmps[j].MONTH);
				});
				var arrMonthDaysHoursKWH = _.filter(arrDaysHoursValues, function(arr){
			        return (arr.POINTID == objBreaker.kwhextid && arr.MONTH == arrMonthsAmps[j].MONTH);
				});
				
				
				var arrDays = new Array();
				
				var ckwhadjtot = 0;
				var ckwhtot = 0;
				
				for ( var k = 0; arrMonthDaysAmps != null && k < arrMonthDaysAmps.length; k++ ) {
					var objDay = new Object();
					objDay["node"] = arrMonthDaysAmps[k].DAY;
					objDay["day"] = arrMonthDaysAmps[k].DAY;
					objDay["powerid"] = powerid;
					
					objDay["ampsintid"] = objBreaker.ampsintid;
					objDay["ampsextid"] = objBreaker.ampsextid;
					objDay["kwextid"] = objBreaker.kwextid;
					objDay["kwhextid"] = objBreaker.kwhextid;
					
					objDay["volts"] = parseInt(volts);
					
					var ampsmax = Math.max(0,arrMonthDaysAmps[k].MAX);
					var ampsmean = Math.max(0,arrMonthDaysAmps[k].MEAN);
					objDay["ampsmax"] = ampsmax;
					objDay["ampsmean"] = ampsmean;
					objDay["ampsminutes"] = arrMonthDaysAmps[k].MINUTES;
					objDay["ampsacumul"] = Math.max(0,arrMonthDaysAmps[k].ACUMUL);
					
					var objDayHoursAmps = _.find(arrMonthDaysHoursAmps, function(arr){ return (arr.DAY == arrMonthDaysAmps[k].DAY); });
					if(objDayHoursAmps != null){
						
						var hampssum = objDayHoursAmps.MEAN;
						var hampshours = objDayHoursAmps.HOURS;
						
						objDay["hampsmax"] = Math.max(0,objDayHoursAmps.MAX);
						objDay["hampssum"] = Math.max(0,objDayHoursAmps.MEAN);
						objDay["hampshours"] = objDayHoursAmps.HOURS;
						objDay["hampsminutes"] = objDayHoursAmps.MINUTES;
						objDay["hampsacumul"] = Math.max(0,objDayHoursAmps.ACUMUL);
						
						if(hampssum > 0){
							var ckwh = 0;
							if(voltage == '120V Single Phase' || voltage == '240V Single Phase' || voltage == '240V Three Phase'){
								ckwh = parseFloat(hampssum * volts / 1000);
							}
							if(voltage == '208V Single Phase'){
								ckwh = parseFloat(hampssum * volts / 2 / 1000);
							}
							if(voltage == '208V Three Phase'){
								ckwh = parseFloat(hampssum * Math.sqrt(3) * volts / 3 / 1000);
							}
						}
						ckwhtot += ckwh;
						objDay["ckwh"] = parseFloat(ckwh);
						var ckwhadj = 24 * ckwh / hampshours;
						ckwhadjtot += ckwhadj;
						objDay["ckwhadj"] = parseFloat(ckwhadj);
					}
					else{
						objDay["hampsmax"] = 0;
						objDay["hampssum"] = 0;
						objDay["hampshours"] = 0;
						objDay["hampsminutes"] = 0;
						objDay["hampsacumul"] = 0;
						objDay["ckwh"] = 0;
						objDay["ckwhadj"] = 0;
					}
					
					var objDayKW = _.find(arrMonthDaysKW, function(arr){ return (arr.DAY == arrMonthDaysAmps[k].DAY); });
					if(objDayKW != null){
						objDay["kwmax"] = Math.max(0,objDayKW.MAX);
						objDay["kwmean"] = Math.max(0,objDayKW.MEAN);
						objDay["kwminutes"] = objDayKW.MINUTES;
						objDay["kwacumul"] = Math.max(0,objDayKW.ACUMUL);
					}
					else{
						objDay["kwmax"] = 0;
						objDay["kwmean"] = 0;
						objDay["kwminutes"] = 0;
						objDay["kwacumul"] = 0;
					}
					
					if(objDayHoursAmps != null){
						
						var hampssum = objDayHoursAmps.MEAN;
						
						var ckwmax = 0;
						var ckwmean = 0;
						if(hampssum > 0){
							var ckwh = 0;
							if(voltage == '120V Single Phase' || voltage == '240V Single Phase' || voltage == '240V Three Phase'){
								ckwmax = parseFloat(ampsmax * volts / 1000);
								ckwmean = parseFloat(ampsmean * volts / 1000);
							}
							if(voltage == '208V Single Phase'){
								ckwmax = parseFloat(ampsmax * volts / 2 / 1000);
								ckwmean = parseFloat(ampsmean * volts / 2 / 1000);
							}
							if(voltage == '208V Three Phase'){
								ckwmax = parseFloat(ampsmax * Math.sqrt(3) * volts / 3 / 1000);
								ckwmean = parseFloat(ampsmean * Math.sqrt(3) * volts / 3 / 1000);
							}
						}
						objDay["ckwmax"] = ckwmax;
						objDay["ckwmaxfact"] = objDay.kwmax / ckwmax;
						objDay["ckwmean"] = ckwmean;
						objDay["ckwmeanfact"] = objDay.kwmean / ckwmean;
						objDay["ckwhadjfact"] = parseFloat(ckwhadj) * objDay.kwmean / ckwmean;
					}
					else{
						objDay["ckwmax"] = 0;
						objDay["ckwmaxfact"] = 0;
						objDay["ckwmean"] = 0;
						objDay["ckwmeanfact"] = 0;
						objDay["ckwhadjfact"] = 0;
					}
					
					var objDayHoursKW = _.find(arrMonthDaysHoursKW, function(arr){ return (arr.DAY == arrMonthDaysAmps[k].DAY); });
					if(objDayHoursKW != null){
						objDay["hkwmax"] = Math.max(0,objDayHoursKW.MAX);
						objDay["hkwmean"] = Math.max(0,objDayHoursKW.MEAN);
						objDay["hkwminutes"] = objDayHoursKW.MINUTES;
						objDay["hkwacumul"] = Math.max(0,objDayHoursKW.ACUMUL);
					}
					else{
						objDay["hkwmax"] = 0;
						objDay["hkwmean"] = 0;
						objDay["hkwminutes"] = 0;
						objDay["hkwacumul"] = 0;
					}
					
					
					
					var objDayKWH = _.find(arrMonthDaysKWH, function(arr){ return (arr.DAY == arrMonthDaysAmps[k].DAY); });
					if(objDayKWH != null){
						objDay["kwhmax"] = Math.max(0,objDayKWH.MAX);
						objDay["kwhmean"] = Math.max(0,objDayKWH.MEAN);
						objDay["kwhminutes"] = objDayKWH.MINUTES;
						objDay["kwhacumul"] = Math.max(0,objDayKWH.ACUMUL);
						objDay["kwhdiffmax"] = Math.max(0,objDayKWH.MAX) - previouskwhmax;
						previouskwhmax = Math.max(0,objDayKWH.MAX);
						objDay["kwhdiffmean"] = Math.max(0,objDayKWH.MEAN) - previouskwhmean;
						previouskwhmean = Math.max(0,objDayKWH.MEAN);
					}
					else{
						objDay["kwhmax"] = 0;
						objDay["kwhmean"] = 0;
						objDay["kwhminutes"] = 0;
						objDay["kwhacumul"] = 0;
						objDay["kwhdiffmax"] = 0;
						objDay["kwhdiffmean"] = 0;
					}
					
					var objDayHoursKWH = _.find(arrMonthDaysHoursKWH, function(arr){ return (arr.DAY == arrMonthDaysAmps[k].DAY); });
					if(objDayHoursKWH != null){
						objDay["hkwhmax"] = Math.max(0,objDayHoursKWH.MAX);
						objDay["hkwhmean"] = Math.max(0,objDayHoursKWH.MEAN);
						objDay["hkwhminutes"] = objDayHoursKWH.MINUTES;
						objDay["hkwhacumul"] = Math.max(0,objDayHoursKWH.ACUMUL);
						
					}
					else{
						objDay["hkwhmax"] = 0;
						objDay["hkwhmean"] = 0;
						objDay["hkwhminutes"] = 0;
						objDay["hkwhacumul"] = 0;
					}
					objDay["iconCls"] = 'day';
					objDay["leaf"] = true;

					arrDays.push(objDay);
				}
				objMonth["ckwh"] = parseFloat(ckwhtot);
				objMonth["ckwhadj"] = parseFloat(ckwhadjtot);
				
				objMonth["children"] = arrDays;
				arrMonths.push(objMonth);
			}
			objBreaker["children"] = arrMonths;

			
			var arrMonthsKW = _.filter(arrMonths, function(arr){
		        return (arr.POINTID == arrPointsAmps[i].extid);
			});
			var arrMonthsAmps = _.filter(arrMonths, function(arr){
		        return (arr.POINTID == arrPointsAmps[i].extid);
			});
		
			
			arrBreakers.push(objBreaker);
		}
		
		objPower["children"] = arrBreakers;
		
		var objFile = nlapiLoadFile(1529231);
		html = objFile.getValue();
		html = html.replace(new RegExp('{treeJSON}','g'), JSON.stringify(objPower));
		response.write( html );

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
