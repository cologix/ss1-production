nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_Day_Capacity_1.js
//	Script Name:	CLGX_SS_DCIM_PWR_Day_Capacity_1
//	Script Id:		customscript_clgx_ss_dcim_pwr_day_cap_1
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		06/12/2018
//-------------------------------------------------------------------------------------------------

function scheduled_dcim_pwr_day_capacity_1(){
    try{
    	var context = nlapiGetContext();
        var environment = context.getEnvironment();
        
        if(environment == 'PRODUCTION'){
        	
        	update_devices_capacity_sums_amps ('panel');
    		var usage = 10000 - parseInt(context.getRemainingUsage());
            nlapiLogExecution('DEBUG','Results ', ' | update_devices_capacity_sums_amps = ' + usage  + '  |');
            
    		//update_devices_capacity_sums_amps ('ups');
    		//update_devices_capacity_sums_amps ('generator');
    		
            var status = nlapiScheduleScript('customscript_clgx_ss_dcim_pwr_day_cap_2', 'customdeploy_clgx_ss_dcim_pwr_day_cap_2' ,null);
        	
        }
    }
    catch (error){
    	
    	var str = String(error);
        if (str.match('SSS_UNKNOWN_HOST') || str.match('SSS_INVALID_HOST_CERT') || str.match('SSS_CONNECTION_CLOSED') || str.match('SSS_REQUEST_TIME_EXCEEDED') || str.match('SSS_CONNECTION_TIME_OUT') || str.match('UNEXPECTED_ERROR') || str.match('Unexpected token')) {
        	var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
        	nlapiLogExecution('DEBUG','End', 'Finished powers processing because of error and reschedule it.');
        }
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
        	if (str.match('TypeError: Cannot read property "phases" from undefined')) {
        		var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
        		//nlapiLogExecution('ERROR','Unexpected Error', error.toString());
                //throw nlapiCreateError('99999', error.toString());
        	} else {
                nlapiLogExecution('ERROR','Unexpected Error', error.toString());
                throw nlapiCreateError('99999', error.toString());
        	}
        }
    }
}
