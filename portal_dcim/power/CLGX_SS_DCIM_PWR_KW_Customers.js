nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_KW_Customers.js
//	Script Name:	CLGX_SS_DCIM_PWR_KW_Customers
//	Script Id:		customscript_clgx_ss_dcim_pwr_kw_cust
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		02/17/2015
//-------------------------------------------------------------------------------------------------

function scheduled_dcim_pwr_kw_cust(){

    try{

        var today = moment().format('M/D/YYYY');
        var yesterday = moment().subtract('days',1).format('M/D/YYYY');
        var fromid = 432742;

        var objFile = nlapiLoadFile(2146675);
        var arrCustomers = JSON.parse(objFile.getValue());

        for ( var i = 0; arrCustomers != null && i < arrCustomers.length; i++ ) {
            //Cat 1/10/20 DONT SEND NOTIFICATIONS to MTL9 AND MTL10
            var arrColumns = new Array();
            if( arrCustomers[i].facilityid!=52) {
                arrColumns.push(new nlobjSearchColumn('internalid', null, null));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_kw', null, null));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_kw_max', null, null));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_date', null, null));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_file', null, null));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_email', null, null));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_customer", null, "anyof", arrCustomers[i].customerid));
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_facility", null, "anyof", arrCustomers[i].facilityid));
                var searchCustomer = nlapiSearchRecord('customrecord_clgx_dcim_customer_peak', null, arrFilters, arrColumns);

                if (searchCustomer != null) {
                    arrCustomers[i]["id"] = parseInt(searchCustomer[0].getValue('internalid', null, null));
                    arrCustomers[i]["peak"] = parseFloat(searchCustomer[0].getValue('custrecord_clgx_dcim_peak_kw', null, null));
                    arrCustomers[i]["contract"] = parseFloat(searchCustomer[0].getValue('custrecord_clgx_dcim_peak_kw_max', null, null));
                    arrCustomers[i]["date"] = searchCustomer[0].getValue('custrecord_clgx_dcim_peak_date', null, null);
                    arrCustomers[i]["file"] = parseInt(searchCustomer[0].getValue('custrecord_clgx_dcim_peak_file', null, null));
                    arrCustomers[i]["email"] = searchCustomer[0].getValue('custrecord_clgx_dcim_peak_email', null, null);
                } else {
                    arrCustomers[i]["id"] = 0;
                    arrCustomers[i]["peak"] = 0;
                    arrCustomers[i]["contract"] = 0;
                    arrCustomers[i]["date"] = '';
                    arrCustomers[i]["file"] = 0;
                    arrCustomers[i]["email"] = '';
                }
            }else{

                arrCustomers[i]["id"] = 0;
                arrCustomers[i]["peak"] = 0;
                arrCustomers[i]["contract"] = 0;
                arrCustomers[i]["date"] = '';
                arrCustomers[i]["file"] = 0;
                arrCustomers[i]["email"] = '';
            }
            //Liz 6/20/19 COMMENT OUT FOR PROD
            //nlapiLogExecution('DEBUG','facilityid', arrCustomers[i].facilityid);
            //arrCustomers[i]["opsreps"] = get_ops_reps(arrCustomers[i].facilityid);

            var recCustomer = nlapiLoadRecord('customer',arrCustomers[i].customerid);
            arrCustomers[i]["salesrepid"] = parseInt(recCustomer.getFieldValue('salesrep'));
            arrCustomers[i]["salesrep"] = recCustomer.getFieldText('salesrep');
            arrCustomers[i]["salesrepemail"] = nlapiLookupField('employee', parseInt(recCustomer.getFieldValue('salesrep')), 'email');
        }

        var arrOutages = _.filter(arrCustomers, function(arr){
            return (arr.peak > arr.contract && arr.email == 'F' && Math.ceil(moment().diff(moment(arr.date), 'days', true)) < 6);
        });


        //send emails ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        var emailAdminSubject = 'ADMIN - Cologix Power Notice - Peak Exceeded for ' + yesterday;
        var emailAdminBody = '';
        emailAdminBody += '<br><br><table border="1" cellpadding="5">';
        emailAdminBody += '<tr><td>Customer</td><td>Contract<td>Peak</td></tr>';

        for ( var i = 0; arrOutages != null && i < arrOutages.length; i++ ) {

            emailAdminBody += '<tr><td>' + arrOutages[i].customer + '</td><td>' + arrOutages[i].contract + '<td>' + arrOutages[i].peak + '</td></tr>';

            var mtl = 0;
            if(arrOutages[i].facilityid == 2 || arrOutages[i].facilityid == 4 || arrOutages[i].facilityid == 5 || arrOutages[i].facilityid == 6 || arrOutages[i].facilityid == 7 || arrOutages[i].facilityid == 9 || arrOutages[i].facilityid == 19){
                mtl = 1;
            }
            if(mtl == 1){
                var emailCustomerSubject = 'Cologix - Avis de dépassement de la demande de pointe en kilowatt pour / Power Notice - kW Peak Exceeded for - ' + arrOutages[i].customer + ' - ' + yesterday;
                var objFile = nlapiLoadFile(2180578);
            }
            else{
                var emailCustomerSubject = 'Cologix Power Notice - kW Peak Exceeded for - ' + arrOutages[i].customer + ' - ' + yesterday;
                var objFile = nlapiLoadFile(2165436);
            }
            var emailCustomerBody = objFile.getValue();


            var strSalesRep = arrOutages[i].salesrep + ' (' + arrOutages[i].salesrepemail + ')';
            emailCustomerBody = emailCustomerBody.replace(new RegExp('{salesrep}','g'), strSalesRep);
            emailCustomerBody = emailCustomerBody.replace(new RegExp('{contractPeak}','g'), arrOutages[i].contract);
            emailCustomerBody = emailCustomerBody.replace(new RegExp('{currentPeak}','g'), arrOutages[i].peak);
            var arrFiltersContacts=new Array();
            var arrColumnsContacts=new Array();
            arrFiltersContacts.push(new nlobjSearchFilter("internalid", "company", "anyof",arrOutages[i].customerid));
            var searchContactsOutage = nlapiSearchRecord('contact', "customsearch_clgx_dcim_outage_notif_con", arrFiltersContacts, arrColumnsContacts);

            if(searchContactsOutage!=null) {
                var arrContactIDsSearch=new Array();
                var arrContactIDsName=new Array();
                var arrContactEmailsSearch=new Array();

                for ( var m = 0;searchContactsOutage != null && m < searchContactsOutage.length; m++ ) {
                    var searchCnt = searchContactsOutage[m];
                    var columns = searchCnt.getAllColumns();
                    arrContactIDsSearch.push(searchCnt.getValue(columns[0]));
                    arrContactIDsName.push(searchCnt.getText(columns[0]));
                    arrContactEmailsSearch.push(searchCnt.getText(columns[3]));
                }

                arrContactIDsSearch=_.uniq(arrContactIDsSearch);
                arrContactEmailsSearch=_.uniq(arrContactEmailsSearch);
                arrContactIDsName=_.uniq(arrContactIDsName);
                // send email to customer contacts
                for ( var r = 0; arrContactIDsSearch != null && r < arrContactIDsSearch.length; r++ ) {
                    nlapiSendEmail(fromid, arrContactIDsSearch[r], emailCustomerSubject, emailCustomerBody, null, null, null, null,true);
                }

            } else {
                // send email to customer contacts
                var arrContacts = arrOutages[i].contacts;
                for ( var j = 0; arrContacts != null && j < arrContacts.length; j++ ) {
                    nlapiSendEmail(fromid, arrContacts[j].contactid, emailCustomerSubject, emailCustomerBody, null, null, null, null,true);
                }
            }


            /*
            var strSalesRep = arrOutages[i].salesrep + ' (' + arrOutages[i].salesrepemail + ')';
            emailCustomerBody = emailCustomerBody.replace(new RegExp('{salesrep}','g'), strSalesRep);
            emailCustomerBody = emailCustomerBody.replace(new RegExp('{contractPeak}','g'), arrOutages[i].contract);
            emailCustomerBody = emailCustomerBody.replace(new RegExp('{currentPeak}','g'), arrOutages[i].peak);
            var arrFiltersContacts=new Array();
            var arrColumnsContacts=new Array();
            arrFiltersContacts.push(new nlobjSearchFilter("internalid", "company", "anyof",arrOutages[i].customerid));
            var searchTechContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_tecrole", arrFiltersContacts, arrColumnsContacts);
            //var searchTechContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_notif", arrFiltersContacts, arrColumnsContacts);
            var searchPrimaryContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_prrole", arrFiltersContacts, arrColumnsContacts);
            var searchSecondaryContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_secrole", arrFiltersContacts, arrColumnsContacts);
            var searchDecisionMaker = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_dmrole", arrFiltersContacts, arrColumnsContacts);
            //  var searchOpsOutage = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_ops", arrFiltersContacts, arrColumnsContacts);
            //search for Primary/Secondary/Tech Contacts
            if((searchTechContact!=null)||(searchPrimaryContact!=null)||(searchSecondaryContact!=null)||(searchDecisionMaker!=null))
            {
                var arrContactIDsSearch=new Array();
                var arrContactIDsName=new Array();
                var arrContactEmailsSearch=new Array();

                for ( var m = 0;searchTechContact != null && m < searchTechContact.length; m++ ) {
                    var searchTCnt = searchTechContact[m];
                    var columns = searchTCnt.getAllColumns();
                    arrContactIDsSearch.push(searchTCnt.getValue(columns[0]));
                    arrContactIDsName.push(searchTCnt.getText(columns[0]));
                    arrContactEmailsSearch.push(searchTCnt.getText(columns[3]));
                }
                for ( var m = 0;searchPrimaryContact != null && m < searchPrimaryContact.length; m++ ) {
                    var searchPCnt = searchPrimaryContact[m];
                    var columns = searchPCnt.getAllColumns();
                    arrContactIDsSearch.push(searchPCnt.getValue(columns[0]));
                    arrContactIDsName.push(searchPCnt.getText(columns[0]));
                    arrContactEmailsSearch.push(searchPCnt.getText(columns[3]));
                }
                for ( var m = 0;searchSecondaryContact != null && m < searchSecondaryContact.length; m++ ) {
                    var searchSCnt = searchSecondaryContact[m];
                    var columns = searchSCnt.getAllColumns();
                    arrContactIDsSearch.push(searchSCnt.getValue(columns[0]));
                    arrContactIDsName.push(searchSCnt.getText(columns[0]));
                    arrContactEmailsSearch.push(searchSCnt.getText(columns[3]));
                }
                for ( var m = 0;searchDecisionMaker!= null && m <searchDecisionMaker.length; m++ ) {
                    var searchDCnt = searchDecisionMaker[m];
                    var columns = searchDCnt.getAllColumns();
                    arrContactIDsSearch.push(searchDCnt.getValue(columns[0]));
                    arrContactIDsName.push(searchDCnt.getText(columns[0]));
                    arrContactEmailsSearch.push(searchDCnt.getText(columns[3]));
                }
                arrContactIDsSearch=_.uniq(arrContactIDsSearch);
                arrContactEmailsSearch=_.uniq(arrContactEmailsSearch);
                arrContactIDsName=_.uniq(arrContactIDsName);
                // send email to customer contacts
                for ( var r = 0; arrContactIDsSearch != null && r < arrContactIDsSearch.length; r++ ) {
                	nlapiSendEmail(fromid, arrContactIDsSearch[r], emailCustomerSubject, emailCustomerBody, null, null, null, null,true);
                }

            }
            else{
                // send email to customer contacts
                var arrContacts = arrOutages[i].contacts;
                for ( var j = 0; arrContacts != null && j < arrContacts.length; j++ ) {
                    nlapiSendEmail(fromid, arrContacts[j].contactid, emailCustomerSubject, emailCustomerBody, null, null, null, null,true);
                }
            }
            */

            // flag customers here - email sent
            var fields = ['custrecord_clgx_dcim_peak_email','custrecord_clgx_dcim_peak_email_date'];
            var values = ['T',today];
            nlapiSubmitField('customrecord_clgx_dcim_customer_peak', arrOutages[i].id, fields, values);

            //Liz 6/20/19 COMMENT OUT FOR PROD
            // send email to ops reps
            // var arrOpsReps = arrOutages[i].opsreps;
            //nlapiLogExecution('DEBUG','debug', '| arrOpsReps = ' + JSON.stringify( arrOpsReps ) + ' | ');
            //  for ( var j = 0; arrOpsReps != null && j < arrOpsReps.length; j++ ) {
            //      nlapiSendEmail(fromid, arrOpsReps[j].id, emailCustomerSubject, emailCustomerBody, null, null, null, null,true);
            //     nlapiLogExecution('ERROR','Sent email to ', arrOpsReps[j].id);
            //  }

            // send email to sales rep
            nlapiSendEmail(fromid,arrOutages[i].salesrepid, emailCustomerSubject, emailCustomerBody, null, null, null, null,true);

            clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_alerts", emailCustomerSubject, emailCustomerBody);

            //Liz 6/20/19 ADD TO PROD BEGIN
            //Product Emails
            clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_ccm_mtl_2", emailCustomerSubject, emailCustomerBody);
            //CCM Emails
            if(mtl == 1){
                clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_ccm_mtl", emailCustomerSubject, emailCustomerBody);
            }
            else{
                clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_ccm_nonmtl", emailCustomerSubject, emailCustomerBody);
            }
            //Liz 6/20/19 ADD TO PROD END

            /*nlapiSendEmail(fromid, 211645, emailCustomerSubject, emailCustomerBody, null, null, null, null,true); // Charles
            nlapiSendEmail(fromid, 71418, emailCustomerSubject, emailCustomerBody, null, null, null, null,true); // Dan
            nlapiSendEmail(fromid, 206211, emailCustomerSubject, emailCustomerBody, null, null, null, null,true);*/ // Catalina
        }
        // admin emails
        emailAdminBody += '</table><br><br>';
        if(arrOutages != null){
            clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_alerts", emailAdminSubject, emailAdminBody);

            /*nlapiSendEmail(fromid, 71418, emailAdminSubject, emailAdminBody, null, null, null, null,true); // Dan
            nlapiSendEmail(fromid, 206211, emailAdminSubject, emailAdminBody, null, null, null, null,true); // Catalina
            nlapiSendEmail(fromid,13091,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Phil
            nlapiSendEmail(fromid,-5,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Phil
            nlapiSendEmail(fromid,294,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Jason
            nlapiSendEmail(fromid,211645,emailAdminSubject,emailAdminBody,null,null,null,null,true);*/ // Charles
        }
        else{
            clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_admin", "No KW outages today", "");
            //nlapiSendEmail(fromid, 71418, 'No KW outages today', '', null, null, null, null,true); // Dan
        }

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}



//Liz 6/20/19 COMMENT OUT FOR PROD
/*function get_ops_reps (arrFacilitiesIDs){

    var arrOpsReps = new Array();
    for ( var i = 0; i < arrFacilitiesIDs.length; i++ ) {

        if(arrFacilitiesIDs[i] == 2 || arrFacilitiesIDs[i] == 4 || arrFacilitiesIDs[i] == 5 || arrFacilitiesIDs[i] == 6 || arrFacilitiesIDs[i] == 7 || arrFacilitiesIDs[i] == 9 || arrFacilitiesIDs[i] == 19){
        	try {
        			arrOpsReps = clgx_get_employee_list_from_savedsearch("customsearch_clgxe_dcim_ccm_mtl");
        	} catch(ex) {
        			nlapiLogExecution("DEBUG", "get_ops_reps - KWH Day - MTL", ex);
        	}
            var objOpsRep = new Object();
            objOpsRep["opid"] = 212518;
            objOpsRep["op"] = 'Amy J Connell';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 168983;
            objOpsRep["op"] = 'Donato Di Rienzo';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 3118;
            objOpsRep["op"] = 'Sorin Arion';
            arrOpsReps.push(objOpsRep);

        }

        else{
        	try {
        		arrOpsReps = clgx_get_employee_list_from_savedsearch("customsearch_clgxe_dcim_ccm_nonmtl");
        	} catch(ex) {
        		nlapiLogExecution("DEBUG", "get_ops_reps - KWH Day - NonMTL", ex);
        	}


            var objOpsRep = new Object();
            objOpsRep["opid"] = 212518;
            objOpsRep["op"] = 'Amy J Connell';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 1034604;
            objOpsRep["op"] = 'Yazmine Myers';
            arr.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 9735;
            objOpsRep["op"] = 'Pauli Guild';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 8795;
            objOpsRep["op"] = 'Mario Novello';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 1552824;
            objOpsRep["op"] = 'Kathy Segui';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 600794;
            objOpsRep["op"]   = "Bryan Moore";
            arr.push(objOpsRep);
        }
    }
    return arrOpsReps;
}*/

