nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_Failover_Devices.js
//	Script Name:	CLGX_SS_DCIM_PWR_Failover_Devices
//	Script Id:		customscript_clgx_ss_dcim_fail_devs
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		10/13/2015
//-------------------------------------------------------------------------------------------------
function scheduled_clgx_ss_dcim_fail_devs(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        var context = nlapiGetContext();
        var environment = context.getEnvironment()
    	
    		if(environment == 'PRODUCTION'){

			var date = new Date();
			var startScript = moment();
	        var emailAdminSubject = 'OD 4.3 - Finish Trees failover';
			var emailAdminBody = '';
			emailAdminBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');

			var i = 0;
			var arrDevices = new Array();
	    		var searchDevices = nlapiLoadSearch('customrecord_clgx_dcim_devices', 'customsearch_clgx_dcim_capacity_failover');
			var resultSet = searchDevices.runSearch();
			resultSet.forEachResult(function(searchResult) {
				
				arrDevices.push({
						"index"          : i,
						"parentid"       : parseInt(searchResult.getValue('custrecord_clgx_dcim_device_parent',null,null)),
						"parent"         : searchResult.getText('custrecord_clgx_dcim_device_parent',null,null),
						"deviceid"       : parseInt(searchResult.getValue('internalid',null,null)),
						"device"         : searchResult.getValue('name',null,null),
						"powers"         : searchResult.getValue('custrecord_clgx_dcim_device_has_powers',null,null),
						
						"kwsp"           : parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_day_kw_p',null,null)),
						"kwsa"           : parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_day_kw_a',null,null)),
						"kwsab"          : parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_day_kw_ab',null,null)),
						
						"ampsp_a"        : parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_day_amp_p_a',null,null)),
						"ampsa_a"        : parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_day_amp_a_a',null,null)),
						"ampsab_a"       : parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_day_amp_ab_a',null,null)),
						
						"ampsp_b"        : parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_day_amp_p_b',null,null)),
						"ampsa_b"        : parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_day_amp_a_b',null,null)),
						"ampsab_b"       : parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_day_amp_ab_b',null,null)),
						
						"ampsp_c"        : parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_day_amp_p_c',null,null)),
						"ampsa_c"        : parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_day_amp_a_c',null,null)),
						"ampsab_c"       : parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_day_amp_ab_c',null,null)),
						
						"children"       : false
				});	
				i = i + 1;
				return true;
			});

			
		/*
	    	var arrColumns = new Array();
	    	var arrFilters = new Array();
	    	var searchDevices = nlapiSearchRecord('customrecord_clgx_dcim_devices', 'customsearch_clgx_dcim_capacity_failover', arrFilters, arrColumns);
	    	var arrDevices = new Array();
	    	for ( var i = 0; searchDevices != null && i < searchDevices.length; i++ ) {
	    		
	    		var objDevice = new Object();
	    		
	    		objDevice["index"] = i;
	    		objDevice["parentid"] = parseInt(searchDevices[i].getValue('custrecord_clgx_dcim_device_parent',null,null));
	    		objDevice["parent"] = searchDevices[i].getText('custrecord_clgx_dcim_device_parent',null,null);
	    		objDevice["deviceid"] = parseInt(searchDevices[i].getValue('internalid',null,null));
	    		objDevice["device"] = searchDevices[i].getValue('name',null,null);
	    		objDevice["powers"] = searchDevices[i].getValue('custrecord_clgx_dcim_device_has_powers',null,null);

	    		objDevice["kwsp"] = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_device_day_kw_p',null,null));
	    		objDevice["kwsa"] = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_device_day_kw_a',null,null));
	    		objDevice["kwsab"] = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_device_day_kw_ab',null,null));
	    		
	    		objDevice["ampsp_a"] = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_device_day_amp_p_a',null,null));
	    		objDevice["ampsa_a"] = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_device_day_amp_a_a',null,null));
	    		objDevice["ampsab_a"] = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_device_day_amp_ab_a',null,null));
	    		
	    		objDevice["ampsp_b"] = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_device_day_amp_p_b',null,null));
	    		objDevice["ampsa_b"] = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_device_day_amp_a_b',null,null));
	    		objDevice["ampsab_b"] = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_device_day_amp_ab_b',null,null));
	    		
	    		objDevice["ampsp_c"] = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_device_day_amp_p_c',null,null));
	    		objDevice["ampsa_c"] = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_device_day_amp_a_c',null,null));
	    		objDevice["ampsab_c"] = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_device_day_amp_ab_c',null,null));

	    		objDevice["children"] = false;
	    		
	    		arrDevices.push(objDevice);
	    	}
	    	*/
		
		for ( var i = 0; arrDevices != null && i < arrDevices.length; i++ ) {
	    		var arrChildren = _.filter(arrDevices, function(arr){
	    	        return (arr.parentid == arrDevices[i].deviceid);
	    		});
	    		if(arrChildren.length > 0){
	    			arrDevices[i].children = true;
	    		}
	    	}

	    	var arrNoChildren = _.filter(arrDevices, function(arr){
	            return (arr.children == false);
	    	});
	
	    	for ( var i = 0; arrNoChildren != null && i < arrNoChildren.length; i++ ) {
	    		
	    		var objParent1 = _.find(arrDevices, function(arr){ return (arr.deviceid == arrNoChildren[i].parentid); });
	    		if(objParent1 != null){
	    			
	    			arrDevices[objParent1.index].kwsp += arrNoChildren[i].kwsp;
	    			arrDevices[objParent1.index].kwsa += arrNoChildren[i].kwsa;
	    			arrDevices[objParent1.index].kwsab += arrNoChildren[i].kwsab;
	    			
				arrDevices[objParent1.index].ampsp_a += arrNoChildren[i].ampsp_a;
        			arrDevices[objParent1.index].ampsa_a += arrNoChildren[i].ampsa_a;
        			arrDevices[objParent1.index].ampsab_a += arrNoChildren[i].ampsab_a;
        			
    				arrDevices[objParent1.index].ampsp_b += arrNoChildren[i].ampsp_b;
        			arrDevices[objParent1.index].ampsa_b += arrNoChildren[i].ampsa_b;
        			arrDevices[objParent1.index].ampsab_b += arrNoChildren[i].ampsab_b;
        			
    				arrDevices[objParent1.index].ampsp_c += arrNoChildren[i].ampsp_c;
        			arrDevices[objParent1.index].ampsa_c += arrNoChildren[i].ampsa_c;
        			arrDevices[objParent1.index].ampsab_c += arrNoChildren[i].ampsab_c;
        			
				var objParent2 = _.find(arrDevices, function(arr){ return (arr.deviceid == objParent1.parentid); });
	    			if(objParent2 != null){
	        			
	        			arrDevices[objParent2.index].kwsp += arrNoChildren[i].kwsp;
	        			arrDevices[objParent2.index].kwsa += arrNoChildren[i].kwsa;
	        			arrDevices[objParent2.index].kwsab += arrNoChildren[i].kwsab;
	        			
	    				arrDevices[objParent2.index].ampsp_a += arrNoChildren[i].ampsp_a;
	        			arrDevices[objParent2.index].ampsa_a += arrNoChildren[i].ampsa_a;
	        			arrDevices[objParent2.index].ampsab_a += arrNoChildren[i].ampsab_a;
	        			
	    				arrDevices[objParent2.index].ampsp_b += arrNoChildren[i].ampsp_b;
	        			arrDevices[objParent2.index].ampsa_b += arrNoChildren[i].ampsa_b;
	        			arrDevices[objParent2.index].ampsab_b += arrNoChildren[i].ampsab_b;
	        			
	    				arrDevices[objParent2.index].ampsp_c += arrNoChildren[i].ampsp_c;
	        			arrDevices[objParent2.index].ampsa_c += arrNoChildren[i].ampsa_c;
	        			arrDevices[objParent2.index].ampsab_c += arrNoChildren[i].ampsab_c;

	    				var objParent3 = _.find(arrDevices, function(arr){ return (arr.deviceid == objParent2.parentid); });
	    				if(objParent3 != null){
	            			
	            			arrDevices[objParent3.index].kwsp += arrNoChildren[i].kwsp;
	            			arrDevices[objParent3.index].kwsa += arrNoChildren[i].kwsa;
	            			arrDevices[objParent3.index].kwsab += arrNoChildren[i].kwsab;
	            			
	        				arrDevices[objParent3.index].ampsp_a += arrNoChildren[i].ampsp_a;
	            			arrDevices[objParent3.index].ampsa_a += arrNoChildren[i].ampsa_a;
	            			arrDevices[objParent3.index].ampsab_a += arrNoChildren[i].ampsab_a;
		        			
		    				arrDevices[objParent3.index].ampsp_b += arrNoChildren[i].ampsp_b;
		        			arrDevices[objParent3.index].ampsa_b += arrNoChildren[i].ampsa_b;
		        			arrDevices[objParent3.index].ampsab_b += arrNoChildren[i].ampsab_b;
		        			
		    				arrDevices[objParent3.index].ampsp_c += arrNoChildren[i].ampsp_c;
		        			arrDevices[objParent3.index].ampsa_c += arrNoChildren[i].ampsa_c;
		        			arrDevices[objParent3.index].ampsab_c += arrNoChildren[i].ampsab_c;

	    					var objParent4 = _.find(arrDevices, function(arr){ return (arr.deviceid == objParent3.parentid); });
	    					if(objParent4 != null && objParent4 != undefined){
	                			
	                			arrDevices[objParent4.index].kwsp += arrNoChildren[i].kwsp;
	                			arrDevices[objParent4.index].kwsa += arrNoChildren[i].kwsa;
	                			arrDevices[objParent4.index].kwsab += arrNoChildren[i].kwsab;
	                			
	            				arrDevices[objParent4.index].ampsp_a += arrNoChildren[i].ampsp_a;
	                			arrDevices[objParent4.index].ampsa_a += arrNoChildren[i].ampsa_a;
	                			arrDevices[objParent4.index].ampsab_a += arrNoChildren[i].ampsab_a;
	    	        			
		    	    				arrDevices[objParent4.index].ampsp_b += arrNoChildren[i].ampsp_b;
		    	        			arrDevices[objParent4.index].ampsa_b += arrNoChildren[i].ampsa_b;
		    	        			arrDevices[objParent4.index].ampsab_b += arrNoChildren[i].ampsab_b;
		    	        			
		    	    				arrDevices[objParent4.index].ampsp_c += arrNoChildren[i].ampsp_c;
		    	        			arrDevices[objParent4.index].ampsa_c += arrNoChildren[i].ampsa_c;
		    	        			arrDevices[objParent4.index].ampsab_c += arrNoChildren[i].ampsab_c;

	    						var objParent5 = _.find(arrDevices, function(arr){ return (arr.deviceid == objParent4.parentid); });
	    						if(objParent5 != null){
	                    			
	                    			arrDevices[objParent5.index].kwsp += arrNoChildren[i].kwsp;
	                    			arrDevices[objParent5.index].kwsa += arrNoChildren[i].kwsa;
	                    			arrDevices[objParent5.index].kwsab += arrNoChildren[i].kwsab;
	                    			
	                				arrDevices[objParent5.index].ampsp_a += arrNoChildren[i].ampsp_a;
	                    			arrDevices[objParent5.index].ampsa_a += arrNoChildren[i].ampsa_a;
	                    			arrDevices[objParent5.index].ampsab_a += arrNoChildren[i].ampsab_a;
	        	        			
		        	    				arrDevices[objParent5.index].ampsp_b += arrNoChildren[i].ampsp_b;
		        	        			arrDevices[objParent5.index].ampsa_b += arrNoChildren[i].ampsa_b;
		        	        			arrDevices[objParent5.index].ampsab_b += arrNoChildren[i].ampsab_b;
		        	        			
		        	    				arrDevices[objParent5.index].ampsp_c += arrNoChildren[i].ampsp_c;
		        	        			arrDevices[objParent5.index].ampsa_c += arrNoChildren[i].ampsa_c;
		        	        			arrDevices[objParent5.index].ampsab_c += arrNoChildren[i].ampsab_c;

	    							var objParent6 = _.find(arrDevices, function(arr){ return (arr.deviceid == objParent5.parentid); });
	    							if(objParent6 != null){
	                        			
	                        			arrDevices[objParent6.index].kwsp += arrNoChildren[i].kwsp;
	                        			arrDevices[objParent6.index].kwsa += arrNoChildren[i].kwsa;
	                        			arrDevices[objParent6.index].kwsab += arrNoChildren[i].kwsab;
	                        			
	                    				arrDevices[objParent6.index].ampsp_a += arrNoChildren[i].ampsp_a;
	                        			arrDevices[objParent6.index].ampsa_a += arrNoChildren[i].ampsa_a;
	                        			arrDevices[objParent6.index].ampsab_a += arrNoChildren[i].ampsab_a;
	            	        			
		            	    				arrDevices[objParent6.index].ampsp_b += arrNoChildren[i].ampsp_b;
		            	        			arrDevices[objParent6.index].ampsa_b += arrNoChildren[i].ampsa_b;
		            	        			arrDevices[objParent6.index].ampsab_b += arrNoChildren[i].ampsab_b;
		            	        			
		            	    				arrDevices[objParent6.index].ampsp_c += arrNoChildren[i].ampsp_c;
		            	        			arrDevices[objParent6.index].ampsa_c += arrNoChildren[i].ampsa_c;
		            	        			arrDevices[objParent6.index].ampsab_c += arrNoChildren[i].ampsab_c;

	    								var objParent7 = _.find(arrDevices, function(arr){ return (arr.deviceid == objParent6.parentid); });
	    								if(objParent7 != null){
	                            			
	                            			arrDevices[objParent7.index].kwsp += arrNoChildren[i].kwsp;
	                            			arrDevices[objParent7.index].kwsa += arrNoChildren[i].kwsa;
	                            			arrDevices[objParent7.index].kwsab += arrNoChildren[i].kwsab;
	                            			
	                        				arrDevices[objParent7.index].ampsp_a += arrNoChildren[i].ampsp_a;
	                            			arrDevices[objParent7.index].ampsa_a += arrNoChildren[i].ampsa_a;
	                            			arrDevices[objParent7.index].ampsab_a += arrNoChildren[i].ampsab_a;
	                	        			
		                	    				arrDevices[objParent7.index].ampsp_b += arrNoChildren[i].ampsp_b;
		                	        			arrDevices[objParent7.index].ampsa_b += arrNoChildren[i].ampsa_b;
		                	        			arrDevices[objParent7.index].ampsab_b += arrNoChildren[i].ampsab_b;
		                	        			
		                	    				arrDevices[objParent7.index].ampsp_c += arrNoChildren[i].ampsp_c;
		                	        			arrDevices[objParent7.index].ampsa_c += arrNoChildren[i].ampsa_c;
		                	        			arrDevices[objParent7.index].ampsab_c += arrNoChildren[i].ampsab_c;

	    									var objParent8 = _.find(arrDevices, function(arr){ return (arr.deviceid == objParent7.parentid); });
	    									if(objParent8 != null){
	                                			
	                                			arrDevices[objParent8.index].kwsp += arrNoChildren[i].kwsp;
	                                			arrDevices[objParent8.index].kwsa += arrNoChildren[i].kwsa;
	                                			arrDevices[objParent8.index].kwsab += arrNoChildren[i].kwsab;
	                                			
	                            				arrDevices[objParent8.index].ampsp_a += arrNoChildren[i].ampsp_a;
	                                			arrDevices[objParent8.index].ampsa_a += arrNoChildren[i].ampsa_a;
	                                			arrDevices[objParent8.index].ampsab_a += arrNoChildren[i].ampsab_a;
	                    	        			
		                    	    				arrDevices[objParent8.index].ampsp_b += arrNoChildren[i].ampsp_b;
		                    	        			arrDevices[objParent8.index].ampsa_b += arrNoChildren[i].ampsa_b;
		                    	        			arrDevices[objParent8.index].ampsab_b += arrNoChildren[i].ampsab_b;
		                    	        			
		                    	    				arrDevices[objParent8.index].ampsp_c += arrNoChildren[i].ampsp_c;
		                    	        			arrDevices[objParent8.index].ampsa_c += arrNoChildren[i].ampsa_c;
		                    	        			arrDevices[objParent8.index].ampsab_c += arrNoChildren[i].ampsab_c;

	    										// add more levels up here
	    										
	    									}
	    								}
	    							}
	    						}
	    					}
	    				}
	    			}
	    			
	    		}
	    	}
	    	
	    	var arrYesChildren = _.filter(arrDevices, function(arr){
	            return (arr.children == true);
	    	});
	    	
    	
		var file = nlapiCreateFile('devices.json', 'PLAINTEXT', JSON.stringify(arrDevices));
	    	file.setFolder(1323614);
		var fileid = nlapiSubmitFile(file);
		
    	
		var file = nlapiCreateFile('devices_no_children.json', 'PLAINTEXT', JSON.stringify(arrNoChildren));
	    	file.setFolder(1323614);
		var fileid = nlapiSubmitFile(file);
		
			
		var file = nlapiCreateFile('devices_yes_children.json', 'PLAINTEXT', JSON.stringify(arrYesChildren));
	    	file.setFolder(1323614);
		var fileid = nlapiSubmitFile(file);
		
			
		emailAdminBody += '<h2>' + arrYesChildren.length + ' devices processed</h2>';
		//emailAdminBody += '<table border="1" cellpadding="5">';
	    //emailAdminBody += '<tr><td>Device</td><td>Amps P</td><td>Amps A</td><td>Amps A+B</td><td>kW P</td><td>kW A</td><td>kW A+B</td></tr>';
		    
	    	for ( var i = 0; arrYesChildren != null && i < arrYesChildren.length; i++ ) {
	    		
		    	var rec = nlapiLoadRecord('customrecord_clgx_dcim_devices', arrYesChildren[i].deviceid);
	            
	            rec.setFieldValue('custrecord_clgx_dcim_device_day_kw_p',parseFloat((arrYesChildren[i].kwsp).toFixed(2)));
	            rec.setFieldValue('custrecord_clgx_dcim_device_day_kw_a',parseFloat((arrYesChildren[i].kwsa).toFixed(2)));
	            rec.setFieldValue('custrecord_clgx_dcim_device_day_kw_ab',parseFloat((arrYesChildren[i].kwsab).toFixed(2)));
	            
	            rec.setFieldValue('custrecord_clgx_dcim_device_day_amp_p_a',parseFloat((arrYesChildren[i].ampsp_a).toFixed(2)));
	            rec.setFieldValue('custrecord_clgx_dcim_device_day_amp_a_a',parseFloat((arrYesChildren[i].ampsa_a).toFixed(2)));
	            rec.setFieldValue('custrecord_clgx_dcim_device_day_amp_ab_a',parseFloat((arrYesChildren[i].ampsab_a).toFixed(2)));
	            
	            rec.setFieldValue('custrecord_clgx_dcim_device_day_amp_p_b',parseFloat((arrYesChildren[i].ampsp_b).toFixed(2)));
	            rec.setFieldValue('custrecord_clgx_dcim_device_day_amp_a_b',parseFloat((arrYesChildren[i].ampsa_b).toFixed(2)));
	            rec.setFieldValue('custrecord_clgx_dcim_device_day_amp_ab_b',parseFloat((arrYesChildren[i].ampsab_b).toFixed(2)));
	            
	            rec.setFieldValue('custrecord_clgx_dcim_device_day_amp_p_c',parseFloat((arrYesChildren[i].ampsp_c).toFixed(2)));
	            rec.setFieldValue('custrecord_clgx_dcim_device_day_amp_a_c',parseFloat((arrYesChildren[i].ampsa_c).toFixed(2)));
	            rec.setFieldValue('custrecord_clgx_dcim_device_day_amp_ab_c',parseFloat((arrYesChildren[i].ampsab_c).toFixed(2)));
		    	
	            var id = nlapiSubmitRecord(rec, false, true);
		    	
		    	//emailAdminBody += '<tr><td>' + arrDevices[i].deviceid + '</td><td>' + kwsp + '</td><td>' + kwsa + '</td><td>' + kwsab + '</td><td>' + ampsp_a + '</td><td>' + ampsa_a + '</td><td>' + ampsab_a + '</td></tr>';
	    	}
	    
// ============================ Send admin email ===============================================================================================
			
		    //emailAdminBody += '</table>';
			var endScript = moment();
			emailAdminBody += '<br><br>';
			emailAdminBody += 'End : ' + endScript.format('M/D/YYYY h:mm:ss a') + '<br>';
			emailAdminBody += 'Total execution minutes : ' + (endScript.diff(startScript)/60000).toFixed(1) + '<br>';
			var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
			emailAdminBody += 'Total usage : ' + usageConsumtion;
			emailAdminBody += '<br><br>';

			clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_admin", emailAdminSubject, emailAdminBody);
			
			/*nlapiSendEmail(432742,71418,emailAdminSubject,emailAdminBody,null,null,null,null,true);
			nlapiSendEmail(432742,1349020,emailAdminSubject,emailAdminBody,null,null,null,null,true);*/

		}
		nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}



