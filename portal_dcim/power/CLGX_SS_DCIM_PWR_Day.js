nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_Day.js
//	Script Name:	CLGX_SS_DCIM_PWR_Day
//	Script Id:		customscript_clgx_ss_dcim_pwr_day
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		06/13/2016
//-------------------------------------------------------------------------------------------------

function scheduled_dcim_pwr_day(){
    try{
        var context = nlapiGetContext();
        var environment = context.getEnvironment();

        if(environment == 'PRODUCTION'){

            var startScript = moment();
            var deployment = context.getDeploymentId();
            if(deployment == 'customdeploy_clgx_ss_dcim_pwr_day_ab'){
                var ss = 'customsearch_clgx_dcim_pwrs_day_ab';
            }
            if(deployment == 'customdeploy_clgx_ss_dcim_pwr_day_p'){
                var ss = 'customsearch_clgx_dcim_pwrs_day_p';
            }

            var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', ss);

            for ( var k = 0; searchPowers != null && k < searchPowers.length; k++ ) {

                var startExec = moment();
                var execMinutes = (startExec.diff(startScript)/60000).toFixed(1);
                if (context.getRemainingUsage() < 2000 || execMinutes > 50 || k > 990 ){
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
                    break;
                }

                var powerid = parseInt(searchPowers[k].getValue('internalid',null,null));

                var power = get_power (powerid);

                //nlapiLogExecution('DEBUG','Results ', '| powerid = ' + powerid + ' | ' + ' | power = '+ JSON.stringify(power) + '  |');

                if(power){

                    for ( var i = 0; i < power.primary.phases; i++ ) {
                        var last  = get_power_day_values (power.primary.power, power.primary.breakers[i].breaker, power.primary.nstart);
                        if((last.day != power.primary.nstart || last.search == 0) && (power.primary.breakers[i].amps.avg > 0 || power.primary.breakers[i].kws.avg > 0 || power.primary.breakers[i].kwsh.cal > 0)){
                            var primary = create_power_day_values (power.primary, last, i);
                        }
                    }
                    if(power.pair){
                        for ( var i = 0; i < power.secondary.phases; i++ ) {
                            var last  = get_power_day_values (power.secondary.power, power.secondary.breakers[i].breaker, power.secondary.nstart);
                            if((last.day != power.secondary.nstart || last.search == 0) && (power.secondary.breakers[i].amps.avg > 0 || power.secondary.breakers[i].kws.avg > 0 || power.secondary.breakers[i].kwsh.cal > 0)){
                                var secondary = create_power_day_values (power.secondary, last, i);
                            }
                        }
                    }

                    var email = 0;
                    if(power.amp_usg > 80){
                        power.count += 1;
                        //if(power.count == 1 || power.count == 5){
                        if((power.count == 1 || power.count == 5)  && power.primary.facility !=51 && power.primary.facility !=52){
                            email = 1;
                            send_usage_email (power);
                        }
                    } else {
                        if(power.count < 5){
                            power.count = 0;
                        }
                    }

                    update_power (power, true, email);
                    if(power.pair){
                        update_power (power, false, email);
                    }

                    var endExec = moment();
                    var execSec = (endExec.diff(startExec)/1000).toFixed(1);
                    var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
                    nlapiLogExecution('DEBUG','Results ', '| power = ' + powerid + ' | ' + k + ' / ' + searchPowers.length + ' | date = ' + power.primary.nstart + ' | Usage = '+ usageConsumtion  + ' | Sec = '+ execSec + '  |');

                }

            }
            if(searchPowers != null){
                var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
            }
            else{

                if(deployment == 'customdeploy_clgx_ss_dcim_pwr_day_p'){
                    clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_admin", "OD 2.1 - Finishing Powers Updates P", "");

                    /*nlapiSendEmail(432742,71418,'OD 2.1 - Finishing Powers Updates P','',null,null,null,null);
                    nlapiSendEmail(432742,1349020,'OD 2.1 - Finishing Powers Updates P','',null,null,null,null);*/
                }

                if(deployment == 'customdeploy_clgx_ss_dcim_pwr_day_ab'){

                    clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_admin", "OD 2.2 - Finishing Powers Updates A+B", "");

                    send_amps_report();
                    create_capacity_ids_files();

                    var status = nlapiScheduleScript('customscript_clgx_ss_dcim_pwr_day_cap_1', 'customdeploy_clgx_ss_dcim_pwr_day_cap_1' ,null);


                    /*
                     Moved to separate scheduled scripts

                    // CLGX_SS_DCIM_PWR_Day_Capacity_1
                    update_devices_capacity_sums_amps ('panel');
                    var usage = 10000 - parseInt(context.getRemainingUsage());
                    nlapiLogExecution('DEBUG','Results ', ' | update_devices_capacity_sums_amps = ' + usage  + '  |');

                    //update_devices_capacity_sums_amps ('ups');
                    //update_devices_capacity_sums_amps ('generator');

                    // CLGX_SS_DCIM_PWR_Day_Capacity_2
                    update_devices_capacity_sums_kw ('panel');
                    var usage = 10000 - parseInt(context.getRemainingUsage());
                    nlapiLogExecution('DEBUG','Results ', ' | update_devices_capacity_sums_kw_panel = ' + usage  + '  |');

                    // CLGX_SS_DCIM_PWR_Day_Capacity_3
                    update_devices_capacity_sums_kw ('ups');
                    var usage = 10000 - parseInt(context.getRemainingUsage());
                    nlapiLogExecution('DEBUG','Results ', ' | update_devices_capacity_sums_kw_ups = ' + usage  + '  |');

                    //update_devices_capacity_sums_kw ('generator');

                    // CLGX_SS_DCIM_PWR_Day_Capacity_4
                    update_devices_capacity_vals();
                    var usage = 10000 - parseInt(context.getRemainingUsage());
                    nlapiLogExecution('DEBUG','Results ', ' | update_devices_capacity_vals = ' + usage  + '  |');

                    clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_admin", "OD 2.3 - Finishing Powers Capacities", "");

                    var status = nlapiScheduleScript('customscript_clgx_ss_dcim_pwr_kw', 'customdeploy_clgx_ss_dcim_pwr_kw' ,null);
                    */


                }

                //nlapiLogExecution('DEBUG','End', 'No more powers to update.');
            }
        }
    }
    catch (error){

        var str = String(error);
        if (str.match('SSS_UNKNOWN_HOST') || str.match('SSS_INVALID_HOST_CERT') || str.match('SSS_CONNECTION_CLOSED') || str.match('SSS_REQUEST_TIME_EXCEEDED') || str.match('SSS_CONNECTION_TIME_OUT') || str.match('UNEXPECTED_ERROR') || str.match('Unexpected token')) {
            var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
            nlapiLogExecution('DEBUG','End', 'Finished powers processing because of error and reschedule it.');
        }
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            if (str.match('TypeError: Cannot read property "phases" from undefined')) {
                var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                //nlapiLogExecution('ERROR','Unexpected Error', error.toString());
                //throw nlapiCreateError('99999', error.toString());
            } else {
                nlapiLogExecution('ERROR','Unexpected Error', error.toString());
                throw nlapiCreateError('99999', error.toString());
            }
        }
    }
}