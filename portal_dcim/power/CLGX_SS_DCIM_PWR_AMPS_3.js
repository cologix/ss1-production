nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_AMPS_3.js
//	Script Name:	CLGX_SS_DCIM_PWR_AMPS_3
//	Script Id:		customscript_clgx_ss_dcim_pwr_amps_3
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/30/2014
//-------------------------------------------------------------------------------------------------

function scheduled_dcim_pwr_amps_3(){
	
    try{

        var today = moment().format('M/D/YYYY');
        var yesterday = moment().subtract('days',1).format('M/D/YYYY');
        var fromid = 432742;

        // --------------------------------------------------------------------------------------------------------
        
        var objFile = nlapiLoadFile(3123895);
        var arrPowersIDs1 = JSON.parse(objFile.getValue());
        
        var objFile = nlapiLoadFile(3123989);
        var arrPowersIDs2 = JSON.parse(objFile.getValue());
        
        var arrPowersIDs = _.compact(_.uniq(_.union(arrPowersIDs1,arrPowersIDs2)));
        
		var jsonPowersIDs = nlapiCreateFile('powers_ids.json', 'PLAINTEXT', JSON.stringify(arrPowersIDs));
		jsonPowersIDs.setFolder(1294374);
		nlapiSubmitFile(jsonPowersIDs);
		
		var jsonPowersIDs = nlapiCreateFile('powers_ids_queue.json', 'PLAINTEXT', JSON.stringify(arrPowersIDs));
		jsonPowersIDs.setFolder(1294374);
		nlapiSubmitFile(jsonPowersIDs);
		
		// --------------------------------------------------------------------------------------------------------

        var objFile = nlapiLoadFile(3123894);
        var arrPowersUsageAll1 = JSON.parse(objFile.getValue());
        
        var objFile = nlapiLoadFile(3123988);
        var arrPowersUsageAll2 = JSON.parse(objFile.getValue());
        
        var arrPowersUsageAll = _.compact(_.uniq(_.union(arrPowersUsageAll1,arrPowersUsageAll2)));
        arrPowersUsageAll = _.sortBy(arrPowersUsageAll, function(obj){ return obj.customer;});
        
		var jsonPowersUsageAll = nlapiCreateFile('powers_usage_all.json', 'PLAINTEXT', JSON.stringify(arrPowersUsageAll));
		jsonPowersUsageAll.setFolder(1294374);
		nlapiSubmitFile(jsonPowersUsageAll);
		
		// --------------------------------------------------------------------------------------------------------
        
        var objFile = nlapiLoadFile(2259379);
        var arrPowersUsage1 = JSON.parse(objFile.getValue());
        
        var objFile = nlapiLoadFile(3123897);
        var arrPowersUsage2 = JSON.parse(objFile.getValue());
        
        var arrPowersUsage = _.compact(_.uniq(_.union(arrPowersUsage1,arrPowersUsage2)));
        arrPowersUsage = _.sortBy(arrPowersUsage, function(obj){ return obj.customer;});
        
		var jsonPowersUsage = nlapiCreateFile('powers_usage.json', 'PLAINTEXT', JSON.stringify(arrPowersUsage));
		jsonPowersUsage.setFolder(1294374);
		nlapiSubmitFile(jsonPowersUsage);
		
		var emailAdminSubject = 'OD 3.1 - Power utilization report for ' + yesterday;
		var emailAdminBody = '';
	    emailAdminBody += '<table border="1" cellpadding="5">';
	    emailAdminBody += '<tr><td>Customer</td><td>Service</td><td>Circuit</td><td>Power</td><td>Pair</td><td>Amps</td><td>Usage</td><td>Old#</td><td>New#</td><td>Email</td></tr>';
		
		for ( var i = 0; arrPowersUsage != null && i < arrPowersUsage.length; i++ ) {
	    	emailAdminBody += '<tr><td>' + arrPowersUsage[i].customer + '</td><td>' + arrPowersUsage[i].service + '</td><td>' + arrPowersUsage[i].circuit + '</td><td>' + arrPowersUsage[i].power + '</td><td>' + arrPowersUsage[i].pair + '</td><td>' + (arrPowersUsage[i].absum).toFixed(2) + '</td><td>' + arrPowersUsage[i].usage + '%</td><td>' + arrPowersUsage[i].oldcount + '</td><td>' + arrPowersUsage[i].newcount + '</td><td>' + arrPowersUsage[i].email + '</td></tr>';
		}

        var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
        emailAdminBody += '</table><br><br>';
		emailAdminBody += 'Usage = ' + usageConsumtion;
		
		clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_pwr_alerts", emailAdminSubject, emailAdminBody);		
		
		/*nlapiSendEmail(432742,71418,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Dan
		nlapiSendEmail(432742,-5,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Val
		nlapiSendEmail(432742,13091,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Phil
		nlapiSendEmail(432742,294,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Jason
		nlapiSendEmail(432742,211645,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Charles
		nlapiSendEmail(432742,179749,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Steve
		nlapiSendEmail(432742,2406,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Lisa
		nlapiSendEmail(432742,1349020,emailAdminSubject,emailAdminBody,null,null,null,null,true);*/ // Alex
		
		// --------------------------------------------------------------------------------------------------------
		
        var status = nlapiScheduleScript('customscript_clgx_ss_dcim_pwr_amps_cust', 'customdeploy_clgx_ss_dcim_pwr_amps_cust' ,null);
		var status = nlapiScheduleScript('customscript_clgx_ss_dcim_fail_pwrs', 'customdeploy_clgx_ss_dcim_fail_pwrs' ,null);
		
	}
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}