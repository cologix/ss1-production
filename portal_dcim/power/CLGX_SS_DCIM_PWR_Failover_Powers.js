nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_Failover_Powers.js
//	Script Name:	CLGX_SS_DCIM_PWR_Failover_Powers
//	Script Id:		customscript_clgx_ss_dcim_fail_pwrs
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		10/08/2015
//-------------------------------------------------------------------------------------------------
function scheduled_clgx_ss_dcim_fail_pwrs(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        var context = nlapiGetContext();
        var environment = context.getEnvironment()
    	
    	if(environment == 'PRODUCTION'){
    		
    			/*
	        var objFile = nlapiLoadFile(3058101);
			var arrPowers = JSON.parse(objFile.getValue());
			var arrNewPowers = JSON.parse(objFile.getValue());
			
			if(arrPowers.length > 0){
				if(arrPowers.length > 500){
			        var hour = moment().hour();
			     	if(hour < 7 || hour > 18){ // before 9 AM and after 6 PM on east coast 
			     		var loopndx = 500;
			     	}
			     	else{
			     		var loopndx = 500;
			     	}
				}
				else{
					var loopndx = arrPowers.length;
				}
	
				var date = new Date();
				var startScript = moment();
		        var emailAdminSubject = 'OD 4.1 - Powers failover - ' + startScript.format('M/D/YYYY');
				var emailAdminBody = '';
				emailAdminBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');
				
				emailAdminBody += '<h2>' + arrPowers.length + ' contacts processed</h2>';
				emailAdminBody += '<table border="1" cellpadding="5">';
			    emailAdminBody += '<tr><td>Power</td><td>Pair</td><td>kW A</td><td>kW A+B</td><td>Amps A_A</td><td>Amps A_B</td><td>Amps A_C</td><td>Amps A+B_A</td><td>Amps A+B_B</td><td>Amps A+B_C</td></tr>';
			    
			    for ( var i = 0; arrPowers != null && i < loopndx; i++ ) {
					
					var sumAMPsA_A = 0;
					var sumAMPsA_B = 0;
					var sumAMPsA_C = 0;
					
					var sumAMPsAB_A = 0;
					var sumAMPsAB_B = 0;
					var sumAMPsAB_C = 0;
					
					var sumKWsAB = 0;
					var sumKWsA = 0;
			        
                    var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', arrPowers[i]);
                    var pairid = recPower.getFieldValue('custrecord_clgx_dcim_pair_power');
                    
                    var arrColumns = new Array();
					var arrFilters = new Array();
					arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_power",null,"anyof",arrPowers[i]));
					var searchPower = nlapiSearchRecord('customrecord_clgx_dcim_points_day', 'customsearch_clgx_dcim_pwr_day_fail_vals', arrFilters, arrColumns);
					
					for ( var j = 0; searchPower != null && j < searchPower.length; j++ ) {
						
						var pwrAMPs = parseFloat(searchPower[j].getValue('custrecord_clgx_dcim_points_day_amps_avg',null,null));
						var pwrKWs =  parseFloat(searchPower[j].getValue('custrecord_clgx_dcim_points_day_kw_avg',null,null));
						var pwrPhase =  searchPower[j].getValue('custrecord_clgx_dcim_points_day_phase',null,null);
						
						if(pwrPhase == 'A'){
							sumAMPsA_A += pwrAMPs;
							sumAMPsAB_A += pwrAMPs;
						}
						if(pwrPhase == 'B'){
							sumAMPsA_B += pwrAMPs;
							sumAMPsAB_B += pwrAMPs;
						}
						if(pwrPhase == 'C'){
							sumAMPsA_C += pwrAMPs;
							sumAMPsAB_C += pwrAMPs;
						}
						sumKWsA += pwrKWs;
						sumKWsAB += pwrKWs;
					}

					if(pairid > 0){
						var arrColumns = new Array();
						var arrFilters = new Array();
						arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_power",null,"anyof",pairid));
						var searchPair = nlapiSearchRecord('customrecord_clgx_dcim_points_day', 'customsearch_clgx_dcim_pwr_day_fail_vals', arrFilters, arrColumns);
						
						for ( var j = 0; searchPair != null && j < searchPair.length; j++ ) {
							
							var pairAMPs = parseFloat(searchPair[j].getValue('custrecord_clgx_dcim_points_day_amps_avg',null,null));
							var pairKWs =  parseFloat(searchPair[j].getValue('custrecord_clgx_dcim_points_day_kw_avg',null,null));
							var pairPhase =  searchPair[j].getValue('custrecord_clgx_dcim_points_day_phase',null,null);
							
							if(pairPhase == 'A'){
								sumAMPsAB_A += pairAMPs;
							}
							if(pairPhase == 'B'){
								sumAMPsAB_B += pairAMPs;
							}
							if(pairPhase == 'C'){
								sumAMPsAB_C += pairAMPs;
							}
							sumKWsAB += pairKWs;
							
						}
					}
					
					recPower.setFieldValue('custrecord_clgx_dcim_sum_day_amp_a_a', parseFloat(sumAMPsA_A.toFixed(2)));
					recPower.setFieldValue('custrecord_clgx_dcim_sum_day_amp_a_b', parseFloat(sumAMPsA_B.toFixed(2)));
					recPower.setFieldValue('custrecord_clgx_dcim_sum_day_amp_a_c', parseFloat(sumAMPsA_C.toFixed(2)));
					
					recPower.setFieldValue('custrecord_clgx_dcim_sum_day_amp_ab_a', parseFloat(sumAMPsAB_A.toFixed(2)));
					recPower.setFieldValue('custrecord_clgx_dcim_sum_day_amp_ab_b', parseFloat(sumAMPsAB_B.toFixed(2)));
					recPower.setFieldValue('custrecord_clgx_dcim_sum_day_amp_ab_c', parseFloat(sumAMPsAB_C.toFixed(2)));
					
					recPower.setFieldValue('custrecord_clgx_dcim_sum_day_kw_a', parseFloat(sumKWsA.toFixed(2)));
					recPower.setFieldValue('custrecord_clgx_dcim_sum_day_kw_ab', parseFloat(sumKWsAB.toFixed(2)));
					
					if(searchPower != null || searchPair != null){
						var id = nlapiSubmitRecord(recPower, false, true);
						emailAdminBody += '<tr><td>' + arrPowers[i] + '</td><td>' + pairid + '</td><td>' + sumKWsA.toFixed(2) + '</td><td>' + sumKWsAB.toFixed(2) + '</td><td>' + sumAMPsA_A.toFixed(2) + '</td><td>' + sumAMPsA_B.toFixed(2) + '</td><td>' + sumAMPsA_C.toFixed(2) + '</td><td>' + sumAMPsAB_A.toFixed(2) + '</td><td>' + sumAMPsAB_B.toFixed(2) + '</td><td>' + sumAMPsAB_C.toFixed(2) + '</td></tr>';
					}
					
					arrNewPowers = _.reject(arrNewPowers, function(num){
				        return (num == arrPowers[i]);
					});
			    	
			    	var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
		            var index = i + 1;
		            nlapiLogExecution('DEBUG','Results ', ' | Index = ' + index + ' of ' + arrPowers.length + ' | Usage = '+ usageConsumtion + '  | PowerID = ' + arrPowers[i] + ' |');
				}
				
				var file = nlapiCreateFile('powers_ids_queue.json', 'PLAINTEXT', JSON.stringify(arrNewPowers));
				file.setFolder(1294374);
				nlapiSubmitFile(file);
	
	// ============================ Send admin email ===============================================================================================
				
			    emailAdminBody += '</table>';
				var endScript = moment();
				emailAdminBody += '<br><br>';
				emailAdminBody += 'End : ' + endScript.format('M/D/YYYY h:mm:ss a') + '<br>';
				emailAdminBody += 'Total execution minutes : ' + (endScript.diff(startScript)/60000).toFixed(1) + '<br>';
				var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
				emailAdminBody += 'Total usage : ' + usageConsumtion;
				emailAdminBody += '<br><br>';
				if(arrPowers != null){
					emailAdminSubject += ' | '+ loopndx + ' of '+ arrPowers.length + ' powers in ' + (endScript.diff(startScript)/60000).toFixed(1) + ' mins | ';
				}
				else{
					emailAdminSubject += ' | 0 powers in ' + (endScript.diff(startScript)/60000).toFixed(1) + ' mins | ';
				}
				//nlapiSendEmail(432742,71418,emailAdminSubject,emailAdminBody,null,null,null,null);
				
				var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
				
			}
			else{
				clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_admin", "OD 4.1 - Finish Powers failover", "");
				
				//nlapiSendEmail(432742,71418,'OD 4.1 - Finish Powers failover','',null,null,null,null,true);
				//nlapiSendEmail(432742,1349020,'OD 4.1 - Finish Powers failover','',null,null,null,null,true);
				
				// reset all panels and all devices fields to 0
		    		var searchDevices = nlapiLoadSearch('customrecord_clgx_dcim_devices', 'customsearch_clgx_dcim_capacity_failover');
				var resultSet = searchDevices.runSearch();
				resultSet.forEachResult(function(searchResult) {
		    		var arrFields = new Array();
		    		arrFields.push('custrecord_clgx_dcim_device_day_kw_p');
		    		arrFields.push('custrecord_clgx_dcim_device_day_kw_a');
		    		arrFields.push('custrecord_clgx_dcim_device_day_kw_ab');
		    		arrFields.push('custrecord_clgx_dcim_device_day_amp_p_a');
		    		arrFields.push('custrecord_clgx_dcim_device_day_amp_a_a');
		    		arrFields.push('custrecord_clgx_dcim_device_day_amp_ab_a');
		    		arrFields.push('custrecord_clgx_dcim_device_day_amp_p_b');
		    		arrFields.push('custrecord_clgx_dcim_device_day_amp_a_b');
		    		arrFields.push('custrecord_clgx_dcim_device_day_amp_ab_b');
		    		arrFields.push('custrecord_clgx_dcim_device_day_amp_p_c');
		    		arrFields.push('custrecord_clgx_dcim_device_day_amp_a_c');
		    		arrFields.push('custrecord_clgx_dcim_device_day_amp_ab_c');
		    		nlapiSubmitField('customrecord_clgx_dcim_devices', parseInt(searchResult.getValue('internalid',null,null)), arrFields, [0,0,0,0,0,0,0,0,0,0,0,0]);
					return true;
				});
		    	
		    	
		    	// create panels ids queue
				//var objFile = nlapiLoadFile(3057891);
				//var arrPowersIDs = JSON.parse(objFile.getValue());

				var arrPanelsIDs = new Array();
				var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pnl_day_fail_ids');
				var resultSet = searchPowers.runSearch();
				resultSet.forEachResult(function(searchResult) {
					arrPanelsIDs.push(parseInt(searchResult.getValue('custrecord_clgx_dcim_device',null,'GROUP')));
					return true;
				});
				
				var arrPanelsUniqIDs = _.compact(_.uniq(arrPanelsIDs));
				
				var jsonPanelsIDs = nlapiCreateFile('panels_ids.json', 'PLAINTEXT', JSON.stringify(arrPanelsUniqIDs));
				jsonPanelsIDs.setFolder(1294374);
				nlapiSubmitFile(jsonPanelsIDs);
				
				var jsonPanelsIDs = nlapiCreateFile('panels_ids_queue.json', 'PLAINTEXT', JSON.stringify(arrPanelsUniqIDs));
				jsonPanelsIDs.setFolder(1294374);
				nlapiSubmitFile(jsonPanelsIDs);
				
				var status = nlapiScheduleScript('customscript_clgx_ss_dcim_fail_pnls', 'customdeploy_clgx_ss_dcim_fail_pnls');
			}
        */

			// reset all panels and all devices power fields to 0
	    		var searchDevices = nlapiLoadSearch('customrecord_clgx_dcim_devices', 'customsearch_clgx_dcim_capacity_failover');
			var resultSet = searchDevices.runSearch();
			resultSet.forEachResult(function(searchResult) {
	    		var arrFields = new Array();
	    		arrFields.push('custrecord_clgx_dcim_device_day_kw_p');
	    		arrFields.push('custrecord_clgx_dcim_device_day_kw_a');
	    		arrFields.push('custrecord_clgx_dcim_device_day_kw_ab');
	    		arrFields.push('custrecord_clgx_dcim_device_day_amp_p_a');
	    		arrFields.push('custrecord_clgx_dcim_device_day_amp_a_a');
	    		arrFields.push('custrecord_clgx_dcim_device_day_amp_ab_a');
	    		arrFields.push('custrecord_clgx_dcim_device_day_amp_p_b');
	    		arrFields.push('custrecord_clgx_dcim_device_day_amp_a_b');
	    		arrFields.push('custrecord_clgx_dcim_device_day_amp_ab_b');
	    		arrFields.push('custrecord_clgx_dcim_device_day_amp_p_c');
	    		arrFields.push('custrecord_clgx_dcim_device_day_amp_a_c');
	    		arrFields.push('custrecord_clgx_dcim_device_day_amp_ab_c');
	    		nlapiSubmitField('customrecord_clgx_dcim_devices', parseInt(searchResult.getValue('internalid',null,null)), arrFields, [0,0,0,0,0,0,0,0,0,0,0,0]);
				return true;
			});
	    	
			// create panels ids queue
			var arrPanelsIDs = new Array();
			var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pnl_day_fail_ids');
			var resultSet = searchPowers.runSearch();
			resultSet.forEachResult(function(searchResult) {
				arrPanelsIDs.push(parseInt(searchResult.getValue('custrecord_clgx_dcim_device',null,'GROUP')));
				return true;
			});
			
			var arrPanelsUniqIDs = _.compact(_.uniq(arrPanelsIDs));
			
			var jsonPanelsIDs = nlapiCreateFile('panels_ids.json', 'PLAINTEXT', JSON.stringify(arrPanelsUniqIDs));
			jsonPanelsIDs.setFolder(1294374);
			nlapiSubmitFile(jsonPanelsIDs);
			
			var jsonPanelsIDs = nlapiCreateFile('panels_ids_queue.json', 'PLAINTEXT', JSON.stringify(arrPanelsUniqIDs));
			jsonPanelsIDs.setFolder(1294374);
			nlapiSubmitFile(jsonPanelsIDs);
			
			// schedule panels capacity processing
			var status = nlapiScheduleScript('customscript_clgx_ss_dcim_fail_pnls', 'customdeploy_clgx_ss_dcim_fail_pnls');
		
			
		}

        
        
        
		nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}