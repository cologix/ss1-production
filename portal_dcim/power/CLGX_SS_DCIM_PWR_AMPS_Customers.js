nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_AMPS_Customers.js
//	Script Name:	CLGX_SS_DCIM_PWR_AMPS_Customers
//	Script Id:		customscript_clgx_ss_dcim_pwr_amps_cust
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		02/17/2015
//-------------------------------------------------------------------------------------------------

function scheduled_dcim_pwr_amps_cust(){

    try{

        var today = moment().format('M/D/YYYY');
        var yesterday = moment().subtract('days',1).format('M/D/YYYY');
        var fromid = 432742;

        var objFile = nlapiLoadFile(1925992);
        var arrAllOutages = JSON.parse(objFile.getValue());

        var arrOutages = _.filter(arrAllOutages, function(arr){
            //return (arr.emailsent == 'F');
            return (arr.email > 0);
        });
        if(arrOutages.length > 0 ){

            var arrServicesIDs = _.uniq(_.pluck(arrOutages, 'serviceid'));
            var arrServices = new Array();
            for ( var i = 0; arrServicesIDs != null && i < arrServicesIDs.length; i++ ) {
                var recService = nlapiLoadRecord('job',arrServicesIDs[i]);
                var objService = new Object();
                objService["customerid"] = parseInt(recService.getFieldValue('parent'));
                objService["customer"] = recService.getFieldText('parent');
                objService["serviceid"] = arrServicesIDs[i];
                arrServices.push(objService);
            }

            var arrCustomersIDs = _.uniq(_.pluck(arrServices, 'customerid'));
            var arrCustomers = new Array();
            for ( var i = 0; arrCustomersIDs != null && i < arrCustomersIDs.length; i++ ) {
                var objCustomer = new Object();

                objCustomer["customerid"] = arrCustomersIDs[i];
                var recCustomer = nlapiLoadRecord('customer',arrCustomersIDs[i]);
                objCustomer["customer"] = recCustomer.getFieldValue('entityid');
                objCustomer["subsidiary"] = recCustomer.getFieldText('subsidiary');
                objCustomer["language"] = recCustomer.getFieldText('language');

                var arrCustomerServices = _.filter(arrServices, function(arr){
                    return (arr.customerid == arrCustomersIDs[i]);
                });
                var arrPowers = new Array();
                var arrFacilitiesIDs = new Array();
                for ( var j = 0; arrCustomerServices != null && j < arrCustomerServices.length; j++ ) {
                    var arrServicePowers = _.filter(arrOutages, function(arr){
                        return (arr.serviceid == arrCustomerServices[j].serviceid);
                    });
                    for ( var k = 0; arrServicePowers != null && k < arrServicePowers.length; k++ ) {
                        var objPower = new Object();

                        objPower["powerid"] = arrServicePowers[k].powerid;
                        objPower["power"] = arrServicePowers[k].power;
                        var pairid = arrServicePowers[k].pairid;
                        objPower["pairid"] = pairid;
                        objPower["pair"] = arrServicePowers[k].pair;
                        //objPower["emailsent"] = arrServicePowers[k].emailsent;
                        objPower["email"] = arrServicePowers[k].email;
                        objPower["facilityid"] = arrServicePowers[k].facilityid;
                        objPower["facility"] = arrServicePowers[k].facility;
                        if(_.indexOf(arrFacilitiesIDs, arrServicePowers[k].facilityid) == -1){
                            arrFacilitiesIDs.push(arrServicePowers[k].facilityid);
                        }
                        objPower["serviceid"] = arrServicePowers[k].serviceid;
                        objPower["service"] = arrServicePowers[k].service;
                        objPower["breaker"] = arrServicePowers[k].breaker;
                        objPower["circuit"] = arrServicePowers[k].circuit;
                        objPower["circuitpair"] = arrServicePowers[k].circuitpair;
                        objPower["volts"] = arrServicePowers[k].volts;
                        objPower["space"] = arrServicePowers[k].space;
                        var recPower = nlapiLoadRecord ('customrecord_clgx_power_circuit',arrServicePowers[k].powerid);
                        var so = recPower.getFieldText('custrecord_power_circuit_service_order');
                        objPower["so"] =  so.replace("Service Order #", "");

                        var panelid = parseInt(recPower.getFieldValue('custrecord_clgx_power_panel_pdpm'));
                        if(panelid > 0){
                            var panel = nlapiLookupField('customrecord_ncfar_asset', panelid, 'altname');
                            objPower["panelid"] = parseInt(panelid);
                            objPower["panel"] = panel;
                            var paneltype = nlapiLookupField('customrecord_ncfar_asset', panelid, 'custrecord_clgx_panel_type');
                            if(paneltype){
                                objPower["type"] = parseInt(paneltype);
                            } else {
                                objPower["type"] = 0;
                            }
                        }
                        else{
                            objPower["panelid"] = 0;
                            objPower["panel"] = '';
                        }
                        if(pairid > 0){
                            var recPair = nlapiLoadRecord ('customrecord_clgx_power_circuit',pairid);
                            var pairpanelid = parseInt(recPair.getFieldValue('custrecord_clgx_power_panel_pdpm'));
                            if(pairpanelid > 0){
                                var pairpanel = nlapiLookupField('customrecord_ncfar_asset', pairpanelid, 'altname');
                                objPower["pairpanelid"] = parseInt(pairpanelid);
                                objPower["pairpanel"] = pairpanel;
                            }
                            else{
                                objPower["pairpanelid"] = 0;
                                objPower["pairpanel"] = '';
                            }
                        }
                        else{
                            objPower["pairpanelid"] = 0;
                            objPower["pairpanel"] = '';
                        }
                        objPower["amps"] = arrServicePowers[k].amps;
                        objPower["index"] = arrServicePowers[k].index;
                        objPower["ampspair"] = arrServicePowers[k].ampspair;
                        objPower["absum"] = arrServicePowers[k].absum;
                        objPower["usage"] = arrServicePowers[k].usage;
                        objPower["email"] = arrServicePowers[k].email;
                        arrPowers.push(objPower);
                    }
                }
                objCustomer["powers"] = arrPowers;
                objCustomer["facilities"] = arrFacilitiesIDs;
                objCustomer["opsreps"] = get_ops_reps(arrFacilitiesIDs);
                objCustomer["salesrepid"] = parseInt(recCustomer.getFieldValue('salesrep'));
                objCustomer["salesrep"] = recCustomer.getFieldText('salesrep');
                objCustomer["salesrepemail"] = nlapiLookupField('employee', parseInt(recCustomer.getFieldValue('salesrep')), 'email');

                var nbrContacts = recCustomer.getLineItemCount('contactroles');
                var arrContacts = new Array();
                var arrEmails = new Array();
                var arrFiltersContacts=new Array();
                var arrColumnsContacts=new Array();
                arrFiltersContacts.push(new nlobjSearchFilter("internalid", "company", "anyof",arrCustomersIDs[i]));
                var searchContactsOutages = nlapiSearchRecord('contact', "customsearch_clgx_dcim_outage_notif_con", arrFiltersContacts, arrColumnsContacts);

                if(searchContactsOutages!=null) {
                    for ( var m = 0;searchContactsOutages != null && m < searchContactsOutages.length; m++ ) {
                        var searchCnt = searchContactsOutages[m];
                        var columns = searchCnt.getAllColumns();
                        var contactid = parseInt(searchCnt.getValue(columns[0]));
                        var contact = searchCnt.getText(columns[0]);
                        var email = searchCnt.getValue(columns[3]);
                        if((_.indexOf(arrEmails, email) == -1) && (email != '' )&& (email != null)){
                            var objContact = new Object();
                            objContact["contactid"] = contactid;
                            objContact["contact"] = contact;
                            objContact["email"] = email;
                            arrContacts.push(objContact);
                            arrEmails.push(email);
                        }
                    }
                } else {
                    for ( var j = 0; j < nbrContacts; j++ ) {
                        var contactid = parseInt(recCustomer.getLineItemValue('contactroles', 'contact', j + 1));
                        var contact = recCustomer.getLineItemValue('contactroles', 'contactname', j + 1);
                        var email = recCustomer.getLineItemValue('contactroles', 'email', j + 1);
                        if((_.indexOf(arrEmails, email) == -1) && (email != '') && (email != null)){
                            var objContact = new Object();
                            objContact["contactid"] = contactid;
                            objContact["contact"] = contact;
                            objContact["email"] = email;
                            arrContacts.push(objContact);
                            arrEmails.push(email);
                        }
                    }
                }


                /*
                var searchTechContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_tecrole", arrFiltersContacts, arrColumnsContacts);
                //var searchTechContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_notif", arrFiltersContacts, arrColumnsContacts);
                var searchPrimaryContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_prrole", arrFiltersContacts, arrColumnsContacts);
                var searchSecondaryContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_secrole", arrFiltersContacts, arrColumnsContacts);
                var searchDecisionMaker = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_dmrole", arrFiltersContacts, arrColumnsContacts);
                //  var searchOpsOutage = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_ops", arrFiltersContacts, arrColumnsContacts);

                //search for Primary/Secondary/Tech Contacts
                if((searchTechContact!=null)||(searchPrimaryContact!=null)||(searchSecondaryContact!=null)||(searchDecisionMaker!=null))
                {

                    for ( var m = 0;searchTechContact != null && m < searchTechContact.length; m++ ) {
                        var searchTCnt = searchTechContact[m];
                        var columns = searchTCnt.getAllColumns();
                        var contactid = parseInt(searchTCnt.getValue(columns[0]));
                        var contact = searchTCnt.getText(columns[0]);
                        var email = searchTCnt.getValue(columns[3]);
                        if((_.indexOf(arrEmails, email) == -1) && (email != '' )&& (email != null)){
                            var objContact = new Object();
                            objContact["contactid"] = contactid;
                            objContact["contact"] = contact;
                            objContact["email"] = email;
                            arrContacts.push(objContact);
                            arrEmails.push(email);
                        }
                    }
                    for ( var m = 0;searchPrimaryContact != null && m < searchPrimaryContact.length; m++ ) {
                        var searchPCnt = searchPrimaryContact[m];
                        var columns = searchPCnt.getAllColumns();
                        var contactid = parseInt(searchPCnt.getValue(columns[0]));
                        var contact = searchPCnt.getText(columns[0]);
                        var email = searchPCnt.getValue(columns[3]);
                        if((_.indexOf(arrEmails, email) == -1) && (email != '' )&& (email != null)){
                            var objContact = new Object();
                            objContact["contactid"] = contactid;
                            objContact["contact"] = contact;
                            objContact["email"] = email;
                            arrContacts.push(objContact);
                            arrEmails.push(email);
                        }
                    }
                    for ( var m = 0;searchSecondaryContact != null && m < searchSecondaryContact.length; m++ ) {
                        var searchSCnt = searchSecondaryContact[m];
                        var columns = searchSCnt.getAllColumns();
                        var contactid = parseInt(searchSCnt.getValue(columns[0]));
                        var contact = searchSCnt.getText(columns[0]);
                        var email = searchSCnt.getValue(columns[3]);
                        if((_.indexOf(arrEmails, email) == -1) && (email != '' )&& (email != null)){
                            var objContact = new Object();
                            objContact["contactid"] = contactid;
                            objContact["contact"] = contact;
                            objContact["email"] = email;
                            arrContacts.push(objContact);
                            arrEmails.push(email);
                        }
                    }
                    for ( var m = 0;searchDecisionMaker!= null && m <searchDecisionMaker.length; m++ ) {
                        var searchDCnt = searchDecisionMaker[m];
                        var columns = searchDCnt.getAllColumns();
                        var contactid = parseInt(searchDCnt.getValue(columns[0]));
                        var contact = searchDCnt.getText(columns[0]);
                        var email = searchDCnt.getValue(columns[3]);
                        if((_.indexOf(arrEmails, email) == -1) && (email != '') && (email != null)){
                            var objContact = new Object();
                            objContact["contactid"] = contactid;
                            objContact["contact"] = contact;
                            objContact["email"] = email;
                            arrContacts.push(objContact);
                            arrEmails.push(email);
                        }
                    }

                }
                else
                {
                for ( var j = 0; j < nbrContacts; j++ ) {
                    var contactid = parseInt(recCustomer.getLineItemValue('contactroles', 'contact', j + 1));
                    var contact = recCustomer.getLineItemValue('contactroles', 'contactname', j + 1);
                    var email = recCustomer.getLineItemValue('contactroles', 'email', j + 1);
                    if((_.indexOf(arrEmails, email) == -1) && (email != '') && (email != null)){
                        var objContact = new Object();
                        objContact["contactid"] = contactid;
                        objContact["contact"] = contact;
                        objContact["email"] = email;
                        arrContacts.push(objContact);
                        arrEmails.push(email);
                    }
                }
                }
                */

                objCustomer["contacts"] = arrContacts;

                arrCustomers.push(objCustomer);
            }

// send emails ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

            var emailAdminSubject = 'ADMIN - Cologix Power Notice - Draw Exceeded for ' + yesterday;
            var emailAdminBody = '';
            emailAdminBody += '<br><br><table border="1" cellpadding="5">';
            emailAdminBody += '<tr><td>Customer</td><td>Service<br/>Order#<td>Space</td></td><td>Primary<br/>Power</td><td>Primary<br/>Panel</td><td>Primary<br>Rating</td><td>Primary<br>Draw</td><td>Redundant<br>Power</td><td>Redundant<br>Panel</td><td>Redundant<br>Rating</td><td>Redundant<br>Draw</td><td>Total<br>Draw</td><td>Utilization</td></tr>';

            for ( var j = 0; arrCustomers != null && j < arrCustomers.length; j++ ) {

                var arrPowers = arrCustomers[j].powers;
                var arrContacts = arrCustomers[j].contacts;
                var arrOpsReps = arrCustomers[j].opsreps;

                for ( var i = 0; arrPowers != null && i < arrPowers.length; i++ ) {
                if (arrPowers[i].facilityid!=51 && arrPowers[i].facilityid!=52){

                    if (arrPowers[i].facilityid == 2 || arrPowers[i].facilityid == 4 || arrPowers[i].facilityid == 5 || arrPowers[i].facilityid == 6 || arrPowers[i].facilityid == 7 || arrPowers[i].facilityid == 9 || arrPowers[i].facilityid == 19) {
                        var mtl = 1;
                        var emailSubject = 'Cologix - Avis de consommation électrique excédée pour / Power Notice - Draw Exceeded for - ' + arrCustomers[j].customer + ' - ' + yesterday;
                    } else {
                        var mtl = 0;
                        var emailSubject = 'Cologix Power Notice - Draw Exceeded for - ' + arrCustomers[j].customer + ' - ' + yesterday;
                    }

                    var emailHTMLPowers = '';
                    var emailHTMLPowersFR = '';

                    emailAdminBody += '<tr><td>' + arrCustomers[j].customer + '</td><td>' + arrPowers[i].so + '<td>' + arrPowers[i].space + '</td></td><td>' + arrPowers[i].power + '</td><td>' + arrPowers[i].panel + '</td><td>' + arrPowers[i].circuit + '</td><td>' + arrPowers[i].amps + '</td><td>' + arrPowers[i].pair + '</td><td>' + arrPowers[i].pairpanel + '</td><td>' + arrPowers[i].circuitpair + '</td><td>' + arrPowers[i].ampspair + '</td><td>' + arrPowers[i].absum + '</td><td>' + arrPowers[i].usage + '%</td></tr>';

                    emailHTMLPowers += '<table width="560" cellpadding="5" cellspacing="0" border="1" style="border-style: solid;border-color: #CCCCCC;">';

                    emailHTMLPowers += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                    emailHTMLPowers += 'Service Order';
                    emailHTMLPowers += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                    emailHTMLPowers += arrPowers[i].so;
                    emailHTMLPowers += '</td></tr>';

                    emailHTMLPowers += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                    emailHTMLPowers += 'Space';
                    emailHTMLPowers += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                    emailHTMLPowers += arrPowers[i].space;
                    emailHTMLPowers += '</td></tr>';

                    emailHTMLPowers += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                    emailHTMLPowers += 'Primary Power';
                    emailHTMLPowers += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                    emailHTMLPowers += arrPowers[i].power;
                    emailHTMLPowers += '</td></tr>';

                    emailHTMLPowers += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                    emailHTMLPowers += 'Primary Panel';
                    emailHTMLPowers += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                    emailHTMLPowers += arrPowers[i].panel;
                    emailHTMLPowers += '</td></tr>';

                    emailHTMLPowers += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                    emailHTMLPowers += 'Primary Rating';
                    emailHTMLPowers += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                    emailHTMLPowers += arrPowers[i].circuit + ' Amps';
                    emailHTMLPowers += '</td></tr>';

                    emailHTMLPowers += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                    emailHTMLPowers += 'Primary Draw';
                    emailHTMLPowers += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                    emailHTMLPowers += arrPowers[i].amps + ' Amps';
                    emailHTMLPowers += '</td></tr>';

                    if (arrPowers[i].pair != null && arrPowers[i].pair != '' && arrPowers[i].pair != '- None -') {
                        emailHTMLPowers += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowers += 'Redundant Power';
                        emailHTMLPowers += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowers += arrPowers[i].pair;
                        emailHTMLPowers += '</td></tr>';

                        emailHTMLPowers += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowers += 'Redundant Panel';
                        emailHTMLPowers += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowers += arrPowers[i].pairpanel;
                        emailHTMLPowers += '</td></tr>';

                        emailHTMLPowers += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowers += 'Redundant Rating';
                        emailHTMLPowers += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowers += arrPowers[i].circuitpair + ' Amps';
                        emailHTMLPowers += '</td></tr>';

                        emailHTMLPowers += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowers += 'Redundant Draw';
                        emailHTMLPowers += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowers += arrPowers[i].ampspair + ' Amps';
                        emailHTMLPowers += '</td></tr>';

                        emailHTMLPowers += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowers += 'Total Draw';
                        emailHTMLPowers += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowers += arrPowers[i].absum + ' Amps';
                        emailHTMLPowers += '</td></tr>';
                    }
                    emailHTMLPowers += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                    emailHTMLPowers += 'Utilization';
                    emailHTMLPowers += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                    emailHTMLPowers += arrPowers[i].usage + ' %';
                    emailHTMLPowers += '</td></tr>';
                    emailHTMLPowers += '</table><br/>';


                    if (mtl == 1) {
                        emailHTMLPowersFR += '<table width="560" cellpadding="5" cellspacing="0" border="1" style="border-style: solid;border-color: #CCCCCC;">';

                        emailHTMLPowersFR += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowersFR += 'Commande de service';
                        emailHTMLPowersFR += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowersFR += arrPowers[i].so;
                        emailHTMLPowersFR += '</td></tr>';

                        emailHTMLPowersFR += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowersFR += 'Espace (cabinet/cage)';
                        emailHTMLPowersFR += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowersFR += arrPowers[i].space;
                        emailHTMLPowersFR += '</td></tr>';

                        emailHTMLPowersFR += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowersFR += 'Alimentation primaire';
                        emailHTMLPowersFR += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowersFR += arrPowers[i].power;
                        emailHTMLPowersFR += '</td></tr>';

                        emailHTMLPowersFR += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowersFR += 'Paneaux primaire';
                        emailHTMLPowersFR += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowersFR += arrPowers[i].panel;
                        emailHTMLPowersFR += '</td></tr>';

                        emailHTMLPowersFR += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowersFR += 'Capacit&eacute; nominale du circuit primaire';
                        emailHTMLPowersFR += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowersFR += arrPowers[i].circuit + ' Amps';
                        emailHTMLPowersFR += '</td></tr>';

                        emailHTMLPowersFR += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowersFR += 'Consommation du circuit primaire';
                        emailHTMLPowersFR += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowersFR += arrPowers[i].amps + ' Amps';
                        emailHTMLPowersFR += '</td></tr>';

                        if (arrPowers[i].pair != null && arrPowers[i].pair != '' && arrPowers[i].pair != '- None -') {
                            emailHTMLPowersFR += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                            emailHTMLPowersFR += 'Alimentation redondante';
                            emailHTMLPowersFR += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                            emailHTMLPowersFR += arrPowers[i].pair;
                            emailHTMLPowersFR += '</td></tr>';

                            emailHTMLPowersFR += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                            emailHTMLPowersFR += 'Paneaux redondant';
                            emailHTMLPowersFR += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                            emailHTMLPowersFR += arrPowers[i].pairpanel;
                            emailHTMLPowersFR += '</td></tr>';

                            emailHTMLPowersFR += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                            emailHTMLPowersFR += 'Capacit&eacute; nominale du circuit redondant';
                            emailHTMLPowersFR += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                            emailHTMLPowersFR += arrPowers[i].circuitpair + ' Amps';
                            emailHTMLPowersFR += '</td></tr>';

                            emailHTMLPowersFR += '<tr><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                            emailHTMLPowersFR += 'Consommation du circuit redondant';
                            emailHTMLPowersFR += '</td><td class="center" align="left" style="margin: 0; font-weight:300; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                            emailHTMLPowersFR += arrPowers[i].ampspair + ' Amps';
                            emailHTMLPowersFR += '</td></tr>';

                            emailHTMLPowersFR += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                            emailHTMLPowersFR += 'Consommation totale';
                            emailHTMLPowersFR += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                            emailHTMLPowersFR += arrPowers[i].absum + ' Amps';
                            emailHTMLPowersFR += '</td></tr>';
                        }
                        emailHTMLPowersFR += '<tr><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowersFR += 'Utilisation';
                        emailHTMLPowersFR += '</td><td class="center" align="left" style="background-color:#EEEEEE; margin: 0; font-weight:bold; font-size:13px ; color:#666666; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; line-height: 23px;mso-line-height-rule: exactly;">';
                        emailHTMLPowersFR += arrPowers[i].usage + ' %';
                        emailHTMLPowersFR += '</td></tr>';
                        emailHTMLPowersFR += '</table><br/>';
                    }

                    if (mtl == 1) {
                        if (arrPowers[i].email == 1) {
                            var objFile = nlapiLoadFile(2163606);
                        } else {
                            var objFile = nlapiLoadFile(3085802);
                        }
                    } else {
                        if (arrPowers[i].email == 1) {
                            var objFile = nlapiLoadFile(2153544);
                        } else {
                            var objFile = nlapiLoadFile(3085903);
                        }
                    }

                    var emailBodyHTML = objFile.getValue();

                    var strSalesRep = arrCustomers[j].salesrep + ' (' + arrCustomers[j].salesrepemail + ')';
                    emailBodyHTML = emailBodyHTML.replace(new RegExp('{powers}', 'g'), emailHTMLPowers);
                    if (mtl == 1) {
                        emailBodyHTML = emailBodyHTML.replace(new RegExp('{powersFR}', 'g'), emailHTMLPowersFR);
                    }
                    emailBodyHTML = emailBodyHTML.replace(new RegExp('{salesrep}', 'g'), strSalesRep);

                    if (arrCustomers[j].powers[i].type != 5 && arrCustomers[j].powers[i].type != 6) {
                        // send email to customer contacts
                        for (var k = 0; arrContacts != null && k < arrContacts.length; k++) {
                            nlapiSendEmail(fromid, arrContacts[k].contactid, emailSubject, emailBodyHTML, null, null, null, null, true);
                        }
                    }

                    var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', arrPowers[i].powerid);
                    recPower.setFieldValue('custrecord_clgx_dcim_points_email', 'T');
                    recPower.setFieldValue('custrecord_clgx_dcim_points_email_date', today);
                    nlapiSubmitRecord(recPower, false, true);
                    if (arrPowers[i].pairid > 0) {
                        var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', arrPowers[i].pairid);
                        recPower.setFieldValue('custrecord_clgx_dcim_points_email', 'T');
                        recPower.setFieldValue('custrecord_clgx_dcim_points_email_date', today);
                        nlapiSubmitRecord(recPower, false, true);
                    }

                    if (arrCustomers[j].powers[i].type != 5 && arrCustomers[j].powers[i].type != 6) {
                        // send email to ops reps
                        for (var k = 0; arrOpsReps != null && k < arrOpsReps.length; k++) {
                            nlapiSendEmail(fromid, arrOpsReps[k].id, emailSubject, emailBodyHTML, null, null, null, null, true); // ops reps
                        }
                        // send email to sales rep
                        nlapiSendEmail(fromid, arrCustomers[j].salesrepid, emailSubject, emailBodyHTML, null, null, null, null, true); //sales rep
                    }

                    clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_pwr_alerts", emailSubject, emailBodyHTML);

                    /*nlapiSendEmail(fromid, 71418, emailSubject, emailBodyHTML, null, null, null, null,true); // Dan
                    nlapiSendEmail(fromid, 211645, emailSubject, emailBodyHTML, null, null, null, null,true);*/ // Charles

                    var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                    nlapiLogExecution('DEBUG', 'Value', '| Power = ' + arrPowers[i].power + '| Customer = ' + arrCustomers[j].customer + ' | Usage = ' + usageConsumtion + ' |');
                }
                }
            }

            emailAdminBody += '</table><br><br>';
            if(arrCustomers[j].powers[i].type != 5 && arrCustomers[j].powers[i].type != 6){
                clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_pwr_alerts", emailAdminSubject, emailAdminBody);

                /*nlapiSendEmail(fromid, 71418, emailAdminSubject, emailAdminBody, null, null, null, null,true); // Dan
                nlapiSendEmail(fromid,-5,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Val
                nlapiSendEmail(fromid,13091,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Phil
                nlapiSendEmail(fromid,294,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Jason
                nlapiSendEmail(fromid,211645,emailAdminSubject,emailAdminBody,null,null,null,null,true);*/ // Charles
            }
        }
        else{
            clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_admin", "OD 3.2 - No New Amps Outages Today", "No New Amps Outages Today");
            //nlapiSendEmail(fromid, 71418, 'OD 3.2 - No New Amps Outages Today', 'No New Amps Outages Today', null, null, null, null,true);
        }

//---------- End Sections ------------------------------------------------------------------------------------------------

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}



function get_ops_reps (arrFacilitiesIDs){

    var arrOpsReps = new Array();
    for ( var i = 0; i < arrFacilitiesIDs.length; i++ ) {

        if(arrFacilitiesIDs[i] == 2 || arrFacilitiesIDs[i] == 4 || arrFacilitiesIDs[i] == 5 || arrFacilitiesIDs[i] == 6 || arrFacilitiesIDs[i] == 7 || arrFacilitiesIDs[i] == 9 || arrFacilitiesIDs[i] == 19){

            try {
                arrOpsReps = clgx_get_employee_list_from_savedsearch("customsearch_clgxe_dcim_ccm_mtl");
            } catch(ex) {
                nlapiLogExecution("DEBUG", "get_ops_reps - AMPS Customer - MTL", ex);
            }

            /*var objOpsRep = new Object();
            objOpsRep["opid"] = 212518;
            objOpsRep["op"] = 'Amy J Connell';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 168983;
            objOpsRep["op"] = 'Donato Di Rienzo';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 3118;
            objOpsRep["op"] = 'Sorin Arion';
            arrOpsReps.push(objOpsRep);*/

        }

        else{

            try {
                arrOpsReps = clgx_get_employee_list_from_savedsearch("customsearch_clgxe_dcim_ccm_nonmtl");
            } catch(ex) {
                nlapiLogExecution("DEBUG", "get_ops_reps - AMPS Customer - Non-MTL", ex);
            }

            /*var objOpsRep = new Object();
            objOpsRep["opid"] = 212518;
            objOpsRep["op"] = 'Amy J Connell';
            arrOpsReps.push(objOpsRep);*/

            /*var objOpsRep = new Object();
            objOpsRep["opid"] = 1034604;
            objOpsRep["op"] = 'Yazmine Myers';
            arrOpsReps.push(objOpsRep);*/

            /*var objOpsRep = new Object();
            objOpsRep["opid"] = 9735;
            objOpsRep["op"] = 'Pauli Guild';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 8795;
            objOpsRep["op"] = 'Mario Novello';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 1552824;
            objOpsRep["op"] = 'Kathy Segui';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 600794;
            objOpsRep["op"]   = "Bryan Moore";
            arr.push(objOpsRep);*/
        }
    }

    return arrOpsReps;
}

