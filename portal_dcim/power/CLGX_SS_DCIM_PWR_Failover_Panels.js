nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_Failover_Panels.js
//	Script Name:	CLGX_SS_DCIM_PWR_Failover_Panels
//	Script Id:		customscript_clgx_ss_dcim_fail_pnls
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		10/08/2015
//-------------------------------------------------------------------------------------------------
function scheduled_clgx_ss_dcim_fail_pnls(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        var context = nlapiGetContext();
        var environment = context.getEnvironment()
    	
    	if(environment == 'PRODUCTION'){
    		
	        var objFile = nlapiLoadFile(3061472);
			var arrPanels = JSON.parse(objFile.getValue());
			var arrNewPanels = JSON.parse(objFile.getValue());
			
			if(arrPanels.length > 0){
				if(arrPanels.length > 500){
			        var hour = moment().hour();
			     	if(hour < 7 || hour > 18){ // before 9 AM and after 6 PM on east coast 
			     		var loopndx = 500;
			     	}
			     	else{
			     		var loopndx = 500;
			     	}
				}
				else{
					var loopndx = arrPanels.length;
				}
	
				var date = new Date();
				var startScript = moment();
		        var emailAdminSubject = 'OD 4.2 Panels failover - ' + startScript.format('M/D/YYYY');
				var emailAdminBody = '';
				emailAdminBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');
				
				emailAdminBody += '<h2>' + arrPanels.length + ' panels processed</h2>';
				emailAdminBody += '<table border="1" cellpadding="5">';
			    emailAdminBody += '<tr><td>Panel</td><td>kW A</td><td>kW A+B</td><td>kW P</td><td>Amps A_A</td><td>Amps A_B</td><td>Amps A_C</td><td>Amps A+B_A</td><td>Amps A+B_B</td><td>Amps A+B_C</td><td>Amps P_A</td><td>Amps P_B</td><td>Amps P_C</td></tr>';
			    
			    for ( var i = 0; arrPanels != null && i < loopndx; i++ ) {
					
			    	var sumKWsA = 0;
			    	var sumKWsAB = 0;
			    	var sumKWsP = 0;
			    	
			    	var sumAMPsA_A = 0;
					var sumAMPsA_B = 0;
					var sumAMPsA_C = 0;
					
					var sumAMPsAB_A = 0;
					var sumAMPsAB_B = 0;
					var sumAMPsAB_C = 0;
					
					var sumAMPsP_A = 0;
					var sumAMPsP_B = 0;
					var sumAMPsP_C = 0;
			        
					var arrColumns = new Array();
					var arrFilters = new Array();
					arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_device",null,"anyof",arrPanels[i]));
					var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pnl_day_fail_vals', arrFilters, arrColumns);
					
					for ( var j = 0; searchPowers != null && j < searchPowers.length; j++ ) {
						
						var pairid = parseInt(searchPowers[j].getValue('custrecord_clgx_dcim_pair_power',null,null));
						if(pairid > 0){}
						else{
							pairid = 0;
						}
						
						sumKWsA += parseFloat(searchPowers[j].getValue('custrecord_clgx_dcim_sum_day_kw_a',null,null));
						sumKWsAB += parseFloat(searchPowers[j].getValue('custrecord_clgx_dcim_sum_day_kw_ab',null,null));
						if(pairid == 0){
							sumKWsP += parseFloat(searchPowers[j].getValue('custrecord_clgx_dcim_sum_day_kw_a',null,null));
						}
						
						sumAMPsA_A += parseFloat(searchPowers[j].getValue('custrecord_clgx_dcim_sum_day_amp_a_a',null,null));
						sumAMPsA_B += parseFloat(searchPowers[j].getValue('custrecord_clgx_dcim_sum_day_amp_a_b',null,null));
						sumAMPsA_C += parseFloat(searchPowers[j].getValue('custrecord_clgx_dcim_sum_day_amp_a_c',null,null));
						
						sumAMPsAB_A += parseFloat(searchPowers[j].getValue('custrecord_clgx_dcim_sum_day_amp_ab_a',null,null));
						sumAMPsAB_B += parseFloat(searchPowers[j].getValue('custrecord_clgx_dcim_sum_day_amp_ab_b',null,null));
						sumAMPsAB_C += parseFloat(searchPowers[j].getValue('custrecord_clgx_dcim_sum_day_amp_ab_c',null,null));
						
						if(pairid == 0){
							sumAMPsP_A += parseFloat(searchPowers[j].getValue('custrecord_clgx_dcim_sum_day_amp_a_a',null,null));
							sumAMPsP_B += parseFloat(searchPowers[j].getValue('custrecord_clgx_dcim_sum_day_amp_a_b',null,null));
							sumAMPsP_C += parseFloat(searchPowers[j].getValue('custrecord_clgx_dcim_sum_day_amp_a_c',null,null));
						}
					}
					
					var recPanel = nlapiLoadRecord('customrecord_clgx_dcim_devices', arrPanels[i]);
					
					recPanel.setFieldValue('custrecord_clgx_dcim_device_day_kw_a', parseFloat(sumKWsA.toFixed(2)));
					recPanel.setFieldValue('custrecord_clgx_dcim_device_day_kw_ab', parseFloat(sumKWsAB.toFixed(2)));
					recPanel.setFieldValue('custrecord_clgx_dcim_device_day_kw_p', parseFloat(sumKWsP.toFixed(2)));
					
					recPanel.setFieldValue('custrecord_clgx_dcim_device_day_amp_a_a', parseFloat(sumAMPsA_A.toFixed(2)));
					recPanel.setFieldValue('custrecord_clgx_dcim_device_day_amp_a_b', parseFloat(sumAMPsA_B.toFixed(2)));
					recPanel.setFieldValue('custrecord_clgx_dcim_device_day_amp_a_c', parseFloat(sumAMPsA_C.toFixed(2)));
					
					recPanel.setFieldValue('custrecord_clgx_dcim_device_day_amp_ab_a', parseFloat(sumAMPsAB_A.toFixed(2)));
					recPanel.setFieldValue('custrecord_clgx_dcim_device_day_amp_ab_b', parseFloat(sumAMPsAB_B.toFixed(2)));
					recPanel.setFieldValue('custrecord_clgx_dcim_device_day_amp_ab_c', parseFloat(sumAMPsAB_C.toFixed(2)));
					
					recPanel.setFieldValue('custrecord_clgx_dcim_device_day_amp_p_a', parseFloat(sumAMPsP_A.toFixed(2)));
					recPanel.setFieldValue('custrecord_clgx_dcim_device_day_amp_p_b', parseFloat(sumAMPsP_B.toFixed(2)));
					recPanel.setFieldValue('custrecord_clgx_dcim_device_day_amp_p_c', parseFloat(sumAMPsP_C.toFixed(2)));
					
					var id = nlapiSubmitRecord(recPanel, false, true);
					
					emailAdminBody += '<tr><td>' + arrPanels[i] + '</td><td>' + sumKWsA.toFixed(2) + '</td><td>' + sumKWsAB.toFixed(2) + '</td><td>' + sumKWsP.toFixed(2) + '</td><td>' + sumAMPsA_A.toFixed(2) + '</td><td>' + sumAMPsA_B.toFixed(2) + '</td><td>' + sumAMPsA_C.toFixed(2) + '</td><td>' + sumAMPsAB_A.toFixed(2) + '</td><td>' + sumAMPsAB_B.toFixed(2) + '</td><td>' + sumAMPsAB_C.toFixed(2) + '</td><td>' + sumAMPsP_A.toFixed(2) + '</td><td>' + sumAMPsP_B.toFixed(2) + '</td><td>' + sumAMPsP_C.toFixed(2) + '</td></tr>';
					
			    	arrNewPanels = _.reject(arrNewPanels, function(num){
				        return (num == arrPanels[i]);
					});
			    	
			    	var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
		            var index = i + 1;
		            nlapiLogExecution('DEBUG','Results ', ' | Index = ' + index + ' of ' + arrPanels.length + ' | Usage = '+ usageConsumtion + '  | PanelID = ' + arrPanels[i] + ' |');
				}
				
				var file = nlapiCreateFile('panels_ids_queue.json', 'PLAINTEXT', JSON.stringify(arrNewPanels));
				file.setFolder(1294374);
				nlapiSubmitFile(file);
	
	// ============================ Send admin email ===============================================================================================
				
			    emailAdminBody += '</table>';
				var endScript = moment();
				emailAdminBody += '<br><br>';
				emailAdminBody += 'End : ' + endScript.format('M/D/YYYY h:mm:ss a') + '<br>';
				emailAdminBody += 'Total execution minutes : ' + (endScript.diff(startScript)/60000).toFixed(1) + '<br>';
				var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
				emailAdminBody += 'Total usage : ' + usageConsumtion;
				emailAdminBody += '<br><br>';
				if(arrPanels != null){
					emailAdminSubject += ' | '+ loopndx + ' of '+ arrPanels.length + ' panels in ' + (endScript.diff(startScript)/60000).toFixed(1) + ' mins | ';
				}
				else{
					emailAdminSubject += ' | 0 panels in ' + (endScript.diff(startScript)/60000).toFixed(1) + ' mins | ';
				}
				//nlapiSendEmail(432742,71418,emailAdminSubject,emailAdminBody,null,null,null,null);
				
				var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
				
			}
			else{
				clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_admin", "OD 4.2 - Finish Panels failover", "");
				
				/*nlapiSendEmail(432742,71418,'OD 4.2 - Finish Panels failover','',null,null,null,null,true);
				nlapiSendEmail(432742,1349020,'OD 4.2 - Finish Panels failover','',null,null,null,null,true);*/
				
				// end of powers processing, process trees
		        var status = nlapiScheduleScript('customscript_clgx_ss_dcim_fail_devs', 'customdeploy_clgx_ss_dcim_fail_devs');
			}	
		}
		nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}