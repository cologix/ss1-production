nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_AMPS.js
//	Script Name:	CLGX_SS_DCIM_PWR_AMPS
//	Script Id:		customscript_clgx_ss_dcim_pwr_amps
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/30/2014
//-------------------------------------------------------------------------------------------------

function scheduled_dcim_pwr_amps(){
	
    try{

    	var yesterday = moment().subtract('days',1).format('M/D/YYYY');
		
    	// the search returns > 4000 so split it by Voltage
    	var arrVolts = new Array();
    	var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_volts',null,'GROUP').setSort(false));
		var arrFilters = new Array();
		var searchVolts = nlapiSearchRecord('customrecord_clgx_dcim_points_day', null, arrFilters, arrColumns);
		for ( var i = 0; searchVolts != null && i < searchVolts.length; i++ ) {
			arrVolts.push(parseInt(searchVolts[i].getValue('custrecord_clgx_dcim_points_day_volts',null,'GROUP')));
		}
    	
		var previousid = 0;
		var index = 0;
    	var arrBreakers = new Array();
    	for ( var i = 0; arrVolts != null && i < arrVolts.length; i++ ) {

			var arrColumns = new Array();
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_volts",null,"equalto",arrVolts[i]));
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_date",null,"on",yesterday));
			//arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_power",null,"anyof",[13604,13605,13606,13607,13608]));
			var searchPoints = nlapiLoadSearch('customrecord_clgx_dcim_points_day', 'customsearch_clgx_dcim_pwr_util_outage');
			searchPoints.addColumns(arrColumns);
			searchPoints.addFilters(arrFilters);
			var resultSet = searchPoints.runSearch();
			resultSet.forEachResult(function(searchResult) {
				var columns = searchResult.getAllColumns();
				var objBreaker = new Object();
				objBreaker["customerid"] = parseInt(searchResult.getValue(columns[0]));
				objBreaker["customer"] = searchResult.getText(columns[0]);
				objBreaker["emailsent"] = searchResult.getValue(columns[8]);
				
				var oldcount = parseInt(searchResult.getValue(columns[11]));
				if(oldcount > 0){
					objBreaker["oldcount"] = oldcount;
					objBreaker["newcount"] = oldcount;
				}
				else{
					objBreaker["oldcount"] = 0;
					objBreaker["newcount"] = 0;
				}
				
				objBreaker["facilityid"] = parseInt(searchResult.getValue(columns[7]));
				objBreaker["facility"] = searchResult.getText(columns[7]);
				objBreaker["serviceid"] = parseInt(searchResult.getValue(columns[1]));
				objBreaker["service"] = searchResult.getText(columns[1]);
				
				
				var powerid = parseInt(searchResult.getValue(columns[2]));
				objBreaker["powerid"] = powerid;
				objBreaker["power"] = searchResult.getText(columns[2]);
				
				if(powerid != previousid){
					index = 1;
				}
				else{
					index = index + 1;
				}
				objBreaker["index"] = index;
				previousid = powerid;
				
				
				var pairid = parseInt(searchResult.getValue(columns[3]));
				if(pairid > 0){
					objBreaker["pairid"] = parseInt(searchResult.getValue(columns[3]));
					objBreaker["pair"] = searchResult.getText(columns[3]);
				}
				else{
					objBreaker["pairid"] = 0;
					objBreaker["pair"] = '';
				}
				objBreaker["breaker"] = parseInt(searchResult.getValue(columns[4]));
				objBreaker["circuit"] = parseInt(searchResult.getValue(columns[5]));
				objBreaker["space"] = searchResult.getText(columns[10]);
				objBreaker["volts"] = searchResult.getText(columns[9]);
				objBreaker["amps"] = parseFloat(searchResult.getValue(columns[6]));
				
				arrBreakers.push(objBreaker);
				return true;
			});
    	}
    	
		var arrPowersIDs = new Array();
    	for ( var i = 0; arrBreakers != null && i < arrBreakers.length; i++ ) {
			
			arrPowersIDs.push(arrBreakers[i].powerid);
			arrPowersIDs.push(arrBreakers[i].pairid);
			
			var objBreaker = _.find(arrBreakers, function(arr){ return (arr.powerid == arrBreakers[i].pairid && arr.index == arrBreakers[i].index ); });
			if(objBreaker != null){
				arrBreakers[i]["circuitpair"] = objBreaker.circuit;
				arrBreakers[i]["ampspair"] = objBreaker.amps;
				var ab = arrBreakers[i].amps + objBreaker.amps
			}
			else{
				arrBreakers[i]["circuitpair"] = '';
				arrBreakers[i]["ampspair"] = 0;
				var ab = arrBreakers[i].amps;
			}
			arrBreakers[i]["absum"] = ab;
			var usage = parseFloat(((ab*100) / arrBreakers[i].circuit).toFixed(2));
			arrBreakers[i]["usage"] = usage;
			
			if(usage >= 80){
				arrBreakers[i].newcount = arrBreakers[i].oldcount + 1;
			}
		}
		
		var strBreakers = JSON.stringify(arrBreakers);
		var jsonUsageAll = nlapiCreateFile('powers_usage_all.json', 'PLAINTEXT', strBreakers);
		jsonUsageAll.setFolder(1294374);
		nlapiSubmitFile(jsonUsageAll);
		
		var arrPowersUniqIDs = _.compact(_.uniq(arrPowersIDs));
		var jsonPowersIDs = nlapiCreateFile('powers_ids.json', 'PLAINTEXT', JSON.stringify(arrPowersUniqIDs));
		jsonPowersIDs.setFolder(1294374);
		nlapiSubmitFile(jsonPowersIDs);
		var jsonPowersIDs = nlapiCreateFile('powers_ids_queue.json', 'PLAINTEXT', JSON.stringify(arrPowersUniqIDs));
		jsonPowersIDs.setFolder(1294374);
		nlapiSubmitFile(jsonPowersIDs);
		
		var arrOutage = _.filter(arrBreakers, function(arr){
	        //return (arr.usage > 80 || arr.oldcount > 0);
	        return (arr.usage > 80);
		});
		
		// keep only the A part of A+B Powers
		var arrUniqIDs = new Array();
		var arrOutageA = new Array();
		for ( var i = 0; arrOutage != null && i < arrOutage.length; i++ ) {
			if(_.indexOf(arrUniqIDs, arrOutage[i].powerid) == -1){
				arrOutageA.push(arrOutage[i]);
				arrUniqIDs.push(arrOutage[i].powerid);
				if(arrOutage[i].pairid != '- None -'){
					arrUniqIDs.push(arrOutage[i].pairid);
				}
			}
		}
		
		arrOutageA = _.sortBy(arrOutageA, function(obj){ return obj.customer;});
		
		for ( var i = 0; arrOutageA != null && i < arrOutageA.length; i++ ) {
			
			var oldcount = arrOutageA[i].oldcount;
			var newcount = arrOutageA[i].newcount;
			
			if(newcount > oldcount){
				if(newcount == 1){
					arrOutageA[i]["email"] = 1;
				}
				else if(newcount == 5){
					arrOutageA[i]["email"] = 2;
				}
				else{
					arrOutageA[i]["email"] = 0;
				}
				nlapiSubmitField('customrecord_clgx_power_circuit', arrOutageA[i].powerid, 'custrecord_clgx_dcim_points_occurrences', arrOutageA[i].newcount);
				if(arrOutageA[i].pairid > 0){
					nlapiSubmitField('customrecord_clgx_power_circuit', arrOutageA[i].pairid, 'custrecord_clgx_dcim_points_occurrences', arrOutageA[i].newcount);
				}
			}
			else{
				if(oldcount < 5){
					nlapiSubmitField('customrecord_clgx_power_circuit', arrOutageA[i].powerid, 'custrecord_clgx_dcim_points_occurrences', 0);
					if(arrOutageA[i].pairid > 0){
						nlapiSubmitField('customrecord_clgx_power_circuit', arrOutageA[i].pairid, 'custrecord_clgx_dcim_points_occurrences', 0);
					}
				}
				arrOutageA[i]["email"] = 0;
			}
		}
		
		var strUsage = JSON.stringify(arrOutageA);
		var jsonUsage = nlapiCreateFile('powers_usage.json', 'PLAINTEXT', strUsage);
		jsonUsage.setFolder(1294374);
		nlapiSubmitFile(jsonUsage);

		var emailAdminSubject = 'OD 3.1 - Power utilization report for ' + yesterday;
		var emailAdminBody = '';
	    emailAdminBody += '<table border="1" cellpadding="5">';
	    emailAdminBody += '<tr><td>Customer</td><td>Service</td><td>Circuit</td><td>Breaker</td><td>Power</td><td>Amps</td><td>Pair</td><td>Amps</td><td>Amps(A+B)</td><td>Usage</td><td>Old#</td><td>New#</td><td>Email</td></tr>';
		
		for ( var i = 0; arrOutageA != null && i < arrOutageA.length; i++ ) {
	    	emailAdminBody += '<tr><td>' + arrOutageA[i].customer + '</td><td>' + arrOutageA[i].service + '</td><td>' + arrOutageA[i].circuit + '</td><td>' + arrOutageA[i].breaker + '</td><td>' + arrOutageA[i].power + '</td><td>' + arrOutageA[i].amps + '</td><td>' + arrOutageA[i].pair + '</td><td>' + arrOutageA[i].ampspair + '</td><td>' + arrOutageA[i].absum + '</td><td>' + arrOutageA[i].usage + '%</td><td>' + arrOutageA[i].oldcount + '</td><td>' + arrOutageA[i].newcount + '</td><td>' + arrOutageA[i].email + '</td></tr>';
		}

        var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
        nlapiLogExecution('DEBUG', 'Usage', '| Usage = ' + usageConsumtion + ' |');
        
		emailAdminBody += '</table><br><br>';
		emailAdminBody += 'Usage = ' + usageConsumtion;
		
		clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_pwr_alerts", emailAdminSubject, emailAdminBody);
		
		/*nlapiSendEmail(432742,71418,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Dan
		nlapiSendEmail(432742,-5,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Val
		nlapiSendEmail(432742,13091,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Phil
		nlapiSendEmail(432742,294,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Jason
		nlapiSendEmail(432742,211645,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Charles
		nlapiSendEmail(432742,179749,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Steve
		nlapiSendEmail(432742,2406,emailAdminSubject,emailAdminBody,null,null,null,null,true); // Lisa
		nlapiSendEmail(432742,1349020,emailAdminSubject,emailAdminBody,null,null,null,null,true);*/ // Lisa
		
		var status = nlapiScheduleScript('customscript_clgx_ss_dcim_pwr_amps_cust', 'customdeploy_clgx_ss_dcim_pwr_amps_cust' ,null);
		var status = nlapiScheduleScript('customscript_clgx_ss_dcim_fail_pwrs', 'customdeploy_clgx_ss_dcim_fail_pwrs' ,null);
		
	}
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}