//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_Failover.js
//	Script Name:	CLGX_SS_DCIM_PWR_Failover
//	Script Id:		customscript_clgx_ss_dcim_pwr_fail
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/20/2015
//-------------------------------------------------------------------------------------------------
function scheduled_clgx_ss_dcim_pwr_fail(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        var context = nlapiGetContext();
        var environment = context.getEnvironment()
    	
    	if(environment == 'PRODUCTION'){
    		
	        var objFile = nlapiLoadFile(3058101);
			var arrPowers = JSON.parse(objFile.getValue());
			var arrNewPowers = JSON.parse(objFile.getValue());
			
			if(arrPowers.length > 0){
				if(arrPowers.length > 500){
			        var hour = moment().hour();
			     	if(hour < 7 || hour > 18){ // before 9 AM and after 6 PM on east coast 
			     		var loopndx = 500;
			     	}
			     	else{
			     		var loopndx = 500;
			     	}
				}
				else{
					var loopndx = arrPowers.length;
				}
	
				var date = new Date();
				var startScript = moment();
		        var emailAdminSubject = '9.0 Powers failover - ' + startScript.format('M/D/YYYY');
				var emailAdminBody = '';
				emailAdminBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');
				
				emailAdminBody += '<h2>' + arrPowers.length + ' contacts processed</h2>';
				emailAdminBody += '<table border="1" cellpadding="5">';
			    emailAdminBody += '<tr><td>Power</td><td>Pair</td><td>Amps A</td><td>kW A</td><td>KW15min A</td><td>Amps A+B</td><td>kW A+B</td><td>KW15min A+B</td></tr>';
			    
			    for ( var i = 0; arrPowers != null && i < loopndx; i++ ) {
					
					var sumAMPsA = 0;
					var sumKWsA = 0;
					
					var sumAMPsAB = 0;
					var sumKWsAB = 0;
					
			        var arrSumA = new Array();
			        for ( var j = 0; j < 96; j++ ) {
			        	arrSumA[j] = 0;
			        }
			        
			        var arrSumAB = new Array();
			        for ( var j = 0; j < 96; j++ ) {
			        	arrSumAB[j] = 0;
			        }
			        
                    var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', arrPowers[i]);
                    var pairid = recPower.getFieldValue('custrecord_clgx_dcim_pair_power');
                    
                    //nlapiSendEmail(71418,71418,i,JSON.parse( arrPowers[i] ),null,null,null,null);
                    //nlapiSendEmail(71418,71418,'pairid',JSON.parse( pairid ),null,null,null,null);
                    
                    var arrColumns = new Array();
					var arrFilters = new Array();
					arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_power",null,"anyof",arrPowers[i]));
					var searchPower = nlapiSearchRecord('customrecord_clgx_dcim_points_day', 'customsearch_clgx_dcim_pwr_day_fail_vals', arrFilters, arrColumns);
					
					for ( var j = 0; searchPower != null && j < searchPower.length; j++ ) {
						
						var pwrAMPs = parseFloat(searchPower[j].getValue('custrecord_clgx_dcim_points_day_amps_avg',null,null));
						var pwrKWs =  parseFloat(searchPower[j].getValue('custrecord_clgx_dcim_points_day_kw_avg',null,null));
						
						sumAMPsA += pwrAMPs;
						sumKWsA += pwrKWs;
						
						sumAMPsAB += pwrAMPs;
						sumKWsAB += pwrKWs;
						
						var pwrKWs15min =  searchPower[j].getValue('custrecord_clgx_dcim_points_day_kw15min',null,null);
						
			            var arr15min = pwrKWs15min.split(',');
			            for ( var k = 0; k < arr15min.length; k++ ) {
			            	arrSumA[k] += parseFloat(arr15min[k]);
			            	arrSumAB[k] += parseFloat(arr15min[k]);
			            }
					}

					if(pairid > 0){
						var arrColumns = new Array();
						var arrFilters = new Array();
						arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_power",null,"anyof",pairid));
						var searchPair = nlapiSearchRecord('customrecord_clgx_dcim_points_day', 'customsearch_clgx_dcim_pwr_day_fail_vals', arrFilters, arrColumns);
						
						for ( var j = 0; searchPair != null && j < searchPair.length; j++ ) {
							
							var pairAMPs = parseFloat(searchPair[j].getValue('custrecord_clgx_dcim_points_day_amps_avg',null,null));
							var pairKWs =  parseFloat(searchPair[j].getValue('custrecord_clgx_dcim_points_day_kw_avg',null,null));
							
							sumAMPsAB += pairAMPs;
							sumKWsAB += pairKWs;
							
							var pairKWs15min =  searchPair[j].getValue('custrecord_clgx_dcim_points_day_kw15min',null,null);
							
							var arr15min = pairKWs15min.split(',');
				            for ( var k = 0; k < arr15min.length; k++ ) {
				            	arrSumAB[k] += parseFloat(arr15min[k]);
				            }
						}
					}
					
					for ( var j = 0; j < arrSumA.length; j++ ) {
						arrSumA[j] = parseFloat(arrSumA[j].toFixed(2));
					}
					for ( var j = 0; j < arrSumAB.length; j++ ) {
						arrSumAB[j] = parseFloat(arrSumAB[j].toFixed(2));
					}
					
					recPower.setFieldValue('custrecord_clgx_dcim_sum_day_amp_a', parseFloat(sumAMPsA.toFixed(2)));
					recPower.setFieldValue('custrecord_clgx_dcim_sum_day_kw_a', parseFloat(sumKWsA.toFixed(2)));
					recPower.setFieldValue('custrecord_clgx_dcim_sum_day_kw15m_a', arrSumA.join());
					
					recPower.setFieldValue('custrecord_clgx_dcim_sum_day_amp_ab', parseFloat(sumAMPsAB.toFixed(2)));
					recPower.setFieldValue('custrecord_clgx_dcim_sum_day_kw_ab', parseFloat(sumKWsAB.toFixed(2)));
					recPower.setFieldValue('custrecord_clgx_dcim_sum_day_kw15m_ab', arrSumAB.join());

					var id = nlapiSubmitRecord(recPower, false, true);
					
					emailAdminBody += '<tr><td>' + arrPowers[i] + '</td><td>' + pairid + '</td><td>' + sumAMPsA.toFixed(2) + '</td><td>' + sumKWsA.toFixed(2) + '</td><td>' + arrSumA.join() + '</td><td>' + sumAMPsAB.toFixed(2) + '</td><td>' + sumKWsAB.toFixed(2) + '</td><td>' + arrSumAB.join() + '</td></tr>';
					
			    	arrNewPowers = _.reject(arrNewPowers, function(num){
				        return (num == arrPowers[i]);
					});
			    	
			    	var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
		            var index = i + 1;
		            nlapiLogExecution('DEBUG','Results ', ' | Index = ' + index + ' of ' + arrPowers.length + ' | Usage = '+ usageConsumtion + '  | PowerID = ' + arrPowers[i] + ' |');
				}
				
				var file = nlapiCreateFile('powers_ids_queue.json', 'PLAINTEXT', JSON.stringify(arrNewPowers));
				file.setFolder(1294374);
				nlapiSubmitFile(file);
	
	// ============================ Send admin email ===============================================================================================
				
			    emailAdminBody += '</table>';
				var endScript = moment();
				emailAdminBody += '<br><br>';
				emailAdminBody += 'End : ' + endScript.format('M/D/YYYY h:mm:ss a') + '<br>';
				emailAdminBody += 'Total execution minutes : ' + (endScript.diff(startScript)/60000).toFixed(1) + '<br>';
				var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
				emailAdminBody += 'Total usage : ' + usageConsumtion;
				emailAdminBody += '<br><br>';
				if(arrPowers != null){
					emailAdminSubject += ' | '+ loopndx + ' of '+ arrPowers.length + ' powers in ' + (endScript.diff(startScript)/60000).toFixed(1) + ' mins | ';
				}
				else{
					emailAdminSubject += ' | 0 powers in ' + (endScript.diff(startScript)/60000).toFixed(1) + ' mins | ';
				}
				
				clgx_send_employee_emails_from_savedsearch("customsearch_clgx_dcim_failover_finish", emailAdminSubject, emailAdminBody);
				
				/*nlapiSendEmail(432742,71418,emailAdminSubject,emailAdminBody,null,null,null,null,true);
				nlapiSendEmail(432742,1349020,emailAdminSubject,emailAdminBody,null,null,null,null,true);*/
				
				var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
				
			}
			else{
				clgx_send_employee_emails_from_savedsearch("customsearch_clgx_dcim_failover_finish", "9.0 Finish Powers failover", "");
				
				/*nlapiSendEmail(432742,71418,'9.0 Finish Powers failover','',null,null,null,null,true);
				nlapiSendEmail(432742,1349020,'9.0 Finish Powers failover','',null,null,null,null,true);*/
				//var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_ns2sp', 'customdeploy_clgx_ss_sp_sync_ns2sp0');
			}	
		}
		nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}