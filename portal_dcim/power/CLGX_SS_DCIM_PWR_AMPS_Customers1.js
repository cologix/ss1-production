nlapiLogExecution("audit","FLOStart",new Date().getTime());
var customerID=3168;
 var arrContacts = new Array();
  var arrCustomers = new Array();
  var objCustomer = new Object();
                var arrEmails = new Array();
                var arrFiltersContacts=new Array();
                var arrColumnsContacts=new Array();
                arrFiltersContacts.push(new nlobjSearchFilter("internalid", "company", "anyof",customerID));
                var searchTechContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_tecrole", arrFiltersContacts, arrColumnsContacts);
                var searchPrimaryContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_prrole", arrFiltersContacts, arrColumnsContacts);
                var searchSecondaryContact = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_secrole", arrFiltersContacts, arrColumnsContacts);
                var searchDecisionMaker = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_dmrole", arrFiltersContacts, arrColumnsContacts);
                //  var searchOpsOutage = nlapiSearchRecord('contact', "customsearch_clgx_ssr_contact_ops", arrFiltersContacts, arrColumnsContacts);
                //search for Primary/Secondary/Tech Contacts
                if((searchTechContact!=null)||(searchPrimaryContact!=null)||(searchSecondaryContact!=null)||(searchDecisionMaker!=null))
                {

                    for ( var m = 0;searchTechContact != null && m < searchTechContact.length; m++ ) {
                        var searchTCnt = searchTechContact[m];
                        var columns = searchTCnt.getAllColumns();
                        var contactid = parseInt(searchTCnt.getValue(columns[0]));
                        var contact = searchTCnt.getText(columns[0]);
                        var email = searchTCnt.getValue(columns[3]);
                        if((_.indexOf(arrEmails, email) == -1)&&( email != '' )&& (email != null)){
                            var objContact = new Object();
                            objContact["contactid"] = contactid;
                            objContact["contact"] = contact;
                            objContact["email"] = email;
                            arrContacts.push(objContact);
arrEmails.push(email);
                        }
                    }
                    for ( var m = 0;searchPrimaryContact != null && m < searchPrimaryContact.length; m++ ) {
                        var searchPCnt = searchPrimaryContact[m];
                        var columns = searchPCnt.getAllColumns();
                        var contactid = parseInt(searchPCnt.getValue(columns[0]));
                        var contact = searchPCnt.getText(columns[0]);
                        var email = searchPCnt.getValue(columns[3]);
                        if((_.indexOf(arrEmails, email) == -1) && (email != '' )&& (email != null)){
                            var objContact = new Object();
                            objContact["contactid"] = contactid;
                            objContact["contact"] = contact;
                            objContact["email"] = email;
                            arrContacts.push(objContact);
arrEmails.push(email);
                        }
                    }
                    for ( var m = 0;searchSecondaryContact != null && m < searchSecondaryContact.length; m++ ) {
                        var searchSCnt = searchSecondaryContact[m];
                        var columns = searchSCnt.getAllColumns();
                        var contactid = parseInt(searchSCnt.getValue(columns[0]));
                        var contact = searchSCnt.getText(columns[0]);
                        var email = searchSCnt.getValue(columns[3]);
                        if((_.indexOf(arrEmails, email) == -1 )&& (email != '') &&( email != null)){
                            var objContact = new Object();
                            objContact["contactid"] = contactid;
                            objContact["contact"] = contact;
                            objContact["email"] = email;
                            arrContacts.push(objContact);
arrEmails.push(email);
                        }
                    }
                    for ( var m = 0;searchDecisionMaker!= null && m <searchDecisionMaker.length; m++ ) {
                        var searchDCnt = searchDecisionMaker[m];
                        var columns = searchDCnt.getAllColumns();
                        var contactid = parseInt(searchDCnt.getValue(columns[0]));
                        var contact = searchDCnt.getText(columns[0]);
                        var email = searchDCnt.getValue(columns[3]);
                        if((_.indexOf(arrEmails, email) == -1 )&& (email != '' )&& (email != null)){
                            var objContact = new Object();
                            objContact["contactid"] = contactid;
                            objContact["contact"] = contact;
                            objContact["email"] = email;
                            arrContacts.push(objContact);
arrEmails.push(email);
                        }
                    }

                }
objCustomer["contacts"] = arrContacts;


                arrCustomers.push(objCustomer);
var x=0;