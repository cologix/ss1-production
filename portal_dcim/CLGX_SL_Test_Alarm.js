nlapiLogExecution("audit","FLOStart",new Date().getTime());
function suitelet_dcim_framework (request, response){
    try {

        var objFile = nlapiLoadFile(3377226);
        var html = objFile.getValue();
        response.write( html );
    } 	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}