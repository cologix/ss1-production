nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Capacity_Report.js
//	Script Name:	CLGX_SU_Capacity_Report
//	Script Id:		customscript_clgx_su_capacity_report
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Project
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Date:			07/10/2014
//-------------------------------------------------------------------------------------------------

function beforeSubmit (type) {
	try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Date:	07/10/2014
// Details:	Record was edited and saved - put device in queue to update it's points
//-----------------------------------------------------------------------------------------------------------------
		var currentContext = nlapiGetContext();
		var assetid = nlapiGetRecordId();
		var userid = nlapiGetUser();
		var roleid = nlapiGetRole();

		if(currentContext.getExecutionContext() == 'userinterface' && (type == 'edit' || type == 'create')){
			if (roleid == '-5' || roleid == '3' || roleid == '18' || roleid == '1052' || userid == 2421) { // admin, full, Keith, Phil
				
				var deviceid = nlapiGetFieldValue('custrecord_clgx_dcim_cr_device');
				
				if(deviceid != null && deviceid != ''){
					//var externalid = nlapiLookupField('customrecord_clgx_dcim_devices', deviceid, 'externalid');
					
					
	        		// verify if device is in update queue
					var arrColumns = new Array();
					arrColumns.push(new nlobjSearchColumn('internalid',null,null));
					var arrFilters = new Array();
					arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",deviceid));
					arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_device_updated",null,"is",'T'));
					var searchDevicesIds = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
					
					if(searchDevicesIds == null){
						nlapiSubmitField('customrecord_clgx_dcim_devices', 'deviceid', 'custrecord_clgx_dcim_device_updated', 'T');
					}
					
				}
			}
		}

//---------- End Sections ------------------------------------------------------------------------------------------------
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}