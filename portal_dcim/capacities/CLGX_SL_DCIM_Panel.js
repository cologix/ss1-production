nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_Panel.js
//	Script Name:	CLGX_SL_DCIM_Panel
//	Script Id:		customscript_clgx_sl_dcim_panel
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		06/24/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=306&deploy=1  
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_panel (request, response){
	try {

		var famid = request.getParameter('famid');
		
		var deviceid = request.getParameter('deviceid');
		var device = nlapiLookupField('customrecord_clgx_dcim_devices', deviceid, 'name');
		var pdpm = request.getParameter('pdpm');
		var vertical = request.getParameter('vertical');

		var objFile = nlapiLoadFile(2010695);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{nameTree}','g'), device);
		html = html.replace(new RegExp('{jsonTree}','g'), getBreakersTreeJSON (deviceid,pdpm, vertical));
		response.write( html );
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function getBreakersTreeJSON (deviceid,pdpm,vertical){

	var arrPoints = new Array();
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_points_device',null,'anyof',deviceid));
	var searchPoints = nlapiSearchRecord('customrecord_clgx_dcim_points', 'customsearch_clgx_dcim_capacity_panel', arrFilters, arrColumns);
	for ( var i = 0; searchPoints != null && i < searchPoints.length; i++ ) {
		var searchPoint = searchPoints[i];
		var columns = searchPoint.getAllColumns();
		var objPoint = new Object();
		objPoint["panelid"] = parseInt(searchPoint.getValue(columns[0]));
		objPoint["panel"] = searchPoint.getText(columns[0]);
		objPoint["pointid"] = parseInt(searchPoint.getValue(columns[1]));
		objPoint["point"] = searchPoint.getValue(columns[3]);
		objPoint["module"] = parseInt(searchPoint.getValue(columns[4]));
		objPoint["breaker"] = parseInt(searchPoint.getValue(columns[5]));
		objPoint["value"] = (parseFloat(searchPoint.getValue(columns[6]))).toFixed(2);
		objPoint["unit"] = searchPoint.getValue(columns[7]);
		arrPoints.push(objPoint);
	}
	
	var arrBreakers = new Array();
	var arrPointsAmps = _.filter(arrPoints, function(arr){
	    return (arr.unit == 'Amps');
	});
	for ( var i = 0; arrPointsAmps != null && i < arrPointsAmps.length; i++ ) {
		var objBreaker = new Object();
		objBreaker["panel1id"] = arrPointsAmps[i].panelid;
		objBreaker["panel1"] = arrPointsAmps[i].panel;
		objBreaker["module1"] = arrPointsAmps[i].module;
		objBreaker["breaker1"] = arrPointsAmps[i].breaker;
		
		objBreaker["amps1pointid"] = arrPointsAmps[i].pointid;
		objBreaker["amps1point"] = arrPointsAmps[i].point;
		objBreaker["amps1value"] = parseFloat(arrPointsAmps[i].value);
		
		var objPointKW = _.find(arrPoints, function(arr){ return (arr.module == arrPointsAmps[i].module && arr.breaker == arrPointsAmps[i].breaker && arr.unit == 'kW'); });
		if(objPointKW != null){
			objBreaker["kw1pointid"] = objPointKW.pointid;
			objBreaker["kw1point"] = objPointKW.point;
			objBreaker["kw1value"] = parseFloat(objPointKW.value);
		}
		else{
			objBreaker["kw1pointid"] = 0;
			objBreaker["kw1point"] = '';
			objBreaker["kw1value"] = 0;
		}
		
		objBreaker["power1id"] = 0;
		objBreaker["power1"] = '';
		objBreaker["since1"] = '';
		objBreaker["customer1id"] = 0;
		objBreaker["customer1"] = '';
		objBreaker["service1id"] = '';
		objBreaker["service1"] = '';
		objBreaker["so1id"] = '';
		objBreaker["so1"] = '';
		objBreaker["space1id"] = '';
		objBreaker["space1"] = '';
		objBreaker["volts1"] = '';
		objBreaker["circuit1"] = '';
		objBreaker["soldamps1"] = 0;
		
		objBreaker["panel2id"] = 0;
		objBreaker["panel2"] = '';
		objBreaker["module2"] = 0;
		objBreaker["breaker2"] = 0;
		objBreaker["power2id"] = 0;
		objBreaker["power2"] = '';
		objBreaker["since2"] = '';
		objBreaker["customer2id"] = 0;
		objBreaker["customer2"] = '';
		objBreaker["service2id"] = '';
		objBreaker["service2"] = '';
		objBreaker["so2id"] = '';
		objBreaker["so2"] = '';
		objBreaker["space2id"] = '';
		objBreaker["space2"] = '';
		objBreaker["volts2"] = '';
		objBreaker["circuit2"] = '';
		objBreaker["soldamps2"] = 0;
		
		objBreaker["amps2pointid"] = 0;
		objBreaker["amps2point"] = '';
		objBreaker["amps2value"] = 0;
		objBreaker["kw2pointid"] = 0;
		objBreaker["kw2point"] = '';
		objBreaker["kw2value"] = 0;
		
		objBreaker["ampsall"] = 0;
		objBreaker["kwall"] = 0;
		objBreaker["dups"] = 0;
		objBreaker["allpowers"] = '';
		objBreaker["dupsall"] = 0;
		
		arrBreakers.push(objBreaker);
	}

	
	var arrPowers = get_powers(deviceid);
	var arrPowers1 = arrPowers[0];
	var arrPowers2 = arrPowers[1];
	
	var arrAllPowers = get_all_powers(deviceid,pdpm);
	
	for ( var i = 0; arrBreakers != null && i < arrBreakers.length; i++ ) {
		
		var objPower1 = _.find(arrPowers1, function(arr){ return (arr.ampspointid === arrBreakers[i].amps1pointid); });
		
		var nbrPowers = 0;
		var arrBreakerPowers = _.filter(arrPowers1, function(arr){
	        return (arr.ampspointid === arrBreakers[i].amps1pointid);
		});
		if (arrBreakerPowers != null){
			var arrBreakerPowersUniq = _.uniq(_.pluck(arrBreakerPowers, 'power'));
			var arrBreakerCustomerUniq = _.uniq(_.pluck(arrBreakerPowers, 'customer'));
			nbrPowers = arrBreakerPowersUniq.length;
		}
		if (objPower1 != null){
			
			arrBreakers[i].power1id = objPower1.powerid;
			if(nbrPowers > 1){
				for ( var j = 0; arrBreakerPowersUniq != null && j < arrBreakerPowersUniq.length; j++ ) {
					arrBreakers[i].power1 += arrBreakerPowersUniq[j] + '<br>';
				}
				for ( var j = 0; arrBreakerCustomerUniq != null && j < arrBreakerCustomerUniq.length; j++ ) {
					arrBreakers[i].customer1 += arrBreakerCustomerUniq[j] + '<br>';
				}
				arrBreakers[i].dups = nbrPowers;
			}
			else{
				arrBreakers[i].power1 = objPower1.power;
			}
			var arrServiceName = ((objPower1.service).split(" : "));
			var len = arrServiceName.length;

			arrBreakers[i].since1 = objPower1.since;
			arrBreakers[i].customer1id = objPower1.customerid;
			arrBreakers[i].customer1 = arrServiceName[len-2]; // the name of customer but not the parent
			arrBreakers[i].service1id = objPower1.serviceid;
			arrBreakers[i].service1 = _.last((objPower1.service).split(" : "));
			arrBreakers[i].so1id = objPower1.soid;
			arrBreakers[i].so1 = (objPower1.so).replace("Service Order #", "");
			arrBreakers[i].space1id = objPower1.spaceid;
			arrBreakers[i].space1 = objPower1.space;
			arrBreakers[i].volts1 = objPower1.volts;
			arrBreakers[i].circuit1 = objPower1.circuit;
			arrBreakers[i].soldamps1 = objPower1.soldamps;
			arrBreakers[i].amps1pointid = objPower1.ampspointid;
			arrBreakers[i].amps1point = objPower1.ampspoint;
			arrBreakers[i].amps1value = parseFloat(objPower1.ampsavg);
			arrBreakers[i].ampsall = parseFloat(objPower1.ampsavg);
			
			var arrAllPowersBreaker = _.filter(arrAllPowers, function(arr){
				return (arr.module == arrBreakers[i].module1 && arr.breaker == arrBreakers[i].breaker1);
			});
			if(arrAllPowersBreaker != null){
				for ( var j = 0; arrAllPowersBreaker != null && j < arrAllPowersBreaker.length; j++ ) {
					arrBreakers[i].allpowers += arrAllPowersBreaker[j].power + '<br>';
				}
				arrBreakers[i].dupsall = arrAllPowersBreaker.length;
			}
			else{
				arrBreakers[i].dupsall = 0;
			}
			
			
			if(objPower1.power2id > 0){
				var objPower2 = _.find(arrPowers2, function(arr){ return (arr.powerid === objPower1.power2id); });
				if (objPower2 != null){
					arrBreakers[i].panel2id = objPower2.panelid;
					arrBreakers[i].panel2 = objPower2.panel;
					arrBreakers[i].power2id = objPower2.powerid;
					arrBreakers[i].power2 = objPower2.power;
					arrBreakers[i].since2 = objPower2.since;
					arrBreakers[i].customer2id = objPower2.customerid;
					arrBreakers[i].customer2 = objPower2.customer;
					arrBreakers[i].service2id = objPower2.serviceid;
					arrBreakers[i].service2 = _.last((objPower2.service).split(" : "));
					arrBreakers[i].so2id = objPower2.soid;
					arrBreakers[i].so2 = (objPower2.so).replace("Service Order #", "");
					arrBreakers[i].space2id = objPower1.spaceid;
					arrBreakers[i].space2 = objPower1.space;
					arrBreakers[i].volts2 = objPower2.volts;
					arrBreakers[i].circuit2 = objPower2.circuit;
					arrBreakers[i].soldamps2 = objPower2.soldamps;
					arrBreakers[i].amps2pointid = objPower2.ampspointid;
					arrBreakers[i].amps2point = objPower2.ampspoint;
					arrBreakers[i].amps2value = parseFloat(objPower2.ampsavg);
					arrBreakers[i].ampsall += parseFloat(objPower2.ampsavg);
				}
			}
			else{
				arrPoints[i].ampsall = 0;
				arrPoints[i].kwall = 0;
			}
		}
	}
	
	var device = nlapiLookupField('customrecord_clgx_dcim_devices', deviceid, 'name');
	
	var objTree = new Object();
	objTree["text"] = '.';
	
	var arrPanels = new Array();
	var objPanel = new Object();
	objPanel["nodeid"] = deviceid;
	objPanel["node"] = device;
	objPanel["expanded"] = true;
	objPanel["iconCls"] = 'device';
	objPanel["leaf"] = false;
	
	var arrModulesIDs = _.uniq(_.pluck(arrBreakers, 'module1'));
	
	var arrModules = new Array();
	for ( var i = 0; arrModulesIDs != null && i < arrModulesIDs.length; i++ ) {
		
		var objModule = new Object();
		objModule["nodeid"] = arrModulesIDs[i];
		objModule["node"] = 'Module ' + arrModulesIDs[i];
		objModule["expanded"] = true;
		objModule["iconCls"] = 'module';
		objModule["leaf"] = false;
		
		var arrModuleBreakers = _.filter(arrBreakers, function(arr){
	        return (arr.module1 == arrModulesIDs[i]);
		});
		var arrBreakersIDs = _.uniq(_.pluck(arrModuleBreakers, 'breaker1'));
		
		if(arrBreakersIDs != null){
			
			if(pdpm == 0){
				var arrOddsEvens = new Array();
				var arrOdds = new Array();
				var objOdd = new Object();
				objOdd["nodeid"] = 0;
				objOdd["node"] = 'Odd Breakers';
				objOdd["expanded"] = true;
				objOdd["iconCls"] = 'module';
				objOdd["leaf"] = false;
				var arrEvens = new Array();
				var objEven = new Object();
				objEven["nodeid"] = 0;
				objEven["node"] = 'Even Breakers';
				objEven["expanded"] = true;
				objEven["iconCls"] = 'module';
				objEven["leaf"] = false;
			}

			var arrBreakerPoints = new Array();
			for ( var j = 0; arrBreakersIDs != null && j < arrBreakersIDs.length; j++ ) {
				
				var odeven = j % 2;
			
				var objPoint = _.find(arrBreakers, function(arr){ return (arr.module1 == arrModulesIDs[i] && arr.breaker1 == arrBreakersIDs[j]); });
				if(objPoint != null){
					var objBreaker = new Object();
					objBreaker["nodeid"] = arrBreakersIDs[j];
					objBreaker["node"] = 'Breaker ' + arrBreakersIDs[j];
	
					objBreaker["amps1pointid"] = objPoint.amps1pointid;
					objBreaker["amps1point"] = objPoint.amps1point;
					objBreaker["amps1value"] = objPoint.amps1value;
			
					objBreaker["power1id"] = objPoint.power1id;
					objBreaker["power1"] = objPoint.power1;
					objBreaker["since1"] = objPoint.since1;
					objBreaker["customer1id"] = objPoint.customer1id;
					objBreaker["customer1"] = objPoint.customer1;
					objBreaker["service1id"] = objPoint.service1id;
					objBreaker["service1"] = objPoint.service1;
					objBreaker["so1id"] = objPoint.so1id;
					objBreaker["so1"] = objPoint.so1;
					objBreaker["space1id"] = objPoint.space1id;
					objBreaker["space1"] = objPoint.space1;
					objBreaker["volts1"] = objPoint.volts1;
					objBreaker["circuit1"] = objPoint.circuit1;
					objBreaker["soldamps1"] = objPoint.soldamps1;
					objBreaker["amps1pointid"] = objPoint.amps1pointid;
					objBreaker["amps1point"] = objPoint.amps1point;
					objBreaker["amps1value"] = objPoint.amps1value;
					
					objBreaker["panel2id"] = objPoint.panel2id;
					objBreaker["panel2"] = objPoint.panel2;
					objBreaker["module2"] = objPoint.module2;
					objBreaker["breaker2"] = objPoint.breaker2;
					objBreaker["power2id"] = objPoint.power2id;
					objBreaker["power2"] = objPoint.power2;
					objBreaker["since2"] = objPoint.since2;
					objBreaker["customer2id"] = objPoint.customer2id;
					objBreaker["customer2"] = objPoint.customer2;
					objBreaker["service2id"] = objPoint.service2id;
					objBreaker["service2"] = objPoint.service2;
					objBreaker["so2id"] = objPoint.so2id;
					objBreaker["so2"] = objPoint.so2;
					objBreaker["space2id"] = objPoint.space2id;
					objBreaker["space2"] = objPoint.space2;
					objBreaker["volts2"] = objPoint.volts2;
					objBreaker["circuit2"] = objPoint.circuit2;
					objBreaker["soldamps2"] = objPoint.soldamps2;
					objBreaker["amps2pointid"] = objPoint.amps2pointid;
					objBreaker["amps2point"] = objPoint.amps2point;
					objBreaker["amps2value"] = objPoint.amps2value;

					var allamps = (objPoint.ampsall).toFixed(2);
					objBreaker["ampsall"] = parseFloat(allamps);
					objBreaker["utilisation"] = parseFloat(allamps);

					objBreaker["dups"] = objPoint.dups;
					objBreaker["allpowers"] = objPoint.allpowers;
					objBreaker["dupsall"] = objPoint.dupsall;
					
					objBreaker["iconCls"] = 'breaker';
					objBreaker["leaf"] = true;
					if(pdpm == 0){
						if(odeven == 0){
							arrOdds.push(objBreaker);
						}
						else{
							arrEvens.push(objBreaker);
						}
					}
					else{
						arrBreakerPoints.push(objBreaker);
					}
				}
			}
			if(pdpm == 0){
				
				objOdd["children"] = arrOdds;
				arrOddsEvens.push(objOdd);
				
				objEven["children"] = arrEvens;
				arrOddsEvens.push(objEven);
				
				objModule["children"] = arrOddsEvens;
			}
			else{
				objModule["children"] = arrBreakerPoints;
			}
		}
		arrModules.push(objModule);
	}
	objPanel["children"] = arrModules;
	arrPanels.push(objPanel);
	objTree["children"] = arrPanels;

	return JSON.stringify(objTree);
}


function get_powers (deviceid){
	
	// search all powers, including pairs
	var arrPowers1IDs = new Array();
	var arrPowers2IDs = new Array();
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('parent','custrecord_cologix_power_service',null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_device",null,"anyof",deviceid));
	var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_panels_pwr', arrFilters, arrColumns);
	for ( var i = 0; searchPowers != null && i < searchPowers.length; i++ ) {
        var powerid = searchPowers[i].getValue('internalid',null,null);
        if(_.indexOf(arrPowers1IDs, powerid) < 0){
        	arrPowers1IDs.push(parseInt(powerid));
        }
        var pairid = searchPowers[i].getValue('custrecord_clgx_dcim_pair_power',null,null);
        if(pairid != null && pairid != '' && _.indexOf(arrPowers2IDs, pairid) < 0){
        	arrPowers2IDs.push(parseInt(pairid));
        }
	}
	
	// Build Powers Array ------------------------------------------------------------------------------------------------------------------------------------------------
	var arrPowers1 = new Array();
	if(arrPowers1IDs.length > 0){
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('parent','custrecord_cologix_power_service',null));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrPowers1IDs));
		var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_panels_powers');
		searchPowers.addColumns(arrColumns);
		searchPowers.addFilters(arrFilters);
		var resultSet = searchPowers.runSearch();
		resultSet.forEachResult(function(searchResult) {
			var columns = searchResult.getAllColumns();
			var objPower = new Object();
			
			objPower["panelid"] = parseInt(searchResult.getValue('custrecord_clgx_od_fam_device','CUSTRECORD_CLGX_POWER_PANEL_PDPM',null));
			objPower["panel"] = searchResult.getText('custrecord_clgx_od_fam_device','CUSTRECORD_CLGX_POWER_PANEL_PDPM',null);
			
			objPower["module"] = parseInt(searchResult.getValue(columns[9]));
			objPower["breaker"] = parseInt(searchResult.getValue(columns[10]));
			
			objPower["powerid"] = parseInt(searchResult.getValue('internalid',null,null));
			objPower["power"] = searchResult.getValue('name',null,null);
			objPower["since"] = searchResult.getValue('custrecord_clgx_dcim_date_provision',null,null);
			
			objPower["power2id"] = parseInt(searchResult.getValue('custrecord_clgx_dcim_pair_power',null,null));
			objPower["power2"] = searchResult.getText('custrecord_clgx_dcim_pair_power',null,null);
			
			objPower["spaceid"] = parseInt(searchResult.getValue('custrecord_cologix_power_space',null,null));
			objPower["space"] = searchResult.getText('custrecord_cologix_power_space',null,null);
									
			objPower["customerid"] = parseInt(searchResult.getValue('parent','custrecord_cologix_power_service',null));
			objPower["customer"] = searchResult.getText('parent','custrecord_cologix_power_service',null);
			
			objPower["serviceid"] = parseInt(searchResult.getValue('custrecord_cologix_power_service',null,null));
			objPower["service"] = searchResult.getText('custrecord_cologix_power_service',null,null);
			
			objPower["soid"] = parseInt(searchResult.getValue('custrecord_power_circuit_service_order',null,null));
			objPower["so"] = searchResult.getText('custrecord_power_circuit_service_order',null,null);
			
			var volts = searchResult.getText('custrecord_cologix_power_volts',null,null);
			objPower["volts"] = volts;
			
			var circuit = searchResult.getText('custrecord_cologix_power_amps',null,null);
			var soldamps = circuit.substring(0, circuit.length - 1);
			if(circuit != null && circuit != ''){
				objPower["circuit"] = circuit;
				objPower["soldamps"] = parseInt(circuit.substring(0, circuit.length - 1)) * 0.8
			}
			else{
				objPower["circuit"] = '';
				objPower["soldamps"] = 0;
			}

			objPower["ampspointid"] = parseInt(searchResult.getValue('internalid','custrecord_clgx_dcim_points_current',null));
			objPower["ampsextid"] = parseInt(searchResult.getValue('externalid','custrecord_clgx_dcim_points_current',null));
			objPower["ampspoint"] = searchResult.getValue('name','custrecord_clgx_dcim_points_current',null);
			objPower["ampsavg"] = (parseFloat(searchResult.getValue('custrecord_clgx_dcim_points_day_avg','custrecord_clgx_dcim_points_current',null))).toFixed(2);
			
			arrPowers1.push(objPower);
			return true;
		});
	}
	
	var arrPowers2 = new Array();
	if(arrPowers2IDs.length > 0){
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('parent','custrecord_cologix_power_service',null));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrPowers2IDs));
		var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_panels_powers');
		searchPowers.addColumns(arrColumns);
		searchPowers.addFilters(arrFilters);
		var resultSet = searchPowers.runSearch();
		resultSet.forEachResult(function(searchResult) {
			var columns = searchResult.getAllColumns();
			var objPower = new Object();
			
			objPower["panelid"] = parseInt(searchResult.getValue('custrecord_clgx_od_fam_device','CUSTRECORD_CLGX_POWER_PANEL_PDPM',null));
			objPower["panel"] = searchResult.getText('custrecord_clgx_od_fam_device','CUSTRECORD_CLGX_POWER_PANEL_PDPM',null);
			
			objPower["module"] = parseInt(searchResult.getValue(columns[9]));
			objPower["breaker"] = parseInt(searchResult.getValue(columns[10]));
			
			objPower["powerid"] = parseInt(searchResult.getValue('internalid',null,null));
			objPower["power"] = searchResult.getValue('name',null,null);
			objPower["since"] = searchResult.getValue('custrecord_clgx_dcim_date_provision',null,null);
			
			objPower["power2id"] = parseInt(searchResult.getValue('custrecord_clgx_dcim_pair_power',null,null));
			objPower["power2"] = searchResult.getText('custrecord_clgx_dcim_pair_power',null,null);
			
			objPower["spaceid"] = parseInt(searchResult.getValue('custrecord_cologix_power_space',null,null));
			objPower["space"] = searchResult.getText('custrecord_cologix_power_space',null,null);
						
			objPower["customerid"] = parseInt(searchResult.getValue('parent','custrecord_cologix_power_service',null));
			objPower["customer"] = searchResult.getText('parent','custrecord_cologix_power_service',null);
			
			objPower["serviceid"] = parseInt(searchResult.getValue('custrecord_cologix_power_service',null,null));
			objPower["service"] = searchResult.getText('custrecord_cologix_power_service',null,null);
			
			objPower["soid"] = parseInt(searchResult.getValue('custrecord_power_circuit_service_order',null,null));
			objPower["so"] = searchResult.getText('custrecord_power_circuit_service_order',null,null);
			
			var volts = searchResult.getText('custrecord_cologix_power_volts',null,null);
			objPower["volts"] = volts;
			
			var circuit = searchResult.getText('custrecord_cologix_power_amps',null,null);
			var soldamps = circuit.substring(0, circuit.length - 1);
			if(circuit != null && circuit != ''){
				objPower["circuit"] = circuit;
				objPower["soldamps"] = parseInt(circuit.substring(0, circuit.length - 1)) * 0.8
			}
			else{
				objPower["circuit"] = '';
				objPower["soldamps"] = 0;
			}

			objPower["ampspointid"] = parseInt(searchResult.getValue('internalid','custrecord_clgx_dcim_points_current',null));
			objPower["ampsextid"] = parseInt(searchResult.getValue('externalid','custrecord_clgx_dcim_points_current',null));
			objPower["ampspoint"] = searchResult.getValue('name','custrecord_clgx_dcim_points_current',null);
			objPower["ampsavg"] = (parseFloat(searchResult.getValue('custrecord_clgx_dcim_points_day_avg','custrecord_clgx_dcim_points_current',null))).toFixed(2);

			arrPowers2.push(objPower);
			return true;
		});
	}
	
	var arrPowers = new Array();
	arrPowers.push(arrPowers1);
	arrPowers.push(arrPowers2);
	
	return arrPowers;
}


function get_all_powers (deviceid, pdpm){
	
	var arrPowers = new Array();

	var famid = nlapiLookupField('customrecord_clgx_dcim_devices', deviceid, 'custrecord_clgx_dcim_device_fam');
	
	if(famid != null && famid != ''){
		//var pdpm = nlapiLookupField('customrecord_ncfar_asset', famid, 'custrecord_clgx_fam_pdpm');
		
		if(pdpm == 0){
	
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_power_panel_pdpm",null,"anyof",famid));
			var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pwrs_dups_others');
			searchPowers.addFilters(arrFilters);
			var resultSet = searchPowers.runSearch();
			resultSet.forEachResult(function(searchResult) {
				var columns = searchResult.getAllColumns();
				
				var voltage = searchResult.getText(columns[5]);
				var breaker = parseInt(searchResult.getValue(columns[4]));
				
				// these voltages  - only one breaker
				if(voltage == '120V Single Phase' || voltage == '240V Single Phase'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = 1;
					objPower["breaker"] = breaker;
					arrPowers.push(objPower);
				}
				// this voltages - 2 breakers
				else if (voltage == '208V Single Phase'){
					for ( var i = 0; i < 2; i++ ) {
						var objPower = new Object();
						objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
						objPower["fam"] = searchResult.getText(columns[0]);
						objPower["famname"] = searchResult.getValue(columns[1]);
						objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
						objPower["power"] = searchResult.getValue(columns[3]);
						objPower["module"] = 1;
						objPower["breaker"] = breaker + i*2;
						arrPowers.push(objPower);
					}
				}
				// these voltages - 3 breakers
				else if (voltage == '208V Three Phase' || voltage == '240V Three Phase'){
					for ( var i = 0;  i < 3; i++ ) {
						var objPower = new Object();
						objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
						objPower["fam"] = searchResult.getText(columns[0]);
						objPower["famname"] = searchResult.getValue(columns[1]);
						objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
						objPower["power"] = searchResult.getValue(columns[3]);
						objPower["module"] = 1;
						objPower["breaker"] = breaker + i*2;
						arrPowers.push(objPower);
					}
				}
				else{
				}
				return true;
			});
			
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_power_panel_pdpm",null,"anyof",famid));
			var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pwrs_dups_vertic');
			searchPowers.addFilters(arrFilters);
			var resultSet = searchPowers.runSearch();
			resultSet.forEachResult(function(searchResult) {
				var columns = searchResult.getAllColumns();
				
				var voltage = searchResult.getText(columns[5]);
				var breaker = parseInt(searchResult.getValue(columns[4]));
				
				// these voltages  - only one breaker
				if(voltage == '120V Single Phase' || voltage == '240V Single Phase'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = 1;
					objPower["breaker"] = breaker;
					arrPowers.push(objPower);
				}
				// this voltages - 2 breakers
				else if (voltage == '208V Single Phase'){
					for ( var i = 0; i < 2; i++ ) {
						var objPower = new Object();
						objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
						objPower["fam"] = searchResult.getText(columns[0]);
						objPower["famname"] = searchResult.getValue(columns[1]);
						objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
						objPower["power"] = searchResult.getValue(columns[3]);
						objPower["module"] = 1;
						objPower["breaker"] = breaker + i;
						arrPowers.push(objPower);
					}
				}
				// these voltages - 3 breakers
				else if (voltage == '208V Three Phase' || voltage == '240V Three Phase'){
					for ( var i = 0;  i < 3; i++ ) {
						var objPower = new Object();
						objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
						objPower["fam"] = searchResult.getText(columns[0]);
						objPower["famname"] = searchResult.getValue(columns[1]);
						objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
						objPower["power"] = searchResult.getValue(columns[3]);
						objPower["module"] = 1;
						objPower["breaker"] = breaker + i;
						arrPowers.push(objPower);
					}
				}
				else{
				}
				return true;
			});

		
		}
		else{
			
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_power_panel_pdpm",null,"anyof",famid));
			var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pwrs_dups_pdpm');
			searchPowers.addFilters(arrFilters);
			var resultSet = searchPowers.runSearch();
			resultSet.forEachResult(function(searchResult) {
				var columns = searchResult.getAllColumns();
				
				var voltage = searchResult.getText(columns[5]);
				var breaker = parseInt(searchResult.getValue(columns[4]));
				var module = parseInt(searchResult.getText(columns[6]));
				var lines = searchResult.getText(columns[7]);
	
				if(lines == 'Line 1'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 1;
					arrPowers.push(objPower);
				}
				if(lines == 'Line 2'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 2;
					arrPowers.push(objPower);
				}
				if(lines == 'Line 3'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 3;
					arrPowers.push(objPower);
				}
				if(lines == 'Lines 1, 2'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 1;
					arrPowers.push(objPower);
					
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 2;
					arrPowers.push(objPower);
				}
				if(lines == 'Lines 2, 3'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 2;
					arrPowers.push(objPower);
					
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 3;
					arrPowers.push(objPower);
				}
				if(lines == 'Lines 1, 3'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 1;
					arrPowers.push(objPower);
					
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 3;
					arrPowers.push(objPower);
				}
				if(lines == 'Lines 1, 2, 3'){
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 1;
					arrPowers.push(objPower);
					
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 2;
					arrPowers.push(objPower);
					
					var objPower = new Object();
					objPower["famid"] = parseInt(searchResult.getValue(columns[0]));
					objPower["fam"] = searchResult.getText(columns[0]);
					objPower["famname"] = searchResult.getValue(columns[1]);
					objPower["powerid"] = parseInt(searchResult.getValue(columns[2]));
					objPower["power"] = searchResult.getValue(columns[3]);
					objPower["module"] = module;
					objPower["breaker"] = 3;
					arrPowers.push(objPower);
				}
	
				return true;
			});
	
		}
	}

	return arrPowers;
}
