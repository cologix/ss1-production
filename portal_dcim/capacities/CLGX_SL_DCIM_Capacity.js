nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_Capacity.js
//	Script Name:	CLGX_SL_DCIM_Capacity
//	Script Id:		customscript_clgx_sl_dcim_capacity
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		06/11/2014
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_fams_capacity (request, response){
	try {
		
		var arrStrRoot = request.getParameter('root').split(',');
		var arrRequestIDs = new Array();
		for ( var i = 0; arrStrRoot != null && i < arrStrRoot.length; i++ ) {
			arrRequestIDs.push(parseInt(arrStrRoot[i]));
		}
		
		var objFile = nlapiLoadFile(1297970);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{dataChart}','g'), get_org_chart (arrRequestIDs));
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function get_org_chart (arrRequestIDs){
	// get all children devices for the requested root devices
	var arrDevices = get_devices (arrRequestIDs);
	var arrDevicesIDs = _.compact(_.pluck(arrDevices, 'deviceid'));
	
	// construct the orgChart
	var arrData = new Array();
	var arrNode = new Array();
    arrNode[0] = 'Child';
    arrNode[1] = 'Parent';
    arrData.push(arrNode);

	for ( var i = 0; arrDevices != null && i < arrDevices.length; i++ ) {
		var arrNode = new Array();
	    arrNode[0] = get_node_html (arrDevices[i]);
	    var objParent = _.find(arrDevices, function(arr){ return (arr.deviceid == arrDevices[i].parentid); });
	    if(objParent != null){
	    	arrNode[1] = get_node_html (objParent);
	    }
	    else{
	    	arrNode[1] = '';
	    }
	    arrData.push(arrNode);
	}
	return JSON.stringify(arrData);
}

function get_devices (arrRequestIDs){

	var arrDevices = new Array();
	var searchDevices = nlapiLoadSearch('customrecord_clgx_dcim_devices', 'customsearch_clgx_dcim_rpt_cap_devices');
	var resultSet = searchDevices.runSearch();
	resultSet.forEachResult(function(searchResult) {
		var columns = searchResult.getAllColumns();
		var objDevice = new Object();
		objDevice["parentid"] = parseInt(searchResult.getValue('custrecord_clgx_dcim_device_parent',null,null));
		objDevice["nodeid"] = parseInt(searchResult.getValue('internalid',null,null));
		arrDevices.push(objDevice);
		return true;
	});
	var arrDevicesIDs = _.pluck(arrDevices, 'nodeid');

	// extract only the requested tree nodes ---------------------------------------------------------------------------------------------------------------------------------
	var arrIDs = new Array();
	var arrRootNodes = _.filter(arrDevices, function(arr){
        return (arrRequestIDs.indexOf(arr.nodeid) > -1);
	});
	for ( var i = 0; arrRootNodes != null && i < arrRootNodes.length; i++ ) {
		arrIDs.push(arrRootNodes[i].nodeid);
		var arrNodes1 = _.filter(arrDevices, function(arr){
	        return (arr.parentid === arrRootNodes[i].nodeid);
		});
		for ( var j = 0; arrNodes1 != null && j < arrNodes1.length; j++ ) {
			arrIDs.push(arrNodes1[j].nodeid);
			var arrNodes2 = _.filter(arrDevices, function(arr){
    	        return (arr.parentid === arrNodes1[j].nodeid);
    		});
    		for ( var k = 0; arrNodes2 != null && k < arrNodes2.length; k++ ) {
    			arrIDs.push(arrNodes2[k].nodeid);
    			var arrNodes3 = _.filter(arrDevices, function(arr){
        	        return (arr.parentid === arrNodes2[k].nodeid);
        		});
        		for ( var l = 0; arrNodes3 != null && l < arrNodes3.length; l++ ) {
        			arrIDs.push(arrNodes3[l].nodeid);
        			var arrNodes4 = _.filter(arrDevices, function(arr){
            	        return (arr.parentid === arrNodes3[l].nodeid);
            		});
            		for ( var m = 0; arrNodes4 != null && m < arrNodes4.length; m++ ) {
            			arrIDs.push(arrNodes4[m].nodeid);
            			var arrNodes5 = _.filter(arrDevices, function(arr){
                	        return (arr.parentid === arrNodes4[m].nodeid);
                		});
                		for ( var n = 0; arrNodes5 != null && n < arrNodes5.length; n++ ) {
                			arrIDs.push(arrNodes5[n].nodeid);
                			/*
                			var arrNodes6 = _.filter(arrDevices, function(arr){
                    	        return (arr.parentid === arrNodes5[n].nodeid);
                    		});
                    		for ( var o = 0; arrNodes6 != null && o < arrNodes6.length; n++ ) {
                    			arrIDs.push(arrNodes6[o].nodeid);
                    			var arrNodes7 = _.filter(arrDevices, function(arr){
                        	        return (arr.parentid === arrNodes6[o].nodeid);
                        		});
                        		for ( var p = 0; arrNodes7 != null && p < arrNodes7.length; n++ ) {
                        			arrIDs.push(arrNodes7[p].nodeid);
                        			
                        			// add more levels here
                        			
                        		}
                    		}
                    		*/
                		}
            		}
        		}
    		}
		}
	}
	
	// build and return the devices grid array ---------------------------------------------------------------------------------------------------------------------------------
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('name',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_parent',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_pair',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_virtual',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_fam',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_category',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_panel_type','custrecord_clgx_dcim_device_fam',null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_avg','custrecord_clgx_dcim_device_load',null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_load_ow',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_avg','custrecord_clgx_dcim_device_current',null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_current_ow',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_capacity',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_failover',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_failover_ow',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_has_powers',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_day_kw_a',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_day_kw_ab',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrIDs));
	arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	var searchDevices = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
	
	var arrDevices = new Array();
	for ( var i = 0; searchDevices != null && i < searchDevices.length; i++ ) {
		var objDevice = new Object();
		objDevice["index"] = i;
		objDevice["parentid"] = parseInt(searchDevices[i].getValue('custrecord_clgx_dcim_device_parent',null,null));
		objDevice["parent"] = searchDevices[i].getText('custrecord_clgx_dcim_device_parent',null,null);
		objDevice["deviceid"] = parseInt(searchDevices[i].getValue('internalid',null,null));
		objDevice["device"] = searchDevices[i].getValue('name',null,null);
		objDevice["pairid"] = parseInt(searchDevices[i].getValue('custrecord_clgx_dcim_device_pair',null,null));
		objDevice["pair"] = searchDevices[i].getText('custrecord_clgx_dcim_device_pair',null,null);
		objDevice["virtual"] = searchDevices[i].getValue('custrecord_clgx_dcim_device_virtual',null,null);
		objDevice["famid"] = parseInt(searchDevices[i].getValue('custrecord_clgx_dcim_device_fam',null,null));
		
		var virtual = searchDevices[i].getValue('custrecord_clgx_dcim_device_virtual',null,null);
		var categoryid = parseInt(searchDevices[i].getValue('custrecord_clgx_dcim_device_category',null,null)) || 0;
		var category = searchDevices[i].getText('custrecord_clgx_dcim_device_category',null,null) || '';
		objDevice["categoryid"] = categoryid;
		objDevice["category"] = category;
		
		var capacity = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_device_capacity',null,null)) || 0;
		objDevice["capacity"] = capacity;
		
		var kwsab = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_device_day_kw_ab',null,null)) || 0;
		objDevice["failow"] = kwsab;
		objDevice["kwsab"] = kwsab;
		
		var load = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_points_day_avg','custrecord_clgx_dcim_device_load',null)) || 0;
		objDevice["load"] = load;


		if(categoryid == 1 || categoryid == 3){
			var available = capacity - kwsab;
			objDevice["available"] = available;
		} 
		else if (categoryid > 0 && categoryid != 1 && categoryid != 3) {
			var available = capacity - load;
			objDevice["available"] = available;
		} 
		else {
			objDevice["available"] = 0;
		}
		
		arrDevices.push(objDevice);
	}

	return arrDevices;
}

function get_node_html(objDevice){
    
	var device = objDevice.device;
	var deviceid = objDevice.deviceid;
	var capacity = round(objDevice.capacity);
	var load = round(objDevice.load);
	//var current = round(objDevice.current);
	//var fail = round(objDevice.fail);
	var failow = round(objDevice.failow);
	
	var available = round(objDevice.available);
	//var available = round(objDevice.capacity - objDevice.failow);
	var virtual = objDevice.virtual;

	var virtualcss = '';
	if(virtual == 'T'){
		virtualcss = ' class="virtual"';
	}
	
	var html = '';
	html += '<table align="center" width="100%"' + virtualcss + '><tr class="bb2 ac hd0">';
	html += '<td colspan="2" nowrap><a href="/app/common/custom/custrecordentry.nl?rectype=218&id=' + deviceid + '" " target="_blank">' + device + '</a></td>';
	html += '</tr><tr class="bb" class="bb">';
	html += '<td nowrap class="al">Derated Capacity</td>';
	html += '<td class="ar">' + capacity + '</td>';
	if(load > 0){
		html += '</tr><tr class="bb">';
		html += '<td nowrap class="al">Actual Load (OD)</td>';
		html += '<td class="ar">' + load + '</td>';
	}
	/*
	if(current > 0){
		html += '</tr><tr class="bb">';
		html += '<td nowrap class="al">Current</td>';
		html += '<td class="ar">' + current + '</td>';
	}
	*/
	html += '</tr><tr class="bb hd1">';
	html += '<td nowrap class="al">Load w/ Failover</td>';
	html += '<td class="ar">' + failow + '</td>';
	if(available > 0){
		html += '</tr><tr class="bb hd2">';
	}
	else{
		html += '</tr><tr class="bb hd3">';
	}
	html += '<td nowrap class="al">Available Capacity</td>';
	html += '<td class="ar">' + available + '</td>';
	html += '</tr>';
	html += '</table>';

	return html;
}


function round(value) {
	  return Number(Math.round(value+'e'+2)+'e-'+2);
}




/*
function get_panel (deviceid, device){
	
	// Build Points Array ------------------------------------------------------------------------------------------------------------------------------------------------
	var arrPoints = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_device",null,"anyof",deviceid));
	var searchPoints = nlapiLoadSearch('customrecord_clgx_dcim_points', 'customsearch_clgx_dcim_panel_points');
	searchPoints.addFilters(arrFilters);
	var resultSet = searchPoints.runSearch();
	resultSet.forEachResult(function(searchResult) {
		var columns = searchResult.getAllColumns();
		var objPoint = new Object();
		objPoint["deviceid"] = parseInt(searchResult.getValue(columns[0]));
		objPoint["device"] = searchResult.getText(columns[0]);
		objPoint["pointid"] = parseInt(searchResult.getValue(columns[1]));
		objPoint["point"] = searchResult.getValue(columns[2]);
		objPoint["module"] = parseInt(searchResult.getValue(columns[3]));
		objPoint["breaker"] = parseInt(searchResult.getValue(columns[4]));
		objPoint["phase"] = searchResult.getValue(columns[5]);
		
		objPoint["power1id"] = 0;
		objPoint["power1"] = '';
		objPoint["service1"] = '';
		objPoint["volts1"] = '';
		objPoint["circuit1"] = '';
		objPoint["soldamps1"] = 0;
		objPoint["amps1"] = parseFloat(searchResult.getValue(columns[6]));
		
		objPoint["power2id"] = 0;
		objPoint["power2"] = '';
		objPoint["service2"] = '';
		objPoint["volts2"] = '';
		objPoint["circuit2"] = '';
		objPoint["soldamps2"] = 0;
		objPoint["amps2"] = 0;
		
		objPoint["ampsall"] = 0;
		
		arrPoints.push(objPoint);
		return true;
	});
	
	var arrPowers = get_powers(deviceid);
	
	var arrPowers1 = arrPowers[0];
	var arrPowers2 = arrPowers[1];
	
	for ( var i = 0; arrPoints != null && i < arrPoints.length; i++ ) {
		
		var objPower1 = _.find(arrPowers1, function(arr){ return (arr.pointid === arrPoints[i].pointid); });
		if (objPower1 != null){
			arrPoints[i].power1id = objPower1.powerid;
			arrPoints[i].power1 = objPower1.power;
			arrPoints[i].service1 = objPower1.service;
			arrPoints[i].volts1 = objPower1.volts;
			arrPoints[i].circuit1 = objPower1.circuit;
			arrPoints[i].soldamps1 = objPower1.soldamps;
			arrPoints[i].ampsall = objPower1.amps1;
			
			if(objPower1.power2id > 0){
				var objPower2 = _.find(arrPowers2, function(arr){ return (arr.powerid === objPower1.power2id); });
				if (objPower2 != null){
					arrPoints[i].power2id = objPower2.powerid;
					arrPoints[i].power2 = objPower2.power;
					arrPoints[i].service2 = objPower2.service;
					arrPoints[i].volts2 = objPower2.volts;
					arrPoints[i].circuit2 = objPower2.circuit;
					arrPoints[i].amps2 = objPower2.amps1;
					arrPoints[i].soldamps2 = objPower2.soldamps;
					arrPoints[i].ampsall += objPower2.amps1;
				}
			}
			else{
				arrPoints[i].ampsall = 0;
			}
		}
	}
	
	
	// calculate totals amps and KW
	var totalAmps1 = 0;
	var totalAmps2 = 0;
	var totalAmps3 = 0;
	var totalAmps1ABC = [0,0,0];
	var totalAmps2ABC = [0,0,0];
	var totalAmps3ABC = [0,0,0];
	var totalAmpsAll = [0,0,0];
	
	for ( var i = 0; arrPoints != null && i < arrPoints.length; i++ ) {
		if(arrPoints[i].amps1 > 0){
			if(arrPoints[i].phase == 'A'){
				totalAmps1ABC[0] += arrPoints[i].amps1;
			}
			if(arrPoints[i].phase == 'B'){
				totalAmps1ABC[1] += arrPoints[i].amps1;
			}
			if(arrPoints[i].phase == 'C'){
				totalAmps1ABC[2] += arrPoints[i].amps1;
			}
		}
		if(arrPoints[i].amps2 > 0){
			if(arrPoints[i].phase == 'A'){
				totalAmps2ABC[0] += arrPoints[i].amps2;
			}
			if(arrPoints[i].phase == 'B'){
				totalAmps2ABC[1] += arrPoints[i].amps2;
			}
			if(arrPoints[i].phase == 'C'){
				totalAmps2ABC[2] += arrPoints[i].amps2;
			}
		}
		if(arrPoints[i].soldamps1 > 0){
			if(arrPoints[i].phase == 'A'){
				totalAmps3ABC[0] += arrPoints[i].soldamps1;
			}
			if(arrPoints[i].phase == 'B'){
				totalAmps3ABC[1] += arrPoints[i].soldamps1;
			}
			if(arrPoints[i].phase == 'C'){
				totalAmps3ABC[2] += arrPoints[i].soldamps1;
			}
		}
		
		if(arrPoints[i].ampsall > 0){
			if(arrPoints[i].phase == 'A'){
				totalAmpsAll[0] += arrPoints[i].ampsall;
			}
			if(arrPoints[i].phase == 'B'){
				totalAmpsAll[1] += arrPoints[i].ampsall;
			}
			if(arrPoints[i].phase == 'C'){
				totalAmpsAll[2] += arrPoints[i].ampsall;
			}
		}
		
		totalAmps1 += arrPoints[i].amps1;
		totalAmps2 += arrPoints[i].amps2;
	}
	
	var objPanel = new Object();
	objPanel["deviceid"] = deviceid;
	objPanel["device"] = device;
	
	objPanel["amps1a"] = totalAmps1ABC[0];
	objPanel["amps1b"] = totalAmps1ABC[1];
	objPanel["amps1c"] = totalAmps1ABC[2];
	objPanel["kw1"] = ((totalAmps1ABC[0] + totalAmps1ABC[1] + totalAmps1ABC[2]) * 0.208 * Math.sqrt(3))/3;
	
    objPanel["amps2a"] = totalAmps2ABC[0];
	objPanel["amps2b"] = totalAmps2ABC[1];
	objPanel["amps2c"] = totalAmps2ABC[2];
	objPanel["kw2"] = ((totalAmps2ABC[0] + totalAmps2ABC[1] + totalAmps2ABC[2]) * 0.208 * Math.sqrt(3))/3;
	
    objPanel["amps3a"] = totalAmps3ABC[0];
	objPanel["amps3b"] = totalAmps3ABC[1];
	objPanel["amps3c"] = totalAmps3ABC[2];
	objPanel["kw3"] = ((totalAmps3ABC[0] + totalAmps3ABC[1] + totalAmps3ABC[2]) * 0.208 * Math.sqrt(3))/3;
	
	
    objPanel["ampsalla"] = totalAmpsAll[0];
	objPanel["ampsallb"] = totalAmpsAll[1];
	objPanel["ampsallc"] = totalAmpsAll[2];
	objPanel["kwall"] = ((totalAmpsAll[0] + totalAmpsAll[1] + totalAmpsAll[2]) * 0.208 * Math.sqrt(3))/3;
	
	objPanel["points"] = arrPoints;
	
	nlapiSubmitField('customrecord_clgx_dcim_devices', deviceid, 'custrecord_clgx_dcim_device_failover', objPanel.kwall);
	
	return objPanel;
}

function get_panel_html (objPanel){
	
	var arrPoints = objPanel.points;
	
	arrModules = _.uniq(_.pluck(arrPoints, 'module'));
	var nbrModules = arrModules.length;
	
	if(nbrModules > 1){
		var colspan1 = 9;
		var colspan2 = 2;
		var rowspan = 3;
		var colmodule = '<td>Mod</td>';
	}
	else{
		var colspan1 = 8;
		var colspan2 = 1;
		var rowspan = 1;
		var colmodule = '';
	}
	var html =  '<table class="device">';
	html += '<tr class="bb2 ac hd0"><td align="center" colspan="' + colspan1 + '"><a href="/app/common/custom/custrecordentry.nl?rectype=218&id=' + objPanel.deviceid + '" " target="_blank">' + objPanel.device + '</a></td></tr>';
	html += '<tr class="bb hd1"><td align="center" colspan="' + colspan2 + '"></td><td align="center" colspan="2">Phase<br/>A</td><td align="center" colspan="2">Phase<br/>B</td><td align="center" colspan="2">Phase<br/>C</td><td align="center">KW</td></tr>';
	html += '<tr class="bb2"><td align="center" colspan="' + colspan2 + '">Load A</td><td align="center" colspan="2">' + parseFloat((objPanel.amps1a).toFixed(2)) + 'A</td><td align="center" colspan="2">' + parseFloat((objPanel.amps1b).toFixed(2)) + 'A</td><td align="center" colspan="2">' + parseFloat((objPanel.amps1c).toFixed(2)) + 'A</td><td align="center">' + parseFloat(objPanel.kw1.toFixed(2)) + '</td></tr>';
	html += '<tr class="bb2"><td align="center" colspan="' + colspan2 + '">Load B</td><td align="center" colspan="2">' + parseFloat((objPanel.amps2a).toFixed(2)) + 'A</td><td align="center" colspan="2">' + parseFloat((objPanel.amps2b).toFixed(2)) + 'A</td><td align="center" colspan="2">' + parseFloat((objPanel.amps2c).toFixed(2)) + 'A</td><td align="center">' + parseFloat(objPanel.kw2.toFixed(2)) + '</td></tr>';
	html += '<tr class="bb2 hd1"><td align="center" colspan="' + colspan2 + '">Failover</td><td align="center" colspan="2">' + parseFloat((objPanel.ampsalla).toFixed(2)) + 'A</td><td align="center" colspan="2">' + parseFloat((objPanel.ampsallb).toFixed(2)) + 'A</td><td align="center" colspan="2">' + parseFloat((objPanel.ampsallc).toFixed(2)) + 'A</td><td align="center">' + parseFloat(objPanel.kwall.toFixed(2)) + '</td></tr>';
	html += '<tr class="bb2"><td align="center" colspan="' + colspan2 + '">Sold</td><td align="center" colspan="2">' + parseFloat((objPanel.amps3a).toFixed(2)) + 'A</td><td align="center" colspan="2">' + parseFloat((objPanel.amps3b).toFixed(2)) + 'A</td><td align="center" colspan="2">' + parseFloat((objPanel.amps3c).toFixed(2)) + 'A</td><td align="center">' + parseFloat(objPanel.kw3.toFixed(2)) + '</td></tr>';
	html += '<tr><td colspan="' + colspan1 + '">&nbsp;</td></tr>';
	
	html += '<tr class="bb2 ac">' + colmodule + '<td>Breaker</td><td align="right" colspan="3">Power<br/>A,B or P</td><td align="right" colspan="3">Power<br/>B or A</td><td align="right">A+B</td></tr>';
	
	
	for ( var i = 0; arrModules != null && i < arrModules.length; i++ ) {

		var module = i + 1;
		var arrBreakers = _.filter(arrPoints, function(arr){
			return (arr.module == module);
		});
		//var arrBreakersIDs = _.pluck(arrModuleBreakers, 'breaker');
		
		for ( var j = 0; arrBreakers != null && j < arrBreakers.length; j++ ) {
			
			if((j == 2 && nbrModules > 1) || ((j%6) == 5 && nbrModules == 1)){
				html += '<tr class="bb2">';
			}
			else{
				html += '<tr class="bb">';
			}
			if(j == 0 && nbrModules > 1){
				html += '<td align="center" rowspan="' + rowspan + '" class="bb2">M' + arrBreakers[j].module + '</td>';
			}
			html += '<td align="center">' + arrBreakers[j].breaker + '(' + arrBreakers[j].phase + ')</td>';
			
			
			
			if(arrBreakers[j].power1id > 0){
				html += '<td align="center"><a href="/app/common/custom/custrecordentry.nl?rectype=17&id=' + arrBreakers[j].power1id + '" title=" | ' + arrBreakers[j].power1 + ' | ' + arrBreakers[j].service1 + ' | " target="_blank"><img src="//www.cologix.com//images/icon/famfamfam_silk_icons/icons/lightning_go.png" width="16px" height="16px"></a></td>';
				html += '<td align="right"><a href="#" title="' + arrBreakers[j].volts1 + '">' + arrBreakers[j].circuit1 + '</a></td>';
			}
			else{
				html += '<td align="center"></td>';
				html += '<td align="center"></td>';
			}
			
			if(arrBreakers[j].power1 == '' && arrBreakers[j].amps1 > 0){
				html += '<td align="right"><span class="hd3">' + (arrBreakers[j].amps1).toFixed(2) + '</span></td>';
			}
			else{
				html += '<td align="right">' + (arrBreakers[j].amps1).toFixed(2) + '</td>';
			}
			
			if(arrBreakers[j].power2id > 0){
				html += '<td align="center"><a href="/app/common/custom/custrecordentry.nl?rectype=17&id=' + arrBreakers[j].power2id + '" title=" | ' + arrBreakers[j].power2 + ' | ' + arrBreakers[j].service2 + ' | " target="_blank"><img src="//www.cologix.com//images/icon/famfamfam_silk_icons/icons/lightning_go.png" width="16px" height="16px"></a></td>';
				html += '<td align="right"><a href="#" title="' + arrBreakers[j].volts2 + '">' + arrBreakers[j].circuit2 + '</a></td>';
				html += '<td align="right">' + (arrBreakers[j].amps2).toFixed(2) + '</td>';
			}
			else{
				html += '<td align="center"></td>';
				html += '<td align="center"></td>';
				html += '<td align="center"></td>';
			}
			if(arrBreakers[j].power == ''  && arrBreakers[j].ampsall > 0){
				html += '<td align="right"><span class="hd3">' + (arrBreakers[j].ampsall).toFixed(2) + '</span></td>';
			}
			else{
				html += '<td align="right">' + (arrBreakers[j].ampsall).toFixed(2) + '</td>';
			}
			
			html += '</tr>';
		}
	}
	html += '</table>';

	return html;
}

function get_powers (deviceid){
	
	// search all powers, including pairs
	var arrPowers1IDs = new Array();
	var arrPowers2IDs = new Array();
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_device",null,"anyof",deviceid));
	var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_panels_pwr', arrFilters, arrColumns);
	for ( var i = 0; searchPowers != null && i < searchPowers.length; i++ ) {
        var powerid = searchPowers[i].getValue('internalid',null,null);
        if(_.indexOf(arrPowers1IDs, powerid) < 0){
        	arrPowers1IDs.push(parseInt(powerid));
        }
        var pairid = searchPowers[i].getValue('custrecord_clgx_dcim_pair_power',null,null);
        if(pairid != null && pairid != '' && _.indexOf(arrPowers2IDs, pairid) < 0){
        	arrPowers2IDs.push(parseInt(pairid));
        }
	}
	
	// Build Powers Array ------------------------------------------------------------------------------------------------------------------------------------------------
	var arrPowers1 = new Array();
	if(arrPowers1IDs.length > 0){
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrPowers1IDs));
		var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_panels_pwr_points');
		searchPowers.addFilters(arrFilters);
		var resultSet = searchPowers.runSearch();
		resultSet.forEachResult(function(searchResult) {
			var columns = searchResult.getAllColumns();
			var objPower = new Object();
			
			objPower["panelid"] = parseInt(searchResult.getValue('custrecord_clgx_od_fam_device','CUSTRECORD_CLGX_POWER_PANEL_PDPM',null));
			objPower["panel"] = searchResult.getText('custrecord_clgx_od_fam_device','CUSTRECORD_CLGX_POWER_PANEL_PDPM',null);
			
			objPower["module"] = parseInt(searchResult.getValue(columns[9]));
			objPower["breaker"] = parseInt(searchResult.getValue(columns[10]));
			
			objPower["powerid"] = parseInt(searchResult.getValue('internalid',null,null));
			objPower["power"] = searchResult.getValue('name',null,null);
			
			objPower["power2id"] = parseInt(searchResult.getValue('custrecord_clgx_dcim_pair_power',null,null));
			objPower["power2"] = searchResult.getText('custrecord_clgx_dcim_pair_power',null,null);
			
			objPower["spaceid"] = parseInt(searchResult.getValue('custrecord_cologix_power_space',null,null));
			objPower["space"] = searchResult.getText('custrecord_cologix_power_space',null,null);
			
			objPower["serviceid"] = parseInt(searchResult.getValue('custrecord_cologix_power_service',null,null));
			objPower["service"] = searchResult.getText('custrecord_cologix_power_service',null,null);
			
			objPower["soid"] = parseInt(searchResult.getValue('custrecord_power_circuit_service_order',null,null));
			objPower["so"] = searchResult.getText('custrecord_power_circuit_service_order',null,null);
			
			var volts = searchResult.getText('custrecord_cologix_power_volts',null,null);
			objPower["volts"] = volts;
			
			var circuit = searchResult.getText('custrecord_cologix_power_amps',null,null);
			var soldamps = circuit.substring(0, circuit.length - 1);
			if(circuit != null && circuit != ''){
				objPower["circuit"] = circuit;
				
				if(volts == '120V Single Phase' || volts == '240V Single Phase'){
					objPower["soldamps"] = parseInt(circuit.substring(0, circuit.length - 1)) * 0.8;
				}
				else if(volts == '208V Single Phase'){
					objPower["soldamps"] = parseInt(circuit.substring(0, circuit.length - 1)) * 0.8 / 2;
				}
				else if(volts == '208V Three Phase' || volts == '240V Three Phase' || volts == '480V Three Phase'){
					objPower["soldamps"] = parseInt(circuit.substring(0, circuit.length - 1)) * 0.8 / 3;
				}
				else{
					objPower["soldamps"] = parseInt(circuit.substring(0, circuit.length - 1)) * 0.8;
				}

			}
			else{
				objPower["circuit"] = '';
				objPower["soldamps"] = 0;
			}

			objPower["pointid"] = parseInt(searchResult.getValue('internalid','CUSTRECORD_CLGX_DCIM_POINTS_CURRENT',null));
			objPower["externalid"] = parseInt(searchResult.getValue('externalid','CUSTRECORD_CLGX_DCIM_POINTS_CURRENT',null));
			objPower["point"] = searchResult.getValue('name','CUSTRECORD_CLGX_DCIM_POINTS_CURRENT',null);
			objPower["amps1"] = parseFloat(searchResult.getValue('custrecord_clgx_dcim_points_day_avg','CUSTRECORD_CLGX_DCIM_POINTS_CURRENT',null));

			arrPowers1.push(objPower);
			return true;
		});
	}
	
	var arrPowers2 = new Array();
	if(arrPowers2IDs.length > 0){
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrPowers2IDs));
		var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_panels_pwr_points');
		searchPowers.addFilters(arrFilters);
		var resultSet = searchPowers.runSearch();
		resultSet.forEachResult(function(searchResult) {
			var columns = searchResult.getAllColumns();
			var objPower = new Object();
			
			objPower["panelid"] = parseInt(searchResult.getValue('custrecord_clgx_od_fam_device','CUSTRECORD_CLGX_POWER_PANEL_PDPM',null));
			objPower["panel"] = searchResult.getText('custrecord_clgx_od_fam_device','CUSTRECORD_CLGX_POWER_PANEL_PDPM',null);
			
			objPower["module"] = parseInt(searchResult.getValue(columns[9]));
			objPower["breaker"] = parseInt(searchResult.getValue(columns[10]));
			
			objPower["powerid"] = parseInt(searchResult.getValue('internalid',null,null));
			objPower["power"] = searchResult.getValue('name',null,null);
			
			objPower["power2id"] = parseInt(searchResult.getValue('custrecord_clgx_dcim_pair_power',null,null));
			objPower["power2"] = searchResult.getText('custrecord_clgx_dcim_pair_power',null,null);
			
			objPower["spaceid"] = parseInt(searchResult.getValue('custrecord_cologix_power_space',null,null));
			objPower["space"] = searchResult.getText('custrecord_cologix_power_space',null,null);
			
			objPower["serviceid"] = parseInt(searchResult.getValue('custrecord_cologix_power_service',null,null));
			objPower["service"] = searchResult.getText('custrecord_cologix_power_service',null,null);
			
			objPower["soid"] = parseInt(searchResult.getValue('custrecord_power_circuit_service_order',null,null));
			objPower["so"] = searchResult.getText('custrecord_power_circuit_service_order',null,null);
			
			var volts = searchResult.getText('custrecord_cologix_power_volts',null,null);
			objPower["volts"] = volts;
			
			var circuit = searchResult.getText('custrecord_cologix_power_amps',null,null);
			var soldamps = circuit.substring(0, circuit.length - 1);
			if(circuit != null && circuit != ''){
				objPower["circuit"] = circuit;
				
				if(volts == '120V Single Phase' || volts == '240V Single Phase'){
					objPower["soldamps"] = parseInt(soldamps) * 0.8;
				}
				else if(volts == '208V Single Phase'){
					objPower["soldamps"] = parseInt(soldamps) * 0.8 / 2;
				}
				else if(volts == '208V Three Phase' || volts == '240V Three Phase' || volts == '480V Three Phase'){
					objPower["soldamps"] = parseInt(soldamps) * 0.8 / 3;
				}
				else{
					objPower["soldamps"] = parseInt(soldamps) * 0.8;
				}
			}
			else{
				objPower["circuit"] = '';
				objPower["soldamps"] = 0;
			}

			objPower["pointid"] = parseInt(searchResult.getValue('internalid','CUSTRECORD_CLGX_DCIM_POINTS_CURRENT',null));
			objPower["externalid"] = parseInt(searchResult.getValue('externalid','CUSTRECORD_CLGX_DCIM_POINTS_CURRENT',null));
			objPower["point"] = searchResult.getValue('name','CUSTRECORD_CLGX_DCIM_POINTS_CURRENT',null);
			
			objPower["amps1"] = parseFloat(searchResult.getValue('custrecord_clgx_dcim_points_day_avg','CUSTRECORD_CLGX_DCIM_POINTS_CURRENT',null));
			

			arrPowers2.push(objPower);
			return true;
		});
	}
	
	//nlapiSendEmail(71418,71418,'arrPowers for ' + deviceid,JSON.stringify(arrPowers),null,null,null,null);
	var arrPowers = new Array();
	arrPowers.push(arrPowers1);
	arrPowers.push(arrPowers2);
	
	return arrPowers;
}
*/
