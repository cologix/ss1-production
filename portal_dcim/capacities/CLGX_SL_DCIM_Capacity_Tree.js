nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_Capacity_Tree.js
//	Script Name:	CLGX_SL_DCIM_Capacity_Tree
//	Script Id:		customscript_clgx_sl_dcim_capacity_tree
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		1/8/2015
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_fams_capacity_tree (request, response){
	try {
		
		var tree = request.getParameter('tree');
		var treeid = request.getParameter('treeid');
		
		var objFile = nlapiLoadFile(1983590);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{nameTree}','g'), tree);
		html = html.replace(new RegExp('{jsonTree}','g'), get_json_tree (treeid));
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function get_json_tree (treeid){
	
	/*
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_contact_to_sp_contact',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_contact_to_sp_action',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_contact_to_sp_done",null,"is",'F'));
	var searchRecords = nlapiSearchRecord('customrecord_clgx_dcim_devices', 'customsearch_clgx_dcim_capacity_trees');
	*/
	
	var arrDevices = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device_pwr_tree',null,'anyof', treeid));
	var searchDevices = nlapiLoadSearch('customrecord_clgx_dcim_devices', 'customsearch_clgx_dcim_capacity_trees');
	searchDevices.addFilters(arrFilters);
	var resultSet = searchDevices.runSearch();
	resultSet.forEachResult(function(searchResult) {
		
		var objDevice = new Object();
		
		var parentid  = parseInt(searchResult.getValue('custrecord_clgx_dcim_device_parent',null,null)) || 0;
		objDevice["parentid"] = parentid;
		
		objDevice["parent"] = searchResult.getText('custrecord_clgx_dcim_device_parent',null,null);
		
		var nodeid  = parseInt(searchResult.getValue('internalid',null,null));
		objDevice["nodeid"] = nodeid;
		objDevice["node"] = searchResult.getValue('name',null,null);
		
		objDevice["pairid"] = parseInt(searchResult.getValue('custrecord_clgx_dcim_device_pair',null,null));
		objDevice["pair"] = searchResult.getText('custrecord_clgx_dcim_device_pair',null,null);
		var virtual = searchResult.getValue('custrecord_clgx_dcim_device_virtual',null,null);
		objDevice["virtual"] = virtual;
		
		var famid = parseInt(searchResult.getValue('custrecord_clgx_dcim_device_fam',null,null));
		objDevice["famid"] = famid;
		objDevice["fam"] = searchResult.getText('custrecord_clgx_dcim_device_fam',null,null);
		objDevice["famname"] = searchResult.getValue('altname','custrecord_clgx_dcim_device_fam',null);

		var paneltype = searchResult.getValue('custrecord_clgx_panel_type','custrecord_clgx_dcim_device_fam',null);

		var pdpm = 'F';
		if(paneltype == 2){
			objDevice["pdpm"] = 1;
			pdpm = 'T';
		}
		else{
			objDevice["pdpm"] = 0;
		}
		
		var vertical = 'F';
		if(paneltype == 3){
			objDevice["vertical"] = 1;
			vertical = 'T';
		}
		else{
			objDevice["vertical"] = 0;
		}
		
		var categoryid = parseInt(searchResult.getValue('custrecord_clgx_dcim_device_category',null,null)) || 0
		objDevice["categoryid"] = categoryid;
		objDevice["category"] = searchResult.getText('custrecord_clgx_dcim_device_category',null,null) || '';
		
		objDevice["currentid"] = parseInt(searchResult.getValue('custrecord_clgx_dcim_device_current',null,null));
		objDevice["currentname"] = searchResult.getText('custrecord_clgx_dcim_device_current',null,null);
		var current = parseFloat(searchResult.getValue('custrecord_clgx_dcim_points_day_avg','custrecord_clgx_dcim_device_current',null));
		var currentow = parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_current_ow',null,null));
		if(currentow > 0){
			objDevice["current"] = currentow;
		}
		else{
			if(current > 0){
				objDevice["current"] = current;
			}
			else{
				objDevice["current"] = 0;
			}
		}
		
		var haspowers = searchResult.getValue('custrecord_clgx_dcim_device_has_powers',null,null);
		if(haspowers == 'T'){
			objDevice["powers"] = 1;
			var powers = 1;
		}
		else{
			objDevice["powers"] = 0;
			var powers = 0;
		}
		
		var namedcapacity = parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_namecapacity',null,null)) || '';
		objDevice["namedcapacity"] = namedcapacity;

		var capacity = parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_capacity',null,null)) || '';
		objDevice["capacity"] = capacity;
		
		var fail = parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_failover',null,null)) || '';
		objDevice["fail"] = fail;
		
		var failow = parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_failover_ow',null,null)) || '';
		objDevice["failow"] = failow;

		var apparent = parseFloat(searchResult.getValue('custrecord_clgx_dcim_points_day_avg','CUSTRECORD_CLGX_DCIM_DEVICE_APP_PWR_OUT',null)) || '';
		objDevice["apparent"] = apparent;

		
		/*
		if(famid > 0 && powers == 1){
			objDevice["fampwrs"] = get_fam_powers (arrRootNodes[i].famid);
		}
		else{
			objDevice["fampwrs"] = '';
		}
		if(powers == 1){
			var arrNbrPowers = get_nbr_powers (nodeid);
			objDevice["nbrpowers"] = arrNbrPowers[0];
			objDevice["nbrbreakers"] = arrNbrPowers[1];
			objDevice["nbrAbreakers"] = get_nbr_breakers (nodeid);
		}
		else{
			objDevice["nbrpowers"] = '';
			objDevice["nbrbreakers"] = '';
			objDevice["nbrAbreakers"] = '';
		}
		*/
		var breakernamedcapacity = parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_breakcapacty',null,null)) || '';
		objDevice["breakernamedcapacity"] = breakernamedcapacity;
		
		var breakerderatedcapacity = parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_deratcapacty',null,null)) || '';
		objDevice["breakerderatedcapacity"] = breakerderatedcapacity;
		

		/*
		if(powers == 1){
			objDevice["kwsp"] = searchResult.getValue('custrecord_clgx_dcim_device_day_kw_p',null,null);
		}
		else{
			objDevice["kwsp"] = '';
		}
		*/
		
		var kwsp = parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_day_kw_p',null,null)) || '';
		objDevice["kwsp"] = kwsp;
		
		var kwsa = parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_day_kw_a',null,null)) || '';
		objDevice["kwsa"] = kwsa;
		
		var kwsab = parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_day_kw_ab',null,null)) || '';
		objDevice["kwsab"] = kwsab;
		
		
		var pwrload = parseFloat(searchResult.getValue('custrecord_clgx_dcim_points_day_avg','CUSTRECORD_CLGX_DCIM_DEVICE_LOAD',null));
		
		
		
		
		/*
		if(pwrload > 0 && virtual == 'F'){
			objDevice["kwsa"] = pwrload.toFixed(2);
		}
		else if(virtual == 'T'){
			objDevice["kwsa"] = '';
		}
		else{
			objDevice["kwsa"] = kwsa.toFixed(2);
		}
		*/
		

		/*
		if(powers == 1 && kwsab > 0){
			objDevice["kwsab"] = kwsab;
			var available = capacity - kwsab;
		}
		else{
			objDevice["kwsab"] = '';
			var available = capacity - kwsa;
		}
		*/

		
		var ampsp_a = searchResult.getValue('custrecord_clgx_dcim_device_day_amp_p_a',null,null);
		var ampsp_b = searchResult.getValue('custrecord_clgx_dcim_device_day_amp_p_b',null,null);
		var ampsp_c = searchResult.getValue('custrecord_clgx_dcim_device_day_amp_p_c',null,null);
		
		var ampsab_a = searchResult.getValue('custrecord_clgx_dcim_device_day_amp_ab_a',null,null);
		var ampsab_b = searchResult.getValue('custrecord_clgx_dcim_device_day_amp_ab_b',null,null);
		var ampsab_c = searchResult.getValue('custrecord_clgx_dcim_device_day_amp_ab_c',null,null);
		
		var ampsa_a = searchResult.getValue('custrecord_clgx_dcim_device_day_amp_a_a',null,null);
		var ampsa_b = searchResult.getValue('custrecord_clgx_dcim_device_day_amp_a_b',null,null);
		var ampsa_c = searchResult.getValue('custrecord_clgx_dcim_device_day_amp_a_c',null,null);
		
		
		if(powers == 1){
			objDevice["ampsp_a"] = ampsp_a;
			objDevice["ampsp_b"] = ampsp_b;
			objDevice["ampsp_c"] = ampsp_c;
			objDevice["ampsab_a"] = ampsab_a;
			objDevice["ampsab_b"] = ampsab_b;
			objDevice["ampsab_c"] = ampsab_c;
		}
		else{
			objDevice["ampsp_a"] = '';
			objDevice["ampsp_b"] = '';
			objDevice["ampsp_c"] = '';
			objDevice["ampsab_a"] = '';
			objDevice["ampsab_b"] = '';
			objDevice["ampsab_c"] = '';
		}

		
		if(virtual == 'F'){
			objDevice["ampsa_a"] = ampsa_a;
			objDevice["ampsa_b"] = ampsa_b;
			objDevice["ampsa_c"] = ampsa_c;
		}
		else{
			objDevice["ampsa_a"] = '';
			objDevice["ampsa_b"] = '';
			objDevice["ampsa_c"] = '';
		}
		
		
		if(powers == 1){
			var available_a = breakerderatedcapacity - ampsab_a;
			var available_b = breakerderatedcapacity - ampsab_b;
			var available_c = breakerderatedcapacity - ampsab_c;
		}
		else{
			var available_a = breakerderatedcapacity - ampsa_a;
			var available_b = breakerderatedcapacity - ampsa_b;
			var available_c = breakerderatedcapacity - ampsa_c;
		}
		objDevice["available_a"] = available_a.toFixed(2);
		objDevice["available_b"] = available_b.toFixed(2);
		objDevice["available_c"] = available_c.toFixed(2);
		
		objDevice["real_a"] = parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_phase_a_val',null,null)) || '';
		objDevice["real_b"] = parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_phase_b_val',null,null)) || '';
		objDevice["real_c"] = parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_phase_c_val',null,null)) || '';
		
		objDevice["loadid"] = parseInt(searchResult.getValue('custrecord_clgx_dcim_device_load',null,null)) || '';
		objDevice["loadname"] = searchResult.getText('custrecord_clgx_dcim_device_load',null,null) || '';
		var load = parseFloat(searchResult.getValue('custrecord_clgx_dcim_points_day_avg','custrecord_clgx_dcim_device_load',null)) || 0;
		var loadow = parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_load_ow',null,null)) || 0;
		objDevice["load"] = load.toFixed(2);
		objDevice["loadow"] = loadow.toFixed(2);
		var loadval = parseFloat(searchResult.getValue('custrecord_clgx_dcim_device_load_val',null,null)) || 0;
		objDevice["loadval"] = loadval.toFixed(2);
		
		if(categoryid == 1 || categoryid == 3){
			var available = capacity - kwsab;
			objDevice["available"] = available.toFixed(2);
		} 
		else if (categoryid > 0 && categoryid != 1 && categoryid != 3) {
			var available = capacity - parseFloat(load);
			objDevice["available"] = available.toFixed(2);
		} 
		else {
			objDevice["available"] = '';
		}
		

		/*
		if(loadow > 0){
			objDevice["load"] = loadow;
		}
		else{
			if(load > 0){
				objDevice["load"] = load;
			}
			else{
				objDevice["load"] = kwsab;
			}
		}
		*/
		
		objDevice["expanded"] = true;
		objDevice["iconCls"] = 'group';
    	
		objDevice["children"] = [];
		
		arrDevices.push(objDevice);
		return true;
	});
	var arrDevicesIDs = _.pluck(arrDevices, 'nodeid');

	
	// change grid into a tree -------------------------------------------------------------------------------------------------------------
	var buildTree = function(tree, item) {
	    if (item) { // if item then have parent
	        for (var k=0; k<tree.length; k++) { // parses the entire tree in order to find the parent
	            if (tree[k].nodeid === item.parentid) { // bingo!
	                tree[k].children.push(item); // add the child to his parent
	                break;
	            }
	            else buildTree(tree[k].children, item); // if item doesn't match but tree have childs then parses childs again to find item parent
	        }
	    }
	    else { // if no item then is a root item, multiple root items are supported
	        var idx = 0;
	        while (idx < tree.length)
	            if (tree[idx].parentid > 0) buildTree(tree, tree.splice(idx, 1)[0]) // if have parent then remove it from the array to relocate it to the right place
	            else idx++; // if doesn't have parent then is root and move it to the next object
	    }
	}
	buildTree(arrDevices);
	
	
	/*
	var arrAtention = get_orphan_panels (treeid, arrDevicesIDs);
	if(arrAtention[0].children.length > 0){
		arrDevices.push(arrAtention[0]);
	}
	if(arrAtention[1].children.length > 0){
		arrDevices.push(arrAtention[1]);
	}
	*/
	
	
	var objTree = new Object();
	objTree["text"] = '.';
	objTree["children"] = arrDevices;

	return JSON.stringify(objTree);
}

function get_nbr_powers (nodeid){
	
	var nbrpowers = '';
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,'COUNT'));
	arrColumns.push(new nlobjSearchColumn('internalid','custrecord_clgx_dcim_points_current','COUNT'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_device",null,"anyof",nodeid));
	var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', null, arrFilters, arrColumns);
	if(searchPowers != null){
		if(searchPowers.length > 0){
			var columns = searchPowers[0].getAllColumns();
			nbrpowers = parseInt(searchPowers[0].getValue(columns[0]));
			nbrAbreakers = parseInt(searchPowers[0].getValue(columns[1]));
		}
	}
	var arrNbrPowers = new Array();
	arrNbrPowers[0] = nbrpowers;
	arrNbrPowers[1] = nbrAbreakers;
	return arrNbrPowers;
}

function get_nbr_powers_fam (famid){
	
	var nbrpowers = '';
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,'COUNT'));
	arrColumns.push(new nlobjSearchColumn('internalid','custrecord_clgx_dcim_points_current','COUNT'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_power_panel_pdpm",null,"anyof",famid));
	var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', null, arrFilters, arrColumns);
	if(searchPowers != null){
		if(searchPowers.length > 0){
			var columns = searchPowers[0].getAllColumns();
			nbrpowers = parseInt(searchPowers[0].getValue(columns[0]));
			nbrAbreakers = parseInt(searchPowers[0].getValue(columns[1]));
		}
	}
	var arrNbrPowers = new Array();
	arrNbrPowers[0] = nbrpowers;
	arrNbrPowers[1] = nbrAbreakers;
	return arrNbrPowers;
}


function get_nbr_breakers (nodeid){
	
	var nbrbreakers = '';
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_device",null,"anyof",nodeid));
	var searchBreakers = nlapiSearchRecord('customrecord_clgx_dcim_points', 'customsearch_clgx_dcim_capacity_trees_pn', arrFilters, arrColumns);
	if(searchBreakers != null){
		if(searchBreakers.length > 0){
			nbrbreakers = searchBreakers.length;
		}
	}
	return nbrbreakers;
}


function get_fam_powers (famid){
	
	var nbrpowers = 0;
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_power_panel_pdpm",null,"anyof",famid));
	var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', null, arrFilters, arrColumns);
	if(searchPowers != null){
		if(searchPowers.length > 0){
			nbrpowers = searchPowers.length;
		}
	}
	
	return nbrpowers;
}


function get_orphan_panels (treeid, arrIDs){
	
	var facilityid = nlapiLookupField('customrecord_clgx_power_capacity_tree', treeid, 'custrecord_clgx_pwr_tree_facility');
	
	var arrPanels = new Array();
	var arrOrphans = new Array();
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	//arrFilters.push(new nlobjSearchFilter('custrecord_cologix_power_facility',null,'anyof',facilityid));
	if(arrIDs.length> 0){
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_od_fam_device','custrecord_clgx_power_panel_pdpm','noneof',arrIDs));
	}
	var searchPanels = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pwrs_orphan_panel', arrFilters, arrColumns);
	
	for ( var i = 0; searchPanels != null && i < searchPanels.length; i++ ) {
    	var searchPanel = searchPanels[i];
    	
    	var panelid = searchPanel.getValue(columns[2]);
    	var index = _.indexOf(arrIDs, parseInt(panelid));
    	if(index == -1){
    	
			if(panelid != null && panelid != '' && panelid != '- None -'){
				var objPanel = new Object();
				objPanel["nodeid"] = parseInt(panelid);
				objPanel["node"] = searchPanel.getText(columns[2]);
				var famid = parseInt(searchPanel.getValue(columns[0]));
				objPanel["famid"] = famid;
				objPanel["fam"] = searchPanel.getText(columns[0]);
				objPanel["famname"] = searchPanel.getValue(columns[1]);
				
				/*
		        if(famid > 0){
    				objPanel["fampwrs"] = get_fam_powers (famid);
    			}
    			else{
    				objPanel["fampwrs"] = 0;
    			}
				*/
				objPanel["powers"] = 1;
				//objPanel["nbrpowers"] = parseInt(searchPanel.getValue(columns[3]));

			    var arrNbrPowers = get_nbr_powers (panelid);
				objPanel["nbrpowers"] = arrNbrPowers[0];
				objPanel["nbrbreakers"] = arrNbrPowers[1];
				objPanel["nbrAbreakers"] = get_nbr_breakers (panelid);

				objPanel["iconCls"] = 'atention';
				objPanel["leaf"] = true;
				arrPanels.push(objPanel);
			}
			else{
				var objOrphan = new Object();
				objOrphan["nodeid"] = parseInt(searchPanel.getValue(columns[0]));
				objOrphan["node"] = searchPanel.getValue(columns[1]);
				var famid = parseInt(searchPanel.getValue(columns[0]));
				objOrphan["famid"] = famid;
				objOrphan["fam"] = searchPanel.getText(columns[0]);
				objOrphan["famname"] = searchPanel.getValue(columns[1]);
				/*
		        if(famid > 0){
    				objOrphan["fampwrs"] = get_fam_powers (famid);
    			}
    			else{
    				objOrphan["fampwrs"] = 0;
    			}
				*/
				objOrphan["powers"] = 1;
				objOrphan["nbrpowers"] = parseInt(searchPanel.getValue(columns[3]));
				
				var arrNbrPowers = get_nbr_powers_fam (famid);
				objOrphan["nbrpowers"] = arrNbrPowers[0];
				objOrphan["nbrbreakers"] = arrNbrPowers[1];
				objOrphan["nbrAbreakers"] = 0;
				
				objOrphan["iconCls"] = 'alarm';
				objOrphan["leaf"] = true;
				arrOrphans.push(objOrphan);
			}
		}
	}
	var objPanelsNode = new Object();
	objPanelsNode["nodeid"] = 0;
	objPanelsNode["node"] = 'Panels not in tree';
    objPanelsNode["expanded"] = true;
	objPanelsNode["iconCls"] = 'atention';
	//objPanelsNode["leaf"] = false;
	objPanelsNode["children"] = arrPanels;
	
	var objOrphansNode = new Object();
	objOrphansNode["nodeid"] = 0;
	objOrphansNode["node"] = 'Orphan Panels (FAM - No Device)';
    objOrphansNode["expanded"] = true;
	objOrphansNode["iconCls"] = 'alarm';
	//objOrphansNode["leaf"] = false;
	objOrphansNode["children"] = arrOrphans;
	
	var arrAtention = new Array();
	arrAtention[0] = objPanelsNode;
	arrAtention[1] = objOrphansNode;

	return arrAtention;

}


