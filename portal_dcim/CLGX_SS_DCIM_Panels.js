//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_Panels.js
//	Script Name:	CLGX_SS_DCIM_Panels
//	Script Id:		customscript_clgx_ss_dcim_panels
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		8/7/2014
//-------------------------------------------------------------------------------------------------
function scheduled_dcim_panels(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Date:	6/20/2014
// Details:	Flag All Panels for updating points and schedule the update points script
//-----------------------------------------------------------------------------------------------------------------
        // pull out all panels for processing
        var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,null));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("externalid",null,"noneof",'@NONE@'));
		var searchPanels = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);

		for ( var i = 0; searchPanels != null && i < searchPanels.length; i++ ) {
			var panelid = searchPanels[i].getValue('internalid',null,null);
			nlapiSubmitField('customrecord_clgx_dcim_devices', panelid, 'custrecord_clgx_dcim_device_updated', 'T');
		}
		
        var status = nlapiScheduleScript('customscript_clgx_ss_dcim_points', 'customdeploy_clgx_ss_dcim_points' ,null);

		
//-----------------------------------------------------------------------------------------------------------------
		nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

