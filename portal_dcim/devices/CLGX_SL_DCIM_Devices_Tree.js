nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_Devices_Tree.js
//	Script Name:	CLGX_SL_DCIM_Devices_Tree
//	Script Id:		customscript_clgx_sl_dcim_devices_tree
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/26/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=299&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_devices_tree (request, response){
	try {

		// load devices_grid_add.json file and change it to array
		var objFile = nlapiLoadFile(1905426);
		var treeDevices = JSON.parse(objFile.getValue());
		response.write( JSON.stringify(treeDevices) );
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
