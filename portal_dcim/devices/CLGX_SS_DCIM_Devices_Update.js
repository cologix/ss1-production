nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_Devices_Update.js
//	Script Name:	CLGX_SS_DCIM_Devices_Update
//	Script Id:		customscript_clgx_ss_dcim_devices_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/19/2014
//-------------------------------------------------------------------------------------------------

function scheduled_dcim_devices_update(){
    try{
        //nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Date:	6/20/2014
// Details:	Update devices and points
//-----------------------------------------------------------------------------------------------------------------

    	var context = nlapiGetContext();
        
        var deployment = context.getDeploymentId();
    	if(deployment == 'customdeploy_clgx_ss_dcim_devices_updt_1'){
    		var fileid = 4226113;
    	}
    	if(deployment == 'customdeploy_clgx_ss_dcim_devices_updt_2'){
    		var fileid = 4226114;
    	}
    	
    	// load devices_grid_update_1.json or devices_grid_update_2.json depending on script deployment
		var objFile = nlapiLoadFile(fileid);
		var arrDevices = JSON.parse(objFile.getValue());
		
		// load devices_grid.json
		var objFileAll = nlapiLoadFile(1905427);
		var arrAllDevices = JSON.parse(objFileAll.getValue());
		
		if(arrDevices.length > 0){
			
			var externalid = arrDevices[0];
			var objDevice = _.find(arrAllDevices, function(arr){ return (arr.externalid == externalid); });
			
			if(deployment == 'customdeploy_clgx_ss_dcim_devices_updt_1'){
				//nlapiLogExecution('DEBUG','Queue 2', JSON.stringify(objDevice)); 
	    	}
	    	if(deployment == 'customdeploy_clgx_ss_dcim_devices_updt_2'){
	    		//nlapiLogExecution('DEBUG','Queue 3', JSON.stringify(objDevice)); 
	    	}
			
			if(objDevice != null){
				
				var arrColumns = new Array();
				arrColumns.push(new nlobjSearchColumn('internalid',null,null));
				var arrFilters = new Array();
				arrFilters.push(new nlobjSearchFilter("externalid",null,"is",externalid));
				var searchDevice = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
				
				if(searchDevice){
					
					var deviceid = searchDevice[0].getValue('internalid',null,null);
					
					// update device name ------------------------------------------------------------------------------------------------------------------------------
					nlapiSubmitField('customrecord_clgx_dcim_devices', deviceid, 'name', objDevice.name);
					
// get OpenData new points ------------------------------------------------------------------------------------------------------------------------------
			    	
					var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/devices/get_points_update.cfm?deviceid=' + objDevice.externalid);
					var pointsJSON = requestURL.body;
					var arrPointsJSON= JSON.parse( pointsJSON );
					
					// build new points arrays
					var arrNewPoints = new Array();
					for ( var j = 0; arrPointsJSON != null && j < arrPointsJSON.length; j++ ) {
						
						/*
						var objPoint = new Object();
						objPoint["externalid"] = (arrPointsJSON[j].pointid).toString();
						objPoint["name"] = arrPointsJSON[j].name;
						if(arrPointsJSON[j].davg){
							objPoint["average"] = arrPointsJSON[j].davg;
						}
						else{
							objPoint["average"] = 0;
						}
						*/
						var objPoint = new Object();
						objPoint["externalid"] = (arrPointsJSON[j].POINTID).toString();
						objPoint["name"] = arrPointsJSON[j].NAME;
						objPoint["average"] = parseFloat(arrPointsJSON[j].DAVG);
						arrNewPoints.push(objPoint);
					}
					var arrNewExternalIds = _.compact(_.uniq(_.pluck(arrNewPoints, 'externalid')));
					
// get Netsuite old points ------------------------------------------------------------------------------------------------------------------------------
					
					var arrColumns = new Array();
					arrColumns.push(new nlobjSearchColumn('internalid',null,null));
					arrColumns.push(new nlobjSearchColumn('externalid',null,null));
					arrColumns.push(new nlobjSearchColumn('name',null,null));
					var arrFilters = new Array();
					arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_device",null,"anyof",deviceid));
					arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
					//arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_deleted",null,"is",'F'));
					var searchPoints = nlapiSearchRecord('customrecord_clgx_dcim_points', null, arrFilters, arrColumns);
					
					// build old points arrays
					var arrOldPoints = new Array();
					for ( var j = 0; searchPoints != null && j < searchPoints.length; j++ ) {
						var objPoint = new Object();
						objPoint["internalid"] = searchPoints[j].getValue('internalid',null,null);
						objPoint["externalid"] = searchPoints[j].getValue('externalid',null,null);
						objPoint["name"] = searchPoints[j].getValue('name',null,null);
						arrOldPoints.push(objPoint);
					}
					var arrOldExternalIds = _.compact(_.uniq(_.pluck(arrOldPoints, 'externalid')));
					
					var arrDelete = _.difference(arrOldExternalIds, arrNewExternalIds);
					var arrUpdate = _.intersection(arrNewExternalIds, arrOldExternalIds);
					var arrAdd = _.difference(arrNewExternalIds, arrOldExternalIds);
					
					
// delete points ------------------------------------------------------------------------------------------------------------------------------
					
					var arrDeletedPoints = new Array();
					var arrDeletedErrors = new Array();
					
					for ( var j = 0; arrDelete != null && j < arrDelete.length; j++ ) {
						
						var objOldPoint = _.find(arrOldPoints, function(arr){ return (arr.externalid == arrDelete[j]) ; });
						
						if(objOldPoint){
							
							try {
								nlapiDeleteRecord('customrecord_clgx_dcim_points', objOldPoint.internalid);
								arrDeletedPoints.push(objOldPoint.internalid);
							}
							catch (error) {
								arrDeletedErrors.push(objOldPoint.internalid);
								
								if(objOldPoint.name.substr(objOldPoint.name.length - 3) == 'DEL'){
									var delname = objOldPoint.name;
								}
								else{
									var delname = (objOldPoint.name + '-DEL').substring(0,299);
								}
									try {
										nlapiSubmitField('customrecord_clgx_dcim_points', objOldPoint.internalid, ['name','custrecord_clgx_dcim_points_deleted'], [delname,'T']);
									}
									catch (error) {
										// send email?
									}
							}
							
						}
					}
					//nlapiLogExecution('ERROR','Deleted Points', '| Deleted = ' + arrDeletedPoints.length + '| Errors = ' + arrDeletedErrors.length + ' |');
					
// update points ------------------------------------------------------------------------------------------------------------------------------
					
					var arrUpdatedPoints = new Array();
					var arrUpdatedErrors= new Array();
					
					for ( var j = 0; arrUpdate != null && j < arrUpdate.length; j++ ) {
						
						var objNewPoint = _.find(arrNewPoints, function(arr){ return (arr.externalid == arrUpdate[j]) ; });
						var objOldPoint = _.find(arrOldPoints, function(arr){ return (arr.externalid == arrUpdate[j]) ; });
						
						if(objNewPoint && objOldPoint){
							
							var fields = ['name', 'custrecord_clgx_dcim_points_day_avg'];
							var values = [objNewPoint.name, objNewPoint.average];
							
							try {
								nlapiSubmitField('customrecord_clgx_dcim_points', objOldPoint.internalid, fields, values);
								arrUpdatedPoints.push(objOldPoint.internalid);
							}
							catch (error) {
								arrUpdatedErrors.push(objOldPoint.internalid);
								// send email?
							}
						}
					}
					//nlapiLogExecution('ERROR','Updated Points', '| Updated = ' + arrUpdatedPoints.length + '| Errors = ' + arrUpdatedErrors.length + ' |');
					
// add points ------------------------------------------------------------------------------------------------------------------------------
					
					var arrAddedPoints = new Array();
					var arrAddedErrors= new Array();
					
					for ( var j = 0; arrAdd != null && j < arrAdd.length; j++ ) {
						
						var objNewPoint = _.find(arrNewPoints, function(arr){ return (arr.externalid == arrAdd[j]) ; });
						
						if(objNewPoint){
							
							var arrColumns = new Array();
							arrColumns.push(new nlobjSearchColumn('internalid',null,null));
							var arrFilters = new Array();
							arrFilters.push(new nlobjSearchFilter("name",null,"is", objNewPoint.name));
							arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_device",null,"anyof",deviceid));
							arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
							arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_deleted",null,"is",'F'));
							var searchRecords = nlapiSearchRecord('customrecord_clgx_dcim_points', null, arrFilters, arrColumns);
							
							if(searchRecords == null) {
								
								try {
						            var record = nlapiCreateRecord('customrecord_clgx_dcim_points');
						            record.setFieldValue('name', objNewPoint.name);
						            record.setFieldValue('externalid', arrAdd[j].toString());
						            record.setFieldValue('custrecord_clgx_dcim_points_device', deviceid);
						            record.setFieldValue('custrecord_clgx_dcim_points_day_avg', objNewPoint.average);
						            record.setFieldValue('custrecord_clgx_dcim_points_deleted', 'F');
						            var idRec = nlapiSubmitRecord(record, false, true);
						            arrAddedPoints.push(idRec);
								}
								catch (error) {
									arrAddedErrors.push(arrAdd[j]);
									/*
							        if (error.getDetails != undefined){
							            nlapiLogExecution('ERROR','Add Points Error', '| Device = ' + objDevice.name + '| Point = ' + objNewPoint.name + ' | Error = ' + error.getCode() + ': ' + error.getDetails() + ' |');
							            throw error;
							        }
							        else{
							        	nlapiLogExecution('ERROR','Add Points Error', '| Device = ' + objDevice.name + '| Point = ' + objNewPoint.name + ' | Error = ' + error.toString() + ' |');
							            throw nlapiCreateError('99999', error.toString());
							        }
							        */
								}
							}
						}
					}
					//nlapiLogExecution('ERROR','Added Points', '| Added = ' + arrAddedPoints.length + '| Errors = ' + arrAddedErrors.length + ' |');
		            
					//nlapiLogExecution('ERROR','Points To Process', '| DeviceID = ' + deviceid + '| Delete = ' + arrDeletedPoints.length + '/' + arrDelete.length + ' | Update = ' + arrUpdatedPoints.length + '/'  + arrUpdate.length + ' | Add = ' + arrAddedPoints.length + '/'  + arrAdd.length + ' |');
					
// ------------------------------------------------------------------------------------------------------------------------------	
					
				}
				//var usage = 10000 - nlapiGetContext().getRemainingUsage();
			}

	    	
	    	// substract processed device from array and update devices_grid_update.json file for next loop 
			var arrNewDevices = _.reject(arrDevices, function(num){
		        return (num == externalid);
			});
			
			
			if(deployment == 'customdeploy_clgx_ss_dcim_devices_updt_1'){
	    		var filename= 'devices_grid_update_1.json'
	    	}
	    	if(deployment == 'customdeploy_clgx_ss_dcim_devices_updt_2'){
	    		var filename = 'devices_grid_update_2.json'
	    	}
			var strGrid = JSON.stringify(arrNewDevices);
			var jsonGrid = nlapiCreateFile(filename, 'PLAINTEXT', strGrid);
			jsonGrid.setFolder(1330014);
			nlapiSubmitFile(jsonGrid);
			
			// reschedule  script to continue with the next device to update
			var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
		}
		else{ // there is no device to add, schedule the update devices script
			
			if(deployment == 'customdeploy_clgx_ss_dcim_devices_updt_1'){
				clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_alerts", "OD 1.4. Finishing updating devices - 2.", "");
				
				/*nlapiSendEmail(432742,71418,'OD 1.4. Finishing updating devices - 2.','');
				nlapiSendEmail(432742,1349020,'OD 1.4. Finishing updating devices - 2.','');*/
	    	}
	    	if(deployment == 'customdeploy_clgx_ss_dcim_devices_updt_2'){
	    		clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_alerts", "OD 1.4. Finishing updating devices - 1.", "");
	    		
	    		/*nlapiSendEmail(432742,71418,'OD 1.4. Finishing updating devices - 1.','');
	    		nlapiSendEmail(432742,1349020,'OD 1.4. Finishing updating devices - 1.','');*/
	    	}
		}

		
//-----------------------------------------------------------------------------------------------------------------
		//nlapiLogExecution('DEBUG','Usage', nlapiGetContext().getRemainingUsage()); 
        //nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    
    catch (error){
    	
    	var str = String(error);
        if (str.match('SSS_REQUEST_TIME_EXCEEDED') || str.match('SSS_CONNECTION_TIME_OUT') || str.match('UNEXPECTED_ERROR')) {
        	//nlapiSendEmail(432742,71418, str + ' - Finished powers processing because of communication error and reschedule it','',null,null,null,null);
        	var status = nlapiScheduleScript('customscript_clgx_ss_dcim_devices_update', 'customdeploy_clgx_ss_dcim_devices_update' ,null);
        	nlapiLogExecution('DEBUG','End', 'Finished powers processing because of communication error and reschedule it.');
        }
        
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }    
    
}

