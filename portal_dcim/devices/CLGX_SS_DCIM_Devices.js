nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_Devices.js
//	Script Name:	CLGX_SS_DCIM_Devices
//	Script Id:		customscript_clgx_ss_dcim_devices
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		6/18/2014
//-------------------------------------------------------------------------------------------------
function scheduled_dcim_devices(){
    try{
        //nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Date:	6/20/2014
// Details:	Replicate Devices
//-----------------------------------------------------------------------------------------------------------------

    	var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/devices/get_nodes.cfm');
		//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/getnodes.cfm');
		var jsonNodes = requestURL.body;
		var arrNodes= JSON.parse( jsonNodes );
		
		var arrNewDevices = new Array();
		var arrNewDevicesExternalIDs = new Array();
		var arrNewDevicesNames = new Array();
		
		var objTree = new Object();
		objTree["text"] = '.';
		
		// extract level 1 parents -------------------------------------------------------------------------------------------------------------
		var arrNL1 = _.filter(arrNodes, function(arr){
	        return (arr.LEVEL === 1 && arr.TYPE === 'TREE');
		});
		
		var arrNodesLevel1 = new Array();
		for ( var i = 0; arrNL1 != null && i < arrNL1.length; i++ ) {
		if(arrNL1[i].CHILD.charAt(0) != '.' && arrNL1[i].CHILD.charAt(0) != '1'){

	    	var objNL1 = new Object();
	    	objNL1["node"] = arrNL1[i].CHILD;
	    	objNL1["nodeid"] = parseInt(arrNL1[i].CHILDID);
	    	//objNL1["expanded"] = true;
	    	//objNL1["iconCls"] = getMarketIcon(arrNL1[i].CHILD);
	    	////objNL1["leaf"] = false;
	    	
	    	// extract level 2 parents -------------------------------------------------------------------------------------------------------------
	    	var arrNL2 = _.filter(arrNodes, function(arr){
		        return (arr.LEVEL === 2 && arr.TYPE === 'TREE' && arr.PARENTID === objNL1.nodeid );
			});
	    	var arrNodesLevel2 = new Array();
	    	for ( var j = 0; arrNL2 != null && j < arrNL2.length; j++ ) {
	        	var objNL2 = new Object();
	        	objNL2["node"] = arrNL2[j].CHILD;
	        	objNL2["nodeid"] = parseInt(arrNL2[j].CHILDID);
	        	//objNL2["expanded"] = false;
	        	//objNL2["iconCls"] = 'facility';
	        	//objNL2["leaf"] = false;
	    		
	        	// extract level 3 parents -------------------------------------------------------------------------------------------------------------
	        	var arrNL3 = _.filter(arrNodes, function(arr){
	    	        return (arr.LEVEL === 3 && arr.TYPE === 'TREE' && arr.PARENTID === objNL2.nodeid );
	    		});
	        	var arrNodesLevel3 = new Array();
	        	for ( var k = 0; arrNL3 != null && k < arrNL3.length; k++ ) {
	            	var objNL3 = new Object();
	            	objNL3["node"] = arrNL3[k].CHILD;
	            	objNL3["nodeid"] = parseInt(arrNL3[k].CHILDID);
	            	//objNL3["expanded"] = false;
	            	//objNL3["iconCls"] = 'group';
	            	//objNL3["leaf"] = false;
	        		
	            	// extract level 4 parents -------------------------------------------------------------------------------------------------------------
	            	var arrNL4 = _.filter(arrNodes, function(arr){
	        	        return (arr.LEVEL === 4 && arr.TYPE === 'TREE' && arr.PARENTID === objNL3.nodeid );
	        		});
	            	var arrNodesLevel4 = new Array();
	            	for ( var l = 0; arrNL4 != null && l < arrNL4.length; l++ ) {
	                	var objNL4 = new Object();
	                	objNL4["node"] = arrNL4[l].CHILD;
	                	objNL4["nodeid"] = parseInt(arrNL4[l].CHILDID);
	                	//objNL4["expanded"] = false;
	                	//objNL4["iconCls"] = 'group';
	                	//objNL4["leaf"] = false;
	                	
	                	// extract level 5 children -------------------------------------------------------------------------------------------------------------
	                	var arrCL5 = _.filter(arrNodes, function(arr){
	            	        return (arr.LEVEL === 5 && arr.TYPE === 'DEVICE' && arr.PARENTID === objNL4.nodeid );
	            		});
	                	var arrNodesLevel5 = new Array();
	                	for ( var m = 0; arrCL5 != null && m < arrCL5.length; m++ ) {
	                    	var objCL5 = new Object();
	                    	objCL5["node"] = arrCL5[m].DEVICE;
	                    	objCL5["nodeid"] = parseInt(arrCL5[m].DEVICEID);
	                    	//objCL5["alarm"] = -1;
	            			objCL5["facilityid"] = objNL2.nodeid;
	            			objCL5["facility"] = objNL2.node;
	                    	//objCL5["iconCls"] = 'device';
	                    	//objCL5["leaf"] = true;
	                    	objCL5["children"] = [];
	                    	arrNodesLevel5.push(objCL5);
			            	
			            	// build device object and push it to devices array
	                    	var objDevice = new Object();
	    	            	objDevice["name"] = arrCL5[m].DEVICE;
	    	            	objDevice["externalid"] = parseInt(arrCL5[m].DEVICEID);
	    	            	objDevice["facilityid"] = getNetsuiteFacility(objNL2.node);
	    	            	arrNewDevices.push(objDevice);
	    	            	arrNewDevicesNames.push(arrCL5[m].DEVICE);
	    	            	arrNewDevicesExternalIDs.push(parseInt(arrCL5[m].DEVICEID));
	                	}
	                	objNL4["children"] = arrNodesLevel5;
	                	arrNodesLevel4.push(objNL4);
	            	}
	            	// extract level 4 children -------------------------------------------------------------------------------------------------------------
	            	var arrCL4 = _.filter(arrNodes, function(arr){
	        	        return (arr.LEVEL === 4 && arr.TYPE === 'DEVICE' && arr.PARENTID === objNL3.nodeid );
	        		});
	            	for ( var l = 0; arrCL4 != null && l < arrCL4.length; l++ ) {
	                	var objCL4 = new Object();
	                	objCL4["node"] = arrCL4[l].DEVICE;
	                	objCL4["nodeid"] = parseInt(arrCL4[l].DEVICEID);
	                	//objCL4["alarm"] = -1;
	        			objCL4["facilityid"] = objNL2.nodeid;
	        			objCL4["facility"] = objNL2.node;
	                	//objCL4["iconCls"] = 'device';
	                	//objCL4["leaf"] = true;
	                	objCL4["children"] = [];
	                	arrNodesLevel4.push(objCL4);
		            	
		            	// build device object and push it to devices array
                    	var objDevice = new Object();
    	            	objDevice["name"] = arrCL4[l].DEVICE;
    	            	objDevice["externalid"] = parseInt(arrCL4[l].DEVICEID);
    	            	objDevice["facilityid"] = getNetsuiteFacility(objNL2.node);
    	            	arrNewDevices.push(objDevice);
    	            	arrNewDevicesNames.push(arrCL4[l].DEVICE);
    	            	arrNewDevicesExternalIDs.push(parseInt(arrCL4[l].DEVICEID));
	            	}
	            	objNL3["children"] = arrNodesLevel4;
	        		arrNodesLevel3.push(objNL3);
	        	}
	        	// extract level 3 children -------------------------------------------------------------------------------------------------------------
	        	var arrCL3 = _.filter(arrNodes, function(arr){
	    	        return (arr.LEVEL === 3 && arr.TYPE === 'DEVICE' && arr.PARENTID === objNL2.nodeid );
	    		});
	        	for ( var k = 0; arrCL3 != null && k < arrCL3.length; k++ ) {
	            	var objCL3 = new Object();
	            	objCL3["node"] = arrCL3[k].DEVICE;
	            	objCL3["nodeid"] = parseInt(arrCL3[k].DEVICEID);
	            	//objCL3["alarm"] = -1;
	    			objCL3["facilityid"] = objNL2.nodeid;
	    			objCL3["facility"] = objNL2.node;
	            	//objCL3["iconCls"] = 'device';
	            	//objCL3["leaf"] = true;
	            	objCL3["children"] = [];
	            	arrNodesLevel3.push(objCL3);
	            	
	            	// build device object and push it to devices array
                	var objDevice = new Object();
	            	objDevice["name"] = arrCL3[k].DEVICE;
	            	objDevice["externalid"] = parseInt(arrCL3[k].DEVICEID);
	            	objDevice["facilityid"] = getNetsuiteFacility(objNL2.node);
	            	arrNewDevices.push(objDevice);
	            	arrNewDevicesNames.push(arrCL3[k].DEVICE);
	            	arrNewDevicesExternalIDs.push(parseInt(arrCL3[k].DEVICEID));
	        	}
	        	objNL2["children"] = arrNodesLevel3;
	        	arrNodesLevel2.push(objNL2);
	    	}

	    	objNL1["children"] = arrNodesLevel2;
	    	arrNodesLevel1.push(objNL1);
		}
		}
		objTree["children"] = arrNodesLevel1;
		
		//var strTree = JSON.stringify(objTree);
		//var jsonTreeFile = nlapiCreateFile('dcim_tree.txt', 'PLAINTEXT', strTree);
		//jsonTreeFile.setFolder(1080872);
		//nlapiSubmitFile(jsonTreeFile);
		
		
// build existing devices arrays ------------------------------------------------------------------------------------
		var arrOldDevicesInternalIDs = new Array();
		var arrOldDevicesExternalIDs = new Array();
		
		var searchDevices = nlapiLoadSearch('customrecord_clgx_dcim_devices', 'customsearch_clgx_dcim_devices');
		var resultSet = searchDevices.runSearch();
		resultSet.forEachResult(function(searchResult) {
			arrOldDevicesInternalIDs.push(parseInt(searchResult.getValue('internalid', null, null)));
			arrOldDevicesExternalIDs.push(parseInt(searchResult.getValue('externalid', null, null)));
			return true;
		});
		
		// build add, update, delete arrays ------------------------------------------------------------------------------------------------------------------------------------------------
		var arrAdd = _.difference(arrNewDevicesExternalIDs, arrOldDevicesExternalIDs);
		var arrUpdate = _.intersection(arrNewDevicesExternalIDs, arrOldDevicesExternalIDs);
		var arrDelete = _.difference(arrOldDevicesExternalIDs, arrNewDevicesExternalIDs);
		
		//nlapiSendEmail(432742,71418,'arrAdd',JSON.stringify(arrAdd),null,null,null,null);
    	
		
		for ( var i = 0; arrDelete != null && i < arrDelete.length; i++ ) {
			var internalid = arrOldDevicesInternalIDs[_.indexOf(arrOldDevicesExternalIDs, arrDelete[i])];
			var oldname = nlapiLookupField('customrecord_clgx_dcim_devices', internalid, 'name');
			var delname = oldname + ' (Deleted)';
			nlapiSubmitField('customrecord_clgx_dcim_devices', internalid, ['name','custrecord_clgx_dcim_device_deleted'], [delname,'T']);
		}

		for ( var i = 0; arrUpdate != null && i < arrUpdate.length; i++ ) {
			var internalid = arrOldDevicesInternalIDs[_.indexOf(arrOldDevicesExternalIDs, arrUpdate[i])];
			var newname = arrNewDevicesNames[_.indexOf(arrNewDevicesExternalIDs, arrUpdate[i])];
			var fields = ['name','custrecord_clgx_dcim_device_dummy'];
			var values = [newname,internalid];
			nlapiSubmitField('customrecord_clgx_dcim_devices', internalid, fields, values);
		}


		for ( var i = 0; arrAdd != null && i < arrAdd.length; i++ ) {
	    	var arrAddDevice = _.filter(arrNewDevices, function(arr){
		        return (arr.externalid === arrAdd[i] );
			});
	    	
	    	
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("name",null,"is",arrAddDevice[0].name));
			//arrFilters.push(new nlobjSearchFilter("externalid",null,"equalto",arrAddDevice[0].externalid));
			arrFilters.push(new nlobjSearchFilter("isinactive",null,"noneof",'@NONE@'));
			var searchRecords = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
			
			nlapiLogExecution('DEBUG','Device', '| Name = ' + arrAddDevice[0].name + ' | ExternalID = ' + arrAddDevice[0].externalid + ' | Facility = ' + arrAddDevice[0].facilityid) + ' |'; 
			
	    	if(arrAddDevice[0].facilityid > 0 && searchRecords == null){
		    	var record = nlapiCreateRecord('customrecord_clgx_dcim_devices');
	            record.setFieldValue('name', arrAddDevice[0].name);
	            record.setFieldValue('externalid', arrAddDevice[0].externalid);
	            record.setFieldValue('custrecord_clgx_dcim_device_facility', arrAddDevice[0].facilityid);
	            record.setFieldValue('custrecord_clgx_dcim_device_deleted', 'F');
	            record.setFieldValue('custrecord_clgx_dcim_device_has_powers', 'F');
	            var idRec = nlapiSubmitRecord(record, false, true);
	            nlapiSubmitField('customrecord_clgx_dcim_devices', idRec, 'custrecord_clgx_dcim_device_dummy', idRec);
	    	}

		}

		/*
		var strUpdate = JSON.stringify(arrUpdate);
		var jsonUpdate = nlapiCreateFile('devices_update.txt', 'PLAINTEXT', strUpdate);
		jsonUpdate.setFolder(1080872);
		nlapiSubmitFile(jsonUpdate);
		
		var strAdd = JSON.stringify(arrAdd);
		var jsonAdd = nlapiCreateFile('devices_add.txt', 'PLAINTEXT', strAdd);
		jsonAdd.setFolder(1080872);
		nlapiSubmitFile(jsonAdd);
		
		
		var strDelete = JSON.stringify(arrDelete);
		var jsonDelete = nlapiCreateFile('devices_delete.txt', 'PLAINTEXT', strDelete);
		jsonDelete.setFolder(1080872);
		nlapiSubmitFile(jsonDelete);
		*/
		
		// update any missed power circuits pairs ------------------------------------------------------------------------------------------------------------------------------------------------
		var arrPowers = new Array();
		var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_powers_pairs');
		var resultSet = searchPowers.runSearch();
		resultSet.forEachResult(function(searchResult) {
			var columns = searchResult.getAllColumns();
			var objPower = new Object();
			objPower["power1id"] = parseInt(searchResult.getValue('internalid',null,null));
			objPower["power1"] = searchResult.getValue('name',null,null);
			arrPowers.push(objPower);
			return true;
		});
		for ( var i = 0; arrPowers != null && i < arrPowers.length; i++ ) {
			var power2 = get_power2_name (arrPowers[i].power1);
			var objPower2 = _.find(arrPowers, function(arr){ return (arr.power1 == power2); });
			if(objPower2 != null){
				//nlapiSubmitField('customrecord_clgx_power_circuit', arrPowers[i].power1id, 'custrecord_clgx_dcim_pair_power', objPower2.power1id);
				//nlapiSubmitField('customrecord_clgx_power_circuit', objPower2.power1id, 'custrecord_clgx_dcim_pair_power', arrPowers[i].power1id);
				
				var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', arrPowers[i].power1id);
				recPower.setFieldValue('custrecord_clgx_dcim_pair_power', objPower2.power1id);
				nlapiSubmitRecord(recPower, false, true);
				
				var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', objPower2.power1id);
				recPower.setFieldValue('custrecord_clgx_dcim_pair_power', arrPowers[i].power1id);
				nlapiSubmitRecord(recPower, false, true);
				
			}
		}
		
		// flag devices with power circuits as panels ------------------------------------------------------------------------------------------------------------------------------------------------
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,null).setSort(false));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_device_has_powers",null,"is",'T'));
		var searchRecords = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
		for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
			var id = searchResult[i].getValue('internalid',null,null);
			nlapiSubmitField('customrecord_clgx_dcim_devices', id, 'custrecord_clgx_dcim_device_has_powers', 'F');
	    }
		var searchResults = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_devices_with_pwrs', null, null);
		for ( var i = 1; searchResults != null && i < searchResults.length; i++ ) {
        	var searchResult = searchResults[i];
        	var id = searchResult.getValue('custrecord_clgx_od_fam_device','CUSTRECORD_CLGX_POWER_PANEL_PDPM','GROUP');
        	nlapiSubmitField('customrecord_clgx_dcim_devices', id, 'custrecord_clgx_dcim_device_has_powers', 'T');
        }
		
		
		// flag All Panels for updating points and schedule the update points script ------------------------------------------------------------------------------------------------------------------------------------------------
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,null));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("externalid",null,"noneof",'@NONE@'));
		var searchPanels = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
		for ( var i = 0; searchPanels != null && i < searchPanels.length; i++ ) {
			nlapiSubmitField('customrecord_clgx_dcim_devices', searchPanels[i].getValue('internalid',null,null), 'custrecord_clgx_dcim_device_updated', 'T');
		}
		// schedule the script to update all points
        var status = nlapiScheduleScript('customscript_clgx_ss_dcim_points', 'customdeploy_clgx_ss_dcim_points' ,null);

		
//-----------------------------------------------------------------------------------------------------------------
		nlapiLogExecution('DEBUG','Usage', nlapiGetContext().getRemainingUsage()); 
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}



function getMarketIcon(market){
	if(market == 'Columbus' || market == 'Dallas' || market == 'Jacksonville' || market == 'Minneapolis'){
		return 'us';
	}
	else{
		return 'ca';
	}
}


function getNetsuiteFacility(facility){
	
	var facilityid = 0;
	switch(facility) {
	
	case 'COL Site Metrics':
		facilityid = 24;
		break;
	case 'COL Collectors':
		facilityid = 24;
		break;
	case 'COL1':
		facilityid = 24;
		break;
	case 'COL2':
		facilityid = 24;
		break;
		
	case 'DAL Site Metrics':
		facilityid = 3;
		break;
	case 'DAL Collectors':
		facilityid = 3;
		break;
	case 'DAL1':
		facilityid = 3;
		break;
	case 'DAL2':
		facilityid = 18;
		break;
		
	case 'JAX1':
		facilityid = 21;
		break;
		
	case 'MIN Collectors':
		facilityid = 17;
		break;
	case 'MIN1':
		facilityid = 17;
		break;
	case 'MIN2':
		facilityid = 17;
		break;
		
	case 'MTL Site Metrics':
		facilityid = 4;
		break;
	case 'MTL Collectors':
		facilityid = 4;
		break;
	case 'MTL1':
		facilityid = 2;
		break;
	case 'MTL2':
		facilityid = 5;
		break;		
	case 'MTL3':
		facilityid = 4;
		break;
	case 'MTL4':
		facilityid = 9;
		break;	
	case 'MTL5':
		facilityid = 6;
		break;
	case 'MTL6':
		facilityid = 7;
		break;	
	case 'MTL7':
		facilityid = 19;
		break;
		
	case 'TOR1':
		facilityid = 8;
		break;
	case 'TOR2':
		facilityid = 15;
		break;	
		
		
	case 'VAN1':
		facilityid = 14;
		break;
	case 'VAN2':
		facilityid = 20;
		break;	
		
		
		
	default:
		facilityid = 0;
	}
	return facilityid;

}

function get_power2_name (power) {
	var lastChar = power.substr(power.length - 1);
	var power2 = '';
	if(lastChar == 'A' || lastChar == 'B'){
		if(lastChar == 'A'){
			power2 = power.slice(0, - 1) + 'B';
		}
		else{
			power2 = power.slice(0, - 1) + 'A';
		}
	}
	return  power2;
}