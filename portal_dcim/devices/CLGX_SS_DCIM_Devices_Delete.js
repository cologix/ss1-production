nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_Devices_Delete.js
//	Script Name:	CLGX_SS_DCIM_Devices_Delete
//	Script Id:		customscript_clgx_ss_dcim_devices_delete
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/19/2014
//-------------------------------------------------------------------------------------------------

function scheduled_dcim_devices_delete(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Date:	6/20/2014
// Details:	Process devices that don't exist anymore on OpenData
//-----------------------------------------------------------------------------------------------------------------

        // load devices_grid_delete.json file and change it to array
		var objFile = nlapiLoadFile(1906033);
		var arrDevices = JSON.parse(objFile.getValue());
		
		// if there is at least one device to process as deleted
		if(arrDevices.length > 0){
			
			var externalid = arrDevices[0];
			
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("externalid",null,"is",externalid));
			arrFilters.push(new nlobjSearchFilter("isinactive",null,"noneof",'@NONE@'));
			var searchDevice = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
			
			if (searchDevice != null) {
				
				var deviceid = searchDevice[0].getValue('internalid',null,null);
				var recDevice = nlapiLoadRecord ('customrecord_clgx_dcim_devices',deviceid);
				
				var oldname = recDevice.getFieldValue('name');
				var delname = oldname + ' - DEL';
				nlapiSendEmail(432742,71418,'OD 1.2 - Deleting device ' + oldname ,'',null,null,null,null,true);
				nlapiSendEmail(432742,1349020,'OD 1.2 - Deleting device ' + oldname ,'',null,null,null,null,true);
				
				
				// unlink fam from device -----------------------------------------------------------------------------------------------------------------------
				var famid = recDevice.getFieldValue('custrecord_clgx_dcim_device_fam');
				if(famid != null && famid != ''){
					nlapiSubmitField('customrecord_ncfar_asset', famid, 'custrecord_clgx_od_fam_device', '');
				}
				
				// update device - remove all linked devices and points -----------------------------------------------------------------------------------
				recDevice.setFieldValue('name', delname);
				recDevice.setFieldValue('custrecord_clgx_dcim_device_parent', '');
				recDevice.setFieldValue('custrecord_clgx_dcim_device_pair', '');
				recDevice.setFieldValue('custrecord_clgx_dcim_device_dummy', '');
				recDevice.setFieldValue('custrecord_clgx_dcim_device_fam', '');
				recDevice.setFieldValue('custrecord_clgx_dcim_device_load', '');
				recDevice.setFieldValue('custrecord_clgx_dcim_device_load_ow', 0);
				recDevice.setFieldValue('custrecord_clgx_dcim_device_current', '');
				recDevice.setFieldValue('custrecord_clgx_dcim_device_current_ow', 0);
				recDevice.setFieldValue('custrecord_clgx_dcim_device_capacity', 0);
				recDevice.setFieldValue('custrecord_clgx_dcim_device_failover', 0);
				recDevice.setFieldValue('custrecord_clgx_dcim_device_failover_ow', 0);
				recDevice.setFieldValue('custrecord_clgx_dcim_device_has_powers', 'F');
				recDevice.setFieldValue('custrecord_clgx_dcim_device_deleted', 'T');
				var idRec = nlapiSubmitRecord(recDevice, false, true);
				
				// search devices that have this device as parent and clear it -----------------------------------------------------------------------------------
				var arrColumns = new Array();
				arrColumns.push(new nlobjSearchColumn('internalid',null,null));
				var arrFilters = new Array();
				arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device_parent',null,"anyof",deviceid));
				arrFilters.push(new nlobjSearchFilter('isinactive',null,"is",'F'));
				arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device_deleted',null,"is",'F'));
				var searchParents = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
				for ( var i = 0; searchPowers != null && i < searchPowers.length; i++ ) {
					var searchParent = searchParents[i];
					var childid = searchParent.getValue('internalid',null,null);
					//nlapiSubmitField('customrecord_clgx_power_circuit', childid, 'custrecord_clgx_dcim_device_parent', '');
					var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', childid);
					recPower.setFieldValue('custrecord_clgx_dcim_device_parent', '');
					nlapiSubmitRecord(recPower, false, true);
				}

				// search devices that have this device as pair and clear it -----------------------------------------------------------------------------------
				var arrColumns = new Array();
				arrColumns.push(new nlobjSearchColumn('internalid',null,null));
				var arrFilters = new Array();
				arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device_pair',null,"anyof",deviceid));
				arrFilters.push(new nlobjSearchFilter('isinactive',null,"is",'F'));
				arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device_deleted',null,"is",'F'));
				var searchPairs = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
				for ( var i = 0; searchPairs != null && i < searchPairs.length; i++ ) {
					var searchPair = searchPairs[i];
					var pairid = searchPair.getValue('internalid',null,null);
					//nlapiSubmitField('customrecord_clgx_power_circuit', pairid, 'custrecord_clgx_dcim_device_pair', '');
					var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', pairid);
					recPower.setFieldValue('custrecord_clgx_dcim_device_pair', '');
					nlapiSubmitRecord(recPower, false, true);
				}

				// search powers linked to this device and clear device and points -----------------------------------------------------------------------------------
				var arrColumns = new Array();
				arrColumns.push(new nlobjSearchColumn('internalid',null,null));
				var arrFilters = new Array();
				arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device',null,"anyof",deviceid));
				arrFilters.push(new nlobjSearchFilter('isinactive',null,"is",'F'));
				var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', null, arrFilters, arrColumns);
				var arrPowers = new Array();
				for ( var i = 0; searchPowers != null && i < searchPowers.length; i++ ) {
					var searchPower = searchPowers[i];
					var powerid = searchPower.getValue('internalid',null,null);
					arrPowers.push(powerid);
					var arrNull = new Array();
					//var fields = ['custrecord_clgx_dcim_device','custrecord_clgx_dcim_points_current','custrecord_clgx_dcim_points_demand','custrecord_clgx_dcim_points_consumption'];
					//var values = ['',arrNull,arrNull,arrNull];
					//nlapiSubmitField('customrecord_clgx_power_circuit', powerid, fields, values);
					var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', powerid);
					recPower.setFieldValue('custrecord_clgx_dcim_device', '');
					recPower.setFieldValue('custrecord_clgx_dcim_points_current', null);
					recPower.setFieldValue('custrecord_clgx_dcim_points_demand', null);
					recPower.setFieldValue('custrecord_clgx_dcim_points_consumption', null);
					recPower.setFieldValue('custrecord_clgx_dcim_points_voltage', null);
					recPower.setFieldValue('custrecord_clgx_dcim_points_pwr_factor', null);
					nlapiSubmitRecord(recPower, false, true);
				}
				if(searchPowers != null){ //send email if device has powers
					var emailAdminSubject = 'A panel with powers was deleted on OpenData';
				    var emailAdminBody = '';
					emailAdminBody += 'DeviceID : ' + deviceid + '<br><br>';
					emailAdminBody += 'PowerIDs : ' + JSON.stringify(arrPowers) + '<br><br>';
					
					clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_alerts", emailAdminSubject, emailAdminBody);
					
					/*nlapiSendEmail(432742,71418,emailAdminSubject,emailAdminBody,null,null,null,null,true);
					nlapiSendEmail(432742,211645,emailAdminSubject,emailAdminBody,null,null,null,null,true);
					nlapiSendEmail(432742,1349020,emailAdminSubject,emailAdminBody,null,null,null,null,true);*/
				}
				
				// search the points of this device -------------------------------------------------------------------------------------------------------------------
				var arrPoints = new Array();
				var arrColumns = new Array();
				arrColumns.push(new nlobjSearchColumn('internalid',null,null));
				var arrFilters = new Array();
				arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_device",null,"anyof", deviceid));
				var searchPoints = nlapiLoadSearch('customrecord_clgx_dcim_points', 'customsearch_clgx_dcim_delete_device');
				searchPoints.addColumns(arrColumns);
				searchPoints.addFilters(arrFilters);
				var resultSet = searchPoints.runSearch();
				resultSet.forEachResult(function(searchResult) {
					arrPoints.push(parseInt(searchResult.getValue('internalid',null,null)));
					return true;
				});
				
				// search if there is data collected for this device -------------------------------------------------------------------------------------------------------------------
				var arrColumns = new Array();
				arrColumns.push(new nlobjSearchColumn('internalid',null,null));
				var arrFilters = new Array();
				if(arrPoints.length > 0){
					arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_points_day_kw_pnt',null,"anyof",arrPoints));
				}
				else{
					arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_points_day_kw_pnt',null,"anyof",[0]));
				}
				arrFilters.push(new nlobjSearchFilter('isinactive',null,"is",'F'));
				var searchKWH = nlapiSearchRecord('customrecord_clgx_dcim_points_day', null, arrFilters, arrColumns);
				
				if(searchKWH != null){ // there are points with collected data, keep them for legacy
					// mark all points of the device as deleted
					for ( var i = 0; arrPoints != null && i < arrPoints.length; i++ ) {
						
						var recPoint = nlapiLoadRecord ('customrecord_clgx_dcim_points',arrPoints[i]);
						var oldname = recPoint.getFieldValue('name');
						var delname = oldname + ' - DEL';
						// update point ------------------------------------------------------------------------------------
						recPoint.setFieldValue('name', delname);
						recPoint.setFieldValue('custrecord_clgx_dcim_points_deleted', 'T');
						var idRec = nlapiSubmitRecord(recPoint, false, true);
					}
				}
				else{ // device and it's points are no longer linked to any records
					// delete all points and the device
					for ( var i = 0; arrPoints != null && i < arrPoints.length; i++ ) {
						try {
							nlapiDeleteRecord('customrecord_clgx_dcim_points', arrPoints[i]);
						}
						catch (error) {
					        if (error.getDetails != undefined){
					        	var errorBody = error.getCode() + ': ' + error.getDetails();
					        }
					        else{
					        	var errorBody = error.toString();
					        }
							//nlapiSendEmail(71418,71418,'Can not delete point ' +  arrPoints[i] + ' on device ' + deviceid, errorBody,null,null,null,null);
						}
					}
					try {
						nlapiDeleteRecord('customrecord_clgx_dcim_devices', deviceid);
					}
					catch (error) {
				        if (error.getDetails != undefined){
				        	var errorBody = error.getCode() + ': ' + error.getDetails();
				        }
				        else{
				        	var errorBody = error.toString();
				        }
				        
				        clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_alerts", "Can not delete device: " + deviceid, errorBody);
						//nlapiSendEmail(71418,71418,'Can not delete device ' + deviceid, errorBody,null,null,null,null);
					}
				}
			}

			// exclude the processed device from array and recreate devices_grid_delete.json file
			var arrNewDevicesIDs = _.reject(arrDevices, function(num){
				return num == externalid;
			});
			var strIDs = JSON.stringify(arrNewDevicesIDs);
			var jsonIDs = nlapiCreateFile('devices_grid_delete.json', 'PLAINTEXT', strIDs);
			jsonIDs.setFolder(1330014);
			nlapiSubmitFile(jsonIDs);
			
			// reschedule this script to continue with the next device to delete
			var status = nlapiScheduleScript('customscript_clgx_ss_dcim_devices_delete', 'customdeploy_clgx_ss_dcim_devices_delete' ,null);
		}
		else{ // there is no device to delete, schedule the add devices script
			
			clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_alerts", "OD 1.2 - Finishing deleting devices.", "");
			/*nlapiSendEmail(432742,71418,'OD 1.2 - Finishing deleting devices.','',null,null,null,null,true);
			nlapiSendEmail(432742,1349020,'OD 1.2 - Finishing deleting devices.','',null,null,null,null,true);*/
			var status = nlapiScheduleScript('customscript_clgx_ss_dcim_devices_add', 'customdeploy_clgx_ss_dcim_devices_add' ,null);
		}
		
//-----------------------------------------------------------------------------------------------------------------
		nlapiLogExecution('DEBUG','Usage', nlapiGetContext().getRemainingUsage()); 
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}