nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_DCIM_Devices.js
//	Script Name:	CLGX_SL_DCIM_Devices
//	Script Id:		customscript_clgx_sl_dcim_devices
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/14/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=299&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_dcim_devices (request, response){
	try {

		var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/devices/get_nodes.cfm');
		//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/getnodes.cfm');
		var jsonNodes = requestURL.body;
		var arrNodes= JSON.parse( jsonNodes );
		
		var objTree = new Object();
		objTree["text"] = '.';
		
		// extract level 1 parents
		var arrNL1 = _.filter(arrNodes, function(arr){
	        return (arr.LEVEL === 1 && arr.TYPE === 'TREE' && arr.CHILD != 'Far Hills' && arr.CHILD != 'FAR1');
		});
		
		var arrNodesLevel1 = new Array();
		for ( var i = 0; arrNL1 != null && i < arrNL1.length; i++ ) {
		//if(arrNL1[i].CHILD.charAt(0) != '.' && arrNL1[i].CHILD.charAt(0) != '1'){

	    	var objNL1 = new Object();
	    	objNL1["node"] = arrNL1[i].CHILD;
	    	objNL1["nodeid"] = parseInt(arrNL1[i].CHILDID);
	    	objNL1["expanded"] = false;
	    	objNL1["iconCls"] = getMarketIcon(arrNL1[i].CHILD);
	    	objNL1["leaf"] = false;
	    	
	    	// extract level 2 parents -------------------------------------------------------------------------------------------------------------
	    	var arrNL2 = _.filter(arrNodes, function(arr){
		        return (arr.LEVEL === 2 && arr.TYPE === 'TREE' && arr.PARENTID === objNL1.nodeid );
			});
	    	var arrNodesLevel2 = new Array();
	    	for ( var j = 0; arrNL2 != null && j < arrNL2.length; j++ ) {
	        	var objNL2 = new Object();
	        	objNL2["node"] = arrNL2[j].CHILD;
	        	objNL2["nodeid"] = parseInt(arrNL2[j].CHILDID);
	        	objNL2["expanded"] = false;
	        	objNL2["iconCls"] = 'location';
	        	objNL2["leaf"] = false;
	    		
	        	// extract level 3 parents -------------------------------------------------------------------------------------------------------------
	        	var arrNL3 = _.filter(arrNodes, function(arr){
	    	        return (arr.LEVEL === 3 && arr.TYPE === 'TREE' && arr.PARENTID === objNL2.nodeid );
	    		});
	        	var arrNodesLevel3 = new Array();
	        	for ( var k = 0; arrNL3 != null && k < arrNL3.length; k++ ) {
	            	var objNL3 = new Object();
	            	objNL3["node"] = arrNL3[k].CHILD;
	            	objNL3["nodeid"] = parseInt(arrNL3[k].CHILDID);
	            	objNL3["expanded"] = false;
	            	objNL3["iconCls"] = 'group';
	            	objNL3["leaf"] = false;
	        		
	            	// extract level 4 parents -------------------------------------------------------------------------------------------------------------
	            	var arrNL4 = _.filter(arrNodes, function(arr){
	        	        return (arr.LEVEL === 4 && arr.TYPE === 'TREE' && arr.PARENTID === objNL3.nodeid );
	        		});
	            	var arrNodesLevel4 = new Array();
	            	for ( var l = 0; arrNL4 != null && l < arrNL4.length; l++ ) {
	                	var objNL4 = new Object();
	                	objNL4["node"] = arrNL4[l].CHILD;
	                	objNL4["nodeid"] = parseInt(arrNL4[l].CHILDID);
	                	objNL4["expanded"] = false;
	                	objNL4["iconCls"] = 'group';
	                	objNL4["leaf"] = false;

	                	// extract level 5 children -------------------------------------------------------------------------------------------------------------
	                	var arrCL5 = _.filter(arrNodes, function(arr){
	            	        return (arr.LEVEL === 5 && arr.TYPE === 'DEVICE' && arr.PARENTID === objNL4.nodeid );
	            		});
	                	var arrNodesLevel5 = new Array();
	                	for ( var m = 0; arrCL5 != null && m < arrCL5.length; m++ ) {
	                		var objCL5 = new Object();
	                    	objCL5["node"] = arrCL5[m].DEVICE;
	                    	objCL5["nodeid"] = parseInt(arrCL5[m].DEVICEID);
                			objCL5["alarm"] = arrCL5[m].ALARM;
                			if(arrCL5[m].EXTERNALID != '' && arrCL5[m].EXTERNALID != null && arrCL5[m].EXTERNALID != 'DEL00000'){
                				objCL5["famid"] = Number(arrCL5[m].EXTERNALID.substring(3));
                				objCL5["fam"] = arrCL5[m].EXTERNALID;
                			}
                			else{
                				objCL5["famid"] = 0;
                				objCL5["fam"] = '';
                			}
	            			objCL5["facilityid"] = objNL2.nodeid;
	            			objCL5["facility"] = objNL2.node;
	                    	objCL5["iconCls"] = 'device';
	                    	objCL5["leaf"] = true;
	                    	objCL5["children"] = [];
	                    	arrNodesLevel5.push(objCL5);
	                	}
	                	objNL4["children"] = arrNodesLevel5;
	                	arrNodesLevel4.push(objNL4);
	            	}
	            	// extract level 4 children -------------------------------------------------------------------------------------------------------------
	            	var arrCL4 = _.filter(arrNodes, function(arr){
	        	        return (arr.LEVEL == 4 && arr.TYPE == 'DEVICE' && arr.PARENTID == objNL3.nodeid );
	        		});
	            	for ( var l = 0; arrCL4 != null && l < arrCL4.length; l++ ) {
	            		var objCL4 = new Object();
	                	objCL4["node"] = arrCL4[l].DEVICE;
	                	objCL4["nodeid"] = parseInt(arrCL4[l].DEVICEID);
            			objCL4["alarm"] = arrCL4[l].ALARM;
                    	if(arrCL4[l].EXTERNALID != '' && arrCL4[l].EXTERNALID != null && arrCL4[l].EXTERNALID != 'DEL00000'){
            				objCL4["famid"] = Number(arrCL4[l].EXTERNALID.substring(3));
            				objCL4["fam"] = arrCL4[l].EXTERNALID;
            			}
            			else{
            				objCL4["famid"] = 0;
            				objCL4["fam"] = '';
            			}
	        			objCL4["facilityid"] = objNL2.nodeid;
	        			objCL4["facility"] = objNL2.node;
	                	objCL4["iconCls"] = 'device';
	                	objCL4["leaf"] = true;
	                	objCL4["children"] = [];
	                	arrNodesLevel4.push(objCL4);
	            	}
	            	objNL3["children"] = arrNodesLevel4;
	        		arrNodesLevel3.push(objNL3);
	        	}
	        	// extract level 3 children -------------------------------------------------------------------------------------------------------------
	        	var arrCL3 = _.filter(arrNodes, function(arr){
	    	        return (arr.LEVEL === 3 && arr.TYPE === 'DEVICE' && arr.PARENTID === objNL2.nodeid );
	    		});
	        	for ( var k = 0; arrCL3 != null && k < arrCL3.length; k++ ) {
	        		var objCL3 = new Object();
	            	objCL3["node"] = arrCL3[k].DEVICE;
	            	objCL3["nodeid"] = parseInt(arrCL3[k].DEVICEID);
        			objCL3["alarm"] = arrCL3[k].ALARM;
                	if(arrCL3[k].EXTERNALID != '' && arrCL3[k].EXTERNALID != null && arrCL3[k].EXTERNALID != 'DEL00000'){
        				objCL3["famid"] = Number(arrCL3[k].EXTERNALID.substring(3));
        				objCL3["fam"] = arrCL3[k].EXTERNALID;
        			}
        			else{
        				objCL3["famid"] = 0;
        				objCL3["fam"] = '';
        			}
	    			objCL3["facilityid"] = objNL2.nodeid;
	    			objCL3["facility"] = objNL2.node;
	            	objCL3["iconCls"] = 'device';
	            	objCL3["leaf"] = true;
	            	objCL3["children"] = [];
	            	arrNodesLevel3.push(objCL3);
	        	}
	        	objNL2["children"] = arrNodesLevel3;
	        	arrNodesLevel2.push(objNL2);
	    	}

	    	objNL1["children"] = arrNodesLevel2;
	    	arrNodesLevel1.push(objNL1);
		//}
		}
		objTree["children"] = arrNodesLevel1;

		response.write( JSON.stringify(objTree) );
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function getMarketIcon(market){
	if(market == 'Columbus' || market == 'Dallas' || market == 'Jacksonville' || market == 'Minneapolis'){
		return 'us';
	}
	else{
		return 'ca';
	}
}

