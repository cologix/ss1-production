nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_Devices_Add.js
//	Script Name:	CLGX_SS_DCIM_Devices_Add
//	Script Id:		customscript_clgx_ss_dcim_devices_add
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/19/2014
//-------------------------------------------------------------------------------------------------

function scheduled_dcim_devices_add(){
    try{
        
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Date:	6/20/2014
// Details:	Process devices that don't exist anymore on OpenData
//-----------------------------------------------------------------------------------------------------------------

        // load devices_grid_add.json file and change it to array
		var objFile = nlapiLoadFile(1906031);
		var arrDevices = JSON.parse(objFile.getValue());
		
		// load devices_grid.json file and change it to array
		var objFileAll = nlapiLoadFile(1905427);
		var arrAllDevices = JSON.parse(objFileAll.getValue());
		
		if(arrDevices.length > 0){
			
			var externalid = arrDevices[0];
			var objDevice = _.find(arrAllDevices, function(arr){ return (arr.externalid == externalid); });
			
			if(objDevice != null){
				
				var arrColumns = new Array();
				arrColumns.push(new nlobjSearchColumn('internalid',null,null));
				var arrFilters = new Array();
				arrFilters.push(new nlobjSearchFilter("name",null,"is",objDevice.name));
				arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
				var searchRecords = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
				
				// add device
				if(objDevice.facilityid > 0 && searchRecords == null){
					
					
					try{
						
						var record = nlapiCreateRecord('customrecord_clgx_dcim_devices');
			            record.setFieldValue('name', objDevice.name);
			            //record.setFieldValue('externalid', parseInt(objDevice.externalid).toFixed(0));
			            record.setFieldValue('externalid', (objDevice.externalid).toFixed(0));
			            record.setFieldValue('custrecord_clgx_dcim_device_facility', objDevice.facilityid);
			            record.setFieldValue('custrecord_clgx_dcim_device_deleted', 'F');
			            record.setFieldValue('custrecord_clgx_dcim_device_has_powers', 'F');
			            var idNewDevice = nlapiSubmitRecord(record, false, true);
			            nlapiSubmitField('customrecord_clgx_dcim_devices', idNewDevice, 'custrecord_clgx_dcim_device_dummy', idNewDevice);
			            
			            nlapiLogExecution('DEBUG','Add device', '| DeviceID = ' + idNewDevice + '| Device = ' + objDevice.name);
						
			            // add points
			            var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/devices/get_points_update.cfm?deviceid=' + objDevice.externalid);
				    	//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/getpointsupdate.cfm?deviceid=' + objDevice.externalid);
				    	//var requestURL = nlapiRequestURL('https://command1.nnj2.cologix.net:10313/devices/' + objDevice.externalid + '/points/last_avg_values',null,{'Content-type': 'application/json'},null,'GET');
				        var pointsJSON = requestURL.body;
						var arrNewPoints= JSON.parse( pointsJSON );
	
						for ( var i = 0; arrNewPoints != null && i < arrNewPoints.length; i++ ) {
							var objPoint = arrNewPoints[i];
							if(objPoint.DAVG == '' || objPoint.DAVG == null){
								objPoint.DAVG = 0;
							}
							var record = nlapiCreateRecord('customrecord_clgx_dcim_points');
				            record.setFieldValue('name', objPoint.NAME);
				            record.setFieldValue('externalid', Number(objPoint.POINTID).toString());
				            record.setFieldValue('custrecord_clgx_dcim_points_device', idNewDevice);
				            record.setFieldValue('custrecord_clgx_dcim_points_day_avg', parseFloat(objPoint.DAVG));
				            record.setFieldValue('custrecord_clgx_dcim_points_deleted', 'F');
				            var idNewPoint = nlapiSubmitRecord(record, false, true);
						}
						
						clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_alerts", "OD 1.3 - Adding device " + objDevice.name, JSON.stringify(arrNewPoints));
						
						/*nlapiSendEmail(432742,71418,'OD 1.3 - Adding device ' + objDevice.name ,JSON.stringify(arrNewPoints));
						nlapiSendEmail(432742,1349020,'OD 1.3 - Adding device ' + objDevice.name ,JSON.stringify(arrNewPoints));*/
						
					}
					
					catch (error){
						nlapiLogExecution('DEBUG','Existing device', '| Device = ' + objDevice.name);
						
					}
					
		    	}
				
				// substract processed device from array and update devices_grid_add.json file for next loop 
				var arrNewDevices = _.reject(arrDevices, function(num){
			        return (num == externalid);
				});
				var strGrid = JSON.stringify(arrNewDevices);
				var jsonGrid = nlapiCreateFile('devices_grid_add.json', 'PLAINTEXT', strGrid);
				jsonGrid.setFolder(1330014);
				nlapiSubmitFile(jsonGrid);
				
				//nlapiLogExecution('ERROR','Add device', '| DeviceID = ' + objDevice.externalid + '| Device = ' + objDevice.name + '| Points = ' + arrNewPoints.length + ' |');
			}
			
			// reschedule this script to continue with the next device to add
			var status = nlapiScheduleScript('customscript_clgx_ss_dcim_devices_add', 'customdeploy_clgx_ss_dcim_devices_add' ,null);
		}
		else{ // there is no device to add, schedule the update devices script
			clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_dcim_alerts", "OD 1.3 - Finishing adding devices.", "");
			
			/*nlapiSendEmail(432742,71418,'OD 1.3 - Finishing adding devices.','');
			nlapiSendEmail(432742,1349020,'OD 1.3 - Finishing adding devices.','');*/
			//var status = nlapiScheduleScript('customscript_clgx_ss_dcim_devices_update', 'customdeploy_clgx_ss_dcim_devices_update' ,null);
		}

//-----------------------------------------------------------------------------------------------------------------
		//nlapiLogExecution('DEBUG','Usage', nlapiGetContext().getRemainingUsage()); 
        //nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

