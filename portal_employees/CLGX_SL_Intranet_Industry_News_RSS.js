//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Intranet_Industry_News_RSS.js
//	Script Name:	CLGX_SL_Intranet_Industry_News_RSS
//	Script Id:		customscript_clgx_sl_intranet_ind_news
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/31/2013
//-------------------------------------------------------------------------------------------------

function suitelet_intranet_ind_news (request, response){
	try {

		var objFile = nlapiLoadFile(346196);
		var html = objFile.getValue();

		response.write( html );

	} 
	catch (error) {

		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}
