nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_Intranet_News.js
//	Script Name:	CLGX_RL_Intranet_News
//	Script Id:		customscript_clgx_rl_intranet_news
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/31/2013
//-------------------------------------------------------------------------------------------------

function restlet_intranet_news (){
	try {
		var objFile = nlapiLoadFile(357883);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{Announcements}','g'),buildAnnouncements());
		return html;

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function buildAnnouncements(){

	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_eportal_announcement',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	var searchAnnouncements = nlapiSearchRecord('customrecord_clgx_eportal_announcements', null, arrFilters, arrColumns);

	var htmlAnnouncements = '';

	for ( var i = 0; searchAnnouncements != null && i < searchAnnouncements.length; i++ ) {
		var searchAnnouncement = searchAnnouncements[i];
		htmlAnnouncements += '\n<li><a href="#" target="_self">' + searchAnnouncement.getValue('custrecord_clgx_eportal_announcement', null, null) + '</a></li>';
	}
    return htmlAnnouncements;
}

