//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_Intranet_Industry_News_RSS.js
//	Script Name:	CLGX_RL_Intranet_Industry_News_RSS
//	Script Id:		customscript_clgx_rl_intranet_ind_news
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/31/2013
//-------------------------------------------------------------------------------------------------

function restlet_intranet_ind_news (datain){
	try {
		var objFile = nlapiLoadFile(805999);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{cologixNews}','g'),cologixNews());
		html = html.replace(new RegExp('{industryNews}','g'),industryNews());
		return html;
	} 
	catch (error) {

		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}

function cologixNews (datain){
	return 'Cologix News';
}

function industryNews (datain){
	return 'Now on Cologix.com—Most Recent News & Resources';
}