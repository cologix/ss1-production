nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Intranet_Calendar.js
//	Script Name:	CLGX_SL_Intranet_Calendar
//	Script Id:		customscript_clgx_sl_intranet_calendar
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/31/2013
//-------------------------------------------------------------------------------------------------

function suitelet_intranet_employee_calendar (){
	try {

		
		var currentContext = nlapiGetContext();
		var userID = nlapiGetContext().getUser();
		var userEmail = nlapiLookupField('employee', userID, 'email');

		response.write( '<iframe src="https://www.google.com/calendar/embed?showTitle=0&amp;mode=WEEK&amp;src=' + userEmail + '&amp;height=600&amp;wkst=1" width="1000" height="580" frameborder="0" scrolling="no"></iframe>' );
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function eventsJSON(){

	var date = new Date();
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('title',null,null));
	arrColumns.push(new nlobjSearchColumn('startdate',null,null));
	arrColumns.push(new nlobjSearchColumn('starttime',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('startdate',null,'onorafter',date));
	arrFilters.push(new nlobjSearchFilter('custevent_clgx_eportal_event',null,'is','T'));
	
	var searchEvents = nlapiSearchRecord('calendarevent', null, arrFilters, arrColumns);

	var stEvents = 'var dataEvents =  [';

	for ( var i = 0; searchEvents != null && i < searchEvents.length; i++ ) {
		var searchEvent = searchEvents[i];
		
		var title = searchEvent.getValue('title', null, null);
		title = title.toString().replace(/"/g, '\\"');
		title = title.toString().replace(/'/g, "\\'");
		
		stEvents += '\n{eventid:' + searchEvent.getValue('internalid', null, null) + 
						',event:"' +  title + 
						'",date:"' +  searchEvent.getValue('startdate', null, null) + 
						'",time:"' + searchEvent.getValue('starttime', null, null) + '"},'; 
	}

	var strLen = stEvents.length;
	if (searchEvents != null){
		stEvents = stEvents.slice(0,strLen-1);
	}
	stEvents += '];';
    return stEvents;
}