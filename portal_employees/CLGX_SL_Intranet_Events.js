nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Intranet_Events.js
//	Script Name:	CLGX_SL_Intranet_Events
//	Script Id:		customscript_clgx_sl_intranet_events
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/31/2013
//-------------------------------------------------------------------------------------------------

function suitelet_intranet_events (){
	try {

		var objFile = nlapiLoadFile(350508);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{eventsJSON}','g'),eventsJSON());
		
		response.write( html );
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function eventsJSON(){

	var date = new Date();
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('title',null,null));
	arrColumns.push(new nlobjSearchColumn('startdate',null,null));
	arrColumns.push(new nlobjSearchColumn('organizer',null,null));
	arrColumns.push(new nlobjSearchColumn('attendee',null,null));
	arrColumns.push(new nlobjSearchColumn('custevent_clgx_event_speakers',null,null));
	arrColumns.push(new nlobjSearchColumn('custevent_clgx_event_part_type',null,null));
	
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('startdate',null,'onorafter',date));
	arrFilters.push(new nlobjSearchFilter('custevent_clgx_eportal_event',null,'is','T'));
	
	var searchEvents = nlapiSearchRecord('calendarevent', null, arrFilters, arrColumns);
	var stEvents = 'var dataEvents =  [';

	for ( var i = 0; searchEvents != null && i < searchEvents.length; i++ ) {
		var searchEvent = searchEvents[i];
		
		var title = searchEvent.getValue('title', null, null);
		title = title.toString().replace(/"/g, '\\"');
		title = title.toString().replace(/'/g, "\\'");
		var eventid = searchEvent.getValue('internalid', null, null);
		var attendeeList = '';
		for ( var j = 0; searchEvents != null && j < searchEvents.length; j++ ) {
			var searchEvent2 = searchEvents[j];
			var eventid2 = searchEvent2.getValue('internalid', null, null);
			var attendee = searchEvent2.getText('attendee', null, null);
			var comma =  " ";
			if (eventid2 == eventid)
			{
				if (attendeeList != "")
					comma = ", ";
				attendeeList += comma + attendee;
			}
		}
		attendeeList += '"},';
	
		
		if( (!(stEvents.indexOf(eventid) > -1))){
			stEvents += '\n{eventid:' + searchEvent.getValue('internalid', null, null) + 
			',event:"' +  title + 
			'",date:"' +  searchEvent.getValue('startdate', null, null) + 
			'",organizer:"' + searchEvent.getText('organizer', null, null) + 
			'",speakers:"' + searchEvent.getValue('custevent_clgx_event_speakers', null, null) + 
			'",participationtype:"' + searchEvent.getText('custevent_clgx_event_part_type', null, null) + 
			'",attendee:"' + attendeeList;
		}

	}


	var strLen = stEvents.length;
	if (searchEvents != null){
		stEvents = stEvents.slice(0,strLen-1);
	}
	stEvents += '];';
    return stEvents;
}