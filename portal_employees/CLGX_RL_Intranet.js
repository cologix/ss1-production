nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_Intranet.js
//	Script Name:	CLGX_RL_Intranet
//	Script Id:		customscript_clgx_rl_intranet
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/31/2013
//-------------------------------------------------------------------------------------------------

function restlet_intranet_employee (){
	try {
		var objFile = nlapiLoadFile(343729);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{menu}','g'),buildMenu());
		html = html.replace(new RegExp('{building}','g'),getRandomInt (1, 13));
		return html;

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function buildMenu(){

	var currentContext = nlapiGetContext();
	var currentRole = currentContext.getRole();
	var currentUser = nlapiGetUser();
	var language = currentContext.getPreference('language');
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_parent',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_sort_order',null,null).setSort(false));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_title',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_description',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_type',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_fileid',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_href',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_all_roles',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_roles',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_employees',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_menu_sys_parent',null,'anyof',13));
	var searchMenuSections = nlapiSearchRecord('customrecord_clgx_menus_system', null, arrFilters, arrColumns);

	var htmlMenu = '';
// Start Menu Sections -------------------------------------------------------------------------------------------------------
	for ( var i = 0; searchMenuSections != null && i < searchMenuSections.length; i++ ) {
		var searchMenuSection = searchMenuSections[i];

		// determine if user has rights to see this menu element
		var internalid0 = searchMenuSection.getValue('internalid', null, null);
		var menuRec0 = nlapiLoadRecord ('customrecord_clgx_menus_system',internalid0);
		hasRightsAll0 = menuRec0.getFieldValue('custrecord_clgx_menu_sys_all_roles');
		var arrRecUsers0 = menuRec0.getFieldValues('custrecord_clgx_menu_sys_employees');
		var arrRecRoles0 = menuRec0.getFieldValues('custrecord_clgx_menu_sys_roles');
		
		if(language == 'en_US' || language == 'en'){
			title0 = menuRec0.getFieldValue('custrecord_clgx_menu_sys_title');
			description0 = menuRec0.getFieldValue('custrecord_clgx_menu_sys_description');
		}
		else{
			title0 = menuRec0.getFieldValue('custrecord_clgx_menu_sys_title_fr');
			description0 = menuRec0.getFieldValue('custrecord_clgx_menu_sys_description_fr');
		}
		
		if(hasRightsAll0 == 'T' || (inArray(currentRole,arrRecRoles0)) || (inArray(currentUser,arrRecUsers0)) ){
			
			var arguments = '';
			if(searchMenuSection.getValue('custrecord_clgx_menu_sys_type', null, null) == 1){
				arguments = '&id=' + searchMenuSection.getValue('custrecord_clgx_menu_sys_fileid', null, null);
			}
			
			htmlMenu += '<li>';
			if(searchMenuSection.getValue('custrecord_clgx_menu_sys_href', null, null) == '#' || searchMenuSection.getValue('custrecord_clgx_menu_sys_href', null, null) == ''){
				htmlMenu += '<a class="drop" href="#" title="' + description0 + '">' + title0 + '</a>\n';
			}
			else{
				htmlMenu += '<a class="drop" href="' + searchMenuSection.getValue('custrecord_clgx_menu_sys_href', null, null) + arguments + '" title="' + description0 + '" target="content">' + title0 + '</a>\n';
			}
	
			var parentMenuID = searchMenuSection.getValue('internalid', null, null);
	
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_parent',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_sort_order',null,null).setSort(false));
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_title',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_description',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_type',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_fileid',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_href',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_all_roles',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_roles',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_employees',null,null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
			arrFilters.push(new nlobjSearchFilter('custrecord_clgx_menu_sys_parent',null,'anyof',parentMenuID));
			var searchMenuLevel1Elements = nlapiSearchRecord('customrecord_clgx_menus_system', null, arrFilters, arrColumns);
	
			if(searchMenuLevel1Elements != null){
				
				htmlMenu += '<div class="dropdown_1column">\n';
				htmlMenu += '<div class="col_1 firstcolumn">\n';
				htmlMenu += '<ul class="levels">';
	// Start Level 1 of Menu -------------------------------------------------------------------------------------------------------
				for ( var j = 0; searchMenuLevel1Elements != null && j < searchMenuLevel1Elements.length; j++ ) {
					var searchMenuLevel1Element = searchMenuLevel1Elements[j];
					
					// determine if user has rights to see this menu element
					var internalid1 = searchMenuLevel1Element.getValue('internalid', null, null);
					var menuRec1 = nlapiLoadRecord ('customrecord_clgx_menus_system',internalid1);
					hasRightsAll1 = menuRec1.getFieldValue('custrecord_clgx_menu_sys_all_roles');
					var arrRecUsers1 = menuRec1.getFieldValues('custrecord_clgx_menu_sys_employees');
					var arrRecRoles1 = menuRec1.getFieldValues('custrecord_clgx_menu_sys_roles');
					
					if(language == 'en_US' || language == 'en'){
						title1 = menuRec1.getFieldValue('custrecord_clgx_menu_sys_title');
						description1 = menuRec1.getFieldValue('custrecord_clgx_menu_sys_description');
					}
					else{
						title1 = menuRec1.getFieldValue('custrecord_clgx_menu_sys_title_fr');
						description1 = menuRec1.getFieldValue('custrecord_clgx_menu_sys_description_fr');
					}
					
			        if(hasRightsAll1 == 'T' || (inArray(currentRole,arrRecRoles1)) || (inArray(currentUser,arrRecUsers1)) ){
		
						var arguments = '';
						if(searchMenuLevel1Element.getValue('custrecord_clgx_menu_sys_type', null, null) == 1){
							arguments = '&id=' + searchMenuLevel1Element.getValue('custrecord_clgx_menu_sys_fileid', null, null);
						}
						
						htmlMenu += '<li>\n';
						if(searchMenuLevel1Element.getValue('custrecord_clgx_menu_sys_href', null, null) == '#' || searchMenuLevel1Element.getValue('custrecord_clgx_menu_sys_href', null, null) == ''){
							htmlMenu += '<a href="#" title="' + description1 + '">' + title1 + '</a>\n';
						}
						else{
							htmlMenu += '<a href="' + searchMenuLevel1Element.getValue('custrecord_clgx_menu_sys_href', null, null) + arguments + '" title="' + description1 + '" target="content">' + title1 + '</a>\n';
						}
						var parentMenuLevel1ID = searchMenuLevel1Element.getValue('internalid', null, null);
						
						var arrColumns = new Array();
						arrColumns.push(new nlobjSearchColumn('internalid',null,null));
						arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_parent',null,null));
						arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_sort_order',null,null).setSort(false));
						arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_title',null,null));
						arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_description',null,null));
						arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_type',null,null));
						arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_fileid',null,null));
						arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_href',null,null));
						arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_all_roles',null,null));
						arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_roles',null,null));
						arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_employees',null,null));
						var arrFilters = new Array();
						arrFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
						arrFilters.push(new nlobjSearchFilter('custrecord_clgx_menu_sys_parent',null,'anyof',parentMenuLevel1ID));
						var searchMenuLevel2Elements = nlapiSearchRecord('customrecord_clgx_menus_system', null, arrFilters, arrColumns);
		
						if(searchMenuLevel2Elements != null){
							htmlMenu += '<ul>';
		// Start Level 2 of Menu -------------------------------------------------------------------------------------------------------
							for ( var k = 0; searchMenuLevel2Elements != null && k < searchMenuLevel2Elements.length; k++ ) {
								var searchMenuLevel2Element = searchMenuLevel2Elements[k];
								
								// determine if user has rights to see this menu element
								var internalid2 = searchMenuLevel2Element.getValue('internalid', null, null);
								var menuRec2 = nlapiLoadRecord ('customrecord_clgx_menus_system',internalid2);
								hasRightsAll2 = menuRec2.getFieldValue('custrecord_clgx_menu_sys_all_roles');
								var arrRecUsers2 = menuRec2.getFieldValues('custrecord_clgx_menu_sys_employees');
								var arrRecRoles2 = menuRec2.getFieldValues('custrecord_clgx_menu_sys_roles');
								
								if(language == 'en_US' || language == 'en'){
									title2 = menuRec2.getFieldValue('custrecord_clgx_menu_sys_title');
									description2 = menuRec2.getFieldValue('custrecord_clgx_menu_sys_description');
								}
								else{
									title2 = menuRec2.getFieldValue('custrecord_clgx_menu_sys_title_fr');
									description2 = menuRec2.getFieldValue('custrecord_clgx_menu_sys_description_fr');
								}
								
						        if(hasRightsAll2 == 'T' || (inArray(currentRole,arrRecRoles2)) || (inArray(currentUser,arrRecUsers2)) ){

									var arguments = '';
									if(searchMenuLevel2Element.getValue('custrecord_clgx_menu_sys_type', null, null) == 1){
										arguments = '&id=' + searchMenuLevel2Element.getValue('custrecord_clgx_menu_sys_fileid', null, null);
									}
									
									htmlMenu += '<li>\n';
									if(searchMenuLevel2Element.getValue('custrecord_clgx_menu_sys_href', null, null) == '#' || searchMenuLevel2Element.getValue('custrecord_clgx_menu_sys_href', null, null) == ''){
										htmlMenu += '<a href="#" title="' + description2 + '">' + title2 + '</a>\n';
									}
									else{
										htmlMenu += '<a href="' + searchMenuLevel2Element.getValue('custrecord_clgx_menu_sys_href', null, null) + arguments + '" title="' + description2 + '" target="content">' + title2 + '</a>\n';
									}
									var parentMenuLevel2ID = searchMenuLevel2Element.getValue('internalid', null, null);
									
									var arrColumns = new Array();
									arrColumns.push(new nlobjSearchColumn('internalid',null,null));
									arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_parent',null,null));
									arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_sort_order',null,null).setSort(false));
									arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_title',null,null));
									arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_description',null,null));
									arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_type',null,null));
									arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_fileid',null,null));
									arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_href',null,null));
									arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_all_roles',null,null));
									arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_roles',null,null));
									arrColumns.push(new nlobjSearchColumn('custrecord_clgx_menu_sys_employees',null,null));
									var arrFilters = new Array();
									arrFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
									arrFilters.push(new nlobjSearchFilter('custrecord_clgx_menu_sys_parent',null,'anyof',parentMenuLevel2ID));
									var searchMenuLevel3Elements = nlapiSearchRecord('customrecord_clgx_menus_system', null, arrFilters, arrColumns);
									
									if(searchMenuLevel3Elements != null){
										htmlMenu += '<ul>';
										
										
			// Start Level 3 of Menu -------------------------------------------------------------------------------------------------------
										for ( var l = 0; searchMenuLevel3Elements != null && l < searchMenuLevel3Elements.length; l++ ) {
											var searchMenuLevel3Element = searchMenuLevel3Elements[l];

											// determine if user has rights to see this menu element
											var internalid3 = searchMenuLevel3Element.getValue('internalid', null, null);
											var menuRec3 = nlapiLoadRecord ('customrecord_clgx_menus_system',internalid3);
											hasRightsAll3 = menuRec3.getFieldValue('custrecord_clgx_menu_sys_all_roles');
											var arrRecUsers3 = menuRec3.getFieldValues('custrecord_clgx_menu_sys_employees');
											var arrRecRoles3 = menuRec3.getFieldValues('custrecord_clgx_menu_sys_roles');
											
											if(language == 'en_US' || language == 'en'){
												title3 = menuRec3.getFieldValue('custrecord_clgx_menu_sys_title');
												description3 = menuRec3.getFieldValue('custrecord_clgx_menu_sys_description');
											}
											else{
												title3 = menuRec3.getFieldValue('custrecord_clgx_menu_sys_title_fr');
												description3 = menuRec3.getFieldValue('custrecord_clgx_menu_sys_description_fr');
											}
											
									        if(hasRightsAll2 == 'T' || (inArray(currentRole,arrRecRoles3)) || (inArray(currentUser,arrRecUsers3)) ){
										     
												var arguments = '';
												if(searchMenuLevel3Element.getValue('custrecord_clgx_menu_sys_type', null, null) == 1){
													arguments = '&id=' + searchMenuLevel3Element.getValue('custrecord_clgx_menu_sys_fileid', null, null);
												}
												
												htmlMenu += '<li>\n';
												if(searchMenuLevel3Element.getValue('custrecord_clgx_menu_sys_href', null, null) == '#' || searchMenuLevel3Element.getValue('custrecord_clgx_menu_sys_href', null, null) == ''){
													htmlMenu += '<a href="#" title="' + description3 + '">' + title3 + '</a>\n';
												}
												else{
													htmlMenu += '<a href="' + searchMenuLevel3Element.getValue('custrecord_clgx_menu_sys_href', null, null) + arguments + '" title="' + description3 + '" target="content">' + title3 + '</a>\n';
												}
												var parentMenuLevel3ID = searchMenuLevel3Element.getValue('internalid', null, null);
												
												
												
												
												// Insert more menu levels here
												
												
												
												
												htmlMenu += '</li>\n';
									        }
										}
										htmlMenu += '</ul>\n';
			// Finish Level 3 of Menu -------------------------------------------------------------------------------------------------------
									}
									htmlMenu += '</li>\n';
						        }
							}
							htmlMenu += '</ul>\n';
		// Finish Level 2 of Menu -------------------------------------------------------------------------------------------------------
						}
						htmlMenu += '</li>\n';
			        }
				}
				htmlMenu += '</ul>\n';
	// Finish Level 1 of Menu -------------------------------------------------------------------------------------------------------
				htmlMenu += '</div>\n';
				htmlMenu += '</div>\n';
			}
			htmlMenu += '</li>\n';
		}
	}
// Finish Menu Sections -------------------------------------------------------------------------------------------------------
    return htmlMenu;
}


function getRandomInt (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function stringToArray (str) {
 //Use ChrCode 5 as a separator
 var strChar5 = String.fromCharCode(5);
 //Use the Split method to create an array, where Chrcode 5 is the separator/delimiter
 var multiSelectStringArray = str.split(strChar5);
         return multiSelectStringArray;
}

function inArray(val, arr){	
    var bIsValueFound = false;
    if (arr != null){
	    for(var i = 0; i < arr.length; i++){
	        if(val == arr[i]){
	            bIsValueFound = true;
	            break;
	        }
	    }
    }
    return bIsValueFound;
}