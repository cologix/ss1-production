nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SP_Intranet.js
//	Script Name:	CLGX_SP_Intranet
//	Script Id:		customscript_clgx_sl_intranet
//	Script Runs:	On Server
//	Script Type:	Portlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		04/03/2013
//-------------------------------------------------------------------------------------------------

function portlet_intranet (portlet, column)
{
	portlet.setTitle('ePortal');
	var content = '';
	//content = '<iframe name="ePortal" id="ePortal" src="/app/site/hosting/restlet.nl?script=187&deploy=1" height="700px" width="1600px" frameborder="0" scrolling="no"></iframe>';
	content = '<iframe name="ePortal" id="ePortal" src="/app/site/hosting/scriptlet.nl?script=398&deploy=1" height="700px" width="1600px" frameborder="0" scrolling="no"></iframe>';
	portlet.setHtml(content);
	
	//nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_intranet_frame', 'customdeploy_clgx_sl_intranet_frame');
	
}
