nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_Intranet_File_Cabinet.js
//	Script Name:	CLGX_RL_Intranet_File_Cabinet
//	Script Id:		customscript_clgx_rl_intranet_file_cabinet
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/31/2013
//-------------------------------------------------------------------------------------------------

function restlet_intranet_file_cabinet (datain){
	try {
		var txtArg = datain;
		txtArg = txtArg.replace(/\"/g,"");
		txtArg = txtArg.replace(/\:/g,",");
		txtArg = txtArg.replace(/\{/g,"");
		txtArg = txtArg.replace(/\}/g,"");
		var arrArguments = [];
		var arrArguments = txtArg.split( "," );
		
		var pdfFile = nlapiLoadFile(arrArguments[1]);
		var urlFile = pdfFile.getURL();
		var nameFile = pdfFile.getName();

		var objFile = nlapiLoadFile(348199);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{urlFile}','g'),urlFile);
		html = html.replace(new RegExp('{nameFile}','g'),nameFile);
		return html;
	} 
	catch (error) {

		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}
