nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Intranet_File_Cabinet.js
//	Script Name:	CLGX_SL_Intranet_File_Cabinet
//	Script Id:		customscript_clgx_sl_intranet_file_cabinet
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/31/2013
//-------------------------------------------------------------------------------------------------

function suitelet_intranet_file_cabinet (){
	try {

		var id = request.getParameter('id');
		
		var pdfFile = nlapiLoadFile(id);
		var urlFile = pdfFile.getURL();
		var nameFile = pdfFile.getName();

		var objFile = nlapiLoadFile(348199);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{urlFile}','g'),urlFile);
		html = html.replace(new RegExp('{nameFile}','g'),nameFile);
		
		response.write( html );
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function buildAnnouncements(){

	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_eportal_announcement',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	var searchAnnouncements = nlapiSearchRecord('customrecord_clgx_eportal_announcements', null, arrFilters, arrColumns);

	var htmlAnnouncements = '';

	for ( var i = 0; searchAnnouncements != null && i < searchAnnouncements.length; i++ ) {
		var searchAnnouncement = searchAnnouncements[i];
		htmlAnnouncements += '\n<li><a href="#" target="_self">' + searchAnnouncement.getValue('custrecord_clgx_eportal_announcement', null, null) + '</a></li>';
	}
    return htmlAnnouncements;
}

