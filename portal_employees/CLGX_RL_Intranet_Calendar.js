nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_Intranet_Calendar.js
//	Script Name:	CLGX_RL_Intranet_Calendar
//	Script Id:		customscript_clgx_rl_intranet_calendar
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/31/2013
//-------------------------------------------------------------------------------------------------

function restlet_intranet_employee_calendar (){
	try {
		
		var currentContext = nlapiGetContext();
		var userID = nlapiGetContext().getUser();
		var userEmail = nlapiLookupField('employee', userID, 'email');

		return '<iframe src="https://www.google.com/calendar/embed?showTitle=0&amp;mode=WEEK&amp;src=' + userEmail + '&amp;height=600&amp;wkst=1" width="1000" height="580" frameborder="0" scrolling="no"></iframe>';

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
