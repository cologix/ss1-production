//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Intranet_News_Redirect.js
//	Script Name:	CLGX_SL_Intranet_News_Redirect
//	Script Id:		customscript_clgx_sl_intranet_news_redirect
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/31/2013
//-------------------------------------------------------------------------------------------------

function suitelet_intranet_news_redirect (){
	try {
		//nlapiSetRedirectURL('EXTERNAL', 'https://www.cologix.com/netsuite_includes/intranet/industry_news.html');

		var objFile = nlapiLoadFile(805999);
		var html = objFile.getValue();
		response.write( html );
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
