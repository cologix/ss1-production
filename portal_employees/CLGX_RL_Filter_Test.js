nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_Filter_Test.js
//	Script Name:	CLGX_RL_Filter_Test
//	Script Id:		customscript_clgx_rl_filter_test
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/31/2013
//-------------------------------------------------------------------------------------------------

function restlet_intranet_filter_test (){
	try {
		var objFile = nlapiLoadFile(351071);
		var html = objFile.getValue();
		//html = html.replace(new RegExp('{menu}','g'),buildMenu());
		return html;

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


