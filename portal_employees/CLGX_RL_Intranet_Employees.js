nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_Intranet_Employee.js
//	Script Name:	CLGX_RL_Intranet_Employee
//	Script Id:		customscript_clgx_rl_intranet_employee
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		02/21/2013
//-------------------------------------------------------------------------------------------------

function restlet_intranet_employee (){
	try {
		var objFile = nlapiLoadFile(348039);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{employeesJSON}','g'),employeesJSON());
		html = html.replace(new RegExp('{functionsJSON}','g'),functionsJSON());
		return html;
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function employeesJSON(){

	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('firstname',null,null).setSort(false));
	arrColumns.push(new nlobjSearchColumn('lastname',null,null).setSort(false));
	arrColumns.push(new nlobjSearchColumn('image',null,null));
	arrColumns.push(new nlobjSearchColumn('title',null,null));
	arrColumns.push(new nlobjSearchColumn('email',null,null));
	arrColumns.push(new nlobjSearchColumn('phone',null,null));
	arrColumns.push(new nlobjSearchColumn('mobilephone',null,null));
	arrColumns.push(new nlobjSearchColumn('custentity_cologix_skype_id',null,null));
	arrColumns.push(new nlobjSearchColumn('departmentnohierarchy',null,null));
	arrColumns.push(new nlobjSearchColumn('locationnohierarchy',null,null));
	arrColumns.push(new nlobjSearchColumn('supervisor',null,null))
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	arrFilters.push(new nlobjSearchFilter('email',null,'contains','cologix.com'));
	arrFilters.push(new nlobjSearchFilter('internalid',null,'noneof',12827));
	var searchEmployees = nlapiSearchRecord('employee', null, arrFilters, arrColumns);

	var stEmployees = 'var dataEmployees =  [';

	for ( var i = 0; searchEmployees != null && i < searchEmployees.length; i++ ) {
		var searchEmployee = searchEmployees[i];

		var imageID = searchEmployee.getValue('image', null, null);
		if(imageID != null && imageID !=''){
			var imageFile = nlapiLoadFile(imageID);
			var imageURL = imageFile.getURL();
		}
		else{
			var imageURL = '';
		}
		
		stEmployees += '\n{employeeid:' + searchEmployee.getValue('internalid', null, null) + 
						',name:"' +  searchEmployee.getValue('firstname', null, null) + ' ' + searchEmployee.getValue('lastname', null, null) +
						'",image:"' + imageURL + 
						'",title:"' + searchEmployee.getValue('title', null, null) + 
						'",email:"' + searchEmployee.getValue('email', null, null) + 
						'",phone:"' + searchEmployee.getValue('phone', null, null) + 
						'",mobile:"' + searchEmployee.getValue('mobilephone', null, null) + 
						'",skype:"' + searchEmployee.getValue('custentity_cologix_skype_id', null, null) + 
						'",department:"' + searchEmployee.getText('departmentnohierarchy', null, null) + 
						'",location:"' + searchEmployee.getText('locationnohierarchy', null, null) + 
						'",supervisor:"' + searchEmployee.getText('supervisor', null, null) + 
						'",supervisorid:"' + searchEmployee.getValue('supervisor', null, null) + '"},'; 
	}

	var strLen = stEmployees.length;
	if (searchEmployees != null){
		stEmployees = stEmployees.slice(0,strLen-1);
	}
	stEmployees += '];';
    return stEmployees;
}


function functionsJSON(){

	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('firstname',null,null).setSort(false));
	arrColumns.push(new nlobjSearchColumn('lastname',null,null).setSort(false));
	arrColumns.push(new nlobjSearchColumn('image',null,null));
	arrColumns.push(new nlobjSearchColumn('title',null,null));
	arrColumns.push(new nlobjSearchColumn('email',null,null));
	arrColumns.push(new nlobjSearchColumn('phone',null,null));
	arrColumns.push(new nlobjSearchColumn('mobilephone',null,null));
	arrColumns.push(new nlobjSearchColumn('custentity_cologix_skype_id',null,null));
	arrColumns.push(new nlobjSearchColumn('departmentnohierarchy',null,null));
	arrColumns.push(new nlobjSearchColumn('locationnohierarchy',null,null));
	arrColumns.push(new nlobjSearchColumn('supervisor',null,null))
	arrColumns.push(new nlobjSearchColumn('custentity_clgx_functional_lead',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	arrFilters.push(new nlobjSearchFilter('custentity_clgx_functional_lead',null,'noneof','@NONE@'));
	arrFilters.push(new nlobjSearchFilter('internalid',null,'noneof',12827));
	var searchEmployees = nlapiSearchRecord('employee', null, arrFilters, arrColumns);

	var stEmployees = 'var dataFunctions =  [';

	for ( var i = 0; searchEmployees != null && i < searchEmployees.length; i++ ) {
		var searchEmployee = searchEmployees[i];

		var strFunctions = searchEmployee.getValue('custentity_clgx_functional_lead', null, null);

		if (strFunctions != '' && strFunctions != null){
		
			var imageID = searchEmployee.getValue('image', null, null);
			if(imageID != null && imageID !=''){
				var imageFile = nlapiLoadFile(imageID);
				var imageURL = imageFile.getURL();
			}
			else{
				var imageURL = '';
			}

			var arrFunctions = new Array();
			var arrFunctions = strFunctions.split(",");
			
			for(var j=0; j<arrFunctions.length; j++){
			
				stEmployees += '\n{employeeid:' + searchEmployee.getValue('internalid', null, null) + 
								',name:"' +  searchEmployee.getValue('firstname', null, null) + ' ' + searchEmployee.getValue('lastname', null, null) +
								'",image:"' + imageURL + 
								'",title:"' + searchEmployee.getValue('title', null, null) + 
								'",email:"' + searchEmployee.getValue('email', null, null) + 
								'",phone:"' + searchEmployee.getValue('phone', null, null) + 
								'",mobile:"' + searchEmployee.getValue('mobilephone', null, null) + 
								'",skype:"' + searchEmployee.getValue('custentity_cologix_skype_id', null, null) + 
								'",department:"' + searchEmployee.getText('departmentnohierarchy', null, null) + 
								'",location:"' + searchEmployee.getText('locationnohierarchy', null, null) + 
								'",supervisor:"' + searchEmployee.getText('supervisor', null, null) + 
								'",supervisorid:"' + searchEmployee.getValue('supervisor', null, null) + 
								'",function:"' + arrFunctions[j] + '"},'; 
			}
		}
		}

	var strLen = stEmployees.length;
	if (searchEmployees != null){
		//stEmployees = stEmployees.slice(0,strLen-1);
	}
	stEmployees += '];';
    return stEmployees;
}