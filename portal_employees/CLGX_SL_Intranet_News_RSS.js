nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Intranet_News_RSS.js
//	Script Name:	CLGX_SL_Intranet_News_RSS
//	Script Id:		customscript_clgx_sl_intranet_news_rss
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/31/2013
//-------------------------------------------------------------------------------------------------

function suitelet_intranet_news_rss (){
    try {
        //nlapiSetRedirectURL('EXTERNAL', 'https://www.cologix.com/netsuite_includes/intranet/industry_news.html');

        var objFile = nlapiLoadFile(814709);
        var html = objFile.getValue();
        html = html.replace(new RegExp('{cologixNews}','g'),cologixNews());
        html = html.replace(new RegExp('{industryNews1}','g'),industryNews1());
        html = html.replace(new RegExp('{Announcements}','g'),buildAnnouncements());
        html = html.replace(new RegExp('{industryNews}','g'),industryNews());
        response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

//function cologixNews (){
//
//    //var response = nlapiRequestURL('https://www.cologix.com/en/company-profile/news.html?format=feed&type=rss');
//    //var response = nlapiRequestURL('http://www.cologix.com/cologix.com/en/index.php?option=com_content&view=category&id=12&format=feed&type=rss');
//    //var response=nlapiLoadFile(3356619);
//	
//	var response = nlapiRequestURL('http://www.cologix.com/feed/atom/');
//	var xmlNews = nlapiStringToXML(removeNamespace(response.getBody()));
//
//	var xmlItems = nlapiSelectNodes(xmlNews, '//entry');
//
//    var arrTitles = new Array();
//    var arrLinks = new Array();
//    var arrDescr = new Array();
//    var arrAuthors = new Array();
//    var arrPubDates = new Array();
//
//    for ( var i = 0; i < xmlItems.length; i++ ) {
//    	var strTitle = nlapiSelectValue(xmlItems[i], 'title');
//        arrTitles.push(strTitle);
//        var strLink = nlapiSelectValue(xmlItems[i], 'link/@href');
//        arrLinks.push(strLink);
//        var strDescr = nlapiSelectValue(xmlItems[i], 'summary');
//        arrDescr.push(strDescr);
//        var strAuthor = nlapiSelectValue(xmlItems[i], 'author/name');
//        arrAuthors.push(strAuthor);
//        var strPubDate = nlapiSelectValue(xmlItems[i], 'published');
//        arrPubDates.push(strPubDate);
//    }
//
//    var html = '';
//    for(var i=0; i< (xmlItems.length < 4 ? xmlItems.length : 4); i++){
//        html += '<div class="feedget-element" data-element-id="0" style="height: 164px; opacity: 1;">';
//        html += '<div class="feedget-element-header">';
//        html += '<span class="feedget-element-title">';
//        html += '<a href="' + arrLinks[i] + '" target="_blank">' + arrTitles[i] + '</a>';
//        html += '</span>';
//        html += '<span class="feedget-element-date">' + arrPubDates[i] + '</span>';
//        html += '</div>';
//        html += '<span class="feedget-element-text">' + arrDescr[i] + '</span>';
//        html += '</div>';
//    }
//
//    return html;
//}
//
//function cologixNewsUpd() {
//	var response = nlapiRequestURL("https://www.cologix.com/news/press-release/");
//	
//}
function cologixNews(){

	//var response = nlapiRequestURL( 'https://www.datacenterdynamics.com/rss/news');
  	var response = nlapiRequestURL( 'https://www.cologix.com/feed/?post_type=cl_blog');
    var xmlNews = nlapiStringToXML(response.getBody());

    var xmlItems = nlapiSelectNodes(xmlNews, '/rss/channel/item');

    var arrTitles = new Array();
    var arrLinks = new Array();
    var arrDescr = new Array();
    var arrAuthors = new Array();
    var arrPubDates = new Array();

    for ( var i = 0; i < 2; i++ ) {
        var strTitle = nlapiSelectValue(xmlItems[i], 'title');
        arrTitles.push(strTitle);
        var strLink = nlapiSelectValue(xmlItems[i], 'link');
        arrLinks.push(strLink);
        var strDescr = nlapiSelectValue(xmlItems[i], 'description');
        arrDescr.push(strDescr);
        var strAuthor = nlapiSelectValue(xmlItems[i], 'author');
        arrAuthors.push(strAuthor);
        var strPubDate = nlapiSelectValue(xmlItems[i], 'pubDate');
        arrPubDates.push(strPubDate);
    }

    var html = '';
    for(var i=0; i< 2; i++){
    	     html += '<div class="feedget-element" data-element-id="2" style="height: 200px; opacity: 1;">';
    	        html += '<div class="feedget-element-header">';
    	        html += '<span class="feedget-element-title">';
    	        html += '<a href="' + arrLinks[i] + '" target="_blank">' + arrTitles[i] + '</a>';
    	        html += '</span>';
    	        html += '<span class="feedget-element-date">' + arrPubDates[i] + '</span>';
    	        html += '</div>';
    	        var desc = arrDescr[i];
    	        var descFinal=arrDescr[i];
    	        if(desc.indexOf("<img src='/sites/default/files/imagecache/rss-feed-image/") > -1){
    	            var sSplit=desc.split("<img src='/sites/default/files/imagecache/rss-feed-image/");
    	            var descFinal=sSplit[0]+"<img width='380px'  height='200px' src='https://www.cologix.com/sites/default/files/"+sSplit[1];
    	        }
    	        html += '<span class="feedget-element-text">' + descFinal + '</span>';
    	        html += '</div>';
    }

    return html;

}
function industryNews1(){
	//var response = nlapiRequestURL( 'https://www.datacenterdynamics.com/rss/news');
	var response = nlapiRequestURL( 'https://www.cologix.com/feed/?post_type=cl_resource');
    var xmlNews = nlapiStringToXML(response.getBody());

    var xmlItems = nlapiSelectNodes(xmlNews, '/rss/channel/item');

    var arrTitles = new Array();
    var arrLinks = new Array();
    var arrDescr = new Array();
    var arrAuthors = new Array();
    var arrPubDates = new Array();

    for ( var i = 0; i < 2; i++ ) {
        var strTitle = nlapiSelectValue(xmlItems[i], 'title');
        arrTitles.push(strTitle);
        var strLink = nlapiSelectValue(xmlItems[i], 'link');
        arrLinks.push(strLink);
        var strDescr = nlapiSelectValue(xmlItems[i], 'description');
        arrDescr.push(strDescr);
        var strAuthor = nlapiSelectValue(xmlItems[i], 'author');
        arrAuthors.push(strAuthor);
        var strPubDate = nlapiSelectValue(xmlItems[i], 'pubDate');
        arrPubDates.push(strPubDate);
    }

    var html = '';
    for(var i=0; i< 2; i++){
    	     html += '<div class="feedget-element" data-element-id="0" style="height: 200px; opacity: 1;">';
    	        html += '<div class="feedget-element-header">';
    	        html += '<span class="feedget-element-title">';
    	        html += '<a href="' + arrLinks[i] + '" target="_blank">' + arrTitles[i] + '</a>';
    	        html += '</span>';
    	        html += '<span class="feedget-element-date">' + arrPubDates[i] + '</span>';
    	        html += '</div>';
    	        var desc = arrDescr[i];
    	        var descFinal=arrDescr[i];
    	        if(desc.indexOf("<img src='/sites/default/files/imagecache/rss-feed-image/") > -1){
    	            var sSplit=desc.split("<img src='/sites/default/files/imagecache/rss-feed-image/");
    	            var descFinal=sSplit[0]+"<img width='380px'  height='200px' src='http://www.cologix.com/sites/default/files/"+sSplit[1];
    	        }
    	        html += '<span class="feedget-element-text">' + descFinal + '</span>';
    	        html += '</div>';
    }

    return html;

}
function industryNews(){

    //var response = nlapiRequestURL('http://www.datacenterdynamics.com/focus/themes/colocation-and-hosting/feed');
    //var response = nlapiRequestURL( 'https://www.datacenterdynamics.com/rss/news');
    var response = nlapiRequestURL( 'https://www.cologix.com/feed/?post_type=cl_news');
    var xmlNews = nlapiStringToXML(response.getBody());

    var xmlItems = nlapiSelectNodes(xmlNews, '/rss/channel/item');

    var arrTitles = new Array();
    var arrLinks = new Array();
    var arrDescr = new Array();
    var arrAuthors = new Array();
    var arrPubDates = new Array();

    for ( var i = 2; i < xmlItems.length; i++ ) {
        var strTitle = nlapiSelectValue(xmlItems[i], 'title');
        arrTitles.push(strTitle);
        var strLink = nlapiSelectValue(xmlItems[i], 'link');
        arrLinks.push(strLink);
        var strDescr = nlapiSelectValue(xmlItems[i], 'description');
        arrDescr.push(strDescr);
        var strAuthor = nlapiSelectValue(xmlItems[i], 'author');
        arrAuthors.push(strAuthor);
        var strPubDate = nlapiSelectValue(xmlItems[i], 'pubDate');
        arrPubDates.push(strPubDate);
    }

    var html = '';
    for(var i=2; i< 4; i++){
    	     html += '<div class="feedget-element" data-element-id="1" style="height: 200px; opacity: 1;">';
    	        html += '<div class="feedget-element-header">';
    	        html += '<span class="feedget-element-title">';
    	        html += '<a href="' + arrLinks[i] + '" target="_blank">' + arrTitles[i] + '</a>';
    	        html += '</span>';
    	        html += '<span class="feedget-element-date">' + arrPubDates[i] + '</span>';
    	        html += '</div>';
    	        var desc = arrDescr[i];
    	        var descFinal=arrDescr[i];
    	        if(desc.indexOf("<img src='/sites/default/files/imagecache/rss-feed-image/") > -1){
    	            var sSplit=desc.split("<img src='/sites/default/files/imagecache/rss-feed-image/");
    	            //var descFinal=sSplit[0]+"<img width='380px'  height='200px' src='http://www.datacenterdynamics.com/sites/default/files/"+sSplit[1];
                  var descFinal=sSplit[0]+"<img width='380px'  height='200px' src='https://www.cologix.com/sites/default/files/"+sSplit[1];
    	        }
    	        html += '<span class="feedget-element-text">' + descFinal + '</span>';
    	        html += '</div>';
    }

    return html;

}


function buildAnnouncements(){

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_eportal_announcement',null,null));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('custrecord_clgx_news_on_eportal',null,'is','T'));
    arrFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
    var searchAnnouncements = nlapiSearchRecord('customrecord_clgx_eportal_announcements', null, arrFilters, arrColumns);

    var htmlAnnouncements = '';

    for ( var i = 0; searchAnnouncements != null && i < searchAnnouncements.length; i++ ) {
        var searchAnnouncement = searchAnnouncements[i];
        htmlAnnouncements += '\n<li><a href="#" target="_self">' + searchAnnouncement.getValue('custrecord_clgx_eportal_announcement', null, null) + '</a></li>';
    }
    return htmlAnnouncements;
}

function removeNamespace(xmlDocument) {
	return xmlDocument.replace(new RegExp("xmlns=\"http://www.w3.org/2005/Atom\"", "g"), "");
}