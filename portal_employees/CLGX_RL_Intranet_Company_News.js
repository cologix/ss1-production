//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_Intranet_Company_News.js
//	Script Name:	CLGX_RL_Intranet_Company_News
//	Script Id:		customscript_clgx_rl_intranet_comp_news
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/31/2013
//-------------------------------------------------------------------------------------------------

function restlet_intranet_comp_news (datain){
	try {
		var objFile = nlapiLoadFile(346194);
		var html = objFile.getValue();
		//html = html.replace(new RegExp('{customersJSON}','g'),customersJSON());
		return html;
	} 
	catch (error) {

		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}
