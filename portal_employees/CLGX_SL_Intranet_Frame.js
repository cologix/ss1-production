nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Intranet_Frame.js
//	Script Name:	CLGX_SL_Intranet_Frame
//	Script Id:		customscript_clgx_sl_intranet_frame
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/31/2013
//-------------------------------------------------------------------------------------------------

function suitelet_serv_inv_frame (request, response){
	try {
		var formFrame = nlapiCreateForm('employee Portal');
		var fieldFrame = formFrame.addField('custpage_clgx_frame_html','inlinehtml', null, null, null);
		var frameHTML = '<iframe name="servicePortal" id="servicePortal" src="/app/site/hosting/restlet.nl?script=187&deploy=1" height="700px" width="1420px" frameborder="0" scrolling="no"></iframe>';
		fieldFrame.setDefaultValue(frameHTML);
		response.writePage( formFrame );
} 
catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
	if (error.getDetails != undefined){
	    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	    throw error;
	}
	else{
	    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	    throw nlapiCreateError('99999', error.toString());
	}
} // End Catch Errors Section ------------------------------------------------------------------------------------------
}