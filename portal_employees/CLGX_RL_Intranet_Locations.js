nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_Intranet_Locations.js
//	Script Name:	CLGX_RL_Intranet_Locations
//	Script Id:		customscript_clgx_rl_intranet_locations
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		04/03/2013
//-------------------------------------------------------------------------------------------------

function restlet_intranet_locations (){
	try {
		var objFile = nlapiLoadFile(369115);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{locationsJSON}','g'),locationsJSON());
		return html;
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function locationsJSON(){

	var date = new Date();
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('name',null,null));
	arrColumns.push(new nlobjSearchColumn('phone',null,null));
	arrColumns.push(new nlobjSearchColumn('address1',null,null));
	arrColumns.push(new nlobjSearchColumn('address2',null,null));
	arrColumns.push(new nlobjSearchColumn('address3',null,null));
	arrColumns.push(new nlobjSearchColumn('city',null,null));
	arrColumns.push(new nlobjSearchColumn('country',null,null));
	arrColumns.push(new nlobjSearchColumn('zip',null,null));
	
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	arrFilters.push(new nlobjSearchFilter('internalid',null,'noneof',25));
	var searchLocations = nlapiSearchRecord('location', null, arrFilters, arrColumns);

	var stLocations = 'var dataLocations =  [';

	for ( var i = 0; searchLocations != null && i < searchLocations.length; i++ ) {
		var searchLocation = searchLocations[i];
		
		var strAddress = searchLocation.getValue('address1', null, null) + ' ' + searchLocation.getValue('address2', null, null) + ' ' + searchLocation.getValue('address3', null, null) + ' ' + searchLocation.getValue('city', null, null) + ' ' + searchLocation.getValue('country', null, null) + ' ' + searchLocation.getValue('zip', null, null);
		stLocations += '\n{location:"' + searchLocation.getValue('name', null, null) + 
						'",phone:"' +  searchLocation.getValue('phone', null, null) + 
						'",address:"' + strAddress + '"},'; 
	}

	var strLen = stLocations.length;
	if (searchLocations != null){
		stLocations = stLocations.slice(0,strLen-1);
	}
	stLocations += '];';
    return stLocations;
}