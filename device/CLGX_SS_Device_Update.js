nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Device_Update.js
//	Script Name:	CLGX_SS_Device_Update
//	Script Id:		customscript_clgx_ss_device_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------

function scheduled_device_update(){
    try{
        
        var context = nlapiGetContext();
        var deviceid  = context.getSetting('SCRIPT','custscript_clgx_device_update_id');
        
    	var externalid = nlapiLookupField('customrecord_clgx_dcim_devices', deviceid, 'externalid');

		var arrNew = get_new_points (externalid);
		var arrNewIds = _.compact(_.uniq(_.pluck(arrNew, 'externalid')));

		var arrOld = get_old_points (deviceid);
		var arrOldIds = _.compact(_.uniq(_.pluck(arrOld, 'externalid')));
		
		var arrDelete = _.difference(arrOldIds, arrNewIds);
		var arrUpdate = _.intersection(arrNewIds, arrOldIds);
		var arrAdd = _.difference(arrNewIds, arrOldIds);

		if(arrDelete.length > 0){
			var arrDeleted = delete_points (arrDelete,arrOld);
			//nlapiLogExecution('DEBUG','Deleted Points', '| Deleted = ' + arrDeleted[0].length + '| Errors = ' + arrDeleted[1].length + ' |');
		}
		if(arrUpdate.length > 0){
			var arrUpdated = update_points (arrUpdate,arrNew,arrOld);
			//nlapiLogExecution('DEBUG','Updated Points', '| Updated = ' + arrUpdated[0].length + '| Errors = ' + arrUpdated[1].length + ' |');
		}
		if(arrAdd.length > 0){
			var arrAdded = add_points (deviceid,arrAdd,arrNew);
			//nlapiLogExecution('DEBUG','Added Points', '| Added = ' + arrAdded[0].length + '| Errors = ' + arrAdded[1].length + ' |');
		}
		
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,null).setSort(true));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_od_fam_device",'CUSTRECORD_CLGX_POWER_PANEL_PDPM',"anyof",deviceid));
		var search = nlapiSearchRecord('customrecord_clgx_power_circuit', null, arrFilters, arrColumns);
		if(search){
			for ( var i = 0; search != null && i < search.length; i++ ) {
				var powerid = parseInt(search[i].getValue('internalid',null,null));	
				nlapiLogExecution('DEBUG','Updated Power', '| PowerID = ' + powerid +  ' |');
				var pwr = update_power (powerid);
			}
		}

        //nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
    	
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

