nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Device.js
//	Script Name:	CLGX_SU_Device
//	Script Id:		customscript_clgx_su_device
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		10/11/2016
//-------------------------------------------------------------------------------------------------

function beforeLoad (type, form) {
    try {
    	
    	var currentContext = nlapiGetContext();
        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'view' || type == 'edit')) {
        	
        	var deviceid = nlapiGetRecordId();
        	
    		var arrColumns = new Array();
    		arrColumns.push(new nlobjSearchColumn('internalid',null,null).setSort(true));
    		var arrFilters = new Array();
    		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_device",null,"anyof",deviceid));
    		var search = nlapiSearchRecord('customrecord_clgx_power_circuit', null, arrFilters, arrColumns);
    		
    		if(search){
    	    	var pwrsTab = form.addTab('custpage_tab_powers', 'Power Circuits');
    			var pwrsField = form.addField('custpage_powers','inlinehtml', null, null, 'custpage_tab_powers');
    			pwrsField.setDefaultValue('<iframe name="powers" id="powers" src="/app/site/hosting/scriptlet.nl?script=441&deploy=1&deviceid=' + deviceid + '" height="700px" width="1400px" frameborder="0" scrolling="no"></iframe>');
    		}
        }
    }
    catch (error) { 
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function beforeSubmit(type){
    try {
    	
    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function afterSubmit(type){
    try {
    	
		var deviceid = nlapiGetRecordId();
		var update = nlapiGetFieldValue('custrecord_clgx_dcim_device_upd_points');
    	
    	if(update == 'T'){
    		
    		nlapiSubmitField('customrecord_clgx_dcim_devices', deviceid, 'custrecord_clgx_dcim_device_upd_points', 'F');

    		var arrParam = new Array();
            arrParam['custscript_clgx_device_update_id'] = deviceid;
            nlapiScheduleScript('customscript_clgx_ss_device_update', null ,arrParam);  
    	}
    	
    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } 
}

