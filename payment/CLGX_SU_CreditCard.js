nlapiLogExecution("audit","FLOStart",new Date().getTime());
function afterSubmit(type){
	try {
		var currentContext = nlapiGetContext();
		if ((currentContext.getExecutionContext() == 'userevent') && (type == 'edit')) {
			var id = nlapiGetRecordId();
			var recCard = nlapiLoadRecord('customrecord_clgx_credit_cards', id);
			var company_id = recCard.getFieldValue('custrecord_token_customer');
			
			if (CC_HasCardOnFile(company_id) == false) {
				var recCompany = nlapiLoadRecord('customer', company_id);
				recCompany.setFieldValue('custentity_clgx_cc_enabled', 'F');
				nlapiSubmitRecord(recCompany, false, true);	
			}
		}
	} catch (error) { // Start Catch Errors Section
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error afterSubmit', error.getCode() + ': ' + error.getDetails());
			alert(error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR', 'Unexpected Error afterSubmit', error.toString());
			alert(error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}
