nlapiLogExecution("audit","FLOStart",new Date().getTime());
function RunQueue() {
	try {
		nlapiLogExecution('DEBUG', 'CC RunQueue Begin');
		CC_ProcessQueue();
		nlapiLogExecution('DEBUG', 'CC RunQueue End');
	} catch (error) {
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error CC_ProcessQueue', error.getCode() + ': ' + error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR', 'Unexpected Error CC_ProcessQueue', error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}