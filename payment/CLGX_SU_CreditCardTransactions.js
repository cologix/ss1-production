nlapiLogExecution("audit","FLOStart",new Date().getTime());
function beforeLoad(type, form) {
	try {
		var currentContext = nlapiGetContext();
		if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'view' || type == 'edit')) {
		
			var id = nlapiGetRecordId();
			var recQueue = nlapiLoadRecord('customrecord_clgx_ccards_processing', id);
			var status = recQueue.getFieldValue('custrecord_clgx_ccards_status');
			var type = recQueue.getFieldValue('custrecord_clgx_ccards_txn_type');
			var amount = recQueue.getFieldValue('custrecord_clgx_ccards_amount');
			var paymentid = recQueue.getFieldValue('custrecord_clgx_ccards_payment_id');
			var payment_amount = recQueue.getFieldValue('custrecord_clgx_ccards_amount');
			var payment_refunded = recQueue.getFieldValue('custrecord_clgx_ccards_refunded_amt');
			payment_unapplied = payment_amount - payment_refunded;
			//Test date
			var saledate = moment(recQueue.getFieldValue('custrecord_clgx_ccards_trans_date')).format('MM/DD/YYYY');
			if (saledate == null) {
				return;
			}			
			var today_date = moment().format('MM/DD/YYYY');
			if (today_date == saledate) { 
				today = true;
			} else {
				today = false;
			}		
			
			if ((today == false) && (status == 5) && (type == 1) && (amount > 0) && (payment_unapplied > 0)) {
				form.setScript('customscript_clgx_lib_creditcards');
				form.addButton('custpage_add_createlead_button', 'Issue Refund', 'UserEvent_RefundCharge()');
			}
			if ((today == true) && (status == 5) && (type == 1) && (amount > 0) && (payment_amount != null)) {
				form.setScript('customscript_clgx_lib_creditcards');
				form.addButton('custpage_add_createlead_button', 'Void Charge', 'UserEvent_VoidCharge()');
			}
		}
	} catch (error) { // Start Catch Errors Section
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error UserEvent_VoidCharge', error.getCode() + ': ' + error.getDetails());
			alert(error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR', 'Unexpected Error UserEvent_VoidCharge', error.toString());
			alert(error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}

function afterSubmit (type) {
	try {
		var currentContext = nlapiGetContext();
		if ((currentContext.getExecutionContext() == 'userevent') && (type == 'create' || type == 'edit')) {
			nlapiLogExecution('DEBUG', 'Process Error CreditCardTransactions_afterSubmit schedule it');
			ScheduleCCQueue();
			nlapiLogExecution('DEBUG', 'Process Error CreditCardTransactions_afterSubmit scheduled');
		}
	} catch (error) { // Start Catch Errors Section
		if (error.getDetails != undefined) {
			nlapiLogExecution('ERROR', 'Process Error CreditCardTransactions_afterSubmit', error.getCode() + ': ' + error.getDetails());
			alert(error.getDetails());
			throw error;
		} else {
			nlapiLogExecution('ERROR', 'Unexpected Error CreditCardTransactions_afterSubmit', error.toString());
			alert(error.toString());
			throw nlapiCreateError('99999', error.toString());
		}
	}
}