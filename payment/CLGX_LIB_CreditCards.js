nlapiLogExecution("audit","FLOStart",new Date().getTime());
// Credit Card Processing
/* **** QUEUE STATUS ***
 * 0 Pending Submit to Processor
 * 1 Pending Approve
 * 2 Pending Decline
 *
 * 10 Submitted to Processor
 *
 * 100 Success, Payment Created
 * 101 Voided
 *
 * 205 Error: Token Missing
 * 204 Error: No card on file
 * 203 Error: Balance less than zero
 * 202 Error: CC is not enabled on the account
 * 201 Error: Unable to load Company Record
 *
 * 301 Error: Processing API Error
 *
 */

/*
 * Merchant ID #1 = USD into US Bank SB
 * Merchant ID #2 = USD into US Bank PROD
 * Merchant ID #3 = CAD into CA Bank SB
 * Merchant ID #4 = CAD into CA Bank PROD
 */

var TXN_TYPE_SALE   = 1;
var TXN_TYPE_REFUND = 2;
var TXN_TYPE_VOID   = 3;

var SUBMIT_SOURCE_AUTOMATED = 1;
var SUBMIT_SOURCE_MANUAL    = 2;
var SUBMIT_SOURCE_WEB       = 3;

var STATUS_PENDING_SUBMIT 				= 1;
var STATUS_PENDING_APPROVE 				= 2;
var STATUS_PENDING_DECLINE 				= 3;
var STATUS_SUBMITTED 					= 4;
var STATUS_SUCCESS 						= 5;
var STATUS_VOIDED 						= 6;
var STATUS_ERROR_TOKEN 					= 7;
var STATUS_ERROR_NOCARD 				= 8;
var STATUS_ERROR_BALANCE_ZERO 			= 9;
var STATUS_ERROR_CC_NOT_ENABLED			= 10;
var STATUS_ERROR_UNABLE_TO_LOAD_COMPANY = 11;
var STATUS_ERROR_API_ERROR				= 12;

var objMTHS={'January':'01', 'February':'02', 'March':'03', 'April':'04', 'May':'05', 'June':'06', 'July':'07', 'August':'08', 'September':'09', 'October':'10', 'November':'11', 'December':'12'};


function CC_Process(customerArr) {
    // 1. Lookup queue.
    // 2. Lookup customer
    // 3. Verify the customer authorize profile
    // 4. Verify the default customer defaut card
    // 4. Verify customer has valid credit card enabled, else abort.
    // 5. Submit info to Authorize
    try {
        nlapiLogExecution('DEBUG', 'CC ProcessQueue: '+customerArr.customer);
        var merchant_id=1;
        var amount=parseFloat(customerArr.tocharge)
        var filters = new Array();
        var columns = new Array();
        //ENV=1; SB
        //ENV=2; PROD
        filters[0] = new nlobjSearchFilter('custrecord_clgx_auth_net_subsidiary',null, 'is', customerArr.subsidiaryid);
        filters[1] = new nlobjSearchFilter('custrecord_clgx_auth_net_type',null, 'is', 2);
        columns[0] = new nlobjSearchColumn('internalid', null, null);
        var searchresults = nlapiSearchRecord('customrecord_clgx_authorize_net', null, filters, columns);
        var merchant_id = searchresults[0].getValue(columns[0]);

        if(merchant_id ==2)
        {
            var currency=1;
        }
        else if(merchant_id ==4){
            var currency=3;
        }
        var sDate=moment().format('MM/DD/YYYY');
        var customerProfile = nlapiLookupField('customer', customerArr.customerid, 'custentity_clgx_customer_authorize_id') || 0;
        if (customerProfile == 0) {
            var recordTransaction = nlapiCreateRecord('customrecord_clgx_ccards_processing');
            recordTransaction.setFieldValue('custrecord_clgx_ccards_status', 7);
            recordTransaction.setFieldValue('custrecord_clgx_ccards_response_text', 'Error. No Authorize Customer ID');
            recordTransaction.setFieldValue('name', 'Automatic Credit Card Charge: '+customerArr.customer);
            //record.setFieldValue('custrecord_clgx_ccards_status', 0);
            recordTransaction.setFieldValue('custrecord_clgx_ccards_txn_type', TXN_TYPE_SALE);
            recordTransaction.setFieldValue('custrecord_clgx_ccards_submit_source', SUBMIT_SOURCE_AUTOMATED);
            recordTransaction.setFieldValue('custrecord_clgx_ccards_submit_date', sDate);
            recordTransaction.setFieldValue('custrecord_clgx_ccards_merchant_id', merchant_id);
            recordTransaction.setFieldValue('custrecord_clgx_ccards_amount',customerArr.tocharge);
            var id = nlapiSubmitRecord(recordTransaction, false, true);

            return;
        }
        else {
            var arrC = new Array();
            var arrF = new Array();
            arrF.push(new nlobjSearchFilter("custrecord_token_customer", null, "anyof", customerArr.customerid));

            var defaultCard = nlapiSearchRecord('customrecord_clgx_credit_cards', 'customsearch_clgx_ss_ccards_fortr', arrF, arrC);
            if (defaultCard == null) {
                //send an email to ...
                /*  var recordTransaction = nlapiCreateRecord('customrecord_clgx_ccards_processing');
                 recordTransaction.setFieldValue('custrecord_clgx_ccards_status', 7);
                 recordTransaction.setFieldValue('custrecord_clgx_ccards_response_text', 'Error. No Default Card');
                 recordTransaction.setFieldValue('name', 'Automatic Credit Card Charge: '+customerArr.customer);
                 //record.setFieldValue('custrecord_clgx_ccards_status', 0);
                 recordTransaction.setFieldValue('custrecord_clgx_ccards_txn_type', TXN_TYPE_SALE);
                 recordTransaction.setFieldValue('custrecord_clgx_ccards_submit_source', SUBMIT_SOURCE_AUTOMATED);
                 recordTransaction.setFieldValue('custrecord_clgx_ccards_submit_date', sDate);
                 recordTransaction.setFieldValue('custrecord_clgx_ccards_merchant_id', merchant_id);
                 recordTransaction.setFieldValue('custrecord_clgx_ccards_amount',customerArr.tocharge);
                 var id = nlapiSubmitRecord(recordTransaction, false, true);
                 return;*/
            }
            else {
                if(customerArr.tocharge>0) {
                    nlapiSubmitField('customer', customerArr.customerid, ['custentity_clgx_cc_paused'], 'T');
                    var searchCard = defaultCard[0];
                    var columnsC = searchCard.getAllColumns();
                    var paymentPID = searchCard.getValue(columnsC[0]);
                    var recordAuth = nlapiLoadRecord('customrecord_clgx_authorize_net', merchant_id);
                    var apiID = recordAuth.getFieldValue('custrecord_clgx_auth_net_api_id');
                    var apiKEY = recordAuth.getFieldValue('custrecord_clgx_auth_net_transaction_key');

                    //Send the transaction to Authorize;
                    var data = {
                        "createTransactionRequest": {
                            "merchantAuthentication": {
                                "name": apiID,
                                "transactionKey": apiKEY
                            },
                            "refId": "",
                            "transactionRequest": {
                                "transactionType": "authCaptureTransaction",
                                "amount": customerArr.tocharge,
                                "profile": {
                                    "customerProfileId": customerProfile,
                                    "paymentProfile": {"paymentProfileId": paymentPID}
                                }
                            }
                        }
                    };
                    var req = nlapiRequestURL('https://api.authorize.net/xml/v1/request.api', JSON.stringify(data), {'Content-type': 'application/json'}, null, 'POST');
                    var str = req.body;
                    //   nlapiSendEmail(206211, 206211, 'str', str, null, null, null, null,true);
                    var resp = JSON.parse(str.slice(1));
                    var resp_code = resp.messages.resultCode;
                    var message = resp.messages.message[0];
                    var messagesStatus = resp.transactionResponse.messages;
                    if (messagesStatus === undefined && (resp.transactionResponse.responseCode == 2 || resp.transactionResponse.responseCode == 3)) {
                        var errorObj = resp.transactionResponse.errors[0];
                        var errorCode = errorObj.errorcode;
                        var errorText = errorObj.errorText;
                        var mess_text = errorText;
                    }
                    else {
                        var mess_text = message.text;
                    }


                    var recordTransaction = nlapiCreateRecord('customrecord_clgx_ccards_processing');
                    recordTransaction.setFieldValue('custrecord_clgx_ccards_lstrres', str);
                    recordTransaction.setFieldValue('custrecord_clgx_ccards_customer', customerArr.customerid);
                    recordTransaction.setFieldValue('name', 'Automatic Credit Card Charge: ' + customerArr.customer);
                    //record.setFieldValue('custrecord_clgx_ccards_status', 0);
                    recordTransaction.setFieldValue('custrecord_clgx_ccards_txn_type', TXN_TYPE_SALE);
                    recordTransaction.setFieldValue('custrecord_clgx_ccards_submit_source', SUBMIT_SOURCE_AUTOMATED);
                    recordTransaction.setFieldValue('custrecord_clgx_ccards_submit_date', sDate);
                    recordTransaction.setFieldValue('custrecord_clgx_ccards_merchant_id', merchant_id);
                    recordTransaction.setFieldValue('custrecord_clgx_ccards_amount', customerArr.tocharge);
                    recordTransaction.setFieldValue('custrecord_clgx_ccards_response_code', resp_code);
                    recordTransaction.setFieldValue('custrecord_clgx_ccards_response_text', mess_text);
                    // 1 = Approved
                    // 2 = Declined
                    // 3 = Error
                    // 4 = Held for Review
                    var body = '';
                    body = body + 'Customer ID: ' + customerArr.customerid + '\n';
                    body = body + 'Company Name: ' + customerArr.customer + '\n';
                    body = body + 'Transaction Type: ' + TXN_TYPE_SALE + '\n';
                    body = body + 'Transaction Amount: ' + customerArr.tocharge + '\n';
                    body = body + 'Transaction Date: ' + sDate + '\n';
                    body = body + 'Result: Error\n';
                    body = body + 'Response: ' + mess_text + '\n';
                    body = body + 'Transaction ID: ' + resp.transactionResponse.transId + '\n';
                    var subject = 'Credit Card Decline Detail - ' + customerArr.customer
                    if (messagesStatus !== undefined && messagesStatus[0].code == 1) { //Success
                        recordTransaction.setFieldValue('custrecord_clgx_ccards_status', 5);
                        if (resp.transactionResponse["splitTenderPayments"] !== undefined) {
                            var splitTenderPayments = resp.transactionResponse["splitTenderPayments"];
                            recordTransaction.setFieldValue('custrecord_clgx_ccards_amountcharged', splitTenderPayments.splitTenderPayment.approvedAmount);
                        }
                        else {
                            recordTransaction.setFieldValue('custrecord_clgx_ccards_amountcharged', customerArr.tocharge);

                        }
                        recordTransaction.setFieldValue('custrecord_clgx_ccards_authorization', resp.transactionResponse.authCode);

                    }
                    else if (resp.transactionResponse.responseCode == 2) {
                        recordTransaction.setFieldValue('custrecord_clgx_ccards_status', 3);
                        var record = nlapiCreateRecord('supportcase');
                        record.setFieldValue('company', customerArr.customerid);
                        record.setFieldValue('title', subject);
                        record.setFieldValue('category', 11);
                        record.setFieldValue('custevent_cologix_sub_case_type', 54);
                        record.setFieldValue('incomingmessage', body);
                        record.setFieldValue('priority', 2);
                        record.setFieldValue('origin', 1);
                        var idCase = nlapiSubmitRecord(record, false, true);
                        recordTransaction.setFieldValue('custrecord_clgx_ccards_case_id', idCase);
                        recordTransaction.setFieldValue('custrecord_clgx_ccards_amountcharged', 0);

                        nlapiSubmitField('customer', customerArr.customerid, ['custentity_clgx_cc_paused'], 'T');


                    }
                    else if (resp.transactionResponse.responseCode == 3 || resp_code == 'Error') {
                        recordTransaction.setFieldValue('custrecord_clgx_ccards_status', 7);
                        var record = nlapiCreateRecord('supportcase');
                        record.setFieldValue('company', customerArr.customerid);
                        record.setFieldValue('title', subject);
                        record.setFieldValue('category', 11);
                        record.setFieldValue('custevent_cologix_sub_case_type', 54);
                        record.setFieldValue('incomingmessage', body);
                        record.setFieldValue('priority', 2);
                        record.setFieldValue('origin', 1);
                        var idCase = nlapiSubmitRecord(record, false, true);
                        recordTransaction.setFieldValue('custrecord_clgx_ccards_case_id', idCase);
                        recordTransaction.setFieldValue('custrecord_clgx_ccards_amountcharged', 0);

                        nlapiSubmitField('customer', customerArr.customerid, ['custentity_clgx_cc_paused'], 'T');

                    }
                    else if (messagesStatus[0].code == 4) {
                        recordTransaction.setFieldValue('custrecord_clgx_ccards_status', 2);
                    }
                    recordTransaction.setFieldValue('custrecord_clgx_ccards_transactionid', resp.transactionResponse.transId);
                    recordTransaction.setFieldValue('custrecord_clgx_ccards_avs', resp.transactionResponse.avsResultCode);
                    recordTransaction.setFieldValue('custrecord_clgx_ccards_ccv', resp.transactionResponse.cvvResultCode);
                    recordTransaction.setFieldValue('custrecord_clgx_ccards_cavv', resp.transactionResponse.cavvResultCode);
                    recordTransaction.setFieldValue('custrecord_clgx_ccards_reftransid', resp.transactionResponse.refTransID);
                    recordTransaction.setFieldValue('custrecord_clgx_ccards_accountnumber', resp.transactionResponse.accountNumber);
                    recordTransaction.setFieldValue('custrecord_clgx_ccards_accounttype', resp.transactionResponse.accountType);
                    if (resp.transactionResponse["splitTenderPayments"] !== undefined) {
                        var splitTenderPayments = resp.transactionResponse["splitTenderPayments"];
                        recordTransaction.setFieldValue('custrecord_clgx_ccards_splittenderpaymen', 'Yes');
                        recordTransaction.setFieldValue('custrecord_clgx_ccards_requestedamount', splitTenderPayments.splitTenderPayment.requestedAmount);
                        recordTransaction.setFieldValue('custrecord_clgx_ccards_approvedamount', splitTenderPayments.splitTenderPayment.approvedAmount);
                        recordTransaction.setFieldValue('custrecord_clgx_ccards_balanceoncard', splitTenderPayments.splitTenderPayment.balanceOnCard);
                        var amount = parseFloat(splitTenderPayments.splitTenderPayment.approvedAmount).toFixed(2);
                    }
                    else {
                        recordTransaction.setFieldValue('custrecord_clgx_ccards_splittenderpaymen', 'No');
                    }

                    var id = nlapiSubmitRecord(recordTransaction, false, true);
                    if (messagesStatus !== undefined && messagesStatus[0].code == 1) {
                        var typeCard=resp.transactionResponse.accountType.charAt(0);
                        nlapiSubmitField('customrecord_clgx_ccards_processing', id, ['custrecord_clgx_ccards_payment_id'], createPayment(currency, amount, resp.transactionResponse.transId, customerArr.subsidiaryid, id, customerArr.customerid,typeCard));

                    }
                }
                return;


            }
        }


    } catch (error) {
        if (error.getDetails != undefined) {
            nlapiLogExecution('ERROR', 'Process Error CC_ProcessQueue', error.getCode() + ': ' + error.getDetails());
            throw error;
        } else {
            nlapiLogExecution('ERROR', 'Unexpected Error CC_ProcessQueue', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }

}

function createPayment(currency,amount,transId,subsidiary,id,customer,typeCard){
    try {
        nlapiLogExecution('DEBUG', 'Start_CC_ProcessReturn - [' + id + ']');
        // Create Payment Record
        var arrC = new Array();
        var arrF = new Array();
        arrF.push(new nlobjSearchFilter("name", null, "anyof",customer ));

        var searchLocations = nlapiSearchRecord('transaction','customsearch_clgx_ss_ccards_in', arrF, arrC);
	    var searchProForma = nlapiSearchRecord('transaction','customsearch_clgx_proforma_invoices', arrF, arrC);
        var canAutoApply = "T";
        if (searchProForma != null)
        	canAutoApply = "F";
        
        var searchLoc = searchLocations[0];
        var columnsC = searchLoc.getAllColumns();
        var location = searchLoc.getValue(columnsC[0]);
        var record = nlapiCreateRecord('customerpayment');
        record.setFieldValue('autoapply', 'F');
        record.setFieldValue('currency', currency);
        record.setFieldValue('exchangerate', 1);
        record.setFieldValue('customer', customer);
        record.setFieldValue('payment', amount);
        record.setFieldValue('trandate', moment().format('MM/DD/YYYY'));
        record.setFieldValue('tranid', "cc auth "+typeCard+' '+transId);
        record.setFieldValue('location', location);
        record.setFieldValue('custbody_clgx_credit_card_tr',id);
        record.setFieldValue('memo','Credit card authorization #'+ transId+' received from Authorized.net.')
        record.setFieldValue('aracct', 122);
        record.setFieldValue('subsidiary', subsidiary);
        record.setFieldValue('undepfunds', 'T');
        record.setFieldValue('custbody_clgx_credit_card_trans', id);

        var newid = nlapiSubmitRecord(record, false, true);
        nlapiLogExecution('DEBUG', 'CC_ProcessReturn - [' + newid + ']');

        // Update Customer Balance- It's done automatically when a Payment is created
        //  var recCompany = nlapiLoadRecord('customer', customer);
        //   var amountB=recCompany.getFieldValue('custentity_clgx_customer_balance');
        //   var balance=parseFloat(amountB)-parseFloat(amount);
        //  recCompany.setFieldValue('custentity_clgx_customer_balance',balance);
        // nlapiSubmitRecord(recCompany, false, true);
        // var arrParam=new Array();
        // arrParam['custscript_customerid'] = customer;
        //var status = nlapiScheduleScript('customscript_clgx_ss_update_balances', null ,arrParam);
        var balance = clgx_update_balances (customer);
        var customerBalance = parseFloat(nlapiLookupField('customer', customer, 'custentity_clgx_customer_balance') || 0);
        if(customerBalance>0){
            nlapiSubmitField('customer', customer, ['custentity_clgx_cc_paused'], 'T');
        }
        else if(customerBalance==0){

            nlapiSubmitField('customer', customer, ['custentity_clgx_cc_paused'], 'F');

        }
	    nlapiSubmitField('customerpayment', newid, 'autoapply',  canAutoApply);
        return newid;

    } catch (error) { // Start Catch Errors Section
        if (error.getDetails != undefined) {
            nlapiLogExecution('ERROR', 'Process Error CC_ProcessReturn', error.getCode() + ': ' + error.getDetails());
            throw error;
        } else {
            nlapiLogExecution('ERROR', 'Unexpected Error CC_ProcessReturn', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
        return false;
    }
}