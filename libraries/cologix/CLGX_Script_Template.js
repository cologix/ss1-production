//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_SO_Renewals_Opportunity.js
//	Script Name:	CLGX_SL_SO_Renewals_Opportunity
//	Script Id:		customscript_clgx_sl_so_renewals_oppty
//	Script Nbr:		259
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		dan.tansanu@cologix.com
//	Created:		01/23/2014
//	DebuggerURL:	https://debugger.netsuite.com/app/site/hosting/scriptlet.nl?script=259&deploy=1
//	SystemURL:		https://system.na2.netsuite.com/app/site/hosting/scriptlet.nl?script=259&deploy=1
//	SandboxURL:		https://sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=259&deploy=1
//	SBDebuggerURL:	https://debugger.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=259&deploy=1
//-------------------------------------------------------------------------------------------------
function suitelet_so_renewals_oppty (request, response){
	try {


		
		
		
		
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
