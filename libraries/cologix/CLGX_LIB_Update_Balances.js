//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_LIB_Update_Balances.js
//	Script Name:	CLGX_LIB_Update_Balances.js
//	Script Id:		none
//	Script Runs:	Included On Transactions Scripts
//	Script Type:	Library
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		02/19/2016
//-------------------------------------------------------------------------------------------------

function clgx_update_balances (customerid){

	var balance = 0;
	var unapplied = 0;
	var sumremaining = 0;
	
// Get IDs of all consolidate invoices with open invoices ====================================================================================
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
	var searchOpenCInvoices = nlapiSearchRecord('transaction', 'customsearch_clgx_inv_balance_open', arrFilters, arrColumns);
	
	if(searchOpenCInvoices){
		
		var arrOpenCInvoicesIDs = new Array();
		for ( var i = 0; searchOpenCInvoices != null && i < searchOpenCInvoices.length; i++ ) {
			arrOpenCInvoicesIDs.push(parseInt(searchOpenCInvoices[i].getValue('custbody_consolidate_inv_cinvoice',null,'GROUP')));
		}
		var arrOpenCInvoicesIDs = _.compact(_.uniq(arrOpenCInvoicesIDs));
	
// Get Sums of all consolidate invoices with open invoices ====================================================================================
		
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
		arrFilters.push(new nlobjSearchFilter("custbody_consolidate_inv_cinvoice",null,"anyof",arrOpenCInvoicesIDs));
		var searchCInvoices = nlapiSearchRecord('transaction', 'customsearch_clgx_inv_balance_cinvoice', arrFilters, arrColumns);
		var arrCInvoices = new Array();
		for ( var i = 0; searchCInvoices != null && i < searchCInvoices.length; i++ ) {
			var cinvoice = parseInt(searchCInvoices[i].getValue('custbody_consolidate_inv_cinvoice',null,'GROUP'));
			var remaining = parseFloat(searchCInvoices[i].getValue('fxamountremaining',null,'SUM'));
			sumremaining += remaining;
			nlapiSubmitField('customrecord_clgx_consolidated_invoices', cinvoice, 'custrecord_clgx_consol_inv_balance', remaining);
		}
	}

// Get Sums of unapplied payments and credit memos  ====================================================================================
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
	var searchUnapplied = nlapiSearchRecord('transaction', 'customsearch_clgx_inv_balance_unapplied', arrFilters, arrColumns);
	if(searchUnapplied){
		var fxamountremaining = searchUnapplied[0].getValue('fxamountremaining',null,'SUM');
		if(fxamountremaining !== '' && fxamountremaining !== null){
			unapplied = parseFloat(fxamountremaining);
		}
	}
	
	var balance = sumremaining - unapplied;
	nlapiSubmitField('customer', customerid, 'custentity_clgx_customer_balance', balance);

	return balance;
}
