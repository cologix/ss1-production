//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_LIB_Transaction_Totals.js
//	Script Name:	CLGX_LIB_Transaction_Totals.js
//	Script Id:		none
//	Script Runs:	Included On Transactions Scripts
//	Script Type:	Library
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/14/2014
//-------------------------------------------------------------------------------------------------


// manage totals by location/class pair for transaction and opportunity records


function clgx_transaction_totals (customer, rectype, transaction){

    if(rectype == 'oppty'){
        var rec = nlapiLoadRecord('opportunity', transaction);
    }
    else if(rectype == 'proposal'){
        var rec = nlapiLoadRecord('estimate', transaction);
    }
    else{
        var rec = nlapiLoadRecord('salesorder', transaction);
    }


//extract locations totals from transaction items -----------------------------------------------------------------------

    var nbrItems = rec.getLineItemCount('item');
    var arrItems = new Array();
    for ( var i = 0; i < nbrItems; i++ ) {
        var objItem = new Object();
        var rate =  rec.getLineItemValue('item', 'rate', i + 1);
        if(rectype == 'oppty' || rectype == 'proposal'){
            var quantity =  rec.getLineItemValue('item', 'quantity', i + 1);
        }
        else{
            var quantity =  rec.getLineItemValue('item', 'custcol_clgx_qty2print', i + 1);
        }
        objItem["itemid"] = parseInt(rec.getLineItemValue('item', 'item', i + 1));
        objItem["location"] = parseInt(rec.getLineItemValue('item', 'location', i + 1));
        objItem["classtype"] = rec.getLineItemText('item', 'class', i + 1);
        objItem["class"] = parseInt(rec.getLineItemValue('item', 'class', i + 1));
        objItem["rate"] = parseFloat(rate);
        if(quantity != null){
            objItem["quantity"] = parseFloat(quantity);
            objItem["amount"] = parseFloat(rate) * parseFloat(quantity);
        }
        else{
            objItem["quantity"] = 1;
            objItem["amount"] = parseFloat(rate);
        }
        arrItems.push(objItem);
    }
    var arrLocationsIDs = _.sortBy(_.uniq(_.map(arrItems, 'location')), function(num){ return num; });

    var arrLocations = new Array();
    for ( var i = 0; arrLocationsIDs != null && i < arrLocationsIDs.length; i++ ) {

        var objLocation = new Object();
        objLocation["location"] = arrLocationsIDs[i];

        var arrLocationMRCItems = _.filter(arrItems, function(arr){
            return (arr.location == arrLocationsIDs[i] && (arr.classtype).indexOf("Recurring") > -1);
        });
        var totalMRC = 0;
        for ( var j = 0; arrLocationMRCItems != null && j < arrLocationMRCItems.length; j++ ) {
            if(arrLocationMRCItems[j].itemid != 549){
                totalMRC += arrLocationMRCItems[j].amount;
            }
        }
        objLocation["mrc"] = totalMRC;

        var arrLocationNRCItems = _.filter(arrItems, function(arr){
            return (arr.location == arrLocationsIDs[i] && (arr.classtype).indexOf("NRC") > -1);
        });
        var totalNRC = 0;
        for ( var j = 0; arrLocationNRCItems != null && j < arrLocationNRCItems.length; j++ ) {
            totalNRC += arrLocationNRCItems[j].amount;
        }
        objLocation["nrc"] = totalNRC;

        arrLocations.push(objLocation);
    }

//extract locations totals from custom record -----------------------------------------------------------------------


    if(rectype == 'oppty'){

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_oppty_location',null,null).setSort(false));
       // arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_oppty_class',null,null).setSort(false));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_opportunity",null,"anyof",transaction));
        //arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_oppty_class",null,"anyof",1));
        var searchTotals = nlapiSearchRecord('customrecord_clgx_totals_opportunity', null, arrFilters, arrColumns);

        var arrTotals = new Array();
        for ( var i = 0; searchTotals != null && i < searchTotals.length; i++ ) {
            var searchTotal = searchTotals[i];
            var internalid = parseInt(searchTotal.getValue('internalid',null,null));
            var location = parseInt(searchTotal.getValue('custrecord_clgx_totals_oppty_location',null,null));
            //var classtype = parseInt(searchTotal.getValue('custrecord_clgx_totals_oppty_class',null,null));
            var objLocation = new Object();
            objLocation["id"] = internalid;
            objLocation["location"] = location;
            //objLocation["classtype"] = classtype;
            arrTotals.push(objLocation);
        }
    }
    else{

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_location',null,null));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_transaction",null,"anyof",transaction));
        //arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_oppty_class",null,"anyof",1));
        var searchTotals = nlapiSearchRecord('customrecord_clgx_totals_transaction', null, arrFilters, arrColumns);

        var arrTotals = new Array();
        for ( var i = 0; searchTotals != null && i < searchTotals.length; i++ ) {
            var searchTotal = searchTotals[i];
            var internalid = parseInt(searchTotal.getValue('internalid',null,null));
            var location = parseInt(searchTotal.getValue('custrecord_clgx_totals_location',null,null));
            //var classtype = parseInt(searchTotal.getValue('custrecord_clgx_totals_class',null,null));
            var objLocation = new Object();

            objLocation["id"] = internalid;
            objLocation["location"] = location;
            //objLocation["classtype"] = classtype;
            arrTotals.push(objLocation);
        }

    }

//compare arrays, update, add or delete locations totals -----------------------------------------------------------------------

    for ( var i = 0; arrLocations != null && i < arrLocations.length; i++ ) {

        var objTotalLocationMRC= _.find(arrTotals, function(arr){ return (arr.location == arrLocations[i].location)});
        //var objTotalLocationNRC= _.find(arrTotals, function(arr){ return (arr.location == arrLocations[i].location && arr.classtype == 2)});

        if(objTotalLocationMRC != null){ // if recurring total exist for this location, then update it
            if(rectype == 'oppty'){
                var fields = ['custrecord_clgx_totals_oppty_total','custrecord_clgx_totals_oppty_total_nrc'];
                var values = [parseFloat(arrLocations[i].mrc).toFixed(2),parseFloat(arrLocations[i].nrc).toFixed(2)];
                nlapiSubmitField('customrecord_clgx_totals_opportunity', objTotalLocationMRC.id, fields, values);

            }
            else{
                var fields = ['custrecord_clgx_totals_total','custrecord_clgx_totals_total_nrc'];
                var values = [parseFloat(arrLocations[i].mrc).toFixed(2),parseFloat(arrLocations[i].nrc).toFixed(2)];
                nlapiSubmitField('customrecord_clgx_totals_transaction', objTotalLocationMRC.id, fields, values);

            }
        }
        else{ // if recurring total does not exist for this location, then create it
            if(rectype == 'oppty'){
                var rec = nlapiCreateRecord('customrecord_clgx_totals_opportunity');
                rec.setFieldValue('custrecord_clgx_totals_oppty_customer', customer);
                rec.setFieldValue('custrecord_clgx_totals_opportunity', transaction);
                rec.setFieldValue('custrecord_clgx_totals_oppty_location', arrLocations[i].location);
                //rec.setFieldValue('custrecord_clgx_totals_oppty_class', 1);
                rec.setFieldValue('custrecord_clgx_totals_oppty_total', parseFloat(arrLocations[i].mrc).toFixed(2));
                rec.setFieldValue('custrecord_clgx_totals_oppty_total_nrc', parseFloat(arrLocations[i].nrc).toFixed(2));
                var recID = nlapiSubmitRecord(rec, false, true);
            }
            else{
                var rec = nlapiCreateRecord('customrecord_clgx_totals_transaction');
                rec.setFieldValue('custrecord_clgx_totals_customer', customer);
                rec.setFieldValue('custrecord_clgx_totals_transaction', transaction);
                rec.setFieldValue('custrecord_clgx_totals_location', arrLocations[i].location);
                //rec.setFieldValue('custrecord_clgx_totals_class', 1);
                rec.setFieldValue('custrecord_clgx_totals_total', parseFloat(arrLocations[i].mrc).toFixed(2));
                rec.setFieldValue('custrecord_clgx_totals_total_nrc', parseFloat(arrLocations[i].nrc).toFixed(2));
                var recID = nlapiSubmitRecord(rec, false, true);
            }
        }
    }

    // loop totals array and delete locations totals records that do not exist anymore on transaction
    for ( var i = 0; arrTotals != null && i < arrTotals.length; i++ ) {

        var objTotalLocation = _.find(arrLocations, function(arr){ return (arr.location == arrTotals[i].location)});

        if(objTotalLocation == null){
            if(rectype == 'oppty'){
                nlapiDeleteRecord('customrecord_clgx_totals_opportunity', arrTotals[i].id);
            }
            else{
                nlapiDeleteRecord('customrecord_clgx_totals_transaction', arrTotals[i].id);
            }
        }
    }
}


