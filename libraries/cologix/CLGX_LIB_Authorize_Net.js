//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_LIB_Authorize_Net.js
//	Script Name:	CLGX_LIB_Authorize_Net
//	Script Id:		none
//	Script Runs:	Included On Each Other Script
//	Script Type:	Library
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		02/01/2017
//-------------------------------------------------------------------------------------------------

function create_customer_profile (gateid, customerid){

	var cred = get_credentials (gateid);
	
	var rec = nlapiLoadRecord('customer',customerid);
    var customer = rec.getFieldValue('entityid');
    
    var data = {
        "createCustomerProfileRequest": {
            "merchantAuthentication": {
                "name": cred.name,
                "transactionKey": cred.key
            },
            "profile": {
                "merchantCustomerId": customerid,
                "description": customer
            }
        }
    };
    var resp = autorize_net_request (data);
    return resp;
}

function create_customer_and_pay_profiles (gateid, customerid, card, expire, type, mode){

	var cred = get_credentials (gateid);
	
	var rec = nlapiLoadRecord('customer',customerid);
    var customer = rec.getFieldValue('entityid');
    
	var data = {
		    "createCustomerProfileRequest": {
		        "merchantAuthentication": {
	                "name": cred.name,
	                "transactionKey": cred.key
		        },
		        "profile": {
		            "merchantCustomerId": customerid,
		            "description": customer,
		            "paymentProfiles": {
		                "customerType": type,
		                "payment": {
		                    "creditCard": {
		                        "cardNumber": card,
		                        "expirationDate": expire
		                    }
		                }
		            }
		        },
		        "validationMode": mode
		    }
		};
	
	var resp = autorize_net_request (data);
	return resp;
}

function create_pay_profile (gateid, authcustid, card, type, dflt){
	
	var cred = get_credentials (gateid);
	
	var data = {
	  "createCustomerPaymentProfileRequest": {
	    "merchantAuthentication": {
            "name": cred.name,
            "transactionKey": cred.key
	    },
	    "customerProfileId": authcustid,
	    "paymentProfile": {
	    	"customerType": type,
            "billTo": {
		        "firstName": card.first,
		        "lastName": card.last,
		        "address": card.address,
		        "city": card.city,
		        "state": card.state,
		        "zip": card.zip,
		        "country": card.country
            },
            "payment": {
            	"creditCard": {
            		"cardNumber": card.number,
            		"expirationDate": card.expire
            	}
            },
            "defaultPaymentProfile": dflt
	    },
	  }
	};
	var resp = autorize_net_request (data);
	return resp;
}


function charge_pay_profile (gateid, authcustid, authpayid, invoiceid, amount){
	
	var cred = get_credentials (gateid);
	
	var data = {
	    "createTransactionRequest": {
	        "merchantAuthentication": {
                "name": cred.name,
                "transactionKey": cred.key
	        },
	        "refId": invoiceid,
	        "transactionRequest": {
	            "transactionType": "authCaptureTransaction",
	            "amount": amount,
	              "profile": {
	    		  	"customerProfileId": authcustid,
	    		  	"paymentProfile": { "paymentProfileId": authpayid }
	  			},
	        }
	    }
	};
	var resp = autorize_net_request (data);
	return resp;
}

function get_credentials (gateid){
	var rec = nlapiLoadRecord('customrecord_clgx_authorize_net', gateid);
	return {
			"name":rec.getFieldValue('custrecord_clgx_auth_net_api_id'),
			"key":rec.getFieldValue('custrecord_clgx_auth_net_transaction_key')
	};
}

function autorize_net_request (data){
	
	var req = nlapiRequestURL('https://apitest.authorize.net/xml/v1/request.api',JSON.stringify(data),{'Content-type': 'application/json'},null,'POST');
	var str = req.body;
	var resp = JSON.parse(str.slice(1));
	return resp;
}
