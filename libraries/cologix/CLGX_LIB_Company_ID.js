
// get company id depending of the environment
function clgx_get_compid (){
	
	var context = nlapiGetContext();
    var environment = context.getEnvironment();
    
    var compid = '1337135'
    if(environment == 'SANDBOX'){
    		compid += '_sb';
    }
    else if(environment == 'BETA'){
		compid += '_rp';
    }
    else {}
    return compid;
}

