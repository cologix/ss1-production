//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_LIB_Global.js
//	Script Name:	CLGX_LIB_Global
//	Script Id:		none
//	Script Runs:	Included On Each Other Script
//	Script Type:	Library
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		9/27/2013
//-------------------------------------------------------------------------------------------------

// ============================= Specific Cologix/Netsuite Functions ======================================================

function clgx_get_peak_subfacility (customerid, facilityid){
	
	var subfacilities = [];
	var columns = [];
	var filters = [];
	var search = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pwr_peaks_subfac', filters, columns);
	for ( var i = 0; search != null && i < search.length; i++ ) {
		var columns = search[i].getAllColumns();
		subfacilities.push({
			"customer_id"     : parseInt(search[i].getValue(columns[0])),
			"facility_id"     : parseInt(search[i].getValue(columns[1])),
			"subfacility_id"  : parseInt(search[i].getValue(columns[2])),
		});
	}
	
	var peaks = [];
    var columns = [];
    columns.push(new nlobjSearchColumn('internalid',null,null).setSort(true));
    columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_customer',null,null));
    columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_facility',null,null));
    var filters = [];
    var search = nlapiSearchRecord('customrecord_clgx_dcim_customer_peak', null, filters, columns);
	for ( var i = 0; search != null && i < search.length; i++ ) {
		var columns = search[i].getAllColumns();
		
		var customer_id = parseInt(search[i].getValue(columns[1]));
		var facility_id = parseInt(search[i].getValue(columns[2]));
		var subfacility_id = 0;
		var obj = _.find(subfacilities, function(arr){ return (arr.customer_id == customer_id && arr.facility_id == facility_id) ; });
		if(obj){
			subfacility_id = obj.subfacility_id;
		}
		
		peaks.push({
			"peak_id"         : parseInt(search[i].getValue(columns[0])),
			"customer_id"     : customer_id,
			"facility_id"     : facility_id,
			"location_id"     : parseInt(clgx_return_locationid (search[i].getValue(columns[2]))),
			"subfacility_id"  : subfacility_id
		});
    }
	
	var subfacilityid = null;
	var obj = _.find(peaks, function(arr){ return (arr.customer_id == customerid && arr.facility_id == facilityid) ; });
	if(obj){
		subfacilityid = obj.subfacility_id;
	}

}

function clgx_return_consolidate_location (locationid) {
	var consolidateLocation = 0;
	
	nlapiLogExecution("DEBUG", "locationid", JSON.stringify(locationid));
	
	//DAL1, DAL2, DAL3
	if(locationid == 2 || locationid == 17 || locationid == 73) {
		consolidateLocation = 18; //DAL
	} else if(locationid == 34 || locationid == 39) { //COL1&2, COL3
		consolidateLocation = 26; //COL
	} else if(locationid == 31) { //JAX1
		consolidateLocation = 25; //JAX1
	} else if(locationid == 40) { //JAX2
		consolidateLocation = 27; //JAX2
	} else if(locationid == 42) { //LAK1
		consolidateLocation = 28; //LAK1
	} else if(locationid == 16 || locationid == 35) { //MIN1&2, MIN3
		consolidateLocation = 22; //MIN
	} else if(locationid == 5 || locationid == 8 || locationid == 9 || locationid == 10 
			|| locationid == 11 || locationid == 12 || locationid == 27 || locationid == 74 
			|| locationid == 77 || locationid == 78 || locationid == 79) { //MTL1-11
		consolidateLocation = 24; //MTL
	} else if(locationid == 53 || locationid == 54 || locationid == 55 || locationid == 56) { //NNJ1-4
		consolidateLocation = 29; //NNJ
	} else if(locationid == 6 || locationid == 15 || locationid == 71) { //TOR1-3
		consolidateLocation = 21; //TOR
	} else if(locationid == 7 || locationid == 28 || locationid == 60) {
		consolidateLocation = 20; //VAN
	} else {
		consolidateLocation = 18;
	}
	
	return consolidateLocation;
}

//return consolidate location
/*function clgx_return_consolidate_location (locationid){

    var consolocation = 18;
    switch(parseInt(locationid)) {
        case 2: // Dallas Infomart
            consolocation = 18; // DAL
            break;
        case 5: // MTL1
            consolocation = 24; // MTL
            break;
        case 6: // 151 Front Street West
            consolocation = 21; // TOR
            break;
        case 7: // Harbour Centre
            consolocation = 20; // VAN
            break;
        case 8: // MTL2
            consolocation = 24; // MTL
            break;
        case 9: // MTL3
            consolocation = 24; // MTL
            break;
        case 10: // MTL4
            consolocation = 24; // MTL
            break;
        case 11: // MTL5
            consolocation = 24; // MTL
            break;
        case 12: // MTL6
            consolocation = 24; // MTL
            break;
        case 13: // 156 Front Street West
            consolocation = 21; // TOR
            break;
        case 14: // Barrie Office
            consolocation = 21; // TOR
            break;
        case 15: // 905 King Street West
            consolocation = 21; // TOR
            break;
        case 16: // MIN1&2
            consolocation = 22; // MIN
            break;
        case 17: // Dallas Infomart - Ste 2010
            consolocation = 18; // DAL
            break;
        case 18: // 151 Front Street West - Ste 822
            consolocation = 23; // 151 Front Street West - Ste 822
            break;
        case 27: // MTL7
            consolocation = 24; // MTL
            break;
        case 28: // 1050 Pender
            consolocation = 20; // VAN
            break;
        case 29: // JAX1
            consolocation = 25; // JAX1
            break;
        case 31: // JAX1
            consolocation = 25; // JAX1
            break;
        case 33: // Columbus
            consolocation = 26; // COL
            break;
        case 34: // COL1&2
            consolocation = 26; // COL
            break;
        case 35: // MIN3
            consolocation = 22; // MIN
            break;
        case 39: // COL3
            consolocation = 26; // COL
            break;
        case 40: // JAX2
            consolocation = 27; // JAX2
            break;
        case 42: // LAK1
            consolocation = 28; // Jacksonville
            break;
        case 52: // NNJ
            consolocation = 29; // New Jersey
            break;
        case 53: // NNJ1
            consolocation = 29; // New Jersey
            break;
        case 54: // NNJ2
            consolocation = 29; // New Jersey
            break;
        case 55: // NNJ3
            consolocation = 29; // New Jersey
            break;
        case 56: // NNJ4
            consolocation = 29; // New Jersey
            break;

        default:
            consolocation = 18;
    }
    return consolocation;
}*/

// return the internalid of the location that has this facility linked to it
function clgx_return_locationid (facilityid){
    var locationid =  nlapiLookupField('customrecord_cologix_facility', facilityid, 'custrecord_clgx_facility_location');
    if(locationid != null && locationid != ''){
        return locationid;
    }
    else{
        return 0;
    }
}

//return the internalid of the facility that has this location linked to it
function clgx_return_facilityid (locationid){
    // initialize facilityid to 0 in case none is found
    var facilityid = 0;
    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('custrecord_clgx_facility_location',null,'anyof', locationid));
    arrFilters.push(new nlobjSearchFilter('isinactive',null,'is', 'F'));
    var searchResults = nlapiSearchRecord('customrecord_cologix_facility', null, arrFilters, arrColumns);
    // it should be just one result, so return the first value from the search
    if(searchResults != null){
        facilityid = searchResults[0].getValue('internalid',null,null);
    }
    return facilityid;
}

//return all child locations of a parent location/market id
function clgx_return_child_locations_of_marketid (marketid) {

    var arrLocations = new Array();
    if(marketid == 40){ // Jacksonville2 exception - no reporting location
        arrLocations.push(40);
    }
    else{
        // load all locations, loop them and look for that market children
        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        var arrFilters = new Array();
        var searchResults = nlapiSearchRecord('location', null, arrFilters, arrColumns);


        for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
            var searchResult = searchResults[i];
            var locationid = searchResult.getValue('internalid', null, null);
            var locationRec = nlapiLoadRecord('location',locationid);

            if (marketid == locationRec.getFieldValue('parent')){
                arrLocations.push(locationid);
            }
        }
    }
    return arrLocations;
}

//return all child locations of a parent location/market name
function clgx_return_child_locations_of_marketname (market) {

    if(market != 'All'){
        // find first the internalid of the market
        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter('name',null,'is', market));
        var searchLocation = nlapiSearchRecord('location', null, arrFilters, arrColumns);

        if(searchLocation != null){
            marketid = searchLocation[0].getValue('internalid',null,null);
        }
        else{
            var marketid = 0;
        }
    }

    // load all locations, loop them and look for that market children
    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    var arrFilters = new Array();
    var searchResults = nlapiSearchRecord('location', null, arrFilters, arrColumns);

    var arrLocations = new Array();
    for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
        var searchResult = searchResults[i];
        var locationid = searchResult.getValue('internalid', null, null);
        var locationRec = nlapiLoadRecord('location',locationid);
        if(market != 'All'){
            if (marketid == locationRec.getFieldValue('parent')){
                arrLocations.push(locationid);
            }
        }
        else{
            arrLocations.push(locationid);
        }
    }
    return arrLocations;
}

//return market name from marketid
function clgx_return_market_name (marketid){
    if(marketid > 0){
        var market =  nlapiLookupField('location', marketid, 'name');
        if(market == null || market == ''){
            market = 'All';
        }
    }
    else{
        market = 'All';
    }
    return market;
}

//return arrDepartments from regionid
function clgx_get_region_departments(departmentid){
    switch(parseInt(departmentid)) {
        case 9:
            return [9, 18,23,27,28,29, 24,30,31, 25,32,33,34, 26,35,36,37,38];
            break;
        case 23:
            return [23,27,28,29];
            break;
        case 24:
            return [24,30,31,32];
            break;
        case 25:
            return [25,33,34,38];
            break;
        case 26:
            return [26,35,36,37];
            break;
        default:
            return [0];
    }
}

//return region name from id
function clgx_return_region_name(departmentid){
    switch(departmentid) {

        case "9":
            return 'All';
            break;

        case "23":
            return 'Canada';
            break;
        case "27":
            return 'Canada';
            break;
        case "28":
            return 'Canada';
            break;
        case "29":
            return 'Canada';
            break;

        case "24":
            return 'Cross Market';
            break;
        case "30":
            return 'Cross Market';
            break;
        case "31":
            return 'Cross Market';
            break;
        case "32":
            return 'Cross Market';
            break;

        case "25":
            return 'US North';
            break;
        case "33":
            return 'US North';
            break;
        case "34":
            return 'US North';
            break;
        case "38":
            return 'US North';
            break;

        case "26":
            return 'US South';
            break;
        case "35":
            return 'US South';
            break;
        case "36":
            return 'US South';
            break;
        case "37":
            return 'US South';
            break;

        default:
            return '';
    }
}

//return region id from name
function clgx_return_departments_ids(department){
    switch(department) {

        case 'All Sales':
            return [9,23,27,28,29,24,30,31,25,32,33,34,26,35,36,37];
            break;
        case 'All':
            return [9,23,27,28,29,24,30,31,25,32,33,34,26,35,36,37];
            break;

        case 'Canada':
            return [23,27,28,29];
            break;
        case 'Montreal':
            return [27];
            break;
        case 'Toronto':
            return [28];
            break;
        case 'Vancouver':
            return [29];
            break;

        case 'Cross Market':
            return [24,30,31,32];
            break;
        case 'Account Management':
            return [30];
            break;
        case 'Carrier':
            return [31];
            break;
        case 'Channel':
            return [32];
            break;

        case 'US North':
            return [25,33,34,38];
            break;
        case 'Columbus':
            return [33];
            break;
        case 'Minneapolis':
            return [34];
            break;
        case 'New Jersey':
            return [38];
            break;

        case 'US South':
            return [26,35,36,37];
            break;
        case 'Dallas':
            return [35];
            break;
        case 'Jacksonville':
            return [36];
            break;
        case 'Lakeland':
            return [37];
            break;

        default:
            return [0];
    }
}

//return marketid from market name
function clgx_get_marketid(market){
    switch(market) {
        case 'New Jersey':
            return 52;
            break;
        case 'Jacksonville':
            return 29;
            break;
        case 'Jacksonville2':
            return 43;
            break;
        case 'Dallas':
            return 20;
            break;
        case 'Minneapolis':
            return 22;
            break;
        case 'Montreal':
            return 25;
            break;
        case 'Toronto':
            return 23;
            break;
        case 'Columbus':
            return 33;
            break;
        case 'Cologix HQ':
            return 1;
            break;
        default:
            return 0;
    }
}


// return disconect days value
function clgx_get_disco_days (discoTerms){
    // 30 Days	 	1
    // 60 Days	 	2
    // 90 Days	 	3
    // 120 Days	 	4
    // See MSA	 	5 - 90 Days
    var discoDays = 0;
    switch(discoTerms) {
        case '30 Days':
            discoDays = 30;
            break;
        case '60 Days':
            discoDays = 60;
            break;
        case '90 Days':
            discoDays = 90;
            break;
        case '120 Days':
            discoDays = 120;
            break;
        case 'See MSA':
            discoDays = 90;
            break;
        default:
            discoDays = 0;
    }
    return discoDays;
}

// return item category id
function clgx_get_item_categoryid (category) {
    switch(category) {
        case 'XC':
            return 4;
            break;
        case 'Network':
            return 5;
            break;
        case 'Power':
            return 8;
            break;
        case 'Space':
            return 10;
            break;
        case 'VXC':
            return 11;
            break;
        default:
            return 4;
    }
}

function get_contract_fileFolder(){

    var fileFolder = 2681;

    var d = new Date();
    var m = parseInt(d.getMonth());
    var y = parseInt(d.getFullYear());

    var arr2017 = [3820995,3820996,3821097,3821098,3821099,3821100,3821101,3821102,3821103,3821104,3821105,3821106];
    
    fileFolder = arr2017[m];
    
    /*
    //var arr2016 = [2664240,2664242,2664243,2664244,2664245,2664246,2664247,2664248,2664249,2664250,2664251,2664252];
    //var arr2015 = [1594724,1594725,1594726,1594727,1594728,1594729,1594730,1594731,1594732,1594733,1594734,1594735];
    
    if(y == 2017){
        fileFolder = arr2017[m];
    }
    if(y == 2016){
        fileFolder = arr2016[m];
    } else {
        fileFolder = arr2015[m];
    }
	*/
    
    return fileFolder;
}

function get_contract_FrenchItemCategory (categoryID){
    var frenchCategory = '';
    switch(categoryID) {
        case '1':
            frenchCategory = 'Location d\'?quipement';
            break;
        case '2':
            frenchCategory = 'Vente d\'?quipement';
            break;
        case '3':
            frenchCategory = 'Service d\'installation';
            break;
        case '4':
            frenchCategory = 'Interconnexion';
            break;
        case '5':
            frenchCategory = 'R?seau';
            break;
        case '6':
            frenchCategory = 'Autre non R?curent';
            break;
        case '7':
            frenchCategory = 'Autre R?curent';
            break;
        case '8':
            frenchCategory = '?lectricit?';
            break;
        case '9':
            frenchCategory = 'Support Technique';
            break;
        case '10':
            frenchCategory = 'Espace';
            break;
        case '11':
            frenchCategory = 'Interconnexion virtuelle';
            break;
        default:
            frenchCategory = '';
    }
    return frenchCategory;
}


/*function clgx_send_employee_emails_from_savedsearch(ssid, subject, body) {
	if(ssid != null || ssid != "") {
		try {
			var list       = clgx_get_employee_list_from_savedsearch(ssid);
            var listLength = list.length; 
            
            for(var e = 0; e < listLength; e++) {
            	nlapiSendEmail(432742, list[e], subject, body, null, null, null, null, true);
            }
		} catch(ex) {
			nlapiLogExecution("ERROR", "clgx_send_employee_emails_from_savedsearch", ex);
		}
	}
}


function clgx_get_employee_list_from_savedsearch(ssid) {
	if(ssid != null && ssid != "") {
		var finalArray   = new Array();
		var columns      = new Array();
		columns.push(new nlobjSearchColumn("email", null, null));
		
		var search       = nlapiSearchRecord("employee", ssid, null, columns);
		var searchLength = search.length;
		
		for(var s = 0; s < searchLength; s++) {
			finalArray.push(search[s].getValue("email"));
		}
		
		return finalArray;
	}
	
	return [];
}*/


function clgx_send_employee_emails_from_savedsearch(ssid, subject, body) {
	if(ssid != null || ssid != "") {
		try {
			var list       = clgx_get_employee_list_from_savedsearch(ssid);
            var listLength = list.length; 
            
            for(var e = 0; e < listLength; e++) {
            	nlapiSendEmail(432742, list[e].email, subject, body, null, null, null, null, true);
            }
		} catch(ex) {
			nlapiLogExecution("ERROR", "clgx_send_employee_emails_from_savedsearch", ex);
		}
	}
}


function clgx_get_employee_list_from_savedsearch(ssid) {
	if(ssid != null && ssid != "") {
		var finalArray   = new Array();
		var columns      = new Array();
		
		columns.push(new nlobjSearchColumn("internalid", null, null));
		columns.push(new nlobjSearchColumn("firstname", null, null));
		columns.push(new nlobjSearchColumn("lastname", null, null));
		columns.push(new nlobjSearchColumn("email", null, null));
		
		var search       = nlapiSearchRecord("employee", ssid, null, columns);
		var searchLength = search.length;
		
		for(var s = 0; s < searchLength; s++) {
			finalArray.push({"id": search[s].getValue("internalid"), "name": (search[s].getValue("firstname") + " " + search[s].getValue("lastname")), "email": search[s].getValue("email")});
		}
		
		return finalArray;
	}
	
	return [];
}


//============================= Useful General Other Functions ======================================================

//check if a value is in the array
function clgx_is_value_in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}

//return a string of length and include special characters or not
function clgx_generate_password(length, special) {
    var iteration = 0;
    var password = "";
    var randomNumber;
    if(special == undefined){
        var special = false;
    }
    while(iteration < length){
        randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
        if(!special){
            if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
            if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
            if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
            if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
        }
        iteration++;
        password += String.fromCharCode(randomNumber);
    }
    return password;
}

//return a UUID (Universal Unique Identifier)
function clgx_generate_uuid() {
    var s = [], itoh = '0123456789ABCDEF';
    for (var i = 0; i <36; i++) s[i] = Math.floor(Math.random()*0x10);
    s[14] = 4;
    s[19] = (s[19] & 0x3) | 0x8;
    for (var i = 0; i <36; i++) s[i] = itoh[s[i]];
    s[8] = s[13] = s[18] = s[23] = '-';
    return s.join('');
}

// get a random integer between a min and a max value
function clgx_get_rand_int (min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
//============================= Useful General String Functions ======================================================

//replace a character in a string
function clgx_set_char_at(str,index,chr) {
    if(index > str.length-1) return str;
    return str.substr(0,index) + chr + str.substr(index+1);
}


function addCommas(nStr){
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function convertBR(input) {
// Converts carriage returns  to <BR> for display in HTML
    var output = "";
    for (var i = 0; i < input.length; i++) {
        if ((input.charCodeAt(i) == 13) && (input.charCodeAt(i + 1) == 10)) {
            i++;
            output += "<br/>";
        }
        else {
            output += input.charAt(i);
        }
    }
    return output;
}

function HTMLEncode(str){
    var i = str.length,
        aRet = [];

    while (i--) {
        var iC = str[i].charCodeAt();
        if (iC < 65 || iC > 127 || (iC>90 && iC<97)) {
            aRet[i] = '&#'+iC+';';
        } else {
            aRet[i] = str[i];
        }
    }
    return aRet.join('');
}

function precise_round(num,decimals){
    var sign = num >= 0 ? 1 : -1;
    return (Math.round((num*Math.pow(10,decimals))+(sign*0.001))/Math.pow(10,decimals)).toFixed(decimals);
}

function inArray(val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}

/**
 * Allows arrays to be chunked into blocks of a specified size.
 * 
 * Usage: var array = [1, 2, 3, 4].chunk(2);
 * Returns: [[1, 2], [3, 4]]
 * 
 * @param {size} Integer
 * @return Array
 */
Object.defineProperty(Array.prototype, "chunk", {
	value: function(size) {
		var temp = [];
		for(var e = 0; e < this.length; e += size) {
			temp.push(this.slice(e, e + size));
		}
		return temp;
	}
});
