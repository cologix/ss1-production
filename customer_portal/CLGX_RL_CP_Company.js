nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Company.js
//	ScriptID:	customscript_clgx_rl_cp_company
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=653&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/12/2016
//------------------------------------------------------

function post (datain){
    try {

        var start = moment();

        var is_json = (datain.constructor.name == 'Object');
        if (!is_json) {
            datain = JSON.parse(datain);
        }

        var cfid = datain['cfid'];
        var nsid = datain['nsid'];
        var ip = datain['ip'];
        var radix = datain['radix'];
        var mrid = datain['mrid'];
        var crid = datain['crid'];
        var erid = datain['erid'];
        var arid = datain['arid'];
        var masq = datain['masq'] || 'F';

        var obj = {};

        var objErr = {};
        objErr["login"] = 'T';
        objErr["error"] = 'F';
        objErr["code"] = '';
        objErr["msg"] = '';
        objErr["fail"] = 0;
        obj["login"] = objErr;

        if(cfid && nsid && ip && radix && mrid && crid && erid && arid){



            var modifierid = parseInt(mrid, radix);
            var contactid = parseInt(crid, radix);
            var companyid = parseInt(erid, radix);
            nlapiLogExecution('DEBUG','companyid', companyid);

            var askid = parseInt(arid, radix);

            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('internalid',null,null));
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_api_cust_portal_entity',null,null));
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_api_cust_portal_entities',null,null));
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_api_cust_portal_sadmin',null,null));
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_api_cust_portal_language',null,null));
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_modifier",null,"anyof",modifierid));
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_contact",null,"anyof",contactid));
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_entity",null,"anyof",companyid));
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_cfid",null,"is",cfid));
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_uuid",null,"is",nsid));
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_ip",null,"is",ip));
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_radix",null,"equalto",radix));
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_status",null,"anyof",1));
            var session = nlapiSearchRecord('customrecord_clgx_api_cust_portal_logins', null, arrFilters, arrColumns);

            if(session){

                var sid = parseInt(session[0].getValue('internalid',null,null));
                var scompanies = JSON.parse(session[0].getValue('custrecord_clgx_api_cust_portal_entities',null,null));
                var scompany = _.find(scompanies, function(arr){ return (arr.id == askid) ; });
                var sadmin = parseInt(session[0].getValue('custrecord_clgx_api_cust_portal_sadmin',null,null)) || 0;
                var lang = parseInt(session[0].getValue('custrecord_clgx_api_cust_portal_language',null,null));

                if(scompany || sadmin){

                    var arrColumns = new Array();
                    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
                    var arrFilters = new Array();
                    arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",askid));
                    var company = nlapiSearchRecord('customer', null, arrFilters, arrColumns);

                    if(company){

                        if(!sadmin || scompany){
                            // find contact for company / modifier

                            var columns = new Array();
                            var filters = new Array();
                            filters.push(new nlobjSearchFilter('company',null,'anyof',askid));
                            filters.push(new nlobjSearchFilter('custentity_clgx_modifier',null,'anyof',modifierid));
                            var contact = nlapiSearchRecord('contact', 'customsearch_clgx_cp_contacts', filters, columns);

                            var newcontactid = parseInt(contact[0].getValue('internalid',null,null));
                            var name = contact[0].getValue('entityid',null,null);
                            var role = parseInt(contact[0].getValue('contactrole',null,null)) || 0;
                            var nacid = parseInt(contact[0].getValue('custentity_clgx_legacy_contact_id',null,null)) || 0;

                        } else {

                            var newcontactid = contactid;
                            //var name = nlapiLookupField('contact', contactid, 'entityid');
                            //var role = nlapiLookupField('contact', contactid, 'contactrole');
                            //var nacid = parseInt(nlapiLookupField('contact', contactid, 'custentity_clgx_legacy_contact_id')) || 0;
                            var fields = ['entityid','contactrole','custentity_clgx_legacy_contact_id'];
                            var columns = nlapiLookupField('contact', contactid, fields);
                            var name = columns.entityid;
                            name = name.replace(/'/g, '&#39;');
                            var role = parseInt(columns.contactrole) || 0;
                            var nacid = columns.custentity_clgx_legacy_contact_id || 0;
                        }

                        if(companyid != askid){ // if other company refresh nsid and radix

                            nsid = get_uuid();
                            radix = get_radix();
                        }

                        var recModifier = nlapiLoadRecord('customrecord_clgx_modifier', modifierid);
                        var username = recModifier.getFieldValue('name');
                        var password = recModifier.getFieldValue('custrecord_clgx_modifier_password') || '';
                        var terms = recModifier.getFieldValue('custrecord_clgx_modifier_accepted_terms') || 'F';
                        var sadmin = parseInt(recModifier.getFieldValue('custrecord_clgx_modifier_super_admin')) || 0;
                        var pdate = recModifier.getFieldValue('custrecord_clgx_modifier_password_date') || moment().subtract(3, 'months').format('M/D/YYYY');
                        var pdays = moment().diff(moment(pdate), 'days');

                        var objUser = {};
                        objUser["mid"] = modifierid;
                        var rmid = modifierid.toString(radix);
                        objUser["mrid"] = rmid;
                        objUser["id"] = newcontactid;
                        var rid = newcontactid.toString(radix);
                        objUser["rid"] = rid;
                        objUser["companyid"] = askid;
                        objUser["nac"] = nacid;
                        objUser["name"] = name;
                        objUser["uname"] = username;
                        objUser["passwd"] = password;
                        objUser["sadmin"] = sadmin;
                        objUser["role"] = role;
                        objUser["terms"] = terms;
                        objUser["pdate"] = pdate;
                        objUser["pdays"] = pdays;

                        if (lang == 17) {
                            objUser["langid"] = 17;
                            objUser["lang"] = 'fr';
                        }
                        else {
                            objUser["langid"] = 1;
                            objUser["lang"] = 'en';
                        }
                        obj["user"] = objUser;

                        obj.companies = get_companies(modifierid,radix);

                        var objNS = {};
                        objNS["ip"] = ip;
                        objNS["cfid"] = cfid;
                        objNS["nsid"] = nsid;
                        objNS["radix"] = radix;
                        obj["ns"] = objNS;
                        obj["masq"] = masq;
                        //if(sadmin==0)
                        //{
                        //  sadmin='0';
                        //}
                        obj.company = get_company(contactid,modifierid,askid,radix,sadmin);

                        var now = moment().format("M/D/YYYY h:mm:ss a");
                        if(companyid != askid){
                            var newsid = set_session(obj);
                            nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last','custrecord_clgx_api_cust_portal_status'], [now,2]);
                            nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', newsid, ['custrecord_clgx_api_cust_portal_masquara'], [masq]);
                        } else {
                            nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);
                        }

                        obj.login.login = 'T';
                        obj.login.error = 'F';
                        obj.login.code = 'SUCCESS';

                        if(lang == 17){
                            obj.login.msg = 'Bonjour ' + obj.user.name + '. Vous gérez - ' + obj.company.name;
                        } else {
                            obj.login.msg = 'Hello ' + obj.user.name + '. You\'re managing - ' + obj.company.name;
                        }


                    } else {
                        obj.login.error = 'T';
                        obj.login.code = 'NO_COMPANY';
                        obj.login.msg = 'Company does not exist';
                    }

                } else {
                    obj.login.error = 'T';
                    obj.login.code = 'NO_RIGHTS';
                    obj.login.msg = 'No rights for this company';
                }

            } else {
                obj.login.error = 'T';
                obj.login.code = 'NO_SESSION';
                obj.login.msg = 'Session request not valid';
            }

        } else {
            obj.login.error = 'T';
            obj.login.code = 'MISS_ARGS';
            obj.login.msg = 'Missing arguments';
        }

        return obj;
    }

    catch (error) {

        if (error.getDetails != undefined){
            var code = error.getCode();
            var msg = error.getDetails();
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            //throw error;
        } else {
            var code = 'ERROR';
            var msg = error.toString();
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
         //   throw nlapiCreateError('99999', error.toString());
        }
        var obj = new Object();
        obj["error"] = 'T';
        obj["code"] = code;
        obj["msg"] = msg;
        obj["me"] = {};
        return obj;
    }
}