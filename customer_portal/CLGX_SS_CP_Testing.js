nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_LIB_CP_Login.js
//	ScriptID:	customscript_clgx_lib_cp_global
//	ScriptType:	Library
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/12/2016
//------------------------------------------------------
function get_values() {
    var rights = get_company(834809,15454,533463,10,0);
    var x=0;
}

function get_company(contactid,modifierid,companyid,radix,sdamin) {


    var obj = new Object();

    obj["rights"] = get_rights(contactid,modifierid,companyid,radix,0,sdamin);
    nlapiSendEmail(206211,206211,'rightsprod1',obj.rights);
    nlapiLogExecution('DEBUG','rightsprod1', obj.rights);


    return obj;
}

function get_cases(id,radix){

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("internalid","company","anyof",id));
    var records = nlapiSearchRecord('supportcase', 'customsearch_clgx_api_cases', filters, columns);

    var arr = new Array();
    for ( var i = 0; records != null && i < records.length; i++ ) {
        var obj = new Object();
        var caseid = parseInt(records[i].getValue('internalid',null,null));
        //obj["id"] = caseid;
        obj["rid"] = caseid.toString(radix);
        obj["number"] = records[i].getValue('casenumber',null,null);
        obj["title"] = records[i].getValue('title',null,null) || '';
        //obj["statusid"] = parseInt(records[i].getValue('status',null,null)) || 0;
        obj["status"] = records[i].getText('status',null,null) || '';
        //obj["priorityid"] = parseInt(records[i].getValue('priority',null,null)) || 0;
        obj["priority"] = records[i].getText('priority',null,null) || '';
        arr.push(obj);
    }
    return arr;
}

function get_cases_colllections (id,radix){

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("internalid","company","anyof",id));
    var records = nlapiSearchRecord('supportcase', 'customsearch_clgx_cp_home_collections', filters, columns);
    var arr = new Array();
    if(records){
        var max = 5;
        if(records.length < max){
            max = records.length;
        }
        for ( var i = 0; i < max; i++ ) {
            var obj = new Object();
            var caseid = parseInt(records[i].getValue('internalid',null,null));
            //obj["id"] = caseid;
            obj["rid"] = caseid.toString(radix);
            obj["type"] = 'o';
            obj["number"] = records[i].getValue('casenumber',null,null);
            obj["title"] = records[i].getValue('title',null,null) || '';
            arr.push(obj);
        }
    }
    return arr;
}

function get_cases_maintenances(id,radix){

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("internalid","company","anyof",id));
    var records = nlapiSearchRecord('supportcase', 'customsearch_clgx_cp_home_maintenances', filters, columns);

    var total = 0;
    var arr = new Array();
    if(records){
        total = records.length;
        var max = 5;
        if(records.length < max){
            max = records.length;
        }
        for ( var i = 0; i < max; i++ ) {
            var obj = new Object();
            var caseid = parseInt(records[i].getValue('internalid',null,null));
            //obj["id"] = caseid;
            obj["rid"] = caseid.toString(radix);
            obj["type"] = 'm';
            obj["number"] = records[i].getValue('casenumber',null,null);
            obj["title"] = records[i].getValue('title',null,null) || '';
            obj["type"] = records[i].getText('custevent_cologix_sub_case_type',null,null) || '';
            obj["date"] = records[i].getValue('custevent_cologix_case_sched_followup',null,null) || '';
            arr.push(obj);
        }
    }
    return {
        "total": total,
        "cases": arr
    };
}

function get_cases_last (id,radix){

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("internalid","company","anyof",id));
    var records = nlapiSearchRecord('supportcase', 'customsearch_clgx_cp_home_last_cases', filters, columns);

    var total = 0;
    var arr = new Array();
    if(records){
        total = records.length;
        var max = 5;
        if(records.length < max){
            max = records.length;
        }
        for ( var i = 0; i < max; i++ ) {
            var obj = new Object();
            var caseid = parseInt(records[i].getValue('internalid',null,null));
            //obj["id"] = caseid;
            obj["rid"] = caseid.toString(radix);
            obj["type"] = 'o';
            obj["number"] = records[i].getValue('casenumber',null,null);
            obj["title"] = records[i].getValue('title',null,null) || '';
            obj["type"] = records[i].getText('type',null,null) + ' / ' + records[i].getText('custevent_cologix_sub_case_type',null,null);
            arr.push(obj);
        }
    }
    return {
        "total": total,
        "cases": arr
    };
}

function get_orders(id,radix){

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
    var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders', filters, columns);

    var arr = new Array();
    for ( var i = 0; records != null && i < records.length; i++ ) {
        var obj = new Object();
        var orderid = parseInt(records[i].getValue('internalid',null,'GROUP'));
        obj["id"] = orderid;
        obj["rid"] = orderid.toString(radix);
        obj["number"] = records[i].getValue('transactionnumber',null,'GROUP');
        obj["date"] = records[i].getValue('trandate',null,'GROUP') || '';
        //obj["typeid"] = parseInt(records[i].getValue('type',null,'GROUP')) || 0;
        obj["type"] = records[i].getText('type',null,'GROUP') || '';
        //obj["statid"] = parseInt(records[i].getValue('status',null,'GROUP')) || 0;
        obj["status"] = records[i].getText('status',null,'GROUP') || '';
        arr.push(obj);
    }
    return arr;
}

function get_services(id){

    var arr = ['rpt_utilization'];
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
    var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders_categs', filters);
    for ( var i = 0; records != null && i < records.length; i++ ) {
        var columns = records[i].getAllColumns();
        arr.push(records[i].getValue(columns[0]));
    }
    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_cust_customer",null,"is",id));
    var records = nlapiSearchRecord('customrecord_clgx_dcim_rpt_cust_customer', 'customsearch_clgx_cp_services_reports', filters);
    if(records){
        var consumption = records[0].getValue('custrecord_clgx_dcim_rpt_cust_rpt_kwh','CUSTRECORD_CLGX_DCIM_RPT_CUST_REPORT','GROUP') || 'F';
        var demand = records[0].getValue('custrecord_clgx_dcim_rpt_cust_rpt_kw','CUSTRECORD_CLGX_DCIM_RPT_CUST_REPORT','GROUP') || 'F';
        var environmentals = records[0].getValue('custrecord_clgx_dcim_rpt_cust_rpt_enviro','CUSTRECORD_CLGX_DCIM_RPT_CUST_REPORT','GROUP') || 'F';
        if(consumption == 'T') {
            arr.push('rpt_consumption');
        }
        if(demand == 'T') {
            arr.push('rpt_demand');
        }
        if(environmentals == 'T') {
            arr.push('rpt_environmentals');
        }
    }
    return arr;
}

function get_spaces(id){

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
    var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders', filters, columns);
    var soids = new Array();
    for ( var i = 0; records != null && i < records.length; i++ ) {
        soids.push(parseInt(records[i].getValue('internalid',null,'GROUP')));
    }

    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_space_service_order",null,"anyof",soids));
    var spaces = nlapiSearchRecord('customrecord_cologix_space', 'customsearch_clgx_ss_cp_spaceids', filters);
    var arr=[{"facid":0,"facname":"","spaceid":0,"spacename":"", "market": 0}];
    for ( var j = 0; spaces != null && j < spaces.length; j++ ) {
        var searchSpace = spaces[j];
        var columns = searchSpace.getAllColumns();
        var facid=parseInt(searchSpace.getValue(columns[0]));
        var facname=searchSpace.getValue(columns[1]);
        var spid=parseInt(searchSpace.getValue(columns[2]));
        var spname=searchSpace.getValue(columns[3]);

        //get facility name

        var filtersF = new Array();
        var columnsF = new Array();
        columnsF.push(new nlobjSearchColumn('name',null,null).setSort(false));
        filtersF.push(new nlobjSearchFilter("custrecord_clgx_facility_location",null,"is",facid));
        var facilities = nlapiSearchRecord('customrecord_cologix_facility', null, filtersF, columnsF);
        for ( var i = 0; facilities != null && i < facilities.length; i++ ) {
            var facname=facilities[i].getValue('name',null,null);
            //
        }
        var obj = {
            "facid": facid,
            "facname": facname,
            "spid": spid,
            "spname": spname
        };
        arr.push(obj);
    }
    return arr;
}
function get_billingadd(id){

    var filters = new Array();
    filters.push(new nlobjSearchFilter("internalid",null,"anyof",id));
    var arr=[];
    var arrdef=[];
    var list = nlapiSearchRecord('customer','customsearch_clgx_cp_customer_billing_ad', filters);
    for ( var i = 0; list != null && i < list.length; i++ ) {
        var searchlist = list[i];
        var columns = searchlist.getAllColumns();
        var address=searchlist.getValue(columns[0]);
        var isbiling=searchlist.getValue(columns[1]);
        var internalID=searchlist.getValue(columns[2]);
        if(isbiling=='T')
        {
            var obj = {
                "value": parseInt(internalID),
                "text":  address
            };
            arrdef.push(obj)
        }
        else
        {
            var obj = {
                "value": parseInt(internalID),
                "text":  address
            };
            arr.push(obj);
        }


    }
    if(arrdef.length>0)
    {
        return arrdef;
    }
    else
    {
        return arr;
    }

}


function get_facilities(id){

    var arr = [{"value":0,"text":""}];

    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
    var locations = nlapiSearchRecord('transaction', 'customsearch_clgx_cp_orders_locations', filters);
    var ids = [];
    for ( var i = 0; locations != null && i < locations.length; i++ ) {
        ids.push(parseInt(locations[i].getValue('location',null,'GROUP')));
    }
    if(ids.length > 1){
        var filters = new Array();
        var columns = new Array();
        columns.push(new nlobjSearchColumn('internalid',null,null));
        columns.push(new nlobjSearchColumn('name',null,null).setSort(false));
        filters.push(new nlobjSearchFilter("custrecord_clgx_facility_location",null,"anyof",ids));
        var facilities = nlapiSearchRecord('customrecord_cologix_facility', null, filters, columns);
        for ( var i = 0; facilities != null && i < facilities.length; i++ ) {
            var obj = {
                "value": parseInt(facilities[i].getValue('internalid',null,null)),
                "text": facilities[i].getValue('name',null,null)
            };
            arr.push(obj);
        }
    }
    return arr;
}

function get_locations(id){

    /*
     var arr = [{"value":0,"text":""}];
     var filters = new Array();
     filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
     var locations = nlapiSearchRecord('transaction', 'customsearch_clgx_cp_orders_locations', filters);
     for ( var i = 0; locations != null && i < locations.length; i++ ) {
     var obj = {
     "value": parseInt(locations[i].getValue('location',null,'GROUP')),
     "text": locations[i].getValue('locationnohierarchy',null,'GROUP')
     };
     arr.push(obj);
     }
     */
    var arrLocations = [];
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
    var locations = nlapiSearchRecord('transaction', 'customsearch_clgx_cp_orders_locations', filters);
    for ( var i = 0; locations != null && i < locations.length; i++ ) {
        arrLocations.push(parseInt(locations[i].getValue('location',null,'GROUP')));
    }

    var arr = [{"value":0,"text":""}];
    var columns = new Array();
    columns.push(new nlobjSearchColumn('internalid',null,null));
    columns.push(new nlobjSearchColumn('name',null,null).setSort(false));
    columns.push(new nlobjSearchColumn('custrecord_clgx_facility_location',null,null));
    columns.push(new nlobjSearchColumn('custrecord_clgx_facilty_market',null,null));
    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_clgx_facility_location",null,"anyof",arrLocations));
    var facilities = nlapiSearchRecord('customrecord_cologix_facility', null, filters, columns);
    for ( var i = 0; facilities != null && i < facilities.length; i++ ) {
        var obj = {
            "value": parseInt(facilities[i].getValue('custrecord_clgx_facility_location',null,null)),
            "text": facilities[i].getValue('name',null,null)
        };
        arr.push(obj);
    }

    return arr;
}

function get_markets(id){

    var arr = [{"value":0,"text":""}];
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
    var locations = nlapiSearchRecord('transaction', 'customsearch_clgx_cp_orders_markets', filters);
    for ( var i = 0; locations != null && i < locations.length; i++ ) {
        var obj = {
            "value": parseInt(locations[i].getValue('custbody_clgx_consolidate_locations',null,'GROUP')),
            "text": locations[i].getText('custbody_clgx_consolidate_locations',null,'GROUP')
        };
        arr.push(obj);
    }
    return arr;
}

function get_invoices_last(id,radix){

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_customer",null,"anyof",id));
    var records = nlapiSearchRecord('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_cp_invoices_dashboard', filters, columns);

    var arr = new Array();
    if(records){
        var max = 5;
        if(records.length < max){
            max = records.length;
        }
        for ( var i = 0; i < max; i++ ) {
            var obj = new Object();
            var invoiceid = parseInt(records[i].getValue('internalid',null,null));
            //obj["id"] = invoiceid;
            obj["rid"] = invoiceid.toString(radix);
            obj["number"] = records[i].getValue('name',null,null);
            obj["date"] = records[i].getValue('custrecord_clgx_consol_inv_date',null,null) || '';
            obj["total"] = parseFloat(records[i].getValue('custrecord_clgx_consol_inv_total',null,null)) || 0;
            arr.push(obj);
        }
    }
    return arr;
}
function get_rights(contactid,modifierid,companyid,radix,nac,sdamin) {
    if(sdamin){
        return {
            "users":sdamin,
            "security":sdamin,
            "cases":sdamin,
            "casesfin":sdamin,
            "reports":1,
            "visits":sdamin,
            "orders":sdamin,
            "invoices":1,
            "ccards":sdamin,
            "colocation":1,
            "network":1,
            "domains":sdamin,
            "managed":1
        };

    } else {

        var columns = new Array();
        columns.push(new nlobjSearchColumn('internalid',null,null).setSort(true));
        var filters = new Array();
        filters.push(new nlobjSearchFilter("company",null,"is",companyid));
        filters.push(new nlobjSearchFilter("custentity_clgx_modifier",null,"is",modifierid));
        var search = nlapiSearchRecord('contact', null, filters, columns);

        if(search){
            var internalid = parseInt(search[0].getValue('internalid',null,null));
            nlapiLogExecution('DEBUG','internalid', internalid);
            var rights = nlapiLookupField('contact', internalid, 'custentity_clgx_cp_user_rights_json') || get_min_rights();
            nlapiLogExecution('DEBUG','rightsDebug', rights);
        } else {
            var rights = get_min_rights();
        }

        return JSON.parse(rights);
    }
}


function get_min_rights() {
    return {
        "users":0,
        "security":0,
        "cases":0,
        "casesfin":0,
        "reports":0,
        "visits":0,
        "orders":0,
        "invoices":0,
        "ccards":0,
        "colocation":0,
        "network":0,
        "domains":0,
        "managed":0
    };
}



function get_uuid() {
    var s = [], itoh = '0123456789ABCDEF';
    for (var i = 0; i <36; i++) s[i] = Math.floor(Math.random()*0x10);
    s[14] = 4;
    s[19] = (s[19] & 0x3) | 0x8;
    for (var i = 0; i <36; i++) s[i] = itoh[s[i]];
    s[8] = s[13] = s[18] = s[23] = '-';
    return s.join('');
}

function generate_password(length, special) {
    var iteration = 0;
    var password = "";
    var randomNumber;
    if(special == undefined){
        var special = false;
    }
    while(iteration < length){
        randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
        if(!special){
            if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
            if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
            if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
            if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
        }
        iteration++;
        password += String.fromCharCode(randomNumber);
    }
    return password;
}

function get_radix() {
    return Math.floor(Math.random() * (35 - 5)) + 5;
}

function send_new_password (modifierid, contactid, email, lang) {

    var newpass = generate_password(12, false);

    if(lang == 17){
        var subject = 'Portail de Cologix';
        var body = 'Madame, Monsieur,\n\n' +
            'Voici votre mot de passe:\n\n' + newpass + '\n\n' +
            'Merci de faire affaire avec Cologix.\n\n' +
            'Au plaisir de vous &ecirc;tre utile,\n' +
            'Cologix\n\n';
        var msg = 'Un nouveau mot de passe a été envoyé à votre e-mail.';
    } else {
        var subject = 'Cologix Customer Portal - Password Reset';
        var body = 'Dear Customer,\n\n' +
            'Here is your new password:\n\n' + newpass + '\n\n' +
            'Thank you for your business.\n\n' +
            'Sincerely,\n' +
            'Cologix\n\n';
        var msg = 'A new password was sent to the email we have on file.';
    }

    nlapiSendEmail(432742, email, subject, body, null, null, null, null, null, null, "support@cologix.com");

    var redate = moment().format('M/D/YYYY');
    var fields = ['custrecord_clgx_modifier_password','custrecord_clgx_modifier_password_date','custrecord_clgx_modifier_password_redate'];
    var values = [newpass,null,redate];
    nlapiSubmitField('customrecord_clgx_modifier', modifierid, fields, values);

    return msg;
}

function set_session(obj){

    var now = moment().format("M/D/YYYY h:mm:ss a");
    var record = nlapiCreateRecord('customrecord_clgx_api_cust_portal_logins');
    record.setFieldValue('custrecord_clgx_api_cust_portal_uuid', obj.ns.nsid);
    record.setFieldValue('custrecord_clgx_api_cust_portal_cfid', obj.ns.cfid);
    record.setFieldValue('custrecord_clgx_api_cust_portal_ip', obj.ns.ip);
    record.setFieldValue('custrecord_clgx_api_cust_portal_radix', obj.ns.radix);
    record.setFieldValue('custrecord_clgx_api_cust_portal_date', now);
    record.setFieldValue('custrecord_clgx_api_cust_portal_last', now);
    record.setFieldValue('custrecord_clgx_api_cust_portal_modifier', obj.user.mid);
    record.setFieldValue('custrecord_clgx_api_cust_portal_contact', obj.user.id);
    record.setFieldValue('custrecord_clgx_api_cust_portal_entity', obj.company.id);
    record.setFieldValue('custrecord_clgx_api_cust_portal_rights', JSON.stringify(obj.company.rights));
    record.setFieldValue('custrecord_clgx_api_cust_portal_entities', JSON.stringify(obj.companies));
    if(obj.user.sadmin > 0){
        record.setFieldValue('custrecord_clgx_api_cust_portal_sadmin', obj.user.sadmin);
    }
    record.setFieldValue('custrecord_clgx_api_cust_portal_language', obj.user.langid);
    record.setFieldValue('custrecord_clgx_api_cust_portal_status', 1);
    var recid = nlapiSubmitRecord(record, false,true);
    return recid;
}

function get_billing_contacts(companyid){

    var filters = [];
    filters.push(new nlobjSearchFilter("company",null,"is",companyid));
    filters.push(new nlobjSearchFilter("contactrole",null,"is",1));
    var searchUsers = nlapiSearchRecord('contact', 'customsearch_clgx_cp_contacts', filters);

    if(searchUsers){
        return searchUsers.length;
    } else {
        return 0;
    }
}