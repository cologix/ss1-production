nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Consumption.js
//	ScriptID:	customscript_clgx_rl_cp_consumption
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=680&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	07/06/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    if(srights.reports > 0){

        var panel = datain['p'] || "0";
        var panelid = parseInt(panel, radix);

        obj["data"] = get_kwh(companyid,contactid,modifierid,radix,panelid);

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing consumption.';

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_CONSUMPTION';
        obj.msg = 'No rights for consumption.';
    }
    return obj;
});

function get_kwh(companyid,contactid,modifierid,radix,panelid){
    nlapiLogExecution("DEBUG", "0", companyid+';'+contactid+';'+modifierid+';'+radix+';'+panelid);
    // SOs of the customer
    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",companyid));
    var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders', filters, columns);
    var ids = new Array();
    for ( var i = 0; records != null && i < records.length; i++ ) {
        ids.push(parseInt(records[i].getValue('internalid',null,'GROUP')));
    }

    // array of panels
    var panels = [];
    var filters = new Array();
    filters.push(new nlobjSearchFilter('custrecord_power_circuit_service_order',null,'anyof', ids));
    var search = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_cp_kwh_panels', filters);
    for ( var i = 0; search != null && i < search.length; i++ ) {
        var pid = parseInt(search[i].getValue('custrecord_clgx_dcim_device',null,'GROUP')) || 0;
        var panel = {
            "id": pid,
            "rid": pid.toString(radix),
            "name": search[i].getText('custrecord_clgx_dcim_device',null,'GROUP') || ''
        };
        panels.push(panel);
    }
    if(panels.length>0) {
        if (panelid == 0) {
            panelid = panels[0].id;
        }

        // months of rolling year
        var months = [];
        for (var i = 0; i < 7; i++) {
            var month = {
                "id": i,
                "month": moment().startOf('month').subtract('months', i).format('MM/YY')
            };
            months.push(month);
        }

        var from = moment().startOf('month').subtract('months', 6).format('M/D/YYYY');
        var to = moment().format('M/D/YYYY');

        // start building response object
        var resp = {
            "months": months,
            "from": from,
            "to": to,
            "panels": panels,
            "panel": {}
        };
        nlapiLogExecution("DEBUG", "1", companyid + ';' + contactid + ';' + modifierid + ';' + radix + ';' + panelid);
        var breakers = [];
        var filters = new Array();
        filters.push(new nlobjSearchFilter('custrecord_clgx_dcim_points_day_so', null, 'anyof', ids));
        filters.push(new nlobjSearchFilter('custrecord_clgx_dcim_points_day_pnl', null, 'anyof', panelid));
        filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_date", null, "within", from, to));
        var search = nlapiLoadSearch('customrecord_clgx_dcim_points_day', 'customsearch_clgx_cp_kwh_breakers');
        search.addFilters(filters);
        var resultSet = search.runSearch();
        resultSet.forEachResult(function (searchResult) {

            var columns = searchResult.getAllColumns();
            var powerid = parseInt(searchResult.getValue('custrecord_clgx_dcim_points_day_power', null, 'GROUP')) || 0;
            var breaker = parseInt(searchResult.getText('custrecord_clgx_dcim_points_day_breaker', null, 'GROUP')) || 0;
            var phase = searchResult.getValue('custrecord_clgx_dcim_points_day_phase', null, 'GROUP') || '';

            var breaker = {
                "powerid": powerid,
                "node": phase,
                "nodetype": "breaker",
                "kwh": parseFloat(searchResult.getValue('custrecord_clgx_dcim_points_day_kw_adj', null, 'SUM')),
                "kwh0": parseFloat(searchResult.getValue(columns[4])),
                "kwh1": parseFloat(searchResult.getValue(columns[5])),
                "kwh2": parseFloat(searchResult.getValue(columns[6])),
                "kwh3": parseFloat(searchResult.getValue(columns[7])),
                "kwh4": parseFloat(searchResult.getValue(columns[8])),
                "kwh5": parseFloat(searchResult.getValue(columns[9])),
                "kwh6": parseFloat(searchResult.getValue(columns[10])),
                "faicon": 'fa fa-bolt',
                "leaf": true
            };
            breakers.push(breaker);
            return true;
        });
        nlapiLogExecution("DEBUG", "2", companyid + ';' + contactid + ';' + modifierid + ';' + radix + ';' + panelid);
        var powers = [];
        var filters = new Array();
        filters.push(new nlobjSearchFilter('custrecord_power_circuit_service_order', null, 'anyof', ids));
        filters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device', null, 'anyof', panelid));
        var search = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_cp_kwh_powers');
        search.addFilters(filters);
        var resultSet = search.runSearch();
        resultSet.forEachResult(function (searchResult) {

            var powerid = parseInt(searchResult.getValue('internalid', null, null));
            var soid = parseInt(searchResult.getValue('custrecord_power_circuit_service_order', null, null));
            var name = (searchResult.getText('custrecord_power_circuit_service_order', null, null)).split("#");
            var so = (name[name.length - 1]).trim() || '';

            var spaceid = parseInt(searchResult.getValue('custrecord_cologix_power_space', null, null));
            var arrBreakers = _.filter(breakers, function (arr) {
                return (arr.powerid == powerid);
            });
            var sum = _.reduce(_.pluck(arrBreakers, 'kwh'), function (memo, num) {
                return memo + num;
            }, 0);
            var sum0 = _.reduce(_.pluck(arrBreakers, 'kwh0'), function (memo, num) {
                return memo + num;
            }, 0);
            var sum1 = _.reduce(_.pluck(arrBreakers, 'kwh1'), function (memo, num) {
                return memo + num;
            }, 0);
            var sum2 = _.reduce(_.pluck(arrBreakers, 'kwh2'), function (memo, num) {
                return memo + num;
            }, 0);
            var sum3 = _.reduce(_.pluck(arrBreakers, 'kwh3'), function (memo, num) {
                return memo + num;
            }, 0);
            var sum4 = _.reduce(_.pluck(arrBreakers, 'kwh4'), function (memo, num) {
                return memo + num;
            }, 0);
            var sum5 = _.reduce(_.pluck(arrBreakers, 'kwh5'), function (memo, num) {
                return memo + num;
            }, 0);
            var sum6 = _.reduce(_.pluck(arrBreakers, 'kwh6'), function (memo, num) {
                return memo + num;
            }, 0);

            var power = {
                "panelid": parseInt(searchResult.getValue('custrecord_clgx_dcim_device', null, null)),
                "node": searchResult.getValue('name', null, null),
                "nodetype": "power",
                "rid": powerid.toString(radix),
                "rsoid": soid.toString(radix),
                "so": so,
                "rspaceid": spaceid.toString(radix),
                "space": searchResult.getText('custrecord_cologix_power_space', null, null),
                "volts": searchResult.getText('custrecord_cologix_power_volts', null, null),
                "amps": searchResult.getText('custrecord_cologix_power_amps', null, null),
                "module": parseInt(searchResult.getText('custrecord_cologix_power_upsbreaker', null, null)) || 0,
                "kwh": sum,
                "kwh0": sum0,
                "kwh1": sum1,
                "kwh2": sum2,
                "kwh3": sum3,
                "kwh4": sum4,
                "kwh5": sum5,
                "kwh6": sum6,
                "children": _.map(arrBreakers, function (obj) {
                    return _.omit(obj, ['powerid']);
                }),
                "expanded": false,
                "faicon": 'fa fa-plug',
                "leaf": false
            };
            powers.push(power);
            return true;
        });
        nlapiLogExecution("DEBUG", "3", companyid + ';' + contactid + ';' + modifierid + ';' + radix + ';' + panelid);
        var sum = _.reduce(_.pluck(powers, 'kwh'), function (memo, num) {
            return memo + num;
        }, 0);
        var sum0 = _.reduce(_.pluck(powers, 'kwh0'), function (memo, num) {
            return memo + num;
        }, 0);
        var sum1 = _.reduce(_.pluck(powers, 'kwh1'), function (memo, num) {
            return memo + num;
        }, 0);
        var sum2 = _.reduce(_.pluck(powers, 'kwh2'), function (memo, num) {
            return memo + num;
        }, 0);
        var sum3 = _.reduce(_.pluck(powers, 'kwh3'), function (memo, num) {
            return memo + num;
        }, 0);
        var sum4 = _.reduce(_.pluck(powers, 'kwh4'), function (memo, num) {
            return memo + num;
        }, 0);
        var sum5 = _.reduce(_.pluck(powers, 'kwh5'), function (memo, num) {
            return memo + num;
        }, 0);
        var sum6 = _.reduce(_.pluck(powers, 'kwh6'), function (memo, num) {
            return memo + num;
        }, 0);
        var totals = {
            //"panelid": panelid,
            "node": 'Totals',
            "nodetype": "panel",
            //"rid": panelid.toString(radix),
            "kwh": sum,
            "kwh0": sum0,
            "kwh1": sum1,
            "kwh2": sum2,
            "kwh3": sum3,
            "kwh4": sum4,
            "kwh5": sum5,
            "kwh6": sum6,
            "faicon": 'fa fa-chevron-circle-right',
            "leaf": true
        };
        powers.push(totals);
        nlapiLogExecution("DEBUG", "4", companyid + ';' + contactid + ';' + modifierid + ';' + radix + ';' + panelid);
        resp.panel = {
            "node": nlapiLookupField('customrecord_clgx_dcim_devices', panelid, 'name'),
            "nodetype": "panel",
            "rid": panelid.toString(radix),
            "children": _.map(powers, function (obj) {
                return _.omit(obj, ['panelid']);
            }),
            "expanded": false,
            "faicon": 'fa fa-plug',
            "leaf": false
        };
    }
    else{
        var resp = {
            "months": '',
            "from": '',
            "to": "",
            "panels": [],
            "panel": {}
        };
    }

    return resp;
}


function get_breaker_phase(breaker, paneltype){
    var mod3 = breaker % 3;
    var mod6 = breaker % 6;
    var phase = '';
    if(paneltype == 1){ // Traditional
        if(mod6 == 1 || mod6 == 2){
            phase = 'A';
        }
        if(mod6 == 3 || mod6 == 4){
            phase = 'B';
        }
        if(mod6 == 5 || mod6 == 0){
            phase = 'C';
        }
    }
    else if(paneltype > 1){ // PDPM or Vertical PDU or Cyberex
        if(mod3 == 1){
            phase = 'A';
        }
        if(mod3 == 2){
            phase = 'B';
        }
        if(mod3 == 0){
            phase = 'C';
        }
    }
    else{
        phase = '';
    }
    return phase;
}

