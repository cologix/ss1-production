nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Environmentals.js
//	ScriptID:	customscript_clgx_rl_cp_environmentals
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=687&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	07/28/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

	if(srights.reports > 0){
		
		obj["data"] = get_environmentals(companyid,contactid,modifierid,radix);
		
		var now = moment().format("M/D/YYYY h:mm:ss a");
		nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);
		
		obj.code = 'SUCCESS';
		obj.msg = 'You are managing environmentals.';
		
	} else {
		obj.error = 'T';
		obj.code = 'NO_RIGHTS_environmentals';
		obj.msg = 'No rights for environmentals.';
	}
	return obj;
});

function get_environmentals(companyid,contactid,modifierid,radix){
	
	var reports = [];
	var environmentals = [];
	var averages = [];
	var ids = [];
	
	var columns = new Array();
	columns.push(new nlobjSearchColumn('internalid',null,null));
	columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_report',null,null));
    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_cust_customer",null,"anyof",companyid));
    var search = nlapiSearchRecord('customrecord_clgx_dcim_rpt_cust_customer', null, filters, columns);
    for ( var i = 0; search != null && i < search.length; i++ ) {
    	reports.push(parseInt(search[i].getValue('custrecord_clgx_dcim_rpt_cust_report',null,null)));
	}
    
    if(reports){
    	
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_dev_fac',null,null));
		columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_dev_device',null,null));
		columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_rpt_cust_dev_points',null,null));
	    var filters = new Array();
	    filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_cust_dev_rpt",null,"anyof",reports));
	    var search = nlapiSearchRecord('customrecord_clgx_dcim_rpt_cust_dev', null, filters, columns);
	    for ( var i = 0; search != null && i < search.length; i++ ) {
	    	
	    	var facility = search[i].getText('custrecord_clgx_dcim_rpt_cust_dev_fac',null,null) || '';
	    	var device = search[i].getText('custrecord_clgx_dcim_rpt_cust_dev_device',null,null) || '';
	    	var pointsids = search[i].getValue('custrecord_clgx_dcim_rpt_cust_dev_points',null,null);
	    	var points = search[i].getText('custrecord_clgx_dcim_rpt_cust_dev_points',null,null);
	    	var arrPoitsIDs = pointsids.split(',');
	    	var arrPoits = points.split(',');
	    	
	    	for ( var j = 0; arrPoitsIDs != null && j < arrPoitsIDs.length; j++ ) {
	        	var obj = {
	            		"facility": facility,
	            		"device": device,
	            		"pointid": parseInt(arrPoitsIDs[j]),
	            		"point": arrPoits[j],
	            		"average": 0
	            	};
	        	environmentals.push(obj);
	        	ids.push(parseInt(arrPoitsIDs[j]));
	    	}
		}
	    
	    if(ids.length > 0){
			var columns = new Array();
			columns.push(new nlobjSearchColumn('internalid',null,null));
			columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_avg',null,null));
			columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_units',null,null));
		    var filters = new Array();
		    filters.push(new nlobjSearchFilter("internalid",null,"anyof",ids));
		    var search = nlapiSearchRecord('customrecord_clgx_dcim_points', null, filters, columns);
		    for ( var i = 0; search != null && i < search.length; i++ ) {
		    	
		    	var unitsid = parseInt(search[i].getValue('custrecord_clgx_dcim_points_units',null,null)) || 0;
		    	var units = search[i].getText('custrecord_clgx_dcim_points_units',null,null) || '';
		    	var value = parseFloat(search[i].getValue('custrecord_clgx_dcim_points_day_avg',null,null)) || 0;
		    	if(unitsid == 3){
		    		value = (value - 32) * 0.5556;
		    	}
		    	var average = value.toFixed(2) + ' ' + units
		    	var obj = {
		        		"pointid": parseInt(search[i].getValue('internalid',null,null)),
		        		"average": average
		        	};
		    	averages.push(obj);
			}
		    
		    for ( var i = 0; environmentals != null && i < environmentals.length; i++ ) {
		    	var obj = _.find(averages, function(arr){ return (arr.pointid == environmentals[i].pointid) ; });
				if(obj){
					environmentals[i].average = obj.average;
				}
		    }
	    }
	}
	return environmentals;
}