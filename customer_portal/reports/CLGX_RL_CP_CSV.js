nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_CSV.js
//	ScriptID:	customscript_clgx_rl_cp_csv
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=678&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	07/05/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

	if(srights.reports > 0){

		var rid = datain['rid'] || "0";
		var id = parseInt(rid, radix);
		try {
        	var file = nlapiLoadFile(id);
    		obj["id"] = id;
    		obj["rid"] = rid;
    		//obj["url"] = 'https://system.na2.netsuite.com' + file.getURL(),
    		obj["url"] = 'https://1337135.app.netsuite.com' + file.getURL(),
    		obj["name"] = file.getName().replace(/,/g, "");
             var bodyFile = file.getValue();
            obj["cont"]=bodyFile;
        }
        catch (e) {
    		obj["id"] = '0';
    		obj["rid"] = '0';
    		obj["url"] = '',
    		obj["name"] = '';
            obj["cont"]='';
        }
		
		var now = moment().format("M/D/YYYY h:mm:ss a");
		nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);
		
		obj.code = 'SUCCESS';
		obj.msg = 'You are managing powers.';
		
	} else {
		obj.error = 'T';
		obj.code = 'NO_RIGHTS_POWERS';
		obj.msg = 'No rights for powers.';
	}
	return obj;
});

