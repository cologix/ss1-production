//------------------------------------------------------
//	Script:		CLGX_RL_CP_Powers_kWs.js
//	ScriptID:	customscript_clgx_rl_cp_powers_kws
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=677&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	07/05/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv) {

	if(srights.reports > 0){
		
		var direction = datain['direction'] || 'ASC';
		var order_by = datain['order_by'] || '';
		var page = datain['page'] || 1;
		var per_page = datain['per_page'];
		var search = datain['search'] || '{}';
		var csv = datain['csv'] || 0;
		
		obj["data"] = get_peaks(companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv);
		
		var now = moment().format("M/D/YYYY h:mm:ss a");
		nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);
		
		obj.code = 'SUCCESS';
		obj.msg = 'You are managing powers.';
		
	} else {
		obj.error = 'T';
		obj.code = 'NO_RIGHTS_POWERS';
		obj.msg = 'No rights for powers.';
	}
	
	return obj;
	
});

function get_peaks(companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv){
	
	var filters = new Array();
	filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_day_customer",null,"is",companyid));
	var searchDays = nlapiSearchRecord('customrecord_clgx_dcim_customer_peak_day', 'customsearch_clgx_cp_customer_peak_days', filters);
	var days = [];
	for ( var i = 0; searchDays != null && i < searchDays.length; i++ ) {
		var date = searchDays[i].getValue('custrecord_clgx_dcim_peak_day_date',null,null);
		var file = parseInt(searchDays[i].getValue('custrecord_clgx_dcim_peak_day_file',null,null));
		var day = {
				"id": parseInt(searchDays[i].getValue('custrecord_clgx_dcim_peak_day_facilty',null,null)),
				"month": moment(searchDays[i].getValue('custrecord_clgx_dcim_peak_day_date',null,null)).format('MM/YYYY'),
				"node": searchDays[i].getValue('custrecord_clgx_dcim_peak_day_date',null,null),
				"nodetype": "day",
				"peak": parseFloat(searchDays[i].getValue('custrecord_clgx_dcim_peak_day_kw',null,null)),
				"csv": file,
				"rcsv": file.toString(radix),
	  			"faicon": 'fa fa-calendar-o',
	  			"leaf": true
		};
		days.push(day);
	}
	var arrMonths = _.uniq(_.pluck(days, 'month'));
	
	var filters = new Array();
	filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_customer",null,"is",customerid));
	var searchPeaks = nlapiSearchRecord('customrecord_clgx_dcim_customer_peak', 'customsearch_clgx_cp_customer_peak', filters);
	var facilities = [];
	for ( var i = 0; searchPeaks != null && i < searchPeaks.length; i++ ) {
		
		var facilityid = parseInt(searchPeaks[i].getValue('custrecord_clgx_dcim_peak_facility',null,null));
		var file = parseInt(searchDays[i].getValue('custrecord_clgx_dcim_peak_file',null,null));
		
		var arrM = [];
		for ( var j = 0; j < arrMonths.length; j++ ) {
			
			var arrPeakDays = _.filter(days, function(arr){
				return (arr.id == facilityid && arr.month == arrMonths[j]);
			});
			var objM = {
					"node": arrMonths[j],
					"nodetype": "month",
					"children": _.map(arrPeakDays, function(obj) { return _.omit(obj, ['id', 'month']); }),
		  			"faicon": 'fa fa-calendar',
		  			"extended": false,
		  			"leaf": false
			};
			arrM.push(objM);
		}
		var facility = {
				//"id": facilityid,
				"node": searchPeaks[i].getText('custrecord_clgx_dcim_peak_facility',null,null),
				"nodetype": "facility",
				"max": parseFloat(searchPeaks[i].getValue('custrecord_clgx_dcim_peak_kw_max',null,null)),
				"peak": parseFloat(searchPeaks[i].getValue('custrecord_clgx_dcim_peak_kw',null,null)),
				"date": searchPeaks[i].getValue('custrecord_clgx_dcim_peak_date',null,null),
				"csv": file,
				"rcsv": file.toString(radix),
				"update": searchPeaks[i].getValue('custrecord_clgx_dcim_peak_update',null,null),
				"sent": searchPeaks[i].getValue('custrecord_clgx_dcim_peak_email_date',null,null),
				"children": arrM,
	  			"faicon": 'fa fa-building-o',
	  			"extended": true,
	  			"leaf": false
		};
		facilities.push(facility);
	}
	return facilities
}