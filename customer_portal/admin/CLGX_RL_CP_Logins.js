nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Logins.js
//	ScriptID:	customscript_clgx_rl_cp_logins
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=662&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/17/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv) {

		var direction = datain['direction'] || 'ASC';
		var order_by = datain['order_by'] || '';
		var page = datain['page'] || 1;
		var per_page = datain['per_page'];
		var search = datain['search'] || '';
		var csv = datain['csv'] || 0;
		
		obj["logins"] = get_logins(companyid,contactid,modifierid,radix,sid,direction,order_by,page,per_page,search,srights.users,csv);
		
		var now = moment().format("M/D/YYYY h:mm:ss a");
		nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);
		
		obj.code = 'SUCCESS';
		obj.msg = 'You are managing logins.';
		
	return obj;
});

function get_logins(companyid,contactid,modifierid,radix,sid,direction,order_by,page,per_page,search,rights,csv) {
	
	var obj = {};
	var arr = [];
	var total = 0;
	var pages = 0;
	var start = 0;
	var end = 0;
	var has_more = false;
	
	var users = [{"value": 0, "text": '*'}];
	var usersids = [];
	
	search = JSON.parse(search);
	
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_clgx_api_cust_portal_entity',null,'anyof',companyid));
	if(rights == 0){
		filters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_contact",null,"anyof",contactid));
	}
	var records = nlapiSearchRecord('customrecord_clgx_api_cust_portal_logins', 'customsearch_clgx_cp_sessions_users', filters, columns);
	if(records){
		for ( var i = 0; records != null && i < records.length; i++ ) {
			
			var userid = parseInt(records[i].getValue('internalid','custrecord_clgx_api_cust_portal_contact',null));
			var user = records[i].getValue('entityid','custrecord_clgx_api_cust_portal_contact',null);
			
			if(_.indexOf(usersids, userid) == -1 && userid){
				usersids.push(userid);
				users.push({"value": userid, "text": user});
			}
		}
		users = _.sortBy(users, function(obj){ return obj.value;});
	}
	
	var columns = new Array();
	var filters = new Array();
	if(search){
		if(rights == 0){
			filters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_contact",null,"anyof",contactid));
		}
		if(search.users > 0){
			filters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_contact",null,"anyof",search.users));
		}
		if(search.from && search.to){
			filters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_date",null,"within",search.from,search.to));
		}
		if(search.from && !search.to){
			filters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_date",null,"on",search.from));
		}
		if(!search.from && search.to){
			filters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_date",null,"on",search.to));
		}
    }
	filters.push(new nlobjSearchFilter('custrecord_clgx_api_cust_portal_entity',null,'anyof',companyid));
	var records = nlapiSearchRecord('customrecord_clgx_api_cust_portal_logins', 'customsearch_clgx_cp_sessions_users', filters, columns);
	
	if(records){
		
		total = records.length;
		pages = Math.ceil(total / per_page);
		page = (parseInt(page) > pages ? pages : parseInt(page));
		has_more = (pages > 1 ? true : false);
		start = (page - 1) * per_page;
		end = (start + per_page > total ? total : start + per_page);
		
		for ( var i = start; i < end; i++ ) {
			var rec = {};
			rec["user"] = records[i].getValue('entityid','custrecord_clgx_api_cust_portal_contact',null);
			rec["username"] = records[i].getText('custrecord_clgx_api_cust_portal_modifier',null,null);
			//var id = parseInt(records[i].getValue('internalid',null,null));
			//rec["rid"] = id.toString(radix);
			rec["sid"] = records[i].getValue('custrecord_clgx_api_cust_portal_uuid',null,null);
			rec["ip"] = records[i].getValue('custrecord_clgx_api_cust_portal_ip',null,null);
			rec["start"] = records[i].getValue('custrecord_clgx_api_cust_portal_date',null,null);
			rec["last"] = records[i].getValue('custrecord_clgx_api_cust_portal_last',null,null);
			rec["status"] = records[i].getText('custrecord_clgx_api_cust_portal_status',null,null);
			rec["lang"] = records[i].getText('custrecord_clgx_api_cust_portal_language',null,null);
			
			arr.push(rec);
		}
	}
	obj["data"] = arr;
	if(csv){
		obj["csv"] = get_logins_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,rights,csv);
	}
	obj["search"] = search;
	obj["lists"] = {"users": users};
	obj["direction"] = direction;
	obj["order_by"] = order_by;
	obj["page"] = page;
	obj["pages"] = pages;
	obj["per_page"] = per_page;
	obj["has_more"] = has_more;
	obj["start"] = start;
	obj["end"] = end;
	obj["total"] = total;
	
	return obj;
}

function get_logins_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,rights,csv){
	
	var text = 'User, User Name,Session ID,IP, Date\n';
	
	var columns = new Array();
	var filters = new Array();
	if(search){
		if(rights == 0){
			filters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_contact",null,"anyof",contactid));
		}
		if(search.users > 0){
			filters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_contact",null,"anyof",search.users));
		}
		if(search.from && search.to){
			filters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_date",null,"within",search.from,search.to));
		}
		if(search.from && !search.to){
			filters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_date",null,"on",search.from));
		}
		if(!search.from && search.to){
			filters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_date",null,"on",search.to));
		}
    }
	filters.push(new nlobjSearchFilter('custrecord_clgx_api_cust_portal_entity',null,'anyof',companyid));
	var records = nlapiSearchRecord('customrecord_clgx_api_cust_portal_logins', 'customsearch_clgx_cp_sessions_users', filters, columns);
	
	for ( var i = 0; records != null && i < records.length; i++ ) {
		text += records[i].getValue('entityid','custrecord_clgx_api_cust_portal_contact',null) + ',';
		text += records[i].getText('custrecord_clgx_api_cust_portal_modifier',null,null) + ',';
		text += records[i].getValue('custrecord_clgx_api_cust_portal_uuid',null,null) + ',';
		text += records[i].getValue('custrecord_clgx_api_cust_portal_ip',null,null) + ',';
		text += records[i].getValue('custrecord_clgx_api_cust_portal_date',null,null);
		text += '\n';
	}
	return text;
}