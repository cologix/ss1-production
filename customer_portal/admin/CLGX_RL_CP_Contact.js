nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Contact.js
//	ScriptID:	customscript_clgx_rl_cp_contact
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=683&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	07/15/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,id) {

    var rid = datain['rid'] || '0';
    var id = parseInt(rid, radix);
    var act = datain['act'] || 'get';
    var copy = datain['copy'] || 0;
    // nlapiLogExecution('DEBUG', 'action', act);

    if(srights.users > 0 || contactid == id){

        obj["id"] = id;
        obj["rid"] = rid;

        if(act == 'get'){
            obj["data"] = get_contact(companyid,contactid,modifierid,radix,sid,id,false);
        }
        if(act == 'me'){
            obj["data"] = get_contact(companyid,contactid,modifierid,radix,sid,id,true);
        }
        if(act == 'switch'){
            var uname = datain['uname'] || '';
            obj["resp"] = get_contact_impersonate(companyid,contactid,modifierid,radix,sid,uname);
        }
        if(act == 'new'){
            obj["users"] = get_contacts(companyid);
        }
        if(act == 'create'){
            var user = datain['user'];
            obj["resp"] = create_contact(companyid,contactid,modifierid,radix,sid,id,user,copy);
        }
        if(act == 'update'){
            var user = datain['user'];
            obj["resp"] = update_contact(companyid,contactid,modifierid,radix,sid,id,user);
        }
        if(act == 'rights'){
            var rights = datain['rights'];
            obj["resp"] = rights_contact(companyid,contactid,modifierid,radix,sid,id,rights,copy);
        }
        if(act == 'terms'){
            nlapiSubmitField('customrecord_clgx_modifier', modifierid, 'custrecord_clgx_modifier_accepted_terms', 'T');
        }
        if(act == 'passwd'){
            var passwd = datain['passwd'];
            obj["resp"] = update_password(companyid,contactid,modifierid,radix,sid,id,passwd);
        }
        if(act == 'uname'){
            var uname = datain['uname'];
            obj["resp"] = update_user_name(companyid,contactid,modifierid,radix,sid,id,uname);
        }

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing users.';

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_USERS';
        obj.msg = 'No rights for users.';
    }

    return obj;
});

var put = wrap(function put(datain,obj,srights,companyid,contactid,modifierid,radix,sid,id) {

    if(srights.users > 0){

        var act = datain['act'] || 0;
        var rid = datain['rid'] || '0';
        var id = parseInt(rid, radix);

        // get modifier and header company of contact id
        var modifid = nlapiLookupField('contact', id, 'custentity_clgx_modifier') || 0;
        var compid = nlapiLookupField('contact', id, 'company') || 0;

        if(act == 1){
            // grant access for contact id to companyid - create modifier for contact
        }
        else if(act == 2){
            // revoke access for contact id to companyid - if just one contact for the modifier delete modifid and inactivate contact (???)
            // if more contacts for that modifier
        }
        else if(act == 3){
            // delete contact id - delete modifier and make all contacts linked to it inactive
        }
        else if(act == 4){
            // grant access for contact id (from another company) to companyid - create new contact for companyid and link it to modifid
        }
        else{
            // no other case
        }

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing users.';

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_USERS';
        obj.msg = 'No rights for users.';
    }

    return obj;
});

function get_contact (companyid,contactid,modifierid,radix,sid,id,me){

    var contact = nlapiLoadRecord('contact', id);
    var mid = parseInt(contact.getFieldValue('custentity_clgx_modifier')) || 0;

    var username = '';
    var password = '';
    var language = '';
    var terms = '';
    var attempts = 0;
    var locked = 'F';

    if(mid){

        var modifier = nlapiLoadRecord('customrecord_clgx_modifier', mid);
        username = modifier.getFieldValue('name') || '';
        if(me){
            password = modifier.getFieldValue('custrecord_clgx_modifier_password') || '';
        }
        language = parseInt(modifier.getFieldValue('custrecord_clgx_modifier_language')) || 1;
        terms = modifier.getFieldValue('custrecord_clgx_modifier_accepted_terms') || 'F';
        attempts = parseInt(modifier.getFieldValue('custrecord_clgx_modifier_login_attempts')) || 0;
        if(attempts > 5){
            locked = 'T';
        }
    }

    var rights = {};
    if(!me){
        rights = JSON.parse(contact.getFieldValue('custentity_clgx_cp_user_rights_json')) || get_min_rights();
    }

    var lastlogin = '';
    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_contact",null,"anyof",id));
    filters.push(new nlobjSearchFilter('custrecord_clgx_api_cust_portal_entity',null,'anyof',companyid));
    var logins = nlapiSearchRecord('customrecord_clgx_api_cust_portal_logins', 'customsearch_clgx_cp_sessions_users', filters);
    if(logins){
        lastlogin = logins[0].getValue('custrecord_clgx_api_cust_portal_date',null,null) || '';
    }

    var user = {
        "rid":id.toString(radix),
        "rmid":mid.toString(radix),
        "username":username,
        "password":password,
        "name":contact.getFieldValue('entityid'),
        "firstname":contact.getFieldValue('firstname'),
        "lastname":contact.getFieldValue('lastname'),
        "email":contact.getFieldValue('email'),
        "altemail":contact.getFieldValue('altemail'),
        "phone":contact.getFieldValue('phone'),
        "mobilephone":contact.getFieldValue('mobilephone'),
        "smsgate": contact.getFieldValue('custentity_email2sms_gateway') || '',
        "officephone":contact.getFieldValue('officephone'),
        "title":contact.getFieldValue('title'),
        "role1": contact.getFieldValue('contactrole') || 0,
        "role2": contact.getFieldValue('custentity_clgx_contact_secondary_role') || 0,
        "language":language,
        "pin": contact.getFieldValue('custentity_clgx_contact_pin') || '',
        "escalation": contact.getFieldValue('custentity_cologix_esc_seq') || '',
        "card": contact.getFieldValue('custentity_cologix_access_crd_num') || '',
        "securityid": contact.getFieldValue('custentity_cologix_co_security_id') || '',
        "centers": contact.getFieldValues('custentity_clgx_dc_loc') || [],
        "suites": contact.getFieldValue('custentity_clgx_contact_access_prv_suite') || 'F',
        "terms": terms,
        "attempts": attempts,
        "locked": locked,
        "lastlogin": lastlogin,
        "email_outage": contact.getFieldValue('custentity_clgx_ops_outage_notification') || 'F',
        "email_ship": contact.getFieldValue('custentity_clgx_contact_notify_ship') || 'F',
        "email_viol": contact.getFieldValue('custentity_clgx_contact_notify_violation') || 'F',
        //"sms_ship": contact.getFieldValue('custentity_clgx_contact_notify_ship_sms') || 'F',
        // "sms_incident": contact.getFieldValue('custentity_clgx_sms_incidents') || 'F',
        // "sms_maintain": contact.getFieldValue('custentity_clgx_mntce_sms') || 'F',
        // "sms_advisory": contact.getFieldValue('custentity_clgx_advisory_sms') || 'F',
        "rights": rights,
        "users":[{"value":0,"text":""}]
    };

    if(!me){

        var filters = new Array();
        filters.push(new nlobjSearchFilter("internalid",null,"noneof",id));
        filters.push(new nlobjSearchFilter("company",null,"is",companyid));
        filters.push(new nlobjSearchFilter("custentity_clgx_cp_user_rights_json",null,"isnot",''));
        var contacts = nlapiSearchRecord('contact', 'customsearch_clgx_cp_contacts', filters);

        for ( var i = 0; contacts != null && i < contacts.length; i++ ) {
            var internalid = parseInt(contacts[i].getValue('internalid',null,null));
            var obj = {
                "value": internalid,
                "text": contacts[i].getValue('entityid',null,null)
            };
            user.users.push(obj);
        }
    }
    return user;
}

function get_contact_impersonate (companyid,contactid,modifierid,radix,sid,uname) {
    var obj = {};
    var password = '';
    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_modifier_password',null,null));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("name",null,"is",uname));
    var modifer = nlapiSearchRecord('customrecord_clgx_modifier', null, arrFilters, arrColumns);
    if(modifer){
        password = modifer[0].getValue('custrecord_clgx_modifier_password',null,null) || '';
        obj = {
            "username": uname,
            "password": password
        };
    } else {
        obj = {
            "username": '',
            "password": ''
        };
    }
    return obj;
}

function update_contact(companyid,contactid,modifierid,radix,sid,id,user) {

    try {

        var modifid = nlapiLookupField('contact', id, 'custentity_clgx_modifier') || 0;
        if(!modifid && user.email){
            var language = 'en_US';
            var fields = ['firstname','lastname'];
            var columns = nlapiLookupField('contact', id, fields);
            var fname = columns.firstname || '';
            var lname = columns.lastname || '';
            modifid = clgx_cp_modifier (id,fname,lname,language);
        }

        var nameContactR= nlapiLookupField('contact', contactid, 'entityid') || '';
        var modifierName=nlapiLookupField('customrecord_clgx_modifier', modifid, 'name') || '';
        if(modifid){
            nlapiSubmitField('customrecord_clgx_modifier', modifid, 'custrecord_clgx_modifier_language', user.language);
        }

        var arrCenters = (user.centers).split(",");

        var rec = nlapiLoadRecord('contact', id);
        var centersROld= rec.getFieldValues('custentity_clgx_dc_loc') || [];
        var centersTextROld= rec.getFieldTexts('custentity_clgx_dc_loc') || "";
        rec.setFieldValue('firstname', user.firstname);
        rec.setFieldValue('lastname', user.lastname);
        rec.setFieldValue('email', user.email);
        rec.setFieldValue('altemail', user.altemail);
        rec.setFieldValue('phone', user.phone);
        rec.setFieldValue('mobilephone', user.mobilephone);
        rec.setFieldValue('custentity_email2sms_gateway', user.smsgate);
        rec.setFieldValue('officephone', user.officephone);
        rec.setFieldValue('title', user.title);
        if(user.role1 != 0){
            rec.setFieldValue('contactrole', user.role1);
        } else {
            rec.setFieldValue('contactrole', null);
        }
        if(user.role2 != 0){
            rec.setFieldValue('custentity_clgx_contact_secondary_role', user.role2);
        } else {
            rec.setFieldValue('custentity_clgx_contact_secondary_role', null);
        }
        nlapiLogExecution('DEBUG','SMSgate', user.smsgate);
        rec.setFieldValues('custentity_clgx_dc_loc', arrCenters);
        rec.setFieldValue('custentity_clgx_contact_pin', user.pin);
        rec.setFieldValue('custentity_cologix_esc_seq', user.escalation);
        rec.setFieldValue('custentity_cologix_co_security_id', user.securityid);
        rec.setFieldValue('custentity_cologix_access_crd_num', user.card);
        rec.setFieldValue('custentity_clgx_contact_access_prv_suite', user.suites);
        rec.setFieldValue('custentity_clgx_ops_outage_notification', user.email_outage);
        rec.setFieldValue('custentity_clgx_contact_notify_ship', user.email_ship);
        rec.setFieldValue('custentity_clgx_contact_notify_violation', user.email_viol);
        //rec.setFieldValue('custentity_clgx_contact_notify_ship_sms', user.sms_ship);
        //rec.setFieldValue('custentity_clgx_sms_incidents', user.sms_incident);
        // rec.setFieldValue('custentity_clgx_mntce_sms', user.sms_maintain);
        // rec.setFieldValue('custentity_clgx_advisory_sms', user.sms_advisory);
        recid = nlapiSubmitRecord(rec, false,true);
        var contactRecord = nlapiLoadRecord('contact', recid);
        var centersR= contactRecord.getFieldValues('custentity_clgx_dc_loc') || [];
        var centersTextR= contactRecord.getFieldTexts('custentity_clgx_dc_loc') || "";
        var emailR= contactRecord.getFieldValue('email');
        var nameconR=contactRecord.getFieldValue('entityid') || '';
        //create a case for each inactivated contact
        // if(centersR.length>0) {
        var emptyArr=[];
        /* var x=[];
         for (var r = 0; centersTextROld != null && r < centersTextROld.length; r++) {
         x.push(centersTextROld[r]);
         }
         var y=[];
         for (var r = 0; centersTextR != null && r < centersTextR.length; r++) {
         y.push(centersTextR[r]);
         }
         var centersR1=_.difference(x, y);
         nlapiLogExecution('DEBUG', 'centersR1', centersR1);
         centersTextROld=x;
         var centersTextR1=y;
         if(centersR1.length>0) {
         centersTextR1=_.difference(centersTextROld, centersTextR1);
         var recordCase = nlapiCreateRecord('supportcase');
         recordCase.setFieldValue('title', 'Data Center access has been revoked for ' + nameconR);
         recordCase.setFieldValue('company', companyid);
         // record.setFieldValue('contact', contactid);
         recordCase.setFieldValue('email', emailR);
         var facilityStrR = centersTextR1[0];
         var facArrayR = facilityStrR.split(':');
         var facilityR = facArrayR[1].replace(/ /g, '');
         recordCase.setFieldText('custevent_cologix_facility', facilityR);
         recordCase.setFieldValue('category', 1);
         recordCase.setFieldValue('custevent_cologix_sub_case_type', 77);
         recordCase.setFieldValue('assigned', 379486);
         recordCase.setFieldValue('status', 6);
         recordCase.setFieldText('origin', 'Web');
         recordCase.setFieldValue('messagenew', 'T');
         var centersStringR = centersTextR1[0];
         for (var r = 1; centersTextR1 != null && r < centersTextR1.length; r++) {
         centersStringR += '; ' + centersTextR1[r];
         }
         var messageR = modifierName + ' has been edited through the customer portal.  Please have their Data Center Access revoked for the following facilities:' + centersStringR+'\nThis change has been requested by '+nameContactR;
         recordCase.setFieldValue('incomingmessage', messageR);

         try {
         var recordId = nlapiSubmitRecord(recordCase, true, true);
         }
         catch (error) {
         nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
         }
         }
         var centersTextR=y;
         centersR=_.difference(centersTextR, centersTextROld);
         nlapiLogExecution('DEBUG', 'centersTextR', centersTextR);
         nlapiLogExecution('DEBUG', 'centersTextROld', centersTextROld);
         nlapiLogExecution('DEBUG', 'centersR', centersR);
         if(centersR.length>0) {
         centersTextR=_.difference(centersTextR, centersTextROld);
         var recordCase = nlapiCreateRecord('supportcase');
         recordCase.setFieldValue('title', 'Data Center access has been granted for ' + nameconR);
         recordCase.setFieldValue('company', companyid);
         // record.setFieldValue('contact', contactid);
         recordCase.setFieldValue('email', emailR);
         var facilityStrR = centersTextR[0];
         var facArrayR = facilityStrR.split(':');
         var facilityR = facArrayR[1].replace(/ /g, '');
         recordCase.setFieldText('custevent_cologix_facility', facilityR);
         recordCase.setFieldValue('category', 1);
         recordCase.setFieldValue('custevent_cologix_sub_case_type', 77);
         recordCase.setFieldValue('assigned', 379486);
         recordCase.setFieldValue('status', 6);
         recordCase.setFieldText('origin', 'Web');
         recordCase.setFieldValue('messagenew', 'T');
         var centersStringR = centersTextR[0];
         for (var r = 1; centersTextR != null && r < centersTextR.length; r++) {
         centersStringR += '; ' + centersTextR[r];
         }
         var messageR = modifierName + ' has been edited through the customer portal.  Please have their Data Center Access activated for the following facilities:' + centersStringR+'\nThis change has been requested by '+nameContactR;
         recordCase.setFieldValue('incomingmessage', messageR);

         try {
         var recordId = nlapiSubmitRecord(recordCase, true, true);
         }
         catch (error) {
         nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
         }
         }
         // }
         */
        if(modifid){
            if(user.locked == 'T'){
                nlapiSubmitField('customrecord_clgx_modifier', modifid, 'custrecord_clgx_modifier_login_attempts', 6);
            } else {
                nlapiSubmitField('customrecord_clgx_modifier', modifid, 'custrecord_clgx_modifier_login_attempts', 0);
            }
        }
        return 6;
    }
    catch (error) {

        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
        }

        return 7;
    }
}

function create_contact(companyid,contactid,modifierid,radix,sid,id,user,copy) {
    try {
        var arrCenters = (user.centers).split(",");
        var nameContactR= nlapiLookupField('contact', contactid, 'entityid') || '';
        var contact = nlapiCreateRecord('contact');
        contact.setFieldValue('company', companyid);
        contact.setFieldValue('firstname', user.firstname);
        contact.setFieldValue('lastname', user.lastname);
        contact.setFieldValue('email', user.email);
        contact.setFieldValue('altemail', user.altemail);
        contact.setFieldValue('phone', user.phone);
        contact.setFieldValue('mobilephone', user.mobilephone);
        contact.setFieldValue('custentity_email2sms_gateway', user.smsgate);
        contact.setFieldValue('officephone', user.officephone);
        contact.setFieldValue('title', user.title);
        if(user.role1 != 0){
            contact.setFieldValue('contactrole', user.role1);
        } else {
            contact.setFieldValue('contactrole', null);
        }
        if(user.role2 != 0){
            contact.setFieldValue('custentity_clgx_contact_secondary_role', user.role2);
        } else {
            contact.setFieldValue('custentity_clgx_contact_secondary_role', null);
        }
        contact.setFieldValues('custentity_clgx_dc_loc', arrCenters);
        contact.setFieldValue('custentity_clgx_contact_pin', user.pin);
        contact.setFieldValue('custentity_cologix_esc_seq', user.escalation);
        contact.setFieldValue('custentity_cologix_co_security_id', user.securityid);
        contact.setFieldValue('custentity_cologix_access_crd_num', user.card);
        contact.setFieldValue('custentity_clgx_contact_access_prv_suite', user.suites);
        contact.setFieldValue('custentity_clgx_ops_outage_notification', user.email_outage);
        contact.setFieldValue('custentity_clgx_contact_notify_ship', user.email_ship);
        contact.setFieldValue('custentity_clgx_contact_notify_violation', user.email_viol);
        //contact.setFieldValue('custentity_clgx_contact_notify_ship_sms', user.sms_ship);
        //  contact.setFieldValue('custentity_clgx_sms_incidents', user.sms_incident);
        //  contact.setFieldValue('custentity_clgx_mntce_sms', user.sms_maintain);
        //  contact.setFieldValue('custentity_clgx_advisory_sms', user.sms_advisory);

        if(copy > 0){

            var crights = nlapiLookupField('contact', copy, 'custentity_clgx_cp_user_rights_json');
            var rights = JSON.parse(crights);
            if(parseInt(rights.users) == 4){
                contact.setFieldValue('custentity_clgx_access_sig', 'T');
                contact.setFieldValue('custentity_clgx_contact_is_portal_admin', 'Yes');
            } else {
                contact.setFieldValue('custentity_clgx_access_sig', 'F');
                contact.setFieldValue('custentity_clgx_contact_is_portal_admin', 'No');
            }
            if(parseInt(rights.cases) == 4){
                contact.setFieldValue('custentity_clgx_authorized_req_rh', 'T');
            } else {
                contact.setFieldValue('custentity_clgx_authorized_req_rh', 'F');
            }
            contact.setFieldValue('custentity_clgx_cp_user_rights_json', JSON.stringify(rights));
        } else {
            contact.setFieldValue('custentity_clgx_cp_user_rights_json', JSON.stringify(get_min_rights()));
        }

        var recid = nlapiSubmitRecord(contact, false,true);
        var rid = (parseInt(recid)).toString(radix);

        var uname = ((user.firstname).replace(/ /g,'')).charAt(0) + (user.lastname).replace(/ /g,'') + recid;

        var modifer = nlapiCreateRecord('customrecord_clgx_modifier');
        modifer.setFieldValue('name', uname);
        var password=generate_password(15);
        modifer.setFieldValue('custrecord_clgx_modifier_password', password);
        modifer.setFieldValue('custrecord_clgx_modifier_language', 1);
        var modiferid = nlapiSubmitRecord(modifer, false,true);

        var rec = nlapiLoadRecord('contact', recid);
        var centersR= rec.getFieldValues('custentity_clgx_dc_loc') || [];
        var centersTextR= rec.getFieldTexts('custentity_clgx_dc_loc') || "";
        var nameconR=rec.getFieldValue('entityid') || '';
        /*  if(centersR.length>0) {
         var recordCase = nlapiCreateRecord('supportcase');
         recordCase.setFieldValue('title', 'User account created in Portal for ' + nameconR);
         recordCase.setFieldValue('company', companyid);
         // record.setFieldValue('contact', contactid);
         recordCase.setFieldValue('email', user.email);
         var facilityStrR = centersTextR[0];
         var facArrayR = facilityStrR.split(':');
         var facilityR = facArrayR[1].replace(/ /g, '');
         recordCase.setFieldText('custevent_cologix_facility', facilityR);
         recordCase.setFieldValue('category', 1);
         recordCase.setFieldValue('custevent_cologix_sub_case_type', 77);
         recordCase.setFieldValue('assigned', 379486);
         recordCase.setFieldValue('status', 6);
         recordCase.setFieldText('origin', 'Web');
         recordCase.setFieldValue('messagenew', 'T');
         var centersStringR = centersTextR[0];
         for (var r = 1; centersTextR != null && r < centersTextR.length; r++) {
         centersStringR += '; ' + centersTextR[r];
         }
         var messageR = uname+ ' has been created through the customer portal.  Please have their Data Center Access activated for the following facilities:' + centersStringR+'\nThis change has been requested by '+nameContactR;
         recordCase.setFieldValue('incomingmessage', messageR);

         try {
         var recordId = nlapiSubmitRecord(recordCase, true, true);
         }
         catch (error) {
         nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
         }
         }*/

        //send email to the user
        var lan = user.language || 0;
        var objFile = nlapiLoadFile(3777744);
        var emailUserSubject = 'Cologix Portal - An account has been created for you';
        var emailUserSubject1 = 'Cologix Notification';
        var objFile1 = nlapiLoadFile(3830853);
        if (lan == 17) {
            var objFile = nlapiLoadFile(3830849);
            var emailUserSubject = 'Portail Cologix - Un compte a été créé pour vous';
            var emailUserSubject1 = 'Cologix Notification';
            var objFile1 = nlapiLoadFile(3830854);
        }


        var emailUserBody = objFile.getValue();
        emailUserBody = emailUserBody.replace(new RegExp('{fname}','g'), user.firstname);
        emailUserBody = emailUserBody.replace(new RegExp('{lname}','g'), user.lastname);
        emailUserBody = emailUserBody.replace(new RegExp('{username}','g'), uname);
        var fromid = 432742;
        nlapiSendEmail(fromid, recid, emailUserSubject, emailUserBody, null, null, null, null,true);

        //temporary password

        var emailUserBody1 = objFile1.getValue();
        emailUserBody1 = emailUserBody1.replace(new RegExp('{fname}','g'), user.firstname);
        emailUserBody1 = emailUserBody1.replace(new RegExp('{lname}','g'), user.lastname);
        emailUserBody1 = emailUserBody1.replace(new RegExp('{password}','g'), password);
        //  nlapiSendEmail(fromid, recid, emailUserSubject1, emailUserBody1, null, null, null, null,true);

        nlapiSubmitField('contact', recid, 'custentity_clgx_modifier', modiferid);

        return {
            "id": recid,
            "rid": rid,
            "uname": uname,
            "name": user.firstname + user.lastname
        };
    }
    catch (error) {
        nlapiLogExecution('DEBUG', 'Process Error', error.getCode() + ': ' + error.getDetails());

        return {
            "id": 0,
            "rid": '0',
            "uname": '',
            "name": ''
        };
    }
}

function update_password(companyid,contactid,modifierid,radix,sid,id,passwd) {
    try {
        //nlapiLogExecution('DEBUG','debug', '| passwd = ' + passwd + ' | ');

        var redate = moment().format('M/D/YYYY');
        var fields = ['custrecord_clgx_modifier_password','custrecord_clgx_modifier_password_date','custrecord_clgx_modifier_password_redate'];
        var values = [passwd,redate,null];
        nlapiSubmitField('customrecord_clgx_modifier', modifierid, fields, values);

        return 1;
    }
    catch (error) {
        return 2;
    }
}
function update_user_name(companyid,contactid,modifierid,radix,sid,id,uname) {
    try {

        var filters = new Array();
        filters.push(new nlobjSearchFilter("name",null,"is",uname));
        var search = nlapiSearchRecord('customrecord_clgx_modifier', null, filters);
        if(search){
            return 4;

        } else {
            nlapiSubmitField('customrecord_clgx_modifier', modifierid, 'name', uname);
            return 3;
        }
    }
    catch (error) {
        return 5;
    }
}

function rights_contact(companyid,contactid,modifierid,radix,sid,id,rights,copy) {
    try {

        if(copy > 0){

            var crights = nlapiLookupField('contact', copy, 'custentity_clgx_cp_user_rights_json');
            var rights = JSON.parse(crights);

            if(parseInt(rights.users) == 4){
                var sig = 'T';
                var adm = 'Yes';
            } else {
                var sig = 'F';
                var adm = 'No';
            }
            if(parseInt(rights.cases) == 4){
                var rh = 'T';
            } else {
                var rh = 'F';
            }
            var fields = ['custentity_clgx_contact_is_portal_admin','custentity_clgx_access_sig','custentity_clgx_authorized_req_rh','custentity_clgx_cp_user_rights_json'];
            var values = [adm, sig, rh, JSON.stringify( rights ) ];
            nlapiSubmitField('contact', id, fields, values);

        } else {

            if(rights.users == 4){
                var sig = 'T';
                var adm = 'Yes';
            } else {
                var sig = 'F';
                var adm = 'No';
            }
            if(rights.cases == 4){
                var rh = 'T';
            } else {
                var rh = 'F';
            }
            var fields = ['custentity_clgx_contact_is_portal_admin','custentity_clgx_access_sig','custentity_clgx_authorized_req_rh','custentity_clgx_cp_user_rights_json'];
            var values = [adm, sig, rh, JSON.stringify( rights ) ];
            nlapiSubmitField('contact', id, fields, values);

        }
        return 'success';
    }
    catch (error) {
        return 'error';
    }
}

function get_contacts (companyid){
    var users = [{"value":0,"text":""}];
    var filters = [];
    filters.push(new nlobjSearchFilter("company",null,"is",companyid));
    filters.push(new nlobjSearchFilter("custentity_clgx_cp_user_rights_json",null,"isnot",''));
    var searchUsers = nlapiSearchRecord('contact', 'customsearch_clgx_cp_contacts', filters);
    for ( var i = 0; searchUsers != null && i < searchUsers.length; i++ ) {
        var user = {
            "value": parseInt(searchUsers[i].getValue('internalid',null,null)),
            "text": searchUsers[i].getValue('entityid',null,null)
        };
        users.push(user);
    }
    return users;
}


function get_min_rights() {
    return {
        "users":0,
        "security":0,
        "cases":0,
        "casesfin":0,
        "reports":0,
        "visits":0,
        "shipments":0,
        "orders":0,
        "invoices":0,
        "ccards":0,
        "colocation":0,
        "network":0,
        "domains":0,
        "managed":0,
        "shipments":0
    };
}



function generate_password(length, special) {
    var iteration = 0;
    var password = "";
    var randomNumber;
    if(special == undefined){
        var special = false;
    }
    while(iteration < length){
        randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
        if(!special){
            if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
            if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
            if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
            if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
        }
        iteration++;
        password += String.fromCharCode(randomNumber);
    }
    return password;
}


function clgx_cp_modifier (contactId,fname,ltname,language) {
    if (language == 'en_US') {
        var lan = 1;
    }
    if (language == 'fr_CA') {
        var lan = 17;
    }
    var modifierID=0;
    var nameModifier = fname.charAt(0).toLowerCase() + (ltname.replace(/\s+/g, '')).toLowerCase();
    nameModifier = nameModifier.replace(/\W+/g, '');
    nameModifier = nameModifier.replace(/[0-9]/g, '');
    if (nameModifier.length > 1) {
        var rec = nlapiCreateRecord('customrecord_clgx_modifier');
        rec.setFieldValue('name', nameModifier);
        rec.setFieldValue('custrecord_clgx_modifier_language', lan);
        var id = nlapiSubmitRecord(rec, true);

        var recordModifier = nlapiLoadRecord('customrecord_clgx_modifier', id);
        var nameMod = recordModifier.getFieldValue('name');
        recordModifier.setFieldValue('name', nameMod + id);
        nlapiSubmitRecord(recordModifier, false, true);

        var recordcontact = nlapiLoadRecord('contact', contactId);
        recordcontact.setFieldValue('custentity_clgx_modifier', id);
        nlapiSubmitRecord(recordcontact, false, false);

        modifierID=id;
    }
    return modifierID;
}