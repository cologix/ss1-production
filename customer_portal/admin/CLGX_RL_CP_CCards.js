nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_CCards.js
//	ScriptID:	customscript_clgx_rl_cp_ccards
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=705&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	09/29/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    var direction = datain['direction'] || 'ASC';
    var order_by = datain['order_by'] || '';
    var page = datain['page'] || 1;
    var per_page = datain['per_page'];

    obj["ccards"] = get_ccards(companyid,contactid,modifierid,radix,sid,direction,order_by,page,per_page,srights.users);

    var now = moment().format("M/D/YYYY h:mm:ss a");
    nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

    obj.code = 'SUCCESS';
    obj.msg = 'You are managing credit cards.';

    return obj;
});

function get_ccards(companyid,contactid,modifierid,radix,sid,direction,order_by,page,per_page,rights,csv) {

    var obj = {};
    var arr = [];
    var total = 0;
    var pages = 0;
    var start = 0;
    var end = 0;
    var has_more = false;

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter('custrecord_token_customer',null,'anyof',companyid));
    var records = nlapiSearchRecord('customrecord_clgx_credit_cards', 'customsearch_clgx_cp_credit_cards', filters, columns);

    if(records){

        total = records.length;
        pages = Math.ceil(total / per_page);
        page = (parseInt(page) > pages ? pages : parseInt(page));
        has_more = (pages > 1 ? true : false);
        start = (page - 1) * per_page;
        end = (start + per_page > total ? total : start + per_page);

        for ( var i = start; i < end; i++ ) {
            var rec = {};
            var id = parseInt(records[i].getValue('internalid',null,null));
            rec["id"] = id;
            rec["rid"] = id.toString(radix);
            rec["token"] = records[i].getValue('custrecord_token',null,null);
            rec["last4"] = records[i].getValue('custrecord_card_last_four',null,null);
            rec["expire"] = records[i].getValue('custrecord_card_exp_month',null,null) + '/' + records[i].getValue('custrecord_card_exp_year',null,null);
            rec["cctype"] = records[i].getValue('custrecord_card_type',null,null);
            rec["default"] = records[i].getValue('custrecord_card_isdefault',null,null);
            arr.push(rec);
        }
    }
    obj["data"] = arr;
    obj["direction"] = direction;
    obj["order_by"] = order_by;
    obj["page"] = page;
    obj["pages"] = pages;
    obj["per_page"] = per_page;
    obj["has_more"] = has_more;
    obj["start"] = start;
    obj["end"] = end;
    obj["total"] = total;

    return obj;
}
