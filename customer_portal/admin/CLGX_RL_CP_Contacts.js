nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Contacts.js
//	ScriptID:	customscript_clgx_rl_cp_contacts
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=661&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/17/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv,others) {

    //if(srights.users > 0){

    var direction = datain['direction'] || 'ASC';
    var order_by = datain['order_by'] || '';
    var page = datain['page'] || 1;
    var per_page = datain['per_page'];
    var search = datain['search'] || '';
    var others = datain['others'] || 0;
    var csv = datain['csv'] || 0;

    obj["users"] = get_contacts(companyid,contactid,srights,modifierid,radix,direction,order_by,page,per_page,search,sid,csv);
    obj["billing_contacts"] = get_billing_contacts(companyid);

    var now = moment().format("M/D/YYYY h:mm:ss a");
    nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

    obj.code = 'SUCCESS';
    obj.msg = 'You are managing users.';
    /*
     } else {
     obj.error = 'T';
     obj.code = 'NO_RIGHTS_USERS';
     obj.msg = 'No rights for users.';
     }
     */
    return obj;
});

var put = wrap(function put(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv,others) {

    if(srights.users > 0){

        var act = datain['act'] || 0;
        var rid = datain['rid'] || '0';
        var id = parseInt(rid, radix);

        nlapiLogExecution('DEBUG','act',act);
        // get modifier and header company of contact id
        var modifid = nlapiLookupField('contact', id, 'custentity_clgx_modifier') || 0;
        var compid = nlapiLookupField('contact', id, 'company') || 0;
        var compName = nlapiLookupField('contact', id, 'company',true) || '';
        var fname=nlapiLookupField('contact', id, 'firstname') || '';
        var lname=nlapiLookupField('contact', id, 'lastname') || '';
        var namecon=nlapiLookupField('contact', id, 'entityid') || '';
        var emailcon=nlapiLookupField('contact', id, 'email') || '';
        var nameContactR= nlapiLookupField('contact', contactid, 'entityid') || '';

        if(compid!=0) {
            var cust=nlapiLoadRecord('customer',compid);
            var lan = cust.getFieldText('language') || '';
            if (lan == 'US English') {
                var language = 1;
            }
            if (lan == 'French (Canada)') {
                var language = 17;
            }
        }

        if(act == 1){
            // grant access for contact id to companyid - create modifier for contact
            if((fname!='')&&(lname!='')) {
                var nameModifier = fname.charAt(0).toLowerCase() + (lname.replace(/\s+/g, '')).toLowerCase();
                nameModifier = nameModifier.replace(/\W+/g, '');
                nameModifier = nameModifier.replace(/[0-9]/g, '');
                var password = randomPassword(12);
                var rec = nlapiCreateRecord('customrecord_clgx_modifier');
                rec.setFieldValue('custrecord_clgx_modifier_password', password);
                rec.setFieldValue('name', nameModifier);
                if(language!='' ) {
                    rec.setFieldValue('custrecord_clgx_modifier_language', language);
                }
                var idMOD = nlapiSubmitRecord(rec, true);


                var recordModifier=nlapiLoadRecord('customrecord_clgx_modifier',idMOD);
                var nameMod = recordModifier.getFieldValue('name');
                recordModifier.setFieldValue('name', nameMod+idMOD);
                nlapiSubmitRecord(recordModifier,false, true);

                var recordcontact=nlapiLoadRecord('contact',id);
                recordcontact.setFieldValue('isinactive', 'F');
                recordcontact.setFieldValue('custentity_clgx_modifier', idMOD);
                nlapiSubmitRecord(recordcontact,false, false);
                if(emailcon!='')
                {
                    var emailUserSubject = 'Cologix Portal - An account has been created for you';
                    var objFile = nlapiLoadFile(3777744);
                    var emailUserSubject1 = 'Cologix Portal - Temporary Password';
                    var objFile1 = nlapiLoadFile(3830853);
                    if (lan == 'French (Canada)') {
                        var objFile = nlapiLoadFile(3830849);
                        var emailUserSubject = 'Portail Cologix - Un compte a été créé pour vous';
                        var emailUserSubject1 = 'Portail Cologix - Mot de Passe Temporaire';
                        var objFile1 = nlapiLoadFile(3830854);
                    }
                    var emailUserBody = objFile.getValue();
                    emailUserBody = emailUserBody.replace(new RegExp('{fname}','g'), fname);
                    emailUserBody = emailUserBody.replace(new RegExp('{lname}','g'), lname);
                    emailUserBody = emailUserBody.replace(new RegExp('{username}','g'), nameMod+idMOD);


                    var fromid = 432742;
                    nlapiSendEmail(fromid, id, emailUserSubject, emailUserBody, null, null, null, null,true);
                    //temporary password

                    var emailUserBody1 = objFile1.getValue();
                    emailUserBody1 = emailUserBody1.replace(new RegExp('{fname}','g'), fname);
                    emailUserBody1 = emailUserBody1.replace(new RegExp('{lname}','g'), lname);
                    emailUserBody1 = emailUserBody1.replace(new RegExp('{password}','g'), password);
                    //  nlapiSendEmail(fromid, id, emailUserSubject1, emailUserBody1, null, null, null, null,true);

                }
            }
        }
        else if(act == 2){
            // revoke access for contact id to companyid - if just one contact for the modifier delete modifid and inactivate contact (???)
            // if more contacts for that modifier
            var arrFilters = new Array();
            var arrColums = new Array();
            var contactIDs= new Array();
            //contactIDs.push(id);
            if(modifid!=0) {
                arrColums[0] = new nlobjSearchColumn('internalid', null, null);
                arrFilters.push(new nlobjSearchFilter("custentity_clgx_modifier", null, "anyof", modifid));
                var searchModifiers = nlapiSearchRecord('contact', null, arrFilters, arrColums);
                var modRecord = nlapiLoadRecord('customrecord_clgx_modifier', modifid);
                var modifierName = modRecord.getFieldValue('name');
                if (searchModifiers.length == 1) {
                    for (var i = 0; searchModifiers != null && i < searchModifiers.length; i++) {
                        var searchModifier = searchModifiers[i];
                        contactIDs.push(searchModifier.getValue('internalid', null));
                    }
                    for (var i = 0; contactIDs != null && i < contactIDs.length; i++) {
                        var contactRecord = nlapiLoadRecord('contact', contactIDs[i]);
                        //  contactRecord.setFieldValue('custentity_clgx_modifier', null);
                        var centersR = contactRecord.getFieldValues('custentity_clgx_dc_loc') || [];
                        var centersTextR = contactRecord.getFieldTexts('custentity_clgx_dc_loc') || "";
                        var emailR = contactRecord.getFieldValue('email');
                        var nameconR = contactRecord.getFieldValue('entityid') || '';
                        //create a case for each inactivated contact
                        if (centersR.length > 0) {
                            var recordCase = nlapiCreateRecord('supportcase');
                            recordCase.setFieldValue('title', 'User account inactivated in Portal for ' + nameconR);
                            recordCase.setFieldValue('company', companyid);
                            // record.setFieldValue('contact', contactid);
                            recordCase.setFieldValue('email', emailR);
                            var facilityStrR = centersTextR[0];
                            var facArrayR = facilityStrR.split(':');
                            var facilityR = facArrayR[1].replace(/ /g, '');
                            var facCase = clgx_return_facilityid(centersR[0]);
                            recordCase.setFieldText('custevent_cologix_facility', facCase);
                            recordCase.setFieldValue('category', 1);
                            recordCase.setFieldValue('custevent_cologix_sub_case_type', 77);
                            recordCase.setFieldValue('assigned', 946879);
                            recordCase.setFieldValue('status', 6);
                            recordCase.setFieldText('origin', 'Web');
                            recordCase.setFieldValue('messagenew', 'T');
                            var centersStringR = centersTextR[0];
                            for (var r = 1; centersTextR != null && r < centersTextR.length; r++) {
                                centersStringR += '; ' + centersTextR[r];
                            }
                            var messageR = modifierName + ' has been inactivated through the customer portal.  Please have their Data Center Access removed for the following facilities:' + centersStringR + '\nThis change has been requested by ' + nameContactR;
                            recordCase.setFieldValue('incomingmessage', messageR);

                            try {
                                var recordId = nlapiSubmitRecord(recordCase, true, true);
                            } catch (error) {
                                nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
                            }
                        }
                        contactRecord.setFieldValue('isinactive', 'T');
                        nlapiSubmitRecord(contactRecord, true, true);
                    }
                    // nlapiDeleteRecord('customrecord_clgx_modifier', modifid);

                    modRecord.setFieldValue('isinactive', 'T');
                    nlapiSubmitRecord(modRecord, true, true);
                }
            }
            else
            {
                nlapiLogExecution('DEBUG','contact2', id);

                var contactRecord = nlapiLoadRecord('contact', id);
                var centersR= contactRecord.getFieldValues('custentity_clgx_dc_loc') || [];
                var centersTextR= contactRecord.getFieldTexts('custentity_clgx_dc_loc') || "";
                var emailR= contactRecord.getFieldValue('email');
                var nameconR=contactRecord.getFieldValue('entityid') || '';
                //create a case for each inactivated contact
                if(centersR.length>0) {
                    var recordCase = nlapiCreateRecord('supportcase');
                    recordCase.setFieldValue('title', 'User account inactivated in Portal for ' + nameconR);
                    recordCase.setFieldValue('company', companyid);
                    // record.setFieldValue('contact', contactid);
                    recordCase.setFieldValue('email', emailR);
                    var facilityStrR = centersTextR[0];
                    var facArrayR = facilityStrR.split(':');
                    var facilityR = facArrayR[1].replace(/ /g, '');
                    var facCase=clgx_return_facilityid(centersR[0]);
                    recordCase.setFieldText('custevent_cologix_facility', facCase);
                    recordCase.setFieldValue('category', 1);
                    recordCase.setFieldValue('custevent_cologix_sub_case_type', 77);
                    recordCase.setFieldValue('assigned', 946879);
                    recordCase.setFieldValue('status', 6);
                    recordCase.setFieldText('origin', 'Web');
                    recordCase.setFieldValue('messagenew', 'T');
                    var centersStringR = centersTextR[0];
                    for (var r = 1; centersTextR != null && r < centersTextR.length; r++) {
                        centersStringR += '; ' + centersTextR[r];
                    }
                    var messageR = modifierName + ' has been inactivated through the customer portal.  Please have their Data Center Access removed for the following facilities:' + centersStringR+'\nThis change has been requested by '+nameContactR;
                    recordCase.setFieldValue('incomingmessage', messageR);

                    try {
                        var recordId = nlapiSubmitRecord(recordCase, true, true);
                    }
                    catch (error) {
                        nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
                    }
                }
                //contactRecord.setFieldValue('custentity_clgx_modifier', null);
                contactRecord.setFieldValue('isinactive', 'T');
                nlapiSubmitRecord(contactRecord, true, true);

                var arrFilters = new Array();
                var arrColums = new Array();
                var foundNO='F';
                arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",modifid));
                var searchModifiers = nlapiSearchRecord('customrecord_clgx_modifier','customsearch3391', arrFilters,arrColums);

                if(searchModifiers!=null) {
                    for (var i = 0; searchModifiers != null && i < searchModifiers.length; i++) {
                        var searchModifier = searchModifiers[i];
                        var columns = searchModifier.getAllColumns();
                        var inactive=searchModifier.getValue(columns[0]);
                        if(inactive=='F')
                        {
                            var foundNO='T';
                        }
                    }
                }
                if(foundNO=='F' && modifid!=0){
                    var modRecord = nlapiLoadRecord('customrecord_clgx_modifier', modifid);
                    modRecord.setFieldValue('isinactive', 'T');
                    nlapiSubmitRecord(modRecord, true, true);
                }
            }

        }
        else if(act == 3){
            // delete contact id - delete modifier and make all contacts linked to it inactive
            var arrFilters = new Array();
            var arrColums = new Array();
            var contactIDs= new Array();
            nlapiLogExecution('DEBUG','id55555',id);
            contactIDs.push(id);
            var contactRec=nlapiLoadRecord('contact',id);
            // nlapiLogExecution('DEBUG','namecon', namecon+emailcon);
            if(modifid!=0) {
                var modRecord = nlapiLoadRecord('customrecord_clgx_modifier', modifid);
                var modifierName = modRecord.getFieldValue('name');
                arrColums[0] = new nlobjSearchColumn('internalid', null, null);
                arrFilters.push(new nlobjSearchFilter("custentity_clgx_modifier", null, "anyof", modifid));
                var searchModifiers = nlapiSearchRecord('contact', null, arrFilters, arrColums);
                for (var i = 0; searchModifiers != null && i < searchModifiers.length; i++) {
                    var searchModifier = searchModifiers[i];
                    contactIDs.push(searchModifier.getValue('internalid', null));
                }
            }
            for ( var i = 0; contactIDs != null && i < contactIDs.length; i++ ) {
                nlapiLogExecution('DEBUG','id',contactIDs[i]);
                var contactRecord=nlapiLoadRecord('contact',contactIDs[i]);
                //contactRecord.setFieldValue('custentity_clgx_modifier',null);
                var centersR= contactRecord.getFieldValues('custentity_clgx_dc_loc') || [];
                var centersTextR= contactRecord.getFieldTexts('custentity_clgx_dc_loc') || "";
                var emailR= contactRecord.getFieldValue('email');
                var nameconR=contactRecord.getFieldValue('entityid') || '';
                //create a case for each inactivated contact
                if(centersR.length>0) {
                    var recordCase = nlapiCreateRecord('supportcase');
                    recordCase.setFieldValue('title', 'User account inactivated in Portal for ' + nameconR);
                    recordCase.setFieldValue('company', companyid);
                    // record.setFieldValue('contact', contactid);
                    recordCase.setFieldValue('email', emailR);
                    var facilityStrR = centersTextR[0];
                    var facArrayR = facilityStrR.split(':');
                    var facilityR = facArrayR[1].replace(/ /g, '');
                    nlapiLogExecution('DEBUG', 'Location', centersR[0]);

                    var facCase=clgx_return_facilityid(centersR[0]);
                    nlapiLogExecution('DEBUG', 'Facility', facCase);
                    recordCase.setFieldText('custevent_cologix_facility', facCase);
                    recordCase.setFieldValue('category', 1);
                    recordCase.setFieldValue('custevent_cologix_sub_case_type', 77);
                    recordCase.setFieldValue('assigned', 946879);
                    recordCase.setFieldValue('status', 6);
                    recordCase.setFieldText('origin', 'Web');
                    recordCase.setFieldValue('messagenew', 'T');
                    var centersStringR = centersTextR[0];
                    for (var r = 1; centersTextR != null && r < centersTextR.length; r++) {
                        centersStringR += '; ' + centersTextR[r];
                    }
                    var messageR = modifierName + ' has been inactivated through the customer portal.  Please have their Data Center Access removed for the following facilities:' + centersStringR+'\nThis change has been requested by '+nameContactR;
                    recordCase.setFieldValue('incomingmessage', messageR);

                    try {
                        var recordId = nlapiSubmitRecord(recordCase, true, true);
                    }
                    catch (error) {
                        nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
                    }
                }
                contactRecord.setFieldValue('isinactive','T');
                nlapiSubmitRecord(contactRecord,true,true);
            }
            // nlapiDeleteRecord('customrecord_clgx_modifier',modifid);
            if(modifid!=0) {
                modRecord.setFieldValue('isinactive', 'T');
                nlapiSubmitRecord(modRecord, true, true);
            }


        }
        else if(act == 4){
            // grant access for contact id (from another company) to companyid - create new contact for companyid and link it to modifid
            // Copy Record

            var arrFilters = new Array();
            var arrColums = new Array();

            arrFilters.push(new nlobjSearchFilter("entityid",null,"is",namecon));
            arrFilters.push(new nlobjSearchFilter("company",null,"anyof",companyid));
            arrColums[0] = new nlobjSearchColumn('internalid', null, null);
            arrColums[1] = new nlobjSearchColumn('custentity_clgx_modifier', null, null);
            var searchContacts = nlapiSearchRecord('contact',null, arrFilters,arrColums);

            if(searchContacts!=null){

                for ( var i = 0; searchContacts != null && i < searchContacts.length; i++ ) {
                    var searchContact = searchContacts[i];
                    var contactID=searchContact.getValue('internalid', null);
                    var modID=searchContact.getValue('custentity_clgx_modifier', null);
                }
                var recordModifier=nlapiLoadRecord('customrecord_clgx_modifier',modID);
                recordModifier.setFieldValue('isinactive', 'F');
                nlapiSubmitRecord(recordModifier,false, true);
                var recordcontact=nlapiLoadRecord('contact',contactID);
                recordcontact.setFieldValue('isinactive', 'F');
                nlapiSubmitRecord(recordcontact,false, false);

                var emailUserSubject = 'Cologix Portal - You have been granted access to a new Company';
                var objFile = nlapiLoadFile(3777949);
                var language = nlapiLookupField('customer', companyid, 'language') || '';
                if(language ==17){
                    var emailUserSubject = 'Portail Cologix - Vous avez accès à une nouvelle compagnie';
                    var objFile = nlapiLoadFile(3830851);
                }
                if(emailcon!='')
                {

                    var emailUserBody = objFile.getValue();
                    emailUserBody =emailUserBody.replace(new RegExp('{fname}','g'), fname);
                    emailUserBody =emailUserBody.replace(new RegExp('{lname}','g'), lname);
                    emailUserBody = emailUserBody.replace(new RegExp('{companyname}','g'), compName);
                    var fromid = 432742;
                    nlapiSendEmail(fromid, id, emailUserSubject, emailUserBody, null, null, null, null,true);
                }

            }else {
                var rec = nlapiCopyRecord('contact', id);
                rec.setFieldValue('company', companyid);
                rec.setFieldValue('companyid', companyid);
                var nameorig = rec.getFieldValue('nameorig');
                rec.setFieldValue('entityid', nameorig);
                var newcontact = nlapiSubmitRecord(rec, true, true);
                var emailUserSubject = 'Cologix Portal - You have been granted access to a new Company';
                var objFile = nlapiLoadFile(3777949);
                var language = nlapiLookupField('customer', companyid, 'language') || '';
                if(language ==17){
                    var emailUserSubject = 'Portail Cologix - Vous avez accès à une nouvelle compagnie';
                    var objFile = nlapiLoadFile(3830851);
                }
                if(emailcon!='')
                {

                    var emailUserBody = objFile.getValue();
                    emailUserBody =emailUserBody.replace(new RegExp('{fname}','g'), fname);
                    emailUserBody =emailUserBody.replace(new RegExp('{lname}','g'), lname);
                    emailUserBody = emailUserBody.replace(new RegExp('{companyname}','g'), compName);
                    var fromid = 432742;
                    nlapiSendEmail(fromid, id, emailUserSubject, emailUserBody, null, null, null, null,true);
                }
            }

        }
        else{
            // no other case
        }

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing users.';

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_USERS';
        obj.msg = 'No rights for users.';
    }

    return obj;
});


function get_contacts(companyid,contactid,srights,modifierid,radix,direction,order_by,page,per_page,search,sid,csv){

    var objData = {};
    var total = 0;
    var pages = 0;
    var start = 0;
    var end = 0;
    var has_more = false;

    search = JSON.parse(search);

    // Modifiers of managed company ===========================================================================================================

    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("company",null,"is",companyid));
    arrFilters.push(new nlobjSearchFilter("custentity_clgx_modifier",null,"noneof",'@NONE@'));
    var searchModifiers = nlapiSearchRecord('contact', 'customsearch_clgx_cp_contacts', arrFilters);
    var arrModifiers = [];
    for ( var i = 0; searchModifiers != null && i < searchModifiers.length; i++ ) {
        arrModifiers.push(parseInt(searchModifiers[i].getValue('custentity_clgx_modifier',null,null)));
    }
    var arrM = _.compact(_.uniq(arrModifiers));

    // Exclude managed company from others to filter out modifiers ===========================================================================================================

    var allcompanies = JSON.parse(nlapiLookupField('customrecord_clgx_api_cust_portal_logins', sid, 'custrecord_clgx_api_cust_portal_entities'));
    var othercompanies = _.reject(allcompanies, function(obj){
        return (obj.id == companyid);
    });
    var ids = _.pluck(othercompanies, 'id');

    // All users of other companies except those with the same modifiers as the users of managed company ===========================================================================================================

    var contacts = [];
    if(ids.length > 0){

        var filters = [];
        if(arrM){
            filters.push(new nlobjSearchFilter("custentity_clgx_modifier",null,"noneof",arrM));
        }
        filters.push(new nlobjSearchFilter("company",null,"anyof",ids));
        var searchOthers = nlapiSearchRecord('contact', 'customsearch_clgx_cp_contacts', filters);

        for ( var i = 0; searchOthers != null && i < searchOthers.length; i++ ) {

            var customerid = parseInt(searchOthers[i].getValue('company',null,null));
            var modifierid = parseInt(searchOthers[i].getValue('custentity_clgx_modifier',null,null)) || 0;
            var contactid = parseInt(searchOthers[i].getValue('internalid',null,null));

            var contact = {
                "rid": contactid.toString(radix),
                "type": "user",
                //"mrid": modifierid.toString(radix),
                //"companyrid": customerid.toString(radix),
                "companyid": customerid,
                "company": searchOthers[i].getText('company',null,null),
                "name": searchOthers[i].getValue('entityid',null,null),
                "username": searchOthers[i].getText('custentity_clgx_modifier',null,null) || '',
                "faicon":'fa fa-user'
            };
            contacts.push(contact);
        }
    }
    // group other companies contacts by company and add company lines for delimitation
    var others = [];
    for ( var i = 0; othercompanies != null && i < othercompanies.length; i++ ) {
        var companyothers = _.filter(contacts, function(arr){
            return (arr.companyid == othercompanies[i].id);
        });
        var obj = {
            "rid": othercompanies[i].rid,
            "name": othercompanies[i].name,
            "type": "company",
            "faicon":'fa fa-building-o'
        };
        others.push(obj);
        for ( var j = 0; companyothers != null && j < companyothers.length; j++ ) {
            others.push(companyothers[j]);
        }
    }

    // Users of managed company ===========================================================================================================

    var filters = [];
    if(search){
        if(search.name){
            filters.push(new nlobjSearchFilter("entityid",null,"contains",search.name));
        }
        if(search.email){
            filters.push(new nlobjSearchFilter("email",null,"contains",search.email));
        }
        if(search.phone){
            filters.push(new nlobjSearchFilter("phone",null,"contains",search.phone));
        }
    }
    filters.push(new nlobjSearchFilter("company",null,"is",companyid));
    if(srights.users == 0 && srights.security<4){ // non admin users see only admins
        filters.push(new nlobjSearchFilter("custentity_clgx_cp_user_rights_json",null,"contains",'"users":4'));
    }
    var columns = [];
    columns.push(new nlobjSearchColumn('custentity_clgx_dc_loc', null, null));
    columns.push(new nlobjSearchColumn('custentity_email2sms_gateway', null, null));
    columns.push(new nlobjSearchColumn('custentity_clgx_ops_outage_notification', null, null));
    columns.push(new nlobjSearchColumn('custentity_clgx_contact_notify_ship', null, null));
    columns.push(new nlobjSearchColumn('custentity_clgx_contact_notify_violation', null, null));
    //columns.push(new nlobjSearchColumn('custentity_clgx_contact_notify_ship_sms', null, null));
    // columns.push(new nlobjSearchColumn('custentity_clgx_sms_incidents', null, null));
    // columns.push(new nlobjSearchColumn('custentity_clgx_mntce_sms', null, null));
    // columns.push(new nlobjSearchColumn('custentity_clgx_advisory_sms', null, null));

    var searchUsers = nlapiSearchRecord('contact', 'customsearch_clgx_cp_contacts', filters,columns);

    var users = [];
    var thisContact = '';
    if(searchUsers){

        total = searchUsers.length;
        pages = Math.ceil(total / per_page);
        page = (parseInt(page) > pages ? pages : parseInt(page));
        has_more = (pages > 1 ? true : false);
        start = (page - 1) * per_page;
        end = (start + per_page > total ? total : start + per_page);

        var filters2 = new Array();
        filters2.push(new nlobjSearchFilter('custrecord_clgx_api_cust_portal_entity',null,'anyof',companyid));
        var logins = nlapiSearchRecord('customrecord_clgx_api_cust_portal_logins', 'customsearch_clgx_ss_cp_last_login_2', filters2);
        var lastLogin='';

        for ( var i = start; i < end; i++ ) {

            var customerid = parseInt(searchUsers[i].getValue('company',null,null));
            var modifierid = parseInt(searchUsers[i].getValue('custentity_clgx_modifier',null,null)) || 0;
            var contactid = parseInt(searchUsers[i].getValue('internalid',null,null));

            var rigthsStr = searchUsers[i].getValue('custentity_clgx_cp_user_rights_json',null,null) || '';
            if(rigthsStr != ''){
                var rights = JSON.parse(rigthsStr);
            } else {
                rights = get_min_rights();
            }
            //FOLLOWING CODE WAS REMOVED AS IT BREAKS GOVERNANCE NS RULES WHEN Customer has  > 500 contacts
//            var filtersLL=new Array();
//            filtersLL.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_contact",null,"is",contactid));
//            var searchLastLogins = nlapiSearchRecord('customrecord_clgx_api_cust_portal_logins', 'customsearch_clgx_ss_cp_last_login', filtersLL);
//            var lastLogin='';
//            if(searchLastLogins != null)
//            {
//                var searchLastLogin = searchLastLogins[0];
//                var columnsLL = searchLastLogin.getAllColumns();
//                var lastLogin = searchLastLogin.getValue(columnsLL[0]);
//            }

            lastLogin = '';
            for (var a = 0; a < logins.length; a++){
                thisContact = logins[a].getValue('custrecord_clgx_api_cust_portal_contact',null,null) || '';
                if (thisContact == contactid){
                    lastLogin = logins[a].getValue('custrecord_clgx_api_cust_portal_date',null,null) || '';
                    break;
                }
            }
            var contact = {
                "rid": contactid.toString(radix),
                "mrid": modifierid.toString(radix),
                "companyrid": customerid.toString(radix),
                "company": searchUsers[i].getText('company',null,null),
                "name": searchUsers[i].getValue('entityid',null,null),
                "username": searchUsers[i].getText('custentity_clgx_modifier',null,null) || '',
                "title": searchUsers[i].getValue('title',null,null),
                "email": searchUsers[i].getValue('email',null,null),
                "phone": searchUsers[i].getValue('phone',null,null),
                "last_login":lastLogin,
                "centers":searchUsers[i].getText('custentity_clgx_dc_loc',null,null) || '',
                "role1": searchUsers[i].getText('contactrole',null,null) || '',
                "role2": searchUsers[i].getText('custentity_clgx_contact_secondary_role',null,null) || '',
                "smsgate": searchUsers[i].getValue('custentity_email2sms_gateway') || '',
                "email_outage": searchUsers[i].getValue('custentity_clgx_ops_outage_notification') || 'F',
                "email_ship": searchUsers[i].getValue('custentity_clgx_contact_notify_ship') || 'F',
                "email_viol": searchUsers[i].getValue('custentity_clgx_contact_notify_violation') || 'F',
                //"sms_ship": searchUsers[i].getValue('custentity_clgx_contact_notify_ship_sms') || 'F',
                //   "sms_incident": searchUsers[i].getValue('custentity_clgx_sms_incidents') || 'F',
                //  "sms_maintain": searchUsers[i].getValue('custentity_clgx_mntce_sms') || 'F',
                //  "sms_advisory": searchUsers[i].getValue('custentity_clgx_advisory_sms') || 'F',

                "users": parseInt(rights.users),
                "cases": parseInt(rights.cases),
                "casesfin": parseInt(rights.casesfin),
                "reports": parseInt(rights.reports),
                "visits": parseInt(rights.visits),
                "shipments": parseInt(rights.shipments),
                "security": parseInt(rights.security),
                "orders": parseInt(rights.orders),
                "invoices": parseInt(rights.invoices),
                "colocation": parseInt(rights.colocation),
                "network": parseInt(rights.network),
                "managed": parseInt(rights.managed),
                "domains": parseInt(rights.domains)
            };
            users.push(contact);
        }
    }

    // ===========================================================================================================

    objData["data"] = {};

    objData.data["users"] = users;
    objData.data["others"] = others;

    if(csv){
        objData["csv"] = get_logins_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv);
    }
    objData["search"] = search;
    objData["lists"] = {};
    objData["direction"] = direction;
    objData["order_by"] = order_by;
    objData["page"] = page;
    objData["pages"] = pages;
    objData["per_page"] = per_page;
    objData["has_more"] = has_more;
    objData["start"] = start;
    objData["end"] = end;
    objData["total"] = total;

    return objData;
}


function get_logins_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv){

    var text = 'Name,Title,Role1,Role2,Email,Phone,DC Access Card(s),Users,Security Requester,Cases Operations,Cases Finance,Reports,Visits,Shipments,Orders,Invoices,Colocation,Network,DNS Management,Managed, Email Outage, Email Shipping, Email Violation, Email to SMS gateway\n';
    var arrRights = ['None','View','','','Full'];

    var columns = new Array();
    var filters = new Array();
    if(search){
        if(search.name){
            filters.push(new nlobjSearchFilter("entityid",null,"contains",search.name));
        }
        if(search.email){
            filters.push(new nlobjSearchFilter("email",null,"contains",search.email));
        }
        if(search.phone){
            filters.push(new nlobjSearchFilter("phone",null,"contains",search.phone));
        }
    }
    filters.push(new nlobjSearchFilter("internalid",'company',"is",companyid));
    columns.push(new nlobjSearchColumn('custentity_clgx_dc_loc', null, null));
    columns.push(new nlobjSearchColumn('custentity_email2sms_gateway', null, null));
    columns.push(new nlobjSearchColumn('custentity_clgx_ops_outage_notification', null, null));
    columns.push(new nlobjSearchColumn('custentity_clgx_contact_notify_ship', null, null));
    columns.push(new nlobjSearchColumn('custentity_clgx_contact_notify_violation', null, null));
    //columns.push(new nlobjSearchColumn('custentity_clgx_contact_notify_ship_sms', null, null));
    // columns.push(new nlobjSearchColumn('custentity_clgx_sms_incidents', null, null));
    // columns.push(new nlobjSearchColumn('custentity_clgx_mntce_sms', null, null));
    // columns.push(new nlobjSearchColumn('custentity_clgx_advisory_sms', null, null));
    var records = nlapiSearchRecord('contact', 'customsearch_clgx_cp_contacts', filters, columns);

    for ( var i = 0; records != null && i < records.length; i++ ) {

        var rigthsStr = records[i].getValue('custentity_clgx_cp_user_rights_json',null,null) || '';
        if(rigthsStr != ''){
            var rights = JSON.parse(rigthsStr);
        } else {
            rights = get_min_rights();
        }
//here
        /*     var contactid = parseInt(records[i].getValue('internalid',null,null));

         var filtersLL=new Array();
         filtersLL.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_contact",null,"is",contactid));
         var searchLastLogins = nlapiSearchRecord('customrecord_clgx_api_cust_portal_logins', 'customsearch_clgx_ss_cp_last_login', filtersLL);
         var lastLogin='';
         if(searchLastLogins != null)
         {
         var searchLastLogin = searchLastLogins[0];
         var columnsLL = searchLastLogin.getAllColumns();
         var lastLogin = searchLastLogin.getValue(columnsLL[0]);
         }
         */
//here
        text += records[i].getValue('entityid',null,null) + ',';
        text += records[i].getValue('title',null,null) + ',';
        text += records[i].getText('contactrole',null,null) + ',';
        text += records[i].getText('custentity_clgx_contact_secondary_role',null,null) + ',';
        text += records[i].getValue('email',null,null) + ',';
        text += records[i].getValue('phone',null,null) + ',';
        var loc=records[i].getText('custentity_clgx_dc_loc',null,null);
        //nlapiLogExecution('DEBUG', 'LOC', loc);

        loc=loc.replace(/,/g , ";");
        // nlapiLogExecution('DEBUG', 'LOC1', loc);
        text += loc + ',';
        //  text += lastLogin + ',';


        if(rights.users == 4){
            text += 'Admin,';
        }
        else if(rights.users == 0){
            text += 'None,';
        }
        else {
            text += rights.users + ',';
        }
        text += arrRights[rights.security] + ',';
        text += arrRights[rights.cases] + ',';
        text += arrRights[rights.casesfin] + ',';
        text += arrRights[rights.reports] + ',';
        text += arrRights[rights.visits] + ',';
        text += arrRights[rights.shipments] + ',';
        text += arrRights[rights.orders] + ',';
        text += arrRights[rights.invoices] + ',';
        text += arrRights[rights.colocation] + ',';
        text += arrRights[rights.network] + ',';
        text += arrRights[rights.domains]+',';
        text += arrRights[rights.managed] + ',';


        text += records[i].getValue('custentity_clgx_ops_outage_notification')+',';
        text += records[i].getValue('custentity_clgx_contact_notify_ship')+',';
        text += records[i].getValue('custentity_clgx_contact_notify_violation')+',';
        text += records[i].getValue('custentity_email2sms_gateway');
        //text += records[i].getValue('custentity_clgx_contact_notify_ship_sms')+',';
        // text += records[i].getValue('custentity_clgx_sms_incidents')+',';
        // text += records[i].getValue('custentity_clgx_mntce_sms')+',';
        // text += records[i].getValue('custentity_clgx_advisory_sms');


        text += '\n';

    }
    return text;
}

function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}


function get_min_rights() {
    return {
        "users":0,
        "security":0,
        "cases":0,
        "casesfin":0,
        "reports":0,
        "visits":0,
        "shipments":0,
        "orders":0,
        "invoices":0,
        "ccards":0,
        "colocation":0,
        "network":0,
        "domains":0,
        "managed":0,
        "shipments":0
    };
}


function get_billing_contacts(companyid){
    var response = 0;
    //nlapiLogExecution('ERROR','debug', " | companyid = " + companyid + " |");
    try {
        var filters = [];
        filters.push(new nlobjSearchFilter("company",null,"is",companyid));
        filters.push(new nlobjSearchFilter("role",null,"anyof",1));
        var searchUsers = nlapiSearchRecord('contact', 'customsearch_clgx_cp_contacts', filters);
        if(searchUsers){
            response = searchUsers.length;
        }
    }
    catch (error) {
        //nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
    }
    return response;
}