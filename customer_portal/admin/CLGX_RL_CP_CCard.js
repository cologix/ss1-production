nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_CCard.js
//	ScriptID:	customscript_clgx_rl_cp_ccard
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=706&deploy=1
//	@authors:	Ryan Pavely - ryan.pavely@cologix.com
//	Created:	10/03/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,id) {

    var id = datain['id'] || 0;
    var act = datain['act'] || 'get';
    var customerProfileId=datain['customerProfileId']||'';
    var customerPaymentProfileId=datain['customerPaymentProfileId']||'';
    var cclast4=datain['cclast4']||'';
    var ccexpMth=datain['ccexpMth']||'';
    var ccexpYear=datain['ccexpYear']||'';
    var ccType=datain['ccType']||'';
    var last=datain['last']||0;
    var first=datain['first']||0;
    
    if(first==1){
        first='T';
    }
    else{
        first='F';
    }

    if(srights.invoices > 0){

        obj["id"] = id;

        if(act == 'get'){
            obj["resp"] = true;
        }

        if(act == 'default'){
            obj["resp"] = ccard_default(companyid,contactid,modifierid,radix,sid,id);
            obj.code = 'SUCCESS';
            obj.msg = 'You are managing credit cards.';
        }
        if(act == 'delete'){
            obj["resp"] = ccard_delete(companyid,contactid,modifierid,radix,sid,id,last);
            if(obj["resp"]==true) {
                obj.code = 400;
                obj.msg = 'The card has been deleted.';
            }else
            {
                obj.code = 500;
                obj.msg = 'Error.';
            }
        }

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);


        if(act == 'create'){
            obj["resp"] = ccard_create(obj,customerProfileId,customerPaymentProfileId,cclast4,ccexpMth,ccexpYear,ccType,companyid,first);
            if(obj["resp"]==true) {
                obj.code = 400;
                obj.msg = 'A new card has been added.';
            }else
            {
                obj.code = 500;
                obj.msg = 'Error.';
            }
        }

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_CCARDS';
        obj.msg = 'No rights for credit cards.';
    }

    return obj;
});
function ccard_create(obj,customerProfileId,customerPaymentProfileId,cclast4,ccexpMth,ccexpYear,ccType,companyid,first){

    nlapiLogExecution('DEBUG','customerProfileId', customerProfileId + ': ' + customerPaymentProfileId);
    var record = nlapiCreateRecord('customrecord_clgx_credit_cards');
    record.setFieldValue('custrecord_token_customer', companyid);
    record.setFieldValue('custrecord_token', customerPaymentProfileId);
    record.setFieldValue('custrecord_card_last_four', cclast4);
    record.setFieldValue('custrecord_card_exp_month', ccexpMth);
    record.setFieldValue('custrecord_card_exp_year', ccexpYear);
    record.setFieldValue('custrecord_card_type', ccType);
    record.setFieldValue('custrecord_card_isdefault', first);
    var idRec = nlapiSubmitRecord(record, false, true);

    return true;
}


function ccard_default(companyid,contactid,modifierid,radix,sid,id){

    nlapiSubmitField('customrecord_clgx_credit_cards', id, 'custrecord_card_isdefault', 'T');

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null).setSort(true));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('custrecord_token_customer', null, 'anyof', companyid));
    arrFilters.push(new nlobjSearchFilter('internalid', null, 'noneof', id));
    arrFilters.push(new nlobjSearchFilter('isinactive',null,'is', 'F'));
    var cards = nlapiSearchRecord('customrecord_clgx_credit_cards', null, arrFilters, arrColumns);

    for ( var i = 0; cards != null && i < cards.length; i++ ) {
        var internalid = parseInt(cards[i].getValue('internalid',null,null));
        nlapiSubmitField('customrecord_clgx_credit_cards', internalid, 'custrecord_card_isdefault', 'F');
    }

    return true;
}

function ccard_delete(companyid,contactid,modifierid,radix,sid,id,last) {

    var rec = nlapiLoadRecord('customrecord_clgx_credit_cards', id);
    rec.setFieldValue('isinactive', 'T');

    nlapiSubmitRecord(rec, false,true);
    if(last==1)
    {
        nlapiSubmitField('customer', companyid, 'custentity_clgx_cc_enabled', 'F');
    }

    return true;
}
