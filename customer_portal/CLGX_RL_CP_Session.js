nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Session.js
//	ScriptID:	customscript_clgx_rl_cp_session
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=652&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/12/2016
//------------------------------------------------------

function post (datain){
	try {
		
		var start = moment();
		
		var is_json = (datain.constructor.name == 'Object');
		if (!is_json) {
			datain = JSON.parse(datain);
		}
		
		var cfid = datain['cfid'];
		var nsid = datain['nsid'];
		var ip = datain['ip'];
		var radix = datain['radix'];
		var lang = datain['lang'];
		var act = datain['act'];
		
		var obj = new Object();
		obj["error"] = 'T';
		obj["code"] = 'USERERROR';
		obj["msg"] = '';
	  
		if(nsid && ip && radix && act && lang){
			
			var langid = 1;
			if(lang == 'fr'){
				langid = 17;
			}
			
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_uuid",null,"is",nsid));
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_cfid",null,"is",cfid));
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_ip",null,"is",ip));
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_radix",null,"equalto",radix));
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_status",null,"noneof",[2,3]));
			var session = nlapiSearchRecord('customrecord_clgx_api_cust_portal_logins', null, arrFilters, arrColumns);
			
			if(session){
				var sid = parseInt(session[0].getValue('internalid',null,null));
				var now = moment().format("M/D/YYYY h:mm:ss a");
				
				obj.error = 'F';
				obj.code = 'SUCCESS';
				
				if(act == 'delete'){
					nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last','custrecord_clgx_api_cust_portal_status','custrecord_clgx_api_cust_portal_language'], [now,2,langid]);
					obj.msg = 'Session was deleted';
				} else {
					nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, 'custrecord_clgx_api_cust_portal_last', now);
					obj.msg = 'Session was updated';
				}
			
			} else {
				obj.msg = 'Session ID not valid';
			}
		} else {
			obj.msg = 'Missing arguments';
		}
		
		var usage = 5000 - parseInt(nlapiGetContext().getRemainingUsage());
		obj["usage"] = usage;
		
		var end = moment();
		var msec = end.diff(start);
		obj["time"] = msec/1000;
		
		obj["start"] = start.format('M/D/YYYY h:mm:ss');
		obj["last"] = end.format('M/D/YYYY h:mm:ss');
		
		obj["kb"] = ((JSON.stringify(obj)).length/1000).toFixed(3);
		
		return obj;
	}
  
	catch (error) {
	  
	  if (error.getDetails != undefined){
	      var code = error.getCode();
	      var msg = error.getDetails();
	      //nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	      //throw error;
	  } else {
	      var code = 'ERROR';
	      var msg = error.toString();
	      //nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		  //throw nlapiCreateError('99999', error.toString());
	  }
		var obj = new Object();
		obj["error"] = 'T';
		obj["code"] = code;
		obj["msg"] = msg;
		obj["me"] = {};
		return obj;
	}
}

