nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_App_Lists.js
//	ScriptID:	customscript_clgx_rl_cp_login
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=685&deploy=1
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	07/26/2016
//------------------------------------------------------

function get (datain){
    try {

        var obj = new Object();
        obj["speeds"] = get_speeds();
        obj["rates"] = get_rates();

        return obj;
    }

    catch (error) {

        if (error.getDetails != undefined){
            var code = error.getCode();
            var msg = error.getDetails();
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
         //   throw error;
        } else {
            var code = 'ERROR';
            var msg = error.toString();
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
           // throw nlapiCreateError('99999', error.toString());
        }
        var obj = new Object();
        obj["error"] = 'T';
        obj["code"] = code;
        obj["msg"] = msg;
        obj["me"] = {};
        return obj;
    }
}


function get_speeds() {
    var arr = [{"value":0,"text":""}];
    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid', null, null));
    arrColumns.push(new nlobjSearchColumn('name', null, null).setSort(false));
    var arrFilters = new Array();
    var list = nlapiSearchRecord('customlist_clgx_active_xc_speed', null, arrFilters, arrColumns);
    for (var i = 0; list != null && i < list.length; i++) {
        var obj = {
            "value": parseInt(list[i].getValue('internalid', null, null)),
            "text": list[i].getValue('name', null, null)
        }
        arr.push(obj);
    }
    return arr;
}

function get_rates(){
    var arr = [];
    //1
    var itemRecord=nlapiLoadRecord('serviceitem',591);
    var priceLevels1 = itemRecord.getLineItemMatrixValue('price1', 'price', 1, 1);
    arr.push(priceLevels1);
    //2
    var itemRecord=nlapiLoadRecord('serviceitem',590);
    var priceLevels1 = itemRecord.getLineItemMatrixValue('price1', 'price', 1, 1);
    arr.push(priceLevels1);

    var arrColumns = new Array();
    var arrFilters = new Array();

    var search = nlapiSearchRecord('serviceitem', 'customsearchclgx_ss_rl_cp_app_lists', arrFilters, arrColumns);
    var arr3=new Array();
    var arr4=new Array();
    if(search!=null) {

        for (var v = 0; search != null && v < search.length; v++) {
            var obj3=new Object();
            var obj4=new Object();
            var searchS = search[v];
            var columns = searchS.getAllColumns();
            var fac = searchS.getText(columns[0]);
            var price = searchS.getValue(columns[1]);
            var id = searchS.getValue(columns[2]);
            if(id==93){
                if(fac=='151 Front Street West'){
                    obj3.fac='TOR1';
                }else if(fac=="Harbour Centre"){
                    obj3.fac='VAN1';
                }
                else{
                    obj3.fac=fac;
                }
                obj3.price=price;
                arr3.push(obj3);
            }
            if(id==92){
                if(fac=='151 Front Street West'){
                    obj4.fac='TOR1';
                }else if(fac=="Harbour Centre"){
                    obj4.fac='VAN1';
                }
                else{
                    obj4.fac=fac;
                }
                obj4.price=price;
                arr4.push(obj4);
            }
        }
    }
    //3
    // var itemRecord=nlapiLoadRecord('serviceitem',93);
    // var priceLevels1 = itemRecord.getLineItemMatrixValue('price1', 'price', 1, 1);
    arr.push(arr3);
    //4
    // var itemRecord=nlapiLoadRecord('serviceitem',92);
    // var priceLevels1 = itemRecord.getLineItemMatrixValue('price1', 'price', 1, 1);
    arr.push(arr4);
    //5
    var itemRecord=nlapiLoadRecord('serviceitem',270);
    var priceLevels1 = itemRecord.getLineItemMatrixValue('price1', 'price', 1, 1);
    arr.push(priceLevels1);
    //6
    var itemRecord=nlapiLoadRecord('serviceitem',509);
    var priceLevels1 = itemRecord.getLineItemMatrixValue('price1', 'price', 1, 1);
    arr.push(priceLevels1);
    //7
    var itemRecord=nlapiLoadRecord('serviceitem',385);
    var priceLevels1 = itemRecord.getLineItemMatrixValue('price1', 'price', 1, 1);
    arr.push(priceLevels1);
    //8
    var itemRecord=nlapiLoadRecord('serviceitem',510);
    var priceLevels1 = itemRecord.getLineItemMatrixValue('price1', 'price', 1, 1);
    arr.push(priceLevels1);
    //9
    var itemRecord=nlapiLoadRecord('serviceitem',345);
    var priceLevels1 = itemRecord.getLineItemMatrixValue('price1', 'price', 1, 1);
    arr.push(priceLevels1);
    //10
    var itemRecord=nlapiLoadRecord('serviceitem',344);
    var priceLevels1 = itemRecord.getLineItemMatrixValue('price1', 'price', 1, 1);
    arr.push(priceLevels1);
    //11
    var itemRecord=nlapiLoadRecord('serviceitem',570);
    var priceLevels1 = itemRecord.getLineItemMatrixValue('price1', 'price', 1, 1);
    arr.push(priceLevels1);
    return arr;
}