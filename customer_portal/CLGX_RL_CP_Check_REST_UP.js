nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Check_REST_UP.js
//	ScriptID:	customscript_clgx_RL_CP_Check_REST_UP
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=708&deploy=1
//	@authors:	Catalina Taran
//	Created:	10/04/2016
//------------------------------------------------------

function get(datain) {
  var obj=new Object();
    obj["status"]="up";
      nlapiLogExecution('DEBUG', 'Return OBJ', JSON.stringify(obj));
    return obj;
}

function post(datain) {
    var obj={status:'up'};
    return obj;
}