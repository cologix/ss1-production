//------------------------------------------------------
//	Script:		CLGX_LIB_CP_Login.js
//	ScriptID:	customscript_clgx_lib_cp_global
//	ScriptType:	Library
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/12/2016
//------------------------------------------------------

function get_companies(modifierid,radix) {

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter('custentity_clgx_modifier',null,'anyof',modifierid));
    var records = nlapiSearchRecord('contact', 'customsearch_clgx_cp_modifier_companies', filters, columns);
    var arr = new Array();
    for ( var i = 0; records != null && i < records.length; i++ ) {
        var obj = new Object();

        var companyid = parseInt(records[i].getValue('company',null,null));
        obj["id"] = companyid;
        obj["rid"] = companyid.toString(radix);
        var companyname=records[i].getText('company',null,null);
        companyname=companyname.replace("'", "&#39;", 'g');
        obj["name"] = companyname;

        var contactid = parseInt(records[i].getValue('internalid',null,null));
        obj["cid"] = contactid;
        obj["crid"] = contactid.toString(radix);
        obj["cname"] = records[i].getValue('entityid',null,null);
        arr.push(obj);

    }
    return arr;
}

function get_company(contactid,modifierid,companyid,radix,sdamin) {

    var rec = nlapiLoadRecord('customer', companyid);
    var arrName = (rec.getFieldValue('entityid')).split(":");
    var rate_card=rec.getFieldValue('custentity_clgx_rate_card');
    if(rate_card==null)
    {
        rate_card=0;
    }
    var category = rec.getFieldText('category');
    var isCarrier = category.indexOf('Carrier');
    if (isCarrier !== -1) {
        var response = 'yes';
    }
    else{
        var response = 'no';
    }
    var obj = new Object();
    obj["id"] = companyid;
    obj["rid"] = companyid.toString(radix);
    var companyname=(arrName[arrName.length-1]).trim();
    companyname=companyname.replace("'", "&#39;", 'g');
    obj["name"] = companyname;
    // obj["name"] = (arrName[arrName.length-1]).trim();
    var nac = parseInt(rec.getFieldValue('custentity_clgx_matrix_entity_id')) || 0;
    obj["nac"] = nac;
    obj["balance"] = parseFloat(rec.getFieldValue('custentity_clgx_customer_balance')) || 0;
    obj["ccard"] = rec.getFieldValue('custentity_clgx_cc_enabled') || 'F';
    obj["authorizeid"] = rec.getFieldValue('custentity_clgx_customer_authorize_id') || '';
    obj["mailed"] = rec.getFieldValue('custentity_clgx_customer_mailed_invoices') || 'F';

    obj["subsidiary"] = rec.getFieldValue('subsidiary') || 0;

    var objRep = new Object();
    var repid = parseInt(rec.getFieldValue('salesrep')) || 0;
    //objRep["id"] = repid;
    //objRep["rid"] = repid.toString(radix);
    objRep["name"] = rec.getFieldText('salesrep') || '';
    if(repid > 0){
        var fields = ['email','phone'];
        var columns = nlapiLookupField('employee', repid, fields);
        objRep["email"] = columns.email;
        objRep["phone"] = columns.phone;
    }
    else{
        objRep["email"] = '';
        objRep["phone"] = '';
    }
    obj["salesrep"] = objRep;

    var objCCM = new Object();
    var ccmid = parseInt(rec.getFieldValue('custentity_clgx_cust_ccm')) || 0;
    //objRep["id"] = repid;
    //objRep["rid"] = repid.toString(radix);
    objCCM["name"] = rec.getFieldText('custentity_clgx_cust_ccm') || '';
    if(ccmid > 0){
        var fields = ['email','phone'];
        var columns = nlapiLookupField('employee', ccmid, fields);
        objCCM["email"] = columns.email;
        objCCM["phone"] = columns.phone;
    }
    else{
        objCCM["email"] = '';
        objCCM["phone"] = '';
    }
    obj["ccm"] = objCCM;
    obj["mReports"] = get_mreports(companyid);
    obj["services"] = get_services(companyid);
    var locations = get_locations(companyid);
    obj["locations"] = locations;
    obj["nac_locations"] = _.filter(locations, function(arr){
        return (arr.nacid > 0);
    });

    var facilities = get_facilities(companyid);
    obj["facilities"] = facilities;
    obj["nac_facilities"] = _.filter(facilities, function(arr){
        return (arr.nacid > 0);
    });

    obj["markets"] = get_markets(companyid);

    obj["spaces"] = get_spaces(companyid);
    obj["billingaddress"] = get_billingadd(companyid);
    obj["ratecard"] = rate_card;
    obj["iscarrier"] = response;
    //nlapiSendEmail(206211,206211,'fct arguments',contactid+','+modifierid+','+companyid+','+radix+','+nac+','+sdamin);
    obj["rights"] = get_rights(contactid,modifierid,companyid,radix,nac,sdamin);

    if(obj.rights.casesfin > 0){
        obj["cases_col"] = get_cases_colllections(companyid,radix);
    } else {
        obj["cases_col"] = [];
    }
    if(obj.rights.cases > 0){
        obj["cases_lst"] = get_cases_last(companyid,radix);
        obj["cases_mnt"] = get_cases_maintenances(companyid,radix);
    } else {
        obj["cases_lst"] = [];
        obj["cases_mnt"] = [];
    }
    if(obj.rights.invoices > 0){
        obj["invoices_lst"] = get_invoices_last(companyid,radix);
    } else {
        obj["invoices_lst"] = [];
    }

    obj["billing_contacts"] = get_billing_contacts(companyid);

    return obj;
}

function get_cases(id,radix){

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("internalid","company","anyof",id));
    var records = nlapiSearchRecord('supportcase', 'customsearch_clgx_api_cases', filters, columns);

    var arr = new Array();
    for ( var i = 0; records != null && i < records.length; i++ ) {
        var obj = new Object();
        var caseid = parseInt(records[i].getValue('internalid',null,null));
        //obj["id"] = caseid;
        obj["rid"] = caseid.toString(radix);
        obj["number"] = records[i].getValue('casenumber',null,null);
        obj["title"] = records[i].getValue('title',null,null) || '';
        //obj["statusid"] = parseInt(records[i].getValue('status',null,null)) || 0;
        obj["status"] = records[i].getText('status',null,null) || '';
        //obj["priorityid"] = parseInt(records[i].getValue('priority',null,null)) || 0;
        obj["priority"] = records[i].getText('priority',null,null) || '';
        arr.push(obj);
    }
    return arr;
}

function get_cases_colllections (id,radix){

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("internalid","company","anyof",id));
    var records = nlapiSearchRecord('supportcase', 'customsearch_clgx_cp_home_collections', filters, columns);
    var arr = new Array();
    if(records){
        var max = 5;
        if(records.length < max){
            max = records.length;
        }
        for ( var i = 0; i < max; i++ ) {
            var obj = new Object();
            var caseid = parseInt(records[i].getValue('internalid',null,null));
            //obj["id"] = caseid;
            obj["rid"] = caseid.toString(radix);
            obj["type"] = 'o';
            obj["number"] = records[i].getValue('casenumber',null,null);
            obj["title"] = records[i].getValue('title',null,null) || '';
            arr.push(obj);
        }
    }
    return arr;
}

function get_cases_maintenances(id,radix){

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("internalid","company","anyof",id));
    var records = nlapiSearchRecord('supportcase', 'customsearch_clgx_cp_home_maintenances', filters, columns);

    var total = 0;
    var arr = new Array();
    if(records){
        total = records.length;
        var max = 5;
        if(records.length < max){
            max = records.length;
        }
        for ( var i = 0; i < max; i++ ) {
            var obj = new Object();
            var caseid = parseInt(records[i].getValue('internalid',null,null));
            //obj["id"] = caseid;
            obj["rid"] = caseid.toString(radix);
            obj["type"] = 'm';
            obj["number"] = records[i].getValue('casenumber',null,null);
            obj["title"] = records[i].getValue('title',null,null) || '';
            obj["type"] = records[i].getText('custevent_cologix_sub_case_type',null,null) || '';
            obj["date"] = records[i].getValue('custevent_cologix_case_sched_followup',null,null) || '';
            arr.push(obj);
        }
    }
    return {
        "total": total,
        "cases": arr
    };
}

function get_cases_last (id,radix){

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("internalid","company","anyof",id));
    var records = nlapiSearchRecord('supportcase', 'customsearch_clgx_cp_home_last_cases', filters, columns);

    var total = 0;
    var arr = new Array();
    if(records){
        total = records.length;
        var max = 5;
        if(records.length < max){
            max = records.length;
        }
        for ( var i = 0; i < max; i++ ) {
            var obj = new Object();
            var caseid = parseInt(records[i].getValue('internalid',null,null));
            //obj["id"] = caseid;
            obj["rid"] = caseid.toString(radix);
            obj["type"] = 'o';
            obj["number"] = records[i].getValue('casenumber',null,null);
            obj["title"] = records[i].getValue('title',null,null) || '';
            obj["type"] = records[i].getText('type',null,null) + ' / ' + records[i].getText('custevent_cologix_sub_case_type',null,null);
            arr.push(obj);
        }
    }
    return {
        "total": total,
        "cases": arr
    };
}

function get_orders(id,radix){

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
    var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders', filters, columns);

    var arr = new Array();
    for ( var i = 0; records != null && i < records.length; i++ ) {
        var obj = new Object();
        var orderid = parseInt(records[i].getValue('internalid',null,'GROUP'));
        obj["id"] = orderid;
        obj["rid"] = orderid.toString(radix);
        obj["number"] = records[i].getValue('transactionnumber',null,'GROUP');
        obj["date"] = records[i].getValue('trandate',null,'GROUP') || '';
        //obj["typeid"] = parseInt(records[i].getValue('type',null,'GROUP')) || 0;
        obj["type"] = records[i].getText('type',null,'GROUP') || '';
        //obj["statid"] = parseInt(records[i].getValue('status',null,'GROUP')) || 0;
        obj["status"] = records[i].getText('status',null,'GROUP') || '';
        arr.push(obj);
    }
    return arr;
}

function get_services(id){

    var arr = ['rpt_utilization'];
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
    var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders_categs', filters);
    for ( var i = 0; records != null && i < records.length; i++ ) {
        var columns = records[i].getAllColumns();
        arr.push(records[i].getValue(columns[0]));
    }
    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_rpt_cust_customer",null,"is",id));
    var records = nlapiSearchRecord('customrecord_clgx_dcim_rpt_cust_customer', 'customsearch_clgx_cp_services_reports', filters);
    if(records){
        var consumption = records[0].getValue('custrecord_clgx_dcim_rpt_cust_rpt_kwh','CUSTRECORD_CLGX_DCIM_RPT_CUST_REPORT','GROUP') || 'F';
        var demand = records[0].getValue('custrecord_clgx_dcim_rpt_cust_rpt_kw','CUSTRECORD_CLGX_DCIM_RPT_CUST_REPORT','GROUP') || 'F';
        var environmentals = records[0].getValue('custrecord_clgx_dcim_rpt_cust_rpt_enviro','CUSTRECORD_CLGX_DCIM_RPT_CUST_REPORT','GROUP') || 'F';
        if(consumption == 'T') {
            arr.push('rpt_consumption');
        }
        if(demand == 'T') {
            arr.push('rpt_demand');
        }
        if(environmentals == 'T') {
            arr.push('rpt_environmentals');
        }
    }
    return arr;
}

function get_spaces(id){

    var columns = new Array();
    var arr=[{"facid":0,"facname":"","spaceid":0,"spacename":"", "market": 0}];

    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
    var spaces = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders_4', filters, columns);
    for ( var j = 0; spaces != null && j < spaces.length; j++ ) {
        var searchSpace = spaces[j];
        var columns = searchSpace.getAllColumns();
        var facid=parseInt(searchSpace.getValue(columns[0]));
        var facname=searchSpace.getValue(columns[1]);
        var spid=parseInt(searchSpace.getValue(columns[2]));
        var spname=searchSpace.getValue(columns[3]);

        //get facility name

        var filtersF = new Array();
        var columnsF = new Array();
        columnsF.push(new nlobjSearchColumn('name',null,null).setSort(false));
        columnsF.push(new nlobjSearchColumn('custrecord_clgx_facilty_market',null,null).setSort(false));
        filtersF.push(new nlobjSearchFilter("custrecord_clgx_facility_location",null,"is",facid));
        var facilities = nlapiSearchRecord('customrecord_cologix_facility', null, filtersF, columnsF);
        for ( var i = 0; facilities != null && i < facilities.length; i++ ) {
            var facname=facilities[i].getValue('name',null,null);
            var market=facilities[i].getValue('custrecord_clgx_facilty_market',null,null);
            //
        }
        var filtersM = new Array();
        var columnsM = new Array();
        var arrM=new Array();
        filtersM.push(new nlobjSearchFilter("custrecord_clgx_facilty_market",null,"is",market));
        var facilitiesM = nlapiSearchRecord('customrecord_cologix_facility','customsearch_clgx_ss_cp_facmarket', filtersM, columnsM);
        for ( var i = 0; facilitiesM != null && i < facilitiesM.length; i++ ) {
            var facilityM = facilitiesM[i];
            var columns = facilityM.getAllColumns();
            var facnameM=facilityM.getValue(columns[1]);
            var facidM=parseInt(facilityM.getValue(columns[0]));
            var objM = {
                "facid": facidM,
                "facname": facnameM

            };
            arrM.push(objM);
            //
        }
        var obj = {
            "facid": facid,
            "facname": facname,
            "spid": spid,
            "spname": spname,
            "market":market

        };
        obj["storeMarket_"+facid]=arrM;
        arr.push(obj);
    }

    return arr;
}
function get_billingadd(id){

    var filters = new Array();
    filters.push(new nlobjSearchFilter("internalid",null,"anyof",id));
    var arr=[];
    var arrdef=[];
    var list = nlapiSearchRecord('customer','customsearch_clgx_cp_customer_billing_ad', filters);
    for ( var i = 0; list != null && i < list.length; i++ ) {
        var searchlist = list[i];
        var columns = searchlist.getAllColumns();
        var address=searchlist.getValue(columns[0]);
        var isbiling=searchlist.getValue(columns[1]);
        var internalID=searchlist.getValue(columns[2]);
        if(isbiling=='T')
        {
            var obj = {
                "value": parseInt(internalID),
                "text":  address
            };
            arrdef.push(obj)
        }
        else
        {
            var obj = {
                "value": parseInt(internalID),
                "text":  address
            };
            arr.push(obj);
        }


    }
    if(arrdef.length>0)
    {
        return arrdef;
    }
    else
    {
        return arr;
    }

}


function get_facilities(id){

    var arr = [{"value":0,"text":""}];

    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
    var locations = nlapiSearchRecord('transaction', 'customsearch_clgx_cp_orders_locations', filters);
    var ids = [];
    for ( var i = 0; locations != null && i < locations.length; i++ ) {
        ids.push(parseInt(locations[i].getValue('location',null,'GROUP')));
    }
    if(ids.length > 0){
        var filters = new Array();
        var columns = new Array();
        columns.push(new nlobjSearchColumn('internalid',null,null));
        columns.push(new nlobjSearchColumn('name',null,null).setSort(false));
        columns.push(new nlobjSearchColumn('custrecord_clgx_facility_matrixid',null,null));
        filters.push(new nlobjSearchFilter("custrecord_clgx_facility_location",null,"anyof",ids));
        var facilities = nlapiSearchRecord('customrecord_cologix_facility', null, filters, columns);
        for ( var i = 0; facilities != null && i < facilities.length; i++ ) {
            var obj = {
                "value": parseInt(facilities[i].getValue('internalid',null,null)),
                "text": facilities[i].getValue('name',null,null),
                "nacid": parseInt(facilities[i].getValue('custrecord_clgx_facility_matrixid',null,null)) || 0
            };
            arr.push(obj);
        }
    }
    return arr;
}

function get_locations(id){

    /*
     var arr = [{"value":0,"text":""}];
     var filters = new Array();
     filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
     var locations = nlapiSearchRecord('transaction', 'customsearch_clgx_cp_orders_locations', filters);
     for ( var i = 0; locations != null && i < locations.length; i++ ) {
     var obj = {
     "value": parseInt(locations[i].getValue('location',null,'GROUP')),
     "text": locations[i].getValue('locationnohierarchy',null,'GROUP')
     };
     arr.push(obj);
     }
     */
    var arrLocations = [];
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
    var locations = nlapiSearchRecord('transaction', 'customsearch_clgx_cp_orders_locations', filters);
    for ( var i = 0; locations != null && i < locations.length; i++ ) {
        arrLocations.push(parseInt(locations[i].getValue('location',null,'GROUP')));
    }

    var arr = [{"value":0,"text":""}];
    if(arrLocations.length > 0) {
        var columns = new Array();
        columns.push(new nlobjSearchColumn('internalid', null, null));
        columns.push(new nlobjSearchColumn('name', null, null).setSort(false));
        columns.push(new nlobjSearchColumn('custrecord_clgx_facility_location', null, null));
        columns.push(new nlobjSearchColumn('custrecord_clgx_facility_matrixid', null, null));
        columns.push(new nlobjSearchColumn('custrecord_clgx_facilty_market', null, null));
        var filters = new Array();
        filters.push(new nlobjSearchFilter("custrecord_clgx_facility_location", null, "anyof", arrLocations));
        filters.push(new nlobjSearchFilter('isinactive', null, 'is', 'F'));
        var facilities = nlapiSearchRecord('customrecord_cologix_facility', null, filters, columns);
        for (var i = 0; facilities != null && i < facilities.length; i++) {
            var obj = {
                "value": parseInt(facilities[i].getValue('custrecord_clgx_facility_location', null, null)),
                "text": facilities[i].getValue('name', null, null),
                "nacid": parseInt(facilities[i].getValue('custrecord_clgx_facility_matrixid', null, null)) || 0
            };
            arr.push(obj);
        }
    }
    return arr;
}

function get_markets(id){

    var arr = [{"value":0,"text":""}];
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
    var locations = nlapiSearchRecord('transaction', 'customsearch_clgx_cp_orders_markets', filters);
    for ( var i = 0; locations != null && i < locations.length; i++ ) {
        var obj = {
            "value": parseInt(locations[i].getValue('custbody_clgx_consolidate_locations',null,'GROUP')),
            "text": locations[i].getText('custbody_clgx_consolidate_locations',null,'GROUP')
        };
        arr.push(obj);
    }
    return arr;
}

function get_invoices_last(id,radix){

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_customer",null,"anyof",id));
    var records = nlapiSearchRecord('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_cp_invoices_dashboard', filters, columns);

    var arr = new Array();
    if(records){
        var max = 5;
        if(records.length < max){
            max = records.length;
        }
        for ( var i = 0; i < max; i++ ) {
            var obj = new Object();
            var invoiceid = parseInt(records[i].getValue('internalid',null,null));
            //obj["id"] = invoiceid;
            obj["rid"] = invoiceid.toString(radix);
            obj["number"] = records[i].getValue('name',null,null);
            obj["date"] = records[i].getValue('custrecord_clgx_consol_inv_date',null,null) || '';
            obj["total"] = parseFloat(records[i].getValue('custrecord_clgx_consol_inv_total',null,null)) || 0;
            var pdfid = parseInt(records[i].getValue('custrecord_clgx_consol_inv_pdf_file_id',null,null)) || 0;
            obj["pdf"] = pdfid;
            obj["rpdf"] = pdfid.toString(radix);
            var jsonid = parseInt(records[i].getValue('custrecord_clgx_consol_inv_json_file_id',null,null)) || 0;
            obj["json"] = jsonid;
            obj["rjson"] = jsonid.toString(radix);
            arr.push(obj);
        }
    }
    return arr;
}
function get_rights(contactid,modifierid,companyid,radix,nac,sdamin) {
    if(sdamin>0){
        return {
            "users":sdamin,
            "security":4,
            "cases":sdamin,
            "casesfin":sdamin,
            "reports":1,
            "visits":sdamin,
            "orders":1,
            "invoices":1,
            "ccards":4,
            "colocation":1,
            "network":1,
            "domains":sdamin,
            "managed":1,
            "shipments": 4
        };

    } else {

        var columns = new Array();
        columns.push(new nlobjSearchColumn('internalid',null,null).setSort(true));
        var filters = new Array();
        filters.push(new nlobjSearchFilter("company",null,"is",companyid));
        filters.push(new nlobjSearchFilter("custentity_clgx_modifier",null,"is",modifierid));
        filters.push(new nlobjSearchFilter('isinactive',null,'is', 'F'));
        var search = nlapiSearchRecord('contact', null, filters, columns);

        if(search){
            var internalid = parseInt(search[0].getValue('internalid',null,null));
            var rights = nlapiLookupField('contact', internalid, 'custentity_clgx_cp_user_rights_json') || JSON.stringify( get_min_rights() );
        } else {
            var rights = get_min_rights();
        }

        return JSON.parse(rights);
    }
}


function get_min_rights() {
    return {
        "users":0,
        "security":0,
        "cases":0,
        "casesfin":0,
        "reports":0,
        "visits":0,
        "orders":0,
        "invoices":0,
        "ccards":0,
        "colocation":0,
        "network":0,
        "domains":0,
        "managed":0,
        "shipments":0
    };
}




function get_uuid() {
    var s = [], itoh = '0123456789ABCDEF';
    for (var i = 0; i <36; i++) s[i] = Math.floor(Math.random()*0x10);
    s[14] = 4;
    s[19] = (s[19] & 0x3) | 0x8;
    for (var i = 0; i <36; i++) s[i] = itoh[s[i]];
    s[8] = s[13] = s[18] = s[23] = '-';
    return s.join('');
}

function generate_password(length, special) {
    var iteration = 0;
    var password = "";
    var randomNumber;
    if(special == undefined){
        var special = false;
    }
    while(iteration < length){
        randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
        if(!special){
            if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
            if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
            if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
            if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
        }
        iteration++;
        password += String.fromCharCode(randomNumber);
    }
    return password;
}

function get_radix() {
    return Math.floor(Math.random() * (10 - 5)) + 5;
}

function send_new_password (modifierid, contactid, email, lang) {

    var newpass = generate_password(12, false);

    if(lang == 17){
        var subject = 'Portail de Cologix';
        var body = 'Madame, Monsieur,\n\n' +
            'Voici votre mot de passe:\n\n' + newpass + '\n\n' +
            'Merci de faire affaire avec Cologix.\n\n' +
            'Au plaisir de vous &ecirc;tre utile,\n' +
            'Cologix\n\n';
        var msg = 'Un nouveau mot de passe a été envoyé à votre e-mail.';
    } else {
        var subject = 'Cologix Customer Portal - Password Reset';
        var body = 'Dear Customer,\n\n' +
            'Here is your new password:\n\n' + newpass + '\n\n' +
            'Thank you for your business.\n\n' +
            'Sincerely,\n' +
            'Cologix\n\n';
        var msg = 'A new password was sent to the email we have on file.';
    }

    nlapiSendEmail(432742, contactid, subject, body, null, null, null, null, true, null, "support@cologix.com");

    var redate = moment().format('M/D/YYYY');
    var fields = ['custrecord_clgx_modifier_password','custrecord_clgx_modifier_password_date','custrecord_clgx_modifier_password_redate','custrecord_clgx_modifier_reset_password'];
    var values = [newpass,null,redate,'T'];
    nlapiSubmitField('customrecord_clgx_modifier', modifierid, fields, values);

    return msg;
}

function set_session(obj){

    var now = moment().format("M/D/YYYY h:mm:ss a");
    var record = nlapiCreateRecord('customrecord_clgx_api_cust_portal_logins');
    record.setFieldValue('custrecord_clgx_api_cust_portal_uuid', obj.ns.nsid);
    record.setFieldValue('custrecord_clgx_api_cust_portal_cfid', obj.ns.cfid);
    record.setFieldValue('custrecord_clgx_api_cust_portal_ip', obj.ns.ip);
    record.setFieldValue('custrecord_clgx_api_cust_portal_radix', obj.ns.radix);
    record.setFieldValue('custrecord_clgx_api_cust_portal_date', now);
    record.setFieldValue('custrecord_clgx_api_cust_portal_last', now);
    record.setFieldValue('custrecord_clgx_api_cust_portal_modifier', obj.user.mid);
    record.setFieldValue('custrecord_clgx_api_cust_portal_contact', obj.user.id);
    record.setFieldValue('custrecord_clgx_api_cust_portal_entity', obj.company.id);
    record.setFieldValue('custrecord_clgx_api_cust_portal_rights', JSON.stringify(obj.company.rights));
    record.setFieldValue('custrecord_clgx_api_cust_portal_entities', JSON.stringify(obj.companies));
    if(obj.user.sadmin > 0){
        record.setFieldValue('custrecord_clgx_api_cust_portal_sadmin', obj.user.sadmin);
    }
    record.setFieldValue('custrecord_clgx_api_cust_portal_language', obj.user.langid);
    record.setFieldValue('custrecord_clgx_api_cust_portal_status', 1);
    //record.setFieldValue('custrecord_clgx_api_cust_portal_masquara', obj.masq);
    var recid = nlapiSubmitRecord(record, false,true);
    return recid;
}

function get_billing_contacts(companyid){
    var response = 0;
    //nlapiLogExecution('ERROR','debug', " | companyid = " + companyid + " |");
    try {
        var filters = [];
        filters.push(new nlobjSearchFilter("company",null,"is",companyid));
        filters.push(new nlobjSearchFilter("role",null,"anyof",1));
        var searchUsers = nlapiSearchRecord('contact', 'customsearch_clgx_cp_contacts', filters);
        if(searchUsers){
            response = searchUsers.length;
        }
    }
    catch (error) {
        //nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
    }
    return response;
}
function get_mreports(companyid) {

    var arrFil=new Array();
    var arrCol=new Array();
    arrFil.push(new nlobjSearchFilter("custrecord_clgx_monitoring_cust",null,"anyof",companyid));
    var search = nlapiSearchRecord('customrecord_clgx_monitoring_report','customsearch_clgx2_power_cooling_rpt', arrFil, arrCol);
    var obj = new Object();
    var reports=[];
    if(search!=null) {

        for (var i = 0; search != null && i < search.length; i++) {

            var searchR = search[i];
            var columns = searchR.getAllColumns();
            var report = {
                'id': searchR.getValue(columns[1]),
                "facid": searchR.getValue(columns[3]),
                "fac": searchR.getText(columns[3]),
                "typeid": searchR.getValue(columns[4]),
                "type": searchR.getText(columns[4])
            };
            reports.push(report);
        }
    }

    reports= _.groupBy(reports, 'type');

    obj['mreports'] = reports;

    return obj;
}