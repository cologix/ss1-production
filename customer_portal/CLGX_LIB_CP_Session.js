//------------------------------------------------------
//	Script:		CLGX_LIB_CP_Session.js
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/24/2016
//------------------------------------------------------

function wrap(func) {

    return function (datain) {

        try {

            var nsid = datain['nsid'];
            var cfid = datain['cfid'];
            var ip = datain['ip'];
            var radix = datain['radix'];
            var mrid = datain['mrid'];
            var crid = datain['crid'];
            var erid = datain['erid'];

            var obj = {};
            obj["error"] = 'F';
            obj["code"] = '';
            obj["msg"] = '';
            obj["rights"] = 0;

            if(nsid && cfid && ip && radix && mrid && crid && erid){

                var modifierid = parseInt(mrid, radix);
                var contactid = parseInt(crid, radix);
                var companyid = parseInt(erid, radix);

                var arrColumns = new Array();
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_modifier",null,"anyof",modifierid));
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_contact",null,"anyof",contactid));
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_entity",null,"anyof",companyid));
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_uuid",null,"is",nsid));
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_cfid",null,"is",cfid));
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_ip",null,"is",ip));
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_radix",null,"equalto",radix));
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_status",null,"anyof",1));
                var session = nlapiSearchRecord('customrecord_clgx_api_cust_portal_logins', 'customsearch_clgx_cp_sesssion_check', arrFilters, arrColumns);

                if(session){

                    var sid = parseInt(session[0].getValue('internalid',null,null));
                    var sadmin = parseInt(session[0].getValue('custrecord_clgx_api_cust_portal_sadmin',null,null));
                    var scompanies = JSON.parse(session[0].getValue('custrecord_clgx_api_cust_portal_entities',null,null));
                    var scompany = _.find(scompanies, function(arr){ return (arr.id == companyid) ; });
                    var srights = JSON.parse(session[0].getValue('custrecord_clgx_api_cust_portal_rights',null,null));

                    if(scompany || sadmin){

                        var obj = func(datain,obj,srights,companyid,contactid,modifierid,radix,sid);

                    } else {
                        obj.error = 'T';
                        obj.code = 'NO_RIGHTS_COMPANY';
                        obj.msg = 'No rights for this company';
                    }

                } else {
                    obj.error = 'T';
                    obj.code = 'NO_SESSION';
                    obj.msg = 'Session request not valid.';
                }

            } else {
                obj.error = 'T';
                obj.code = 'MISS_ARGS';
                obj.msg = 'Missing arguments';
            }

            return obj;

        } catch (error) {

            if (error.getDetails != undefined){
                var code = error.getCode();
                var msg = error.getDetails();
                nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
              //  throw error;
            } else {
                var code = 'ERROR';
                var msg = error.toString();
                nlapiLogExecution('ERROR','Unexpected Error', error.toString());
             //   throw nlapiCreateError('99999', error.toString());
            }
            var obj = new Object();
            obj["error"] = 'T';
            obj["code"] = code;
            obj["msg"] = msg;
            obj["me"] = {};
            return obj;
        }
    };
}