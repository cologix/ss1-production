nlapiLogExecution("audit","FLOStart",new Date().getTime());
/**
 * Created by ctaran on 2017-06-27.
 */

//------------------------------------------------------
//	Script:		CLGX_SS_CP_CreateXC.js
//	ScriptID:	customscript_CLGX_SS_CP_CreateXC
//	ScriptType:	Scheuled
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//------------------------------------------------------

function create_xcs() {
    try {

        var columns = new Array();
        var filters = new Array();
        var records = nlapiSearchRecord('customrecord_clgx_cp_q_transactions', 'customsearch_clgx_cp_create_xcs_portal', filters, columns);
        if(records){
            for ( var i = 0; records != null && i < records.length; i++ ) {
                var search = records[i];
                var columns = search.getAllColumns();
                var propID=search.getValue(columns[0]);
                nlapiLogExecution('DEBUG','Test', propID);
                var portId=search.getValue(columns[1]);
                var evc=search.getValue(columns[2]);
                var qID=search.getValue(columns[3]);
                var columnsSO = new Array();
                var filtersSO = new Array();
                filtersSO.push(new nlobjSearchFilter("createdfrom",null,"anyof",propID));

                columnsSO.push(new nlobjSearchColumn('internalid', null, null));

                var recordsSO = nlapiSearchRecord('transaction', null, filtersSO, columnsSO);
                if(recordsSO!=null){
                    var soID=recordsSO[0].getValue('internalid',null,null);
                    var soRecord=nlapiLoadRecord('salesorder',soID);
                    var locationid=soRecord.getFieldValue('location');
                    var serviceid = soRecord.getLineItemValue('item', 'custcol_clgx_so_col_service_id', evc);
                    var item = soRecord.getLineItemValue('item', 'item', evc);
                    var facid= clgx_return_facilityid (locationid);
                    var newRecord = nlapiCreateRecord('customrecord_cologix_crossconnect');
                    newRecord.setFieldValue('custrecord_clgx_xc_facility', facid);
                    newRecord.setFieldValue('custrecord_xconnect_service_order', soID);
                    newRecord.setFieldValue('custrecord_cologix_xc_service', serviceid);
                    newRecord.setFieldValue('custrecord_cologix_xc_type', 32);
                    newRecord.setFieldValue('custrecord_cologix_xc_status', 1);
                    newRecord.setFieldValue('custrecord_clgx_xc_type', 1);
                    newRecord.setFieldValue('custrecord_cologix_carrier_name', 2763);

                    if(item==792 || item==793) {
                        newRecord.setFieldValue('custrecord_cologix_xc_circuit_type', 10);

                    }else if(item==794 || item==795){
                        newRecord.setFieldValue('custrecord_cologix_xc_circuit_type', 21);

                    }
                    if(item==793 || item==795) {
                        newRecord.setFieldValue('custrecord_clgx_xc_type', 2);
                        newRecord.setFieldValue('custrecord_clgx_port_id', portId +' A');
                    }
                    else{
                        newRecord.setFieldValue('custrecord_clgx_port_id', portId);
                    }


                    nlapiSubmitRecord(newRecord, false,true);
                    if(item==793 || item==795) {
                        var newRecord = nlapiCreateRecord('customrecord_cologix_crossconnect');
                        newRecord.setFieldValue('custrecord_clgx_xc_facility', facid);
                        newRecord.setFieldValue('custrecord_xconnect_service_order', soID);
                        newRecord.setFieldValue('custrecord_cologix_xc_service', serviceid);
                        newRecord.setFieldValue('custrecord_cologix_xc_type', 32);
                        newRecord.setFieldValue('custrecord_cologix_xc_status', 1);
                        newRecord.setFieldValue('custrecord_cologix_carrier_name', 2763);
                        if(item==792 || item==793) {
                            newRecord.setFieldValue('custrecord_cologix_xc_circuit_type', 10);

                        }else if(item==794 || item==795){
                            newRecord.setFieldValue('custrecord_cologix_xc_circuit_type', 21);

                        }
                        newRecord.setFieldValue('custrecord_clgx_xc_type', 2);
                        newRecord.setFieldValue('custrecord_clgx_port_id', portId +' B');
                        nlapiSubmitRecord(newRecord, false,true);
                    }
                    nlapiSubmitField('customrecord_clgx_cp_q_transactions', qID, 'custrecord_clgx_cp_tq_xcpr', 'T');

                }

              
            }



        }
        var columns = new Array();
        var filters = new Array();
        var records = nlapiSearchRecord('customrecord_clgx_cp_q_transactions', 'customsearch_clgx_cp_create_xcs_portal_2', filters, columns);
        if(records){
            for ( var i = 0; records != null && i < records.length; i++ ) {
                var search = records[i];
                var columns = search.getAllColumns();
                var propID=search.getValue(columns[0]);
                nlapiLogExecution('DEBUG','Test', propID);
                var portId=search.getValue(columns[1]);
                var evc=search.getValue(columns[2])||1;
                nlapiLogExecution('DEBUG','EVC', evc);
                var qID=search.getValue(columns[3]);
                var columnsSO = new Array();
                var filtersSO = new Array();
                filtersSO.push(new nlobjSearchFilter("createdfrom",null,"anyof",propID));

                columnsSO.push(new nlobjSearchColumn('internalid', null, null));

                var recordsSO = nlapiSearchRecord('transaction', null, filtersSO, columnsSO);
                if(recordsSO!=null){
                    var soID=recordsSO[0].getValue('internalid',null,null);
                    var soRecord=nlapiLoadRecord('salesorder',soID);
                    var locationid=soRecord.getFieldValue('location');
                    var serviceid = soRecord.getLineItemValue('item', 'custcol_clgx_so_col_service_id', 1);
                    var item = soRecord.getLineItemValue('item', 'item', 1);
                    var notes=soRecord.getFieldValue('custbody_clgx_order_notes');
                    var notes=soRecord.getFieldValue('custbody_clgx_order_notes');
                    var arrNotes=notes.split(';');
                    var provider=arrNotes[2].substring(2, arrNotes[2].lenght);
                    var skeySt=arrNotes[3].substring(2, arrNotes[3].lenght);
                    var skeyArr=skeySt.split(':');
                    var skey=skeyArr[1];
                    var stagSt=arrNotes[4].substring(2, arrNotes[4].lenght);
                    var stagArr=stagSt.split(':');
                    var stag=stagArr[1];
                    var speed=arrNotes[6].substring(2, arrNotes[6].lenght);
                    // nlapiLogExecution('DEBUG','Speed before', speed);
                    speed=speed.slice(0,-2);
                    if ( speed.indexOf("/") > -1 ){
                        var speedArr=speed.split('/');
                        var speedArr1=speedArr[1].split(':');
                        var speed=speedArr1[1].substring(1, speedArr1[1].lenght);

                    }
                    // nlapiLogExecution('DEBUG','Test Here', 'Test Here');
                    // nlapiLogExecution('DEBUG','Speed before', speed);
                    var newRecord = nlapiCreateRecord('customrecord_cologix_vxc');
                    if(speed==1){
                        speed=3;
                        newRecord.setFieldValue('custrecord_clgx_vxc_speed', speed);
                    }
                    else if(speed==2){
                        speed=11;
                        newRecord.setFieldValue('custrecord_clgx_vxc_speed', speed);
                    }
                    else if(speed==5){
                        speed=10;
                        newRecord.setFieldValue('custrecord_clgx_vxc_speed', speed);
                    }
                    else if(speed==10){
                        speed=4;
                        newRecord.setFieldValue('custrecord_clgx_vxc_speed', speed);
                    }
                    else if(speed!='null' && speed!='') {
                        newRecord.setFieldText('custrecord_clgx_vxc_speed', speed);
                    }
                    //   nlapiLogExecution('DEBUG','Speed after', speed);


                    var facid= clgx_return_facilityid (locationid);


                    newRecord.setFieldValue('custrecord_cologix_vxc_facility', facid);
                    newRecord.setFieldValue('custrecord_cologix_service_order', soID);
                    newRecord.setFieldValue('custrecord_cologix_vxc_service', serviceid);
                    newRecord.setFieldValue('custrecord_cologix_vxc_type', 29);
                    newRecord.setFieldValue('custrecord_cologix_vxc_status', 1);
                    if(provider!='null' && provider!='') {
                        newRecord.setFieldText('custrecord_clgx_cloud_provider', provider);
                    }
                    if(skey!='null' && skey!='') {
                        newRecord.setFieldValue('custrecord_clgx_vxc_skey', skey);
                    }
                    if(stag!='null' && stag!='') {
                        newRecord.setFieldValue('custrecord_clgx_vxc_msft_stag', stag);
                    }

                    var vxc=nlapiSubmitRecord(newRecord, false,true);
                    // nlapiLogExecution('DEBUG','Item', item);
                    if(item==798) {
                        nlapiLogExecution('DEBUG','Item', item);
                        var copyvxc = nlapiCopyRecord('customrecord_cologix_vxc', vxc);
                        var copiedId = nlapiSubmitRecord(copyvxc, false,true);

                    }
                    nlapiSubmitField('customrecord_clgx_cp_q_transactions', qID, 'custrecord_clgx_cp_tq_vxcpr', 'T');

                }

              
            }



        }


    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

}