nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Orders.js
//	ScriptID:	customscript_clgx_rl_cp_orders
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=656&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/17/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv) {

    if(srights.orders > 0){

        var direction = datain['direction'] || 'ASC';
        var order_by = datain['order_by'] || '';
        var page = datain['page'] || 1;
        var per_page = datain['per_page'];
        var search = datain['search'] || '';
        var csv = datain['csv'] || 0;

        obj["orders"] = get_orders(companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv);

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing orders.';

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_ORDERS';
        obj.msg = 'No rights for orders.';
    }

    return obj;

});

function get_orders(companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv){

    var obj = {};
    var arr = [];
    var total = 0;
    var pages = 0;
    var start = 0;
    var end = 0;
    var has_more = false;

    search = JSON.parse(search);

    var columns = new Array();
    var filters = new Array();

    filters.push(new nlobjSearchFilter("entity",null,"anyof",companyid));

    var columnsI = new Array();
    var filtersI = new Array();
    filtersI.push(new nlobjSearchFilter("name",null,"anyof",companyid));
    var invoices = nlapiSearchRecord('invoice','customsearch_clgx_api_inv_sos', filtersI, columnsI);
    var arrInvSOs=new Array();
    if(invoices!=null) {
        for (var i = 0; invoices != null && i < invoices.length; i++) {
            var searchInv = invoices[i];
            var columnsI = searchInv.getAllColumns();
            var soID = searchInv.getValue(columnsI[0]);
            arrInvSOs.push(soID);
        }
    }
    if(search){
        if(search.id){
            //filters.push(new nlobjSearchFilter("transactionnumber",null,"contains",search.id));
            // filters.push(new nlobjSearchFilter("tranid",null,"contains",search.id));
            var filters=[];
            filters =[ [['custbody_cologix_legacy_so', 'is', search.id ],
                'or',
                ['tranid', 'contains', search.id] ], 'and', ['entity', 'anyof', companyid]];

        }

        if(search.statuses){

            if(search.statuses == 1){
                filters.push(new nlobjSearchFilter("status",null,"anyof","Opprtnty:A"));
                if(search.id) {
                    var filters = [];
                    filters = [[['custbody_cologix_legacy_so', 'is', search.id],
                        'or',
                        ['tranid', 'contains', search.id]],
                        'and', ['entity', 'anyof', companyid],
                        'and', ['status', 'anyof', "Opprtnty:A"]
                    ];
                }


            }
            if(search.statuses == 2){
                filters.push(new nlobjSearchFilter("status",null,"anyof","Estimate:A"));
                if(search.id) {
                    var filters = [];
                    filters = [[['custbody_cologix_legacy_so', 'is', search.id],
                        'or',
                        ['tranid', 'contains', search.id]],
                        'and', ['entity', 'anyof', companyid],
                        'and', ['status', 'anyof', "Estimate:A"]
                    ];
                }

            }
            if(search.statuses == 3){
                filters.push(new nlobjSearchFilter("status",null,"anyof","SalesOrd:A"));
                filters.push(new nlobjSearchFilter("custbody_clgx_wtng_cust_response",null,"is","F"));

                if(search.id) {
                    var filters = [];
                    filters = [[['custbody_cologix_legacy_so', 'is', search.id],
                        'or',
                        ['tranid', 'contains', search.id]],
                        'and', ['entity', 'anyof', companyid],
                        'and', ['status', 'anyof', "SalesOrd:A"],
                        'and', ['custbody_clgx_wtng_cust_response', 'is', "F"],
                    ];
                }

            }
            if(search.statuses == 4){
                filters.push(new nlobjSearchFilter("status",null,"anyof","SalesOrd:A"));
                filters.push(new nlobjSearchFilter("custbody_clgx_wtng_cust_response",null,"is","T"));


                if(search.id) {
                    var filters = [];
                    filters = [[['custbody_cologix_legacy_so', 'is', search.id],
                        'or',
                        ['tranid', 'contains', search.id]],
                        'and', ['entity', 'anyof', companyid],
                        'and', ['status', 'anyof', "SalesOrd:A"],
                        'and', ['custbody_clgx_wtng_cust_response', 'is', "T"],
                    ];
                }

            }
            if(search.statuses == 5){
                filters.push(new nlobjSearchFilter("status",null,"anyof","SalesOrd:F"));
                if(search.id) {
                    var filters = [];
                    filters = [[['custbody_cologix_legacy_so', 'is', search.id],
                        'or',
                        ['tranid', 'contains', search.id]],
                        'and', ['entity', 'anyof', companyid],
                        'and', ['status', 'anyof', "SalesOrd:F"]
                    ];
                }

            }
            if(search.statuses == 6){
                filters.push(new nlobjSearchFilter("status",null,"anyof","SalesOrd:G"));
                if(search.id) {
                    var filters = [];
                    filters = [[['custbody_cologix_legacy_so', 'is', search.id],
                        'or',
                        ['tranid', 'contains', search.id]],
                        'and', ['entity', 'anyof', companyid],
                        'and', ['status', 'anyof', "SalesOrd:G"]
                    ];
                }

            }
        }
        if(search.from && search.to){
            filters.push(new nlobjSearchFilter("trandate",null,"within",search.from,search.to));

        }
        if(search.from && !search.to){
            filters.push(new nlobjSearchFilter("trandate",null,"on",search.from));

        }
        if(!search.from && search.to){
            filters.push(new nlobjSearchFilter("trandate",null,"on",search.to));

        }
    }

    var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders', filters, columns);

    if(records){

        total = records.length;
        pages = Math.ceil(total / per_page);
        page = (parseInt(page) > pages ? pages : parseInt(page));
        has_more = (pages > 1 ? true : false);
        start = (page - 1) * per_page;
        end = (start + per_page > total ? total : start + per_page);

        for ( var i = start; i < end; i++ ) {

            var cols = records[i].getAllColumns();

            var rec = new Object();
            var idL  = records[i].getValue(cols[8]);
            if(idL != '- None -'){
                var number = records[i].getValue(cols[8]);
            }
            else {
                var number= records[i].getValue('tranid',null,'GROUP');
            }
            var orderid = parseInt(records[i].getValue('internalid', null, 'GROUP'));
            rec["id"] = orderid;
            rec["rid"] = orderid.toString(radix);
            rec["number"] = number;
            var title = records[i].getValue(cols[7]) || '';
            if(title == '- None -'){
                title = '';
            }
            rec["title"] = title;
            var date = records[i].getValue(cols[3]) || '';
            //rec["date"] = records[i].getValue('trandate',null,'GROUP') || '';
            rec["date"] = date;
            var requestor= records[i].getText(cols[10]) ;
            var requestor1=records[i].getValue(cols[11]) ;
           // if(requestor=='- None -')
         //   {
                if(requestor1=='- None -' ||requestor1==null || requestor1==" " ) {
                    requestor = '';
                }
                else {
                    requestor=requestor1;
                }
            //}
            //rec["date"] = records[i].getValue('trandate',null,'GROUP') || '';
            rec["requestor"] = requestor;

            var type = records[i].getText('type',null,'GROUP') || '';
            var status = records[i].getText('status',null,'GROUP') || '';
            var wait = records[i].getValue('custbody_clgx_wtng_cust_response',null,'GROUP') || 'F';
            if (type == 'Service Order') {
                if (in_array(orderid, arrInvSOs)&&(type != 'Service Order' && status != 'Billed')){
                    rec["status"] = 7;
                }
                else if (type == 'Service Order' && status == 'Pending Approval'){
                    if(wait == 'F'){
                        rec["status"] = 3;
                    } else {
                        rec["status"] = 4;
                    }
                } else if (type == 'Service Order' && status == 'Pending Billing'){
                    rec["status"] = 5;
                } else if (type == 'Service Order' && status == 'Billed'){
                    rec["status"] = 6;
                }
            }
            else if(type == 'Opportunity' && status == 'In Progress'){
                rec["status"] = 1;
            } else if (type == 'Proposal' && (status == 'Open' || status=='Expired')){
                rec["status"] = 2;
            }  else {
                rec["status"] = 0;
            }
            arr.push(rec);
        }
    }
    obj["data"] = arr;
    if(csv){
        obj["csv"] = get_orders_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv);
    }
    obj["search"] = search;
    //obj["lists"] = {"types": types, "statuses": statuses};
    obj["lists"] = {};
    obj["direction"] = direction;
    obj["order_by"] = order_by;
    obj["page"] = page;
    obj["pages"] = pages;
    obj["per_page"] = per_page;
    obj["has_more"] = has_more;
    obj["start"] = start;
    obj["end"] = end;
    obj["total"] = total;

    return obj;
}

function get_orders_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv){

    var text = 'ID,Title,Date,Status\n';
    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",companyid));
    var columnsI = new Array();
    var filtersI = new Array();
    filtersI.push(new nlobjSearchFilter("name",null,"anyof",companyid));
    var invoices = nlapiSearchRecord('invoice','customsearch_clgx_api_inv_sos', filtersI, columnsI);
    var arrInvSOs=new Array();
    if(invoices!=null) {
        for (var i = 0; invoices != null && i < invoices.length; i++) {
            var searchInv = invoices[i];
            var columnsI = searchInv.getAllColumns();
            var soID = searchInv.getValue(columnsI[0]);
            arrInvSOs.push(soID);
        }
    }
    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",companyid));
    if(search){
        if(search.id){
            //filters.push(new nlobjSearchFilter("transactionnumber",null,"contains",search.id));
            // filters.push(new nlobjSearchFilter("tranid",null,"contains",search.id));
            var filters=[];
            filters =[ [['custbody_cologix_legacy_so', 'is', search.id ],
                'or',
                ['tranid', 'contains', search.id] ], 'and', ['entity', 'anyof', companyid]];

        }

        if(search.statuses){

            if(search.statuses == 1){
                filters.push(new nlobjSearchFilter("status",null,"anyof","Opprtnty:A"));
                if(search.id) {
                    var filters = [];
                    filters = [[['custbody_cologix_legacy_so', 'is', search.id],
                        'or',
                        ['tranid', 'contains', search.id]],
                        'and', ['entity', 'anyof', companyid],
                        'and', ['status', 'anyof', "Opprtnty:A"]
                    ];
                }


            }
            if(search.statuses == 2){
                filters.push(new nlobjSearchFilter("status",null,"anyof","Estimate:A"));
                if(search.id) {
                    var filters = [];
                    filters = [[['custbody_cologix_legacy_so', 'is', search.id],
                        'or',
                        ['tranid', 'contains', search.id]],
                        'and', ['entity', 'anyof', companyid],
                        'and', ['status', 'anyof', "Estimate:A"]
                    ];
                }

            }
            if(search.statuses == 3){
                filters.push(new nlobjSearchFilter("status",null,"anyof","SalesOrd:A"));
                filters.push(new nlobjSearchFilter("custbody_clgx_wtng_cust_response",null,"is","F"));

                if(search.id) {
                    var filters = [];
                    filters = [[['custbody_cologix_legacy_so', 'is', search.id],
                        'or',
                        ['tranid', 'contains', search.id]],
                        'and', ['entity', 'anyof', companyid],
                        'and', ['status', 'anyof', "SalesOrd:A"],
                        'and', ['custbody_clgx_wtng_cust_response', 'is', "F"],
                    ];
                }

            }
            if(search.statuses == 4){
                filters.push(new nlobjSearchFilter("status",null,"anyof","SalesOrd:A"));
                filters.push(new nlobjSearchFilter("custbody_clgx_wtng_cust_response",null,"is","T"));


                if(search.id) {
                    var filters = [];
                    filters = [[['custbody_cologix_legacy_so', 'is', search.id],
                        'or',
                        ['tranid', 'contains', search.id]],
                        'and', ['entity', 'anyof', companyid],
                        'and', ['status', 'anyof', "SalesOrd:A"],
                        'and', ['custbody_clgx_wtng_cust_response', 'is', "T"],
                    ];
                }

            }
            if(search.statuses == 5){
                filters.push(new nlobjSearchFilter("status",null,"anyof","SalesOrd:F"));
                if(search.id) {
                    var filters = [];
                    filters = [[['custbody_cologix_legacy_so', 'is', search.id],
                        'or',
                        ['tranid', 'contains', search.id]],
                        'and', ['entity', 'anyof', companyid],
                        'and', ['status', 'anyof', "SalesOrd:F"]
                    ];
                }

            }
            if(search.statuses == 6){
                filters.push(new nlobjSearchFilter("status",null,"anyof","SalesOrd:G"));
                if(search.id) {
                    var filters = [];
                    filters = [[['custbody_cologix_legacy_so', 'is', search.id],
                        'or',
                        ['tranid', 'contains', search.id]],
                        'and', ['entity', 'anyof', companyid],
                        'and', ['status', 'anyof', "SalesOrd:G"]
                    ];
                }

            }
        }
        if(search.from && search.to){
            filters.push(new nlobjSearchFilter("trandate",null,"within",search.from,search.to));
        }
        if(search.from && !search.to){
            filters.push(new nlobjSearchFilter("trandate",null,"on",search.from));
        }
        if(!search.from && search.to){
            filters.push(new nlobjSearchFilter("trandate",null,"on",search.to));
        }
    }
    var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders', filters, columns);

    for ( var i = 0; records != null && i < records.length; i++ ) {

        text += records[i].getValue('tranid',null,'GROUP') + ',';
        var cols = records[i].getAllColumns();
        var title = records[i].getValue(cols[7]) || '';
        if(title == '- None -'){
            title = '';
        }
        text +=title+',';
        var orderid = parseInt(records[i].getValue('internalid', null, 'GROUP'));
        var idL  = records[i].getValue(cols[3]);
        if(idL != '- None -'){
            text += idL + ',';
        }
        else {
            text += records[i].getValue(cols[3]) + ',';
        }

        var type = records[i].getText('type',null,'GROUP') || '';
        var status = records[i].getText('status',null,'GROUP') || '';
        var statusName='';
        var wait = records[i].getValue('custbody_clgx_wtng_cust_response',null,'GROUP') || 'F';
        if (type == 'Service Order') {
            if (in_array(orderid, arrInvSOs)&&(type != 'Service Order' && status != 'Billed')){
                statusName = "Billing";
            }
            else if (type == 'Service Order' && status == 'Pending Approval'){
                if(wait == 'F'){
                    statusName = "Provisioning";
                } else {
                    statusName  ="Customer Wait";
                }
            } else if (type == 'Service Order' && status == 'Pending Billing'){
                statusName = "Active";
            } else if (type == 'Service Order' && status == 'Billed'){
                statusName ="Closed";
            }
        }
        else if(type == 'Opportunity' && status == 'In Progress'){
            statusName = "In Review";
        } else if (type == 'Proposal' && (status == 'Open' || status=='Expired')){
            statusName = "In Progress";
        }  else {
            statusName ="";
        }

        text += statusName;
        text += '\n';
    }
    return text;
}

function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}