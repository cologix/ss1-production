nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Ports.js
//	ScriptID:	customscript_clgx_rl_cp_ports
//	ScriptType:	RESTlet
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	08/24/2018
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv) {

    //if(srights.xcs > 0){



    var getPorts=get_ports(companyid);
    obj["ports"]=getPorts[0];
    obj["locationsXC"]=getPorts[1];

    return obj;

});

function get_ports(id){
    var columns = new Array();
    var filters = new Array();

    var arrNNI=new Array();
    var ignoreIDs=[20510	,
        20512	,
        20545	,
        20546	,
        23494	,
        23528	,
        23529	,
        23532	,
        23533	,
        23660	,
        23661	,
        23662	,
        23663	,
        23689	,
        23753	,
        23754	,
        23912	,
        23913	,
        23973	,
        24039	,
        24040	,
        24225	,
        24312	,
        24521	,
        24551	,
        24552	,
        24789	,
        24790	,
        24978	,
        24979	,
        25152	,
        25153	,
        25567	,
        25596	,
        25597	,
        25851	,
        25852	,
        25940	,
        25941	,
        26364	,
        23491	,
        23396	,
        23612	,
        23613	,
        23492	,
        23493	,
        24873	,
        24874];
    var records = nlapiSearchRecord('customrecord_cologix_crossconnect', 'customsearch_clgx_xc_80_4xsoldbandwidt_2', filters, columns);
    if (records != null) {

        for (var j = 0; records != null && j < records.length; j++) {
            var searchAP = records[j];
            var columns = searchAP.getAllColumns();
            arrNNI.push( searchAP.getValue(columns[0]));
        }
    }

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
    var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders_3', filters, columns);
    var locationsXC = new Array();
    for ( var i = 0; records != null && i < records.length; i++ ) {
        var searchAP = records[i];
        var columns = searchAP.getAllColumns();
        var loc =searchAP.getValue(columns[0]);
        locationsXC.push(loc);
    }

    var arr=new Array();
    arr=[];
    var objFnal=new Object();
    var arrObj=new Array();
    var columns = new Array();
    var filters = new Array();
    //  [0] = {nlobjSearchFilter} isinactive   is false
    // noneof
    //  [1] = {nlobjSearchFilter} custrecord_clgx_xc_facility
    //   [2] = {nlobjSearchFilter} custrecord_cologix_xc_type anyof
    filters.push(new nlobjSearchFilter('custrecord_clgx_xcex_customer_id', null, 'equalto', id));
    filters.push(new nlobjSearchFilter('internalid', null, 'noneof', ignoreIDs));

    var records = nlapiSearchRecord('customrecord_cologix_crossconnect', 'customsearch_clgx_api_xcs_clouds_2_2_2', filters , columns);

    if(records!=null) {

        for (var j = 0; records != null && j < records.length; j++) {
            var searchAP = records[j];
            var columns = searchAP.getAllColumns();
            var name = searchAP.getValue(columns[0]);
            var portid = searchAP.getValue(columns[3]);
            var speed = searchAP.getText(columns[1]);
            var status = searchAP.getValue(columns[5]);
            var statusA =searchAP.getText(columns[5]);
            var aEnd=searchAP.getValue(columns[8]);
            var xcid=searchAP.getValue(columns[9]);
            var soldBandwidth=searchAP.getValue(columns[10]);
            var cProvider=searchAP.getValue(columns[11]);
            var nniProvider=searchAP.getValue(columns[12]);
            //var thenumspeed = 0;

            var thenumspeed = speed.match(/\d/g);
            if(thenumspeed!=null && thenumspeed!='') {
                thenumspeed = thenumspeed.join("");
            }
            else{
                thenumspeed=0;
            }

            if(aEnd==""){
                var statusA ='pending install';
                status = 1;
            }
            else {

                if (status == '') {
                    // set the state
                    status = 1;
                    var statusA = 'pending EVC';
                }
                if (statusA == 'non-existing' || statusA == 'nonexisting') {
                    // set the state
                    var statusA = 'pending EVC';
                }
            }
            var location= searchAP.getText(columns[4]);
            var location_id= searchAP.getValue(columns[6]);
            var type= searchAP.getValue(columns[7]);
            if(type==2){
                var ha=1;
            }
            else
            {
                var ha=0;
            }
            var addEVC=1;
            if(thenumspeed!=0) {
                if ((parseInt(soldBandwidth) >= parseInt(4000) * parseInt(thenumspeed)) && (cProvider == '')) {
                    addEVC = 2;
                }
            }
            var pProvider=0;
            if(thenumspeed!=0) {
                if ((parseInt(soldBandwidth) >= parseInt(4000) * parseInt(thenumspeed)) && (cProvider != '')) {
                    nlapiLogExecution('DEBUG', 'DEBUG', 'speed*4: ' + parseInt(4000) * parseInt(thenumspeed));
                    pProvider = 1;
                }
            }

            var obj = new Object();
            obj.id = portid;
            obj.rid = portid;
            obj.name = name;
            obj.speed = speed;
            obj.provider = "";
            obj.skey = "";
            obj.status = status;
            obj.ha = ha;
            obj.addevc = addEVC;
            obj.pProvider = pProvider;
            obj.nniProvider = nniProvider;
            obj.arrNNI=arrNNI;
            obj.delete = 1;
            obj.edit = 1;
            obj.scart = 0;
            obj.location = location;
            obj.statusA = statusA;
            obj.red = 2;
            obj.location_id = location_id;
            obj.vlans="";
            obj.so="";

            arrObj.push(obj);
            var columnsv = new Array();
            var filtersv = new Array();
            filtersv.push(new nlobjSearchFilter("custrecord_clgx_cloud_connect_port_z",null,"anyof",parseInt(portid)));
            var records1 = nlapiSearchRecord('customrecord_cologix_vxc', 'customsearch6885', filtersv, columnsv);
            if(records1!=null) {

                for (var i = 0; records1 != null && i < records1.length; i++) {
                    var searchAP1 = records1[i];
                    var columns1 = searchAP1.getAllColumns();
                    var name = '&nbsp&nbsp&nbsp'+searchAP1.getValue(columns1[0]);
                    var evcid = searchAP1.getValue(columns1[4]);
                    var speed = searchAP1.getText(columns1[1]);
                    var provider = searchAP1.getText(columns1[2]);
                    var skey = searchAP1.getValue(columns1[3]);
                    var status1 = searchAP1.getValue(columns1[7]);
                    var statusA = searchAP1.getValue(columns1[8]);
                    var zEVlan = searchAP1.getValue(columns1[9]);
                    if(zEVlan!=null){
                        arrObj[j].vlans=arrObj[j].vlans+zEVlan+';';
                    }
                    if(status1!=3){
                        var statusA =searchAP1.getText(columns1[7]);
                    }
                    if(status1=='' || status1=='nonexisting' || status1=='nonexisting'){
                        var statusA ='pending';
                        status1=1;
                    }
                    if(statusA=='non-existing' || statusA=='nonexisting'){
                        var statusA ='pending';
                    }
                    var obj = new Object();
                    obj.id = evcid;
                    obj.rid = portid;
                    obj.name = name;
                    obj.namee = searchAP1.getValue(columns1[0]);
                    obj.so ="";
                    obj.speed = speed;
                    obj.provider =provider;
                    obj.skey = skey;
                    obj.portxc= searchAP1.getText(columns1[5]);
                    obj.status = status1;
                    obj.ha =ha;
                    obj.addevc = 0;
                    obj.delete = 1;
                    obj.edit = 1;
                    obj.scart = 0;
                    obj.location = location;
                    obj.statusA = statusA;
                    obj.red = 2;
                    obj.vnni=searchAP1.getValue(columns1[11]);
                    arrObj.push(obj);
                }
            }




        }
    }


    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("name","custrecord_clgx_active_port_so","anyof",id));
    var arrayAPorts=new Array();

    var records = nlapiSearchRecord('customrecord_clgx_active_port','customsearch_clgx_ss_rl_cp_ports', filters , columns);
    if(records!=null) {

        for (var j = 0; records != null && j < records.length; j++) {
            var searchAP = records[j];
            var columns = searchAP.getAllColumns();
            var idAPort = searchAP.getValue(columns[0]);
            arrayAPorts.push(idAPort);
        }
    }

    var columns = new Array();
    var filters = new Array();

    //filters.push(new nlobjSearchFilter("custrecord_clgx_a_end_port", null, "anyof", arrayAPorts));
    filters.push(new nlobjSearchFilter("name","custrecord_xconnect_service_order","anyof",id));
    filters.push(new nlobjSearchFilter('internalid', null, 'noneof', ignoreIDs));

    var records = nlapiSearchRecord('customrecord_cologix_crossconnect', 'customsearch_clgx_api_xcs_clouds_2_2', filters, columns);
    var aux = '';
    if (records != null) {

        for (var j = 0; records != null && j < records.length; j++) {
            var searchAP = records[j];
            var columns = searchAP.getAllColumns();
            var redundant = 0;
            var name = searchAP.getValue(columns[0]);
            var portid = searchAP.getValue(columns[3]);
            var speed = searchAP.getText(columns[1]);
            var status = searchAP.getValue(columns[5]);
            var statusA = searchAP.getText(columns[5]);
            var aEnd = searchAP.getValue(columns[8]);
            var soldBandwidth=searchAP.getValue(columns[12]);
            var cProvider=searchAP.getValue(columns[13]);
            var nniProvider=searchAP.getValue(columns[14]);
            var thenumspeed =0;
            var thenumspeed = speed.match(/\d/g);
            if(thenumspeed!=null && thenumspeed!='') {
                thenumspeed = thenumspeed.join("");
            } else {
                thenumspeed =0;
            }

            nlapiLogExecution('DEBUG','DEBUG', 'cProvider: ' + cProvider+'Id: '+name+'speed'+thenumspeed);
            if ((in_array(aEnd, arrayAPorts) ) || aEnd == '') {
                var so = searchAP.getValue(columns[9]);
                var xcid = searchAP.getValue(columns[10]);
                var service = searchAP.getValue(columns[11]);
                if (aEnd == "") {
                    var statusA = 'pending install';
                    status = 1;
                } else {
                    if (status == '') {
                        // set the state
                        status = 1;
                        var statusA = 'pending EVC';
                    }
                    if (statusA == 'non-existing' || statusA == 'nonexisting') {
                        // set the state
                        var statusA = 'pending EVC';
                    }
                }
                var location = searchAP.getText(columns[4]);
                var location_id = searchAP.getValue(columns[6]);
                var type = searchAP.getValue(columns[7]);
                if (type == 2) {
                    var ha = 1;
                } else {
                    var ha = 0;
                }
                var addEVC=1;
                if(thenumspeed!=0) {
                    if ((parseInt(soldBandwidth) >= parseInt(4000) * parseInt(thenumspeed)) && (cProvider == '')) {
                        nlapiLogExecution('DEBUG', 'DEBUG', 'speed*4: ' + parseInt(4000) * parseInt(thenumspeed));
                        addEVC = 2;
                    }
                }
                var pProvider=0;
                if(thenumspeed!=0) {
                    if ((parseInt(soldBandwidth) >= parseInt(4000) * parseInt(thenumspeed)) && (cProvider != '')) {
                        nlapiLogExecution('DEBUG', 'DEBUG', 'speed*4: ' + parseInt(4000) * parseInt(thenumspeed));
                        pProvider = 1;
                    }
                }

                var obj = new Object();
                obj.id = portid;
                obj.rid = portid;
                obj.name = name;
                obj.speed = speed;
                obj.provider = "";
                obj.skey = "";
                obj.status = status;
                obj.ha = ha;
                obj.addevc = addEVC;
                obj.pProvider = pProvider;
                obj.nniProvider = nniProvider;
                obj.arrNNI=arrNNI;
                obj.delete = 1;
                obj.edit = 1;
                obj.scart = 0;
                obj.location = location;
                obj.statusA = statusA;
                obj.red = 2;
                obj.location_id = location_id;
                obj.vlans = "";
                obj.xcid = xcid;
                obj.so = so;
                obj.service = service;
                obj.redundant = '';

                for (var k = 0; arrObj != null && k < arrObj.length; k++) {
                    if (arrObj[k].service == service && arrObj[k].addevc == 1) {
                        obj.redundant = arrObj[k].name;
                        arrObj[k].redundant = name;
                    }

                }


                arrObj.push(obj);
                var columnsv = new Array();
                var filtersv = new Array();
                filtersv.push(new nlobjSearchFilter("custrecord_clgx_cloud_connect_port_z", null, "anyof", parseInt(portid)));
                var records1 = nlapiSearchRecord('customrecord_cologix_vxc', 'customsearch6885', filtersv, columnsv);
                if (records1 != null) {

                    for (var i = 0; records1 != null && i < records1.length; i++) {
                        var searchAP1 = records1[i];
                        var columns1 = searchAP1.getAllColumns();
                        var name = '&nbsp&nbsp&nbsp' + searchAP1.getValue(columns1[0]);
                        var evcid = searchAP1.getValue(columns1[4]);
                        var speed = searchAP1.getText(columns1[1]);
                        var provider = searchAP1.getText(columns1[2]);
                        var skey = searchAP1.getValue(columns1[3]);

                        var status1 = searchAP1.getValue(columns1[7]);
                        var statusA = searchAP1.getValue(columns1[8]);
                        var zEVlan = searchAP1.getValue(columns1[9]);
                        vlan = zEVlan;
                        if (zEVlan == null) {
                            zEVlan = 2;
                            var vlan = '';
                        }
                        if (status1 != 3) {
                            var statusA = searchAP1.getText(columns1[7]);
                        }
                        if (status1 == '' || status1 == 'nonexisting' || status1 == 'nonexisting') {
                            var statusA = 'pending';
                            status1 = 1;
                        }
                        if (statusA == 'non-existing' || statusA == 'nonexisting') {
                            var statusA = 'pending';
                        }
                        var obj = new Object();
                        obj.id = evcid;
                        obj.rid = portid;
                        obj.name = name;
                        obj.namee = searchAP1.getValue(columns1[0]);
                        obj.so = searchAP1.getValue(columns1[10]);
                        obj.speed = speed;
                        obj.provider = provider;
                        obj.skey = skey;
                        obj.portxc = searchAP1.getText(columns1[5]);
                        obj.status = status1;
                        obj.ha = ha;
                        obj.addevc = 0;
                        obj.delete = 1;
                        obj.edit = 1;
                        obj.scart = 0;
                        obj.location = location;
                        obj.statusA = statusA;
                        obj.red = 2;
                        obj.egress_vlan = zEVlan;
                        obj.vlan = vlan;
                        obj.vnni = searchAP1.getValue(columns1[11]);
                        arrObj.push(obj);
                    }
                }


            }
        }
    }



    return [arrObj,locationsXC];
}
//check if a value is in the array
function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}