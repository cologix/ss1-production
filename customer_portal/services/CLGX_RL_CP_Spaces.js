nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Spaces.js
//	ScriptID:	customscript_clgx_rl_cp_spaces
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=658&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/17/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv) {

	//if(srights.spaces > 0){
	if(srights.colocation > 0){
		
		var direction = datain['direction'] || 'ASC';
		var order_by = datain['order_by'] || '';
		var page = datain['page'] || 1;
		var per_page = datain['per_page'];
		var stype = datain['stype'];
		var search = datain['search'] || '';
		var csv = datain['csv'] || 0;
		
		obj["spaces"] = get_spaces(companyid,contactid,modifierid,radix,direction,order_by,page,per_page,stype,search,csv);
		
		var now = moment().format("M/D/YYYY h:mm:ss a");
		nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);
		
		obj.code = 'SUCCESS';
		obj.msg = 'You are managing spaces.';
		
	} else {
		obj.error = 'T';
		obj.code = 'NO_RIGHTS_SPACES';
		obj.msg = 'No rights for spaces.';
	}
	
	return obj;
	
});

function get_spaces(companyid,contactid,modifierid,radix,direction,order_by,page,per_page,stype,search,csv){

	var obj = {};
	var arr = [];
	var total = 0;
	var pages = 0;
	var start = 0;
	var end = 0;
	var has_more = false;

	var facilities = [{"value": 0, "text": '*'}];
	var facilitiesids = [];

	var sos = [{"value": 0, "text": '*'}];
	var sosids = [];
	
	search = JSON.parse(search);
	
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter("entity",null,"anyof",companyid));
	var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders', filters, columns);
	var ids = new Array();
	for ( var i = 0; records != null && i < records.length; i++ ) {
		ids.push(parseInt(records[i].getValue('internalid',null,'GROUP')));
	}
	
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter("custrecord_space_service_order",null,"anyof",ids));
	var records = nlapiSearchRecord('customrecord_cologix_space', 'customsearch_clgx_api_spaces', filters, columns);
	if(records){
		for ( var i = 0; records != null && i < records.length; i++ ) {
			
			var facilityid = parseInt(records[i].getValue('custrecord_cologix_space_location',null,null)) || 0;
			var facility = records[i].getText('custrecord_cologix_space_location',null,null) || '';
			if(_.indexOf(facilitiesids, facilityid) == -1 && facilityid){
				facilitiesids.push(facilityid);
				facilities.push({"value": facilityid, "text": facility});
			}
			facilities = _.sortBy(facilities, function(obj){ return obj.value;});
			
			var soid = parseInt(records[i].getValue('custrecord_space_service_order',null,null)) || 0;
			var name = (records[i].getText('custrecord_space_service_order',null,null)).split("#");
			var so = (name[name.length-1]).trim() || '';
			if(_.indexOf(sosids, soid) == -1 && soid){
				sosids.push(soid);
				sos.push({"value": soid, "text": so});
			}
			sos = _.sortBy(sos, function(obj){ return obj.value;});
			
		}
	}

	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter("custrecord_space_service_order",null,"anyof",ids));
	if(search){
		if(search.id){
			filters.push(new nlobjSearchFilter("name",null,"contains",search.id));
		}
		if(search.sos){
			filters.push(new nlobjSearchFilter("custrecord_space_service_order",null,"anyof",search.sos));
		}
		if(search.facilities){
			filters.push(new nlobjSearchFilter("custrecord_cologix_space_location",null,"anyof",search.facilities));
		}
    }
	var records = nlapiSearchRecord('customrecord_cologix_space', 'customsearch_clgx_api_spaces', filters, columns);
	
	if(records){
		
		total = records.length;
		pages = Math.ceil(total / per_page);
		page = (parseInt(page) > pages ? pages : parseInt(page));
		has_more = (pages > 1 ? true : false);
		start = (page - 1) * per_page;
		end = (start + per_page > total ? total : start + per_page);
		
		for ( var i = start; i < end; i++ ) {
			
			var rec = new Object();
			var spaceid = parseInt(records[i].getValue('internalid',null,null));
			rec["id"] = spaceid;
			rec["rid"] = spaceid.toString(radix);
			rec["number"] = records[i].getValue('name',null,null);
			
			var soid = parseInt(records[i].getValue('custrecord_space_service_order',null,null)) || 0;
			rec["soid"] = soid;
			rec["rsoid"] = soid.toString(radix);
			var name = (records[i].getText('custrecord_space_service_order',null,null)).split("#");
			rec["so"] = (name[name.length-1]).trim();
			
			rec["facilityid"] = parseInt(records[i].getValue('custrecord_cologix_space_location',null,null)) || 0;
			rec["facility"] = records[i].getText('custrecord_cologix_space_location',null,null) || '';
			arr.push(rec);
		}
	}
	obj["data"] = arr;
	if(csv){
		obj["csv"] = get_spaces_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv);
	}
	obj["search"] = search;
	obj["lists"] = {"sos": sos,"facilities": facilities};
	obj["direction"] = direction;
	obj["order_by"] = order_by;
	obj["page"] = page;
	obj["pages"] = pages;
	obj["per_page"] = per_page;
	obj["has_more"] = has_more;
	obj["start"] = start;
	obj["end"] = end;
	obj["total"] = total;
	
	return obj;
}
function get_spaces_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv){
	
	var text = 'ID,Service Order,Facility\n';
	
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter("entity",null,"anyof",companyid));
	var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders', filters, columns);
	var ids = new Array();
	for ( var i = 0; records != null && i < records.length; i++ ) {
		ids.push(parseInt(records[i].getValue('internalid',null,'GROUP')));
	}
	
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter("custrecord_space_service_order",null,"anyof",ids));
	if(search){
		if(search.id){
			filters.push(new nlobjSearchFilter("name",null,"contains",search.id));
		}
		if(search.sos){
			filters.push(new nlobjSearchFilter("custrecord_space_service_order",null,"anyof",search.sos));
		}
		if(search.facilities){
			filters.push(new nlobjSearchFilter("custrecord_cologix_space_location",null,"anyof",search.facilities));
		}
    }
	var records = nlapiSearchRecord('customrecord_cologix_space', 'customsearch_clgx_api_spaces', filters, columns);
	
	for ( var i = 0; records != null && i < records.length; i++ ) {
		
		text += records[i].getValue('name',null,null) + ',';
		var name = (records[i].getText('custrecord_space_service_order',null,null)).split("#");
		text += (name[name.length-1]).trim() + ',';
		text += records[i].getText('custrecord_cologix_space_location',null,null);
		text += '\n';

	}
	return text;
}