//------------------------------------------------------
//	Script:		CLGX_SS_CP_Order1.js
//	ScriptID:	customscript_clgx_ss_cp_order1
//	ScriptType:	RESTlet
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	08/03/2016
//------------------------------------------------------

function update_proposal(){
    try{

        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');

        //retrieve script parameters
        var proposalid = nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_prop_id');
        var languagePortal=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_lan1')||'en';
        var title=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_title1');
        var companyid=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_company1');
        var facility=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_location');
        var qty=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_quantity');
        var strMemoOrder=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_memo');
        var requested=nlapiGetContext().getSetting('SCRIPT','custbody_clgx_target_install_date');
        var item=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_item');
        var rate=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_rate');
        var totalmrc=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_amount');
        var taxcode1=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_taxcode1');
        var taxcode2=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_taxcode2');
        var record = nlapiCreateRecord('opportunity');
        record.setFieldValue('title', title);
        record.setFieldValue('entity', companyid);
        record.setFieldValue('location', facility);
        record.setFieldValue('memo', strMemoOrder);
        //record.setFieldValue('forecasttype', 3);
        //record.setFieldValue('entitystatus', 11);
        // record.setFieldValue('statusRef', 'inProgress');
        record.setFieldValue('custbody_cologix_opp_sale_type', 2);
        // record.setFieldValue('memo', strMemo.substr(0,999));
        record.setFieldValue('leadsource', 73127);
        record.setFieldValue('custbody_clgx_target_install_date', requested);
        record.setLineItemValue('item', 'item', 1, item);
        record.setLineItemValue('item', 'quantity', 1, qty);
        record.setLineItemValue('item', 'rate', 1, rate);
        record.setLineItemValue('item', 'location', 1, facility);
        record.setLineItemValue('item', 'price', 1, 1);
        //record.setLineItemValue('item', 'pricelevels',1, -1);
        record.setLineItemValue('item', 'amount', 1, totalmrc);
        if (taxcode1 > 0) {
            record.setLineItemValue('item', 'taxcode', 1, taxcode1);
        }
        //item 244
        record.setLineItemValue('item', 'item', 2, 244);
        record.setLineItemValue('item', 'quantity', 2, qty);
        record.setLineItemValue('item', 'rate', 2, rate);
        record.setLineItemValue('item', 'amount', 2, 0);
        record.setLineItemValue('item', 'amount', 2, parseInt(qty) * parseInt(rate));
        record.setLineItemValue('item', 'location', 2, facility);
        record.setLineItemValue('item', 'price', 2, -1);
        record.setLineItemValue('item', 'pricelevels', 2, -1);
        if (taxcode2 > 0) {
            record.setLineItemValue('item', 'taxcode', 2, taxcode2);
        }
        try {
            var recordId = nlapiSubmitRecord(record, true, true);
        }
        catch (error) {
            nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
        }
        if (recordId != null) {
            var totals = clgx_transaction_totals(companyid, 'oppty', recordId);

            var recProposal = nlapiLoadRecord('estimate', proposalid);
            recProposal.setFieldValue('opportunity', recordId);
            nlapiSubmitRecord(recProposal, true, true);
            //create the SO
            var arrParam = new Array();
            arrParam['custscript_proposalid'] = proposalid;
            arrParam['custscript_userid'] = 642142;
            var status = nlapiScheduleScript('customscript_clgx_ss_so', null, arrParam);
            // var agreement=AgreementCreater(proposalid);
            // nlapiSendEmail(206211,206211,'agreement',agreement,null,null,null,null);
        }
        nlapiLogExecution('DEBUG','Finished Execution', '|-------------------------- Finished Scheduled Script --------------------------|');
    }
    catch (error){

        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
