nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_SS_CP_OrderCloudOPP.js
//	ScriptID:	customscript_clgx_ss_cp_ordcloudopp
//	ScriptType:	Scheuled
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	10/03/2016
//------------------------------------------------------

function create_orders() {

    nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');

    //retrieve script parameters

    var languagePortal=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_orOPP_lanc')||'en';
    var title=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_orOPP_title');
    var companyid=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_orOPP_company');
    var facility=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_orOPP_loc');
    // var providerName=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_orOPP_provider');
    var tfacility=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_orOPP_facT');
    var type=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_orOPP_type');
    var ratecard=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_orOPP_ratec');
    var serverIP=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_orOPP_server');
    var requestor=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_orOPP_req');
    var uname=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_orOPP_uname');
    var timestamp=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_orOPP_timestampc');
    var contactid=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_orOPP_contact');
    // nlapiLogExecution('DEBUG','providerName',providerName);
    nlapiLogExecution('DEBUG','requestor',requestor);
    var arrCustomerDetails = nlapiLookupField('customer',companyid,['salesrep','companyname','custentity_clgx_cust_ccm']);
    var salesRepID = arrCustomerDetails['salesrep'];
    var custCompName = arrCustomerDetails['companyname'];
    var ccm = arrCustomerDetails['custentity_clgx_cust_ccm'];
    var fname=nlapiLookupField('contact', contactid, 'firstname') || '';
    var lname=nlapiLookupField('contact', contactid, 'lastname') || '';
    var emailContact=nlapiLookupField('contact', contactid, 'email') || '';
    var custContactName=fname+' '+lname;
    var contactRec=nlapiLoadRecord('contact',requestor);
    var nameAtt=contactRec.getFieldValue('entitytitle');
    var strMemoOrder =
        ' On-Ramp Facility : ' + tfacility + ';\n\n' +
        ' Type : ' + type ;

    if(parseInt(salesRepID) > 0){
        var arrRepDetails = nlapiLookupField('employee',salesRepID,['entityid']);
        var salesRepName = arrRepDetails['entityid'];
    }
    else{
        var salesRepName = '';
    }
    var taxcode1 = 0;
    var taxcode2 = 0;

    //MTL
    var taxcode1 = 0;
    var taxcode2 = 0;

    //MTL
    if (facility == 5 || facility == 8 || facility == 9 || facility == 10 || facility == 11 || facility == 12 || facility == 27) {
        var taxcode1 = 308;
        var taxcode2 = 308;
    } else if (facility == 6 || facility == 13 || facility == 15) { //TOR
        var taxcode1 = 11;
        var taxcode2 = 11;
    }
    else if (facility == 7 || facility == 28) { //VAN
        var taxcode1 = 297;
        var taxcode2 = 297;
    }
    else if (facility == 34 || facility == 39 || facility == 38) { //COL
        var taxcode1 = 515;
        var taxcode2 = 515;
    }
    else if (facility == 31 || facility == 40) { //JAX
        var taxcode1 = 498;
        var taxcode2 = 498;
    }
    else if (facility == 42) { //LAK
        var taxcode1 = 565;
        var taxcode2 = 565;
    }
    else if (facility == 53 || facility == 54 || facility == 55|| facility==56) { //NNJ
        var taxcode1 = -635;
        var taxcode2 = -635;
    }
    var date = new Date();


    //PORT ORDER
    var record = nlapiCreateRecord('opportunity');
    record.setFieldValue('title', title);
    record.setFieldValue('entity', companyid);
    record.setFieldValue('memo', strMemoOrder.substr(0,999));
    record.setFieldValue('custbody_clgx_so_portal_req', requestor);
    record.setFieldValue('location', facility);
    record.setFieldValue('forecasttype', 3);
    record.setFieldValue('entitystatus', 11);
    // record.setFieldValue('memo', strMemo.substr(0,999));
    // record.setFieldValue('statusRef', 'inProgress');
    record.setFieldValue('custbody_cologix_opp_sale_type', 2);
    record.setFieldValue('leadsource', 73127);

    try {
        var recordId = nlapiSubmitRecord(record, true, true);

    }
    catch (error) {
        nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
    }
    if (recordId != null) {
        var totals = clgx_transaction_totals(companyid, 'oppty', recordId);
        var proposalNumber= nlapiLookupField('opportunity',recordId,'tranid');
        if (ratecard != 0) {
            if(languagePortal=='en')
            {

                var emailSubjectProposal="New Order "+proposalNumber+" - "+custCompName;
                var agree='The initial term of this Service Order shall be one (1) month and shall automatically renew for successive one (1) month terms, unless terminated by either party, in writing, at least one (1) month prior to the end of the then current term. Your Cologix rate card rates apply for the ordered services.\n\n Notwithstanding anything to the contrary in the agreement between the parties, during the Term, all service fees identified above shall increase on each 12-month anniversary of the Service Commencement Date in an amount equal to the greater of three percent (3%) or CPI. Such increase shall be automatically invoiced to Customer upon each 12-month anniversary of the Service Commencement Date.\n\nEach of Customer and Cologix hereby consents to the execution of this Service Order by electronic means.\n';
                var emailBodyProposal = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
                    'Thank you for submitting your order. A Cologix employee will be contacting you soon regarding the installation of your services. Please allow up to 24 hours for this order to be visible in the Portal.\n\n' +
                    'New Order : ' + proposalNumber + '\n' +
                    'Title : ' + title + '\n' +
                    'Portal Username : ' + uname + '\n' +
                    'Server Date and Time : ' + timestamp + '\n' +
                    'Server Name : ' + serverIP + '\n' +
                    'Contractual Terms Agreed To: '+agree+'\n'+
                    'Sincerely,\n' +
                    salesRepName + '\n\n';
            }
            else if(languagePortal=='fr')
            {
                var emailSubjectProposal="Nouvelle Commande "+proposalNumber+" - "+custCompName;
                var agree='La durée initiale de cette ordonnance de service est de un (1) mois et se renouvelle automatiquement pour les termes successifs d’un (1) mois, à moins d’être résilié par l’un ou l’autre des parties, par écrit, au moins un (1) mois avant la fin du Terme actuel. Vos tarifs de carte tarifaire Cologix s’appliquent pour les services commandés.\n\nThe initial term of this Service Order shall be one (1) month and shall automatically renew for successive one (1) month terms, unless terminated by either party, in writing, at least one (1) month prior to the end of the then current term. Your Cologix rate card rates apply for the ordered services. La durée initiale de cette ordonnance de service est de un (1) mois et se renouvelle automatiquement pour les termes successifs d’un (1) mois, à moins d’être résilié par l’un ou l’autre des parties, par écrit, au moins un (1) mois avant la fin du Terme actuel. Vos tarifs de carte tarifaire Cologix s’appliquent pour les services commandés. Notwithstanding anything to the contrary in the agreement between the parties, during the Term, all service fees identified above shall increase on each 12-month anniversary of the Service Commencement Date in an amount equal to the greater of three percent (3%) or CPI. Such increase shall be automatically invoiced to Customer upon each 12-month anniversary of the Service Commencement Date. Malgré toute disposition contraire dans le contrat conclu entre les parties, pendant la durée, tous les frais de service précisés plus haut augmenteront automatiquement tous les douze (12) mois à compter de la date anniversaire de la date de début du service, en fonction d’un taux égal à 3 % ou à l’indice des prix à la consommation, selon le plus élevé de ces deux taux.\n\nChacune des parties, le Client et Cologix, acceptent par les présentes que la signature et le passage de cette Commande de Service soient fait par moyen électronique.\n';
                var emailBodyProposal = 'Bonjour ' + custContactName + ' (' + custCompName + '),\n\n' +
                    "Nous vous remercions d'avoir envoyé votre commande. Un employé de Cologix vous contactera rapidement concernant l'installation de vos services. Veuillez laisser jusqu'à 24 heures pour que cette commande soit visible dans le portail.\n\n" +
                    'Nouvelle Order : ' + proposalNumber + '\n' +
                    'Titre : ' + title + '\n' +
                    'Nom d’utilisateur du portail : ' + uname + '\n' +
                    'Date et heure du serveur : ' + timestamp + '\n' +
                    'Nom du serveur : ' + serverIP + '\n' +
                    'Termes contractuels convenue par: '+agree+'\n'+
                    'Au plaisir de vous être utile,\n' +
                    salesRepName + '\n\n';
            }
        }
        else{
            var emailSubjectProposal="New Order "+proposalNumber+" - "+title;
            var agree='The initial term of this Service Order shall be one (1) month and shall automatically renew for successive one (1) month terms, unless terminated by either party, in writing, at least one (1) month prior to the end of the then current term. The Cologix standard rates apply for the ordered services.\n\nNotwithstanding anything to the contrary in the agreement between the parties, during the Term, all service fees identified above shall increase on each 12-month anniversary of the Service Commencement Date in an amount equal to the greater of three percent (3%) or CPI. Such increase shall be automatically invoiced to Customer upon each 12-month anniversary of the Service Commencement Date.\n\nEach of Customer and Cologix hereby consents to the execution of this Service Order by electronic means.\n'
            var emailBodyProposal = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
                'Thank you for submitting your order.  A Cologix employee will be contacting you soon regarding the installation of your services.\n\n' +
                'New Order :' + proposalNumber + '\n' +
                'Title : ' + title + '\n' +
                'Portal Username : ' + uname + '\n' +
                'Server Date and Time : ' + timestamp + '\n' +
                'Server Name : ' + serverIP + '\n\n' +
                'Contractual Terms Agreed To: '+agree+'\n'+
                'Sincerely,\n' +
                salesRepName + '\n\n';
            if(languagePortal=='fr')
            {
                var emailSubjectProposal="Nouvelle Commande "+proposalNumber+" - "+title;
                var agree='La durée initiale de cette ordonnance de service est de un (1) mois et se renouvelle automatiquement pour les termes successifs d’un (1) mois, à moins d’être résilié par l’un ou l’autre des parties, par écrit, au moins un (1) mois avant la fin du Terme actuel. Les tarifs standard de Cologix s’appliquent pour les services commandés.\n\nMalgré toute disposition contraire dans le contrat conclu entre les parties, pendant la durée, tous les frais de service précisés plus haut augmenteront automatiquement tous les douze (12) mois à compter de la date anniversaire de la date de début du service, en fonction d’un taux égal à 3 % ou à l’indice des prix à la consommation, selon le plus élevé de ces deux taux.\n\nChacune des parties, le Client et Cologix, acceptent par les présentes que la signature et le passage de cette Commande de Service soient fait par moyen électronique.\n'
                var emailBodyProposal = 'Bonjour ' + custContactName + ' (' + custCompName + '),\n\n' +
                    "Nous vous remercions d'avoir soumis votre commande. Un employé Cologix vous contactera bientôt concernant l'installation de vos services.\n\n" +
                    'Nouvelle Commande :'+proposalNumber + '\n' +
                    'Titre : ' + title + '\n' +
                    'Nom d’utilisateur du portail : ' + uname + '\n' +
                    'Date et heure du serveur : ' + timestamp + '\n' +
                    'Nom du serveur : ' + serverIP + '\n' +
                    'Termes contractuels convenue par: '+agree+'\n'+
                    'Au plaisir de vous être utile,\n' +
                    salesRepName + '\n\n';
            }
        }
        nlapiSendEmail(salesRepID,contactid,emailSubjectProposal,emailBodyProposal,null,null,null,null,true);
        if(ccm!=null) {
            nlapiSendEmail(salesRepID, salesRepID, emailSubjectProposal, emailBodyProposal, null, null, null, null, true);
        }
    }

    var recordQ = nlapiCreateRecord('customrecord_clgx_cp_q_transactions');
    recordQ.setFieldValue('custrecord_clgx_cp_tq_oppid', recordId);
    recordQ.setFieldValue('custrecord_clgx_cp_tq_cust', companyid);
    recordQ.setFieldValue('custrecord_clgx_cp_tq_contact', contactid);
    recordQ.setFieldValue('custrecord_clgx_cp_tq_conemail', emailContact);
    recordQ.setFieldValue('custrecord_clgx_cp_tq_ratecard', ratecard);
    recordQ.setFieldValue('custrecord_clgx_cp_tq_portalserver', serverIP);
    recordQ.setFieldValue('custrecord_clgx_cp_tq_trpr', 'T');
    recordQ.setFieldValue('custrecord_clgx_cp_tq_agproc', 'T');
    recordQ.setFieldValue('custrecord_clgx_cp_tq_xcpr', 'T');
    var recordQID = nlapiSubmitRecord(recordQ, true, true);
}