nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_SS_CP_Order.js
//	ScriptID:	customscript_clgx_ss_cp_order
//	ScriptType:	RESTlet
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	08/03/2016
//------------------------------------------------------

function update_proposal(){
    try{

        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');

        //retrieve script parameters
        var proposalid = nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_id');

        var languagePortal=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_lan')||'en';
        var titleProp=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_title');
        var companyid=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_company');
        var contactid=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_contact');
        var trq=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_trq')||'';
        var arrCustomerDetails = nlapiLookupField('customer',companyid,['salesrep','companyname']);
        var salesRepID = arrCustomerDetails['salesrep'];
        var custCompName = arrCustomerDetails['companyname'];
        var fname=nlapiLookupField('contact', contactid, 'firstname') || '';
        var lname=nlapiLookupField('contact', contactid, 'lastname') || '';
        var custContactName=fname+' '+lname;
        if(parseInt(salesRepID) > 0){
            var arrRepDetails = nlapiLookupField('employee',salesRepID,['entityid']);
            var salesRepName = arrRepDetails['entityid'];
        }
        // var proposalid = '1203353';

        var linkToFolder = '/app/common/media/mediaitemfolders.nl?folder=';
        var fileFolder = get_contract_fileFolder();
        linkToFolder += fileFolder;

        var recProposal = nlapiLoadRecord('estimate', proposalid);
        var stSONbr = recProposal.getFieldValue('tranid');
        var stSORep = recProposal.getFieldText('salesrep');
        var stSOCustId = recProposal.getFieldValue('entity');
        var stSOAddr = recProposal.getFieldValue('billaddress');
        var stCurrency = recProposal.getFieldValue('currency');
        var stSOAtt = recProposal.getFieldValue('custbody_clgx_contract_terms_attention');
        var stSON = recProposal.getFieldValue('custbody_clgx_contract_terms_notes');

        var billAddress1 = recProposal.getFieldValue('billaddr1');
        var billAddress2 = recProposal.getFieldValue('billaddr2');
        var billCity = recProposal.getFieldValue('billcity');
        var billCountry = recProposal.getFieldValue('billcountry');
        var billState = recProposal.getFieldValue('billstate');
        var billZipcode = recProposal.getFieldValue('billzip');


        if(stSON == null){
            stSON = '';
        }
        //nlapiLogExecution('DEBUG','stSONotes = ' + recProposal.getFieldValue('custbody_clgx_contract_terms_notes'));
        var stSONotes = convertBR(stSON);
        stSONotes = stSONotes.replace(/\&/g," and ");
        stSONotes = stSONotes.replace(/'/g, "&rsquo;");

        var stSOTitle = recProposal.getFieldValue('custbody_clgx_contract_terms_title');
        var stSOLang = nlapiLookupField('customrecord_clgx_contract_terms', stSOTitle,'custrecord_clgx_contract_terms_lang');

        var termsbody = recProposal.getFieldValue('custbody_clgx_contract_terms_body');
        if(termsbody == null){
            termsbody = '';
        }
        termsbody = termsbody.replace(/&/g,'and');

        if (stSOLang == 17){ // if French
            var stSOTerms = convertBR(termsbody);
            //var stSOTerms = HTMLEncode(termsbody);
            // var stSOTerms = htmlEscape(termsbody);

        }
        else{
            termsbody = termsbody.replace(/'/g, "&rsquo;");
            var stSOTerms = convertBR(termsbody);
        }

        var arrSOs = recProposal.getFieldValues('custbody_clgx_renewed_from_sos');
        var arrSOsNames = recProposal.getFieldTexts('custbody_clgx_renewed_from_sos');

        var powerUsage = recProposal.getFieldValue('custbody_clgx_pwusg_meter_power_usage');
        var usageRate = recProposal.getFieldValue('custbody_clgx_pwusg_util_rate');

        var commitKWUsage = recProposal.getFieldValue('custbody_clgx_pwusg_commit_util_kwh');
        var kwhburstrate = recProposal.getFieldValue('custbody_clgx_pwusg_commit_util_rate');

        var ipBurst = recProposal.getFieldValue('custbody_clgx_ip_burst');
        var ipBurstRate = recProposal.getFieldValue('custbody_clgx_ip_burst_rate');

        var powerType=recProposal.getFieldText('custbody_clgx_power_type_so');
        var highUtilization=recProposal.getFieldValue('custbody_clgx_hd_high_utilization');
        var highRate=recProposal.getFieldValue('custbody_clgx_hd_high_rate');
        var uhighUtilization=recProposal.getFieldValue('custbody_clgx_hd_ultra_high_utz');
        var uhighRate=recProposal.getFieldValue('custbody_clgx_hd_ultra_high_rate');
        var hdColo=recProposal.getFieldValue('custbody_clgx_hd_colo_so');

        //Determine currency
        var currencyType = '';
        if (stCurrency == 1){
            currencyType = 'USD';
        }
        else if (stCurrency == 3){
            currencyType = 'CDN';
        }
        else{}

        var custCompName = nlapiLookupField('customer', stSOCustId, 'companyname');
        custCompName = custCompName.replace(/\&/g,"and");
        custCompName = custCompName.replace(/\ /g,"_");

        /*
         var arrCustomerDetails = nlapiLookupField('customer', stSOCustId, ['companyname', 'billaddress1', 'billaddress2', 'billcity', 'billcountry', 'billstate', 'billzipcode', 'language']);
         var custCompName = arrCustomerDetails['companyname'];
         custCompName = custCompName.replace(/\&/g,"and");

         var billAddress1 = arrCustomerDetails['billaddress1'];
         var billAddress2 = arrCustomerDetails['billaddress2'];
         var billCity = arrCustomerDetails['billcity'];
         var billCountry = arrCustomerDetails['billcountry'];
         var billState = arrCustomerDetails['billstate'];
         var billZipcode = arrCustomerDetails['billzipcode'];
         */


        if (stSOLang == 17){ // if French
            var stLang = 1;
            var langPrefix = 'FR_';
            var filePrefix = 'SO_';
            var template = 3819552; // Load French Contract Template

        }
        else { // if English
            var stLang = 0;
            var langPrefix = 'EN_';
            var filePrefix = 'SO_';
            var template = 3819551; // Load English Contract Template

        }

        var arrTitleBQ = new Array();
        arrTitleBQ[0] = 'Quote Details';
        arrTitleBQ[1] = 'D&eacute;tails des Services';

        var arrTitleContract = new Array();
        arrTitleContract[0] = 'Services Details';
        arrTitleContract[1] = 'D&eacute;tails des Services';

        var arrTitleBQTotals = new Array();
        arrTitleBQTotals[0] = 'Quote Totals';
        arrTitleBQTotals[1] = 'D&eacute;tails des Services - Totaux ';

        var arrTitleContractTotals = new Array();
        arrTitleContractTotals[0] = 'Services Totals';
        arrTitleContractTotals[1] = 'D&eacute;tails des Services - Totaux ';

        var arrTitleMRC = new Array();
        arrTitleMRC[0] = 'RECURRING CHARGES';
        arrTitleMRC[1] = 'FRAIS R&Eacute;CURRENTS';

        var arrTitleNRC = new Array();
        arrTitleNRC[0] = 'NON RECURRING CHARGES';
        arrTitleNRC[1] = 'FRAIS NON R&Eacute;CURRENTS';

        var arrTitleDisc = new Array();
        arrTitleDisc[0] = 'CATEGORY : ONE TIME DISCOUNT';
        arrTitleDisc[1] = 'CAT&Eacute;GORIE: PREMIER MOIS GRATUIT';

        var arrFootMRC = new Array();
        arrFootMRC[0] = 'Total Recurring Charges';
        arrFootMRC[1] = 'Total des frais r&eacute;currents';

        var arrFootNRC = new Array();
        arrFootNRC[0] = 'Total Non Recurring Charges';
        arrFootNRC[1] = 'Total des frais non r&eacute;currents';


        var arrFootDisc = new Array();
        arrFootDisc[0] = 'Total Discounts';
        arrFootDisc[1] = 'Total r&eacute;ductions';


        var arrFootAllRC = new Array();
        arrFootAllRC[0] = 'Grand Total Recurring Charges';
        arrFootAllRC[1] = 'Grand total des frais r&eacute;currents';

        var arrFootAllNRC = new Array();
        arrFootAllNRC[0] = 'Grand Total Non Recurring Charges';
        arrFootAllNRC[1] = 'Grand total des frais non r&eacute;currents';

        var arrFootAllDisc = new Array();
        arrFootAllDisc[0] = 'Grand Total Discounts';
        arrFootAllDisc[1] = 'Grand total r&eacute;ductions';


        //
        var arrAttn = new Array();
        arrAttn[0] = 'Attn';
        arrAttn[1] = '&Agrave; l\'attention de';

        var arrName = new Array();
        arrName[0] = 'Name';
        arrName[1] = 'Nom';

        var arrDescr = new Array();
        arrDescr[0] = 'Description';
        arrDescr[1] = 'Description';

        var arrTerms = new Array();
        arrTerms[0] = 'Service Term';
        arrTerms[1] = 'P&eacute;riode de service';

        var arrQty = new Array();
        arrQty[0] = 'Qty';
        arrQty[1] = 'Quantit&eacute;';

        var arrRate = new Array();
        arrRate[0] = 'Rate';
        arrRate[1] = 'Tarif';

        var arrAmount = new Array();
        arrAmount[0] = 'Amount';
        arrAmount[1] = 'Montant';

        var arrUsageCharges = new Array();
        arrUsageCharges[0] = 'USAGE CHARGES';
        arrUsageCharges[1] = 'Frais d\'utilisation';




        var arrPowerUsage = new Array();
        arrPowerUsage[0] = powerType+' usage rate';
        arrPowerUsage[1] = 'Taux d\'utilisation de '+powerType;

        var arrKWHBurstRate = new Array();
        arrKWHBurstRate[0] = powerType+' overage rate';
        arrKWHBurstRate[1] = 'Prix au '+powerType+' modulable';

        var arrKWHCommitUsage = new Array();
        arrKWHCommitUsage[0] = 'Commited '+powerType+' usage';
        arrKWHCommitUsage[1] = 'Engagement par rapport au '+powerType+' consomm&eacute;';
        var  arrHDColo = new Array();
        arrHDColo[0] = 'High Utilization';
        arrHDColo[1] = 'High Utilization';
        var  arrUHDColo = new Array();
        arrUHDColo[0] = 'Ultra High Utlization';
        arrUHDColo[1] = 'Ultra High Utlization';


        var arrIPBurst = new Array();
        arrIPBurst[0] = 'IP Charges';
        //arrIPBurst[1] = 'Adresses IP modulable';
        arrIPBurst[1] = 'Frais adresses IP';


        var arrIPBurstItem = new Array();
        arrIPBurstItem[0] = 'IP Burst';
        arrIPBurstItem[1] = 'Adresses IP modulable';

        var arrIPBurstRate = new Array();
        //arrIPBurstRate[0] = 'IP Burst Rate';
        //arrIPBurstRate[1] = 'Prix des adresses IP modulable';
        arrIPBurstRate[0] = 'Rate';
        arrIPBurstRate[1] = 'Tarif';

        var arrNotes = new Array();
        arrNotes[0] = 'Notes';
        arrNotes[1] = 'Notes';

        var arrFromSOs = new Array();
        arrFromSOs[0] = 'This order is replacing the following Service Orders';
        arrFromSOs[1] = 'Cette commande remplace les ordres de service suivants';


        // start items sections --------------------------------------------------------------------------------
        var tabledata = '';

        var nbrItems = recProposal.getLineItemCount('item');
        // build array of unique locations
        var arrLocations = new Array();
        for (var i = 0; i < nbrItems; i++){
            var locid = recProposal.getLineItemValue('item','location', i + 1)
            if(!inArray(locid,arrLocations)){
                arrLocations.push(locid);
            }
        }

        // loop unique locations
        var grandTotalRC = 0;
        var grandTotalNRC = 0;
        var grandTotalDisc = 0;
        for (var l = 0; arrLocations != null && l < arrLocations.length; l++){
            var arrLocation = nlapiLookupField('location', arrLocations[l], ['name', 'address1', 'address2', 'city', 'country', 'state', 'zip']);
            var stLocName = arrLocation['name'];
            stLocName = stLocName.replace(/\&/g,"and");

            var arrNRC = new Array();
            var arrRC = new Array();
            var arrDisc = new Array();
            for (var m = 0; m < parseInt(nbrItems); m++) {
                var stBS = recProposal.getLineItemText('item', 'billingschedule', m + 1);
                var itemID = recProposal.getLineItemValue('item', 'item', m + 1);
                var stClass = recProposal.getLineItemText('item', 'class', m + 1);
                if (stClass.indexOf("NRC") > -1 && itemID != 549) {
                    arrNRC.push(m + 1);
                }
                else if(stClass.indexOf("Recurring") > -1 && itemID == 549){
                    arrDisc.push(m + 1);
                }
                else {
                    arrRC.push(m + 1);
                }
            }

            tabledata += '<table class="profile" align="center">';
            tabledata += '<tr>';

            tabledata += '<td><h2>' + arrTitleContract[stLang] + ' / ' + stLocName + '</h2></td>';

            tabledata += '</tr>';
            tabledata += '</table>';






            if (arrRC.length > 0) { // there are recurring items, so start recurring block --------------------------------------------------


                tabledata += '<table cellpadding="1" class="profile" align="center" table-layout="fixed">';
                tabledata += '<tr>';
                tabledata += '<td width="30%">&nbsp;</td>';
                tabledata += '<td width="40%">&nbsp;</td>';
                tabledata += '<td width="10%">&nbsp;</td>';
                tabledata += '<td width="10%">&nbsp;</td>';
                tabledata += '<td width="10%">&nbsp;</td>';
                tabledata += '</tr>';

                tabledata += '<tr>';
                tabledata += '<td colspan="5">' + arrTitleMRC[stLang] + '</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td colspan="5">&nbsp;</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td class="cool"><b>' + arrDescr[stLang] + '</b></td>';
                tabledata += '<td class="cool"><b>' + arrTerms[stLang] + '</b></td>';
                tabledata += '<td class="cool" align="center"><b>' + arrQty[stLang] + '</b></td>';
                tabledata += '<td class="cool" align="right"><b>' + arrRate[stLang] + '</b></td>';
                tabledata += '<td class="cool" align="right"><b>' + arrAmount[stLang] + '</b></td>';
                tabledata += '</tr>';

                var totalRC = 0;
                for (var j = 0; j < nbrItems; ++j) {

                    var currentLocation = recProposal.getLineItemValue('item', 'location', j + 1);
                    var currentBS = recProposal.getLineItemText('item', 'billingschedule', j + 1);
                    var currentID = recProposal.getLineItemValue('item', 'item', j + 1);

                    if(currentLocation == arrLocations[l] && currentBS != 'Non Recurring' && currentID != 549){

                        var stItemId = recProposal.getLineItemValue('item', 'item', j + 1);
                        var stItemType = recProposal.getLineItemValue('item', 'itemtype', parseInt(j + 1));
                        var stTerms = recProposal.getLineItemText('item', 'billingschedule', j + 1);
                        if(stItemType == 'Discount'){
                            var recItem = nlapiLoadRecord('discountitem', stItemId);
                        }
                        else if (stItemType == 'OthCharge'){
                            var recItem = nlapiLoadRecord('otherchargeitem', stItemId);
                        }
                        else{
                            var recItem = nlapiLoadRecord('serviceitem', stItemId);
                        }
                        var stName = recItem.getFieldValue('displayname');
                        if(stLang != 0){
                            var frenchName = recItem.getLineItemValue('translations', 'displayname', 3);
                            if (frenchName != null && frenchName != ''){
                                stName = frenchName;
                            }
                        }

                        var stQty = recProposal.getLineItemValue('item', 'quantity', parseInt(j + 1));
                        var stRate = recProposal.getLineItemValue('item', 'rate', parseInt(j + 1));
                        var stAmnt = recProposal.getLineItemValue('item', 'amount', parseInt(j + 1));

                        if (stItemId != 210) {
                            totalRC += parseFloat(stAmnt);
                            grandTotalRC += parseFloat(stAmnt);
                        }

                        tabledata += '<tr>';
                        tabledata += '<td>' + nlapiEscapeXML(stName) + '</td>';
                        if (stItemId == 210) {
                            tabledata += '<td>&nbsp;</td>';
                        }
                        else{
                            tabledata += '<td>' + nlapiEscapeXML(stTerms) + '</td>';
                        }

                        if (stQty == null || stItemId == 210) {
                            tabledata += '<td align="right">&nbsp;</td>';
                        }
                        else {
                            tabledata += '<td align="center">' + nlapiEscapeXML(parseFloat(stQty)) + '</td>';
                        }
                        if (stRate == null || stRate == '') {
                            tabledata += '<td align="right">&nbsp;</td>';
                        }
                        else {
                            tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stRate)) + '</td>';
                        }
                        if (stAmnt == null || stItemId == 210) {
                            tabledata += '<td align="right">&nbsp;</td>';
                        }
                        else {
                            tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stAmnt)) + '</td>';
                        }
                        tabledata += '</tr>';
                    }

                }

                tabledata += '<tr>';
                tabledata += '<td colspan="5">&nbsp;</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td colspan="5" class="profile t-border" align="right"><b>' + arrFootMRC[stLang] + ' (' + stLocName + '): ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(totalRC).toFixed(2))) + '</b></td>';
                tabledata += '</tr>';

                tabledata += '</table>';

            } // end recurring block ---------------------------------------------------------------------------


            //tabledata += '<br/><br/><br/>';


            if (arrNRC.length > 0) { // start non recurring block -----------------------------------------------

                tabledata += '<table cellpadding="1" class="profile" align="center" table-layout="fixed">';
                tabledata += '<tr>';
                tabledata += '<td width="30%">&nbsp;</td>';
                tabledata += '<td width="40%">&nbsp;</td>';
                tabledata += '<td width="10%">&nbsp;</td>';
                tabledata += '<td width="10%">&nbsp;</td>';
                tabledata += '<td width="10%">&nbsp;</td>';
                tabledata += '</tr>';

                tabledata += '<tr>';
                tabledata += '<td colspan="5">' + arrTitleNRC[stLang] + '</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td colspan="5">&nbsp;</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td class="cool" colspan="2"><b>' + arrDescr[stLang] + '</b></td>';
                tabledata += '<td class="cool" align="center"><b>' + arrQty[stLang] + '</b></td>';
                tabledata += '<td class="cool" align="right"><b>' + arrRate[stLang] + '</b></td>';
                tabledata += '<td class="cool" align="right"><b>' + arrAmount[stLang] + '</b></td>';
                tabledata += '</tr>';

                var totalNRC = 0;
                for (var k = 0; k < nbrItems; ++k) {

                    var currentLocation = recProposal.getLineItemValue('item', 'location', k + 1);
                    var currentBS = recProposal.getLineItemText('item', 'billingschedule', k + 1);

                    if(currentLocation == arrLocations[l] && currentBS == 'Non Recurring'){

                        var stItemId = recProposal.getLineItemValue('item', 'item', parseInt(k + 1));
                        var stItemType = recProposal.getLineItemValue('item', 'itemtype', parseInt(k + 1));
                        if(stItemType == 'Discount'){
                            var recItem = nlapiLoadRecord('discountitem', stItemId);
                        }
                        else{
                            var recItem = nlapiLoadRecord('serviceitem', stItemId);
                        }
                        var stName = recItem.getFieldValue('displayname');

                        if(stLang != 0){
                            var frenchName = recItem.getLineItemValue('translations', 'displayname', 3);
                            if (frenchName != null && frenchName != ''){
                                stName = frenchName;
                            }
                        }

                        var stQty = recProposal.getLineItemValue('item', 'quantity', parseInt(k + 1));
                        var stRate = recProposal.getLineItemValue('item', 'rate', parseInt(k + 1));
                        var stAmnt = recProposal.getLineItemValue('item', 'amount', parseInt(k + 1));

                        totalNRC += parseFloat(stAmnt);
                        grandTotalNRC += parseFloat(stAmnt);

                        tabledata += '<tr>';
                        tabledata += '<td colspan="2">' + nlapiEscapeXML(stName) + '</td>';
                        if (stQty == null || stQty == '') {
                            tabledata += '<td align="right">&nbsp;</td>';
                        }
                        else {
                            tabledata += '<td align="center">' + nlapiEscapeXML(parseFloat(stQty)) + '</td>';
                        }
                        if (stRate == null || stRate == '') {
                            tabledata += '<td align="right">&nbsp;</td>';
                        }
                        else {
                            tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stRate)) + '</td>';
                        }
                        if (stAmnt == null || stAmnt == '') {
                            tabledata += '<td align="right">&nbsp;</td>';
                        }
                        else {
                            tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stAmnt)) + '</td>';
                        }
                        tabledata += '</tr>';
                    }
                }

                tabledata += '<tr>';
                tabledata += '<td colspan="5">&nbsp;</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td colspan="5" class="profile t-border" align="right"><b>' + arrFootNRC[stLang] + ' (' + stLocName + '): ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(totalNRC).toFixed(2))) + '</b></td>';
                tabledata += '</tr>';

                tabledata += '</table>';

            } // end non recurring block ------------------------------------------------------------------------


            if (arrDisc.length > 0) { // there are first month discount items, so start discount block --------------------------------------------------

                tabledata += '<table cellpadding="1" class="profile" align="center" table-layout="fixed">';
                tabledata += '<tr>';
                tabledata += '<td width="30%">&nbsp;</td>';
                tabledata += '<td width="40%">&nbsp;</td>';
                tabledata += '<td width="10%">&nbsp;</td>';
                tabledata += '<td width="10%">&nbsp;</td>';
                tabledata += '<td width="10%">&nbsp;</td>';
                tabledata += '</tr>';

                tabledata += '<tr>';
                tabledata += '<td colspan="5">' + arrTitleDisc[stLang] + '</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td colspan="5">&nbsp;</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td class="cool"><b>' + arrName[stLang] + '</b></td>';
                tabledata += '<td class="cool"><b>' + arrDescr[stLang] + '</b></td>';
                //tabledata += '<td class="cool"><b>' + arrTerms[stLang] + '</b></td>';
                tabledata += '<td class="cool">&nbsp;</td>';
                //tabledata += '<td class="cool" align="center"><b>' + arrQty[stLang] + '</b></td>';
                tabledata += '<td class="cool" align="right"><b>' + arrRate[stLang] + '</b></td>';
                tabledata += '<td class="cool" align="right"><b>' + arrAmount[stLang] + '</b></td>';
                tabledata += '</tr>';

                var totalDisc = 0;
                for (var n = 0; n < nbrItems; ++n) {

                    var currentLocation = recProposal.getLineItemValue('item', 'location', n + 1);
                    var currentBS = recProposal.getLineItemText('item', 'billingschedule', n + 1);
                    var currentID = recProposal.getLineItemValue('item', 'item', n + 1);
                    var description = recProposal.getLineItemValue('item', 'description', n + 1);

                    if(currentLocation == arrLocations[l] && currentID == 549){

                        var stItemId = recProposal.getLineItemValue('item', 'item', n + 1);
                        var stItemType = recProposal.getLineItemValue('item', 'itemtype', parseInt(n + 1));
                        var stTerms = recProposal.getLineItemText('item', 'billingschedule', n + 1);
                        if(stItemType == 'Discount'){
                            var recItem = nlapiLoadRecord('discountitem', stItemId);
                        }
                        else{
                            var recItem = nlapiLoadRecord('serviceitem', stItemId);
                        }
                        var stName = recItem.getFieldValue('displayname');
                        var stDescription = recItem.getFieldValue('description');
                        if(stLang != 0){
                            var frenchName = recItem.getLineItemValue('translations', 'displayname', 3);
                            var frenchDescr = recItem.getLineItemValue('translations', 'description', 3);
                            if (frenchName != null && frenchName != ''){
                                stName = frenchName;
                            }
                        }

                        var stQty = recProposal.getLineItemValue('item', 'quantity', parseInt(n + 1));
                        var stRate = recProposal.getLineItemValue('item', 'rate', parseInt(n + 1));
                        var stAmnt = recProposal.getLineItemValue('item', 'amount', parseInt(n + 1));

                        totalDisc += parseFloat(stAmnt);
                        grandTotalDisc += parseFloat(stAmnt);

                        tabledata += '<tr>';
                        tabledata += '<td>' + nlapiEscapeXML(stName) + '</td>';
                        tabledata += '<td>' + nlapiEscapeXML(description) + '</td>';
                        //tabledata += '<td>&nbsp;</td>';
                        tabledata += '<td>&nbsp;</td>';
                        if (stRate == null || stRate == '') {
                            tabledata += '<td align="right">&nbsp;</td>';
                        }
                        else {
                            tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stRate)) + '</td>';
                        }
                        tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stAmnt)) + '</td>';
                        tabledata += '</tr>';
                    }

                }

                tabledata += '<tr>';
                tabledata += '<td colspan="5">&nbsp;</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td colspan="5" class="profile t-border" align="right"><b>' + arrFootDisc[stLang] + ' (' + stLocName + '): ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(totalDisc).toFixed(2))) + '</b></td>';
                tabledata += '</tr>';

                tabledata += '</table>';

            } // end discount block ---------------------------------------------------------------------------

        }
        if(arrLocations != null){
            if(parseInt(arrLocations.length) > 1 && (parseInt(grandTotalRC) > 0 || parseInt(grandTotalNRC) > 0)){

                //tabledata += '<br />';


                tabledata += '<table class="profile page-break" align="center">';
                tabledata += '<tr>';
                tabledata += '<td><h2>' + arrTitleContractTotals[stLang] + '</h2></td>';

                tabledata += '</tr>';
                tabledata += '</table>';

                tabledata += '<table cellpadding="1" class="profile" align="center" table-layout="fixed">';


                if(parseInt(grandTotalRC) > 0){
                    tabledata += '<tr>';
                    tabledata += '<td>&nbsp;</td>';
                    tabledata += '</tr>';
                    tabledata += '<tr>';
                    tabledata += '<td class="profile t-border" align="right"><b>' + arrFootAllRC[stLang] + ': ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(grandTotalRC).toFixed(2))) + '</b></td>';
                    tabledata += '</tr>';
                }
                if(parseInt(grandTotalNRC) > 0){
                    tabledata += '<tr>';
                    tabledata += '<td>&nbsp;</td>';
                    tabledata += '</tr>';
                    tabledata += '<tr>';
                    tabledata += '<td class="profile t-border" align="right"><b>' + arrFootAllNRC[stLang] + ': ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(grandTotalNRC).toFixed(2))) + '</b></td>';
                    tabledata += '</tr>';
                }
                if(parseInt(grandTotalDisc) > 0){
                    tabledata += '<tr>';
                    tabledata += '<td>&nbsp;</td>';
                    tabledata += '</tr>';
                    tabledata += '<tr>';
                    tabledata += '<td class="profile t-border" align="right"><b>' + arrFootAllDisc[stLang] + ': ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(grandTotalDisc).toFixed(2))) + '</b></td>';
                    tabledata += '</tr>';
                }
                tabledata += '</table>';
            }
        }

        //build customer address
        var tablehtmlAddress = '';
        tablehtmlAddress = '<table cellpadding="0" border="0" table-layout="fixed">';
        tablehtmlAddress += '<tr>';
        tablehtmlAddress += '<td>';
        tablehtmlAddress += '<b>' + nlapiEscapeXML(custCompName) + '</b><br/>';
        tablehtmlAddress += '<b>' + arrAttn[stLang] + ':</b>' + nlapiEscapeXML(stSOAtt) + '<br/>';
        tablehtmlAddress += nlapiEscapeXML(billAddress1) + '<br/>';
        //if (billAddress2) {
        //	tablehtmlAddress += nlapiEscapeXML(billAddress2) + '<br/>';
        //}
        tablehtmlAddress += nlapiEscapeXML(billCity) + ', ' + nlapiEscapeXML(billState) + ' ' + nlapiEscapeXML(billZipcode) + ' <br/>';
        tablehtmlAddress += nlapiEscapeXML(billCountry);
        tablehtmlAddress += '</td>';
        tablehtmlAddress += '</tr>';
        tablehtmlAddress += '</table>';

        //build data center/location address

        var arrLocation = nlapiLookupField('location', arrLocations[0], ['name', 'address1', 'address2', 'city', 'country', 'state', 'zip']);
        var stLocName = arrLocation['name'];
        var stLocAddr1 = arrLocation['address1'];
        var stLocAddr2 = arrLocation['address2'];
        var stLocCity = arrLocation['city'];
        var stLocCountry = arrLocation['country'];
        var stLocState = arrLocation['state'];
        var stLocZip = arrLocation['zip'];

        var tablehtmlLocation = '<b>';
        tablehtmlLocation += nlapiEscapeXML(stLocName) + '</b><br/>';
        tablehtmlLocation += nlapiEscapeXML(stLocAddr1) + '<br/>';
        if (stLocAddr2) {
            tablehtmlLocation += nlapiEscapeXML(stLocAddr2) + '<br/>';
        }
        tablehtmlLocation += nlapiEscapeXML(stLocCity) + ', ' + nlapiEscapeXML(stLocState) + ' ' + nlapiEscapeXML(stLocZip) + ' <br/>';
        tablehtmlLocation += nlapiEscapeXML(stLocCountry);

        //build Usage Charges section
        var tablePowerCharges = '';
        if (powerUsage == 'T') {
            tablePowerCharges += '<table cellpadding="5" class="profile" align="center" table-layout="fixed" style="page-break-inside: avoid;">';
            tablePowerCharges += '<tr>';
            tablePowerCharges += '<td width="30%">&nbsp;</td>';
            tablePowerCharges += '<td width="40%">&nbsp;</td>';
            tablePowerCharges += '<td width="10%">&nbsp;</td>';
            tablePowerCharges += '<td width="10%">&nbsp;</td>';
            tablePowerCharges += '<td width="10%">&nbsp;</td>';
            tablePowerCharges += '</tr>';
            tablePowerCharges += '<tr>';
            tablePowerCharges += '<td colspan="5">' + arrUsageCharges[stLang] + '</td>';
            tablePowerCharges += '</tr>';
            tablePowerCharges += '<tr>';
            tablePowerCharges += '<td colspan="5">&nbsp;</td>';
            tablePowerCharges += '</tr>';
            tablePowerCharges += '<tr>';
            tablePowerCharges += '<td class="cool"><b>' + arrDescr[stLang] + '</b></td>';
            tablePowerCharges += '<td class="cool">&nbsp;</td>';
            tablePowerCharges += '<td class="cool" align="right"><b>' + arrQty[stLang] + '</b></td>';
            tablePowerCharges += '<td class="cool" align="right"><b>' + arrRate[stLang] + '</b></td>';
            tablePowerCharges += '<td class="cool" align="right">&nbsp;</td>';
            tablePowerCharges += '</tr>';
            if(usageRate!=null && usageRate!='') {
                tablePowerCharges += '<tr>';
                tablePowerCharges += '<td>' + arrPowerUsage[stLang] + '</td>';
                tablePowerCharges += '<td>&nbsp;</td>';
                tablePowerCharges += '<td>&nbsp;</td>';
                tablePowerCharges += '<td align="right">$' + usageRate + '</td>';
                tablePowerCharges += '<td>&nbsp;</td>';
                tablePowerCharges += '</tr>';
            }
            if(commitKWUsage!=null && commitKWUsage!='') {

                tablePowerCharges += '<tr>';
                tablePowerCharges += '<td>' + arrKWHCommitUsage[stLang] + '</td>';
                tablePowerCharges += '<td>&nbsp;</td>';
                tablePowerCharges += '<td align="right">' + parseFloat(commitKWUsage) + '</td>';
                tablePowerCharges += '<td>&nbsp;</td>';
                tablePowerCharges += '<td>&nbsp;</td>';
                tablePowerCharges += '</tr>';
            }

            if(kwhburstrate!=null && kwhburstrate!='') {
                tablePowerCharges += '<tr>';
                tablePowerCharges += '<td>' + arrKWHBurstRate[stLang] + '</td>';
                tablePowerCharges += '<td>&nbsp;</td>';
                tablePowerCharges += '<td>&nbsp;</td>';
                tablePowerCharges += '<td align="right">$' + kwhburstrate + '</td>';
                tablePowerCharges += '<td>&nbsp;</td>';
                tablePowerCharges += '</tr>';
            }


            tablePowerCharges += '<tr>';
            tablePowerCharges += '<td colspan="5" class="profile t-border">&nbsp;</td>';
            tablePowerCharges += '</tr>';
            tablePowerCharges += '</table>';
            //tablePowerCharges += '<br/>';
        }
        if(hdColo=='T'){
            tablePowerCharges += '<table cellpadding="1" class="profile" align="center" table-layout="fixed" style="page-break-inside: avoid;">';
            tablePowerCharges += '<tr>';
            tablePowerCharges += '<td width="30%">&nbsp;</td>';
            tablePowerCharges += '<td width="40%">&nbsp;</td>';
            tablePowerCharges += '<td width="10%">&nbsp;</td>';
            tablePowerCharges += '<td width="10%">&nbsp;</td>';
            tablePowerCharges += '<td width="10%">&nbsp;</td>';
            tablePowerCharges += '</tr>';
            tablePowerCharges += '<tr>';
            tablePowerCharges += '<td colspan="5">' + arrUsageCharges[stLang] + '</td>';
            tablePowerCharges += '</tr>';
            tablePowerCharges += '<tr>';
            tablePowerCharges += '<td colspan="5">&nbsp;</td>';
            tablePowerCharges += '</tr>';
            tablePowerCharges += '<tr>';
            tablePowerCharges += '<td class="cool"><b>' + arrDescr[stLang] + '</b></td>';
            tablePowerCharges += '<td class="cool">&nbsp;</td>';
            tablePowerCharges += '<td class="cool" align="right"><b>' + arrQty[stLang] + '</b></td>';
            tablePowerCharges += '<td class="cool" align="right"><b>' + arrRate[stLang] + '</b></td>';
            tablePowerCharges += '<td class="cool" align="right">&nbsp;</td>';
            tablePowerCharges += '</tr>';
            tablePowerCharges += '<tr>';
            tablePowerCharges += '<td>' + arrHDColo[stLang] + '</td>';
            tablePowerCharges += '<td>&nbsp;</td>';
            tablePowerCharges += '<td align="right">' + highUtilization + '</td>';
            tablePowerCharges += '<td align="right">$' + highRate + '</td>';
            tablePowerCharges += '<td>&nbsp;</td>';
            tablePowerCharges += '</tr>';

            tablePowerCharges += '<tr>';
            tablePowerCharges += '<td>' + arrUHDColo[stLang] + '</td>';
            tablePowerCharges += '<td>&nbsp;</td>';
            tablePowerCharges += '<td align="right">' + uhighUtilization + '</td>';
            tablePowerCharges += '<td align="right">$' + uhighRate + '</td>';
            tablePowerCharges += '<td>&nbsp;</td>';
            tablePowerCharges += '</tr>';
            tablePowerCharges += '<tr>';
            tablePowerCharges += '<td colspan="5" class="profile t-border">&nbsp;</td>';
            tablePowerCharges += '</tr>';
            tablePowerCharges += '</table>';

        }
        //build IP Burst section
        var tableIPBurst = '';
        if (ipBurst == 'T') {
            tableIPBurst += '<table cellpadding="5" class="profile" align="center" table-layout="fixed" style="page-break-inside: avoid;">';
            tableIPBurst += '<tr>';
            tableIPBurst += '<td width="30%">&nbsp;</td>';
            tableIPBurst += '<td width="40%">&nbsp;</td>';
            tableIPBurst += '<td width="10%">&nbsp;</td>';
            tableIPBurst += '<td width="10%">&nbsp;</td>';
            tableIPBurst += '<td width="10%">&nbsp;</td>';
            tableIPBurst += '</tr>';
            tableIPBurst += '<tr>';
            tableIPBurst += '<td colspan="5">' + arrIPBurst[stLang] + '</td>';
            tableIPBurst += '</tr>';
            tableIPBurst += '<tr>';
            tableIPBurst += '<td colspan="5">&nbsp;</td>';
            tableIPBurst += '</tr>';
            tableIPBurst += '<tr>';
            tableIPBurst += '<td class="cool"><b>' + arrDescr[stLang] + '</b></td>';
            tableIPBurst += '<td class="cool">&nbsp;</td>';
            tableIPBurst += '<td class="cool" align="center">&nbsp;</td>';
            tableIPBurst += '<td class="cool" align="right"><b>' + arrIPBurstRate[stLang] + '</b></td>';
            tableIPBurst += '<td class="cool" align="right">&nbsp;</td>';
            tableIPBurst += '</tr>';
            tableIPBurst += '<tr>';
            tableIPBurst += '<td>' + arrIPBurstItem[stLang] + '</td>';
            tableIPBurst += '<td>&nbsp;</td>';
            tableIPBurst += '<td>&nbsp;</td>';
            tableIPBurst += '<td align="right">' + ipBurstRate + '</td>';
            tableIPBurst += '<td>&nbsp;</td>';
            tableIPBurst += '</tr>';
            tableIPBurst += '<tr>';
            tableIPBurst += '<td colspan="5" class="profile t-border">&nbsp;</td>';
            tableIPBurst += '</tr>';
            tableIPBurst += '</table>';
            //tableIPBurst += '<br/>';
        }



        //build Notes
        var tablehtmlNotes = '';
        if (stSONotes != '') {
            tablehtmlNotes += '<table cellpadding="2" class="profile" align="center" table-layout="fixed" style="page-break-inside: avoid;">';

            // if there is something in Notes
            if(stSONotes != ''){
                tablehtmlNotes += '<tr><td><h2>' + arrNotes[stLang] + '</h2></td></tr>';
                tablehtmlNotes += '<tr><td class="profile t-border" style="font-family: Arial, Helvetica, sans-serif; font-size:7pt;">'+ stSONotes + '</td></tr>';
            }
            tablehtmlNotes += '</table>';
            tablehtmlNotes += '<br/>';
        }

        //build SOs
        var tablehtmlSOs = '';
        if(arrSOs != null){
            if (arrSOs.length > 0) {
                tablehtmlSOs += '<table cellpadding="2" class="profile" align="center" table-layout="fixed" style="page-break-inside: avoid;">';
                // if a renewal, print SOs
                if(arrSOs.length > 0){
                    var txtNotes = '';
                    for (var i = 0; i < arrSOs.length; i++){
                        txtNotes += arrSOsNames[i] + ', ';
                    }
                    txtNotes = txtNotes.slice(0,txtNotes.length-2);
                    tablehtmlSOs += '<tr><td><h2>' + arrFromSOs[stLang] + '</h2></td></tr>';
                    tablehtmlSOs += '<tr><td class="profile t-border" style="font-family: Arial, Helvetica, sans-serif; font-size:7pt;">' + txtNotes + '</td></tr>';
                }
                tablehtmlSOs += '</table>';
                tablehtmlSOs += '<br/>';
            }
        }

        var objFile = nlapiLoadFile(template);
        var stMainHTML = objFile.getValue();

        var d = new moment();

        stMainHTML = stMainHTML.replace(new RegExp('{location}', 'g'), tablehtmlLocation);
        stMainHTML = stMainHTML.replace(new RegExp('{billAddress}', 'g'), tablehtmlAddress);
        stMainHTML = stMainHTML.replace(new RegExp('{sodate}', 'g'), d.format('M/D/YYYY'));
        stMainHTML = stMainHTML.replace(new RegExp('{sonbr}', 'g'), stSONbr);
        stMainHTML = stMainHTML.replace(new RegExp('{sorep}', 'g'), stSORep);
        stMainHTML = stMainHTML.replace(new RegExp('{totalcharges}', 'g'), '');
        stMainHTML = stMainHTML.replace(new RegExp('{items}', 'g'), tabledata);
        stMainHTML = stMainHTML.replace(new RegExp('{usage}', 'g'), tablePowerCharges);
        stMainHTML = stMainHTML.replace(new RegExp('{ipburst}', 'g'), tableIPBurst);
        stMainHTML = stMainHTML.replace(new RegExp('{notes}', 'g'), tablehtmlNotes);
        stMainHTML = stMainHTML.replace(new RegExp('{sos}', 'g'), tablehtmlSOs);
        stMainHTML = stMainHTML.replace(new RegExp('{terms}', 'g'), stSOTerms);



        //var htmlName = filePrefix + langPrefix + stSONbr + '_' + custCompName + '_' + d.format('M/D/YYYY') + '_' + d.format('H-m-s') + '_' + currencyType + '.html'
        //var htmlFile = nlapiCreateFile(htmlName, 'PLAINTEXT', stMainHTML);
        //htmlFile.setFolder(fileFolder);
        //nlapiSubmitFile(htmlFile);
        if (stSOLang != 17) { // if French
            stMainHTML = stMainHTML.replace(/[^A-Za-z 0-9 \.,\?""!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '');
        }
        stMainHTML = stMainHTML.replace(new RegExp('{signerDate}', 'g'), d.format('ll'));
        var filePDF = nlapiXMLToPDF(stMainHTML);
        //custCompName = custCompName.replace(/\ /g,"_");
        filePDF.setName(filePrefix + langPrefix + stSONbr + '_' + custCompName + '_' + d.format('M/D/YYYY') + '_' + d.format('H-m-s') + '_' + currencyType + '.pdf');
        filePDF.setFolder(fileFolder);

        var fileId = nlapiSubmitFile(filePDF);


        nlapiSubmitField('estimate', proposalid, 'custbody_clgx_prop_contract_file_id', fileId);

        nlapiSubmitField('estimate', proposalid, 'custbody_clgx_contract_terms_ready', 'F');
        nlapiSubmitField('estimate', proposalid, 'custbody_clgx_print_budgetray_quote', 'F');
        if(trq!=''){
            var pdf = nlapiLoadFile(fileId);
            //var pdfurl = 'https://system.na2.netsuite.com' + pdf.getURL();
            var pdfurl = 'https://1337135.app.netsuite.com' + pdf.getURL();
            var pdfname = pdf.getName().replace(/,/g, "");
            nlapiSubmitField('customrecord_clgx_cp_q_transactions', trq, 'custrecord_clgx_cp_tq_pdf', pdfurl);
            nlapiSubmitField('customrecord_clgx_cp_q_transactions', trq, 'custrecord_clgx_cp_tq_pdf_name', pdfname);
            var serverIP = nlapiLookupField('customrecord_clgx_cp_q_transactions', trq,'custrecord_clgx_cp_tq_portalserver');
            var requestURL = nlapiRequestURL('https://my.cologix.com/portal/echosign/create/?id=' + trq);
        }


        // var agreement=AgreementCreater(proposalid);
        // nlapiSendEmail(206211,206211,'agreement',agreement,null,null,null,null);
        nlapiLogExecution('DEBUG','Finished Execution', '|-------------------------- Finished Scheduled Script --------------------------|');
    }
    catch (error){

        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function AgreementCreater(proposal) {

    try {
        // methods
        var recType = 'estimate';
        var recId = proposal;
        var estimate = nlapiLoadRecord('estimate',proposal);
        var opportunity = estimate.getFieldText('opportunity');
        var today = new Date();
        var timestamp = ' (';
        timestamp += (today.getFullYear() + '-' + today.getMonth() + 1) + '-' + today.getDate();
        timestamp += ' ' + today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds() + ')';
        log(recType, recId);
        var agreement = nlapiCreateRecord('customrecord_echosign_agreement');
        agreement.setFieldValue('custrecord_echosign_parent_type', recType);
        agreement.setFieldValue('custrecord_echosign_parent_record', recId);
        agreement.setFieldValue('custrecord_echosign_senddocinteractive', 'T');
        agreement.setFieldValue('custrecord_echosign_days_until_deadline', 7);
        agreement.setFieldValue('custrecord_echosign_reminder', 2);
        var agreeId = nlapiSubmitRecord(agreement, true, true);
        log('Agreement Created', agreeId);
        var object = nlapiLoadRecord(recType, recId);
        var entityTypes = Util.getNsEntityTypeIds();
        var transTypes = Util.getNsTransactionTypeIds();

        try {
            if (isEntity(recType))
                nlapiSubmitField('customrecord_echosign_agreement', agreeId, 'custrecord_echosign_entity_id', recId);
            else if (object.getFieldValue('entity'))
                nlapiSubmitField('customrecord_echosign_agreement', agreeId, 'custrecord_echosign_entity_id', object.getFieldValue('entity'));
        } catch (ex) {
        }

        var autoAddSigner = (nlapiGetContext().getSetting('script', 'custscript_echosign_auto_add_signer') === 'T');

        var signer = null;

        if (transTypes.indexOf(recType) > -1) {
            nlapiSubmitField('customrecord_echosign_agreement', agreeId, 'name', 'Cologix Service Order #' + object.getFieldValue('tranid') + ' - ' + opportunity);
            // only attach transaction PDF if company setting is true
            nlapiLogExecution('DEBUG', 'custscript_echosign_auto_attach_transpdf', nlapiGetContext().getSetting('SCRIPT', 'custscript_echosign_auto_attach_transpdf'));
            if (nlapiGetContext().getSetting('SCRIPT', 'custscript_echosign_auto_attach_transpdf') !== 'F') {
                try {
                    var doc = nlapiCreateRecord('customrecord_echosign_document');
                    doc.setFieldValue('custrecord_echosign_agreement', agreeId);
                    log('Document Record');
                    /* var file = nlapiPrintRecord('TRANSACTION', recId, 'PDF');
                     log('File Created');
                     if (nlapiGetContext().getSetting('SCRIPT', 'custscript_echosign_agreement_folder') !== null) file.setFolder(nlapiGetContext().getSetting('SCRIPT', 'custscript_echosign_agreement_folder'));
                     else file.setFolder(-4);
                     var fileId = nlapiSubmitFile(file);
                     log('File Saved', fileId);*/
                    var fileId = estimate.getFieldValue('custbody_clgx_prop_contract_file_id');
                    doc.setFieldValue('custrecord_echosign_file', fileId);
                    var docId = nlapiSubmitRecord(doc);
                    log('Document Record Created', docId);
                } catch (e) {
                    nlapiLogExecution('ERROR', 'Error Creating Document');
                    nlapiLogExecution('ERROR', e.name || e.getCode(), e.message || e.getDetails());
                }
            }

            if (autoAddSigner) {
                // there is company preference to set the related contact on
                // the transaction as the first signer
                //  if (nlapiGetContext().getSetting('script', 'custscript_use_trans_contact_as_signer') === 'T') {
                // figure out the primary contact on the transaction
                // object
                var termsContact = estimate.getFieldValue('custbody_clgx_contract_terms_contact');
                var contactRecord = nlapiLoadRecord('contact', termsContact);
                var emailterms = contactRecord.getFieldValue('email');
                var idterms = contactRecord.getFieldValue('id');
                var entityterms = contactRecord.getFieldValue('entityid');
                var salesrep = estimate.getFieldValue('salesrep');
                var contactRecordSR = nlapiLoadRecord('contact', salesrep);
                var supervisor = contactRecordSR.getFieldValue('supervisor');
                var contactRecordSupervisor = nlapiLoadRecord('contact', supervisor);
                var emailSupervisor = contactRecordSupervisor.getFieldValue('email');
                var idSupervisor = contactRecordSupervisor.getFieldValue('id');
                var entitySupervisor = contactRecordSupervisor.getFieldValue('entityid');
                var
                /*var res = nlapiSearchRecord('transaction', null, [
                 new nlobjSearchFilter('internalid', null, 'is', object.getId()), new nlobjSearchFilter('mainline', null, 'is', 'T')], [
                 new nlobjSearchColumn('email', 'contactprimary'), new nlobjSearchColumn('contactrole', 'contactprimary'), new nlobjSearchColumn('internalid', 'contactprimary'), new nlobjSearchColumn('entityid', 'contactprimary')]);
                 */
                //   if (res && res.length && res[0].getValue('email', 'contactprimary')) {
                //customer
                    signer = nlapiCreateRecord('customrecord_echosign_signer');
                signer.setFieldValue('custrecord_echosign_signer', idterms);
                signer.setFieldValue('custrecord_echosign_entityid', entityterms);
                signer.setFieldValue('custrecord_echosign_agree', agreeId);
                signer.setFieldValue('custrecord_echosign_email', emailterms);
                signer.setFieldValue('custrecord_echosign_to_order', 0);
                signer.setFieldValue('custrecord_echosign_signer_order', 1);
                //supervisor


                nlapiSubmitRecord(signer);

                //  }
                // }

                // use email of the entity on the transaction object
                // if the setting is not set or setting is set but no
                // contact found on transaction
                if (!signer) {
                    if (nlapiLookupField('entity', object.getFieldValue('entity'), 'email') !== null && nlapiLookupField('entity', object.getFieldValue('entity'), 'email').length !== 0) {
                        signer = nlapiCreateRecord('customrecord_echosign_signer');
                        signer.setFieldValue('custrecord_echosign_agree', agreeId);
                        try {
                            var entityFlds = nlapiLookupField('entity', object.getFieldValue('entity'), ['email', 'entityid']);
                            // signer.setFieldValue('custrecord_echosign_signer',
                            // object.getFieldValue('entity'));
                            signer.setFieldValue('custrecord_echosign_entityid', entityFlds['entityid']);
                            signer.setFieldValue('custrecord_echosign_email', entityFlds['email']);
                            signer.setFieldValue('custrecord_echosign_to_order', 0);
                            signer.setFieldValue('custrecord_echosign_signer_order', 1);
                        } catch (e) {
                            nlapiLogExecution('ERROR', 'Invalid Email', nlapiLookupField('entity', object.getFieldValue('entity'), 'email'));
                        }

                        signer.setFieldValue('custrecord_echosign_role', 1);
                        nlapiSubmitRecord(signer);
                    }
                }
            }

        } else if (entityTypes.indexOf(recType) > -1) {
            var agreementName = 'Agreement for ';
            if (object.getFieldValue('companyname') !== null && object.getFieldValue('companyname').length !== 0) {
                agreementName += object.getFieldValue('companyname');
            } else {
                if (object.getFieldValue('firstname') !== null && object.getFieldValue('firstname').length !== 0)
                    agreementName += object.getFieldValue('firstname');
                if (object.getFieldValue('lastname') !== null && object.getFieldValue('lastname').length !== 0)
                    agreementName += ' ' + object.getFieldValue('lastname');
            }
            if (recType === 'contact')
                nlapiSubmitField('customrecord_echosign_agreement', agreeId, 'custrecord_echosign_signer', recId);

            nlapiSubmitField('customrecord_echosign_agreement', agreeId, 'name', agreementName);

            if (autoAddSigner) {
                if (object.getFieldValue('email') !== null && object.getFieldValue('email').length !== 0) {
                    try {
                        signer = nlapiCreateRecord('customrecord_echosign_signer');
                        signer.setFieldValue('custrecord_echosign_agree', agreeId);
                        signer.setFieldValue('custrecord_echosign_entityid', object.getFieldValue('entityid'));
                        signer.setFieldValue('custrecord_echosign_email', object.getFieldValue('email'));
                        signer.setFieldValue('custrecord_echosign_to_order', 0);
                        signer.setFieldValue('custrecord_echosign_signer_order', 1);
                        signer.setFieldValue('custrecord_echosign_role', 1);
                        nlapiSubmitRecord(signer);
                    } catch (e) {
                        nlapiLogExecution('ERROR', 'Invalid Email', object.getFieldValue('email'));
                    }
                }
            }
        }
    }
    catch (error){

        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
    return agreeId;
}