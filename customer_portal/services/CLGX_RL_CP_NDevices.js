//------------------------------------------------------
//	Script:		CLGX_RL_CP_NDevices.js
//	ScriptID:	customscript_clgx_rl_cp_ndevices
//	ScriptType:	RESTlet
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	02/15/2018
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv) {
    var direction = datain['direction'] || 'ASC';
    var order_by = datain['order_by'] || '';
    var page = datain['page'] || 1;
    var per_page = datain['per_page'];
    var csv = datain['csv'] || 0;

    var getPorts=get_records(companyid,direction,order_by,page,per_page,csv,radix);
    return getPorts;

});
var put = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix) {
    var id = datain['rid'] || 0;
    var update = datain['update'] || 0;
    var note = datain['note'] || '';
    nlapiLogExecution('DEBUG', 'id record', id);
    if(update>0){
        var getPorts=update_record(parseInt(id),note);
    }
    else {
        var getPorts = get_record(parseInt(id), update, radix);
    }
    return getPorts;

});


function get_records(id,direction,order_by,page,per_page,csv,radix){
    var total = 0;
    var pages = 0;
    var start = 0;
    var end = 0;
    var has_more = false;
    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("name",'custrecord_clgx_ms_service_order',"anyof",id));
    var records = nlapiSearchRecord('customrecord_clgx_managed_services','customsearch5628', filters, columns);

    var obj=new Object();
    var arrObj=new Array();
    if(records!=null) {
        total = records.length;
        nlapiLogExecution('DEBUG', 'id', id);
        nlapiLogExecution('DEBUG', 'total1', total);
        nlapiLogExecution('DEBUG', 'per_page', per_page);
        pages = Math.ceil(total / per_page);
        page = (parseInt(page) > pages ? pages : parseInt(page));
        has_more = (pages > 1 ? true : false);
        start = (page - 1) * per_page;
        end = (start + per_page > total ? total : start + per_page);
        nlapiLogExecution('DEBUG', 'start', start);
        nlapiLogExecution('DEBUG', 'end', end);

        for ( var j = start; j < end; j++ ) {
            var searchAP = records[j];
            var columns = searchAP.getAllColumns();
            var id = searchAP.getValue(columns[0]);
            var name= searchAP.getValue(columns[1]);
            var description= searchAP.getValue(columns[2]);
            var so = searchAP.getValue(columns[3]);
            var soid = parseInt(searchAP.getValue(columns[6]));
            var created= searchAP.getValue(columns[4]);
            var internalid= parseInt(searchAP.getValue(columns[5]));

            var rec = new Object();
            rec.id = id;
            rec.name = name;
            rec.description = description;
            rec.so = so;
            rec.created = created;
            rec.soid = soid.toString(radix);
            rec.internalid =internalid;
            arrObj.push(rec)

        }
        obj["data"] = arrObj;
        obj["direction"] = direction;
        obj["order_by"] = order_by;
        obj["page"] = page;
        obj["pages"] = pages;
        obj["per_page"] = per_page;
        obj["has_more"] = has_more;
        obj["start"] = start;
        obj["end"] = end;
        obj["total"] = total;
    }else{
        obj["data"] = [];
        obj["page"] = page;
        obj["pages"] = 1;
        obj["per_page"] = 16;
        obj["has_more"] =false;
        obj["start"] = 0;
        obj["end"] = 0;
        obj["total"] = 0;
    }
    return obj;
}

function get_record(id, update,radix){
    nlapiLogExecution('DEBUG', 'radix', radix);
    nlapiLogExecution('DEBUG', 'id', id);
    var obj=new Object();
    var record = nlapiLoadRecord('customrecord_clgx_managed_services',id);

    obj["id"] = record.getFieldValue('name');
    obj["internalid"] = parseInt(record.getFieldValue('id'));
    obj["name"] = record.getFieldValue('custrecord_clgx_ms_device_name');

    obj["note"] =record.getFieldValue('custrecord_clgx_ms_note')||'';
    obj["ip"]=record.getFieldValue('custrecord_clgx_ip_address')||'';
    var type=record.getFieldText('custrecord_clgx_managed_services_type');
    if(type=='Security'){
        var equipment=record.getFieldValue('custrecord_clgx_ms_firewall')||0;
    }else{
        var equipment=record.getFieldValue('custrecord_clgx_ms_router_switch')||0;
    }

    var filters = new Array();
    var columns = new Array();
    filters.push(new nlobjSearchFilter("internalid",null,"anyof",equipment));
    var records = nlapiSearchRecord('customrecord_cologix_equipment','customsearch5716', filters, columns);

    var arrObj=new Array();
    if(records!=null) {


        for (var j = 0; j < records.length; j++) {
            var searchAP = records[j];
            var columns = searchAP.getAllColumns();
            var prtg = searchAP.getValue(columns[0])||'';
            var rec = new Object();
           /* if(prtg!=''){
                var req = nlapiRequestURL('https://corp-nnj-prtglab.corp.cologix.net/api/getsensordetails.json?id='+prtg+'&version=17.3.33.2830&username=catalina.taran&passhash=1102549655', 'GET');
                if (req.code == 200) {
                    var str = req.body;
                    var resp = JSON.parse(str);
                    rec.status = resp['sensordata'].statustext;
                }else {
                    rec.status = '';
                }
                var req = nlapiRequestURL('https://corp-nnj-prtglab.corp.cologix.net/api/table.json?content=values&output=json&columns=datetime,value_&count=1&id='+prtg+'&noraw=1&sortby=-datetime&usecaption=true&username=catalina.taran&passhash=1102549655', 'GET');
                if (req.code == 200) {
                    var str = req.body;
                    var resp = JSON.parse(str);
                    rec.curbpsin = resp['values'][1]['Traffic In (speed)'];
                    rec.curbpsout = resp['values'][1]['Traffic Out (speed)'];

                }else {
                    rec.curbpsin = '';
                    rec.curbpsout = '';

                }

            }else{
                rec.status = '';
                rec.curbpsin = '';
                rec.curbpsout = '';
            }

*/

            rec.prtg = prtg;
            rec.name=searchAP.getValue(columns[1])||'';;

            arrObj.push(rec)

        }
    }
    obj["ports"] = arrObj;
    return obj;
}

function update_record(id,note){
    nlapiLogExecution('DEBUG', 'id', id);
    nlapiLogExecution('DEBUG', 'note', note);
    var obj=new Object();
    var record = nlapiLoadRecord('customrecord_clgx_managed_services',id);
if(note!='') {
    //record.setFieldValue('custrecord_clgx_ms_note',note);
    //nlapiSubmitRecord(record, true,true);
}

    obj["resp"] = "The record has been updated";
    return obj;
}