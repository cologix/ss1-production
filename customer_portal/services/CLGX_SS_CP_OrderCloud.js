nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_SS_CP_OrderCloud.js
//	ScriptID:	customscript_clgx_ss_cp_ordercloud
//	ScriptType:	Scheuled
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	10/03/2016
//------------------------------------------------------

function create_orders() {

    nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');

    //retrieve script parameters
    var languagePortal=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_lanc')||'en';
    var title=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_titlec');
    var companyid=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_companyc');
    var facility=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_locc');
    var port_name=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_port_namec');
    var qty=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_qtyc');
    var item=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_itemc');
    var rate=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_ratec');
    var totalmrc=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_amountc');
    var totalnrc=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_totalnrc');
    var providerName=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_provc')||'';
    var contactid=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_contactc');
    var tfacility=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_tfacc');
    var speedName=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_speednamec');
    var conn_type=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_conn_tyc');
    var billing=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_billingc');
    var ratecard=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_ratecarc');
    var evc=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_evcc');
    var service_id=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_service_idc')||'';
    var stag=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_stag')||'';
    var notes=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_notesc')||'';
    var po=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_poc');
    var serverIP=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_serverc');
    var requestor=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_reqc');
    var boxfolder=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_boxfc');
    var uname=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_unamec');
    var timestamp=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_timestampc');
   // nlapiLogExecution('DEBUG','market',market);
    nlapiLogExecution('DEBUG','providerName',providerName);
    nlapiLogExecution('DEBUG','requestor',requestor);
    nlapiLogExecution('DEBUG','boxfolder',boxfolder);
    nlapiLogExecution('DEBUG','po',po);
    nlapiLogExecution('DEBUG','notes',notes);
    var arrCustomerDetails = nlapiLookupField('customer',companyid,['salesrep','companyname','custentity_clgx_cust_ccm']);
    var salesRepID = arrCustomerDetails['salesrep'];
    var custCompName = arrCustomerDetails['companyname'];
    var ccm = arrCustomerDetails['custentity_clgx_cust_ccm'];
    var fname=nlapiLookupField('contact', contactid, 'firstname') || '';
    var lname=nlapiLookupField('contact', contactid, 'lastname') || '';
    var emailContact=nlapiLookupField('contact', contactid, 'email') || '';
    var custContactName=fname+' '+lname;
    var contactRec=nlapiLoadRecord('contact',requestor);
    var nameAtt=contactRec.getFieldValue('entitytitle');
    if(po!=null && po!='') {
        po = po.replace(/%23/g, "#");
    }
    if(notes!=null && notes!='') {
        notes = notes.replace(/%23/g, "#");
    }
    var strMemoOrder =
        ' Port ID : ' + port_name + ';\n\n' +
        ' On-Ramp Facility : ' + tfacility + ';\n\n' +
        ''+ providerName + ';\n\n' +
        ' Service ID/S-Key : ' + service_id + ';\n\n' +
        ' S-TAG : ' + stag + ';\n\n' +
        ' Type : ' + conn_type + ';\n\n' +
        ''+ speedName + ';\n\n' +
        ' Notes : ' + notes;

    if(parseInt(salesRepID) > 0){
        var arrRepDetails = nlapiLookupField('employee',salesRepID,['entityid']);
        var salesRepName = arrRepDetails['entityid'];
    }
    else{
        var salesRepName = '';
    }
    var taxcode1 = 0;
    var taxcode2 = 0;

    //MTL
    if (facility == 5 || facility == 8 || facility == 9 || facility == 10 || facility == 11 || facility == 12 || facility == 27) {
        var taxcode1 = 308;
        var taxcode2 = 308;
    } else if (facility == 6 || facility == 13 || facility == 15) { //TOR
        var taxcode1 = 11;
        var taxcode2 = 11;
    }
    else if (facility == 7 || facility == 28) { //VAN
        var taxcode1 = 297;
        var taxcode2 = 297;
    }
    else if (facility == 34 || facility == 39 || facility == 38) { //COL
        var taxcode1 = 515;
        var taxcode2 = 515;
    }
    else if (facility == 31 || facility == 40) { //JAX
        var taxcode1 = 498;
        var taxcode2 = 498;
    }
    else if (facility == 42) { //LAK
        var taxcode1 = 565;
        var taxcode2 = 565;
    }
    else if (facility == 53 || facility == 54 || facility == 55|| facility==56) { //NNJ
        var taxcode1 = -635;
        var taxcode2 = -635;
    }
    var date = new Date();
    nlapiLogExecution('DEBUG', 'title', title);
    nlapiLogExecution('DEBUG', 'facility', facility);
    nlapiLogExecution('DEBUG', 'totalmrc', totalmrc);
    nlapiLogExecution('DEBUG', 'billing', billing);
    nlapiLogExecution('DEBUG', 'evc', evc);

    if(evc>=1)
    {
        if(evc==2) {
            var arrItems = item.split(';');
            var arrRates=rate.split(';');
            var arrtotalMRC=totalmrc.split(';');
            var arrSp=speedName.split('/');
            var arrSpEVC=arrSp[1].split(':');
        }
        else{
            var arrItems=new Array();
            var arrRates=new Array();
            var arrtotalMRC=new Array();
            var arrSpEVC=new Array();
            arrItems[1]=item;
            arrRates[1]=rate;
            arrtotalMRC[1]=totalmrc;
            arrSpEVC[1]=speedName;
        }
        nlapiLogExecution('DEBUG', 'arrItems[1]', arrItems[1]);
        if (ratecard != 0) {
            var record = nlapiCreateRecord('opportunity');
            record.setFieldValue('title', title);
            record.setFieldValue('entity', companyid);
            record.setFieldValue('memo', strMemoOrder.substr(0,999));
            record.setFieldValue('custbody_clgx_so_portal_req', requestor);
            record.setFieldValue('location', facility);
            record.setFieldValue('forecasttype', 3);
            record.setFieldValue('entitystatus', 11);
            // record.setFieldValue('memo', strMemo.substr(0,999));
            // record.setFieldValue('statusRef', 'inProgress');
            record.setFieldValue('custbody_cologix_opp_sale_type', 2);
            record.setFieldValue('leadsource', 73127);
            if(evc==2) {
                record.setLineItemValue('item', 'item', 1, arrItems[1]);
                record.setLineItemValue('item', 'quantity', 1, qty);
                record.setLineItemValue('item', 'rate', 1, 0);
                record.setLineItemValue('item', 'location', 1, facility);
                record.setLineItemValue('item', 'price', 1, -1);
                record.setLineItemValue('item', 'pricelevels', 1, -1);
                record.setLineItemValue('item', 'amount', 1, 0);
                record.setLineItemValue('item', 'description', 1, arrSpEVC[1]);

                if (taxcode1 !=0) {
                    record.setLineItemValue('item', 'taxcode', 1, taxcode1);
                }
                record.setLineItemValue('item', 'item', 2, arrItems[0]);
                record.setLineItemValue('item', 'quantity', 2, qty);
                record.setLineItemValue('item', 'rate', 2, 0);
                record.setLineItemValue('item', 'location', 2, facility);
                record.setLineItemValue('item', 'price', 2, -1);
                record.setLineItemValue('item', 'pricelevels', 2, -1);
                record.setLineItemValue('item', 'amount', 2, 0);
                // record.setLineItemValue('item', 'description', 1, speedName);
                if (taxcode1 !=0) {
                    record.setLineItemValue('item', 'taxcode', 2, taxcode1);
                }
                //item 244
                record.setLineItemValue('item', 'item', 3, 244);
                record.setLineItemValue('item', 'quantity', 3, qty);
                record.setLineItemValue('item', 'rate', 3, 0);
                record.setLineItemValue('item', 'amount', 3, 0);
                record.setLineItemValue('item', 'location', 3, facility);
                record.setLineItemValue('item', 'price', 3, -1);
                record.setLineItemValue('item', 'pricelevels', 3, -1);
                if (taxcode2 !=0) {
                    record.setLineItemValue('item', 'taxcode', 3, taxcode2);
                }

            }
            else {
                record.setLineItemValue('item', 'item', 1, arrItems[1]);
                record.setLineItemValue('item', 'quantity', 1, qty);
                record.setLineItemValue('item', 'rate', 1, 0);
                record.setLineItemValue('item', 'location', 1, facility);
                record.setLineItemValue('item', 'price', 1, -1);
                record.setLineItemValue('item', 'pricelevels', 1, -1);
                record.setLineItemValue('item', 'amount', 1, 0);
                record.setLineItemValue('item', 'description', 1, arrSpEVC[1]);
                if (taxcode1 !=0) {
                    record.setLineItemValue('item', 'taxcode', 1, taxcode1);
                }
            }

            try {
                var recordId = nlapiSubmitRecord(record, true, true);

            }
            catch (error) {
                nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
            }
            if (recordId != null) {
                var totals = clgx_transaction_totals(companyid, 'oppty', recordId);
                //create the proposal

                // var recProposal = nlapiTransformRecord(fromrecord, recordId, torecord);
                var termstitle=2;
                if(languagePortal=='fr')
                {
                    var termstitle=3;
                }
                var recProposal = nlapiCreateRecord('estimate');
                recProposal.setFieldValue('entity', companyid);
                recProposal.setFieldValue('memo', '');
                recProposal.setFieldValue('custbody_clgx_contract_terms_title', termstitle);
                recProposal.setFieldValue('otherrefnum', po);
                recProposal.setFieldValue('custbodyclgx_aa_type', 3);
                recProposal.setFieldValue('custbody_cologix_annual_accelerator', '3');
                var stTermsBody = nlapiLookupField('customrecord_clgx_contract_terms', termstitle, 'custrecord_clgx_contract_terms_body');
                recProposal.setFieldValue('custbody_clgx_contract_terms_body', stTermsBody);
                recProposal.setFieldValue('custbody_clgx_order_notes', strMemoOrder.substr(0,999));
                recProposal.setFieldValue('opportunity', recordId);
                recProposal.setFieldValue('entitystatus',11);
                recProposal.setFieldValue('title', title);
                recProposal.setFieldValue('location', facility);
                recProposal.setFieldValue('custbody_clgx_contract_terms_contact', contactid);
                //  recProposal.setFieldValue('forecasttype', 3);
                // recProposal.setFieldValue('entitystatus', 11);
                recProposal.setFieldValue('billingaddress_text', billing);
                // record.setFieldValue('statusRef', 'inProgress');
                recProposal.setFieldValue('custbody_cologix_opp_sale_type', 2);
                recProposal.setFieldValue('leadsource', 73127);

                //add the items
                if(evc==2) {
                    recProposal.setLineItemValue('item', 'item', 1, arrItems[1]);
                    recProposal.setLineItemValue('item', 'quantity', 1, qty);
                    recProposal.setLineItemValue('item', 'rate', 1, 0);
                    recProposal.setLineItemValue('item', 'location', 1, facility);
                    recProposal.setLineItemValue('item', 'price', 1, -1);
                    recProposal.setLineItemValue('item', 'pricelevels', 1, -1);
                    recProposal.setLineItemValue('item', 'amount', 1, 0);
                    recProposal.setLineItemValue('item', 'billingschedule', 1, 3);
                    recProposal.setLineItemValue('item', 'description', 1, arrSpEVC[1]);
                    if (taxcode1 !=0) {
                        recProposal.setLineItemValue('item', 'taxcode', 1, taxcode1);
                    }
                    recProposal.setLineItemValue('item', 'item', 2, arrItems[0]);
                    recProposal.setLineItemValue('item', 'quantity', 2, qty);
                    recProposal.setLineItemValue('item', 'rate', 2, 0);
                    recProposal.setLineItemValue('item', 'location', 2, facility);
                    recProposal.setLineItemValue('item', 'price', 2, -1);
                    recProposal.setLineItemValue('item', 'pricelevels', 2, -1);
                    recProposal.setLineItemValue('item', 'amount', 2, 0);
                    recProposal.setLineItemValue('item', 'billingschedule', 2, 3);
                    // recProposal.setLineItemValue('item', 'description', 1, speedName);
                    if (taxcode1 !=0) {
                        recProposal.setLineItemValue('item', 'taxcode', 2, taxcode1);
                    }
                    //item 244
                    recProposal.setLineItemValue('item', 'item', 3, 244);
                    recProposal.setLineItemValue('item', 'quantity', 3, qty);
                    recProposal.setLineItemValue('item', 'rate', 3, 0);
                    recProposal.setLineItemValue('item', 'amount', 3, 0);
                    recProposal.setLineItemValue('item', 'location', 3, facility);
                    recProposal.setLineItemValue('item', 'price', 3, -1);
                    recProposal.setLineItemValue('item', 'pricelevels', 3, -1);
                    recProposal.setLineItemValue('item', 'billingschedule', 3, 17);
                    if (taxcode2 !=0) {
                        recProposal.setLineItemValue('item', 'taxcode', 3, taxcode2);
                    }

                }
                else {
                    recProposal.setLineItemValue('item', 'item', 1, arrItems[1]);
                    recProposal.setLineItemValue('item', 'quantity', 1, qty);
                    recProposal.setLineItemValue('item', 'rate', 1, 0);
                    recProposal.setLineItemValue('item', 'location', 1, facility);
                    recProposal.setLineItemValue('item', 'price', 1, -1);
                    recProposal.setLineItemValue('item', 'pricelevels', 1, -1);
                    recProposal.setLineItemValue('item', 'amount', 1, 0);
                    recProposal.setLineItemValue('item', 'description', 1, arrSpEVC[1]);
                    recProposal.setLineItemValue('item', 'billingschedule', 1, 3);
                    if (taxcode1 !=0) {
                        recProposal.setLineItemValue('item', 'taxcode', 1, taxcode1);
                    }
                }

                try {
                    var recordIdPR = nlapiSubmitRecord(recProposal, true, true);
                    var proposalNumber= nlapiLookupField('estimate',recordIdPR,'tranid');
                    if(languagePortal=='en')
                    {

                        var emailSubjectProposal="New Order "+proposalNumber+" - "+custCompName;
                        var agree='The initial term of this Service Order shall be one (1) month and shall automatically renew for successive one (1) month terms, unless terminated by either party, in writing, at least one (1) month prior to the end of the then current term. Your Cologix rate card rates apply for the ordered services.\n\n Notwithstanding anything to the contrary in the agreement between the parties, during the Term, all service fees identified above shall increase on each 12-month anniversary of the Service Commencement Date in an amount equal to the greater of three percent (3%) or CPI. Such increase shall be automatically invoiced to Customer upon each 12-month anniversary of the Service Commencement Date.\n\nEach of Customer and Cologix hereby consents to the execution of this Service Order by electronic means.\n';
                        var emailBodyProposal = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
                            'Thank you for submitting your order. A Cologix employee will be contacting you soon regarding the installation of your services. Please allow up to 24 hours for this order to be visible in the Portal.\n\n' +
                            'New Order : ' + proposalNumber + '\n' +
                            'Title : ' + title + '\n' +
                            'Portal Username : ' + uname + '\n' +
                            'Server Date and Time : ' + timestamp + '\n' +
                            'Server Name : ' + serverIP + '\n' +
                            'Contractual Terms Agreed To: '+agree+'\n'+
                            'Sincerely,\n' +
                            salesRepName + '\n\n';
                    }
                    else if(languagePortal=='fr')
                    {
                        var emailSubjectProposal="Nouvelle Commande "+proposalNumber+" - "+custCompName;
                        var agree='La durée initiale de cette ordonnance de service est de un (1) mois et se renouvelle automatiquement pour les termes successifs d’un (1) mois, à moins d’être résilié par l’un ou l’autre des parties, par écrit, au moins un (1) mois avant la fin du Terme actuel. Vos tarifs de carte tarifaire Cologix s’appliquent pour les services commandés.\n\nThe initial term of this Service Order shall be one (1) month and shall automatically renew for successive one (1) month terms, unless terminated by either party, in writing, at least one (1) month prior to the end of the then current term. Your Cologix rate card rates apply for the ordered services. La durée initiale de cette ordonnance de service est de un (1) mois et se renouvelle automatiquement pour les termes successifs d’un (1) mois, à moins d’être résilié par l’un ou l’autre des parties, par écrit, au moins un (1) mois avant la fin du Terme actuel. Vos tarifs de carte tarifaire Cologix s’appliquent pour les services commandés. Notwithstanding anything to the contrary in the agreement between the parties, during the Term, all service fees identified above shall increase on each 12-month anniversary of the Service Commencement Date in an amount equal to the greater of three percent (3%) or CPI. Such increase shall be automatically invoiced to Customer upon each 12-month anniversary of the Service Commencement Date. Malgré toute disposition contraire dans le contrat conclu entre les parties, pendant la durée, tous les frais de service précisés plus haut augmenteront automatiquement tous les douze (12) mois à compter de la date anniversaire de la date de début du service, en fonction d’un taux égal à 3 % ou à l’indice des prix à la consommation, selon le plus élevé de ces deux taux.\n\nChacune des parties, le Client et Cologix, acceptent par les présentes que la signature et le passage de cette Commande de Service soient fait par moyen électronique.\n';
                        var emailBodyProposal = 'Bonjour ' + custContactName + ' (' + custCompName + '),\n\n' +
                            "Nous vous remercions d'avoir envoyé votre commande. Un employé de Cologix vous contactera rapidement concernant l'installation de vos services. Veuillez laisser jusqu'à 24 heures pour que cette commande soit visible dans le portail.\n\n" +
                            'Nouvelle Order : ' + proposalNumber + '\n' +
                            'Titre : ' + title + '\n' +
                            'Nom d’utilisateur du portail : ' + uname + '\n' +
                            'Date et heure du serveur : ' + timestamp + '\n' +
                            'Nom du serveur : ' + serverIP + '\n' +
                            'Termes contractuels convenue par: '+agree+'\n'+
                            'Au plaisir de vous être utile,\n' +
                            salesRepName + '\n\n';
                    }
                    nlapiSendEmail(salesRepID,contactid,emailSubjectProposal,emailBodyProposal,null,null,null,null,true);
                    if(ccm!=null) {
                        nlapiSendEmail(salesRepID, ccm, emailSubjectProposal, emailBodyProposal, null, null, null, null, true);
                    }
                }
                catch (error) {
                    nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
                }
                var recordQ = nlapiCreateRecord('customrecord_clgx_cp_q_transactions');
                recordQ.setFieldValue('custrecord_clgx_cp_tq_oppid', recordId);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_cust', companyid);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_contact', contactid);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_conemail', emailContact);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_ratecard', ratecard);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_folderid', boxfolder);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_add_vxc', 'T');
                recordQ.setFieldValue('custrecord_clgx_cp_tq_portalserver', serverIP);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_trpr', 'T');
                if (recordIdPR != null) {
                    var tranid = nlapiLookupField('estimate',recordIdPR,'tranid');
                    recordQ.setFieldValue('custrecord_clgx_cp_tq_propid', tranid);
                    recordQ.setFieldValue('custrecord_clgx_cp_tq_prid', recordIdPR);
                    var recordQID = nlapiSubmitRecord(recordQ, true, true);
                    var serverIP = nlapiLookupField('customrecord_clgx_cp_q_transactions', recordQID,'custrecord_clgx_cp_tq_portalserver');
                    //  var requestURL = nlapiRequestURL('https://my.cologix.com/portal/echosign/upload/?id=' + recordQID);
                    var totals = clgx_transaction_totals(companyid, 'proposal', recordIdPR);
                }
            }
        }
        else {
            var record = nlapiCreateRecord('opportunity');
            record.setFieldValue('title', title);
            record.setFieldValue('entity', companyid);
            record.setFieldValue('location', facility);
            record.setFieldValue('memo', strMemoOrder.substr(0,999));
            record.setFieldValue('custbody_clgx_so_portal_req', requestor);
            //record.setFieldValue('forecasttype', 3);
            //record.setFieldValue('entitystatus', 11);
            // record.setFieldValue('statusRef', 'inProgress');
            record.setFieldValue('custbody_cologix_opp_sale_type', 2);
            // record.setFieldValue('memo', strMemo.substr(0,999));
            record.setFieldValue('leadsource', 73127);

            if(evc==2) {
                record.setLineItemValue('item', 'item', 1, arrItems[1]);
                record.setLineItemValue('item', 'quantity', 1, qty);
                record.setLineItemValue('item', 'rate', 1, arrRates[1]);
                record.setLineItemValue('item', 'location', 1, facility);
                record.setLineItemValue('item', 'price', 1, -1);
                record.setLineItemValue('item', 'pricelevels', 1, -1);
                record.setLineItemValue('item', 'amount', 1,arrtotalMRC[1]);
                record.setLineItemValue('item', 'description', 1, arrSpEVC[1]);
                if (taxcode1 !=0) {
                    record.setLineItemValue('item', 'taxcode', 1, taxcode1);
                }
                record.setLineItemValue('item', 'item', 2, arrItems[0]);
                record.setLineItemValue('item', 'quantity', 2, 1);
                record.setLineItemValue('item', 'rate', 2, arrRates[0]);
                record.setLineItemValue('item', 'location', 2, facility);
                record.setLineItemValue('item', 'price', 2, -1);
                record.setLineItemValue('item', 'pricelevels', 2, -1);
                record.setLineItemValue('item', 'amount', 2,arrtotalMRC[0]);
                // record.setLineItemValue('item', 'description', 1, speedName);
                if (taxcode1 !=0) {
                    record.setLineItemValue('item', 'taxcode', 2, taxcode1);
                }
                //item 244
                record.setLineItemValue('item', 'item', 3, 244);
                record.setLineItemValue('item', 'quantity', 3, 1);
                record.setLineItemValue('item', 'rate', 3, totalnrc);
                record.setLineItemValue('item', 'amount', 3, totalnrc);
                record.setLineItemValue('item', 'location', 3, facility);
                record.setLineItemValue('item', 'price', 3, -1);
                record.setLineItemValue('item', 'pricelevels', 3, -1);
                if (taxcode2 !=0) {
                    record.setLineItemValue('item', 'taxcode', 3, taxcode2);
                }

            }
            else {
                record.setLineItemValue('item', 'item', 1, arrItems[1]);
                record.setLineItemValue('item', 'quantity', 1, qty);
                record.setLineItemValue('item', 'rate', 1, arrRates[1]);
                record.setLineItemValue('item', 'location', 1, facility);
                record.setLineItemValue('item', 'price', 1, -1);
                record.setLineItemValue('item', 'pricelevels', 1, -1);
                record.setLineItemValue('item', 'amount', 1, arrtotalMRC[1]);
                record.setLineItemValue('item', 'description', 1, arrSpEVC[1]);
                if (taxcode1 !=0) {
                    record.setLineItemValue('item', 'taxcode', 1, taxcode1);
                }
            }



            try {
                var recordId = nlapiSubmitRecord(record, true, true);
            }
            catch (error) {
                nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
            }
            if (recordId != null) {
                var totals = clgx_transaction_totals(companyid, 'oppty', recordId);
                //create the proposal

                var termstitle=2;
                if(languagePortal=='fr')
                {
                    var termstitle=3;
                }
                var recProposal = nlapiCreateRecord('estimate');
                recProposal.setFieldValue('entity', companyid);
                recProposal.setFieldValue('memo', '');
                recProposal.setFieldValue('custbody_clgx_contract_terms_title', termstitle);
                recProposal.setFieldValue('custbodyclgx_aa_type', 3);
                recProposal.setFieldValue('custbody_cologix_annual_accelerator', '3');
                recProposal.setFieldValue('otherrefnum', po);
                var stTermsBody = nlapiLookupField('customrecord_clgx_contract_terms', termstitle, 'custrecord_clgx_contract_terms_body');
                recProposal.setFieldValue('custbody_clgx_contract_terms_body', stTermsBody);
                recProposal.setFieldValue('custbody_clgx_order_notes', strMemoOrder.substr(0,999));
                recProposal.setFieldValue('opportunity', recordId);
                recProposal.setFieldValue('entitystatus',11);
                recProposal.setFieldValue('title', title);
                recProposal.setFieldValue('location', facility);
                recProposal.setFieldValue('custbody_clgx_contract_terms_attention', nameAtt);
                recProposal.setFieldValue('custbody_clgx_contract_terms_contact', contactid);
                //  recProposal.setFieldValue('forecasttype', 3);
                // recProposal.setFieldValue('entitystatus', 11);
                recProposal.setFieldValue('billingaddress_text', billing);
                // record.setFieldValue('statusRef', 'inProgress');
                recProposal.setFieldValue('custbody_cologix_opp_sale_type', 2);
                recProposal.setFieldValue('leadsource', 73127);
                //add the items
                if(evc==2) {
                    recProposal.setLineItemValue('item', 'item', 1, arrItems[1]);
                    recProposal.setLineItemValue('item', 'quantity', 1, qty);
                    recProposal.setLineItemValue('item', 'rate', 1, arrRates[1]);
                    recProposal.setLineItemValue('item', 'location', 1, facility);
                    recProposal.setLineItemValue('item', 'price', 1, -1);
                    recProposal.setLineItemValue('item', 'pricelevels', 1, -1);
                    recProposal.setLineItemValue('item', 'amount', 1,arrtotalMRC[1]);
                    recProposal.setLineItemValue('item', 'description', 1, arrSpEVC[1]);
                    recProposal.setLineItemValue('item', 'billingschedule', 1, 3);
                    if (taxcode1 !=0) {
                        recProposal.setLineItemValue('item', 'taxcode', 1, taxcode1);
                    }
                    recProposal.setLineItemValue('item', 'item', 2, arrItems[0]);
                    recProposal.setLineItemValue('item', 'quantity', 2, 1);
                    recProposal.setLineItemValue('item', 'rate', 2, arrRates[0]);
                    recProposal.setLineItemValue('item', 'location', 2, facility);
                    recProposal.setLineItemValue('item', 'price', 2, -1);
                    recProposal.setLineItemValue('item', 'pricelevels', 2, -1);
                    recProposal.setLineItemValue('item', 'amount', 2,arrtotalMRC[0]);
                    recProposal.setLineItemValue('item', 'billingschedule', 2, 3);
                    // recProposal.setLineItemValue('item', 'description', 1, speedName);
                    if (taxcode1 !=0) {
                        recProposal.setLineItemValue('item', 'taxcode', 2, taxcode1);
                    }
                    //item 244
                    recProposal.setLineItemValue('item', 'item', 3, 244);
                    recProposal.setLineItemValue('item', 'quantity', 3, 1);
                    recProposal.setLineItemValue('item', 'rate', 3, totalnrc);
                    recProposal.setLineItemValue('item', 'amount', 3, totalnrc);
                    recProposal.setLineItemValue('item', 'location', 3, facility);
                    recProposal.setLineItemValue('item', 'price', 3, -1);
                    recProposal.setLineItemValue('item', 'pricelevels', 3, -1);
                    recProposal.setLineItemValue('item', 'billingschedule', 3, 17);
                    if (taxcode2 !=0) {
                        recProposal.setLineItemValue('item', 'taxcode', 3, taxcode2);
                    }

                }
                else {
                    recProposal.setLineItemValue('item', 'item', 1, arrItems[1]);
                    recProposal.setLineItemValue('item', 'quantity', 1, qty);
                    recProposal.setLineItemValue('item', 'rate', 1, arrRates[1]);
                    recProposal.setLineItemValue('item', 'location', 1, facility);
                    recProposal.setLineItemValue('item', 'price', 1, -1);
                    recProposal.setLineItemValue('item', 'pricelevels', 1, -1);
                    recProposal.setLineItemValue('item', 'amount', 1, arrtotalMRC[1]);
                    recProposal.setLineItemValue('item', 'description', 1, arrSpEVC[1]);
                    recProposal.setLineItemValue('item', 'billingschedule', 1, 3);
                    if (taxcode1 !=0) {
                        recProposal.setLineItemValue('item', 'taxcode', 1, taxcode1);
                    }
                }

                try {
                    var recordIdPR = nlapiSubmitRecord(recProposal, true, true);
                    var tranid = nlapiLookupField('estimate',recordIdPR,'tranid');
                    var emailSubjectProposal="New Order SO"+tranid+" - "+title;
                    var agree='The initial term of this Service Order shall be one (1) month and shall automatically renew for successive one (1) month terms, unless terminated by either party, in writing, at least one (1) month prior to the end of the then current term. The Cologix standard rates apply for the ordered services.\n\nNotwithstanding anything to the contrary in the agreement between the parties, during the Term, all service fees identified above shall increase on each 12-month anniversary of the Service Commencement Date in an amount equal to the greater of three percent (3%) or CPI. Such increase shall be automatically invoiced to Customer upon each 12-month anniversary of the Service Commencement Date.\n\nEach of Customer and Cologix hereby consents to the execution of this Service Order by electronic means.\n'
                    var emailBodyProposal = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
                        'Thank you for submitting your order.  A Cologix employee will be contacting you soon regarding the installation of your services.\n\n' +
                        'New Order : SO' + tranid + '\n' +
                        'Title : ' + title + '\n' +
                        'Portal Username : ' + uname + '\n' +
                        'Server Date and Time : ' + timestamp + '\n' +
                        'Server Name : ' + serverIP + '\n\n' +
                        'Contractual Terms Agreed To: '+agree+'\n'+
                        'Sincerely,\n' +
                        salesRepName + '\n\n';
                    if(languagePortal=='fr')
                    {
                        var emailSubjectProposal="Nouvelle Commande SO"+tranid+" - "+title;
                        var agree='La durée initiale de cette ordonnance de service est de un (1) mois et se renouvelle automatiquement pour les termes successifs d’un (1) mois, à moins d’être résilié par l’un ou l’autre des parties, par écrit, au moins un (1) mois avant la fin du Terme actuel. Les tarifs standard de Cologix s’appliquent pour les services commandés.\n\nMalgré toute disposition contraire dans le contrat conclu entre les parties, pendant la durée, tous les frais de service précisés plus haut augmenteront automatiquement tous les douze (12) mois à compter de la date anniversaire de la date de début du service, en fonction d’un taux égal à 3 % ou à l’indice des prix à la consommation, selon le plus élevé de ces deux taux.\n\nChacune des parties, le Client et Cologix, acceptent par les présentes que la signature et le passage de cette Commande de Service soient fait par moyen électronique.\n'
                        var emailBodyProposal = 'Bonjour ' + custContactName + ' (' + custCompName + '),\n\n' +
                            "Nous vous remercions d'avoir soumis votre commande. Un employé Cologix vous contactera bientôt concernant l'installation de vos services.\n\n" +
                            'Nouvelle Commande :  SO'+tranid + '\n' +
                            'Titre : ' + title + '\n' +
                            'Nom d’utilisateur du portail : ' + uname + '\n' +
                            'Date et heure du serveur : ' + timestamp + '\n' +
                            'Nom du serveur : ' + serverIP + '\n' +
                            'Termes contractuels convenue par: '+agree+'\n'+
                            'Au plaisir de vous être utile,\n' +
                            salesRepName + '\n\n';
                    }
                    nlapiSendEmail(salesRepID,contactid,emailSubjectProposal,emailBodyProposal,null,null,null,null,true);

                }
                catch (error) {
                    nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
                }

                var recordQ = nlapiCreateRecord('customrecord_clgx_cp_q_transactions');
                recordQ.setFieldValue('custrecord_clgx_cp_tq_oppid', recordId);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_cust', companyid);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_contact', contactid);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_conemail', emailContact);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_ratecard', ratecard);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_folderid', boxfolder);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_trpr', 'T');
                recordQ.setFieldValue('custrecord_clgx_cp_tq_portalserver', serverIP);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_add_vxc', 'T');
                if(evc==2){
                    recordQ.setFieldValue('custrecord_clgx_cp_tq_add_xc', 'T');
                    recordQ.setFieldValue('custrecord_clgx_cp_tq_add_vxc', 'T');
                    recordQ.setFieldValue('custrecord_clgx_cp_tq_xcevc', 2);
                    recordQ.setFieldValue('custrecord_clgx_cp_tq_xcpid', port_name);
                }
                if (recordIdPR != null) {
                    var tranid = nlapiLookupField('estimate',recordIdPR,'tranid');
                    recordQ.setFieldValue('custrecord_clgx_cp_tq_propid', tranid);
                    recordQ.setFieldValue('custrecord_clgx_cp_tq_prid', recordIdPR);
                    var recordQID = nlapiSubmitRecord(recordQ, true, true);
                    var totals = clgx_transaction_totals(companyid, 'proposal', recordIdPR);
                    var recordQID = nlapiSubmitRecord(recordQ, true, true);
                    var arrParam = new Array();
                    arrParam['custscript_ss_cp_order_id'] = recordIdPR;
                    arrParam['custscript_ss_cp_order_lan'] = languagePortal;
                    arrParam['custscript_ss_cp_order_title'] = title;
                    arrParam['custscript_ss_cp_order_company']=companyid;
                    arrParam['custscript_ss_cp_order_contact']=contactid;
                    arrParam['custscript_ss_cp_order_trq']=recordQID;
                    nlapiLogExecution('DEBUG', 'SS', recordIdPR+';'+languagePortal+';'+';'+title+';'+companyid+';'+contactid+';'+recordQID);
                    var status = nlapiScheduleScript('customscript_clgx_ss_cp_order', null, arrParam);
                    //   var requestURL = nlapiRequestURL('https://my.cologix.com/portal/echosign/upload/?id=' + recordQID);

                }
            }
        }

    }
    //PORT ORDER
    else {




        if (ratecard != 0) {
            var record = nlapiCreateRecord('opportunity');
            record.setFieldValue('title', title);
            record.setFieldValue('entity', companyid);
            record.setFieldValue('memo', strMemoOrder.substr(0,999));
            record.setFieldValue('custbody_clgx_so_portal_req', requestor);
            record.setFieldValue('location', facility);
            record.setFieldValue('forecasttype', 3);
            record.setFieldValue('entitystatus', 11);
            // record.setFieldValue('memo', strMemo.substr(0,999));
            // record.setFieldValue('statusRef', 'inProgress');
            record.setFieldValue('custbody_cologix_opp_sale_type', 2);
            record.setFieldValue('leadsource', 73127);
            record.setLineItemValue('item', 'item', 1, item);
            record.setLineItemValue('item', 'quantity', 1, qty);
            record.setLineItemValue('item', 'rate', 1, 0);
            record.setLineItemValue('item', 'location', 1, facility);
            record.setLineItemValue('item', 'price', 1, -1);
            record.setLineItemValue('item', 'pricelevels', 1, -1);
            record.setLineItemValue('item', 'amount', 1, 0);
            if (taxcode1 !=0) {
                record.setLineItemValue('item', 'taxcode', 1, taxcode1);
            }
            //item 244
            record.setLineItemValue('item', 'item', 2, 244);
            record.setLineItemValue('item', 'quantity', 2, qty);
            record.setLineItemValue('item', 'rate', 2, 0);
            record.setLineItemValue('item', 'amount', 2, 0);
            record.setLineItemValue('item', 'location', 2, facility);
            record.setLineItemValue('item', 'price', 2, -1);
            record.setLineItemValue('item', 'pricelevels', 2, -1);
            if (taxcode2 !=0) {
                record.setLineItemValue('item', 'taxcode', 2, taxcode2);
            }
            try {
                var recordId = nlapiSubmitRecord(record, true, true);

            }
            catch (error) {
                nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
            }
            if (recordId != null) {
                var totals = clgx_transaction_totals(companyid, 'oppty', recordId);
                //create the proposal

                // var recProposal = nlapiTransformRecord(fromrecord, recordId, torecord);
                var termstitle=2;
                if(languagePortal=='fr')
                {
                    var termstitle=3;
                }
                var recProposal = nlapiCreateRecord('estimate');
                recProposal.setFieldValue('entity', companyid);
                recProposal.setFieldValue('memo', '');
                recProposal.setFieldValue('custbody_clgx_contract_terms_title', termstitle);
                recProposal.setFieldValue('otherrefnum', po);
                recProposal.setFieldValue('custbodyclgx_aa_type', 3);
                recProposal.setFieldValue('custbody_cologix_annual_accelerator', '3');
                var stTermsBody = nlapiLookupField('customrecord_clgx_contract_terms', termstitle, 'custrecord_clgx_contract_terms_body');
                recProposal.setFieldValue('custbody_clgx_contract_terms_body', stTermsBody);
                recProposal.setFieldValue('custbody_clgx_order_notes', strMemoOrder.substr(0,999));
                recProposal.setFieldValue('opportunity', recordId);
                recProposal.setFieldValue('entitystatus',11);
                recProposal.setFieldValue('title', title);
                recProposal.setFieldValue('location', facility);
                recProposal.setFieldValue('custbody_clgx_contract_terms_contact', contactid);
                //  recProposal.setFieldValue('forecasttype', 3);
                // recProposal.setFieldValue('entitystatus', 11);
                recProposal.setFieldValue('billingaddress_text', billing);
                // record.setFieldValue('statusRef', 'inProgress');
                recProposal.setFieldValue('custbody_cologix_opp_sale_type', 2);
                recProposal.setFieldValue('leadsource', 73127);

                //add the items
                recProposal.setLineItemValue('item', 'item', 1, item);
                recProposal.setLineItemValue('item', 'quantity', 1, qty);
                recProposal.setLineItemValue('item', 'rate', 1, 0);
                recProposal.setLineItemValue('item', 'location', 1, facility);
                recProposal.setLineItemValue('item', 'price', 1, -1);
                recProposal.setLineItemValue('item', 'pricelevels', 1, -1);
                recProposal.setLineItemValue('item', 'amount', 1, 0);
                recProposal.setLineItemValue('item', 'billingschedule', 1, 3);
                if (taxcode1 !=0) {
                    recProposal.setLineItemValue('item', 'taxcode', 1, taxcode1);
                }
                recProposal.setLineItemValue('item', 'item', 2, 244);
                recProposal.setLineItemValue('item', 'quantity', 2, qty);
                recProposal.setLineItemValue('item', 'rate', 2, 0);
                recProposal.setLineItemValue('item', 'amount', 2, 0);
                recProposal.setLineItemValue('item', 'location', 2, facility);
                recProposal.setLineItemValue('item', 'price', 2, -1);
                recProposal.setLineItemValue('item', 'pricelevels', 2, -1);
                recProposal.setLineItemValue('item', 'billingschedule', 2, 17);
                if (taxcode2 !=0) {
                    recProposal.setLineItemValue('item', 'taxcode', 2, taxcode2);
                }
                try {
                    var recordIdPR = nlapiSubmitRecord(recProposal, true, true);
                    var proposalNumber= nlapiLookupField('estimate',recordIdPR,'tranid');
                    if(languagePortal=='en')
                    {

                        var emailSubjectProposal="New Order "+proposalNumber+" - "+custCompName;
                        var agree='The initial term of this Service Order shall be one (1) month and shall automatically renew for successive one (1) month terms, unless terminated by either party, in writing, at least one (1) month prior to the end of the then current term. Your Cologix rate card rates apply for the ordered services.\n\n Notwithstanding anything to the contrary in the agreement between the parties, during the Term, all service fees identified above shall increase on each 12-month anniversary of the Service Commencement Date in an amount equal to the greater of three percent (3%) or CPI. Such increase shall be automatically invoiced to Customer upon each 12-month anniversary of the Service Commencement Date.\n\nEach of Customer and Cologix hereby consents to the execution of this Service Order by electronic means.\n';
                        var emailBodyProposal = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
                            'Thank you for submitting your order. A Cologix employee will be contacting you soon regarding the installation of your services. Please allow up to 24 hours for this order to be visible in the Portal.\n\n' +
                            'New Order : ' + proposalNumber + '\n' +
                            'Title : ' + title + '\n' +
                            'Portal Username : ' + uname + '\n' +
                            'Server Date and Time : ' + timestamp + '\n' +
                            'Server Name : ' + serverIP + '\n' +
                            'Contractual Terms Agreed To: '+agree+'\n'+
                            'Sincerely,\n' +
                            salesRepName + '\n\n';
                    }
                    else if(languagePortal=='fr')
                    {
                        var emailSubjectProposal="Nouvelle Commande "+proposalNumber+" - "+custCompName;
                        var agree='La durée initiale de cette ordonnance de service est de un (1) mois et se renouvelle automatiquement pour les termes successifs d’un (1) mois, à moins d’être résilié par l’un ou l’autre des parties, par écrit, au moins un (1) mois avant la fin du Terme actuel. Vos tarifs de carte tarifaire Cologix s’appliquent pour les services commandés.\n\nThe initial term of this Service Order shall be one (1) month and shall automatically renew for successive one (1) month terms, unless terminated by either party, in writing, at least one (1) month prior to the end of the then current term. Your Cologix rate card rates apply for the ordered services. La durée initiale de cette ordonnance de service est de un (1) mois et se renouvelle automatiquement pour les termes successifs d’un (1) mois, à moins d’être résilié par l’un ou l’autre des parties, par écrit, au moins un (1) mois avant la fin du Terme actuel. Vos tarifs de carte tarifaire Cologix s’appliquent pour les services commandés. Notwithstanding anything to the contrary in the agreement between the parties, during the Term, all service fees identified above shall increase on each 12-month anniversary of the Service Commencement Date in an amount equal to the greater of three percent (3%) or CPI. Such increase shall be automatically invoiced to Customer upon each 12-month anniversary of the Service Commencement Date. Malgré toute disposition contraire dans le contrat conclu entre les parties, pendant la durée, tous les frais de service précisés plus haut augmenteront automatiquement tous les douze (12) mois à compter de la date anniversaire de la date de début du service, en fonction d’un taux égal à 3 % ou à l’indice des prix à la consommation, selon le plus élevé de ces deux taux.\n\nChacune des parties, le Client et Cologix, acceptent par les présentes que la signature et le passage de cette Commande de Service soient fait par moyen électronique.\n';
                        var emailBodyProposal = 'Bonjour ' + custContactName + ' (' + custCompName + '),\n\n' +
                            "Nous vous remercions d'avoir envoyé votre commande. Un employé de Cologix vous contactera rapidement concernant l'installation de vos services. Veuillez laisser jusqu'à 24 heures pour que cette commande soit visible dans le portail.\n\n" +
                            'Nouvelle Order : ' + proposalNumber + '\n' +
                            'Titre : ' + title + '\n' +
                            'Nom d’utilisateur du portail : ' + uname + '\n' +
                            'Date et heure du serveur : ' + timestamp + '\n' +
                            'Nom du serveur : ' + serverIP + '\n' +
                            'Termes contractuels convenue par: '+agree+'\n'+
                            'Au plaisir de vous être utile,\n' +
                            salesRepName + '\n\n';
                    }
                    nlapiSendEmail(salesRepID,contactid,emailSubjectProposal,emailBodyProposal,null,null,null,null,true);
                    if(ccm!=null) {
                        nlapiSendEmail(salesRepID, ccm, emailSubjectProposal, emailBodyProposal, null, null, null, null, true);
                    }
                }
                catch (error) {
                    nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
                }
                var recordQ = nlapiCreateRecord('customrecord_clgx_cp_q_transactions');
                recordQ.setFieldValue('custrecord_clgx_cp_tq_oppid', recordId);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_cust', companyid);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_contact', contactid);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_conemail', emailContact);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_ratecard', ratecard);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_folderid', boxfolder);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_portalserver', serverIP);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_trpr', 'T');
                if (recordIdPR != null) {
                    var tranid = nlapiLookupField('estimate',recordIdPR,'tranid');
                    recordQ.setFieldValue('custrecord_clgx_cp_tq_propid', tranid);
                    recordQ.setFieldValue('custrecord_clgx_cp_tq_prid', recordIdPR);
                    var recordQID = nlapiSubmitRecord(recordQ, true, true);
                    var serverIP = nlapiLookupField('customrecord_clgx_cp_q_transactions', recordQID,'custrecord_clgx_cp_tq_portalserver');
                    //  var requestURL = nlapiRequestURL('https://my.cologix.com/portal/echosign/upload/?id=' + recordQID);
                    var totals = clgx_transaction_totals(companyid, 'proposal', recordIdPR);
                }
            }
        }
        else {
            var record = nlapiCreateRecord('opportunity');
            record.setFieldValue('title', title);
            record.setFieldValue('entity', companyid);
            record.setFieldValue('location', facility);
            record.setFieldValue('memo', strMemoOrder.substr(0,999));
            record.setFieldValue('custbody_clgx_so_portal_req', requestor);
            //record.setFieldValue('forecasttype', 3);
            //record.setFieldValue('entitystatus', 11);
            // record.setFieldValue('statusRef', 'inProgress');
            record.setFieldValue('custbody_cologix_opp_sale_type', 2);
            // record.setFieldValue('memo', strMemo.substr(0,999));
            record.setFieldValue('leadsource', 73127);
            record.setLineItemValue('item', 'item', 1, item);
            record.setLineItemValue('item', 'quantity', 1, qty);
            var itemRate=nlapiLookupField('item', item, 'custitem_clgx_item_install_rate') || 'no';
            var installRate=rate;

            record.setLineItemValue('item', 'rate', 1, rate);
            record.setLineItemValue('item', 'location', 1, facility);
            record.setLineItemValue('item', 'price', 1, 1);
            //record.setLineItemValue('item', 'pricelevels',1, -1);
            record.setLineItemValue('item', 'amount', 1, totalmrc);
            if (taxcode1 !=0) {
                record.setLineItemValue('item', 'taxcode', 1, taxcode1);
            }
            //item 244
            record.setLineItemValue('item', 'item', 2, 244);
            record.setLineItemValue('item', 'quantity', 2, 1);
            record.setLineItemValue('item', 'rate', 2, totalnrc);
            record.setLineItemValue('item', 'amount', 2, 0);
            record.setLineItemValue('item', 'amount', 2, totalnrc);
            record.setLineItemValue('item', 'location', 2, facility);
            record.setLineItemValue('item', 'price', 2, -1);
            record.setLineItemValue('item', 'pricelevels', 2, -1);
            if (taxcode2 !=0) {
                record.setLineItemValue('item', 'taxcode', 2, taxcode2);
            }
            try {
                var recordId = nlapiSubmitRecord(record, true, true);
            }
            catch (error) {
                nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
            }
            if (recordId != null) {
                var totals = clgx_transaction_totals(companyid, 'oppty', recordId);
                //create the proposal

                var termstitle=2;
                if(languagePortal=='fr')
                {
                    var termstitle=3;
                }
                var recProposal = nlapiCreateRecord('estimate');
                recProposal.setFieldValue('entity', companyid);
                recProposal.setFieldValue('memo', '');
                recProposal.setFieldValue('custbody_clgx_contract_terms_title', termstitle);
                recProposal.setFieldValue('custbodyclgx_aa_type', 3);
                recProposal.setFieldValue('custbody_cologix_annual_accelerator', '3');
                recProposal.setFieldValue('otherrefnum', po);
                var stTermsBody = nlapiLookupField('customrecord_clgx_contract_terms', termstitle, 'custrecord_clgx_contract_terms_body');
                recProposal.setFieldValue('custbody_clgx_contract_terms_body', stTermsBody);
                recProposal.setFieldValue('custbody_clgx_order_notes', strMemoOrder.substr(0,999));
                recProposal.setFieldValue('opportunity', recordId);
                recProposal.setFieldValue('entitystatus',11);
                recProposal.setFieldValue('title', title);
                recProposal.setFieldValue('location', facility);
                recProposal.setFieldValue('custbody_clgx_contract_terms_attention', nameAtt);
                recProposal.setFieldValue('custbody_clgx_contract_terms_contact', contactid);
                //  recProposal.setFieldValue('forecasttype', 3);
                // recProposal.setFieldValue('entitystatus', 11);
                recProposal.setFieldValue('billingaddress_text', billing);
                // record.setFieldValue('statusRef', 'inProgress');
                recProposal.setFieldValue('custbody_cologix_opp_sale_type', 2);
                recProposal.setFieldValue('leadsource', 73127);
                //add the items
                recProposal.setLineItemValue('item', 'item', 1, item);
                recProposal.setLineItemValue('item', 'quantity', 1, qty);
                var itemRate=nlapiLookupField('item', item, 'custitem_clgx_item_install_rate') || 'no';
                var installRate=rate;
                if(itemRate!='no')
                {
                    installRate=itemRate;
                }
                recProposal.setLineItemValue('item', 'rate', 1, rate);
                recProposal.setLineItemValue('item', 'location', 1, facility);
                recProposal.setLineItemValue('item', 'price', 1, 1);
                //  recProposal.setLineItemValue('item', 'pricelevels',1, -1);
                recProposal.setLineItemValue('item', 'amount', 1, totalmrc);
                recProposal.setLineItemValue('item', 'billingschedule', 1, 3);
                if (taxcode1 !=0) {
                    recProposal.setLineItemValue('item', 'taxcode', 1, taxcode1);
                }
                //item 244
                recProposal.setLineItemValue('item', 'item', 2, 244);
                recProposal.setLineItemValue('item', 'quantity', 2, 1);
                recProposal.setLineItemValue('item', 'rate', 2, totalnrc);
                //recProposal.setLineItemValue('item', 'amount',2, 0);
                recProposal.setLineItemValue('item', 'amount', 2, totalnrc);
                recProposal.setLineItemValue('item', 'location', 2, facility);
                recProposal.setLineItemValue('item', 'price', 2, -1);
                recProposal.setLineItemValue('item', 'pricelevels', 2, -1);
                recProposal.setLineItemValue('item', 'billingschedule', 2, 17);
                if (taxcode2 !=0) {
                    recProposal.setLineItemValue('item', 'taxcode', 2, taxcode2);
                }
                try {
                    var recordIdPR = nlapiSubmitRecord(recProposal, true, true);
                    var tranid = nlapiLookupField('estimate',recordIdPR,'tranid');
                    var emailSubjectProposal="New Order SO"+tranid+" - "+title;
                    var agree='The initial term of this Service Order shall be one (1) month and shall automatically renew for successive one (1) month terms, unless terminated by either party, in writing, at least one (1) month prior to the end of the then current term. The Cologix standard rates apply for the ordered services.\n\nNotwithstanding anything to the contrary in the agreement between the parties, during the Term, all service fees identified above shall increase on each 12-month anniversary of the Service Commencement Date in an amount equal to the greater of three percent (3%) or CPI. Such increase shall be automatically invoiced to Customer upon each 12-month anniversary of the Service Commencement Date.\n\nEach of Customer and Cologix hereby consents to the execution of this Service Order by electronic means.\n'
                    var emailBodyProposal = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
                        'Thank you for submitting your order.  A Cologix employee will be contacting you soon regarding the installation of your services.\n\n' +
                        'New Order : SO' + tranid + '\n' +
                        'Title : ' + title + '\n' +
                        'Portal Username : ' + uname + '\n' +
                        'Server Date and Time : ' + timestamp + '\n' +
                        'Server Name : ' + serverIP + '\n\n' +
                        'Contractual Terms Agreed To: '+agree+'\n'+
                        'Sincerely,\n' +
                        salesRepName + '\n\n';
                    if(languagePortal=='fr')
                    {
                        var emailSubjectProposal="Nouvelle Commande SO"+tranid+" - "+title;
                        var agree='La durée initiale de cette ordonnance de service est de un (1) mois et se renouvelle automatiquement pour les termes successifs d’un (1) mois, à moins d’être résilié par l’un ou l’autre des parties, par écrit, au moins un (1) mois avant la fin du Terme actuel. Les tarifs standard de Cologix s’appliquent pour les services commandés.\n\nMalgré toute disposition contraire dans le contrat conclu entre les parties, pendant la durée, tous les frais de service précisés plus haut augmenteront automatiquement tous les douze (12) mois à compter de la date anniversaire de la date de début du service, en fonction d’un taux égal à 3 % ou à l’indice des prix à la consommation, selon le plus élevé de ces deux taux.\n\nChacune des parties, le Client et Cologix, acceptent par les présentes que la signature et le passage de cette Commande de Service soient fait par moyen électronique.\n'
                        var emailBodyProposal = 'Bonjour ' + custContactName + ' (' + custCompName + '),\n\n' +
                            "Nous vous remercions d'avoir soumis votre commande. Un employé Cologix vous contactera bientôt concernant l'installation de vos services.\n\n" +
                            'Nouvelle Commande :  SO'+tranid + '\n' +
                            'Titre : ' + title + '\n' +
                            'Nom d’utilisateur du portail : ' + uname + '\n' +
                            'Date et heure du serveur : ' + timestamp + '\n' +
                            'Nom du serveur : ' + serverIP + '\n' +
                            'Termes contractuels convenue par: '+agree+'\n'+
                            'Au plaisir de vous être utile,\n' +
                            salesRepName + '\n\n';
                    }
                    nlapiSendEmail(salesRepID,contactid,emailSubjectProposal,emailBodyProposal,null,null,null,null,true);

                }
                catch (error) {
                    nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
                }

                var recordQ = nlapiCreateRecord('customrecord_clgx_cp_q_transactions');
                recordQ.setFieldValue('custrecord_clgx_cp_tq_oppid', recordId);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_cust', companyid);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_contact', contactid);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_conemail', emailContact);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_ratecard', ratecard);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_folderid', boxfolder);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_trpr', 'T');
                recordQ.setFieldValue('custrecord_clgx_cp_tq_add_xc', 'T');
                recordQ.setFieldValue('custrecord_clgx_cp_tq_xcevc', 1);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_xcpid', port_name);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_portalserver', serverIP);
                if (recordIdPR != null) {
                    var tranid = nlapiLookupField('estimate',recordIdPR,'tranid');
                    recordQ.setFieldValue('custrecord_clgx_cp_tq_propid', tranid);
                    recordQ.setFieldValue('custrecord_clgx_cp_tq_prid', recordIdPR);
                    var recordQID = nlapiSubmitRecord(recordQ, true, true);
                    var totals = clgx_transaction_totals(companyid, 'proposal', recordIdPR);
                    var recordQID = nlapiSubmitRecord(recordQ, true, true);
                    var arrParam = new Array();
                    arrParam['custscript_ss_cp_order_id'] = recordIdPR;
                    arrParam['custscript_ss_cp_order_lan'] = languagePortal;
                    arrParam['custscript_ss_cp_order_title'] = title;
                    arrParam['custscript_ss_cp_order_company']=companyid;
                    arrParam['custscript_ss_cp_order_contact']=contactid;
                    arrParam['custscript_ss_cp_order_trq']=recordQID;
                    nlapiLogExecution('DEBUG', 'SS', recordIdPR+';'+languagePortal+';'+';'+title+';'+companyid+';'+contactid+';'+recordQID);
                    var status = nlapiScheduleScript('customscript_clgx_ss_cp_order', null, arrParam);
                    //   var requestURL = nlapiRequestURL('https://my.cologix.com/portal/echosign/upload/?id=' + recordQID);

                }
            }
        }
    }
}