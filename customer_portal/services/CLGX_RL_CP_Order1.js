nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Order.js
//	ScriptID:	customscript_clgx_rl_cp_order
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=676&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/17/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    if(srights.orders > 0){

        var rid = datain['rid'] || "0";
        var id = parseInt(rid, radix);

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('type',null,'GROUP'));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("internalid",null,"is",id));
        var searchType = nlapiSearchRecord('transaction', null, arrFilters, arrColumns);

        var trantype = searchType[0].getValue('type',null,'GROUP');

        //nlapiLogExecution('DEBUG','id', id);

        obj["order"] = {"id":id,"rid":rid};
        if(trantype == 'Opprtnty'){
            obj = get_opportunity(obj);
        }
        if(trantype == 'Estimate'){
            obj = get_proposal(obj);
        }
        if(trantype == 'SalesOrd'){
            obj = get_salesorder(obj,radix);
        }

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing invoices.';

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_INVOICES';
        obj.msg = 'No rights for invoices.';
    }

    return obj;

});


var put = wrap(function put(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    if(srights.orders > 0){

        var title= datain['title'] || "";
        var facility = datain['facility'] || '0';
        var spaceid = datain['spaceid'] || "";
        var afacility = datain['afacility'] || "0";
        var tfacility = datain['tfacility'] || "";
        var metrotype = datain['metrotype'] || "";
        var conn_type = datain['conn_type'] || "";
        var speed = datain['speed'] || "";
        var loa=datain['loa']||'0';
        var billing=datain['billing']||'0';
        var requested=datain['requested']||"";
        var qty=datain['qty']||"";
        var rate=datain['rate']||"";
        var totalmrc=datain['totalmrc']||"";
        var ratecard=datain['ratecard']||"";
        var other=datain['other']||'0';
        var item=datain['item']||'0';
        var languagePortal=datain['lanportal']||'en';
        var notes=datain['notes']||"";
        var po=datain['po']||"";
        var arrCustomerDetails = nlapiLookupField('customer',companyid,['salesrep','companyname','custentity_clgx_cust_ccm']);
        var salesRepID = arrCustomerDetails['salesrep'];
        var custCompName = arrCustomerDetails['companyname'];
        var ccm = arrCustomerDetails['custentity_clgx_cust_ccm'];
        var fname=nlapiLookupField('contact', contactid, 'firstname') || '';
        var lname=nlapiLookupField('contact', contactid, 'lastname') || '';
        var emailContact=nlapiLookupField('contact', contactid, 'email') || '';
        var custContactName=fname+' '+lname;
        var strMemoOrder =
            'Space ID : ' + spaceid + ';\n\n' +
            ' Terminating Facility : ' + tfacility + ';\n\n' +
            ' Metro Connect Type : ' + metrotype + ';\n\n' +
            ' Cross Connect Type : ' + conn_type + ';\n\n' +
            ' Speed : ' + speed + ';\n\n' +
            ' Notes : ' + notes;

        if(parseInt(salesRepID) > 0){
            var arrRepDetails = nlapiLookupField('employee',salesRepID,['entityid']);
            var salesRepName = arrRepDetails['entityid'];
        }
        else{
            var salesRepName = '';
        }
        var taxcode1 = 0;
        var taxcode2 = 0;

        //MTL
        if (facility == 5 || facility == 8 || facility == 9 || facility == 10 || facility == 11 || facility == 12 || facility == 27) {
            var taxcode1 = 308;
            var taxcode2 = 308;
        } else if (facility == 6 || facility == 13 || facility == 15) { //TOR
            var taxcode1 = 11;
            var taxcode2 = 11;
        }
        else if (facility == 7 || facility == 28) { //VAN
            var taxcode1 = 297;
            var taxcode2 = 297;
        }
        else if (facility == 34 || facility == 39 || facility == 38) { //COL
            var taxcode1 = 515;
            var taxcode2 = 515;
        }
        else if (facility == 31 || facility == 40) { //JAX
            var taxcode2 = 498;
        }
        else if (facility == 42) { //LAK
            var taxcode2 = 565;
        }
        var date = new Date();
        nlapiLogExecution('DEBUG', 'title', title);
        nlapiLogExecution('DEBUG', 'facility', facility);
        nlapiLogExecution('DEBUG', 'totalmrc', totalmrc);
        nlapiLogExecution('DEBUG', 'billing', billing);


        // create a new opportunity



        if (ratecard != 0) {

            //create the proposal

            // var recProposal = nlapiTransformRecord(fromrecord, recordId, torecord);
            var termstitle=2;
            if(languagePortal=='fr')
            {
                var termstitle=3;
            }
            var recProposal = nlapiCreateRecord('estimate');
            recProposal.setFieldValue('entity', companyid);
            recProposal.setFieldValue('memo', '');
            recProposal.setFieldValue('custbody_clgx_contract_terms_title', termstitle);
            recProposal.setFieldValue('otherrefnum', po);
            recProposal.setFieldValue('custbodyclgx_aa_type', 3);
            recProposal.setFieldValue('custbody_cologix_annual_accelerator', '3');
            var stTermsBody = nlapiLookupField('customrecord_clgx_contract_terms', termstitle, 'custrecord_clgx_contract_terms_body');
            recProposal.setFieldValue('custbody_clgx_contract_terms_body', stTermsBody);
            recProposal.setFieldValue('custbody_clgx_order_notes', strMemoOrder.substr(0,999));
            // recProposal.setFieldValue('opportunity', recordId);
            recProposal.setFieldValue('title', title);
            recProposal.setFieldValue('location', facility);
            recProposal.setFieldValue('custbody_clgx_contract_terms_contact', contactid);
            //  recProposal.setFieldValue('forecasttype', 3);
            // recProposal.setFieldValue('entitystatus', 11);
            recProposal.setFieldValue('billingaddress_text', billing);
            // record.setFieldValue('statusRef', 'inProgress');
            recProposal.setFieldValue('custbody_cologix_opp_sale_type', 2);
            recProposal.setFieldValue('leadsource', 73127);
            recProposal.setFieldValue('custbody_clgx_target_install_date', requested);

            //add the items
            recProposal.setLineItemValue('item', 'item', 1, item);
            recProposal.setLineItemValue('item', 'quantity', 1, qty);
            recProposal.setLineItemValue('item', 'rate', 1, 0);
            recProposal.setLineItemValue('item', 'location', 1, facility);
            recProposal.setLineItemValue('item', 'price', 1, -1);
            recProposal.setLineItemValue('item', 'pricelevels', 1, -1);
            recProposal.setLineItemValue('item', 'amount', 1, 0);
            recProposal.setLineItemValue('item', 'billingschedule', 1, 3);
            if (taxcode1 > 0) {
                recProposal.setLineItemValue('item', 'taxcode', 1, taxcode1);
            }
            recProposal.setLineItemValue('item', 'item', 2, 244);
            recProposal.setLineItemValue('item', 'quantity', 2, qty);
            recProposal.setLineItemValue('item', 'rate', 2, 0);
            recProposal.setLineItemValue('item', 'amount', 2, 0);
            recProposal.setLineItemValue('item', 'location', 2, facility);
            recProposal.setLineItemValue('item', 'price', 2, -1);
            recProposal.setLineItemValue('item', 'pricelevels', 2, -1);
            recProposal.setLineItemValue('item', 'billingschedule', 2, 17);
            if (taxcode2 > 0) {
                recProposal.setLineItemValue('item', 'taxcode', 2, taxcode2);
            }
            try {
                var recordIdPR = nlapiSubmitRecord(recProposal, true, true);
                var proposalNumber= nlapiLookupField('estimate',recordIdPR,'tranid');
                if(languagePortal=='en')
                {

                    var emailSubjectProposal="New Proposal "+proposalNumber+" - "+custCompName;
                    var emailBodyProposal = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
                        'New Proposal : ' + proposalNumber + '\n' +
                        'Title : ' + title + '\n' +
                        'Thank you for your service request.  We are reviewing the details and will follow up with you shortly.\n\n' +
                        'Sincerely,\n' +
                        salesRepName + '\n\n';
                }
                else if(languagePortal=='fr')
                {
                    var emailSubjectProposal="Nouvelle Proposition "+proposalNumber+" - "+custCompName;
                    var emailBodyProposal = 'Bonjour ' + custContactName + ' (' + custCompName + '),\n\n' +
                        'Nouvelle Proposition : ' + proposalNumber + '\n' +
                        'Titre : ' + title + '\n' +
                        "Nous vous remercions de votre demande de service. Nous examinons les détails et nous suivrons avec vous sous peu.\n\n" +
                        'Au plaisir de vous être utile,\n' +
                        salesRepName + '\n\n';
                }
                //  nlapiSendEmail(salesRepID,contactid,emailSubjectProposal,emailBodyProposal,null,null,null,null,true);
                if(ccm!=null) {
                    //    nlapiSendEmail(salesRepID, ccm, emailSubjectProposal, emailBodyProposal, null, null, null, null, true);
                }
            }
            catch (error) {
                nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
            }
            if (recordIdPR != null) {
                var totals = clgx_transaction_totals(companyid, 'proposal', recordIdPR);

            }

        }
        else {
            //create the proposal

            var termstitle=2;
            if(languagePortal=='fr')
            {
                var termstitle=3;
            }
            var recProposal = nlapiCreateRecord('estimate');
            recProposal.setFieldValue('entity', companyid);
            recProposal.setFieldValue('memo', '');
            recProposal.setFieldValue('custbody_clgx_contract_terms_title', termstitle);
            recProposal.setFieldValue('custbodyclgx_aa_type', 3);
            recProposal.setFieldValue('custbody_cologix_annual_accelerator', '3');
            recProposal.setFieldValue('otherrefnum', po);
            var stTermsBody = nlapiLookupField('customrecord_clgx_contract_terms', termstitle, 'custrecord_clgx_contract_terms_body');
            recProposal.setFieldValue('custbody_clgx_contract_terms_body', stTermsBody);
            recProposal.setFieldValue('custbody_clgx_order_notes', strMemoOrder.substr(0,999));
            //    recProposal.setFieldValue('opportunity', recordId);
            recProposal.setFieldValue('title', title);
            recProposal.setFieldValue('location', facility);
            recProposal.setFieldValue('custbody_clgx_contract_terms_contact', contactid);
            //  recProposal.setFieldValue('forecasttype', 3);
            // recProposal.setFieldValue('entitystatus', 11);
            recProposal.setFieldValue('billingaddress_text', billing);
            // record.setFieldValue('statusRef', 'inProgress');
            recProposal.setFieldValue('custbody_cologix_opp_sale_type', 2);
            recProposal.setFieldValue('leadsource', 73127);
            recProposal.setFieldValue('custbody_clgx_target_install_date', requested);

            //add the items
            recProposal.setLineItemValue('item', 'item', 1, item);
            recProposal.setLineItemValue('item', 'quantity', 1, qty);
            recProposal.setLineItemValue('item', 'rate', 1, rate);
            recProposal.setLineItemValue('item', 'location', 1, facility);
            recProposal.setLineItemValue('item', 'price', 1, 1);
            //  recProposal.setLineItemValue('item', 'pricelevels',1, -1);
            recProposal.setLineItemValue('item', 'amount', 1, totalmrc);
            recProposal.setLineItemValue('item', 'billingschedule', 1, 3);
            if (taxcode1 > 0) {
                recProposal.setLineItemValue('item', 'taxcode', 1, taxcode1);
            }
            //item 244
            recProposal.setLineItemValue('item', 'item', 2, 244);
            recProposal.setLineItemValue('item', 'quantity', 2, qty);
            recProposal.setLineItemValue('item', 'rate', 2, rate);
            //recProposal.setLineItemValue('item', 'amount',2, 0);
            recProposal.setLineItemValue('item', 'amount', 2, parseInt(qty) * parseInt(rate));
            recProposal.setLineItemValue('item', 'location', 2, facility);
            recProposal.setLineItemValue('item', 'price', 2, -1);
            recProposal.setLineItemValue('item', 'pricelevels', 2, -1);
            recProposal.setLineItemValue('item', 'billingschedule', 2, 17);
            if (taxcode2 > 0) {
                recProposal.setLineItemValue('item', 'taxcode', 2, taxcode2);
            }
            try {
                var recordIdPR = nlapiSubmitRecord(recProposal, true, true);
                var tranid = nlapiLookupField('estimate',recordIdPR,'tranid');
                var emailSubjectProposal="New Order SO"+tranid+" - "+title;
                var emailBodyProposal = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
                    'New Order : SO' + tranid + '\n' +
                    'Title : ' + title + '\n' +
                    'Thank you for submitting your order.  A Cologix employee will be contacting you soon regarding the installation of your services.\n\n' +
                    'Sincerely,\n' +
                    salesRepName + '\n\n';
                if(languagePortal=='fr')
                {
                    var emailSubjectProposal="Nouvelle Commande SO"+tranid+" - "+title;
                    var emailBodyProposal = 'Bonjour ' + custContactName + ' (' + custCompName + '),\n\n' +
                        'Nouvelle Commande :  SO'+tranid + '\n' +
                        'Titre : ' + title + '\n' +
                        "Nous vous remercions d'avoir soumis votre commande. Un employé Cologix vous contactera bientôt concernant l'installation de vos services.\n\n" +
                        'Au plaisir de vous être utile,\n' +
                        salesRepName + '\n\n';
                }
                //   nlapiSendEmail(salesRepID,contactid,emailSubjectProposal,emailBodyProposal,null,null,null,null,true);

            }
            catch (error) {
                nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
            }

            if (recordIdPR != null) {
                var totals = clgx_transaction_totals(companyid, 'proposal', recordIdPR);
                var tranid = nlapiLookupField('estimate',recordIdPR,'tranid');
                var billaddress=nlapiLookupField('estimate',recordIdPR,'billaddress');
                var curr=nlapiLookupField('estimate',recordIdPR,'currency');
                var stSOAtt = nlapiLookupField('estimate',recordIdPR,'custbody_clgx_contract_terms_attention');
                var stSON = nlapiLookupField('estimate',recordIdPR,'custbody_clgx_contract_terms_notes');
                var recProposal = nlapiLoadRecord('estimate',recordIdPR);
                var billAddress1 = recProposal.getFieldValue('billaddr1');
                var billAddress2 = recProposal.getFieldValue('billaddr2');
                var billCity = recProposal.getFieldValue('billcity');
                var billCountry = recProposal.getFieldValue('billcountry');
                var billState = recProposal.getFieldValue('billstate');
                var billZipcode = recProposal.getFieldValue('billzip');
                var stSOLang = nlapiLookupField('customrecord_clgx_contract_terms', termstitle,'custrecord_clgx_contract_terms_lang');
                var fileID=update_proposal(recordIdPR,languagePortal,title,companyid,contactid,tranid,salesRepName,companyid,billaddress,curr,stSOAtt,stSON,billAddress1,billAddress2,billCity,billCountry,billState,billZipcode,termstitle,stSOLang,stTermsBody,facility,item,qty,rate,totalmrc);
            }

        }
        if(recordIdPR!=null)
        {


            obj["propid"]=tranid;
            obj["loa"]=loa;
            obj["id"] = recordIdPR;
            obj["number"] = tranid;
            obj["type"] =  "proposal";
            obj["emailContact"]=emailContact;
            var pdf = nlapiLoadFile(fileID);
            obj["rpdf"] = fileID.toString(radix);
            //obj["pdfurl"] = 'https://system.na2.netsuite.com' + pdf.getURL();
            obj["pdfurl"] = 'https://1337135.app.netsuite.com' + pdf.getURL();
            obj["pdfname"] = pdf.getName().replace(/,/g, "");
            nlapiLogExecution('DEBUG', 'pdfurl', obj["pdfurl"]);
            nlapiLogExecution('DEBUG', 'pdfname', obj["pdfname"]);

            //schedule script opportunity+SO
            var arrParam = new Array();
            arrParam['custscript_ss_cp_prop_id'] = recordIdPR;
            arrParam['custscript_ss_cp_order_lan1'] = languagePortal;
            arrParam['custscript_ss_cp_order_title1'] = title;
            arrParam['custscript_ss_cp_order_company1']=companyid;
            arrParam['custscript_ss_cp_order_location']=facility;
            arrParam['custscript_ss_cp_order_memo']=strMemoOrder.substr(0,999);
            arrParam['custscript_ss_cp_order_install_date']=requested;
            arrParam['custscript_ss_cp_order_item']=item;
            arrParam['custscript_ss_cp_order_quantity']=qty;
            arrParam['custscript_ss_cp_order_rate']=rate;
            arrParam['custscript_ss_cp_order_amount']=totalmrc;
            arrParam['custscript_ss_cp_order_taxcode1']=taxcode1;
            arrParam['custscript_ss_cp_order_taxcode2']=taxcode2;
            var status = nlapiScheduleScript('customscript_clgx_ss_cp_order1', null, arrParam);
        }


        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'A new order has been added.';

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_CASES';
        obj.msg = 'No rights for orders.';
    }

    return obj;

});

function update_proposal(proposalid,languagePortal,titleProp,companyid,contactid,stSONbr,stSORep,stSOCustId,stSOAddr,stCurrency,stSOAtt,stSON,billAddress1,billAddress2,billCity,billCountry,billState,billZipcode,stSOTitle,stSOLang,termsbody,facility,item,qty,rate,totalmrc){
    try{

        // var proposalid = '1203353';

        var linkToFolder = '/app/common/media/mediaitemfolders.nl?folder=';
        var fileFolder = get_contract_fileFolder();
        linkToFolder += fileFolder;

        if(stSON == null){
            stSON = '';
        }
        //nlapiLogExecution('DEBUG','stSONotes = ' + recProposal.getFieldValue('custbody_clgx_contract_terms_notes'));
        var stSONotes = convertBR(stSON);
        stSONotes = stSONotes.replace(/\&/g," and ");
        stSONotes = stSONotes.replace(/'/g, "&rsquo;");
        if(termsbody == null){
            termsbody = '';
        }
        termsbody = termsbody.replace(/&/g,'and');

        if (stSOLang == 17){ // if French
            var stSOTerms = convertBR(termsbody);
            //var stSOTerms = HTMLEncode(termsbody);
            // var stSOTerms = htmlEscape(termsbody);

        }
        else{
            termsbody = termsbody.replace(/'/g, "&rsquo;");
            var stSOTerms = convertBR(termsbody);
        }

        //Determine currency
        var currencyType = '';
        if (stCurrency == 1){
            currencyType = 'USD';
        }
        else if (stCurrency == 3){
            currencyType = 'CDN';
        }
        else{}

        var custCompName = nlapiLookupField('customer', stSOCustId, 'companyname');
        //  var  emailsignature=nlapiLookupField('contact', contactid, 'email');
        custCompName = custCompName.replace(/\&/g,"and");
        custCompName = custCompName.replace(/\ /g,"_");


        if (stSOLang == 17){ // if French
            var stLang = 1;
            var langPrefix = 'FR_';
            var filePrefix = 'SO_';
            var template = 3819552; // Load French Contract Template

        }
        else { // if English
            var stLang = 0;
            var langPrefix = 'EN_';
            var filePrefix = 'SO_';
            var template = 3819551; // Load English Contract Template

        }

        var arrTitleBQ = new Array();
        arrTitleBQ[0] = 'Quote Details';
        arrTitleBQ[1] = 'D&eacute;tails des Services';

        var arrTitleContract = new Array();
        arrTitleContract[0] = 'Services Details';
        arrTitleContract[1] = 'D&eacute;tails des Services';

        var arrTitleBQTotals = new Array();
        arrTitleBQTotals[0] = 'Quote Totals';
        arrTitleBQTotals[1] = 'D&eacute;tails des Services - Totaux ';

        var arrTitleContractTotals = new Array();
        arrTitleContractTotals[0] = 'Services Totals';
        arrTitleContractTotals[1] = 'D&eacute;tails des Services - Totaux ';

        var arrTitleMRC = new Array();
        arrTitleMRC[0] = 'RECURRING CHARGES';
        arrTitleMRC[1] = 'FRAIS R&Eacute;CURRENTS';

        var arrTitleNRC = new Array();
        arrTitleNRC[0] = 'NON RECURRING CHARGES';
        arrTitleNRC[1] = 'FRAIS NON R&Eacute;CURRENTS';

        var arrTitleDisc = new Array();
        arrTitleDisc[0] = 'CATEGORY : ONE TIME DISCOUNT';
        arrTitleDisc[1] = 'CAT&Eacute;GORIE: PREMIER MOIS GRATUIT';

        var arrFootMRC = new Array();
        arrFootMRC[0] = 'Total Recurring Charges';
        arrFootMRC[1] = 'Total des frais r&eacute;currents';

        var arrFootNRC = new Array();
        arrFootNRC[0] = 'Total Non Recurring Charges';
        arrFootNRC[1] = 'Total des frais non r&eacute;currents';


        var arrFootDisc = new Array();
        arrFootDisc[0] = 'Total Discounts';
        arrFootDisc[1] = 'Total r&eacute;ductions';


        var arrFootAllRC = new Array();
        arrFootAllRC[0] = 'Grand Total Recurring Charges';
        arrFootAllRC[1] = 'Grand total des frais r&eacute;currents';

        var arrFootAllNRC = new Array();
        arrFootAllNRC[0] = 'Grand Total Non Recurring Charges';
        arrFootAllNRC[1] = 'Grand total des frais non r&eacute;currents';

        var arrFootAllDisc = new Array();
        arrFootAllDisc[0] = 'Grand Total Discounts';
        arrFootAllDisc[1] = 'Grand total r&eacute;ductions';


        //
        var arrAttn = new Array();
        arrAttn[0] = 'Attn';
        arrAttn[1] = '&Agrave; l\'attention de';

        var arrName = new Array();
        arrName[0] = 'Name';
        arrName[1] = 'Nom';

        var arrDescr = new Array();
        arrDescr[0] = 'Description';
        arrDescr[1] = 'Description';

        var arrTerms = new Array();
        arrTerms[0] = 'Service Term';
        arrTerms[1] = 'P&eacute;riode de service';

        var arrQty = new Array();
        arrQty[0] = 'Qty';
        arrQty[1] = 'Quantit&eacute;';

        var arrRate = new Array();
        arrRate[0] = 'Rate';
        arrRate[1] = 'Tarif';

        var arrAmount = new Array();
        arrAmount[0] = 'Amount';
        arrAmount[1] = 'Montant';



        var arrNotes = new Array();
        arrNotes[0] = 'Notes';
        arrNotes[1] = 'Notes';


        // start items sections --------------------------------------------------------------------------------
        var tabledata = '';

        var nbrItems = 2;
        // build array of unique locations
        var arrLocations = new Array();
        arrLocations.push(facility);

        // loop unique locations
        var grandTotalRC = 0;
        var grandTotalNRC = 0;
        var grandTotalDisc = 0;
        for (var l = 0; arrLocations != null && l < arrLocations.length; l++){
            var arrLocation = nlapiLookupField('location', arrLocations[l], ['name', 'address1', 'address2', 'city', 'country', 'state', 'zip']);
            var stLocName = arrLocation['name'];
            stLocName = stLocName.replace(/\&/g,"and");

            tabledata += '<table class="profile" align="center">';
            tabledata += '<tr>';

            tabledata += '<td><h2>' + arrTitleContract[stLang] + ' / ' + stLocName + '</h2></td>';

            tabledata += '</tr>';
            tabledata += '</table>';
            // there are recurring items, so start recurring block --------------------------------------------------


            tabledata += '<table cellpadding="1" class="profile" align="center" table-layout="fixed">';
            tabledata += '<tr>';
            tabledata += '<td width="30%">&nbsp;</td>';
            tabledata += '<td width="40%">&nbsp;</td>';
            tabledata += '<td width="10%">&nbsp;</td>';
            tabledata += '<td width="10%">&nbsp;</td>';
            tabledata += '<td width="10%">&nbsp;</td>';
            tabledata += '</tr>';

            tabledata += '<tr>';
            tabledata += '<td colspan="5">' + arrTitleMRC[stLang] + '</td>';
            tabledata += '</tr>';
            tabledata += '<tr>';
            tabledata += '<td colspan="5">&nbsp;</td>';
            tabledata += '</tr>';
            tabledata += '<tr>';
            tabledata += '<td class="cool"><b>' + arrDescr[stLang] + '</b></td>';
            tabledata += '<td class="cool"><b>' + arrTerms[stLang] + '</b></td>';
            tabledata += '<td class="cool" align="center"><b>' + arrQty[stLang] + '</b></td>';
            tabledata += '<td class="cool" align="right"><b>' + arrRate[stLang] + '</b></td>';
            tabledata += '<td class="cool" align="right"><b>' + arrAmount[stLang] + '</b></td>';
            tabledata += '</tr>';

            var totalRC = 0;
            var currentLocation = facility;
            var currentBS = 'Month to Month';
            var stTerms='Month to Month';
            var currentID = item;

            var stItemId = item;
            var recItem = nlapiLoadRecord('serviceitem', stItemId);

            var stName = recItem.getFieldValue('displayname');
            if(stLang != 0){
                var frenchName = recItem.getLineItemValue('translations', 'displayname', 3);
                if (frenchName != null && frenchName != ''){
                    stName = frenchName;
                }
            }

            var stQty = qty;
            var stRate = rate;
            var stAmnt = totalmrc;

            if (stItemId != 210) {
                totalRC += parseFloat(stAmnt);
                grandTotalRC += parseFloat(stAmnt);
            }

            tabledata += '<tr>';
            tabledata += '<td>' + nlapiEscapeXML(stName) + '</td>';
            tabledata += '<td>' + nlapiEscapeXML(stTerms) + '</td>';
            tabledata += '<td align="center">' + nlapiEscapeXML(parseFloat(stQty)) + '</td>';
            tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stRate)) + '</td>';
            tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stAmnt)) + '</td>';
            tabledata += '</tr>';




            tabledata += '<tr>';
            tabledata += '<td colspan="5">&nbsp;</td>';
            tabledata += '</tr>';
            tabledata += '<tr>';
            tabledata += '<td colspan="5" class="profile t-border" align="right"><b>' + arrFootMRC[stLang] + ' (' + stLocName + '): ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(totalRC).toFixed(2))) + '</b></td>';
            tabledata += '</tr>';

            tabledata += '</table>';

            // end recurring block ---------------------------------------------------------------------------


            // start non recurring block -----------------------------------------------

            tabledata += '<table cellpadding="1" class="profile" align="center" table-layout="fixed">';
            tabledata += '<tr>';
            tabledata += '<td width="30%">&nbsp;</td>';
            tabledata += '<td width="40%">&nbsp;</td>';
            tabledata += '<td width="10%">&nbsp;</td>';
            tabledata += '<td width="10%">&nbsp;</td>';
            tabledata += '<td width="10%">&nbsp;</td>';
            tabledata += '</tr>';

            tabledata += '<tr>';
            tabledata += '<td colspan="5">' + arrTitleNRC[stLang] + '</td>';
            tabledata += '</tr>';
            tabledata += '<tr>';
            tabledata += '<td colspan="5">&nbsp;</td>';
            tabledata += '</tr>';
            tabledata += '<tr>';
            tabledata += '<td class="cool" colspan="2"><b>' + arrDescr[stLang] + '</b></td>';
            tabledata += '<td class="cool" align="center"><b>' + arrQty[stLang] + '</b></td>';
            tabledata += '<td class="cool" align="right"><b>' + arrRate[stLang] + '</b></td>';
            tabledata += '<td class="cool" align="right"><b>' + arrAmount[stLang] + '</b></td>';
            tabledata += '</tr>';

            var totalNRC = 0;

            var recItem = nlapiLoadRecord('serviceitem', 244);
            var stName = recItem.getFieldValue('displayname');

            if(stLang != 0){
                var frenchName = recItem.getLineItemValue('translations', 'displayname', 3);
                if (frenchName != null && frenchName != ''){
                    stName = frenchName;
                }
            }

            var stQty = qty;
            var stRate = rate;
            var stAmnt = parseInt(qty) * parseInt(rate);

            totalNRC += parseFloat(stAmnt);
            grandTotalNRC += parseFloat(stAmnt);

            tabledata += '<tr>';
            tabledata += '<td colspan="2">' + nlapiEscapeXML(stName) + '</td>';
            tabledata += '<td align="center">' + nlapiEscapeXML(parseFloat(stQty)) + '</td>';
            tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stRate)) + '</td>';
            tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stAmnt)) + '</td>';
            tabledata += '</tr>';



            tabledata += '<tr>';
            tabledata += '<td colspan="5">&nbsp;</td>';
            tabledata += '</tr>';
            tabledata += '<tr>';
            tabledata += '<td colspan="5" class="profile t-border" align="right"><b>' + arrFootNRC[stLang] + ' (' + stLocName + '): ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(totalNRC).toFixed(2))) + '</b></td>';
            tabledata += '</tr>';

            tabledata += '</table>';

            // end non recurring block ------------------------------------------------------------------------



        }
        if(arrLocations != null){
            if(parseInt(arrLocations.length) > 1 && (parseInt(grandTotalRC) > 0 || parseInt(grandTotalNRC) > 0)){

                //tabledata += '<br />';


                tabledata += '<table class="profile page-break" align="center">';
                tabledata += '<tr>';
                tabledata += '<td><h2>' + arrTitleContractTotals[stLang] + '</h2></td>';

                tabledata += '</tr>';
                tabledata += '</table>';

                tabledata += '<table cellpadding="1" class="profile" align="center" table-layout="fixed">';


                if(parseInt(grandTotalRC) > 0){
                    tabledata += '<tr>';
                    tabledata += '<td>&nbsp;</td>';
                    tabledata += '</tr>';
                    tabledata += '<tr>';
                    tabledata += '<td class="profile t-border" align="right"><b>' + arrFootAllRC[stLang] + ': ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(grandTotalRC).toFixed(2))) + '</b></td>';
                    tabledata += '</tr>';
                }
                if(parseInt(grandTotalNRC) > 0){
                    tabledata += '<tr>';
                    tabledata += '<td>&nbsp;</td>';
                    tabledata += '</tr>';
                    tabledata += '<tr>';
                    tabledata += '<td class="profile t-border" align="right"><b>' + arrFootAllNRC[stLang] + ': ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(grandTotalNRC).toFixed(2))) + '</b></td>';
                    tabledata += '</tr>';
                }
                if(parseInt(grandTotalDisc) > 0){
                    tabledata += '<tr>';
                    tabledata += '<td>&nbsp;</td>';
                    tabledata += '</tr>';
                    tabledata += '<tr>';
                    tabledata += '<td class="profile t-border" align="right"><b>' + arrFootAllDisc[stLang] + ': ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(grandTotalDisc).toFixed(2))) + '</b></td>';
                    tabledata += '</tr>';
                }
                tabledata += '</table>';
            }
        }

        //build customer address
        var tablehtmlAddress = '';
        tablehtmlAddress = '<table cellpadding="0" border="0" table-layout="fixed">';
        tablehtmlAddress += '<tr>';
        tablehtmlAddress += '<td>';
        tablehtmlAddress += '<b>' + nlapiEscapeXML(custCompName) + '</b><br/>';
        tablehtmlAddress += '<b>' + arrAttn[stLang] + ':</b>' + nlapiEscapeXML(stSOAtt) + '<br/>';
        tablehtmlAddress += nlapiEscapeXML(billAddress1) + '<br/>';
        //if (billAddress2) {
        //	tablehtmlAddress += nlapiEscapeXML(billAddress2) + '<br/>';
        //}
        tablehtmlAddress += nlapiEscapeXML(billCity) + ', ' + nlapiEscapeXML(billState) + ' ' + nlapiEscapeXML(billZipcode) + ' <br/>';
        tablehtmlAddress += nlapiEscapeXML(billCountry);
        tablehtmlAddress += '</td>';
        tablehtmlAddress += '</tr>';
        tablehtmlAddress += '</table>';

        //build data center/location address

        var arrLocation = nlapiLookupField('location', arrLocations[0], ['name', 'address1', 'address2', 'city', 'country', 'state', 'zip']);
        var stLocName = arrLocation['name'];
        var stLocAddr1 = arrLocation['address1'];
        var stLocAddr2 = arrLocation['address2'];
        var stLocCity = arrLocation['city'];
        var stLocCountry = arrLocation['country'];
        var stLocState = arrLocation['state'];
        var stLocZip = arrLocation['zip'];

        var tablehtmlLocation = '<b>';
        tablehtmlLocation += nlapiEscapeXML(stLocName) + '</b><br/>';
        tablehtmlLocation += nlapiEscapeXML(stLocAddr1) + '<br/>';
        if (stLocAddr2) {
            tablehtmlLocation += nlapiEscapeXML(stLocAddr2) + '<br/>';
        }
        tablehtmlLocation += nlapiEscapeXML(stLocCity) + ', ' + nlapiEscapeXML(stLocState) + ' ' + nlapiEscapeXML(stLocZip) + ' <br/>';
        tablehtmlLocation += nlapiEscapeXML(stLocCountry);

        //build Usage Charges section
        var tablePowerCharges = '';

        //build IP Burst section
        var tableIPBurst = '';



        //build Notes
        var tablehtmlNotes = '';
        if (stSONotes != '') {
            tablehtmlNotes += '<table cellpadding="2" class="profile" align="center" table-layout="fixed" style="page-break-inside: avoid;">';

            // if there is something in Notes
            if(stSONotes != ''){
                tablehtmlNotes += '<tr><td><h2>' + arrNotes[stLang] + '</h2></td></tr>';
                tablehtmlNotes += '<tr><td class="profile t-border" style="font-family: Arial, Helvetica, sans-serif; font-size:7pt;">'+ stSONotes + '</td></tr>';
            }
            tablehtmlNotes += '</table>';
            tablehtmlNotes += '<br/>';
        }

        //build SOs
        var tablehtmlSOs = '';

        var objFile = nlapiLoadFile(template);
        var stMainHTML = objFile.getValue();

        var d = new moment();

        stMainHTML = stMainHTML.replace(new RegExp('{location}', 'g'), tablehtmlLocation);
        stMainHTML = stMainHTML.replace(new RegExp('{billAddress}', 'g'), tablehtmlAddress);
        stMainHTML = stMainHTML.replace(new RegExp('{sodate}', 'g'), d.format('M/D/YYYY'));
        stMainHTML = stMainHTML.replace(new RegExp('{sonbr}', 'g'), stSONbr);
        stMainHTML = stMainHTML.replace(new RegExp('{sorep}', 'g'), stSORep);
        stMainHTML = stMainHTML.replace(new RegExp('{totalcharges}', 'g'), '');
        stMainHTML = stMainHTML.replace(new RegExp('{items}', 'g'), tabledata);
        stMainHTML = stMainHTML.replace(new RegExp('{usage}', 'g'), tablePowerCharges);
        stMainHTML = stMainHTML.replace(new RegExp('{ipburst}', 'g'), tableIPBurst);
        stMainHTML = stMainHTML.replace(new RegExp('{notes}', 'g'), tablehtmlNotes);
        stMainHTML = stMainHTML.replace(new RegExp('{sos}', 'g'), tablehtmlSOs);
        stMainHTML = stMainHTML.replace(new RegExp('{terms}', 'g'), stSOTerms);



        //var htmlName = filePrefix + langPrefix + stSONbr + '_' + custCompName + '_' + d.format('M/D/YYYY') + '_' + d.format('H-m-s') + '_' + currencyType + '.html'
        //var htmlFile = nlapiCreateFile(htmlName, 'PLAINTEXT', stMainHTML);
        //htmlFile.setFolder(fileFolder);
        //nlapiSubmitFile(htmlFile);
        if (stSOLang != 17) { // if French
            stMainHTML = stMainHTML.replace(/[^A-Za-z 0-9 \.,\?""!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '');
        }
        stMainHTML = stMainHTML.replace(new RegExp('{signerDate}', 'g'), d.format('ll'));
        var filePDF = nlapiXMLToPDF(stMainHTML);
        //custCompName = custCompName.replace(/\ /g,"_");
        filePDF.setName(filePrefix + langPrefix + stSONbr + '_' + custCompName + '_' + d.format('M/D/YYYY') + '_' + d.format('H-m-s') + '_' + currencyType + '.pdf');
        filePDF.setFolder(fileFolder);

        var fileId = nlapiSubmitFile(filePDF);

        nlapiSubmitField('estimate', proposalid, 'custbody_clgx_prop_contract_file_id', fileId);

        nlapiSubmitField('estimate', proposalid, 'custbody_clgx_contract_terms_ready', 'F');
        nlapiSubmitField('estimate', proposalid, 'custbody_clgx_print_budgetray_quote', 'F');

        // var agreement=AgreementCreater(proposalid);
        // nlapiSendEmail(206211,206211,'agreement',agreement,null,null,null,null);
        nlapiLogExecution('DEBUG','Finished Execution', '|-------------------------- Finished Scheduled Script --------------------------|');
        return fileId;
    }
    catch (error){

        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
         //   throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
           // throw nlapiCreateError('99999', error.toString());
        }
    }
}

function get_opportunity(obj){

    var rec = nlapiLoadRecord('opportunity', obj.order.id);
    obj.order["node_en"] = rec.getFieldValue('tranid');
    obj.order["node_fr"] = rec.getFieldValue('tranid');
    obj.order["type"] = 'o';
    obj.order["faicon"] = 'fa fa-cogs';
    obj.order["leaf"] = false;

    var nbrItems = rec.getLineItemCount('item');
    var items = [];
    for (var i = 0; i < nbrItems; i++) {

        var arrName = (rec.getLineItemText('item', 'item', i + 1)).split(":");
        var arrLoc = (rec.getLineItemText('item', 'location', i + 1)).split(":");
        var location = arrLoc[arrLoc.length - 1];
        var memo = rec.getLineItemValue('item', 'memo', i + 1) || '';
        var category = rec.getLineItemValue('item', 'custcol_cologix_invoice_item_category', i + 1) || 0;

        var quantity = parseFloat(rec.getLineItemValue('item', 'quantity', i + 1));
        var rate = parseFloat(rec.getLineItemValue('item', 'rate', i + 1));
        var amount = parseFloat(rec.getLineItemValue('item', 'amount', i + 1));

        var itemid = rec.getLineItemValue('item', 'item', i + 1);
        var recItem = nlapiLoadRecord('serviceitem', itemid);
        var item_en = arrName[arrName.length - 1];
        var item_fr = item_en;
        var french = recItem.getLineItemValue('translations', 'displayname', 3);
        if (french){
            item_fr = french;
        }
        var item = {
            "node_en": item_en,
            "node_fr": item_fr,
            "type": "i",
            "category": category,
            "memo": memo,
            "quantity": quantity,
            "rate": rate,
            "amount": amount,
            "location": location,
            "faicon": 'fa fa-cogs',
            "leaf": true
        };
        items.push(item);
    }
    obj.order["children"] = items;

    return obj;
}

function get_proposal(obj){

    var rec = nlapiLoadRecord('estimate', obj.order.id);
    obj.order["node_en"] = rec.getFieldValue('tranid');
    obj.order["node_fr"] = rec.getFieldValue('tranid');
    obj.order["type"] = 'p';
    obj.order["faicon"] = 'fa fa-cogs';
    obj.order["leaf"] = false;

    var nbrItems = rec.getLineItemCount('item');
    var items = [];
    for (var i = 0; i < nbrItems; i++) {

        var arrName = (rec.getLineItemText('item', 'item', i + 1)).split(":");
        var arrLoc = (rec.getLineItemText('item', 'location', i + 1)).split(":");
        var location = arrLoc[arrLoc.length - 1];
        var memo = rec.getLineItemValue('item', 'memo', i + 1) || '';
        var category = rec.getLineItemValue('item', 'custcol_cologix_invoice_item_category', i + 1) || 0;

        var quantity = parseFloat(rec.getLineItemValue('item', 'quantity', i + 1));
        var rate = parseFloat(rec.getLineItemValue('item', 'rate', i + 1));
        var amount = parseFloat(rec.getLineItemValue('item', 'amount', i + 1));

        var itemid = rec.getLineItemValue('item', 'item', i + 1);
        var recItem = nlapiLoadRecord('serviceitem', itemid);
        var item_en = arrName[arrName.length - 1];
        var item_fr = item_en;
        var french = recItem.getLineItemValue('translations', 'displayname', 3);
        if (french){
            item_fr = french;
        }

        var item = {
            "node_en": item_en,
            "node_fr": item_fr,
            "type": "i",
            "category": category,
            "memo": memo,
            "quantity": quantity,
            "rate": rate,
            "amount": amount,
            "location": location,
            "faicon": 'fa fa-cogs',
            "leaf": true
        };
        items.push(item);
    }
    obj.order["children"] = items;

    return obj;
}

function get_salesorder(obj,radix){

    var rec = nlapiLoadRecord('salesorder', obj.order.id);
    obj.order["node_en"] = rec.getFieldValue('tranid');
    obj.order["node_fr"] = rec.getFieldValue('tranid');
    obj.order["type"] = 's';

    var legacydate = rec.getFieldValue('custbody_cologix_legacy_inst_dt') || '';
    var installdate = rec.getFieldValue('custbody_cologix_service_actl_instl_dt') || '';

    if(legacydate){
        obj.order["install"] = legacydate;
    } else {
        obj.order["install"] = installdate;
    }

    obj.order["start"] = rec.getFieldValue('custbody_cologix_so_contract_start_dat') || '';
    obj.order["end"] = rec.getFieldValue('enddate') || '';
    obj.order["terms"] = rec.getFieldValue('custbody_cologix_biling_terms') || '';
    obj.order["accelerator"] = rec.getFieldValue('custbody_cologix_annual_accelerator') || '';
    obj.order["legacy_id"] = rec.getFieldValue('custbody_cologix_legacy_so') || '';
    obj.order["expanded"] = true;
    obj.order["faicon"] = 'fa fa-cogs';
    obj.order["leaf"] = false;

    var nbrItems = rec.getLineItemCount('item');
    var items = [];
    for (var i = 0; i < nbrItems; i++) {

        var sched = rec.getLineItemValue('item', 'billingschedule', i + 1);
        var cls = rec.getLineItemText('item', 'class', i + 1);

        if(sched || cls.indexOf("NRC") > -1){

            var arrName = (rec.getLineItemText('item', 'item', i + 1)).split(":");
            var arrLoc = (rec.getLineItemText('item', 'location', i + 1)).split(":");
            var location = arrLoc[arrLoc.length - 1];
            var memo = rec.getLineItemValue('item', 'description', i + 1) || '';
            var category = rec.getLineItemValue('item', 'custcol_cologix_invoice_item_category', i + 1) || 0;

            var service = parseInt(rec.getLineItemValue('item', 'custcol_clgx_so_col_service_id', i + 1)) || 0;
            var quantity = parseFloat(rec.getLineItemValue('item', 'custcol_clgx_qty2print', i + 1));
            var rate = parseFloat(rec.getLineItemValue('item', 'rate', i + 1));
            var amount = quantity * rate;
            var itemid = rec.getLineItemValue('item', 'item', i + 1);
            try {
                var recItem = nlapiLoadRecord('serviceitem', itemid);
                var item_en = arrName[arrName.length - 1];
                var item_fr = item_en;
                var french = recItem.getLineItemValue('translations', 'displayname', 3);
                if (french){
                    item_fr = french;
                }
                var item = {
                    "node_en": item_en,
                    "node_fr": item_fr,
                    "type": "i",
                    "category": category,
                    "memo": memo,
                    "quantity": quantity,
                    "rate": rate,
                    "amount": amount,
                    "location": location,
                    "service": service,
                    "children": get_inventory(service,radix),
                    "faicon": 'fa fa-cog',
                    "leaf": false
                };
                items.push(item);
            }
            catch (error) {
            }
        }
    }
    obj.order["children"] = items;

    return obj;
}

function get_inventory(service,radix){

    var children = [];
    if(service){

        var rec = nlapiLoadRecord('job', service);
        var columns = new Array();
        columns.push(new nlobjSearchColumn('internalid',null,null));
        columns.push(new nlobjSearchColumn('name',null,null).setSort(false));
        var filters = new Array();
        filters.push(new nlobjSearchFilter("custrecord_cologix_power_service",null,"anyof",service));
        var search = nlapiSearchRecord('customrecord_clgx_power_circuit', null, filters, columns);
        for ( var i = 0; search != null && i < search.length; i++ ) {
            var intid = parseInt(search[i].getValue('internalid',null,null)) || 0;
            var name = search[i].getValue('name',null,null) || '';
            var child = {
                //"id": intid,
                "type": "power",
                "rid": intid.toString(radix),
                "node_en": name,
                "node_fr": name,
                "faicon": 'fa fa-plug',
                "leaf": true
            };
            children.push(child);
        }

        var columns = new Array();
        columns.push(new nlobjSearchColumn('internalid',null,null));
        columns.push(new nlobjSearchColumn('name',null,null).setSort(false));
        var filters = new Array();
        filters.push(new nlobjSearchFilter("custrecord_cologix_space_project",null,"anyof",service));
        var search = nlapiSearchRecord('customrecord_cologix_space', null, filters, columns);
        for ( var i = 0; search != null && i < search.length; i++ ) {
            var intid = parseInt(search[i].getValue('internalid',null,null)) || 0;
            var name = search[i].getValue('name',null,null) || '';
            var child = {
                //"id": intid,
                "type": "space",
                "rid": intid.toString(radix),
                "node_en": name,
                "node_fr": name,
                "faicon": 'fa fa-map-o',
                "leaf": true
            };
            children.push(child);
        }

        var columns = new Array();
        columns.push(new nlobjSearchColumn('internalid',null,null));
        columns.push(new nlobjSearchColumn('name',null,null).setSort(false));
        columns.push(new nlobjSearchColumn('custrecord_cologix_xc_type',null,null));
        var filters = new Array();
        filters.push(new nlobjSearchFilter("custrecord_cologix_xc_service",null,"anyof",service));
        var search = nlapiSearchRecord('customrecord_cologix_crossconnect', null, filters, columns);
        for ( var i = 0; search != null && i < search.length; i++ ) {

            var intid = parseInt(search[i].getValue('internalid',null,null)) || 0;
            var name = search[i].getValue('name',null,null) || '';
            var typeid = parseInt(search[i].getValue('custrecord_cologix_xc_type',null,null)) || 0;

            if(typeid == 19 || typeid == 23 || typeid == 24){
                var type = 'active';
                var faicon = 'fa fa-share-alt-square';
            }
            else if(typeid == 28 || typeid == 29 || typeid == 30 || typeid == 31){
                var type = 'cloud';
                var faicon = 'fa fa-cloud-upload';
            }
            else if(typeid == 20 || typeid == 21 || typeid == 22){
                var type = 'metro';
                var faicon = 'fa fa-share-alt';
            }
            else{
                var type = 'xc';
                var faicon = 'fa fa-share-alt-square';
            }
            var child = {
                //"id": intid,
                "typeid": typeid,
                "type": type,
                "rid": intid.toString(radix),
                "node_en": name,
                "node_fr": name,
                "faicon": faicon,
                "leaf": true
            };
            children.push(child);
        }
    }
    return children;
}