nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_XC.js
//	ScriptID:	customscript_clgx_rl_cp_xc
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=688&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	08/01/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    var type = datain['type'] || "xc";
    var rid = datain['rid'] || "0";
    var id = parseInt(rid, radix);

    if((type == 'xc' && srights.colocation > 0) || (type != 'xc' && srights.network > 0)){

        obj["xc"] = {"id":id,"rid":rid};
        obj = get_xc(obj,radix);

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are viewing xc.';

        nlapiLogExecution('DEBUG','debug', '| obj = ' + JSON.stringify( obj ) + ' | ');
    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_XC';
        obj.msg = 'No rights for xc.';
    }

    return obj;
});
var put = wrap(function put(datain,obj,srights,companyid,contactid,modifierid,radix,sid,id) {

    var rid = datain['rid'] || '0';
    var note = datain['note'] || '';
    var pName = datain['pName'] || '';
    var copy = datain['copy'] || 0;
    nlapiLogExecution('DEBUG','debug-update', rid);
    if(srights.colocation > 0){
        if(note!='') {
            nlapiSubmitField('customrecord_cologix_crossconnect', rid, 'custrecord_clgx_xc_note', note);
        }
        if(pName!=''){
            nlapiSubmitField('customrecord_cologix_crossconnect', rid, 'custrecord_clgx_port_id',pName);
        }
        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing xcs.';
        obj["resp"] =3;
        // nlapiLogExecution('DEBUG','debug', '| obj = ' + JSON.stringify( obj ) + ' | ');

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_Interconnection';
        obj.msg = 'No rights for Interconnection.';
    }

    return obj;
});

function get_xc(obj,radix){

    var rec = nlapiLoadRecord('customrecord_cologix_crossconnect', obj.xc.id);

    var soid = parseInt(rec.getFieldValue('custrecord_xconnect_service_order')) || 0;
    var name = (rec.getFieldText('custrecord_xconnect_service_order')).split("#");
    var so = (name[name.length-1]).trim() || '';

    obj.xc["number"] = rec.getFieldValue('name') || '';
    obj.xc["sorid"] = soid.toString(radix);
    obj.xc["so"] = so;
    obj.xc["facility"] = rec.getFieldText('custrecord_clgx_xc_facility') || '';
    obj.xc["type"] = rec.getFieldText('custrecord_cologix_xc_circuit_type') || '';
    obj.xc["carrier"] = rec.getFieldText('custrecord_cologix_carrier_name') || '';
    obj.xc["circuit"] = rec.getFieldValue('custrecord_cologix_xc_carriercirct') || '';
    obj.xc["note"] = rec.getFieldValue('custrecord_clgx_xc_note') || '';

    return obj;
}






