nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Cloud.js
//	ScriptID:	customscript_clgx_rl_cp_cloud
//	ScriptType:	RESTlet
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	08/28/2017
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    var type = datain['type'] || "xc";
    var rid = datain['rid'] || "0";
    var id = parseInt(rid, radix);
    nlapiLogExecution('DEBUG','debug', id);
    nlapiLogExecution('DEBUG','type', type);
    if(((type == 'xc' || type == 'vxc') && srights.colocation > 0) ){

        obj["xc"] = {"id":id,"rid":rid};
        if(type=='xc') {
            obj = get_xc(obj, radix);
            nlapiLogExecution('DEBUG','type', type);
        }
        else{
            obj = get_vxc(obj, radix);
        }

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are viewing xc.';

        nlapiLogExecution('DEBUG','debug', '| obj = ' + JSON.stringify( obj ) + ' | ');
    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_XC';
        obj.msg = 'No rights for xc.';
    }

    return obj;
});
var put = wrap(function put(datain,obj,srights,companyid,contactid,modifierid,radix,sid,id) {

    var rid = datain['rid'] || '0';
    var note = datain['note'] || '';
    var copy = datain['copy'] || 0;
    nlapiLogExecution('DEBUG','debug-update', rid);
    if(srights.colocation > 0){
        nlapiSubmitField('customrecord_cologix_crossconnect', rid, 'custrecord_clgx_xc_note',note);

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing xcs.';
        obj["resp"] =3;
        // nlapiLogExecution('DEBUG','debug', '| obj = ' + JSON.stringify( obj ) + ' | ');

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_Interconnection';
        obj.msg = 'No rights for Interconnection.';
    }

    return obj;
});

function get_xc(obj,radix){

    var rec = nlapiLoadRecord('customrecord_cologix_crossconnect', obj.xc.id);
    nlapiLogExecution('DEBUG','debug', 'get_xc');
    var soid = parseInt(rec.getFieldValue('custrecord_xconnect_service_order')) || 0;
    var name = (rec.getFieldText('custrecord_xconnect_service_order')).split("#");
    var so = (name[name.length-1]).trim() || '';

    obj.xc["number"] = rec.getFieldValue('name') || '';
    obj.xc["sorid"] = soid.toString(radix);
    obj.xc["so"] = so;
    obj.xc["facility"] = rec.getFieldText('custrecord_clgx_xc_facility') || '';
    obj.xc["type"] = rec.getFieldText('custrecord_clgx_xc_type') || '';
    obj.xc["pName"] = rec.getFieldValue('custrecord_clgx_port_id') || '';
    obj.xc["circuit"] = rec.getFieldText('custrecord_cologix_xc_circuit_type') || '';
    obj.xc["note"] = rec.getFieldValue('custrecord_clgx_xc_note') || '';
    return obj;
}
function get_vxc(obj,radix){

    var rec = nlapiLoadRecord('customrecord_cologix_vxc', obj.xc.id);

    var soid = parseInt(rec.getFieldValue('custrecord_cologix_service_order')) || 0;
    var name = (rec.getFieldText('custrecord_cologix_service_order')).split("#");
    var so = (name[name.length-1]).trim() || '';

    obj.xc["number"] = rec.getFieldValue('name') || '';
    obj.xc["sorid"] = soid.toString(radix);
    obj.xc["so"] = so;
    obj.xc["facility"] = rec.getFieldText('custrecord_cologix_vxc_facility') || '';
    obj.xc["provider"] = rec.getFieldText('custrecord_clgx_cloud_provider') || '';
    obj.xc["speed"] = rec.getFieldText('custrecord_clgx_vxc_speed') || '';
    obj.xc["skey"] = rec.getFieldValue('custrecord_clgx_vxc_skey') || '';
    obj.xc["mstag"] = rec.getFieldValue('custrecord_clgx_vxc_msft_stag') || '';
    obj.xc["aPort"] = rec.getFieldText('custrecord_clgx_cloud_connect_port') || '';
    obj.xc["vlan"] = rec.getFieldValue('custrecord_clgx_vxc_stag') || '';
    var xcID = rec.getFieldValue('custrecord_clgx_cloud_connect_port') || 0;
    if(xcID>0){
        obj.xc["pName"] = nlapiLookupField('customrecord_cologix_crossconnect', xcID, 'custrecord_clgx_port_id')||'';
    }
    return obj;
}







