nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_SS_CP_OrderXC.js
//	ScriptID:	customscript_clgx_ss_cp_orderxc
//	ScriptType:	Scheuled
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	10/03/2016
//------------------------------------------------------

function create_orders() {

    nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');

    //retrieve script parameters
    var languagePortal=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_lan11')||'en';
    var title=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_title1_1');
    var companyid=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_company_1');
    var facility=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_loc');
    var qty=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_qty');
    var requested=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_clgx_target_install');
    var item=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_item');
    var rate=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_rate');
    var ratenrc=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_ratenrc');

    var totalmrc=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_amount');
    var spaceid=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_spaceid');
    var contactid=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_contact');
    var afacility=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_afac');
    var tfacility=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_tfac');
    var metrotype=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_metroty');
    var conn_type=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_conn_ty');
    var speed=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_speed');
    var loa=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_loa');
    var billing=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_billing');
    var ratecard=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_ratecar');
    var other=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_other');
    var notes=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_notes');
    var po=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_po');
    var serverIP=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_server');
    var requestor=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_req');
    var boxfolder=nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_ss_cp_order_boxf');
    var uname=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_uname');
    var timestamp=nlapiGetContext().getSetting('SCRIPT','custscript_ss_cp_order_timestamp');

    nlapiLogExecution('DEBUG','tfacility',tfacility);
    nlapiLogExecution('DEBUG','requestor',requestor);
    nlapiLogExecution('DEBUG','boxfolder',boxfolder);
    var arrCustomerDetails = nlapiLookupField('customer',companyid,['salesrep','companyname','custentity_clgx_cust_ccm']);
    var salesRepID = arrCustomerDetails['salesrep'];
    var custCompName = arrCustomerDetails['companyname'];
    var ccm = arrCustomerDetails['custentity_clgx_cust_ccm'];
    var fname=nlapiLookupField('contact', contactid, 'firstname') || '';
    var lname=nlapiLookupField('contact', contactid, 'lastname') || '';
    var emailContact=nlapiLookupField('contact', contactid, 'email') || '';
    var custContactName=fname+' '+lname;
    var contactRec=nlapiLoadRecord('contact',requestor);
    var nameAtt=contactRec.getFieldValue('entitytitle');
    var strMemoOrder =
        'Space ID : ' + spaceid + ';\n\n' +
        ' Terminating Facility : ' + tfacility + ';\n\n' +
        ' Metro Connect Type : ' + metrotype + ';\n\n' +
        ' Cross Connect Type : ' + conn_type + ';\n\n' +
        ' Speed : ' + speed + ';\n\n' +
        ' Notes : ' + notes;

    if(parseInt(salesRepID) > 0){
        var arrRepDetails = nlapiLookupField('employee',salesRepID,['entityid']);
        var salesRepName = arrRepDetails['entityid'];
    }
    else{
        var salesRepName = '';
    }
    if(other==1)
    {



        var strMemo = 'A request for a new order was received from:\n\n' +
            'Contact Name : ' + custContactName + '\n\n' +
            'Notes : ' + notes;
        // create a new opportunity
        var record = nlapiCreateRecord('opportunity');
        record.setFieldValue('entity', companyid);
        record.setFieldValue('title', 'Portal new order request');
        record.setFieldValue('location', facility);
        record.setFieldValue('leadsource', 73127);
        record.setFieldValue('memo', strMemo.substr(0,999));
        var idRec = nlapiSubmitRecord(record, false,true);

        var arrOpptyDetails = nlapiLookupField('opportunity',idRec,['tranid']);
        var location = nlapiLookupField('opportunity',idRec,'location',true);
        var opptyNbr = arrOpptyDetails['tranid'];

        var emailSubject = 'New Service Request '+opptyNbr+'-'+custCompName;
        var emailSubjectRep = 'New Order Request for ' + custCompName + ' (Asked by ' + custContactName + ')';
        var emailBody = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
            'Your request for a new order was received with the following info:\n\n' +
            'Reference Number : ' + opptyNbr + '\n' +
            'Data Center : ' + location + '\n' +
            'Notes : ' + notes + '\n\n' +
            'Thank you for your business.  I will be following up with you shortly to finalize the details of this request.\n\n' +
            'Sincerely,\n' +
            salesRepName + '\n\n';
        var emailBodyCustomer ='Thank you for your service request.  We are reviewing the details and will follow up with you shortly.';
        if(languagePortal=='fr')
        {
            var emailSubject = 'Nouvelle Demande de Service '+opptyNbr+'-'+custCompName;
            var emailBodyCustomer='Nous vous remercions de votre demande de service. Nous examinons présentement les détails et nous vous contacterons sous peu.';
        }
        
        nlapiLogExecution("DEBUG", "salesRepID", salesRepID);
        
        if (salesRepID != null && salesRepID != ''){
            nlapiSendEmail(salesRepID,salesRepID,emailSubjectRep,emailBody,null,null,null,null,true);
            nlapiSendEmail(salesRepID,contactid,emailSubject,emailBodyCustomer,null,null,null,null,true);
            //if (superviserID != null){
            //	nlapiSendEmail(salesRepID,superviserID,emailSubjectRep,emailBody,null,null,null,null);
            //}
        }
        obj["id"] = idRec;
        obj["number"] =  opptyNbr;
        obj["type"] =  "opportunity";
    }
    else {
        var taxcode1 = 0;
        var taxcode2 = 0;

        //MTL
        if (facility == 5 || facility == 8 || facility == 9 || facility == 10 || facility == 11 || facility == 12 || facility == 27) {
            var taxcode1 = 308;
            var taxcode2 = 308;
        } else if (facility == 6 || facility == 13 || facility == 15) { //TOR
            var taxcode1 = 11;
            var taxcode2 = 11;
        }
        else if (facility == 7 || facility == 28) { //VAN
            var taxcode1 = 297;
            var taxcode2 = 297;
        }
        else if (facility == 34 || facility == 39 || facility == 38) { //COL
            var taxcode2 = 515;
        }
        else if (facility == 31 || facility == 40) { //JAX
            var taxcode2 = 498;
        }
        else if (facility == 42) { //LAK
            var taxcode2 = 565;
        }
        else if (facility == 53 || facility == 54 || facility == 55|| facility==56) { //NNJ
            var taxcode2 = -635;
        }
        var date = new Date();
        nlapiLogExecution('DEBUG', 'title', title);
        nlapiLogExecution('DEBUG', 'facility', facility);
        nlapiLogExecution('DEBUG', 'totalmrc', totalmrc);
        nlapiLogExecution('DEBUG', 'billing', billing);


        // create a new opportunity



        if (ratecard != 0) {
            var record = nlapiCreateRecord('opportunity');
            record.setFieldValue('title', title);
            record.setFieldValue('entity', companyid);
            record.setFieldValue('memo', strMemoOrder.substr(0,999));
            record.setFieldValue('custbody_clgx_so_portal_req', requestor);
            record.setFieldValue('location', facility);
            record.setFieldValue('forecasttype', 3);
            record.setFieldValue('entitystatus', 11);
            // record.setFieldValue('memo', strMemo.substr(0,999));
            // record.setFieldValue('statusRef', 'inProgress');
            record.setFieldValue('custbody_cologix_opp_sale_type', 2);
            record.setFieldValue('leadsource', 73127);
            record.setFieldValue('custbody_clgx_target_install_date', requested);
            record.setLineItemValue('item', 'item', 1, item);
            record.setLineItemValue('item', 'quantity', 1, qty);
            record.setLineItemValue('item', 'rate', 1, 0);
            record.setLineItemValue('item', 'location', 1, facility);
            record.setLineItemValue('item', 'price', 1, -1);
            record.setLineItemValue('item', 'pricelevels', 1, -1);
            record.setLineItemValue('item', 'amount', 1, 0);
            if (taxcode1 != 0) {
                record.setLineItemValue('item', 'taxcode', 1, taxcode1);
            }
            //item 244
            record.setLineItemValue('item', 'item', 2, 244);
            record.setLineItemValue('item', 'quantity', 2, qty);
            record.setLineItemValue('item', 'rate', 2, 0);
            record.setLineItemValue('item', 'amount', 2, 0);
            record.setLineItemValue('item', 'location', 2, facility);
            record.setLineItemValue('item', 'price', 2, -1);
            record.setLineItemValue('item', 'pricelevels', 2, -1);
            if (taxcode2 !=0) {
                record.setLineItemValue('item', 'taxcode', 2, taxcode2);
            }
            try {
                var recordId = nlapiSubmitRecord(record, true, true);

            }
            catch (error) {
                nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
            }
            if (recordId != null) {
                var totals = clgx_transaction_totals(companyid, 'oppty', recordId);
                //create the proposal

                // var recProposal = nlapiTransformRecord(fromrecord, recordId, torecord);
                var termstitle=2;
                if(languagePortal=='fr')
                {
                    var termstitle=3;
                }
                var recProposal = nlapiCreateRecord('estimate');
                recProposal.setFieldValue('entity', companyid);
                recProposal.setFieldValue('memo', '');
                recProposal.setFieldValue('custbody_clgx_contract_terms_title', termstitle);
                recProposal.setFieldValue('otherrefnum', po);
                recProposal.setFieldValue('custbodyclgx_aa_type', 3);
                recProposal.setFieldValue('custbody_cologix_annual_accelerator', '3');
                var stTermsBody = nlapiLookupField('customrecord_clgx_contract_terms', termstitle, 'custrecord_clgx_contract_terms_body');
                recProposal.setFieldValue('custbody_clgx_contract_terms_body', stTermsBody);
                recProposal.setFieldValue('custbody_clgx_order_notes', strMemoOrder.substr(0,999));
                recProposal.setFieldValue('opportunity', recordId);
                recProposal.setFieldValue('entitystatus',11);
                recProposal.setFieldValue('title', title);
                recProposal.setFieldValue('location', facility);
                recProposal.setFieldValue('custbody_clgx_contract_terms_contact', contactid);
                //  recProposal.setFieldValue('forecasttype', 3);
                // recProposal.setFieldValue('entitystatus', 11);
                recProposal.setFieldValue('billingaddress_key', billing);
                // record.setFieldValue('statusRef', 'inProgress');
                recProposal.setFieldValue('custbody_cologix_opp_sale_type', 2);
                recProposal.setFieldValue('leadsource', 73127);
                recProposal.setFieldValue('custbody_clgx_target_install_date', requested);

                //add the items
                recProposal.setLineItemValue('item', 'item', 1, item);
                recProposal.setLineItemValue('item', 'quantity', 1, qty);
                recProposal.setLineItemValue('item', 'rate', 1, 0);
                recProposal.setLineItemValue('item', 'location', 1, facility);
                recProposal.setLineItemValue('item', 'price', 1, -1);
                recProposal.setLineItemValue('item', 'pricelevels', 1, -1);
                recProposal.setLineItemValue('item', 'amount', 1, 0);
                recProposal.setLineItemValue('item', 'billingschedule', 1, 3);
                if (taxcode1 !=0) {
                    recProposal.setLineItemValue('item', 'taxcode', 1, taxcode1);
                }
                recProposal.setLineItemValue('item', 'item', 2, 244);
                recProposal.setLineItemValue('item', 'quantity', 2, qty);
                recProposal.setLineItemValue('item', 'rate', 2, 0);
                recProposal.setLineItemValue('item', 'amount', 2, 0);
                recProposal.setLineItemValue('item', 'location', 2, facility);
                recProposal.setLineItemValue('item', 'price', 2, -1);
                recProposal.setLineItemValue('item', 'pricelevels', 2, -1);
                recProposal.setLineItemValue('item', 'billingschedule', 2, 17);
                if (taxcode2 !=0) {
                    recProposal.setLineItemValue('item', 'taxcode', 2, taxcode2);
                }
                try {
                    var recordIdPR = nlapiSubmitRecord(recProposal, true, true);
                    var proposalNumber= nlapiLookupField('estimate',recordIdPR,'tranid');
                    var poSubject=' ';
                    if(po!='' && po!=null){
                        poSubject=' PO# '+po;
                    }
                    if(languagePortal=='en')
                    {

                        var emailSubjectProposal="New Order "+proposalNumber+" - "+custCompName+poSubject;
                        var agree='The initial term of this Service Order shall be one (1) month and shall automatically renew for successive one (1) month terms, unless terminated by either party, in writing, at least one (1) month prior to the end of the then current term. Your Cologix rate card rates apply for the ordered services.\n\n Notwithstanding anything to the contrary in the agreement between the parties, during the Term, all service fees identified above shall increase on each 12-month anniversary of the Service Commencement Date in an amount equal to the greater of three percent (3%) or CPI. Such increase shall be automatically invoiced to Customer upon each 12-month anniversary of the Service Commencement Date.\n\nEach of Customer and Cologix hereby consents to the execution of this Service Order by electronic means.\n';
                        var emailBodyProposal = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
                            'Thank you for submitting your order. A Cologix employee will be contacting you soon regarding the installation of your services. Please allow up to 24 hours for this order to be visible in the Portal.\n\n' +
                            'New Order : ' + proposalNumber + '\n' +
                            'Title : ' + title + '\n' +
                            'Portal Username : ' + uname + '\n' +
                            'Server Date and Time : ' + timestamp + '\n' +
                            'Server Name : ' + serverIP + '\n' +
                            'Contractual Terms Agreed To: '+agree+'\n'+
                            'Sincerely,\n' +
                            salesRepName + '\n\n';
                    }
                    else if(languagePortal=='fr')
                    {
                        var emailSubjectProposal="Nouvelle Commande "+proposalNumber+" - "+custCompName+poSubject;
                        var agree='La durée initiale de cette ordonnance de service est de un (1) mois et se renouvelle automatiquement pour les termes successifs d’un (1) mois, à moins d’être résilié par l’un ou l’autre des parties, par écrit, au moins un (1) mois avant la fin du Terme actuel. Vos tarifs de carte tarifaire Cologix s’appliquent pour les services commandés.\n\nThe initial term of this Service Order shall be one (1) month and shall automatically renew for successive one (1) month terms, unless terminated by either party, in writing, at least one (1) month prior to the end of the then current term. Your Cologix rate card rates apply for the ordered services. La durée initiale de cette ordonnance de service est de un (1) mois et se renouvelle automatiquement pour les termes successifs d’un (1) mois, à moins d’être résilié par l’un ou l’autre des parties, par écrit, au moins un (1) mois avant la fin du Terme actuel. Vos tarifs de carte tarifaire Cologix s’appliquent pour les services commandés. Notwithstanding anything to the contrary in the agreement between the parties, during the Term, all service fees identified above shall increase on each 12-month anniversary of the Service Commencement Date in an amount equal to the greater of three percent (3%) or CPI. Such increase shall be automatically invoiced to Customer upon each 12-month anniversary of the Service Commencement Date. Malgré toute disposition contraire dans le contrat conclu entre les parties, pendant la durée, tous les frais de service précisés plus haut augmenteront automatiquement tous les douze (12) mois à compter de la date anniversaire de la date de début du service, en fonction d’un taux égal à 3 % ou à l’indice des prix à la consommation, selon le plus élevé de ces deux taux.\n\nChacune des parties, le Client et Cologix, acceptent par les présentes que la signature et le passage de cette Commande de Service soient fait par moyen électronique.\n';
                        var emailBodyProposal = 'Bonjour ' + custContactName + ' (' + custCompName + '),\n\n' +
                            "Nous vous remercions d'avoir envoyé votre commande. Un employé de Cologix vous contactera rapidement concernant l'installation de vos services. Veuillez laisser jusqu'à 24 heures pour que cette commande soit visible dans le portail.\n\n" +
                            'Nouvelle Order : ' + proposalNumber + '\n' +
                            'Titre : ' + title + '\n' +
                            'Nom d’utilisateur du portail : ' + uname + '\n' +
                            'Date et heure du serveur : ' + timestamp + '\n' +
                            'Nom du serveur : ' + serverIP + '\n' +
                            'Termes contractuels convenue par: '+agree+'\n'+
                            'Au plaisir de vous être utile,\n' +
                            salesRepName + '\n\n';
                    }
                    nlapiSendEmail(salesRepID,contactid,emailSubjectProposal,emailBodyProposal,null,null,null,null,true);
                    if(ccm!=null) {
                        nlapiSendEmail(salesRepID, ccm, emailSubjectProposal, emailBodyProposal, null, null, null, null, true);
                    }
                }
                catch (error) {
                    nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
                }
                var recordQ = nlapiCreateRecord('customrecord_clgx_cp_q_transactions');
                recordQ.setFieldValue('custrecord_clgx_cp_tq_oppid', recordId);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_cust', companyid);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_contact', contactid);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_conemail', emailContact);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_loa', loa);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_ratecard', ratecard);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_folderid', boxfolder);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_portalserver', serverIP);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_trpr', 'T');
                if (recordIdPR != null) {
                    var tranid = nlapiLookupField('estimate',recordIdPR,'tranid');
                    recordQ.setFieldValue('custrecord_clgx_cp_tq_propid', tranid);
                    recordQ.setFieldValue('custrecord_clgx_cp_tq_prid', recordIdPR);
                    var recordQID = nlapiSubmitRecord(recordQ, true, true);
                    var serverIP = nlapiLookupField('customrecord_clgx_cp_q_transactions', recordQID,'custrecord_clgx_cp_tq_portalserver');
                    //  var requestURL = nlapiRequestURL('https://my.cologix.com/portal/echosign/upload/?id=' + recordQID);
                    var totals = clgx_transaction_totals(companyid, 'proposal', recordIdPR);
                }
            }
        }
        else {
            var record = nlapiCreateRecord('opportunity');
            record.setFieldValue('title', title);
            record.setFieldValue('entity', companyid);
            record.setFieldValue('location', facility);
            record.setFieldValue('memo', strMemoOrder.substr(0,999));
            record.setFieldValue('custbody_clgx_so_portal_req', requestor);
            //record.setFieldValue('forecasttype', 3);
            //record.setFieldValue('entitystatus', 11);
            // record.setFieldValue('statusRef', 'inProgress');
            record.setFieldValue('custbody_cologix_opp_sale_type', 2);
            // record.setFieldValue('memo', strMemo.substr(0,999));
            record.setFieldValue('leadsource', 73127);
            record.setFieldValue('custbody_clgx_target_install_date', requested);
            record.setLineItemValue('item', 'item', 1, item);
            record.setLineItemValue('item', 'quantity', 1, qty);
            var itemRate=nlapiLookupField('item', item, 'custitem_clgx_item_install_rate') || 'no';
            var installRate=ratenrc;
            if(itemRate!='no')
            {
                installRate=itemRate;
            }
            record.setLineItemValue('item', 'rate', 1, rate);
            record.setLineItemValue('item', 'location', 1, facility);
            record.setLineItemValue('item', 'price', 1, 1);
            //record.setLineItemValue('item', 'pricelevels',1, -1);
            record.setLineItemValue('item', 'amount', 1, totalmrc);
            if (taxcode1 !=0) {
                record.setLineItemValue('item', 'taxcode', 1, taxcode1);
            }
            //item 244
            record.setLineItemValue('item', 'item', 2, 244);
            record.setLineItemValue('item', 'quantity', 2, qty);
            record.setLineItemValue('item', 'rate', 2, installRate);
            record.setLineItemValue('item', 'amount', 2, 0);
            record.setLineItemValue('item', 'amount', 2, parseInt(qty) * parseInt(installRate));
            record.setLineItemValue('item', 'location', 2, facility);
            record.setLineItemValue('item', 'price', 2, -1);
            record.setLineItemValue('item', 'pricelevels', 2, -1);
            if (taxcode2!=0) {
                record.setLineItemValue('item', 'taxcode', 2, taxcode2);
            }
            try {
                var recordId = nlapiSubmitRecord(record, true, true);
            }
            catch (error) {
                nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
            }
            if (recordId != null) {
                var totals = clgx_transaction_totals(companyid, 'oppty', recordId);
                //create the proposal

                var termstitle=2;
                if(languagePortal=='fr')
                {
                    var termstitle=3;
                }
                var recProposal = nlapiCreateRecord('estimate');
                recProposal.setFieldValue('entity', companyid);
                recProposal.setFieldValue('memo', '');
                recProposal.setFieldValue('custbody_clgx_contract_terms_title', termstitle);
                recProposal.setFieldValue('custbodyclgx_aa_type', 3);
                recProposal.setFieldValue('custbody_cologix_annual_accelerator', '3');
                recProposal.setFieldValue('otherrefnum', po);
                var stTermsBody = nlapiLookupField('customrecord_clgx_contract_terms', termstitle, 'custrecord_clgx_contract_terms_body');
                recProposal.setFieldValue('custbody_clgx_contract_terms_body', stTermsBody);
                recProposal.setFieldValue('custbody_clgx_order_notes', strMemoOrder.substr(0,999));
                recProposal.setFieldValue('opportunity', recordId);
                recProposal.setFieldValue('entitystatus',11);
                recProposal.setFieldValue('title', title);
                recProposal.setFieldValue('location', facility);
                recProposal.setFieldValue('custbody_clgx_contract_terms_attention', nameAtt);
                recProposal.setFieldValue('custbody_clgx_contract_terms_contact', contactid);
                //  recProposal.setFieldValue('forecasttype', 3);
                // recProposal.setFieldValue('entitystatus', 11);
                recProposal.setFieldValue('billingaddress_key', billing);
                // record.setFieldValue('statusRef', 'inProgress');
                recProposal.setFieldValue('custbody_cologix_opp_sale_type', 2);
                recProposal.setFieldValue('leadsource', 73127);
                recProposal.setFieldValue('custbody_clgx_target_install_date', requested);

                //add the items
                recProposal.setLineItemValue('item', 'item', 1, item);
                recProposal.setLineItemValue('item', 'quantity', 1, qty);
                var itemRate=nlapiLookupField('item', item, 'custitem_clgx_item_install_rate') || 'no';
                var installRate=ratenrc;
                if(itemRate!='no')
                {
                    installRate=itemRate;
                }
                recProposal.setLineItemValue('item', 'rate', 1, rate);
                recProposal.setLineItemValue('item', 'location', 1, facility);
                recProposal.setLineItemValue('item', 'price', 1, 1);
                //  recProposal.setLineItemValue('item', 'pricelevels',1, -1);
                recProposal.setLineItemValue('item', 'amount', 1, totalmrc);
                recProposal.setLineItemValue('item', 'billingschedule', 1, 3);
                if (taxcode1!=0) {
                    recProposal.setLineItemValue('item', 'taxcode', 1, taxcode1);
                }
                //item 244
                recProposal.setLineItemValue('item', 'item', 2, 244);
                recProposal.setLineItemValue('item', 'quantity', 2, qty);
                recProposal.setLineItemValue('item', 'rate', 2, installRate);
                //recProposal.setLineItemValue('item', 'amount',2, 0);
                recProposal.setLineItemValue('item', 'amount', 2, parseInt(qty) * parseInt(installRate));
                recProposal.setLineItemValue('item', 'location', 2, facility);
                recProposal.setLineItemValue('item', 'price', 2, -1);
                recProposal.setLineItemValue('item', 'pricelevels', 2, -1);
                recProposal.setLineItemValue('item', 'billingschedule', 2, 17);
                if (taxcode2 !=0) {
                    recProposal.setLineItemValue('item', 'taxcode', 2, taxcode2);
                }
                try {
                    var poSubject=' ';
                    if(po!='' && po!=null){
                        poSubject=' PO# '+po;
                    }
                    var recordIdPR = nlapiSubmitRecord(recProposal, true, true);
                    var tranid = nlapiLookupField('estimate',recordIdPR,'tranid');
                    var emailSubjectProposal="New Order SO"+tranid+" - "+title+poSubject;
                    var agree='The initial term of this Service Order shall be one (1) month and shall automatically renew for successive one (1) month terms, unless terminated by either party, in writing, at least one (1) month prior to the end of the then current term. The Cologix standard rates apply for the ordered services.\n\nNotwithstanding anything to the contrary in the agreement between the parties, during the Term, all service fees identified above shall increase on each 12-month anniversary of the Service Commencement Date in an amount equal to the greater of three percent (3%) or CPI. Such increase shall be automatically invoiced to Customer upon each 12-month anniversary of the Service Commencement Date.\n\nEach of Customer and Cologix hereby consents to the execution of this Service Order by electronic means.\n'
                    var emailBodyProposal = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
                        'Thank you for submitting your order.  A Cologix employee will be contacting you soon regarding the installation of your services.\n\n' +
                        'New Order : SO' + tranid + '\n' +
                        'Title : ' + title + '\n' +
                        'Portal Username : ' + uname + '\n' +
                        'Server Date and Time : ' + timestamp + '\n' +
                        'Server Name : ' + serverIP + '\n\n' +
                        'Contractual Terms Agreed To: '+agree+'\n'+
                        'Sincerely,\n' +
                        salesRepName + '\n\n';
                    if(languagePortal=='fr')
                    {
                        var emailSubjectProposal="Nouvelle Commande SO"+tranid+" - "+title+poSubject;
                        var agree='La durée initiale de cette ordonnance de service est de un (1) mois et se renouvelle automatiquement pour les termes successifs d’un (1) mois, à moins d’être résilié par l’un ou l’autre des parties, par écrit, au moins un (1) mois avant la fin du Terme actuel. Les tarifs standard de Cologix s’appliquent pour les services commandés.\n\nMalgré toute disposition contraire dans le contrat conclu entre les parties, pendant la durée, tous les frais de service précisés plus haut augmenteront automatiquement tous les douze (12) mois à compter de la date anniversaire de la date de début du service, en fonction d’un taux égal à 3 % ou à l’indice des prix à la consommation, selon le plus élevé de ces deux taux.\n\nChacune des parties, le Client et Cologix, acceptent par les présentes que la signature et le passage de cette Commande de Service soient fait par moyen électronique.\n'
                        var emailBodyProposal = 'Bonjour ' + custContactName + ' (' + custCompName + '),\n\n' +
                            "Nous vous remercions d'avoir soumis votre commande. Un employé Cologix vous contactera bientôt concernant l'installation de vos services.\n\n" +
                            'Nouvelle Commande :  SO'+tranid + '\n' +
                            'Titre : ' + title + '\n' +
                            'Nom d’utilisateur du portail : ' + uname + '\n' +
                            'Date et heure du serveur : ' + timestamp + '\n' +
                            'Nom du serveur : ' + serverIP + '\n' +
                            'Termes contractuels convenue par: '+agree+'\n'+
                            'Au plaisir de vous être utile,\n' +
                            salesRepName + '\n\n';
                    }
                    nlapiSendEmail(salesRepID,contactid,emailSubjectProposal,emailBodyProposal,null,null,null,null,true);

                }
                catch (error) {
                    nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
                }

                var recordQ = nlapiCreateRecord('customrecord_clgx_cp_q_transactions');
                recordQ.setFieldValue('custrecord_clgx_cp_tq_oppid', recordId);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_cust', companyid);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_contact', contactid);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_conemail', emailContact);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_loa', loa);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_ratecard', ratecard);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_folderid', boxfolder);
                recordQ.setFieldValue('custrecord_clgx_cp_tq_trpr', 'T');
                recordQ.setFieldValue('custrecord_clgx_cp_tq_portalserver', serverIP);
                if (recordIdPR != null) {
                    var tranid = nlapiLookupField('estimate',recordIdPR,'tranid');
                    recordQ.setFieldValue('custrecord_clgx_cp_tq_propid', tranid);
                    recordQ.setFieldValue('custrecord_clgx_cp_tq_prid', recordIdPR);
                    var recordQID = nlapiSubmitRecord(recordQ, true, true);
                    var totals = clgx_transaction_totals(companyid, 'proposal', recordIdPR);
                    var recordQID = nlapiSubmitRecord(recordQ, true, true);
                    var arrParam = new Array();
                    arrParam['custscript_ss_cp_order_id'] = recordIdPR;
                    arrParam['custscript_ss_cp_order_lan'] = languagePortal;
                    arrParam['custscript_ss_cp_order_title'] = title;
                    arrParam['custscript_ss_cp_order_company']=companyid;
                    arrParam['custscript_ss_cp_order_contact']=contactid;
                    arrParam['custscript_ss_cp_order_trq']=recordQID;
                    nlapiLogExecution('DEBUG', 'SS', recordIdPR+';'+languagePortal+';'+';'+title+';'+companyid+';'+contactid+';'+recordQID);
                    var status = nlapiScheduleScript('customscript_clgx_ss_cp_order', null, arrParam);
                    //   var requestURL = nlapiRequestURL('https://my.cologix.com/portal/echosign/upload/?id=' + recordQID);

                }
            }
        }
    }
}