nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Powers.js
//	ScriptID:	customscript_clgx_rl_cp_powers
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=659&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/17/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv) {

    //if(srights.powers > 0){
    if(srights.colocation > 0){

        var direction = datain['direction'] || 'ASC';
        var order_by = datain['order_by'] || '';
        var page = datain['page'] || 1;
        var per_page = datain['per_page'];
        var search = datain['search'] || '{}';
        var csv = datain['csv'] || 0;

        obj["powers"] = get_powers(companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv);

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing powers.';

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_POWERS';
        obj.msg = 'No rights for powers.';
    }

    return obj;

});

function get_powers(companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv){

    var obj = {};
    var arr = [];
    var total = 0;
    var pages = 0;
    var start = 0;
    var end = 0;
    var has_more = false;

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",companyid));
    var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders', filters, columns);
    var ids = new Array();
    for ( var i = 0; records != null && i < records.length; i++ ) {
        ids.push(parseInt(records[i].getValue('internalid',null,'GROUP')));
    }

    var facilities = [{"value": 0, "text": '*'}];
    var facilitiesids = [];

    var sos = [{"value": 0, "text": '*'}];
    var sosids = [];

    var spaces = [{"value": 0, "text": '*'}];
    var spacesids = [];

    var panels = [{"value": 0, "text": '*'}];
    var panelsids = [];

    var volts = [{"value": 0, "text": '*'}];
    var voltsids = [];

    var amps = [{"value": 0, "text": '*'}];
    var ampsids = [];

    search = JSON.parse(search);

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_power_circuit_service_order",null,"anyof",ids));
    var records = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_cp_powers', filters, columns);
    if(records){

        for ( var i = 0; records != null && i < records.length; i++ ) {

            var ampid = parseInt(records[i].getValue('custrecord_cologix_power_amps',null,null)) || 0;
            var amp = records[i].getText('custrecord_cologix_power_amps',null,null) || '';
            if(_.indexOf(ampsids, ampid) == -1 && ampid){
                ampsids.push(ampid);
                amps.push({"value": ampid, "text": amp});
            }
            amps = _.sortBy(amps, function(obj){ return obj.value;});

            var voltid = parseInt(records[i].getValue('custrecord_cologix_power_volts',null,null)) || 0;
            var volt = records[i].getText('custrecord_cologix_power_volts',null,null) || '';
            if(_.indexOf(voltsids, voltid) == -1 && voltid){
                voltsids.push(voltid);
                volts.push({"value": voltid, "text": volt});
            }
            volts = _.sortBy(volts, function(obj){ return obj.value;});

            var soid = parseInt(records[i].getValue('custrecord_power_circuit_service_order',null,null)) || 0;
            var so = (records[i].getText('custrecord_power_circuit_service_order',null,null)).split("#") || '';
            if(_.indexOf(sosids, soid) == -1 && soid){
                sosids.push(soid);
                sos.push({"value": soid, "text": so[1]});
            }
            sos = _.sortBy(sos, function(obj){ return obj.value;});

            var spaceid = parseInt(records[i].getValue('custrecord_cologix_power_space',null,null)) || 0;
            var space = records[i].getText('custrecord_cologix_power_space',null,null) || '';
            if(_.indexOf(spacesids, spaceid) == -1 && spaceid){
                spacesids.push(spaceid);
                spaces.push({"value": spaceid, "text": space});
            }
            spaces = _.sortBy(spaces, function(obj){ return obj.value;});

            var panelid = parseInt(records[i].getValue('custrecord_clgx_dcim_device',null,null)) || 0;
            var panel = records[i].getText('custrecord_clgx_dcim_device',null,null) || '';
            if(_.indexOf(panelsids, panelid) == -1 && panelid){
                panelsids.push(panelid);
                panels.push({"value": panelid, "text": panel});
            }
            panels = _.sortBy(panels, function(obj){ return obj.value;});

            var facilityid = parseInt(records[i].getValue('custrecord_cologix_power_facility',null,null)) || 0;
            var facility = records[i].getText('custrecord_cologix_power_facility',null,null) || '';
            if(_.indexOf(facilitiesids, facilityid) == -1 && facilityid){
                facilitiesids.push(facilityid);
                facilities.push({"value": facilityid, "text": facility});
            }
            facilities = _.sortBy(facilities, function(obj){ return obj.value;});
        }
    }

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_power_circuit_service_order",null,"anyof",ids));
    if(search){
        if(search.name){
            filters.push(new nlobjSearchFilter("name",null,"contains",search.name));
        }
        if(search.sos){
            filters.push(new nlobjSearchFilter("custrecord_power_circuit_service_order",null,"anyof",search.sos));
        }
        if(search.spaces){
            filters.push(new nlobjSearchFilter("custrecord_cologix_power_space",null,"anyof",search.spaces));
        }
        if(search.panels){
            filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_device",null,"anyof",search.panels));
        }
        if(search.volts){
            filters.push(new nlobjSearchFilter("custrecord_cologix_power_volts",null,"anyof",search.volts));
        }
        if(search.amps){
            filters.push(new nlobjSearchFilter("custrecord_cologix_power_amps",null,"anyof",search.amps));
        }
        if(search.facilities){
            filters.push(new nlobjSearchFilter("custrecord_cologix_power_facility",null,"anyof",search.facilities));
        }
    }
    var records = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_cp_powers', filters, columns);
    if(records){
        total = records.length;
        pages = Math.ceil(total / per_page);
        page = (parseInt(page) > pages ? pages : parseInt(page));
        has_more = (pages > 1 ? true : false);
        start = (page - 1) * per_page;
        end = (start + per_page > total ? total : start + per_page);

        for ( var i = start; i < end; i++ ) {

            var rec = new Object();

            var powerid = parseInt(records[i].getValue('internalid',null,null));
            rec["id"] = powerid;
            rec["rid"] = powerid.toString(radix);
            rec["number"] = records[i].getValue('name',null,null);
            rec["module"] = parseInt(records[i].getText('custrecord_cologix_power_upsbreaker',null,null)) || 0;
            rec["breaker"] = parseInt(records[i].getText('custrecord_cologix_power_circuitbreaker',null,null)) || 0;

            var soid = parseInt(records[i].getValue('custrecord_power_circuit_service_order',null,null)) || 0;
            rec["soid"] = soid;
            rec["rsoid"] = soid.toString(radix);
            var name = (records[i].getText('custrecord_power_circuit_service_order',null,null)).split("#");
            rec["so"] = (name[name.length-1]).trim();

            rec["voltsid"] = records[i].getValue('custrecord_cologix_power_volts',null,null) || '';
            rec["volts"] = records[i].getText('custrecord_cologix_power_volts',null,null) || '';

            rec["ampsid"] = records[i].getValue('custrecord_cologix_power_amps',null,null) || '';
            rec["amps"] = records[i].getText('custrecord_cologix_power_amps',null,null) || '';

            var strAmps = (records[i].getText('custrecord_cologix_power_amps',null,null)).slice(0, -1) || '0';
            var intAmps = parseInt(strAmps);
            rec["camps"] = intAmps;

            rec["facilityid"] = parseInt(records[i].getValue('custrecord_cologix_power_facility',null,null)) || 0;
            rec["facility"] = records[i].getText('custrecord_cologix_power_facility',null,null) || '';

            var spaceid =  parseInt(records[i].getValue('custrecord_cologix_power_space',null,null)) || 0;
            rec["spaceid"] = spaceid;
            rec["rspaceid"] = spaceid.toString(radix);
            rec["space"] = records[i].getText('custrecord_cologix_power_space',null,null) || '';

            var panelid =  parseInt(records[i].getValue('custrecord_clgx_dcim_device',null,null)) || 0;
            rec["panelid"] = panelid;
            rec["rpanelid"] = panelid.toString(radix);
            rec["panel"] = records[i].getText('custrecord_clgx_dcim_device',null,null) || '';

            rec["typeid"] = parseInt(records[i].getValue('custrecord_clgx_panel_type','CUSTRECORD_CLGX_POWER_PANEL_PDPM',null)) || 0;
            rec["type"] = records[i].getText('custrecord_clgx_panel_type','CUSTRECORD_CLGX_POWER_PANEL_PDPM',null) || '';

            var phasea = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_a_a',null,null)) || 0;
            var phaseb = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_a_b',null,null)) || 0;
            var phasec = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_a_c',null,null)) || 0;
            rec["phasea"] = phasea;
            rec["phaseb"] = phaseb;
            rec["phasec"] = phasec;

            var phasea2 = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_ab_a',null,null)) || 0;
            var phaseb2 = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_ab_b',null,null)) || 0;
            var phasec2 = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_ab_c',null,null)) || 0;
            var maxamps = Math.max(phasea2,phaseb2,phasec2);
            rec["phasea2"] = phasea2;
            rec["phaseb2"] = phaseb2;
            rec["phasec2"] = phasec2;
            rec["maxamps"] = maxamps;

          //  rec["utilization"] = maxamps * 100 / intAmps;
            rec["utilization"] = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_ab_usg',null,null)) || 0;

            var kwa = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_kw_a',null,null)) || 0;
            var kwab = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_kw_ab',null,null)) || 0;
            rec["kwa"] = kwa;
            rec["kwab"] = kwab;

            arr.push(rec);
        }
    }

    obj["data"] = arr;
    if(csv){
        obj["csv"] = get_powers_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv);
    }
    obj["lists"] = {"sos": sos, "volts": volts, "amps": amps, "spaces": spaces, "panels": panels, "facilities": facilities};
    obj["search"] = search;
    obj["direction"] = direction;
    obj["order_by"] = order_by;
    obj["page"] = page;
    obj["pages"] = pages;
    obj["per_page"] = per_page;
    obj["has_more"] = has_more;
    obj["start"] = start;
    obj["end"] = end;
    obj["total"] = total;

    return obj;
}


function get_powers_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv){

    var text = 'ID,Service Order,Facility,Space,Branch Circuit,Voltage,Amperage,Utilization (%),Average kW,Outlet Label,Phase A Amps,Phase B Amps,Phase C Amps\n';

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",companyid));
    var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders', filters, columns);
    var ids = new Array();
    for ( var i = 0; records != null && i < records.length; i++ ) {
        ids.push(parseInt(records[i].getValue('internalid',null,'GROUP')));
    }

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_power_circuit_service_order",null,"anyof",ids));
    if(search){
        if(search.name){
            filters.push(new nlobjSearchFilter("name",null,"contains",search.name));
        }
        if(search.sos){
            filters.push(new nlobjSearchFilter("custrecord_power_circuit_service_order",null,"anyof",search.sos));
        }
        if(search.spaces){
            filters.push(new nlobjSearchFilter("custrecord_cologix_power_space",null,"anyof",search.spaces));
        }
        if(search.panels){
            filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_device",null,"anyof",search.panels));
        }
        if(search.volts){
            filters.push(new nlobjSearchFilter("custrecord_cologix_power_volts",null,"anyof",search.volts));
        }
        if(search.amps){
            filters.push(new nlobjSearchFilter("custrecord_cologix_power_amps",null,"anyof",search.amps));
        }
        if(search.facilities){
            filters.push(new nlobjSearchFilter("custrecord_cologix_power_facility",null,"anyof",search.facilities));
        }
    }
    var records = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_cp_powers', filters, columns);

    for ( var i = 0; records != null && i < records.length; i++ ) {

        text += records[i].getValue('name',null,null) + ',';
        var name = (records[i].getText('custrecord_power_circuit_service_order',null,null)).split("#");
        text += (name[name.length-1]).trim() + ',';
        text += records[i].getText('custrecord_cologix_power_facility',null,null) + ',';
        text += records[i].getText('custrecord_cologix_power_space',null,null) + ',';
        text += records[i].getText('custrecord_clgx_dcim_device',null,null) + ',';
        text += records[i].getText('custrecord_cologix_power_volts',null,null) + ',';
        text += records[i].getText('custrecord_cologix_power_amps',null,null) + ',';
        var phasea2 = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_ab_a',null,null)) || 0;
        var phaseb2 = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_ab_b',null,null)) || 0;
        var phasec2 = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_ab_c',null,null)) || 0;
        var utilization=parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_ab_usg',null,null)) || 0;
        var maxamps = Math.max(phasea2,phaseb2,phasec2);
        var strAmps = (records[i].getText('custrecord_cologix_power_amps',null,null)).slice(0, -1) || '0';
        var intAmps = parseFloat(strAmps);
       // text +=(parseFloat(maxamps * 100 )/ parseFloat(intAmps)).toFixed(2)+ '%,';
        text +=parseFloat(utilization).toFixed(2)+ '%,';
        var kwa = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_kw_a',null,null)) || 0;
        text +=(kwa).toFixed(2)+ ',';
        text += records[i].getText('custrecord_cologix_power_circuitbreaker',null,null) + ',';
        text += parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_a_a',null,null)) + ',';
        text += parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_a_b',null,null)) + ',';
        text += parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_a_c',null,null)) ;
        text += '\n';
    }
    return text;
}