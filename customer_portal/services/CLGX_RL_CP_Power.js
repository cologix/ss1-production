nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Power.js
//	ScriptID:	customscript_clgx_rl_cp_power
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=690&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	08/01/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

	var type = datain['type'] || "xc";
	var rid = datain['rid'] || "0";
	var id = parseInt(rid, radix);
		
	if(srights.colocation > 0){
		
		obj["power"] = {"id":id,"rid":rid};
		obj = get_power(obj,radix);
		
		var now = moment().format("M/D/YYYY h:mm:ss a");
		nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);
		
		obj.code = 'SUCCESS';
		obj.msg = 'You are viewing power.';
		
	} else {
		obj.error = 'T';
		obj.code = 'NO_RIGHTS_SPACE';
		obj.msg = 'No rights for power.';
	}
	
	return obj;
	
});

function get_power(obj,radix){
	
	var rec = nlapiLoadRecord('customrecord_clgx_power_circuit', obj.power.id);
	
	var soid = parseInt(rec.getFieldValue('custrecord_power_circuit_service_order') ) || 0;
	var so = (rec.getFieldText('custrecord_power_circuit_service_order') ).split("#");
	var strAmps = (rec.getFieldText('custrecord_cologix_power_amps')).slice(0, -1) || '0';
	var intAmps = parseInt(strAmps);
	var spaceid =  parseInt(rec.getFieldValue('custrecord_cologix_power_space')) || 0;
	var deviceid =  parseInt(rec.getFieldValue('custrecord_clgx_dcim_device')) || 0;
	var device =  rec.getFieldText('custrecord_clgx_dcim_device') || '';
	var panelid =  parseInt(rec.getFieldValue('custrecord_clgx_power_panel_pdpm')) || 0;
	
	if(panelid){
		var panel = nlapiLoadRecord('customrecord_ncfar_asset', panelid);
		var typeid = parseInt(panel.getFieldValue('custrecord_clgx_panel_type')) || 0;
		var type = panel.getFieldText('custrecord_clgx_panel_type') || '';
	} else {
		var typeid = 0;
		var type = '';
	}
	
	var phasea = parseFloat(rec.getFieldValue('custrecord_clgx_dcim_sum_day_amp_a_a')) || 0;
	var phaseb = parseFloat(rec.getFieldValue('custrecord_clgx_dcim_sum_day_amp_a_b')) || 0;
	var phasec = parseFloat(rec.getFieldValue('custrecord_clgx_dcim_sum_day_amp_a_c')) || 0;
	
	var kwa = parseFloat(rec.getFieldValue('custrecord_clgx_dcim_sum_day_kw_a')) || 0;
	var kwab = parseFloat(rec.getFieldValue('custrecord_clgx_dcim_sum_day_kw_ab')) || 0;
	
	obj.power["primary"] = {
		"id": obj.power.id,
		"rid": obj.power.rid,
		"number": rec.getFieldValue('name') || '',
		"module": parseInt(rec.getFieldText('custrecord_cologix_power_upsbreaker')) || 0,
		"lines": rec.getFieldText('custrecord_cologix_power_subpanel') || '',
		"breaker": parseInt(rec.getFieldText('custrecord_cologix_power_circuitbreaker')) || 0,
		"soid": soid,
		"rsoid": soid.toString(radix),
		"so": so[1],
		"volts": rec.getFieldText('custrecord_cologix_power_volts') || '',
		"amps": rec.getFieldText('custrecord_cologix_power_amps') || '',
		"camps": intAmps,
		"facility": rec.getFieldText('custrecord_cologix_power_facility') || '',
		"spaceid": spaceid,
		"rspaceid": spaceid.toString(radix),
		"space": rec.getFieldText('custrecord_cologix_power_space') || '',
		"panelid": deviceid,
		"rpanelid": deviceid.toString(radix),
		"panel": device,
		"typeid": typeid,
		"type": type,
		"box": rec.getFieldText('custrecord_clgx_outlet_box_serial_no_pwr') || '',
		"bus": rec.getFieldText('custrecord_clgx_busway_pwr') || '',
		"phasea": phasea,
		"phaseb": phaseb,
		"phasec": phasec,
		"kwa": kwa
	};
	
	var phasea = parseFloat(rec.getFieldValue('custrecord_clgx_dcim_sum_day_amp_a_a')) || 0;
	var phaseb = parseFloat(rec.getFieldValue('custrecord_clgx_dcim_sum_day_amp_a_b')) || 0;
	var phasec = parseFloat(rec.getFieldValue('custrecord_clgx_dcim_sum_day_amp_a_c')) || 0;
	var maxamps = Math.max(phasea,phaseb,phasec);
	
	obj.power["total"] = {
		"phasea": phasea,
		"phaseb": phaseb,
		"phasec": phasec,
		"maxamps": maxamps,
		"utilization": maxamps * 100 / intAmps,
		"kwab": kwab
	};
	
	var pairid = parseInt(rec.getFieldValue('custrecord_clgx_dcim_pair_power') ) || 0;
	
	if(pairid){
		
		var rec = nlapiLoadRecord('customrecord_clgx_power_circuit', pairid);
		
		var soid = parseInt(rec.getFieldValue('custrecord_power_circuit_service_order') ) || 0;
		var so = (rec.getFieldText('custrecord_power_circuit_service_order') ).split("#");
		var strAmps = (rec.getFieldText('custrecord_cologix_power_amps')).slice(0, -1) || '0';
		var intAmps = parseInt(strAmps);
		var spaceid =  parseInt(rec.getFieldValue('custrecord_cologix_power_space')) || 0;
		var deviceid =  parseInt(rec.getFieldValue('custrecord_clgx_dcim_device')) || 0;
		var device =  rec.getFieldText('custrecord_clgx_dcim_device') || '';
		var panelid =  parseInt(rec.getFieldValue('custrecord_clgx_power_panel_pdpm')) || 0;
		
		if(panelid){
			var panel = nlapiLoadRecord('customrecord_ncfar_asset', panelid);
			var typeid = parseInt(panel.getFieldValue('custrecord_clgx_panel_type')) || 0;
			var type = panel.getFieldText('custrecord_clgx_panel_type') || '';
		} else {
			var typeid = 0;
			var type = '';
		}
		
		var phasea = parseFloat(rec.getFieldValue('custrecord_clgx_dcim_sum_day_amp_a_a')) || 0;
		var phaseb = parseFloat(rec.getFieldValue('custrecord_clgx_dcim_sum_day_amp_a_b')) || 0;
		var phasec = parseFloat(rec.getFieldValue('custrecord_clgx_dcim_sum_day_amp_a_c')) || 0;
		
		var kwa = parseFloat(rec.getFieldValue('custrecord_clgx_dcim_sum_day_kw_a')) || 0;
		
		obj.power["secondary"] = {
			"id": obj.power.id,
			"rid": obj.power.rid,
			"number": rec.getFieldValue('name') || '',
			"module": parseInt(rec.getFieldText('custrecord_cologix_power_upsbreaker')) || 0,
			"lines": rec.getFieldText('custrecord_cologix_power_subpanel') || '',
			"breaker": parseInt(rec.getFieldText('custrecord_cologix_power_circuitbreaker')) || 0,
			"soid": soid,
			"rsoid": soid.toString(radix),
			"so": so[1],
			"volts": rec.getFieldText('custrecord_cologix_power_volts') || '',
			"amps": rec.getFieldText('custrecord_cologix_power_amps') || '',
			"camps": intAmps,
			"facility": rec.getFieldText('custrecord_cologix_power_facility') || '',
			"spaceid": spaceid,
			"rspaceid": spaceid.toString(radix),
			"space": rec.getFieldText('custrecord_cologix_power_space') || '',
			"panelid": deviceid,
			"rpanelid": deviceid.toString(radix),
			"panel": device,
			"typeid": typeid,
			"type": type,
			"box": rec.getFieldText('custrecord_clgx_outlet_box_serial_no_pwr') || '',
			"bus": rec.getFieldText('custrecord_clgx_busway_pwr') || '',
			"phasea": phasea,
			"phaseb": phaseb,
			"phasec": phasec,
			"kwa": kwa
		};
		
	} else {
		
		obj.power["secondary"] = {
				"id": 0,
				"rid": '',
				"number": '',
				"module": 0,
				"lines": '',
				"breaker": 0,
				"soid": 0,
				"rsoid": '',
				"so": '',
				"volts": '',
				"amps": '',
				"camps": 0,
				"facility": '',
				"spaceid": 0,
				"rspaceid": '',
				"space": '',
				"panelid": 0,
				"rpanelid": '',
				"panel": '',
				"typeid": 0,
				"type": '',
				"box": '',
				"bus": '',
				"phasea": 0,
				"phaseb": 0,
				"phasec": 0
			};
	}
	
	return obj;
}
