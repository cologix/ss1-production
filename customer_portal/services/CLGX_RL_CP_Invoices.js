nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Invoices.js
//	ScriptID:	customscript_clgx_rl_cp_invoices
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=657&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/17/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv) {

    if(srights.invoices > 0){

        var direction = datain['direction'] || 'ASC';
        var order_by = datain['order_by'] || '';
        var page = datain['page'] || 1;
        var per_page = datain['per_page'] || 0;
        var search = datain['search'] || '';
        var csv = datain['csv'] || 0;

        obj["invoices"] = get_invoices (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv);

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing invoices.';

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_INVOICES';
        obj.msg = 'No rights for invoices.';
    }

    return obj;
});


function get_invoices (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv){

    var obj = {};
    var arr = [];
    var total = 0;
    var pages = 0;
    var start = 0;
    var end = 0;
    var has_more = false;

    search = JSON.parse(search);

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_customer",null,"anyof",companyid));
    if(search){
        //if(search.id){
        //	filters.push(new nlobjSearchFilter("name",null,"contains",search.id));
        //}
        if(search.from && search.to){
            filters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_date",null,"within",search.from,search.to));
        }
        if(search.from && !search.to){
            filters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_date",null,"on",search.from));
        }
        if(!search.from && search.to){
            filters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_date",null,"on",search.to));
        }
    }
    var records = nlapiSearchRecord('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_api_invoices', filters, columns);

    if(records){

        total = records.length;
        pages = Math.ceil(total / per_page);
        page = (parseInt(page) > pages ? pages : parseInt(page));
        has_more = (pages > 1 ? true : false);
        start = (page - 1) * per_page;
        end = (start + per_page > total ? total : start + per_page);

        for ( var i = start; i < end; i++ ) {

            var rec = new Object();
            var invoiceid = parseInt(records[i].getValue('internalid',null,null));
            //rec["id"] = invoiceid;
            rec["rid"] = invoiceid.toString(radix);
            rec["number"] = records[i].getValue('name',null,null);
            rec["date"] = records[i].getValue('custrecord_clgx_consol_inv_date',null,null) || '';
            rec["total"] = parseFloat(records[i].getValue('custrecord_clgx_consol_inv_total',null,null)) || 0;
            rec["balance"] = parseFloat(records[i].getValue('custrecord_clgx_consol_inv_balance',null,null)) || 0;
            rec["cur"] = records[i].getValue('custrecord_clgx_consol_inv_currency',null,null);
            var pdfid = parseInt(records[i].getValue('custrecord_clgx_consol_inv_pdf_file_id',null,null)) || 0;
            rec["pdf"] = pdfid;
            rec["rpdf"] = pdfid.toString(radix);
            var jsonid = parseInt(records[i].getValue('custrecord_clgx_consol_inv_json_file_id',null,null)) || 0;
            rec["json"] = jsonid;
            rec["rjson"] = jsonid.toString(radix);

            var month = records[i].getText('custrecord_clgx_consol_inv_month_display',null,null) || '';
            var year = records[i].getValue('custrecord_clgx_consol_inv_year_display',null,null) || '';
            if(month && year){
                rec["from"] = month + '/01/' + year;
                rec["to"] = moment(month + '/01/' + year).endOf('month').format('M/D/YYYY');
            } else {
                rec["from"] = '';
                rec["to"] = '';
            }
            rec["location"] = records[i].getText('custrecord_clgx_consol_inv_clocation',null,null) || '';;
            arr.push(rec);
        }
    }

    obj["data"] = arr;
    if(csv){
        obj["csv"] = get_invoices_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv);
    }
    obj["search"] = search;
    obj["lists"] = {};
    obj["direction"] = direction;
    obj["order_by"] = order_by;
    obj["page"] = page;
    obj["pages"] = pages;
    obj["per_page"] = per_page;
    obj["has_more"] = has_more;
    obj["start"] = start;
    obj["end"] = end;
    obj["total"] = total;

    return obj;
}


function get_invoices_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,search,csv){

    var text = 'ID,Date,From,To,Currency,Market,Total\n';

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_customer",null,"anyof",companyid));
    if(search){
        //if(search.id){
        //	filters.push(new nlobjSearchFilter("name",null,"contains",search.id));
        //}
        if(search.from && search.to){
            filters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_date",null,"within",search.from,search.to));
        }
        if(search.from && !search.to){
            filters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_date",null,"on",search.from));
        }
        if(!search.from && search.to){
            filters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_date",null,"on",search.to));
        }
    }
    var records = nlapiSearchRecord('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_api_invoices', filters, columns);

    for ( var i = 0; records != null && i < records.length; i++ ) {
        var month = records[i].getText('custrecord_clgx_consol_inv_month_display',null,null) || '';
        var year = records[i].getValue('custrecord_clgx_consol_inv_year_display',null,null) || '';
        text += records[i].getValue('name',null,null) + ',';
        text += records[i].getValue('custrecord_clgx_consol_inv_date',null,null) + ',';
        if(month && year){
            text += moment( month + '/01/' + year).format('M/D/YYYY')+ ',';
            text += moment(month + '/01/' + year).endOf('month').format('M/D/YYYY')+ ',';
        } else {
            text += ''+ ',';
            text += ''+ ',';
        }
        text += records[i].getValue('custrecord_clgx_consol_inv_currency',null,null)+ ',';
        text += records[i].getText('custrecord_clgx_consol_inv_clocation',null,null)+ ',';
        text += records[i].getValue('custrecord_clgx_consol_inv_total',null,null) + ',';
        text += '\n';
    }
    return text;
}