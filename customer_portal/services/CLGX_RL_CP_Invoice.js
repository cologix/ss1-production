nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Invoice.js
//	ScriptID:	customscript_clgx_rl_cp_invoice
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=675&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/17/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    if(srights.invoices > 0){

        var rid = datain['rid'] || "0";
        var type = datain['type'] || 'json';

        var invoiceid = parseInt(rid, radix);
        var rec = nlapiLoadRecord('customrecord_clgx_consolidated_invoices', invoiceid);

        obj["id"] = invoiceid;
        obj["rid"] = rid;
        obj["number"] = rec.getFieldValue('name');

        var pdfid = parseInt(rec.getFieldValue('custrecord_clgx_consol_inv_pdf_file_id'));
        var jsonid = parseInt(rec.getFieldValue('custrecord_clgx_consol_inv_json_file_id'));

        var inv = {};

        try {
            var pdf = nlapiLoadFile(pdfid);
            obj["rpdf"] = pdfid.toString(radix);
            //obj["pdfurl"] = 'https://system.na2.netsuite.com' + pdf.getURL(),
            obj["pdfurl"] = 'https://1337135.app.netsuite.com' + pdf.getURL(),
            obj["pdfname"] = pdf.getName().replace(/,/g, "");
            var bodyFile = pdf.getValue();
            obj["cont"]=bodyFile;
        }
        catch (e) {
            obj["rpdf"] = '0';
            obj["pdfurl"] = '',
                obj["pdfname"] = '';
            obj["cont"]='';
        }

        try {
            var json = nlapiLoadFile(jsonid);
            obj["rjson"] = jsonid.toString(radix);

            if(type == 'json'){

                var content = JSON.parse(json.getValue());

                inv["node"] = content.node;
                inv["nodetype"] = content.nodetype;
                inv["id"] = invoiceid;
                inv["number"] = rec.getFieldValue('name');
                var pdfid = parseInt(rec.getFieldValue('custrecord_clgx_consol_inv_pdf_file_id'));
                inv["rpdf"] = pdfid.toString(radix);
                inv["date"] = rec.getFieldValue('custrecord_clgx_consol_inv_date');
                inv["start"] = content.start;
                inv["end"] = content.end;
                inv["from"] = content.from;
                inv["to"] = content.to;
                inv["due"] = content.due;

                var cc = rec.getFieldValue('custrecord_clgx_consol_inv_cc_enabled');
                if(cc == 'T'){
                    inv["cc"] = true;
                } else {
                    inv["cc"] = false;
                }
                inv["children"] = [];
                for ( var i = 0; i < content.children.length; i++ ) {
                    if((content.children[i].nodetype == 'category' || content.children[i].nodetype == 'totals' || content.children[i].nodetype == 'cpis') && content.children[i].children.length > 0){
                        inv.children.push(content.children[i]);
                    }
                    if(content.children[i].nodetype == 'contacts' && content.children[i].children.length > 0){
                        content.children[i].node = 'Emailed to';
                        var childrenObj=content.children[i];
                        var childArr=childrenObj.children;
                        for ( var j = 0;j < childArr.length; j++ )
                        {
                            var email=nlapiLookupField('contact', childArr[j].contactid, 'email') ;
                            childArr[j].node=email;
                        }
                        childrenObj.children=childArr;
                        inv.children.push(childrenObj);
                    }
                }
            }
        }
        catch (e) {
            obj["rjson"] = '0';
        }

        obj["content"] = inv;

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing invoices.';

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_INVOICES';
        obj.msg = 'No rights for invoices.';
    }

    return obj;

});