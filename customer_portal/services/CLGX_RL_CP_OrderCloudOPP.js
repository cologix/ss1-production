nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_OrderCloudOPP.js
//	ScriptID:	customscript_clgx_rl_cp_ordercloudopp
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=676&deploy=1
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	17/08/2017
//------------------------------------------------------



var put = wrap(function put(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    if(srights.orders > 0){

        var title= datain['title'] || "";
        var facility = datain['facility'] || '';
        var tfacility = datain['tfacility'] || "";
        var providerName = datain['providerName'] || "";
        nlapiLogExecution('DEBUG','providerName',providerName);
        var type = datain['type'] || "";
        var lang = datain['lang'] || "en";
        var ratecard = datain['ratecard'] || 0;
        var server=datain['server']||"";
        var requestor=datain['requestor']|| "";
        var uname=datain['uname']||"";
        var timestamp=moment().format("YYYY-MM-DD HH:mm:ss  A Z");
        //schedule script opportunity+SO
        var arrParam = new Array();
        arrParam['custscript_ss_cp_orOPP_lanc'] = lang;
        arrParam['custscript_ss_cp_orOPP_title'] = title;
        arrParam['custscript_ss_cp_orOPP_company']=companyid;
        arrParam['custscript_ss_cp_orOPP_contact']=contactid;
        arrParam['custscript_ss_cp_orOPP_loc']=facility;
        arrParam['custscript_ss_cp_orOPP_provider']=providerName;
        arrParam['custscript_ss_cp_orOPP_facT']=tfacility;
        arrParam['custscript_ss_cp_orOPP_type']=type;
        arrParam['custscript_ss_cp_orOPP_server']=server;
        arrParam['custscript_ss_cp_orOPP_req']=requestor;
        arrParam['custscript_ss_cp_orOPP_uname']=uname;
        arrParam['custscript_ss_cp_orOPP_timestampc']=timestamp;
        arrParam['custscript_ss_cp_orOPP_ratec']=ratecard;
        var status = nlapiScheduleScript('customscript_clgx_ss_cp_ordercloudopp', null, arrParam);



        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        if(ratecard==0) {
            obj.msg = 'Thank you for submitting your order.  You will receive an email update regarding the status of your order.';
            if (lang != 'en') {
                obj.msg = 'Nous vous remercions d&#39;avoir envoy&eacute; votre commande. Vous recevrez une mise &agrave; jour par courriel concernant le statut de votre commande.';

            }
        }
        else{
            obj.msg = 'Thank you for submitting your order.  You will receive an email update regarding the status of your order.';
            if (lang != 'en') {
                obj.msg = 'Nous vous remercions d&#39;avoir envoy&eacute; votre commande. Vous recevrez une mise &agrave; jour par courriel concernant le statut de votre commande.';

            }
        }
    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_Orders';
        obj.msg = 'No rights for orders.';
    }

    return obj;

});