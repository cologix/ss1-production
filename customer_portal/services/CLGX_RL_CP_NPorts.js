//------------------------------------------------------
//	Script:		CLGX_RL_CP_NPorts.js
//	ScriptID:	customscript_clgx_rl_cp_nports
//	ScriptType:	RESTlet
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	01/04/2018
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv) {
    var direction = datain['direction'] || 'ASC';
    var order_by = datain['order_by'] || '';
    var page = datain['page'] || 1;
    var per_page = datain['per_page'];
    var csv = datain['csv'] || 0;

    var getPorts=get_nports(companyid,direction,order_by,page,per_page,csv);
    return getPorts;

});
var put = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv) {

    var id = datain['id'] || 0;

    var getPorts=get_nport(id);
    return getPorts;

});


function get_nports(id,direction,order_by,page,per_page,csv){
    var total = 0;
    var pages = 0;
    var start = 0;
    var end = 0;
    var has_more = false;
    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("name",'custrecord_xconnect_service_order',"anyof",id));
    var records = nlapiSearchRecord('customrecord_cologix_crossconnect','customsearch5523', filters, columns);

    var obj=new Object();
    var arrObj=new Array();
    if(records!=null) {
        total = records.length;
        nlapiLogExecution('DEBUG', 'id', id);
        nlapiLogExecution('DEBUG', 'total1', total);
        nlapiLogExecution('DEBUG', 'per_page', per_page);
        pages = Math.ceil(total / per_page);
        page = (parseInt(page) > pages ? pages : parseInt(page));
        has_more = (pages > 1 ? true : false);
        start = (page - 1) * per_page;
        end = (start + per_page > total ? total : start + per_page);
        nlapiLogExecution('DEBUG', 'start', start);
        nlapiLogExecution('DEBUG', 'end', end);

        for ( var j = start; j < end; j++ ) {
            var searchAP = records[j];
            var columns = searchAP.getAllColumns();
            var id = searchAP.getValue(columns[0]);
            var name = searchAP.getText(columns[1]);
            var note = searchAP.getValue(columns[2]);
            var device= searchAP.getValue(columns[3]);
            var created= searchAP.getValue(columns[4]);
            var internalid= searchAP.getValue(columns[5]);
            var rec = new Object();
            rec.id = id;
            rec.name = name;
            rec.note = note;
            rec.device = device;
            rec.created = created;
            rec.internalid =internalid;
            arrObj.push(rec)

        }
        obj["data"] = arrObj;
        //   nlapiLogExecution('DEBUG', 'obj["data"]', obj["data"]);
        /*  if(csv){
              obj["csv"] = get_cases_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,type,search,csv);
          }*/

        obj["direction"] = direction;
        obj["order_by"] = order_by;
        obj["page"] = page;
        obj["pages"] = pages;
        obj["per_page"] = per_page;
        obj["has_more"] = has_more;
        obj["start"] = start;
        obj["end"] = end;
        obj["total"] = total;
    }else{
        obj["data"] = [];
        obj["page"] = page;
        obj["pages"] = 1;
        obj["per_page"] = 16;
        obj["has_more"] =false;
        obj["start"] = 0;
        obj["end"] = 0;
        obj["total"] = 0;
    }
    return obj;
}

function get_nport(id){

    var obj=new Object();
    var record = nlapiLoadRecord('customrecord_cologix_crossconnect',id);

    obj["id"] = record.getFieldValue('name');
    obj["port"] = record.getFieldText('custrecord_clgx_a_end_port');
    var soID=record.getFieldValue('custrecord_xconnect_service_order');
    var customer=nlapiLookupField('salesorder', soID, 'entity') || 0;
    if(customer>0){
        obj["accountnumber"] = nlapiLookupField('customer', customer, 'accountnumber') || 0;;
    }
    var sonbr=nlapiLookupField('salesorder', soID, 'custbody_cologix_legacy_so') || 0;
    if(sonbr==0){obj["tranid"] = nlapiLookupField('salesorder', soID, 'tranid') || 0;}else{
        obj["tranid"] =sonbr;
    }

    obj["description"] =record.getFieldValue('custrecord_clgx_xc_note')||'';
    obj["router"]=record.getFieldValue('custrecord_clgx_device_name_xc');
    obj["prtg"]=record.getFieldValue('custrecord_clgx_prtg_id')||0;
    return obj;
}