nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Update_Portal_Transactions.js
//	ScriptID:	customscript_clgx_RL_CP_UPDATE_Portal_Tr
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=709&deploy=1
//	@authors:	Catalina Taran
//	Created:	10/04/2016
//------------------------------------------------------

function put(datain) {
    var rid = datain['rid'] || "0";
    var boxid = datain['boxid'] || "";
    var agreementid=datain['agreementid'] || "";
    var agreementstatus=datain['agreementstatus'] || "";
    var test = datain['test'] || "0";
    var rate = datain['rate'] || "0";
    if(test==1)
    {
        nlapiLogExecution('DEBUG','responseFinal', datain['resultFinal']);
    }
    else {
        if (boxid != '') {
            nlapiSubmitField('customrecord_clgx_cp_q_transactions', rid, 'custrecord_clgx_cp_tq_folderid', boxid);
        }
        if (agreementid != '') {
            nlapiSubmitField('customrecord_clgx_cp_q_transactions', rid, 'custrecord_clgx_cp_tq_agreementid', agreementid);
        }
        if ((agreementid != '') || (boxid != '')) {
            nlapiSubmitField('customrecord_clgx_cp_q_transactions', rid, 'custrecord_clgx_cp_tq_trpr', 'T');
        }
        if (agreementstatus != '') {
            nlapiSubmitField('customrecord_clgx_cp_q_transactions', rid, 'custrecord_clgx_cp_tq_agproc', 'T');
            var proposalid = nlapiLookupField('customrecord_clgx_cp_q_transactions', rid, 'custrecord_clgx_cp_tq_prid');
            var ratecard = nlapiLookupField('customrecord_clgx_cp_q_transactions', rid, 'custrecord_clgx_cp_tq_ratecard');
            //

            //create the SO
            if(ratecard==0)
            {
                var arrParam = new Array();
                arrParam['custscript_proposalid'] = proposalid;
                arrParam['custscript_userid'] = 642142;
               nlapiLogExecution('DEBUG','SS', proposalid);
                var status = nlapiScheduleScript('customscript_clgx_ss_so', null, arrParam);
            }
        }
    }

}
