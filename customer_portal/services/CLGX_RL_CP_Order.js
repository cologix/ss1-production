nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Order.js
//	ScriptID:	customscript_clgx_rl_cp_order
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=676&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/17/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    if(srights.orders > 0){

        var rid = datain['rid'] || "0";
        var id = parseInt(rid, radix);

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('type',null,'GROUP'));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("internalid",null,"is",id));
        var searchType = nlapiSearchRecord('transaction', null, arrFilters, arrColumns);

        var trantype = searchType[0].getValue('type',null,'GROUP');

        nlapiLogExecution('DEBUG','id', id+';'+trantype);

        obj["order"] = {"id":id,"rid":rid};
        if(trantype == 'Opprtnty'){
            obj = get_opportunity(obj);
        }
        if(trantype == 'Estimate'){
            obj = get_proposal(obj);
        }
        if(trantype == 'SalesOrd'){
            obj = get_salesorder(obj,radix);
        }

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing invoices.';

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_INVOICES';
        obj.msg = 'No rights for invoices.';
    }

    return obj;

});


var put = wrap(function put(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    if(srights.orders > 0){

        var title= datain['title'] || "";
        var facility = datain['facility'] || '0';
        var spaceid = datain['spaceid'] || "";
        var afacility = datain['afacility'] || "0";
        var tfacility = datain['tfacility'] || "";
        var metrotype = datain['metrotype'] || "";
        var conn_type = datain['conn_type'] || "";
        var speed = datain['speed'] || "";
        var loa=datain['loa']||'0';
        var billing=datain['billing']||'0';
        var requested=datain['requested']||"";
        var qty=datain['qty']||"";
        var rate=datain['rate']||"";
        var totalmrc=datain['totalmrc']||"";
        var ratecard=datain['ratecard']||"";
        var other=datain['other']||'0';
        var item=datain['item']||'0';
        var languagePortal=datain['lanportal']||'en';
        var notes=datain['notes']||"";
        var po=datain['po']||"";
        var arrCustomerDetails = nlapiLookupField('customer',companyid,['salesrep','companyname']);
        var salesRepID = arrCustomerDetails['salesrep'];
        var custCompName = arrCustomerDetails['companyname'];
        var cust=nlapiLoadRecord('customer',companyid);
        var ccm = cust.getFieldValue('custentity_clgx_cust_ccm');
       // var ccm = arrCustomerDetails['custentity_clgx_cust_ccm'];
        var fname=nlapiLookupField('contact', contactid, 'firstname') || '';
        var lname=nlapiLookupField('contact', contactid, 'lastname') || '';
        var custContactName=fname+' '+lname;
        var strMemoOrder =
            'Space ID : ' + spaceid + ';\n\n' +
            ' Terminating Facility : ' + tfacility + ';\n\n' +
            ' Metro Connect Type : ' + metrotype + ';\n\n' +
            ' Cross Connect Type : ' + conn_type + ';\n\n' +
            ' Speed : ' + speed + ';\n\n' +
            ' Notes : ' + notes;

        if(parseInt(salesRepID) > 0){
            var arrRepDetails = nlapiLookupField('employee',salesRepID,['entityid']);
            var salesRepName = arrRepDetails['entityid'];
        }
        else{
            var salesRepName = '';
        }
        if(other==1)
        {



            var strMemo = 'A request for a new order was received from:\n\n' +
                'Contact Name : ' + custContactName + '\n\n' +
                'Notes : ' + notes;
            // create a new opportunity
            var record = nlapiCreateRecord('opportunity');
            record.setFieldValue('entity', companyid);
            record.setFieldValue('title', 'Portal new order request');
            record.setFieldValue('location', facility);
            record.setFieldValue('leadsource', 73127);
            record.setFieldValue('memo', strMemo.substr(0,999));
            var idRec = nlapiSubmitRecord(record, false,true);

            var arrOpptyDetails = nlapiLookupField('opportunity',idRec,['tranid']);
            var location = nlapiLookupField('opportunity',idRec,'location',true);
            var opptyNbr = arrOpptyDetails['tranid'];

            var emailSubject = 'New Service Request '+opptyNbr+'-'+custCompName;
            var emailSubjectRep = 'New Order Request for ' + custCompName + ' (Asked by ' + custContactName + ')';
            var emailBody = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
                'Your request for a new order was received with the following info:\n\n' +
                'Reference Number : ' + opptyNbr + '\n' +
                'Data Center : ' + location + '\n' +
                'Notes : ' + notes + '\n\n' +
                'Thank you for your business.  I will be following up with you shortly to finalize the details of this request.\n\n' +
                'Sincerely,\n' +
                salesRepName + '\n\n';
            var emailBodyCustomer ='Thank you for your service request.  We are reviewing the details and will follow up with you shortly.';
            if(languagePortal=='fr')
            {
                var emailSubject = 'Nouvelle Demande de Service '+opptyNbr+'-'+custCompName;
                var emailBodyCustomer='Nous vous remercions de votre demande de service. Nous examinons présentement les détails et nous vous contacterons sous peu.';
            }
            if (salesRepID != null && salesRepID != ''){
                nlapiSendEmail(salesRepID,salesRepID,emailSubjectRep,emailBody,null,null,null,null,true);
                nlapiSendEmail(salesRepID,contactid,emailSubject,emailBodyCustomer,null,null,null,null,true);
                //if (superviserID != null){
                //	nlapiSendEmail(salesRepID,superviserID,emailSubjectRep,emailBody,null,null,null,null);
                //}
            }
            obj["id"] = idRec;
            obj["number"] =  opptyNbr;
            obj["type"] =  "opportunity";
        }
        else {
            var taxcode1 = 0;
            var taxcode2 = 0;

            //MTL
            if (facility == 5 || facility == 8 || facility == 9 || facility == 10 || facility == 11 || facility == 12 || facility == 27) {
                var taxcode1 = 308;
                var taxcode2 = 308;
            } else if (facility == 6 || facility == 13 || facility == 15) { //TOR
                var taxcode1 = 11;
                var taxcode2 = 11;
            }
            else if (facility == 7 || facility == 28) { //VAN
                var taxcode1 = 297;
                var taxcode2 = 297;
            }
            else if (facility == 34 || facility == 39 || facility == 38) { //COL
                var taxcode1 = 515;
                var taxcode2 = 515;
            }
            else if (facility == 31 || facility == 40) { //JAX
                var taxcode2 = 498;
            }
            else if (facility == 42) { //LAK
                var taxcode2 = 565;
            }
            var date = new Date();
            nlapiLogExecution('DEBUG', 'title', title);
            nlapiLogExecution('DEBUG', 'facility', facility);
            nlapiLogExecution('DEBUG', 'totalmrc', totalmrc);
            nlapiLogExecution('DEBUG', 'billing', billing);


            // create a new opportunity



            if (ratecard != 0) {
                var record = nlapiCreateRecord('opportunity');
                record.setFieldValue('title', title);
                record.setFieldValue('entity', companyid);
                record.setFieldValue('memo', strMemoOrder.substr(0,999));
                record.setFieldValue('location', facility);
                record.setFieldValue('forecasttype', 3);
                record.setFieldValue('entitystatus', 11);
                // record.setFieldValue('memo', strMemo.substr(0,999));
                // record.setFieldValue('statusRef', 'inProgress');
                record.setFieldValue('custbody_cologix_opp_sale_type', 2);
                record.setFieldValue('leadsource', 73127);
                record.setFieldValue('custbody_clgx_target_install_date', requested);
                record.setLineItemValue('item', 'item', 1, item);
                record.setLineItemValue('item', 'quantity', 1, qty);
                record.setLineItemValue('item', 'rate', 1, 0);
                record.setLineItemValue('item', 'location', 1, facility);
                record.setLineItemValue('item', 'price', 1, -1);
                record.setLineItemValue('item', 'pricelevels', 1, -1);
                record.setLineItemValue('item', 'amount', 1, 0);
                if (taxcode1 > 0) {
                    record.setLineItemValue('item', 'taxcode', 1, taxcode1);
                }
                //item 244
                record.setLineItemValue('item', 'item', 2, 244);
                record.setLineItemValue('item', 'quantity', 2, qty);
                record.setLineItemValue('item', 'rate', 2, 0);
                record.setLineItemValue('item', 'amount', 2, 0);
                record.setLineItemValue('item', 'location', 2, facility);
                record.setLineItemValue('item', 'price', 2, -1);
                record.setLineItemValue('item', 'pricelevels', 2, -1);
                if (taxcode2 > 0) {
                    record.setLineItemValue('item', 'taxcode', 2, taxcode2);
                }
                try {
                    var recordId = nlapiSubmitRecord(record, true, true);

                }
                catch (error) {
                    nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
                }
                if (recordId != null) {
                    var totals = clgx_transaction_totals(companyid, 'oppty', recordId);
                    //create the proposal

                    // var recProposal = nlapiTransformRecord(fromrecord, recordId, torecord);
                    var termstitle=2;
                    if(languagePortal=='fr')
                    {
                        var termstitle=3;
                    }
                    var recProposal = nlapiCreateRecord('estimate');
                    recProposal.setFieldValue('entity', companyid);
                    recProposal.setFieldValue('memo', '');
                    recProposal.setFieldValue('custbody_clgx_contract_terms_title', termstitle);
                    recProposal.setFieldValue('otherrefnum', po);
                    recProposal.setFieldValue('custbodyclgx_aa_type', 3);
                    recProposal.setFieldValue('custbody_cologix_annual_accelerator', '3');
                    var stTermsBody = nlapiLookupField('customrecord_clgx_contract_terms', termstitle, 'custrecord_clgx_contract_terms_body');
                    recProposal.setFieldValue('custbody_clgx_contract_terms_body', stTermsBody);
                    recProposal.setFieldValue('custbody_clgx_order_notes', strMemoOrder.substr(0,999));
                    recProposal.setFieldValue('opportunity', recordId);
                    recProposal.setFieldValue('entitystatus',11);
                    recProposal.setFieldValue('title', title);
                    recProposal.setFieldValue('location', facility);
                    recProposal.setFieldValue('custbody_clgx_contract_terms_contact', contactid);
                    //  recProposal.setFieldValue('forecasttype', 3);
                    // recProposal.setFieldValue('entitystatus', 11);
                    recProposal.setFieldValue('billingaddress_text', billing);
                    // record.setFieldValue('statusRef', 'inProgress');
                    recProposal.setFieldValue('custbody_cologix_opp_sale_type', 2);
                    recProposal.setFieldValue('leadsource', 73127);
                    recProposal.setFieldValue('custbody_clgx_target_install_date', requested);

                    //add the items
                    recProposal.setLineItemValue('item', 'item', 1, item);
                    recProposal.setLineItemValue('item', 'quantity', 1, qty);
                    recProposal.setLineItemValue('item', 'rate', 1, 0);
                    recProposal.setLineItemValue('item', 'location', 1, facility);
                    recProposal.setLineItemValue('item', 'price', 1, -1);
                    recProposal.setLineItemValue('item', 'pricelevels', 1, -1);
                    recProposal.setLineItemValue('item', 'amount', 1, 0);
                    recProposal.setLineItemValue('item', 'billingschedule', 1, 3);
                    if (taxcode1 > 0) {
                        recProposal.setLineItemValue('item', 'taxcode', 1, taxcode1);
                    }
                    recProposal.setLineItemValue('item', 'item', 2, 244);
                    recProposal.setLineItemValue('item', 'quantity', 2, qty);
                    recProposal.setLineItemValue('item', 'rate', 2, 0);
                    recProposal.setLineItemValue('item', 'amount', 2, 0);
                    recProposal.setLineItemValue('item', 'location', 2, facility);
                    recProposal.setLineItemValue('item', 'price', 2, -1);
                    recProposal.setLineItemValue('item', 'pricelevels', 2, -1);
                    recProposal.setLineItemValue('item', 'billingschedule', 2, 17);
                    if (taxcode2 > 0) {
                        recProposal.setLineItemValue('item', 'taxcode', 2, taxcode2);
                    }
                    try {
                        var recordIdPR = nlapiSubmitRecord(recProposal, true, true);
                        var proposalNumber= nlapiLookupField('estimate',recordIdPR,'tranid');
                        if(languagePortal=='en')
                        {

                            var emailSubjectProposal="New Proposal "+proposalNumber+" - "+custCompName;
                            var emailBodyProposal = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
                                'New Proposal : ' + proposalNumber + '\n' +
                                'Title : ' + title + '\n' +
                                'Thank you for your service request.  We are reviewing the details and will follow up with you shortly.\n\n' +
                                'Sincerely,\n' +
                                salesRepName + '\n\n';
                        }
                        else if(languagePortal=='fr')
                        {
                            var emailSubjectProposal="Nouvelle Proposition "+proposalNumber+" - "+custCompName;
                            var emailBodyProposal = 'Bonjour ' + custContactName + ' (' + custCompName + '),\n\n' +
                                'Nouvelle Proposition : ' + proposalNumber + '\n' +
                                'Titre : ' + title + '\n' +
                                "Nous vous remercions de votre demande de service. Nous examinons les détails et nous suivrons avec vous sous peu.\n\n" +
                                'Au plaisir de vous être utile,\n' +
                                salesRepName + '\n\n';
                        }
                        nlapiSendEmail(salesRepID,contactid,emailSubjectProposal,emailBodyProposal,null,null,null,null,true);
                        if(ccm!=null) {
                            nlapiSendEmail(salesRepID, ccm, emailSubjectProposal, emailBodyProposal, null, null, null, null, true);
                        }
                    }
                    catch (error) {
                        nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
                    }
                    if (recordIdPR != null) {
                        var totals = clgx_transaction_totals(companyid, 'proposal', recordIdPR);

                    }
                }
            }
            else {
                var record = nlapiCreateRecord('opportunity');
                record.setFieldValue('title', title);
                record.setFieldValue('entity', companyid);
                record.setFieldValue('location', facility);
                record.setFieldValue('memo', strMemoOrder.substr(0,999));
                //record.setFieldValue('forecasttype', 3);
                //record.setFieldValue('entitystatus', 11);
                // record.setFieldValue('statusRef', 'inProgress');
                record.setFieldValue('custbody_cologix_opp_sale_type', 2);
                // record.setFieldValue('memo', strMemo.substr(0,999));
                record.setFieldValue('leadsource', 73127);
                record.setFieldValue('custbody_clgx_target_install_date', requested);
                record.setLineItemValue('item', 'item', 1, item);
                record.setLineItemValue('item', 'quantity', 1, qty);
                record.setLineItemValue('item', 'rate', 1, rate);
                record.setLineItemValue('item', 'location', 1, facility);
                record.setLineItemValue('item', 'price', 1, 1);
                //record.setLineItemValue('item', 'pricelevels',1, -1);
                record.setLineItemValue('item', 'amount', 1, totalmrc);
                if (taxcode1 > 0) {
                    record.setLineItemValue('item', 'taxcode', 1, taxcode1);
                }
                //item 244
                record.setLineItemValue('item', 'item', 2, 244);
                record.setLineItemValue('item', 'quantity', 2, qty);
                record.setLineItemValue('item', 'rate', 2, rate);
                record.setLineItemValue('item', 'amount', 2, 0);
                record.setLineItemValue('item', 'amount', 2, parseInt(qty) * parseInt(rate));
                record.setLineItemValue('item', 'location', 2, facility);
                record.setLineItemValue('item', 'price', 2, -1);
                record.setLineItemValue('item', 'pricelevels', 2, -1);
                if (taxcode2 > 0) {
                    record.setLineItemValue('item', 'taxcode', 2, taxcode2);
                }
                try {
                    var recordId = nlapiSubmitRecord(record, true, true);
                }
                catch (error) {
                    nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
                }
                if (recordId != null) {
                    var totals = clgx_transaction_totals(companyid, 'oppty', recordId);
                    //create the proposal

                    var termstitle=2;
                    if(languagePortal=='fr')
                    {
                        var termstitle=3;
                    }
                    var recProposal = nlapiCreateRecord('estimate');
                    recProposal.setFieldValue('entity', companyid);
                    recProposal.setFieldValue('memo', '');
                    recProposal.setFieldValue('custbody_clgx_contract_terms_title', termstitle);
                    recProposal.setFieldValue('custbodyclgx_aa_type', 3);
                    recProposal.setFieldValue('custbody_cologix_annual_accelerator', '3');
                    recProposal.setFieldValue('otherrefnum', po);
                    var stTermsBody = nlapiLookupField('customrecord_clgx_contract_terms', termstitle, 'custrecord_clgx_contract_terms_body');
                    recProposal.setFieldValue('custbody_clgx_contract_terms_body', stTermsBody);
                    recProposal.setFieldValue('custbody_clgx_order_notes', strMemoOrder.substr(0,999));
                    recProposal.setFieldValue('opportunity', recordId);
                    recProposal.setFieldValue('entitystatus',11);
                    recProposal.setFieldValue('title', title);
                    recProposal.setFieldValue('location', facility);
                    recProposal.setFieldValue('custbody_clgx_contract_terms_contact', contactid);
                    //  recProposal.setFieldValue('forecasttype', 3);
                    // recProposal.setFieldValue('entitystatus', 11);
                    recProposal.setFieldValue('billingaddress_text', billing);
                    // record.setFieldValue('statusRef', 'inProgress');
                    recProposal.setFieldValue('custbody_cologix_opp_sale_type', 2);
                    recProposal.setFieldValue('leadsource', 73127);
                    recProposal.setFieldValue('custbody_clgx_target_install_date', requested);

                    //add the items
                    recProposal.setLineItemValue('item', 'item', 1, item);
                    recProposal.setLineItemValue('item', 'quantity', 1, qty);
                    recProposal.setLineItemValue('item', 'rate', 1, rate);
                    recProposal.setLineItemValue('item', 'location', 1, facility);
                    recProposal.setLineItemValue('item', 'price', 1, 1);
                    //  recProposal.setLineItemValue('item', 'pricelevels',1, -1);
                    recProposal.setLineItemValue('item', 'amount', 1, totalmrc);
                    recProposal.setLineItemValue('item', 'billingschedule', 1, 3);
                    if (taxcode1 > 0) {
                        recProposal.setLineItemValue('item', 'taxcode', 1, taxcode1);
                    }
                    //item 244
                    recProposal.setLineItemValue('item', 'item', 2, 244);
                    recProposal.setLineItemValue('item', 'quantity', 2, qty);
                    recProposal.setLineItemValue('item', 'rate', 2, rate);
                    //recProposal.setLineItemValue('item', 'amount',2, 0);
                    recProposal.setLineItemValue('item', 'amount', 2, parseInt(qty) * parseInt(rate));
                    recProposal.setLineItemValue('item', 'location', 2, facility);
                    recProposal.setLineItemValue('item', 'price', 2, -1);
                    recProposal.setLineItemValue('item', 'pricelevels', 2, -1);
                    recProposal.setLineItemValue('item', 'billingschedule', 2, 17);
                    if (taxcode2 > 0) {
                        recProposal.setLineItemValue('item', 'taxcode', 2, taxcode2);
                    }
                    try {
                        var recordIdPR = nlapiSubmitRecord(recProposal, true, true);
                        var tranid = nlapiLookupField('estimate',recordIdPR,'tranid');
                        var emailSubjectProposal="New Order SO"+tranid+" - "+title;
                        var emailBodyProposal = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
                            'New Order : SO' + tranid + '\n' +
                            'Title : ' + title + '\n' +
                            'Thank you for submitting your order.  A Cologix employee will be contacting you soon regarding the installation of your services.\n\n' +
                            'Sincerely,\n' +
                            salesRepName + '\n\n';
                        if(languagePortal=='fr')
                        {
                            var emailSubjectProposal="Nouvelle Commande SO"+tranid+" - "+title;
                            var emailBodyProposal = 'Bonjour ' + custContactName + ' (' + custCompName + '),\n\n' +
                                'Nouvelle Commande :  SO'+tranid + '\n' +
                                'Titre : ' + title + '\n' +
                                "Nous vous remercions d'avoir soumis votre commande. Un employé Cologix vous contactera bientôt concernant l'installation de vos services.\n\n" +
                                'Au plaisir de vous être utile,\n' +
                                salesRepName + '\n\n';
                        }
                        nlapiSendEmail(salesRepID,contactid,emailSubjectProposal,emailBodyProposal,null,null,null,null,true);

                    }
                    catch (error) {
                        nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
                    }


                    if (recordIdPR != null) {
                        var totals = clgx_transaction_totals(companyid, 'proposal', recordIdPR);
                        var arrParam = new Array();
                        arrParam['custscript_ss_cp_order_id'] = recordIdPR;
                        arrParam['custscript_ss_cp_order_lan'] = languagePortal;
                        arrParam['custscript_ss_cp_order_title'] = title;
                        arrParam['custscript_ss_cp_order_company']=companyid;
                        arrParam['custscript_ss_cp_order_contact']=contactid;
                        var status = nlapiScheduleScript('customscript_clgx_ss_cp_order', null, arrParam);
                    }
                }
            }
            obj["id"] = recordId;
            obj["number"] =  nlapiLookupField('opportunity',recordId,'tranid');
            obj["type"] =  "opportunity";
            if(recordIdPR!=null)
            {

                var tranid = nlapiLookupField('estimate',recordIdPR,'tranid');
                obj["propid"]=tranid;
                obj["loa"]=loa;
                obj["id"] = recordIdPR;
                obj["number"] = tranid;
                obj["type"] =  "proposal";
            }


            var now = moment().format("M/D/YYYY h:mm:ss a");
            nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

            obj.code = 'SUCCESS';
            obj.msg = 'A new order has been added.';
        }

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_CASES';
        obj.msg = 'No rights for orders.';
    }

    return obj;

});


function get_opportunity(obj){

    var rec = nlapiLoadRecord('opportunity', obj.order.id);
    obj.order["node_en"] = rec.getFieldValue('tranid');
    obj.order["node_fr"] = rec.getFieldValue('tranid');
    obj.order["type"] = 'o';
    obj.order["faicon"] = 'fa fa-cogs';
    obj.order["leaf"] = false;

    var nbrItems = rec.getLineItemCount('item');
    var items = [];
    for (var i = 0; i < nbrItems; i++) {

        var arrName = (rec.getLineItemText('item', 'item', i + 1)).split(":");
        var arrLoc = (rec.getLineItemText('item', 'location', i + 1)).split(":");
        var location = arrLoc[arrLoc.length - 1];
        var memo = rec.getLineItemValue('item', 'memo', i + 1) || '';
        var category = rec.getLineItemValue('item', 'custcol_cologix_invoice_item_category', i + 1) || 0;

        var quantity = parseFloat(rec.getLineItemValue('item', 'quantity', i + 1));
        var rate = parseFloat(rec.getLineItemValue('item', 'rate', i + 1));
        var amount = parseFloat(rec.getLineItemValue('item', 'amount', i + 1));

        var itemid = rec.getLineItemValue('item', 'item', i + 1);
        var recItem = nlapiLoadRecord('serviceitem', itemid);
        var item_en = arrName[arrName.length - 1];
        var item_fr = item_en;
        var french = recItem.getLineItemValue('translations', 'displayname', 3);
        if (french){
            item_fr = french;
        }
        var item = {
            "node_en": item_en,
            "node_fr": item_fr,
            "type": "i",
            "category": category,
            "memo": memo,
            "quantity": quantity,
            "rate": rate,
            "amount": amount,
            "location": location,
            "faicon": 'fa fa-cogs',
            "leaf": true
        };
        items.push(item);
    }
    obj.order["children"] = items;

    return obj;
}

function get_proposal(obj){

    var rec = nlapiLoadRecord('estimate', obj.order.id);
    obj.order["node_en"] = rec.getFieldValue('tranid');
    obj.order["node_fr"] = rec.getFieldValue('tranid');
    obj.order["type"] = 'p';
    obj.order["faicon"] = 'fa fa-cogs';
    obj.order["leaf"] = false;

    var nbrItems = rec.getLineItemCount('item');
    var items = [];
    for (var i = 0; i < nbrItems; i++) {

        var arrName = (rec.getLineItemText('item', 'item', i + 1)).split(":");
        var arrLoc = (rec.getLineItemText('item', 'location', i + 1)).split(":");
        var location = arrLoc[arrLoc.length - 1];
        var memo = rec.getLineItemValue('item', 'memo', i + 1) || '';
        var category = rec.getLineItemValue('item', 'custcol_cologix_invoice_item_category', i + 1) || 0;

        var quantity = parseFloat(rec.getLineItemValue('item', 'quantity', i + 1));
        var rate = parseFloat(rec.getLineItemValue('item', 'rate', i + 1));
        var amount = parseFloat(rec.getLineItemValue('item', 'amount', i + 1));

        var itemid = rec.getLineItemValue('item', 'item', i + 1);
        var recItem = nlapiLoadRecord('serviceitem', itemid);
        var item_en = arrName[arrName.length - 1];
        var item_fr = item_en;
        var french = recItem.getLineItemValue('translations', 'displayname', 3);
        if (french){
            item_fr = french;
        }

        var item = {
            "node_en": item_en,
            "node_fr": item_fr,
            "type": "i",
            "category": category,
            "memo": memo,
            "quantity": quantity,
            "rate": rate,
            "amount": amount,
            "location": location,
            "faicon": 'fa fa-cogs',
            "leaf": true
        };
        items.push(item);
    }
    obj.order["children"] = items;

    return obj;
}

function get_salesorder(obj,radix){

    var rec = nlapiLoadRecord('salesorder', obj.order.id);
    obj.order["node_en"] = rec.getFieldValue('tranid');
    obj.order["node_fr"] = rec.getFieldValue('tranid');
    obj.order["type"] = 's';

    var legacydate = rec.getFieldValue('custbody_cologix_legacy_inst_dt') || '';
    var installdate = rec.getFieldValue('custbody_cologix_service_actl_instl_dt') || '';
    var opportunity=rec.getFieldValue('opportunity') || '';
    if(opportunity!='') {
        var title = nlapiLoadRecord('opportunity', opportunity).getFieldValue('title');
    }
    else{
        var title='';
    }
    var requestor1 =rec.getFieldValue('custbody_clgx_contract_terms_attention');
    var aetype=rec.getFieldText('custbodyclgx_aa_type');
    // if(requestor=='')
    // {
    var requestor=requestor1;
    // }
    obj.order["requestor"] =requestor;
    obj.order["title"] =title;
    obj.order["aetype"] =aetype;
    // }

    if(legacydate){
        obj.order["install"] = legacydate;
    } else {
        obj.order["install"] = installdate;
    }

    obj.order["start"] = rec.getFieldValue('custbody_cologix_so_contract_start_dat') || '';
    obj.order["end"] = rec.getFieldValue('enddate') || '';
    obj.order["terms"] = rec.getFieldValue('custbody_cologix_biling_terms') || '';
    obj.order["accelerator"] = rec.getFieldValue('custbody_cologix_annual_accelerator') || '';
    obj.order["legacy_id"] = rec.getFieldValue('custbody_cologix_legacy_so') || '';
    obj.order["expanded"] = true;
    obj.order["faicon"] = 'fa fa-cogs';
    obj.order["leaf"] = false;

    var nbrItems = rec.getLineItemCount('item');
    var items = [];
    for (var i = 0; i < nbrItems; i++) {

        var sched = rec.getLineItemValue('item', 'billingschedule', i + 1);
        var cls = rec.getLineItemText('item', 'class', i + 1);

        if(sched || cls.indexOf("NRC") > -1){

            var arrName = (rec.getLineItemText('item', 'item', i + 1)).split(":");
            var arrLoc = (rec.getLineItemText('item', 'location', i + 1)).split(":");
            var location = arrLoc[arrLoc.length - 1];
            var memo = rec.getLineItemValue('item', 'description', i + 1) || '';
            var category = rec.getLineItemValue('item', 'custcol_cologix_invoice_item_category', i + 1) || 0;

            var service = parseInt(rec.getLineItemValue('item', 'custcol_clgx_so_col_service_id', i + 1)) || 0;
            var quantity = parseFloat(rec.getLineItemValue('item', 'custcol_clgx_qty2print', i + 1));
            var rate = parseFloat(rec.getLineItemValue('item', 'rate', i + 1));
            var amount = quantity * rate;
            var itemid = rec.getLineItemValue('item', 'item', i + 1);
            try {
                var recItem = nlapiLoadRecord('serviceitem', itemid);
                var item_en = arrName[arrName.length - 1];
                var item_fr = item_en;
                var french = recItem.getLineItemValue('translations', 'displayname', 3);
                if (french){
                    item_fr = french;
                }
                var item = {
                    "node_en": item_en,
                    "node_fr": item_fr,
                    "type": "i",
                    "category": category,
                    "memo": memo,
                    "quantity": quantity,
                    "rate": rate,
                    "amount": amount,
                    "location": location,
                    "service": service,
                    "children": get_inventory(service,radix),
                    "faicon": 'fa fa-cog',
                    "leaf": false
                };
                items.push(item);
            }
            catch (error) {
            }
        }
    }
    obj.order["children"] = items;
    nlapiLogExecution('DEBUG','debug', '| obj = ' + JSON.stringify( obj ) + ' | ');
    return obj;
}

function get_inventory(service,radix){

    var children = [];
    if(service){

        var rec = nlapiLoadRecord('job', service);
        var columns = new Array();
        columns.push(new nlobjSearchColumn('internalid',null,null));
        columns.push(new nlobjSearchColumn('name',null,null).setSort(false));
        var filters = new Array();
        filters.push(new nlobjSearchFilter("custrecord_cologix_power_service",null,"anyof",service));
        var search = nlapiSearchRecord('customrecord_clgx_power_circuit', null, filters, columns);
        for ( var i = 0; search != null && i < search.length; i++ ) {
            var intid = parseInt(search[i].getValue('internalid',null,null)) || 0;
            var name = search[i].getValue('name',null,null) || '';
            var child = {
                //"id": intid,
                "type": "power",
                "rid": intid.toString(radix),
                "node_en": name,
                "node_fr": name,
                "faicon": 'fa fa-plug',
                "leaf": true
            };
            children.push(child);
        }

        var columns = new Array();
        columns.push(new nlobjSearchColumn('internalid',null,null));
        columns.push(new nlobjSearchColumn('name',null,null).setSort(false));
        var filters = new Array();
        filters.push(new nlobjSearchFilter("custrecord_cologix_space_project",null,"anyof",service));
        var search = nlapiSearchRecord('customrecord_cologix_space', null, filters, columns);
        for ( var i = 0; search != null && i < search.length; i++ ) {
            var intid = parseInt(search[i].getValue('internalid',null,null)) || 0;
            var name = search[i].getValue('name',null,null) || '';
            var child = {
                //"id": intid,
                "type": "space",
                "rid": intid.toString(radix),
                "node_en": name,
                "node_fr": name,
                "faicon": 'fa fa-map-o',
                "leaf": true
            };
            children.push(child);
        }

        var columns = new Array();
        columns.push(new nlobjSearchColumn('internalid',null,null));
        columns.push(new nlobjSearchColumn('name',null,null).setSort(false));
        columns.push(new nlobjSearchColumn('custrecord_cologix_xc_type',null,null));
        var filters = new Array();
        filters.push(new nlobjSearchFilter("custrecord_cologix_xc_service",null,"anyof",service));
        var search = nlapiSearchRecord('customrecord_cologix_crossconnect', null, filters, columns);
        for ( var i = 0; search != null && i < search.length; i++ ) {

            var intid = parseInt(search[i].getValue('internalid',null,null)) || 0;
            var name = search[i].getValue('name',null,null) || '';
            var typeid = parseInt(search[i].getValue('custrecord_cologix_xc_type',null,null)) || 0;

            if(typeid == 19 || typeid == 23 || typeid == 24){
                var type = 'active';
                var faicon = 'fa fa-share-alt-square';
            }
            else if(typeid == 28 || typeid == 29 || typeid == 30 || typeid == 31){
                var type = 'cloud';
                var faicon = 'fa fa-cloud-upload';
            }
            else if(typeid == 20 || typeid == 21 || typeid == 22){
                var type = 'metro';
                var faicon = 'fa fa-share-alt';
            }
            else{
                var type = 'xc';
                var faicon = 'fa fa-share-alt-square';
            }
            var child = {
                //"id": intid,
                "typeid": typeid,
                "type": type,
                "rid": intid.toString(radix),
                "node_en": name,
                "node_fr": name,
                "faicon": faicon,
                "leaf": true
            };
            children.push(child);
        }
    }
    return children;
}