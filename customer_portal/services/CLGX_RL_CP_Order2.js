nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Order.js
//	ScriptID:	customscript_clgx_rl_cp_order
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=676&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/17/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    if(srights.orders > 0){

        var rid = datain['rid'] || "0";
        var id = parseInt(rid, radix);

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('type',null,'GROUP'));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("internalid",null,"is",id));
        var searchType = nlapiSearchRecord('transaction', null, arrFilters, arrColumns);

        var trantype = searchType[0].getValue('type',null,'GROUP');

        //nlapiLogExecution('DEBUG','id', id);

        obj["order"] = {"id":id,"rid":rid};
        if(trantype == 'Opprtnty'){
            obj = get_opportunity(obj);
        }
        if(trantype == 'Estimate'){
            obj = get_proposal(obj);
        }
        if(trantype == 'SalesOrd'){
            obj = get_salesorder(obj,radix);
        }

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing invoices.';

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_INVOICES';
        obj.msg = 'No rights for invoices.';
    }

    return obj;

});


var put = wrap(function put(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    if(srights.orders > 0){

        var title= datain['title'] || "";
        var facility = datain['facility'] || '0';
        var spaceid = datain['spaceid'] || "";
        var afacility = datain['afacility'] || "0";
        var tfacility = datain['tfacility'] || "";
        var metrotype = datain['metrotype'] || "";
        var conn_type = datain['conn_type'] || "";
        var speed = datain['speed'] || "";
        var loa=datain['loa']||'0';
        var billing=datain['billing']||'0';
        var requested=datain['requested']||"";
        var qty=datain['qty']||"";
        var rate=datain['rate']||"";
        var totalmrc=datain['totalmrc']||"";
        var ratecard=datain['ratecard']||"";
        var other=datain['other']||'0';
        var item=datain['item']||'0';
        var languagePortal=datain['lanportal']||'en';
        var notes=datain['notes']||"";
        var po=datain['po']||"";
        var server=datain['server']||"";
        nlapiLogExecution('DEBUG','LOA',loa);
        nlapiLogExecution('DEBUG','REQUESTED',requested);


        //schedule script opportunity+SO
        var arrParam = new Array();
        arrParam['custscript_ss_cp_order_lan1'] = languagePortal;
        arrParam['custscript_ss_cp_order_title1'] = title;
        arrParam['custscript_ss_cp_order_company1']=companyid;
        arrParam['custscript_ss_cp_order_location']=facility;
        arrParam['custscript_clgx_target_install_date']=requested;
        arrParam['custscript_ss_cp_order_item']=item;
        arrParam['custscript_ss_cp_order_quantity']=qty;
        arrParam['custscript_ss_cp_order_rate']=rate;
        arrParam['custscript_ss_cp_order_amount']=totalmrc;

        arrParam['custscript_ss_cp_order_contactid']=contactid;
        arrParam['custscript_ss_cp_order_spaceid']=spaceid;
        arrParam['custscript_ss_cp_order_afacility']=afacility;
        arrParam['custscript_ss_cp_order_tfacility']=tfacility;
        arrParam['custscript_ss_cp_order_metrotype']=metrotype;
        arrParam['custscript_ss_cp_order_conn_type']=conn_type;
        arrParam['custscript_ss_cp_order_speed']=speed;
        arrParam['custscript_ss_cp_order_loa']=loa;
        arrParam['custscript_ss_cp_order_billing']=billing;
        arrParam['custscript_ss_cp_order_ratecard']=ratecard;
        arrParam['custscript_ss_cp_order_other']=other;
        arrParam['custscript_ss_cp_order_notes']=notes;
        arrParam['custscript_ss_cp_order_po']=po;
        arrParam['custscript_ss_cp_order_server']=server;

        var status = nlapiScheduleScript('customscript_clgx_ss_cp_order1', null, arrParam);



        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'Your order has been processed in the system. You will soon receive an email requesting your signature on the order form. The Service Order will be generated once you have signed the document.';
        if(languagePortal!='en'){
            obj.msg = 'Votre commande a été traitée dans le système. Vous recevrez bientôt un courriel demandant votre signature sur le formulaire de commande. L&#39;ordre de service sera généré une fois que vous avez signé le document.';

        }
    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_Orders';
        obj.msg = 'No rights for orders.';
    }

    return obj;

});

function get_opportunity(obj){

    var rec = nlapiLoadRecord('opportunity', obj.order.id);
    obj.order["node_en"] = rec.getFieldValue('tranid');
    obj.order["node_fr"] = rec.getFieldValue('tranid');
    obj.order["type"] = 'o';
    obj.order["faicon"] = 'fa fa-cogs';
    obj.order["leaf"] = false;

    var nbrItems = rec.getLineItemCount('item');
    var items = [];
    for (var i = 0; i < nbrItems; i++) {

        var arrName = (rec.getLineItemText('item', 'item', i + 1)).split(":");
        var arrLoc = (rec.getLineItemText('item', 'location', i + 1)).split(":");
        var location = arrLoc[arrLoc.length - 1];
        var memo = rec.getLineItemValue('item', 'memo', i + 1) || '';
        var category = rec.getLineItemValue('item', 'custcol_cologix_invoice_item_category', i + 1) || 0;

        var quantity = parseFloat(rec.getLineItemValue('item', 'quantity', i + 1));
        var rate = parseFloat(rec.getLineItemValue('item', 'rate', i + 1));
        var amount = parseFloat(rec.getLineItemValue('item', 'amount', i + 1));

        var itemid = rec.getLineItemValue('item', 'item', i + 1);
        var recItem = nlapiLoadRecord('serviceitem', itemid);
        var item_en = arrName[arrName.length - 1];
        var item_fr = item_en;
        var french = recItem.getLineItemValue('translations', 'displayname', 3);
        if (french){
            item_fr = french;
        }
        var item = {
            "node_en": item_en,
            "node_fr": item_fr,
            "type": "i",
            "category": category,
            "memo": memo,
            "quantity": quantity,
            "rate": rate,
            "amount": amount,
            "location": location,
            "faicon": 'fa fa-cogs',
            "leaf": true
        };
        items.push(item);
    }
    obj.order["children"] = items;

    return obj;
}

function get_proposal(obj){

    var rec = nlapiLoadRecord('estimate', obj.order.id);
    obj.order["node_en"] = rec.getFieldValue('tranid');
    obj.order["node_fr"] = rec.getFieldValue('tranid');
    obj.order["type"] = 'p';
    obj.order["faicon"] = 'fa fa-cogs';
    obj.order["leaf"] = false;

    var nbrItems = rec.getLineItemCount('item');
    var items = [];
    for (var i = 0; i < nbrItems; i++) {

        var arrName = (rec.getLineItemText('item', 'item', i + 1)).split(":");
        var arrLoc = (rec.getLineItemText('item', 'location', i + 1)).split(":");
        var location = arrLoc[arrLoc.length - 1];
        var memo = rec.getLineItemValue('item', 'memo', i + 1) || '';
        var category = rec.getLineItemValue('item', 'custcol_cologix_invoice_item_category', i + 1) || 0;

        var quantity = parseFloat(rec.getLineItemValue('item', 'quantity', i + 1));
        var rate = parseFloat(rec.getLineItemValue('item', 'rate', i + 1));
        var amount = parseFloat(rec.getLineItemValue('item', 'amount', i + 1));

        var itemid = rec.getLineItemValue('item', 'item', i + 1);
        var recItem = nlapiLoadRecord('serviceitem', itemid);
        var item_en = arrName[arrName.length - 1];
        var item_fr = item_en;
        var french = recItem.getLineItemValue('translations', 'displayname', 3);
        if (french){
            item_fr = french;
        }

        var item = {
            "node_en": item_en,
            "node_fr": item_fr,
            "type": "i",
            "category": category,
            "memo": memo,
            "quantity": quantity,
            "rate": rate,
            "amount": amount,
            "location": location,
            "faicon": 'fa fa-cogs',
            "leaf": true
        };
        items.push(item);
    }
    obj.order["children"] = items;

    return obj;
}

function get_salesorder(obj,radix){

    var rec = nlapiLoadRecord('salesorder', obj.order.id);
    obj.order["node_en"] = rec.getFieldValue('tranid');
    obj.order["node_fr"] = rec.getFieldValue('tranid');
    obj.order["type"] = 's';

    var legacydate = rec.getFieldValue('custbody_cologix_legacy_inst_dt') || '';
    var installdate = rec.getFieldValue('custbody_cologix_service_actl_instl_dt') || '';

    if(legacydate){
        obj.order["install"] = legacydate;
    } else {
        obj.order["install"] = installdate;
    }

    obj.order["start"] = rec.getFieldValue('custbody_cologix_so_contract_start_dat') || '';
    obj.order["end"] = rec.getFieldValue('enddate') || '';
    obj.order["terms"] = rec.getFieldValue('custbody_cologix_biling_terms') || '';
    obj.order["accelerator"] = rec.getFieldValue('custbody_cologix_annual_accelerator') || '';
    obj.order["legacy_id"] = rec.getFieldValue('custbody_cologix_legacy_so') || '';
    obj.order["expanded"] = true;
    obj.order["faicon"] = 'fa fa-cogs';
    obj.order["leaf"] = false;

    var nbrItems = rec.getLineItemCount('item');
    var items = [];
    for (var i = 0; i < nbrItems; i++) {

        var sched = rec.getLineItemValue('item', 'billingschedule', i + 1);
        var cls = rec.getLineItemText('item', 'class', i + 1);

        if(sched || cls.indexOf("NRC") > -1){

            var arrName = (rec.getLineItemText('item', 'item', i + 1)).split(":");
            var arrLoc = (rec.getLineItemText('item', 'location', i + 1)).split(":");
            var location = arrLoc[arrLoc.length - 1];
            var memo = rec.getLineItemValue('item', 'description', i + 1) || '';
            var category = rec.getLineItemValue('item', 'custcol_cologix_invoice_item_category', i + 1) || 0;

            var service = parseInt(rec.getLineItemValue('item', 'custcol_clgx_so_col_service_id', i + 1)) || 0;
            var quantity = parseFloat(rec.getLineItemValue('item', 'custcol_clgx_qty2print', i + 1));
            var rate = parseFloat(rec.getLineItemValue('item', 'rate', i + 1));
            var amount = quantity * rate;
            var itemid = rec.getLineItemValue('item', 'item', i + 1);
            try {
                var recItem = nlapiLoadRecord('serviceitem', itemid);
                var item_en = arrName[arrName.length - 1];
                var item_fr = item_en;
                var french = recItem.getLineItemValue('translations', 'displayname', 3);
                if (french){
                    item_fr = french;
                }
                var item = {
                    "node_en": item_en,
                    "node_fr": item_fr,
                    "type": "i",
                    "category": category,
                    "memo": memo,
                    "quantity": quantity,
                    "rate": rate,
                    "amount": amount,
                    "location": location,
                    "service": service,
                    "children": get_inventory(service,radix),
                    "faicon": 'fa fa-cog',
                    "leaf": false
                };
                items.push(item);
            }
            catch (error) {
            }
        }
    }
    obj.order["children"] = items;

    return obj;
}

function get_inventory(service,radix){

    var children = [];
    if(service){

        var rec = nlapiLoadRecord('job', service);
        var columns = new Array();
        columns.push(new nlobjSearchColumn('internalid',null,null));
        columns.push(new nlobjSearchColumn('name',null,null).setSort(false));
        var filters = new Array();
        filters.push(new nlobjSearchFilter("custrecord_cologix_power_service",null,"anyof",service));
        var search = nlapiSearchRecord('customrecord_clgx_power_circuit', null, filters, columns);
        for ( var i = 0; search != null && i < search.length; i++ ) {
            var intid = parseInt(search[i].getValue('internalid',null,null)) || 0;
            var name = search[i].getValue('name',null,null) || '';
            var child = {
                //"id": intid,
                "type": "power",
                "rid": intid.toString(radix),
                "node_en": name,
                "node_fr": name,
                "faicon": 'fa fa-plug',
                "leaf": true
            };
            children.push(child);
        }

        var columns = new Array();
        columns.push(new nlobjSearchColumn('internalid',null,null));
        columns.push(new nlobjSearchColumn('name',null,null).setSort(false));
        var filters = new Array();
        filters.push(new nlobjSearchFilter("custrecord_cologix_space_project",null,"anyof",service));
        var search = nlapiSearchRecord('customrecord_cologix_space', null, filters, columns);
        for ( var i = 0; search != null && i < search.length; i++ ) {
            var intid = parseInt(search[i].getValue('internalid',null,null)) || 0;
            var name = search[i].getValue('name',null,null) || '';
            var child = {
                //"id": intid,
                "type": "space",
                "rid": intid.toString(radix),
                "node_en": name,
                "node_fr": name,
                "faicon": 'fa fa-map-o',
                "leaf": true
            };
            children.push(child);
        }

        var columns = new Array();
        columns.push(new nlobjSearchColumn('internalid',null,null));
        columns.push(new nlobjSearchColumn('name',null,null).setSort(false));
        columns.push(new nlobjSearchColumn('custrecord_cologix_xc_type',null,null));
        var filters = new Array();
        filters.push(new nlobjSearchFilter("custrecord_cologix_xc_service",null,"anyof",service));
        var search = nlapiSearchRecord('customrecord_cologix_crossconnect', null, filters, columns);
        for ( var i = 0; search != null && i < search.length; i++ ) {

            var intid = parseInt(search[i].getValue('internalid',null,null)) || 0;
            var name = search[i].getValue('name',null,null) || '';
            var typeid = parseInt(search[i].getValue('custrecord_cologix_xc_type',null,null)) || 0;

            if(typeid == 19 || typeid == 23 || typeid == 24){
                var type = 'active';
                var faicon = 'fa fa-share-alt-square';
            }
            else if(typeid == 28 || typeid == 29 || typeid == 30 || typeid == 31){
                var type = 'cloud';
                var faicon = 'fa fa-cloud-upload';
            }
            else if(typeid == 20 || typeid == 21 || typeid == 22){
                var type = 'metro';
                var faicon = 'fa fa-share-alt';
            }
            else{
                var type = 'xc';
                var faicon = 'fa fa-share-alt-square';
            }
            var child = {
                //"id": intid,
                "typeid": typeid,
                "type": type,
                "rid": intid.toString(radix),
                "node_en": name,
                "node_fr": name,
                "faicon": faicon,
                "leaf": true
            };
            children.push(child);
        }
    }
    return children;
}