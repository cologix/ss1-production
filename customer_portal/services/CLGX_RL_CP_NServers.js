//------------------------------------------------------
//	Script:		CLGX_RL_CP_NServers.js
//	ScriptID:	customscript_clgx_rl_cp_nservers
//	ScriptType:	RESTlet
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	03/12/2018
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv) {
    var direction = datain['direction'] || 'ASC';
    var order_by = datain['order_by'] || '';
    var page = datain['page'] || 1;
    var per_page = datain['per_page'];
    var csv = datain['csv'] || 0;

    var getPorts=get_records(companyid,direction,order_by,page,per_page,csv,radix);
    return getPorts;

});
var put = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix) {
    var id = datain['rid'] || 0;
    var update = datain['update'] || 0;
    var note = datain['note'] || '';
    nlapiLogExecution('DEBUG', 'id record', id);
    if(update>0){
        var getPorts=update_record(parseInt(id),note);
    }
    else {
        var getPorts = get_record(parseInt(id), update, radix);
    }
    return getPorts;

});


function get_records(id,direction,order_by,page,per_page,csv,radix){
    nlapiLogExecution('DEBUG', 'id customer', id);
    var total = 0;
    var pages = 0;
    var start = 0;
    var end = 0;
    var has_more = false;
    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("name",'custrecord_clgx_ms_service_order',"anyof",id));
    var records = nlapiSearchRecord('customrecord_clgx_managed_services','customsearch5760', filters, columns);

    var obj=new Object();
    var arrObj=new Array();
    if(records!=null) {
        total = records.length;
        nlapiLogExecution('DEBUG', 'id', id);
        nlapiLogExecution('DEBUG', 'total1', total);
        nlapiLogExecution('DEBUG', 'per_page', per_page);
        pages = Math.ceil(total / per_page);
        page = (parseInt(page) > pages ? pages : parseInt(page));
        has_more = (pages > 1 ? true : false);
        start = (page - 1) * per_page;
        end = (start + per_page > total ? total : start + per_page);
        nlapiLogExecution('DEBUG', 'start', start);
        nlapiLogExecution('DEBUG', 'end', end);

        for ( var j = start; j < end; j++ ) {
            var searchAP = records[j];
            var columns = searchAP.getAllColumns();
            var id = searchAP.getValue(columns[0]);
            var name= searchAP.getValue(columns[1]);
            var type= searchAP.getText(columns[2]);
            var make = searchAP.getText(columns[3]);
            var  model = searchAP.getText(columns[4]);
            var internalid= parseInt(searchAP.getValue(columns[5]));

            var rec = new Object();
            rec.id = id;
            rec.name = name;
            rec.type = type;
            rec.make = make;
            rec.model = model;
            rec.internalid =internalid;
            arrObj.push(rec)

        }
        obj["data"] = arrObj;
        obj["direction"] = direction;
        obj["order_by"] = order_by;
        obj["page"] = page;
        obj["pages"] = pages;
        obj["per_page"] = per_page;
        obj["has_more"] = has_more;
        obj["start"] = start;
        obj["end"] = end;
        obj["total"] = total;
    }else{
        obj["data"] = [];
        obj["page"] = page;
        obj["pages"] = 1;
        obj["per_page"] = 16;
        obj["has_more"] =false;
        obj["start"] = 0;
        obj["end"] = 0;
        obj["total"] = 0;
    }
    return obj;
}

function get_record(id, update,radix){
    nlapiLogExecution('DEBUG', 'radix', radix);
    nlapiLogExecution('DEBUG', 'id', id);
    var obj=new Object();
    var record = nlapiLoadRecord('customrecord_clgx_managed_services',id);

    obj["id"] = record.getFieldValue('name');
    obj["internalid"] = parseInt(record.getFieldValue('id'));
    obj["name"] = record.getFieldValue('custrecord_clgx_ms_note');

    obj["type"] =record.getFieldText('custrecord_clgx_managed_services_type')||'';
    obj["ipmis"]=record.getFieldValue('custrecord_clgx_ms_ipmi')||'';
    obj["ipmisv"]=record.getFieldValue('custrecord_clgx_ms_ipmi_vpn')||'';
    var equipment=parseInt(record.getFieldValue('custrecord_clgx_ms_equipment'))||0;
    var equipmentRecord = nlapiLoadRecord('customrecord_cologix_equipment',equipment);
    obj["manufacturer"]=equipmentRecord.getFieldText('custrecord_clgx_equipment_make')||'';
    obj["model"]=equipmentRecord.getFieldText('custrecord_cologix_equipment_model')||'';



    var filters = new Array();
    var columns = new Array();
    filters.push(new nlobjSearchFilter("internalid",null,"anyof",equipment));
    var records = nlapiSearchRecord('customrecord_cologix_equipment','customsearch5775', filters, columns);

    var arrObj=new Array();
    if(records!=null) {


        for (var j = 0; j < records.length; j++) {
            var searchAP = records[j];
            var columns = searchAP.getAllColumns();
            var id = searchAP.getValue(columns[0])||'';
            var description = searchAP.getValue(columns[1])||'';

            var rec = new Object();


            rec.id = id;
            rec.description=description;

            arrObj.push(rec)

        }
    }
    obj["cpus"]=new Object();
    obj["cpus"]["data"] = arrObj;
    var filters = new Array();
    var columns = new Array();
    filters.push(new nlobjSearchFilter("internalid",null,"anyof",equipment));
    var records = nlapiSearchRecord('customrecord_cologix_equipment','customsearch5776', filters, columns);

    var arrObj=new Array();
    if(records!=null) {


        for (var j = 0; j < records.length; j++) {
            var searchAP = records[j];
            var columns = searchAP.getAllColumns();
            var id = searchAP.getValue(columns[0])||'';
            var description = searchAP.getValue(columns[1])||'';

            var rec = new Object();


            rec.id = id;
            rec.description=description;

            arrObj.push(rec)

        }
    }
    obj["disks"]=new Object();
    obj["disks"]["data"] = arrObj;

    var filters = new Array();
    var columns = new Array();
    filters.push(new nlobjSearchFilter("internalid",null,"anyof",equipment));
    var records = nlapiSearchRecord('customrecord_cologix_equipment','customsearch5777', filters, columns);

    var arrObj=new Array();
    if(records!=null) {
        for (var j = 0; j < records.length; j++) {
            var searchAP = records[j];
            var columns = searchAP.getAllColumns();
            var id = searchAP.getValue(columns[0])||'';
            var description = searchAP.getValue(columns[1])||'';

            var rec = new Object();
            rec.id = id;
            rec.description=description;

            arrObj.push(rec)

        }
    }
    obj["memory"]=new Object();
    obj["memory"]["data"] = arrObj;



    return obj;
}

function update_record(id,note){
    nlapiLogExecution('DEBUG', 'id', id);
    nlapiLogExecution('DEBUG', 'note', note);
    var obj=new Object();
    var record = nlapiLoadRecord('customrecord_clgx_managed_services',id);
    if(note!='') {
        //record.setFieldValue('custrecord_clgx_ms_note',note);
        //nlapiSubmitRecord(record, true,true);
    }

    obj["resp"] = "The record has been updated";
    return obj;
}