nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Space.js
//	ScriptID:	customscript_clgx_rl_cp_space
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=689&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	08/01/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

	var type = datain['type'] || "xc";
	var rid = datain['rid'] || "0";
	var id = parseInt(rid, radix);
		
	if(srights.colocation > 0){
		
		obj["space"] = {"id":id,"rid":rid};
		obj = get_space(obj,radix);
		
		var now = moment().format("M/D/YYYY h:mm:ss a");
		nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);
		
		obj.code = 'SUCCESS';
		obj.msg = 'You are viewing space.';
		
		nlapiLogExecution('DEBUG','debug', '| obj = ' + JSON.stringify( obj ) + ' | ');
	} else {
		obj.error = 'T';
		obj.code = 'NO_RIGHTS_SPACE';
		obj.msg = 'No rights for space.';
	}
	
	return obj;
	
});

function get_space(obj,radix){
	
	var rec = nlapiLoadRecord('customrecord_cologix_space', obj.space.id);

	var soid = parseInt(rec.getFieldValue('custrecord_space_service_order')) || 0;
	var name = (rec.getFieldText('custrecord_space_service_order')).split("#");
	var so = (name[name.length-1]).trim() || '';
		
	obj.space["number"] = rec.getFieldValue('name') || '';
	obj.space["sorid"] = soid.toString(radix);
	obj.space["so"] = so;
	obj.space["facility"] = rec.getFieldText('custrecord_cologix_space_location') || '';
	
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter("custrecord_cologix_power_space",null,"anyof",obj.space.id));
	var records = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_cp_powers', filters, columns);
	
	var arr = [];
	for ( var i = 0; records != null && i < records.length; i++ ) {
		
		var rec = new Object();
		
		var powerid = parseInt(records[i].getValue('internalid',null,null));
		rec["id"] = powerid;
		rec["rid"] = powerid.toString(radix);
		rec["number"] = records[i].getValue('name',null,null);
		rec["module"] = parseInt(records[i].getText('custrecord_cologix_power_upsbreaker',null,null)) || 0;
		rec["breaker"] = parseInt(records[i].getText('custrecord_cologix_power_circuitbreaker',null,null)) || 0;
		
		var soid = parseInt(records[i].getValue('custrecord_power_circuit_service_order',null,null)) || 0;
		rec["soid"] = soid;
		rec["rsoid"] = soid.toString(radix);
		var name = (records[i].getText('custrecord_power_circuit_service_order',null,null)).split("#");
		rec["so"] = (name[name.length-1]).trim();
		
		rec["voltsid"] = records[i].getValue('custrecord_cologix_power_volts',null,null) || '';
		rec["volts"] = records[i].getText('custrecord_cologix_power_volts',null,null) || '';
		
		rec["ampsid"] = records[i].getValue('custrecord_cologix_power_amps',null,null) || '';
		rec["amps"] = records[i].getText('custrecord_cologix_power_amps',null,null) || '';
		
		var strAmps = (records[i].getText('custrecord_cologix_power_amps',null,null)).slice(0, -1) || '0';
		var intAmps = parseInt(strAmps);
		rec["camps"] = intAmps;
		
		rec["facilityid"] = parseInt(records[i].getValue('custrecord_cologix_power_facility',null,null)) || 0;
		rec["facility"] = records[i].getText('custrecord_cologix_power_facility',null,null) || '';
		
		var spaceid =  parseInt(records[i].getValue('custrecord_cologix_power_space',null,null)) || 0;
		rec["spaceid"] = spaceid;
		rec["rspaceid"] = spaceid.toString(radix);
		rec["space"] = records[i].getText('custrecord_cologix_power_space',null,null) || '';
		
		var panelid =  parseInt(records[i].getValue('custrecord_clgx_dcim_device',null,null)) || 0;
		rec["panelid"] = panelid;
		rec["rpanelid"] = panelid.toString(radix);
		rec["panel"] = records[i].getText('custrecord_clgx_dcim_device',null,null) || '';
		
		rec["typeid"] = parseInt(records[i].getValue('custrecord_clgx_panel_type','CUSTRECORD_CLGX_POWER_PANEL_PDPM',null)) || 0;
		rec["type"] = records[i].getText('custrecord_clgx_panel_type','CUSTRECORD_CLGX_POWER_PANEL_PDPM',null) || '';
		
		var phasea = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_a_a',null,null)) || 0;
		var phaseb = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_a_b',null,null)) || 0;
		var phasec = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_a_c',null,null)) || 0;
		rec["phasea"] = phasea;
		rec["phaseb"] = phaseb;
		rec["phasec"] = phasec;
		
		var phasea2 = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_ab_a',null,null)) || 0;
		var phaseb2 = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_ab_b',null,null)) || 0;
		var phasec2 = parseFloat(records[i].getValue('custrecord_clgx_dcim_sum_day_amp_ab_c',null,null)) || 0;
		var maxamps = Math.max(phasea2,phaseb2,phasec2);
		rec["phasea2"] = phasea2;
		rec["phaseb2"] = phaseb2;
		rec["phasec2"] = phasec2;
		rec["maxamps"] = maxamps;
		
		rec["utilization"] = maxamps * 100 / intAmps;
		
		arr.push(rec);
	}
	obj.space["powers"] = arr;
	
	
	
	return obj;
}
