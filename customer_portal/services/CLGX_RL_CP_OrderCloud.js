nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_OrderCloud.js
//	ScriptID:	customscript_clgx_rl_cp_ordercloud
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=676&deploy=1
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	04/19/2017
//------------------------------------------------------



var put = wrap(function put(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    if(srights.orders > 0){

        var title= datain['title'] || "";
        var facility = datain['facility'] || '';
        var port_name = datain['port_name'] || "";
        var tfacility = datain['tfacility'] || "";
        var providerName = datain['providerName'] || "";
        nlapiLogExecution('DEBUG','facility ',facility);
        nlapiLogExecution('DEBUG','providerName',providerName);
        var conn_type = datain['conn_type'] || "";
        var speedName = datain['speedName'] || "";
        nlapiLogExecution('DEBUG','speedName',speedName);
        var billing=datain['billing']||'0';
        var requested=datain['requested']||"";
        var qty=datain['qty']||"";
        var rate=datain['rate']||"";
        var totalmrc=datain['totalmrc']||"";
        var totalnrc=datain['totalnrc']||0;
        var service_id=datain['service_id']||"";
        var stag=datain['stag']||"";
        var ratecard=datain['ratecard']||"";
        var evc=datain['evc']||'0';
        var item=datain['item']||'0';
        var languagePortal=datain['lanportal']||'en';
        var notes=datain['notes']||"";
        var po=datain['po']||"";
        var server=datain['server']||"";
        var requestor=datain['requestor']|| "";
        var boxfolder=datain['boxfolder']||"";
        var uname=datain['uname']||"";
        var timestamp=moment().format("YYYY-MM-DD HH:mm:ss  A Z");

        //schedule script opportunity+SO
        var arrParam = new Array();
        arrParam['custscript_ss_cp_order_lanc'] = languagePortal;
        arrParam['custscript_ss_cp_order_titlec'] = title;
        arrParam['custscript_ss_cp_order_companyc']=companyid;
        arrParam['custscript_ss_cp_order_locc']=facility;
        arrParam['custscript_ss_cp_port_namec']=port_name;
        arrParam['custscript_ss_cp_order_itemc']=item;
        arrParam['custscript_ss_cp_order_qtyc']=qty;
        arrParam['custscript_ss_cp_order_ratec']=rate;
        arrParam['custscript_ss_cp_order_amountc']=totalmrc;
        arrParam['custscript_ss_cp_order_totalnrc']=totalnrc;
        arrParam['custscript_ss_cp_order_contactc']=contactid;
        arrParam['custscript_ss_cp_order_tfacc']=tfacility;
        arrParam['custscript_ss_cp_order_provc']=providerName;
        arrParam['custscript_ss_cp_order_conn_tyc']=conn_type;
        arrParam['custscript_ss_cp_order_speednamec']=speedName;
        arrParam['custscript_ss_cp_order_billingc']=billing;
        arrParam['custscript_ss_cp_order_ratecarc']=ratecard;
        arrParam['custscript_ss_cp_order_evcc']=evc;
        arrParam['custscript_ss_cp_order_service_idc']=service_id;
        arrParam['custscript_ss_cp_order_stag']=stag;
        arrParam['custscript_ss_cp_order_notesc']=notes;
        arrParam['custscript_ss_cp_order_poc']=po;
        arrParam['custscript_ss_cp_order_serverc']=server;
        arrParam['custscript_ss_cp_order_reqc']=requestor;
        arrParam['custscript_ss_cp_order_boxfc']=boxfolder;
        arrParam['custscript_ss_cp_order_unamec']=uname;
        arrParam['custscript_ss_cp_order_timestampc']=timestamp;


        var status = nlapiScheduleScript('customscript_clgx_ss_cp_ordercloud', null, arrParam);



        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        if(ratecard==0) {
            obj.msg = 'Thank you for submitting your order.  You will receive an email update regarding the status of your order.';
            if (languagePortal != 'en') {
                obj.msg = 'Nous vous remercions d&#39;avoir envoy&eacute; votre commande. Vous recevrez une mise &agrave; jour par courriel concernant le statut de votre commande.';
                
            }
        }
        else{
            obj.msg = 'Thank you for submitting your order.  You will receive an email update regarding the status of your order.';
            if (languagePortal != 'en') {
                obj.msg = 'Nous vous remercions d&#39;avoir envoy&eacute; votre commande. Vous recevrez une mise &agrave; jour par courriel concernant le statut de votre commande.';

            }
        }
    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_Orders';
        obj.msg = 'No rights for orders.';
    }

    return obj;

});