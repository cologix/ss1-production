nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Clouds.js
//	ScriptID:	customscript_clgx_rl_cp_clouds
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=660&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/17/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv) {

    //if(srights.xcs > 0){
    if(srights.colocation > 0 || srights.network > 0){

        var direction = datain['direction'] || 'ASC';
        var order_by = datain['order_by'] || '';
        var page = datain['page'] || 1;
        var per_page = datain['per_page'];
        var stype = datain['stype'];
        var search = datain['search'] || '';
        var csv = datain['csv'] || 0;

        obj["xcs"] = get_xcs(companyid,contactid,modifierid,radix,direction,order_by,page,per_page,stype,search,csv);

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing xcs.';

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_XCS';
        obj.msg = 'No rights for xcs.';
    }

    return obj;

});

function get_xcs(companyid,contactid,modifierid,radix,direction,order_by,page,per_page,stype,search,csv) {

    var obj = {};
    var arr = [];
    var total = 0;
    var pages = 0;
    var start = 0;
    var end = 0;
    var has_more = false;

    var facilities = [{"value": 0, "text": '*'}];
    var facilitiesids = [];



    var sos = [{"value": 0, "text": '*'}];
    var sosids = [];

    var providers = [{"value": 0, "text": '*'}];
    var providersids = [];

    search = JSON.parse(search);

    var columns0 = new Array();
    var filters0 = new Array();
    var arrXCIDs=new Array();
    filters0.push(new nlobjSearchFilter("name", "custrecord_xconnect_service_order", "anyof", companyid));
    var records1 = nlapiSearchRecord('customrecord_cologix_crossconnect', 'customsearch_clgx_api_xcs_clouds_3', filters0, columns0);
    if (records1) {
        for (var i = 0; records1 != null && i < records1.length; i++) {
            var row = records1[i];
            var columns = row.getAllColumns();
            var soid = parseInt(row.getValue(columns[2])) || 0;
            var name = (row.getText(columns[2])).split("#");
            var xcid = parseInt(row.getValue(columns[0]));
            arrXCIDs.push(xcid);
            var so = (name[name.length - 1]).trim() || '';
            if (_.indexOf(sosids, soid) == -1 && soid) {
                sosids.push(soid);
                sos.push({"value": soid, "text": so});
            }
            sos = _.sortBy(sos, function (obj) {
                return obj.value;
            });
            var facilityid = parseInt(row.getValue(columns[3])) || 0;
            var facility = row.getText(columns[3]) || '';
            if (_.indexOf(facilitiesids, facilityid) == -1 && facilityid) {
                facilitiesids.push(facilityid);
                facilities.push({"value": facilityid, "text": facility});
            }
            facilities = _.sortBy(facilities, function (obj) {
                return obj.value;
            });

        }
    }
    if(arrXCIDs.length>0) {
        var columns00 = new Array();
        var filters00 = new Array();
        filters00.push(new nlobjSearchFilter("custrecord_clgx_cloud_connect_port", null, "anyof", arrXCIDs));
        var vxcSS = nlapiSearchRecord('customrecord_cologix_vxc', 'customsearch_clgx_api_vxcs_clouds', filters00, columns00);
        if (vxcSS) {
            for (var j = 0; vxcSS != null && j < vxcSS.length; j++) {
                var row = vxcSS[j];
                var columns = row.getAllColumns();
                var providerid = parseInt(row.getValue(columns[4])) || 0;
                var provider = row.getText(columns[4]) || '';
                if (_.indexOf(providersids, providerid) == -1 && providerid) {
                    providersids.push(providerid);
                    providers.push({"value": providerid, "text": provider});
                }
                providers = _.sortBy(providers, function (obj) {
                    return obj.value;
                });

            }

        }
        var columns = new Array();
        var filters = new Array();
        if (search) {
            if (search.id) {
                var intid = parseInt((search.id).substr(2), 10);
                filters.push(new nlobjSearchFilter("internalid", null, "is", intid));
            }
            if (search.sos) {
                filters.push(new nlobjSearchFilter("custrecord_xconnect_service_order", null, "anyof", search.sos));
            }
            if (search.facilities) {
                filters.push(new nlobjSearchFilter("custrecord_clgx_xc_facility", null, "anyof", search.facilities));
            }
            if (search.pName) {
                filters.push(new nlobjSearchFilter("custrecord_clgx_port_id", null, "contains", search.pName));

            }
        }

        var columns1 = new Array();
        var filters1 = new Array();
        if (search && (search.providers || search.skey)) {
            if (search.skey) {
                filters1.push(new nlobjSearchFilter("custrecord_clgx_vxc_skey", null, "is", search.skey));
            }
            nlapiLogExecution('DEBUG', 'providers', search.providers);
            if (search.providers) {
                filters1.push(new nlobjSearchFilter("custrecord_clgx_cloud_provider", null, "anyof", search.providers));
            }

            filters1.push(new nlobjSearchFilter("name", "custrecord_cologix_service_order", "anyof", companyid));
            var records = nlapiSearchRecord('customrecord_cologix_vxc', 'customsearch_clgx_api_vxcs_clouds', filters1, columns1);
            if (records) {
                for (var j = 0; records != null && j < records.length; j++) {
                    var rec = new Object();
                    var row = records[j];
                    var columns = row.getAllColumns();
                    var xcid = parseInt(row.getValue(columns[1]));
                    rec["id"] = xcid;
                    rec["rid"] = xcid.toString(radix);
                    rec["number"] = row.getValue(columns[0]);
                    var soid = parseInt(row.getValue(columns[5])) || 0;
                    rec["soid"] = soid;
                    rec["rsoid"] = soid.toString(radix);
                    var name = (row.getText(columns[5])).split("#");
                    rec["so"] = (name[name.length - 1]).trim();
                    rec["facilityid"] = 0;
                    rec["facility"] = '';
                    rec["vlan"] = row.getValue(columns[2]) || '';
                    rec["speed"] = row.getText(columns[6]) || '';
                    rec["status"] = '';
                    rec["pName"] = '';
                    rec["skey"] = row.getValue(columns[3]) || '';
                    rec["provider"] = row.getText(columns[4]) || '';
                    rec["rtype"] = 'vxc';
                    arr.push(rec);
                }

            }
        }
        else {
            filters.push(new nlobjSearchFilter("name", "custrecord_xconnect_service_order", "anyof", companyid));
            var totalvxc = 0;
            var records = nlapiSearchRecord('customrecord_cologix_crossconnect', 'customsearch_clgx_api_xcs_clouds_3', filters, columns);
            if (records) {
                //nlapiLogExecution('DEBUG','test Here RECORDS', records.length);
                for (var i = 0; records != null && i < records.length; i++) {
                    var rec = new Object();
                    var row = records[i];
                    var columns = row.getAllColumns();
                    var xcid = parseInt(row.getValue(columns[0]));
                    rec["id"] = xcid;
                    rec["rid"] = xcid.toString(radix);
                    rec["number"] = row.getValue(columns[1]);
                    var soid = parseInt(row.getValue(columns[2])) || 0;
                    rec["soid"] = soid;
                    rec["rsoid"] = soid.toString(radix);
                    var name = (row.getText(columns[2])).split("#");
                    rec["so"] = (name[name.length - 1]).trim();
                    var facilityid = parseInt(row.getValue(columns[3])) || 0;
                    var facility = row.getText(columns[3]) || '';
                    rec["facilityid"] = facilityid;
                    rec["facility"] = facility;
                    rec["speed"] = row.getText(columns[4]) || '';
                    rec["vlan"] = row.getValue(columns[5]) || '';
                    rec["status"] = row.getText(columns[6]) || '';
                    rec["pName"] = row.getValue(columns[7]) || '';
                    rec["rtype"] = 'xc';
                    rec["skey"] = '';
                    rec["provider"] = '';
                    arr.push(rec);
                    var columns1 = new Array();
                    var filters1 = new Array();
                    filters1.push(new nlobjSearchFilter("custrecord_clgx_cloud_connect_port", null, "anyof", xcid));
                    var vxcSS = nlapiSearchRecord('customrecord_cologix_vxc', 'customsearch_clgx_api_vxcs_clouds', filters1, columns1);
                    if (vxcSS) {
                        //nlapiLogExecution('DEBUG','test Here', vxcSS.length);
                        for (var j = 0; vxcSS != null && j < vxcSS.length; j++) {
                            totalvxc++;
                            var rec = new Object();
                            var row = vxcSS[j];
                            var columns = row.getAllColumns();
                            var xcid = parseInt(row.getValue(columns[1]));
                            rec["id"] = xcid;
                            rec["rid"] = xcid.toString(radix);
                            rec["number"] = row.getValue(columns[0]);
                            var soid = parseInt(row.getValue(columns[5])) || 0;
                            rec["soid"] = soid;
                            rec["rsoid"] = soid.toString(radix);
                            var name = (row.getText(columns[5])).split("#");
                            rec["so"] = (name[name.length - 1]).trim();
                            rec["facilityid"] = 0;
                            rec["facility"] = '';
                            rec["vlan"] = row.getValue(columns[2]) || '';
                            rec["speed"] = row.getText(columns[6]) || '';
                            rec["status"] = '';
                            rec["pName"] = '';
                            rec["skey"] = row.getValue(columns[3]) || '';
                            rec["provider"] = row.getText(columns[4]) || '';
                            rec["rtype"] = 'vxc';
                            arr.push(rec);
                        }

                    }
                }
            }
        }
    }
    var newArray=new Array();
        if (records) {

            total = parseInt(records.length);
            nlapiLogExecution('DEBUG','total0', total);
            if (totalvxc) {
                total  =parseInt(total)+ parseInt(totalvxc);
            }
            nlapiLogExecution('DEBUG','per_page', per_page);
            nlapiLogExecution('DEBUG','total', total);
           // nlapiLogExecution('DEBUG','total', parseInt(vxcSS.length));
            pages = Math.ceil(total / per_page);
            nlapiLogExecution('DEBUG','pages', pages);
            page = (parseInt(page) > pages ? pages : parseInt(page));
            has_more = (pages > 1 ? true : false);
            start = (page - 1) * per_page;
            end = (start + per_page > total ? total : start + per_page);
            var newArray = arr.slice(start, end);

        }
        obj["data"] = newArray;
        if (csv) {
            obj["csv"] = get_xcs_csv(companyid, contactid, modifierid, radix, direction, order_by, page, per_page, stype, search, csv);
        }
        obj["search"] = search;
        obj["lists"] = { "sos": sos, "providers": providers, "facilities": facilities};
        obj["direction"] = direction;
        obj["order_by"] = order_by;
        obj["page"] = page;
        obj["pages"] = pages;
        obj["per_page"] = per_page;
        obj["has_more"] = has_more;
        obj["start"] = start;
        obj["end"] = end;
        obj["total"] = total;

        return obj;

}

function get_xcs_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,stype,search,csv){

    var text = 'Facility,Port Name,ID,Service Order,Cloud Provider,Service ID/S-Key,Speed,VLAN\n';


    var columns1 = new Array();
    var filters1 = new Array();
    if (search && (search.providers || search.skey)) {
        if (search.skey) {
            filters1.push(new nlobjSearchFilter("custrecord_clgx_vxc_skey", null, "is", search.skey));
        }

        if (search.providers) {
            filters1.push(new nlobjSearchFilter("custrecord_clgx_cloud_provider", null, "anyof", search.providers));
        }
        filters1.push(new nlobjSearchFilter("name", "custrecord_cologix_service_order", "anyof", companyid));
        var records = nlapiSearchRecord('customrecord_cologix_vxc', 'customsearch_clgx_api_vxcs_clouds', filters1, columns1);
        if (records) {
                for (var j = 0; records != null && j < records.length; j++) {
                    var row = records[j];
                    var columns = row.getAllColumns();
                    text += '' + ',';
                    text += '' + ',';
                    text += row.getValue(columns[0]) + ',';
                    var name = (row.getText(columns[5])).split("#");
                    text += (name[name.length - 1]).trim() + ',';
                    text += row.getText(columns[4]) + ',';
                    text += row.getValue(columns[3]) + ',';
                    text += row.getText(columns[6]) + ',';
                    text += row.getValue(columns[2]) + ',';
                    text += '\n';
                }

        }
    }
    else {
        var columns = new Array();
        var filters = new Array();
        if(search){
            if(search.id){
                var intid = parseInt((search.id).substr(2), 10);
                filters.push(new nlobjSearchFilter("internalid",null,"is",intid));
            }
            if(search.sos){
                filters.push(new nlobjSearchFilter("custrecord_xconnect_service_order",null,"anyof",search.sos));
            }
            if(search.facilities){
                filters.push(new nlobjSearchFilter("custrecord_clgx_xc_facility",null,"anyof",search.facilities));
            }
            if(search.pName){
                filters.push(new nlobjSearchFilter("custrecord_clgx_port_id", null, "is", search.pName));

            }
        }
        filters.push(new nlobjSearchFilter("name", "custrecord_xconnect_service_order", "anyof", companyid));
        var records = nlapiSearchRecord('customrecord_cologix_crossconnect', 'customsearch_clgx_api_xcs_clouds_3', filters, columns);
        if (records) {
            for (var i = 0; records != null && i < records.length; i++) {
                var row = records[i];
                var columns = row.getAllColumns();
                var xcid = parseInt(row.getValue(columns[0]));
                text += row.getText(columns[3]) + ',';
                text += row.getValue(columns[7]) + ',';
                text += row.getValue(columns[1]) + ',';
                var name = (row.getText(columns[5])).split("#");
                text += (name[name.length - 1]).trim() + ',';
                text += row.getValue(columns[1]) + ',';
                text += '' + ',';
                text += '' + ',';
                text += row.getText(columns[4]) + ',';
                text += row.getValue(columns[5]) + ',';
                text += '\n';
                var columns1 = new Array();
                var filters1 = new Array();
                filters1.push(new nlobjSearchFilter("custrecord_clgx_cloud_connect_port", null, "anyof", xcid));
                var vxcSS = nlapiSearchRecord('customrecord_cologix_vxc', 'customsearch_clgx_api_vxcs_clouds', filters1, columns1);
                if (vxcSS) {
                    for (var j = 0; vxcSS != null && j < vxcSS.length; j++) {
                        var row = vxcSS[j];
                        var columns = row.getAllColumns();
                        text += '' + ',';
                        text += '' + ',';
                        text += row.getValue(columns[0]) + ',';
                        var name = (row.getText(columns[5])).split("#");
                        text += (name[name.length - 1]).trim() + ',';
                        text += row.getText(columns[4]) + ',';
                        text += row.getValue(columns[3]) + ',';
                        text += row.getText(columns[6]) + ',';
                        text += row.getValue(columns[2]) + ',';
                        text += '\n';
                    }

                }
            }
        }
    }
    return text;
}