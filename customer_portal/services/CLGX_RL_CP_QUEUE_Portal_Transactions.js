nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_QUEUE_Portal_Transactions.js
//	ScriptID:	customscript_clgx_RL_CP_QUEUE_Portal_Tr
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=708&deploy=1
//	@authors:	Catalina Taran
//	Created:	10/04/2016
//------------------------------------------------------

function get(datain) {
    var rid = datain['rid'] || " ";
    var arrColumns = new Array();
    var arrFilters = new Array();
    var objArray=new Array();
    // if(rid!='')
    // {
    //    arrFilters.push(new nlobjSearchFilter("internalid",null,"is",rid));
    // }
    var searchTransactions = nlapiSearchRecord('customrecord_clgx_cp_q_transactions', 'customsearch_clgx_cp_tq_ss', arrFilters, arrColumns);
    for ( var i = 0; searchTransactions != null && i < searchTransactions.length; i++ ) {
        var searchTr = searchTransactions[i];
        var columns = searchTr.getAllColumns();
        var customer=searchTr.getValue(columns[1]);
        var customerName = nlapiLookupField('customer',customer,'companyname');
        var pdfURL=searchTr.getValue(columns[5]);
        if(pdfURL!=null && pdfURL!='') {
            var arrURL = pdfURL.split("id=");
            var idFileArr = arrURL[1].split("&");
            var idFile = idFileArr[0];

            var pdf = nlapiLoadFile(idFile);
            var bodyFile = pdf.getValue();
        }
        else{
            var bodyFile='';
        }
        var obj = {"proposal":searchTr.getValue(columns[0]),
            "customer":customerName,
            "email":searchTr.getValue(columns[2]),
            "loa":searchTr.getValue(columns[3]),
            "pdfName":searchTr.getValue(columns[4]),
            "pdfURL":searchTr.getValue(columns[5]),
            "rate":searchTr.getValue(columns[6]),
            "boxFolder":searchTr.getValue(columns[7]),
            "prTransaction":searchTr.getValue(columns[9]),
            "prAgreement":searchTr.getValue(columns[10]),
            "id":searchTr.getValue(columns[11]),
            "cont":bodyFile,
            "agreementID":searchTr.getValue(columns[12])
        };
        objArray.push(obj);
    }



    return objArray;

}


