nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Ports.js
//	ScriptID:	customscript_clgx_rl_cp_ports
//	ScriptType:	RESTlet
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	08/24/2018
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv) {

    //if(srights.xcs > 0){



    var getPorts=get_ports(companyid);
    obj["ports"]=getPorts[0];
    obj["evcMenu"]=getPorts[1];
    obj["locationsXC"]=getPorts[2];

    return obj;

});

function get_ports(id){
    var evcMenu=0;
    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
    var records = nlapiSearchRecord('transaction', 'customsearch_clgx_ss_cp_show_evc', filters, columns);
    if(records!=null) {
        var evcMenu=1;
    }
    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
    var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders_3', filters, columns);
    var locationsXC = new Array();
    for ( var i = 0; records != null && i < records.length; i++ ) {
        var searchAP = records[i];
        var columns = searchAP.getAllColumns();
        var loc =searchAP.getValue(columns[0]);
        locationsXC.push(loc);
    }

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
    var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders', filters, columns);
    var ids = new Array();
    for ( var i = 0; records != null && i < records.length; i++ ) {
        ids.push(parseInt(records[i].getValue('internalid',null,'GROUP')));
    }
    var arr=new Array();
    arr=[];
    var objFnal=new Object();
    var arrObj=new Array();
    var columns = new Array();
    var filters = new Array();
    if(ids.length>0){
        filters.push(new nlobjSearchFilter("custrecord_xconnect_service_order",null,"anyof",ids));
        var records = nlapiSearchRecord('customrecord_cologix_crossconnect', 'customsearch_clgx_api_xcs_clouds_2', filters, columns);

        if(records!=null) {
            //nlapiSendEmail(206211,206211,'Test1','Test1',null,null,null,null,true);

            for (var j = 0; records != null && j < records.length; j++) {
                var searchAP = records[j];
                var columns = searchAP.getAllColumns();
                var loc = searchAP.getText(columns[0]);
                var portid = searchAP.getValue(columns[1]);
                var type = searchAP.getValue(columns[2]);
                var xc= searchAP.getValue(columns[3]);

                if (loc != null && loc != '') {
                    var obj = new Object();
                    obj.portID = portid+' ('+xc+')';
                    obj.loc = loc;
                    obj.type = type;
                    arrObj.push(obj)
                }
            }
            var groups = _.groupBy(arrObj, function (value) {
                return value.loc + '#' + value.type;
            });

            var objFnal = _.map(groups, function (group) {
                return {
                    market: group[0].loc,
                    type: group[0].type,
                    ports: _.pluck(group, 'portID')
                }
            });
        }
    }



    return [objFnal,evcMenu,locationsXC];
}