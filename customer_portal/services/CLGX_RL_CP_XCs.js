nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_XCs.js
//	ScriptID:	customscript_clgx_rl_cp_xcs
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=660&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/17/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv) {

    //if(srights.xcs > 0){
    if(srights.colocation > 0 || srights.network > 0){

        var direction = datain['direction'] || 'ASC';
        var order_by = datain['order_by'] || '';
        var page = datain['page'] || 1;
        var per_page = datain['per_page'];
        var stype = datain['stype'];
        var search = datain['search'] || '';
        var csv = datain['csv'] || 0;

        obj["xcs"] = get_xcs(companyid,contactid,modifierid,radix,direction,order_by,page,per_page,stype,search,csv);

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing xcs.';

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_XCS';
        obj.msg = 'No rights for xcs.';
    }

    return obj;

});

function get_xcs(companyid,contactid,modifierid,radix,direction,order_by,page,per_page,stype,search,csv){

    var obj = {};
    var arr = [];
    var total = 0;
    var pages = 0;
    var start = 0;
    var end = 0;
    var has_more = false;

    var facilities = [{"value": 0, "text": '*'}];
    var facilitiesids = [];

    var types = [{"value": 0, "text": '*'}];
    var typesids = [];

    var sos = [{"value": 0, "text": '*'}];
    var sosids = [];

    var carriers = [{"value": 0, "text": '*'}];
    var carriersids = [];

    search = JSON.parse(search);

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",companyid));
    var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders', filters, columns);
    var ids = new Array();
    for ( var i = 0; records != null && i < records.length; i++ ) {
        ids.push(parseInt(records[i].getValue('internalid',null,'GROUP')));
    }

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_xconnect_service_order",null,"anyof",ids));
    var records = nlapiSearchRecord('customrecord_cologix_crossconnect', 'customsearch_clgx_api_xcs_' + stype, filters, columns);
    if(records){
        for ( var i = 0; records != null && i < records.length; i++ ) {

            var facilityid = parseInt(records[i].getValue('custrecord_clgx_xc_facility',null,null)) || 0;
            var facility = records[i].getText('custrecord_clgx_xc_facility',null,null) || '';
            if(_.indexOf(facilitiesids, facilityid) == -1 && facilityid){
                facilitiesids.push(facilityid);
                facilities.push({"value": facilityid, "text": facility});
            }
            facilities = _.sortBy(facilities, function(obj){ return obj.value;});

            var soid = parseInt(records[i].getValue('custrecord_xconnect_service_order',null,null)) || 0;
            var name = (records[i].getText('custrecord_xconnect_service_order',null,null)).split("#");
            var so = (name[name.length-1]).trim() || '';
            if(_.indexOf(sosids, soid) == -1 && soid){
                sosids.push(soid);
                sos.push({"value": soid, "text": so});
            }
            sos = _.sortBy(sos, function(obj){ return obj.value;});

            var typeid = parseInt(records[i].getValue('custrecord_cologix_xc_circuit_type',null,null)) || 0;
            var type = records[i].getText('custrecord_cologix_xc_circuit_type',null,null) || '';
            if(_.indexOf(typesids, typeid) == -1 && typeid){
                typesids.push(typeid);
                types.push({"value": typeid, "text": type});
            }
            types = _.sortBy(types, function(obj){ return obj.value;});

            var carrierid = parseInt(records[i].getValue('custrecord_cologix_carrier_name',null,null)) || 0;
            var name = (records[i].getText('custrecord_cologix_carrier_name',null,null)).split(":")
            var carrier = (name[name.length-1]).trim() || '';
            if(_.indexOf(carriersids, carrierid) == -1 && carrierid){
                carriersids.push(carrierid);
                carriers.push({"value": carrierid, "text": carrier});
            }
            carriers = _.sortBy(carriers, function(obj){ return obj.value;});

        }
    }

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_xconnect_service_order",null,"anyof",ids));
    if(search){
        if(search.id){
           // var intid = parseInt((search.id).substr(2), 10);
            filters.push(new nlobjSearchFilter("name",null,"haskeywords",search.id));
        }
        if(search.types){
            filters.push(new nlobjSearchFilter("custrecord_cologix_xc_circuit_type",null,"anyof",search.types));
        }
        if(search.carriers){
            filters.push(new nlobjSearchFilter("custrecord_cologix_carrier_name",null,"anyof",search.carriers));
        }
        if(search.sos){
            filters.push(new nlobjSearchFilter("custrecord_xconnect_service_order",null,"anyof",search.sos));
        }
        if(search.facilities){
            filters.push(new nlobjSearchFilter("custrecord_clgx_xc_facility",null,"anyof",search.facilities));
        }
    }
    if(stype=='xcs') {
        columns.push(new nlobjSearchColumn('custrecord_clgx_xc_note', null, null));
    }
    var records = nlapiSearchRecord('customrecord_cologix_crossconnect', 'customsearch_clgx_api_xcs_' + stype, filters, columns);

    if(records){

        total = records.length;
        pages = Math.ceil(total / per_page);
        page = (parseInt(page) > pages ? pages : parseInt(page));
        has_more = (pages > 1 ? true : false);
        start = (page - 1) * per_page;
        end = (start + per_page > total ? total : start + per_page);

        for ( var i = start; i < end; i++ ) {

            var rec = new Object();
            var xcid = parseInt(records[i].getValue('internalid',null,null));
            rec["id"] = xcid;
            rec["rid"] = xcid.toString(radix);
            rec["number"] = records[i].getValue('name',null,null);

            var soid = parseInt(records[i].getValue('custrecord_xconnect_service_order',null,null)) || 0;
            rec["soid"] = soid;
            rec["rsoid"] = soid.toString(radix);
            var name = (records[i].getText('custrecord_xconnect_service_order',null,null)).split("#");
            rec["so"] = (name[name.length-1]).trim();

            rec["type"] = records[i].getText('custrecord_cologix_xc_circuit_type',null,null) || '';
            var name = (records[i].getText('custrecord_cologix_carrier_name',null,null)).split(":")
            rec["carrier"] = (name[name.length-1]).trim();
            rec["circuit"] = records[i].getValue('custrecord_cologix_xc_carriercirct',null,null) || '';
            rec["date"] = records[i].getValue('custrecord_clgx_net_prov_date',null,null) || '';

            rec["facilityid"] = parseInt(records[i].getValue('custrecord_clgx_xc_facility',null,null)) || 0;
            rec["facility"] = records[i].getText('custrecord_clgx_xc_facility',null,null) || '';
            if(stype=='xcs') {
                rec["note"]=records[i].getValue('custrecord_clgx_xc_note',null,null) || '';
            }
            arr.push(rec);

        }
    }
    obj["data"] = arr;
    if(csv){
        obj["csv"] = get_xcs_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,stype,search,csv);
    }
    obj["search"] = search;
    obj["lists"] = {"types": types, "sos": sos, "carriers": carriers, "facilities": facilities};
    obj["direction"] = direction;
    obj["order_by"] = order_by;
    obj["page"] = page;
    obj["pages"] = pages;
    obj["per_page"] = per_page;
    obj["has_more"] = has_more;
    obj["start"] = start;
    obj["end"] = end;
    obj["total"] = total;

    return obj;
}

function get_xcs_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,stype,search,csv){

    var text = 'ID,Service Order,Facility,Type,Carrier,Circuit\n';

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("entity",null,"anyof",companyid));
    var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders', filters, columns);
    var ids = new Array();
    for ( var i = 0; records != null && i < records.length; i++ ) {
        ids.push(parseInt(records[i].getValue('internalid',null,'GROUP')));
    }

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("custrecord_xconnect_service_order",null,"anyof",ids));
    if(search){
        if(search.id){
            // var intid = parseInt((search.id).substr(2), 10);
            filters.push(new nlobjSearchFilter("name",null,"haskeywords",search.id));
        }
        if(search.types){
            filters.push(new nlobjSearchFilter("custrecord_cologix_xc_circuit_type",null,"anyof",search.types));
        }
        if(search.carriers){
            filters.push(new nlobjSearchFilter("custrecord_cologix_carrier_name",null,"anyof",search.carriers));
        }
        if(search.sos){
            filters.push(new nlobjSearchFilter("custrecord_xconnect_service_order",null,"anyof",search.sos));
        }
        if(search.facilities){
            filters.push(new nlobjSearchFilter("custrecord_clgx_xc_facility",null,"anyof",search.facilities));
        }
    }
    var records = nlapiSearchRecord('customrecord_cologix_crossconnect', 'customsearch_clgx_api_xcs_' + stype, filters, columns);

    for ( var i = 0; records != null && i < records.length; i++ ) {

        text += records[i].getValue('name',null,null) + ',';
        var name = (records[i].getText('custrecord_xconnect_service_order',null,null)).split("#");
        text += (name[name.length-1]).trim() + ',';
        text += records[i].getText('custrecord_clgx_xc_facility',null,null) + ',';
        var circuit=records[i].getText('custrecord_cologix_xc_circuit_type',null,null).replace(/,/g , "");
        text += circuit + ',';
        var name = (records[i].getText('custrecord_cologix_carrier_name',null,null)).split(":");
        if((name[name.length-1]).trim()==null)
        {
            var carrier='';
        }
        else{
            var carrier=(name[name.length-1]).trim();
        }
        text += carrier + ',';
        var carriercirct=records[i].getValue('custrecord_cologix_xc_carriercirct',null,null).replace(/,/g , "");
        text += carriercirct;
        text += '\n';
    }
    return text;
}