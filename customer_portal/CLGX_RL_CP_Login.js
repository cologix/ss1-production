nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Login.js
//	ScriptID:	customscript_clgx_rl_cp_login
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=651&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/12/2016
//------------------------------------------------------

function post (datain){
    try {

        var cfid = datain['cfid'] || '';
        var ip = datain['ip'] || '';
        var masq = datain['masq'] || 'F';
        var username = datain['username'] || '';
        var password = datain['password'] || '';
        var askpass = datain['askpass'] || '';
        var obj = {};

        var objErr = {};
        objErr["login"] = 'F';
        objErr["error"] = 'F';
        objErr["code"] = '';
        objErr["msg"] = '';
        objErr["fail"] = 0;

        obj["login"] = objErr;

        if(username){

            var columns = new Array();
            var filters = new Array();
            filters.push(new nlobjSearchFilter('name',null,'is',username));
            var modifier = nlapiSearchRecord('customrecord_clgx_modifier', 'customsearch_clgx_cp_modifier_login', filters, columns);

            if(modifier){

                var modifierid = parseInt(modifier[0].getValue('internalid',null,null));
                var lang = parseInt(modifier[0].getValue('custrecord_clgx_modifier_language',null,null)) || 1;
                var attempts = parseInt(modifier[0].getValue('custrecord_clgx_modifier_login_attempts',null,null)) || 0;

                if(username && askpass){

                    // find first existing contact email
                    var columns = new Array();
                    var filters = new Array();
                    filters.push(new nlobjSearchFilter('custentity_clgx_modifier',null,'anyof',modifierid));
                    filters.push(new nlobjSearchFilter('email',null,'isnotempty'));
                    var contact = nlapiSearchRecord('contact', 'customsearch_clgx_cp_contacts', filters, columns);



                    if(contact){

                        var email = contact[0].getValue('email',null,null);
                        var contactid = parseInt(contact[0].getValue('internalid',null,null));

                        nlapiLogExecution('DEBUG','contactid', contactid);

                        if(attempts < 6){

                            obj.login.error = 'F';
                            obj.login.code = 'SUCCESS';
                            obj.login.msg = send_new_password(modifierid,contactid,email,lang);

                        } else {

                            obj.login.error = 'T';
                            obj.login.code = 'DISABLED_ACCOUNT';

                            if(lang == 17){
                                obj.login.msg = 'Vous avez fait 6 tentatives de connexion infructueuses et votre compte est maintenant désactivé. Veuillez contacter le soutien technique de Cologix (855.449.4357).';
                            } else {
                                obj.login.msg = 'You had 6 consecutive failed logins attempts. Your account is disabled. Please contact Cologix support (855.449.4357).';
                            }
                        }
                    } else {

                        obj.login.error = 'T';
                        obj.login.code = 'NO_EMAIL';

                        if(lang == 17){
                            obj.login.msg = 'Il n\'y a pas d\'e-mail associée à ce nom d\'utilisateur. Veuillez contacter le soutien technique de Cologix';
                        } else {
                            obj.login.msg = 'There is no email associated with this user name. Please contact support.';
                        }
                    }

                } else if (username && password)  {

                    var recPassword = modifier[0].getValue('custrecord_clgx_modifier_password',null,null) || '';
                    var terms = modifier[0].getValue('custrecord_clgx_modifier_accepted_terms',null,null) || 'F';
                    var sadmin = parseInt(modifier[0].getValue('custrecord_clgx_modifier_super_admin',null,null)) || 0;
                    var pdate = modifier[0].getValue('custrecord_clgx_modifier_password_date',null,null) || moment().subtract(3, 'months').format('M/D/YYYY');
                    var pRDate = modifier[0].getValue('custrecord_clgx_modifier_password_redate',null,null) || 0;
                    nlapiLogExecution('DEBUG','pRDate', pRDate);
                    if(pRDate!=0){
                        var pRDays = moment().diff(moment(pRDate), 'days');
                    }
                    else{
                        var pRDays = -1;
                    }
                    nlapiLogExecution('DEBUG','pRDays', pRDays);
                    nlapiLogExecution('DEBUG','pdate', pdate);
                    var pdays = moment().diff(moment(pdate), 'days');
                    nlapiLogExecution('DEBUG','pdays', pdays);

                    if(password == recPassword && attempts < 7){

                        // find first existing contact for this modifier
                        var columns = new Array();
                        var filters = new Array();
                        filters.push(new nlobjSearchFilter('custentity_clgx_modifier',null,'is',modifierid));
                        filters.push(new nlobjSearchFilter('email',null,'isnotempty'));
                        var contact = nlapiSearchRecord('contact', 'customsearch_clgx_cp_contacts', filters, columns);

                        var contactid = parseInt(contact[0].getValue('internalid',null,null));
                        var clgxid = parseInt(contact[0].getValue('company',null,null)) || 0;
                        var nacid = parseInt(contact[0].getValue('custentity_clgx_legacy_contact_id',null,null)) || 0;
                        var name = contact[0].getValue('entityid',null,null);
                        name = name.replace(/'/g, '&#39;');
                        var role = parseInt(contact[0].getValue('contactrole',null,null)) || 0;
                        nlapiSubmitField('customrecord_clgx_modifier', modifierid, 'custrecord_clgx_modifier_login_attempts', 0); // re-initiate login attempts

                        var radix = get_radix();

                        // get user info from first company
                        var objUser = {};
                        objUser["mid"] = modifierid;
                        var rmid = modifierid.toString(radix);
                        objUser["mrid"] = rmid;
                        objUser["id"] = contactid;
                        var rid = contactid.toString(radix);
                        objUser["rid"] = rid;
                        objUser["companyid"] = clgxid;
                        objUser["nac"] = nacid;
                        objUser["name"] = name;
                        objUser["uname"] = username;
                        objUser["passwd"] = password;
                        objUser["sadmin"] = sadmin;
                        objUser["role"] = role;
                        objUser["terms"] = terms;
                        objUser["pdate"] = pdate;
                        objUser["pdays"] = pdays;
                        objUser["pRDays"] = pRDays;


                        if (lang == 17) {
                            objUser["langid"] = 17;
                            objUser["lang"] = 'fr';
                        }
                        else {
                            objUser["langid"] = 1;
                            objUser["lang"] = 'en';
                        }
                        obj["user"] = objUser;

                        var arrFA = new Array();
                        var arrCA = new Array();
                        var searchAnnouncements = nlapiSearchRecord('customrecord_clgx_eportal_announcements','customsearch_clgx_cp_announcements', arrFA,arrCA);
                        var arrAnnouncements=[];
                        if(searchAnnouncements!=null) {
                            for (var k = 0; searchAnnouncements != null && k < searchAnnouncements.length; k++) {
                                var searchAnnouncement = searchAnnouncements[k];
                                var columnsA = searchAnnouncement.getAllColumns();
                                var text = searchAnnouncement.getValue(columnsA[0]);
                                arrAnnouncements.push(text);
                            }
                        }



                        obj.companies = get_companies(modifierid,radix);
                        var objNS = {};
                        objNS["ip"] = ip;
                        objNS["cfid"] = cfid;
                        objNS["nsid"] = get_uuid();
                        objNS["radix"] = radix;
                        obj["ns"] = objNS;
                        obj["announcements"]   = arrAnnouncements;
                        if(sadmin==0)
                        {
                            sadmin='0';
                        }

                        //nlapiLogExecution('ERROR','debug', " | clgxid = " + clgxid + " | password = " + password + " | askpass = " + askpass + " | modifierid = " + modifierid + " | lang = " + lang + " | attempts = " + attempts + " |");
                        //nlapiLogExecution('ERROR','debug', " | recPassword = " + recPassword + " | terms = " + terms + " | sadmin = " + sadmin + " | pdate = " + pdate + " | pdays = " + pdays + " |");
                        //nlapiLogExecution('ERROR','debug', " | objUser = " + JSON.stringify(obj) + " |");


                        if(obj.companies.length > 0){ // if any company
                            //obj.company = get_company(contactid,modifierid,obj.companies[0].id,radix,sadmin);
                            obj.company = get_company(contactid,modifierid,clgxid,radix,sadmin);
                        } else {
                            obj.company = [];
                        }

                        obj.login.login = 'T';
                        obj.login.error = 'F';
                        obj.login.code = 'SUCCESS';

                        if(lang == 17){
                            obj.login.msg = 'Bonjour ' + obj.user.name + '. Vous gérez - ' + obj.company.name;
                        } else {
                            obj.login.msg = 'Hello ' + obj.user.name + '. You are managing - ' + obj.company.name;
                        }
                        obj["masq"] = masq;
                        var newsid = set_session(obj);
                        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', newsid, ['custrecord_clgx_api_cust_portal_masquara'], [masq]);

                    } else {

                        if(attempts > 4){

                            obj.login.error = 'T';
                            obj.login.code = 'DISABLED_ACCOUNT';

                            if(lang == 17){
                                obj.login.msg = 'Vous avez fait 6 tentatives de connexion infructueuses et votre compte est maintenant désactivé. Veuillez contacter le soutien technique de Cologix.';
                            } else {
                                obj.login.msg = 'You had 6 consecutive failed logins attempts. Your account is disabled. Please contact Cologix support.';
                            }
                            obj.login.fail = 6;

                        } else {
                            var left = 5 - attempts;
                            obj.login.error = 'T';
                            obj.login.code = 'INCORRECT_LOGIN';

                            if(lang == 17){
                                obj.login.msg = 'Info de connexion incorrecte! Veuillez r&eacute;essayer. Il vous reste ' + left + ' tentatives.';
                            } else {
                                obj.login.msg = 'Incorrect login! Please try again. You have ' + left + ' attempt(s) left.';
                            }

                            obj.login.fail = attempts + 1;
                        }
                        nlapiSubmitField('customrecord_clgx_modifier', modifierid, 'custrecord_clgx_modifier_login_attempts', attempts + 1);

                    }

                } else {

                    obj.login.error = 'T';
                    obj.login.code = 'WRONG_PASSWORD';

                    if(lang == 17){
                        obj.login.msg = 'Info de connexion incorrecte! Veuillez réessayer.';
                    } else {
                        obj.login.msg = 'Incorrect login! Please try again.';
                    }
                }

            } else {

                obj.login.error = 'T';
                obj.login.code = 'WRONG_USER_NAME';
                obj.login.msg = 'You do not have access to the portal. Please contact your portal admin.';
            }


        } else {

            obj.login.error = 'T';
            obj.login.code = 'NO_USER_NAME';
            obj.login.msg = 'Incorrect login! Please try again.';
        }

        //nlapiLogExecution('ERROR','debug', " | obj = " + JSON.stringify(obj) + " |");
        nlapiLogExecution('DEBUG', 'Return OBJ', JSON.stringify(obj));

        return obj;
    }

    catch (error) {

        if (error.getDetails != undefined){
            var code = error.getCode();
            var msg = error.getDetails();
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            //  throw error;
        } else {
            var code = 'ERROR';
            var msg = error.toString();
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            //   throw nlapiCreateError('99999', error.toString());
        }
        var obj = new Object();
        obj["error"] = 'T';
        obj["code"] = code;
        obj["msg"] = msg;
        obj["me"] = {};
        return obj;
    }
}