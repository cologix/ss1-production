nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_NewCase.js
//	ScriptID:	customscript_clgx_rl_cp_newcase
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=675&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	07/13/2016
//------------------------------------------------------


var put = wrap(function put(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

  
    if(srights.cases > 0){
         var subject= datain['subject'] || " ";
         nlapiSendEmail(206211,206211,'test3',subject,null,null,null,null);
        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);
  
 nlapiSendEmail(206211,206211,'test2','test2',null,null,null,null);
    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_CASES';
        obj.msg = 'No rights for cases.';
    }

    return obj;

});

