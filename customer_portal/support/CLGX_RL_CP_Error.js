nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Error.js
//	ScriptID:	customscript_clgx_rl_cp_error
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=675&deploy=1
//	@authors:	Catalina Taran- catalina.taran@cologix.com
//	Created:	08/17/2016
//------------------------------------------------------

var put = wrap(function put(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    var message = datain['message'] || "";
    var email = nlapiLookupField('contact', contactid, 'email');
    var record = nlapiCreateRecord('supportcase');
    record.setFieldValue('title', 'Error on portal');
    record.setFieldValue('company', 2763);
    record.setFieldValue('assigned', 9974);
    // record.setFieldValue('contact', contactid);
    record.setFieldValue('email', email);
    record.setFieldValue('category', 10);
    record.setFieldValue('custevent_cologix_sub_case_type', 56);
    record.setFieldValue('custevent_cologix_facility', 16);
    record.setFieldValue('priority', 2);
    record.setFieldText('origin', 'Web');
    record.setFieldValue('incomingmessage', message);

    // nlapiLogExecution('ERROR','message', message);
    try {
        var recordId = nlapiSubmitRecord(record,true,true);
        //  nlapiLogExecution('ERROR','recordId', recordId);
    }
    catch (error) {
        nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
    }


    obj["id"] = recordId;
    obj["rid"] = recordId;

    var now = moment().format("M/D/YYYY h:mm:ss a");
    nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

    obj.code = 'SUCCESS';
    obj.msg = 'A new case has been added.';


    return obj;

});