nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Notifications.js
//	ScriptID:	customscript_clgx_rl_cp_notifications
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=661&deploy=1
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	08/31/2016
//------------------------------------------------------
var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {
    nlapiLogExecution('DEBUG', 'companyid', companyid);
    try{
        obj["notifications"] = get_contacts(companyid, 1);
        obj["notifications_email"] = get_contacts(companyid, 2);
        obj["notifications_sms"] = get_contacts(companyid, 3);
        obj["notifications_all"] = get_contacts(companyid, 4);
        obj.code = 'SUCCESS';
        obj.msg = 'You are managing shipments.';
        obj.error = 'F';
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            //throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
      //      throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------


    return obj;
});



function get_contacts(companyid,order){

    var objData = [];


    // Modifiers of managed company ===========================================================================================================

    var arrFilters = new Array();
    var arrContacts= new Array();
    arrFilters.push(new nlobjSearchFilter("company",null,"is",companyid));
    var searchContacts = nlapiSearchRecord('contact', 'customsearch_clgx_cp_contacts_shipping', arrFilters,arrContacts);
    for ( var i = 0; searchContacts != null && i < searchContacts.length; i++ ) {
        var searchC = searchContacts[i];
        var columns = searchC.getAllColumns();
        if(order==1) {
            if (searchC.getValue(columns[2]) == 'T' || searchC.getValue(columns[3]) == 'T') {
                var name = searchC.getValue(columns[4]) + " " + searchC.getValue(columns[5]);
                var objEmail={"email":searchC.getValue(columns[6]),
                    "sms":searchC.getValue(columns[7])};
                var id1=searchC.getValue(columns[1]);
                var contactObj = {
                    "value": searchC.getValue(columns[1]),
                    "value_netsuite": searchC.getValue(columns[0]),
                    "text": name
                };
                contactObj[id1]=objEmail;
                objData.push(contactObj);
            }
        }
        if(order==2) {
            if (searchC.getValue(columns[2]) == 'T' ) {
                var name = searchC.getValue(columns[3]) + " " + searchC.getValue(columns[4]) + "(email:" + searchC.getValue(columns[6]) + ")";
                var contactObj = {
                    "value": searchC.getValue(columns[1]),
                    "value_netsuite": searchC.getValue(columns[0]),
                    "text": name
                };
                objData.push(contactObj);
            }
        }
        if(order==3) {
            if (searchC.getValue(columns[3]) == 'T') {
                var name = searchC.getValue(columns[3]) + " " + searchC.getValue(columns[4]) + "(email:" + searchC.getValue(columns[6]) + "; sms:" + searchC.getValue(columns[7]) + ")";
                var contactObj = {
                    "value": searchC.getValue(columns[1]),
                    "value_netsuite": searchC.getValue(columns[0]),
                    "text": name
                };
                objData.push(contactObj);
            }
        }
        if(order==4) {
            var name = searchC.getValue(columns[4]) + " " + searchC.getValue(columns[5]);
            var objEmail={"email":searchC.getValue(columns[6]),
                "sms":searchC.getValue(columns[7])};
            var id1=searchC.getValue(columns[1]);
            var contactObj = {
                "value": searchC.getValue(columns[1]),
                "value_netsuite": searchC.getValue(columns[0]),
                "text": name
            };
            contactObj[id1]=objEmail;
            objData.push(contactObj);

        }



    }
    return objData;
}