nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Message.js
//	ScriptID:	customscript_clgx_rl_cp_message
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=681&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	07/11/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    if(srights.cases > 0 || srights.casesfin > 0){

        var rid = datain['rid'] || "0";
        var messageid = parseInt(rid, radix);
        
        try {
        	var rec = nlapiLoadRecord('message', messageid);
            obj.code = 'SUCCESS';
            obj.msg = 'You are viewing a message';
            var message=rec.getFieldValue('message');
            if(message==null)
            {
             message=' ';
            }
            obj["message"] = message;
        }
        catch (e) {
        	obj.error = 'T';
            obj.code = 'ERROR';
            obj["message"] = 'No message';
        }
        
        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_CASES';
        obj.msg = 'No rights for cases.';
    }

    return obj;

});

