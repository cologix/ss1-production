nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Case.js
//	ScriptID:	customscript_clgx_rl_cp_case
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=675&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	07/06/2016
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    if(srights.cases > 0 || srights.casesfin > 0){

        var rid = datain['rid'] || "0";
        var message = datain['message'] || "";
        var close = datain['close'] || false;
        var type = datain['type'] || "";
      nlapiLogExecution('DEBUG', 'rid', rid);
      nlapiLogExecution('DEBUG', 'radix', radix);
        var caseid = parseInt(rid, radix);
nlapiLogExecution('DEBUG', 'caseid', caseid+';'+close+';'+contactid);
        var contact = nlapiLookupField('contact', contactid, 'entityid');
        
        if(message || close=='on'){
            if(close=='on'){
                var objCase = nlapiLoadRecord('supportcase', caseid);
                objCase.setFieldValue('status', 5);
                nlapiSubmitRecord(objCase, true,true);
            }  
          if(message!='') {
                var objCase = nlapiLoadRecord('supportcase', caseid);
                objCase.setFieldValue('messagenew', 'T');
                message += '\n\nUpdated By: ' + contact;
                objCase.setFieldValue('incomingmessage', message);
                nlapiSubmitRecord(objCase, true,true);
            }
        }

        var rec = nlapiLoadRecord('supportcase', caseid);

        obj["id"] = caseid;
        obj["rid"] = rid;
        obj["type"] = type;
        obj["number"] = rec.getFieldValue('casenumber');
        obj["closed"] = rec.getFieldValue('enddate');
        obj["created"] = rec.getFieldValue('createddate');
        obj["scheduled"] = rec.getFieldValue('custevent_cologix_case_sched_followup') + ' ' + rec.getFieldValue('custevent_cologix_sched_start_time');

        var createdby = (rec.getFieldText('contact')).split(":") || '';
        obj["createdby"] = createdby[createdby.length - 1];


        obj["category"] = rec.getFieldText('category');
        obj["subject"] = rec.getFieldValue('title');
        obj["facility"] = rec.getFieldText('custevent_cologix_facility');
        obj["priority"] = rec.getFieldText('priority');
        var comments = [];
        var columns = new Array();
        var filters = new Array();
        filters.push(new nlobjSearchFilter("internalid",'case',"anyof",caseid));
        var records = nlapiSearchRecord('message', "customsearch_clgx_cp_case_messages", filters, columns);
        for ( var i = 0; records != null && i < records.length; i++ ) {
            var comment = {};
            var id = parseInt(records[i].getValue('internalid',null,null));
            comment["id"] = id;
            comment["rid"] = id.toString(radix);
            comment["created"] = records[i].getValue('messagedate',null,null);
            comment["author"] = records[i].getText('author',null,null);
            comment["recipient"] = records[i].getText('recipient',null,null);
            comment["subject"] = records[i].getValue('subject',null,null);
            //comment["comment"] = records[i].getValue('message',null,null);
            comments.push(comment);
        }
        obj["comments"] = comments;

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing cases.';

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_CASES';
        obj.msg = 'No rights for cases.';
    }
  nlapiLogExecution('DEBUG', 'Return OBJ Case Number', obj.number);
    nlapiLogExecution('DEBUG', 'Return OBJ', JSON.stringify(obj));

    return obj;

});

var put = wrap(function put(datain,obj,srights,companyid,contactid,modifierid,radix,sid) {

    if(srights.cases > 0 || srights.casesfin > 0 || srights.security > 0) {

        var subject = datain['subject'] || "";
        var priority = datain['priority'] || '3';
        var type = datain['type'] || "1";
        var request_type=datain['request_type']||'1';
        var requesteddate = datain['requested'] || "";
        var requestedtime = datain['requestedtime'] || "";
        var requesteddate1 = datain['requested1'] || "";
        var requestedtime1 = datain['requestedtime1'] || "";
        var facility = datain['facility'] || "";
        var message = datain['message'] || "";
        var rectype = datain['rectype'] || '0';
        var recid = datain['recid'] || '0';
        var recname = datain['recname'] || "";
        var so = datain['so'] || "";
        var userid = datain['userid'] || "";
        var sr = datain['sr'] || '0';
        var email = nlapiLookupField('contact', contactid, 'email');
        if (sr != 0) {
            var usrID = parseInt(userid, radix);
            var emailUsr = nlapiLookupField('contact', usrID, 'email');
            var entityid = nlapiLookupField('contact', contactid, 'entityid');
            var date = new Date();
            var record = nlapiCreateRecord('supportcase');
            var redirect = '';
            record.setFieldValue('title', subject);
            record.setFieldValue('company', companyid);
            record.setFieldValue('contact', usrID);
            record.setFieldValue('email', emailUsr);
            if (requesteddate != null && requesteddate != '') {
                record.setFieldValue('custevent_cologix_case_sched_followup', requesteddate);
            }
            if (requestedtime != null && requestedtime != '') {
                record.setFieldValue('custevent_cologix_sched_start_time', requestedtime);
            }
            if (requesteddate1 != null && requesteddate1 != '') {
                record.setFieldValue('custevent_clgx_case_end_date', requesteddate1);
            }
            if (requestedtime1 != null && requestedtime1 != '') {
                record.setFieldValue('custevent_clgx_case_end_time', requestedtime1);
            }
            var messageComplete='Requester -'+entityid+'\n';
            messageComplete +=message;
            if (messageComplete != null && messageComplete != '') {
                record.setFieldValue('incomingmessage', messageComplete);
            }
            record.setFieldValue('custevent_cologix_facility', facility);
            record.setFieldValue('category', 1);
            record.setFieldValue('custevent_cologix_sub_case_type', 77);
            record.setFieldValue('assigned', 946879);
            redirect = 'admin/users/';
            if(srights.cases > 0 || srights.casesfin > 0) {
                redirect = 'support/cases/?r=1';
            }
            else 
            {
                redirect = 'admin/users/';
            }

            record.setFieldValue('priority', priority);
            record.setFieldText('origin', 'Web');
            record.setFieldValue('custevent_clgx_case_request_type', request_type);


            // record.setFieldValue('startdate', nlapiDateToString(date, 'date'));
            // record.setFieldValue('starttime', nlapiDateToString(date, 'timeofday'));

            try {
                var recordId = nlapiSubmitRecord(record, true, true);
            }
            catch (error) {
                nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
            }
        }
        else {
            var recordid = parseInt(recid, radix);
            if (so != '') {
                if (rectype != 3) {
                    var soid = parseInt(so, radix);
                }
                else {
                    var soid = so;
                }
            }
            else {
                var soid = '';
            }
            var date = new Date();
            var record = nlapiCreateRecord('supportcase');
            var redirect = '';
            record.setFieldValue('title', subject);
            record.setFieldValue('company', companyid);
            record.setFieldValue('contact', contactid);
            record.setFieldValue('email', email);
            if (soid != '') {
                record.setFieldValue('custevent2', soid);

            }
            if (requesteddate != null && requesteddate != '') {
                record.setFieldValue('custevent_cologix_case_sched_followup', requesteddate);
            }
            if (requestedtime != null && requestedtime != '') {
                record.setFieldValue('custevent_cologix_sched_start_time', requestedtime);
            }

            if (message != null && message != '') {
                record.setFieldValue('incomingmessage', message);
            }
            if (rectype != 0) {
                if (rectype == 1) {
                    record.setFieldValue('custevent_clgx_case_cons_inv', recordid);
                }
                else if (rectype == 2) {
                    record.setFieldValue('custevent2', recordid);
                }
                else if (rectype == 3) {
                    record.setFieldValue('custevent_power_circuit', recordid);
                }
                else if (rectype == 4) {
                    record.setFieldValue('custevent_cologix_space', recordid);
                }
                else if (rectype == 5) {
                    record.setFieldValue('custevent_cologix_cross_connect', recordid);
                }

            }
            record.setFieldValue('custevent_cologix_facility', facility);
            if (type == 1) {
                record.setFieldValue('category', 1);
                record.setFieldValue('custevent_cologix_sub_case_type', 5);
                record.setFieldValue('assigned', 379486);
                redirect = 'cases';
            }
            else if (type == 2) {
                record.setFieldValue('category', 1);
                record.setFieldValue('custevent_cologix_sub_case_type', 6);
                record.setFieldValue('assigned', 379486);
                redirect = 'cases';
            }
            else if (type == 3) {
                record.setFieldValue('category', 11);
                record.setFieldValue('assigned', 12399);
                redirect = 'casesfin';
            }
            else if (type == 4) {
                record.setFieldValue('category', 3);
                record.setFieldValue('custevent_cologix_sub_case_type', 86);
                record.setFieldValue('assigned', 379486);
                redirect = 'cases';
            }
            if (soid != '') {
                record.setFieldValue('custevent_cologix_sub_case_type', 50);
            }
            record.setFieldValue('priority', priority);
            record.setFieldText('origin', 'Web');
            record.setFieldValue('company', companyid);
            record.setFieldValue('startdate', nlapiDateToString(date, 'date'));
            record.setFieldValue('starttime', nlapiDateToString(date, 'timeofday'));

            try {
                var recordId = nlapiSubmitRecord(record, true, true);
            }
            catch (error) {
                nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
            }
        }

        obj["id"] = recordId;
        obj["rid"] = recordId;
        obj["number"] = nlapiLookupField('supportcase', recordId, 'casenumber');

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'A new case has been added.';
        obj.redirect=redirect;

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_CASES';
        obj.msg = 'No rights for cases.';
    }
    nlapiLogExecution('DEBUG', 'Return OBJ', JSON.stringify(obj));
    return obj;

});