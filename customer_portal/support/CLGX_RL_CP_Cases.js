nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_Cases.js
//	ScriptID:	customscript_clgx_rl_cp_cases
//	ScriptType:	RESTlet
//	ScriptURL:	/app/site/hosting/restlet.nl?script=655&deploy=1
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	05/17/2016
//------------------------------------------------------


var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv) {

    if(srights.cases > 0 || srights.casesfin > 0){

        var direction = datain['direction'] || 'ASC';
        var order_by = datain['order_by'] || '';
        var page = datain['page'] || 1;
        var per_page = datain['per_page'];
        var type = datain['type'];
        var search = datain['search'] || '';
        var csv = datain['csv'] || 0;

        obj["cases"] = get_cases(companyid,contactid,modifierid,radix,direction,order_by,page,per_page,type,search,csv);

        var now = moment().format("M/D/YYYY h:mm:ss a");
        nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last'], [now]);

        obj.code = 'SUCCESS';
        obj.msg = 'You are managing cases.';

    } else {
        obj.error = 'T';
        obj.code = 'NO_RIGHTS_CASES';
        obj.msg = 'No rights for cases.';
    }
    nlapiLogExecution('DEBUG', 'Return OBJ', JSON.stringify(obj));
    return obj;

});

function get_cases(companyid,contactid,modifierid,radix,direction,order_by,page,per_page,type,search,csv){

    var obj = {};
    var arr = [];
    var total = 0;
    var pages = 0;
    var start = 0;
    var end = 0;
    var has_more = false;

    var facilities = [{"value": 0, "text": '*'}];
    var facilitiesids = [];

    var statuses = [{"value": 0, "text": '*'}];
    var statusesids = [];

    var priorities = [{"value": 0, "text": '*'}];
    var prioritiesids = [];

    search = JSON.parse(search);

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("internalid",'company',"is",companyid));
    var records = nlapiSearchRecord('supportcase', 'customsearch_clgx_cp_cases' + type, filters, columns);
    if(records){
        for ( var i = 0; records != null && i < records.length; i++ ) {
            var facilityid = parseInt(records[i].getValue('custevent_cologix_facility',null,null)) || 0;
            var facility = records[i].getText('custevent_cologix_facility',null,null) || '';
            if(_.indexOf(facilitiesids, facilityid) == -1 && facilityid){
                facilitiesids.push(facilityid);
                facilities.push({"value": facilityid, "text": facility});
            }
            facilities = _.sortBy(facilities, function(obj){ return obj.value;});

            var statusid = parseInt(records[i].getValue('status',null,null)) || 0;
            var status = records[i].getText('status',null,null) || '';
            if(_.indexOf(statusesids, statusid) == -1 && statusid){
                statusesids.push(statusid);
                statuses.push({"value": statusid, "text": status});
            }
            statuses = _.sortBy(statuses, function(obj){ return obj.value;});

            var priorityid = parseInt(records[i].getValue('priority',null,null)) || 0;
            var priority = records[i].getText('priority',null,null) || '';
            if(_.indexOf(prioritiesids, priorityid) == -1 && priorityid){
                prioritiesids.push(priorityid);
                priorities.push({"value": priorityid, "text": priority});
            }
            priorities = _.sortBy(priorities, function(obj){ return obj.value;});
        }
    }

    var columns = new Array();
    var filters = new Array();
 filters.push(new nlobjSearchFilter("custrecord_clgx_gq_cid",null,"anyof",parseInt(companyid)));
//filters.push(new nlobjSearchFilter("custrecord_clgx_gq_json",null,"contains",strCompanyId));
    var recordsQ = nlapiSearchRecord('customrecord_clgx_global_queue', 'customsearch_clgx_cp_cases_queue' , filters, columns);
    if(recordsQ){

        for ( var i = 0; i < recordsQ.length; i++ ) {

            var rec = {};
            // var caseid = parseInt(records[i].getValue('internalid',null,null));
            //rec["id"] = caseid;
            rec["rid"] = '';
            var json=JSON.parse(recordsQ[i].getValue('custrecord_clgx_gq_json',null,null));

            rec["number"] = '';
            rec["title"] = json[0].title || '';
            rec["status"] = parseInt(10);
            var fac=json[0].custevent_cologix_facility;
            var facname=nlapiLookupField('customrecord_cologix_facility', fac, 'name') || '';
            var opened=json[0].startdate+' '+json[0].starttime;
            rec["priority"] = parseInt(json[0].priority) || 0;
            rec["facilityid"] = parseInt(fac) || 0;
            rec["facility"] = facname || '';
            rec["assigned"] =  '';
            rec["opened"] = opened || '';
            rec["closed"] = '';

            arr.push(rec);
        }
    }


    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("internalid",'company',"is",companyid));
    if(search){
        if(search.id){
            filters.push(new nlobjSearchFilter("casenumber",null,"contains",search.id));
        }
        if(search.subject){
            filters.push(new nlobjSearchFilter("title",null,"contains",search.subject));
        }
        if(search.priorities){
            filters.push(new nlobjSearchFilter("priority",null,"anyof",search.priorities));
        }
        if(search.statuses){

            if(search.statuses == 1){
                filters.push(new nlobjSearchFilter("status",null,"anyof",[1,9]));
            }
            if(search.statuses == 2){
                filters.push(new nlobjSearchFilter("status",null,"anyof",[2,3,4,6,7,8]));
            }
        }
        if(search.from && search.to){
            filters.push(new nlobjSearchFilter("createddate",null,"within",search.from,search.to));
        }
        if(search.from && !search.to){
            filters.push(new nlobjSearchFilter("createddate",null,"on",search.from));
        }
        if(!search.from && search.to){
            filters.push(new nlobjSearchFilter("createddate",null,"on",search.to));
        }

        if(search.facilities){
            filters.push(new nlobjSearchFilter("custevent_cologix_facility",null,"anyof",search.facilities));
        }
    }
    var records = nlapiSearchRecord('supportcase', 'customsearch_clgx_cp_cases' + type, filters, columns);

    if(records){

        total = records.length;
        pages = Math.ceil(total / per_page);
        page = (parseInt(page) > pages ? pages : parseInt(page));
        has_more = (pages > 1 ? true : false);
        start = (page - 1) * per_page;
        end = (start + per_page > total ? total : start + per_page);

        for ( var i = start; i < end; i++ ) {

            var rec = {};
            var caseid = parseInt(records[i].getValue('internalid',null,null));
            //rec["id"] = caseid;
            rec["rid"] = caseid.toString(radix);
            rec["number"] = records[i].getValue('casenumber',null,null);
            rec["title"] = records[i].getValue('title',null,null) || '';
            rec["status"] = parseInt(records[i].getValue('status',null,null));

            /*
             6 Awaiting Assignment - In Progress
             8 Awaiting CAB Approval - In Progress
             9 CAB Approved - Scheduled
             3 Escalated - In Progress
             2 In Progress - In Progress
             1 Not Started - Scheduled
             4 Re-Opened - In Progress
             7 Resolved - Closed

             switch(statusid) {
             case 6: // Awaiting Assignment
             rec["status"] = 'In Progress';
             break;
             case 8: // Awaiting CAB Approval
             rec["status"] = 'In Progress';
             break;
             case 9: // CAB Approved
             rec["status"] = '';
             break;
             case 3: // Escalated
             rec["status"] = 'In Progress';
             break;
             case 2: // In Progress
             rec["status"] = 'In Progress';
             break;
             case 1: // Not Started
             rec["status"] = '';
             break;
             case 4: // Re-Opened
             rec["status"] = 'In Progress';
             break;
             case 7: // Resolved
             rec["status"] = 'Closed';
             break;
             default:
             rec["status"] = '';
             }

             Awaiting Assignment	In Progress
             Awaiting Cab Approval	N/A would not be assigned to customers
             CAB Approved	Scheduled
             Escalated	In Progress
             In Progress	In Progress
             Not Started	Scheduled
             Re-Opened	In Progress
             Resolved	Resolved
             Closed	Closed
             */


            rec["priority"] = parseInt(records[i].getValue('priority',null,null)) || 0;
            rec["facilityid"] = parseInt(records[i].getValue('custevent_cologix_facility',null,null)) || 0;
            rec["facility"] = records[i].getText('custevent_cologix_facility',null,null) || '';
            rec["assigned"] = records[i].getText('assigned',null,null) || '';
            rec["opened"] = records[i].getValue('createddate',null,null) || '';
            rec["closed"] = records[i].getValue('closed',null,null) || '';

            arr.push(rec);
        }
    }


    obj["data"] = arr;
    if(csv){
        obj["csv"] = get_cases_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,type,search,csv);
    }
    obj["lists"] = {"statuses": statuses, "priorities": priorities, "facilities": facilities};
    obj["search"] = search;
    obj["direction"] = direction;
    obj["order_by"] = order_by;
    obj["page"] = page;
    obj["pages"] = pages;
    obj["per_page"] = per_page;
    obj["has_more"] = has_more;
    obj["start"] = start;
    obj["end"] = end;
    obj["total"] = total;

    return obj;
}

function get_cases_csv (companyid,contactid,modifierid,radix,direction,order_by,page,per_page,type,search,csv){
    if(type=='l')
    {
        var text = 'ID,Subject,Closed,Facility,Priority\n';
    }else {
        var text = 'ID,Subject,Status,Opened,Facility,Priority\n';
    }
    // nlapiLogExecution('DEBUG', 'type', type);

    var columns = new Array();
    var filters = new Array();
//filters.push(new nlobjSearchFilter("custrecord_clgx_gq_cid",null,"is",parseInt(companyid)));
  filters.push(new nlobjSearchFilter("custrecord_clgx_gq_cid",null,"anyof",parseInt(companyid)));
    var recordsQ = nlapiSearchRecord('customrecord_clgx_global_queue', 'customsearch_clgx_cp_cases_queue' , filters, columns);
    if(recordsQ){

        for ( var i = 0; i < recordsQ.length; i++ ) {

            var rec = {};
            // var caseid = parseInt(records[i].getValue('internalid',null,null));
            //rec["id"] = caseid;
            rec["rid"] = '';
            var json=JSON.parse(recordsQ[i].getValue('custrecord_clgx_gq_json',null,null));

            text += '' + ',';
            text += json[0].title + ',';
            var opened=json[0].startdate+' '+json[0].starttime;
            text += 'Processing' + ',';
            text += opened + ',';
            var fac=json[0].custevent_cologix_facility;
            var facname=nlapiLookupField('customrecord_cologix_facility', fac, 'name') || '';

            text += facname+ ',';
            var priority=json[0].priority;
            nlapiLogExecution('DEBUG', 'priority', priority);
            switch(parseInt(priority)) {
                case 1: // Urgent
                    var pr = 'Urgent';
                    break;
                case 2: // Medium
                    var pr = 'Medium';
                    break;
                case 3: // Urgent
                    var pr = 'Low';
                    break;

                default:
                    var pr = '';
            }
          nlapiLogExecution('DEBUG', 'pr', pr);
            text += pr;
            text += '\n';
        }
    }

    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter("internalid",'company',"is",companyid));
    if(search){
        if(search.id){
            filters.push(new nlobjSearchFilter("casenumber",null,"contains",search.id));
        }
        if(search.subject){
            filters.push(new nlobjSearchFilter("title",null,"contains",search.subject));
        }
        if(search.priorities){
            filters.push(new nlobjSearchFilter("priority",null,"anyof",search.priorities));
        }
        if(search.statuses){
            filters.push(new nlobjSearchFilter("status",null,"anyof",search.statuses));
        }
        if(search.from && search.to){
            filters.push(new nlobjSearchFilter("createddate",null,"within",search.from,search.to));
        }
        if(search.from && !search.to){
            filters.push(new nlobjSearchFilter("createddate",null,"on",search.from));
        }
        if(!search.from && search.to){
            filters.push(new nlobjSearchFilter("createddate",null,"on",search.to));
        }

        if(search.facilities){
            filters.push(new nlobjSearchFilter("custevent_cologix_facility",null,"anyof",search.facilities));
        }
    }
    var records = nlapiSearchRecord('supportcase', 'customsearch_clgx_cp_cases' + type, filters, columns);

    for ( var i = 0; records != null && i < records.length; i++ ) {

        /*
         6 Awaiting Assignment - In Progress
         8 Awaiting CAB Approval - In Progress
         9 CAB Approved - Scheduled
         3 Escalated - In Progress
         2 In Progress - In Progress
         1 Not Started - Scheduled
         4 Re-Opened - In Progress
         7 Resolved - Closed*/

        var statusid=parseInt(records[i].getValue('status',null,null));
        //   nlapiLogExecution('DEBUG', 'statusid', statusid);
        switch(statusid) {
            case 6: // Awaiting Assignment
                var status = 'In Progress';
                break;
            case 8: // Awaiting CAB Approval
                var status = 'In Progress';
                break;
            case 9: // CAB Approved
                var status = 'Scheduled';
                break;
            case 3: // Escalated
                var status = 'In Progress';
                break;
            case 2: // In Progress
                var status = 'In Progress';
                break;
            case 1: // Not Started
                var status = 'Scheduled';
                break;
            case 4: // Re-Opened
                var status = 'In Progress';
                break;
            case 5: // Closed
                var status = 'Closed';
                break;
            case 7: // Resolved
                var status = 'Closed';
                break;
            default:
                var status = '';
        }

        text += records[i].getValue('casenumber',null,null) + ',';
        text += records[i].getValue('title',null,null).replace(/,/g , " ") + ',';

        if(type=='l')
        {
            text += records[i].getValue('closed',null,null) + ',';
        }else {
            text += status + ',';
            text += records[i].getValue('createddate',null,null) + ',';
        }
        text += records[i].getText('custevent_cologix_facility',null,null)+ ',';
        text += records[i].getText('priority',null,null);
        text += '\n';

    }

    return text;
}