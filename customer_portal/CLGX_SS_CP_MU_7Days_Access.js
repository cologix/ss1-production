nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CP_MU_7Days_Access.js
//	Script Name:	CLGX_SS_CP_MU_7Days_Access
//	Script Id:		customscript_clgx_ss_cp_mu_7days_access
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		8/13/2016
//-------------------------------------------------------------------------------------------------

function scheduled_cp_mu_7days_access(){
    try{
        
        var context = nlapiGetContext();
        var start = moment();
        
        var searchResults = nlapiSearchRecord('customrecord_clgx_modifier', 'customsearch_clgx_cp_mu_7days_access');
        
        for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
        	
        	var loop = moment();
        	var minutes = (loop.diff(start)/60000).toFixed(1);
        	
            if ( context.getRemainingUsage() <= 100 || minutes > 50 || i > 990 ){
            	var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                break; 
            }
            
			var id = searchResults[i].getValue('internalid',null,null);
			
	        var fields = ['custrecord_clgx_modifier_password','custrecord_clgx_modifier_password_date','custrecord_clgx_modifier_password_redate','custrecord_clgx_modifier_accepted_terms'];
	    	var values = ['',null,null,'F'];
	    	nlapiSubmitField('customrecord_clgx_modifier', id, fields, values);
        }
    }
    catch (error){
    	
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
