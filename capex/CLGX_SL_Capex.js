nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Capex.js
//	Script Name:	CLGX_SL_Capex
//	Script Id:		customscript_clgx_sl_capex
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/04/2017
//	URL:			/app/site/hosting/scriptlet.nl?script=764&deploy=1  
//-------------------------------------------------------------------------------------------------

function suitelet_capex (request, response){
    try {
    	
        //var searchProj = nlapiSearchRecord('job', 'customsearch_clgx_rep_cpx_projects', arrFiltersProjects, null);
        //var searchBudgetProj = nlapiSearchRecord('customrecord_clgx_budget', 'customsearch_clgx_rep_cpx_budget', null, null);
        //var searchForecastProj = nlapiSearchRecord('customrecord_clgx_cap_bud_forecast', 'customsearch_clgx_rep_cpx_forecasts', null, null);
        
        //var searchPOs = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_pos', arrFiltersPOs, null);
        //var searchActuals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFiltersActuals, null);
        //var searchPOs = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_global_pos', arrFiltersPOs, null);
        //var searchActuals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_global_actuals', arrFiltersActuals, null);

        var search = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_pos', null, null);
        var pos = [];
        for ( var i = 0; search != null && i < search.length; i++ ) {
        	var row = search[i];
            var columns = row.getAllColumns();
            var vals = [];
            vals.push(parseInt(row.getValue(columns[0])));
            for ( var c = 1; c < columns.length; c++ ) {
            	vals.push(parseFloat(row.getValue(columns[c])));
            }
            pos.push(vals)
        }
    	
    	var search = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', null, null);
        var act = [];
        for ( var i = 0; search != null && i < search.length; i++ ) {
        	var row = search[i];
            var columns = row.getAllColumns();
            var vals = [];
            vals.push(parseInt(row.getValue(columns[0])));
            for ( var c = 1; c < columns.length; c++ ) {
            	vals.push(parseFloat(row.getValue(columns[c])));
            }
            act.push(vals);
        }
    	
    	var search = nlapiSearchRecord('job', 'customsearch_clgx_rep_cpx_projects', null, null);
        var jobs = [];
        for ( var i = 0; search != null && i < search.length; i++ ) {
        	jobs.push({
        		"inactive": search[i].getValue('isinactive',null,null) || 'T',
        		"subsidiary": cleanStr (search[i].getText('subsidiarynohierarchy',null,null) || ''),
        		"facility": search[i].getText('custentity_cologix_facility',null,null) || '',
        		"category": search[i].getText('custentity_clgx_capex_category',null,null) || '',
        		"projectid": parseInt(search[i].getValue('internalid',null,null)) || 0,
        		"project": cleanStr (search[i].getValue('entityid',null,null) || 'T'),
        		"description": cleanStr (search[i].getValue('jobname',null,null) || 'T')
            });  
        }
    	
        var projects = [];
        for ( var i = 0; i < jobs.length; i++ ) {
        	
        	var po = _.find(pos, function(arr){ return (arr[0] == jobs[i].projectid) ; });
        	var ac = _.find(act, function(arr){ return (arr[0] == jobs[i].projectid) ; });
        	
        	projects.push({
        		
        		"projectid": jobs[i].projectid,
        		"project": jobs[i].project,
        		"inactive": jobs[i].inactive,
        		"subsidiary": jobs[i].subsidiary,
        		"facility": jobs[i].facility,
        		"category": jobs[i].category,
        		"description": jobs[i].description,
        		
        		"pos_billed": (po ? po[1] : 0),
        		"pos_unbilled": (po ? po[2] : 0),
        		
        		"pos_TOTAL": (po ? po[3] : 0),
        		"pos_TOT2011": (po ? po[4] : 0),
    	        "pos_JAN2012": (po ? po[5] : 0),
    	        "pos_FEB2012": (po ? po[6] : 0),
    	        "pos_MAR2012": (po ? po[7] : 0),
    	        "pos_APR2012": (po ? po[8] : 0),
    	        "pos_MAY2012": (po ? po[9] : 0),
    	        "pos_JUN2012": (po ? po[10] : 0),
    	        "pos_JUL2012": (po ? po[11] : 0),
    	        "pos_AUG2012": (po ? po[12] : 0),
    	        "pos_SEP2012": (po ? po[13] : 0),
    	        "pos_OCT2012": (po ? po[14] : 0),
    	        "pos_NOV2012": (po ? po[15] : 0),
    	        "pos_DEC2012": (po ? po[16] : 0),
    	        "pos_TOT2012": (po ? po[17] : 0),
    	        "pos_JAN2013": (po ? po[18] : 0),
    	        "pos_FEB2013": (po ? po[19] : 0),
    	        "pos_MAR2013": (po ? po[20] : 0),
    	        "pos_APR2013": (po ? po[21] : 0),
    	        "pos_MAY2013": (po ? po[22] : 0),
    	        "pos_JUN2013": (po ? po[23] : 0),
    	        "pos_JUL2013": (po ? po[24] : 0),
    	        "pos_AUG2013": (po ? po[25] : 0),
    	        "pos_SEP2013": (po ? po[26] : 0),
    	        "pos_OCT2013": (po ? po[27] : 0),
    	        "pos_NOV2013": (po ? po[28] : 0),
    	        "pos_DEC2013": (po ? po[29] : 0),
    	        "pos_TOT2013": (po ? po[30] : 0),
    	        "pos_JAN2014": (po ? po[31] : 0),
    	        "pos_FEB2014": (po ? po[32] : 0),
    	        "pos_MAR2014": (po ? po[33] : 0),
    	        "pos_APR2014": (po ? po[34] : 0),
    	        "pos_MAY2014": (po ? po[35] : 0),
    	        "pos_JUN2014": (po ? po[36] : 0),
    	        "pos_JUL2014": (po ? po[37] : 0),
    	        "pos_AUG2014": (po ? po[38] : 0),
    	        "pos_SEP2014": (po ? po[39] : 0),
    	        "pos_OCT2014": (po ? po[40] : 0),
    	        "pos_NOV2014": (po ? po[41] : 0),
    	        "pos_DEC2014": (po ? po[42] : 0),
    	        "pos_TOT2014": (po ? po[43] : 0),
    	        "pos_JAN2015": (po ? po[44] : 0),
    	        "pos_FEB2015": (po ? po[45] : 0),
    	        "pos_MAR2015": (po ? po[46] : 0),
    	        "pos_APR2015": (po ? po[47] : 0),
    	        "pos_MAY2015": (po ? po[48] : 0),
    	        "pos_JUN2015": (po ? po[49] : 0),
    	        "pos_JUL2015": (po ? po[50] : 0),
    	        "pos_AUG2015": (po ? po[51] : 0),
    	        "pos_SEP2015": (po ? po[52] : 0),
    	        "pos_OCT2015": (po ? po[53] : 0),
    	        "pos_NOV2015": (po ? po[54] : 0),
    	        "pos_DEC2015": (po ? po[55] : 0),
    	        "pos_TOT2015": (po ? po[56] : 0),
    	        "pos_JAN2016": (po ? po[57] : 0),
    	        "pos_FEB2016": (po ? po[58] : 0),
    	        "pos_MAR2016": (po ? po[59] : 0),
    	        "pos_APR2016": (po ? po[60] : 0),
    	        "pos_MAY2016": (po ? po[61] : 0),
    	        "pos_JUN2016": (po ? po[62] : 0),
    	        "pos_JUL2016": (po ? po[63] : 0),
    	        "pos_AUG2016": (po ? po[64] : 0),
    	        "pos_SEP2016": (po ? po[65] : 0),
    	        "pos_OCT2016": (po ? po[66] : 0),
    	        "pos_NOV2016": (po ? po[67] : 0),
    	        "pos_DEC2016": (po ? po[68] : 0),
    	        "pos_TOT2016": (po ? po[69] : 0),
    	        "pos_JAN2017": (po ? po[70] : 0),
    	        "pos_FEB2017": (po ? po[71] : 0),
    	        "pos_MAR2017": (po ? po[72] : 0),
    	        "pos_APR2017": (po ? po[73] : 0),
    	        "pos_MAY2017": (po ? po[74] : 0),
    	        "pos_JUN2017": (po ? po[75] : 0),
    	        "pos_JUL2017": (po ? po[76] : 0),
    	        "pos_AUG2017": (po ? po[77] : 0),
    	        "pos_SEP2017": (po ? po[78] : 0),
    	        "pos_OCT2017": (po ? po[79] : 0),
    	        "pos_NOV2017": (po ? po[80] : 0),
    	        "pos_DEC2017": (po ? po[81] : 0),
    	        "pos_TOT2017": (po ? po[82] : 0),
    	        
    	        "act_TOTAL": (ac ? ac[1] : 0),
        		"act_TOT2011": (ac ? ac[2] : 0),
    	        "act_JAN2012": (ac ? ac[3] : 0),
    	        "act_FEB2012": (ac ? ac[4] : 0),
    	        "act_MAR2012": (ac ? ac[5] : 0),
    	        "act_APR2012": (ac ? ac[6] : 0),
    	        "act_MAY2012": (ac ? ac[7] : 0),
    	        "act_JUN2012": (ac ? ac[8] : 0),
    	        "act_JUL2012": (ac ? ac[9] : 0),
    	        "act_AUG2012": (ac ? ac[10] : 0),
    	        "act_SEP2012": (ac ? ac[11] : 0),
    	        "act_OCT2012": (ac ? ac[12] : 0),
    	        "act_NOV2012": (ac ? ac[13] : 0),
    	        "act_DEC2012": (ac ? ac[14] : 0),
    	        "act_TOT2012": (ac ? ac[15] : 0),
    	        "act_JAN2013": (ac ? ac[16] : 0),
    	        "act_FEB2013": (ac ? ac[17] : 0),
    	        "act_MAR2013": (ac ? ac[18] : 0),
    	        "act_APR2013": (ac ? ac[19] : 0),
    	        "act_MAY2013": (ac ? ac[20] : 0),
    	        "act_JUN2013": (ac ? ac[21] : 0),
    	        "act_JUL2013": (ac ? ac[22] : 0),
    	        "act_AUG2013": (ac ? ac[23] : 0),
    	        "act_SEP2013": (ac ? ac[24] : 0),
    	        "act_OCT2013": (ac ? ac[25] : 0),
    	        "act_NOV2013": (ac ? ac[26] : 0),
    	        "act_DEC2013": (ac ? ac[27] : 0),
    	        "act_TOT2013": (ac ? ac[28] : 0),
    	        "act_JAN2014": (ac ? ac[29] : 0),
    	        "act_FEB2014": (ac ? ac[30] : 0),
    	        "act_MAR2014": (ac ? ac[31] : 0),
    	        "act_APR2014": (ac ? ac[32] : 0),
    	        "act_MAY2014": (ac ? ac[33] : 0),
    	        "act_JUN2014": (ac ? ac[34] : 0),
    	        "act_JUL2014": (ac ? ac[35] : 0),
    	        "act_AUG2014": (ac ? ac[36] : 0),
    	        "act_SEP2014": (ac ? ac[37] : 0),
    	        "act_OCT2014": (ac ? ac[38] : 0),
    	        "act_NOV2014": (ac ? ac[39] : 0),
    	        "act_DEC2014": (ac ? ac[40] : 0),
    	        "act_TOT2014": (ac ? ac[41] : 0),
    	        "act_JAN2015": (ac ? ac[42] : 0),
    	        "act_FEB2015": (ac ? ac[43] : 0),
    	        "act_MAR2015": (ac ? ac[44] : 0),
    	        "act_APR2015": (ac ? ac[45] : 0),
    	        "act_MAY2015": (ac ? ac[46] : 0),
    	        "act_JUN2015": (ac ? ac[47] : 0),
    	        "act_JUL2015": (ac ? ac[48] : 0),
    	        "act_AUG2015": (ac ? ac[49] : 0),
    	        "act_SEP2015": (ac ? ac[50] : 0),
    	        "act_OCT2015": (ac ? ac[51] : 0),
    	        "act_NOV2015": (ac ? ac[52] : 0),
    	        "act_DEC2015": (ac ? ac[53] : 0),
    	        "act_TOT2015": (ac ? ac[54] : 0),
    	        "act_JAN2016": (ac ? ac[55] : 0),
    	        "act_FEB2016": (ac ? ac[56] : 0),
    	        "act_MAR2016": (ac ? ac[57] : 0),
    	        "act_APR2016": (ac ? ac[58] : 0),
    	        "act_MAY2016": (ac ? ac[59] : 0),
    	        "act_JUN2016": (ac ? ac[60] : 0),
    	        "act_JUL2016": (ac ? ac[61] : 0),
    	        "act_AUG2016": (ac ? ac[62] : 0),
    	        "act_SEP2016": (ac ? ac[63] : 0),
    	        "act_OCT2016": (ac ? ac[64] : 0),
    	        "act_NOV2016": (ac ? ac[65] : 0),
    	        "act_DEC2016": (ac ? ac[66] : 0),
    	        "act_TOT2016": (ac ? ac[67] : 0),
    	        "act_JAN2017": (ac ? ac[68] : 0),
    	        "act_FEB2017": (ac ? ac[69] : 0),
    	        "act_MAR2017": (ac ? ac[70] : 0),
    	        "act_APR2017": (ac ? ac[71] : 0),
    	        "act_MAY2017": (ac ? ac[72] : 0),
    	        "act_JUN2017": (ac ? ac[73] : 0),
    	        "act_JUL2017": (ac ? ac[74] : 0),
    	        "act_AUG2017": (ac ? ac[75] : 0),
    	        "act_SEP2017": (ac ? ac[76] : 0),
    	        "act_OCT2017": (ac ? ac[77] : 0),
    	        "act_NOV2017": (ac ? ac[78] : 0),
    	        "act_DEC2017": (ac ? ac[79] : 0),
    	        "act_TOT2017": (ac ? ac[80] : 0)
            });
        }
        
        
    	var objFile = nlapiLoadFile(4785754);
        var html = objFile.getValue();

        html = html.replace(new RegExp('{dataPeriods}','g'), JSON.stringify(projects));
        
        response.write(html);
        
    }
    catch (error) {
        if (error.getDetails !== undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function cleanStr (str){
    var string = '';
    string = str.replace(/\'/g," ");
    string = string.replace(/\"/g," ");
    string = string.replace(/\,/g," ");
    string = string.replace(/\;/g," ");
    return string;
}
