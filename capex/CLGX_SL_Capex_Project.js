nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Capex_Project.js
//	Script Name:	CLGX_SL_Capex_Project
//	Script Id:		customscript_clgx_sl_capex_project
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/04/2017
//	URL:			/app/site/hosting/scriptlet.nl?script=760&deploy=1  
//-------------------------------------------------------------------------------------------------

function suitelet_capex_project(request, response){
	
    try {
    	
    	var projid = request.getParameter('projid');
    	
    	var project = nlapiLookupField('customer', projid, 'entityid');
            	
    	var objFile = nlapiLoadFile(4778110);
        var html = objFile.getValue();

        html = html.replace(new RegExp('{dataPOs}','g'), JSON.stringify(get_pos(projid)));
        html = html.replace(new RegExp('{dataBills}','g'), JSON.stringify(get_bills(projid)));
        html = html.replace(new RegExp('{dataBillsCredits}','g'), JSON.stringify(get_credits(projid)));
        html = html.replace(new RegExp('{dataJournals}','g'), JSON.stringify(get_journals(projid)));
        html = html.replace(new RegExp('{dataJournalsCapLabor}','g'), JSON.stringify(get_cap_labor(projid)));
        html = html.replace(new RegExp('{dataPeriods}','g'), JSON.stringify(get_totals(projid)));
        html = html.replace(new RegExp('{project}','g'), project);
        
        response.write(html);
        
    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function get_totals (projid){
	
	var arr = [];
	
	// 0
    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter('recordType',null,'is','vendorbill'));
    filters.push(new nlobjSearchFilter('class',null,'noneof',3));
    filters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var search = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', filters, columns);
    var vals = [];
    for ( var i = 0; search != null && i < search.length; i++ ) {
        var row = search[i];
        var columns = row.getAllColumns();
        for ( var c = 1; c < columns.length; c++ ) {
        	vals.push(parseFloat(row.getValue(columns[c])));
        }
    }
    arr.push({
		"type": 'Bills',
		"vals": vals
    });

    // 1
    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter('recordType',null,'is','vendorcredit'));
    filters.push(new nlobjSearchFilter('class',null,'noneof',3));
    filters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var search = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', filters, columns);
    var vals = [];
    for ( var i = 0; search != null && i < search.length; i++ ) {
        var row = search[i];
        var columns = row.getAllColumns();
        for ( var c = 1; c < columns.length; c++ ) {
        	vals.push(parseFloat(row.getValue(columns[c])));
        }
    }
    arr.push({
		"type": 'Bills Credit',
		"vals": vals
    });
    
    // 2
    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter('class',null,'noneof',3));
    filters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var search = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actual_j', filters, columns);
    var vals = [];
    for ( var i = 0; search != null && i < search.length; i++ ) {
        var row = search[i];
        var columns = row.getAllColumns();
        for ( var c = 1; c < columns.length; c++ ) {
        	vals.push(parseFloat(row.getValue(columns[c])));
        }
    }
    arr.push({
		"type": 'Journal',
		"vals": vals
    });
    
    // 3
    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter('class',null,'anyof',3));
    filters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var search = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actual_j', filters, columns);
    var vals = [];
    for ( var i = 0; search != null && i < search.length; i++ ) {
        var row = search[i];
        var columns = row.getAllColumns();
        for ( var c = 1; c < columns.length; c++ ) {
        	vals.push(parseFloat(row.getValue(columns[c])));
        }
    }
    arr.push({
		"type": 'Cap Labor',
		"vals": vals
    });  
    
    // 4
    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var search = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', filters, columns);
    var vals = [];
    for ( var i = 0; search != null && i < search.length; i++ ) {
        var row = search[i];
        var columns = row.getAllColumns();
        for ( var c = 1; c < columns.length; c++ ) {
        	vals.push(parseFloat(row.getValue(columns[c])));
        }
    }
    arr.push({
		"type": 'Total Actuals',
		"vals": vals
    });
    /*
    arr.push({
		"type": 'Forecasts',
		"vals": vals
    });
    */
    
    // 5
    var columns = new Array();
    var filters = new Array();
    filters.push(new nlobjSearchFilter('custbody_cologix_project_name',null,'is',projid));
    var search = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_pos', filters, columns);
    var vals = [];
    for ( var i = 0; search != null && i < search.length; i++ ) {
        var row = search[i];
        var columns = row.getAllColumns();
        vals.push(parseFloat(row.getValue(columns[2])));
        for ( var c = 4; c < columns.length; c++ ) {
        	vals.push(parseFloat(row.getValue(columns[c])));
        }
    }
    arr.push({
		"type": 'Unbilled POs',
		"vals": vals
    });
    
    // 6
    var vals = [];
    for ( var i = 0; search != null && i < search.length; i++ ) {
        var row = search[i];
        var columns = row.getAllColumns();
        vals.push(parseFloat(row.getValue(columns[1])));
        for ( var c = 4; c < columns.length; c++ ) {
        	vals.push(parseFloat(row.getValue(columns[c])));
        }
    }
    arr.push({
		"type": 'Billed POs',
		"vals": vals
    });
    
    // 7
    var vals = [];
    for ( var i = 0; search != null && i < search.length; i++ ) {
        var row = search[i];
        var columns = row.getAllColumns();
        vals.push(parseFloat(row.getValue(columns[3])));
        for ( var c = 4; c < columns.length; c++ ) {
        	vals.push(parseFloat(row.getValue(columns[c])));
        }
    }
    arr.push({
		"type": 'Total POs',
		"vals": vals
    });
    
    /*
    // 8
    var vals = [];
    for ( var c = 0; c < 80; c++ ) {
    	vals.push(arr[5].vals[c] + arr[8].vals[c]);
    }
    arr.push({
		"type": 'Committed Capital',
		"vals": vals
    });
    
    
    var vals = [];
    for ( var c = 0; c < 80; c++ ) {
    	vals.push(0);
    }
    arr.push({
		"type": 'Budget',
		"vals": vals
    });
    
    var vals = [];
    for ( var c = 0; c < 80; c++ ) {
    	vals.push(arr[10].vals[c] - arr[9].vals[c]);
    }
    arr.push({
		"type": 'Committed Capital Variance',
		"vals": vals
    });
    
    var vals = [];
    for ( var c = 0; c < 80; c++ ) {
    	vals.push(arr[5].vals[c] + arr[6].vals[c] + arr[8].vals[c]);
    }
    arr.push({
		"type": 'Total Committed and Projected',
		"vals": vals
    });
    
    var vals = [];
    for ( var c = 0; c < 80; c++ ) {
    	vals.push(arr[10].vals[c] - arr[12].vals[c]);
    }
    arr.push({
		"type": 'Total Project Variance',
		"vals": vals
    });
    
    
    var vals = [];
    for ( var c = 0; c < 80; c++ ) {
    	vals.push(arr[10].vals[c] - arr[5].vals[c]);
    }
    arr.push({
		"type": 'Budget Variance',
		"vals": vals
    });
    */
    
    var output = [];
    for ( var i = 0; i < arr.length; i++ ) {
    	output.push({
    	       	"type": arr[i].type,
    	       	"TOT2011": arr[i].vals[1],
    	        "JAN2012": arr[i].vals[2],
    	        "FEB2012": arr[i].vals[3],
    	        "MAR2012": arr[i].vals[4],
    	        "APR2012": arr[i].vals[5],
    	        "MAY2012": arr[i].vals[6],
    	        "JUN2012": arr[i].vals[7],
    	        "JUL2012": arr[i].vals[8],
    	        "AUG2012": arr[i].vals[9],
    	        "SEP2012": arr[i].vals[10],
    	        "OCT2012": arr[i].vals[11],
    	        "NOV2012": arr[i].vals[12],
    	        "DEC2012": arr[i].vals[13],
    	        "TOT2012": arr[i].vals[14],
    	        "JAN2013": arr[i].vals[15],
    	        "FEB2013": arr[i].vals[16],
    	        "MAR2013": arr[i].vals[17],
    	        "APR2013": arr[i].vals[18],
    	        "MAY2013": arr[i].vals[19],
    	        "JUN2013": arr[i].vals[20],
    	        "JUL2013": arr[i].vals[21],
    	        "AUG2013": arr[i].vals[22],
    	        "SEP2013": arr[i].vals[23],
    	        "OCT2013": arr[i].vals[24],
    	        "NOV2013": arr[i].vals[25],
    	        "DEC2013": arr[i].vals[26],
    	        "TOT2013": arr[i].vals[27],
    	        "JAN2014": arr[i].vals[28],
    	        "FEB2014": arr[i].vals[29],
    	        "MAR2014": arr[i].vals[30],
    	        "APR2014": arr[i].vals[31],
    	        "MAY2014": arr[i].vals[32],
    	        "JUN2014": arr[i].vals[33],
    	        "JUL2014": arr[i].vals[34],
    	        "AUG2014": arr[i].vals[35],
    	        "SEP2014": arr[i].vals[36],
    	        "OCT2014": arr[i].vals[37],
    	        "NOV2014": arr[i].vals[38],
    	        "DEC2014": arr[i].vals[39],
    	        "TOT2014": arr[i].vals[40],
    	        "JAN2015": arr[i].vals[41],
    	        "FEB2015": arr[i].vals[42],
    	        "MAR2015": arr[i].vals[43],
    	        "APR2015": arr[i].vals[44],
    	        "MAY2015": arr[i].vals[45],
    	        "JUN2015": arr[i].vals[46],
    	        "JUL2015": arr[i].vals[47],
    	        "AUG2015": arr[i].vals[48],
    	        "SEP2015": arr[i].vals[49],
    	        "OCT2015": arr[i].vals[50],
    	        "NOV2015": arr[i].vals[51],
    	        "DEC2015": arr[i].vals[52],
    	        "TOT2015": arr[i].vals[53],
    	        "JAN2016": arr[i].vals[54],
    	        "FEB2016": arr[i].vals[55],
    	        "MAR2016": arr[i].vals[56],
    	        "APR2016": arr[i].vals[57],
    	        "MAY2016": arr[i].vals[58],
    	        "JUN2016": arr[i].vals[59],
    	        "JUL2016": arr[i].vals[60],
    	        "AUG2016": arr[i].vals[61],
    	        "SEP2016": arr[i].vals[62],
    	        "OCT2016": arr[i].vals[63],
    	        "NOV2016": arr[i].vals[64],
    	        "DEC2016": arr[i].vals[65],
    	        "TOT2016": arr[i].vals[66],
    	        "JAN2017": arr[i].vals[67],
    	        "FEB2017": arr[i].vals[68],
    	        "MAR2017": arr[i].vals[69],
    	        "APR2017": arr[i].vals[70],
    	        "MAY2017": arr[i].vals[71],
    	        "JUN2017": arr[i].vals[72],
    	        "JUL2017": arr[i].vals[73],
    	        "AUG2017": arr[i].vals[74],
    	        "SEP2017": arr[i].vals[75],
    	        "OCT2017": arr[i].vals[76],
    	        "NOV2017": arr[i].vals[77],
    	        "DEC2017": arr[i].vals[78],
    	        "TOT2017": arr[i].vals[79],
    	        "TOTAL": arr[i].vals[0]
    	});
    }

    return output;
}

function get_pos (projid){

    var columns = [];
    columns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
    columns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    columns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
    columns.push(new nlobjSearchColumn('companyname','vendor','GROUP'));
    columns.push(new nlobjSearchColumn('internalid','vendor','GROUP'));
    var filters = [];
    filters.push(new nlobjSearchFilter('custbody_cologix_project_name',null,'is',projid));
    var search = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_pos', filters, columns);

    var arr = [];
    var values = [];
    for ( var i = 0; search != null && i < search.length; i++ ) {
        var row = search[i];
        var columns = row.getAllColumns();
        arr.push({
	    	"periodid": parseInt(row.getValue('postingperiod',null,'GROUP')) || 0,
	    	"period": row.getText('postingperiod',null,'GROUP') || '',
	    	"number": cleanStr (row.getValue('tranid',null,'GROUP')) || '',
	    	"internalid": parseInt(row.getValue('internalid',null,'GROUP')) || 0,
	    	"vendorid": parseInt(row.getValue('internalid','vendor','GROUP')) || 0,
	    	"vendor": cleanStr (row.getValue('companyname','vendor','GROUP')) || '',
	    	"billed": parseFloat(row.getValue(columns[6])) || 0,
	    	"unbilled": parseFloat(row.getValue(columns[7])) || 0,
	    	"total": parseFloat(row.getValue('fxamount',null,'SUM')) || 0
        });
    }
    return arr;
}

function get_bills (projid){

    var columns = [];
    columns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
    columns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    columns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
    columns.push(new nlobjSearchColumn('companyname','vendor','GROUP'));
    columns.push(new nlobjSearchColumn('internalid','vendor','GROUP'));
    columns.push(new nlobjSearchColumn('internalid','appliedtotransaction','MAX'));
    columns.push(new nlobjSearchColumn('tranid','appliedtotransaction','MAX'));
    var filters = [];
    filters.push(new nlobjSearchFilter('recordType',null,'is','vendorbill'));
    filters.push(new nlobjSearchFilter('class',null,'noneof',3));
    filters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var search = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', filters, columns);

    var arr = [];
    for ( var i = 0; search != null && i < search.length; i++ ) {
        var row = search[i];
        arr.push({
        	"periodid": parseInt(row.getValue('postingperiod',null,'GROUP')) || 0,
        	"period": row.getText('postingperiod',null,'GROUP') || '',
        	"number": cleanStr (row.getValue('tranid',null,'GROUP')) || '',
        	"internalid": parseInt(row.getValue('internalid',null,'GROUP')) || 0,
        	"vendorid": parseInt(row.getValue('internalid','vendor','GROUP')) || 0,
        	"vendor": cleanStr (row.getValue('companyname','vendor','GROUP')) || '',
        	"ponbr": parseInt(row.getValue('tranid','appliedtotransaction','MAX')) || 0,
        	"poid": parseInt(row.getValue('internalid','appliedtotransaction','MAX')) || 0,
        	"amount": parseFloat(row.getValue('fxamount',null,'SUM')) || 0
        });
    }
    return arr;
}

function get_credits (projid){

    var columns = [];
    columns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
    columns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    columns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
    columns.push(new nlobjSearchColumn('companyname','vendor','GROUP'));
    columns.push(new nlobjSearchColumn('internalid','vendor','GROUP'));
    columns.push(new nlobjSearchColumn('internalid','appliedtotransaction','MAX'));
    columns.push(new nlobjSearchColumn('tranid','appliedtotransaction','MAX'));
    var filters = [];
    filters.push(new nlobjSearchFilter('recordType',null,'is','vendorcredit'));
    filters.push(new nlobjSearchFilter('class',null,'noneof',3));
    filters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var search = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', filters, columns);

    var arr = [];
    for ( var i = 0; search != null && i < search.length; i++ ) {
    	var row = search[i];
        arr.push({
        	"periodid": parseInt(row.getValue('postingperiod',null,'GROUP')) || 0,
        	"period": row.getText('postingperiod',null,'GROUP') || '',
        	"number": cleanStr (row.getValue('tranid',null,'GROUP')) || '',
        	"internalid": parseInt(row.getValue('internalid',null,'GROUP')) || 0,
        	"vendorid": parseInt(row.getValue('internalid','vendor','GROUP')) || 0,
        	"vendor": cleanStr (row.getValue('companyname','vendor','GROUP')) || '',
        	"ponbr": parseInt(row.getValue('tranid','appliedtotransaction','MAX')) || 0,
        	"poid": parseInt(row.getValue('internalid','appliedtotransaction','MAX')) || 0,
        	"amount": parseFloat(row.getValue('fxamount',null,'SUM')) || 0
        });
    	
    }
    return arr;
}

function get_journals (projid){

    var columns = new Array();
    columns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
    columns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    columns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
    columns.push(new nlobjSearchColumn('memo',null,'GROUP'));
    var filters = new Array();
    filters.push(new nlobjSearchFilter('class',null,'noneof',3));
    filters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var search = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actual_j', filters, columns);

    var arr = new Array();
    for ( var i = 0; search != null && i < search.length; i++ ) {
        var row = search[i];
        arr.push({
        	"periodid": parseInt(row.getValue('postingperiod',null,'GROUP')) || 0,
        	"period": row.getText('postingperiod',null,'GROUP') || '',
        	"internalid": parseInt(row.getValue('internalid',null,'GROUP')) || 0,
        	"number": row.getValue('tranid',null,'GROUP') || '',
        	"memo": cleanStr (row.getValue('memo',null,'GROUP')) || '',
        	"amount": parseFloat(row.getValue('fxamount',null,'SUM')) || 0
        });
    }
    return arr;
}

function get_cap_labor (projid){

    var columns = new Array();
    columns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
    columns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    columns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
    columns.push(new nlobjSearchColumn('memo',null,'GROUP'));
    var filters = new Array();
    filters.push(new nlobjSearchFilter('class',null,'anyof',3));
    filters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var search = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actual_j', filters, columns);

    var arr = new Array();
    for ( var i = 0; search != null && i < search.length; i++ ) {
    	var row = search[i];
        arr.push({
        	"periodid": parseInt(row.getValue('postingperiod',null,'GROUP')) || 0,
        	"period": row.getText('postingperiod',null,'GROUP') || '',
        	"number": row.getValue('tranid',null,'GROUP') || '',
        	"internalid": parseInt(row.getValue('internalid',null,'GROUP')) || 0,
        	"memo": cleanStr (row.getValue('memo',null,'GROUP')) || '',
        	"amount": parseFloat(row.getValue('fxamount',null,'SUM')) || 0
        });
        
    }
    return arr;
}

function cleanStr (str){
	var string  = str.replace(/[\|&;\$%@"<>\(\)\+,]/g, "")
    return string;
}

