//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Test.js
//	Script Name:	CLGX_SL_Test
//	Script Id:		customscript_clgx_sl_test
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/11/2013
//-------------------------------------------------------------------------------------------------

function suitelet_test (request, response){
	try {
		
		var html = '';
		
		var arrOldData = new Array();
		for ( var i = 1; i < 6; i++ ) {
			var objOldData = new Object();
			objOldData["internalid"] = i;
			objOldData["externalid"] = i + 10;
			//objOldData["name"] = 'name' + i;
			arrOldData.push(objOldData);
		}
		html += '<br>Old Data<br>';
		var strOldData = JSON.stringify(arrOldData);
		html += strOldData;
		
		var arrNewData = new Array();
		for ( var i = 4; i < 9; i++ ) {
			var objNewData = new Object();
			//objNewData["internalid"] = i;
			objNewData["externalid"] = i + 10;
			objNewData["name"] = 'name' + i;
			arrNewData.push(objNewData);
		}
		
		html += '<br><br>New Data<br>';
		var strNewData = JSON.stringify(arrNewData);
		html += strNewData;
	
		
		
		
		
		
		html += '<br><br>arrOldInternalIDs<br>';
		var arrOldInternalIDs = _.pluck(arrOldData, 'internalid');
		var strOldInternalIDs = JSON.stringify(arrOldInternalIDs);
		html += strOldInternalIDs;
		
		html += '<br><br>arrOldExternalIDs<br>';
		var arrOldExternalIDs = _.pluck(arrOldData, 'externalid');
		var strOldExternalIDs = JSON.stringify(arrOldExternalIDs);
		html += strOldExternalIDs;
		
		
		html += '<br><br>arrNewExternalIDs<br>';
		var arrNewExternalIDs = _.pluck(arrNewData, 'externalid');
		var strNewExternalIDs = JSON.stringify(arrNewExternalIDs);
		html += strNewExternalIDs;
		
		html += '<br><br>arrNewNames<br>';
		var arrNewNames = _.pluck(arrNewData, 'name');
		var strNewNames = JSON.stringify(arrNewNames);
		html += strNewNames;
		
		
		
		
		
		
		
		
		
		
		html += '<br><br>arrAdd<br>';
		var arrAdd = _.difference(arrNewExternalIDs, arrOldExternalIDs);
		var strAdd = JSON.stringify(arrAdd);
		html += strAdd;
		
		html += '<br><br>arrDelete<br>';
		var arrDelete = _.difference(arrOldExternalIDs, arrNewExternalIDs);
		var strDelete = JSON.stringify(arrDelete);
		html += strDelete;
		
		html += '<br><br>arrUpdate<br>';
		var arrUpdate = _.intersection(arrNewExternalIDs, arrOldExternalIDs);
		var strUpdate = JSON.stringify(arrUpdate);
		html += strUpdate;
		
		
		
		
		
		
		


		html += '<br><br>LOOP ADD - External IDS / Names<br>';
		for ( var i = 0; arrAdd != null && i < arrAdd.length; i++ ) {
			html += arrAdd[i] + ' - ' + arrNewNames[_.indexOf(arrNewExternalIDs, arrAdd[i])] + '<br>';
		}

		html += '<br><br>LOOP DELETE - Internal IDS <br>';
		for ( var i = 0; arrDelete != null && i < arrDelete.length; i++ ) {
			html += arrOldInternalIDs[_.indexOf(arrOldExternalIDs, arrDelete[i])] + '<br>';
		}

		html += '<br><br>LOOP UPDATE - Internal IDS / NAMES<br>';
		for ( var i = 0; arrUpdate != null && i < arrUpdate.length; i++ ) {
			html += arrOldInternalIDs[_.indexOf(arrOldExternalIDs, arrUpdate[i])] + ' - ' + arrNewNames[_.indexOf(arrNewExternalIDs, arrUpdate[i])] + '<br>';
		}
		
		response.write(html);

//---------- End Sections ------------------------------------------------------------------------------------------------
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}