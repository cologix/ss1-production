nlapiLogExecution("audit","FLOStart",new Date().getTime());
function saveRecord(){
    try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 4/4/2013
// Details:	If Status is is Closed-Lost, Loss reason is mandatory
//-------------------------------------------------------------------------------------------------
        var allowSave = 1;
        var alertMsg = '';
        var allowSaveCase=nlapiGetFieldValue("custpage_clgx_case_allowsave");

  alert(allowSaveCase);
 return false;
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}