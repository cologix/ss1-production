nlapiLogExecution("audit","FLOStart",new Date().getTime());
function beforeLoad(type, form) {
    try {
        nlapiLogExecution('DEBUG','User Event - Before Load','|-------------STARTED--------------|');

            //add hidden field for allow save
            var allowSave = form.addField('custpage_clgx_case_allowsave', 'integer', 'AllowSave');
            form.getField('custpage_clgx_case_allowsave').setDisplayType('hidden');
          //  nlapiSetFieldValue("custpage_clgx_case_allowsave", 1);
        nlapiLogExecution('DEBUG','User Event - Befor Load','|-------------FINISHED--------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function beforeSubmit(type) {
    try {
        nlapiLogExecution('DEBUG','User Event - Before Submit','|-------------STARTED--------------|');
        //   nlapiSetFieldValue('email','mbyers@telesphere.com111');
        //   nlapiSetFieldValue("custpage_clgx_case_allowsave", 10);


        var record = nlapiGetNewRecord();
        if ( type == 'create' )
        {
            record.setFieldValue('custpage_clgx_case_allowsave', 0);
        }

        //nlapiSetFieldValue("custpage_clgx_case_allowsave", 100);
            nlapiLogExecution('DEBUG','User Event - Befor Submit','|-------------FINISHED--------------|');
        }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
