nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_Devices_JSON.js
//	Script Name:	CLGX_SS_DCIM_Devices_JSON
//	Script Id:		customscript_clgx_ss_dcim_devices_json
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/19/2014
//-------------------------------------------------------------------------------------------------
function scheduled_dcim_devices_json(){
    try{
        //nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');

        var context = nlapiGetContext();
        var environment = context.getEnvironment();
        if(environment == 'PRODUCTION'){

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Date:	6/20/2014
// Details:	Construct JSON tree and grid JSON files
//-----------------------------------------------------------------------------------------------------------------

            var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/devices/get_nodes.cfm');
            //var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/getnodes.cfm');
            //var requestURL = nlapiRequestURL('https://command1.nnj2.cologix.net:10313/devices/tree',null,{'Content-type': 'application/json'},null,'GET');
            var jsonNodes = requestURL.body;
            var arrNodes= JSON.parse( jsonNodes );

            //var objFile = nlapiLoadFile(9352824);
            //var arrNodes = JSON.parse(objFile.getValue());

            var strTree = JSON.stringify(arrNodes);
            var jsonTree = nlapiCreateFile('devices_modius.json', 'PLAINTEXT', strTree);
            jsonTree.setFolder(1330014);
            nlapiSubmitFile(jsonTree);

            var arrNewDevices = new Array();
            var arrNewDevicesExternalIDs = new Array();
            var arrNewDevicesNames = new Array();

            var objTree = new Object();
            objTree["text"] = '.';

            // extract level 1 parents -------------------------------------------------------------------------------------------------------------
            var arrNL1 = _.filter(arrNodes, function(arr){
                return (arr.LEVEL === 1 && arr.TYPE === 'TREE');
            });

            var arrNodesLevel1 = new Array();
            for ( var i = 0; arrNL1 != null && i < arrNL1.length; i++ ) {
                if(arrNL1[i].CHILD.charAt(0) != '.'){
                    //if(arrNL1[i].CHILD.charAt(0) != '.' && arrNL1[i].CHILD.charAt(0) != '1'){

                    var objNL1 = new Object();
                    objNL1["node"] = arrNL1[i].CHILD;
                    objNL1["nodetype"] = 'market';
                    objNL1["nodeid"] = parseInt(arrNL1[i].CHILDID);
                    objNL1["faicon"] = getMarketIconSVG(arrNL1[i].CHILD);
                    objNL1["expanded"] = false;
                    //objNL1["iconCls"] = getMarketIcon(arrNL1[i].CHILD);
                    objNL1["leaf"] = false;

                    // extract level 2 parents -------------------------------------------------------------------------------------------------------------
                    var arrNL2 = _.filter(arrNodes, function(arr){
                        return (arr.LEVEL === 2 && arr.TYPE === 'TREE' && arr.PARENTID === objNL1.nodeid );
                    });
                    var arrNodesLevel2 = new Array();
                    for ( var j = 0; arrNL2 != null && j < arrNL2.length; j++ ) {
                        var objNL2 = new Object();
                        objNL2["node"] = arrNL2[j].CHILD;
                        objNL2["nodetype"] = 'facility';
                        objNL2["nodeid"] = parseInt(arrNL2[j].CHILDID);
                        objNL2["faicon"] = 'fa fa-building-o SteelBlue2';
                        objNL2["expanded"] = false;
                        //objNL2["iconCls"] = 'location';
                        objNL2["leaf"] = false;

                        // extract level 3 parents -------------------------------------------------------------------------------------------------------------
                        var arrNL3 = _.filter(arrNodes, function(arr){
                            return (arr.LEVEL === 3 && arr.TYPE === 'TREE' && arr.PARENTID === objNL2.nodeid );
                        });
                        var arrNodesLevel3 = new Array();
                        for ( var k = 0; arrNL3 != null && k < arrNL3.length; k++ ) {
                            var objNL3 = new Object();
                            objNL3["node"] = arrNL3[k].CHILD;
                            objNL3["nodetype"] = 'devices';
                            objNL3["nodeid"] = parseInt(arrNL3[k].CHILDID);
                            objNL3["faicon"] = 'fa fa-cogs SteelBlue2';
                            objNL3["expanded"] = false;
                            //objNL3["iconCls"] = 'group';
                            objNL3["leaf"] = false;

                            // extract level 4 parents -------------------------------------------------------------------------------------------------------------
                            var arrNL4 = _.filter(arrNodes, function(arr){
                                return (arr.LEVEL === 4 && arr.TYPE === 'TREE' && arr.PARENTID === objNL3.nodeid );
                            });
                            var arrNodesLevel4 = new Array();
                            for ( var l = 0; arrNL4 != null && l < arrNL4.length; l++ ) {
                                var objNL4 = new Object();
                                objNL4["node"] = arrNL4[l].CHILD;
                                objNL4["nodetype"] = 'devices';
                                objNL4["nodeid"] = parseInt(arrNL4[l].CHILDID);
                                objNL4["faicon"] = 'fa fa-cogs SteelBlue1';
                                objNL4["expanded"] = false;
                                //objNL4["iconCls"] = 'group';
                                objNL4["leaf"] = false;
                                if(objNL4["node"]=='MTL2 BCM UPS 300-2 Pnls'){
                                    var found=1;
                                }

                                // extract level 5 parents -------------------------------------------------------------------------------------------------------------
                                var arrNL5 = _.filter(arrNodes, function(arr){
                                    return (arr.LEVEL === 5 && arr.TYPE === 'TREE' && arr.PARENTID === objNL4.nodeid );
                                });
                                var arrNodesLevel5 = new Array();
                                for ( var m = 0; arrNL5 != null && m < arrNL5.length; m++ ) {
                                    var objNL5 = new Object();
                                    objNL5["node"] = arrNL5[m].CHILD;
                                    objNL5["nodetype"] = 'devices';
                                    objNL5["nodeid"] = parseInt(arrNL5[m].CHILDID);
                                    objNL5["faicon"] = 'fa fa-cogs SteelBlue1';
                                    objNL5["expanded"] = false;
                                    //objNL5["iconCls"] = 'group';
                                    objNL5["leaf"] = false;

                                    // extract level 6 children -------------------------------------------------------------------------------------------------------------
                                    var arrCL6 = _.filter(arrNodes, function(arr){
                                        return (arr.LEVEL === 6 && arr.TYPE === 'DEVICE' && arr.PARENTID === objNL5.nodeid );
                                    });
                                    var arrNodesLevel6 = new Array();
                                    for ( var n = 0; arrCL6 != null && n < arrCL6.length; n++ ) {
                                        var objCL6 = new Object();
                                        objCL6["node"] = arrCL6[n].DEVICE;
                                        objCL6["nodetype"] = 'device';
                                        objCL6["nodeid"] = parseInt(arrCL6[n].DEVICEID);
                                        objCL6["faicon"] = 'fa fa-cog SteelBlue1';
                                        objCL6["alarm"] = arrCL6[n].ALARM;
                                        if(arrCL6[n].EXTERNALID != '' && arrCL6[n].EXTERNALID != null && arrCL6[n].EXTERNALID != 'DEL00000'){
                                            objCL6["famid"] = Number(arrCL6[n].EXTERNALID.substring(3));
                                            objCL6["fam"] = arrCL6[n].EXTERNALID;
                                        }
                                        else{
                                            objCL6["famid"] = 0;
                                            objCL6["fam"] = '';
                                        }
                                        objCL6["facilityid"] = objNL2.nodeid;
                                        objCL6["facility"] = objNL2.node;
                                        //objCL6["iconCls"] = 'device';
                                        objCL6["leaf"] = true;
                                        objCL6["children"] = [];
                                        arrNodesLevel6.push(objCL6);

                                        // build device object and push it to devices array
                                        var objDevice = new Object();
                                        objDevice["name"] = arrCL6[n].DEVICE;
                                        objDevice["externalid"] = Number(arrCL6[n].DEVICEID);
                                        objDevice["facilityid"] = getNetsuiteFacility(objNL2.node);
                                        if(objDevice.facilityid > 0){
                                            arrNewDevices.push(objDevice);
                                        }
                                    }
                                    objNL5["children"] = arrNodesLevel6;
                                    arrNodesLevel5.push(objNL5);
                                }

                                // extract level 5 children -------------------------------------------------------------------------------------------------------------
                                var arrCL5 = _.filter(arrNodes, function(arr){
                                    return (arr.LEVEL === 5 && arr.TYPE === 'DEVICE' && arr.PARENTID === objNL4.nodeid );
                                });
                                for ( var m = 0; arrCL5 != null && m < arrCL5.length; m++ ) {
                                    var objCL5 = new Object();
                                    objCL5["node"] = arrCL5[m].DEVICE;
                                    objCL5["nodetype"] = 'device';
                                    objCL5["nodeid"] = parseInt(arrCL5[m].DEVICEID);
                                    objCL5["faicon"] = 'fa fa-cog SteelBlue1';
                                    objCL5["alarm"] = arrCL5[m].ALARM;
                                    if(arrCL5[m].EXTERNALID != '' && arrCL5[m].EXTERNALID != null && arrCL5[m].EXTERNALID != 'DEL00000'){
                                        objCL5["famid"] = Number(arrCL5[m].EXTERNALID.substring(3));
                                        objCL5["fam"] = arrCL5[m].EXTERNALID;
                                    }
                                    else{
                                        objCL5["famid"] = 0;
                                        objCL5["fam"] = '';
                                    }
                                    objCL5["facilityid"] = objNL2.nodeid;
                                    objCL5["facility"] = objNL2.node;
                                    //objCL5["iconCls"] = 'device';
                                    objCL5["leaf"] = true;
                                    objCL5["children"] = [];
                                    arrNodesLevel5.push(objCL5);

                                    // build device object and push it to devices array
                                    var objDevice = new Object();
                                    objDevice["name"] = arrCL5[m].DEVICE;
                                    objDevice["externalid"] = Number(arrCL5[m].DEVICEID);
                                    objDevice["facilityid"] = getNetsuiteFacility(objNL2.node);
                                    if(objDevice.facilityid > 0){
                                        arrNewDevices.push(objDevice);
                                    }
                                }
                                objNL4["children"] = arrNodesLevel5;
                                arrNodesLevel4.push(objNL4);
                            }
                            // extract level 4 children -------------------------------------------------------------------------------------------------------------
                            var arrCL4 = _.filter(arrNodes, function(arr){
                                return (arr.LEVEL === 4 && arr.TYPE === 'DEVICE' && arr.PARENTID === objNL3.nodeid );
                            });
                            for ( var l = 0; arrCL4 != null && l < arrCL4.length; l++ ) {
                                var objCL4 = new Object();
                                objCL4["node"] = arrCL4[l].DEVICE;
                                objCL4["nodetype"] = 'device';
                                objCL4["nodeid"] = parseInt(arrCL4[l].DEVICEID);
                                objCL4["faicon"] = 'fa fa-cog SteelBlue1';
                                objCL4["alarm"] = arrCL4[l].ALARM;
                                if(arrCL4[l].EXTERNALID != '' && arrCL4[l].EXTERNALID != null && arrCL4[l].EXTERNALID != 'DEL00000'){
                                    objCL4["famid"] = Number(arrCL4[l].EXTERNALID.substring(3));
                                    objCL4["fam"] = arrCL4[l].EXTERNALID;
                                }
                                else{
                                    objCL4["famid"] = 0;
                                    objCL4["fam"] = '';
                                }
                                objCL4["facilityid"] = objNL2.nodeid;
                                objCL4["facility"] = objNL2.node;
                                //objCL4["iconCls"] = 'device';
                                objCL4["leaf"] = true;
                                objCL4["children"] = [];
                                arrNodesLevel4.push(objCL4);

                                // build device object and push it to devices array
                                var objDevice = new Object();
                                objDevice["name"] = arrCL4[l].DEVICE;
                                objDevice["externalid"] = Number(arrCL4[l].DEVICEID);
                                objDevice["facilityid"] = getNetsuiteFacility(objNL2.node);
                                if(objDevice.facilityid > 0){
                                    arrNewDevices.push(objDevice);
                                }
                            }
                            objNL3["children"] = arrNodesLevel4;
                            arrNodesLevel3.push(objNL3);
                        }
                        // extract level 3 children -------------------------------------------------------------------------------------------------------------
                        var arrCL3 = _.filter(arrNodes, function(arr){
                            return (arr.LEVEL === 3 && arr.TYPE === 'DEVICE' && arr.PARENTID === objNL2.nodeid );
                        });
                        for ( var k = 0; arrCL3 != null && k < arrCL3.length; k++ ) {
                            var objCL3 = new Object();
                            objCL3["node"] = arrCL3[k].DEVICE;
                            objCL3["nodetype"] = 'device';
                            objCL3["nodeid"] = parseInt(arrCL3[k].DEVICEID);
                            objCL3["faicon"] = 'fa fa-cog SteelBlue1';
                            objCL3["alarm"] = arrCL3[k].ALARM;
                            if(arrCL3[k].EXTERNALID != '' && arrCL3[k].EXTERNALID != null && arrCL3[k].EXTERNALID != 'DEL00000'){
                                objCL3["famid"] = Number(arrCL3[k].EXTERNALID.substring(3));
                                objCL3["fam"] = arrCL3[k].EXTERNALID;
                            }
                            else{
                                objCL3["famid"] = 0;
                                objCL3["fam"] = '';
                            }
                            objCL3["facilityid"] = objNL2.nodeid;
                            objCL3["facility"] = objNL2.node;
                            //objCL3["iconCls"] = 'device';
                            objCL3["leaf"] = true;
                            objCL3["children"] = [];
                            arrNodesLevel3.push(objCL3);

                            // build device object and push it to devices array
                            var objDevice = new Object();
                            objDevice["name"] = arrCL3[k].DEVICE;
                            objDevice["externalid"] = Number(arrCL3[k].DEVICEID);
                            objDevice["facilityid"] = getNetsuiteFacility(objNL2.node);
                            if(objDevice.facilityid > 0){
                                arrNewDevices.push(objDevice);
                            }
                        }
                        objNL2["children"] = arrNodesLevel3;
                        arrNodesLevel2.push(objNL2);
                    }

                    // extract level 2 children -------------------------------------------------------------------------------------------------------------
                    var arrCL2 = _.filter(arrNodes, function(arr){
                        return (arr.LEVEL === 2 && arr.TYPE === 'DEVICE' && arr.PARENTID === objNL1.nodeid );
                    });
                    for ( var l = 0; arrCL2 != null && l < arrCL2.length; l++ ) {
                        var objCL2 = new Object();
                        objCL2["node"] = arrCL2[l].DEVICE;
                        objCL2["nodetype"] = 'device';
                        objCL2["nodeid"] = parseInt(arrCL2[l].DEVICEID);
                        objCL2["faicon"] = 'fa fa-cog SteelBlue1';
                        objCL2["alarm"] = arrCL2[l].ALARM;
                        if(arrCL2[l].EXTERNALID != '' && arrCL2[l].EXTERNALID != null && arrCL2[l].EXTERNALID != 'DEL00000'){
                            objCL2["famid"] = Number(arrCL2[l].EXTERNALID.substring(3));
                            objCL2["fam"] = arrCL2[l].EXTERNALID;
                        }
                        else{
                            objCL2["famid"] = 0;
                            objCL2["fam"] = '';
                        }
                        objCL2["facilityid"] = objNL1.nodeid;
                        objCL2["facility"] = objNL1.node;
                        //objCL2["iconCls"] = 'device';
                        objCL2["leaf"] = true;
                        objCL2["children"] = [];
                        arrNodesLevel2.push(objCL2);

                        // build device object and push it to devices array
                        var objDevice = new Object();
                        objDevice["name"] = arrCL2[l].DEVICE;
                        objDevice["externalid"] = Number(arrCL2[l].DEVICEID);
                        objDevice["facilityid"] = getNetsuiteFacility(objNL1.node);
                        if(objDevice.facilityid > 0){
                            arrNewDevices.push(objDevice);
                        }
                    }
                    objNL1["children"] = arrNodesLevel2;
                    arrNodesLevel1.push(objNL1);
                }
            }
            objTree["children"] = arrNodesLevel1;


            //var arrNewDevicesExtIDs = _.compact(_.pluck(arrNewDevices, 'externalid'));
            var arrNewDevicesExtIDs = _.map(arrNewDevices, 'externalid');

            var strTree = JSON.stringify(objTree);
            var jsonTree = nlapiCreateFile('devices_tree.json', 'PLAINTEXT', strTree);
            jsonTree.setFolder(1330014);
            nlapiSubmitFile(jsonTree);

            var strGrid = JSON.stringify(arrNewDevices);
            var jsonGrid = nlapiCreateFile('devices_grid.json', 'PLAINTEXT', strGrid);
            jsonGrid.setFolder(1330014);
            nlapiSubmitFile(jsonGrid);

            var strGridOD = JSON.stringify(arrNodes);
            var jsonGridOD = nlapiCreateFile('devices_od.json', 'PLAINTEXT', strGridOD);
            jsonGridOD.setFolder(1330014);
            nlapiSubmitFile(jsonGridOD);

            var arrOldDevicesIntIDs = new Array();
            var arrOldDevicesExtIDs = new Array();
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",139944));
            var searchDevices = nlapiLoadSearch('customrecord_clgx_dcim_devices', 'customsearch_clgx_dcim_devices');
            searchDevices.addFilters(arrFilters);
            var resultSet = searchDevices.runSearch();
            resultSet.forEachResult(function(searchResult) {
                arrOldDevicesIntIDs.push(parseInt(searchResult.getValue('internalid', null, null)));
                arrOldDevicesExtIDs.push(parseInt(searchResult.getValue('externalid', null, null)));
                return true;
            });

// build add, update, delete arrays ------------------------------------------------------------------------------------------------------------------------------------------------

            var arrAdd = _.difference(arrNewDevicesExtIDs, arrOldDevicesExtIDs);
            var arrDelete = _.difference(arrOldDevicesExtIDs, arrNewDevicesExtIDs);
            var arrUpdate = _.intersection(arrNewDevicesExtIDs, arrOldDevicesExtIDs);

            arrUpdate = _.sortBy(arrUpdate, function(num){ return num; });
            var arrSplit = _.chunk(arrUpdate, (arrUpdate.length / 2 + 1));

            var str = JSON.stringify(arrAdd);
            var file = nlapiCreateFile('devices_grid_add.json', 'PLAINTEXT', str);
            file.setFolder(1330014);
            nlapiSubmitFile(file);

            var str = JSON.stringify(arrDelete);
            var file = nlapiCreateFile('devices_grid_delete.json', 'PLAINTEXT', str);
            file.setFolder(1330014);
            nlapiSubmitFile(file);

            var str = JSON.stringify(arrUpdate);
            var file = nlapiCreateFile('devices_grid_update.json', 'PLAINTEXT', str);
            file.setFolder(1330014);
            nlapiSubmitFile(file);

            var str = JSON.stringify(arrSplit[0]);
            var file = nlapiCreateFile('devices_grid_update_1.json', 'PLAINTEXT', str);
            file.setFolder(1330014);
            nlapiSubmitFile(file);

            var str = JSON.stringify(arrSplit[1]);
            var file = nlapiCreateFile('devices_grid_update_2.json', 'PLAINTEXT', str);
            file.setFolder(1330014);
            nlapiSubmitFile(file);

            nlapiSendEmail(432742,71418,'OD 1.1 - Created JSON files','');

            var status = nlapiScheduleScript('customscript_clgx_ss_dcim_devices_delete', 'customdeploy_clgx_ss_dcim_devices_delete' ,null);
            //var status = nlapiScheduleScript('customscript_clgx_ss_dcim_devices_add', 'customdeploy_clgx_ss_dcim_devices_add' ,null);
            var status = nlapiScheduleScript('customscript_clgx_ss_dcim_devices_update', 'customdeploy_clgx_ss_dcim_devices_updt_1' ,null);
            var status = nlapiScheduleScript('customscript_clgx_ss_dcim_devices_update', 'customdeploy_clgx_ss_dcim_devices_updt_2' ,null);
        }
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function getMarketIcon(market){
    if(market == 'Montreal' || market == 'Toronto' || market == 'Vancouver'){
        return 'ca';
    }
    else{
        return 'us';
    }
}

function getMarketIconSVG(market){
    if(market == 'Montreal' || market == 'Toronto' || market == 'Vancouver'){
        return 'flag-icon flag-icon-ca';
    }
    else{
        return 'flag-icon flag-icon-us';
    }
}

function getNetsuiteFacility(facility){
    var facilityid = 0;
    switch(facility) {

        case 'COL Site Metrics':
            facilityid = 24;
            break;
        case 'COL Canara VM BCM Collector Test':
            facilityid = 24;
            break;
        case 'COL Collectors':
            facilityid = 24;
            break;
        case 'COL1':
            facilityid = 24;
            break;
        case 'COL2':
            facilityid = 24;
            break;
        case 'COL3 - Not in Production':
            facilityid = 29;
            break;
        case 'COL3':
            facilityid = 29;
            break;

        case 'DAL Site Metrics':
            facilityid = 3;
            break;
        case 'DAL Collectors':
            facilityid = 3;
            break;
        case 'DAL1':
            facilityid = 3;
            break;
        case 'DAL2':
            facilityid = 18;
            break;

        case 'JAX1':
            facilityid = 21;
            break;
        case 'JAX2':
            facilityid = 27;
            break;

        case 'LAK1':
            facilityid = 28;
            break;

        case 'MIN1':
            facilityid = 17;
            break;
        case 'MIN Collectors':
            facilityid = 17;
            break;
        case 'MIN1 Metrics':
            facilityid = 17;
            break;
        case 'MIN1 Suite 100':
            facilityid = 17;
            break;
        case 'MIN1 Suite 261':
            facilityid = 17;
            break;
        case 'MIN2':
            facilityid = 17;
            break;
        case 'MIN3':
            facilityid = 25;
            break;

        case 'MTL Site Metrics':
            facilityid = 4;
            break;
        case 'MTL Collectors':
            facilityid = 4;
            break;
        case 'MTL1':
            facilityid = 2;
            break;
        case 'MTL2':
            facilityid = 5;
            break;
        case 'MTL3':
            facilityid = 4;
            break;
        case 'MTL4':
            facilityid = 9;
            break;
        case 'MTL5':
            facilityid = 6;
            break;
        case 'MTL6':
            facilityid = 7;
            break;
        case 'MTL7':
            facilityid = 19;
            break;

        case 'NNJ1':
            facilityid = 34;
            break;
        case 'NNJ2':
            facilityid = 33;
            break;
        case 'NNJ3':
            facilityid = 35;
            break;
        case 'NNJ4':
            facilityid = 36;
            break;

        case 'TOR1':
            facilityid = 8;
            break;
        case 'TOR2':
            facilityid = 15;
            break;

        case 'VAN1':
            facilityid = 14;
            break;
        case 'VAN2':
            facilityid = 20;
            break;

        default:
            facilityid = 0;
    }
    return facilityid;
}

function get_power2_name (power) {
    var lastChar = power.substr(power.length - 1);
    var power2 = '';
    if(lastChar == 'A' || lastChar == 'B'){
        if(lastChar == 'A'){
            power2 = power.slice(0, - 1) + 'B';
        }
        else{
            power2 = power.slice(0, - 1) + 'A';
        }
    }
    return  power2;
}