function create() {
    try {
        var obj=new Object();

        var objBase= {
            "info": {
                "_postman_id": "c6e95b0e-c837-4dc3-a8f7-c149def73cb0",
                "name": "Migrate_VXC_Prod_12Feb2020",
                "schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
            },
            "item": []
        };
var obj3={
    "customer_id": 243468,
    "contact_id": 562657,
    "title": "VXC 366",
    "location_id": 0,
    "facility_id": 0,
    "oppty_id": 0,
    "estimate_id": 0,
    "so_id": 0,
    "po_id": 123,
    "billing": "Peter PiperAnonymous - US2300 15th RoadSte 302Denverville DC 80201",
    "items": [{
        "type": "vxc",
        "location_id": 2,
        "facility_id": 3,
        "item_id": 797,
        "mrc": 100,
        "nrc": 0,
        "name": "VXC 366",
        "provider_id": 3,
        "cloud_reference_id": "726781068157",
        "azure_primary_port": "",
        "azure_secondary_port": "",
        "tagged": true,
        "speed_id": 3,
        "speed": "1Gbps",
        "new_xc_id": "",
        "ns_xc_id": 25226,
        "vxlan": 0,
        "a_egress_vlan": 107,
        "z_egress_vlan": 555,
        "egress_type_id": 1,
        "egress_type": "swap",
        "ns_vxc_id": 0,
        "ns_qu_id": 0,
        "service_id": 0,
        "line_id": 0,
        "xcs": []
    }
    ]
};

        var searchColumn = new Array();
        var searchFilter=new Array();
        var searchXCs = nlapiSearchRecord('customrecord_cologix_vxc','customsearch7506',searchFilter,searchColumn);
        for ( var i = 0; searchXCs != null && i < searchXCs.length; i++ ) {
            var searchXC = searchXCs[i];
            var columns = searchXC.getAllColumns();
            var title = searchXC.getValue(columns[0]);
            var location = searchXC.getValue(columns[1]);
            var soid = searchXC.getValue(columns[2]);
            var customer_id = searchXC.getValue(columns[3]);
            var provider=searchXC.getValue(columns[4]);
            var cloud_reference_id=searchXC.getValue(columns[5]);
            var serviceid = searchXC.getValue(columns[6]);
            var azure_primary_port=searchXC.getValue(columns[7]);
            var tagged=searchXC.getValue(columns[8]);
            var speed=searchXC.getText(columns[9]);
            var speed_id=searchXC.getValue(columns[9]);
            var xc_id=searchXC.getValue(columns[10]);
            var a_egress_vlan = searchXC.getValue(columns[11]);
            var z_egress_vlan = searchXC.getValue(columns[12]);
            var internalID=searchXC.getValue(columns[13]);
            var nameP=title;
            if(speed<1000){
                speed=speed+'Mbs';
            }
            else{
                speed=speed+'Gbps';
            }
            if(title==""){
                nameP=internalID;
            }
            if(customer_id==''){
                customer_id=0;
            }
            if(soid==''){
                soid=0;
            }
            if(serviceid==''){
                serviceid=0;
            }
            if(provider==''){
                provider=0;
            }

            var stRaw = "{\n\"customer_id\": "+customer_id+",\n\"contact_id\": 0,\n\"title\": \""+title+"\",\n\"location_id\": 0,\n\"facility_id\": "+location+",\n\"oppty_id\": 0,\n\"estimate_id\": 0,\n\"so_id\": "+soid+",\n\"po_id\": 123,\n\"billing\": \"\",\n\"items\": [{\n\"type\": \"vxc\",\n\"location_id\": "+location+",\n\"facility_id\": "+location+",\n\"item_id\": 797,\n\"mrc\": 100,\n\"nrc\": 0,\n\"name\": \""+title+"\",\n\"provider_id\": "+provider+",\n\"cloud_reference_id\": \""+cloud_reference_id+"\",\n\"azure_primary_port\": \""+azure_primary_port+"\",\n\"azure_secondary_port\": \"\",\n\"tagged\": true,\n\"speed_id\":"+speed_id+",\n\"speed\": \""+speed+"\",\n\"new_xc_id\": \"\",\n\"ns_xc_id\": "+xc_id+",\n\"vxlan\": 0,\n\"a_egress_vlan\": "+a_egress_vlan+",\n\"z_egress_vlan\": "+z_egress_vlan+",\n\"egress_type_id\": 1,\n\"egress_type\": \"swap\",\n\"ns_vxc_id\": 0,\n\"ns_qu_id\": 0,\n\"service_id\": "+serviceid+",\n\"line_id\": 0,\n\"xcs\": []\n}\n]\n}";

            obj= {
                "name": nameP,
                "request": {
                    "auth": {
                        "type": "oauth1",
                        "oauth1": [
                            {
                                "key": "consumerKey",
                                "value": "863c066eae50cd25ea39ae06ea82b45e29bf8f09939036429aa454aad12539f6",
                                "type": "string"
                            },
                            {
                                "key": "consumerSecret",
                                "value": "320d2f8253390e82b9da88e8cc54bf96d7f37584bd45b763eb3b6cd32c8fae5d",
                                "type": "string"
                            },
                            {
                                "key": "tokenSecret",
                                "value": "cf6b9fefc698b997fdc9e0d4bfb59622d38aa2857ddb1be061b33eac8f3d253f",
                                "type": "string"
                            },
                            {
                                "key": "token",
                                "value": "689ad54e9d679707bcc6c859166d6498a6e5eaea486c0e97d87f699158bb2bce",
                                "type": "string"
                            },
                            {
                                "key": "realm",
                                "value": "1337135",
                                "type": "string"
                            },
                            {
                                "key": "addParamsToHeader",
                                "value": true,
                                "type": "boolean"
                            },
                            {
                                "key": "nonce",
                                "value": "",
                                "type": "string"
                            },
                            {
                                "key": "timestamp",
                                "value": "",
                                "type": "string"
                            },
                            {
                                "key": "addEmptyParamsToSign",
                                "value": true,
                                "type": "boolean"
                            },
                            {
                                "key": "signatureMethod",
                                "value": "HMAC-SHA1",
                                "type": "string"
                            },
                            {
                                "key": "version",
                                "value": "1.0",
                                "type": "string"
                            }
                        ]
                    },
                    "method": "POST",
                    "header": [
                        {
                            "key": "Content-Type",
                            "value": "application/json"
                        }
                    ],
                    "body": {
                        "mode": "raw",
                        "raw":stRaw

                    },
                    "url": {
                        "raw": "https://1337135.restlets.api.netsuite.com/app/site/hosting/restlet.nl?deploy=1&script=1729",
                        "protocol": "https",
                        "host": [
                            "rest",
                            "netsuite",
                            "com"
                        ],
                        "path": [
                            "app",
                            "site",
                            "hosting",
                            "restlet.nl"
                        ],
                        "query": [
                            {
                                "key": "deploy",
                                "value": "1"
                            },
                            {
                                "key": "script",
                                "value": "1729"
                            }
                        ]
                    }
                },
                "response": []
            };
            objBase["item"].push(obj);

        }
        var jsonfinal = JSON.stringify(objBase);
        var fileName = nlapiCreateFile('migratevxc12Feb2020.json', 'PLAINTEXT', jsonfinal);
        fileName.setFolder(10549381);
        var fileid=nlapiSubmitFile(fileName);

    }
    catch (error){
        var context = nlapiGetContext();
        var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
        if (status == 'QUEUED') {
            nlapiLogExecution('DEBUG', 'End', status + ' / Finished scheduled script due to an error and re-schedule it.');
            return false;
        }
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}