function create() {
    try {
 var obj=new Object();
 var objBase= {
     "info": {
         "_postman_id": "c6e95b0e-c837-4dc3-a8f7-c149def73cb0",
         "name": "Migrate_XC_Prod_12Feb2020",
         "schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json"
     },
     "item": []
 };
        var searchColumn = new Array();
        var searchFilter=new Array();
        var searchXCs = nlapiSearchRecord('customrecord_cologix_crossconnect','customsearch7876',searchFilter,searchColumn);
        for ( var i = 0; searchXCs != null && i < searchXCs.length; i++ ) {
            var searchXC = searchXCs[i];
            var columns = searchXC.getAllColumns();
            var customer_id = searchXC.getValue(columns[0]);
            var title = searchXC.getValue(columns[1]);
            var location = searchXC.getValue(columns[2]);
            var soid = searchXC.getValue(columns[3]);
            var serviceid = searchXC.getValue(columns[4]);
            var media_id = searchXC.getValue(columns[5]);
            var speed=searchXC.getValue(columns[6]);
            var provider=searchXC.getValue(columns[7]);
            var redundant=searchXC.getValue(columns[8]);
            var internalID=searchXC.getValue(columns[10]);
            var contact_id=searchXC.getValue(columns[11]);
            var nameP=title;
            if(title==""){
                nameP=internalID;
            }
            if(customer_id==''){
                customer_id=0;
            }
            if(soid==''){
                soid=0;
            }
            if(serviceid==''){
                serviceid=0;
            }
            if(provider==''){
                provider=0;
            }
            if(contact_id=='')
            {
                contact_id=0;
            }

            var stRaw = "{\n  \"customer_id\": " + customer_id + ",\n  \"contact_id\": "+contact_id+",\n  \"title\": \"" + title + "\",\n  \"location_id\":" + location + ",\n  \"facility_id\": 0,\n  \"oppty_id\": 0,\n  \"estimate_id\": 0,\n  \"so_id\": " + soid + ",\n  \"po_id\": \"\",\n  \"billing\": \"\",\n  \"items\": [{\n    \"type\": \"xc\",\n    \"location_id\": " + location + ",\n    \"facility_id\": 0,\n    \"item_id\": 792,\n    \"mrc\": 100,\n    \"nrc\": 500,\n    \"service_id\": " + serviceid + ",\n    \"line_id\": 0,\n    \"media_id\": " + media_id + ",\n    \"speed_id\": " + speed + ",\n    \"provider_id\": " + provider + ",\n    \"redundant\": " + redundant + ",\n    \"prm_new_xc_id\": \"\",\n    \"prm_name\": \" "+ title + "\",\n    \"prm_ns_xc_id\": 0,\n    \"prm_ns_qu_id\": 0,\n    \"sec_new_xc_id\": \"\",\n    \"sec_name\": \"\",\n    \"sec_ns_xc_id\": 0,\n    \"sec_ns_qu_id\": 0\n  }\n  ]\n}";

            obj= {
                "name": nameP,
                "request": {
                    "auth": {
                        "type": "oauth1",
                        "oauth1": [
                            {
                                "key": "consumerKey",
                                "value": "863c066eae50cd25ea39ae06ea82b45e29bf8f09939036429aa454aad12539f6",
                                "type": "string"
                            },
                            {
                                "key": "consumerSecret",
                                "value": "320d2f8253390e82b9da88e8cc54bf96d7f37584bd45b763eb3b6cd32c8fae5d",
                                "type": "string"
                            },
                            {
                                "key": "tokenSecret",
                                "value": "cf6b9fefc698b997fdc9e0d4bfb59622d38aa2857ddb1be061b33eac8f3d253f",
                                "type": "string"
                            },
                            {
                                "key": "token",
                                "value": "689ad54e9d679707bcc6c859166d6498a6e5eaea486c0e97d87f699158bb2bce",
                                "type": "string"
                            },
                            {
                                "key": "realm",
                                "value": "1337135",
                                "type": "string"
                            },
                            {
                                "key": "addParamsToHeader",
                                "value": true,
                                "type": "boolean"
                            },
                            {
                                "key": "nonce",
                                "value": "",
                                "type": "string"
                            },
                            {
                                "key": "timestamp",
                                "value": "",
                                "type": "string"
                            },
                            {
                                "key": "addEmptyParamsToSign",
                                "value": true,
                                "type": "boolean"
                            },
                            {
                                "key": "signatureMethod",
                                "value": "HMAC-SHA1",
                                "type": "string"
                            },
                            {
                                "key": "version",
                                "value": "1.0",
                                "type": "string"
                            }
                        ]
                    },
                    "method": "POST",
                    "header": [
                        {
                            "key": "Content-Type",
                            "value": "application/json"
                        }
                    ],
                    "body": {
                        "mode": "raw",
                        "raw":stRaw

                    },
                    "url": {
                        "raw": "https://1337135.restlets.api.netsuite.com/app/site/hosting/restlet.nl?deploy=1&script=1729",
                        "protocol": "https",
                        "host": [
                            "1337135.restlets.api",
                            "netsuite",
                            "com"
                        ],
                        "path": [
                            "app",
                            "site",
                            "hosting",
                            "restlet.nl"
                        ],
                        "query": [
                            {
                                "key": "deploy",
                                "value": "1"
                            },
                            {
                                "key": "script",
                                "value": "1729"
                            }
                        ]
                    }
                },
                "response": []
            };
            objBase["item"].push(obj);

        }
        var jsonfinal = JSON.stringify(objBase);
        var fileName = nlapiCreateFile('migratexc12Feb.json', 'PLAINTEXT', jsonfinal);
        fileName.setFolder(10549381);
        var fileid=nlapiSubmitFile(fileName);

    }
    catch (error){
        var context = nlapiGetContext();
        var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
        if (status == 'QUEUED') {
            nlapiLogExecution('DEBUG', 'End', status + ' / Finished scheduled script due to an error and re-schedule it.');
            return false;
        }
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
