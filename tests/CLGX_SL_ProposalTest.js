nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Proposal.js
//	Script Name:	CLGX_SU_Proposal
//	Script Id:		customscript_clgx_su_proposal
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Proposal - Sandbox |
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		1/12/2012
//	Includes:		CLGX_LIB_Transaction_Totals.js, underscore-min.js
//-------------------------------------------------------------------------------------------------

function beforeLoad (type, form) {
    try {
        nlapiLogExecution('DEBUG','User Event - Before Load','|-------------STARTED--------------|');

        var currentContext = nlapiGetContext();
        stRole = currentContext.getRole();
        var allow = 0;
        if (stRole == '-5' || stRole == '3' || stRole == '18') {
            allow = 1;
        }

        var closed = 0;
        if(closed == 1 && allow == 0 && (type == 'create' || type == 'edit')){ // if module is closed and any other role then admin
            var arrParam = new Array();
            arrParam['custscript_internal_message'] = 'This module is closed for modifications. Sorry for the inconvenience. Please come back later.';
            nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
        }



        if (currentContext.getExecutionContext() == 'userinterface'){
//------------- Begin Section 1 -------------------------------------------------------------------
// Created:	4/17/2014
// Details:	No role can create a new proposal unless from an opportunity or admin or full roles
//-------------------------------------------------------------------------------------------------

            if(allow == 0 && type == 'create' && stRole != 1039){ // if any other role then admin or customer care
                var opportunity = nlapiGetFieldValue('opportunity');
                if(opportunity == null || opportunity == ''){
                    var arrParam = new Array();
                    arrParam['custscript_internal_message'] = 'You can\'t create a proposal directly. Please create an opportunity first.';
                    nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
                }
            }

//------------- Begin Section 2 -------------------------------------------------------------------
// Created:	4/1/2013
// Details:	If this customer is on credit hold, display an warning
//-------------------------------------------------------------------------------------------------

            if (type == 'view' || type == 'edit'){
                var customerID = nlapiGetFieldValue('entity');
                if(customerID != null && customerID != ''){
                    var recCustomer = nlapiLoadRecord('customer', customerID);
                    var onHold = recCustomer.getFieldValue('creditholdoverride');
                    if(onHold == 'ON'){
                        nlapiSetFieldValue('custbody_clgx_credit_hold_warning', '<div style="color:#FF0000">This customer is on credit hold!</div>');
                    }
                }
            }

//------------- Begin Section 3 -------------------------------------------------------------------
// Created:	4/15/2014
// Details:	Display download links for BQ and Contract
//-------------------------------------------------------------------------------------------------

            if (type == 'view' || type == 'edit'){
                var fileQuoteUrl = '';
                var fileQuoteID = nlapiGetFieldValue('custbody_clgx_prop_bq_file_id');
                if(fileQuoteID != null && fileQuoteID != ''){

                    try {
                        var objFileQuote = nlapiLoadFile(fileQuoteID);
                        fileQuoteUrl = objFileQuote.getURL();
                    }
                    catch (e) {
                        var str = String(e);
                        if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
                            fileQuoteID = 0;
                        }
                    }

                    if(fileQuoteUrl != ''){
                        nlapiSetFieldValue('custbody_clgx_download_budgetray_quote', '<a href="' + fileQuoteUrl + '" target="_blank"><img src="//www.cologix.com/images/icon/application_pdf.png" alt="BQ" height="16" width="16"></a>');
                    }

                }
                var fileContractUrl = '';
                var fileContractID = nlapiGetFieldValue('custbody_clgx_prop_contract_file_id');
                if(fileContractID != null && fileContractID != ''){
                    try {
                        var objFileContract = nlapiLoadFile(fileContractID);
                        fileContractUrl = objFileContract.getURL();
                    }
                    catch (e) {
                        var str = String(e);
                        if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
                            fileContractID = 0;
                        }
                    }

                    if(fileContractUrl != ''){
                        nlapiSetFieldValue('custbody_clgx_download_contract', '<a href="' + fileContractUrl + '" target="_blank"><img src="//www.cologix.com/images/icon/acrobat_reader_16x16.gif" alt="Contract" height="16" width="16"></a>');
                    }
                }
            }

//------------- Begin Section 4 -----------------------------------------------------------------------------------
// Created:	4/15/2014
// Details: Display transaction totals in the header in an inlineHTML field
//-----------------------------------------------------------------------------------------------------------------

            if (type == 'view' || type == 'edit') {

                var proposalid = nlapiGetRecordId();

                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_location',null,null));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_class',null,null));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_total',null,null));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_transaction",null,"anyof",proposalid));
                var searchTotals = nlapiSearchRecord('customrecord_clgx_totals_transaction', null, arrFilters, arrColumns);

                var html = '<table cellpadding="2" border="1" style="font-family: verdana,arial,sans-serif;font-size:11px;color:#333333;border-width: 1px;border-color: #999999;border-collapse: collapse;padding:5px;">';

                html += '<tr>';
                html += '<th style="padding: 5px;background-color: #dedede;">Location</th>';
                html += '<th style="padding: 5px;background-color: #dedede;">Class</th>';
                html += '<th style="padding: 5px;background-color: #dedede;">Total</th>';
                html += '</tr>';

                for ( var i = 0; searchTotals != null && i < searchTotals.length; i++ ) {
                    var searchTotal = searchTotals[i];
                    html += '<tr>';
                    html += '<td style="padding: 5px;">' + searchTotal.getText('custrecord_clgx_totals_location',null,null) + '</td>';
                    html += '<td style="padding: 5px;">' + searchTotal.getText('custrecord_clgx_totals_class',null,null) + '</td>';
                    html += '<td style="padding: 5px;">$' + addCommas(searchTotal.getValue('custrecord_clgx_totals_total',null,null)) + '</td>';
                    html += '</tr>';

                }
                html += '</table>';

                nlapiSetFieldValue('custbody_clgx_transaction_totals', html);
            }

//---------- End Sections ------------------------------------------------------------------------------------------------
        }
        nlapiLogExecution('DEBUG','User Event - Befor Load','|-------------FINISHED--------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function beforeSubmit(type){
    try {
        nlapiLogExecution('DEBUG','User Event - Before Load','|-------------STARTED--------------|');

//------------- Begin Section 1 -------------------------------------------------------------------
// Created:	2/7/2014
// Details:	When a proposal is deleted, update any SO that was used to it's renewal
//-------------------------------------------------------------------------------------------------
        var currentContext = nlapiGetContext();
        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'delete')){

            var arrSOs = nlapiGetFieldValues('custbody_clgx_renewed_from_sos');
            for ( var i = 0; arrSOs != null && i < arrSOs.length; i++ ) {
                nlapiSubmitField('salesorder', arrSOs[i], 'custbody_clgx_so_renewed_on_proposal', '');
            }
        }

//---------- End Sections ------------------------------------------------------------------------------------------------

        nlapiLogExecution('DEBUG','User Event - Befor Load','|-------------FINISHED--------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function userEventAfterSubmit(type)
{

    var xml = '<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n<pdf>\n<body size="letter-landscape" font-size=\"10\">\n';

    xml += '<table width="100%" align="center">\n';
    xml += '<tr>\n';
    xml += '<td>Deposit Number: ' + 'tranid' + '</td>\n';
    xml += '</tr>\n';
    xml += '<tr>\n';
    xml += '<td>Date: ' + 'trandate' + '</td>\n';
    xml += '</tr>\n';
    xml += '<tr>\n';
    xml += '<td>Total: ' + 'total' + '</td>\n';
    xml += '</tr>\n';
    xml += '<tr>\n';
    xml += '<td>Posting Period: ' + 'postingperiod' + '</td>\n';
    xml += '</tr>\n';
    xml += '</table>\n';

    xml += '<br /><br />\n';

    xml += '<table width="100%" align="center">\n';
    xml += '<thead>\n';
    xml += '<tr>\n';
    xml += '<th>Date</th>\n';
    xml += '<th>ID</th>\n';
    xml += '<th>Customer</th>\n';
    xml += '<th>Payment Amount</th>\n';
    xml += '<th>Transaction Amount</th>\n';
    xml += '<th>Transaction Type</th>\n';
    xml += '<th>Payment Method</th>\n';
    xml += '</tr>\n';
    xml += '</thead>\n';
    xml += '<tbody>\n';

    for (var i = 1; i < 500; i++)
    {

            xml += '<tr>\n';
            xml += '<td>' + '10/01/2015' + '</td>\n';
            xml += '<td>' + '155' + '</td>\n';
            xml += '<td>' +'Cisco'+i + '</td>\n';
            xml += '<td>' + '1500' + '</td>\n';
            xml += '<td>' + '1500' + '</td>\n';
            xml += '<td>' + 'Invoice' + '</td>\n';
            xml += '<td>' + 'DD' + '</td>\n';
            xml += '</tr>\n';

    }

    xml += '</tbody>\n';
    xml += '</table>\n';
    xml += '</body>\n</pdf>';

    var pdf = nlapiXMLToPDF(xml);
    pdf.setName('TestFile.pdf');
    var emailSubject = 'TestFile';
    var emailBody = 'tabledata1: '+  xml;
    nlapiSendEmail(206211, 206211,emailSubject,emailBody,null, null, null, pdf, null, null); // Send email to Catalina
    return;

}
function testProposal(type){
    try {

//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 1/12/2012
// Details:	Create the PDF contract or quote from proposal.
//-------------------------------------------------------------------------------------------------

        var currentContext = nlapiGetContext();
        //  if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'edit')) {

        var proposalid ="471192";
        var contractPDF = 'T';
        var quotePDF = 'T';
        var closed=0;
        var w=0;

        if (contractPDF == 'T' || quotePDF == 'T') {

            var linkToFolder = 'https://system.netsuite.com/app/common/media/mediaitemfolders.nl?folder=';
            var fileFolder = "2777984715";
            linkToFolder += fileFolder;

            var recProposal = nlapiLoadRecord('estimate', proposalid);
            var stSONbr = recProposal.getFieldValue('tranid');
            var stSORep = recProposal.getFieldText('salesrep');
            var stSOCustId = recProposal.getFieldValue('entity');
            var stSOAddr = recProposal.getFieldValue('billaddress');
            var stCurrency = recProposal.getFieldValue('currency');
            var stSOAtt = recProposal.getFieldValue('custbody_clgx_contract_terms_attention');
            var stSON = recProposal.getFieldValue('custbody_clgx_contract_terms_notes');
            if(stSON == null){
                stSON = '';
            }
            //nlapiLogExecution('DEBUG','stSONotes = ' + recProposal.getFieldValue('custbody_clgx_contract_terms_notes'));
            var stSONotes = convertBR(stSON);
            stSONotes = stSONotes.replace(/\&/g," and ");

            var termsbody = recProposal.getFieldValue('custbody_clgx_contract_terms_body');
            if(termsbody == null){
                termsbody = '';
            }
            termsbody = termsbody.replace(/&/g,'and');
            var stSOTerms = convertBR(termsbody);

            var stSOTitle = recProposal.getFieldValue('custbody_clgx_contract_terms_title');
            var stSOLang = nlapiLookupField('customrecord_clgx_contract_terms', stSOTitle,'custrecord_clgx_contract_terms_lang');

            var arrSOs = recProposal.getFieldValues('custbody_clgx_renewed_from_sos');
            var arrSOsNames = recProposal.getFieldTexts('custbody_clgx_renewed_from_sos');

            var powerUsage = recProposal.getFieldValue('custbody_clgx_pwusg_meter_power_usage');
            var usageRate = recProposal.getFieldValue('custbody_clgx_pwusg_util_rate');

            //Determine currency
            var currencyType = '';
            if (stCurrency == 1){
                currencyType = 'USD';
            }
            else if (stCurrency == 3){
                currencyType = 'CDN';
            }
            else{}

            var arrCustomerDetails = nlapiLookupField('customer', stSOCustId, ['companyname', 'billaddress1', 'billaddress2', 'billcity', 'billcountry', 'billstate', 'billzipcode', 'language']);
            var custCompName = arrCustomerDetails['companyname'];
            custCompName = custCompName.replace(/\&/g,"and");

            var billAddress1 = arrCustomerDetails['billaddress1'];
            var billAddress2 = arrCustomerDetails['billaddress2'];
            var billCity = arrCustomerDetails['billcity'];
            var billCountry = arrCustomerDetails['billcountry'];
            var billState = arrCustomerDetails['billstate'];
            var billZipcode = arrCustomerDetails['billzipcode'];
            var billLanguage = arrCustomerDetails['language'];

            if (stSOLang == 17){ // if French
                var stLang = 1;
                var langPrefix = 'FR_';
                if (quotePDF == 'T'){
                    var filePrefix = 'BQ_';
                    var template = 346192; // Load French Quote Template
                }
                else{
                    var filePrefix = 'SO_';
                    var template = 23046; // Load French Contract Template
                }
            }
            else { // if English
                var stLang = 0;
                var stLang = 0;
                var langPrefix = 'EN_';
                if (quotePDF == 'T'){
                    var filePrefix = 'BQ_';
                    var template = 1864209; // Load English Quote Template
                }
                else{
                    var filePrefix = 'SO_';
                    var template = 23041; // Load English Contract Template
                }
            }

            var arrTitleBQ = new Array();
            arrTitleBQ[0] = 'Quote Details';
            arrTitleBQ[1] = 'D&eacute;tails des Services';

            var arrTitleContract = new Array();
            arrTitleContract[0] = 'Services Details';
            arrTitleContract[1] = 'D&eacute;tails des Services';

            var arrTitleBQTotals = new Array();
            arrTitleBQTotals[0] = 'Quote Totals';
            arrTitleBQTotals[1] = 'D&eacute;tails des Services - Totaux ';

            var arrTitleContractTotals = new Array();
            arrTitleContractTotals[0] = 'Services Totals';
            arrTitleContractTotals[1] = 'D&eacute;tails des Services - Totaux ';

            var arrTitleMRC = new Array();
            arrTitleMRC[0] = 'RECURRING CHARGES';



            arrTitleMRC[1] = 'FRAIS R&Eacute;CURRENTS';

            var arrTitleNRC = new Array();
            arrTitleNRC[0] = 'NON RECURRING CHARGES';
            arrTitleNRC[1] = 'FRAIS NON R&Eacute;CURRENTS';

            var arrTitleDisc = new Array();
            arrTitleDisc[0] = 'CATEGORY : ONE TIME DISCOUNT';
            arrTitleDisc[1] = 'CAT&Eacute;GORIE: PREMIER MOIS GRATUIT';

            var arrFootMRC = new Array();
            arrFootMRC[0] = 'Total Recurring Charges';
            arrFootMRC[1] = 'Total des frais r&eacute;currents';

            var arrFootNRC = new Array();
            arrFootNRC[0] = 'Total Non Recurring Charges';
            arrFootNRC[1] = 'Total des frais non r&eacute;currents';


            var arrFootDisc = new Array();
            arrFootDisc[0] = 'Total Discounts';
            arrFootDisc[1] = 'Total r&eacute;ductions';


            var arrFootAllRC = new Array();
            arrFootAllRC[0] = 'Grand Total Recurring Charges';
            arrFootAllRC[1] = 'Grand total des frais r&eacute;currents';

            var arrFootAllNRC = new Array();
            arrFootAllNRC[0] = 'Grand Total Non Recurring Charges';
            arrFootAllNRC[1] = 'Grand total des frais non r&eacute;currents';

            var arrFootAllDisc = new Array();
            arrFootAllDisc[0] = 'Grand Total Discounts';
            arrFootAllDisc[1] = 'Grand total r&eacute;ductions';


            //
            var arrAttn = new Array();
            arrAttn[0] = 'Attn';
            arrAttn[1] = '&Agrave; l\'attention de';

            var arrName = new Array();
            arrName[0] = 'Name';
            arrName[1] = 'Nom';

            var arrDescr = new Array();
            arrDescr[0] = 'Description';
            arrDescr[1] = 'Description';

            var arrTerms = new Array();
            arrTerms[0] = 'Service Term';
            arrTerms[1] = 'P&eacute;riode de service';

            var arrQty = new Array();
            arrQty[0] = 'Qty';
            arrQty[1] = 'Quantit&eacute;';

            var arrRate = new Array();
            arrRate[0] = 'Rate';
            arrRate[1] = 'Tarif';

            var arrAmount = new Array();
            arrAmount[0] = 'Amount';
            arrAmount[1] = 'Montant';

            var arrUsageCharges = new Array();
            arrUsageCharges[0] = 'USAGE CHARGES';
            arrUsageCharges[1] = 'Frais d\'utilisation';

            var arrPowerUsage = new Array();
            arrPowerUsage[0] = 'KW h usage rate';
            arrPowerUsage[1] = 'Taux d\'utilisation de KW h';

            var arrNotes = new Array();
            arrNotes[0] = 'Notes';
            arrNotes[1] = 'Notes';

            var arrFromSOs = new Array();
            arrFromSOs[0] = 'This order is replacing the following Service Orders';
            arrFromSOs[1] = 'Cette commande remplace les ordres de service suivants';


            // start items sections --------------------------------------------------------------------------------


            var tabledata='';
            var nbrItems = recProposal.getLineItemCount('item');
            // build array of unique locations
            var arrLocations = new Array();
            for (var i = 0; i < nbrItems; i++){
                var locid = recProposal.getLineItemValue('item','location', i + 1)
                if(!inArray(locid,arrLocations)){
                    arrLocations.push(locid);
                }
            }

            // loop unique locations
            var grandTotalRC = 0;
            var grandTotalNRC = 0;
            var grandTotalDisc = 0;
            for (var l = 0; arrLocations != null && l < arrLocations.length; l++){
                var arrLocation = nlapiLookupField('location', arrLocations[l], ['name', 'address1', 'address2', 'city', 'country', 'state', 'zip']);
                var stLocName = arrLocation['name'];
                stLocName = stLocName.replace(/\&/g,"and");

                var arrNRC = new Array();
                var arrRC = new Array();
                var arrDisc = new Array();
                for (var m = 0; m < parseInt(nbrItems); m++) {
                    var stBS = recProposal.getLineItemText('item', 'billingschedule', m + 1);
                    var itemID = recProposal.getLineItemValue('item', 'item', m + 1);
                    var stClass = recProposal.getLineItemText('item', 'class', m + 1);
                    if (stClass != 'Recurring' && itemID != 549) {
                        arrNRC.push(m + 1);
                    }
                    else if(stClass == 'Recurring' && itemID == 549){
                        arrDisc.push(m + 1);
                    }
                    else {
                        arrRC.push(m + 1);
                    }
                }

                tabledata += '<table class="profile" align="center">\n';
                tabledata += '<tr>\n';
                if (quotePDF == 'T'){
                    tabledata += '<td><h2>' + arrTitleBQ[stLang] + ' / ' + stLocName + '</h2></td>\n';
                }
                else{
                    tabledata += '<td><h2>' + arrTitleContract[stLang] + ' / ' + stLocName + '</h2></td>\n';
                }
                tabledata += '</tr>\n';
                tabledata += '</table>\n';






                if (arrRC.length > 0) { // there are recurring items, so start recurring block --------------------------------------------------


                    tabledata += '<table  class="profile" align="center" >\n';
                    tabledata += '<tr>\n';
                    tabledata += '<td width="30%">&nbsp;</td>\n';
                    tabledata += '<td width="40%">&nbsp;</td>\n';
                    tabledata += '<td width="10%">&nbsp;</td>\n';
                    tabledata += '<td width="10%">&nbsp;</td>\n';
                    tabledata += '<td width="10%">&nbsp;</td>\n';
                    tabledata += '</tr>\n';

                    tabledata += '<tr>\n';
                    tabledata += '<td colspan="5">' + arrTitleMRC[stLang] + '</td>\n';
                    tabledata += '</tr>\n';
                    tabledata += '<tr>\n';
                    tabledata += '<td colspan="5">&nbsp;</td>\n';
                    tabledata += '</tr>\n';
                    tabledata += '<tr>\n';
                    tabledata += '<td class="cool"><b>' + arrDescr[stLang] + '</b></td>\n';
                    tabledata += '<td class="cool"><b>' + arrTerms[stLang] + '</b></td>\n';
                    tabledata += '<td class="cool" align="center"><b>' + arrQty[stLang] + '</b></td>\n';
                    tabledata += '<td class="cool" align="right"><b>' + arrRate[stLang] + '</b></td>\n';
                    tabledata += '<td class="cool" align="right"><b>' + arrAmount[stLang] + '</b></td>\n';
                    tabledata += '</tr>\n';

                    var totalRC = 0;
                    for (var j = 0; j < nbrItems; ++j) {
                        ++w;

                        var currentLocation = recProposal.getLineItemValue('item', 'location', j + 1);
                        var currentBS = recProposal.getLineItemText('item', 'billingschedule', j + 1);
                        var currentID = recProposal.getLineItemValue('item', 'item', j + 1);

                        if(currentLocation == arrLocations[l] && currentBS != 'Non Recurring' && currentID != 549){

                            var stItemId = recProposal.getLineItemValue('item', 'item', j + 1);
                            var stItemType = recProposal.getLineItemValue('item', 'itemtype', parseInt(j + 1));
                            var stTerms = recProposal.getLineItemText('item', 'billingschedule', j + 1);
                            if(stItemType == 'Discount'){
                                var recItem = nlapiLoadRecord('discountitem', stItemId);
                            }
                            else{
                                var recItem = nlapiLoadRecord('serviceitem', stItemId);
                            }
                            var stName = recItem.getFieldValue('displayname');
                            if(stLang != 0){
                                var frenchName = recItem.getLineItemValue('translations', 'displayname', 3);
                                if (frenchName != null && frenchName != ''){
                                    stName = frenchName;
                                }
                            }

                            var stQty = recProposal.getLineItemValue('item', 'quantity', parseInt(j + 1));
                            var stRate = recProposal.getLineItemValue('item', 'rate', parseInt(j + 1));
                            var stAmnt = recProposal.getLineItemValue('item', 'amount', parseInt(j + 1));

                            if (stItemId != 210) {
                                totalRC += parseFloat(stAmnt);
                                grandTotalRC += parseFloat(stAmnt);
                            }

                            tabledata += '<tr>\n';
                            tabledata += '<td>' + nlapiEscapeXML(stName) + '</td>\n';
                            if (stItemId == 210) {
                                tabledata += '<td>&nbsp;</td>\n';
                            }
                            else{
                                tabledata += '<td>' + nlapiEscapeXML(stTerms) + '</td>\n';
                            }

                            if (stQty == null || stItemId == 210) {
                                tabledata += '<td align="right">&nbsp;</td>\n';
                            }
                            else {
                                tabledata += '<td align="center">' + nlapiEscapeXML(parseInt(stQty)) + '</td>\n';
                            }
                            if (stRate == null || stRate == '') {
                                tabledata += '<td align="right">&nbsp;</td>\n';
                            }
                            else {
                                tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stRate)) + '</td>\n';
                            }
                            if (stAmnt == null || stItemId == 210) {
                                tabledata += '<td align="right">&nbsp;</td>\n';
                            }
                            else {
                                tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stAmnt)) + '</td>\n';
                            }
                            tabledata += '</tr>\n';
                            //  if(stRate==300){
                            //      tabledata += '</table>';
                            //     tabledata += '</div>';
                            //     tabledata += '<div>';
                            //    tabledata += '<table cellpadding="1" class="profile" align="center" table-layout="fixed">';
                            //   tabledata += '<tr>';
                            //  tabledata += '<td colspan="5" >&nbsp;</td>';
                            //  tabledata += '</tr>';
                            //  tabledata += '</table>';
                            //  closed=1;



                            // }
                            // else{
                            //  closed=0;
                            // }
                        }

                    }
                    //if(closed==0)
                    // {

                    tabledata += '</table>\n';
                    // }

                    tabledata += '<table cellpadding="1" class="profile" align="center" table-layout="fixed">\n';
                    if(closed==0)
                    {
                        tabledata += '<tr>\n';
                        tabledata += '<td colspan="5" >&nbsp;</td>\n';
                        tabledata += '</tr>\n';
                    }
                    // tabledata +='<div>blablablablablablablablablablablablablabla</div>';
                    tabledata += '<tr>\n';
                    tabledata += '<td colspan="5" class="profile t-border" align="right"><b>' + arrFootMRC[stLang] + ' (' + stLocName + '): ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(totalRC).toFixed(2))) + '</b></td>\n';
                    tabledata += '</tr>\n';
                    tabledata += '</table>\n';


                }




                // end recurring block ---------------------------------------------------------------------------


                //tabledata += '<br/><br/><br/>';


                if (arrNRC.length > 0) { // start non recurring block -----------------------------------------------

                    tabledata += '<table cellpadding="1" class="profile" align="center" table-layout="fixed">\n';
                    tabledata += '<tr>\n';
                    tabledata += '<td width="30%">&nbsp;</td>\n';
                    tabledata += '<td width="40%">&nbsp;</td>\n';
                    tabledata += '<td width="10%">&nbsp;</td>\n';
                    tabledata += '<td width="10%">&nbsp;</td>\n';
                    tabledata += '<td width="10%">&nbsp;</td>\n';
                    tabledata += '</tr>\n';

                    tabledata += '<tr>\n';
                    tabledata += '<td colspan="5">' + arrTitleNRC[stLang] + '</td>\n';
                    tabledata += '</tr>\n';
                    tabledata += '<tr>\n';
                    tabledata += '<td colspan="5">&nbsp;</td>\n';
                    tabledata += '</tr>\n';
                    tabledata += '<tr>\n';
                    tabledata += '<td class="cool" colspan="2"><b>' + arrDescr[stLang] + '</b></td>\n';
                    tabledata += '<td class="cool" align="center"><b>' + arrQty[stLang] + '</b></td>\n';
                    tabledata += '<td class="cool" align="right"><b>' + arrRate[stLang] + '</b></td>\n';
                    tabledata += '<td class="cool" align="right"><b>' + arrAmount[stLang] + '</b></td>\n';
                    tabledata += '</tr>\n';

                    var totalNRC = 0;
                    for (var k = 0; k < nbrItems; ++k) {

                        var currentLocation = recProposal.getLineItemValue('item', 'location', k + 1);
                        var currentBS = recProposal.getLineItemText('item', 'billingschedule', k + 1);

                        if(currentLocation == arrLocations[l] && currentBS == 'Non Recurring'){

                            var stItemId = recProposal.getLineItemValue('item', 'item', parseInt(k + 1));
                            var stItemType = recProposal.getLineItemValue('item', 'itemtype', parseInt(k + 1));
                            if(stItemType == 'Discount'){
                                var recItem = nlapiLoadRecord('discountitem', stItemId);
                            }
                            else{
                                var recItem = nlapiLoadRecord('serviceitem', stItemId);
                            }
                            var stName = recItem.getFieldValue('displayname');

                            if(stLang != 0){
                                var frenchName = recItem.getLineItemValue('translations', 'displayname', 3);
                                if (frenchName != null && frenchName != ''){
                                    stName = frenchName;
                                }
                            }

                            var stQty = recProposal.getLineItemValue('item', 'quantity', parseInt(k + 1));
                            var stRate = recProposal.getLineItemValue('item', 'rate', parseInt(k + 1));
                            var stAmnt = recProposal.getLineItemValue('item', 'amount', parseInt(k + 1));

                            totalNRC += parseFloat(stAmnt);
                            grandTotalNRC += parseFloat(stAmnt);

                            tabledata += '<tr>\n';
                            tabledata += '<td colspan="2">' + nlapiEscapeXML(stName) + '</td>\n';
                            if (stQty == null || stQty == '') {
                                tabledata += '<td align="right">&nbsp;</td>\n';
                            }
                            else {
                                tabledata += '<td align="center">' + nlapiEscapeXML(parseInt(stQty)) + '</td>\n';
                            }
                            if (stRate == null || stRate == '') {
                                tabledata += '<td align="right">&nbsp;</td>\n';
                            }
                            else {
                                tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stRate)) + '</td>\n';
                            }
                            if (stAmnt == null || stAmnt == '') {
                                tabledata += '<td align="right">&nbsp;</td>\n';
                            }
                            else {
                                tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stAmnt)) + '</td>\n';
                            }
                            tabledata += '</tr>\n';
                        }
                    }

                    tabledata += '<tr>\n';
                    tabledata += '<td colspan="5">&nbsp;</td>\n';
                    tabledata += '</tr>\n';
                    tabledata += '<tr>\n';
                    tabledata += '<td colspan="5" class="profile t-border" align="right"><b>' + arrFootNRC[stLang] + ' (' + stLocName + '): ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(totalNRC).toFixed(2))) + '</b></td>\n';
                    tabledata += '</tr>\n';

                    tabledata += '</table>\n';

                } // end non recurring block ------------------------------------------------------------------------


                if (arrDisc.length > 0) { // there are first month discount items, so start discount block --------------------------------------------------

                    tabledata += '<table cellpadding="1" class="profile" align="center" table-layout="fixed">\n';
                    tabledata += '<tr>\n';
                    tabledata += '<td width="30%">&nbsp;</td>\n';
                    tabledata += '<td width="40%">&nbsp;</td>\n';
                    tabledata += '<td width="10%">&nbsp;</td>\n';
                    tabledata += '<td width="10%">&nbsp;</td>\n';
                    tabledata += '<td width="10%">&nbsp;</td>\n';
                    tabledata += '</tr>\n';

                    tabledata += '<tr>\n';
                    tabledata += '<td colspan="5">' + arrTitleDisc[stLang] + '</td>\n';
                    tabledata += '</tr>\n';
                    tabledata += '<tr>\n';
                    tabledata += '<td colspan="5">&nbsp;</td>\n';
                    tabledata += '</tr>\n';
                    tabledata += '<tr>\n';
                    tabledata += '<td class="cool"><b>' + arrName[stLang] + '</b></td>\n';
                    tabledata += '<td class="cool"><b>' + arrDescr[stLang] + '</b></td>\n';
                    //tabledata += '<td class="cool"><b>' + arrTerms[stLang] + '</b></td>';
                    tabledata += '<td class="cool">&nbsp;</td>\n';
                    //tabledata += '<td class="cool" align="center"><b>' + arrQty[stLang] + '</b></td>';
                    tabledata += '<td class="cool" align="right"><b>' + arrRate[stLang] + '</b></td>\n';
                    tabledata += '<td class="cool" align="right"><b>' + arrAmount[stLang] + '</b></td>\n';
                    tabledata += '</tr>';

                    var totalDisc = 0;
                    for (var n = 0; n < nbrItems; ++n) {

                        var currentLocation = recProposal.getLineItemValue('item', 'location', n + 1);
                        var currentBS = recProposal.getLineItemText('item', 'billingschedule', n + 1);
                        var currentID = recProposal.getLineItemValue('item', 'item', n + 1);
                        var description = recProposal.getLineItemValue('item', 'description', n + 1);

                        if(currentLocation == arrLocations[l] && currentID == 549){

                            var stItemId = recProposal.getLineItemValue('item', 'item', n + 1);
                            var stItemType = recProposal.getLineItemValue('item', 'itemtype', parseInt(n + 1));
                            var stTerms = recProposal.getLineItemText('item', 'billingschedule', n + 1);
                            if(stItemType == 'Discount'){
                                var recItem = nlapiLoadRecord('discountitem', stItemId);
                            }
                            else{
                                var recItem = nlapiLoadRecord('serviceitem', stItemId);
                            }
                            var stName = recItem.getFieldValue('displayname');
                            var stDescription = recItem.getFieldValue('description');
                            if(stLang != 0){
                                var frenchName = recItem.getLineItemValue('translations', 'displayname', 3);
                                var frenchDescr = recItem.getLineItemValue('translations', 'description', 3);
                                if (frenchName != null && frenchName != ''){
                                    stName = frenchName;
                                }
                            }

                            var stQty = recProposal.getLineItemValue('item', 'quantity', parseInt(n + 1));
                            var stRate = recProposal.getLineItemValue('item', 'rate', parseInt(n + 1));
                            var stAmnt = recProposal.getLineItemValue('item', 'amount', parseInt(n + 1));

                            totalDisc += parseFloat(stAmnt);
                            grandTotalDisc += parseFloat(stAmnt);

                            tabledata += '<tr>\n';
                            tabledata += '<td>' + nlapiEscapeXML(stName) + '</td>\n';
                            tabledata += '<td>' + nlapiEscapeXML(description) + '</td>\n';
                            //tabledata += '<td>&nbsp;</td>';
                            tabledata += '<td>&nbsp;</td>\n';
                            if (stRate == null || stRate == '') {
                                tabledata += '<td align="right">&nbsp;</td>\n';
                            }
                            else {
                                tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stRate)) + '</td>\n';
                            }
                            tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stAmnt)) + '</td>\n';
                            tabledata += '</tr>\n';
                        }

                    }

                    tabledata += '<tr>\n';
                    tabledata += '<td colspan="5">&nbsp;</td>\n';
                    tabledata += '</tr>\n';
                    tabledata += '<tr>\n';
                    tabledata += '<td colspan="5" class="profile t-border" align="right"><b>' + arrFootDisc[stLang] + ' (' + stLocName + '): ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(totalDisc).toFixed(2))) + '</b></td>\n';
                    tabledata += '</tr>\n';

                    tabledata += '</table>\n';

                } // end discount block ---------------------------------------------------------------------------

            }
            if(arrLocations != null){
                if(parseInt(arrLocations.length) > 1 && (parseInt(grandTotalRC) > 0 || parseInt(grandTotalNRC) > 0)){

                    //tabledata += '<br />';


                    tabledata += '<table class="profile page-break" align="center">\n';
                    tabledata += '<tr>\n';
                    if (quotePDF == 'T'){
                        tabledata += '<td><h2>' + arrTitleBQTotals[stLang] + '</h2></td>\n';
                    }
                    else{
                        tabledata += '<td><h2>' + arrTitleContractTotals[stLang] + '</h2></td>\n';
                    }
                    tabledata += '</tr>\n';
                    tabledata += '</table>\n';

                    tabledata += '<table cellpadding="1" class="profile" align="center" table-layout="fixed">\n';


                    if(parseInt(grandTotalRC) > 0){
                        tabledata += '<tr>\n';
                        tabledata += '<td>&nbsp;</td>\n';
                        tabledata += '</tr>\n';
                        tabledata += '<tr>\n';
                        tabledata += '<td class="profile t-border" align="right"><b>' + arrFootAllRC[stLang] + ': ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(grandTotalRC).toFixed(2))) + '</b></td>\n';
                        tabledata += '</tr>\n';
                    }
                    if(parseInt(grandTotalNRC) > 0){
                        tabledata += '<tr>\n';
                        tabledata += '<td>&nbsp;</td>\n';
                        tabledata += '</tr>\n';
                        tabledata += '<tr>\n';
                        tabledata += '<td class="profile t-border" align="right"><b>' + arrFootAllNRC[stLang] + ': ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(grandTotalNRC).toFixed(2))) + '</b></td>\n';
                        tabledata += '</tr>\n';
                    }
                    if(parseInt(grandTotalDisc) > 0){
                        tabledata += '<tr>\n';
                        tabledata += '<td>&nbsp;</td>\n';
                        tabledata += '</tr>\n';
                        tabledata += '<tr>\n';
                        tabledata += '<td class="profile t-border" align="right"><b>' + arrFootAllDisc[stLang] + ': ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(grandTotalDisc).toFixed(2))) + '</b></td>\n';
                        tabledata += '</tr>\n';
                    }
                    tabledata += '</table>\n';
                }
            }

            //build customer address
            var tablehtmlAddress = '';
            tablehtmlAddress = '<table cellpadding="0" border="0" table-layout="fixed">\n';
            tablehtmlAddress += '<tr>\n';
            tablehtmlAddress += '<td>\n';
            tablehtmlAddress += '<b>' + nlapiEscapeXML(custCompName) + '</b><br/>\n';
            tablehtmlAddress += '<b>' + arrAttn[stLang] + ':</b>' + nlapiEscapeXML(stSOAtt) + '<br/>\n';
            tablehtmlAddress += nlapiEscapeXML(billAddress1) + '<br/>\n';
            //if (billAddress2) {
            //	tablehtmlAddress += nlapiEscapeXML(billAddress2) + '<br/>';
            //}
            tablehtmlAddress += nlapiEscapeXML(billCity) + ', ' + nlapiEscapeXML(billState) + ' ' + nlapiEscapeXML(billZipcode) + ' <br/>\n';
            tablehtmlAddress += nlapiEscapeXML(billCountry);
            tablehtmlAddress += '</td>\n';
            tablehtmlAddress += '</tr>\n';
            tablehtmlAddress += '</table>\n';

            //build data center/location address

            var arrLocation = nlapiLookupField('location', arrLocations[0], ['name', 'address1', 'address2', 'city', 'country', 'state', 'zip']);
            var stLocName = arrLocation['name'];
            var stLocAddr1 = arrLocation['address1'];
            var stLocAddr2 = arrLocation['address2'];
            var stLocCity = arrLocation['city'];
            var stLocCountry = arrLocation['country'];
            var stLocState = arrLocation['state'];
            var stLocZip = arrLocation['zip'];

            var tablehtmlLocation = '<b>\n';
            tablehtmlLocation += nlapiEscapeXML(stLocName) + '</b><br/>\n';
            tablehtmlLocation += nlapiEscapeXML(stLocAddr1) + '<br/>\n';
            if (stLocAddr2) {
                tablehtmlLocation += nlapiEscapeXML(stLocAddr2) + '<br/>\n';
            }
            tablehtmlLocation += nlapiEscapeXML(stLocCity) + ', ' + nlapiEscapeXML(stLocState) + ' ' + nlapiEscapeXML(stLocZip) + ' <br/>\n';
            tablehtmlLocation += nlapiEscapeXML(stLocCountry);

            //build Usage Charges section
            var tablePowerCharges = '';
            if (powerUsage == 'T') {
                tablePowerCharges += '<table cellpadding="5" class="profile" align="center" table-layout="fixed" style="page-break-inside: avoid;">\n';
                tablePowerCharges += '<tr>\n';
                tablePowerCharges += '<td width="30%">&nbsp;</td>\n';
                tablePowerCharges += '<td width="40%">&nbsp;</td>\n';
                tablePowerCharges += '<td width="10%">&nbsp;</td>\n';
                tablePowerCharges += '<td width="10%">&nbsp;</td>\n';
                tablePowerCharges += '<td width="10%">&nbsp;</td>\n';
                tablePowerCharges += '</tr>\n';
                tablePowerCharges += '<tr>\n';
                tablePowerCharges += '<td colspan="5">' + arrUsageCharges[stLang] + '</td>\n';
                tablePowerCharges += '</tr>\n';
                tablePowerCharges += '<tr>\n';
                tablePowerCharges += '<td colspan="5">&nbsp;</td>\n';
                tablePowerCharges += '</tr>\n';
                tablePowerCharges += '<tr>\n';
                tablePowerCharges += '<td class="cool"><b>' + arrDescr[stLang] + '</b></td>\n';
                tablePowerCharges += '<td class="cool">&nbsp;</td>\n';
                tablePowerCharges += '<td class="cool" align="center">&nbsp;</td>\n';
                tablePowerCharges += '<td class="cool" align="right"><b>' + arrRate[stLang] + '</b></td>\n';
                tablePowerCharges += '<td class="cool" align="right">&nbsp;</td>\n';
                tablePowerCharges += '</tr>\n';
                tablePowerCharges += '<tr>\n';
                tablePowerCharges += '<td>' + arrPowerUsage[stLang] + '</td>\n';
                tablePowerCharges += '<td>&nbsp;</td>\n';
                tablePowerCharges += '<td>&nbsp;</td>\n';
                tablePowerCharges += '<td align="right">' + usageRate + '</td>\n';
                tablePowerCharges += '<td>&nbsp;</td>\n';
                tablePowerCharges += '</tr>\n';
                tablePowerCharges += '<tr>\n';
                tablePowerCharges += '<td colspan="5" class="profile t-border">&nbsp;</td>\n';
                tablePowerCharges += '</tr>\n';
                tablePowerCharges += '</table>\n';
                tablePowerCharges += '<br/>\n';
            }

            //build Notes
            var tablehtmlNotes = '';
            if (stSONotes != '') {
                tablehtmlNotes += '<table cellpadding="2" class="profile" align="center" table-layout="fixed" style="page-break-inside: avoid;">\n';

                // if there is something in Notes
                if(stSONotes != ''){
                    tablehtmlNotes += '<tr><td><h2>' + arrNotes[stLang] + '</h2></td></tr>\n';
                    tablehtmlNotes += '<tr><td class="profile t-border" style="font-family: Arial, Helvetica, sans-serif; font-size:7pt;">'+ stSONotes + '</td></tr>\n';
                }
                tablehtmlNotes += '</table>\n';
                tablehtmlNotes += '<br/>\n';
            }

            //build SOs
            var tablehtmlSOs = '';
            if(arrSOs != null){
                if (arrSOs.length > 0) {
                    tablehtmlSOs += '<table cellpadding="2" class="profile" align="center" table-layout="fixed" style="page-break-inside: avoid;">\n';
                    // if a renewal, print SOs
                    if(arrSOs.length > 0){
                        var txtNotes = '';
                        for (var i = 0; i < arrSOs.length; i++){
                            txtNotes += arrSOsNames[i] + ', ';
                        }
                        txtNotes = txtNotes.slice(0,txtNotes.length-2);
                        tablehtmlSOs += '<tr><td><h2>' + arrFromSOs[stLang] + '</h2></td></tr>\n';
                        tablehtmlSOs += '<tr><td class="profile t-border" style="font-family: Arial, Helvetica, sans-serif; font-size:7pt;">' + txtNotes + '</td></tr>\n';
                    }
                    tablehtmlSOs += '</table>\n';
                    tablehtmlSOs += '<br/>\n';
                }
            }


            var objFile = nlapiLoadFile(template);
            var stMainHTML = objFile.getValue();

            var d = new moment();

            stMainHTML = stMainHTML.replace(new RegExp('{location}', 'g'), tablehtmlLocation);
            stMainHTML = stMainHTML.replace(new RegExp('{billAddress}', 'g'), tablehtmlAddress);
            stMainHTML = stMainHTML.replace(new RegExp('{sodate}', 'g'), d.format('M/D/YYYY'));
            stMainHTML = stMainHTML.replace(new RegExp('{sonbr}', 'g'), stSONbr);
            stMainHTML = stMainHTML.replace(new RegExp('{sorep}', 'g'), stSORep);
            stMainHTML = stMainHTML.replace(new RegExp('{totalcharges}', 'g'), '');
            stMainHTML = stMainHTML.replace(new RegExp('{items}', 'g'),  tabledata);
            stMainHTML = stMainHTML.replace(new RegExp('{usage}', 'g'), tablePowerCharges);
            stMainHTML = stMainHTML.replace(new RegExp('{notes}', 'g'), tablehtmlNotes);
            stMainHTML = stMainHTML.replace(new RegExp('{sos}', 'g'), tablehtmlSOs);
            stMainHTML = stMainHTML.replace(new RegExp('{terms}', 'g'), stSOTerms);
            var filePDF = nlapiXMLToPDF(stMainHTML);
            filePDF.setName('test.pdf');
            var emailSubject = 'Proposal';
            var emailBody = 'tabledata1: '+stMainHTML;
            nlapiSendEmail(206211, 206211,emailSubject,emailBody,null, null, null, filePDF, null, null); // Send email to Catalina
            return;



            //var htmlName = filePrefix + langPrefix + stSONbr + '_' + custCompName + '_' + d.format('M/D/YYYY') + '_' + d.format('H-m-s') + '_' + currencyType + '.html'
            //var htmlFile = nlapiCreateFile(htmlName, 'PLAINTEXT', stMainHTML);
            //htmlFile.setFolder(fileFolder);
            //nlapiSubmitFile(htmlFile);

            //       var filePDF = nlapiXMLToPDF(textXML);
            //  custCompName = custCompName.replace(/\ /g,"_");
            //filePDF.setName(filePrefix + langPrefix + stSONbr + '_' + custCompName + '_' + d.format('M/D/YYYY') + '_' + d.format('H-m-s') + '_' + currencyType + '.pdf');
            //filePDF.setFolder(fileFolder);

            // var fileId = nlapiSubmitFile(filePDF);

            // if (quotePDF == 'T'){
            //   nlapiSubmitField('estimate', proposalid, 'custbody_clgx_prop_bq_file_id', fileId);
            // }
            // else{
            //  nlapiSubmitField('estimate', proposalid, 'custbody_clgx_prop_contract_file_id', fileId);
            // }

            //     nlapiSubmitField('estimate', proposalid, 'custbody_clgx_contract_terms_ready', 'F');
            //   nlapiSubmitField('estimate', proposalid, 'custbody_clgx_print_budgetray_quote', 'F');
        }

        //  }



//---------- End Section 1 ------------------------------------------------------------------------------------------------



//---------- End Sections  ------------------------------------------------------------------------------------------------
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function inArray(val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}


function addCommas(nStr){
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
