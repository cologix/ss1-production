nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Bill.js
//	Script Name:	CLGX_SU_Bill
//	Script Id:		customscript_clgx_su_bill
//	Script Runs:	On Server
//	Script Type:	User Event
//	Deployments:	Bill, Bill Credit
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/12/2012
//-------------------------------------------------------------------------------------------------

function afterSubmit(type){
    try{
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 2/2/2011
// Details:	If Bill or Bill credit has Project Name on the header, populate column Project ID with the same project
//-------------------------------------------------------------------------------------------------
		var currentContext = nlapiGetContext();
		if (currentContext.getExecutionContext() == 'userinterface' && (type == 'create' || type == 'edit')) {

	        var recBill = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
	        var projectID = recBill.getFieldValue('custbody_cologix_project_name');
	        
	        var stNbrItems = recBill.getLineItemCount('item');
	        
	        if(projectID != null && projectID != ''){
				for (var i = 0; stNbrItems != null && i < parseInt(stNbrItems); i++) {
					recBill.selectLineItem('item', i + 1)
					recBill.setCurrentLineItemValue('item', 'custcol_clgx_journal_project_id', projectID);
					recBill.commitLineItem('item');
				}
				nlapiSubmitRecord(recBill, false, true);
	        }
		}
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}