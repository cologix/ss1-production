nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_Opportunity.js
//	Script Name:	CLGX_CR_Opportunity
//	Script Id:		customscript_clgx_cr_opportunity
//	Script Runs:	On Client
//	Script Type:	Client Script
//	Deployments:	Service Order - Sandbox | Production
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		1/20/2012
//	Updated:		1/20/2012
//	Includes:		CLGX_LIB_Global.js
//-------------------------------------------------------------------------------------------------

function pageInit(type){
    try {
//------------- Begin Section 2 -------------------------------------------------------------------
// Version:	1.0 - 4/1/2013
// Details:	If this customer is on credit hold, display an warning
//-------------------------------------------------------------------------------------------------

        var customerID = nlapiGetFieldValue('entity');
        if(customerID != null && customerID != ''){

            var arrColumns = new Array();
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
            arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof',customerID));
            var searchCustomers = nlapiSearchRecord('customer', null, arrFilters, arrColumns);

            if(searchCustomers != null){
                var recCustomer = nlapiLoadRecord('customer', customerID);
                var onHold = recCustomer.getFieldValue('creditholdoverride');
                if(onHold == 'ON'){
                    nlapiSetFieldValue('custbody_clgx_credit_hold_warning', '<div style="color:#FF0000">This customer is on credit hold!</div>');
                }
            }

        }
//---------- End Section 2 ------------------------------------------------------------------------------------------------
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function fieldChanged(type, name) {
    try {
        nlapiLogExecution('DEBUG','Client Event - fieldChanged','|--------------------STARTED---------------------|');
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 1/20/2012
// Details:	If this customer is on credit hold, display an warning
//-----------------------------------------------------------------------------------------------------------------

        var currentContext = nlapiGetContext();
        var stRole = currentContext.getRole();
        if (currentContext.getExecutionContext() == 'userinterface') {
            if (name == 'custbody_cologix_opp_incremental_mrc') {
                var incremental = nlapiGetFieldValue('custbody_cologix_opp_incremental_mrc');
                if(parseFloat(incremental)<parseFloat(0))
                {
                    alert('Incremental MRC must be greater than zero!');
                    return false;
                }
            }

            if (name == 'entity') {
                var customerID = nlapiGetFieldValue('entity');

                if(customerID != null && customerID != ''){

                    var currentContext = nlapiGetContext();
                    var stRole = currentContext.getRole();

                    var recCustomer = nlapiLoadRecord('customer', customerID);
                    var onHold = recCustomer.getFieldValue('creditholdoverride');
                    if(onHold == 'ON'){
                        nlapiSetFieldValue('custbody_clgx_credit_hold_warning', '<div style="color:#FF0000">This customer is on credit hold!</div>');
                    }
                    else{
                        nlapiSetFieldValue('custbody_clgx_credit_hold_warning', '<div style="color:#FF0000"></div>');
                    }
                }
            }
        }

//---------- End Section 1 ------------------------------------------------------------------------------------------------


        nlapiLogExecution('DEBUG','Client Event - fieldChanged','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function lineInit(type){
    try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 1/20/2012
// Details:	Disable field 'class' on 'item' sublist.
//-----------------------------------------------------------------------------------------------------------------

        if (type=='item'){
            nlapiDisableLineItemField('item', 'class', true);
        }

//---------- End Section 1 ------------------------------------------------------------------------------------------------

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function saveRecord(){
    try {

        var currentContext = nlapiGetContext();
        stRole = currentContext.getRole();

//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 2/28/2013
// Details:	Verify if this customer has services at the designated location. If yes, don't allow save.
//-------------------------------------------------------------------------------------------------
        var allowSave = 1;
        var alertMsg = '';

        var location = nlapiGetFieldValue('location');
        var currentContext = nlapiGetContext();
        var usrRole = currentContext.getRole();
        var facility = returnFacility(location);
        //var facility = parseInt(clgx_return_facilityid(location));
        var customer = nlapiGetFieldValue('entity');
        var saletype = nlapiGetFieldValue('custbody_cologix_opp_sale_type');
        var hasDuplicates = nlapiLookupField('customer', customer, 'hasduplicates');
        var customerStatus = nlapiLookupField('customer', customer, 'entitystatus');

        var serviceColumn = new Array();
        serviceColumn.push(new nlobjSearchColumn('internalid',null,null));
        var serviceFilter = new Array();
        serviceFilter.push(new nlobjSearchFilter('customer',null,'anyof',customer));
        serviceFilter.push(new nlobjSearchFilter('custentity_cologix_facility',null,'anyof',facility));
        var serviceResult = nlapiSearchRecord('job',null,serviceFilter,serviceColumn);
        var itemsCount =nlapiGetLineItemCount('item');
        // alert(itemsCount);
        //Only if user has admin, full or AR Clerk roles can add discount items to opportunity
        // if ((usrRole != 1011)&&(itemsCount>0))
        if (itemsCount>0)
        {
            for ( var j = 1; j <= itemsCount; j++ ) {
                //  var itemId= nlapiGetLineItemValue('item', 'item', j);
                var itemType=  nlapiGetLineItemValue('item', 'itemtype', j);
                var amount= nlapiGetLineItemValue('item', 'amount', j);
                if((itemType=='Discount')&&(usrRole != 1011 && usrRole != 3 && usrRole != 18 && usrRole!=5))
                {
                    alertMsg = 'Discount Item is not valid for this opportunity';
                    allowSave=0;
                }
                if((itemType!='Discount')&&(usrRole != 3 && usrRole != 18)&&(amount<0))
                {
                    alertMsg='Negative amounts are not permited';
                    allowSave=0;

                }
            }
        }

        if (serviceResult != null && (usrRole!=18|| usrRole!=1088) && (saletype == 3 || saletype == 4)) {
            alertMsg = 'You can\'t choose "New Location" or "New Logo" sale types because this customer already has services at this location';
            allowSave = 0;
        }
        else if ((customerStatus == 13 || hasDuplicates == 'T') && saletype == 4 && (usrRole!=18|| usrRole!=1088)){
            alertMsg = 'You can\'t choose "New Logo" sale type because this is an existing customer.';
            allowSave = 0;
        }
        else{
        }

//------------- Begin Section 2 -------------------------------------------------------------------
// Version:	1.0 - 4/4/2013
// Details:	If Status is Closed-Lost, Loss reason is mandatory
//-------------------------------------------------------------------------------------------------
        var status = nlapiGetFieldText('entitystatus');
        var lossReason = nlapiGetFieldValue('winlossreason');
        var order_notes= nlapiGetFieldValue('memo');

        if (status == 'Closed Lost' && (lossReason == null || lossReason == '')) {
            alertMsg = 'If status is "Closed-Lost" then you have to choose the Loss reason.';
            allowSave = 0;
        }
        if (status == 'Closed Lost' && (order_notes == null || order_notes == '')) {
            alertMsg = 'If status is "Closed-Lost" then you have to choose the Loss reason and enter an Order Note.';
            allowSave = 0;
        }
        //------------- Begin Section 2 -------------------------------------------------------------------
        // Date:	01/24/1014
        // Details:	Restrict DAL1 and DAL2 for certain items
        //-------------------------------------------------------------------------------------------------
        // 39 DAL1
        // 40 DAL2
        //ID – 222 “Network – Bandwidth"
        //ID – 523 "IP Services”
        //ID – 355 “Fixed”
        //ID – 554 “Burstable Commit"
        //ID – 555 "Burstable Commit Redundant Port"
        //ID - 556 “Fixed Redundant Port"

        /*
         var nbrItems = nlapiGetLineItemCount('item');

         var strItems = '';
         for ( var i = 1; i < nbrItems + 1; i++ ) {
         var itemid = nlapiGetLineItemValue('item', 'item', i);
         var item = nlapiGetLineItemText('item', 'item', i);
         var locationid = nlapiGetLineItemValue('item', 'location', i);

         if((locationid == 2 || locationid == 17) && stRole != '3' && stRole != '-5' && stRole != '18'){
         if(itemid == 222 || itemid == 523 || itemid == 355 || itemid == 554 || itemid == 555 || itemid == 556){
         strItems += item + ', ';
         allowSave = 0;
         alertMsg = 'There are service(s) that cannot be sold at this facility';
         }
         }
         }
         */
//-------------------------------------------------------------------------------------------------
        if (allowSave == 0) {
            alert(alertMsg);
            return false;
        }
        else{
            return true;
        }

    }

    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function returnFacility(locationID){
    var facilityID = 0;
    switch(locationID) {
        case '1':
            //Cologix HQ
            facilityID = 16;
            break;
        case '2':
            // DAL1
            facilityID = 3;
            break;
        case '5':
            //MTL1
            facilityID = 2;
            break;
        case '6':
            //TOR1
            facilityID = 8;
            break;
        case '7':
            //VAN1
            facilityID = 14;
            break;
        case '8':
            //MTL2
            facilityID = 5;
            break;
        case '9':
            //MTL3
            facilityID = 4;
            break;
        case '10':
            //MTL4
            facilityID = 9;
            break;
        case '11':
            //MTL5
            facilityID = 6;
            break;
        case '12':
            //MTL6
            facilityID = 7;
            break;
        case '13':
            //TOR 156 Front Street West
            facilityID = 23;
            break;
        case '14':
            facilityID = 8;
            break;
        case '15':
            //TOR2
            facilityID = 15;
            break;
        case '16':
            //MIN1&2
            facilityID = 17;
            break;
        case '17':
            // DAL2
            facilityID = 18;
            break;
        case '18':
            facilityID = 8;
            break;
        case '27':
            //MTL7
            facilityID = 19;
            break;
        case '28':
            //VAN2
            facilityID = 20;
            break;
        case '31':
            //JAX1
            facilityID = 21;
            break;
        case '34':
            //COL1&2
            facilityID = 24;
            break;
        case '35':
            //MIN3
            facilityID = 25;
            break;
        case '39':
            // DAL1
            facilityID = 3;
            break;
        case '40':
            // DAL1
            facilityID = 3;
            break;
        case '41':
            // DAL1
            facilityID = 3;
            break;
        default:
            facilityID = 0;
    }
    return facilityID;
}
function getDateTime(){
    var date = new Date();
    var month = parseInt(date.getMonth()) + 1;
    var day = date.getDate();
    var year = date.getFullYear();


    var hour = date.getHours();
    var minute = date.getMinutes();
    var second = date.getSeconds();

    if(parseInt(month) < 10){
        month = '0' + month;
    }
    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = month + '/' + day + '/' + year + ' ' + hour + ':' + minute + ':' + second;

    return formattedDate;
}