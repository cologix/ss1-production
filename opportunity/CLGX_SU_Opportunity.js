nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Opportunity.js
//	Script Name:	CLGX_SU_Opportunity
//	Script Id:		customscript_clgx_su_opportunity
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	0pportunity
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/21/2011
//	Includes:		CLGX_LIB_Transaction_Totals.js, underscore-min.js
//-------------------------------------------------------------------------------------------------

function beforeLoad (type, form) {
    try {

        var currentContext = nlapiGetContext();
        var stRole = currentContext.getRole();
        var allow = 0;
        if (stRole == '-5' || stRole == '3' || stRole == '18') {
            allow = 1;
        }
        var renewedFromSos=nlapiGetFieldValues('custbody_clgx_renewed_from_sos');
        var saletype=nlapiGetFieldValue('custbody_cologix_opp_sale_type');
        if((stRole==1017)&&(saletype==1)&&((renewedFromSos=='')||(renewedFromSos==null)))
        {
            form.getField('custbody_cologix_opp_incremental_mrc').setDisplayType('normal');
          //  form.getField('custbody_cologix_opp_incremental_mrc').setMandatory(true);

        }
        var closed = 0;
        if(closed == 1 && allow == 0 && (type == 'create' || type == 'edit')){ // if module is closed and any other role then admin
            var arrParam = new Array();
            arrParam['custscript_internal_message'] = 'The opportunity module is closed for modifications. Sorry for the inconvenience. Please come back later.';
            nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
        }

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Created:	4/15/2014
// Details: Display transaction totals in the header in an inlineHTML field
//-----------------------------------------------------------------------------------------------------------------

        if (type == 'view' ||type == 'edit') {

            var opptyid = nlapiGetRecordId();

            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_oppty_location',null,null).setSort(false));
            //arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_oppty_class',null,null).setSort(false));
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_oppty_total',null,null));
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_oppty_total_nrc',null,null));
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_opportunity",null,"anyof",opptyid));
            //arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_oppty_class",null,"anyof",1));
            var searchTotals = nlapiSearchRecord('customrecord_clgx_totals_opportunity', null, arrFilters, arrColumns);

            var html = '<table cellpadding="2" border="1" style="font-family: verdana,arial,sans-serif;font-size:11px;color:#333333;border-width: 1px;border-color: #999999;border-collapse: collapse;padding:5px;">';

            html += '<tr>';
            html += '<th style="padding: 5px;background-color: #dedede;">Location</th>';
            //html += '<th style="padding: 5px;background-color: #dedede;">Class</th>';
            html += '<th style="padding: 5px;background-color: #dedede;">Total Recurring</th>';
            html += '<th style="padding: 5px;background-color: #dedede;">Total NRC</th>';
            html += '</tr>';

            for ( var i = 0; searchTotals != null && i < searchTotals.length; i++ ) {
                var searchTotal = searchTotals[i];
                html += '<tr>';
                html += '<td style="padding: 5px;">' + searchTotal.getText('custrecord_clgx_totals_oppty_location',null,null) + '</td>';
                //html += '<td style="padding: 5px;">' + searchTotal.getText('custrecord_clgx_totals_oppty_class',null,null) + '</td>';
                html += '<td style="padding: 5px;">$' + addCommas(searchTotal.getValue('custrecord_clgx_totals_oppty_total',null,null)) + '</td>';
                html += '<td style="padding: 5px;">$' + addCommas(searchTotal.getValue('custrecord_clgx_totals_oppty_total_nrc',null,null)) + '</td>';
                html += '</tr>';

            }
            html += '</table>';

            nlapiSetFieldValue('custbody_clgx_transaction_totals', html);
        }

//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 4/1/2013
// Details:	If this customer is on credit hold, display an warning
//-------------------------------------------------------------------------------------------------
        var currentContext = nlapiGetContext();
        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'view' || type == 'edit')){
            var customerID = nlapiGetFieldValue('entity');

            if(customerID != null && customerID != ''){

                var recCustomer = nlapiLoadRecord('customer', customerID);
                var onHold = recCustomer.getFieldValue('creditholdoverride');
                if(onHold == 'ON'){
                    nlapiSetFieldValue('custbody_clgx_credit_hold_warning', '<div style="color:#FF0000">This customer is on credit hold!</div>');
                }
            }
        }
//---------- End Sections ------------------------------------------------------------------------------------------------

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function beforeSubmit(type){
    try {
        nlapiLogExecution('DEBUG','User Event - Before Load','|-------------STARTED--------------|');

//------------- Begin Section 1 -------------------------------------------------------------------
// Created:	2/7/2014
// Details:	When an opportunity is deleted, update any SO that was used to it's renewal
//-------------------------------------------------------------------------------------------------
        var currentContext = nlapiGetContext();
        var stRole = currentContext.getRole();
        
        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'delete')){

            var arrSOs = nlapiGetFieldValues('custbody_clgx_renewed_from_sos');
            for ( var i = 0; arrSOs != null && i < arrSOs.length; i++ ) {
                nlapiSubmitField('salesorder', arrSOs[i], 'custbody_clgx_so_renewed_on_oppty', '');
            }
        }
        if (type == 'xedit' && ((stRole == '1018')||(stRole =='1046')||(stRole =='1040'))){
        	throw "You may not inline edit an opportunity record!";
        }

//---------- End Sections ------------------------------------------------------------------------------------------------

        nlapiLogExecution('DEBUG','User Event - Befor Load','|-------------FINISHED--------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function afterSubmit(type){
    try {

//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 1/26/2012
// Details:	Calculates NRC and MRC totals from items sublist on header fields.
//-------------------------------------------------------------------------------------------------

        var currentContext = nlapiGetContext();
		var userid = nlapiGetUser();
		var roleid = currentContext.getRole();
        var opptyid = nlapiGetRecordId();
        var inactive = nlapiGetFieldValue('isinactive');
        var json = nlapiGetFieldValue('custbody_clgx_sp_json_sync');
        	
        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'edit' || type == 'create')){

            var stNbrItems = nlapiGetLineItemCount('item');
            var curNRC = 0;
            var curMRC = 0;

            for (var i = 0; i < parseInt(stNbrItems); i++) {
                var currAmount = nlapiGetLineItemValue('item', 'amount', i + 1);
                if (currAmount == null || currAmount == ''){
                    currAmount = 0;
                }
                var stBS = nlapiGetLineItemText('item', 'class', i + 1);
                if (stBS.indexOf("Recurring") > -1) {
                    curMRC = curMRC + parseFloat(currAmount);
                }
                else if (stBS.indexOf("NRC") > -1){
                    curNRC = curNRC + parseFloat(currAmount);
                }
                else {

                }
            }
            //nlapiSubmitField('opportunity', opptyid, ['custbody_clgx_total_recurring','custbody_clgx_total_non_recurring'], [curMRC,curNRC]);
			var recOpportunity = nlapiLoadRecord('opportunity', opptyid);
			recOpportunity.setFieldValue('custbody_clgx_total_recurring', curMRC);
			recOpportunity.setFieldValue('custbody_clgx_total_non_recurring', curNRC);
			nlapiSubmitRecord(recOpportunity, false, true);

        }

//------------- Begin Section 2 -------------------------------------------------------------------
// Version:	1.0 - 4/14/2014
// Details:	Calculates NRC and MRC totals from items sublist to custom record 'Transaction Totals'
//-------------------------------------------------------------------------------------------------

        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'edit' || type == 'create')){

            var customerid = nlapiGetFieldValue('entity');
            var totals = clgx_transaction_totals (customerid, 'oppty', opptyid);

        }
        

//------------- Begin Section 3 -----------------------------------------------------------------------------------
// Created:	04/23/2015
// Details:	Queue transaction to syncronize with SilverPOP
//-----------------------------------------------------------------------------------------------------------------
        
		if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'create' || type == 'xedit' || type == 'edit' || type == 'delete')) {
			
			var action = 1;
			if(type == 'create'){
				action = 0;
			}
			if(type == 'delete'){
				action = 2;
			}
			if(inactive == 'T'){
				action = 3;
			}
			
			var arrColumns = new Array();
	    	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	    	var arrFilters = new Array();
	    	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_transact_to_sp_transact",null,"equalto",opptyid));
	    	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_transact_to_sp_type",null,"equalto",1));
	    	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_transact_to_sp_done',null,'is','F'));
	    	var searchQueue = nlapiSearchRecord('customrecord_clgx_queue_transact_to_sp', null, arrFilters, arrColumns);
	    	
	  		if(searchQueue == null){ // add contact to queue if not in it
	    		var record = nlapiCreateRecord('customrecord_clgx_queue_transact_to_sp');
				record.setFieldValue('custrecord_clgx_transact_to_sp_transact', opptyid);
				record.setFieldValue('custrecord_clgx_transact_to_sp_type', 1);
				record.setFieldValue('custrecord_clgx_transact_to_sp_action', action);
				record.setFieldValue('custrecord_clgx_transact_to_sp_done', 'F');
				record.setFieldValue('custrecord_clgx_transact_to_sp_json', json);
				var idRec = nlapiSubmitRecord(record, false,true);
	  		}
	  		else{ // if in queue change status in case it was different
	          	nlapiSubmitField('customrecord_clgx_queue_transact_to_sp', searchQueue[0].getValue('internalid',null,null), 'custrecord_clgx_transact_to_sp_action', action);
	  		}
		}
       
//---------- End Sections ------------------------------------------------------------------------------------------------

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function addCommas(nStr){
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
