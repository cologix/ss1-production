nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Message.js
//	Script Name:	CLGX_SU_Message
//	Script Id:		customscript_clgx_su_message
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Message - Sandbox | Production
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/21/2011
//-------------------------------------------------------------------------------------------------
function beforeLoad(type) {
	try {

//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 3/4/2013
// Details:	Un check the include transaction field
//-------------------------------------------------------------------------------------------------

		nlapiSetFieldValue('includetransaction', 'F');

//---------- End Section 1 ------------------------------------------------------------------------------------------------

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------

}


function afterSubmit(type){
	
	try {
		
//------------- Begin Section 1 -------------------------------------------------------------------
// Date		07/24/2014
// Details:	If case has child cases, send the same message to them
//-------------------------------------------------------------------------------------------------
		
		var currentContext = nlapiGetContext();
		if (currentContext.getExecutionContext() == 'userinterface') {
			
		var messageid = nlapiGetRecordId();
		var userid = nlapiGetUser();
		var sendto = 'F';
		var message = '';
		
		var messageid = nlapiGetRecordId();
		var recMessage = nlapiLoadRecord ('message',messageid);
		var caseid = recMessage.getFieldValue('activity');
		var message = recMessage.getFieldValue('message');
		
		if(caseid > 0){
			sendto = 'T';
			
			// verify if there are children cases already created
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custevent_clgx_parent_id",null,"anyof",caseid));
			var searchChildren = nlapiSearchRecord('supportcase', null, arrFilters, arrColumns);
			
		    if(searchChildren != null){
			    var arrParam = new Array();
				arrParam['custscript_ss_case_message_userid'] = userid;
				arrParam['custscript_ss_case_message_caseid'] = caseid;
				arrParam['custscript_ss_case_message_sendto'] = sendto;
				arrParam['custscript_ss_case_message_message'] = message;
				arrParam['custscript_ss_case_message_messageid'] = messageid;
		        var status = nlapiScheduleScript('customscript_clgx_ss_case_message', null ,arrParam);
		        //nlapiSubmitField('supportcase', caseid, 'custevent_clgx_locked_case', 'T');
			}
		}


//---------- End Sections ------------------------------------------------------------------------------------------------
	}
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}