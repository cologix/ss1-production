nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_General_Message.js
//	Script Name:	CLGX_SL_General_Message
//	Script Id:		customscript_clgx_sl_general_message
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		6/11/2012
//-------------------------------------------------------------------------------------------------

function suitelet_general_message(request, response){
	try {
		
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 6/11/2012
// Details:	A general script to display messages for different situations
//-----------------------------------------------------------------------------------------------------------------

		var stMessage = '';
		stMessage = request.getParameter('custscript_internal_message');
		
		var form = nlapiCreateForm('COLOGIX Internal Message');
		var htmlMessage = form.addField('custpage_clgx_message','inlinehtml', null, null, null);

		htmlMessage.setDefaultValue(stMessage);
		response.writePage( form );


//---------- End Section 1 ------------------------------------------------------------------------------------------------

} 
catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
	if (error.getDetails != undefined){
	    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	    throw error;
	}
	else{
	    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	    throw nlapiCreateError('99999', error.toString());
	}
} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
