//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Invoices_PRTG.js
//	Script Name:	CLGX_SS_Invoices_PRTG
//	Script Id:	    customscript_clgx_ss_invoices_prtg
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	12/12/2017
//-------------------------------------------------------------------------------------------------
function scheduled_create_invoices(){
    try{
        var initialTime = moment();
        var arrFil=new Array();
        var arrCol=new Array();
        var context = nlapiGetContext();
        var mthStart= moment().add(-1, 'M').format('MM');
        var yearStart= moment().add(-1, 'M').format('YYYY');
        var mthEnd= moment().format('MM');
        var yearEnd= moment().format('YYYY');
        var search = nlapiSearchRecord('customrecord_cologix_crossconnect','customsearch5396', arrFil, arrCol);
        if(search!=null) {
            for (var i = 0; search != null && i < search.length; i++) {
                var startExecTime = moment();
                var totalMinutes = (startExecTime.diff(initialTime) / 60000).toFixed(1);
                if ((context.getRemainingUsage() <= 1000 || totalMinutes > 50) && (i + 1) < search.length) {
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    if (status == 'QUEUED') {
                        nlapiLogExecution('DEBUG', 'End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                        break;
                    }
                }
                var billingQTY = 0;
                var totalPRIN = 0;
                var totalVolIN = 0;
                var totalVolOUT = 0;
                var totalPROUT = 0;
                var arrPRTG=new Array();
                var searchR = search[i];
                var columns = searchR.getAllColumns();
                var serviceID = searchR.getValue(columns[0]);
                var arrFilP = new Array();
                var arrColP = new Array();
                arrFilP.push(new nlobjSearchFilter('internalid', 'custrecord_cologix_xc_service', 'anyof', serviceID));
                var searchP = nlapiSearchRecord('customrecord_cologix_crossconnect', 'customsearch5395', arrFilP, arrColP);
                if (searchP != null) {

                    for (var j = 0; searchP != null && j < searchP.length; j++) {
                        var searchPr = searchP[j];
                        var columnsr = searchPr.getAllColumns();
                        var ipBurstRate = searchPr.getValue(columnsr[3]);
                        var addGBIncrement = searchPr.getValue(columnsr[4]);
                        var gbRate = searchPr.getValue(columnsr[5]);
                        var sumPerIN = searchPr.getValue(columnsr[6]);
                        var sumPerOUT = searchPr.getValue(columnsr[7]);
                        var sumVolIN = searchPr.getValue(columnsr[8]);
                        var sumVolOUT = searchPr.getValue(columnsr[9]);
                        var soID = searchPr.getValue(columnsr[1]);
                        totalPRIN = parseFloat(totalPRIN) + parseFloat(sumPerIN);
                        totalPROUT = parseFloat(totalPROUT) + parseFloat(sumPerOUT);
                        totalVolIN = parseFloat(totalVolIN) + parseFloat(sumVolIN);
                        totalVolOUT = parseFloat(totalVolOUT) + parseFloat(sumVolOUT);
                        var location = searchPr.getValue(columnsr[10]);
                        location = getLocation(location);
                        var prtgID = searchPr.getValue(columnsr[11]);
                        arrPRTG.push(prtgID);


                    }
                    var qty = 0;
                    var servOrder = nlapiLoadRecord('salesorder', soID);
                    var customer = servOrder.getFieldValue('name');
                    var NRItems = servOrder.getLineItemCount('item');
                    var customer = servOrder.getFieldValue('entity');
                    var custRecord = nlapiLoadRecord('customer', customer);
                    var subsidiary = custRecord.getFieldValue('subsidiary');
                    var billing = custRecord.getFieldValue('billaddress');
                    for (var j = 1; j <= NRItems; j++) {
                        var item = servOrder.getLineItemValue('item', 'item', j);
                        var billingSchedule = servOrder.getLineItemValue('item', 'billingschedule_display', j);
                        if ((item == 554) && ((billingSchedule != null))) {

                            var qty = parseFloat(servOrder.getLineItemValue('item', 'custcol_clgx_qty2print', j));
                            var itemDescription = servOrder.getLineItemValue('item', 'description', j)||'';
                        }
                    }
                    if (qty > 0) {
                        if (parseFloat(ipBurstRate) > 0) {
                            if (totalPRIN >= totalPROUT) {
                                var percentage = totalPRIN;
                            } else {
                                var percentage = totalPROUT;
                            }

                            if (percentage > qty) {
                                var billingQTY = (parseFloat(percentage) - parseFloat(qty)).toFixed(2);
                                var rate = parseFloat(ipBurstRate);
                                billingQTY = Math.ceil(billingQTY);
                                var description = '(Overage) ' + (percentage).toFixed(2) + ' mb/s; ' + mthStart + '/21/' + yearStart + ' to ' + mthEnd + '/20/' + yearEnd + ': ' + itemDescription;

                            }
                        } else if (parseFloat(gbRate) > 0) {
                            if (totalVolIN >= totalVolOUT) {
                                var volume = totalVolIN;
                            } else {
                                var volume = totalVolOUT;
                            }

                            if (volume > qty) {
                                var billingQTY = (parseFloat(volume) - parseFloat(qty)).toFixed(2);
                                billingQTY = Math.ceil(billingQTY / parseFloat(addGBIncrement)) * parseFloat(addGBIncrement);
                                var rate = parseFloat(gbRate);
                                billingQTY = Math.ceil(billingQTY);
                                var description = '(Overage) ' + (volume).toFixed(2) + ' Gigabytes; ' + mthStart + '/21/' + yearStart + ' to ' + mthEnd + '/20/' + yearEnd + ': ' + itemDescription;
                            }
                        }
                    }
                    if (billingQTY > 0) {
                        //create the invoice

                        var newInvoice = nlapiCreateRecord('invoice');
                        newInvoice.setFieldValue('location', location);
                        newInvoice.setFieldValue('custbody_clgx_consolidate_locations', clgx_return_consolidate_location(location));
                        newInvoice.setFieldValue('subsidiary', subsidiary);
                        newInvoice.setFieldValue('custbody_clgx_so_link_pur', soID);
                        newInvoice.setLineItemValue('item', 'item', 1, 574);
                        newInvoice.setLineItemValue('item', 'quantity', 1, billingQTY);
                        newInvoice.setLineItemValue('item', 'rate', 1, rate);
                        newInvoice.setLineItemValue('item', 'location', 1, location);
                        newInvoice.setLineItemValue('item', 'amount', 1, parseFloat(billingQTY) * parseFloat(rate));
if(location==33 || location==34 ) {
    var returntax = clgx_return_tax(location, customer);
    if (returntax != '') {
        newInvoice.setLineItemValue('item', 'taxcode', 1, returntax[0]);
        newInvoice.setLineItemValue('item', 'taxcode_display', 1, returntax[1]);

    }
}
                        newInvoice.setLineItemValue('item', 'description', 1, description);


                        newInvoice.setFieldValue('entity', customer); //set customer ID
                        var billingid = '';
                        if ((billing == '') || (billing == null)) {
                            var billingid = clgx_return_billing(location, customer)

                        }
                        if (billingid != '') {
                            var NRABs = custRecord.getLineItemCount('addressbook');
                            for (var j = 1; j <= NRABs; j++) {
                                var internalid = custRecord.getLineItemValue('addressbook', 'internalid', j);
                                if (internalid == billingid) {
                                    var billaddress = custRecord.getLineItemValue('addressbook', 'addressbookaddress_text', j);
                                    var billaddresslist = custRecord.getLineItemValue('addressbook', 'addressid', j);
                                    var billingaddress_key = custRecord.getLineItemValue('addressbook', 'addressbookaddress_key', j);
                                    var billingaddress_text = custRecord.getLineItemValue('addressbook', 'addressbookaddress_text', j);
                                    var billingaddress_type = custRecord.getLineItemValue('addressbook', 'addressbookaddress_type', j);
                                    var billisresidential = custRecord.getLineItemValue('addressbook', 'isresidential', j);
                                    var addressee = custRecord.getLineItemValue('addressbook', 'addressee', j);

                                }


                            }
                            newInvoice.setFieldValue('billaddress', billaddress);
                            newInvoice.setFieldValue('billaddressee', addressee);
                            newInvoice.setFieldValue('billaddresslist', billaddresslist);
                            newInvoice.setFieldValue('billingaddress_key', billingaddress_key);
                            newInvoice.setFieldValue('billingaddress_text', billingaddress_text);
                            newInvoice.setFieldValue('billingaddress_type', billingaddress_type);
                            newInvoice.setFieldValue('billisresidential', 'F');
                        } else {
                            newInvoice.setFieldValue('billaddress', billing);
                            //  newInvoice.setFieldValue('billaddressee', addressee);
                        }

                        var arrDates1 = getDateRange(1);
                        var startDate1 = arrDates1[0];
                        //  newInvoice.setFieldValue('name', customer);
                        newInvoice.setFieldValue('trandate', startDate1);
                        newInvoice.setFieldValue('saleseffectivedate', startDate1);

                        var invoiceId = nlapiSubmitRecord(newInvoice, false, true);


                        var emailBody = '';

                        emailBody = emailBody + 'Please check this invoice and make sure everyting is ok!!!!' + invoiceId + '\r\n';

                        var emailSubject = 'Dear Catalina,';

                        clgx_send_employee_emails_from_savedsearch("customsearchclgxe_invoice_errors", emailSubject, emailBody);

                        //nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null,true); // Send email to Catalina
                    }
                }
                for (var k = 0; arrPRTG != null && k < arrPRTG.length; k++) {
                    var prtgRecord = nlapiLoadRecord('customrecord_clgx_prtg_daily_report', arrPRTG[k]);
                    prtgRecord.setFieldValue('custrecord_clgx_prtg_mu_95_billed', 'T');
                    nlapiSubmitRecord(prtgRecord, false, true);
                }
                var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            }
        }
        var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
        nlapiLogExecution('DEBUG','Finished Execution', 'Total Remaining Usage - ' + context.getRemainingUsage() + 'Total Usage-' + usageConsumtion + '-------------------------- Finished Scheduled Script --------------------------|');

    }

    catch (error){
        // var remoteHandsRecord=nlapiLoadRecord('customrecord_clgx_remote_hands',rhID );
        // remoteHandsRecord.setFieldValue('custrecord_clgx_rh_billed','T');
        // nlapiSubmitRecord(remoteHandsRecord, false,true);
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }


}


function getLocation(location)
{
//COL1&2
    if(location==24)
    {
        var loc=34;
    }
    //TOR 156 Front
    if(location== 23)
    {
        var loc=13;
    }
    //Cologix HQ
    if(location== 16)
    {
        var loc=0;
    }
    //DAL1
    if(location==3)
    {
        var loc=2;
    }
    //DAL2
    if(location==18)
    {
        var loc=17;
    }
    //JAX1
    if(location==21)
    {
        var loc=31;
    }
    //JAX2
    if(location==27)
    {
        var loc=40;
    }
    //MIN1&2
    if(location==17)
    {
        var loc=16;
    }
    //MIN3
    if(location==25)
    {
        var loc=35;
    }
    //MTL1
    if(location==2)
    {
        var loc=5;
    }
    //MTL2
    if(location==5)
    {
        var loc=8;
    }
    //MTL3
    if(location==4)
    {
        var loc=9;
    }
    //MTL4
    if(location==9)
    {
        var loc=10;
    }
    //MTL5
    if(location==6)
    {
        var loc=11;
    }
    //MTL6
    if(location==7)
    {
        var loc=12;
    }
    //MTL7
    if(location==19)
    {
        var loc=27;
    }
    //TOR1
    if(location==8)
    {
        var loc=6;
    }
    //TOR2
    if(location==15)
    {
        var loc=15;
    }
    //VAN1
    if(location==14)
    {
        var loc=28;
    }
    //VAN2
    if(location==20)
    {
        var loc=7;
    }
    //NNJ2
    if(location==33)
    {
        var loc=54;
    }
    //NNJ1
    if(location==34)
    {
        var loc=53;
    }
    //NNJ3
    if(location==35)
    {
        var loc=55;
    }
    //NNJ4
    if(location==36)
    {
        var loc=56;
    }
    return loc;
}

function getDateRange(monthsago){

    var today = new Date();
    var date = nlapiAddMonths(today, monthsago);

    var month = parseInt(date.getMonth());
    var year = date.getFullYear();

    var stDays  = daysInMonth(parseInt(month),parseInt(year));
    var stYear  = year;

    var stMonth = parseInt(month) + 1;

    var stStartDate = stMonth + '/1/' + stYear;
    var stEndDate   = stMonth + '/' + stDays + '/' + stYear;

    var arrDateRange = new Array();
    arrDateRange[0] = stStartDate;
    arrDateRange[1] =stEndDate;

    return arrDateRange;
}

function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}

//check if a value is in the array
function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}

function roundMinBusiness(totbusinessH){
    totbusinessH=totbusinessH.toFixed(2);
    totbusinessH=totbusinessH.toString();
    var hb=totbusinessH.split(".");
    if((hb[1]>0)&&(hb[1]<=15))
    {
        hb[1]=15;
    }
    if((hb[1]>15)&&(hb[1]<=30))
    {
        hb[1]=30;
    }
    if((hb[1]>30)&&(hb[1]<=45))
    {
        hb[1]=45;
    }
    if((hb[1]>45)&&(hb[1]<=59))
    {
        hb[1]=0;
        hb[0]=parseInt(hb[0])+parseInt(1);
    }
    var totalTimeB=hb[0]+'.'+hb[1];
    if(parseInt(hb[0])*parseInt(60)>0)
    {
        totalTimeB=parseInt(hb[0])*parseInt(60)+parseInt(hb[1]);
    }
    else
    {
        totalTimeB=parseInt(hb[1]);
    }

    return totalTimeB;

}

function roundMinAfter(totafterH){
    totafterH=totafterH.toFixed(2);
    totafterH=totafterH.toString();
    var ha=totafterH.split(".");
    if((ha[0]==0)&&(ha[1]==0))
    {
        var totalTimeA=0;
    }
//after hours
    if((ha[0]<1)&&(ha[1]>0))
    {
        var totalTimeA=1;
    }
    else
    {
        if((ha[1]>0)&&(ha[1]<=15))
        {
            ha[1]=15;
        }
        if((ha[1]>15)&&(ha[1]<=30))
        {
            ha[1]=30;
        }
        if((ha[1]>30)&&(ha[1]<=45))
        {
            ha[1]=45;
        }
        if((ha[1]>45)&&(ha[1]<=59))
        {
            ha[1]=0;
            ha[0]=parseInt(ha[0])+parseInt(1);
        }
        var totalTimeA=ha[0]+'.'+ha[1];
    }
    if(parseInt(ha[0])*parseInt(60)>0)
    {
        totalTimeA=parseInt(ha[0])*parseInt(60)+parseInt(ha[1]);
    }
    else
    {
        totalTimeA=parseInt(ha[1]);
    }
    return totalTimeA;
}

//return tax
function clgx_return_tax (locationid, customerid){

    var returntax = '';
    var returntaxText = '';
    var customertax=nlapiLoadRecord('customer', customerid);

    switch(parseInt(locationid)) {

        case 33: // Columbus
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_col'); // COL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_col'); // COL
            break;
        case 34: // COL1&2
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_col'); // COL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_col'); // COL
            break;
        case 2: // Dallas Infomart
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_dal'); // DAL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_dal'); // DAL
            break;
        case 17: // Dallas Infomart - Ste 2010
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_dal'); // DAL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_dal'); // DAL
            break;
        case 31: // JAX1
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_jax'); // JAX1
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_jax'); // JAX1
            break;
        case 40: // JAX2
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_jax'); // JAX2
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_jax'); // JAX2
            break;
        case 42: // LAK1
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_lak'); // LAK1
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_lak'); // LAK1
            break;
        case 16: // MIN1&2
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_min'); // MIN
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_min'); // MIN
            break;
        case 35: // MIN3
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_min'); // MIN
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_min'); // MIN
            break;
        case 5: // MTL1
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 8: // MTL2
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 9: // MTL3
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 10: // MTL4
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 11: // MTL5
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 12: // MTL6
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 27: // MTL7
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        ///NNJ
        case 53: // NNJ1
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_nnj'); // NNJ1
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_nnj'); // NNJ1
            break;
        case 54: // NNJ2
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_nnj'); // NNJ2
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_nnj'); // NNJ2
            break;
        case 55: // NNJ3
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_nnj'); // NNJ3
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_nnj'); // NNJ3
            break;
        case 56: // NNJ4
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_nnj'); // NNJ4
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_nnj'); // NNJ4
            break;
        case 13: // 156 Front Street West
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_tor');// TOR
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_tor');// TOR
            break;
        case 14: // Barrie Office
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_tor'); // TOR
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_tor'); // TOR
            break;
        case 6: // 151 Front Street West
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_tor'); // TOR
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_tor'); // TOR
            break;
        case 15: // 905 King Street West
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_tor'); // TOR
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_tor'); // TOR
            break;
        case 18: // 151 Front Street West - Ste 822
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_tor'); // 151 Front Street West - Ste 822
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_tor'); // 151 Front Street West - Ste 822
            break;
        case 28: // 1050 Pender
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_van'); // VAN
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_van'); // VAN
            break;
        case 7: // Harbour Centre
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_van'); // VAN
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_van'); // VAN
            break;

        default:
            returntax = '';

    }
    var returntaxArray=[returntax,returntaxText]
    return returntaxArray;
}
//return billing address
//return tax
function clgx_return_billing (locationid, customerid){

    var returnbilling = '';
//Bell Canada        -          MTL1              -               1114
//                   -          MTL2              -               24133
//                   -          MTL3              -               24159
//                   -          MTL7              -               24653

    if(customerid==2340)
    {
        if(locationid==5)
        {
            returnbilling=1114;
        }
        if(locationid==8)
        {
            returnbilling=24133;
        }
        if(locationid==9)
        {
            returnbilling=24159;
        }
        if(locationid==27)
        {
            returnbilling=24653;
        }
    }

//    Fibrenoire           -           MTL3              -              24130
//                         -           MTL4              -              11411
//                         -           MTL5              -              15044
    //   -                                MTL1              -              24128

    if(customerid==1720)
    {
        if(locationid==5)
        {
            returnbilling=24128;
        }
        if(locationid==9)
        {
            returnbilling=24130;
        }
        if(locationid==10)
        {
            returnbilling=11411;
        }
        if(locationid==11)
        {
            returnbilling=15044;
        }
    }
//    Hurricane Electric CA -     TOR1 or TOR2    -           6232
//                       -          VAN1 OR VAN2  -           15313
//            -   MTL1, MTL2, MTL3, MTL4, MTL5, MTL6, MTL7  - 15314
    if(customerid==6501)
    {
        if((locationid==14)||(locationid==15)||(locationid==6))
        {
            returnbilling=6232;
        }
        if((locationid==7)||(locationid==28))
        {
            returnbilling=15313;
        }
        if((locationid==5)||(locationid==8)||(locationid==9)||(locationid==10)||(locationid==11)||(locationid==12)||(locationid==27))
        {
            returnbilling=15314;
        }
    }
//    Hurricane Electric US -   MIN1&2, MIN3      -           6084
//        -    DAL1 & DAL2       -          15315
//        -    COL1&2, COL3    -           23627

    if(customerid==9519)
    {
        if((locationid==16)||(locationid==35))
        {
            returnbilling=6084;
        }
        if((locationid==2)||(locationid==17))
        {
            returnbilling=15315;
        }
        if((locationid==34)||(locationid==39))
        {
            returnbilling=23627;
        }
    }

//    Hydro One Telecom Inc - TOR1 or TOR2    -           1032
//        -   MTL1, MTL2, MTL3, MTL4, MTL5, MTL6, MTL7  - 12949
    if(customerid==1764)
    {
        if((locationid==14)||(locationid==15))
        {
            returnbilling=1032;
        }
        if((locationid==5)||(locationid==8)||(locationid==9)||(locationid==10)||(locationid==11)||(locationid==12)||(locationid==27))
        {
            returnbilling=12949;
        }
    }
//    Netelligent Hosting      -          MTL1               -         958
//        -          MTL2               -         11243
//        -          MTL3               -         11244
//        -          MTL4               -         11245
//        -          MTL5               -         11246
//        -      TOR1 or TOR2          -           11372
    if(customerid==2191)
    {
        if((locationid==14)||(locationid==15))
        {
            returnbilling=11372;
        }
        if(locationid==5)
        {
            returnbilling=958;
        }
        if(locationid==8)
        {
            returnbilling=11243;
        }
        if(locationid==9)
        {
            returnbilling=11244;
        }
        if(locationid==10)
        {
            returnbilling=11245;
        }
        if(locationid==11)
        {
            returnbilling=11246;
        }

    }
    if(customerid == 906673) {
        returnbilling = 60117;
    }
    if(customerid == 395249) {
        returnbilling = 60118;
    }
    if(customerid == 395254) {
        returnbilling = 60119;
    }
    if(customerid == 1006770) {
        returnbilling = 60120;
    }
    if(customerid==791328)
    {
        returnbilling=52716;
    }
    if(customerid==137226)
    {
        returnbilling=52717;
    }
    if(customerid==473887)
    {
        returnbilling=54877;
    }
    if(customerid==76059)
    {
        returnbilling=25096;
    }
    if(customerid==76059)
    {
        returnbilling=25096;
    }
    if(customerid==1657)
    {
        returnbilling=1106;
    }
    if(customerid==3847)
    {
        returnbilling=1893;
    }
    if(customerid==10859)
    {
        returnbilling=6878;
    }
    if(customerid==401284)
    {
        returnbilling=24771;
    }
    if(customerid==89)
    {
        returnbilling=36;
    }
    if(customerid==8937)
    {
        returnbilling=5733;
    }
    if(customerid==137232)
    {
        returnbilling=15487;
    }
    if(customerid==922783)
    {
        returnbilling=59322;
    }
    if(customerid==922786)
    {
        returnbilling=59328;
    }
    if(customerid==238400)
    {
        returnbilling=59340;
    }
    if(customerid==789277)
    {
        returnbilling=59336;
    }
    return returnbilling;
}


function eliminateDuplicates(arr) {
    var i,
        len=arr.length,
        out=[],
        obj={};

    for (i=0;i<len;i++) {
        obj[arr[i]]=0;
    }
    for (i in obj) {
        out.push(i);
    }
    return out;
}