//------------------------------------------------------
//	Script:		CLGX_SS_PRTG_Percentile95.js
//	ScriptID:	customscript_CLGX_SS_PRTG_Percentile95
//	ScriptType:	SS
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//------------------------------------------------------

function processData(){
    try {
///
        var initialTime = moment();
        var arrFil=new Array();
        var arrCol=new Array();
        var context = nlapiGetContext();
        var search = nlapiSearchRecord('customrecord_clgx_prtg_daily_report','customsearch5384', arrFil, arrCol);
        // var search = nlapiSearchRecord('customrecord_clgx_active_port','customsearch5358', arrFil, arrCol);
        if(search!=null) {

            for (var k = 0; search != null && k < search.length; k++) {
                var startExecTime = moment();
                var totalMinutes = (startExecTime.diff(initialTime) / 60000).toFixed(1);

                if ((context.getRemainingUsage() <= 1000 || totalMinutes > 50) && (i + 1) < search.length) {
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    if (status == 'QUEUED') {
                        nlapiLogExecution('DEBUG', 'End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                        break;
                    }
                }
                var searchR= search[k];
                var columns = searchR.getAllColumns();
                var ID=searchR.getValue(columns[0]);
                var fileID=parseInt(searchR.getValue(columns[1]));
                var record = nlapiLoadRecord('customrecord_clgx_prtg_daily_report', ID);
                var objFile = nlapiLoadFile(fileID);
                var bodyFile = objFile.getValue();

                var arr = JSON.parse(bodyFile);
                var arrSpeedIn_mbps = new Array();
                var arrSpeedOut_mbps = new Array();
                var totalVolumeIn = 0;
                var totalVolumeOut = 0;
                //  var n=arr.length;
                for (var i = 0; arr != null && i < arr.length; i++) {
                    if(parseFloat(arr[i]["Traffic In (speed)"])>=0){
                        arrSpeedIn_mbps.push(parseFloat(parseFloat(arr[i]["Traffic In (speed)"])*parseFloat(8)) / parseFloat(1000000));
                    }
                    if(parseFloat(arr[i]["Traffic Out (speed)"])>=0) {
                        arrSpeedOut_mbps.push(parseFloat(parseFloat(arr[i]["Traffic Out (speed)"])*parseFloat(8)) / parseFloat(1000000));
                    }
                    if(parseFloat(arr[i]["Traffic In (volume)"])>=0) {
                        totalVolumeIn += parseFloat(arr[i]["Traffic In (volume)"]);
                    }
                    if(parseFloat(arr[i]["Traffic Out (volume)"])>=0) {
                        totalVolumeOut += parseFloat(arr[i]["Traffic Out (volume)"]);
                    }

                }
                var arrSpeedInS_mbps = _.sortBy(arrSpeedIn_mbps, function (num) {
                    return num;
                });
                var arrSpeedOutS_mbps = _.sortBy(arrSpeedOut_mbps, function (num) {
                    return num;
                });
                var nIn = arrSpeedInS_mbps.length;
                var nOut = arrSpeedOutS_mbps.length;
                var rn_In = 1 + ((nIn - 1) * 0.95);
                var rn_Out = 1 + ((nOut - 1) * 0.95);
                var frn_In = Math.floor(rn_In);
                var frn_Out = Math.floor(rn_Out);
                var crn_In = Math.ceil(rn_In);
                var crn_Out = Math.ceil(rn_Out);
                var tst1=arrSpeedInS_mbps[frn_In - 1];
                var tst2=arrSpeedInS_mbps[crn_In - 1];
                if(parseInt(nIn)>0) {
                    if (rn_In == frn_In && rn_In == crn_In) {
                        var prct_In = arrSpeedInS_mbps[rn_In - 1];
                    }
                    else {
                        var prct_In = parseFloat(arrSpeedInS_mbps[frn_In - 1]) + parseFloat((parseFloat(rn_In) - parseFloat(frn_In)) * (parseFloat(arrSpeedInS_mbps[crn_In - 1]) - parseFloat(arrSpeedInS_mbps[frn_In - 1])));

                    }
                }else{
                    var prct_In=parseFloat(0);
                }
                if(parseInt(nOut)>0) {
                    if (rn_Out == frn_Out && rn_Out == crn_Out) {
                        var prct_Out = arrSpeedOutS_mbps[rn_Out - 1];
                    }
                    else {
                        var prct_Out = parseFloat(arrSpeedOutS_mbps[frn_Out - 1]) + parseFloat((parseFloat(rn_Out) - parseFloat(frn_Out)) * (parseFloat(arrSpeedOutS_mbps[crn_Out - 1]) -parseFloat(arrSpeedOutS_mbps[frn_Out - 1])));

                    }
                }else{
                    var prct_Out=parseFloat(0);
                }

                var volumeIn_gb = (parseFloat(totalVolumeIn) / parseFloat(1000000000)).toFixed(2);
                var volumeOut_gb = (parseFloat(totalVolumeOut) / parseFloat(1000000000)).toFixed(2);


                record.setFieldValue('custrecord_clgx_prtg_mu_95_ti_speed', parseFloat(prct_In).toFixed(2));
                record.setFieldValue('custrecord_clgx_prtg_mu_95_to_speed', parseFloat(prct_Out).toFixed(2));
                record.setFieldValue('custrecord_clgx_prtg_mu_95_ti_volume', volumeIn_gb);
                record.setFieldValue('custrecord_clgx_prtg_mu_95_to_volume', volumeOut_gb);
                nlapiSubmitRecord(record, false, true);
            }
        }

    }
    catch (error){
           var context = nlapiGetContext();
       /* var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
        if (status == 'QUEUED') {
            nlapiLogExecution('DEBUG', 'End', status + ' / Finished scheduled script due to an error and re-schedule it.');
            return false;
        }*/
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}