function update(){
    try {
        var initialTime = moment();
        var context = nlapiGetContext();

        var arrFil=new Array();
        var arrCol=new Array();
        var arrXCs=new Array();
        var search = nlapiLoadSearch('customrecord_clgx_prtg_daily_report','customsearch5552');

        var resultSet = search.runSearch();
        resultSet.forEachResult(function(searchResult) {


            var ID=searchResult.getValue('custrecord_clgx_prtg_mu_xc',null,'GROUP');
            arrXCs.push(ID);

            return true;
        });

        var arrFil=new Array();
        var arrCol=new Array();
        if(arrXCs.length>0) {
            arrFil.push(new nlobjSearchFilter('internalid', null, 'noneof', arrXCs));
        }
        var search = nlapiLoadSearch('customrecord_cologix_crossconnect','customsearch5382');
        search.addFilters(arrFil);
        var resultSet = search.runSearch();
        resultSet.forEachResult(function(searchResult) {
            var startExecTime = moment();
            var totalMinutes = (startExecTime.diff(initialTime) / 60000).toFixed(1);

            if ((context.getRemainingUsage() <= 1000|| totalMinutes > 50)) {
                var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                if (status == 'QUEUED') {
                    nlapiLogExecution('DEBUG', 'End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                    return false;
                }
            }
            var portID=searchResult.getValue('internalid','CUSTRECORD_CLGX_ACTIVE_PORT_XC','GROUP');
            var ID=searchResult.getValue('internalid',null,'GROUP');
            var sensorID=searchResult.getValue('custrecord_clgx_prtg_id',null,'GROUP');
            //ignore the duplicates
            //  var countIDs=searchResult.getValue('internalid','CUSTRECORD_CLGX_PRTG_MU_XC','COUNT');

            var today = moment().format('YYYY-MM-DD');
            //  if(countIDs<1){
            var req = nlapiRequestURL('https://corp-nnj-prtglab.corp.cologix.net/api/historicdata.json?id=' + sensorID + '&avg=0&sdate=2020-04-21-00-00-00&edate='+today+'-00-00-00&usecaption=1&version=17.3.33.2830&username=catalina.taran&passhash=1102549655', 'GET');
            if(req.code==200){
                var str = req.body;
                var resp = JSON.parse(str);
                var jsonfinal = JSON.stringify(resp.histdata);
                var fileName = nlapiCreateFile(sensorID+'_04-2020.json', 'PLAINTEXT', jsonfinal);
                fileName.setFolder(9114791);
                var fileid=nlapiSubmitFile(fileName);
                var newRecord = nlapiCreateRecord('customrecord_clgx_prtg_daily_report');
                newRecord.setFieldValue('custrecord_clgx_prtg_daily_report_ap', portID);
                newRecord.setFieldValue('custrecord_clgx_prtg_daily_report_fid', fileid.toString());
                newRecord.setFieldValue('custrecord_clgx_prtg_daily_report_pid', sensorID);
                newRecord.setFieldValue('custrecord_clgx_prtg_daily_report_pid', sensorID);
                newRecord.setFieldValue('custrecord_clgx_prtg_mu_xc', ID);
                newRecord.setFieldValue('name', '21/04-20/05, 2020');
                newRecord.setFieldValue('custrecord_clgx_prtg_daily_report_lu', moment().format("MM/D/YYYY"));
                nlapiSubmitRecord(newRecord, false,true);
            }
            // }
            return true;
        });



    }
    catch (error){
        var context = nlapiGetContext();
        var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
        if (status == 'QUEUED') {
            nlapiLogExecution('DEBUG', 'End', status + ' / Finished scheduled script due to an error and re-schedule it.');
            return false;
        }
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}