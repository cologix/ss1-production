function update(){
    try {
        var initialTime = moment();
        var arrFil=new Array();
        var arrCol=new Array();
        var context = nlapiGetContext();
        var environment = context.getEnvironment();
        //if (environment == 'PRODUCTION'
        var search = nlapiSearchRecord('customrecord_clgx_prtg_daily_report','customsearch_clgx_ss_prtg_update_json', arrFil, arrCol);
        // var search = nlapiSearchRecord('customrecord_clgx_active_port','customsearch5358', arrFil, arrCol);
        if(search!=null) {

            for (var i = 0; search != null && i < search.length; i++) {
                var startExecTime = moment();
                var totalMinutes = (startExecTime.diff(initialTime) / 60000).toFixed(1);

                if ((context.getRemainingUsage() <= 1000 || totalMinutes > 50) && (i + 1) < search.length) {
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    if (status == 'QUEUED') {
                        nlapiLogExecution('DEBUG', 'End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                        break;
                    }
                }
                var searchR = search[i];
                var columns = searchR.getAllColumns();
                // var sensorID=searchR.getValue(columns[0]);
                // var ID=searchR.getValue(columns[1]);
                var prtgID = searchR.getValue(columns[0]);
                var fileID = searchR.getValue(columns[1]);
                var sensorID = searchR.getValue(columns[2]);
                //ignore the duplicates
                var arrFilF = new Array();
                var arrColF = new Array();
                arrFilF.push(new nlobjSearchFilter("internalid", null, "is", parseInt(fileID)));
                arrFilF.push(new nlobjSearchFilter("modified", null, "on", moment().format("MM/D/YYYY")));
                arrColF.push(new nlobjSearchColumn('modified', null, null));
                var searchF = nlapiSearchRecord('file', null, arrFilF, arrColF);
                if (searchF == null) {
                    var objFile = nlapiLoadFile(parseInt(fileID));
                    var bodyFile = objFile.getValue();
                     bodyFile = bodyFile.slice(0, -1);
                    var yesterday = moment().add(-1, 'days').format('YYYY-MM-DD');
//var yesterday1 = moment().add(-1, 'days').format('YYYY-MM-DD');
                    var today = moment().format('YYYY-MM-DD');
                   var req = nlapiRequestURL('https://corp-nnj-prtglab.corp.cologix.net/api/historicdata.json?id=' + sensorID + '&avg=0&sdate='+yesterday+'-00-00-00&edate='+yesterday+'-00-00-00&usecaption=1&version=17.3.33.2830&username=catalina.taran&passhash=1102549655', 'GET');
                  //  var req = nlapiRequestURL('https://corp-nnj-prtglab.corp.cologix.net/api/historicdata.json?id=' + sensorID + '&avg=0&sdate=2019-11-21-00-00-00&edate=2019-12-21-00-00-00&usecaption=1&version=17.3.33.2830&username=catalina.taran&passhash=1102549655', 'GET');
                    //  var req = nlapiRequestURL('https://corp-nnj-prtglab.corp.cologix.net/api/historicdata.json?id=' + sensorID + '&avg=0&sdate=2017-12-21-00-00-00&edate='+today+'-00-00-00&usecaption=1&version=17.3.33.2830&username=catalina.taran&passhash=1102549655', 'GET');
                    // nlapiSendEmail(206211, 206211,'Str',req.body,null,null,null,null,true); // Send email to Catalina
                    if (req.code == 200) {
                        var str = req.body;


                        var resp = JSON.parse(str);
                        var jsonfinal = JSON.stringify(resp.histdata);
                        if(jsonfinal!='') {
                               jsonfinal = jsonfinal.substr(1);

                            jsonfinal = bodyFile + ',' + jsonfinal;
                            var name = objFile.getName();
                            var folderId = objFile.getFolder();
                            var fileType = objFile.getType();
                            //  nlapiDeleteFile(fileID);//delete the older file
                            // var jsonfinal = JSON.stringify(resp.histdata);
                            var newFile = nlapiCreateFile(name, fileType, jsonfinal);//create a new file and add the new updates
                            newFile.setFolder(folderId);
                            nlapiSubmitFile(newFile);//submit it
                            // var fileName = nlapiCreateFile(sensorID+'_'+moment().format("MM-YYYY")+'.json', 'PLAINTEXT', jsonfinal);
                            //fileName.setFolder(6113874);
                            // var fileid=nlapiSubmitFile(fileName);
                            /* var newRecord = nlapiCreateRecord('customrecord_clgx_prtg_daily_report');
                             newRecord.setFieldValue('custrecord_clgx_prtg_daily_report_ap', ID);
                             newRecord.setFieldValue('custrecord_clgx_prtg_daily_report_fid', fileid);
                             newRecord.setFieldValue('custrecord_clgx_prtg_daily_report_pid', sensorID);
                             newRecord.setFieldValue('custrecord_clgx_prtg_daily_report_lu', moment().format("MM-DD-YYYY"));
                             */
                        }

                        var Record = nlapiLoadRecord('customrecord_clgx_prtg_daily_report', prtgID);
                        Record.setFieldValue('custrecord_clgx_prtg_daily_report_lu', moment().format("MM/D/YYYY"));
                        nlapiSubmitRecord(Record, false, true);
                    }
                }

                else {
                    var Record = nlapiLoadRecord('customrecord_clgx_prtg_daily_report', prtgID);
                    Record.setFieldValue('custrecord_clgx_prtg_daily_report_lu', moment().format("MM/D/YYYY"));
                    nlapiSubmitRecord(Record, false, true);
                }
            }
        }



    }
    catch (error){
        var context = nlapiGetContext();
       var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
            if (status == 'QUEUED') {
           nlapiLogExecution('DEBUG', 'End', status + ' / Finished scheduled script due to an error and re-schedule it.');
           return false;
         }
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}