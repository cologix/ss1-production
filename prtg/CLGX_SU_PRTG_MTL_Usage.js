
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_PRTG_MTL_Usage.js
//	Script Name:	CLGX_SU_PRTG_MTL_Usage
//	Script ID:		customscript_CLGX_SU_PRTG_MTL_Usage
//	Script Type:	User Event Script
//	@author:        Catalina Taran - catalina.taran@cologix.com
//	Created:		7/17/2017
//-------------------------------------------------------------------------------------------------


function beforeLoad(type, form) {
    try {
        var currentContext = nlapiGetContext();
        var fileID = parseInt(nlapiGetFieldValue('custrecord_clgx_prtg_daily_report_fid'));
        if ((currentContext.getExecutionContext() == 'userinterface')&&(fileID!=null&&fileID!='')&&((type=='edit')||(type=='view'))){
            var arrFil=new Array();
            var arrCol=new Array();
            arrFil.push(new nlobjSearchFilter("internalid",null,"is",fileID));
            arrCol.push(new nlobjSearchColumn('url',null,null));
            arrCol.push(new nlobjSearchColumn('name',null,null));
            var search = nlapiSearchRecord('file',null, arrFil, arrCol);
            if(search!=null) {
                for ( var i = 0; search != null && i < search.length; i++ ) {
                    var name = search[i].getValue('name', null, null);
                    var url = search[i].getValue('url', null, null);
                    var htmlFile = '<br/><span id="custrecord_clgx_prtg_daily_report_ap_fs_lbl_uir_label" class="smallgraytextnolink uir-label ">JSON FILE</span><a href="https://1337135.app.netsuite.com'+url+'&_xd=T" style="color:blue">'+name+'</a>';
                    nlapiSetFieldValue('custrecord_clgx_prtg_mu_json_file', htmlFile);
                }
            }
                }
    } catch (error) {
        if (error.getDetails != undefined) {
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        } else {
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}