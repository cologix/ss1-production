nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_Space.js
//	Script Name:	CLGX_CR_Space
//	Script Id:		customscript_clgx_cr_space
//	Script Runs:	Client
//	Script Type:	Client Script
//	Deployments:	Expense Report
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		10/25/2013
//-------------------------------------------------------------------------------------------------

function saveRecord() {
	try {
		//------------- Begin Section 1 -------------------------------------------------------------------
		// Details:	Check space name for duplicates
		//-------------------------------------------------------------------------------------------------
				
				var saveOK = 1;
				var response = '';

				var id = nlapiGetRecordId();
				var name = nlapiGetFieldValue('name');

				if((id == null || id == '') && (name != null && name != '')){ // this is a new record

					var arrColumns = new Array();
					arrColumns.push(new nlobjSearchColumn('name',null,null));
					var arrFilters = new Array();
					arrFilters.push(new nlobjSearchFilter("name",null,"is",name));
					arrFilters.push(new nlobjSearchFilter('isinactive',null,'is', 'F'));
					var searchRecords = nlapiSearchRecord('customrecord_cologix_space', null, arrFilters, arrColumns);
					
					if (searchRecords != null){
						saveOK = 0;
						response = 'This space ID (' + name + ') exist already.';
					}
				}
				else{ // existing record
					
					if(name != null && name != ''){

						var arrColumns = new Array();
						arrColumns.push(new nlobjSearchColumn('name',null,null));
						var arrFilters = new Array();
						arrFilters.push(new nlobjSearchFilter("name",null,"is",name));
						arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",id));
						arrFilters.push(new nlobjSearchFilter('isinactive',null,'is', 'F'));
						var searchRecords = nlapiSearchRecord('customrecord_cologix_space', null, arrFilters, arrColumns);
						
						if (searchRecords != null){
							saveOK = 0;
							response = 'This space ID (' + name + ') exist already.';
						}
					}
				}
				
				// display alert and permit save or not ----------------------------------------------------------------
				if(saveOK == 0){
					var alert = confirm(response);
					return false;
				}
				else{
					return true;
				}

			} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
