nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Service_Order_AS.js
//	Script Name:	CLGX_SU_Service_Order_AS
//	Script Id:		customscript_clgx_su_so_as
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Sales Order - Sandbox | Production
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/14/2011
//	Includes:		CLGX_Lib_Global.js, moment.min.js, CLGX_LIB_Transaction_Totals.js, underscore-min.js
//-------------------------------------------------------------------------------------------------

function afterSubmit (type) {
    try {
        var currentContext = nlapiGetContext();
        //------------- Begin Section 2 -------------------------------------------------------------------
// Created:	05/23/2013
// Details:	Manage waiting time for this SO.
//-------------------------------------------------------------------------------------------------

        var start = moment();
        //var body = '00 - ' + moment().format('M/D/YYYY h:mm:ss') + '\n';
        var body = '| ';
        if (type == 'create'){
            var currentWait = nlapiGetFieldValue('custbody_clgx_wtng_cust_response');
            if(currentWait == 'T'){
                var d1 = new moment();
                var d1now=d1.format('MM/DD/YYYY HH:mm:ss');
                var salesorderID = nlapiGetRecordId();
                nlapiSubmitField('salesorder', salesorderID, 'custbody_clgx_wtng_cust_start_hour', d1now);

            }   
        }
        if (type == 'edit' || type == 'xedit') {
            var currentWait = nlapiGetFieldValue('custbody_clgx_wtng_cust_response');


            var oldSO = nlapiGetOldRecord();
            var oldWait = oldSO.getFieldValue('custbody_clgx_wtng_cust_response');
            var oldStart=oldSO.getFieldValue('custbody_clgx_wtng_cust_start_hour');
            var oldTotalH=oldSO.getFieldValue('custbody_clgx_wtng_cust_total_hours');
            var salesorderID = nlapiGetRecordId();
            if((currentWait == 'T' && oldWait == 'F') || (currentWait == 'F' && oldWait == 'T')){ // if wait flag changed when saving in custbody_clgx_wtng_cust_start_hour the start hour
                var d1 = new moment();
                var d1now=d1.format('MM/DD/YYYY HH:mm:ss');
                if(currentWait == 'T'){ // start waiting time - create waiting record
                    nlapiSubmitField('salesorder', salesorderID, 'custbody_clgx_wtng_cust_start_hour', d1now);
                }
                else{ // end waiting time - update waiting record
                    var now =new moment();
                    var nf=now.format('MM/DD/YYYY HH:mm:ss');
                    var date1 = new Date(nf);
                    var date2 = new Date(oldStart);
                    // calculate total waiting time in hours and save it in 'custbody_clgx_wtng_cust_total_hours'
                    var timeDiff = Math.abs(date1.getTime() - date2.getTime()) / 3600000;
                    nlapiSubmitField('salesorder', salesorderID, 'custbody_clgx_wtng_cust_finish_hour', nf);
                    if (oldTotalH!=null) {
                        nlapiSubmitField('salesorder', salesorderID, 'custbody_clgx_wtng_cust_total_hours', Math.round(parseInt(timeDiff) + parseInt(oldTotalH)));
                    }
                    else{
                        nlapiSubmitField('salesorder', salesorderID, 'custbody_clgx_wtng_cust_total_hours', Math.round(timeDiff));
                    }
                    var waitid = nlapiGetFieldValue('custbody_clgx_wtng_cust_response_id');
                }
            }

            body += '01 - ' + moment().diff(start) + ' | ';

        }
        if (currentContext.getExecutionContext() == 'userinterface') {

//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 4/14/2014
// Details:	Calculates NRC and MRC totals from items sublist to custom record 'Transaction Totals'
//-------------------------------------------------------------------------------------------------

            if (type == 'edit'|| type == 'xedit' || type == 'copy' || type == 'create'){
                var soid = nlapiGetRecordId();
                var customerid = nlapiGetFieldValue('entity');
                var totals = clgx_transaction_totals (customerid, 'so', soid);

                body += '02 - ' + moment().diff(start) + ' | ';

            }


//------------- Begin Section 3 -------------------------------------------------------------------
// Created:	2/7/2014
// Details:	When a SO is deleted, update any SO that was used to it's renewal
//-------------------------------------------------------------------------------------------------
            if (type == 'delete'){
                var oldSO = nlapiGetOldRecord();
                var arrSOs = oldSO.getFieldValues('custbody_clgx_renewed_from_sos');
                for ( var i = 0; arrSOs != null && i < arrSOs.length; i++ ) {
                    nlapiSubmitField('salesorder', arrSOs[i], 'custbody_clgx_so_renewed_on_so', '');
                }

                body += '03 - ' + moment().diff(start) + ' | ';

            }

//------------- Begin Section 4 -----------------------------------------------------------------------------------
// Created:	04/23/2015
// Details:	Queue transaction to syncronize with SilverPOP
//-----------------------------------------------------------------------------------------------------------------
            /*
             var currentContext = nlapiGetContext();
             var userid = nlapiGetUser();
             var roleid = currentContext.getRole();
             var transactid = nlapiGetRecordId();
             var inactive = nlapiGetFieldValue('isinactive');
             var json = nlapiGetFieldValue('custbody_clgx_sp_json_sync');

             if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'create' || type == 'xedit' || type == 'edit' || type == 'delete')) {

             var action = 1;
             if(type == 'create'){
             action = 0;
             }
             if(type == 'delete'){
             action = 2;
             }
             if(inactive == 'T'){
             action = 3;
             }

             var arrColumns = new Array();
             arrColumns.push(new nlobjSearchColumn('internalid',null,null));
             var arrFilters = new Array();
             arrFilters.push(new nlobjSearchFilter("custrecord_clgx_transact_to_sp_transact",null,"equalto",transactid));
             arrFilters.push(new nlobjSearchFilter("custrecord_clgx_transact_to_sp_type",null,"equalto",3));
             arrFilters.push(new nlobjSearchFilter('custrecord_clgx_transact_to_sp_done',null,'is','F'));
             var searchQueue = nlapiSearchRecord('customrecord_clgx_queue_transact_to_sp', null, arrFilters, arrColumns);

             if(searchQueue == null){ // add contact to queue if not in it
             var record = nlapiCreateRecord('customrecord_clgx_queue_transact_to_sp');
             record.setFieldValue('custrecord_clgx_transact_to_sp_transact', transactid);
             record.setFieldValue('custrecord_clgx_transact_to_sp_type', 3);
             record.setFieldValue('custrecord_clgx_transact_to_sp_action', action);
             record.setFieldValue('custrecord_clgx_transact_to_sp_done', 'F');
             record.setFieldValue('custrecord_clgx_transact_to_sp_json', json);
             var idRec = nlapiSubmitRecord(record, false,true);
             }
             else{ // if in queue change status in case it was different
             nlapiSubmitField('customrecord_clgx_queue_transact_to_sp', searchQueue[0].getValue('internalid',null,null), 'custrecord_clgx_transact_to_sp_action', action);
             }
             }
             */
//------------- Begin Section 5 -------------------------------------------------------------------
// Created:	4/22/2014
// Details:	If copy line items flag is set to true run copy line items function
            // Fetch all the line items in the current record
            // For each line item:
            // if it has a billing schedule and the status is not NRC
            // create a new line item that uses all the same information, but update the <>
            // reset the old line item rate to 0, the billing schedule to null, the system quantity to the invoice quantity, and the date data to <>
//-------------------------------------------------------------------------------------------------
            if (type == 'create' || type == 'xedit' || type == 'edit') {
                var currentRecord = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
                var copyState = currentRecord.getFieldValue('custbody_clgx_so_iscopy_lines');
                if (copyState == 'T') {


                    nlapiSetRedirectURL('RECORD', 'salesorder', nlapiGetRecordId(), true);


                    // currentRecord.setFieldValue('custbody_clgx_so_iscopy_lines', 'F');
                    // nlapiSubmitRecord(currentRecord, false, true);
                    nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'custbody_clgx_so_iscopy_lines', 'F');


                    var currentRecord = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());

                    var originalItemCount = currentRecord.getLineItemCount('item');
                    var itemsSCArr= new Array();

                    for (var i = 0; i < originalItemCount; i++) {
                        //note currentItemCount vs originalItemCount. We are going to insert some new lines, which will increment the currentItemCount
                        var currentItemCount = currentRecord.getLineItemCount('item');
                        var bs = currentRecord.getLineItemText('item', 'billingschedule', i + 1);
                        var recurring = currentRecord.getLineItemText('item', 'class', i + 1);

                        if (recurring.indexOf("NRC") > -1) {
                            currentRecord.setLineItemValue('item', 'billingschedule', i + 1, null);
                            currentRecord.setLineItemValue('item', 'rate', i + 1, 0);
                            currentRecord.setLineItemValue('item', 'amount', i + 1, 0);


                        }
                        else { // if this item is recurring
                            // if billing schedule is not null copy item to new line and make billing schedule 'Month to Month', then set rate to 0 and remove Bill Schedule on old line
                            if (bs != null && bs != '') {

                                var quantitybilled = currentRecord.getLineItemValue('item', 'quantitybilled', i + 1);
                                var quantity = currentRecord.getLineItemValue('item', 'quantity', i + 1);
                                var rate = currentRecord.getLineItemValue('item', 'rate', i + 1);
                                var amount = currentRecord.getLineItemValue('item', 'amount', i + 1);
                                var billingschedule = currentRecord.getLineItemValue('item', 'billingschedule', i + 1);
                                var customSchedule = currentRecord.getLineItemValue('item', 'custcol_custom_schedule', i + 1);	//LS 4/24/2020
                                var itemSchedule = currentRecord.getLineItemValue('item', 'custcol_item_schedule', i + 1);	    //LS 4/24/2020

                                if (quantitybilled == null) {
                                    quantitybilled = '0';
                                }
                                if (quantity == null) {
                                    quantity = '0';
                                }
                                if (rate == null) {
                                    rate = '0';
                                }
                                if (amount == null) {
                                    amount = '0';
                                }

                                var intCount = currentRecord.getLineItemCount('item');
                                currentRecord.insertLineItem('item', intCount + 1);
                                currentRecord.setLineItemValue('item', 'custcol_clgx_so_col_service_id', intCount + 1, currentRecord.getLineItemValue('item', 'custcol_clgx_so_col_service_id', i + 1));
                                currentRecord.setLineItemValue('item', 'item', intCount + 1, currentRecord.getLineItemValue('item', 'item', i + 1));
                                currentRecord.setLineItemValue('item', 'custcol_clgx_qty2print', intCount + 1, currentRecord.getLineItemValue('item', 'custcol_clgx_qty2print', i + 1));
                                currentRecord.setLineItemValue('item', 'description', intCount + 1, currentRecord.getLineItemValue('item', 'description', i + 1));
                                currentRecord.setLineItemValue('item', 'pricelevels', intCount + 1, currentRecord.getLineItemValue('item', 'pricelevels', i + 1));
                                currentRecord.setLineItemValue('item', 'taxcode', intCount + 1, currentRecord.getLineItemValue('item', 'taxcode', i + 1));
                                currentRecord.setLineItemValue('item', 'taxrate1', intCount + 1, currentRecord.getLineItemValue('item', 'taxrate1', i + 1));
                                currentRecord.setLineItemValue('item', 'taxrate2', intCount + 1, currentRecord.getLineItemValue('item', 'taxrate2', i + 1));
                                currentRecord.setLineItemValue('item', 'class', intCount + 1, currentRecord.getLineItemValue('item', 'class', i + 1));
                                currentRecord.setLineItemValue('item', 'billingschedule', intCount + 1, billingschedule);
                                currentRecord.setLineItemValue('item', 'location', intCount + 1, currentRecord.getLineItemValue('item', 'location', i + 1));
                                currentRecord.setLineItemValue('item', 'quantity', intCount + 1, quantity);
                                currentRecord.setLineItemValue('item', 'quantitybilled', intCount + 1, 0);
                                currentRecord.setLineItemValue('item', 'price', intCount + 1, currentRecord.getLineItemValue('item', 'price', i + 1));
                                currentRecord.setLineItemValue('item', 'options', intCount + 1, currentRecord.getLineItemValue('item', 'options', i + 1));
                                currentRecord.setLineItemValue('item', 'custcol_cologix_unevendaystobill', intCount + 1, currentRecord.getLineItemValue('item', 'custcol_cologix_unevendaystobill', i + 1));
                                currentRecord.setLineItemValue('item', 'custcol_clgx_rate_per_day_28', intCount + 1, currentRecord.getLineItemValue('item', 'custcol_clgx_rate_per_day_28', i + 1));
                                currentRecord.setLineItemValue('item', 'custcol_cologix_rateperday', intCount + 1, currentRecord.getLineItemValue('item', 'custcol_cologix_rateperday', i + 1));
                                currentRecord.setLineItemValue('item', 'custcol_clgx_rate_per_day_31', intCount + 1, currentRecord.getLineItemValue('item', 'custcol_clgx_rate_per_day_31', i + 1));
                                currentRecord.setLineItemValue('item', 'custcol_cologix_invoice_item_category', intCount + 1, currentRecord.getLineItemValue('item', 'custcol_cologix_invoice_item_category', i + 1));
                                currentRecord.setLineItemValue('item', 'rate', intCount + 1, rate);
                                currentRecord.setLineItemValue('item', 'amount', intCount + 1, amount);
                                currentRecord.setLineItemValue('item','custbody_needs_lease_mod',  intCount + 1, 'T');                     //LS 4/24/2020
								currentRecord.setLineItemValue('item','custcol_custom_schedule', intCount + 1, customSchedule);            //LS 4/24/2020
	    				        currentRecord.setLineItemValue('item','custcol_item_schedule', intCount + 1, itemSchedule);                //LS 4/24/2020

                                if (itemSchedule != null && itemSchedule.trim() != '' && itemSchedule != 0)
                                {
                                  var obj = {'itmsc': itemSchedule,'lineNbr': intCount+1};
                                  itemsSCArr.push(obj);
							    }

                                //set the old line item values

                                currentRecord.setLineItemValue('item', 'billingschedule', i + 1, null);
                                currentRecord.setLineItemValue('item', 'quantity', i + 1, quantitybilled);
                                currentRecord.setLineItemValue('item', 'rate', i + 1, 0);
                                currentRecord.setLineItemValue('item', 'amount', i + 1, 0);
                                currentRecord.setLineItemValue('item', 'custcol_clgx_old_rate', i + 1, rate);
                                currentRecord.setLineItemValue('item', 'custcol_clgx_date_closed', i + 1, getDateToday());


                            }
                        }

                    }

                    currentRecord.setFieldValue('custbody_clgx_so_iscopy_lines', 'F');

	                //LS 4/24/2020 BEGIN
					var idAfter = nlapiSubmitRecord(currentRecord, false, true);
                    var recSONewAF = nlapiLoadRecord('salesorder', idAfter);
                    for (var k = 0; itemsSCArr != null && k < itemsSCArr.length; k++)
                    {
                      var recCustomItemSchedule = nlapiLoadRecord('customrecord_custom_item_schedule', parseInt(itemsSCArr[k].itmsc));
                      recCustomItemSchedule.setFieldValue('custrecord_line_id', parseInt(recSONewAF.getLineItemValue('item', 'line', parseInt(itemsSCArr[k].lineNbr))));
                      nlapiSubmitRecord(recCustomItemSchedule, false, true);
                     }
                    //LS 4/24/2020 END
		       }

                body += '04 - ' + moment().diff(start) + ' | ';

            }

//------------- Begin Section 2 -----------------------------------------------------------------------------------
// Created:	01/27/2016
// Details:	Synchronize with NAC
//-----------------------------------------------------------------------------------------------------------------

            var environment = currentContext.getEnvironment();

            if (environment == 'PRODUCTION' && (type == 'xedit'  || type == 'edit' || currentContext.getExecutionContext() == 'userinterface')) {

                var soid = nlapiGetRecordId();
                var clocation = nlapiGetFieldValue('custbody_clgx_consolidate_locations');

                if(clocation == 29){

                    var customerid = nlapiGetFieldValue('entity');
                    var userid = nlapiGetUser();
                    var user = nlapiLookupField('employee', userid, 'entityid');

                    var row = nlapiLoadRecord('customer', customerid);
                    var ret = json_serialize(row)
                    ret = append_attached_contacts (ret, customerid);


                    var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
                    record.setFieldValue('custrecord_clgx_queue_ping_record_id', customerid);
                    record.setFieldValue('custrecord_clgx_queue_ping_record_type', 1);
                    record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
                    var idRec = nlapiSubmitRecord(record, false,true);

                    var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
                    record.setFieldValue('custrecord_clgx_queue_ping_record_id', soid);
                    record.setFieldValue('custrecord_clgx_queue_ping_record_type', 3);
                    record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
                    var idRec = nlapiSubmitRecord(record, false,true);


                    /*
                     try {
                     flexapi('POST','/netsuite/update', {
                     'type': 'customer',
                     'id': customerid,
                     'action': type,
                     'userid': userid,
                     'user': user,
                     'data': ret
                     });
                     //nlapiLogExecution('DEBUG', 'flexapi request customerid: ', customerid);
                     }
                     catch (error) {

                     var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
                     record.setFieldValue('custrecord_clgx_queue_ping_record_id', customerid);
                     record.setFieldValue('custrecord_clgx_queue_ping_record_type', 1);
                     record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
                     var idRec = nlapiSubmitRecord(record, false,true);
                     }
                     try {
                     var row = nlapiLoadRecord('salesorder', soid);
                     var ret = json_serialize(row);
                     ret = append_services_powers (ret, soid, 'salesorder');
                     flexapi('POST','/netsuite/update', {
                     'type': 'salesorder',
                     'id': soid,
                     'action': type,
                     'userid': userid,
                     'user': user,
                     'data': ret
                     });
                     //nlapiLogExecution('DEBUG', 'flexapi request soid: ', soid);
                     } catch (error) {
                     var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
                     record.setFieldValue('custrecord_clgx_queue_ping_record_id', soid);
                     record.setFieldValue('custrecord_clgx_queue_ping_record_type', 3);
                     record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
                     var idRec = nlapiSubmitRecord(record, false,true);
                     }
                     */

                    body += '05 - ' + moment().diff(start) + ' | ';

                }
            }

//---------- End Sections ------------------------------------------------------------------------------------------------
        }

        if (type == 'edit'|| type == 'copy'){

            id = nlapiGetRecordId();

            nlapiLogExecution('ERROR','SO afterSubmit Processing Time - ' + id, moment().diff(start));
            //nlapiSendEmail(71418,71418,'SO afterSubmit Processing Time - ' + id, body,null,null,null,null);

            var record = nlapiCreateRecord('customrecord_clgx_script_exec_time');
            record.setFieldValue('custrecord_clgx_script_exec_time_user', nlapiGetUser());
            record.setFieldValue('custrecord_clgx_script_exec_time_rec', 'so');
            record.setFieldValue('custrecord_clgx_script_exec_time_id', nlapiGetRecordId());
            record.setFieldValue('custrecord_clgx_script_exec_time_context', 3);
            record.setFieldValue('custrecord_clgx_script_exec_time', moment().diff(start));
            record.setFieldValue('custrecord_clgx_script_exec_time_details', body);
            try {
                nlapiSubmitRecord(record, false, true);
            }
            catch (error) {
            }

        }

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}




function getDateRange(month,year){
    var stMonth = month - 1;
    var stDays  = daysInMonth(parseInt(stMonth),parseInt(year));
    var stYear  = year;

    var stStartDate = month + '/1/' + stYear;
    var stEndDate   = month + '/' + stDays + '/' + stYear;

    var arrDateRange = new Array();
    arrDateRange[0] = stStartDate;
    arrDateRange[1] =stEndDate;

    return arrDateRange;
}


function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}


function getDateToday(){
    var date = new Date();
    var month = parseInt(date.getMonth()) + 1;
    var day = date.getDate();
    var year = date.getFullYear();

    if(parseInt(month) < 10){
        month = '0' + month;
    }

    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = month + '/' + day + '/' + year;

    return formattedDate;
}
