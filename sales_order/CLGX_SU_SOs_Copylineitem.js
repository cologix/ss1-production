nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	        CLGX_SU_SOs_Copylineitem.js 
//	Script Name:	CLGX_SU_SOs_Copylineitem
//	Script Id:		customscript_clgx_su_sos_copylineitem
//	Script Runs:	On Server
//	Script Type:	User Script
//	Deployments:	
//	@authors:		derek hitchman derek@erpadvisorsgroup.com
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		
//	Includes:		




function userEventBeforeSubmit(type, form, request){
	nlapiLogExecution('DEBUG', 'Value', '| userEventBeforeSubmit ');
	
	//only run for records already saved which have an ID
	if(nlapiGetRecordId()) {
    	 
	  try{
	    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin User Script--------------------------|'); 
	    		    	
	    	var currentRecord = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
	    	var copyState = currentRecord.getFieldValue('custbody_clgx_so_iscopy_lines');
	         if (copyState == 'T'){
	        	 //comment out the below setFieldValue to cause afterSubmit to do a redirect to edit mode
	        	 //currentRecord.setFieldValue('custbody_clgx_so_iscopy_lines', 'F');
	        	 //copyLineItems();
	        	 //nlapiSubmitRecord(currentRecord, false, true);
	         }
	  }
	  catch (error){
	        if (error.getDetails != undefined){
	            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	            throw error;
	        }
	        else{
	            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	            throw nlapiCreateError('99999', error.toString());
	        }
	    }
	  
	}
}
	 

function userEventAfterSubmit(type, form, request){
	nlapiLogExecution('DEBUG', 'Value', '| userEventAfterSubmit ');
	
	  try{
	    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin User Script--------------------------|'); 
	    		    	
	    	var currentRecord = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
	    	var copyState = currentRecord.getFieldValue('custbody_clgx_so_iscopy_lines');
	    	//<>if we want to redirect in the afterSubmit area
	         if (copyState == 'T'){
	        	 currentRecord.setFieldValue('custbody_clgx_so_iscopy_lines', 'F');
		    	 nlapiSubmitRecord(currentRecord, false, true);
	        	 copyLineItems();
	        	 
	         }
	  }
	  catch (error){
	        if (error.getDetails != undefined){
	            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	            throw error;
	        }
	        else{
	            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	            throw nlapiCreateError('99999', error.toString());
	        }
	    }
}
	 




function copyLineItems (){
	
	//------------- Begin Section 1 -------------------------------------------------------------------
	// Fetch all the line items in the current record
	// For each line item:
		// if it has a billing schedule and the status is not NRC
			//create a new line item that uses all the same information, but update the <>
			//reset the old line item rate to 0, the billing schedule to null, the system quantity to the invoice quantity, and the date data to <>
	// 
	//-------------------------------------------------------------------------------------------------

	    try{
	    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin User Script--------------------------|'); 
	    		    	
	    	var currentRecord = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
	    	
	       	var originalItemCount = currentRecord.getLineItemCount('item');
	       	
	    
	    	for (var i = 0; i < originalItemCount; i++){
	    		//note currentItemCount vs originalItemCount. We are going to insert some new lines, which will increment the currentItemCount
	    		var currentItemCount = currentRecord.getLineItemCount('item');
	    		var bs = currentRecord.getLineItemText('item','billingschedule', i + 1);
	    		var recurring = currentRecord.getLineItemText('item','class', i + 1);

	    		if (recurring.indexOf("NRC") > -1){
	    			currentRecord.setLineItemValue('item','billingschedule', i+1, null);	    			
	    			currentRecord.setLineItemValue('item','rate', i+1, 0);
	    			currentRecord.setLineItemValue('item','amount', i+1, 0);
	    			
    		
	    		}
	    		else{ // if this item is recurring
	    			// if billing schedule is not null copy item to new line and make billing schedule 'Month to Month', then set rate to 0 and remove Bill Schedule on old line
	    			if(bs != null && bs != ''){ 
	    				 
	    				var quantitybilled= currentRecord.getLineItemValue('item','quantitybilled', i + 1);
						var quantity= currentRecord.getLineItemValue('item','quantity', i + 1);
						var rate = currentRecord.getLineItemValue('item','rate', i + 1);
						var amount = currentRecord.getLineItemValue('item','amount', i + 1);
						var billingschedule = currentRecord.getLineItemValue('item','billingschedule', i + 1);
					
						if (quantitybilled == null){
							quantitybilled = '0';
						}
						if (quantity == null){
							quantity = '0';
						}
						if (rate == null){
							rate = '0';
						}
						if (amount == null){
							amount = '0';
						}
						
						var intCount = currentRecord.getLineItemCount('item');
						currentRecord.insertLineItem('item', intCount + 1);
						currentRecord.setLineItemValue('item','custcol_clgx_so_col_service_id', intCount + 1, currentRecord.getLineItemValue('item','custcol_clgx_so_col_service_id', i + 1));
	    				currentRecord.setLineItemValue('item','item', intCount + 1, currentRecord.getLineItemValue('item','item', i + 1));
	    				currentRecord.setLineItemValue('item','custcol_clgx_qty2print', intCount + 1, currentRecord.getLineItemValue('item','custcol_clgx_qty2print', i + 1));
	    				currentRecord.setLineItemValue('item','description', intCount + 1, currentRecord.getLineItemValue('item','description', i + 1));
	    				currentRecord.setLineItemValue('item','pricelevels', intCount + 1, currentRecord.getLineItemValue('item','pricelevels', i + 1));
	    				currentRecord.setLineItemValue('item','taxcode',intCount + 1,  currentRecord.getLineItemValue('item','taxcode', i + 1));
	    				currentRecord.setLineItemValue('item','taxrate1', intCount + 1, currentRecord.getLineItemValue('item','taxrate1', i + 1));
	    				currentRecord.setLineItemValue('item','taxrate2', intCount + 1, currentRecord.getLineItemValue('item','taxrate2', i + 1));
	    				currentRecord.setLineItemValue('item','class', intCount + 1, currentRecord.getLineItemValue('item','class', i + 1));
	    				currentRecord.setLineItemValue('item','billingschedule', intCount + 1, billingschedule);
	    				currentRecord.setLineItemValue('item','location', intCount + 1, currentRecord.getLineItemValue('item','location', i + 1));
	    				currentRecord.setLineItemValue('item','quantity', intCount + 1, quantity);
	    				currentRecord.setLineItemValue('item','quantitybilled', intCount + 1, 0);
	    				currentRecord.setLineItemValue('item','price', intCount + 1, currentRecord.getLineItemValue('item','price', i + 1));
	    				currentRecord.setLineItemValue('item','options', intCount + 1, currentRecord.getLineItemValue('item','options', i + 1));
	    				currentRecord.setLineItemValue('item','custcol_cologix_unevendaystobill', intCount + 1, currentRecord.getLineItemValue('item','custcol_cologix_unevendaystobill', i + 1));
	    				currentRecord.setLineItemValue('item','custcol_clgx_rate_per_day_28', intCount + 1, currentRecord.getLineItemValue('item','custcol_clgx_rate_per_day_28', i + 1));
	    				currentRecord.setLineItemValue('item','custcol_cologix_rateperday', intCount + 1, currentRecord.getLineItemValue('item','custcol_cologix_rateperday', i + 1));
	    				currentRecord.setLineItemValue('item','custcol_clgx_rate_per_day_31', intCount + 1, currentRecord.getLineItemValue('item','custcol_clgx_rate_per_day_31', i + 1));	    				
	    				currentRecord.setLineItemValue('item','custcol_cologix_invoice_item_category', intCount + 1, currentRecord.getLineItemValue('item','custcol_cologix_invoice_item_category', i + 1));
	    				currentRecord.setLineItemValue('item','rate', intCount + 1, rate);
	    				currentRecord.setLineItemValue('item','amount', intCount + 1, amount);
	    				currentRecord.setLineItemValue('item','custbody_needs_lease_mod',  intCount + 1, 'T');  //LS 4/14/2020

	    				//set the old line item values
	    				
	    				currentRecord.setLineItemValue('item','billingschedule', i+1, null);
						currentRecord.setLineItemValue('item','quantity', i+1, quantitybilled);
	    				currentRecord.setLineItemValue('item','rate', i+1, 0);
	    				currentRecord.setLineItemValue('item','amount', i+1, 0);
	    				currentRecord.setLineItemValue('item','custcol_clgx_old_rate', i+1, rate);
	    				currentRecord.setLineItemValue('item','custcol_clgx_date_closed', i+1, getDateToday());	    					    			
	    				
	    				
	    				
					
	    			}
	    		}
	    		
	    	}
	    	
	    	 currentRecord.setFieldValue('custbody_clgx_so_iscopy_lines', 'F');
	        	
	    	 nlapiSubmitRecord(currentRecord, false, true);
	    }
	// ========================================================================================================================
	    catch (error){
	        if (error.getDetails != undefined){
	            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	            throw error;
	        }
	        else{
	            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	            throw nlapiCreateError('99999', error.toString());
	        }
	    }
	    
	  
   	 
	}



	function getDateRange(month,year){
	    var stMonth = month - 1;
	    var stDays  = daysInMonth(parseInt(stMonth),parseInt(year));
	    var stYear  = year;
	    
	    var stStartDate = month + '/1/' + stYear;
	    var stEndDate   = month + '/' + stDays + '/' + stYear;
	    
	    var arrDateRange = new Array();
	        arrDateRange[0] = stStartDate;
	        arrDateRange[1] =stEndDate;
	    
	    return arrDateRange;
	}


	function daysInMonth(intMonth, intYear){
	    if (intMonth < 0 || intMonth > 11){
	        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
	    }
	    var lastDayArray = [
	        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
	    ];
	    if (intMonth != 1){
	        return lastDayArray[intMonth];
	    }
	    if (intYear % 4 != 0){
	        return lastDayArray[1];
	    }
	    if (intYear % 100 == 0 && intYear % 400 != 0){
	        return lastDayArray[1];
	    }
	    return lastDayArray[1] + 1;
	}


	function getDateToday(){
	    var date = new Date();
	    var month = parseInt(date.getMonth()) + 1;
	    var day = date.getDate();
	    var year = date.getFullYear();
	    
	    if(parseInt(month) < 10){
	        month = '0' + month;
	    }
	    
	    if(parseInt(day) < 10){
	        day = '0' + day;
	    }
	    var formattedDate = month + '/' + day + '/' + year;
	    
	    return formattedDate;
	}


