nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SO_Waiting_Time.js
//	Script Name:	CLGX_SS_SO_Waiting_Time
//	Script Id:		customscript_clgx_ss_so_waiting_time
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Service Order
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		5/30/2013
//-------------------------------------------------------------------------------------------------
function scheduled_so_waiting_time(){
    try{
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	2.0 - 6/6/2012
// Details:	Verify and update consolidate locations.
//-----------------------------------------------------------------------------------------------------------------
    	
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 

		var searchSOs = nlapiSearchRecord('salesorder','customsearch_clgx_sched_so_waiting_time',null,null);
		
		for ( var i = 0; searchSOs != null && i < searchSOs.length; i++ ) {
			var searchSO = searchSOs[i];
			var soid  = searchSO.getValue('internalid', null, 'GROUP');

			var searchColumn = new Array();
			searchColumn.push(new nlobjSearchColumn('internalid',null,null));
			searchColumn.push(new nlobjSearchColumn('custrecord_clgx_so_waiting_start_date',null,null));
			searchColumn.push(new nlobjSearchColumn('custrecord_clgx_so_waiting_start_time',null,null));
			var searchFilter = new Array();
			searchFilter.push(new nlobjSearchFilter('custrecord_clgx_so_waiting_service_order',null,'anyof',soid));
			searchFilter.push(new nlobjSearchFilter('custrecord_clgx_so_waiting_end_date',null,'isempty'));
			searchFilter.push(new nlobjSearchFilter('custrecord_clgx_so_waiting_start_date',null,'isnotempty'));
			searchFilter.push(new nlobjSearchFilter('custrecord_clgx_so_waiting_start_time',null,'isnotempty'));
			var searchWaitTimes = nlapiSearchRecord('customrecord_clgx_so_waiting',null,searchFilter,searchColumn);
			
			var totalHours = 0;
			for ( var j = 0; searchWaitTimes != null && j < searchWaitTimes.length; j++ ) {
				var searchWaitTime = searchWaitTimes[j];
				
				var startDate = searchWaitTime.getValue('custrecord_clgx_so_waiting_start_date', null, null);
				var startTime = searchWaitTime.getValue('custrecord_clgx_so_waiting_start_time', null, null);
				var id = searchWaitTime.getValue('internalid', null, null);
				
				hours = hoursDiff (startDate,startTime);
				nlapiSubmitField('customrecord_clgx_so_waiting',id,'custrecord_clgx_so_waiting_hours',hours);
				totalHours += hours;
			}
		    nlapiSubmitField('salesorder',soid,'custbody_clgx_wtng_cust_total_hours',totalHours);
		}

        nlapiLogExecution('DEBUG','Finished Execution', '|-------------------------- Finished Scheduled Script --------------------------|');

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function hoursDiff (startDate,startTime) {

	var date = new Date();
    var hour = date.getHours();
    if(parseInt(hour) > 11){
    	var ampm = 'pm';
    	if(parseInt(hour) > 12){
    		hour = parseInt(hour) - 12;
    	}
    }
    else{
    	var ampm = 'am';
    }
    var minute = date.getMinutes();
    var end = new Date(getDate() + ' ' + hour + ':' + minute + ' ' + ampm);
    var start = new Date(startDate + ' ' + startTime);

    var timeDiff = Math.abs(end.getTime() - start.getTime());
    return  Math.ceil(timeDiff / (1000 * 3600)); 
}

function getDate(){
    var date = new Date();
    var month = parseInt(date.getMonth()) + 1;
    var day = date.getDate();
    var year = date.getFullYear();
    if(parseInt(month) < 10){
        month = '0' + month;
    }
    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = month + '/' + day + '/' + year;
    return formattedDate;
}


function getTime(){
    var date = new Date();
    var hour = date.getHours();
    if(parseInt(hour) > 11){
    	var ampm = 'pm';
    	if(parseInt(hour) > 12){
    		hour = parseInt(hour) - 12;
    	}
    }
    else{
    	var ampm = 'am';
    }

    var minute = date.getMinutes();
    var second = date.getSeconds();
    var formattedTime = hour + ':' + minute + ' ' + ampm;

    return formattedTime;
}