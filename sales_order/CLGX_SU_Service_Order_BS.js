nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Service_Order_BS.js
//	Script Name:	CLGX_SU_Service_Order_BS
//	Script Id:		customscript_clgx_su_so_bs
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Sales Order - Sandbox | Production
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/14/2011
//	Include:		CLGX_Lib_Global.js, moment.min.js
//-------------------------------------------------------------------------------------------------

function beforeSubmit(type){
    try {
        if (nlapiGetContext().getExecutionContext() == 'userinterface') {
//------------- Begin Section 1 -------------------------------------------------------------------
// Created:	11/11/2013
// Details:	Calculate dates if Actual Install Date has been populated
//			If service on any item was modified, save pair service / service order to custom record to process over night
//			Recalculate quantities, amounts and totals
//-------------------------------------------------------------------------------------------------
            if (type == 'edit' || type == 'copy') {

                //var start = moment();
                //var body = '00 - ' + moment().format('M/D/YYYY h:mm:ss') + '\n';
                //var body = '| ';


                var soid = nlapiGetRecordId();

                // verify if oppty is a renewal
                var renewOppty = 0;
                var opptyid = nlapiGetFieldValue('opportunity');
                if(opptyid != null  && opptyid != ''){
                    var renewOppty = nlapiLookupField('opportunity', opptyid, 'custbody_cologix_opp_sale_type');
                }

                var setTerms = 0;
                var totalNRC = 0;
                var totalMRC = 0;
                var total = 0;
                var bs = '';
                var nbrM = 0;
                var termsMonths = '';
                var recurrences = 0;

                var terms = nlapiGetFieldValue('custbody_cologix_biling_terms');
                var status = nlapiGetFieldValue('status');

                // search billing schedule for this so
                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('billingschedule',null,'GROUP'));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof',soid));
                arrFilters.push(new nlobjSearchFilter('billingschedule',null,'noneof',17));
                arrFilters.push(new nlobjSearchFilter('billingschedule',null,'noneof','@NONE@'));
                var searchBS = nlapiSearchRecord('salesorder', null, arrFilters, arrColumns);

                if(searchBS != null){
                    bs = searchBS[0].getText('billingschedule',  null, 'GROUP');
                }

                // pull out the number of recurrences for the billing schedule from the custom billing schedule record
                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_bs_recurrences',null,null));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_bs_months',null,null));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter('custrecord_clgx_bs_name',null,'is',bs));
                var searchRecur = nlapiSearchRecord('customrecord_clgx_billing_schedules', null, arrFilters, arrColumns);
                if(searchRecur != null){
                    recurrences = searchRecur[0].getValue('custrecord_clgx_bs_recurrences',null,null);
                    if(bs == 'Month to Month'){
                        termsMonths = 'MTM';
                        nbrM = 1;
                    }
                    else if(bs == 'Non Recurring'){
                        termsMonths = '';
                        nbrM = 0;
                    }
                    else{
                        termsMonths = searchRecur[0].getValue('custrecord_clgx_bs_months',null,null);
                        nbrM = parseInt(termsMonths);
                    }
                }

                //body += '01 - ' + moment().diff(start) + ' | ';

                var nbrItems = nlapiGetLineItemCount('item');
                for (var i = 0; i < nbrItems; i++) {

                    var rate = nlapiGetLineItemValue('item', 'rate', i + 1);
                    var quantity = nlapiGetLineItemValue('item', 'quantity', i + 1);
                    var qty2print = nlapiGetLineItemValue('item', 'custcol_clgx_qty2print', i + 1);
                    var billed = nlapiGetLineItemValue('item', 'quantitybilled', i + 1);
                    var amount = nlapiGetLineItemValue('item', 'amount', i + 1);
                    var billsched = nlapiGetLineItemText('item', 'billingschedule', i + 1);
                    var classtype = nlapiGetLineItemText('item', 'class', i + 1);
                    var itemtype = nlapiGetLineItemValue('item', 'itemtype', i + 1);
                    var serviceid = nlapiGetLineItemValue('item', 'custcol_clgx_so_col_service_id', i + 1);

                    if (classtype.indexOf("Recurring") > -1 && itemtype == 'Service') {

                        if(quantity == billed && (billsched == null || billsched == '') && parseFloat(amount) == 0){
                            // closed item, don't do anything
                        }
                        else{
                            nlapiSetLineItemValue('item','quantity', i + 1, recurrences * qty2print);
                            setTerms = 1; // there is at least one recurring, then set the Service Terms
                            totalMRC += parseFloat(rate) * parseFloat(qty2print);
                            total += parseFloat(rate) * parseFloat(recurrences) * parseFloat(qty2print);

                            // save pair service / service order to custom record to process - added 11/10/2013
                            //if (serviceid != '' && serviceid != null && serviceid != -1 && status != 'closed' && status != 'cancelled' && status != 'fullyBilled'){
                            //	var serviceSO = nlapiLookupField('job', serviceid, 'custentity_cologix_service_order');
                            //	if(serviceSO != soid){
                            //		var record = nlapiCreateRecord('customrecord_clgx_services_to_update');
                            //		record.setFieldValue('custrecord_clgx_services_to_update_serv', serviceid);
                            //		record.setFieldValue('custrecord_clgx_services_to_update_so', soid);
                            //		var idRec = nlapiSubmitRecord(record, true,true);
                            //	}
                            //}

                        }
                    }
                    else if (classtype.indexOf("Recurring") > -1 && itemtype == 'Discount'){
                        totalMRC += parseFloat(amount)/parseFloat(recurrences);
                        total += parseFloat(amount);
                    }
                    else if (classtype.indexOf("NRC") > -1 && itemtype == 'Service'){
                        nlapiSetLineItemValue('item','quantity', i + 1, qty2print);
                        totalNRC += parseFloat(rate) * parseFloat(qty2print);
                    }
                    else{}
                }
                //Schedule the script to process the service update.
                var difference = buildComparisonObject(soid);
                if(difference && difference.length > 0) {
                    nlapiScheduleScript("customscript_clgx2_ss_so_services", null, { custscript_clgx2_1740_soid: parseInt(soid) });
                }
                // update totals feilds on the so header
                nlapiSetFieldValue('custbody_clgx_total_recurring', total);
                nlapiSetFieldValue('custbody_clgx_total_recurring_month', totalMRC);
                nlapiSetFieldValue('custbody_clgx_total_non_recurring', totalNRC);

                if(terms == '' && termsMonths != ''){
                    nlapiSetFieldValue('custbody_cologix_biling_terms', termsMonths.toString());
                }

                //body += '02 - ' + moment().diff(start) + ' | ';

//---------- Calculate dates ------------------------------------------------------------------------------------------------

                var installDate = nlapiGetFieldValue('custbody_cologix_service_actl_instl_dt');
                var status = nlapiGetFieldValue('status');
                var aatype = nlapiGetFieldValue('custbodyclgx_aa_type');

                if (installDate != null && installDate != '' && status == 'Pending Approval' && setTerms == 1){

                    var d1 = new moment(installDate);
                    var contractEndDate = d1.subtract('days',1).add('months', nbrM).format('M/D/YYYY');
                    var d2 = new moment(installDate);
                    var nextAADate = d2.startOf('month').add('months', 1).add('years', 1).format('M/D/YYYY');
                    var d3 = new moment(installDate);
                    var renewStartDate = d3.add('months', nbrM).format('M/D/YYYY');

                    nlapiSetFieldValue('custbody_cologix_so_contract_start_dat', installDate);
                    nlapiSetFieldValue('enddate', contractEndDate);
                    if(aatype != '' && aatype!= null){
                        nlapiSetFieldValue('custbody_clgx_next_aa_date', nextAADate);
                    }
                    nlapiSetFieldValue('custbody_clgx_renewal_end_date', contractEndDate);
                    nlapiSetFieldValue('custbody_clgx_renewal_start_date', renewStartDate);
                    nlapiSetFieldValue('custbody_clgx_contract_cycle', 1);

                }
                // this is a renewal and there is no actual install date, so use contract start date when it's populated to calculate the other dates
                else if ((installDate == null || installDate == '') && status == 'Pending Approval' && setTerms == 1 && renewOppty == 1){

                    var contractStartDate = nlapiGetFieldValue('custbody_cologix_so_contract_start_dat');
                    var contractCycle = nlapiGetFieldValue('custbody_clgx_contract_cycle');
                    var newContractCycle = parseInt(contractCycle) + 1;

                    if((contractStartDate != null && contractStartDate != '')){

                        var d1 = new moment(contractStartDate);
                        var contractEndDate = d1.subtract('days',1).add('months', nbrM).format('M/D/YYYY');
                        var d3 = new moment(contractStartDate);
                        var renewStartDate = d3.add('months', nbrM).format('M/D/YYYY');

                        nlapiSetFieldValue('custbody_clgx_renewal_end_date', contractEndDate);
                        nlapiSetFieldValue('custbody_clgx_renewal_start_date', renewStartDate);
                        nlapiSetFieldValue('custbody_clgx_contract_cycle', parseInt(newContractCycle));

                    }
                }
                else{}
            }

            //body += '03 - ' + moment().diff(start) + ' | ';

            //nlapiLogExecution('ERROR','SO beforeSubmit Processing Time - ' + soid, moment().diff(start));
            //nlapiSendEmail(71418,71418,'SO beforeSubmit Processing Time - ' + soid, body,null,null,null,null);
            /*
            var record = nlapiCreateRecord('customrecord_clgx_script_exec_time');
			record.setFieldValue('custrecord_clgx_script_exec_time_user', nlapiGetUser());
			record.setFieldValue('custrecord_clgx_script_exec_time_rec', 'so');
			record.setFieldValue('custrecord_clgx_script_exec_time_id', nlapiGetRecordId());
			record.setFieldValue('custrecord_clgx_script_exec_time_context', 2);
			record.setFieldValue('custrecord_clgx_script_exec_time', moment().diff(start));
			record.setFieldValue('custrecord_clgx_script_exec_time_details', body);
			try {
                nlapiSubmitRecord(record, false, true);
            }
            catch (error) {
            }
			*/


//------------- Begin Section 2 -------------------------------------------------------------------
// Created:	4/30/2015
// Details:	When a SO is created, initialize SilverPOP fields
//-------------------------------------------------------------------------------------------------
            /*
            if (type == 'create' || type == 'copy'){
                nlapiSetFieldValue('custbody_clgx_sp_json_sync', '');
                nlapiSetFieldValue('custbody_clgx_sp_sync', 'F');
            }
            */
//---------- End Sections ------------------------------------------------------------------------------------------------
        }

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}



function buildComparisonObject(serviceOrderID) {
    var serviceOrders = buildServiceOrderObject(serviceOrderID);
    var services      = buildServiceObject(serviceOrderID, _.map(serviceOrders, "sid"));

    return JSON.stringify(_.differenceBy( services,serviceOrders, "soid"));
}


function buildServiceOrderObject(serviceOrderID) {
    var serviceOrderJson    = new Array();
    var serviceOrderFilters = new Array();
    serviceOrderFilters.push(new nlobjSearchFilter("type", null, "anyof", "SalesOrd"));
    serviceOrderFilters.push(new nlobjSearchFilter("internalid", null, "anyof", serviceOrderID));
    serviceOrderFilters.push(new nlobjSearchFilter("custcol_clgx_so_col_service_id", null, "noneof", "@NONE@"));

    var serviceOrderColumns = new Array();
    serviceOrderColumns.push(new nlobjSearchColumn("internalid"));
    serviceOrderColumns.push(new nlobjSearchColumn("custcol_clgx_so_col_service_id"));

    var serviceOrderSearchObject = nlapiSearchRecord("transaction", null, serviceOrderFilters, serviceOrderColumns);

    if(serviceOrderSearchObject != null && serviceOrderSearchObject != "") {
        for(var soso = 0; soso < serviceOrderSearchObject.length; soso++) {
            serviceOrderJson.push({ soid: parseInt(serviceOrderSearchObject[soso].getValue("internalid")), sid: parseInt(serviceOrderSearchObject[soso].getValue("custcol_clgx_so_col_service_id")) });
        }
    }

    return serviceOrderJson;
}


function buildServiceObject(serviceOrderID, serviceOrderArray) {
    var serviceJson    = new Array();

    if(serviceOrderArray && serviceOrderArray.length > 0) {
        var serviceFilters = new Array();

        serviceFilters.push(new nlobjSearchFilter("internalid", null, "anyof", serviceOrderArray));
        serviceFilters.push(new nlobjSearchFilter("isinactive", null, "is", "F"));

        var serviceColumns = new Array();
        serviceColumns.push(new nlobjSearchColumn("internalid"));
        serviceColumns.push(new nlobjSearchColumn("custentity_cologix_service_order"));

        var serviceSearchObject = nlapiSearchRecord("job", null, serviceFilters, serviceColumns);

        if(serviceSearchObject != null && serviceSearchObject != "") {
            for(var sso = 0; sso < serviceSearchObject.length; sso++) {
                serviceJson.push({ sid: parseInt(serviceSearchObject[sso].getValue("internalid")), soid: parseInt(serviceSearchObject[sso].getValue("custentity_cologix_service_order")) });
            }
        }
    }

    return serviceJson;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////