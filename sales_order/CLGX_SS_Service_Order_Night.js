nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Service_Order_Night.js
//	Script Name:	CLGX_SS_Service_Order_Night
//	Script Id:		customscript_clgx_ss_so_night
//	Script Runs:	On Serverformatformatf
//	Script Type:	Scheduled Script
//	Deployments:	Service Order
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		02/01/2013
//	Includes:		moment.min.js, CLGX_LIB_Global.js
//-------------------------------------------------------------------------------------------------

function scheduled_service_order(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');
        var context = nlapiGetContext();

        //var date = new Date();
        //var saleseffectivedate = nlapiDateToString(date);
        
        var startScript = moment();
        var emailAdminSubject = 'Report - Service Orders - Night';
        var emailAdminBody = '';
        emailAdminBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');

//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 3/19/2014
// Details:	Process queued proposals.
//-------------------------------------------------------------------------------------------------

        var arrNewSOs = new Array();

        emailAdminBody += '<h2>Proposals processed</h2>';
        emailAdminBody += '<table border="1" cellpadding="5">';
        emailAdminBody += '<tr><td>Proposal</td><td>Location</td><td>Billing Schedule</td><td>Service Order</td><td>Execution Time</td><td>Minutes</td><td>Usage</td></tr>';

        // search all proposals in queue
        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_proposals_to_sos_propid',null,null));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_proposals_to_sos_userid',null,null));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_proposals_to_sos_done",null,"is",'F'));
        var searchProposals = nlapiSearchRecord('customrecord_clgx_proposals_to_sos', null, arrFilters, arrColumns);

        var arrNewSOs = new Array(); // SOs array created from all proposals
        for ( var i = 0; searchProposals != null && i < searchProposals.length; i++ ) { // loop all proposals in queue

            var startExec = moment(); // time when each loop is starting
            var execMinutes = (startExec.diff(startScript)/60000).toFixed(1); // execution time in minutes
            if ( (context.getRemainingUsage() <= 1000 || execMinutes > 50) && (i+1) < searchProposals.length ){ // if 1000 points left or more then 50 min execution time, re-schedule the script
                //var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                /*if ( status == 'QUEUED' ) {
                    nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
                    break;
                }*/
            	break;
            }

            var searchProposal = searchProposals[i];
            var recid = searchProposal.getValue('internalid',null,null);
            var proposalid = searchProposal.getValue('custrecord_clgx_proposals_to_sos_propid',null,null);
            var userid = searchProposal.getValue('custrecord_clgx_proposals_to_sos_userid',null,null);

            nlapiLogExecution('DEBUG','Process queued proposals', '| proposalid: ' + proposalid + ' |');
            
            // search if there are any SOs for this proposal
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("createdfrom",null,"anyof",proposalid));
            var searchSOs = nlapiSearchRecord('salesorder', null, arrFilters, arrColumns);

            if(searchSOs == null){ // no SO was created for this proposal - create them

                var intChar = 1;

                var recProposal = nlapiLoadRecord('estimate', proposalid);
                var proposal = recProposal.getFieldValue('tranid');
                var opptyid = recProposal.getFieldValue('opportunity');
                var incrementalMrc=recProposal.getFieldValue('custbody_clgx_prop_incremental_mrc');
                var subsidiary = recProposal.getFieldValue('subsidiary');
                var currency = recProposal.getFieldValue('currency');
                var exchangerate = recProposal.getFieldValue('exchangerate');

                var arrRenewSOs = recProposal.getFieldValues('custbody_clgx_renewed_from_sos'); // get SOs from which the oppty/proposal were created in case it's a renewal

                var emailUserSubject = 'Report - Service Order(s) created for proposal ' + proposal;
                var emailUserBody = '';
                emailUserBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');
                emailUserBody += '<h2>Report - Service Order(s) created for proposal ' + proposal + '</h2>';
                emailUserBody += '<table border="1" cellpadding="5">';
                emailUserBody += '<tr><td>Location</td><td>Billing Schedule</td><td>Service Order</td></tr>';

                // search locations on proposal items
                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('location',null,'GROUP'));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",proposalid));
                //arrFilters.push(new nlobjSearchFilter("billingschedule",null,"noneof",17));
                arrFilters.push(new nlobjSearchFilter("location",null,"noneof",'@NONE@'));
                arrFilters.push(new nlobjSearchFilter("billingschedule",null,"noneof",'@NONE@'));
                var searchLocations = nlapiSearchRecord('estimate', null, arrFilters, arrColumns);

                // loop locations
                for ( var j = 0; searchLocations != null && j < searchLocations.length; j++ ) {
                    var searchLocation = searchLocations[j];
                    var locationid = searchLocation.getValue('location',  null, 'GROUP');
                    var location = searchLocation.getText('location',  null, 'GROUP');

                    // search the NRC items for this location
                    var arrColumns = new Array();
                    arrColumns.push(new nlobjSearchColumn('rate',null,null));
                    arrColumns.push(new nlobjSearchColumn('taxcode',null,null));
                    var arrFilters = new Array();
                    arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof',proposalid));
                    arrFilters.push(new nlobjSearchFilter('location',null,'anyof',locationid));
                    arrFilters.push(new nlobjSearchFilter('billingschedule',null,'anyof',17));
                    arrFilters.push(new nlobjSearchFilter('billingschedule',null,'noneof','@NONE@'));
                    arrFilters.push(new nlobjSearchFilter('isinactive','item','is','F'));
                    var searchItemsNRCs = nlapiSearchRecord('estimate', 'customsearch_clgx_so_renewals_proposal', arrFilters, arrColumns);

                    // search and group recurring billing schedules for this location
                    var arrColumns = new Array();
                    arrColumns.push(new nlobjSearchColumn('billingschedule',null,'GROUP'));
                    var arrFilters = new Array();
                    arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof',proposalid));
                    arrFilters.push(new nlobjSearchFilter('location',null,'anyof',locationid));
                    arrFilters.push(new nlobjSearchFilter('billingschedule',null,'noneof',17));
                    arrFilters.push(new nlobjSearchFilter('billingschedule',null,'noneof','@NONE@'));
                    arrFilters.push(new nlobjSearchFilter('isinactive','item','is','F'));
                    var searchBSs = nlapiSearchRecord('estimate', null, arrFilters, arrColumns);

                    if (searchBSs != null) { // if there are recurring billing schedules for this location

                        // loop all billing schedules for each location
                        for ( var k = 0;  k < searchBSs.length; k++ ) {
                            var arraySOSOLD=new Array();
                            var startExec = moment();
                            var execMinutes = (startExec.diff(startScript)/60000).toFixed(1);

                            var searchBS = searchBSs[k];
                            var bsid = searchBS.getValue('billingschedule',  null, 'GROUP');
                            var bs = searchBS.getText('billingschedule',  null, 'GROUP');

                            // calculate number of recurring months -----------------------------------------
                            var strMonths = bs.substring(0, 3);
                            if(strMonths == 'Mon') {
                                strMonths = '1';
                            }
                            // remove last character from string if a "M" - for billing schedules 1-9
                            else if(strMonths.charAt( strMonths.length - 1 ) == 'M') {
                                strMonths = strMonths.slice(0, -1);
                            }
                            else{}
                            intMonths = parseInt(strMonths); //  = 3 first digits of the billing schedule text

                            // pull out the number of recurrences for the billing schedule from the custom billing schedule record --------------------------------------------------
                            // TODO - switch to the native billingschedule record
                            var recurrences = 0; // default to 0 in case no result - ???? or 1 ????
                            var arrColumns = new Array();
                            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_bs_recurrences',null,null));
                            var arrFilters = new Array();
                            arrFilters.push(new nlobjSearchFilter('custrecord_clgx_bs_name',null,'is',bs));
                            var searchRecur = nlapiSearchRecord('customrecord_clgx_billing_schedules', null, arrFilters, arrColumns);
                            if(searchRecur != null){
                                recurrences = searchRecur[0].getValue('custrecord_clgx_bs_recurrences',null,null);
                            }

                            // create SO from the proposal by transforming the proposal in SO and inherit all fields
                            var recSONew = nlapiTransformRecord('estimate', proposalid, 'salesorder');
                            var customerid = recSONew.getFieldValue('entity');

                            recSONew.setFieldValue('saleseffectivedate', moment().format('M/D/YYYY') );
                            recSONew.setFieldValue('custbody_clgx_ready_to_uplift', 'T');
                            recSONew.setFieldValue('location', locationid);

                            // inherit proposal number with SO prefix and one letter sufix
                            recSONew.setFieldValue('custbody_clgx_so_incremental_mrc',incrementalMrc);
                            var so = 'SO' + proposal + '-' + intChar.toString();
                            recSONew.setFieldValue('tranid', so);

                            // remove all items from SO to add only the right ones
                            var nbrItems = recSONew.getLineItemCount('item');
                            for (var l = 0; l < nbrItems; l++){
                                recSONew.removeLineItem('item',1);
                            }

                            // add only the items of this pair location/BS
                            var totalNRC = 0;
                            var incrementalAmount=parseFloat(0);
                            if(k == 0){ // only for the first recurring billing schedule - add all NRC items of this location - if new, one pair loc/bs anyway - and if renewal, no NRCs
                                for ( var m = 0; searchItemsNRCs != null && m < searchItemsNRCs.length; m++ ) {
                                    var searchItemsNRC = searchItemsNRCs[m];

                                    var itemRate = parseFloat(searchItemsNRC.getValue('rate',null,null));
                                    var itemQty = searchItemsNRC.getValue('quantity', null, null);
                                    if (itemRate == null || itemRate == '' || itemRate == 'NaN'){
                                        itemRate = 0;
                                    }
                                    if(subsidiary == 6 && currency == 1){ // Canada subsidiary and $US - use itemCalcRate instead of itemRate
                                        itemRate = parseFloat(itemRate) / parseFloat(exchangerate);
                                    }

                                    recSONew.selectNewLineItem('item');
                                    recSONew.setCurrentLineItemValue('item','item', searchItemsNRC.getValue('item',  null, null));
                                    recSONew.setCurrentLineItemValue('item','billingschedule', searchItemsNRC.getValue('billingschedule',  null, null));
                                    recSONew.setCurrentLineItemValue('item','quantity', searchItemsNRC.getValue('quantity', null, null));
                                    recSONew.setCurrentLineItemValue('item','custcol_clgx_qty2print', searchItemsNRC.getValue('quantity', null, null));
                                    recSONew.setCurrentLineItemValue('item','rate', itemRate);
                                    recSONew.setCurrentLineItemValue('item','amount', parseFloat(itemRate) * parseFloat(itemQty));
                                    recSONew.setCurrentLineItemValue('item','description', searchItemsNRC.getValue('memo',  null, null));
                                    recSONew.setCurrentLineItemValue('item','price', searchItemsNRC.getValue('pricelevel',  null, null));
                                    recSONew.setCurrentLineItemValue('item','taxcode', searchItemsNRC.getValue('taxcode',  null, null));
                                    recSONew.setCurrentLineItemValue('item','options', searchItemsNRC.getValue('options',  null, null));
                                    //recSONew.setCurrentLineItemValue('item','costestimatetype', searchItemsNRC.getValue('costestimatetype',  null, null));
                                    recSONew.setCurrentLineItemValue('item','location', searchItemsNRC.getValue('location',  null, null));
                                    recSONew.setCurrentLineItemValue('item','class', searchItemsNRC.getValue('class',  null, null));
                                    recSONew.setCurrentLineItemValue('item','custcol_cologix_oppty_item_category', searchItemsNRC.getValue('custcol_cologix_invoice_item_category',  null, null));
                                    recSONew.commitLineItem('item');

                                    totalNRC += parseFloat(parseFloat(itemRate) * parseFloat(itemQty));
                                }
                            }

                            var totalMRC = 0;
                            var totalMRCBase = 0;
                            var total = 0;
                            var setTerms = 0; // if no recurring items, do not set Service Terms

                            // search items recurring items for the location/BS
                            var arrColumns = new Array();
                            arrColumns.push(new nlobjSearchColumn('rate',null,null));
                            arrColumns.push(new nlobjSearchColumn('taxcode',null,null));
                            arrColumns.push(new nlobjSearchColumn('custentity_cologix_service_order','custcol_clgx_so_col_service_id',null));
                            var arrFilters = new Array();
                            arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof',proposalid));
                            arrFilters.push(new nlobjSearchFilter('location',null,'anyof',locationid));
                            arrFilters.push(new nlobjSearchFilter('billingschedule',null,'anyof',bsid));
                            var searchItems = nlapiSearchRecord('estimate', 'customsearch_clgx_so_renewals_proposal', arrFilters, arrColumns);

                            for ( var n = 0; searchItems != null && n < searchItems.length; n++ ) {
                                var searchItem = searchItems[n];

                                var itemRate = parseFloat(searchItem.getValue('rate',null,null));
                                var itemQty = searchItem.getValue('quantity',null,null);
                                if (itemRate == null || itemRate == '' || itemRate == 'NaN'){
                                    itemRate = 0;
                                }
                                if(subsidiary == 6 && currency == 1){ // Canada subsidiary and $US - use itemCalcRate instead of itemRate
                                    itemRate = parseFloat(itemRate) / parseFloat(exchangerate);
                                }
                                incrementalAmount=parseFloat(incrementalAmount)+(parseFloat(itemRate) * parseFloat(searchItem.getValue('quantity',null,null)));

                                if((searchItem.getValue('custentity_cologix_service_order','custcol_clgx_so_col_service_id', null)!='')&&(searchItem.getValue('custentity_cologix_service_order','custcol_clgx_so_col_service_id', null)!=null))
                                {

                                    arraySOSOLD.push(searchItem.getValue('custentity_cologix_service_order','custcol_clgx_so_col_service_id', null));

                                }

                                recSONew.selectNewLineItem('item');
                                recSONew.setCurrentLineItemValue('item','custcol_clgx_so_col_service_id', searchItem.getValue('custcol_clgx_so_col_service_id',  null, null));
                                recSONew.setCurrentLineItemValue('item','billingschedule', searchItem.getValue('billingschedule',  null, null));
                                recSONew.setCurrentLineItemValue('item','item', searchItem.getValue('item',  null, null));
                                recSONew.setCurrentLineItemValue('item','quantity', parseFloat(recurrences) * parseFloat(itemQty));
                                recSONew.setCurrentLineItemValue('item','custcol_clgx_qty2print', searchItem.getValue('quantity',null,null));
                                recSONew.setCurrentLineItemValue('item','rate', itemRate);
                                recSONew.setCurrentLineItemValue('item','amount', parseFloat(itemRate) * parseFloat(recurrences) * parseFloat(itemQty));
                                recSONew.setCurrentLineItemValue('item','description', searchItem.getValue('memo',  null, null));
                                recSONew.setCurrentLineItemValue('item','price', searchItem.getValue('pricelevel',  null, null));
                                recSONew.setCurrentLineItemValue('item','taxcode', searchItem.getValue('taxcode',  null, null));
                                recSONew.setCurrentLineItemValue('item','options', searchItem.getValue('options',  null, null));
                                //recSONew.setCurrentLineItemValue('item','costestimatetype', searchItem.getValue('costestimatetype',  null, null));
                                recSONew.setCurrentLineItemValue('item','location', searchItem.getValue('location',  null, null));
                                recSONew.setCurrentLineItemValue('item','class', searchItem.getValue('class',  null, null));
                                recSONew.setCurrentLineItemValue('item','custcol_cologix_oppty_item_category', searchItem.getValue('custcol_cologix_invoice_item_category',  null, null));
                                recSONew.commitLineItem('item');

                                total += parseFloat(parseFloat(itemRate) * parseFloat(recurrences) * parseFloat(itemQty));
                                totalMRC += parseFloat(parseFloat(itemRate) * parseFloat(itemQty));
                                setTerms = 1; // there is at least one reccurring item; set terms
                            }
                            intChar += 1;
                            recSONew.setFieldValue('custbody_clgx_total_recurring', total);
                            recSONew.setFieldValue('custbody_clgx_total_recurring_month', totalMRC);
                            recSONew.setFieldValue('custbody_clgx_total_non_recurring', totalNRC);

                            recSONew.setFieldValue('custbody_clgx_consolidate_locations', clgx_return_consolidate_location (locationid));
                            if(setTerms == 1){
                                if(bs == 'Month to Month'){
                                    recSONew.setFieldValue('custbody_cologix_biling_terms', 'MTM');
                                }
                                else{
                                    recSONew.setFieldValue('custbody_cologix_biling_terms', intMonths.toString());
                                }
                            }
                            if(arraySOSOLD.length > 0)
                            {
                                var arrColumnsIn = new Array();
                                var arrFiltersIn = new Array();
                                arrFiltersIn.push(new nlobjSearchFilter('internalid',null,'anyof',arraySOSOLD));
                                arrFiltersIn.push(new nlobjSearchFilter('location',null,'anyof',locationid));
                                arrFiltersIn.push(new nlobjSearchFilter('billingschedule',null,'noneof',17));
                                arrFiltersIn.push(new nlobjSearchFilter('billingschedule',null,'noneof','@NONE@'));
                                arrFiltersIn.push(new nlobjSearchFilter('isinactive','item','is','F'));
                                var searchIncrementalSOs=nlapiSearchRecord('salesorder','customsearch_clgx_ss_incrsosren',arrFiltersIn,arrColumnsIn);
                                if(searchIncrementalSOs!=null)
                                {

                                    var searchIncrementalSO = searchIncrementalSOs[0];
                                    var columnsIn = searchIncrementalSO.getAllColumns();
                                    var oldIcremental = parseFloat(searchIncrementalSO.getValue(columnsIn[0]));

                                    var incrementalFinal=parseFloat(incrementalAmount)-parseFloat(oldIcremental);
                                    if(incrementalFinal>parseFloat(0))
                                    {
                                        recSONew.setFieldValue('custbody_clgx_so_incremental_mrc',incrementalFinal);
                                    }
                                }
                            }
                            var soid = nlapiSubmitRecord(recSONew, false, true);
                            arrNewSOs.push(soid); // add to the array to use later

                            var totals = clgx_transaction_totals (customerid, 'so', soid);

                            var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                            emailAdminBody += '<tr><td>' + location + '</td><td>' + bs + '</td><td><a href="https://1337135.app.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soid + '">' + so + '</a></td><td>' + startExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + execMinutes + '</td><td>'+ usageConsumtion + '</td></tr>';
                            emailUserBody += '<tr><td>' + location + '</td><td>' + bs + '</td><td><a href="https://1337135.app.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soid + '">' + so + '</a></td></tr>';
                            nlapiLogExecution('DEBUG','Created SO - ', location + ' / ' + bs + ' / ' + so);

                            // update back the SOs that renewed the proposal with this soid
                            for ( var o = 0; arrRenewSOs != null && o < arrRenewSOs.length; o++ ) {
                                nlapiSubmitField('salesorder', arrRenewSOs[o], 'custbody_clgx_so_renewed_on_so', soid);
                            }
                        }

                    }
                    else if (searchItemsNRCs != null){ // there is no recurring billing schedules, so create one SO for this location for NRCs

                        var startExec = moment();
                        var execMinutes = (startExec.diff(startScript)/60000).toFixed(1);

                        // create SO from the proposal
                        var recSONew = nlapiTransformRecord('estimate', proposalid, 'salesorder');
                        var customerid = recSONew.getFieldValue('entity');

                        recSONew.setFieldValue('saleseffectivedate', moment().format('M/D/YYYY') );
                        recSONew.setFieldValue('custbody_clgx_ready_to_uplift', 'T');
                        recSONew.setFieldValue('location', locationid);

                        // remove all items from SO to add only the right ones
                        var nbrItems = recSONew.getLineItemCount('item');
                        for (var p = 0; p < nbrItems; p++){
                            recSONew.removeLineItem('item',1);
                        }

                        // inherit proposal number with SO prefix and one letter sufix
                        var so = 'SO' + proposal + '-' + intChar.toString();
                        recSONew.setFieldValue('tranid', so);

                        for ( var q = 0; searchItemsNRCs != null && q < searchItemsNRCs.length; q++ ) {
                            var searchItemsNRC = searchItemsNRCs[q];

                            var itemRate = parseFloat(searchItemsNRC.getValue('rate',null,null));
                            var itemQty = searchItemsNRC.getValue('quantity',  null, null);
                            var bs = searchItemsNRC.getText('billingschedule',  null, null);

                            if (itemRate == null || itemRate == '' || itemRate == 'NaN'){
                                itemRate = 0;
                            }
                            if(subsidiary == 6 && currency == 1){ // Canada subsidiary and $US - use itemCalcRate instead of itemRate
                                itemRate = parseFloat(itemRate) / parseFloat(exchangerate);
                            }

                            recSONew.selectNewLineItem('item');
                            recSONew.setCurrentLineItemValue('item','item', searchItemsNRC.getValue('item',  null, null));
                            recSONew.setCurrentLineItemValue('item','billingschedule', searchItemsNRC.getValue('billingschedule',  null, null));
                            recSONew.setCurrentLineItemValue('item','quantity', searchItemsNRC.getValue('quantity',  null, null));
                            recSONew.setCurrentLineItemValue('item','custcol_clgx_qty2print', searchItemsNRC.getValue('quantity',  null, null));
                            recSONew.setCurrentLineItemValue('item','rate', itemRate);
                            recSONew.setCurrentLineItemValue('item','amount', parseFloat(itemRate) * parseFloat(itemQty));
                            recSONew.setCurrentLineItemValue('item','description', searchItemsNRC.getValue('memo',  null, null));
                            recSONew.setCurrentLineItemValue('item','price', searchItemsNRC.getValue('pricelevel',  null, null));
                            recSONew.setCurrentLineItemValue('item','taxcode', searchItemsNRC.getValue('taxcode',  null, null));
                            recSONew.setCurrentLineItemValue('item','options', searchItemsNRC.getValue('options',  null, null));
                            //recSONew.setCurrentLineItemValue('item','costestimatetype', searchItemsNRC.getValue('costestimatetype',  null, null));
                            recSONew.setCurrentLineItemValue('item','location', searchItemsNRC.getValue('location',  null, null));
                            recSONew.setCurrentLineItemValue('item','class', searchItemsNRC.getValue('class',  null, null));
                            recSONew.setCurrentLineItemValue('item','custcol_cologix_oppty_item_category', searchItemsNRC.getValue('custcol_cologix_invoice_item_category',  null, null));
                            recSONew.commitLineItem('item');
                            totalNRC += parseFloat(parseFloat(itemRate) * parseFloat(itemQty));
                        }
                        intChar += 1;
                        recSONew.setFieldValue('custbody_clgx_total_recurring', 0);
                        recSONew.setFieldValue('custbody_clgx_total_recurring_month', 0);
                        recSONew.setFieldValue('custbody_clgx_total_non_recurring', totalNRC);

                        recSONew.setFieldValue('custbody_clgx_consolidate_locations', clgx_return_consolidate_location (locationid));

                        var soid = nlapiSubmitRecord(recSONew, false, true);

                        var totals = clgx_transaction_totals (customerid, 'so', soid);

                        // only if a renewal, update back the opportunity and the proposal  with the last created SO
                        if(arrRenewSOs != null){
                            nlapiSubmitField('estimate', proposalid, 'custbody_clgx_so_renewed_on_so', soid);
                            if(opptyid != null && opptyid != ''){
                                //nlapiSubmitField('opportunity', opptyid, 'custbody_clgx_so_renewed_on_so', soid);
        						var recOpportunity = nlapiLoadRecord('opportunity', opptyid);
        						recOpportunity.setFieldValue('custbody_clgx_so_renewed_on_so', soid);
        						nlapiSubmitRecord(recOpportunity, false, true);
                            }
                        }

                        var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                        emailAdminBody += '<tr><td>' + location + '</td><td>' + bs + '</td><td><a href="https://1337135.app.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soid + '">' + so + '</a></td><td>' + startExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + execMinutes + '</td><td>'+ usageConsumtion + '</td></tr>';
                        emailUserBody += '<tr><td>' + location + '</td><td>' + bs + '</td><td><a href="https://1337135.app.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soid + '">' + so + '</a></td></tr>';
                    }
                    else{} // No NRCs and no MRCs
                }
                emailUserBody += '</table>';
                nlapiSendEmail(userid,userid,emailUserSubject,emailUserBody,null,null,null,null,true);
            }
            else{ // SOs exist for this proposal - add them to the SOs array

                for ( var r = 0; searchSOs != null && r < searchSOs.length; r++ ) {
                    var searchSO = searchSOs[r];
                    var soid = searchSO.getValue('internalid',null,null);
                    arrNewSOs.push(soid);
                }
            }

            // mark proposal as processed
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('internalid',null,null));
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_proposals_to_sos_propid",null,"anyof",proposalid));
            var searchProposals = nlapiSearchRecord('customrecord_clgx_proposals_to_sos', null, arrFilters, arrColumns);

            for ( var s = 0; searchProposals != null && s < searchProposals.length; s++ ) {
                var searchProposal = searchProposals[s];
                var recid = searchProposal.getValue('internalid',null,null);
                nlapiSubmitField('customrecord_clgx_proposals_to_sos', recid, 'custrecord_clgx_proposals_to_sos_done', 'T');
                //nlapiDeleteRecord('customrecord_clgx_proposal_to_create_sos', recid );
            }

        }
        emailAdminBody += '</table>';

        emailAdminBody += '<h2>Services created for the above service order(s)</h2>';
        emailAdminBody += '<table border="1" cellpadding="5">';
        emailAdminBody += '<tr><td>New</td><td>Service Order</td><td>Service</td><td>Power</td><td>Execution Time</td><td>Minute</td><td>Usage</td></tr>';

        var newSOsLength = arrNewSOs.length; // mark how many are new SOs

        // pull out existing ones with no service
        var arrColumns = new Array();
        var arrFilters = new Array();
        var searchExistingSOs = nlapiSearchRecord('salesorder', 'customsearch_clgx_sched_so', arrFilters, arrColumns);
        for ( var w = 0; searchExistingSOs != null && w < searchExistingSOs.length; w++ ) {
        	
        	var soid = searchExistingSOs[w].getValue('internalid',null,'GROUP');
        	nlapiLogExecution('DEBUG','SOs with no Service', '| soid: ' + soid + ' |');
        	
            arrNewSOs.push(soid);
        }

        for (var i = 0; i < arrNewSOs.length; i++){ // loop new created SOs and create services	for recurring items

            var startExecTime = moment();
            var totalMinutes = (startExecTime.diff(startScript)/60000).toFixed(1);

            if ( context.getRemainingUsage() <= 1000 || totalMinutes > 50 || i > 990 ){

                //var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                if ( status == 'QUEUED' ) {
                    nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                    break;
                }
            }

            nlapiLogExecution('DEBUG', 'Create Services for ', '| SOID = ' + arrNewSOs[i]  + ' |');

            var newSO = 'New';
            if (i > newSOsLength){
                newSO = 'Edit';
            }

            var startExec = moment();
            var execMinutes = (startExec.diff(startScript)/60000).toFixed(1);

            nlapiLogExecution('DEBUG','SO no Services', '| soid: ' + arrNewSOs[i] + ' |');
            
            var recSO = nlapiLoadRecord('salesorder', arrNewSOs[i]);
            var sonbr = recSO.getFieldValue('tranid');
            var customer = recSO.getFieldValue('entity');
            var subsidiary = recSO.getFieldValue('subsidiary');
            var location = recSO.getFieldValue('location');

            var totals = clgx_transaction_totals (customer, 'so', arrNewSOs[i]);

            var nbrItems = recSO.getLineItemCount('item');
            for (var j = 0; j < nbrItems; j++){

                var itemID = recSO.getLineItemValue('item', 'item', j + 1);
                var itemName = recSO.getLineItemText('item', 'item', j + 1);
                var itemCategory = recSO.getLineItemText('item', 'custcol_cologix_invoice_item_category', j + 1);
                var itemServiceID = recSO.getLineItemValue('item', 'custcol_clgx_so_col_service_id', j + 1);
                var itemBillSched = recSO.getLineItemText('item', 'billingschedule', j + 1);
                var itemFacility = parseInt(clgx_return_facilityid(location));
                var itemClass = recSO.getLineItemText('item', 'class', j + 1);
                var qty = recSO.getLineItemValue('item', 'custcol_clgx_qty2print', j + 1);

                //if (itemClass.indexOf("Recurring") > -1 && itemBillSched != null && itemBillSched != '' && itemBillSched != 'Non Recurring' && (itemServiceID == null || itemServiceID == '' || itemServiceID == -1) && itemID != 302 && itemID != 518 && itemID != 529) {
                if (itemClass.indexOf("Recurring") > -1 && itemBillSched != null && itemBillSched != '' && itemBillSched != 'Non Recurring' && (itemServiceID == null || itemServiceID == '' || itemServiceID == -1)) {
                	
                    // Get a new ID for the Service to create it
                    var objNewId = nlapiLoadRecord('customrecord_clgx_auto_generated_numbers', 1);
                    var stLastId = objNewId.getFieldValue('custrecord_clgx_auto_generated_number');
                    var intNextId = parseInt(stLastId) + 1;
                    objNewId.setFieldValue('custrecord_clgx_auto_generated_number', parseInt(intNextId));
                    nlapiSubmitRecord(objNewId,false,true);

                    var stId = new String(intNextId);
                    var stPrefix = 'S00000000';
                    var stNextId = stPrefix.substring(0,9-parseInt(stId.length)) + stId;

                    // create service
                    var record = nlapiCreateRecord('job');
                    record.setFieldValue('entityid', stNextId);
                    record.setFieldValue('custentity_cologix_service_order', arrNewSOs[i]);
                    record.setFieldValue('parent', customer);
                    record.setFieldValue('subsidiary', subsidiary);
                    record.setFieldValue('custentity_cologix_facility', itemFacility);
                    record.setFieldText('custentity_cologix_int_prj', 'No');
                    var idRec = nlapiSubmitRecord(record, false, true);

                    var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                    emailAdminBody += '<tr><td>' + newSO + '</td><td><a href="https://1337135.app.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soid + '">' + sonbr + '</a></td><td><a href="https://1337135.app.netsuite.com/app/common/entity/custjob.nl?id=' + idRec + '">' + stNextId + '</a></td><td></td><td>' + startExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + execMinutes + '</td><td>' + usageConsumtion + '</td></tr>';
                    nlapiLogExecution('DEBUG', 'Value', '| SO = ' + sonbr + ' | Service = ' + stNextId + ' | Usage = '+ usageConsumtion + ' | Minutes = ' + execMinutes + ' |');

                    // create power records if category is "Power"
                    if(itemCategory == 'Power' && itemID !=  546 && itemID !=  381 && itemID !=  575 && itemID != 780){

                        var arrColumns = new Array();
                        arrColumns.push(new nlobjSearchColumn('custitem_clgx_item_power_amps',null,null));
                        arrColumns.push(new nlobjSearchColumn('custitem_clgx_item_power_volts',null,null));
                        var arrFilters = new Array();
                        arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",itemID));
                        var searchItem = nlapiSearchRecord('item', null, arrFilters, arrColumns);
                        if(searchItem != null){
                            var amps = searchItem[0].getValue('custitem_clgx_item_power_amps',null,null);
                            var volts = searchItem[0].getValue('custitem_clgx_item_power_volts',null,null);
                        }
                        else{
                            var amps = '';
                            var volts = '';
                        }

                        nlapiLogExecution('DEBUG','Ampms/Volts ', '| Amps = ' + amps + ' | Volts = ' + volts + ' |');

                        if(itemName.indexOf('A+B') != -1){ // create 2 power records if item name contains "A+B"
                            for (var k = 0; k < qty; k++){
                                // create power circuit A
                                var record = nlapiCreateRecord('customrecord_clgx_power_circuit');
                                record.setFieldValue('custrecord_cologix_power_service', idRec);
                                record.setFieldValue('custrecord_cologix_power_facility', itemFacility);
                                record.setFieldValue('custrecord_cologix_power_amps', amps);
                                record.setFieldValue('custrecord_cologix_power_volts', volts);
                                record.setFieldValue('custrecord_power_circuit_service_order', arrNewSOs[i]);
                                var idPowerA = nlapiSubmitRecord(record, false, true);

                                var powerName = nlapiLookupField('customrecord_clgx_power_circuit', idPowerA, 'name');
                                //nlapiSubmitField('customrecord_clgx_power_circuit', idPowerA, 'name', clgx_set_char_at(powerName,12,'A'));
                    			var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', idPowerA);
                    			recPower.setFieldValue('name', clgx_set_char_at(powerName,12,'A'));
                    			nlapiSubmitRecord(recPower, false, true);

                                emailAdminBody += '<tr><td></td><td></td><td><a href="https://1337135.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=17&id=' + idPowerA + '">' + powerName + ' / A / ' + idPowerA + '</a></td><td></td><td></td><td></td></tr>';
                                //emailUserBody += '<tr><td></td><td></td><td><a href="https://1337135.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=17&id=' + idPowerA + '">' + powerName + ' / A / ' + idPowerA + '</a></td></tr>';
                                nlapiLogExecution('DEBUG','Created Power ', powerName + ' / A / ' + idPowerA);

                                // create power circuit B
                                var record = nlapiCreateRecord('customrecord_clgx_power_circuit');
                                record.setFieldValue('custrecord_cologix_power_service', idRec);
                                record.setFieldValue('custrecord_cologix_power_facility', itemFacility);
                                record.setFieldValue('custrecord_cologix_power_amps', amps);
                                record.setFieldValue('custrecord_cologix_power_volts', volts);
                                record.setFieldValue('custrecord_power_circuit_service_order', arrNewSOs[i]);
                                var idPowerB = nlapiSubmitRecord(record, false, true);

                                //var powerName = nlapiLookupField('customrecord_clgx_power_circuit', idPower, 'name');
                                //nlapiSubmitField('customrecord_clgx_power_circuit', idPowerB, 'name', clgx_set_char_at(powerName,12,'B'));
                    			var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', idPowerB);
                    			recPower.setFieldValue('name', clgx_set_char_at(powerName,12,'B'));
                    			nlapiSubmitRecord(recPower, false, true);

                                // update pair power on each power
                                //nlapiSubmitField('customrecord_clgx_power_circuit', idPowerA, 'custrecord_clgx_dcim_pair_power', idPowerB);
                                //nlapiSubmitField('customrecord_clgx_power_circuit', idPowerB, 'custrecord_clgx_dcim_pair_power', idPowerA);
                                
                    			var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', idPowerA);
                    			recPower.setFieldValue('custrecord_clgx_dcim_pair_power', idPowerB);
                    			nlapiSubmitRecord(recPower, false, true);
                    			
                    			var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', idPowerB);
                    			recPower.setFieldValue('custrecord_clgx_dcim_pair_power', idPowerA);
                    			nlapiSubmitRecord(recPower, false, true);

                    			
                                emailAdminBody += '<tr><td></td><td></td><td><a href="https://1337135.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=17&id=' + idPowerB + '">' + powerName + ' / B / ' + idPowerB + '</a></td><td></td><td></td><td></td></tr>';
                                //emailUserBody += '<tr><td></td><td></td><td><a href="https://1337135.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=17&id=' + idPowerB + '">' + powerName + ' / B / ' + idPowerB + '</a></td></tr>';
                                nlapiLogExecution('DEBUG','Created Power ', powerName + ' / B / ' + idPowerB);
                            }
                        }
                        else if (itemName.indexOf('A+B') == -1 && itemName.indexOf('Kilowatt') == -1){
                            for (var k = 0; k < qty; k++){ // create 1 power record
                                // create power circuit P
                                var record = nlapiCreateRecord('customrecord_clgx_power_circuit');
                                record.setFieldValue('custrecord_cologix_power_service', idRec);
                                record.setFieldValue('custrecord_cologix_power_facility', itemFacility);
                                record.setFieldValue('custrecord_cologix_power_amps', amps);
                                record.setFieldValue('custrecord_cologix_power_volts', volts);
                                record.setFieldValue('custrecord_power_circuit_service_order', arrNewSOs[i]);
                                var idPowerP = nlapiSubmitRecord(record, false, true);

                                var powerName = nlapiLookupField('customrecord_clgx_power_circuit', idPowerP, 'name');
                                //nlapiSubmitField('customrecord_clgx_power_circuit', idPowerP, 'name', clgx_set_char_at(powerName,12,'P'));
                    			var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', idPowerP);
                    			recPower.setFieldValue('name', clgx_set_char_at(powerName,12,'P'));
                    			nlapiSubmitRecord(recPower, false, true);

                                emailAdminBody += '<tr><td></td><td></td><td><a href="https://1337135.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=17&id=' + idPowerP + '">' + powerName + ' / P / ' + idPowerP + '</a></td><td></td><td></td><td></td></tr>';
                                //emailUserBody += '<tr><td></td><td></td><td><a href="https://1337135.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=17&id=' + idPowerP + '">' + powerName + ' / P / ' + idPowerP + '</a></td></tr>';
                                nlapiLogExecution('DEBUG','Created Power ', powerName + ' / P / ' + idPowerP);

                            }
                        }
                        else{var x = 0;}
                    }
                    
                    // link the item to the new service
                    recSO.setLineItemValue('item', 'custcol_clgx_so_col_service_id', j + 1, idRec);
                }
                else{}

            }
            nlapiLogExecution('DEBUG','saleseffectivedate ', moment().format('M/D/YYYY') );
            var soid = nlapiSubmitRecord(recSO, false, true);
        }

        emailAdminBody += '</table>';

//------------- Begin Section 2 -----------------------------------------------------------------------------------
// Created:	01/20/2014
// Details:	Update services and inventory on services if any service did change on the SO item line
//-----------------------------------------------------------------------------------------------------------------

        emailAdminBody += '<h2>Updated Services and Inventory</h2>';
        emailAdminBody += '<table border="1" cellpadding="5">';
        emailAdminBody += '<tr><td>SO</td><td>Service</td><td>Execution Time</td><td>Minute</td><td>Usage</td></tr>';

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null).setSort(true));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_services_to_update_serv',null,null));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_services_to_update_so',null,null));
        var arrFilters = new Array();
        var searchResults = nlapiSearchRecord('customrecord_clgx_services_to_update', null, arrFilters, arrColumns);

        for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {

            var startExec = moment();
            var execMinutes = (startExec.diff(startScript)/60000).toFixed(1);
            if ( (context.getRemainingUsage() <= 1000 || execMinutes > 50) && (i+1) < searchResults.length ){
                //var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                /*if ( status == 'QUEUED' ) {
                    nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
                    break;
                }*/
            }

            var searchResult = searchResults[i];
            var internalid = searchResult.getValue('internalid',null,null);
            var servid = searchResult.getValue('custrecord_clgx_services_to_update_serv',null,null);
            var serv = searchResult.getText('custrecord_clgx_services_to_update_serv',null,null);
            var soid = searchResult.getValue('custrecord_clgx_services_to_update_so',null,null);
            var so = searchResult.getText('custrecord_clgx_services_to_update_so',null,null);

            
            nlapiLogExecution('DEBUG','debug', '| soid = ' + soid + ' | so = ' + so + ' | servid = ' + servid + ' | serv = ' + serv + ' | ');

            
            nlapiSubmitField('job', servid, 'custentity_cologix_service_order', soid);

            var recService = nlapiLoadRecord('job', servid, null, null);

            var nbrXcs = recService.getLineItemCount('recmachcustrecord_cologix_xc_service');
            for ( var j = 0; j < nbrXcs; j++ ) {
                var id = recService.getLineItemValue('recmachcustrecord_cologix_xc_service', 'id', j + 1);
                nlapiSubmitField('customrecord_cologix_crossconnect', id, 'custrecord_xconnect_service_order', soid);
            }
            var nbrVxcs = recService.getLineItemCount('recmachcustrecord_cologix_vxc_service');
            for ( var j = 0; j < nbrVxcs; j++ ) {
                var id = recService.getLineItemValue('recmachcustrecord_cologix_vxc_service', 'id', j + 1);
                nlapiSubmitField('customrecord_cologix_vxc', id, 'custrecord_cologix_service_order', soid);
            }
            var nbrSpaces = recService.getLineItemCount('recmachcustrecord_cologix_space_project');
            for ( var j = 0; j < nbrSpaces; j++ ) {
                var id = recService.getLineItemValue('recmachcustrecord_cologix_space_project', 'id', j + 1);
                nlapiSubmitField('customrecord_cologix_space', id, 'custrecord_space_service_order', soid);
            }
            var nbrPowers = recService.getLineItemCount('recmachcustrecord_cologix_power_service');
            for ( var j = 0; j < nbrPowers; j++ ) {
                var id = recService.getLineItemValue('recmachcustrecord_cologix_power_service', 'id', j + 1);
                //nlapiSubmitField('customrecord_clgx_power_circuit', id, 'custrecord_power_circuit_service_order', soid);
    			var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', id);
    			recPower.setFieldValue('custrecord_power_circuit_service_order', soid);
    			nlapiSubmitRecord(recPower, false, true);
            }
            var nbrOobs = recService.getLineItemCount('recmachcustrecord_clgx_oob_service_id');
            for ( var j = 0; j < nbrOobs; j++ ) {
                var id = recService.getLineItemValue('recmachcustrecord_clgx_oob_service_id', 'id', j + 1);
                nlapiSubmitField('customrecord_clgx_oob_facility', id, 'custrecord_clgx_oob_service_order', soid);
            }

            nlapiDeleteRecord('customrecord_clgx_services_to_update', internalid);

            var index = i + 1;
            var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
            nlapiLogExecution('DEBUG', 'Value', '| SO = ' + soid + ' | Service = ' + servid + ' | Index = ' + index + ' of ' + searchResults.length +  ' | Usage = '+ usageConsumtion + ' | Minutes = ' + execMinutes + ' |');
            emailAdminBody += '<tr><td><a href="https://1337135.app.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soid + '">' + so + '</a></td><td><a href="https://1337135.app.netsuite.com/app/common/entity/custjob.nl?id=' + servid + '">' + serv + '</a></td><td>' + startExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + execMinutes + '</td><td>'+ usageConsumtion + '</td></tr>';
        }
        emailAdminBody += '</table>';
        
//------------- Begin Section 3 -----------------------------------------------------------------------------------
// Details:	Update renewals dates and cycle for expiring SOs.
// Created:	1/3/2014
//-----------------------------------------------------------------------------------------------------------------
        emailAdminBody += '<h2>Update renewal dates for expiring SOs</h2>';
        emailAdminBody += '<table border="1" cellpadding="5">';
        emailAdminBody += '<tr><td>SO</td><td>Terms (Months)</td><td>Contract Dates</td><td>Renew Dates</td><td>Cycle</td><td>Execution Time</td><td>Minute</td><td>Usage</td></tr>';

        // get list of SOs containing inactive items or services, to exclude it from processing
        //var searchInactives = nlapiSearchRecord('transaction', 'customsearch_clgx_sos_with_inactives', null, null);
        //var arrSOs = new Array();
        //for ( var i = 0; searchInactives != null && i < searchInactives.length; i++ ) {
        //	arrSOs.push(searchInactives[i].getValue('internalid', null, 'GROUP')); // build an array with internal ids of those SOs
        //}

        var arrColumns = new Array();
        var arrFilters = new Array();
        //if(arrSOs.length > 0){
        //arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",arrSOs));
        //}
        var searchResults = nlapiSearchRecord('transaction', 'customsearch_clgx_sched_sos_renewals', arrFilters, arrColumns);

        var startScript = moment(); // time when script started

        for ( var j = 0; searchResults != null && j < searchResults.length; j++ ) {

            var startExec = moment(); // time when each loop is starting
            var execMinutes = (startExec.diff(startScript)/60000).toFixed(1); // execution time in minutes
            if ( (context.getRemainingUsage() <= 1000 || execMinutes > 50) && (j+1) < searchResults.length ){ // if 1000 points left or more then 50 min execution time, re-schedule the script
                //var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                /*if ( status == 'QUEUED' ) {
                    nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
                    break;
                }*/
            	break;
            }

            var searchResult = searchResults[j];
            var soid = searchResult.getValue('internalid',null,'GROUP');
            var so = searchResult.getValue('tranid',null,'GROUP');

            // log loop / SO
            var index = j + 1;
            var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage()); // how many points the script consumed
            nlapiLogExecution('DEBUG', 'Value', '| SO = ' + so + ' | ID = ' + soid + ' | Index = ' + index + ' of ' + searchResults.length +  ' | Usage = '+ usageConsumtion + ' | Minutes = ' + execMinutes + ' |');

            // length in month of the first cycle/contract
            var billingTerms = searchResult.getValue('custbody_cologix_biling_terms',null,'GROUP');
            if (billingTerms != 'MTM'){
                var firstCycleLength = parseInt(billingTerms);
            }
            else{
                var firstCycleLength = 1;
            }
            // length in month of the renewed cycle/contract
            var renewTerms = searchResult.getText('custentity_clgx_cust_renewal_term','customerMain','GROUP');
            if (renewTerms == 'Original Contract Term'){
                var renewCycleLength = firstCycleLength;
            }
            else{
                var renewCycleLength = parseInt(renewTerms.substring(0, 3));
            }

            var contractStartDate = searchResult.getValue('custbody_cologix_so_contract_start_dat',null,'GROUP');
            var contractEndDate = searchResult.getValue('enddate',null,'GROUP');

            var renewStartDate = searchResult.getValue('custbody_clgx_renewal_start_date',null,'GROUP');
            var currentEndDate = searchResult.getValue('custbody_clgx_renewal_end_date',null,'GROUP');
            var currentCycle = parseInt(searchResult.getValue('custbody_clgx_contract_cycle',null,'GROUP'));

            var todayMom = moment();
            if(currentEndDate == null || currentEndDate == ''){ // current end date has not been populated
                var contractEndDateMom = new moment(nlapiStringToDate(contractEndDate));
                if (todayMom.isAfter(contractEndDateMom, 'day')){ // we did pass the first cycle, even if current end date has not been populated
                    currentCycle = 2;
                    var addMonths = parseInt(renewCycleLength) ; // add months of both first and renew cycles
                    contractEndDateMom.add('months', addMonths);

                    // verify more than the first cycle ???
                    /*
                     if (todayMom.isAfter(contractEndDateMom, 'day')){ // we did pass the second cycle
                     currentCycle += 1;
                     contractEndDateMom.add('months', parseInt(renewCycleLength)); // add months of renew cycle
                     if (todayMom.isAfter(contractEndDateMom, 'day')){ // we did pass the third cycle
                     currentCycle += 1;
                     contractEndDateMom.add('months', parseInt(renewCycleLength)); // add months of renew cycle
                     if (todayMom.isAfter(contractEndDateMom, 'day')){ // we did pass the forth cycle
                     currentCycle += 1;
                     contractEndDateMom.add('months', parseInt(renewCycleLength)); // add months of renew cycle
                     if (todayMom.isAfter(contractEndDateMom, 'day')){ // we did pass the fith cycle
                     currentCycle += 1;
                     contractEndDateMom.add('months', parseInt(renewCycleLength)); // add months of renew cycle
                     }
                     }
                     }
                     }
                     */

                    currentEndDate = contractEndDateMom.format('M/D/YYYY');
                    renewStartDate = contractEndDateMom.add('days', 1).format('M/D/YYYY');
                }
                else{ // we're still inside the first cycle
                    currentCycle = 1;
                    //contractEndDateMom.add('months', parseInt(firstCycleLength));
                    currentEndDate = contractEndDateMom.format('M/D/YYYY');
                    renewStartDate = contractEndDateMom.add('days', 1).format('M/D/YYYY');
                }
            }
            else{ // current end date is present, most probably this is a renewal
                var currentEndDateMom = new moment(nlapiStringToDate(currentEndDate));
                currentCycle += 1; // if date is present, cycle myust be too, so increase with 1
                currentEndDateMom.add('months', parseInt(renewCycleLength));
                currentEndDate = currentEndDateMom.format('M/D/YYYY');
                renewStartDate = currentEndDateMom.add('days', 1).format('M/D/YYYY');
            }

            var fields = ['custbody_clgx_renewal_start_date','custbody_clgx_renewal_end_date','custbody_clgx_contract_cycle'];
            var values = [renewStartDate, currentEndDate, currentCycle];
            nlapiSubmitField('salesorder', soid, fields, values);
            emailAdminBody += '<tr><td><a href="https://1337135.app.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soid + '">' + so + '</a><td> Contract : ' + firstCycleLength + '<br> Renew : ' + renewCycleLength + '</td><td> Start : ' + contractStartDate + '<br> End: ' + contractEndDate + '</td><td> Start : ' + renewStartDate + '<br> End : ' + currentEndDate + '</td><td>' + currentCycle + '</td><td>' + startExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + execMinutes + '</td><td>'+ usageConsumtion + '</td></tr>';
        }
        emailAdminBody += '</table>';

//-------------------------- End Sections ----------------------------------------------------------------------------

        var endScript = moment();
        emailAdminBody += '<br>';
        emailAdminBody += 'End : ' + endScript.format('M/D/YYYY h:mm:ss a') + '<br>';
        emailAdminBody += 'Total execution minutes : ' + (endScript.diff(startScript)/60000).toFixed(1) + '<br>';
        var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
        emailAdminBody += 'Total usage : ' + usageConsumtion;
        emailAdminBody += '<br><br>';

        clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_so_admin", emailAdminSubject, emailAdminBody);
        /*nlapiSendEmail(432742,71418,emailAdminSubject,emailAdminBody,null,null,null,null,true);
        nlapiSendEmail(432742,206211,emailAdminSubject,emailAdminBody,null,null,null,null,true);
        nlapiSendEmail(432742,2406,emailAdminSubject,emailAdminBody,null,null,null,null,true);
        nlapiSendEmail(432742,1349020,emailAdminSubject,emailAdminBody,null,null,null,null,true);*/



        nlapiLogExecution('DEBUG','Total Remaining Usage', nlapiGetContext().getRemainingUsage());
        nlapiLogExecution('DEBUG','Finished Execution', '|-------------------------- Finished Scheduled Script --------------------------|');
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

