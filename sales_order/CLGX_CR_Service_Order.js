nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_Service_Order.js
//	Script Name:	CLGX_CR_Service_Order
//	Script Id:		customscript_clgx_cr_service_order
//	Script Runs:	On Client
//	Script Type:	Client Script
//	Deployments:	Service Order - Sandbox | Production
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		1/20/2012
//	Updated:		1/20/2012
//-------------------------------------------------------------------------------------------------

function pageInit(type){
    try {
        nlapiLogExecution('DEBUG','Client/Record - PageInit','--------------------STARTED---------------------|');
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 1/10/2012
// Details:	Displays Contract Terms fields disabled or not depending on role. Also, disable 'consolidate separate' fields except for admins
//-------------------------------------------------------------------------------------------------

        var currentContext = nlapiGetContext();
        stRole = currentContext.getRole();

        if (stRole == '3' || stRole == '-5' || stRole == '18' || stRole == '1011' || stRole == '1018' || stRole == '1039' || stRole == '1040' || stRole == '1017'){ // if role is not Admin or full or A/R Clerk or sales person
            nlapiDisableField('custbody_cologix_biling_terms', false);
        }
        else{
            nlapiDisableField('custbody_cologix_biling_terms', true);
        }

        if (stRole != '3' && stRole != '-5' && stRole != '18' && stRole != '1011' && stRole != '1021'){ // if role is not Admin or full or A/R Clerk or Accounting Manager
            nlapiDisableField('custbody_clgx_consolidate_separate', true);
            nlapiDisableField('custbody_clgx_consolidate_separate_id', true);
            nlapiDisableField('custbody_clgx_consolidate_locations', true);
        }

        if (stRole == '1018' || stRole == '1019'|| stRole == '1040'){ // if role is  sales person or Sales VP
            nlapiDisableLineItemField('item', 'item', true);
            nlapiDisableLineItemField('item', 'rate', true);
            nlapiDisableLineItemField('item', 'description', true);
            nlapiDisableLineItemField('item', 'price', true);
            nlapiDisableLineItemField('item', 'amount', true);
            nlapiDisableLineItemField('item', 'taxcode', true);
            nlapiDisableLineItemField('item', 'options', true);
            nlapiDisableLineItemField('item', 'quantity', true);
            nlapiDisableLineItemField('item', 'altsalesamt', true);
            nlapiDisableLineItemField('item', 'rate', true);
            nlapiDisableLineItemField('item', 'custcol_cologix_invoice_item_category', true);
            nlapiDisableLineItemField('item', 'custcol_clgx_qty2print', true);
            nlapiDisableLineItemField('item', 'custcol_cologix_rateperday', true);
            nlapiDisableLineItemField('item', 'custcol_far_trn_relatedasset', true);
            nlapiDisableLineItemField('item', 'custcol_cologix_unevendaystobil', true);
            nlapiDisableLineItemField('item', 'billingschedule', true);
            nlapiDisableLineItemField('item', 'location', true);
            nlapiDisableLineItemField('item', 'isclosed', true);
        }

//---------- End Section 1 ------------------------------------------------------------------------------------------------


        nlapiLogExecution('DEBUG','Client/Record - PageInit','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function lineInit(type){
    try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 1/20/2012
// Details:	Disable or not fields 'class' and 'quantity' on 'item' sublist depending on role.
//-----------------------------------------------------------------------------------------------------------------

        if (type=='item'){
            var currentContext = nlapiGetContext();
            stRole = currentContext.getRole();
            if (stRole != '3' && stRole != '-5' && stRole != '18' && stRole != '1011'){ // if role is not Admin or full or A/R Clerk
                nlapiDisableLineItemField('item', 'class', true);
                nlapiDisableLineItemField('item', 'quantity', true);
            }

            if (stRole == '1018' || stRole == '1019' || stRole == '1040'){ // if role is  sales person or Sales VP
                nlapiDisableLineItemField('item', 'item', true);
                nlapiDisableLineItemField('item', 'rate', true);
                nlapiDisableLineItemField('item', 'description', true);
                nlapiDisableLineItemField('item', 'price', true);
                nlapiDisableLineItemField('item', 'amount', true);
                nlapiDisableLineItemField('item', 'taxcode', true);
                nlapiDisableLineItemField('item', 'options', true);
                nlapiDisableLineItemField('item', 'quantity', true);
                nlapiDisableLineItemField('item', 'altsalesamt', true);
                nlapiDisableLineItemField('item', 'rate', true);
                nlapiDisableLineItemField('item', 'custcol_cologix_invoice_item_category', true);
                nlapiDisableLineItemField('item', 'custcol_clgx_qty2print', true);
                nlapiDisableLineItemField('item', 'custcol_cologix_rateperday', true);
                nlapiDisableLineItemField('item', 'custcol_far_trn_relatedasset', true);
                nlapiDisableLineItemField('item', 'custcol_cologix_unevendaystobil', true);
                nlapiDisableLineItemField('item', 'billingschedule', true);
                nlapiDisableLineItemField('item', 'location', true);
                nlapiDisableLineItemField('item', 'isclosed', true);
            }
        }

//---------- End Section 1 ------------------------------------------------------------------------------------------------

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function fieldChanged(type, name, linenum){
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 6/18/2012
// Details:	Add or remove consolidate separately ID.
//-------------------------------------------------------------------------------------------------
    try {
        if (name == 'custbody_clgx_so_incremental_mrc') {
            var incremental = nlapiGetFieldValue('custbody_clgx_so_incremental_mrc');
            if(parseFloat(incremental)<parseFloat(0))
            {
                alert('Incremental MRC must be greater than zero!');
                return false;
            }
        }
        if (name=='custbody_clgx_consolidate_separate'){
            var separate = nlapiGetFieldValue('custbody_clgx_consolidate_separate');
            var separateID = nlapiGetFieldValue('custbody_clgx_consolidate_separate_id');
            if(separate == 'T'){
                nlapiSetFieldValue('custbody_clgx_consolidate_separate_id', generateSID());
                nlapiDisableField('custbody_clgx_consolidate_separate', true);
            }
            else{
                if(separateID != '0' && separateID != ''){
                    nlapiSetFieldValue('custbody_clgx_consolidate_separate_id', '0');
                }
            }
        }

        if (name=='custbody_clgx_consolidate_separate_id'){
            var separateID = nlapiGetFieldValue('custbody_clgx_consolidate_separate_id');
            if(separateID == '0' || separateID == ''){
                nlapiDisableField('custbody_clgx_consolidate_separate', false);
                nlapiSetFieldValue('custbody_clgx_consolidate_separate', 'F');
            }
            /*
             else{
             nlapiSetFieldValue('custbody_clgx_consolidate_separate', 'F');
             nlapiDisableField('custbody_clgx_consolidate_separate', true);
             }
             */
        }
    }

    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
function saveRecord(){
    try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 4/4/2013
// Details:	If Status is is Closed-Lost, Loss reason is mandatory
//-------------------------------------------------------------------------------------------------
        var allowSave = 1;
        var alertMsg = '';
        var currentContext = nlapiGetContext();
        var usrRole = currentContext.getRole();
        var itemsCount =nlapiGetLineItemCount('item');
        //alert(itemsCount);
        //Only if user has admin, full or AR Clerk roles can add discount items to Service Order
        // if ((usrRole != 1011)&&(itemsCount>0))
        if (itemsCount>0)
        {
            for ( var j = 1; j <= itemsCount; j++ ) {
                //  var itemId= nlapiGetLineItemValue('item', 'item', j);
                var itemType= nlapiGetLineItemValue('item', 'itemtype', j);
                var amount= nlapiGetLineItemValue('item', 'amount', j);
                if((itemType=='Discount')&&(usrRole != 1011 && usrRole != 3 && usrRole != 18 && usrRole!=5))
                {
                    alertMsg = 'Discount Item is not valid for this Service Order';
                    allowSave=0;
                }
                if((itemType!='Discount')&&(usrRole != 3 && usrRole != 18)&&(amount<0))
                {
                    alertMsg='Negative amounts are not permited';
                    allowSave=0;

                }
            }
        }

        if (allowSave == 0) {
            alert(alertMsg);
            return false;
        }
        else{
            return true;
        }
//-------------------------------------------------------------------------------------------------

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function generateSID() {
    var s = [], itoh = '0123456789ABCDEF';
    for (var i = 0; i <36; i++) s[i] = Math.floor(Math.random()*0x10);
    s[14] = 4;
    s[19] = (s[19] & 0x3) | 0x8;
    for (var i = 0; i <36; i++) s[i] = itoh[s[i]];
    s[8] = s[13] = s[18] = s[23] = '-';
    return s.join('');
}