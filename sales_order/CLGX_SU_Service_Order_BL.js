nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Service_Order_BL.js
//	Script Name:	CLGX_SU_Service_Order_BL
//	Script Id:		customscript_clgx_su_so_bl
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Sales Order - Sandbox | Production
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/14/2011
//	Include:		CLGX_Lib_Global.js, moment.min.js
//-------------------------------------------------------------------------------------------------

function beforeLoad (type, form) {
    try {
    	
    	var start = moment();
    	var body = '| ';
    	
        var userid = nlapiGetUser();
        var currentContext = nlapiGetContext();
        if (currentContext.getExecutionContext() == 'userinterface'){

            var  roleid = currentContext.getRole();
            var renewedFromSos=nlapiGetFieldValues('custbody_clgx_renewed_from_sos');
            //Incremental MRC field is editable by Product Manager Role
            if((roleid == -5 || roleid == 3 ||roleid==1017)&&((renewedFromSos=='')||(renewedFromSos==null)))
            {
                form.getField('custbody_clgx_so_incremental_mrc').setDisplayType('normal');

            }
            else{
                if(roleid == -5 || roleid == 3 ||roleid==1017||roleid==18)
                {
                    form.getField('custbody_clgx_so_incremental_mrc').setDisplayType('normal');

                }
            }
            var allow = 0;
            if (roleid == '-5' || roleid == '3' || roleid == '18' || roleid == '1088') {
                allow = 1;
            }
            if(userid == 71418){
            	allow = 0;
            }
            if(userid == 1349020){
            	allow = 0;
            }
            
            var closed = 0;
            if(closed == 1 && allow == 0){ // if module is closed and any other role then admin and full
                var arrParam = new Array();
                arrParam['custscript_internal_message'] = 'This module is closed for modifications. Sorry for the inconvenience. Please come back later.';
                nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
            }
            
            
            var proposalid = nlapiGetFieldValue('createdfrom');
            var arrParam = new Array();

 			
 			body += '01 - ' + moment().diff(start) + ' | ';

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Created:	3/1/2014
// Details:	Schedule a script that will create SOs and Services from the proposal
//-----------------------------------------------------------------------------------------------------------------
            if (type == 'create' && (proposalid == null || proposalid == '') && allow == 0) { // the SO is created directly, not from a proposal and not admin --------------------------------------------------------------------------------
                arrParam['custscript_internal_message'] = 'You can\'t create a service order directly. Please create an opportunity and a proposal first.';
                nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
            }

            if (type == 'create' && (proposalid != null && proposalid != '')) {  // the SO is created from a proposal --------------------------------------------------------------------------------------------
                var schedScript = 0;

// is this proposal in processing queue ----------------------------------------------------------------------------------------------------------------------------------------------

                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('internalid',null,null));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_proposals_to_sos_propid",null,"anyof",proposalid));
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_proposals_to_sos_done",null,"is",'F'));
                var searchQueuedProposal = nlapiSearchRecord('customrecord_clgx_proposals_to_sos', null, arrFilters, arrColumns);

                if(searchQueuedProposal != null){
                    arrParam['custscript_internal_message'] = 'This proposal is already in the processing queue. If you still see this message a day from now, please contact support.';
                    nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
                }
                body += '02 - ' + moment().diff(start) + ' | ';

// proposal has opened SOs -----------------------------------------------------------------------------------------------------------------------------------------------------------------

                var recProposal = nlapiLoadRecord('estimate', proposalid);
                var nbrSOs = recProposal.getLineItemCount('links');

                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
                arrColumns.push(new nlobjSearchColumn('status',null,'GROUP'));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter("createdfrom",null,"anyof",proposalid));
                //arrFilters.push(new nlobjSearchFilter("status",null,"isnot",'Cancelled'));
                var searchOpenSOs = nlapiSearchRecord('salesorder', null, arrFilters, arrColumns);

                for ( var i = 0; searchOpenSOs != null && i < searchOpenSOs.length; i++ ) {
                    var searchOpenSO = searchOpenSOs[i];
                    var statusSO = searchOpenSO.getText('status', null, 'GROUP');
                    
                    if(statusSO != 'Cancelled'){
                        arrParam['custscript_internal_message'] = 'This proposal already has opened service orders created from it.';
                        nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
                    }
                }
                body += '03 - ' + moment().diff(start) + ' | ';
                
// more than one pair location/BS and not a renewal -----------------------------------------------------------------------------------------------------------------------------------------

                var arrSOs = recProposal.getFieldValues('custbody_clgx_renewed_from_sos'); // is this a renewal?
                if(arrSOs != null){
                    var arrSOsLength = arrSOs.length;
                }
                else{
                    var arrSOsLength = 0;
                }
                // search locations and recurring billling schedules on proposal items
                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('location',null,'GROUP'));
                arrColumns.push(new nlobjSearchColumn('billingschedule',null,'GROUP'));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",proposalid));
                arrFilters.push(new nlobjSearchFilter("billingschedule",null,"noneof",17));
                arrFilters.push(new nlobjSearchFilter("billingschedule",null,"noneof",'@NONE@'));
                arrFilters.push(new nlobjSearchFilter("location",null,"noneof",'@NONE@'));
                arrFilters.push(new nlobjSearchFilter("type","item","is",'Service'));
                var searchBSs = nlapiSearchRecord('estimate', null, arrFilters, arrColumns);

                if(searchBSs != null){ // there are pairs location/BS of recurring  items
                    if(searchBSs.length > 1 && arrSOsLength == 0){ // more than one pair location/BS and not a renewal
                        arrParam['custscript_internal_message'] = 'You can not create a service order from a new proposal having multiple locations and/or billing schedules.';
                        nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
                    }
                }
                body += '04 - ' + moment().diff(start) + ' | ';
                

// check for recurring items with no BS -----------------------------------------------------------------------------------------------------------------------------------------

                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",proposalid));
                arrFilters.push(new nlobjSearchFilter("billingschedule",null,"anyof",'@NONE@'));
                arrFilters.push(new nlobjSearchFilter("class",null,"noneof",3));
                arrFilters.push(new nlobjSearchFilter("type","item","is",'Service')); // consider only Service type items
                var searchNoBSs = nlapiSearchRecord('estimate', null, arrFilters, arrColumns);

                if(searchNoBSs != null){
                    arrParam['custscript_internal_message'] = 'This proposal can not be processed because it contains at least one item with no billing schedule.';
                    nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
                }

                body += '05 - ' + moment().diff(start) + ' | ';
                
                
// check for items with no match between Class and Billing Schedule  -----------------------------------------------------------------------------------------------------------------------------------------

                var arrColumns = new Array();
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",proposalid));
                var searchSOsNoMatches = nlapiSearchRecord('transaction', 'customsearch_clgx_prop_no_match_class_bs', arrFilters, arrColumns);

                if(searchSOsNoMatches != null){
                    arrParam['custscript_internal_message'] = 'This proposal can not be processed because it contains at least one item with no match between class and billing schedule.';
                    nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
                }
                
                body += '06 - ' + moment().diff(start) + ' | ';
                
// all checkups are OK, no more redirects to messages, add proposal to custom processing queue and schedule the script -----------------------------------------------------------------------------------------------------------------------------------------

                // put proposal in processing proposal queue
                var record = nlapiCreateRecord('customrecord_clgx_proposals_to_sos');
                record.setFieldValue('custrecord_clgx_proposals_to_sos_propid', proposalid);
                record.setFieldValue('custrecord_clgx_proposals_to_sos_userid', userid);
                var idRec = nlapiSubmitRecord(record, false, true);

                arrParam['custscript_proposalid'] = proposalid;
                arrParam['custscript_userid'] = userid;
                var status = nlapiScheduleScript('customscript_clgx_ss_so', null ,arrParam);

                arrParam['custscript_internal_message'] = 'The proposal will be processed soon. You will receive an email confirmation when it is done.';
                nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
            }
            
            body += '07 - ' + moment().diff(start) + ' | ';
            
//------------- Begin Section ? -----------------------------------------------------------------------------------
// Created:	3/20/2014
// Details:	Deal with SO number when it's a copy.
//-----------------------------------------------------------------------------------------------------------------
            if (type == 'copy'){
                var oldSOid = request.getParameter('id');
                var recOldSO = nlapiLoadRecord('salesorder', oldSOid);
                var enddate = recOldSO.getFieldValue('enddate');
                var comterms = recOldSO.getFieldValue('custbody_clgx_cust_commerical_terms');

                nlapiSetFieldValue('enddate', enddate);
                nlapiSetFieldValue('custbody_clgx_cust_commerical_terms', comterms);

            }
            
            body += '08 - ' + moment().diff(start) + ' | ';
            
//------------- Begin Section 2 -----------------------------------------------------------------------------------
// Version:	1.0 - 12/14/2011
// Details:	Hide fields from the 'Totals' fields group.
//-----------------------------------------------------------------------------------------------------------------
            if (type == 'view' || type == 'edit') {
                if (nlapiGetField('subtotal') != null && nlapiGetField('subtotal') != ''){
                    form.getField('subtotal').setDisplayType('hidden');
                }
                if (nlapiGetField('discounttotal') != null && nlapiGetField('discounttotal') != ''){
                    form.getField('discounttotal').setDisplayType('hidden');
                }
                if (nlapiGetField('taxtotal') != null && nlapiGetField('taxtotal') != ''){
                    form.getField('taxtotal').setDisplayType('hidden');
                }
                if (nlapiGetField('tax2total') != null && nlapiGetField('tax2total') != ''){
                    form.getField('tax2total').setDisplayType('hidden');
                }
                if (nlapiGetField('altshippingcost') != null && nlapiGetField('altshippingcost') != ''){
                    form.getField('altshippingcost').setDisplayType('hidden');
                }
                if (nlapiGetField('total') != null && nlapiGetField('total') != ''){
                    form.getField('total').setDisplayType('hidden');
                }
                if (nlapiGetField('altsalestotal') != null && nlapiGetField('altsalestotal') != ''){
                    form.getField('altsalestotal').setDisplayType('hidden');
                }
                if (nlapiGetField('custbody_clgx_so_pm_flag') != null && nlapiGetField('custbody_clgx_so_pm_flag') != '' && roleid != 1017 && roleid != -5 && roleid != 3 && roleid != 18){
                    form.getField('custbody_clgx_so_pm_flag').setDisplayType('inline');
                }
            }
            
            body += '09 - ' + moment().diff(start) + ' | ';
            
//------------- Begin Section 3 -----------------------------------------------------------------------------------
// Created:	4/15/2014
// Details: Display transaction totals in the header in an inlineHTML field
//-----------------------------------------------------------------------------------------------------------------
            if (type == 'view' ||type == 'edit') {

                var soid = nlapiGetRecordId();

                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_location',null,null).setSort(false));
                //arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_class',null,null).setSort(false));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_total',null,null));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_total_nrc',null,null));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_transaction",null,"anyof",soid));
                //arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_class",null,"anyof",1));
                var searchTotals = nlapiSearchRecord('customrecord_clgx_totals_transaction', null, arrFilters, arrColumns);

                var html = '<table cellpadding="2" border="1" style="font-family: verdana,arial,sans-serif;font-size:11px;color:#333333;border-width: 1px;border-color: #999999;border-collapse: collapse;padding:5px;">';

                html += '<tr>';
                html += '<th style="padding: 5px;background-color: #dedede;">Location</th>';
                //html += '<th style="padding: 5px;background-color: #dedede;">Class</th>';
                html += '<th style="padding: 5px;background-color: #dedede;">Total Recurring</th>';
                html += '<th style="padding: 5px;background-color: #dedede;">Total NRC</th>';
                html += '</tr>';

                for ( var i = 0; searchTotals != null && i < searchTotals.length; i++ ) {
                    var searchTotal = searchTotals[i];
                    html += '<tr>';
                    html += '<td style="padding: 5px;">' + searchTotal.getText('custrecord_clgx_totals_location',null,null) + '</td>';
                    //html += '<td style="padding: 5px;">' + searchTotal.getText('custrecord_clgx_totals_class',null,null) + '</td>';
                    html += '<td style="padding: 5px;">$' + searchTotal.getValue('custrecord_clgx_totals_total',null,null) + '</td>';
                    html += '<td style="padding: 5px;">$' + searchTotal.getValue('custrecord_clgx_totals_total_nrc',null,null) + '</td>';
                    html += '</tr>';

                }
                html += '</table>';

                nlapiSetFieldValue('custbody_clgx_transaction_totals', html);
            }
            
            body += '10 - ' + moment().diff(start) + ' | ';

//------------- Begin Section 4 -----------------------------------------------------------------------------------
// Date - 9/10/2013
// Details:	Check Inventory - Hide install date if inventory was not created correctly - for not admin roles
//-----------------------------------------------------------------------------------------------------------------
            if (type == 'edit') {
                if (allow == 0) {
                	
                	
                    var opportunityId = nlapiGetFieldValue('opportunity');
                    var saletype = 0;
                    if(opportunityId != null && opportunityId != ''){
                        var fields = ['custbody_cologix_opp_sale_type'];
                        var columns = nlapiLookupField('opportunity', opportunityId, fields);
                        saletype = columns.custbody_cologix_opp_sale_type;
                    }

                    var status = nlapiGetFieldValue('status');
                    var installdate = nlapiGetFieldValue('custbody_cologix_service_actl_instl_dt');

                    var hideInstallDate = 'F'; // initialize hide date flag to false

            		if(saletype != 1 && status != 'Pending Billing' && (installdate == '' || installdate == null)){ // if related opportunity is not a renewal and status is not Pending Billing

                        var nbrItems = nlapiGetLineItemCount('item'); // count number of Items`
                        if(nbrItems == 0){ // if no items change flag to true
                            var hideInstallDate = 'T';
                        }

                        for (var i = 0; i < nbrItems; i++) { // loop items

                            var itemService = nlapiGetLineItemValue('item', 'custcol_clgx_so_col_service_id', i + 1);
                            var itemServiceName = nlapiGetLineItemText('item', 'custcol_clgx_so_col_service_id', i + 1);
                            var itemCategory = nlapiGetLineItemText('item', 'custcol_cologix_invoice_item_category', i + 1);
                            var itemQty = nlapiGetLineItemValue('item', 'custcol_clgx_qty2print', i + 1);
                            var itemClass = nlapiGetLineItemText('item', 'class', i + 1);
                            var itemClosed = nlapiGetLineItemValue('item', 'isclosed', i + 1);
                            var itemName = nlapiGetLineItemText('item', 'item', i + 1);
                            var itemID = nlapiGetLineItemValue('item', 'item', i + 1);
                            var itemType = nlapiGetLineItemValue('item', 'itemtype', i + 1);
                            var itemLocation = nlapiGetLineItemValue('item', 'location', i + 1);
                            
                       		//if(itemLocation != 52 && itemLocation != 53 && itemLocation != 54 && itemLocation != 55 && itemLocation != 56){ // verify provisioning only if not NJ
                            if(itemCategory != 'Managed Services'){ // do not verify Managed Services

                                // if there is no service for this recurring and active item change flag to true
                                if(itemClass.indexOf("Recurring") > -1 && (itemService == null || itemService == '') && itemClosed != 'T' && itemType == 'Service' && itemID != 302 && itemID != 518 && itemID != 529 && itemID != 523 && itemID != 552 && itemID != 803){  // not for 'IPv4 Addresses'
                                    var hideInstallDate = 'T';
                                }

                                // if there is a service and item is active
                                if(itemService != null && itemService != ''  && itemService != -1 && itemClosed != 'T' && itemType == 'Service' && itemID != 546){

                                    var index = i + 1;
                                    var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                                    //nlapiLogExecution('DEBUG', 'Value', '| itemService = ' + itemService + ' | itemServiceName = ' + itemServiceName + ' | Index = ' + index + ' | Usage = '+ usageConsumtion + ' |');

                                    // look for XCs in Service
                                    if((itemCategory == 'Interconnection' || itemCategory == 'Network') && itemID != 302 && itemID != 518 && itemID != 529 && itemID != 523 && itemID != 278 && itemID != 803 && itemID != 796 && itemID != 797){
                                    	var recService = nlapiLoadRecord('job', itemService);
                                        var nbrServices = recService.getLineItemCount('recmachcustrecord_cologix_xc_service');

                                        if(itemName.indexOf("Conduit") != -1){ // Network 'Conduit'
                                            // don't verify
                                        }
                                        //else if(itemName.indexOf("Internet Bandwidth ") != -1){ // Internet Bandwidth
                                            // don't verify
                                        //}
                                        else if ((nbrServices < 1 ) && (itemID == 774 || itemID == 769)) { // if no items change flag to true
                                            var hideInstallDate = 'T';
                                        }
                                        else if (itemID == 355 || itemID == 554) {

                                        	var ids = [2,17,53,54,55,56]; // only Dallas and NJ locations
                                        	if(ids.indexOf(parseInt(itemLocation)) > -1){
                                            	
                                        		if(nbrServices < 1){
                                        			var hideInstallDate = 'T';
                                        		} 
                                        		/*
                                        		else {
                                                 var xcid = recService.getLineItemValue('recmachcustrecord_cologix_xc_service', 'id', 1);
                                            		var xctypeid = nlapiLookupField('customrecord_cologix_crossconnect', xcid, 'custrecord_cologix_xc_type');
                                             	if(xctypeid == 25 || xctypeid == 35 || xctypeid == 36){
                                                    		var hideInstallDate = 'T';
                                                            if(userid == 71418){
                                                        		nlapiLogExecution('DEBUG','554', '| itemID = ' + itemID + ' | itemCategory = ' + itemCategory  + ' | xctypeid = ' + parseInt(xctypeid) + ' | ');
                                                        	}
                                            		}
                                        		}
                                        		*/
                                        	}
      
                                        }
                                    	else if (itemID == 798 || itemID == 800){
                                        	var nbrServices = recService.getLineItemCount('recmachcustrecord_cologix_vxc_service');
                                        	if (nbrServices != itemQty*2){
                                        		var hideInstallDate = 'T';
                                        	}
                                    	}
                                        else if ((nbrServices < 2 ) && (itemID == 781 || itemID == 782 || itemID == 793 || itemID == 795)) { // if less then 2 items change flag to true
                                            var hideInstallDate = 'T';
                                        }
                                        else if(nbrServices == 0){ // if no items change flag to true
                                            var hideInstallDate = 'T';
                                        }
                                        else if(itemName.indexOf("Bundle") != -1){ // this is a bundle
                                            for (var j = 0; j < 1; j++) { // verify just the first XC on Service
                                                var xcid = recService.getLineItemValue('recmachcustrecord_cologix_xc_service', 'id', j + 1);
                                                var arrFilters = new Array();
                                                arrFilters.push(new nlobjSearchFilter("custrecord_cologix_xcpath_crossconnect",null,"anyof",xcid));
                                                var arrColumns = new Array();
                                                var searchResults = nlapiSearchRecord('customrecord_cologix_xcpath', 'customsearch_clgx_verify_xc_az_paths', arrFilters, arrColumns);

                                                if(searchResults == null){ // if no path with all conditions
                                                    var hideInstallDate = 'T';
                                                }
                                                else{
                                                    var searchResult = searchResults[0];
                                                    var columns = searchResult.getAllColumns();
                                                    var aSeq = searchResult.getValue(columns[1]);
                                                    var zSeq = searchResult.getValue(columns[2]);

                                                    if(aSeq != 1 || zSeq != 1){ // if none or more than one A or Z
                                                        var hideInstallDate = 'T';
                                                    }
                                                }
                                            }
                                        }
                                        else{ // verify if all XCs to have A & Z Paths

                                            if((nbrServices != itemQty)&&(itemID != 774 && itemID != 769 && itemID != 781 && itemID != 782 && itemID != 793 && itemID != 795 && itemID != 798 && itemID != 800)){ // if number of XCs on Service Order different from # of XCs on Service change flag to true
                                                var hideInstallDate = 'T';
                                                //nlapiSendEmail(71418,71418,'itemName',JSON.stringify(itemName),null,null,null,null);
                                            }
                                            else{
                                                for (var j = 0; j < nbrServices; j++) { // loop and verify all XCs
                                                    var xcid = recService.getLineItemValue('recmachcustrecord_cologix_xc_service', 'id', j + 1);
                                                    var arrFilters = new Array();
                                                    arrFilters.push(new nlobjSearchFilter("custrecord_cologix_xcpath_crossconnect",null,"anyof",xcid));
                                                    var arrColumns = new Array();
                                                    var searchResults = nlapiSearchRecord('customrecord_cologix_xcpath', 'customsearch_clgx_verify_xc_az_paths', arrFilters, arrColumns);

                                                    if(searchResults == null){ // if no path with all conditions
                                                        var hideInstallDate = 'T';
                                                    }
                                                    else{
                                                        var searchResult = searchResults[0];
                                                        var columns = searchResult.getAllColumns();
                                                        var aSeq = searchResult.getValue(columns[1]);
                                                        var zSeq = searchResult.getValue(columns[2]);

                                                        if(aSeq != 1 || zSeq != 1){ // if none or more than one A or Z
                                                            var hideInstallDate = 'T';
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    
                                    if(itemCategory == 'Interconnection' && itemID == 797){ // VXC in Interconnection category exception
                                    	
                                        var recService = nlapiLoadRecord('job', itemService);
                                        var nbrServices = recService.getLineItemCount('recmachcustrecord_cologix_vxc_service');
                                        if(nbrServices != itemQty){
                                            var hideInstallDate = 'T';

                                        }

                                    }
                                    

                                    
                                    // look for Power Circuits in Service
                                    if(itemCategory == 'Power' && itemID != 780){
                                        var recService = nlapiLoadRecord('job', itemService);
                                        var nbrServices = recService.getLineItemCount('recmachcustrecord_cologix_power_service');

                                        if(itemName.indexOf("A+B") != -1){ //this is a double #Nim for one #Bill so fake the #bill
                                            itemQty = itemQty*2;
                                        }

                                        if(itemName.indexOf("Kilowatt Reservation") != -1){
                                            if(parseInt(nbrServices) < 1){ // #Nim at least one or more - if not  change flag to true
                                                //var hideInstallDate = 'T';
                                            }
                                        }
                                        else if(nbrServices != itemQty){ // if number of powers on Service Order different from # of power on Service change flag to true
                                            var hideInstallDate = 'T';
                                        }
                                        else{ // same number of bill and nim power circuits - verify if space on all of them

                                            for (var j = 0; j < nbrServices; j++) { // loop and verify all Powers
                                                var spaceid = recService.getLineItemValue('recmachcustrecord_cologix_power_service', 'custrecord_cologix_power_space', j + 1);
                                                if(spaceid == null || spaceid == null){ // if no space configured on power circuit
                                                    var hideInstallDate = 'T';

                                                }
                                            }
                                        }
                                    }
                                    // look for Spaces in Service
                                    if(itemCategory == 'Space' && itemID != 354 && itemID != 382){

                                        var recService = nlapiLoadRecord('job', itemService);
                                        var nbrServices = recService.getLineItemCount('recmachcustrecord_cologix_space_project');
                                        // verify if qty is not Feet or KW
                                        if(itemName.indexOf("Foot Print") != -1 || itemName.indexOf("Kilowatt") != -1 || itemID == 354){ // deal with exceptions

                                            // verify space
                                            if(parseInt(nbrServices) < 1){ // #Nim at least one or more - if not  change flag to true
                                                var hideInstallDate = 'T';
                                            }
                                            // verify power
                                            if(itemID == 354){ // for this one, also verify at leat one power
                                                var nbrPowers = recService.getLineItemCount('recmachcustrecord_cologix_power_service');
                                                if(parseInt(nbrPowers) < 1){ // #Nim at least one or more - if not  change flag to true
                                                    //var hideInstallDate = 'T';
                                                }
                                            }
                                        }
                                        else{ // this is not an exception
                                            if(nbrServices != itemQty){ // if number of spaces on Service Order different from # of spaces on Service change flag to true
                                                var hideInstallDate = 'T';
                                            }
                                        }
                                    }
                                }

                            } else { // managed services
  
                            	var ids = [628,630,627,637,638,640,829,802,642,643,644,645,649,650,651,655,653,654,658,659,661,662,663,664,666,667,668,669,670,791,672,671,673,674,675,676,677,801,678,712,679,680,681,713,682];
                            	if(ids.indexOf(parseInt(itemID)) > -1){
                            		
                            		var recService = nlapiLoadRecord('job', itemService);
                                    var nbrServices = recService.getLineItemCount('recmachcustrecord_clgx_ms_service');
                                    
                                    //if(userid == 71418){
                            		//	nlapiLogExecution('DEBUG','debug', '| itemID = ' + itemID + ' | itemCategory = ' + itemCategory  + ' | nbrServices = ' + parseInt(nbrServices) + ' | ');
                            		//}
                            		if(parseInt(nbrServices) < 1){
                                        var hideInstallDate = 'T';
                                    }
                            	}
                            }
                            if(userid == 71418 || userid == 1349020){
                        		nlapiLogExecution('DEBUG','debug', '| itemID = ' + itemID + ' | itemCategory = ' + itemCategory  + ' | nbrServices = ' + parseInt(nbrServices) + ' | hideInstallDate = ' + hideInstallDate + ' | ');
                        	}
                        }
                    }

            		
            		var wait = nlapiGetFieldValue('custbody_clgx_wtng_cust_response');
            		if(wait == 'T'){
            			var hideInstallDate = 'T';
            		}
            		
            		
                    if(hideInstallDate == 'T'){ // if flag was changed to true, hide install date field
                        form.getField('custbody_cologix_service_actl_instl_dt').setDisplayType('inline');
                    }
                }
                else{
                    form.getField('custbody_cologix_service_actl_instl_dt').setDisplayType('normal');
                }
                
                
                body += '11 - ' + moment().diff(start) + ' | ';
                
                var id = nlapiGetRecordId();
                //nlapiLogExecution('ERROR','SO beforeLoad Processing Time - ' + id, moment().diff(start));
                //nlapiSendEmail(71418,71418,'SO beforeLoad Processing Time - ' + id, body,null,null,null,null);

                var record = nlapiCreateRecord('customrecord_clgx_script_exec_time');
    			record.setFieldValue('custrecord_clgx_script_exec_time_user', nlapiGetUser());
    			record.setFieldValue('custrecord_clgx_script_exec_time_rec', 'so');
    			record.setFieldValue('custrecord_clgx_script_exec_time_id', nlapiGetRecordId());
    			record.setFieldValue('custrecord_clgx_script_exec_time_context', 1);
    			record.setFieldValue('custrecord_clgx_script_exec_time', moment().diff(start));
    			record.setFieldValue('custrecord_clgx_script_exec_time_details', body);
    			try {
                    nlapiSubmitRecord(record, false, true);
                }
                catch (error) {
                }
    			
            }

        }


//---------- End Sections ------------------------------------------------------------------------------------------------
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            //nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
        	nlapiLogExecution('ERROR','Process Error', error);
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
