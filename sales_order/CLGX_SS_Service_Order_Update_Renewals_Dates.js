nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Service_Order_Update_Renewals_Dates.js
//	Script Name:	CLGX_SS_Service_Order_Update_Renewals_Dates
//	Script Id:		customscript_clgx_ss_so_up_renewal_dates
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		1/3/2014
//	Includes:		moment.min.js, CLGX_LIB_Global.js
//-------------------------------------------------------------------------------------------------
function scheduled_so_up_renewal_dates(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();
        
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Details:	Update renewals dates and cycle for expiring SOs.
// Created:	1/3/2014
//-----------------------------------------------------------------------------------------------------------------
        var date = new Date();
		var startScript = moment();
        var emailAdminSubject = 'Update renewal dates for expiring SOs';
		var emailAdminBody = '';
		emailAdminBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');
		emailAdminBody += '<br><br>';
		emailAdminBody += '<table cellpadding="5" border="1" style="font-family: verdana,arial,sans-serif;font-size:12px;color:#333333;border-width: 1px;border-color: #999999;border-collapse: collapse;padding:5px;">';
        emailAdminBody += '<tr>';
        emailAdminBody += '<td  colspan="8" style="padding: 5px;background-color: #ffffff;"><img src="http://www.cologix.com/templates/cologix/images/logo.png" alt="Cologix" height="37" width="107"></td>';
        emailAdminBody += '</tr>';
        emailAdminBody += '<tr>';
        emailAdminBody += '<td  colspan="8" style="padding: 5px;background-color: #ffffff;"><h2>Update renewal dates for expiring SOs</h2></td>';
        emailAdminBody += '</tr>';
        emailAdminBody += '<tr>';
        emailAdminBody += '<td style="padding: 5px;background-color: #dedede;">Service Order</td>';
        emailAdminBody += '<td style="padding: 5px;background-color: #dedede;">Terms (Months)</td>';
        emailAdminBody += '<td style="padding: 5px;background-color: #dedede;">Contract Dates</td>';
        emailAdminBody += '<td style="padding: 5px;background-color: #dedede;">Renew Dates</td>';
        emailAdminBody += '<td style="padding: 5px;background-color: #dedede;">Cycle</td>';
        emailAdminBody += '<td style="padding: 5px;background-color: #dedede;">Execution Time</td>';
        emailAdminBody += '<td style="padding: 5px;background-color: #dedede;">Minute</td>';
        emailAdminBody += '<td style="padding: 5px;background-color: #dedede;">Usage</td>';
        emailAdminBody += '</tr>';
		
        // get list of SOs containing inactive items or services, to exclude it from processing
        //var searchInactives = nlapiSearchRecord('transaction', 'customsearch_clgx_sos_with_inactives', null, null);
    	//var arrSOs = new Array();
    	//for ( var i = 0; searchInactives != null && i < searchInactives.length; i++ ) {
    	//	arrSOs.push(searchInactives[i].getValue('internalid', null, 'GROUP')); // build an array with internal ids of those SOs
    	//}
    	
        var arrColumns = new Array();
        var arrFilters = new Array();
        //if(arrSOs.length > 0){
        	//arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",arrSOs));
        //}
        var searchResults = nlapiSearchRecord('transaction', 'customsearch_clgx_sched_sos_renewals', arrFilters, arrColumns);

        var startScript = moment(); // time when script started
        
        for ( var j = 0; searchResults != null && j < searchResults.length; j++ ) {
        	
        	var startExec = moment(); // time when each loop is starting
        	var execMinutes = (startExec.diff(startScript)/60000).toFixed(1); // execution time in minutes
            if ( (context.getRemainingUsage() <= 1000 || execMinutes > 50) && (j+1) < searchResults.length ){ // if 1000 points left or more then 50 min execution time, re-schedule the script
               var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
               if ( status == 'QUEUED' ) {
            	   nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
                   break; 
               }
            }

        	var searchResult = searchResults[j];
        	var soid = searchResult.getValue('internalid',null,'GROUP');
        	var so = searchResult.getValue('tranid',null,'GROUP');
        	
        	// log loop / SO
        	var index = j + 1;
        	var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage()); // how many points the script consumed
        	nlapiLogExecution('DEBUG', 'Value', '| SO = ' + so + ' | ID = ' + soid + ' | Index = ' + index + ' of ' + searchResults.length +  ' | Usage = '+ usageConsumtion + ' | Minutes = ' + execMinutes + ' |');
        	
        	// length in month of the first cycle/contract
        	var billingTerms = searchResult.getValue('custbody_cologix_biling_terms',null,'GROUP');
        	if (billingTerms != 'MTM'){
        		var firstCycleLength = parseInt(billingTerms);
        	}
        	else{
        		var firstCycleLength = 1;
        	}
        	// length in month of the renewed cycle/contract
        	var renewTerms = searchResult.getText('custentity_clgx_cust_renewal_term','customerMain','GROUP');
        	if (renewTerms == 'Original Contract Term'){
        		var renewCycleLength = firstCycleLength;
        	}
        	else{
        		var renewCycleLength = parseInt(renewTerms.substring(0, 3));
        	}
        	
        	var contractStartDate = searchResult.getValue('custbody_cologix_so_contract_start_dat',null,'GROUP');
        	var contractEndDate = searchResult.getValue('enddate',null,'GROUP');
        	
        	var renewStartDate = searchResult.getValue('custbody_clgx_renewal_start_date',null,'GROUP');
        	var currentEndDate = searchResult.getValue('custbody_clgx_renewal_end_date',null,'GROUP');
        	var currentCycle = searchResult.getValue('custbody_clgx_contract_cycle',null,'GROUP');
        	
        	var todayMom = moment();
        	if(currentEndDate == null || currentEndDate == ''){ // current end date has not been populated
        		var contractEndDateMom = new moment(nlapiStringToDate(contractEndDate));
        		if (todayMom.isAfter(contractEndDateMom, 'day')){ // we did pass the first cycle, even if current end date has not been populated
        			currentCycle = 2;
        			var addMonths = parseInt(renewCycleLength) ; // add months of both first and renew cycles
        			contractEndDateMom.add('months', addMonths);
        			
        			// verify more than the first cycle ???
        			/*
		        	if (todayMom.isAfter(contractEndDateMom, 'day')){ // we did pass the second cycle
	        			currentCycle += 1;
	        			contractEndDateMom.add('months', parseInt(renewCycleLength)); // add months of renew cycle
			        	if (todayMom.isAfter(contractEndDateMom, 'day')){ // we did pass the third cycle
		        			currentCycle += 1;
		        			contractEndDateMom.add('months', parseInt(renewCycleLength)); // add months of renew cycle
				        	if (todayMom.isAfter(contractEndDateMom, 'day')){ // we did pass the forth cycle
			        			currentCycle += 1;
			        			contractEndDateMom.add('months', parseInt(renewCycleLength)); // add months of renew cycle
					        	if (todayMom.isAfter(contractEndDateMom, 'day')){ // we did pass the fith cycle
				        			currentCycle += 1;
				        			contractEndDateMom.add('months', parseInt(renewCycleLength)); // add months of renew cycle
					        	}
				        	}
			        	}
		        	}
		        	*/
        			
		        	currentEndDate = contractEndDateMom.format('M/D/YYYY');
		    		renewStartDate = contractEndDateMom.add('days', 1).format('M/D/YYYY');
        		}
        		else{ // we're still inside the first cycle
        			currentCycle = 1;
        			//contractEndDateMom.add('months', parseInt(firstCycleLength));
		        	currentEndDate = contractEndDateMom.format('M/D/YYYY');
		    		renewStartDate = contractEndDateMom.add('days', 1).format('M/D/YYYY');
        		}
        	}
        	else{ // current end date is present, most probably this is a renewal
        		var currentEndDateMom = new moment(nlapiStringToDate(currentEndDate));
    			currentCycle = parseInt(currentCycle) + 1; // if date is present, cycle myust be too, so increase with 1
        		currentEndDateMom.add('months', parseInt(renewCycleLength));
	        	currentEndDate = currentEndDateMom.format('M/D/YYYY');
	    		renewStartDate = currentEndDateMom.add('days', 1).format('M/D/YYYY');
        	}
        	
        	var fields = ['custbody_clgx_renewal_start_date','custbody_clgx_renewal_end_date','custbody_clgx_contract_cycle'];
			var values = [renewStartDate, currentEndDate, currentCycle];
			nlapiSubmitField('salesorder', soid, fields, values);
			emailAdminBody += '<tr><td><a href="https://1337135.app.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soid + '">' + so + '</a><td> Contract : ' + firstCycleLength + '<br> Renew : ' + renewCycleLength + '</td><td> Start : ' + contractStartDate + '<br> End: ' + contractEndDate + '</td><td> Start : ' + renewStartDate + '<br> End : ' + currentEndDate + '</td><td>' + currentCycle + '</td><td>' + startExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + execMinutes + '</td><td>'+ usageConsumtion + '</td></tr>';
        }
		emailAdminBody += '</table>';

		var endScript = moment();
		emailAdminBody += '<br>';
		emailAdminBody += 'End : ' + endScript.format('M/D/YYYY h:mm:ss a') + '<br>';
		emailAdminBody += 'Total execution minutes : ' + (endScript.diff(startScript)/60000).toFixed(1) + '<br>';
		var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
		emailAdminBody += 'Total usage : ' + usageConsumtion;
		emailAdminBody += '<br><br>';
		
		clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_so_admin", emailAdminSubject, emailAdminBody);
		//nlapiSendEmail(71418,71418,emailAdminSubject,emailAdminBody,null,null,null,null,true);
		
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}