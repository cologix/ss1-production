//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Service_Order_Mass_Update_Jason.js
//	Script Name:	CLGX_SS_Service_Order_Mass_Update_Jason
//	Script Id:		customscript_clgx_ss_so_mu_jason
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Service Order
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		6/19/2013
//-------------------------------------------------------------------------------------------------
function scheduled_service_order_mu_jason (){
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 2/5/2013
// Details:	Updates service order item prices by creating new lines and closing the old ones.
//-------------------------------------------------------------------------------------------------
    try{
        var context = nlapiGetContext();

    	var arrColumns = new Array();
    	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_so_mu_nbr',null,null));
    	var arrFilters = new Array();
    	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_so_mu_done",null,"is",'F'));
    	var searchResults = nlapiSearchRecord("customrecord_clgx_so_mass_update", null, arrFilters, arrColumns);

        for ( var j = 0; searchResults != null && j < searchResults.length; j++ ) {
        	
            if ( context.getRemainingUsage() <= 1000 && (j+1) < searchResults.length ){
               var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
               if ( status == 'QUEUED' ) {
            	   nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
                   break; 
               }
            }
            
        	var searchResult = searchResults[j];
        	var internalID = searchResult.getValue('internalid');
        	var soNbr = searchResult.getValue('custrecord_clgx_so_mu_nbr');
    	
        	var arrColumns = new Array();
        	arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
        	var arrFilters = new Array();
        	arrFilters.push(new nlobjSearchFilter("tranid",null,"is",soNbr));
        	var searchSOs = nlapiSearchRecord("salesorder", null, arrFilters, arrColumns);
        	
        	var searchSO = searchSOs[0];
        	var soID = searchSO.getValue('internalid',null,'GROUP');

        	var recSO = nlapiLoadRecord('salesorder', soID);
        	var recSONew = nlapiLoadRecord('salesorder', soID);

			recSONew.setFieldValue('trandate', getLastMonthDay());
			recSONew.setFieldValue('startdate', getFirstNextMonthDay());

			var nbrItems = recSO.getLineItemCount('item');
			
			for (var i = 0; i < nbrItems; i++){
			
				var item = recSO.getLineItemValue('item','item', i + 1);
				var rate = recSO.getLineItemValue('item','rate', i + 1);
				var rateNew = recSO.getLineItemValue('item','rate', i + 1);
				var amount = recSO.getLineItemValue('item','amount', i + 1);
				var qty = recSO.getLineItemValue('item','quantity', i + 1);
				var qtyNew = recSO.getLineItemValue('item','quantitybilled', i + 1);
				var description = recSO.getLineItemValue('item','description', i + 1);

				var updateItem = 0;
				var torix = 0;
				if(description == null || description == ''){
					description = '';
				}
				var torix1 = description.indexOf('Torix');
				var torix2 = description.indexOf('torix');
				var torix3 = description.indexOf('TORIX');
				if(torix1 > -1 || torix2 > -1 || torix3 > -1){
					torix = 1;
				}
				
				if (rate < 150 && torix == 1){ // If Torix in description
					rateNew = 150;
					updateItem = 1;
				}
				else if (item == 93 && rate > 150  && rate < 250 && torix == 1){ // If Fiber & Torix in description
					rateNew = 250;
					updateItem = 1;
				}
				else if (item == 93 && rate < 250 && torix == 0){ // If Fiber & no Torix in description
					rateNew = 250;
					updateItem = 1;
				}
				else if (item == 92 && rate < 150 && torix == 0){ // if Copper & no Torix in description
					rateNew = 150;
					updateItem = 1;
				}
				else if (item == 270 && rate < 65 && torix == 0){ // if POTS & no Torix in description
					rateNew = 65;
					updateItem = 1;
				}
				else if (item == 91 && rate < 150 && torix == 0){ // if Coax & no Torix in description
					rateNew = 150;
					updateItem = 1;
				}
				else{
				}
			
				var amountNew = (parseFloat(rateNew) * parseFloat(qty)).toFixed(2);
				var bs = recSO.getLineItemText('item','billingschedule', i + 1);
				var recurring = recSO.getLineItemText('item','class', i + 1);
				
				if(recurring.indexOf("Recurring") > -1){ // if this item is recurring
					
					if(bs != null && bs != ''){ // if billing schedule is not null copy item to new line and make billing schedule 'Month to Month', then set rate to 0 and remove Bill Schedule on old line 
						
						var intCount = recSONew.getLineItemCount('item');
						// set fields on new item line
						recSONew.insertLineItem('item', intCount + 1);
						recSONew.setLineItemValue('item','custcol_clgx_so_col_service_id', intCount + 1, recSO.getLineItemValue('item','custcol_clgx_so_col_service_id', i + 1)); 
						recSONew.setLineItemValue('item','location', intCount + 1, recSO.getLineItemValue('item','location', i + 1)); 
						recSONew.setLineItemValue('item','item', intCount + 1, recSO.getLineItemValue('item','item', i + 1)); 
						recSONew.setLineItemValue('item','description', intCount + 1, recSO.getLineItemValue('item','description', i + 1)); 
						recSONew.setLineItemValue('item','pricelevels', intCount + 1, recSO.getLineItemValue('item','pricelevels', i + 1)); 
						recSONew.setLineItemValue('item','price', intCount + 1, recSO.getLineItemValue('item','price', i + 1)); 
						recSONew.setLineItemValue('item','taxcode', intCount + 1, recSO.getLineItemValue('item','taxcode', i + 1));
						recSONew.setLineItemValue('item','taxrate1', intCount + 1, recSO.getLineItemValue('item','taxrate1', i + 1));
						recSONew.setLineItemValue('item','taxrate2', intCount + 1, recSO.getLineItemValue('item','taxrate2', i + 1));
						recSONew.setLineItemValue('item','class', intCount + 1, recSO.getLineItemValue('item','class', i + 1));
						recSONew.setLineItemValue('item','options', intCount + 1, recSO.getLineItemValue('item','options', i + 1)); 
						recSONew.setLineItemValue('item','custcol_cologix_unevendaystobill', intCount + 1, recSO.getLineItemValue('item','custcol_cologix_unevendaystobill', i + 1));
						recSONew.setLineItemValue('item','custcol_cologix_rateperday', intCount + 1, recSO.getLineItemValue('item','custcol_cologix_rateperday', i + 1)); 
						recSONew.setLineItemValue('item','custcol_cologix_invoice_item_category', intCount + 1, recSO.getLineItemValue('item','custcol_cologix_invoice_item_category', i + 1)); 
						recSONew.setLineItemValue('item','custcol_clgx_qty2print', intCount + 1, recSO.getLineItemValue('item','custcol_clgx_qty2print', i + 1));
						recSONew.setLineItemValue('item','billingschedule', intCount + 1, 3); // make billing schedule for new line item 'Month to Month'
						recSONew.setLineItemValue('item','quantity', intCount + 1, (parseInt(recSO.getLineItemValue('item','custcol_clgx_qty2print', i + 1)) * 12));
						
						if(updateItem == 1){
							recSONew.setLineItemValue('item','rate', intCount + 1, rateNew); 
							//recSONew.setLineItemValue('item','amount', intCount + 1, amountNew);
						}
						else{
							recSONew.setLineItemValue('item','rate', intCount + 1, rate); 
							//recSONew.setLineItemValue('item','amount', intCount + 1, amount);
						}

						// update fields on old line
						recSONew.setLineItemValue('item','quantity', i + 1, qtyNew);
						recSONew.setLineItemValue('item','rate', i + 1, 0);
						recSONew.setLineItemValue('item','amount', i + 1, 0);
						recSONew.setLineItemValue('item','billingschedule', i + 1, null);
						recSONew.setLineItemValue('item','custcol_clgx_so_item_old_rate', i + 1, rate);
						recSONew.setLineItemValue('item','custcol_clgx_so_item_adj_date', i + 1, getDateToday());
						
					}
				}
				else{ // if this item is non recurring set rate to 0 and remove Bill Schedule
					recSONew.setLineItemValue('item','rate', i + 1, 0);
					recSONew.setLineItemValue('item','amount', i + 1, 0);
					recSONew.setLineItemValue('item','billingschedule', i + 1, null);
				}
				
			}
    		nlapiSubmitRecord(recSONew, false, true);
    		
    		nlapiSubmitField('customrecord_clgx_so_mass_update',internalID,['custrecord_clgx_so_mu_done'],['T']);

        	var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
        	nlapiLogExecution('DEBUG', 'Value', 'ID = ' + i + '/' + searchResults.length +  ' / Usage - '+ usageConsumtion);
        }

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function getLastMonthDay(){
    var date = new Date();
	tranDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
	//startDate = tranDate;
	//startDate.setDate(startDate.getDate() + 1);

    var month = parseInt(tranDate.getMonth()) + 1;
    var day = tranDate.getDate();
    var year = tranDate.getFullYear();
    
    if(parseInt(month) < 10){
        month = '0' + month;
    }
    
    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = month + '/' + day + '/' + year;
    
    return formattedDate;
}

function getFirstNextMonthDay(){
    var date = new Date();
    startDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
	startDate.setDate(startDate.getDate() + 1);

    var month = parseInt(startDate.getMonth()) + 1;
    var day = startDate.getDate();
    var year = startDate.getFullYear();
    
    if(parseInt(month) < 10){
        month = '0' + month;
    }
    
    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = month + '/' + day + '/' + year;
    
    return formattedDate;
}

function getDateToday(){
    var date = new Date();
    var month = parseInt(date.getMonth()) + 1;
    var day = date.getDate();
    var year = date.getFullYear();
    
    if(parseInt(month) < 10){
        month = '0' + month;
    }
    
    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = month + '/' + day + '/' + year;
    
    return formattedDate;
}