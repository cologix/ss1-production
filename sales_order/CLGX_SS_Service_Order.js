nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Service_Order.js
//	Script Name:	CLGX_SS_Service_Order
//	Script Id:		customscript_clgx_ss_so
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Service Order
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		03/10/2014
//-------------------------------------------------------------------------------------------------

function scheduled_service_order(){
    try{
//------------- Begin Section 1 -------------------------------------------------------------------
// Created:	2/5/2013
// Details:	Create one SO for each location/BS on the single proposal - add NRC items on first BS of each location
//			Create a new service for any recurring item on creation of the service order
//			Calculate quantities and totals
//-------------------------------------------------------------------------------------------------
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');

        var scriptStart = moment();

        var intChar = 1;
        var date = new Date();
        var arrNewSOs = new Array();

        //retrieve script parameters
        var proposalid = nlapiGetContext().getSetting('SCRIPT','custscript_proposalid');
        var userid = nlapiGetContext().getSetting('SCRIPT','custscript_userid');
        var username = nlapiLookupField('employee', userid, 'entityid');

        var recProposal = nlapiLoadRecord('estimate', proposalid);
        var proposal = recProposal.getFieldValue('tranid');
        var incrementalMrc=recProposal.getFieldValue('custbody_clgx_prop_incremental_mrc');
        var opptyid = recProposal.getFieldValue('opportunity');

        var subsidiary = recProposal.getFieldValue('subsidiary');
        var currency = recProposal.getFieldValue('currency');
        var exchangerate = recProposal.getFieldValue('exchangerate');

        var arrRenewSOs = recProposal.getFieldValues('custbody_clgx_renewed_from_sos'); // get SOs from which the oppty/proposal were created in case it's a renewal
        var arrSOsold=arrRenewSOs;
        // start email report
        var startScript = moment();
        var emailAdminSubject = 'Service order(s) and services created for proposal ' + proposal + ' by ' + username;
        var emailAdminBody = '';
        emailAdminBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');
        emailAdminBody += '<h2>Service order(s) created for proposal ' + proposal + '</h2>';
        emailAdminBody += '<table border="1" cellpadding="5">';
        emailAdminBody += '<tr><td>Location</td><td>Billing Schedule</td><td>Service Order</td><td>Time</td><td>Minutes</td><td>Usage</td></tr>';

        var emailUserSubject = 'Service order(s) and services created for proposal ' + proposal;
        var emailUserBody = '';
        emailUserBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');
        emailUserBody += '<h2>Service order(s) created for proposal ' + proposal + '</h2>';
        emailUserBody += '<table border="1" cellpadding="5">';
        emailUserBody += '<tr><td>Location</td><td>Billing Schedule</td><td>Service Order</td></tr>';

        // search locations and recurring billling schedules on proposal items
        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('location',null,'GROUP'));
        arrColumns.push(new nlobjSearchColumn('billingschedule',null,'GROUP'));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",proposalid));
        arrFilters.push(new nlobjSearchFilter("billingschedule",null,"noneof",17));
        arrFilters.push(new nlobjSearchFilter("billingschedule",null,"noneof",'@NONE@'));
        arrFilters.push(new nlobjSearchFilter("location",null,"noneof",'@NONE@'));
        arrFilters.push(new nlobjSearchFilter("type","item","is",'Service'));
        var searchBSs = nlapiSearchRecord('estimate', null, arrFilters, arrColumns);

        if(searchBSs != null && searchBSs.length == 1){ // use transform method -----------------------------------------------------------------------------------------------

            var startExec = moment();
            var execMinutes = (startExec.diff(scriptStart)/60000).toFixed(1);

            var bs = searchBSs[0].getText('billingschedule',null,'GROUP');
            var location = searchBSs[0].getText('location',  null, 'GROUP');
            var idlocation = searchBSs[0].getValue('location',  null, 'GROUP');
            //  nlapiLogExecution('DEBUG','Started  Transform', proposalid+';'+idlocation+';'+incrementalMrc);

            
            nlapiLogExecution("DEBUG", "idlocation", JSON.stringify(idlocation));
            
            var salesOrder = nlapiTransformRecord('estimate', proposalid, 'salesorder');
            salesOrder.setFieldValue('saleseffectivedate', nlapiDateToString(date));
            salesOrder.setFieldValue('custbody_clgx_consolidate_locations', clgx_return_consolidate_location (idlocation));
            salesOrder.setFieldValue('custbody_clgx_ready_to_uplift', 'T');
            salesOrder.setFieldValue('custbody_clgx_so_incremental_mrc',incrementalMrc);
            var so = 'SO' + proposal + '-1';
            salesOrder.setFieldValue('tranid', so);
            var soid = nlapiSubmitRecord(salesOrder, false, false);
            arrNewSOs.push(soid);
            // nlapiLogExecution('DEBUG','Finish  Transform', proposalid+';'+idlocation+';'+incrementalMrc);

            var recurrences = 0;
            var termsMonths = '';
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_bs_recurrences',null,null));
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_bs_months',null,null));
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_bs_name",null,"is",bs));
            var searchResults = nlapiSearchRecord('customrecord_clgx_billing_schedules', null, arrFilters, arrColumns);
            if(searchResults != null){
                recurrences = searchResults[0].getValue('custrecord_clgx_bs_recurrences',null,null);
                if(bs == 'Month to Month'){
                    termsMonths = 'MTM';
                }
                else if(bs == 'Non Recurring'){
                    termsMonths = '';
                }
                else{
                    termsMonths = searchResults[0].getValue('custrecord_clgx_bs_months',null,null);
                }
            }

            var setTerms = 0; // if there are only NRC lines, do not set Service Terms by default
            var totalNRC = 0;
            var totalMRC = 0;
            var total = 0;

            var objSO = nlapiLoadRecord('salesorder', soid);
            var customerid = objSO.getFieldValue('entity');

            var nbrItems = objSO.getLineItemCount('item');

            for (var i = 0; i < nbrItems; i++) {

                var rate = objSO.getLineItemValue('item', 'rate', i + 1);
                var quantity = objSO.getLineItemValue('item', 'quantity', i + 1);
                var amount = objSO.getLineItemValue('item', 'amount', i + 1);
                var billsched = objSO.getLineItemText('item', 'billingschedule', i + 1);
                var classtype = objSO.getLineItemText('item', 'class', i + 1);
                var itemtype = objSO.getLineItemValue('item', 'itemtype', i + 1);

                if (classtype.indexOf("Recurring") > -1 && itemtype == 'Service') {
                    objSO.setLineItemValue('item','custcol_clgx_qty2print', i + 1, quantity);
                    objSO.setLineItemValue('item','quantity', i + 1, recurrences * quantity);

                    setTerms = 1; // there is at least one recurring, then set the Service Terms

                    totalMRC += parseFloat(rate) * parseFloat(quantity);
                    total += parseFloat(rate) * parseFloat(recurrences) * parseFloat(quantity);
                }
                else if (classtype.indexOf("Recurring") > -1 && itemtype == 'Discount'){
                    totalMRC += parseFloat(amount);
                    total += parseFloat(recurrences) * parseFloat(amount);
                }
                else if (classtype.indexOf("NRC") > -1 && itemtype == 'Service'){
                    objSO.setLineItemValue('item','custcol_clgx_qty2print', i + 1, quantity);
                    objSO.setLineItemValue('item','quantity', i + 1, quantity);
                    totalNRC += parseFloat(rate) * parseFloat(quantity);
                }
                else{}
            }
            // update totals fields on the so header
            objSO.setFieldValue('custbody_clgx_total_recurring', total);
            objSO.setFieldValue('saleseffectivedate', nlapiDateToString(date));
            objSO.setFieldValue('custbody_clgx_total_recurring_month', totalMRC);
            objSO.setFieldValue('custbody_clgx_total_non_recurring', totalNRC);

            if(termsMonths != ''){
                objSO.setFieldValue('custbody_cologix_biling_terms', termsMonths.toString());
            }

            var soid = nlapiSubmitRecord(objSO, false, true);
            var totals = clgx_transaction_totals (customerid, 'so', soid);

            var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
            emailAdminBody += '<tr><td>' + location + '</td><td>' + bs + '</td><td><a href="https://1337135.app.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soid + '">' + so + '</a></td><td>' + startExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + execMinutes + '</td><td>'+ usageConsumtion + '</td></tr>';
            emailUserBody += '<tr><td>' + location + '</td><td>' + bs + '</td><td><a href="https://1337135.app.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soid + '">' + so + '</a></td></tr>';
            nlapiLogExecution('DEBUG','Created SO - ', location + ' / ' + bs + ' / ' + so);
        }
        if((searchBSs != null && searchBSs.length > 1) || searchBSs == null){  // use location/billing schedule pairs method --------------------------------------------------------------------------------------------------------------

            // search and group locations from the proposal recurring items
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('location',null,'GROUP'));
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof',proposalid));
            //arrFilters.push(new nlobjSearchFilter('billingschedule',null,'noneof',17)); // not Non Recurring
            arrFilters.push(new nlobjSearchFilter('location',null,'noneof','@NONE@'));
            arrFilters.push(new nlobjSearchFilter('billingschedule',null,'noneof','@NONE@'));
            arrFilters.push(new nlobjSearchFilter('isinactive','item','is','F'));
            var searchLocations = nlapiSearchRecord('estimate', null, arrFilters, arrColumns);

            // loop locations
            for ( var i = 0; searchLocations != null && i < searchLocations.length; i++ ) {
                var searchLocation = searchLocations[i];
                var locationid = searchLocation.getValue('location',  null, 'GROUP');
                var location = searchLocation.getText('location',  null, 'GROUP');

                // search the NRC items for this location
                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('rate',null,null));
                arrColumns.push(new nlobjSearchColumn('taxcode',null,null));
                arrColumns.push(new nlobjSearchColumn('custentity_cologix_service_order','custcol_clgx_so_col_service_id',null));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof',proposalid));
                arrFilters.push(new nlobjSearchFilter('location',null,'anyof',locationid));
                arrFilters.push(new nlobjSearchFilter('billingschedule',null,'anyof',17));
                arrFilters.push(new nlobjSearchFilter('billingschedule',null,'noneof','@NONE@'));
                arrFilters.push(new nlobjSearchFilter('isinactive','item','is','F'));
                var searchItemsNRCs = nlapiSearchRecord('estimate', 'customsearch_clgx_so_renewals_proposal', arrFilters, arrColumns);

                // search and group recurring billing schedules for this location
                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('billingschedule',null,'GROUP'));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof',proposalid));
                arrFilters.push(new nlobjSearchFilter('location',null,'anyof',locationid));
                arrFilters.push(new nlobjSearchFilter('billingschedule',null,'noneof',17));
                arrFilters.push(new nlobjSearchFilter('billingschedule',null,'noneof','@NONE@'));
                arrFilters.push(new nlobjSearchFilter('isinactive','item','is','F'));
                var searchBSs = nlapiSearchRecord('estimate', null, arrFilters, arrColumns);

                if (searchBSs != null) { // if there are recurring billing schedules for this location

                    // loop all billing schedules for each location
                    for ( var j = 0;  j < searchBSs.length; j++ ) {
                        var arraySOSOLD=new Array();
                        var startExec = moment();
                        var execMinutes = (startExec.diff(scriptStart)/60000).toFixed(1);

                        var searchBS = searchBSs[j];
                        var bsid = searchBS.getValue('billingschedule',  null, 'GROUP');
                        var bs = searchBS.getText('billingschedule',  null, 'GROUP');

                        // calculate number of recurring months -----------------------------------------
                        var strMonths = bs.substring(0, 3);
                        if(strMonths == 'Mon') {
                            strMonths = '1';
                        }
                        // remove last character from string if a "M" - for billing schedules 1-9
                        else if(strMonths.charAt( strMonths.length - 1 ) == 'M') {
                            strMonths = strMonths.slice(0, -1);
                        }
                        else{}
                        var intMonths = parseInt(strMonths); //  = 3 first digits of the billing schedule text

                        // pull out the number of recurrences for the billing schedule from the custom billing schedule record --------------------------------------------------
                        // TODO - switch to the native billingschedule record
                        var recurrences = 0; // default to 0 in case no result - ???? or 1 ????
                        var arrColumns = new Array();
                        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_bs_recurrences',null,null));
                        var arrFilters = new Array();
                        arrFilters.push(new nlobjSearchFilter('custrecord_clgx_bs_name',null,'is',bs));
                        var searchRecur = nlapiSearchRecord('customrecord_clgx_billing_schedules', null, arrFilters, arrColumns);
                        if(searchRecur != null){
                            recurrences = searchRecur[0].getValue('custrecord_clgx_bs_recurrences',null,null);
                        }

                        // create SO from the proposal by transforming the proposal in SO and inherit all fields
                        var recSONew = nlapiTransformRecord('estimate', proposalid, 'salesorder');
                        var customerid = recSONew.getFieldValue('entity');

                        recSONew.setFieldValue('saleseffectivedate', nlapiDateToString(date));
                        recSONew.setFieldValue('custbody_clgx_ready_to_uplift', 'T');
                        recSONew.setFieldValue('location', locationid);

                        // inherit proposal number with SO prefix and one letter sufix
                        var so = 'SO' + proposal + '-' + intChar.toString(); // increment last number
                        recSONew.setFieldValue('tranid', so);

                        // remove all items from SO to add only the right ones
                        var nbrItems = recSONew.getLineItemCount('item');
                        for (var k = 0; k < nbrItems; k++){
                            recSONew.removeLineItem('item',1);
                        }

                        // add only the items of this pair location/BS
                        var totalNRC = 0;
                        var incrementalAmount=parseFloat(0);
                        if(j == 0){ // only for the first recurring billing schedule - add all NRC items of this location - if new, one pair loc/bs anyway - and if renewal, no NRCs
                            for ( var l = 0; searchItemsNRCs != null && l < searchItemsNRCs.length; l++ ) {
                                var searchItemsNRC = searchItemsNRCs[l];

                                var itemRate = parseFloat(searchItemsNRC.getValue('rate',null,null));
                                var itemQty = searchItemsNRC.getValue('quantity', null, null);
                                if (itemRate == null || itemRate == '' || itemRate == 'NaN'){
                                    itemRate = 0;
                                }
                                if(subsidiary == 6 && currency == 1){ // Canada subsidiary and $US - use itemCalcRate instead of itemRate
                                    itemRate = parseFloat(itemRate) / parseFloat(exchangerate);
                                }
                                incrementalAmount=parseFloat(incrementalAmount)+(parseFloat(itemRate) * parseFloat(searchItemsNRC.getValue('quantity',null,null)));
                                if((searchItemsNRC.getValue('custentity_cologix_service_order','custcol_clgx_so_col_service_id', null)!='')&&(searchItemsNRC.getValue('custentity_cologix_service_order','custcol_clgx_so_col_service_id', null)!=null))
                                {
                                    arraySOSOLD.push(searchItemsNRC.getValue('custentity_cologix_service_order','custcol_clgx_so_col_service_id', null));
                                }

                                recSONew.selectNewLineItem('item');
                                recSONew.setCurrentLineItemValue('item','item', searchItemsNRC.getValue('item',  null, null));
                                recSONew.setCurrentLineItemValue('item','billingschedule', searchItemsNRC.getValue('billingschedule',  null, null));
                                recSONew.setCurrentLineItemValue('item','quantity', searchItemsNRC.getValue('quantity', null, null));
                                recSONew.setCurrentLineItemValue('item','custcol_clgx_qty2print', searchItemsNRC.getValue('quantity', null, null));
                                recSONew.setCurrentLineItemValue('item','rate', itemRate);
                                recSONew.setCurrentLineItemValue('item','amount', parseFloat(itemRate) * parseFloat(itemQty));
                                recSONew.setCurrentLineItemValue('item','description', searchItemsNRC.getValue('memo',  null, null));
                                recSONew.setCurrentLineItemValue('item','price', searchItemsNRC.getValue('pricelevel',  null, null));
                                recSONew.setCurrentLineItemValue('item','taxcode', searchItemsNRC.getValue('taxcode',  null, null));
                                recSONew.setCurrentLineItemValue('item','options', searchItemsNRC.getValue('options',  null, null));
                                //recSONew.setCurrentLineItemValue('item','costestimatetype', searchItemsNRC.getValue('costestimatetype',  null, null));
                                recSONew.setCurrentLineItemValue('item','location', searchItemsNRC.getValue('location',  null, null));
                                recSONew.setCurrentLineItemValue('item','class', searchItemsNRC.getValue('class',  null, null));
                                recSONew.setCurrentLineItemValue('item','custcol_cologix_oppty_item_category', searchItemsNRC.getValue('custcol_cologix_invoice_item_category',  null, null));
                                recSONew.commitLineItem('item');

                                totalNRC += parseFloat(parseFloat(itemRate) * parseFloat(itemQty));
                            }
                        }

                        var totalMRC = 0;
                        var total = 0;
                        var setTerms = 0; // if no recurring items, do not set Service Terms
                        var itemIncremental=new Array();
                        // search items recurring items for the location/BS
                        var arrColumns = new Array();
                        arrColumns.push(new nlobjSearchColumn('rate',null,null));
                        arrColumns.push(new nlobjSearchColumn('taxcode',null,null));
                        arrColumns.push(new nlobjSearchColumn('custentity_cologix_service_order','custcol_clgx_so_col_service_id',null));

                        var arrFilters = new Array();
                        arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof',proposalid));
                        arrFilters.push(new nlobjSearchFilter('location',null,'anyof',locationid));
                        arrFilters.push(new nlobjSearchFilter('billingschedule',null,'anyof',bsid));
                        var searchItems = nlapiSearchRecord('estimate', 'customsearch_clgx_so_renewals_proposal', arrFilters, arrColumns);


                        for ( var m = 0; searchItems != null && m < searchItems.length; m++ ) {
                            var searchItem = searchItems[m];

                            var itemRate = parseFloat(searchItem.getValue('rate',null,null));
                            var itemQty = searchItem.getValue('quantity',null,null);
                            if (itemRate == null || itemRate == '' || itemRate == 'NaN'){
                                itemRate = 0;
                            }
                            if(subsidiary == 6 && currency == 1){ // Canada subsidiary and $US - use itemCalcRate instead of itemRate
                                itemRate = parseFloat(itemRate) / parseFloat(exchangerate);
                            }
                            incrementalAmount=parseFloat(incrementalAmount)+(parseFloat(itemRate) * parseFloat(searchItem.getValue('quantity',null,null)));

                            if((searchItem.getValue('custentity_cologix_service_order','custcol_clgx_so_col_service_id', null)!='')&&(searchItem.getValue('custentity_cologix_service_order','custcol_clgx_so_col_service_id', null)!=null))
                            {

                                arraySOSOLD.push(searchItem.getValue('custentity_cologix_service_order','custcol_clgx_so_col_service_id', null));

                            }

                            recSONew.selectNewLineItem('item');
                            recSONew.setCurrentLineItemValue('item','custcol_clgx_so_col_service_id', searchItem.getValue('custcol_clgx_so_col_service_id',  null, null));
                            recSONew.setCurrentLineItemValue('item','billingschedule', searchItem.getValue('billingschedule',  null, null));
                            recSONew.setCurrentLineItemValue('item','item', searchItem.getValue('item',  null, null));
                            itemIncremental.push(searchItem.getValue('item',  null, null));
                            recSONew.setCurrentLineItemValue('item','quantity', parseFloat(recurrences) * parseFloat(itemQty));
                            recSONew.setCurrentLineItemValue('item','custcol_clgx_qty2print', searchItem.getValue('quantity',null,null));
                            recSONew.setCurrentLineItemValue('item','rate', itemRate);
                            recSONew.setCurrentLineItemValue('item','amount', parseFloat(itemRate) * parseFloat(recurrences) * parseFloat(itemQty));
                            recSONew.setCurrentLineItemValue('item','description', searchItem.getValue('memo',  null, null));
                            recSONew.setCurrentLineItemValue('item','price', searchItem.getValue('pricelevel',  null, null));
                            recSONew.setCurrentLineItemValue('item','taxcode', searchItem.getValue('taxcode',  null, null));
                            recSONew.setCurrentLineItemValue('item','options', searchItem.getValue('options',  null, null));
                            //recSONew.setCurrentLineItemValue('item','costestimatetype', searchItem.getValue('costestimatetype',  null, null));
                            recSONew.setCurrentLineItemValue('item','location', searchItem.getValue('location',  null, null));
                            recSONew.setCurrentLineItemValue('item','class', searchItem.getValue('class',  null, null));
                            recSONew.setCurrentLineItemValue('item','custcol_cologix_oppty_item_category', searchItem.getValue('custcol_cologix_invoice_item_category',  null, null));
                            recSONew.commitLineItem('item');

                            total += parseFloat(parseFloat(itemRate) * parseFloat(recurrences) * parseFloat(itemQty));
                            totalMRC += parseFloat(parseFloat(itemRate) * parseFloat(itemQty));
                            setTerms = 1; // there is at least one reccurring item; set terms
                        }
                        intChar += 1;
                        recSONew.setFieldValue('custbody_clgx_total_recurring', total);
                        recSONew.setFieldValue('custbody_clgx_total_recurring_month', totalMRC);
                        recSONew.setFieldValue('custbody_clgx_total_non_recurring', totalNRC);

                        nlapiLogExecution("DEBUG", "locationid-0", JSON.stringify(locationid));
                        
                        recSONew.setFieldValue('custbody_clgx_consolidate_locations', clgx_return_consolidate_location (locationid));
                        if(setTerms == 1){
                            if(bs == 'Month to Month'){
                                recSONew.setFieldValue('custbody_cologix_biling_terms', 'MTM');
                            }
                            else{
                                recSONew.setFieldValue('custbody_cologix_biling_terms', intMonths.toString());
                            }
                        }

                        arraySOSOLD=_.uniq(arraySOSOLD);
                        itemIncremental=_.uniq(itemIncremental);
                        if(arraySOSOLD.length > 0)
                        {
                            var arrColumnsIn = new Array();
                            var arrFiltersIn = new Array();
                            arrFiltersIn.push(new nlobjSearchFilter('internalid',null,'anyof',arraySOSOLD));
                            arrFiltersIn.push(new nlobjSearchFilter('item',null,'anyof',itemIncremental));
                            arrFiltersIn.push(new nlobjSearchFilter('location',null,'anyof',locationid));
                            arrFiltersIn.push(new nlobjSearchFilter('billingschedule',null,'noneof',17));
                            arrFiltersIn.push(new nlobjSearchFilter('billingschedule',null,'noneof','@NONE@'));
                            arrFiltersIn.push(new nlobjSearchFilter('isinactive','item','is','F'));
                            var searchIncrementalSOs=nlapiSearchRecord('salesorder','customsearch_clgx_ss_incrsosren',arrFiltersIn,arrColumnsIn);
                            if(searchIncrementalSOs!=null)
                            {

                                var searchIncrementalSO = searchIncrementalSOs[0];
                                var columnsIn = searchIncrementalSO.getAllColumns();
                                var oldIcremental = parseFloat(searchIncrementalSO.getValue(columnsIn[0]));

                                var incrementalFinal=parseFloat(incrementalAmount)-parseFloat(oldIcremental);
                                if(incrementalFinal>parseFloat(0))
                                {
                                    recSONew.setFieldValue('custbody_clgx_so_incremental_mrc',incrementalFinal);
                                } else  if(incrementalFinal<parseFloat(0)){

                                    recSONew.setFieldValue('custbody_clgx_so_incremental_mrc',0);
                                }
                            }
                        }

                        var soid = nlapiSubmitRecord(recSONew, false, true);

                        arrNewSOs.push(soid); // add to the array to use later


                        var totals = clgx_transaction_totals (customerid, 'so', soid);


                        var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                        emailAdminBody += '<tr><td>' + location + '</td><td>' + bs + '</td><td><a href="https://1337135.app.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soid + '">' + so + '</a></td><td>' + startExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + execMinutes + '</td><td>'+ usageConsumtion + '</td></tr>';
                        emailUserBody += '<tr><td>' + location + '</td><td>' + bs + '</td><td><a href="https://1337135.app.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soid + '">' + so + '</a></td></tr>';
                        nlapiLogExecution('DEBUG','Created SO - ', location + ' / ' + bs + ' / ' + so);

                        // update back the SOs that renewed the proposal with this soid
                        for ( var n = 0; arrRenewSOs != null && n < arrRenewSOs.length; n++ ) {
                            nlapiSubmitField('salesorder', arrRenewSOs[n], 'custbody_clgx_so_renewed_on_so', soid);
                        }
                    }

                }
                else if (searchBSs == null && searchItemsNRCs != null){ // there is no recurring billing schedules, so create one SO for this location for NRCs

                    var startExec = moment();
                    var execMinutes = (startExec.diff(scriptStart)/60000).toFixed(1);

                    // create SO from the proposal
                    var recSONew = nlapiTransformRecord('estimate', proposalid, 'salesorder');
                    var customerid = recSONew.getFieldValue('entity');

                    recSONew.setFieldValue('saleseffectivedate', nlapiDateToString(date));
                    recSONew.setFieldValue('custbody_clgx_ready_to_uplift', 'T');
                    recSONew.setFieldValue('location', locationid);

                    // remove all items from SO to add only the right ones
                    var nbrItems = recSONew.getLineItemCount('item');
                    for (var o = 0; o < nbrItems; o++){
                        recSONew.removeLineItem('item',1);
                    }

                    // inherit proposal number with SO prefix and one letter sufix
                    var so = 'SO' + proposal + '-' + intChar.toString();
                    recSONew.setFieldValue('tranid', so);

                    for ( var p = 0; searchItemsNRCs != null && p < searchItemsNRCs.length; p++ ) {
                        var searchItemsNRC = searchItemsNRCs[p];

                        var itemRate = parseFloat(searchItemsNRC.getValue('rate',null,null));
                        var itemQty = searchItemsNRC.getValue('quantity',  null, null);
                        var bs = searchItemsNRC.getText('billingschedule',  null, null);

                        if (itemRate == null || itemRate == '' || itemRate == 'NaN'){
                            itemRate = 0;
                        }
                        if(subsidiary == 6 && currency == 1){ // Canada subsidiary and $US - use itemCalcRate instead of itemRate
                            itemRate = parseFloat(itemRate) / parseFloat(exchangerate);
                        }

                        recSONew.selectNewLineItem('item');
                        recSONew.setCurrentLineItemValue('item','item', searchItemsNRC.getValue('item',  null, null));
                        recSONew.setCurrentLineItemValue('item','billingschedule', searchItemsNRC.getValue('billingschedule',  null, null));
                        recSONew.setCurrentLineItemValue('item','quantity', searchItemsNRC.getValue('quantity',  null, null));
                        recSONew.setCurrentLineItemValue('item','custcol_clgx_qty2print', searchItemsNRC.getValue('quantity',  null, null));
                        recSONew.setCurrentLineItemValue('item','rate', itemRate);
                        recSONew.setCurrentLineItemValue('item','amount', parseFloat(itemRate) * parseFloat(itemQty));
                        recSONew.setCurrentLineItemValue('item','description', searchItemsNRC.getValue('memo',  null, null));
                        recSONew.setCurrentLineItemValue('item','price', searchItemsNRC.getValue('pricelevel',  null, null));
                        recSONew.setCurrentLineItemValue('item','taxcode', searchItemsNRC.getValue('taxcode',  null, null));
                        recSONew.setCurrentLineItemValue('item','options', searchItemsNRC.getValue('options',  null, null));
                        //recSONew.setCurrentLineItemValue('item','costestimatetype', searchItemsNRC.getValue('costestimatetype',  null, null));
                        recSONew.setCurrentLineItemValue('item','location', searchItemsNRC.getValue('location',  null, null));
                        recSONew.setCurrentLineItemValue('item','class', searchItemsNRC.getValue('class',  null, null));
                        recSONew.setCurrentLineItemValue('item','custcol_cologix_oppty_item_category', searchItemsNRC.getValue('custcol_cologix_invoice_item_category',  null, null));
                        recSONew.commitLineItem('item');
                        totalNRC += parseFloat(parseFloat(itemRate) * parseFloat(itemQty));
                    }
                    intChar += 1;
                    recSONew.setFieldValue('custbody_clgx_total_recurring', 0);
                    recSONew.setFieldValue('custbody_clgx_total_recurring_month', 0);
                    recSONew.setFieldValue('custbody_clgx_total_non_recurring', totalNRC);

                    nlapiLogExecution("DEBUG", "locationid-1", JSON.stringify(locationid));
                    
                    recSONew.setFieldValue('custbody_clgx_consolidate_locations', clgx_return_consolidate_location (locationid));

                    var soid = nlapiSubmitRecord(recSONew, false, true);

                    var totals = clgx_transaction_totals (customerid, 'so', soid);

                    // only if a renewal, update back the opportunity and the proposal  with the last created SO
                    if(arrRenewSOs != null){
                        nlapiSubmitField('estimate', proposalid, 'custbody_clgx_so_renewed_on_so', soid);
                        if(opptyid != null && opptyid != ''){
                            //nlapiSubmitField('opportunity', opptyid, 'custbody_clgx_so_renewed_on_so', soid);
                            var recOpportunity = nlapiLoadRecord('opportunity', opptyid);
                            recOpportunity.setFieldValue('custbody_clgx_so_renewed_on_so', soid);
                            nlapiSubmitRecord(recOpportunity, false, true);
                        }
                    }

                    var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                    emailAdminBody += '<tr><td>' + location + '</td><td>' + bs + '</td><td><a href="https://1337135.app.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soid + '">' + so + '</a></td><td>' + startExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + execMinutes + '</td><td>'+ usageConsumtion + '</td></tr>';
                    emailUserBody += '<tr><td>' + location + '</td><td>' + bs + '</td><td><a href="https://1337135.app.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soid + '">' + so + '</a></td></tr>';
                }
                else{} // No NRCs and no MRCs
            }

        }
        emailAdminBody += '</table>';
        emailUserBody += '</table>';

// loop new created SOs and create services	for recurring items ----------------------------------------------------------------------------------------------------------------------------------------------------------------

        emailAdminBody += '<h2>Services created for the above service order(s)</h2>';
        emailAdminBody += '<table border="1" cellpadding="5">';
        emailAdminBody += '<tr><td>Service Order</td><td>Service</td><td>Power</td><td>Time</td><td>Minute</td><td>Usage</td></tr>';

        //emailUserBody += '<h2>Services created for the above service order(s)</h2>';
        //emailUserBody += '<table border="1" cellpadding="5">';
        //emailUserBody += '<tr><td>Service Order</td><td>Service</td><td>Power</td></tr>';

        for (var i = 0; i < arrNewSOs.length; i++){

            var startExec = moment();
            var execMinutes = (startExec.diff(scriptStart)/60000).toFixed(1);

            var recSO = nlapiLoadRecord('salesorder', arrNewSOs[i]);
            var sonbr = recSO.getFieldValue('tranid');
            var customer = recSO.getFieldValue('entity');
            var subsidiary = recSO.getFieldValue('subsidiary');
            var location = recSO.getFieldValue('location');

            var nbrItems = recSO.getLineItemCount('item');
            for (var j = 0; j < nbrItems; j++){

                var itemID = recSO.getLineItemValue('item', 'item', j + 1);
                var itemName = recSO.getLineItemText('item', 'item', j + 1);
                var itemCategory = recSO.getLineItemText('item', 'custcol_cologix_invoice_item_category', j + 1);
                var itemServiceID = recSO.getLineItemValue('item', 'custcol_clgx_so_col_service_id', j + 1);
                var itemBillSched = recSO.getLineItemText('item', 'billingschedule', j + 1);
                var itemFacility = parseInt(clgx_return_facilityid(location));
                var itemClass = recSO.getLineItemText('item', 'class', j + 1);
                var qty = recSO.getLineItemValue('item', 'custcol_clgx_qty2print', j + 1);

// Create service for this recurring  item ===========================================================================================

                //if (itemClass.indexOf("Recurring") > -1 && itemBillSched != null && itemBillSched != '' && itemBillSched != 'Non Recurring' && (itemServiceID == null || itemServiceID == '' || itemServiceID == -1) && itemID != 302 && itemID != 518 && itemID != 529) {
                if (itemClass.indexOf("Recurring") > -1 && itemBillSched != null && itemBillSched != '' && itemBillSched != 'Non Recurring' && (itemServiceID == null || itemServiceID == '' || itemServiceID == -1)) {
                	
                    // Get a new ID for the Service to create it
                    var objNewId = nlapiLoadRecord('customrecord_clgx_auto_generated_numbers', 1);
                    var stLastId = objNewId.getFieldValue('custrecord_clgx_auto_generated_number');
                    var intNextId = parseInt(stLastId) + 1;
                    objNewId.setFieldValue('custrecord_clgx_auto_generated_number', parseInt(intNextId));
                    nlapiSubmitRecord(objNewId,false,true);

                    var stId = new String(intNextId);
                    var stPrefix = 'S00000000';
                    var stNextId = stPrefix.substring(0,9-parseInt(stId.length)) + stId;

                    // create service
                    var record = nlapiCreateRecord('job');
                    record.setFieldValue('entityid', stNextId);
                    record.setFieldValue('custentity_cologix_service_order', arrNewSOs[i]);
                    record.setFieldValue('parent', customer);
                    record.setFieldValue('subsidiary', subsidiary);
                    record.setFieldValue('custentity_cologix_facility', itemFacility);
                    record.setFieldText('custentity_cologix_int_prj', 'No');
                    var idRec = nlapiSubmitRecord(record, false, true);

                    var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                    emailAdminBody += '<tr><td><a href="https://1337135.app.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soid + '">' + sonbr + '</a></td><td><a href="https://1337135.app.netsuite.com/app/common/entity/custjob.nl?id=' + idRec + '">' + stNextId + '</a></td><td></td><td>' + startExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + execMinutes + '</td><td>' + usageConsumtion + '</td></tr>';
                    //emailUserBody += '<tr><td><a href="https://1337135.app.netsuite.com/app/accounting/transactions/salesord.nl?id=' + soid + '">' + sonbr + '</a></td><td><a href="https://1337135.app.netsuite.com/app/common/entity/custjob.nl?id=' + idRec + '">' + stNextId + '</a></td><td></td></tr>';
                    nlapiLogExecution('DEBUG', 'Value', '| SO = ' + sonbr + ' | Service = ' + stNextId + ' | Usage = '+ usageConsumtion + ' | Minutes = ' + execMinutes + ' |');

                    // create power records if category is "Power"
                    if(itemCategory == 'Power' && itemID !=  546 && itemID !=  381 && itemID !=  575 && itemID != 780){

                        var arrColumns = new Array();
                        arrColumns.push(new nlobjSearchColumn('custitem_clgx_item_power_amps',null,null));
                        arrColumns.push(new nlobjSearchColumn('custitem_clgx_item_power_volts',null,null));
                        var arrFilters = new Array();
                        arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",itemID));
                        var searchItem = nlapiSearchRecord('item', null, arrFilters, arrColumns);
                        if(searchItem != null){
                            var amps = searchItem[0].getValue('custitem_clgx_item_power_amps',null,null);
                            var volts = searchItem[0].getValue('custitem_clgx_item_power_volts',null,null);
                        }
                        else{
                            var amps = '';
                            var volts = '';
                        }

                        nlapiLogExecution('DEBUG','Ampms/Volts ', '| Amps = ' + amps + ' | Volts = ' + volts + ' |');

                        if(itemName.indexOf('A+B') != -1){ // create 2 power records if item name contains "A+B"
                            for (var k = 0; k < qty; k++){
                                // create power circuit A
                                var record = nlapiCreateRecord('customrecord_clgx_power_circuit');
                                record.setFieldValue('custrecord_cologix_power_service', idRec);
                                record.setFieldValue('custrecord_cologix_power_facility', itemFacility);
                                record.setFieldValue('custrecord_cologix_power_amps', amps);
                                record.setFieldValue('custrecord_cologix_power_volts', volts);
                                record.setFieldValue('custrecord_power_circuit_service_order', arrNewSOs[i]);
                                var idPowerA = nlapiSubmitRecord(record, false, true);

                                var powerName = nlapiLookupField('customrecord_clgx_power_circuit', idPowerA, 'name');
                                //nlapiSubmitField('customrecord_clgx_power_circuit', idPowerA, 'name', clgx_set_char_at(powerName,12,'A'));
                                var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', idPowerA);
                                recPower.setFieldValue('name', clgx_set_char_at(powerName,12,'A'));
                                nlapiSubmitRecord(recPower, false, true);

                                emailAdminBody += '<tr><td></td><td></td><td><a href="https://1337135.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=17&id=' + idPowerA + '">' + powerName + ' / A / ' + idPowerA + '</a></td><td></td><td></td><td></td></tr>';
                                //emailUserBody += '<tr><td></td><td></td><td><a href="https://1337135.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=17&id=' + idPowerA + '">' + powerName + ' / A / ' + idPowerA + '</a></td></tr>';
                                nlapiLogExecution('DEBUG','Created Power ', powerName + ' / A / ' + idPowerA);

                                // create power circuit B
                                var record = nlapiCreateRecord('customrecord_clgx_power_circuit');
                                record.setFieldValue('custrecord_cologix_power_service', idRec);
                                record.setFieldValue('custrecord_cologix_power_facility', itemFacility);
                                record.setFieldValue('custrecord_cologix_power_amps', amps);
                                record.setFieldValue('custrecord_cologix_power_volts', volts);
                                record.setFieldValue('custrecord_power_circuit_service_order', arrNewSOs[i]);
                                var idPowerB = nlapiSubmitRecord(record, false, true);

                                //var powerName = nlapiLookupField('customrecord_clgx_power_circuit', idPower, 'name');
                                //nlapiSubmitField('customrecord_clgx_power_circuit', idPowerB, 'name', clgx_set_char_at(powerName,12,'B'));
                                var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', idPowerB);
                                recPower.setFieldValue('name', clgx_set_char_at(powerName,12,'B'));
                                nlapiSubmitRecord(recPower, false, true);

                                // update pair power on each power
                                //nlapiSubmitField('customrecord_clgx_power_circuit', idPowerA, 'custrecord_clgx_dcim_pair_power', idPowerB);
                                //nlapiSubmitField('customrecord_clgx_power_circuit', idPowerB, 'custrecord_clgx_dcim_pair_power', idPowerA);

                                var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', idPowerA);
                                recPower.setFieldValue('custrecord_clgx_dcim_pair_power', idPowerB);
                                nlapiSubmitRecord(recPower, false, true);

                                var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', idPowerB);
                                recPower.setFieldValue('custrecord_clgx_dcim_pair_power', idPowerA);
                                nlapiSubmitRecord(recPower, false, true);


                                emailAdminBody += '<tr><td></td><td></td><td><a href="https://1337135.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=17&id=' + idPowerB + '">' + powerName + ' / B / ' + idPowerB + '</a></td><td></td><td></td><td></td></tr>';
                                //emailUserBody += '<tr><td></td><td></td><td><a href="https://1337135.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=17&id=' + idPowerB + '">' + powerName + ' / B / ' + idPowerB + '</a></td></tr>';
                                nlapiLogExecution('DEBUG','Created Power ', powerName + ' / B / ' + idPowerB);
                            }
                        }
                        else if (itemName.indexOf('A+B') == -1 && itemName.indexOf('Kilowatt') == -1){
                            for (var k = 0; k < qty; k++){ // create 1 power record
                                // create power circuit P
                                var record = nlapiCreateRecord('customrecord_clgx_power_circuit');
                                record.setFieldValue('custrecord_cologix_power_service', idRec);
                                record.setFieldValue('custrecord_cologix_power_facility', itemFacility);
                                record.setFieldValue('custrecord_cologix_power_amps', amps);
                                record.setFieldValue('custrecord_cologix_power_volts', volts);
                                record.setFieldValue('custrecord_power_circuit_service_order', arrNewSOs[i]);
                                var idPowerP = nlapiSubmitRecord(record, false, true);

                                var powerName = nlapiLookupField('customrecord_clgx_power_circuit', idPowerP, 'name');
                                //nlapiSubmitField('customrecord_clgx_power_circuit', idPowerP, 'name', clgx_set_char_at(powerName,12,'P'));
                                var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', idPowerP);
                                recPower.setFieldValue('name', clgx_set_char_at(powerName,12,'P'));
                                nlapiSubmitRecord(recPower, false, true);

                                emailAdminBody += '<tr><td></td><td></td><td><a href="https://1337135.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=17&id=' + idPowerP + '">' + powerName + ' / P / ' + idPowerP + '</a></td><td></td><td></td><td></td></tr>';
                                //emailUserBody += '<tr><td></td><td></td><td><a href="https://1337135.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=17&id=' + idPowerP + '">' + powerName + ' / P / ' + idPowerP + '</a></td></tr>';
                                nlapiLogExecution('DEBUG','Created Power ', powerName + ' / P / ' + idPowerP);

                            }
                        }
                        else{}
                    }
                    nlapiLogExecution('DEBUG','Created Service ', stNextId + ' / ' + idRec);

                    // link the item to the new service
                    recSO.setLineItemValue('item', 'custcol_clgx_so_col_service_id', j + 1, idRec);
                }
                else{}

            }
            var soid = nlapiSubmitRecord(recSO, false, true);
        }

        // mark proposal as processed
        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter('custrecord_clgx_proposals_to_sos_propid',null,'anyof',proposalid));
        var searchProposals = nlapiSearchRecord('customrecord_clgx_proposals_to_sos', null, arrFilters, arrColumns);

        for ( var i = 0; searchProposals != null && i < searchProposals.length; i++ ) {
            var searchProposal = searchProposals[i];
            var recid = searchProposal.getValue('internalid',null,null);
            nlapiSubmitField('customrecord_clgx_proposals_to_sos', recid, 'custrecord_clgx_proposals_to_sos_done', 'T');
            //nlapiDeleteRecord('customrecord_clgx_proposal_to_create_sos', recid );
        }

        var endScript = moment();
        emailAdminBody += '</table><br>';
        emailAdminBody += 'End : ' + endScript.format('M/D/YYYY h:mm:ss a') + '<br>';
        emailAdminBody += 'Total execution minutes : ' + (endScript.diff(startScript)/60000).toFixed(1) + '<br>';
        var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
        emailAdminBody += 'Total usage : ' + usageConsumtion;
        emailAdminBody += '<br><br>';

        //emailUserBody += '</table><br>';
        emailUserBody += 'End : ' + endScript.format('M/D/YYYY h:mm:ss a') + '<br>';
        emailUserBody += 'Total execution minutes : ' + (endScript.diff(startScript)/60000).toFixed(1);
        emailUserBody += '<br><br>';

        //nlapiSendEmail(userid,71418,emailAdminSubject,emailAdminBody,null,null,null,null);
        //nlapiSendEmail(userid,206211,emailAdminSubject,emailAdminBody,null,null,null,null);

        nlapiSendEmail(userid,userid,emailUserSubject,emailUserBody,null,null,null,null,true);

        nlapiLogExecution('DEBUG','Finished Execution', '|-------------------------- Finished Scheduled Script --------------------------|');
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
