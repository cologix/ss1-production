nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Service_Order_Services.js
//	Script Name:	CLGX_SS_Service_Order_Services
//	Script Id:		customscript_clgx_ss_so_services
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Service Order
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		02/01/2013
//-------------------------------------------------------------------------------------------------
function scheduled_sos_services(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();

        var reportSubject = 'Update Services Report';
        var  reportBody = '<table border="1" cellpadding="5">';
        reportBody += '<tr><td>SO</td><td>Service</td><td>Index</td><td>Time</td><td>Minute</td><td>Usage</td></tr>';
        
        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null).setSort(true));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_services_to_update_serv',null,null));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_services_to_update_so',null,null));
        var arrFilters = new Array();
        var searchResults = nlapiSearchRecord('customrecord_clgx_services_to_update', null, arrFilters, arrColumns);

        var scriptStart = moment();
        for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
        	
        	var startExec = moment();
        	var execMinutes = (startExec.diff(scriptStart)/60000).toFixed(1);
        	
            if ( (context.getRemainingUsage() <= 1000 || execMinutes > 50) && (i+1) < searchResults.length ){
               var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
               if ( status == 'QUEUED' ) {
            	   nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
                   break; 
               }
            }

        	var searchResult = searchResults[i];
        	var internalid = searchResult.getValue('internalid',null,null);
        	var servid = searchResult.getValue('custrecord_clgx_services_to_update_serv',null,null);
        	var soid = searchResult.getValue('custrecord_clgx_services_to_update_so',null,null);

        	nlapiSubmitField('job', servid, 'custentity_cologix_service_order', soid);
        	
			var recService = nlapiLoadRecord('job', servid, null, null);

			var nbrXcs = recService.getLineItemCount('recmachcustrecord_cologix_xc_service');
        	for ( var j = 0; j < nbrXcs; j++ ) {
        		var id = recService.getLineItemValue('recmachcustrecord_cologix_xc_service', 'id', j + 1);
        		nlapiSubmitField('customrecord_cologix_crossconnect', id, 'custrecord_xconnect_service_order', soid);
        	}
        	var nbrVxcs = recService.getLineItemCount('recmachcustrecord_cologix_vxc_service');
        	for ( var j = 0; j < nbrVxcs; j++ ) {
        		var id = recService.getLineItemValue('recmachcustrecord_cologix_vxc_service', 'id', j + 1);
        		nlapiSubmitField('customrecord_cologix_vxc', id, 'custrecord_cologix_service_order', soid);
        	}
        	var nbrSpaces = recService.getLineItemCount('recmachcustrecord_cologix_space_project');
        	for ( var j = 0; j < nbrSpaces; j++ ) {
        		var id = recService.getLineItemValue('recmachcustrecord_cologix_space_project', 'id', j + 1);
        		nlapiSubmitField('customrecord_cologix_space', id, 'custrecord_space_service_order', soid);
        	}
        	var nbrPowers = recService.getLineItemCount('recmachcustrecord_cologix_power_service');
        	for ( var j = 0; j < nbrPowers; j++ ) {
        		var id = recService.getLineItemValue('recmachcustrecord_cologix_power_service', 'id', j + 1);
        		//nlapiSubmitField('customrecord_clgx_power_circuit', id, 'custrecord_power_circuit_service_order', soid);
    			var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit', id);
    			recPower.setFieldValue('custrecord_power_circuit_service_order', soid);
    			nlapiSubmitRecord(recPower, false, true);
        	}
        	var nbrOobs = recService.getLineItemCount('recmachcustrecord_clgx_oob_service_id');
        	for ( var j = 0; j < nbrOobs; j++ ) {
        		var id = recService.getLineItemValue('recmachcustrecord_clgx_oob_service_id', 'id', j + 1);
        		nlapiSubmitField('customrecord_clgx_oob_facility', id, 'custrecord_clgx_oob_service_order', soid);
        	}
        	
        	nlapiDeleteRecord('customrecord_clgx_services_to_update', internalid);
        	
        	var index = i + 1;
        	var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
        	nlapiLogExecution('DEBUG', 'Value', '| SO = ' + soid + ' | Service = ' + servid + ' | Index = ' + index + ' of ' + searchResults.length +  ' | Usage = '+ usageConsumtion + ' | Minutes = ' + execMinutes + ' |');
        	reportBody += '<tr><td>' + soid + '</td><td>' + servid + '</td><td>' + index + ' of ' + searchResults.length +  '</td><td>' + startExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + execMinutes + '</td><td>'+ usageConsumtion + '</td></tr>';
        }
        
		reportBody += '</table>';
		
		clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_so_admin", reportSubject, reportBody);
		//nlapiSendEmail(71418,71418,reportSubject,reportBody,null,null,null,null,true);
		
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
