nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Expiring_SOs_JSON.js
//	Script Name:	CLGX_SL_Expiring_SOs_JSON
//	Script Id:		customscript_clgx_sl_exp_sos_json
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/15/2013
//-------------------------------------------------------------------------------------------------
function suitelet_expiring_sos_json (request, response){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var searchInactives = nlapiSearchRecord('transaction', 'customsearch_clgx_sos_with_inactives', null, null);
    	var arrSOs = new Array();
    	for ( var i = 0; searchInactives != null && i < searchInactives.length; i++ ) {
    		var searchInactive = searchInactives[i];
    		var soid = searchInactive.getValue('internalid', null, 'GROUP');
    		arrSOs.push(soid);
    	}
        var arrColumns = new Array();
        var arrFilters = new Array();
        if(arrSOs.length > 0){
        	arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",arrSOs));
        }
        var searchResults = nlapiSearchRecord('transaction', 'customsearch_clgx_contracts_renewals_3', arrFilters, arrColumns);

        var jsonObj = new Object();
        //jsonObj["recordtype"] = recordtype;
        jsonObj["success"] = true;
        if(arrSOs.length > 0){
        	jsonObj["total"] = searchResults.length;
        }
        else{
        	jsonObj["total"] = 0;
        }
    	var rowsArr = new Array();
        for ( var j = 0; searchResults != null && j < searchResults.length; j++ ) {

        	var initialTime = moment();
        	
        	var searchResult = searchResults[j];
        	var internalid = searchResult.getValue('internalid',null,'GROUP');
        	var discoTerms = searchResult.getText('custentity_cglx_disco_notice_terms','customerMain','GROUP');
        	var renewTerms = searchResult.getText('custentity_clgx_cust_renewal_term','customerMain','GROUP');
        	var billingTerms = searchResult.getValue('custbody_cologix_biling_terms',null,'GROUP');
        	var contractStartDate = searchResult.getValue('custbody_cologix_so_contract_start_dat',null,'GROUP');
        	var contractEndDate = searchResult.getValue('enddate',null,'GROUP');
        	var number = searchResult.getValue('tranid',null,'GROUP');

        	// calculated fields
        	var contractEndDateMom = new moment(nlapiStringToDate(contractEndDate));
        	var todayMom = moment();
        	var difMonths = Math.ceil(todayMom.diff(contractEndDateMom, 'months', true));
        	var temp = 'ini';
        	
        	if ((billingTerms == 'MTM' || billingTerms == 'Month to Month') && (renewTerms != 'See MSA' && discoTerms != 'See MSA')){
        		temp='MTM';
        		// > 1900 SOs - Month to Month in contract
        		var renewCycle = difMonths;
        		var renewCycleLength = 1;
        		var addMonths = renewCycle * renewCycleLength;
        		var renewEndDate = contractEndDate;
	    		var renewStartDate = '';
	    		var expiresIn = 0;
	    		discoTerms = '';
	        	actionBy = 0;
        	}
        	else if (billingTerms != 'MTM' && billingTerms != '' && billingTerms != null && (renewTerms == 'See MSA' || discoTerms == 'See MSA') && contractEndDate != ''){
        		// > 106 SOs - not MTM in contract and 'See MSA' in renewal terms
	    		if(contractEndDateMom.isBefore(todayMom, 'day')){
	    			// first cycle expired, Month to Month is starting
		        	var renewCycleLength = 1;
		        	var renewCycle = 1;
		        	var renewEndDate = contractEndDate;
		    		var renewStartDate = '';
		    		var expiresIn = 0;
		    		var actionBy = 0;
	    		}
	    		else{ // still in the first cycle
		        	var renewCycleLength = parseInt(billingTerms);
		        	var renewCycle = 1;
		        	var addMonths = renewCycle * renewCycleLength;
		        	var renewEndDate = contractEndDate;
		    		var renewStartDate = '';
		    		var expiresIn = 0 - Math.ceil(todayMom.diff(renewEndDate, 'days', true));
		        	var actionBy = expiresIn - 90;
		        	if(actionBy < 0){
		        		actionBy = 0;
		        	}
	    		}
    		}
        	else if(billingTerms != 'MTM' && billingTerms != '' && billingTerms != null && renewTerms == 'Month to Month' && (renewTerms != 'See MSA' && discoTerms != 'See MSA') && contractEndDate != ''){
        		// > 233 SOs - not MTM in contract but going to month to month on renewal
        		if(contractEndDateMom.isBefore(todayMom, 'day')){
        			// first cycle expired, Month to Month is starting
    	        	var renewCycleLength = 1;
    	        	var renewCycle = 1;
    	        	var renewEndDate = contractEndDate;
    	    		var renewStartDate = '';
    	    		var expiresIn = 0;
    	    		var discoTerms = '';
    	    		var actionBy = 0;
        		}
        		else{ // still in the first cycle
    	        	var renewCycleLength = parseInt(billingTerms);
    	        	//var renewCycle = 1 + Math.ceil(difMonths/renewCycleLength);
    	        	var renewCycle = 1;
    	        	var addMonths = renewCycle * renewCycleLength;
    	        	var renewEndDate = contractEndDate;
    	    		//var renewStartDate = contractEndDateMom.add('days', 1).format('M/D/YYYY');
    	    		var renewStartDate = '';
    	    		var expiresIn = 0 - Math.ceil(todayMom.diff(renewEndDate, 'days', true));
    	        	var actionBy = expiresIn - get_disco_days (discoTerms);
    	        	if(actionBy < 0){
    	        		actionBy = 0;
    	        	}
        		}
        	}
        	else if (billingTerms != 'MTM' && billingTerms != '' && billingTerms != null && renewTerms == 'Original Contract Term' && (renewTerms != 'See MSA' && discoTerms != 'See MSA') && contractEndDate != ''){
        		//temp='here';
        		// > 227 SOs - not MTM in contract and 'Original Contract Term' in renewal terms
        		if(contractEndDateMom.isBefore(todayMom, 'day')){ // after the first cycle
    	        	var renewCycleLength = parseInt(billingTerms);
    	        	var renewCycle = 1 + Math.ceil(difMonths/renewCycleLength);
    	        	var addMonths = (renewCycle -1 ) * renewCycleLength;
    	        	var renewEndDate = contractEndDateMom.add('months', addMonths).format('M/D/YYYY');
    	    		var renewStartDate = contractEndDateMom.add('days', 1).format('M/D/YYYY');
    	    		var expiresIn = 0 - Math.ceil(todayMom.diff(renewEndDate, 'days', true));
    	        	var actionBy = expiresIn - get_disco_days (discoTerms);
    	        	if(actionBy < 0){
    	        		actionBy = 0;
    	        	}
        		}
        		else{ // still in the first cycle
        			var renewCycleLength = parseInt(billingTerms);
    	        	var renewCycle = 1;
    	        	var addMonths = renewCycle * renewCycleLength;
    	        	var renewEndDate = contractEndDateMom.format('M/D/YYYY');
    	    		var renewStartDate = contractEndDateMom.add('days', 1).format('M/D/YYYY');
    	    		var expiresIn = 0 - Math.ceil(todayMom.diff(renewEndDate, 'days', true));
    	        	var actionBy = expiresIn - get_disco_days (discoTerms);
    	        	if(actionBy < 0){
    	        		actionBy = 0;
    	        	}
        		}
        	}
        	else if (billingTerms != 'MTM' && billingTerms != '' && billingTerms != null && renewTerms == '- None -' && (renewTerms != 'See MSA' && discoTerms != 'See MSA') && contractEndDate != ''){
        		//temp='here';
        		// > 50 SOs - not MTM in contract and 'Original Contract Term' in renewal terms
        		if(contractEndDateMom.isBefore(todayMom, 'day')){ // after the first cycle
        			temp='beforeToday';
    	        	var renewCycleLength = parseInt(billingTerms);
    	        	var renewCycle = 1;
    	        	var addMonths = (renewCycle -1 ) * renewCycleLength;
    	        	var renewEndDate = contractEndDate;
    	    		//var renewStartDate = contractEndDateMom.add('days', 1).format('M/D/YYYY');
    	    		var renewStartDate = '';
    	    		var expiresIn = 0;
    	        	actionBy = 0;
        		}
        		else{ // still in the first cycle
        			temp='afterToday';
        			var renewCycleLength = parseInt(billingTerms);
    	        	var renewCycle = 1;
    	        	var addMonths = renewCycle * renewCycleLength;
    	        	var renewEndDate = contractEndDate;
    	    		//var renewStartDate = contractEndDateMom.add('days', 1).format('M/D/YYYY');
    	    		var renewStartDate = '';
    	    		var expiresIn = 0 - Math.ceil(todayMom.diff(renewEndDate, 'days', true));
    	        	var actionBy = expiresIn - get_disco_days (discoTerms);
    	        	if(actionBy < 0){
    	        		actionBy = 0;
    	        	}
        		}
        	}
        	else if(billingTerms != 'MTM' && billingTerms != '' && billingTerms != null && (renewTerms == '12 Months' || renewTerms == '36 Months') && (renewTerms != 'See MSA' && discoTerms != 'See MSA') && contractEndDate != ''){
        		var temp = '12M';
        		// > 679 SOs - not MTM in contract and '12 Months' in renewal terms
        		if(renewTerms == '12 Months'){
        			var renewCycleLength = 12;
        		}
        		if(renewTerms == '24 Months'){
        			var renewCycleLength = 24;
        		}
        		else{
        			var renewCycleLength = 36;
        		}
        		if(contractEndDateMom.isBefore(todayMom, 'day')){ // after the first cycle
    	        	var renewCycle = 1 + Math.ceil(difMonths/renewCycleLength);
    	        	var addMonths = (renewCycle -1 ) * renewCycleLength;
    	        	var renewEndDate = contractEndDateMom.add('months', addMonths).format('M/D/YYYY');
    	    		var renewStartDate = contractEndDateMom.add('days', 1).format('M/D/YYYY');
    	    		var expiresIn = 0 - Math.ceil(todayMom.diff(renewEndDate, 'days', true));
    	        	var actionBy = expiresIn - get_disco_days (discoTerms);
    	        	if(actionBy < 0){
    	        		actionBy = 0;
    	        	}
        		}
        		else{ // still in the first cycle
        			var renewCycleLength = parseInt(billingTerms);
    	        	var renewCycle = 1;
    	        	var addMonths = renewCycle * renewCycleLength;
    	        	var renewEndDate = contractEndDateMom.format('M/D/YYYY');
    	    		var renewStartDate = contractEndDateMom.add('days', 1).format('M/D/YYYY');
    	    		var expiresIn = 0 - Math.ceil(todayMom.diff(renewEndDate, 'days', true));
    	        	var actionBy = expiresIn - get_disco_days (discoTerms);
    	        	if(actionBy < 0){
    	        		actionBy = 0;
    	        	}
        		}
        	}
        	else{
        		temp = 'else';
	        	var renewCycleLength = 1;
	        	var renewCycle = 1;
	        	var renewEndDate =  contractEndDate;
	    		var renewStartDate = '';
	    		var expiresIn = 0;
	    		var discoTerms = '';
	    		var actionBy = 0;
        	}
        	
        	
        	var rowObj = new Object();
        	rowObj['id'] = internalid;
        	rowObj['discoTerms'] = discoTerms;
        	rowObj['renewTerms'] = renewTerms;
        	rowObj['billingTerms'] = billingTerms;
        	rowObj['contractStartDate'] = contractStartDate;
        	rowObj['contractEndDate'] = contractEndDate;
        	rowObj['renewCycleLength'] = renewCycleLength;
        	rowObj['renewCycle'] = renewCycle;
        	rowObj['renewStartDate'] = renewStartDate;
        	rowObj['addMonths'] = addMonths;
        	rowObj['renewEndDate'] = renewEndDate; 
        	rowObj['actionBy'] = actionBy;
        	rowObj['expiresIn'] = expiresIn;
        	rowObj['number'] = number;
        	//rowObj['temp'] = temp;
        	rowsArr.push(rowObj);

        	/*
        	if(endDate != null && endDate != ''){
            	var fields = ['custbody_clgx_renewal_start_date','custbody_clgx_renewal_end_date','custbody_clgx_contract_cycle','custbody_clgx_mu_flag'];
    			var values = [startDate, endDate, cycle, 'T'];
    			nlapiSubmitField('salesorder', internalid, fields, values);
        	}
			*/
        }
        jsonObj["rows"] = rowsArr;
        
        response.write(JSON.stringify(jsonObj));
        
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

// =======================================================================

function get_contract_months (renewTerms, billingTerms){
	// 12 Months	 	2	 
	// 36 Months	 	4	 
	// Month to Month	 	1	 
	// Original Contract Term	 	3	 
	// See MSA	 	5
	var months = 1;
	switch(renewTerms) {
	case '12 Months':
		months = 12;
		break;
	case '24 Months':
		months = 24;
		break;
	case '36 Months':
		months = 36;
		break;
	case 'Month to Month':
		months = 1;
		break;
	case 'Original Contract Term':
		switch(billingTerms) {
		case 'MTM':
			months = 1;
			break;
		case '':
			months = 1;
			break;
		default:
			months = parseInt(billingTerms);
		}
	case 'See MSA':
		months = 3;
		break;
	default:
		months = 1;
	}
	return months;
}

function get_disco_days (discoTerms){
	// 30 Days	 	1	 
	// 60 Days	 	2	 
	// 90 Days	 	3	 
	// 120 Days	 	4	 
	// See MSA	 	5 - 90 Days
	var discoDays = 0;
	switch(discoTerms) {
	case '30 Days':
		discoDays = 30;
		break;
	case '60 Days':
		discoDays = 60;
		break;
	case '90 Days':
		discoDays = 90;
		break;
	case '120 Days':
		discoDays = 120;
		break;
	case 'See MSA':
		discoDays = 90;
		break;
	default:
		discoDays = 0;
	}
	return discoDays;
}

function getFormatedDate(date2format){
    
	var date = nlapiStringToDate(date2format);
    var month = parseInt(date.getMonth()) + 1;
    var day = date.getDate();
    var year = date.getFullYear();
    
    if(parseInt(month) < 10){
        month = '0' + month;
    }
    
    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = year + '-' + month + '-' + day;
    
    return formattedDate;
}