nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Payment_File_Administration.js
//	Script Name:	CLGX_SU_Payment_File_Administration
//	Script Id:		customscript_CLGX_SU_Payment_File_Administration
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Payment Batch(Custom Record) - Sandbox | Production
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		26/06/2017
//-------------------------------------------------------------------------------------------------

function beforeLoad(type, form) {
    try {

        var currentContext = nlapiGetContext();
        var  roleid = currentContext.getRole();
        form.getField('custrecord_2663_notification_subject').setDisplayType('inline');
         
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
function beforSubmit(type) {
    try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
        //    Send a notification to Danielle when the batch are fully approved
//-----------------------------------------------------------------------------------------------------------------


        if (type==='create') {
            nlapiSendEmail(206211,778516,'The batch are fully approved' , 'Test',null,null,null,null);
        }
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

}
function afterSubmit(type) {
    try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
        //    Send a notification to Danielle when the batch are fully approved
//-----------------------------------------------------------------------------------------------------------------


        if (type==='create') {
            nlapiSendEmail(206211,778516,'The batch are fully approved' , 'Test',null,null,null,null);
        }
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

}