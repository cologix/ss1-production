nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Company_Payment_Batch.js
//	Script Name:	CLGX_SU_Company_Payment_Batch
//	Script Id:		customscript_clgx_su_company_payment_batch
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Payment Batch(Custom Record) - Sandbox | Production
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		26/06/2017
//-------------------------------------------------------------------------------------------------
function afterSubmit(type) {
    try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
        //    Send a notification to Danielle when the batch are fully approved
//-----------------------------------------------------------------------------------------------------------------
        var status = nlapiGetFieldValue('custrecord_2663_batch_status');
        nlapiLogExecution('DEBUG','status', status);

        if (status==4) {
            nlapiSendEmail(206211,778516,'The batch are fully approved' , 'Test',null,null,null,null);
        }
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

}