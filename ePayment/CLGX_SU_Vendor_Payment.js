nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Vendor_Payment.js
//	Script Name:	CLGX_SU_Vendor_Payment
//	Script ID:		customscript_clgx_su_vendor_payment
//	Script Type:	User Event Script
//	@author:        Catalina Taran - catalina.taran@cologix.com
//                  Alex Guzenski - alex.guzenski@cologix.com
//	Created:		7/17/2017
//  Updated:        8/23/2017
//-------------------------------------------------------------------------------------------------

/**
 * Returns a date formatted as mm/dd/yyyy
 *
 * @return string
 */
function getCurrentDate() {
    var d = new Date();
    return ((d.getUTCMonth() + 1) + "/" + (d.getUTCDate()) + "/" + d.getUTCFullYear());
}


/**
 * Checks the memo field format.
 *
 * @param {string} memoText - The memo field text.
 * @return boolean
 */
function checkMemoFormat(memoText) {
    if(memoText != null && memoText != "") {
        if(memoText.match(/([0-9]{7,8})\w+\/([0-9])/) != null) {
            return true;
        }
    }

    return false;
}


/**
 * Returns the formatted vendor name if the character count is higher than 25.
 *
 * @param {string} vendorName - The vendor name.
 * @return string
 */
function formatVendorName(vendorName) {
    if(vendorName.length >= 25) {
        return vendorName.substring(0, 19) + " " +  vendorName.substring((vendorName.length - 5), vendorName.length);
    }

    return vendorName;
}


/**
 * Returns a transaction id generated from the vendor name.
 *
 * @param {string} vendorName - The vendor name.
 * @return string
 */
function createTranID(vendorName, memoText,pfaid) {
    if(vendorName != null && vendorName != "") {
        var fv = formatVendorName(vendorName);
        var pfa=nlapiLoadRecord('customrecord_2663_file_admin',pfaid);
        var pdate=pfa.getFieldValue('custrecord_2663_process_date');
        return "ACH " + pdate + " " + fv;
    }
}

function beforeSubmit(type, form) {
    try {
        var vendor = nlapiGetFieldValue("entity");
        var memo   = nlapiGetFieldValue("memo");
        var pfaid   = nlapiGetFieldValue("custbody_9997_pfa_record");
        if(checkMemoFormat(memo) == true && pfaid!='' && pfaid!=null) {
            nlapiSetFieldValue("tranid", createTranID(vendor, memo,pfaid));
        }
    } catch (error) {
        if (error.getDetails != undefined) {
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        } else {
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}