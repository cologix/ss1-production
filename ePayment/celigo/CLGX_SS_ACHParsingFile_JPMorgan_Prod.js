//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_ACHParsingFile_JPMorgan.js
//	Script Name:	CLGX_SS_ACHParsingFile_JPMorgan
//	Script Id:		customscript_CLGX_SS_ACHParsingFile_JPMorgan
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		21/10/2018
//-------------------------------------------------------------------------------------------------

function sc_caseReports() {
    try {
        var filters = new Array();
        filters[0] = new nlobjSearchFilter('internalid', null, 'is', 9935114);
        nlapiLogExecution('DEBUG', 'Start execution', 'Start execution');

        var columns = new Array();
        var filename = new nlobjSearchColumn('name', 'file');
        var fileid = new nlobjSearchColumn('internalid', 'file');

        columns[0] = filename;
        columns[1] = fileid;

        var searchResult = nlapiSearchRecord('folder', null, filters, columns);
        if (searchResult) {
            for (var l = 0; l < searchResult.length; l++) {

                var name = searchResult[l].getValue(filename);
                var id = searchResult[l].getValue(fileid);
                if (name.indexOf('csv') === -1 ){
                    if(name.indexOf('ACK') !== -1){
                        var caseSubj='File Received Successfully by JPM';

                    }
                    else{
                        var caseSubj='JPM ACH File Upload Unsuccessful';
                    }
                    var arrColumnsC = new Array();
                    var arrFiltersC = new Array();
                    arrFiltersC.push(new nlobjSearchFilter('internalid', 'attachments', 'anyof', id));
                    var rows = nlapiSearchRecord('message','customsearch6571', arrFiltersC, arrColumnsC);
                    if (rows == null) {
                        var file = nlapiLoadFile(parseInt(id));
                        var bodyFile = file.getValue();
                        var name = file.getName();
                        var folder = file.getFolder();
                        var decodedString = decode_base64(bodyFile);
                        var arr = decodedString.split('\n');
                        var text = '';
                        for (var i = 0; i < arr.length; i++) {

                            var RECORD_TYPE_CODE = arr[i].substring(0, 1);
                            if (RECORD_TYPE_CODE == 1) {
                                text += 'PRIORITY CODE,IMMEDIATE DESTINATION,IMMEDIATE ORIGIN,FILE CREATION DATE(YY/MM/DD),FILE CREATION TIME,FILE ID MODIFIER,RECORD SIZE,BLOCKING FACTOR,FORMAT CODE,IMMEDIATE DESTINATION NAME,IMMEDIATE ORIGIN NAME,REFERENCE_CODE\n';
                                var PRIORITY_CODE = arr[i].substring(1, 3);
                                text += PRIORITY_CODE + ',';
                                var IMMEDIATE_DESTINATION = arr[i].substring(3, 13);
                                text += IMMEDIATE_DESTINATION + ',';
                                var IMMEDIATE_ORIGIN = arr[i].substring(13, 23);
                                text += IMMEDIATE_ORIGIN + ',';
                                var FILE_CREATION_DATE = arr[i].substring(23, 29);
                                var FILE_CREATION_DATE1 = (FILE_CREATION_DATE.toString()).substring(0, 2);
                                text += (FILE_CREATION_DATE.toString()).substring(0, 2) + '/' + (FILE_CREATION_DATE.toString()).substring(2, 4) + '/' + (FILE_CREATION_DATE.toString()).substring(4, 6) + ',';
                                var FILE_CREATION_TIME = arr[i].substring(29, 33);
                                text += (FILE_CREATION_TIME.toString()).substring(0, 2) + ':' + (FILE_CREATION_TIME.toString()).substring(2, 4) + ',';
                                var FILE_ID_MODIFIER = arr[i].substring(33, 34);
                                text += FILE_ID_MODIFIER + ',';
                                var RECORD_SIZE = arr[i].substring(34, 37);
                                text += RECORD_SIZE + ',';
                                var BLOCKING_FACTOR = arr[i].substring(37, 39);
                                text += BLOCKING_FACTOR + ',';
                                var FORMAT_CODE = arr[i].substring(39, 40);
                                text += FORMAT_CODE + ',';
                                var IMMEDIATE_DESTINATION_NAME = arr[i].substring(40, 63);
                                text += IMMEDIATE_DESTINATION_NAME + ',';
                                var IMMEDIATE_ORIGIN_NAME = arr[i].substring(63, 86);
                                text += IMMEDIATE_ORIGIN_NAME + ',';
                                var REFERENCE_CODE = arr[i].substring(86, 94);
                                text += REFERENCE_CODE;
                                text += '\n';
                                text += '\r';
                            }
                            else if (RECORD_TYPE_CODE == 5) {
                                text += '\nSERVICE CLASS CODE,COMPANY NAME,COMPANY DISCRETIONARY DATA,COMPANY IDENTIFICATION,STANDARD ENTRY CLASS CODE,COMPANY ENTRY DESCRIPTION,COMPANY DESCRIPTIVE DATE,EFFECTIVE ENTRY DATE(YY/MM/DD),SETTLEMENT DATE(JULIAN),ORIGINATOR STATUS CODE,ORIGINATING DFI IDENTIFICATION BATCH NUMBER\n';


                                var SERVICE_CLASS_CODE = arr[i].substring(1, 4);
                                text += SERVICE_CLASS_CODE + ',';
                                var COMPANY_NAME = arr[i].substring(4, 20);
                                text += COMPANY_NAME + ',';
                                var COMPANY_DISCRETIONARY_DATA = arr[i].substring(20, 40);
                                text += COMPANY_DISCRETIONARY_DATA + ',';
                                var COMPANY_IDENTIFICATION = arr[i].substring(40, 50);
                                text += COMPANY_IDENTIFICATION + ',';
                                var STANDARD_ENTRY_CLASS_CODE = arr[i].substring(50, 53);
                                text += STANDARD_ENTRY_CLASS_CODE + ',';
                                var COMPANY_ENTRY_DESCRIPTION = arr[i].substring(53, 63);
                                text += COMPANY_ENTRY_DESCRIPTION + ',';
                                var COMPANY_DESCRIPTIVE_DATE = arr[i].substring(63, 69);
                                text += COMPANY_DESCRIPTIVE_DATE + ',';
                                var EFFECTIVE_ENTRY_DATE = arr[i].substring(69, 75);
                                text += (EFFECTIVE_ENTRY_DATE.toString()).substring(0, 2) + '/' + (EFFECTIVE_ENTRY_DATE.toString()).substring(2, 4) + '/' + (EFFECTIVE_ENTRY_DATE.toString()).substring(4, 6) + ',';
                                var SETTLEMENT_DATE_JULIAN = arr[i].substring(75, 78);
                                text += SETTLEMENT_DATE_JULIAN + ',';
                                var ORIGINATOR_STATUS_CODE = arr[i].substring(78, 79);
                                text += ORIGINATOR_STATUS_CODE + ',';
                                var ORIGINATING_DFI_IDENTIFICATION_BATCH_NUMBER = arr[i].substring(79, 94);
                                text += ORIGINATING_DFI_IDENTIFICATION_BATCH_NUMBER;


                                text += '\n';
                                text += '\n';
                            }
                            else if (RECORD_TYPE_CODE == 6) {
                                text += 'TRANSACTION CODE,RECEIVING DFI ID,DFI ACCOUNT NUMBER,DOLLAR AMOUNT($$$$$$$$¢¢),INDIVIDUAL IDENTIFICATION NUMBER,INDIVIDUAL NAME,DISCRETIONARY DATA,ADDENDA RECORD INDICATOR,TRACE NUMBER\n';

                                var TRANSACTION_CODE = arr[i].substring(1, 3);
                                text += TRANSACTION_CODE + ',';
                                var RECEIVING_DFI_ID = arr[i].substring(3, 12);
                                text += RECEIVING_DFI_ID + ',';
                                var DFI_ACCOUNT_NUMBER = arr[i].substring(12, 29);
                                text += DFI_ACCOUNT_NUMBER + ',';
                                var DOLLAR_AMOUNT = arr[i].substring(29, 39);
                                text += (DOLLAR_AMOUNT).substring(0, 8)+'.'+(DOLLAR_AMOUNT).substring(8, 10)+ ',';
                                var INDIVIDUAL_IDENTIFICATION_NUMBER = arr[i].substring(39, 54);
                                text += INDIVIDUAL_IDENTIFICATION_NUMBER + ',';
                                var INDIVIDUAL_NAME = arr[i].substring(54, 76);
                                text += INDIVIDUAL_NAME + ',';
                                var DISCRETIONARY_DATA = arr[i].substring(76, 78);
                                text += DISCRETIONARY_DATA + ',';
                                var ADDENDA_RECORD_INDICATOR = arr[i].substring(78, 79);
                                text += ADDENDA_RECORD_INDICATOR + ',';
                                var TRACE_NUMBER = arr[i].substring(79, 94);
                                text += TRACE_NUMBER;
                                text += '\n';
                                text += '\n';
                            }
                            else if (RECORD_TYPE_CODE == 7) {
                                var TRANSACTION_CODE = arr[i].substring(1, 3);
                                var TYPE_OF_RETURN = arr[i].substring(3, 4);
                                if (TYPE_OF_RETURN === 'R') {
                                    text += 'TRANSACTION CODE,TYPE OF RETURN,RETURN REASON CODE,ORIGINAL TRACE NUMBER,DATE OF DEATH(YY/MM/DD),ORIGINAL ABA ,RETURN DESCRIPTION,BANK CONTROL NUMBER,TRACE NUMBER\n';


                                    text += TRANSACTION_CODE + ',';
                                    text += TYPE_OF_RETURN + ',';
                                    var RETURN_REASON_CODE = (arr[i].substring(4, 6)).toString();


                                    var reason_code=return_reason_code(RETURN_REASON_CODE);

                                    text += reason_code + ',';
                                    var ORIGINAL_TRACE_NUMBER = arr[i].substring(6, 21);
                                    text += ORIGINAL_TRACE_NUMBER + ',';
                                    var DATE_OF_DEATH = arr[i].substring(21, 27);
                                    text += (DATE_OF_DEATH.toString()).substring(0, 2) + '/' + (DATE_OF_DEATH.toString()).substring(2, 4) + '/' + (DATE_OF_DEATH.toString()).substring(4, 6) + ',';
                                    var ORIGINAL_ABA = arr[i].substring(27, 35);
                                    text += ORIGINAL_ABA + ',';
                                    var RETURN_DESCRIPTION = arr[i].substring(35, 70);
                                    text += RETURN_DESCRIPTION + ',';
                                    var BANK_CONTROL_NUMBER = arr[i].substring(70, 79);
                                    text += BANK_CONTROL_NUMBER + ',';
                                    var TRACE_NUMBER = arr[i].substring(79, 94);
                                    text += TRACE_NUMBER;
                                    text += '\n';
                                    text += '\n';
                                }

                                else if (TYPE_OF_RETURN === 'N' || TYPE_OF_RETURN === 'T') {
                                    text += 'TRANSACTION CODE,TYPE OF RETURN,RETURN REASON CODE,ORIGINAL TRACE NUMBER,ORIGINAL EFFECTIVE DATE(YY/MM/DD),ORIGINAL ABA ,RETURN DESCRIPTION,ORIGINAL DOLLAR AMOUNT($$$$$$$$¢¢),BANK CONTROL NUMBER,TRACE NUMBER\n';

                                    if (TYPE_OF_RETURN === 'N') {
                                        var desc = 'Notification of a re-deposit and splits';
                                    }
                                    else if (TYPE_OF_RETURN === 'T') {
                                        var desc = 'Return refused for an established Tardy Time Frame';
                                    }
                                    text += TRANSACTION_CODE + ',';
                                    text += desc + ',';
                                    var RETURN_REASON_CODE = (arr[i].substring(4, 6)).toString();


                                    var reason_code=return_reason_code(RETURN_REASON_CODE);
                                    text += reason_code + ',';
                                    var ORIGINAL_TRACE_NUMBER = arr[i].substring(6, 21);
                                    text += ORIGINAL_TRACE_NUMBER + ',';
                                    var ORIGINAL_EFFECTIVE_DATE = arr[i].substring(21, 27);
                                    text += (ORIGINAL_EFFECTIVE_DATE.toString()).substring(0, 2) + '/' + (ORIGINAL_EFFECTIVE_DATE.toString()).substring(2, 4) + '/' + (ORIGINAL_EFFECTIVE_DATE.toString()).substring(4, 6) + ',';
                                    var ORIGINAL_ABA = arr[i].substring(27, 35);
                                    text += ORIGINAL_ABA + ',';
                                    var RETURN_DESCRIPTION = arr[i].substring(35, 58);
                                    text += RETURN_DESCRIPTION + ',';

                                    var ORIGINAL_DOLLAR_AMOUNT = arr[i].substring(58, 68);
                                    text +=  (ORIGINAL_DOLLAR_AMOUNT).substring(0, 8)+'.'+(ORIGINAL_DOLLAR_AMOUNT).substring(8, 10) + ',';

                                    var BANK_CONTROL_NUMBER = arr[i].substring(68, 79);
                                    text += BANK_CONTROL_NUMBER + ',';
                                    var TRACE_NUMBER = arr[i].substring(79, 94);
                                    text += TRACE_NUMBER;
                                    text += '\n';
                                    text += '\n';
                                }
                                else if (TYPE_OF_RETURN === 'C') {
                                    text += 'TRANSACTION CODE,TYPE OF RETURN,CHANGE REASON CODE,ORIGINAL TRACE NUMBER,FILLER,ORIGINAL ABA ,CHANGE INFORMATION,BANK CONTROL NUMBER,TRACE NUMBER\n';


                                    text += TRANSACTION_CODE + ',';
                                    text += 'CHANGE ADDENDA RECORD' + ',';
                                    var CHANGE_REASON_CODE = arr[i].substring(4, 6);
                                    if (CHANGE_REASON_CODE == '01') {
                                        var desc_code = 'New Account Number';
                                    }
                                    else if (CHANGE_REASON_CODE == '02') {
                                        var desc_code = 'New ABA Number';
                                    }
                                    else if (CHANGE_REASON_CODE == '03') {
                                        var desc_code = 'New ABA Number/ Filler / New Account Number';
                                    }
                                    else if (CHANGE_REASON_CODE == '04') {
                                        var desc_code = 'New Name';
                                    }
                                    else if (CHANGE_REASON_CODE == '05') {
                                        var desc_code = 'New Transaction Code';
                                    }
                                    else if (CHANGE_REASON_CODE == '06') {
                                        var desc_code = 'New Account Number / Filler / New Transaction Code';
                                    }
                                    else if (CHANGE_REASON_CODE == '07') {
                                        var desc_code = 'New ABA Number / New Account Number / New Transaction Code';
                                    }


                                    text += desc_code + ',';
                                    var ORIGINAL_TRACE_NUMBER = arr[i].substring(6, 21);
                                    text += ORIGINAL_TRACE_NUMBER + ',';
                                    var FILLER = arr[i].substring(21, 27);
                                    text += FILLER + ',';
                                    var ORIGINAL_ABA = arr[i].substring(27, 35);
                                    text += ORIGINAL_ABA + ',';
                                    var CHANGE_INFORMATION = arr[i].substring(35, 70);
                                    text += CHANGE_INFORMATION + ',';
                                    var BANK_CONTROL_NUMBER = arr[i].substring(70, 79);
                                    text += BANK_CONTROL_NUMBER + ',';
                                    var TRACE_NUMBER = arr[i].substring(79, 94);
                                    text += TRACE_NUMBER;
                                    text += '\n';
                                    text += '\n';
                                }
                            }
                            else if (RECORD_TYPE_CODE == 8) {
                                text += 'SERVICE CLASS CODE,ENTRY/ADDENDA COUNT,ENTRY HASH,TOTAL DEBIT ENTRY DOLLAR AMOUNT($$$$$$$$$$¢¢),TOTAL CREDIT ENTRY DOLLAR AMOUNT($$$$$$$$$$¢¢),COMPANY IDENTIFICATION,RESERVED,ORIGINATING DFI IDENTIFICATION\n';


                                var SERVICE_CLASS_CODE = arr[i].substring(1, 4);
                                text += SERVICE_CLASS_CODE + ',';
                                var ENTRY_ADDENDA_COUNT = arr[i].substring(4, 10);
                                text += ENTRY_ADDENDA_COUNT + ',';
                                var ENTRY_HASH = arr[i].substring(10, 20);
                                text += ENTRY_HASH + ',';
                                var TOTAL_DEBIT_ENTRY_DOLLAR_AMOUNT = arr[i].substring(20, 32);
                                text += (TOTAL_DEBIT_ENTRY_DOLLAR_AMOUNT).substring(0, 10)+'.'+(TOTAL_DEBIT_ENTRY_DOLLAR_AMOUNT).substring(10, 12)  + ',';
                                var TOTAL_CREDIT_ENTRY_DOLLAR_AMOUNT = arr[i].substring(32, 44);
                                text += (TOTAL_CREDIT_ENTRY_DOLLAR_AMOUNT).substring(0, 10)+'.'+(TOTAL_CREDIT_ENTRY_DOLLAR_AMOUNT).substring(10, 12)  + ',';
                                var COMPANY_IDENTIFICATION = arr[i].substring(44, 54);
                                text += COMPANY_IDENTIFICATION + ',';
                                var RESERVED = arr[i].substring(54, 79);
                                text += RESERVED + ',';
                                var ORIGINATING_DFI_IDENTIFICATION = arr[i].substring(79, 94);
                                text += ORIGINATING_DFI_IDENTIFICATION;

                                text += '\n';
                                text += '\n';
                            }
                            else if (RECORD_TYPE_CODE == 9) {
                                text += 'BATCH COUNT,BLOCK COUNT,ENTRY/ADDENDA COUNT,ENTRY HASH,TOTAL DEBIT ENTRY DOLLAR AMOUNT IN FILE($$$$$$$$$$¢¢),TOTAL CREDIT ENTRY DOLLAR AMOUNT IN FILE,RESERVED\n';


                                var BATCH_COUNT = arr[i].substring(1, 7);
                                text += BATCH_COUNT + ',';
                                var BLOCK_COUNT = arr[i].substring(7, 13);
                                text += BLOCK_COUNT + ',';
                                var ENTRY_ADDENDA_COUNT = arr[i].substring(13, 21);
                                text += ENTRY_ADDENDA_COUNT + ',';
                                var ENTRY_HASH = arr[i].substring(21, 31);
                                text += ENTRY_HASH + ',';
                                var TOTAL_DEBIT_ENTRY_DOLLAR_AMOUNT_IN_FILE = arr[i].substring(31, 43);
                                text += (TOTAL_DEBIT_ENTRY_DOLLAR_AMOUNT_IN_FILE).substring(0, 10)+'.'+(TOTAL_DEBIT_ENTRY_DOLLAR_AMOUNT_IN_FILE).substring(10, 12)+ ',';
                                var TOTAL_CREDIT_ENTRY_DOLLAR_AMOUNT_IN_FILE = arr[i].substring(43, 55);
                                text += (TOTAL_CREDIT_ENTRY_DOLLAR_AMOUNT_IN_FILE).substring(0, 10)+'.'+(TOTAL_CREDIT_ENTRY_DOLLAR_AMOUNT_IN_FILE).substring(10, 12)+ ',';
                                var RESERVED = arr[i].substring(55, 94);
                                text += RESERVED;


                                text += '\n';
                                text += '\n';
                            }
                        }
                        var fileName = nlapiCreateFile(name + '.csv', 'CSV', text);
                        fileName.setFolder(folder);
                        var fileid = nlapiSubmitFile(fileName);

                        file.setFolder('9935115');
                        var fileid1 = nlapiSubmitFile(file);


                        var record = nlapiCreateRecord('supportcase');
                        record.setFieldValue('company', 1754659);
                        record.setFieldValue('custevent_cologix_facility', 16);
                        record.setFieldValue('email', 'accounting@cologix.com');
                        record.setFieldValue('profile', 32);

                        // record.setFieldValue('assigned', 487968);
                        record.setFieldValue('category', 12);
                        record.setFieldValue('custevent_cologix_sub_case_type', 122);
                        record.setFieldValue('priority', 2);
                        record.setFieldValue('origin', 1);
                        record.setFieldValue('title', caseSubj);

                        try {
                            var idRec = nlapiSubmitRecord(record, false, true);
                            var msg = nlapiCreateRecord('message');
                            msg.setFieldValue("activity", idRec);
                            msg.setFieldValue("author", 487968);
                            msg.setFieldValue("authoremail", 'accounting@cologix.com');
                            msg.setFieldValue("recipient", 12827);
                            msg.setFieldValue("recipientemail", 'accounting@cologix.com');
                            msg.setFieldValue("hasattachment", 'T');


                            msg.setFieldValue("subject", 'ACH_CASE JPMorgan');
                            msg.setFieldValue("message", 'ACH_CASE JPMorgan');

                            //msg.setLineItemValue('mediaitem', 'mediaitem', 0, 'T');

                            msg.setLineItemValue('mediaitem', 'internalid', 1, fileid);
                            var idRec = nlapiSubmitRecord(msg, false, true);
                            // return idRec;
                        } catch (error) {
                            nlapiLogExecution('ERROR', 'nlapiSubmitRecord Error', error.getCode() + ': ' + error.getDetails());
                        }
                    }

                }
            }
        }
    }

    catch (error) { // Start Catch Errors Section
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function return_reason_code(RETURN_REASON_CODE){
    //ACH RETURN REASON CODES
    /*     R01 – Insufficient Funds				R02 – Account Closed
         R03 – No Account/Unable to Locate		R04 – Invalid Account Number
         R06 – Returned per ODFI’s Request		R07 - Authorization Revoked by Customer
         R08 – Payment Stopped				R09 – Uncollected Funds
         R10 – Customer Advises Not Authorized		R11 – Check Truncation Entry Return
         R12 – Branch Sold to Another DFI			R13 – RDFI Not Qualified to Participate
         R14 – Payee Deceased				R15 – Beneficiary Deceased
         R16 – Account Frozen				R17 – File Record Edit Criteria
         R18 – Improper Effective Entry Date		R19 – Amount Field Error
         R20 – Non-Transaction Account			R21 – Invalid Company Identification
         R22 – Invalid Individual ID Number		R23 – Credit Entry Refused by Receiver
         R24 – Duplicate Entry				R25 – Addenda Error
         R26 – Mandatory Field Error			R27 – Trace Number Error
         R28 – Routing Number Check Digit Error		R29 – Corporate Customer Advises Not Authorized
         R30 – RDFI Not Participant in Check Truncation 	R31 – Permissible Return Entry
         R32 – RDFI Non-Settlement			R33 – Return of XCK Entry
         R34 – Limited Participation DFI			R35 – Return of Improper Debit Entry
         R40 – Return of ENR Entry by Federal Gov’t 	R50 – State Law Affecting RCK Acceptance
         R61 – Misrouted Return				R62 – Incorrect Trace Number
         R63 – Incorrect Dollar Amount			R64 – Incorrect Individual Identification
         R65 – Incorrect Transaction Code			R66 – Incorrect Company Identification
         R67 – Duplicate Return				R68 – Untimely Return
         R69 – Multiple Errors				R70 – Permissible Return Entry Not Accepted
         R71 – Misrouted Dishonored Return		R72 – Untimely Dishonored Return
         R73 – Timely Original Return			R74 – Corrected Return
         R80 – Cross-Border Payment Coding Error		R81 – Non-Participant in Cross-Border Program
         R82 – Invalid Foreign Receiving DFI Identification	R83 – Foreign Receiving DFI Unable to Settle
         R84 – Entry Not Processed by OGO*/
    if(RETURN_REASON_CODE=='01') {
        var reason_code = 'Insufficient Funds';
    } else if(RETURN_REASON_CODE=='02'){
        var reason_code = 'Account Closed';
    }
    else if(RETURN_REASON_CODE=='03'){
        var reason_code = 'No Account/Unable to Locate';
    }
    else if(RETURN_REASON_CODE=='04'){
        var reason_code = 'Invalid Account Number';
    }
    else if(RETURN_REASON_CODE=='06'){
        var reason_code = 'Returned per ODFI’s Request';
    }
    else if(RETURN_REASON_CODE=='07'){
        var reason_code = 'Authorization Revoked by Customer';
    }
    else if(RETURN_REASON_CODE=='08'){
        var reason_code = 'Payment Stopped';
    }
    else if(RETURN_REASON_CODE=='09'){
        var reason_code = 'Uncollected Funds';
    }
    else if(RETURN_REASON_CODE=='10'){
        var reason_code = 'Customer Advises Not Authorized';
    }
    else if(RETURN_REASON_CODE=='11'){
        var reason_code = 'Check Truncation Entry Return';
    }
    else if(RETURN_REASON_CODE=='12'){
        var reason_code = 'Branch Sold to Another DFI';
    }
    else if(RETURN_REASON_CODE=='13'){
        var reason_code = 'RDFI Not Qualified to Participate';
    }
    else if(RETURN_REASON_CODE=='14'){
        var reason_code = 'Payee Deceased';
    }
    else if(RETURN_REASON_CODE=='15'){
        var reason_code = 'Beneficiary Deceased';
    }
    else if(RETURN_REASON_CODE=='16'){
        var reason_code = 'Account Frozen';
    }
    else if(RETURN_REASON_CODE=='17'){
        var reason_code = 'File Record Edit Criteria';
    }
    else if(RETURN_REASON_CODE=='18'){
        var reason_code = 'Improper Effective Entry Date';
    }
    else if(RETURN_REASON_CODE=='19'){
        var reason_code = 'Amount Field Error';
    }
    else if(RETURN_REASON_CODE=='20'){
        var reason_code = 'Non-Transaction Account';
    }
    else if(RETURN_REASON_CODE=='21'){
        var reason_code = 'Invalid Company Identification';
    }
    else if(RETURN_REASON_CODE=='22'){
        var reason_code = 'Invalid Individual ID Number';
    }
    else if(RETURN_REASON_CODE=='23'){
        var reason_code = 'Credit Entry Refused by Receiver';
    }
    else if(RETURN_REASON_CODE=='24'){
        var reason_code = 'Duplicate Entry';
    }
    else if(RETURN_REASON_CODE=='25'){
        var reason_code = 'Addenda Error';
    }
    else if(RETURN_REASON_CODE=='26'){
        var reason_code = 'Mandatory Field Error';
    }
    else if(RETURN_REASON_CODE=='27'){
        var reason_code = 'Trace Number Error';
    }
    else if(RETURN_REASON_CODE=='28'){
        var reason_code = 'Routing Number Check Digit Error';
    }
    else if(RETURN_REASON_CODE=='29'){
        var reason_code = 'Corporate Customer Advises Not Authorized';
    }
    else if(RETURN_REASON_CODE=='30'){
        var reason_code = 'RDFI Not Participant in Check Truncation';
    }
    else if(RETURN_REASON_CODE=='31'){
        var reason_code = 'Permissible Return Entry';
    }
    else if(RETURN_REASON_CODE=='32'){
        var reason_code = 'RDFI Non-Settlement';
    }
    else if(RETURN_REASON_CODE=='33'){
        var reason_code = 'Return of XCK Entry';
    }
    else if(RETURN_REASON_CODE=='34'){
        var reason_code = 'Limited Participation DFI';
    }
    else if(RETURN_REASON_CODE=='35'){
        var reason_code = 'Return of Improper Debit Entry';
    }
    else if(RETURN_REASON_CODE=='40'){
        var reason_code = 'Return of ENR Entry by Federal Govt ';
    }
    else if(RETURN_REASON_CODE=='50'){
        var reason_code = 'State Law Affecting RCK Acceptance';
    }
    else if(RETURN_REASON_CODE=='60'){
        var reason_code = 'Misrouted Return';
    }
    else if(RETURN_REASON_CODE=='61'){
        var reason_code = 'Misrouted Return';
    }
    else if(RETURN_REASON_CODE=='62'){
        var reason_code = 'Incorrect Trace Number';
    }
    else if(RETURN_REASON_CODE=='63'){
        var reason_code = 'Incorrect Dollar Amount';
    }
    else if(RETURN_REASON_CODE=='64'){
        var reason_code = 'Incorrect Individual Identification';
    }
    else if(RETURN_REASON_CODE=='65'){
        var reason_code = 'Incorrect Transaction Code';
    }
    else if(RETURN_REASON_CODE=='66'){
        var reason_code = 'Incorrect Company Identification';
    }
    else if(RETURN_REASON_CODE=='67'){
        var reason_code = 'Duplicate Return';
    }
    else if(RETURN_REASON_CODE=='68'){
        var reason_code = 'Untimely Return';
    }
    else if(RETURN_REASON_CODE=='69'){
        var reason_code = 'Multiple Errors';
    }
    else if(RETURN_REASON_CODE=='70'){
        var reason_code = 'Permissible Return Entry Not Accepted';
    }
    else if(RETURN_REASON_CODE=='71'){
        var reason_code = 'Misrouted Dishonored Return';
    }
    else if(RETURN_REASON_CODE=='72'){
        var reason_code = 'Untimely Dishonored Return';
    }
    else if(RETURN_REASON_CODE=='73'){
        var reason_code = 'Timely Original Return';
    }
    else if(RETURN_REASON_CODE=='74'){
        var reason_code = 'Corrected Return';
    }
    else if(RETURN_REASON_CODE=='80'){
        var reason_code = 'Cross-Border Payment Coding Error';
    }
    else if(RETURN_REASON_CODE=='81'){
        var reason_code = 'Non-Participant in Cross-Border Program';
    }
    else if(RETURN_REASON_CODE=='82'){
        var reason_code = 'Invalid Foreign Receiving DFI Identification';
    }
    else if(RETURN_REASON_CODE=='83'){
        var reason_code = 'Foreign Receiving DFI Unable to Settle';
    }
    else if(RETURN_REASON_CODE=='84'){
        var reason_code = 'Entry Not Processed by OGO';
    }
    return reason_code;
}
function decode_base64 (s)
{
    var e = {}, i, k, v = [], r = '', w = String.fromCharCode;
    var n = [[65, 91], [97, 123], [48, 58], [43, 44], [47, 48]];

    for (z in n)
    {
        for (i = n[z][0]; i < n[z][1]; i++)
        {
            v.push(w(i));
        }
    }
    for (i = 0; i < 64; i++)
    {
        e[v[i]] = i;
    }

    for (i = 0; i < s.length; i+=72)
    {
        var b = 0, c, x, l = 0, o = s.substring(i, i+72);
        for (x = 0; x < o.length; x++)
        {
            c = e[o.charAt(x)];
            b = (b << 6) + c;
            l += 6;
            while (l >= 8)
            {
                r += w((b >>> (l -= 8)) % 256);
            }
        }
    }
    return r;
}