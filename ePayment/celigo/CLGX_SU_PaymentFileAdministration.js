//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_PaymentFileAdministration.js
//	Script Name:	CLGX_SU_PaymentFileAdministration
//	Script Id:		customscript_clgx_su_paymentfileadminist
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	PaymentFileAdministration - Sandbox | Production
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//-------------------------------------------------------------------------------------------------

function beforeLoad(type, form) {

    try {
//Catalina
        if ((type=='create')||(type=='edit')) {
            var id = nlapiGetFieldValue("id");
            var processed=nlapiGetFieldValue("custrecord_2663_file_processed");
            var arrParam = new Array();
            arrParam['custscript_ss_pfa_id'] = id;
            nlapiLogExecution('DEBUG','processed', processed);
                nlapiLogExecution('DEBUG','id', id);
            if(processed==1 || processed==12) {
                var ba = nlapiGetFieldValue('custrecord_2663_bank_account');
                if (ba == 3) {
                    var status = nlapiScheduleScript('customscript_clgx_ss_payment_file_rbc', null, arrParam);
                }
            }

        }


    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

}