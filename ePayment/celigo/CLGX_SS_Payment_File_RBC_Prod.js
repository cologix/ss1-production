//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_Payment_File_RBC.js
//	Script Name:	CLGX_SS_Payment_File_RBC
//	Script Id:		customscript_SS_Payment_File_RBC
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	PaymentFileAdministration - Sandbox | Production
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//-------------------------------------------------------------------------------------------------

function main() {

    try {
//Catalina

//iiooo
        var id=parseInt(nlapiGetContext().getSetting('SCRIPT','custscript_ss_pfa_id'))||'';
        nlapiLogExecution('DEBUG', 'ID', id);
        if(id!=null && id!=''){
            var pf=nlapiLoadRecord('customrecord_2663_file_admin',id);
            //if(processed==4) {
            var file_ref = pf.getFieldValue("custrecord_2663_file_ref");
            var file_ref_static = pf.getFieldValue("custrecord_clgx_file_ref_st");
            var status = pf.getFieldValue("custrecord_2663_file_processed");
            // var ba=nlapiGetFieldValue('custrecord_2663_bank_account');
            nlapiLogExecution('DEBUG', 'file_ref', file_ref);
            nlapiLogExecution('DEBUG', 'file_ref_static', file_ref_static);
            if ((status != 10) && (file_ref_static== null || file_ref_static== '')) {
                if ((file_ref != null && file_ref != '')) {
                    var arrFilF = new Array();
                    var arrColF = new Array();
                    arrFilF.push(new nlobjSearchFilter("internalid", null, "is", parseInt(file_ref)));
                    var searchF = nlapiSearchRecord('file', null, arrFilF, arrColF);
                    if (searchF != null) {
                        var objFile = nlapiLoadFile(parseInt(file_ref));
                        var bodyFile = objFile.getValue();
                        var substr = '$$AAP';
                        if (bodyFile.indexOf(substr) == -1) {
                            nlapiLogExecution('DEBUG', 'No qualifier', 'No qualifier');
                            // var bodyFile1 = bodyFile.replace(bodyFile.substring(24, 20), "TEST");
                            var qualifier = '$$AAPDCPA1464[PROD[NL$$\n';
                            var txtFinal = qualifier + bodyFile;
                            var name = objFile.getName();
                            var folderId = objFile.getFolder();
                            var fileType = objFile.getType();
                            var newFile = nlapiCreateFile(name, fileType, txtFinal);//create a new file and add the new updates
                            newFile.setFolder(folderId);
                            nlapiSubmitFile(newFile);//submit

                            pf.setFieldValue('custrecord_clgx_file_ref_st',file_ref);
                            nlapiSubmitRecord(pf, false,true);
                        }
                    }
                }
                else {
                    var arrParam = new Array();
                    arrParam['custscript_ss_pfa_id'] = id;
                    var context = nlapiGetContext();
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId(), arrParam);
                    if (status == 'QUEUED') {
                        nlapiLogExecution('DEBUG', 'End', status + ' / Finished scheduled script due to FILE REF EMPTY and re-schedule it.');
                        return false;
                    }
                }
            }
        }
        //  }




    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

}