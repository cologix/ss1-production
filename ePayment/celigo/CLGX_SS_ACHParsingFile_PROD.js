//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_ACHParsingFile.js
//	Script Name:	CLGX_SS_ACHParsingFile
//	Script Id:		customscript_CLGX_SS_ACHParsingFile
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		28/04/2017
//-------------------------------------------------------------------------------------------------

function sc_caseReports() {
    try {
        var filters = new Array();
        filters[0] = new nlobjSearchFilter('internalid', null, 'is', 9935110);
        nlapiLogExecution('DEBUG', 'Start execution', 'Start execution');

        var columns = new Array();
        var filename = new nlobjSearchColumn('name', 'file');
        var fileid = new nlobjSearchColumn('internalid', 'file');

        columns[0] = filename;
        columns[1] = fileid;

        var searchResult = nlapiSearchRecord('folder', null, filters, columns);
        if (searchResult) {
            for (var l = 0; l < searchResult.length; l++) {

                var name = searchResult[l].getValue(filename);
                var id = searchResult[l].getValue(fileid);
                if(name.indexOf('downloaded') === -1 && name!=''){
                    var arrColumnsC = new Array();
                    var arrFiltersC = new Array();
                    arrFiltersC.push(new nlobjSearchFilter('internalid', 'attachments', 'anyof', id));
                    var rows = nlapiSearchRecord('message','customsearch6556', arrFiltersC, arrColumnsC);
                    if (rows == null) {
                        var file = nlapiLoadFile(parseInt(id));
                        var bodyFile = file.getValue();
                        var folder = file.getFolder();
                        var name = file.getName();
                        var decodedString = decode_base64(bodyFile);
                        var fileName = nlapiCreateFile(name + '.txt', 'PLAINTEXT', decodedString);
                        fileName.setFolder(folder);
                        var fileid = nlapiSubmitFile(fileName);
                        var arr = decodedString.split('\r');
                        var validArr = new Array();
                        var totalArr = new Array();
                        var totalTr = 0;
                        var validTr = 0;
                        for (i = 0; i < arr.length; i++) {
                            if (arr[i].indexOf('VALID TRANS FOR') !== -1) {
                                var validArr = arr[i].split('    ');
                            }
                            if (arr[i].indexOf('GRAND TOTAL FOR') !== -1) {
                                var totalArr = arr[i].split('    ');
                            }
                        }
                        if (validArr.length > 0) {
                            for (j = 2; j < validArr.length; j++) {
                                if (validArr[j] != '') {
                                    var validTr = validArr[j];
                                    break;
                                }
                            }
                        }
                        if (totalArr.length > 0) {
                            for (k = 2; k < totalArr.length; k++) {
                                if (totalArr[k] != '') {
                                    var totalTr = totalArr[k];
                                    break;
                                }
                            }
                        }
                        if (totalTr != validTr) {
                            var message = 'The ACH file contains ' + (parseInt(totalTr) - parseInt(validTr)) + ' Rejected transactions';

                        }
                        else {
                            var message = '- - All transactions have been successful';
                        }
                        file.setFolder('10113619');
                        var fileid1 = nlapiSubmitFile(file);
                        if(id!=fileid1){
                            nlapiDeleteFile(id);
                        }

                        var record = nlapiCreateRecord('supportcase');
                        record.setFieldValue('company', 1754659);
                        record.setFieldValue('custevent_cologix_facility', 16);
                        record.setFieldValue('email', 'accounting@cologix.com');
                        record.setFieldValue('title', 'ACH_CASE ' + message);
                        record.setFieldValue('profile', 32);

                        // record.setFieldValue('assigned', 487968);
                        record.setFieldValue('category', 12);
                        record.setFieldValue('custevent_cologix_sub_case_type', 122);
                        record.setFieldValue('priority', 2);
                        record.setFieldValue('origin', 1);

                        try {
                            var idRec = nlapiSubmitRecord(record, false, true);
                            var msg = nlapiCreateRecord('message');
                            msg.setFieldValue("activity", idRec);
                            msg.setFieldValue("author", 487968);
                            msg.setFieldValue("authoremail", 'accounting@cologix.com');
                            msg.setFieldValue("recipient", 12827);
                            msg.setFieldValue("recipientemail", 'accounting@cologix.com');
                            msg.setFieldValue("hasattachment", 'T');


                            msg.setFieldValue("subject", message);
                            msg.setFieldValue("message", message);

                            //msg.setLineItemValue('mediaitem', 'mediaitem', 0, 'T');

                            msg.setLineItemValue('mediaitem', 'internalid', 1, fileid);
                            var idRec = nlapiSubmitRecord(msg, false, true);
                            // return idRec;
                        } catch (error) {
                            nlapiLogExecution('ERROR', 'nlapiSubmitRecord Error', error.getCode() + ': ' + error.getDetails());
                        }
                    }

                }
            }
        }
    }

    catch (error) { // Start Catch Errors Section
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function decode_base64 (s)
{
    var e = {}, i, k, v = [], r = '', w = String.fromCharCode;
    var n = [[65, 91], [97, 123], [48, 58], [43, 44], [47, 48]];

    for (z in n)
    {
        for (i = n[z][0]; i < n[z][1]; i++)
        {
            v.push(w(i));
        }
    }
    for (i = 0; i < 64; i++)
    {
        e[v[i]] = i;
    }

    for (i = 0; i < s.length; i+=72)
    {
        var b = 0, c, x, l = 0, o = s.substring(i, i+72);
        for (x = 0; x < o.length; x++)
        {
            c = e[o.charAt(x)];
            b = (b << 6) + c;
            l += 6;
            while (l >= 8)
            {
                r += w((b >>> (l -= 8)) % 256);
            }
        }
    }
    return r;
}