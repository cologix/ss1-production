nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Company_Bank_Details.js
//	Script Name:	CLGX_SU_Company_Bank_Details
//	Script Id:		customscript_clgx_su_company_bank_details
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Company Bank Details(Custom Record) - Sandbox | Production
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		23/06/2017
//-------------------------------------------------------------------------------------------------
function beforeLoad(type, form) {
    try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
        //      Hide Banking information fields from all users/roles except A/P and Accounting Manage
//-----------------------------------------------------------------------------------------------------------------
        var currentContext = nlapiGetContext();
        var roleid = currentContext.getRole();
        if ((currentContext.getExecutionContext() == 'userinterface') && roleid!=1021  ) {
            var bank_num= form.getField('custpage_eft_custrecord_2663_bank_num');
            if(bank_num!=null) {
                form.getField('custpage_eft_custrecord_2663_bank_num').setDisplayType('hidden');
            }
            var branch_num=form.getField('custpage_eft_custrecord_2663_branch_num');
            if(branch_num!=null) {
                form.getField('custpage_eft_custrecord_2663_branch_num').setDisplayType('hidden');
            }
            var acct_num=form.getField('custpage_eft_custrecord_2663_acct_num');
            if(acct_num!=null) {
                form.getField('custpage_eft_custrecord_2663_acct_num').setDisplayType('hidden');
            }
            var issuer_num=form.getField('custpage_eft_custrecord_2663_issuer_num');
            if(issuer_num!=null) {
                form.getField('custpage_eft_custrecord_2663_issuer_num').setDisplayType('hidden');
            }
            var statement_name=form.getField('custpage_eft_custrecord_2663_statement_name');
            if(statement_name!=null) {
                form.getField('custpage_eft_custrecord_2663_statement_name').setDisplayType('hidden');
            }
            var processor_code=form.getField('custpage_eft_custrecord_2663_processor_code');
            if(processor_code!=null) {
                form.getField('custpage_eft_custrecord_2663_processor_code').setDisplayType('hidden');
            }

        }
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

}