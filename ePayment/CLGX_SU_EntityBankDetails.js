function beforeSubmit(type) {
    try {

        var currentContext = nlapiGetContext();
        var  roleid = currentContext.getRole();
        if(roleid== 1034 || roleid== 1012) {
//Catalina
            nlapiSetFieldValue("isinactive", 'T');
        }
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function afterSubmit (type) {
    try {
        var currentContext = nlapiGetContext();
        var recid = nlapiGetRecordId();
        if (currentContext.getExecutionContext() == 'userinterface') {
            var record=nlapiLoadRecord('customrecord_2663_entity_bank_details',recid);
            var isinactive=record.getFieldValue('isinactive');
            var emp = record.getFieldValue('custrecord_2663_parent_employee');
            var vendor = record.getFieldValue('custrecord_2663_parent_vendor');
            nlapiLogExecution('DEBUG','emp', emp);
            nlapiLogExecution('DEBUG','isinactive', isinactive);
            if(emp!=null && emp!='' && isinactive=='T'){
                nlapiLogExecution('DEBUG','isinactive', isinactive);
                nlapiSubmitField('employee', emp, 'custentity_2663_payment_method', 'F');

            }
            if(vendor!=null && vendor!='' && isinactive=='T'){
                nlapiSubmitField('vendor', vendor, 'custentity_2663_payment_method', 'F');

            }
            if(vendor!=null && vendor!='' && isinactive=='F'){
                nlapiLogExecution('DEBUG','isinactive', isinactive);
                nlapiSubmitField('vendor', vendor, 'custentity_2663_payment_method', 'T');

            }
            if(emp!=null && emp!='' && isinactive=='F'){
                nlapiLogExecution('DEBUG','isinactive', isinactive);
                nlapiSubmitField('employee', emp, 'custentity_2663_payment_method', 'T');

            }
        }



//---------- End Sections ------------------------------------------------------------------------------------------------
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}