//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();
        var initialTime = moment();
        
		var searchResults = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pwrs_mu');

        for ( var j = 0; searchResults != null && j < searchResults.length; j++ ) {
        	
        	var startExecTime = moment();
        	var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);
        	
            if ( context.getRemainingUsage() <= 100 || totalMinutes > 50 || j > 990 ){
                var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                if ( status == 'QUEUED' ) {
             	   nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                    break; 
                }
            }

        	var powerid = searchResults[j].getValue('internalid',null,null);
        	
        	
        	
        	var deviceid = 0;
			var pdpm = '';
			
			var power = nlapiLoadRecord('customrecord_clgx_power_circuit',powerid);
			
			var panelid = power.getFieldValue('custrecord_clgx_power_panel_pdpm');
			if(panelid != null && panelid != ''){
				// go to FAM record and see if it's linked to a device and if pdpm
				var fields = ['custrecord_clgx_od_fam_device','custrecord_clgx_fam_pdpm'];
				var columns = nlapiLookupField('customrecord_ncfar_asset', panelid, fields);
				var deviceonfam = columns.custrecord_clgx_od_fam_device;
				pdpm = columns.custrecord_clgx_fam_pdpm;
				if(deviceonfam != null && deviceonfam != ''){
					deviceid = deviceonfam;
				}
			

				if(deviceid > 0 && pdpm != ''){
					
					var volts = power.getFieldText('custrecord_cologix_power_volts');
					var amps = power.getFieldText('custrecord_cologix_power_amps');
					
					var module = power.getFieldText('custrecord_cologix_power_upsbreaker');
					var breaker = power.getFieldText('custrecord_cologix_power_circuitbreaker');
					var line = power.getFieldValue('custrecord_cologix_power_subpanel');
					
					// Build Points Array ------------------------------------------------------------------------------------------------------------------------------------------------
					var arrColumns = new Array();
					arrColumns.push(new nlobjSearchColumn('internalid',null,null));
					arrColumns.push(new nlobjSearchColumn('externalid',null,null));
					arrColumns.push(new nlobjSearchColumn('name',null,null));
					var arrFilters = new Array();
					arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_device",null,"anyof",deviceid));
					var searchPoints = nlapiSearchRecord('customrecord_clgx_dcim_points', null, arrFilters, arrColumns);
					var arrPoints = new Array();
					for ( var i = 0; searchPoints != null && i < searchPoints.length; i++ ) {
						var objPoint = new Object();
						objPoint["pointid"] = parseInt(searchPoints[i].getValue('internalid',null,null));
						objPoint["pointextid"] = parseInt(searchPoints[i].getValue('externalid',null,null));
						objPoint["point"] = searchPoints[i].getValue('name',null,null);
						arrPoints.push(objPoint);
					}
					
					
					var arrAmpsPoints = new Array();
					var arrKWPoints = new Array();
					var arrKWHPoints = new Array();
					
					// this is a PDPM Panel
					if(pdpm == 'T'){

						// examples:
						//Module 01 Breaker 1 Current
						//Module 01 Breaker 1 Power
						//Module 01 Energy since Reset
						
						// these voltages  - only one breaker
						if(volts == '120V Single Phase' || volts == '240V Single Phase'){
							var ampspoint = 'Module ' + ('0' + module).slice(-2) + ' Breaker ' + line + ' Current';
							var objPoint = _.find(arrPoints, function(arr){ return (arr.point == ampspoint); });
							if(objPoint != null){
								arrAmpsPoints.push(objPoint.pointid);
							}
							var kwpoint = 'Module ' + ('0' + module).slice(-2) + ' Breaker ' + line + ' Power';
							var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwpoint); });
							if(objPoint != null){
								arrKWPoints.push(objPoint.pointid);
							}
						}
						// this voltages - 2 breakers
						else if (volts == '208V Single Phase'){
							var arrBreakers = new Array();
							if (line == 8){ // Lines 1, 2
								arrBreakers = [1,2];
								var line2 = 1;
							}
							if (line == 9){ // Lines 2, 3
								arrBreakers = [2,3];
								var line2 = 2;
							}
							if (line == 10){ // Lines 1, 3
								arrBreakers = [1,3];
							}
							for ( var i = 0; arrBreakers != null && i < arrBreakers.length; i++ ) {
								var ampspoint = 'Module ' + ('0' + module).slice(-2) + ' Breaker ' + arrBreakers[i] + ' Current';
								var objPoint = _.find(arrPoints, function(arr){ return (arr.point == ampspoint); });
								if(objPoint != null){
									arrAmpsPoints.push(objPoint.pointid);
								}
								var kwpoint = 'Module ' + ('0' + module).slice(-2) + ' Breaker ' + arrBreakers[i] + ' Power';
								var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwpoint); });
								if(objPoint != null){
									arrKWPoints.push(objPoint.pointid);
								}
							}
						}
						// these voltages - 3 breakers
						else if (volts == '208V Three Phase' || volts == '240V Three Phase'){
							for ( var i = 1;  i < 4; i++ ) {
								var ampspoint = 'Module ' + ('0' + module).slice(-2) + ' Breaker ' + i + ' Current';
								var objPoint = _.find(arrPoints, function(arr){ return (arr.point == ampspoint); });
								if(objPoint != null){
									arrAmpsPoints.push(objPoint.pointid);
								}
								var kwpoint = 'Module ' + ('0' + module).slice(-2) + ' Breaker ' + i + ' Power';
								var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwpoint); });
								if(objPoint != null){
									arrKWPoints.push(objPoint.pointid);
								}
							}
						}
						else{}
						
						// when panel is PDPM, there is only one KWH point per module, not by breaker
						var kwhpoint = 'Module ' + ('0' + module).slice(-2) + ' Energy since Reset';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwhpoint); });
						if(objPoint != null){
							arrKWHPoints.push(objPoint.pointid);
						}
						

						
					}
					else{
						
						// examples:
						//Breaker 01 - Current Reading
						//Breaker 01 - Power Total
						//Breaker 01 - Total Consumption Measured
						
						// these voltages  - only one breaker
						if(volts == '120V Single Phase' || volts == '240V Single Phase'){
							var ampspoint = 'Breaker ' + ('0' + breaker).slice(-2) + ' - Current Reading';
							var objPoint = _.find(arrPoints, function(arr){ return (arr.point == ampspoint); });
							if(objPoint != null){
								arrAmpsPoints.push(objPoint.pointid);
							}
							var kwpoint = 'Breaker ' + ('0' + breaker).slice(-2) + ' - Power Total';
							var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwpoint); });
							if(objPoint != null){
								arrKWPoints.push(objPoint.pointid);
							}
							var kwhpoint = 'Breaker ' + ('0' + breaker).slice(-2) + ' - Total Consumption Measured';
							var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwhpoint); });
							if(objPoint != null){
								arrKWHPoints.push(objPoint.pointid);
							}
						}
						// this voltages - 2 breakers
						else if (volts == '208V Single Phase'){
							for ( var i = 0; i < 2; i++ ) {
								var ampspoint = 'Breaker ' + ('0' + (parseInt(breaker) + i*2)).slice(-2) + ' - Current Reading';
								var objPoint = _.find(arrPoints, function(arr){ return (arr.point == ampspoint); });
								if(objPoint != null){
									arrAmpsPoints.push(objPoint.pointid);
								}
								var kwpoint = 'Breaker ' + ('0' + (parseInt(breaker) + i*2)).slice(-2) + ' - Power Total';
								var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwpoint); });
								if(objPoint != null){
									arrKWPoints.push(objPoint.pointid);
								}
								var kwhpoint = 'Breaker ' + ('0' + (parseInt(breaker) + i*2)).slice(-2) + ' - Total Consumption Measured';
								var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwhpoint); });
								if(objPoint != null){
									arrKWHPoints.push(objPoint.pointid);
								}
							}
						}
						// these voltages - 3 breakers
						else if (volts == '208V Three Phase' || volts == '240V Three Phase'){
							for ( var i = 0;  i < 3; i++ ) {
								var ampspoint = 'Breaker ' + ('0' + (parseInt(breaker) + i*2)).slice(-2) + ' - Current Reading';
								var objPoint = _.find(arrPoints, function(arr){ return (arr.point == ampspoint); });
								if(objPoint != null){
									arrAmpsPoints.push(objPoint.pointid);
								}
								var kwpoint = 'Breaker ' + ('0' + (parseInt(breaker) + i*2)).slice(-2) + ' - Power Total';
								var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwpoint); });
								if(objPoint != null){
									arrKWPoints.push(objPoint.pointid);
								}
								var kwhpoint = 'Breaker ' + ('0' + (parseInt(breaker) + i*2)).slice(-2) + ' - Total Consumption Measured';
								var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwhpoint); });
								if(objPoint != null){
									arrKWHPoints.push(objPoint.pointid);
								}
							}
						}
						else{}
						
					}
					
					// update device and points on power circuit
					if(deviceid > 0){
						power.setFieldValue('custrecord_clgx_dcim_device',deviceid);
					}
					if(arrKWHPoints.length > 0){
						power.setFieldValues('custrecord_clgx_dcim_points_consumption',arrKWHPoints);
					}
					if(arrAmpsPoints.length > 0){
						power.setFieldValues('custrecord_clgx_dcim_points_current',arrAmpsPoints);
					}
					if(arrKWPoints.length > 0){
						power.setFieldValues('custrecord_clgx_dcim_points_demand',arrKWPoints);
					}
					var idRec = nlapiSubmitRecord(power, false, true);

				}
			}
			else{
				
				var circuitid = parseInt(power.getFieldValue('custrecord_clgx_outlet_box_serial_no_pwr'));
				var circuit = power.getFieldText('custrecord_clgx_outlet_box_serial_no_pwr');
				
				if(circuitid > 0){
					var arrColumns = new Array();
					arrColumns.push(new nlobjSearchColumn('internalid',null,null));
					var arrFilters = new Array();
					arrFilters.push(new nlobjSearchFilter("name",null,"is",circuit));
					arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device_deleted',null,'is','F'));
					arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
					var searchDevice = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
		
					if(searchDevice != null){
						
						var deviceid = searchDevice[0].getValue('internalid',null,null);
						power.setFieldValue('custrecord_clgx_dcim_device',deviceid);
						
						// Build Points Array ------------------------------------------------------------------------------------------------------------------------------------------------
						var arrColumns = new Array();
						arrColumns.push(new nlobjSearchColumn('internalid',null,null));
						arrColumns.push(new nlobjSearchColumn('externalid',null,null));
						arrColumns.push(new nlobjSearchColumn('name',null,null));
						var arrFilters = new Array();
						arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_device",null,"anyof",deviceid));
						var searchPoints = nlapiSearchRecord('customrecord_clgx_dcim_points', null, arrFilters, arrColumns);
						var arrPoints = new Array();
						for ( var i = 0; searchPoints != null && i < searchPoints.length; i++ ) {
							var objPoint = new Object();
							objPoint["pointid"] = parseInt(searchPoints[i].getValue('internalid',null,null));
							objPoint["pointextid"] = parseInt(searchPoints[i].getValue('externalid',null,null));
							objPoint["point"] = searchPoints[i].getValue('name',null,null);
							arrPoints.push(objPoint);
						}
						
						var arrAmpsPoints = new Array();
						var objPointAmps = _.find(arrPoints, function(arr){ return (arr.point == 'Current - Total'); });
						if(objPointAmps != null){
							arrAmpsPoints.push(objPointAmps.pointid);
							power.setFieldValues('custrecord_clgx_dcim_points_current',arrAmpsPoints);
						}
													
						var arrKWPoints = new Array();
						var objPointKW = _.find(arrPoints, function(arr){ return (arr.point == 'Power - Total Demand'); });
						if(objPointKW != null){
							arrKWPoints.push(objPointKW.pointid);
							power.setFieldValues('custrecord_clgx_dcim_points_demand',arrKWPoints);
						}
						
						var arrKWHPoints = new Array();
						var objPointKWH = _.find(arrPoints, function(arr){ return (arr.point == 'Power - Total Consumption Measured'); });
						if(objPointKWH != null){
							arrKWHPoints.push(objPointKWH.pointid);
							power.setFieldValues('custrecord_clgx_dcim_points_consumption',arrKWHPoints);
						}

						var idRec = nlapiSubmitRecord(power, false, true);
					}
				}
			}
		
			
			
        	
        	
        	
        	
        	
        	
        	
        	var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            var index = j + 1;
            nlapiLogExecution('DEBUG','Results ', ' | Index = ' + index + ' of ' + searchResults.length + ' | Usage - '+ usageConsumtion + '  |');

        }
        
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
