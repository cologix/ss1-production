//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        var arrFilters = new Array();
        var searchResults = nlapiSearchRecord('customrecord_cologix_sales_activities', null, arrFilters, arrColumns);

        for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
        	
            if ( context.getRemainingUsage() <= 1000 && (i+1) < searchResults.length ){
                var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                if ( status == 'QUEUED' ) {
             	   nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
                    break; 
                }
             }
        	
        	var searchResult = searchResults[i];
        	var recordID = searchResult.getValue('internalid',null,null);

        	nlapiDeleteRecord('customrecord_cologix_sales_activities', recordID);

        	var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
        	nlapiLogExecution('DEBUG', 'Value', 'ID = ' + i + '/' + searchResults.length +  ' / Usage - '+ usageConsumtion);
        }
        

        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

