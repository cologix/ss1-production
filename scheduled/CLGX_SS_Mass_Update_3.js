nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:		CLGX_SS_Mass_Update.js
//	Script Name:		CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:		On Server
//	Script Type:		Scheduled Script
//	Deployments:		Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------

function scheduled_mass_update(){
	
	try{
		nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
		
		var context = nlapiGetContext();

		// vendor, partner, customer, contact, portal_user, service, project
		// opportunity, proposal, salesorder, invoice, cinvoice, journalentry, payment, purchaseorder, creditmemo, vendorbill, vendorcredit, vendorpayment, forecast
		// device, device_capacity, fam, space, power, xc, vxc, equipment
		// case
		
		var rec_type = 'device_capacity';
		var create_file = 0;
		
		switch(rec_type) {
		
		// ============================================= entities ===============================================
		
	    case 'vendor':
			var type = 'vendor';
			var rec_search = 'vendor';
			var file_id = 9203946;
		break;
		
	    case 'partner':
			var type = 'partner';
			var rec_search = 'partner';
			var file_id = 9204446;
		break;
		
	    case 'customer':
			var type = 'customer';
			var rec_search = 'customer';
			var file_id = 9206650;
		break;
		
	    case 'contact':
			var type = 'contact';
			var rec_search = 'contact';
			var file_id = 7770612;
		break;
		
	    case 'portal_user':
			var type = 'customrecord_clgx_modifier';
			var rec_search = 'customrecord_clgx_modifier';
			var file_id = 7849139;
		break;
		
	    case 'service':
			var type = 'job';
			var rec_search = 'job';
			var file_id = 7715562;
		break;
		
	    case 'project':
			var type = 'job';
			var rec_search = 'job';
			var file_id = 9527127;
		break;
		
		// ============================================= transactions ===============================================

	    case 'opportunity':
			var type = 'opportunity';
			var rec_search = 'transaction';
			var file_id = 9240919;
		break;
		
	    case 'proposal':
			var type = 'estimate';
			var rec_search = 'transaction';
			var file_id = 7711011;
		break;
		
	    case 'salesorder':
			var type = 'salesorder';
			var rec_search = 'transaction';
			var file_id = 7714862;
		break;

		// =============================================
		
	    case 'invoice':
			var type = 'invoice';
			var rec_search = 'transaction';
			var file_id = 7912280;
			//var file_id = 7729358;  //_1
			//var file_id = 7729559;  //_2
			//var file_id = 7729859;  //_3
			//var file_id = 7730064;  //_4
		break;
		
	    case 'cinvoice':
			var type = 'customrecord_clgx_consolidated_invoices';
			var rec_search = 'customrecord_clgx_consolidated_invoices';
			var file_id = 7782894;
		break;
		
		// =============================================

	    case 'journalentry':
			var type = 'journalentry';
			var rec_search = 'transaction';
			var file_id = 7803804;
		break;
		
	    case 'payment':
			var type = 'customerpayment';
			var rec_search = 'transaction';
			var file_id = 7945953;
		break;
		
	    case 'purchaseorder':
			var type = 'purchaseorder';
			var rec_search = 'transaction';
			var file_id = 7954926;
		break;
		
	    case 'creditmemo':
			var type = 'creditmemo';
			var rec_search = 'transaction';
			var file_id = 7820401;
		break;
		
	    case 'vendorbill':
			var type = 'vendorbill';
			var rec_search = 'transaction';
			var file_id = 9111883;
		break;
		
	    case 'vendorcredit':
			var type = 'vendorcredit';
			var rec_search = 'transaction';
			var file_id = 9115495;
		break;
		
	    case 'vendorpayment':
			var type = 'vendorpayment';
			var rec_search = 'transaction';
			var file_id = 9115796;
		break;
		
	    case 'forecast':
			var type = 'customrecord_clgx_cap_bud_forecast';
			var rec_search = 'customrecord_clgx_cap_bud_forecast';
			var file_id = 9342701;
		break;
		
		// ============================================= inventory ===============================================
		
	    case 'device':
			var type = 'customrecord_clgx_dcim_devices';
			var rec_search = 'customrecord_clgx_dcim_devices';
			var file_id = 8806401;
		break;
		
	    case 'device_capacity':
			var type = 'customrecord_clgx_dcim_devices_capacity';
			var rec_search = 'customrecord_clgx_dcim_devices_capacity';
			var file_id = 10982741;
		break;
		
	    case 'fam':
			var type = 'customrecord_ncfar_asset';
			var rec_search = 'customrecord_ncfar_asset';
			var file_id = 8810804;
		break;
		
	    case 'space':
			var type = 'customrecord_cologix_space';
			var rec_search = 'customrecord_cologix_space';
			var file_id = 8412663;
		break;

	    case 'power':
			var type = 'customrecord_clgx_power_circuit';
			var rec_search = 'customrecord_clgx_power_circuit';
			var file_id = 8816388;
		break;
		
	    case 'xc':
			var type = 'customrecord_cologix_crossconnect';
			var rec_search = 'customrecord_cologix_crossconnect';
			var file_id = 8842641;
		break;
		
	    case 'vxc':
			var type = 'customrecord_cologix_vxc';
			var rec_search = 'customrecord_cologix_vxc';
			var file_id = 8845042;
		break;
		
	    case 'equipment':
			var type = 'customrecord_cologix_equipment';
			var rec_search = 'customrecord_cologix_equipment';
			var file_id = 8421472;
		break;

		// ============================================= crm ===============================================

	    case 'case':
			var type = 'supportcase';
			var rec_search = 'supportcase';
			var file_id = 9814281;
		break;
		
	    default:
	        //
	}
		
		if(create_file == 1){
			create_ids_file(rec_search, rec_type);
		} else {
		
			var file = nlapiLoadFile(file_id);
		    	var ids = JSON.parse(file.getValue());
		    	
		    	var file = nlapiLoadFile(file_id);
		    	var ids_new = JSON.parse(file.getValue());
			
		    	var event = 'create';
		    	var range = 1000;
		    	var repeat = 1;
		    	if(range > ids.length){
		    		range = ids.length;
		    	}
		    	
			for ( var i = 0; i < range; i++ ) {
	
				//try{
		    			var record = nlapiCreateRecord('customrecord_clgx_dw_record_queue');
		    			record.setFieldValue('custrecord_clgx_dw_rq_rid', ids[i]);
		    			record.setFieldValue('custrecord_clgx_dw_rq_uid', -4);
		    			record.setFieldValue('custrecord_clgx_dw_rq_rid', ids[i]);
		    			record.setFieldValue('custrecord_clgx_dw_rq_rt', type);
		    			record.setFieldValue('custrecord_clgx_dw_rq_et', event);
		    			record.setFieldValue('custrecord_clgx_dw_rq_processed', 'F');
		    			record.setFieldValue('custrecord_clgx_dw_rq_failed', 0);
		    			record.setFieldValue('custrecord_clgx_dw_rq_hist', 'T');
		    			var idRec = nlapiSubmitRecord(record, false,true);
		    			
		    			ids_new.splice(0, 1); 
	
				//} catch (error) {
					
				//}
				
				var usage = 10000 - parseInt(context.getRemainingUsage());
	            var index = i + 1;
	            nlapiLogExecution('DEBUG','Results ', ' | ndx = ' + index + ' / ' + ids.length + ' | id - '+ ids[i] + ' | usage - '+ usage + '  |');
	            
			}
			var file = nlapiCreateFile(rec_type + '_ids.json', 'PLAINTEXT', JSON.stringify(ids_new));
			file.setFolder(5453175);
			nlapiSubmitFile(file);
			
			if(range && repeat){
				var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
			}
		
		}

		nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
	}
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function create_ids_file(rec_search, rec_type) {
	
	var filters = [];
    var columns = [];
	var ids = [];
	
	while (true) {
		search = nlapiSearchRecord(rec_search, 'customsearch_clgx_dw_' + rec_type + '_ids', filters, columns);
			if (!search) {
				break;
			}
			for (var i in search) {
				var id = parseInt(search[i].getValue('internalid',null,'GROUP'));
				ids.push(id);
			}
			if (search.length < 1000) {
				break;
			}
			filters[0] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", id);
	}
	
	var file = nlapiCreateFile(rec_type + '_ids.json', 'PLAINTEXT', JSON.stringify(ids));
	file.setFolder(5453175);
	nlapiSubmitFile(file);
	
	return true;
}