nlapiLogExecution("audit","FLOStart",new Date().getTime());
/**
 * Created by catalinataran on 2016-06-16.
 */
//	Script File:	CLGX_SS_Create_Modifiers.js
//	Script Name:	CLGX_SS_Create_Modifiers
//	Script Id:		customscript_clgx_ss_create_modifiers


function create_modifiers() {
    try {

        var arrColumns = new Array();
        var arrFilters = new Array();

        var searchContacts = nlapiSearchRecord('customer', 'customsearch3383', arrFilters, arrColumns);
        for (var i = 0; searchContacts != null && i < searchContacts.length; i++) {

            var searchContact = searchContacts[i];
            var columns = searchContact.getAllColumns();
            var countComp = searchContact.getValue(columns[1]);
            if (countComp == 1) {
                var language = searchContact.getValue(columns[2]);
                if (language == 'US English') {
                    var lan = 1;
                }
                if (language == 'French (Canada)') {
                    var lan = 17;
                }
                var contactId = searchContact.getValue(columns[3]);
                var fname = searchContact.getValue(columns[4]);
                var ltname = searchContact.getValue(columns[5]);
//fname=fname.charAt(0).toLowerCase();
                var nameModifier = fname.charAt(0).toLowerCase() + (ltname.replace(/\s+/g, '')).toLowerCase();
                nameModifier = nameModifier.replace(/\W+/g, '');
                nameModifier = nameModifier.replace(/[0-9]/g, '');
                var arrColumns1 = new Array();
                var arrFilters1 = new Array();
                arrFilters1.push(new nlobjSearchFilter("name", null, "is", nameModifier));
                var searchModifiers = nlapiSearchRecord('customrecord_clgx_modifier', 'customsearch3384', arrFilters1, arrColumns1);
                if ((searchModifiers == null)&&(nameModifier.length>1)&&(contactId.indexOf(',') == -1)) {
                    var password = randomPassword(12);
                    var rec = nlapiCreateRecord('customrecord_clgx_modifier');
                    rec.setFieldValue('custrecord_clgx_modifier_password', password);
                    rec.setFieldValue('name', nameModifier);
                    rec.setFieldValue('custrecord_clgx_modifier_language', lan);
                    var id = nlapiSubmitRecord(rec, true);


                    var recordModifier=nlapiLoadRecord('customrecord_clgx_modifier',id);
                    var nameMod = recordModifier.getFieldValue('name');
                    recordModifier.setFieldValue('name', nameMod+id);
                    nlapiSubmitRecord(recordModifier,false, true);

                    var recordcontact=nlapiLoadRecord('contact',contactId);
                    recordcontact.setFieldValue( 'custentity_clgx_mu_flag', 'T');
                    recordcontact.setFieldValue('custentity_clgx_modifier', id);
                    nlapiSubmitRecord(recordcontact,false, false);
                }
            }
        }
    }
    catch (error) {

        if (error.getDetails != undefined) {
            nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else {
            nlapiLogExecution('ERROR', 'Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
var x=0;
function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}
