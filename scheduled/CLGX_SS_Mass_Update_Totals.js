//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');

        var context = nlapiGetContext();
        var initialTime = moment();

        var searchResults = nlapiSearchRecord('transaction', 'customsearch7328', null, null);

        for ( var j = 0; searchResults != null && j < searchResults.length; j++ ) {

            var startExecTime = moment();
            var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);

            if ( (context.getRemainingUsage() <= 100 || totalMinutes > 50) && (j+1) < searchResults.length ){
                var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                if ( status == 'QUEUED' ) {
                    nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
                    break;
                }
            }

            var searchResult = searchResults[j];
            var columns = searchResult.getAllColumns();

            var customer = searchResult.getValue('entity',null,'GROUP');
            var id = searchResult.getValue('internalid',null,'GROUP');
            var location = searchResult.getValue('location',null,'GROUP');
            var type = searchResult.getValue('class',null,'GROUP');
            //var total = searchResult.getValue('fxamount',null,'SUM');
            var total = searchResult.getValue(columns[4]);

            var isinactive = nlapiLookupField('customer', customer, 'isinactive');

            if(isinactive == 'F'){
                /*
                var rec = nlapiCreateRecord('customrecord_clgx_totals_opportunity');
                rec.setFieldValue('custrecord_clgx_totals_oppty_customer', customer);
                rec.setFieldValue('custrecord_clgx_totals_opportunity', id);
                rec.setFieldValue('custrecord_clgx_totals_oppty_location', location);
                rec.setFieldValue('custrecord_clgx_totals_oppty_class', type);
                rec.setFieldValue('custrecord_clgx_totals_oppty_total', total);
                var recID = nlapiSubmitRecord(rec, false, true);
                */

                var rec = nlapiCreateRecord('customrecord_clgx_totals_transaction');
                rec.setFieldValue('custrecord_clgx_totals_customer', customer);
                rec.setFieldValue('custrecord_clgx_totals_transaction', id);
                rec.setFieldValue('custrecord_clgx_totals_location', location);
                rec.setFieldValue('custrecord_clgx_totals_class', type);
                rec.setFieldValue('custrecord_clgx_totals_total', total);
                var recID = nlapiSubmitRecord(rec, false, true);

            }
            //nlapiSubmitField('opportunity', id, 'custbody_clgx_mass_update_flag', 'T');
            //nlapiSubmitField('estimate', id, 'custbody_clgx_mass_update_flag', 'T');
            nlapiSubmitField('salesorder', id, 'custbody_clgx_mass_update_flag', 'T');

            var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            var index = j + 1;
            nlapiLogExecution('DEBUG','SO ', ' | id =  ' + id + ' | Index = ' + index + ' of ' + searchResults.length + ' | Usage - '+ usageConsumtion + '  |');

        }

        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|');

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

