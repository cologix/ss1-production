//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();
        var initialTime = moment();

        var searchResults = nlapiSearchRecord('transaction', 'customsearch_clgx_mu_totals_transactions');

        for ( var j = 0; searchResults != null && j < searchResults.length; j++ ) {
        	
        	var startExecTime = moment();
        	var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);
        	
            if ( context.getRemainingUsage() <= 100 || totalMinutes > 50 || j > 990 ){
                var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                if ( status == 'QUEUED' ) {
             	   nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                    break; 
                }
            }

            var transaction = searchResults[j].getValue('internalid',null,'GROUP');
        	var recTransaction = nlapiLoadRecord('salesorder', transaction);
        	var customer = recTransaction.getFieldValue('entity');

			try {
				var totals = clgx_transaction_totals (customer, 'salesorder', transaction);
				nlapiSubmitField('salesorder', transaction, ['custbody_clgx_mass_update_flag'], ['T']);
	        } 
	        catch (e) {
	            var str = String(e);
	            if (str.match('INVALID_KEY_OR_REF')) {
	            	nlapiSubmitField('salesorder', transaction, ['custbody_clgx_mass_update_flag'], ['T']);
	            }
	        }
	        
	        var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            var index = j + 1;
            nlapiLogExecution('DEBUG','Results ', ' | Index = ' + index + ' of ' + searchResults.length + ' | Usage - '+ usageConsumtion + '  |');

        }
        
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
