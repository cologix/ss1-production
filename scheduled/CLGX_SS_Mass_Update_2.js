nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//Script File:	CLGX_SS_Mass_Update.js
//Script Name:	CLGX_SS_Mass_Update
//Script Id:		customscript_clgx_ss_mass_update
//Script Runs:	On Server
//Script Type:	Scheduled Script
//Deployments:	Various
//@authors:		Dan Tansanu - dan.tansanu@cologix.com
//Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
try{
    nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
    
    var context = nlapiGetContext();
    
    var columns = new Array();
	var filters = new Array();
	var ndx = 0;
	var search = nlapiLoadSearch('transaction', 'customsearch7037');
	var resultSet = search.runSearch();
	resultSet.forEachResult(function(searchResult) {
		var columns = searchResult.getAllColumns();
		var rec_id = parseInt(searchResult.getValue(columns[0]));
		var customer = parseInt(searchResult.getValue(columns[1]));
		
		var total = calculate_transaction_totals (rec_id, "so", customer);
		
		ndx = ndx + 1;
    	var usage = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
    	nlapiLogExecution('DEBUG', 'row', '| ndx : ' + ndx + ' | customer = ' + customer + ' | so_id = ' + rec_id + ' | usage : '+ usage + ' |');
		return true;
	});
    

    nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    
}
catch (error){
    if (error.getDetails != undefined){
        nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
        throw error;
    }
    else{
        nlapiLogExecution('ERROR','Unexpected Error', error.toString());
        throw nlapiCreateError('99999', error.toString());
    }
}
}

function calculate_transaction_totals (rec_id, rec_type, customer){
	
    if(rec_type == 'oppty'){
        var rec = nlapiLoadRecord('opportunity', rec_id);
    }
    else if(rec_type == 'proposal'){
        var rec = nlapiLoadRecord('estimate', rec_id);
    }
    else{
        var rec = nlapiLoadRecord('salesorder', rec_id);
    }

//extract locations totals from transaction items -----------------------------------------------------------------------

    var nbrItems = rec.getLineItemCount('item');
    var arrItems = new Array();
    for ( var i = 0; i < nbrItems; i++ ) {
        var objItem = new Object();
        var rate =  rec.getLineItemValue('item', 'rate', i + 1);
        if(rec_type == 'oppty' || rec_type == 'proposal'){
            var quantity =  rec.getLineItemValue('item', 'quantity', i + 1);
        }
        else{
            var quantity =  rec.getLineItemValue('item', 'custcol_clgx_qty2print', i + 1);
        }
        objItem["itemid"] = parseInt(rec.getLineItemValue('item', 'item', i + 1));
        objItem["location"] = parseInt(rec.getLineItemValue('item', 'location', i + 1));
        objItem["classtype"] = rec.getLineItemText('item', 'class', i + 1);
        objItem["class"] = parseInt(rec.getLineItemValue('item', 'class', i + 1));
        objItem["rate"] = parseFloat(rate);
        if(quantity != null){
            objItem["quantity"] = parseFloat(quantity);
            objItem["amount"] = parseFloat(rate) * parseFloat(quantity);
        }
        else{
            objItem["quantity"] = 1;
            objItem["amount"] = parseFloat(rate);
        }
        arrItems.push(objItem);
    }
    var arrLocationsIDs = _.sortBy(_.uniq(_.pluck(arrItems, 'location')), function(num){ return num; });

    var arrLocations = new Array();
    for ( var i = 0; arrLocationsIDs != null && i < arrLocationsIDs.length; i++ ) {

        var objLocation = new Object();
        objLocation["location"] = arrLocationsIDs[i];

        var arrLocationMRCItems = _.filter(arrItems, function(arr){
            return (arr.location == arrLocationsIDs[i] && (arr.classtype).indexOf("Recurring") > -1);
        });
        var totalMRC = 0;
        for ( var j = 0; arrLocationMRCItems != null && j < arrLocationMRCItems.length; j++ ) {
            if(arrLocationMRCItems[j].itemid != 549){
                totalMRC += arrLocationMRCItems[j].amount;
            }
        }
        objLocation["mrc"] = totalMRC;

        var arrLocationNRCItems = _.filter(arrItems, function(arr){
            return (arr.location == arrLocationsIDs[i] && (arr.classtype).indexOf("NRC") > -1);
        });
        var totalNRC = 0;
        for ( var j = 0; arrLocationNRCItems != null && j < arrLocationNRCItems.length; j++ ) {
            totalNRC += arrLocationNRCItems[j].amount;
        }
        objLocation["nrc"] = totalNRC;

        arrLocations.push(objLocation);
    }

//extract locations totals from custom record -----------------------------------------------------------------------


    if(rec_type == 'oppty'){

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_oppty_location',null,null).setSort(false));
       // arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_oppty_class',null,null).setSort(false));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_opportunity",null,"anyof",rec_id));
        //arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_oppty_class",null,"anyof",1));
        var searchTotals = nlapiSearchRecord('customrecord_clgx_totals_opportunity', null, arrFilters, arrColumns);

        var arrTotals = new Array();
        for ( var i = 0; searchTotals != null && i < searchTotals.length; i++ ) {
            var searchTotal = searchTotals[i];
            var internalid = parseInt(searchTotal.getValue('internalid',null,null));
            var location = parseInt(searchTotal.getValue('custrecord_clgx_totals_oppty_location',null,null));
            //var classtype = parseInt(searchTotal.getValue('custrecord_clgx_totals_oppty_class',null,null));
            var objLocation = new Object();
            objLocation["id"] = internalid;
            objLocation["location"] = location;
            //objLocation["classtype"] = classtype;
            arrTotals.push(objLocation);
        }
    }
    else{

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_location',null,null));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_transaction",null,"anyof",rec_id));
        //arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_oppty_class",null,"anyof",1));
        var searchTotals = nlapiSearchRecord('customrecord_clgx_totals_transaction', null, arrFilters, arrColumns);

        var arrTotals = new Array();
        for ( var i = 0; searchTotals != null && i < searchTotals.length; i++ ) {
            var searchTotal = searchTotals[i];
            var internalid = parseInt(searchTotal.getValue('internalid',null,null));
            var location = parseInt(searchTotal.getValue('custrecord_clgx_totals_location',null,null));
            //var classtype = parseInt(searchTotal.getValue('custrecord_clgx_totals_class',null,null));
            var objLocation = new Object();

            objLocation["id"] = internalid;
            objLocation["location"] = location;
            //objLocation["classtype"] = classtype;
            arrTotals.push(objLocation);
        }

    }

//compare arrays, update, add or delete locations totals -----------------------------------------------------------------------

    for ( var i = 0; arrLocations != null && i < arrLocations.length; i++ ) {

        var objTotalLocationMRC= _.find(arrTotals, function(arr){ return (arr.location == arrLocations[i].location)});
        //var objTotalLocationNRC= _.find(arrTotals, function(arr){ return (arr.location == arrLocations[i].location && arr.classtype == 2)});

        if(objTotalLocationMRC != null){ // if recurring total exist for this location, then update it
            if(rec_type == 'oppty'){
                var fields = ['custrecord_clgx_totals_oppty_total','custrecord_clgx_totals_oppty_total_nrc'];
                var values = [parseFloat(arrLocations[i].mrc).toFixed(2),parseFloat(arrLocations[i].nrc).toFixed(2)];
                nlapiSubmitField('customrecord_clgx_totals_opportunity', objTotalLocationMRC.id, fields, values);

            }
            else{
                var fields = ['custrecord_clgx_totals_total','custrecord_clgx_totals_total_nrc'];
                var values = [parseFloat(arrLocations[i].mrc).toFixed(2),parseFloat(arrLocations[i].nrc).toFixed(2)];
                nlapiSubmitField('customrecord_clgx_totals_transaction', objTotalLocationMRC.id, fields, values);

            }
        }
        else{ // if recurring total does not exist for this location, then create it
            if(rec_type == 'oppty'){
                var rec = nlapiCreateRecord('customrecord_clgx_totals_opportunity');
                rec.setFieldValue('custrecord_clgx_totals_oppty_customer', customer);
                rec.setFieldValue('custrecord_clgx_totals_opportunity', rec_id);
                rec.setFieldValue('custrecord_clgx_totals_oppty_location', arrLocations[i].location);
                //rec.setFieldValue('custrecord_clgx_totals_oppty_class', 1);
                rec.setFieldValue('custrecord_clgx_totals_oppty_total', parseFloat(arrLocations[i].mrc).toFixed(2));
                rec.setFieldValue('custrecord_clgx_totals_oppty_total_nrc', parseFloat(arrLocations[i].nrc).toFixed(2));
                var recID = nlapiSubmitRecord(rec, false, true);
            }
            else{
                var rec = nlapiCreateRecord('customrecord_clgx_totals_transaction');
                rec.setFieldValue('custrecord_clgx_totals_customer', customer);
                rec.setFieldValue('custrecord_clgx_totals_transaction', rec_id);
                rec.setFieldValue('custrecord_clgx_totals_location', arrLocations[i].location);
                //rec.setFieldValue('custrecord_clgx_totals_class', 1);
                rec.setFieldValue('custrecord_clgx_totals_total', parseFloat(arrLocations[i].mrc).toFixed(2));
                rec.setFieldValue('custrecord_clgx_totals_total_nrc', parseFloat(arrLocations[i].nrc).toFixed(2));
                var recID = nlapiSubmitRecord(rec, false, true);
            }
        }
    }

    // loop totals array and delete locations totals records that do not exist anymore on transaction
    for ( var i = 0; arrTotals != null && i < arrTotals.length; i++ ) {

        var objTotalLocation = _.find(arrLocations, function(arr){ return (arr.location == arrTotals[i].location)});

        if(objTotalLocation == null){
            if(rec_type == 'oppty'){
                nlapiDeleteRecord('customrecord_clgx_totals_opportunity', arrTotals[i].id);
            }
            else{
                nlapiDeleteRecord('customrecord_clgx_totals_transaction', arrTotals[i].id);
            }
        }
    }

    return true;
}