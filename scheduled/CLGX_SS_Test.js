function suitelet_test(request, response) {
       var headers = request.getAllHeaders();
       var output = '';

       for ( var i in headers ) {
          output += i + ' = ' + headers[i] + '<br />';
       }

       output += '<br /><br />Proxy-Client-IP: ' + request.getHeader('Proxy-Client-IP');
       response.write(output);
   }