//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
    try{
        
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();
        var initialTime = moment();
        
        var searchResults = nlapiSearchRecord('contact', 'customsearch3889');

        for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
        	
        	var startExecTime = moment();
        	var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);
        	if ( context.getRemainingUsage() <= 100 || totalMinutes > 50 || i > 990 ){
 
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    if ( status == 'QUEUED' ) {
                 	   nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                        break; 
                    }
            }
        	
        	var contactid = parseInt(searchResults[i].getValue('internalid',null,null));
			var rights = searchResults[i].getValue('custentity_clgx_cp_user_rights_json',null,null);
	    	if(rights == null || rights == ''){
	    		var oldrights = get_min_rights();
	    	} else {
	    		var oldrights = JSON.parse(nlapiLookupField('contact', contactid, 'custentity_clgx_cp_user_rights_json'));
	    	}


			var newrights = get_min_rights();
			if(oldrights.users){
				newrights.users = parseInt(oldrights.users);
			}
			if(oldrights.users){
				if(oldrights.users == 4){
					newrights.security = parseInt(oldrights.users);
				}
			}
			if(oldrights.casesfin){
				newrights.casesfin = parseInt(oldrights.casesfin);
			}
			if(oldrights.cases){
				newrights.cases = parseInt(oldrights.cases);
			}
			if(oldrights.visits){
				newrights.visits = parseInt(oldrights.visits);
			}
			if(oldrights.orders){
				newrights.orders = parseInt(oldrights.orders);
			}
			if(oldrights.invoices){
				newrights.invoices = parseInt(oldrights.invoices);
			}
			if(oldrights.ccards){
				newrights.ccards = parseInt(oldrights.ccards);
			}
			if(oldrights.colocation){
				newrights.colocation = parseInt(oldrights.colocation);
			}
			if(oldrights.network){
				newrights.network = parseInt(oldrights.network);
			}
			if(oldrights.domains){
				newrights.domains = parseInt(oldrights.domains);
			}
			if(oldrights.managed){
				newrights.managed = parseInt(oldrights.managed);
			}
			if(oldrights.reports){
				newrights.reports = parseInt(oldrights.reports);
			}
			
			var rec = nlapiLoadRecord('contact', contactid);
            rec.setFieldValue('custentity_clgx_cp_user_rights_json', JSON.stringify(newrights)); 
			rec.setFieldValue('custentity_clgx_mu_flag', 'T'); 
			nlapiSubmitRecord(rec, false, true);
			
			var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
        	nlapiLogExecution('DEBUG', 'Value', '| ContactID = ' + contactid + ' | ' + i + '/' + searchResults.length +  ' | Usage = '+ usageConsumtion + ' |');
        }
        
        nlapiLogExecution('DEBUG','Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
       
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function get_min_rights() {
    return {
        "users":0,
        "security":0,
        "cases":0,
        "casesfin":0,
        "reports":0,
        "visits":0,
        "orders":0,
        "invoices":0,
        "ccards":0,
        "colocation":0,
        "network":0,
        "domains":0,
        "managed":0
    };
}
