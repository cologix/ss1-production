//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update2(){
    try{
    	
    	var context = nlapiGetContext();

// ============================ Open Session ===============================================================================================
	    	
	    	var strPost = clgx_sp_login (username,password);
	    	var objHead = clgx_sp_header (strPost.length);
			var requestURL = nlapiRequestURL(strLogURL,strPost,objHead);
			var responseXML = nlapiStringToXML(requestURL.getBody());
			var login = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
			
			if(login == 'true'){
				
				var jsessionid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SESSIONID');
				strURL += jsessionid;
				
				nlapiLogExecution('DEBUG','LOGIN', '| jsessionid = ' + jsessionid + ' |'); 
				
				var searchContacts = nlapiSearchRecord('contact', 'customsearch_clgx_sp_customers_sync');
				
				for ( var i = 0; searchContacts != null && i < searchContacts.length; i++ ) {
		        	var searchContact = searchContacts[i];
		        	var contactid = searchContact.getValue('internalid',null,null);
	
		        	var objFile = nlapiLoadFile(2368801);
		    		var arrCSV = JSON.parse(objFile.getValue());
		            
		            // ====== Build Parameters list =================================
					var objParams = new Array();
					objParams["listid"] = '3325006';
					objParams["email"] = '';
					// ====== Build Columns Fields Array =================================
					var arrColumns = new Array();
					arrColumns.push(JSON.parse('{"name": "NS Internal ID", "value": "' + contactid + '"}'));
					var strPost = clgx_sp_recipient_delete (objParams, arrColumns);
			    	var objHead = clgx_sp_header (strPost.length);
					var requestURL = nlapiRequestURL(strURL,strPost,objHead);
					var strBody = requestURL.getBody();
					var responseXML = nlapiStringToXML(strBody);
		
					var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
					if(success == 'TRUE'){
						nlapiLogExecution('DEBUG','DELETED', '| contactid  ' + contactid + ' was deleted |'); 
					}
					else{
						nlapiLogExecution('DEBUG','NOTDELETED', '| contactid  ' + contactid + ' was not deleted |'); 
					}
		    		
		    		var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
		            var index = i + 1;
		            nlapiLogExecution('DEBUG','Results ', ' | Index = ' + index + ' of ' + searchContacts.length + ' | Usage - '+ usageConsumtion + '  |');
		
		        }

				
	// ============================ Close Session ===============================================================================================
				
		    	var strPost =  clgx_sp_logout ();
				var objHead = clgx_sp_header (strPost.length);
				var requestURL = nlapiRequestURL(strURL,strPost,objHead);
				var strBody = requestURL.getBody();
				var responseXML = nlapiStringToXML(strBody);
				var logout = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
			}
		
    	
    	
    	
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
