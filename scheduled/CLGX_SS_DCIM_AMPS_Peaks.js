nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_AMPS_Peaks.js
//	Script Name:	CLGX_SS_DCIM_AMPS_Peaks
//	Script Id:		customscript_clgx_ss_dcim_amps_peaks
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------

function scheduled__dcim_amps_peaks(){
    try{
        
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
       
    	var customerid = 10605;
    	var facilityid = 20;
    	var userid = '71418';
    	
    	var start = '2015-04-09';
    	var end = '2015-04-10';
    	
    	var customer = nlapiLookupField('customer', customerid, 'entityid');
    	var facility = nlapiLookupField('customrecord_cologix_facility', facilityid, 'name');

    	var arrPowers =  new Array();
		var arrServicesIDs = new Array();
		
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_service',null,'GROUP'));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("parent",'custrecord_cologix_power_service',"anyof",customerid));
		arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
		var searchServices = nlapiSearchRecord('customrecord_clgx_power_circuit', null, arrFilters, arrColumns);
		for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
			var serviceid = parseInt(searchServices[i].getValue('custrecord_cologix_power_service',null,'GROUP'));
			arrServicesIDs.push(serviceid);
		}
		_.uniq(arrServicesIDs);
	
		var arrIDs = new Array();
		
		var arrAMPS = new Array();
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_service",null,"anyof",arrServicesIDs));
		arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_facility",null,"anyof",facilityid));
		arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
		var searchAMPs = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_cust_pwrs_amps', arrFilters, arrColumns);
		
		for ( var i = 0; searchAMPs != null && i < searchAMPs.length; i++ ) {
			var searchAMP = searchAMPs[i];
			var columns = searchAMP.getAllColumns();

			var objAMP = new Object();
			objAMP["power"] = searchAMP.getValue('name',null,null);
			objAMP["module"] = parseInt(searchAMP.getValue(columns[9]));
			objAMP["breaker"] = parseInt(searchAMP.getValue(columns[10]));
			objAMP["ampextid"] = parseInt(searchAMP.getValue('externalid','CUSTRECORD_CLGX_DCIM_POINTS_CURRENT',null));
			arrAMPS.push(objAMP);
		}
		
		var arrKWS = new Array();
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_service",null,"anyof",arrServicesIDs));
		arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_facility",null,"anyof",facilityid));
		arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
		var searchKWs = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_cust_pwrs_kw', arrFilters, arrColumns);
		
		for ( var i = 0; searchKWs != null && i < searchKWs.length; i++ ) {
			var searchKW = searchKWs[i];
			var columns = searchKW.getAllColumns();

			var objKW = new Object();
			objKW["power"] = searchKW.getValue('name',null,null);
			objKW["module"] = parseInt(searchKW.getValue(columns[9]));
			objKW["breaker"] = parseInt(searchKW.getValue(columns[10]));
			objKW["kwextid"] = parseInt(searchKW.getValue('externalid','CUSTRECORD_CLGX_DCIM_POINTS_DEMAND',null));
			arrKWS.push(objKW);
		}
		
		var arrPowers = new Array();
		for ( var i = 0; arrAMPS != null && i < arrAMPS.length; i++ ) {
			var objPower = new Object();
			objPower["power"] = arrAMPS[i].power;
			objPower["module"] = arrAMPS[i].module;
			objPower["breaker"] = arrAMPS[i].breaker;
			objPower["ampextid"] = arrAMPS[i].ampextid;
			
			var objKW = _.find(arrKWS, function(arr){ return (arr.power == arrAMPS[i].power && arr.module == arrAMPS[i].module && arr.breaker == arrAMPS[i].breaker); });
			if(objKW != null){
				objPower["kwextid"] = objKW.kwextid;
			}
			else{
				objPower["kwextid"] = 0;
			}
			arrPowers.push(objPower);
		}

        // construct CSV file header columns
        var csv = 'Power,Module,Breaker,';
        for ( var j = 0; j < 24; j++ ) {
            for ( var k = 0; k < 4; k++ ) {
                csv += ('0' + j).slice(-2) + ':' + ('0' + (k*15)).slice(-2) + 'AMPS,';
                csv += ('0' + j).slice(-2) + ':' + ('0' + (k*15)).slice(-2) + 'KW,';
            }
        }
        csv += '\n';
        
        
        // construct totals array
        var arrTotals = new Array();
        for ( var j = 0; j < 96; j++ ) {
            arrTotals[j] = 0;
        }
        
		for ( var i = 0; arrPowers != null && i < arrPowers.length; i++ ) {

			// get values from OpenData one breaker at a time - AMPS
 			var url = '';
 			url += 'https://lucee-nnj3.dev.nac.net/odins/powers/get_points_amps_day_mins_values.cfm';
 			//url += 'https://command1.cologix.com:10313/netsuite/power/get_points_amps_day_mins_values.cfm';
 			url += '?start=' + start;
 			url += '&end=' + end;
 			url += '&id=' + arrPowers[i].ampextid;
 			
 			var requestURLAMPS = nlapiRequestURL(url);
 			var arrAMPS = (requestURLAMPS.body).split(",");

			// get values from OpenData one breaker at a time - KW
 			var url = '';
 			url += 'https://lucee-nnj3.dev.nac.net/odins/powers/get_points_amps_day_mins_values.cfm';
 			//url += 'https://command1.cologix.com:10313/netsuite/power/get_points_amps_day_mins_values.cfm';
 			url += '?start=' + start;
 			url += '&end=' + end;
 			url += '&id=' + arrPowers[i].kwextid;
 			
 			var requestURLKW = nlapiRequestURL(url);
 			var arrKWS = (requestURLKW.body).split(",");
 			
 			var arrValues = new Array();
 			for ( var j = 0; arrAMPS != null && j < arrAMPS.length; j++ ) {
 				arrValues.push(arrAMPS[j]);
 				arrValues.push(arrKWS[j]);
 			}
 			
 			//csv += arrPowers[i].power + ',' + arrPowers[i].module  + ',' + arrPowers[i].breaker + ',' + requestURLAMPS.body +  ',' + requestURLKW.body + '\n';
 			csv += arrPowers[i].power + ',' + arrPowers[i].module  + ',' + arrPowers[i].breaker + ',' + arrValues.join();
			
 			var index = i + 1;
        	var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
 			nlapiLogExecution('DEBUG','Results ', ' | Index = ' + index + ' of ' + arrPowers.length + ' | Usage - '+ usageConsumtion + '  |');
 			
		}

        var filename = customer + '_' + facility + '_amps_peak_' + start + '.csv';
        var csvFile = nlapiCreateFile(filename, 'PLAINTEXT', csv);
        nlapiSendEmail(userid,userid,'Amps Peak for '+ customer + ' on ' + start,'',null,null,null,csvFile,true);
		
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
