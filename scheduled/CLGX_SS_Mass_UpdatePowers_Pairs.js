//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 


		// Build Powers Array ------------------------------------------------------------------------------------------------------------------------------------------------
		var arrPowers = new Array();
		var searchPowers = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_mu_powers_pairs');
		var resultSet = searchPowers.runSearch();
		resultSet.forEachResult(function(searchResult) {
			var columns = searchResult.getAllColumns();
			var objPower = new Object();
			objPower["power1id"] = parseInt(searchResult.getValue('internalid',null,null));
			objPower["power1"] = searchResult.getValue('name',null,null);
			//objPower["power2id"] = parseInt(searchResult.getValue('internalid',null,null));
			//objPower["power2"] = searchResult.getValue('name',null,null);
			arrPowers.push(objPower);
			return true;
		});
		
        
        var context = nlapiGetContext();
        var initialTime = moment();
        
		for ( var i = 0; arrPowers != null && i < arrPowers.length; i++ ) {
			
        	var startExecTime = moment();
        	var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);
        	
            if ( context.getRemainingUsage() <= 100 || totalMinutes > 50){
                var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                if ( status == 'QUEUED' ) {
             	   nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                    break; 
                }
            }
        	
			var power2 = get_power2_name (arrPowers[i].power1);
			var objPower2 = _.find(arrPowers, function(arr){ return (arr.power1 == power2); });
			
			if(objPower2 != null){
				nlapiSubmitField('customrecord_clgx_power_circuit', arrPowers[i].power1id, 'custrecord_clgx_dcim_pair_power', objPower2.power1id);
				nlapiSubmitField('customrecord_clgx_power_circuit', objPower2.power1id, 'custrecord_clgx_dcim_pair_power', arrPowers[i].power1id);
				
				//arrPowers[i].power2id = objPower2.power1id;
				//arrPowers[i].power2 = objPower2.power1;
			}
			
			
            var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            var index = i + 1;
            nlapiLogExecution('DEBUG','SO ', ' | Index = ' + index + ' of ' + arrPowers.length + ' | Usage - '+ usageConsumtion + '  |');

		}
        
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function get_power2_name (power) {
	var power2 = '';
	var lastChar = power.substr(power.length - 1);
	
	if(lastChar == 'A'){
		power2 = power.slice(0, - 1) + 'B';
	}
	else{
		power2 = power.slice(0, - 1) + 'A';
	}
	return  power2;
}
