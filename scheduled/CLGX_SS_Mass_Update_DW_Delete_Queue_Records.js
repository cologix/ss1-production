nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//Script File:	CLGX_SS_Mass_Update.js
//Script Name:	CLGX_SS_Mass_Update
//Script Id:		customscript_clgx_ss_mass_update
//Script Runs:	On Server
//Script Type:	Scheduled Script
//Deployments:	Various
//@authors:		Dan Tansanu - dan.tansanu@cologix.com
//Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
try{
    nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
    
    var context = nlapiGetContext();

    
    
	var columns = [];
	columns.push(new nlobjSearchColumn('internalid',null,null));
	var filters = [];
	//var searchResults = nlapiSearchRecord('customrecord_clgx_dw_record_queue', 'customsearch_clgx_dw_queue_current_fail', filters, columns);
	//var searchResults = nlapiSearchRecord('customrecord_clgx_dw_record_queue', 'customsearch_clgx_dw_queue_hist_failed', filters, columns);
	
	var searchResults = nlapiSearchRecord('customrecord_clgx_dw_record_queue', 'customsearch_clgx_dw_queue_hist_2', filters, columns);
	
	
	//var searchResults = nlapiSearchRecord('customrecord_clgx_dcim_devices_capacity', null, filters, columns);
	
	
	
	for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
    	
        if ( context.getRemainingUsage() <= 1000 || i > 990 ){
            var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
            if ( status == 'QUEUED' ) {
         	   nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
                break; 
            }
         }
    	
    	var searchResult = searchResults[i];
    	var recordID = searchResult.getValue('internalid',null,null);
    	
    	//nlapiSubmitField('customrecord_clgx_dw_record_queue', recordID, ['custrecord_clgx_dw_rq_processed','custrecord_clgx_dw_rq_error'], ['F',0]);
    	//nlapiDeleteRecord('customrecord_clgx_dw_record_queue', recordID);
    	
    	nlapiDeleteRecord('customrecord_clgx_dw_record_queue', recordID);

    	var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
    	nlapiLogExecution('DEBUG', 'Value', 'ID = ' + i + '/' + searchResults.length +  ' / Usage - '+ usageConsumtion);
    }
    

    nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    
}
catch (error){
    if (error.getDetails != undefined){
        nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
        throw error;
    }
    else{
        nlapiLogExecution('ERROR','Unexpected Error', error.toString());
        throw nlapiCreateError('99999', error.toString());
    }
}
}
