//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update_2(){
    try{
        //nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();
        var initialTime = moment();
        
    	var objFile = nlapiLoadFile(2410387);
		var csv = (objFile.getValue()).replace(/\"/g,"");
		var arrCSV = JSON.parse(csvJSON(csv));
        var arrContacts = _.filter(arrCSV, function(arr){
            return (arr.recipientid != '' && (arr.firstname != '' || arr.lastname != ''));
        });

		for ( var j = 0; arrContacts != null && j < arrContacts.length; j++ ) {
			
	        var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("externalid",null,"is",arrContacts[j].recipientid));
			var searchContact = nlapiSearchRecord('contact', null, arrFilters, arrColumns);
			
			if(searchContact != null){
				
                var comments = 'New Contact from SilverPOP\n\n' +
                'Customer =  ' + arrContacts[j].customer + '\n' +
                'Phone =  ' + arrContacts[j].phone + '\n\n';
				
				
				var fields = ['firstname','lastname','title','comments'];
				var values = [arrContacts[j].firstname,arrContacts[j].lastname,arrContacts[j].title,comments];
				nlapiSubmitField('contact', searchContact[0].getValue('internalid',null,null), fields, values);
				
			}
			
			
        }
		
        //nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


//var csv is the CSV file with headers
function csvJSON(csv){
 
  var lines=csv.split("\n");
  var result = [];
  var headers=["recipientid","contactid","customerid","email","customer","firstname","lastname","phone","title"];
 
  for(var i=1;i<lines.length;i++){
 
	  var obj = {};
	  var currentline=lines[i].split(",");
 
	  for(var j=0;j<headers.length;j++){
		  obj[headers[j]] = currentline[j];
	  }
	  result.push(obj);
  }
  return JSON.stringify(result);
}

function replace_chars (str){
	str = str.replace(/\,/g," ");
	str = str.replace(/\&/g," and ");
	str = str.replace(/\"/g,"");
	str = str.replace(/\'/g,"");
	str = str.replace(/\;/g,"");
	str = str.replace(/\:/g,"");
	str = str.replace(/\s{2,}/g, ' ');
	str = str.replace(/\t/g, ' ');
	str = str.toString().trim().replace(/(\r\n|\n|\r)/g,"");
  return str;
}
