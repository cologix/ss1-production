nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        
        var search = nlapiSearchRecord('transaction', 'customsearch_clgx_dw_salesorder_noserv');
		for ( var i = 0; search != null && i < search.length; i++ ) {
			
			var so_id = parseInt(search[i].getValue('internalid',null,"GROUP"));

            var recSO = nlapiLoadRecord('salesorder', so_id);
            var sonbr = recSO.getFieldValue('tranid');
            var customer = recSO.getFieldValue('entity');
            var subsidiary = recSO.getFieldValue('subsidiary');
            var location = recSO.getFieldValue('location');

            var nbrItems = recSO.getLineItemCount('item');
            for (var j = 0; j < nbrItems; j++){

                var itemServiceID = recSO.getLineItemValue('item', 'custcol_clgx_so_col_service_id', j + 1);
                var itemBillSched = recSO.getLineItemText('item', 'billingschedule', j + 1);
                var itemFacility = parseInt(clgx_return_facilityid(location));
                var itemClass = recSO.getLineItemText('item', 'class', j + 1);
                
                if (itemClass.indexOf("Recurring") > -1 && itemBillSched != null && itemBillSched != '' && itemBillSched != 'Non Recurring' && (itemServiceID == null || itemServiceID == '' || itemServiceID == -1)) {
                	
                    // Get a new ID for the Service to create it
                    var objNewId = nlapiLoadRecord('customrecord_clgx_auto_generated_numbers', 1);
                    var stLastId = objNewId.getFieldValue('custrecord_clgx_auto_generated_number');
                    var intNextId = parseInt(stLastId) + 1;
                    objNewId.setFieldValue('custrecord_clgx_auto_generated_number', parseInt(intNextId));
                    nlapiSubmitRecord(objNewId,false,true);

                    var stId = new String(intNextId);
                    var stPrefix = 'S00000000';
                    var stNextId = stPrefix.substring(0,9-parseInt(stId.length)) + stId;

                    // create service
                    var record = nlapiCreateRecord('job');
                    record.setFieldValue('entityid', stNextId);
                    record.setFieldValue('custentity_cologix_service_order', arrNewSOs[i]);
                    record.setFieldValue('parent', customer);
                    record.setFieldValue('subsidiary', subsidiary);
                    record.setFieldValue('custentity_cologix_facility', itemFacility);
                    record.setFieldText('custentity_cologix_int_prj', 'No');
                    var idRec = nlapiSubmitRecord(record, false, true);

                    var usage = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                    nlapiLogExecution('DEBUG', 'Value', '| SO = ' + sonbr + ' | Service = ' + stNextId + ' | Usage = '+ usage + ' |');

                }
                else{}
                recSO.setLineItemValue('item', 'custcol_clgx_so_col_service_id', j + 1, idRec);
            }
            var soid = nlapiSubmitRecord(recSO, false, true);
		}

        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function clgx_return_facilityid (locationid){

    var facilityid = 0;
    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('custrecord_clgx_facility_location',null,'anyof', locationid));
    arrFilters.push(new nlobjSearchFilter('isinactive',null,'is', 'F'));
    var searchResults = nlapiSearchRecord('customrecord_cologix_facility', null, arrFilters, arrColumns);

    if(searchResults != null){
        facilityid = searchResults[0].getValue('internalid',null,null);
    }
    return facilityid;
}
