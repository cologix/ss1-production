//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();
        var initialTime = moment();
        
        var powers = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch4486');
        
        for ( var i = 0; powers != null && i < powers.length; i++ ) {
        	
        	var startExecTime = moment();
        	var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);
        	
            if ( context.getRemainingUsage() <= 2000 || totalMinutes > 50 || i > 990 ){
 
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    if ( status == 'QUEUED' ) {
                 	   nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                        break; 
                    }
            }

        	var powerid = powers[i].getValue('internalid',null,null);
        	
        	var filters = [];
			filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_day_power",null,"anyof",powerid));
			var days = nlapiSearchRecord('customrecord_clgx_dcim_points_day', 'customsearch_clgax_mu_pwr_day_delete', filters);
			
			for ( var j = 0; days != null && j < days.length; j++ ) {
				var dayid = parseInt(days[j].getValue('internalid',null,null));
				nlapiDeleteRecord('customrecord_clgx_dcim_points_day', dayid);
			}
        	
        	nlapiSubmitField('customrecord_clgx_power_circuit', powerid, ['custrecord_clgx_dcim_date_last_replicate','custrecord_clgx_dcim_mu'], ['5/3/2017','T']);
        	
        	var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            var index = i + 1;
            nlapiLogExecution('DEBUG','Results ', ' | Power = ' + powerid + ' | Index = ' + index + ' of ' + powers.length + ' | Usage - '+ usageConsumtion + '  |');

        }
        
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
