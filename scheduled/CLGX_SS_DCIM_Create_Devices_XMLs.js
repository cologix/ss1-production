nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_Create_Devices_XML.js
//	Script Name:	CLGX_SS_DCIM_Create_Devices_XML
//	Script Id:		customscript_clgx_ss_dcim_create_dev_xml
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_dcim_create_dev_xml(){
    try{
        
    	nlapiLogExecution('DEBUG','Execution', '|--------------------------Start Scheduled Script--------------------------|'); 
        
    	
    	var context = nlapiGetContext();
    	var ndx = parseInt(context.getSetting('SCRIPT','custscript_clgx_devices_xml_ndx')) || 1;
		
        var pos = 43;
        
    	if(pos == 43){
    		var template = 4818512;
    		var folder = 3925605;
    	}
    	if(pos == 24){
    		var template = 4794034;
    		var folder = 3368360;
    	}
    	if(pos == 30){
    		var template = 4794035;
    		var folder = 3368361;
    	}
    	if(pos == 42){
    		var template = 4794036;
    		var folder = 3368362;
    	}
    	if(pos == 421){
    		var template = 4794043;
    		var folder = 3832331;
    	}
    	if(pos == 54){
    		var template = 4794037;
    		var folder = 3368363;
    	}
    	if(pos == 63){
    		var template = 4794038;
    		var folder = 3368464;
    	}
    	if(pos == 66){
    		var template = 4794042;
    		var folder = 3368465;
    	}
    	if(pos == 72){
    		var template = 4794040;
    		var folder = 3368566;
    	} 
    	if(pos == 84){
    		var template = 4794039;
    		var folder = 3368567;
    	}
    	
		for ( var i = ndx; i < (ndx + 7); i++ ) {
    	
			for ( var j = 1; j < 46; j++ ) {
    			
    	    	var objFile = nlapiLoadFile(template);
    			var newxml = objFile.getValue();
    			
    			newxml = newxml.replace(/\.10\.1\.1\./g, '.10.' + i + '.' + j + '.');
    			newxml = newxml.replace(/\.13\.1\.1\./g, '.13.' + i + '.' + j + '.');
    			newxml = newxml.replace(/\.14\.1\.1\./g, '.14.' + i + '.' + j + '.');
    			newxml = newxml.replace(/\.15\.1\.1\./g, '.15.' + i + '.' + j + '.');
    			newxml = newxml.replace(/\.3\.1\.1\./g, '.3.' + i + '.' + j + '.');
    			newxml = newxml.replace(/\.5\.1\.1\./g, '.5.' + i + '.' + j + '.');
    			newxml = newxml.replace(/\.7\.1\.1\./g, '.7.' + i + '.' + j + '.');
    			newxml = newxml.replace(/\.8\.1\.1\./g, '.8.' + i + '.' + j + '.');
    			
    			newxml = newxml.replace(/\.12\.1\.1"/g,  '.12.' + i + '.' + j + '"');
    			newxml = newxml.replace(/\.10\.1\.1"/g,  '.10.' + i + '.' + j + '"');
    			newxml = newxml.replace(/\.9\.1\.1"/g,  '.9.' + i + '.' + j + '"');
    			newxml = newxml.replace(/\.8\.1\.1"/g,  '.8.' + i + '.' + j + '"');
    			newxml = newxml.replace(/\.7\.1\.1"/g,  '.7.' + i + '.' + j + '"');
    			newxml = newxml.replace(/\.6\.1\.1"/g,  '.6.' + i + '.' + j + '"');
    			newxml = newxml.replace(/\.5\.1\.1"/g,  '.5.' + i + '.' + j + '"');
    			
    			var filename = i + '-' + j + '.xml';
 		 		var xmlFile = nlapiCreateFile(filename, 'PLAINTEXT', newxml);
 		 		xmlFile.setFolder(folder);
 				var fileID = nlapiSubmitFile(xmlFile);
    			
 				var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
 				nlapiLogExecution('DEBUG','Results ', ' | File = ' + filename + ' | Consumtion = ' + usageConsumtion + ' |');
    		}
    	}
    	
    	if(i < 35){
    		var arrParam = new Array();
	        arrParam['custscript_clgx_devices_xml_ndx'] = i;
	        nlapiScheduleScript('customscript_clgx_ss_dcim_create_dev_xml', 'customdeploy_clgx_ss_dcim_create_dev_xml' ,arrParam);  
    	}
        
        nlapiLogExecution('DEBUG','Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
       
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
