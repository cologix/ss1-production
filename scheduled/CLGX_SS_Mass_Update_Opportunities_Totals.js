//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();
        var initialTime = moment();
        
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_opportunity',null,'GROUP'));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_oppty_location',null,'GROUP'));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_totals_opportunity',null,'noneof','@NONE@'));
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_totals_oppty_class',null,'anyof',1));
		arrFilters.push(new nlobjSearchFilter('custrecord_clgx_totals_oppty_mu',null,'is','F'));
		var searchResults = nlapiSearchRecord('customrecord_clgx_totals_opportunity', null, arrFilters, arrColumns);

        for ( var j = 0; searchResults != null && j < searchResults.length; j++ ) {
        	
        	var startExecTime = moment();
        	var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);
        	
            if ( context.getRemainingUsage() <= 100 || totalMinutes > 50 || j > 990 ){
                var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                if ( status == 'QUEUED' ) {
             	   nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                    break; 
                }
            }

        	var transaction = searchResults[j].getValue('custrecord_clgx_totals_opportunity',null,'GROUP');
        	var location = searchResults[j].getValue('custrecord_clgx_totals_oppty_location',null,'GROUP');

        	var nrc = 0;
    		var arrColumns = new Array();
    		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_oppty_total',null,null));
    		var arrFilters = new Array();
    		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_opportunity",null,"anyof",transaction));
    		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_oppty_location",null,"anyof",location));
    		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_oppty_class",null,"anyof",2));
    		var searchNRC = nlapiSearchRecord('customrecord_clgx_totals_opportunity', null, arrFilters, arrColumns);
        	if(searchNRC != null){
        		nrc = parseFloat(searchNRC[0].getValue('custrecord_clgx_totals_oppty_total',null,null));
        	}
        	
        	
        	var internalid = 0;
        	var arrColumns = new Array();
    		arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    		var arrFilters = new Array();
    		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_opportunity",null,"anyof",transaction));
    		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_oppty_location",null,"anyof",location));
    		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_oppty_class",null,"anyof",1));
    		var searchMRC = nlapiSearchRecord('customrecord_clgx_totals_opportunity', null, arrFilters, arrColumns);
        	if(searchMRC != null){
        		internalid = parseInt(searchMRC[0].getValue('internalid',null,null));
        	}
        	
        	if(internalid > 0){
        		nlapiSubmitField('customrecord_clgx_totals_opportunity', internalid, ['custrecord_clgx_totals_oppty_total_nrc','custrecord_clgx_totals_oppty_mu'], [nrc,'T']);
        	}
        	
        	var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            var index = j + 1;
            nlapiLogExecution('DEBUG','Results ', ' | Index = ' + index + ' of ' + searchResults.length + ' | Usage - '+ usageConsumtion + '  |');

        }
        
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
