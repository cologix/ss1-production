//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update_2(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();
        var initialTime = moment();
        
        var deviceid = 367;
        	
        
    	var columns = [];
    	columns.push(new nlobjSearchColumn('internalid',null,null).setSort(true));
		var filters = [];
		filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_device",null,"anylike",deviceid));
		var search = nlapiSearchRecord('customrecord_clgx_dcim_points', null, filters, columns);
		
		for ( var i = 0; search != null && i < search.length; i++ ) {
			
			var id = parseInt(search[i].getValue('internalid',null,null));
			nlapiDeleteRecord('customrecord_clgx_dcim_points', id);
			
			var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            var index = j + 1;
            nlapiLogExecution('DEBUG','Results ', ' | Index = ' + index + ' of ' + searchResults.length + ' | Usage - '+ usageConsumtion + '  |');
		}
		nlapiDeleteRecord('customrecord_clgx_dcim_devices', deviceid);

        
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
