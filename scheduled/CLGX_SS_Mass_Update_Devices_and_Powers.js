//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();
        
        var devices = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch3975');
        
        //for ( var i = 0; devices != null && i < devices.length; i++ ) {
        for ( var i = 0; devices != null && i < 10; i++ ) {
        	
        	var deviceid = parseInt(devices[i].getValue('custrecord_clgx_dcim_device',null,'GROUP'));

    		var arrParam = new Array();
            arrParam['custscript_clgx_device_update_id'] = deviceid;
            nlapiScheduleScript('customscript_clgx_ss_device_update', null ,arrParam);  
        	
        	var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            var index = i + 1;
            nlapiLogExecution('DEBUG','Results ', ' | Deviceid = ' + deviceid + ' | Index = ' + index + ' of ' + devices.length + ' | Usage - '+ usageConsumtion + '  |');

        }
        
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
