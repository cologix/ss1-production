//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();
        var searchInactives = nlapiSearchRecord('transaction', 'customsearch_clgx_sos_with_inactives', null, null);
    	var arrSOs = new Array();
    	for ( var i = 0; searchInactives != null && i < searchInactives.length; i++ ) {
    		var searchInactive = searchInactives[i];
    		var soid = searchInactive.getValue('internalid', null, 'GROUP');
			//nlapiSubmitField('salesorder', soid, 'custbody_clgx_ready_to_uplift', 'F');
    		arrSOs.push(soid);
    	}
        var arrColumns = new Array();
        var arrFilters = new Array();
        //arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",172521));
        if(arrSOs.length > 0){
        	arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",arrSOs));
        }
        var searchResults = nlapiSearchRecord('transaction', 'customsearch_clgx_contracts_renewals_3', arrFilters, arrColumns);

        var initialTime = moment();
        
        for ( var j = 0; searchResults != null && j < searchResults.length; j++ ) {
        	
        	var startExecTime = moment();
        	var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(2);
        	
            if ( (context.getRemainingUsage() <= 1000 || totalMinutes > 50) && (j+1) < searchResults.length ){
               var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
               if ( status == 'QUEUED' ) {
            	   nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
                   break; 
               }
            }

        	var searchResult = searchResults[j];
        	var columns = searchResult.getAllColumns();
        	
        	
        	var internalid = searchResult.getValue('internalid',null,'GROUP');
        	
        	var discoTerms = searchResult.getValue('custentity_cglx_disco_notice_terms','customerMain','GROUP');
        	var renewTerms = searchResult.getValue('custentity_clgx_cust_renewal_term','customerMain','GROUP');
        	var billingTerms = searchResult.getValue('custbody_cologix_biling_terms',null,'GROUP');
        	
        	var contractStartDate = searchResult.getValue('custbody_cologix_so_contract_start_dat',null,'GROUP');
        	var contractEndDate = searchResult.getValue('enddate',null,'GROUP');

        	
        	var renewStartDate = searchResult.getValue('custbody_clgx_renewal_start_date',null,'GROUP');
        	var renewEndDate = searchResult.getValue('custbody_clgx_renewal_end_date',null,'GROUP');
        	var renewCycle = searchResult.getValue('custbody_clgx_contract_cycle',null,'GROUP');
        	
        	
        	
        	
        	
        	var internalid = searchResult.getValue(columns[0]);
        	var endDate = searchResult.getValue(columns[1]);
        	var startDate = searchResult.getValue(columns[2]);
        	var cycle = searchResult.getValue(columns[3]);
        	
        	
        	
        	
        	
        	
        	
        	
        	
        	
        	
           	var index = j + 1;
        	var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
        	nlapiLogExecution('DEBUG', 'Value', '| ID = ' + internalid + ' | Index = ' + index + ' of ' + searchResults.length +  ' | Usage = '+ usageConsumtion + ' | Minutes = ' + totalMinutes + ' |');
        	
        	
        	if(endDate != null && endDate != ''){
        		/*
            	var myRecord = nlapiLoadRecord('salesorder', internalid);
            	myRecord.setFieldValue('custbody_clgx_renewal_start_date', startDate);
            	myRecord.setFieldValue('custbody_clgx_renewal_end_date', endDate);
            	myRecord.setFieldValue('custbody_clgx_contract_cycle', cycle);
            	myRecord.setFieldValue('custbody_clgx_mu_flag', 'T');
            	nlapiSubmitRecord(myRecord, false, true);
        		*/
            	var fields = ['custbody_clgx_renewal_start_date','custbody_clgx_renewal_end_date','custbody_clgx_contract_cycle','custbody_clgx_mu_flag'];
    			var values = [startDate, endDate, cycle, 'T'];
    			nlapiSubmitField('salesorder', internalid, fields, values);
        	}
        }
        
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

