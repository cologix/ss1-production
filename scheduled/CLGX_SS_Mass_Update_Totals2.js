//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');

        var context = nlapiGetContext();
        var initialTime = moment();
        //var stopAt = 6;

        var searchResults = nlapiSearchRecord('transaction', 'customsearch7328', null, null);

        for ( var j = 0; searchResults != null && j < searchResults.length; j++ ) {

            var startExecTime = moment();
            var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);

            if ( (context.getRemainingUsage() <= 100 || totalMinutes > 50) && (j+1) < searchResults.length ){

                //if(parseInt(startExecTime.format('H')) < stopAt){

                var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                if ( status == 'QUEUED' ) {
                    nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                    break;
                }
                //}
                //else{
                //	nlapiLogExecution('DEBUG','End', status + ' / Cancel scheduled script due to stop at hour reached.');
                //	break;
                //}
            }

            var searchResult = searchResults[j];
            var customerid = searchResult.getValue('entityid',null,'GROUP');
            var soid = searchResult.getValue('internalid',null,'GROUP');

            var totals = clgx_transaction_totals (customerid, 'so', soid);

            nlapiSubmitField('salesorder', soid, 'custbody_clgx_mass_update_flag', 'T');

            var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            var index = j + 1;
            nlapiLogExecution('DEBUG','SO ', ' | soid =  ' + soid + ' | Index = ' + index + ' of ' + searchResults.length + ' | Usage - '+ usageConsumtion + '  |');

        }

        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|');

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

