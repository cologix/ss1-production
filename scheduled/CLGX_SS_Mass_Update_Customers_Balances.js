//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();
        var initialTime = moment();

        var searchResults = nlapiSearchRecord('customer', 'customsearch_clgx_mu_customer_balance');

        for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
        	
        	var startExecTime = moment();
        	var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);
        	
            if ( context.getRemainingUsage() <= 1000 || totalMinutes > 50 || i > 990 ){
            	var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                break; 
            }
            
            try {
	            var customerid = parseInt(searchResults[i].getValue('internalid',null,null));
	            var balance = clgx_update_balances (customerid);
	            nlapiSubmitField('customer', customerid, 'custentity_clgx_mu_flag', 'T');
			}
			catch (error) {
				nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
			}

            var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            var index = i + 1;
            nlapiLogExecution('DEBUG','Results ', ' | Index = ' + index + ' | Balance = ' + balance + ' | Usage = '+ usageConsumtion + '  |');

        }
        
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
