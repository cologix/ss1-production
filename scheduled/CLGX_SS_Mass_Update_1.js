nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        

    	var context = nlapiGetContext();
    	
    	var ids = [300780,597924,604088,604236,605504,605506,605511,605512,605513,605637,605678,605684,605713,605840,605842,605844,605848,605854,605894,605904,605921,606053,606060,606076,606087,606091,606097,606100,606232,606242,606246,606248,606282,606283,606288,606301,606306,606330,606331,606432,606451,606479,606484,606489,606491,606497,607246,607261,607279,607547,607567,607579,607582,607611,607614,607662,607666,607684,607738,607758,607802,627891,627984,633263,633265,633271,644682,658573,682581,723269,778207,850408,850518,859394,979552,1001927,1224131,1251294,1316200,1317736,1438289,1448808,1458737,1858298,1880166,1923376];
		var cases = get_customers (ids);
		cases = _.sortBy(cases, function(obj){ return obj.customerid;});
		
		var caseid = 12541924;
		var rec = nlapiLoadRecord('supportcase', caseid);
        
        var casenumber = rec.getFieldValue('casenumber');
	    var customer = rec.getFieldValue('company');
	    var title = rec.getFieldValue('title');
	    var assigned = rec.getFieldText('assigned');
        
    	var subsidiary = rec.getFieldValue('subsidiary');
    	var facility = rec.getFieldValue('custevent_cologix_facility');
    	var facilityname = rec.getFieldText('custevent_cologix_facility');
    	var profile = rec.getFieldValue('profile');
    	
    	var casetype = rec.getFieldValue('category');
    	var casetypename = rec.getFieldText('category');
	    var subcasetype = rec.getFieldValue('custevent_cologix_sub_case_type');
	    
	    var priority = rec.getFieldText('priority');
	    var status = rec.getFieldText('status');
	    
        var notify = rec.getFieldValue('custevent_clgx_customer_notification');
	    var followup = rec.getFieldValue('custevent_cologix_case_sched_followup');
    	var starttime = rec.getFieldValue('custevent_cologix_sched_start_time');
    	
    	var subcasestatus = status;
    	
    	var subcases = [];
    	for ( var i = 0; i < cases.length; i++ ) {
    		
    		var listEmails = '';
        	if(cases[i].emails != null){
        		listEmails = cases[i].emails.join();
        	}
        	if(listEmails.length > 300){
        		while (listEmails.length > 300) {
        			cases[i].emails.pop();
        			listEmails = cases[i].emails.join();
        		}
        	}
        	
        	var casesubsidiary = nlapiLookupField('customer', cases[i].customerid, 'subsidiary');
    		
        	var record = nlapiCreateRecord('supportcase');
            record.setFieldValue('title', 'Childcase of ' + casenumber + ' - ' + casetypename + ' at ' + facilityname);
            record.setFieldValue('company', cases[i].customerid);
            record.setFieldValue('custevent_cologix_facility', facility);
            record.setFieldValue('subsidiary', casesubsidiary);
            record.setFieldText('assigned', assigned);
            record.setFieldValue('email', listEmails);
            record.setFieldText('status', subcasestatus);
            record.setFieldText('priority', priority);
            record.setFieldValue('category', casetype);
            record.setFieldValue('custevent_cologix_sub_case_type', subcasetype);
            record.setFieldValue('custevent_cologix_case_sched_followup', followup);
            record.setFieldValue('custevent_cologix_sched_start_time', starttime);
            record.setFieldValue('custevent_clgx_parent_id', caseid);
            record.setFieldValue('origin', 6);
            //var idRec = nlapiSubmitRecord(record, false, true);
            //var idRec = 1;
            
            var index = i +1;
			var usage = 10000 - parseInt(context.getRemainingUsage());
            nlapiLogExecution('DEBUG','debug', '| Case = ' + index + ' / ' + cases.length + ' | Customer = ' + cases[i].customerid + ' | subcase_id - '+ idRec + ' | Usage - '+ usage + '  |');
    		
    	}
    	
		nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function get_customers (ids){

	var customers = [];
	var contacts = [];
	
	if(ids.length > 0){
		var columns = new Array();
		var filters = new Array();
		filters[0] = new nlobjSearchFilter("internalid", "company", "anyof",ids);
		filters[1] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", 0);
		while (true) {
			search = nlapiSearchRecord('contact', 'customsearch_clgx_cmd_mops_contacts', filters, columns);
			if (!search) {
				break;
			}
			for (var i in search) {
				var id = parseInt(search[i].getValue('internalid',null,null));
				contacts.push ({
					"node": search[i].getValue('entityid', null,null),
					"type": 'contact',
					"customerid": parseInt(search[i].getValue('company', null,null)),
					"customer": search[i].getText('company', null,null),
					"contactid": parseInt(search[i].getValue('internalid', null,null)),
					"contact": search[i].getValue('entityid', null,null),
					"email": search[i].getValue('email', null,null),
					"notif": search[i].getValue('custentity_clgx_ops_outage_notification', null,null),
					"faicon": 'user',
					"leaf": true
				});
			}
			if (search.length < 1000) {
				break;
			}
			filters[0] = new nlobjSearchFilter("internalid", "company", "anyof",ids);
			filters[1] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", id);
		}
	}


	
	for ( var i = 0; i < ids.length; i++ ) {
		
		var selection = _.filter(contacts, function(arr){
			return (arr.customerid == ids[i] && arr.notif == 'T');
		});
		if(selection == null || selection.length == 0){
			selection = _.filter(contacts, function(arr){
				return (arr.customerid == ids[i]);
			});
		}

		selection = _.map(selection, function(obj) { return _.omit(obj, ['customerid', 'customer']); });
		var emails = _.compact(_.uniq(_.pluck(selection, 'email')));
		
		if(selection.length > 0){
			var customer = nlapiLookupField('customer', ids[i], 'entityid');
    		customers.push({
    			"node": customer,
    			"type": 'customer',
    			"customerid": ids[i],
    			"customer": customer,
    			"emails": emails,
    			"count": selection.length,
    			"faicon": 'users',
    			"expanded": false,
    			"leaf": false,
    			"children": selection
    		});
		}
	}
	customers = _.sortBy(customers, function(obj){ return obj.node;});
	
	return customers;
}
