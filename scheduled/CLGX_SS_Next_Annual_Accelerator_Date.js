//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Next_Annual_Accelerator_Date.js
//	Script Name:	CLGX_SS_Next_Annual_Accelerator_Date
//	Script Id:		customscript_clgx_next_aa_date
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Sales Order - Sandbox | 
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		1/5/2012
//	Updated:		1/5/2012
//-------------------------------------------------------------------------------------------------

function nextAADate () {
	try {
		nlapiLogExecution('DEBUG','User Event - nextAADate','|--------------------STARTED---------------------|');
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 1/5/2012
// Details:	This script will calculate and write the next annual accelerator date on service orders
//-----------------------------------------------------------------------------------------------------------------
	
	var date = new Date();
	var arrFilters = new Array();
	var arrColumns = new Array();
	
	arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
	arrFilters[0] = new nlobjSearchFilter('custbody_clgx_next_aa_date',null,'onorbefore',date);
	arrFilters[1] = new nlobjSearchFilter('mainline',null,'is','T');
	var searchResults = nlapiSearchRecord('salesorder', null, arrFilters, arrColumns);
	
	var arrOSIds = new Array();
	for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
		
		/*
		var searchResult = searchResults[ i ];
		var stId = searchResult.getValue('internalid');
		
		var fields = ['tranid', 'custbody_cologix_so_contract_start_dat', 'custbody_cologix_biling_terms', 'custbody_clgx_next_aa_date'];
		var arrSO = nlapiLookupField('salesorder', stId, fields);
		var stTranId = arrSO.tranid;
		var stContractDate = arrSO.custbody_cologix_so_contract_start_dat;
		var stServiceTerm = arrSO.custbody_cologix_biling_terms;
		var stNextAADate = arrSO.custbody_clgx_next_aa_date;
		var dtCalcAADate = nlapiAddMonths(nlapiStringToDate(stNextAADate), 12);
		var dtCalcContractDate = nlapiAddMonths(nlapiStringToDate(stContractDate), parseInt(stServiceTerm));
		
		if (dtCalcAADate < dtCalcContractDate){
			nlapiSubmitField('salesorder', stId, 'custbody_clgx_next_aa_date', nlapiDateToString(dtCalcAADate), true); 
			arrOSIds.push(stTranId);
			}
		*/
			
		var searchResult = searchResults[ i ];
		var stId = searchResult.getValue('internalid');
		
		var fields = ['tranid', 'enddate', 'custbody_clgx_next_aa_date'];
		var arrSO = nlapiLookupField('salesorder', stId, fields);
		var stTranId = arrSO.tranid;
		var stNextAADate = arrSO.custbody_clgx_next_aa_date;
		var stEndDate = arrSO.enddate;
		var dtEndDate = nlapiStringToDate(stEndDate);
		var dtCalcAADate = nlapiAddMonths(nlapiStringToDate(stNextAADate), 12);
		
		if (dtCalcAADate < dtEndDate) {
			nlapiSubmitField('salesorder', stId, 'custbody_clgx_next_aa_date', nlapiDateToString(dtCalcAADate), true); 
			arrOSIds.push(stTranId);
		}

	}
	
	if (arrOSIds.length > 0){
		var stSOTranIds = '';
		for (var j=0; j<arrOSIds.length; j++) {
			stSOTranIds += arrOSIds[j] + '\n';
		}
		var emailSubject = 'Daily Report - Next Annual Accelerator Date Updates (' + arrOSIds.length + ' service orders have been updated today)';
		var emailBody = 'The next annual accelerator date has been updated for these service orders:\n' + stSOTranIds + '\n\n\n';
		
		clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_ss_admin", emailSubject, emailBody);
		//nlapiSendEmail(71418,'dan.tansanu@cologix.com',emailSubject,emailBody,null,null,null,null,true);
		//nlapiSendEmail(nlapiGetUser(),'billing@cologix.com',emailSubject,emailBody,null,null,null,null);
		nlapiLogExecution('DEBUG','Daily Report','Daily Report - Next Annual Accelerator Date Updates (' + arrOSIds.length + ' service orders have been updated today)');
	}
	else{
		var emailSubject = 'Daily Report - Next Annual Accelerator Date Updates';
		var emailBody = 'No service orders have been updated today.\n\n\n';
		
		clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_ss_admin", emailSubject, emailBody);
		//nlapiSendEmail(71418,'dan.tansanu@cologix.com',emailSubject,emailBody,null,null,null,null,true);
		nlapiLogExecution('DEBUG','Daily Report','Daily Report - Next Annual Accelerator Date Updates');
	}
	
	


//---------- End Section 1 ------------------------------------------------------------------------------------------------


		nlapiLogExecution('DEBUG','User Event - nextAADate','|--------------------FINISHED---------------------|');  
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
