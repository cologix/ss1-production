nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Saved_Searches.js
//	Script Name:	CLGX_SS_Saved_Searches
//	Script Id:		customscript_clgx_ss_saved_searches
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/3/2014
//-------------------------------------------------------------------------------------------------
function scheduled_saved_searches(){
    try{
    	
    	var searchField = 'custbody_clgx_total_recurring_month';
    	var searchFieldName = 'Total Recurring Monthly';
    	
        var startScript = moment();
        var reportSubject = 'Saved Searches using the field "' + searchFieldName + '" (' + searchField + ')';
        var  reportBody = '';
		reportBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');
        reportBody += '<br><table border="1" cellpadding="5">';
        reportBody += '<tr><td>Index</td><td>Saved Search</td><td>Search ID</td><td>Record Type</td><td>Owner</td><td>Date/Time</td><td>Cumulative<br>Usage</td></tr>';

        
        var arrColumns = new Array();
        var arrFilters = new Array();
    	var searchResults = nlapiSearchRecord('SavedSearch','customsearch_clgx_saved_searches', arrFilters, arrColumns);
    	var arrSearches = new Array;
    	for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
    		var startUsageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
      	   
        	var searchResult = searchResults[i];
        	var internalid = searchResult.getValue('internalid',null,'GROUP');
        	var title = searchResult.getValue('title',null,'GROUP');
        	var rectype = searchResult.getValue('recordtype',null,'GROUP');
        	var owner = searchResult.getText('owner',null,'GROUP');
        	
        	if(internalid > 0){
        		
        		var objSearch = nlapiLoadSearch('opportunity', internalid);
            	
            	var arrColumns = objSearch.getColumns();
            	for ( var j = 0; j < arrColumns.length; j++ ) {
            		var cformula = arrColumns[j].getFormula();
            		if(cformula != null && cformula != ''){
                    	if(cformula.indexOf(searchField) != -1 && !inArray(internalid,arrSearches)){
                            arrSearches.push(internalid);
                            var index = i + 1;
                        	var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                            nlapiLogExecution('DEBUG','Saved Search: ', '| Saved Search ' + internalid + ' | Index: ' + index + '/' + searchResults.length + ' | Consumtion: ' + usageConsumtion + ' |');
                            var endExec = moment();
                	    	var invExecMinutes = (endExec.diff(startScript)/60000).toFixed(1);
                            var endUsageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                            var usageConsumtion = parseInt(endUsageConsumtion) - parseInt(startUsageConsumtion);
                            reportBody += '<tr><td>' + i + ' of ' + searchResults.length + '</td><td><a href="https://1337135.app.netsuite.com/app/common/search/search.nl?cu=T&e=T&id=' + internalid + '">' + title + '</a></td><td>' + internalid + '</td><td>' + rectype + '</td><td>' + owner + '</td><td>' + endExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + endUsageConsumtion + '</td></tr>';
	                    }
            		}
            		var cname = arrColumns[j].getName();
            		if(cname != null && cname != ''){
    					if(cname.indexOf(searchField) != -1 && !inArray(internalid,arrSearches)){
                            arrSearches.push(internalid);
                            var index = i + 1;
                        	var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                            nlapiLogExecution('DEBUG','Saved Search: ', '| Saved Search ' + internalid + ' | Index: ' + index + '/' + searchResults.length + ' | Consumtion: ' + usageConsumtion + ' |');
                            var endExec = moment();
                	    	var invExecMinutes = (endExec.diff(startScript)/60000).toFixed(1);
                            var endUsageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                            var usageConsumtion = parseInt(endUsageConsumtion) - parseInt(startUsageConsumtion);
                            reportBody += '<tr><td>' + i + ' of ' + searchResults.length + '</td><td><a href="https://1337135.app.netsuite.com/app/common/search/search.nl?cu=T&e=T&id=' + internalid + '">' + title + '</a></td><td>' + internalid + '</td><td>' + rectype + '</td><td>' + owner + '</td><td>' + endExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + endUsageConsumtion + '</td></tr>';
	                    }
            		}

            	}
            	
            	var arrFilters = objSearch.getFilters();
            	for ( var k = 0; k < arrFilters.length; k++ ) {
            		var fformula = arrFilters[k].getFormula();
            		if(fformula != null && fformula != ''){
                    	if(fformula.indexOf(searchField) != -1 && !inArray(internalid,arrSearches)){
                            arrSearches.push(internalid);
                            var index = i + 1;
                        	var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                            nlapiLogExecution('DEBUG','Saved Search: ', '| Saved Search ' + internalid + ' | Index: ' + index + '/' + searchResults.length + ' | Consumtion: ' + usageConsumtion + ' |');
                            var endExec = moment();
                	    	var invExecMinutes = (endExec.diff(startScript)/60000).toFixed(1);
                            var endUsageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                            var usageConsumtion = parseInt(endUsageConsumtion) - parseInt(startUsageConsumtion);
                            reportBody += '<tr><td>' + i + ' of ' + searchResults.length + '</td><td><a href="https://1337135.app.netsuite.com/app/common/search/search.nl?cu=T&e=T&id=' + internalid + '">' + title + '</a></td><td>' + internalid + '</td><td>' + rectype + '</td><td>' + owner + '</td><td>' + endExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + endUsageConsumtion + '</td></tr>';
	                    }
            		}
            		var fname = arrFilters[k].getName();
            		if(fname != null && fname != ''){
						if(fname.indexOf(searchField) != -1 && !inArray(internalid,arrSearches)){
	                        arrSearches.push(internalid);
	                        var index = i + 1;
	                    	var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
	                        nlapiLogExecution('DEBUG','Saved Search: ', '| Saved Search ' + internalid + ' | Index: ' + index + '/' + searchResults.length + ' | Consumtion: ' + usageConsumtion + ' |');
	                        var endExec = moment();
	            	    	var invExecMinutes = (endExec.diff(startScript)/60000).toFixed(1);
	                        var endUsageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
	                        var usageConsumtion = parseInt(endUsageConsumtion) - parseInt(startUsageConsumtion);
	                        reportBody += '<tr><td>' + i + ' of ' + searchResults.length + '</td><td><a href="https://1337135.app.netsuite.com/app/common/search/search.nl?cu=T&e=T&id=' + internalid + '">' + title + '</a></td><td>' + internalid + '</td><td>' + rectype + '</td><td>' + owner + '</td><td>' + endExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + endUsageConsumtion + '</td></tr>';
	                    }
					}
            	}
        	}  
    	}
    	
		var endScript = moment();
		reportBody += '</table><br>';
		reportBody += 'End : ' + endScript.format('M/D/YYYY h:mm:ss a') + '<br>';
		reportBody += 'Total execution minutes : ' + (endScript.diff(startScript)/60000).toFixed(1) + '<br>';
		var totalUsageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
		reportBody += 'Total usage : ' + totalUsageConsumtion;
		reportBody += '<br><br>';

		clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_ss_admin", reportSubject, reportBody);
    	//nlapiSendEmail(71418,71418,reportSubject ,reportBody,null,null,null,null,true);
    }

    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function inArray(val, arr){	
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}
