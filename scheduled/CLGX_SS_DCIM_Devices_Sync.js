nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_DCIM_Devices_Sync.js
//	Script Id:		customscript_clgx_ss_dcim_devices_sync
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_dcim_devices_sync(){
    try{
    	//nlapiLogExecution('DEBUG','Started Execution', '|-------------------------- Begin Scheduled Script --------------------------|'); 
        var context = nlapiGetContext();

		//var type = 'Opportunity';
    	//var type = 'Proposal';
    	var type = 'Service Order';
    	
// ============================ Open Session ===============================================================================================
        
    	var strPost = clgx_sp_login (username,password);
    	var objHead = clgx_sp_header (strPost.length);
		var requestURL = nlapiRequestURL(strLogURL,strPost,objHead);
		var responseXML = nlapiStringToXML(requestURL.getBody());
		var login = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
		
		if(login == 'true'){
			
			var jsessionid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SESSIONID');
			strURL += jsessionid;
			
			nlapiLogExecution('DEBUG','Session ID', ' | Session ID = ' + jsessionid + '  |');
			
			var arrColumns = new Array();
			var arrFilters = new Array();
			if(type == 'Opportunity'){
				var searchTransactionsIDs = nlapiSearchRecord('opportunity', 'customsearch_clgx_sp_opportunities_ids', arrFilters, arrColumns);	
        	}
        	else if(type == 'Proposal'){
        		var searchTransactionsIDs = nlapiSearchRecord('transaction', 'customsearch_clgx_sp_proposals_ids', arrFilters, arrColumns);	
        	}
        	else{
        		var searchTransactionsIDs = nlapiSearchRecord('transaction', 'customsearch_clgx_sp_sos_ids', arrFilters, arrColumns);	
        	}
			
			if(searchTransactionsIDs != null){
				if(searchTransactionsIDs.length > 100){
					var loopndx = 100;
				}
				else{
					var loopndx = searchTransactionsIDs.length;
				}
			}
			else{
				loopndx = 0;
			}
			
			for ( var j = 0; searchTransactionsIDs != null && j < loopndx; j++ ) {
				
	        	var transactionid = searchTransactionsIDs[j].getValue('internalid',null,'GROUP');
	        	
	        	if(type == 'Opportunity'){
	        		var recTransact = nlapiLoadRecord ('opportunity',transactionid);
	        	}
	        	else if(type == 'Proposal'){
	        		var recTransact = nlapiLoadRecord ('estimate',transactionid);
	        	}
	        	else{ //Service Order
	        		var recTransact = nlapiLoadRecord ('salesorder',transactionid);
	        	}
    			
	        	var jsonSync = [];
    			var strSync = recTransact.getFieldValue('custbody_clgx_sp_json_sync');
    			if(strSync != '' && strSync != null){
    				jsonSync = JSON.parse(strSync);
    				
    				// delete transaction lines ================================================================================================
    				var objParams = new Object();
    				objParams["tableid"] = 3521196;
    				var strPost = clgx_sp_table_delete_data (objParams,jsonSync);
    		    	var objHead = clgx_sp_header (strPost.length);
    				var requestURL = nlapiRequestURL(strURL,strPost,objHead);
    				var strBody = requestURL.getBody();
    				var responseXML = nlapiStringToXML(strBody);
    				var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
    				if(success == 'true'){
    					//nlapiLogExecution('DEBUG','DELETE ', ' | Delete transaction ID = ' + transactionid + '  |');
    					//nlapiLogExecution('DEBUG','DELETE ', ' | responseXML = ' + responseXML + '  |');
    					var transactdelete = 'YES';
    				}
    				else{
    					//nlapiLogExecution('DEBUG','DELETE ', ' | Did not delete transaction ID = ' + transactionid + '  |');
    					//nlapiLogExecution('DEBUG','DELETE ', ' | responseXML = ' + responseXML + '  |');
    					var transactdelete = 'NO';
    				}
    			}
    			else{
    				var transactdelete = 'NULL';
    			}
    			
    			
    			
    			var arrRows = new Array();
    			var arrRowsJSON = new Array();
    			
    			var arrColumns = new Array();
				var arrFilters = new Array();
				arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",transactionid));
				if(type == 'Opportunity'){
					var searchTransactions = nlapiSearchRecord('opportunity', 'customsearch_clgx_sp_opportunities', arrFilters, arrColumns);
	        	}
	        	else if(type == 'Proposal'){
	        		var searchTransactions = nlapiSearchRecord('transaction', 'customsearch_clgx_sp_proposals', arrFilters, arrColumns);
	        	}
	        	else{
	        		var searchTransactions = nlapiSearchRecord('transaction', 'customsearch_clgx_sp_sos', arrFilters, arrColumns);
	        	}
				
				for ( var i = 0; searchTransactions != null && i < searchTransactions.length; i++ ) {
					var searchTransaction = searchTransactions[i];
					var columns = searchTransaction.getAllColumns();
		        	
					var arrColumns = new Array();
		        	var arrColumnsJSON = new Array();
		        	
		        	var objColumn = new Object();
		        	objColumn["name"] = 'Company Internal ID';
		        	objColumn["value"] = parseInt(searchTransaction.getValue('internalid','customer','GROUP'));
		        	arrColumns.push(objColumn);
		        	
		        	var objColumn = new Object();
		        	objColumn["name"] = 'Transaction Internal ID';
		        	objColumn["value"] = parseInt(searchTransaction.getValue('internalid',null,'GROUP'));
		        	arrColumns.push(objColumn);
		        	arrColumnsJSON.push(objColumn);
		        	
		        	var objColumn = new Object();
		        	objColumn["name"] = 'Transaction Number';
		        	objColumn["value"] = searchTransaction.getValue('tranid',null,'GROUP');
		        	arrColumns.push(objColumn);
		        	
		        	var objColumn = new Object();
		        	objColumn["name"] = 'Transaction End Date';
		        	if(type == 'Opportunity'){
		        		objColumn["value"] = ''
		        	}
		        	else{
		        		objColumn["value"] = searchTransaction.getValue('custbody_clgx_renewal_end_date',null,'GROUP');
		        	}
		        	arrColumns.push(objColumn);
		        	
		        	var objColumn = new Object();
		        	objColumn["name"] = 'Transaction Install Date';
		        	if(type == 'Opportunity'){
		        		objColumn["value"] = ''
		        	}
		        	else{
		        		objColumn["value"] = searchTransaction.getValue('custbody_cologix_service_actl_instl_dt',null,'GROUP');
		        	}
		        	arrColumns.push(objColumn);
		        	
		        	var objColumn = new Object();
		        	objColumn["name"] = 'Transaction Sales Effective Date';
		        	if(type == 'Opportunity'){
		        		objColumn["value"] = ''
		        	}
		        	else{
		        		objColumn["value"] = searchTransaction.getValue('saleseffectivedate',null,'GROUP');
		        	}
		        	arrColumns.push(objColumn);

		        	var objColumn = new Object();
		        	objColumn["name"] = 'Transaction Status';
		        	if(type == 'Opportunity'){
		        		objColumn["value"] = searchTransaction.getText('entitystatus',null,'GROUP');
		        	}
		        	else{
		        		objColumn["value"] = searchTransaction.getText('status',null,'GROUP');
		        	}
		        	arrColumns.push(objColumn);
		        	
		        	var objColumn = new Object();
		        	objColumn["name"] = 'Transaction Type';
		        	objColumn["value"] = type;
		        	arrColumns.push(objColumn);
		        	
		        	var objColumn = new Object();
		        	objColumn["name"] = 'Transaction Lead Source';
		        	var leadsource = searchTransaction.getText('leadsource',null,'GROUP');
		        	if(leadsource != null && leadsource != '' && leadsource != '- None -'){
		        		objColumn["value"] = leadsource;
		        	}
		        	else{
		        		objColumn["value"] = '';
		        	}
		        	arrColumns.push(objColumn);
		        	
		        	var objColumn = new Object();
		        	objColumn["name"] = 'Transaction Forecast Type';
		        	var forecasttype = searchTransaction.getText('forecasttype',null,'GROUP');
		        	if(forecasttype != null && forecasttype != '' && forecasttype != '- None -'){
		        		objColumn["value"] = forecasttype;
		        	}
		        	else{
		        		objColumn["value"] = '';
		        	}
		        	arrColumns.push(objColumn);
		        	
		        	var objColumn = new Object();
		        	objColumn["name"] = 'Transaction Sale Type';
		        	var saletype = searchTransaction.getText('custbody_cologix_opp_sale_type',null,'GROUP');
		        	if(saletype != null && saletype != '' && saletype != '- None -'){
		        		objColumn["value"] = saletype;
		        	}
		        	else{
		        		objColumn["value"] = '';
		        	}
		        	arrColumns.push(objColumn);
		        	
		        	var objColumn = new Object();
		        	objColumn["name"] = 'Location Internal ID';
		        	if(type == 'Opportunity'){
		        		objColumn["value"] = parseInt(searchTransaction.getValue(columns[7]));
		        	}
		        	else{
		        		objColumn["value"] = parseInt(searchTransaction.getValue('internalid','location','GROUP'));
		        	}
		        	arrColumns.push(objColumn);
		        	arrColumnsJSON.push(objColumn);
		        	
		        	var objColumn = new Object();
		        	objColumn["name"] = 'Location Name';
		        	if(type == 'Opportunity'){
		        		objColumn["value"] = replace_chars (searchTransaction.getValue('locationnohierarchy',null,'GROUP'));
		        	}
		        	else{
		        		objColumn["value"] = replace_chars (searchTransaction.getValue('namenohierarchy','location','GROUP'));
		        	}
		        	arrColumns.push(objColumn);
		        	
		        	var objColumn = new Object();
		        	objColumn["name"] = 'Item Internal ID';
		        	objColumn["value"] = parseInt(searchTransaction.getValue('internalid','item','GROUP'));
		        	arrColumns.push(objColumn);
		        	arrColumnsJSON.push(objColumn);
		        	
		        	var objColumn = new Object();
		        	objColumn["name"] = 'Item Name';
		        	objColumn["value"] = replace_chars (searchTransaction.getValue('name','item','GROUP'));
		        	arrColumns.push(objColumn);
		        	
		        	var objColumn = new Object();
		        	objColumn["name"] = 'Item Category';
		        	objColumn["value"] = searchTransaction.getText('custitem_cologix_item_category','item','GROUP');
		        	arrColumns.push(objColumn);

		        	var objColumn = new Object();
		        	objColumn["name"] = 'Item Quantity';
		        	if(type == 'Opportunity'){
		        		objColumn["value"] = parseFloat(searchTransaction.getValue('linequantity',null,'SUM'));
		        	}
		        	else if(type == 'Proposal'){
		        		objColumn["value"] = parseFloat(searchTransaction.getValue('quantity',null,'SUM'));
		        	}
		        	else{
		        		objColumn["value"] = parseFloat(searchTransaction.getValue('custcol_clgx_qty2print',null,'SUM'));
		        	}
		        	arrColumns.push(objColumn);

		        	var objColumn = new Object();
		        	objColumn["name"] = 'Item Total';
		        	//objColumn["value"] = parseFloat(searchTransaction.getValue('lineamount',null,'SUM'));
		        	objColumn["value"] = parseFloat(searchTransaction.getValue(columns[16]));
		        	
		        	if(type == 'Opportunity'){
		        		objColumn["value"] = parseFloat(searchTransaction.getValue('lineamount',null,'SUM'));
		        	}
		        	else if(type == 'Proposal'){
		        		objColumn["value"] = parseFloat(searchTransaction.getValue('amount',null,'SUM'));
		        	}
		        	else{
		        		objColumn["value"] = parseFloat(searchTransaction.getValue('custcol_clgx_qty2print',null,'SUM')) * parseFloat(searchTransaction.getValue('rate',null,'GROUP'));
		        	}
		        	
		        	arrColumns.push(objColumn);
		        	
		        	arrRows.push(arrColumns);
		        	arrRowsJSON.push(arrColumnsJSON);
				}
				
				// create transaction lines ================================================================================================
				var objParams = new Object();
				objParams["tableid"] = 3521196;
				var strPost = clgx_sp_table_insert_data (objParams,arrRows);
		    	var objHead = clgx_sp_header (strPost.length);
				var requestURL = nlapiRequestURL(strURL,strPost,objHead);
				var strBody = requestURL.getBody();
				var responseXML = nlapiStringToXML(strBody);
				var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
				if(success == 'true'){
					var updated = 'T';
					var transactcreate = 'YES';
					//nlapiLogExecution('DEBUG','CREATE ', ' | Create transaction ID = ' + transactionid + '  |');
					//nlapiLogExecution('DEBUG','CREATE ', ' | responseXML = ' + responseXML + '  |');
				}
				else{
					var updated = 'F';
					var transactcreate = 'NO';
					//nlapiLogExecution('DEBUG','CREATE ', ' | Did not create transaction ID = ' + transactionid + '  |');
					//nlapiLogExecution('DEBUG','CREATE ', ' | responseXML = ' + responseXML + '  |');
				}
				
				
				
	        	if(type == 'Opportunity'){
	        		nlapiSubmitField('opportunity', transactionid, ['custbody_clgx_sp_json_sync','custbody_clgx_sp_sync'], [JSON.stringify(arrRowsJSON),updated]);
	        	}
	        	else if(type == 'Proposal'){
	        		nlapiSubmitField('estimate', transactionid, ['custbody_clgx_sp_json_sync','custbody_clgx_sp_sync'], [JSON.stringify(arrRowsJSON),updated]);
	        	}
	        	else{ //Service Order
	        		nlapiSubmitField('salesorder', transactionid, ['custbody_clgx_sp_json_sync','custbody_clgx_sp_sync'], [JSON.stringify(arrRowsJSON),updated]);
	        	}

	        	var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
	            var index = j + 1;
	            nlapiLogExecution('DEBUG','INDEX LOOP ', ' | Index = ' + index + ' of ' + loopndx + ' | Transaction ID = '+ transactionid + ' | Delete = '+ transactdelete + ' | Create = '+ transactcreate + ' | Usage = '+ usageConsumtion + '  |');
	
	            
	        }
	        
// ============================ Close Session ===============================================================================================
			
	    	var strPost =  clgx_sp_logout ();
			var objHead = clgx_sp_header (strPost.length);
			var requestURL = nlapiRequestURL(strURL,strPost,objHead);
			var strBody = requestURL.getBody();
			var responseXML = nlapiStringToXML(strBody);
			var logout = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
			
			if(searchTransactionsIDs != null){
				var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
			}
		}
        nlapiLogExecution('DEBUG','Started Execution', '|-------------------------- Finish Scheduled Script --------------------------|'); 

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function replace_chars (str){
	str = str.replace(/\,/g," ");
	str = str.replace(/\&/g," and ");
	str = str.replace(/\"/g,"");
	str = str.replace(/\'/g,"");
	str = str.replace(/\;/g,"");
	str = str.replace(/\:/g,"");
	str = str.replace(/\s{2,}/g, ' ');
	str = str.replace(/\t/g, ' ');
	str = str.toString().trim().replace(/(\r\n|\n|\r)/g,"");
  return str;
}
