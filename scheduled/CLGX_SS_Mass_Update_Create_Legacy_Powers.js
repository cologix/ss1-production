//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var startScript = moment();
        var emailAdminSubject = 'Create Legacy Power Circuits';
		var emailAdminBody = '';
		emailAdminBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');
		
		emailAdminBody += '<h2>Create Legacy Power Circuits</h2>';
		emailAdminBody += '<table border="1" cellpadding="5">';
        emailAdminBody += '<tr><td>Service Order</td><td>#Bills</td><td>Service</td><td>#NIMs</td><td>Power</td></tr>';
        
        
        var context = nlapiGetContext();
        var initialTime = moment();
        
        var arrColumns = new Array();
        var arrFilters = new Array();
        var searchResults = nlapiSearchRecord('salesorder', 'customsearch_clgx_mu_create_legacy_pwrs', arrFilters, arrColumns);

        for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
        	var startExecTime = moment();
        	var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);
        	
            if ( context.getRemainingUsage() <= 1000 || totalMinutes > 50 || i > 50 ){

                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    if ( status == 'QUEUED' ) {
                 	   nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                        break; 
                    }
            }

            var soid = searchResults[i].getValue('internalid',null,'GROUP');
            
            
            
			var recSO = nlapiLoadRecord('salesorder', soid);
			var sonbr = recSO.getFieldValue('tranid');
			var customer = recSO.getFieldValue('entity');
			var subsidiary = recSO.getFieldValue('subsidiary');
			var location = recSO.getFieldValue('location');

			nlapiLogExecution('DEBUG','Service Order ID', '| Service Order =  ' + sonbr + ' |');
			
			var nbrItems = recSO.getLineItemCount('item');
			for (var j = 0; j < nbrItems; j++){
				
				var itemID = recSO.getLineItemValue('item', 'item', j + 1);
				var itemName = recSO.getLineItemText('item', 'item', j + 1);
				var itemCategory = recSO.getLineItemText('item', 'custcol_cologix_invoice_item_category', j + 1);
				var itemServiceID = recSO.getLineItemValue('item', 'custcol_clgx_so_col_service_id', j + 1);
				var itemService = recSO.getLineItemText('item', 'custcol_clgx_so_col_service_id', j + 1);
				var itemBillSched = recSO.getLineItemText('item', 'billingschedule', j + 1);
				var itemFacility = parseInt(clgx_return_facilityid(location));
				var itemClass = recSO.getLineItemText('item', 'class', j + 1);
				var qty = parseInt(recSO.getLineItemValue('item', 'custcol_clgx_qty2print', j + 1));

				if (itemClass.indexOf("Recurring") > -1 && itemBillSched != null && itemBillSched != '' && itemBillSched != 'Non Recurring' && itemCategory == 'Power' && (itemServiceID != null && itemServiceID != '' && itemServiceID != -1) && itemID != 302 && itemID != 518 && itemID != 529) { // Create service for this recurring  item 

					var recService = nlapiLoadRecord('job', parseInt(itemServiceID), null, null);
					var nbrNIMs = parseInt(recService.getLineItemCount('recmachcustrecord_cologix_power_service'));
					var isinactive = recService.getFieldValue('isinactive');
					
					if(itemName.indexOf('A+B') != -1){
						var nbrBills = qty *2;
					}
					else{
						var nbrBills = qty;
					}
					nlapiLogExecution('DEBUG','Service ID', '| Service =  ' + itemService + ' | NIMS = ' + nbrNIMs + ' | BILS = ' + nbrBills + ' |');
					
					if((nbrNIMs == null || nbrNIMs == '' || nbrNIMs == 0) && isinactive != 'T'){ // no power on service

						var arrColumns = new Array();
						arrColumns.push(new nlobjSearchColumn('custitem_clgx_item_power_amps',null,null));
						arrColumns.push(new nlobjSearchColumn('custitem_clgx_item_power_volts',null,null));
						var arrFilters = new Array();
						arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",itemID));
						var searchItem = nlapiSearchRecord('item', null, arrFilters, arrColumns);
						if(searchItem != null){
							var amps = searchItem[0].getValue('custitem_clgx_item_power_amps',null,null);
							var volts = searchItem[0].getValue('custitem_clgx_item_power_volts',null,null);
						}
						else{
							var amps = '';
							var volts = '';
						}
						
						
						if(itemName.indexOf('A+B') != -1){ // create 2 power records if item name contains "A+B"
							for (var k = 0; k < qty; k++){
								// create power circuit A
								var record = nlapiCreateRecord('customrecord_clgx_power_circuit');
								record.setFieldValue('custrecord_cologix_power_service', itemServiceID);
								record.setFieldValue('custrecord_cologix_power_facility', itemFacility);
								record.setFieldValue('custrecord_cologix_power_amps', amps);
								record.setFieldValue('custrecord_cologix_power_volts', volts);
								record.setFieldValue('custrecord_power_circuit_service_order', soid);
								var idPowerA = nlapiSubmitRecord(record, false, true);
								
								var powerName = nlapiLookupField('customrecord_clgx_power_circuit', idPowerA, 'name');
								nlapiSubmitField('customrecord_clgx_power_circuit', idPowerA, 'name', clgx_set_char_at(powerName,12,'A'));
								
								emailAdminBody += '<tr><td>' + sonbr + '</td><td>' + nbrBills + '</td><td>' + itemService + '</td><td>' + nbrNIMs + '</td><td>' + powerName + ' / A / ' + idPowerA + '</td></tr>';
								nlapiLogExecution('DEBUG','Created Power ', powerName + ' / A / ' + idPowerA);
								
								// create power circuit B
								var record = nlapiCreateRecord('customrecord_clgx_power_circuit');
								record.setFieldValue('custrecord_cologix_power_service', itemServiceID);
								record.setFieldValue('custrecord_cologix_power_facility', itemFacility);
								record.setFieldValue('custrecord_cologix_power_amps', amps);
								record.setFieldValue('custrecord_cologix_power_volts', volts);
								record.setFieldValue('custrecord_power_circuit_service_order', soid);
								var idPowerB = nlapiSubmitRecord(record, false, true);
								
								emailAdminBody += '<tr><td>' + sonbr + '</td><td>' + nbrBills + '</td><td>' + itemService + '</td><td>' + nbrNIMs + '</td><td>' + powerName + ' / B / ' + idPowerB + '</td></tr>';
								nlapiSubmitField('customrecord_clgx_power_circuit', idPowerB, 'name', clgx_set_char_at(powerName,12,'B'));
								
								// update pair power on each power
								nlapiSubmitField('customrecord_clgx_power_circuit', idPowerA, 'custrecord_clgx_dcim_pair_power', idPowerB);
								nlapiSubmitField('customrecord_clgx_power_circuit', idPowerB, 'custrecord_clgx_dcim_pair_power', idPowerA);
								
								nlapiLogExecution('DEBUG','Created Power ', powerName + ' / B / ' + idPowerB);
							}
						}
						else{
							for (var k = 0; k < qty; k++){ // create 1 power record
								// create power circuit P
								var record = nlapiCreateRecord('customrecord_clgx_power_circuit');
								record.setFieldValue('custrecord_cologix_power_service', itemServiceID);
								record.setFieldValue('custrecord_cologix_power_facility', itemFacility);
								record.setFieldValue('custrecord_cologix_power_amps', amps);
								record.setFieldValue('custrecord_cologix_power_volts', volts);
								record.setFieldValue('custrecord_power_circuit_service_order', soid);
								var idPowerP = nlapiSubmitRecord(record, false, true);
								
								var powerName = nlapiLookupField('customrecord_clgx_power_circuit', idPowerP, 'name');
								nlapiSubmitField('customrecord_clgx_power_circuit', idPowerP, 'name', clgx_set_char_at(powerName,12,'P'));
								
								emailAdminBody += '<tr><td>' + sonbr + '</td><td>' + nbrBills + '</td><td>' + itemService + '</td><td>' + nbrNIMs + '</td><td>' + powerName + ' / P / ' + idPowerP + '</td></tr>';
								nlapiLogExecution('DEBUG','Created Power ', powerName + ' / P / ' + idPowerP);
								
							}
						}
					}
					else{
						if(nbrBills != nbrNIMs){
							emailAdminBody += '<tr><td>' + sonbr + '</td><td>' + nbrBills + '</td><td>' + itemService + '</td><td>' + nbrNIMs + '</td><td>nbrBills != nbrNIMs</td></tr>';
						}
						else{
							emailAdminBody += '<tr><td>' + sonbr + '</td><td>' + nbrBills + '</td><td>' + itemService + '</td><td>' + nbrNIMs + '</td><td></td></tr>';
						}
						
					}
						
				}
				

			}
			nlapiSubmitField('salesorder', soid, 'custbody_clgx_mass_update_flag', 'T');
            
            
            var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            var index = i + 1;
            nlapiLogExecution('DEBUG','Results ', ' | SO = ' + index + ' of ' + searchResults.length + ' | Usage = '+ usageConsumtion + '  |');

        }
        
        emailAdminBody += '</table>';
        
		var endScript = moment();
		emailAdminBody += '<br>';
		emailAdminBody += 'End : ' + endScript.format('M/D/YYYY h:mm:ss a') + '<br>';
		emailAdminBody += 'Total execution minutes : ' + (endScript.diff(startScript)/60000).toFixed(1) + '<br>';
		var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
		emailAdminBody += 'Total usage : ' + usageConsumtion;
		emailAdminBody += '<br><br>';
		
		clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_ss_admin", emailAdminSubject, emailAdminBody);
		/*nlapiSendEmail(432742,71418,emailAdminSubject,emailAdminBody,null,null,null,null,true);
		nlapiSendEmail(432742,1349020,emailAdminSubject,emailAdminBody,null,null,null,null,true);*/
        
        nlapiLogExecution('DEBUG','Finish Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
