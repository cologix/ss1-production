//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Mass_Update.js
//	Script Name:	CLGX_SS_Mass_Update
//	Script Id:		customscript_clgx_ss_mass_update
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_mass_update(){
    try{
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 

        var context = nlapiGetContext();
        
        var columns = new Array();
    	var filters = new Array();
    	var contacts = [];
		while (true) {
			search = nlapiSearchRecord('contact', 'customsearch_clgx_sp_sync_contacts_ids', filters, columns);
    		if (!search) {
    			break;
    		}
    		for (var i in search) {
    			var id = parseInt(search[i].getValue('internalid',null,null));
    			contacts.push(id);
    		}
    		if (search.length < 1000) {
    			break;
    		}
    		filters[0] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", id);
    	}
        
        var file = nlapiCreateFile('ns_contacts_mu.json', 'PLAINTEXT', JSON.stringify(contacts));
        file.setFolder(1909409);
		nlapiSubmitFile(file);
        
		

        /*
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();
        
        var objFile = nlapiLoadFile(2665445);
		var arrContacts = JSON.parse(objFile.getValue());
		var arrNewContacts = JSON.parse(objFile.getValue());
		
		if(arrContacts.length > 0){
			
			if(arrContacts.length > 1500){
				var loopndx = 1500;
			}
			else{
				var loopndx = arrContacts.length;
			}
			
			for ( var j = 0; arrContacts != null && j < loopndx; j++ ) {

	    		var record = nlapiCreateRecord('customrecord_clgx_queue_contacts_to_sp');
				record.setFieldValue('custrecord_clgx_contact_to_sp_contact', arrContacts[j]);
				record.setFieldValue('custrecord_clgx_contact_to_sp_action', 1);
				record.setFieldValue('custrecord_clgx_contact_to_sp_done', 'F');
				var idRec = nlapiSubmitRecord(record, false,true);
				
				arrNewContacts = _.reject(arrNewContacts, function(num){
			        return (num == arrContacts[j]);
				});

	            var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
	            var index = j + 1;
	            nlapiLogExecution('DEBUG','Results ', '| id = ' + arrContacts[j] + ' | index = ' + index + ' of ' + arrContacts.length + ' | Usage = '+ usageConsumtion + '  |');
	            
			}
			
			var file = nlapiCreateFile('ns_contacts_mu.json', 'PLAINTEXT', JSON.stringify(arrNewContacts));
			file.setFolder(1909409);
			nlapiSubmitFile(file);

			var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
			
		}

        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
		*/
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
        
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

