//-------------------------------------------------------------------------------------------------
//	Script File:		CLGX_SL_CMD_Alarms_NAC_Notif.js
//	Script Name:		CLGX_SL_CMD_Alarms_NAC_Notif
//	Script Id:		customscript_clgx_sl_cmd_alarms_nac_noti
//	Script Runs:		On Server
//	Script Type:		Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:			01/19/2018
//	URL:				/app/site/hosting/scriptlet.nl?script=1449&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_alarms_nac_notif (request, response){
    try {

    		var alarmid = request.getParameter('alarmid') || 0;
    		var user = nlapiLookupField('employee', nlapiGetUser(), 'entityid');
    		
    		var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/nacas/alarms/set_alarm_comment.cfm?id=' + alarmid + '&user=' + user);
      	var resp = requestURL.body;
      	
      	response.write( 'Thank you ' + user + ', you just notified alarm ' + alarmid + '.');
      	
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
