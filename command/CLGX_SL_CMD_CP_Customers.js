nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_CP_Customers.js
//	Script Name:	CLGX_SL_CMD_CP_Customers
//	Script Id:		customscript_clgx_sl_cmd_cp_customers
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/07/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=750&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_clgx_sl_cmd_cp_customers (request, response){
    try {

    	var start = moment().startOf('month').subtract('months', 0).format('YYYY-MM-DD');
    	
    	var request = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/customer_portal/get_logs_customers.cfm?start=' + start);
        var dataCustomers = JSON.parse( request.body );
        
        var customers = _.uniq(_.pluck(dataCustomers, 'companyid'));
        var modifiers = _.uniq(_.pluck(dataCustomers, 'modifierid'));
        var contacts = _.uniq(_.pluck(dataCustomers, 'contactid'));
        
		var arr = _.pluck(dataCustomers, 'sessions');
		sessions = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
		var arr = _.pluck(dataCustomers, 'hits');
		hits = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
		
        var dataCustomersSums = {
    		"customers": customers.length,
    		"modifiers": modifiers.length,
    		"contacts": contacts.length,
    		"sessions": sessions,
    		"hits": hits
    		
        };
        
        var request = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/customer_portal/get_logs_days.cfm?start=' + start);
        var json = JSON.parse( request.body );
        
        var years = _.uniq(_.pluck(json, 'year'));
        
        var Y = [];
		for ( var y = 0; y < years.length; y++ ) {
			
			var MONTHS = _.filter(json, function(arr){
				return (arr.year == years[y]);
			});
			var months = _.uniq(_.pluck(MONTHS, 'month'));
			
			var y_sum_s = 0;
			var y_sum_h = 0;
			var M = [];
			for ( var m = 0; m < months.length; m++ ) {
				
				var DAYS = _.filter(MONTHS, function(arr){
					return (arr.year == years[y] && arr.month == months[m]);
				});
				
				var m_sum_s = 0;
				var m_sum_h = 0;
				var D = [];
				for ( var d = 0; d < DAYS.length; d++ ) {
					D.push({
						"node": DAYS[d].day,
						"type": 'day',
						"date": months[m] + '/' + DAYS[d].day + '/' + years[y],
						"mysql": years[y] + '-' + months[m] + '-' + DAYS[d].day,
						"customers": DAYS[d].companies,
						"modifiers": DAYS[d].modifiers,
						"contacts": DAYS[d].contacts,
						"sessions": DAYS[d].sessions,
						"hits": DAYS[d].hits,
						"faicon": "calendar-o blue1",
						"leaf": true,
					});
					m_sum_s += DAYS[d].sessions;
					m_sum_h += DAYS[d].hits;
					y_sum_s += DAYS[d].sessions;
					y_sum_h += DAYS[d].hits;
				}
				M.push({
					"node":months[m],
					"type": 'month',
					"sessions": m_sum_s,
					"hits": m_sum_h,
					"faicon": "calendar blue1",
					"expanded": false,
					"leaf": false,
					"children": D
				});
			}
			Y.push({
				"node":years[y],
				"type": 'year',
				"sessions": y_sum_s,
				"hits": y_sum_h,
				"faicon": "calendar blue2",
				"expanded": false,
				"leaf": false,
				"children": M
			});
		}
        
		var tree = {
				"text": ".",
				"children": Y
		};

        
        var objFile = nlapiLoadFile(4584811);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{dataCustomers}','g'), JSON.stringify(dataCustomers));
		html = html.replace(new RegExp('{dataCustomersSums}','g'), JSON.stringify(dataCustomersSums));
		html = html.replace(new RegExp('{treeDays}','g'), JSON.stringify(tree));
		response.write( html );

    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
