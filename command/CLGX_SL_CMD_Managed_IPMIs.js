nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Managed_IPMIs.js
//	Script Name:	CLGX_SL_CMD_Managed_IPMIs
//	Script Id:		customscript_clgx_sl_cmd_managed_ipmis
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/29/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=816&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_managed_ipmis (request, response){
	try {
		
    	var file = nlapiLoadFile(5136342);
    	var tree = JSON.parse(file.getValue());
		
    	var file = nlapiLoadFile(5136644);
		html = file.getValue();
		html = html.replace(new RegExp('{tree}','g'), JSON.stringify(tree));
		response.write( html );

	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}
