nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Device.js
//	Script Name:	CLGX_SL_CMD_Device
//	Script Id:		customscript_clgx_sl_cmd_device
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/01/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=745&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_device (request, response){
	try {
		var deviceid = request.getParameter('deviceid');
		var powerid = request.getParameter('powerid');
		var html = '';
		
		if(deviceid > 0){
			
			var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/devices/get_points.cfm?deviceid=' + deviceid);
			var pointsJSON = requestURL.body;

			var objFile = nlapiLoadFile(4526570);
			html = objFile.getValue();
			html = html.replace(new RegExp('{pointsJSON}','g'), pointsJSON);
		}

		response.write( html );
	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}
