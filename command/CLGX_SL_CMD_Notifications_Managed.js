nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Notifications_Managed.js
//	Script Name:	CLGX_SL_CMD_Notifications_Managed
//	Script Id:		customscript_clgx_sl_cmd_notif_managed
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/06/2017
//	URL:			/app/site/hosting/scriptlet.nl?script=808&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_notif_managed (request, response){
	try {
		
		var facilityid = request.getParameter('facilityid');
		var customerid = nlapiLookupField('customrecord_cologix_facility', facilityid, 'custrecord_cologix_location_subsidiary');

		var file = nlapiLoadFile(11064953);
    	var tree = JSON.parse(file.getValue());
		
    	var facility = _.find(tree.children, function(arr){ return (arr.nodeid == facilityid) ; });
				
    	if(facility){
        	var tree = {
        			"text": '.',
        			"facility": facility.node,
        			"facilityid": facility.nodeid,
        			"customerid": parseInt(customerid),
        			"children": facility.children
        	};
    	} else {
        	var tree = {
        			"text": '.',
        			"facility": '',
        			"facilityid": 0,
        			"customerid": parseInt(customerid),
        			"children": []
        	};
    	}
    	
    	var file = nlapiLoadFile(5093537);
		html = file.getValue();
		html = html.replace(new RegExp('{tree}','g'), JSON.stringify(tree));
		response.write( html );
		
	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}
