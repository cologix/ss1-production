//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_LIB_Notifications.js
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		02/14/2017
//-------------------------------------------------------------------------------------------------

function get_case_subtypes (type){
	
	if(type == 'powers'){
		var types = [2];
	} else {
		var types = [2,3];
	}
	var columns = [];
    columns.push(new nlobjSearchColumn('internalid',null,null));
    columns.push(new nlobjSearchColumn('custrecord_cologix_sub_case_type',null,null).setSort(false));
	columns.push(new nlobjSearchColumn('name',null,null).setSort(false));
	var filters = [];
	filters.push(new nlobjSearchFilter('isinactive',null,'is', 'F'));
	filters.push(new nlobjSearchFilter('custrecord_cologix_sub_case_type',null,'anyof', types));
	var search = nlapiSearchRecord('customrecord_cologix_sub_case_type', null, filters, columns);
	var subtypes = [];
	for ( var i = 0; search != null && i < search.length; i++ ) {
		subtypes.push ({
			"value": parseInt(search[i].getValue('internalid', null,null)),
			"text": search[i].getText('custrecord_cologix_sub_case_type', null,null) + ' / ' + search[i].getValue('name', null,null)
		});
	}
	return subtypes;
}

function get_employees(id){
	
	var columns = [];
    var filters = [];
    filters.push(new nlobjSearchFilter("internalid", "company", "anyof",id));
    var search = nlapiSearchRecord('contact', "customsearch_clgx_cmd_mops_contacts", filters, columns);
    var ids = [];
	for ( var i = 0; search != null && i < search.length; i++ ) {
		ids.push(parseInt(search[i].getValue('internalid',null,null)));
	}
	
	var columns = [];
    columns.push(new nlobjSearchColumn('internalid',null,null));
	columns.push(new nlobjSearchColumn('entityid',null,null));
	var filters = [];
	filters.push(new nlobjSearchFilter('internalid',null,'anyof', ids));
	filters.push(new nlobjSearchFilter('isinactive',null,'is', 'F'));
	filters.push(new nlobjSearchFilter('supportrep',null,'is', 'T'));
	if(id == 2790){
		filters.push(new nlobjSearchFilter('subsidiary',null,'anyof', 5));
	} else {
		filters.push(new nlobjSearchFilter('subsidiary',null,'anyof', 6));
	}
	var search = nlapiSearchRecord('employee', null, filters, columns);
	var employees = [];
	for ( var i = 0; search != null && i < search.length; i++ ) {
		employees.push ({
			"value": parseInt(search[i].getValue('internalid', null,null)),
			"text": search[i].getValue('entityid', null,null)
		});
	}
	employees = _.sortBy(employees, function(obj){ return obj.text;});
	return employees;
}

function get_contacts(id){
	var employees = get_employees(id);
	var objFile = nlapiLoadFile(5155309);
	var groups = JSON.parse(objFile.getValue());
	var contacts = _.union(employees,groups);
	contacts = _.sortBy(contacts, function(obj){ return obj.text;});
	return contacts;
}

function get_spaces_mops (tree){
	
	var ids = [];
	var spaces_ids = [];
	
	for ( var i = 0; i < tree.spaces.length; i++ ) {
	    var columns = [];
	    columns.push(new nlobjSearchColumn('internalid', null,'GROUP'));
		columns.push(new nlobjSearchColumn('mainname', 'CUSTRECORD_SPACE_SERVICE_ORDER','GROUP').setSort(true));
		var filters = [];
		filters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
		filters.push(new nlobjSearchFilter("name",null,"contains",tree.spaces[i]));
		var search = nlapiSearchRecord('customrecord_cologix_space', null, filters, columns);
		for ( var j = 0; search != null && j < search.length; j++ ) {
			spaces_ids.push(parseInt(search[j].getValue('internalid', null,'GROUP')));
			ids.push(parseInt(search[j].getValue('mainname', 'CUSTRECORD_SPACE_SERVICE_ORDER','GROUP')));
		}
	}
	spaces_ids = _.compact(_.uniq(spaces_ids));
	
	// search if any space has standard connection xcs
	var std_connection_ids = [];
	var columns = [];
	columns.push(new nlobjSearchColumn('internalid',null,null));
	var filters = [];
	filters.push(new nlobjSearchFilter("internalid",null,"anyof",spaces_ids));
	filters.push(new nlobjSearchFilter("custrecord_clgx_std_connection",null,"is",'T'));
	var search = nlapiSearchRecord('customrecord_cologix_space', null, filters, columns);
	for ( var i = 0; search != null && i < search.length; i++ ) {
		std_connection_ids.push(parseInt(search[i].getValue('internalid',null,null)));
	}

	if(!_.isEmpty(std_connection_ids)){
		var columns = [];
		var filters = [];
		filters.push(new nlobjSearchFilter("custrecord_cologix_xcpath_space",'CUSTRECORD_COLOGIX_XCPATH_CROSSCONNECT',"anyof",std_connection_ids));
		var search = nlapiSearchRecord('customrecord_cologix_crossconnect', 'customsearch_clgx_mops_spaces_from_xcs', filters, columns);
		for ( var j = 0; search != null && j < search.length; j++ ) {
			ids.push(parseInt(search[j].getValue('mainname', 'CUSTRECORD_XCONNECT_SERVICE_ORDER','GROUP')));
		}
	}
	ids = _.compact(_.uniq(ids));
	
	if(!_.isEmpty(ids)){
		var customers = get_customers (ids);
		var contacts = _.reduce(_.pluck(customers, 'count'), function(memo, num){ return memo + num; }, 0);
		tree.customers = customers.length;
		tree.contacts = contacts;
	} else {
		var customers = [];
	}
	
	tree.children.push({
		"node": 'Impacted Customers / ' + customers.length + ' cases to be created',
		"type": 'customers',
		"faicon": 'users',
		"expanded": false,
		"leaf": false,
		"children": customers
	});

	return tree;
}

function get_powers_mops (tree){
	
	var ids = [];
	
	var columns = [];
	columns.push(new nlobjSearchColumn('mainname', 'custrecord_power_circuit_service_order','GROUP').setSort(true));
	var filters = [];
	filters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	filters.push(new nlobjSearchFilter("custrecord_clgx_power_generator",null,"anyof",tree.devices));
	var search = nlapiSearchRecord('customrecord_clgx_power_circuit', null, filters, columns);
	for ( var j = 0; search != null && j < search.length; j++ ) {
		ids.push(parseInt(search[j].getValue('mainname', 'custrecord_power_circuit_service_order','GROUP')));
	}
	var columns = [];
	columns.push(new nlobjSearchColumn('mainname', 'custrecord_power_circuit_service_order','GROUP').setSort(true));
	var filters = [];
	filters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	filters.push(new nlobjSearchFilter("custrecord_cologix_power_ups_rect",null,"anyof",tree.devices));
	var search = nlapiSearchRecord('customrecord_clgx_power_circuit', null, filters, columns);
	for ( var j = 0; search != null && j < search.length; j++ ) {
		ids.push(parseInt(search[j].getValue('mainname', 'custrecord_power_circuit_service_order','GROUP')));
	}
	var columns = [];
	columns.push(new nlobjSearchColumn('mainname', 'custrecord_power_circuit_service_order','GROUP').setSort(true));
	var filters = [];
	filters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	filters.push(new nlobjSearchFilter("custrecord_clgx_power_panel_pdpm",null,"anyof",tree.devices));
	var search = nlapiSearchRecord('customrecord_clgx_power_circuit', null, filters, columns);
	for ( var j = 0; search != null && j < search.length; j++ ) {
		ids.push(parseInt(search[j].getValue('mainname', 'custrecord_power_circuit_service_order','GROUP')));
	}
	
	var ids = _.compact(_.uniq(ids));
	
	var customers = get_customers (ids);
	var contacts = _.reduce(_.pluck(customers, 'count'), function(memo, num){ return memo + num; }, 0);
	tree.customers = customers.length;
	tree.contacts = contacts;
	
	tree.children.push({
		"node": 'Impacted Customers / ' + customers.length + ' cases to be created',
		"type": 'customers',
		"faicon": 'users',
		"expanded": false,
		"leaf": false,
		"children": customers
	});

	return tree;
} 



function get_equipments_mops (tree){
	
	var ids = [];
	if(tree.equipments.length > 0){
		var columns = [];
		columns.push(new nlobjSearchColumn('mainname', 'custrecord_clgx_ms_service_order',null));
		var filters = [];
		filters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
		filters.push(new nlobjSearchFilter("custrecord_clgx_ms_equipment",null,"anyof",tree.equipments));
		var search = nlapiSearchRecord('customrecord_clgx_managed_services', null, filters, columns);
		for ( var j = 0; search != null && j < search.length; j++ ) {
			ids.push(parseInt(search[j].getValue('mainname', 'custrecord_clgx_ms_service_order',null)));
		}
		var ids = _.compact(_.uniq(ids));
	}

	var customers = get_customers (ids);
	var contacts = _.reduce(_.pluck(customers, 'count'), function(memo, num){ return memo + num; }, 0);
	tree.customers = customers.length;
	tree.contacts = contacts;
	
	tree.children.push({
		"node": 'Impacted Customers / ' + customers.length + ' cases to be created',
		"type": 'customers',
		"faicon": 'users',
		"expanded": false,
		"leaf": false,
		"children": customers
	});

	return tree;
} 


function get_customers (ids){

	var customers = [];
	var contacts = [];
	
	if(ids.length > 0){
		var columns = new Array();
		var filters = new Array();
		filters[0] = new nlobjSearchFilter("internalid", "company", "anyof",ids);
		filters[1] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", 0);
		while (true) {
			search = nlapiSearchRecord('contact', 'customsearch_clgx_cmd_mops_contacts', filters, columns);
			if (!search) {
				break;
			}
			for (var i in search) {
				var id = parseInt(search[i].getValue('internalid',null,null));
				contacts.push ({
					"node": search[i].getValue('entityid', null,null),
					"type": 'contact',
					"customerid": parseInt(search[i].getValue('company', null,null)),
					"customer": search[i].getText('company', null,null),
					"contactid": parseInt(search[i].getValue('internalid', null,null)),
					"contact": search[i].getValue('entityid', null,null),
					"email": search[i].getValue('email', null,null),
					"notif": search[i].getValue('custentity_clgx_ops_outage_notification', null,null),
					"faicon": 'user',
					"leaf": true
				});
			}
			if (search.length < 1000) {
				break;
			}
			filters[0] = new nlobjSearchFilter("internalid", "company", "anyof",ids);
			filters[1] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", id);
		}
	}


	
	for ( var i = 0; i < ids.length; i++ ) {
		
		var selection = _.filter(contacts, function(arr){
			return (arr.customerid == ids[i] && arr.notif == 'T');
		});
		if(selection == null || selection.length == 0){
			selection = _.filter(contacts, function(arr){
				return (arr.customerid == ids[i]);
			});
		}

		selection = _.map(selection, function(obj) { return _.omit(obj, ['customerid', 'customer']); });
		var emails = _.compact(_.uniq(_.pluck(selection, 'email')));
		
		if(selection.length > 0){
			var customer = nlapiLookupField('customer', ids[i], 'entityid');
    		customers.push({
    			"node": customer,
    			"type": 'customer',
    			"customerid": ids[i],
    			"customer": customer,
    			"emails": emails,
    			"count": selection.length,
    			"faicon": 'users',
    			"expanded": false,
    			"leaf": false,
    			"children": selection
    		});
		}
	}
	customers = _.sortBy(customers, function(obj){ return obj.node;});
	
	return customers;
}

/*
function get_customers (ids){

	var columns = new Array();
	var filters = new Array();
	filters[0] = new nlobjSearchFilter("internalid", "company", "anyof",ids);
	filters[1] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", 0);
	var contacts = [];
	while (true) {
		search = nlapiSearchRecord('contact', 'customsearch_clgx_cmd_mops_contacts', filters, columns);
		if (!search) {
			break;
		}
		for (var i in search) {
			var id = parseInt(search[i].getValue('internalid',null,null));
			contacts.push ({
				"node": search[i].getValue('entityid', null,null),
				"type": 'contact',
				"customerid": parseInt(search[i].getValue('company', null,null)),
				"customer": search[i].getText('company', null,null),
				"contactid": parseInt(search[i].getValue('internalid', null,null)),
				"contact": search[i].getValue('entityid', null,null),
				"email": search[i].getValue('email', null,null),
				"notif": search[i].getValue('custentity_clgx_ops_outage_notification', null,null),
				"faicon": 'user',
				"leaf": true
			});
		}
		if (search.length < 1000) {
			break;
		}
		filters[0] = new nlobjSearchFilter("internalid", "company", "anyof",ids);
		filters[1] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", id);
	}

	var customers = [];
	for ( var i = 0; i < ids.length; i++ ) {
		
		var selection = _.filter(contacts, function(arr){
			return (arr.customerid == ids[i] && arr.notif == 'T');
		});
		if(selection == null || selection.length == 0){
			selection = _.filter(contacts, function(arr){
				return (arr.customerid == ids[i]);
			});
		}

		selection = _.map(selection, function(obj) { return _.omit(obj, ['customerid', 'customer']); });
		var emails = _.compact(_.uniq(_.pluck(selection, 'email')));
		
		if(selection.length > 0){
			var customer = nlapiLookupField('customer', ids[i], 'entityid');
    		customers.push({
    			"node": customer,
    			"type": 'customer',
    			"customerid": ids[i],
    			"customer": customer,
    			"emails": emails,
    			"count": selection.length,
    			"faicon": 'users',
    			"expanded": false,
    			"leaf": false,
    			"children": selection
    		});
		}
	}
	customers = _.sortBy(customers, function(obj){ return obj.node;});
	return customers;
}


function get_customers (ids){

    var columns = [];
    var filters = [];
    filters.push(new nlobjSearchFilter("internalid", "company", "anyof",ids));
    filters.push(new nlobjSearchFilter('custentity_clgx_ops_outage_notification',null,'is', 'T'));
    var search = nlapiSearchRecord('contact', "customsearch_clgx_cmd_mops_contacts", filters, columns);
	if(!search){
	    var columns = [];
	    var filters = [];
	    filters.push(new nlobjSearchFilter("internalid", "company", "anyof",ids));
	    var search = nlapiSearchRecord('contact', "customsearch_clgx_cmd_mops_contacts", filters, columns);
	}
	var contacts = [];
	for ( var i = 0; search != null && i < search.length; i++ ) {
		contacts.push ({
			"node": search[i].getValue('entityid', null,null),
			"type": 'customer',
			"customerid": parseInt(search[i].getValue('company', null,null)),
			"customer": search[i].getText('company', null,null),
			"contactid": parseInt(search[i].getValue('internalid', null,null)),
			"contact": search[i].getValue('entityid', null,null),
			"email": search[i].getValue('email', null,null),
			"faicon": 'user',
			"leaf": true
		});
	}
	
	var customers = [];
	for ( var i = 0; i < ids.length; i++ ) {
		var selection = _.filter(contacts, function(arr){
			return (arr.customerid == ids[i]);
		});
		selection = _.map(selection, function(obj) { return _.omit(obj, ['customerid', 'customer']); });
		var emails = _.compact(_.uniq(_.pluck(selection, 'email')));
		if(selection.length > 0){
			var customer = nlapiLookupField('customer', ids[i], 'entityid');
    		customers.push({
    			"node": customer,
    			"type": 'customer',
    			"customerid": ids[i],
    			"customer": customer,
    			"emails": emails,
    			"count": selection.length,
    			"faicon": 'users',
    			"expanded": false,
    			"leaf": false,
    			"children": selection
    		});
		}
	}
	return customers;
}
*/