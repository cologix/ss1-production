nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_FAMs_OrgChart.js
//	Script Name:	CLGX_SL_CMD_FAMs_OrgChart
//	Script Id:		customscript_clgx_sl_cmd_fams_chart
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/01/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=744&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_fams_chart (request, response){
	try {
		var famid = request.getParameter('famid');

		var objFile = nlapiLoadFile(1297970);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{dataChart}','g'), get_org_chart (famid));
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function get_org_chart (famid){

	
	// <span style="font-size:80%">Vice President</span>
	
		var strData = "['Name','Parent', 'Tooltip'],";
		
		var fields = ['internalid','name','altname','custrecord_assetdescr','custrecord_assetserialno','custrecord_assetalternateno'];
		var columns = nlapiLookupField('customrecord_ncfar_asset', famid, fields);
		var internalid = columns.internalid;
		var name = columns.name;
		var altname = columns.altname;
		var descr = columns.custrecord_assetdescr;
		var serial = columns.custrecord_assetserialno;
		var no = columns.custrecord_assetalternateno;
		
		var html = name + '<br/>' + altname;
		//html += altname;
		//var html = '<span style="font-size:90%;color:#008080;">' + name + '</span><br/>';
		//html += '<span style="font-size:80%;">' + altname + '</span><br/>';
		//html += '<span style="font-size:75%;color:#B01030;">' + serial + '</span><br/>';
		//html += '<span style="font-size:75%;color:#B01030;">' + no + '</span>';
		
		strData += "['" + html + "', null ,'" + descr + "'],";
		
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,null));
		arrColumns.push(new nlobjSearchColumn('name',null,null));
		arrColumns.push(new nlobjSearchColumn('altname',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_assetdescr',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_assetserialno',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_assetalternateno',null,null));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custrecord_assetparent",null,"anyof",internalid));
		var searchFAMs1 = nlapiSearchRecord('customrecord_ncfar_asset', null, arrFilters, arrColumns);
		
		if(searchFAMs1 != null){
		for(var i=0; searchFAMs1 != null && i<searchFAMs1.length; i++){
			
			var internalid1 = searchFAMs1[i].getValue('internalid',null,null);
			var name1 = searchFAMs1[i].getValue('name',null,null);
			var altname1 = searchFAMs1[i].getValue('altname',null,null);
			var descr1 = searchFAMs1[i].getValue('custrecord_assetdescr',null,null);
			var serial1 = searchFAMs1[i].getValue('custrecord_assetserialno',null,null);
			var no1 = searchFAMs1[i].getValue('custrecord_assetalternateno',null,null);
			
			var html1 = name1 + '<br/>';
			html1 += altname1;
			
			//var html1 = '<div style="background-color:#ff00ff">' + name1 + '<br/>';
			//html1 += altname1 + '</div>';
			
			//var html1 = '<span style="font-size:90%;color:#008080;">' + name1 + '</span><br/>';
			//html1 += '<span style="font-size:80%;">' + altname1 + '</span><br/>';
			//html1 += '<span style="font-size:75%;color:#B01030;">' + serial1 + '</span><br/>';
			//html1 += '<span style="font-size:75%;color:#B01030;">' + no1 + '</span>';
			
			strData += "['" + html1 + "','" + html + "','" + descr1 + "'],";
			
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null));
			arrColumns.push(new nlobjSearchColumn('name',null,null));
			arrColumns.push(new nlobjSearchColumn('altname',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_assetdescr',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_assetserialno',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_assetalternateno',null,null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_assetparent",null,"anyof",internalid1));
			var searchFAMs2 = nlapiSearchRecord('customrecord_ncfar_asset', null, arrFilters, arrColumns);
			
			if(searchFAMs2 != null){
			for(var j=0; searchFAMs2 != null && j<searchFAMs2.length; j++){
				
				var internalid2 = searchFAMs2[j].getValue('internalid',null,null);
				var name2 = searchFAMs2[j].getValue('name',null,null);
				var altname2 = searchFAMs2[j].getValue('altname',null,null);
				var descr2 = searchFAMs2[j].getValue('custrecord_assetdescr',null,null);
				var serial2 = searchFAMs2[j].getValue('custrecord_assetserialno',null,null);
				var no2 = searchFAMs2[j].getValue('custrecord_assetalternateno',null,null);
				
				var html2 = name2 + '<br/>';
				html2 += altname2;
				
				//var html2 = '<span style="font-size:90%;color:#008080;">' + name2 + '</span><br/>';
				//html2 += '<span style="font-size:80%;">' + altname2 + '</span><br/>';
				//html2 += '<span style="font-size:75%;color:#B01030;">' + serial2 + '</span><br/>';
				//html2 += '<span style="font-size:75%;color:#B01030;">' + no2 + '</span>';
				
				strData += "['" + html2 + "','" + html1 + "','" + descr2 + "'],";
				
				var arrColumns = new Array();
				arrColumns.push(new nlobjSearchColumn('internalid',null,null));
				arrColumns.push(new nlobjSearchColumn('name',null,null));
				arrColumns.push(new nlobjSearchColumn('altname',null,null));
				arrColumns.push(new nlobjSearchColumn('custrecord_assetdescr',null,null));
				arrColumns.push(new nlobjSearchColumn('custrecord_assetserialno',null,null));
				arrColumns.push(new nlobjSearchColumn('custrecord_assetalternateno',null,null));
				var arrFilters = new Array();
				arrFilters.push(new nlobjSearchFilter("custrecord_assetparent",null,"anyof",internalid2));
				var searchFAMs3 = nlapiSearchRecord('customrecord_ncfar_asset', null, arrFilters, arrColumns);
				
				if(searchFAMs3 != null){
				for(var k=0; searchFAMs3 != null && k<searchFAMs3.length; k++){
					
					var internalid3 = searchFAMs3[k].getValue('internalid',null,null);
					var name3 = searchFAMs3[k].getValue('name',null,null);
					var altname3 = searchFAMs3[k].getValue('altname',null,null);
					var descr3 = searchFAMs3[k].getValue('custrecord_assetdescr',null,null);
					var serial3 = searchFAMs3[k].getValue('custrecord_assetserialno',null,null);
					var no3 = searchFAMs3[k].getValue('custrecord_assetalternateno',null,null);
					
					var html3 = name3 + '<br/>';
					html3 += altname3;
					
					//var html3 = '<span style="font-size:90%;color:#008080;">' + name3 + '</span><br/>';
					//html3 += '<span style="font-size:80%;">' + altname3 + '</span><br/>';
					//html3 += '<span style="font-size:75%;color:#B01030;">' + serial3 + '</span><br/>';
					//html3 += '<span style="font-size:75%;color:#B01030;">' + no3 + '</span>';
					
					strData += "['" + html3 + "','" + html2 + "','" + descr3 + "'],";
					
					var html4 = getPowers (internalid3);
					if(html4 != ''){
						strData += "['" + html4 + "','" + html3 + "', null ],";
					}
				}
				}
				else{ // no children, probably Panel, look for Powers
					var html3 = getPowers (internalid2);
					if(html3 != ''){
						strData += "['" + html3 + "','" + html2 + "', null ],";
					}
				}
			}
			}
			else{ // no children, probably Panel, look for Powers
				var html2 = getPowers (internalid1);
				if(html2 != ''){
					strData += "['" + html2 + "','" + html1 + "', null ],";
				}
			}
		}
		}
		else{ // no children, probably Panel, look for Powers
			var html1 = getPowers (internalid);
			if(html1 != ''){
				strData += "['" + html1 + "','" + html + "', null ],";
			}
		}

		return '[' + strData + ']';
}

function getPowers (famid, manager){	

	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('name',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_power_panel_pdpm",null,"anyof",famid));
	var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', null, arrFilters, arrColumns);
	var strPowers = '';
	for(var l=0; searchPowers != null && l<searchPowers.length; l++){
		var internalid = searchPowers[l].getValue('internalid',null,null);
		var name = searchPowers[l].getValue('name',null,null);
		strPowers += '<span style="font-size:90%;"><a href="/app/common/custom/custrecordentry.nl?rectype=17&id=' + internalid + '" target="_blank">' + name + '</a></span><br/>';
	}
	return strPowers;
}