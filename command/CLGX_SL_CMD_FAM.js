nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_FAM.js
//	Script Name:	CLGX_SL_CMD_FAM
//	Script Id:		customscript_clgx_sl_cmd_fam
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/01/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=742&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_clgx_sl_cmd_fam (request, response){
	try {
		var assetid = request.getParameter('assetid');
		var email = request.getParameter('email');
		
		if(assetid != '' ){
			if(email == 'yes'){
				emailContacts (assetid);
			}
			var objFile = nlapiLoadFile(4529984);
			var html = objFile.getValue();
			html = html.replace(new RegExp('{dataCustomers}','g'),jsonData(assetid));
			html = html.replace(new RegExp('{assetid}','g'),assetid);
		}
		else{
			var html = '';
		}
		response.write( html );
	

	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}

function jsonData(assetid){
	
	var arrC = new Array();
	var arrS = new Array();
	var arrCustomers = new Array();
	var arrServices = new Array();
	var arrPowers = new Array();
	
	arrC.push(0);
	arrS.push(0);
	
    var arrFAMs = new Array();
    arrFAMs.push(assetid);
    for ( var i = 0; i < 15; i++ ) {
        var arrChildren = get_fam_children(arrFAMs);
        arrFAMs = _.union(arrFAMs, arrChildren);
    }
    
    
	// Search for Power Circuits linked to this FAM record
	var filterExpression = 	[['custrecord_cologix_power_service', 'noneof', '@NONE@'], 'and', [ [ 'custrecord_cologix_power_ups_rect', 'anyof', arrFAMs ],'or',[ 'custrecord_clgx_power_panel_pdpm', 'anyof', arrFAMs ],'or',[ 'custrecord_clgx_power_generator', 'anyof', arrFAMs ]]];
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('parent','custrecord_cologix_power_service','GROUP').setSort(false));
	searchColumns.push(new nlobjSearchColumn('custrecord_cologix_power_service',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('name',null,'GROUP'));
	var searchPowers = nlapiCreateSearch('customrecord_clgx_power_circuit', filterExpression, searchColumns);
	var resultSet = searchPowers.runSearch();
	resultSet.forEachResult(function(result) {
		
		var customerid = result.getValue('parent', 'custrecord_cologix_power_service', 'GROUP');
		var customer = result.getText('parent', 'custrecord_cologix_power_service', 'GROUP');
		var serviceid = result.getValue('custrecord_cologix_power_service', null, 'GROUP');
		var service = result.getText('custrecord_cologix_power_service', null, 'GROUP');
		var powerid = result.getValue('internalid', null, 'GROUP');
		var power = result.getValue('name', null, 'GROUP');
		/*
		if(!inArray(customerid,arrC)){
			arrC.push(customerid);
			var objCustomer = new Object();
			objCustomer["customerid"] = parseInt(customerid);
			objCustomer["customer"] = customer;
			arrCustomers.push(objCustomer);
		}
		*/
		if(!inArray(serviceid,arrS)){
			arrS.push(serviceid);
			var objService = new Object();
			objService["customerid"] = parseInt(customerid);
			objService["serviceid"] = parseInt(serviceid);
			objService["service"] = service;
			arrServices.push(objService);
		}
		
		var objPower = new Object();
		objPower["customerid"] = parseInt(customerid);
		objPower["powerid"] = parseInt(powerid);
		objPower["power"] = power;
		arrPowers.push(objPower);
		return true;
	});
	

	
	/*
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('parent','custrecord_cologix_power_service',null).setSort(false));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_service',null,null));
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('name',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_ups_rect",null,"anyof",assetid));
	var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', null, arrFilters, arrColumns);
	
	for(var i=0; searchPowers != null && i<searchPowers.length; i++){
		
		var customerid = searchPowers[i].getValue('parent', 'custrecord_cologix_power_service', null);
		var customer = searchPowers[i].getText('parent', 'custrecord_cologix_power_service', null);
		var serviceid = searchPowers[i].getValue('custrecord_cologix_power_service', null, null);
		var service = searchPowers[i].getText('custrecord_cologix_power_service', null, null);
		var powerid = searchPowers[i].getValue('internalid', null, null);
		var power = searchPowers[i].getValue('name', null, null);
		
		if(!inArray(serviceid,arrS)){
			arrS.push(serviceid);
			var objService = new Object();
			objService["customerid"] = parseInt(customerid);
			objService["serviceid"] = parseInt(serviceid);
			objService["service"] = service;
			arrServices.push(objService);
		}
		
		var objPower = new Object();
		objPower["customerid"] = parseInt(customerid);
		objPower["powerid"] = parseInt(powerid);
		objPower["power"] = power;
		arrPowers.push(objPower);
	}
*/
	
	var arrSpaces = new Array();
	// Search for Spaces linked to this FAM record
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('parent','custrecord_cologix_space_project','GROUP').setSort(false));
	searchColumns.push(new nlobjSearchColumn('custrecord_cologix_space_project',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('name',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('custrecord_cologix_space_fams',null,'anyof', arrFAMs));
	var searchSpaces = nlapiSearchRecord('customrecord_cologix_space',null,searchFilter,searchColumns);
	for ( var i = 0; searchSpaces != null && i < searchSpaces.length; i++ ) {
		var searchSpace = searchSpaces[i];
		var customerid = searchSpace.getValue('parent', 'custrecord_cologix_space_project', 'GROUP');
		var customer = searchSpace.getText('parent', 'custrecord_cologix_space_project', 'GROUP');
		var serviceid = searchSpace.getValue('custrecord_cologix_space_project', null, 'GROUP');
		var service = searchSpace.getText('custrecord_cologix_space_project', null, 'GROUP');
		var spaceid = searchSpace.getValue('internalid', null, 'GROUP');
		var space = searchSpace.getValue('name', null, 'GROUP');
		/*
		if(!inArray(customerid,arrC)){
			arrC.push(customerid);
			var objCustomer = new Object();
			objCustomer["customerid"] = parseInt(customerid);
			objCustomer["customer"] = customer;
			arrCustomers.push(objCustomer);
		}
		*/
		if(!inArray(serviceid,arrS)){
			arrS.push(serviceid);
			var objService = new Object();
			objService["customerid"] = parseInt(customerid);
			objService["serviceid"] = parseInt(serviceid);
			objService["service"] = service;
			arrServices.push(objService);
		}
		var objSpace = new Object();
		objSpace["customerid"] = parseInt(customerid);
		objSpace["spaceid"] = parseInt(spaceid);
		objSpace["space"] = space;
		arrSpaces.push(objSpace);
	}
	/*
	// Search for Services linked to this FAM record and add them to services array
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('entityid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('customer',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('custentity_clgx_service_fams',null,'anyof', arrFAMs));
	var searchServices = nlapiSearchRecord('job',null,searchFilter,searchColumns);
	for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
		var searchService = searchServices[i];
		var customerid = searchService.getValue('parent', null, 'GROUP');
		var customer = searchService.getText('parent', null, 'GROUP');
		var serviceid = searchService.getValue('internalid', null, 'GROUP');
		var service = searchService.getValue('entityid', null, 'GROUP');

		if(!inArray(serviceid,arrS)){
			arrS.push(serviceid);
			var objService = new Object();
			objService["customerid"] = parseInt(customerid);
			objService["serviceid"] = parseInt(serviceid);
			objService["service"] = service;
			arrServices.push(objService);
		}
	}
	*/
	// Search all customers of all found services
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('customer',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('internalid',null,'anyof', arrS));
	var searchCustomers = nlapiSearchRecord('job',null,searchFilter,searchColumns);
	
	for ( var i = 0; searchCustomers != null && i < searchCustomers.length; i++ ) {
		var searchCustomer = searchCustomers[i];
		var customerid = searchCustomer.getValue('customer', null, 'GROUP');
		var customer = searchCustomer.getText('customer', null, 'GROUP');
		
		if(!inArray(customerid,arrC)){
			arrC.push(customerid);
			var objCustomer = new Object();
			objCustomer["customerid"] = parseInt(customerid);
			objCustomer["customer"] = customer;
			arrCustomers.push(objCustomer);
		}
	}
	

	var arrContacts = new Array();
	// Build contacts tree
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('internalid','customer',null));
	searchColumns.push(new nlobjSearchColumn('internalid',null,null));
	searchColumns.push(new nlobjSearchColumn('entityid',null,null).setSort(false));
	searchColumns.push(new nlobjSearchColumn('custentity_cologix_esc_seq',null,null));
	searchColumns.push(new nlobjSearchColumn('custentity_clgx_ops_outage_notification',null,null));
	searchColumns.push(new nlobjSearchColumn('email',null,null));
	searchColumns.push(new nlobjSearchColumn('contactrole',null,null));
	searchColumns.push(new nlobjSearchColumn('custentity_clgx_contact_secondary_role',null,null));
	searchColumns.push(new nlobjSearchColumn('isinactive',null,null));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('company',null,'anyof', arrC));
	searchFilter.push(new nlobjSearchFilter('isinactive',null,'is', 'F'));
	var searchContacts = nlapiSearchRecord('contact',null,searchFilter,searchColumns);
	
	for ( var j = 0; searchContacts != null && j < searchContacts.length; j++ ) {

		var searchContact = searchContacts[j];
		var customerid = searchContact.getValue('internalid','customer',null);
		var contactid = searchContact.getValue('internalid',null,null);
		var contact = searchContact.getValue('entityid',null,null);
		var email = searchContact.getValue('email',null,null);
		var role1 = searchContact.getText('contactrole',null,null);
		var role2 = searchContact.getText('custentity_clgx_contact_secondary_role',null,null);
		var notify = searchContact.getValue('custentity_clgx_ops_outage_notification',null,null);
		var inactive = searchContact.getValue('isinactive','contact',null);
		
		var objContact = new Object();
		objContact["customerid"] = parseInt(customerid);
		objContact["contactid"] = parseInt(contactid);
		objContact["contact"] = contact;
		objContact["email"] = email;
		objContact["role1"] = role1;
		objContact["role2"] = role2;
		objContact["notify"] = notify;
		objContact["inactive"] = inactive;
		arrContacts.push(objContact);
	}
	
// start building the tree ------------------------------------------------------------------------------
	var objTree = new Object();
	objTree["text"] = '.';
	var arrCust = new Array();
	for ( var i = 0; i < arrCustomers.length; i++ ) {
		objCust = new Object();
		objCust["entity"] = arrCustomers[i].customer;
		objCust["entityid"] = parseInt(arrCustomers[i].customerid);
		objCust["expanded"] = false;
		objCust["iconCls"] = 'customer';
		objCust["leaf"] = false;
		
		var arrGroups = new Array();
		
		// add Contacts Nodes ------------------------------------------------
		var arrCustContacts = _.filter(arrContacts, function(arr){
	        return arr.customerid === arrCustomers[i].customerid;
		});
		objGroupCust = new Object();
		objGroupCust["entity"] = 'Contacts';
		objGroupCust["expanded"] = false;
		objGroupCust["iconCls"] = 'group';
		objGroupCust["leaf"] = false;
		
		var arrCont = new Array();
		for ( var j = 0; j < arrCustContacts.length; j++ ) {
			objCont = new Object();
			objCont["entity"] = arrCustContacts[j].contact;
			objCont["entityid"] = parseInt(arrCustContacts[j].contactid);
			objCont["email"] = arrCustContacts[j].email;
			objCont["role1"] = arrCustContacts[j].role1;
			objCont["role2"] = arrCustContacts[j].role2;
			objCont["notify"] = arrCustContacts[j].notify;
			if(arrCustContacts[j].email != '' && arrCustContacts[j].inactive != 'F'){
				if(arrCustContacts[j].notify == 'T'){
					objCont["notify"] = true;
				}
				else{
					objCont["notify"] = false;
				}
				objCont["iconCls"] = 'contact';
			}
			else{
				objCont["iconCls"] = 'contact2';
			}
			objCont["leaf"] = true;
			arrCont.push(objCont);
		}
		objGroupCust["children"] = arrCont;
		arrGroups.push(objGroupCust);

		// add Power Circuits Nodes ------------------------------------------------
		var arrCustPowers = _.filter(arrPowers, function(arr){
	        return arr.customerid === arrCustomers[i].customerid;
		});
		objGroupPowers = new Object();
		objGroupPowers["entity"] = 'Power Circuits';
		objGroupPowers["entityid"] = parseInt(arrCustomers[i].customerid);
		objGroupPowers["expanded"] = false;
		objGroupPowers["customer"] = arrCustomers[i].customer;
		objGroupPowers["iconCls"] = 'power';
		objGroupPowers["leaf"] = false;
		
		var arrPow = new Array();
		for ( var j = 0; j < arrCustPowers.length; j++ ) {
			objPow = new Object();
			objPow["entity"] = arrCustPowers[j].power;
			objPow["entityid"] = 0;
			objPow["iconCls"] = 'power';
			objPow["leaf"] = true;
			arrPow.push(objPow);
		}
		objGroupPowers["children"] = arrPow;
		arrGroups.push(objGroupPowers);
		
		// add Spaces Nodes ------------------------------------------------
		var arrCustSpaces = _.filter(arrSpaces, function(arr){
	        return arr.customerid === arrCustomers[i].customerid;
		});
		objGroupSpaces = new Object();
		objGroupSpaces["entity"] = 'Spaces';
		objGroupSpaces["expanded"] = false;
		objGroupSpaces["iconCls"] = 'space';
		objGroupSpaces["leaf"] = false;
		
		var arrSpa = new Array();
		for ( var j = 0; j < arrCustSpaces.length; j++ ) {
			objSpa = new Object();
			objSpa["entity"] = arrCustSpaces[j].space;
			objSpa["entityid"] = parseInt(arrCustSpaces[j].spaceid);
			objSpa["iconCls"] = 'space';
			objSpa["leaf"] = true;
			arrSpa.push(objSpa);
		}
		objGroupSpaces["children"] = arrSpa;
		arrGroups.push(objGroupSpaces);
		
		// add Services Nodes ------------------------------------------------
		var arrCustServices = _.filter(arrServices, function(arr){
	        return arr.customerid === arrCustomers[i].customerid;
		});
		objGroupServices = new Object();
		objGroupServices["entity"] = 'Services';
		objGroupServices["expanded"] = false;
		objGroupServices["iconCls"] = 'service';
		objGroupServices["leaf"] = false;
		
		var arrServ = new Array();
		for ( var j = 0; j < arrCustServices.length; j++ ) {
			objServ = new Object();
			objServ["entity"] = arrCustServices[j].service;
			objServ["entityid"] = parseInt(arrCustServices[j].serviceid);
			objServ["iconCls"] = 'service';
			objServ["leaf"] = true;
			arrServ.push(objServ);
		}
		objGroupServices["children"] = arrServ;
		arrGroups.push(objGroupServices);
		
		objCust["children"] = arrGroups;
		arrCust.push(objCust);
	}
	objTree["children"] = arrCust;
	return JSON.stringify(objTree);
	//var arrData = new Array;
	//arrData.push(JSON.stringify(arrCustomers));
    //return arrData;
}

//check if value is in the array
function inArray(val, arr){	
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}

function get_fam_children (arrFAMs){
	var arrChildren = new Array();
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('internalid',null,null));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('custrecord_assetparent',null,'anyof', arrFAMs));
	var searchChildren = nlapiSearchRecord('customrecord_ncfar_asset',null,searchFilter,searchColumns);
	for ( var i = 0; searchChildren != null && i < searchChildren.length; i++ ) {
		arrChildren.push(parseInt(searchChildren[i].getValue('internalid', null, null)));
	}
	return arrChildren;
}