//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Managed_Services.js
//	Script Name:	CLGX_SL_CMD_Managed_Services
//	Script Id:		customscript_clgx_sl_cmd_managed_services
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/29/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=1581&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_managed_services (request, response){
	try {
		
		var objFile = nlapiLoadFile(11063851);
    	var managed = JSON.parse(objFile.getValue());
    	
    	var type = request.getParameter('type') || 0;
    	
		var selection = _.filter(managed, function(arr){
			return (arr.typeid == type);
		});
    	
    	var file = nlapiLoadFile(11588006);
		html = file.getValue();
		html = html.replace(new RegExp('{jsonGrid}','g'), JSON.stringify(selection));
		html = html.replace(new RegExp('{type}','g'), selection[0].type);
		response.write( html );
		
	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}
