//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Notifications_Equipments.js
//	Script Name:	CLGX_SL_CMD_Notifications_Equipments
//	Script Id:		customscript_clgx_sl_cmd_notif_equipments
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/06/2017
//	URL:			/app/site/hosting/scriptlet.nl?script=776&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_notif_equipments (request, response){
	try {
		
		var facilityid = request.getParameter('facilityid');
		var facility = nlapiLookupField('customrecord_cologix_facility', facilityid, 'name');
		var customerid = nlapiLookupField('customrecord_cologix_facility', facilityid, 'custrecord_cologix_location_subsidiary');
		/*
		var equipments = [];
		var columns = new Array();
		var filters = new Array();
        filters.push(new nlobjSearchFilter('custrecord_cologix_eq_service_facility',null,'anyof', facilityid));
    	var searchEquipments = nlapiLoadSearch('customrecord_cologix_equipment', 'customsearch_clgx_cmd_notif_equipments_a');
		searchEquipments.addColumns(columns);
    	searchEquipments.addFilters(filters);
		var resultSet = searchEquipments.runSearch();
		resultSet.forEachResult(function(searchResult) {
			var columns = searchResult.getAllColumns();
			equipments.push({
				"node_id"            : parseInt(searchResult.getValue('internalid',null,null)),
				"node"               : searchResult.getText('custrecord_clgx_equipment_name',null,null),
				"node_type"          : 'equipment',
				"function_id"        : parseInt(searchResult.getValue('custrecord_clgx_equipment_function_type',null,null)),
				"function"           : searchResult.getText('custrecord_clgx_equipment_function_type',null,null),
				"ip"                 : searchResult.getText('custrecord_clgx_equipment_ip',null,null),
				"status"             : searchResult.getText('custrecord_cologix_equip_status',null,null),
				"type"               : searchResult.getText('custrecord_cologix_equipment_type',null,null),
				"faicon"             : 'gear',
				"checked"            : false,
				"leaf"               : true,
			});
			return true;
		});
		*/
		
		var equipments = [];
		var columns = new Array();
		var filters = new Array();
        filters.push(new nlobjSearchFilter('custrecord_cologix_eq_service_facility','custrecord_clgx_ms_equipment','anyof', facilityid));
    	var searchEquipments = nlapiLoadSearch('customrecord_clgx_managed_services', 'customsearch_clgx_cmd_notif_equipments');
		searchEquipments.addColumns(columns);
    	searchEquipments.addFilters(filters);
		var resultSet = searchEquipments.runSearch();
		resultSet.forEachResult(function(searchResult) {
			var columns = searchResult.getAllColumns();
			equipments.push({
				"node_id"            : parseInt(searchResult.getValue('internalid','custrecord_clgx_ms_equipment',null)),
				"node"               : searchResult.getValue('custrecord_clgx_equipment_name','custrecord_clgx_ms_equipment',null),
				"node_type"          : 'equipment',
				"function_id"        : parseInt(searchResult.getValue('custrecord_clgx_equipment_function_type','custrecord_clgx_ms_equipment',null)),
				"function"           : searchResult.getText('custrecord_clgx_equipment_function_type','custrecord_clgx_ms_equipment',null),
				"ip"                 : searchResult.getText('custrecord_clgx_equipment_ip','custrecord_clgx_ms_equipment',null),
				"status"             : searchResult.getText('custrecord_cologix_equip_status','custrecord_clgx_ms_equipment',null),
				"type"               : searchResult.getText('custrecord_cologix_equipment_type','custrecord_clgx_ms_equipment',null),
				"faicon"             : 'gear',
				"checked"            : false,
				"leaf"               : true,
			});
			return true;
		});
		
		
		var tree = {
    			"text": ".",
    			"facility": facility,
    			"facilityid": facilityid,
    			"customerid": parseInt(customerid),
    			"children": equipments
    	};
    	
    	var file = nlapiLoadFile(12799032);
		html = file.getValue();
		html = html.replace(new RegExp('{tree}','g'), JSON.stringify(tree));
		response.write( html );
		
	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}
