//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SP_CMD.js
//	Script Name:	CLGX_SP_CMD
//	Script Id:		customscript_clgx_sp_cmd
//	Script Runs:	On Server
//	Script Type:	Portlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		8/18/2014
//-------------------------------------------------------------------------------------------------

function portlet_cmd (portlet, column){
	portlet.setTitle('Cologix Command');
	var html = '<iframe name="alarms" id="alarms" src="https://10.250.24.11/opendataEE/dashboard/ops/alarms/imageNav.action?treeId=8" height="500px" width="800px" frameborder="0" scrolling="no"></iframe>';
	portlet.setHtml(html);
}
