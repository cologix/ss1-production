//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Services_Customers.js
//	Script Name:	CLGX_SL_CMD_Services_Customers
//	Script Id:		customscript_clgx_sl_cc_serv_cust
//	Script Runs:	On Server
//	Script Type:	Suitelet
// 	Link			/app/site/hosting/scriptlet.nl?script=723&deploy=1
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/08/2016
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_serv_cust (request, response){
	
	try {
		
		var refresh = request.getParameter('refresh') || 0;
		if(refresh){
			var jsontree = get_json_tree_customers ();
		} else {
			var objFile = nlapiLoadFile(4371811);
			var jsontree = objFile.getValue();
		}
		var objFile = nlapiLoadFile(4436853);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{tree}','g'), jsontree);
		response.write(html);
		
	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}

function get_json_tree_customers (){

	var powers = [];
	var search = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_cc_services_cust_pwrs');
	var result = search.runSearch();
	result.forEachResult(function(row) {
		powers.push({
				"customerid": parseInt(row.getValue('mainname', 'CUSTRECORD_POWER_CIRCUIT_SERVICE_ORDER', 'GROUP')) || 0,
				"locationid": parseInt(row.getValue('location', 'CUSTRECORD_POWER_CIRCUIT_SERVICE_ORDER', 'GROUP')) || 0,
				"count": parseInt(row.getValue('internalid', null, 'COUNT')) || 0
		});
		return true;
	});
	
	var spaces = [];
	var search = nlapiLoadSearch('customrecord_cologix_space', 'customsearch_clgx_cc_services_cust_space');
	var result = search.runSearch();
	result.forEachResult(function(row) {
		spaces.push({
				"customerid": parseInt(row.getValue('mainname', 'CUSTRECORD_SPACE_SERVICE_ORDER', 'GROUP')) || 0,
				"locationid": parseInt(row.getValue('location', 'CUSTRECORD_SPACE_SERVICE_ORDER', 'GROUP')) || 0,
				"count": parseInt(row.getValue('internalid', null, 'COUNT')) || 0
		});
		return true;
	});
	
	var net = [];
	var search = nlapiLoadSearch('customrecord_cologix_crossconnect', 'customsearch_clgx_cc_services_cust_net');
	var result = search.runSearch();
	result.forEachResult(function(row) {
		net.push({
				"customerid": parseInt(row.getValue('mainname', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', 'GROUP')) || 0,
				"locationid": parseInt(row.getValue('location', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', 'GROUP')) || 0,
				"count": parseInt(row.getValue('internalid', null, 'COUNT')) || 0
		});
		return true;
	});
	
	var xcs = [];
	var search = nlapiLoadSearch('customrecord_cologix_crossconnect', 'customsearch_clgx_cc_services_cust_xcs');
	var result = search.runSearch();
	result.forEachResult(function(row) {
		xcs.push({
				"customerid": parseInt(row.getValue('mainname', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', 'GROUP')) || 0,
				"locationid": parseInt(row.getValue('location', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', 'GROUP')) || 0,
				"count": parseInt(row.getValue('internalid', null, 'COUNT')) || 0
		});
		return true;
	});
	
	var vxcs = [];
	var search = nlapiLoadSearch('customrecord_cologix_vxc', 'customsearch_clgx_cc_services_cust_vxcs');
	var result = search.runSearch();
	result.forEachResult(function(row) {
		vxcs.push({
				"customerid": parseInt(row.getValue('mainname', 'CUSTRECORD_COLOGIX_SERVICE_ORDER', 'GROUP')) || 0,
				"locationid": parseInt(row.getValue('location', 'CUSTRECORD_COLOGIX_SERVICE_ORDER', 'GROUP')) || 0,
				"count": parseInt(row.getValue('internalid', null, 'COUNT')) || 0
		});
		return true;
	});
	
	var locations = [];
	var search = nlapiLoadSearch('transaction', 'customsearch_clgx_cc_services_locations');
	var result = search.runSearch();
	result.forEachResult(function(row) {
		
		var columns = row.getAllColumns();
		var customerid = parseInt(row.getValue('entity', null, 'GROUP')) || 0;
		var locationid = parseInt(row.getValue('location', null, 'GROUP')) || 0;
		
		var inv_space = 0;
		var obj = _.find(spaces, function(arr){ return (arr.customerid == customerid && arr.locationid == locationid) ; });
		if(obj){
			inv_space = obj.count;
		}
		
		var inv_power = 0;
		var obj = _.find(powers, function(arr){ return (arr.customerid == customerid && arr.locationid == locationid) ; });
		if(obj){
			inv_power = obj.count;
		}
		
		var inv_net = 0;
		var obj = _.find(net, function(arr){ return (arr.customerid == customerid && arr.locationid == locationid) ; });
		if(obj){
			inv_net = obj.count;
		}
		
		var inv_xcs = 0;
		var obj = _.find(xcs, function(arr){ return (arr.customerid == customerid && arr.locationid == locationid) ; });
		if(obj){
			inv_xcs = obj.count;
		}
		
		var inv_vxcs = 0;
		var obj = _.find(vxcs, function(arr){ return (arr.customerid == customerid && arr.locationid == locationid) ; });
		if(obj){
			inv_vxcs = obj.count;
		}
		
		locations.push({
				"customerid": customerid,
				"customer": row.getText('entity', null, 'GROUP') || '',
				"nodeid": locationid,
				"node": row.getValue('locationnohierarchy', null, 'GROUP') || '',
				"nodetype": "location",
				"sos_count": parseInt(row.getValue('internalid', null, 'COUNT')) || 0,
				"sos_space": parseInt(row.getValue(columns[4])) || 0,
				"inv_space": inv_space,
				"sos_power": parseInt(row.getValue(columns[5])) || 0,
				"inv_power": inv_power,
				"sos_net": parseInt(row.getValue(columns[6])) || 0,
				"inv_net": inv_net,
				"sos_xcs": parseInt(row.getValue(columns[7])) || 0,
				"inv_xcs": inv_xcs,
				"sos_vxcs": parseInt(row.getValue(columns[8])) || 0,
				"inv_vxcs": inv_vxcs,
				"faicon": "building-o",
				"leaf": true
		});
		return true;
	});
	
	var customers = [];
	var search = nlapiLoadSearch('transaction', 'customsearch_clgx_cc_services_customers');
	var result = search.runSearch();
	result.forEachResult(function(row) {
		
		var columns = row.getAllColumns();
		var customerid = parseInt(row.getValue('entity', null, 'GROUP')) || 0;
		
		var inv_space = 0;
		var arr = _.filter(spaces, function(arr){
			return (arr.customerid == customerid);
		});
		if(arr){
			inv_space = _.sumBy(arr, 'count');
		}
		
		var inv_power = 0;
		var arr = _.filter(powers, function(arr){
			return (arr.customerid == customerid);
		});
		if(arr){
			inv_power = _.sumBy(arr, 'count');
		}
		
		var inv_net = 0;
		var arr = _.filter(net, function(arr){
			return (arr.customerid == customerid);
		});
		if(arr){
			inv_net = _.sumBy(arr, 'count');
		}
		
		var inv_xcs = 0;
		var arr = _.filter(xcs, function(arr){
			return (arr.customerid == customerid);
		});
		if(arr){
			inv_xcs = _.sumBy(arr, 'count');
		}
		
		var inv_vxcs = 0;
		var arr = _.filter(vxcs, function(arr){
			return (arr.customerid == customerid);
		});
		if(arr){
			inv_vxcs = _.sumBy(arr, 'count');
		}

		var arrCL = _.filter(locations, function(arr){
			return (arr.customerid == customerid);
		});
		var arrCL = _.map(arrCL, function(obj) { return _.omit(obj, ['customerid', 'customer']); });
		
		customers.push({
				"nodeid": customerid,
				"node": row.getText('entity', null, 'GROUP') || '',
				"nodetype": "customer",
				"sos_count": parseInt(row.getValue('internalid', null, 'COUNT')) || 0,
				"sos_space": parseInt(row.getValue(columns[2])) || 0,
				"inv_space": inv_space,
				"sos_power": parseInt(row.getValue(columns[3])) || 0,
				"inv_power": inv_power,
				"sos_net": parseInt(row.getValue(columns[4])) || 0,
				"inv_net": inv_net,
				"sos_xcs": parseInt(row.getValue(columns[5])) || 0,
				"inv_xcs": inv_xcs,
				"sos_vxcs": parseInt(row.getValue(columns[6])) || 0,
				"inv_vxcs": inv_vxcs,
				"faicon": "user-secret",
				"expanded": false,
				"leaf": false,
				"children": arrCL
		});
		return true;
	});
	
	var tree = {
			"text": ".",
			"children": customers
	};
	
    return JSON.stringify(tree);
}

