nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_RPT_kW_History.js
//	Script Name:	CLGX_SL_CMD_RPT_kW_History
//	Script Id:		customscript_clgx_sl_cmd_rpt_kw_hist
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		02/27/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=739&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_rpt_kw_hist (request, response){
    try {

    	var customerid = request.getParameter('customerid');
    	var facilityid = request.getParameter('facilityid');
    	var contract = request.getParameter('contract');
    	
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_day_date',null,null).setSort(true));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_day_kw',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_peak_day_file',null,null));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_day_customer",null,"anyof",customerid));
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_peak_day_facilty",null,"anyof",facilityid));
		var searchKWs = nlapiSearchRecord('customrecord_clgx_dcim_customer_peak_day', null, arrFilters, arrColumns);
		
		var nbrRecords = searchKWs.length;
		if(nbrRecords > 90){
			nbrRecords = 90;
		}
		
		var arrKW = new Array();
		for ( var i = 0; searchKWs != null && i < nbrRecords; i++ ) {
			
			var searchKW = searchKWs[i];
			var objKW = new Object();
			objKW["date"] = searchKW.getValue('custrecord_clgx_dcim_peak_day_date',null,null);
			objKW["peak"] = parseFloat(searchKW.getValue('custrecord_clgx_dcim_peak_day_kw',null,null));
			objKW["contract"] = parseFloat(contract);
			var file = parseInt(searchKW.getValue('custrecord_clgx_dcim_peak_day_file',null,null));
			if(file > 0){
				objKW["file"] = file;
				var objFile = nlapiLoadFile(file);
				var fileurl = objFile.getURL();
				objKW["fileurl"] = fileurl;
			}
			else{
				objKW["file"] = 0;
				objKW["fileurl"] = '#';
			}	
			arrKW.push(objKW);
		}

    	var objFile = nlapiLoadFile(4510532);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{jsonGrid}','g'), JSON.stringify(arrKW));
		response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
