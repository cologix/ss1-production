//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Topology.js
//	Script Name:	CLGX_SL_CMD_Topology
//	Script Id:		customscript_clgx_sl_cmd_topology
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/06/2017
//	URL:			/app/site/hosting/scriptlet.nl?script=776&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_topology (request, response){
	try {
		
		var facilityid = request.getParameter('facilityid');
		
		var objFile = nlapiLoadFile(4882693);
    	var tree = JSON.parse(objFile.getValue());
		
    	var facility = _.find(tree.children, function(arr){ return (arr.facilityid == facilityid) ; });
				
    	var tree = {
    			"text": ".",
    			"facility": facility.node,
    			"children": facility.children
    	};
    	
    	var objFile = nlapiLoadFile(4883197);
		html = objFile.getValue();
		html = html.replace(new RegExp('{tree}','g'), JSON.stringify(tree));

		response.write( html );
		
	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}
