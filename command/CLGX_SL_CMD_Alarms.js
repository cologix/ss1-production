//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Alarms.js
//	Script Name:	CLGX_SL_CMD_Alarms
//	Script Id:		customscript_clgx_sl_cmd_alarms
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/18/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=306&deploy=1  
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_alarms (request, response){
    try {

    	var userid = nlapiGetUser();
        var request = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/alarms/get_alarms.cfm');
        var alarms = JSON.parse( request.body );
        
        var arr = new Array();
        for ( var i = 0; arrAlarms != null && i < arrAlarms.length; i++ ) {
        	arr.push({
                "deviceid":parseInt(arrAlarms[i].DEVICE_ID),
                "devicename":arrAlarms[i].DEVICE_NAME,
                "devicecode":arrAlarms[i].DEVICE_CODE,

                "pointid":parseInt(arrAlarms[i].POINT_ID),
                "pointname":arrAlarms[i].POINT_NAME,
                "pointcode":arrAlarms[i].POINT_CODE,

                "eventid":parseInt(arrAlarms[i].EVENT_ID),
                "eventname":arrAlarms[i].EVENT_NAME,
                "eventcode":arrAlarms[i].EVENT_CODE,
                "eventresp":arrAlarms[i].RESPONSE,

                "date":arrAlarms[i].DATE,
                "severity":parseInt(arrAlarms[i].SEVERITY),

                "famid":Number(arrAlarms[i].EXTERNAL_ID.substring(3)),
                "fam":arrAlarms[i].EXTERNAL_ID,
                "userid":userid
        	});
        }
        response.write(JSON.stringify(arr));
    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
