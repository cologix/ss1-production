nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Alarms_NAC.js
//	Script Name:	CLGX_SL_CMD_Alarms_NAC
//	Script Id:		customscript_clgx_sl_cmd_alarms_nac
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/26/2017
//	URL:			/app/site/hosting/scriptlet.nl?script=801&deploy=1  
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_alarms_nac (request, response){
    try {
    	var fileObject = nlapiLoadFile(14250767);
        response.write(fileObject.getValue());
    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
