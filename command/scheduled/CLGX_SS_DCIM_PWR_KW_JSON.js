nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_DCIM_PWR_KW_JSON.js
//	Script Name:	CLGX_SS_DCIM_PWR_KW_JSON
//	Script Id:		customscript_clgx_ss_dcim_pwr_kw_json
//	Script Type:	Scheduled Script
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Released:		6/04/2020
//-------------------------------------------------------------------------------------------------

function scheduled_dcim_pwr_kw() {

    try {
        nlapiLogExecution('DEBUG', 'Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');
        var arrColumns = new Array();
        var arrFilters = new Array();
        var arrPowers =  new Array();
        while (true) {

            var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pwrs_kw_cust_pnl', arrFilters, arrColumns);

            if (!searchPowers) {
                break;
            }
            for (var i in searchPowers) {

                var searchPower = searchPowers[i];
                var columns = searchPower.getAllColumns();
                var objPower = new Object();
                objPower["facilityid"] = parseInt(searchPowers[i].getValue(columns[0]));
                objPower["facility"] = searchPower.getText(columns[0]);
                objPower["panelid"] = parseInt(searchPower.getValue(columns[1]));
                objPower["panel"] = searchPower.getText(columns[1]);
                objPower["powerid"] = parseInt(searchPower.getValue(columns[2]));
                var internalid = parseInt(searchPower.getValue(columns[2]));
                objPower["power"] = searchPower.getValue(columns[3]);
                objPower["pairid"] = parseInt(searchPower.getValue(columns[4]));
                objPower["pair"] = searchPower.getText(columns[4]);
                objPower["spaceid"] = parseInt(searchPower.getValue(columns[5]));
                objPower["space"] = searchPower.getText(columns[5]);
                objPower["serviceid"] = parseInt(searchPower.getValue(columns[6]));
                objPower["service"] = _.last((searchPower.getText(columns[6])).split(" : "));
                objPower["soid"] = parseInt(searchPower.getValue(columns[7]));
                objPower["so"] = (searchPower.getText(columns[7])).replace("Service Order #", "");
                objPower["volts"] = searchPower.getText(columns[8]);
                objPower["contract"] = searchPower.getText(columns[9]);
                objPower["module"] = parseInt(searchPower.getValue(columns[10]));
                objPower["breaker"] = parseInt(searchPower.getValue(columns[11]));
                var kwavg = parseFloat(searchPower.getValue(columns[12]));
                objPower["customerid"] = parseInt(searchPowers[i].getValue(columns[13]));
                if (kwavg > 0) {
                    objPower["kwavg"] = kwavg;
                } else {
                    objPower["kwavg"] = 0;
                }

                //var objPwrUsg = _.find(arrAllPowers, function(arr){ return (arr.powerid == parseInt(searchPower.getValue(columns[2]))); });
                /* var objPwrUsg = _.find(arrAllPowers, function (arr) {
                     return (arr.powerid == parseInt(searchPower.getValue(columns[2])));
                 });
                 if (objPwrUsg != null) {
                     objPower["amps"] = round(objPwrUsg.amps);
                     objPower["ampspair"] = round(objPwrUsg.ampspair);
                     objPower["absum"] = round(objPwrUsg.absum);
                     objPower["usage"] = round(objPwrUsg.usage);
                 } else {*/
                objPower["amps"] = 0;
                objPower["ampspair"] = 0;
                objPower["absum"] = 0;
                objPower["usage"] = 0;
                // }
                arrPowers.push(objPower);
                if (searchPowers.length < 1000) {
                    break;
                }
                arrFilters.push(new nlobjSearchFilter("internalIdNumber", null, "greaterthan", internalid));
            }
        }
        var jsonfinal = JSON.stringify(arrPowers);
        var fileName = nlapiCreateFile('pw_kw.json', 'PLAINTEXT', jsonfinal);
        fileName.setFolder(3824960);
        var fileid=nlapiSubmitFile(fileName);
        var x=0;
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}