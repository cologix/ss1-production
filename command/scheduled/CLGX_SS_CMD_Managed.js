nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CMD_Managed.js
//	Script Name:	CLGX_SS_CMD_Managed
//	Script Id:		customscript_clgx_ss_cmd_managed
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		05/18/2018
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_cmd_managed(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        var context = nlapiGetContext();
    	var environment = context.getEnvironment();
        
    	if(environment == 'PRODUCTION'){

    		// =================================================== All managed =================================================== 
    		    	var managed = [];
    		    	var filters = [];
    		    	while (true) {
    		    		search = nlapiSearchRecord('customrecord_clgx_managed_services', 'customsearch_clgx_cmd_managed',filters);
    		    		if (!search) { break; }
    		    		for (var i in search) {
    		    			var id = parseInt(search[i].getValue('internalid',null,null));
    		    			
    		    			var name = (search[i].getText('location', 'custrecord_clgx_ms_service_order', null)).split(":") || '';
    		        		var location = (name[name.length-1]).trim();
    		    			
    		        		var name = (search[i].getText('custrecord_clgx_ms_service_order', null, null)).split("#") || '';
    		        		var so = (name[name.length-1]).trim();
    		
    		        		var name = (search[i].getText('custrecord_clgx_ms_service', null, null)).split(":") || '';
    		        		var service = (name[name.length-1]).trim();
    		
    		        		managed.push({
    		    				"nodeid": parseInt(search[i].getValue('internalid',null,null)) || 0,
    		    				"node": search[i].getValue('custrecord_clgx_ms_device_name',null,null) || '',
    		    				"id": search[i].getValue('name',null,null) || '',
    		    				"nodetype": 'managed',
    		    				"categoryid": 0,
    		    				
    		    				"customerid": parseInt(search[i].getValue('mainname', 'custrecord_clgx_ms_service_order', null)) || 0,
    		    				"customer": search[i].getText('mainname', 'custrecord_clgx_ms_service_order', null) || '',
    		    				
    		    				"locationid": parseInt(search[i].getValue('location', 'custrecord_clgx_ms_service_order', null)) || 0,
    		    				"location": location,
    		    				
    		    				"facilityid": parseInt(search[i].getValue('custrecord_clgx_ms_facility', null, null)) || 0,
    		    				"facility": search[i].getText('custrecord_clgx_ms_facility', null, null) || '',
    		
    		    				"soid": parseInt(search[i].getValue('custrecord_clgx_ms_service_order', null, null)) || 0,
    		        			"so": so,
    		        			
    		        			"serviceid": parseInt(search[i].getValue('custrecord_clgx_ms_service', null, null)) || 0,
    		        			"service": service,
    		        			
    		        			"typeid": parseInt(search[i].getValue('custrecord_clgx_managed_services_type', null, null)) || 0,
    		    				"type": search[i].getText('custrecord_clgx_managed_services_type', null, null) || '',
    		    				
    		    				"equipmentid": parseInt(search[i].getValue('custrecord_clgx_ms_equipment', null, null)) || 0,
    		    				"equipment": search[i].getText('custrecord_clgx_ms_equipment', null, null) || '',
    		    				
    		    				"ipaddress": search[i].getValue('custrecord_clgx_ip_address', null, null) || '',
    		
    		    				"itemid": 0,
    		    				"item": '',
    		        			"faicon": "gear",
    		        			"leaf": true
    		        		});
    		    		}
    		    		if (search.length < 1000) { break; }
    		    		filters[0] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", id);
    		    	}
    	    	
    	    		var arr = [];
    		    	var ids = _.uniq(_.pluck(managed, 'serviceid'));
    		    	for ( var i = 0; i < ids.length; i++ ) {
    		    		var selection = _.filter(managed, function(arr){
    						return (arr.serviceid == ids[i]);
    					});
    		    		arr.push({
    						"customerid": selection[0].customerid,
    						"locationid": selection[0].locationid,
    						"location": selection[0].location,
    						"soid": selection[0].soid,
    						"serviceid": selection[0].serviceid,
    						"count": selection.length
    		    		});
    		    	}
    		    	var file = nlapiCreateFile('CLGX_JSON_CMD_Managed_Counts.json', 'PLAINTEXT', JSON.stringify(arr));
    		    	file.setFolder(3824960);
    		    	nlapiSubmitFile(file);
    	    	
    		    	
    	// =================================================== Services with managed =================================================== 
    		    	    	
    	    	    	var services = [];
    	    	    	var filters = [];
    	    	    	while (true) {
    	    	    		search = nlapiSearchRecord('transaction', 'customsearch_clgx_cmd_managed_jobs',filters);
    	    	    		if (!search) { break; }
    	    	    		for (var i in search) {
    	    	    			var id = parseInt(search[i].getValue('custcol_clgx_so_col_service_id',null,'GROUP'));
    	    	    			var columns = search[i].getAllColumns();
    	    	    			services.push({
    	    	    				"serviceid": parseInt(search[i].getValue('custcol_clgx_so_col_service_id', null, 'GROUP')) || 0,
    	    	    				"soid": parseInt(search[i].getValue('internalid',null,'GROUP')) || 0,
    	    	    				"categoryid": parseInt(search[i].getValue('custcol_cologix_invoice_item_category', null, 'GROUP')) || 0,
    	    	    				"itemid": parseInt(search[i].getValue('item', null, 'GROUP')) || 0,
    	    	    				"item": search[i].getText('item', null, 'GROUP') || ''
    	    	        		});
    	    	    		}
    	    	    		if (search.length < 1000) { break; }
    	    	    		filters[0] = new nlobjSearchFilter("internalIdNumber", 'custcol_clgx_so_col_service_id', "greaterthan", id);
    	    	    	}
    	    	    	var file = nlapiCreateFile('CLGX_JSON_CMD_Managed_Services.json', 'PLAINTEXT', JSON.stringify(services));
    	    	    	file.setFolder(3824960);
    	    	    	nlapiSubmitFile(file);	
    		    	
    	// =================================================== Merge services in managed =================================================== 
    			    	    
    	    	    	for ( var i = 0; i < managed.length; i++ ) {
    	    	    		var service = _.find(services, function(arr){ return (arr.soid == managed[i].soid && arr.serviceid == managed[i].serviceid) ; });
    	    	    		if(service){
    	    	    			managed[i].categoryid = service.categoryid;
    	    	    			managed[i].itemid = service.itemid;
    	    	    			managed[i].item = service.item;
    	    	    		}
    	    	    	}
    	    	    	var file = nlapiCreateFile('CLGX_JSON_CMD_Managed_All.json', 'PLAINTEXT', JSON.stringify(managed));
    	    	    	file.setFolder(3824960);
    	    	    	nlapiSubmitFile(file);
    		    	    	
    	// =================================================== Managed Topology =================================================== 
    		    	    	
    	    	    	var tree = managed_topology (managed);
    	    			
    	    	    	var file = nlapiCreateFile('CLGX_JSON_CMD_Managed_Topology.json', 'PLAINTEXT', JSON.stringify(tree));
    	    	    	file.setFolder(3824960);
    	    	    	nlapiSubmitFile(file);
    		    	    	
    		    	

    	// =================================================== Provisioned Managed =================================================== 
    	    		
    		    	var provisioned = _.filter(managed, function(arr){
    		    		return (arr.categoryid > 0);
    		    	});
    		    	var tree = grid_to_tree ('Managed', 'Provisioned', provisioned);
    				
    		    	var file = nlapiCreateFile('CLGX_JSON_CMD_Managed_Provisioned.json', 'PLAINTEXT', JSON.stringify(tree));
    		    	file.setFolder(3824960);
    		    	nlapiSubmitFile(file);
    	    	
    	// =================================================== Miss configs (No match SO + Job) ===================================================  	
    	    	    
    		    	var misconfigs = _.filter(managed, function(arr){
    		    		return (arr.categoryid == 0);
    		    	});
    		    	var tree = grid_to_tree ('Managed', 'Misconfigs', misconfigs);
    		    	
    			var file = nlapiCreateFile('CLGX_JSON_CMD_Managed_Misconfigs.json', 'PLAINTEXT', JSON.stringify(tree));
    		    	file.setFolder(3824960);
    		    	nlapiSubmitFile(file);
    		   
    		    	
    	// =================================================== Orphans (No SO or no Job) ===================================================   	
    	    	    	
    	    	    	var orphans = [];
    	    	    	var search = nlapiLoadSearch('customrecord_clgx_managed_services', 'customsearch_clgx_cmd_managed_orphans');
    	    	    	var result = search.runSearch();
    	    	    	result.forEachResult(function(row) {
    	    				
    	    	    		var name = (row.getText('custrecord_clgx_ms_service_order', null, null)).split("#") || '';
    	    	    		var so = (name[name.length-1]).trim();
    	    	
    	    	    		var name = (row.getText('custrecord_clgx_ms_service', null, null)).split(":") || '';
    	    	    		var service = (name[name.length-1]).trim();
    	    	
    	    	    		orphans.push({
    	    	    			"nodeid": parseInt(row.getValue('internalid',null,null)) || 0,
    	    	    			"node": row.getValue('name',null,null) || '',
    	    	    			"type": 'managed',
    	    	    			"facilityid": parseInt(row.getValue('custrecord_clgx_ms_facility', null, null)) || 0,
    	    	    			"facility": row.getText('custrecord_clgx_ms_facility', null, null) || '',
    	    	    			"soid": parseInt(row.getValue('custrecord_clgx_ms_service_order', null, null)) || 0,
    	    	    			"so": so,
    	    	    			"serviceid": parseInt(row.getValue('custrecord_clgx_ms_service', null, null)) || 0,
    	    	    			"service": service,
    	    	    			"faicon": "plug",
    	    	    			"leaf": true
    	    	    		});
    	    	    		return true;
    	    	    	});
    	    	    	
    	    	    	var ids = _.uniq(_.pluck(orphans, 'facilityid'));
    	    			var facilities = [];
    	    			for ( var i = 0; i < ids.length; i++ ) {
    	    				var children = _.filter(orphans, function(arr){
    	    					return (arr.facilityid == ids[i]);
    	    				});
    	    				if(children){
    	    					facilities.push({
    	    						"nodeid": children[0].facilityid,
    	    						"node": children[0].facility,
    	    						"type": "facility",
    	    						"faicon": "gear",
    	    						"expanded": false,
    	    						"leaf": false,
    	    						"count": children.length,
    	    						"children": children
    	    					});
    	    				}
    	    			}
    	    			var tree = {
    	    				"text": ".",
    	    				"inventory": 'Managed',
    	    				"type": "Orphans",
    	    				"count": orphans.length,
    	    				"children": facilities
    	    			};
    	    	    	var file = nlapiCreateFile('CLGX_JSON_CMD_Managed_Orphans.json', 'PLAINTEXT', JSON.stringify(tree));
    	    	    	file.setFolder(3824960);
    	    	    	nlapiSubmitFile(file);
	    	
	    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    	}
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

