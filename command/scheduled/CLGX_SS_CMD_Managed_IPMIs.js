nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CMD_Managed_IPMIs.js
//	Script Name:	CLGX_SS_CMD_Managed_IPMIs
//	Script Id:		customscript_clgx_ss_cmd_managed_ipmis
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		02/20/2017
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_cmd_managed_ipmis(){
    try{

    	var requestURL = nlapiRequestURL(
  			  'https://nsapi1.dev.nac.net/v1.0/ipmi/accounts/?per_page=0',
  			  null,
  			  {
  			    'Content-type': 'application/json',
  			    'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
  			  },
  			  null,
		  'GET');
  	
    	var resp = JSON.parse( requestURL.body );
    	var arr1 = resp.data;
    	
		for ( var i = 0; i < arr1.length; i++ ) {
			arr1[i].ipmi = 'account';
    		arr1[i].node = arr1[i].id;
    		arr1[i].faicon = 'tachometer';
    		arr1[i].leaf = true;
    	}
    	
		
    	var requestURL = nlapiRequestURL(
    			  'https://nsapi1.dev.nac.net/v1.0/ipmi/vpn_accounts/?per_page=0',
    			  null,
    			  {
    			    'Content-type': 'application/json',
    			    'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
    			  },
    			  null,
  		  'GET');
    	
      	var resp = JSON.parse( requestURL.body );
      	var arr2 = resp.data;
      	
  		for ( var i = 0; i < arr2.length; i++ ) {
  			arr2[i].ipmi = 'vpn_account';
      		arr2[i].node = arr2[i].id;
      		arr2[i].faicon = 'tachometer';
      		arr2[i].leaf = true;
      	}
  		
  		var arr = _.union(arr1,arr2);
  		
  		var customers = get_customers_nac ();

    	var ids = _.uniq(_.pluck(arr, 'client_id'));
    	var clients = [];
    	for ( var i = 0; i < ids.length; i++ ) {
    		
    		var selection = _.filter(arr, function(arr){
				return (arr.client_id == ids[i]);
			});
    		
    		var nsid = 0;
			var nsname = '';
			var customer = _.find(customers, function(arr){ return (arr.nacid == ids[i]) ; });
			if(customer){
	    		nsid = customer.nsid;
	    		nsname = customer.nsname;
			}
			
    		clients.push({
				"node": selection[0].client_id,
				"nodeid": selection[0].client_id,
				"type": 'customer',
				"nsid": nsid,
				"nsname": nsname,
				"faicon": 'user',
    			"expanded": false,
    			"leaf": false,
    			"children":selection
            });
    	}
    	clients = _.sortBy(clients, function(obj){ return obj.node;});
    	
    	var tree = {
				"node": '.',
				"type": 'ipmis',
    			"children":clients
        };

    	var file = nlapiCreateFile('CLGX_JSON_CMD_Managed_IPMIs.json', 'PLAINTEXT', JSON.stringify(tree));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

