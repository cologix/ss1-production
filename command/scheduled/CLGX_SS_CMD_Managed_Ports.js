nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CMD_Managed_Ports.js
//	Script Name:	CLGX_SS_CMD_Managed_Ports
//	Script Id:		customscript_clgx_ss_cmd_managed_ports
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		02/20/2017
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_cmd_managed_ports(){
    try{

    	var requestURL = nlapiRequestURL(
    			  'https://nsapi1.dev.nac.net/v1.0/services/ports/?per_page=100',
    			  null,
    			  {
    			    'Content-type': 'application/json',
    			    'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
    			  },
    			  null,
  		  'GET');
      	var resp = JSON.parse( requestURL.body );
      	var pages = resp.pages;
      	
      	var arr = [];
      	for ( var i = 1; i < (pages + 1); i++ ) {
      		
          	var requestURL = nlapiRequestURL(
          			  'https://nsapi1.dev.nac.net/v1.0/services/ports/?per_page=100&page=' + i,
          			  null,
          			  {
          			    'Content-type': 'application/json',
          			    'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
          			  },
          			  null,
        		  'GET');
          	var resp = JSON.parse( requestURL.body );
            var temp = resp.data;
      		arr = _.union(arr,temp);
      		
      		nlapiLogExecution('DEBUG','debug', '| Page ' + i + ' / ' + pages + ' | ');
        
      	}
      	
      	for ( var i = 0; i < arr.length; i++ ) {
			arr[i].node = arr[i].id;
			arr[i].type = 'port';
    		arr[i].faicon = 'tags';
    		arr[i].leaf = true;
    	}
    	
		var customers = get_customers_nac ();

    	var ids = _.uniq(_.pluck(arr, 'port_net_device_id'));
    	var devices = [];
    	for ( var i = 0; i < ids.length; i++ ) {
    		
    		var selection = _.filter(arr, function(arr){
				return (arr.port_net_device_id == ids[i]);
			});
			
    		devices.push({
				"node": selection[0].port_net_device_name,
				"nodeid": selection[0].port_net_device_id,
				"type": 'device',
				"faicon": 'cog',
    			"expanded": false,
    			"leaf": false,
    			"children":selection
            });
    	}
    	devices = _.sortBy(devices, function(obj){ return obj.node;});
    	
    	var tree = {
				"node": '.',
				"type": 'ports',
    			"children":devices
        };

    	var file = nlapiCreateFile('CLGX_JSON_CMD_Managed_Ports.json', 'PLAINTEXT', JSON.stringify(tree));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

