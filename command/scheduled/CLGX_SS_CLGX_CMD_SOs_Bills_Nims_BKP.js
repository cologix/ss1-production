//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CLGX_CMD_SOs_Bills_Nims.js
//	Script Name:	CLGX_SS_CLGX_CMD_SOs_Bills_Nims
//	Script Id:		customscript_clgx_ss_clgx_cmd_bills_nims
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		02/210/2017
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_cmd_bills_nims(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
    	
        var context = nlapiGetContext();
        var initial = moment();
        
        var deployment = context.getDeploymentId();
    	if(deployment == 'customdeploy_clgx_ss_clgx_cmd_bill_nim_1'){
    		var fileid = 5086060;
    		var deploy = 1;
    	}
    	if(deployment == 'customdeploy_clgx_ss_clgx_cmd_bill_nim_4'){
    		var fileid = 5086061;
    		var deploy = 2;
    	}
    	if(deployment == 'customdeploy_clgx_ss_clgx_cmd_bill_nim_5'){
    		var fileid = 5086062;
    		var deploy = 3;
    	}
    	
    	/*
    	var file = nlapiLoadFile(file);
    	var arr = JSON.parse(file.getValue());
    	if(arr.length == 0){
            var filters = [];
    		while (true) {
    			search = nlapiSearchRecord('transaction', 'customsearch_clgx_cmd_sos_ids', filters);
        		if (!search) {
        			break;
        		}
        		for (var i in search) {
        			var id = parseInt(search[i].getValue('internalid',null,'GROUP'));
            		arr.push(id);
        		}
        		if (search.length < 1000) {
        			break;
        		}
        		filters[0] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", id);
        	}
    		var file = nlapiCreateFile('CLGX_JSON_CMD_SOs_IDs.json', 'PLAINTEXT', JSON.stringify(arr));
        	file.setFolder(3824960);
    		var fileid = nlapiSubmitFile(file);
    		nlapiLogExecution('DEBUG','debug', '| fileid = ' + fileid + ' | ');
    	}
    	*/
    	
	    var file = nlapiLoadFile(fileid);
    	var arr = JSON.parse(file.getValue());
    	
    	var arrNew = arr;

    	for ( var i = 0; arr != null && i < arr.length; i++ ) {
        	
        	var start = moment();
        	var totalMinutes = (start.diff(initial)/60000).toFixed(1);
        	if ( context.getRemainingUsage() <= 1000 || totalMinutes > 50 ){
 
        		var file = nlapiCreateFile('CLGX_JSON_CMD_SOs_IDs_'+ deploy + '.json', 'PLAINTEXT', JSON.stringify(arrNew));
            	file.setFolder(3824960);
        		nlapiSubmitFile(file);
        		
        		var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                if ( status == 'QUEUED' ) {
             	   nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                   break; 
                }
            }
        	
        	set_so (arr[i]);
        	
			arrNew = _.reject(arrNew, function(num){
		        return (num == arr[i]);
			});
			
			var usage =  10000 - parseInt(context.getRemainingUsage());
        	nlapiLogExecution('DEBUG', 'SO', '| SOID = ' + arr[i] + ' | ' + i + '/' + arr.length +  ' | Usage = '+ usage + ' |');
        }

    	var file = nlapiCreateFile('CLGX_JSON_CMD_SOs_IDs_'+ deploy + '.json', 'PLAINTEXT', JSON.stringify(arrNew));
    	file.setFolder(3824960);
		nlapiSubmitFile(file);
	
		nlapiSendEmail(71418,71418,'finished ' + deployment,'finished' + deployment,null,null,null,null);
		
		nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function set_so (soid) {

	var inventory = {
			"space_bills": 0,
			"space_nims": 0,
			"power_bills": 0,
			"power_nims": 0,
			"network_bills": 0,
			"network_nims": 0,
			"xc_bills": 0,
			"xc_nims": 0,
			"vxc_bills": 0,
			"vxc_nims": 0,
			//"services": []
	};
	
	var filters = [];
	filters.push(new nlobjSearchFilter("internalid",null,"anyof",soid));
	var searchSO = nlapiSearchRecord('transaction', 'customsearch_clgx_cmd_so_bills_nims', filters);
	var services = [];
	for ( var i = 0; searchSO != null && i < searchSO.length; i++ ) {
		
		var serviceid = parseInt(searchSO[i].getValue('custcol_clgx_so_col_service_id',null,null));
		var service = searchSO[i].getText('custcol_clgx_so_col_service_id',null,null);
		
		var itemid = parseInt(searchSO[i].getValue('item',null,null));
		var item = searchSO[i].getText('item',null,null);
		
		var categoryid = parseInt(searchSO[i].getValue('custcol_cologix_invoice_item_category',null,null));
		var category = searchSO[i].getText('custcol_cologix_invoice_item_category',null,null);
		
		var quantity = parseFloat(searchSO[i].getValue('custcol_clgx_qty2print',null,null));
		
		// space
		if(categoryid == 10){
			if(item.indexOf("Foot Print") > -1){
				quantity = 1;
			}
			var columns = [];
	    	columns.push(new nlobjSearchColumn('internalid',null,null));
			var filters = [];
			filters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
			filters.push(new nlobjSearchFilter("custrecord_space_service_order",null,"anyof",soid));
			filters.push(new nlobjSearchFilter("custrecord_cologix_space_project",null,"anyof",serviceid));
			var search = nlapiSearchRecord('customrecord_cologix_space', null, filters, columns);
			if(search){
				inventory.space_nims += search.length;
				if(item.indexOf("Cage") > -1){
					inventory.space_bills += search.length;
				} else {
					inventory.space_bills += quantity;
				}
			} else {
				inventory.space_bills += quantity;
			}
		}
		
		// power
		if(categoryid == 8){
			if(item.indexOf("A+B") > -1){
				quantity = quantity * 2;
			}
			var columns = [];
	    	columns.push(new nlobjSearchColumn('internalid',null,null));
			var filters = [];
			filters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
			filters.push(new nlobjSearchFilter("custrecord_power_circuit_service_order",null,"anyof",soid));
			filters.push(new nlobjSearchFilter("custrecord_cologix_power_service",null,"anyof",serviceid));
			var search = nlapiSearchRecord('customrecord_clgx_power_circuit', null, filters, columns);
			if(search){
				inventory.power_nims += search.length;
			}
			inventory.power_bills += quantity;
		}

		// network
		if(categoryid == 5){
			var columns = [];
	    	columns.push(new nlobjSearchColumn('internalid',null,null));
			var filters = [];
			filters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
			filters.push(new nlobjSearchFilter("custrecord_xconnect_service_order",null,"anyof",soid));
			filters.push(new nlobjSearchFilter("custrecord_cologix_xc_service",null,"anyof",serviceid));
			//filters.push(new nlobjSearchFilter("custcol_cologix_invoice_item_category",'custrecord_cologix_xc_service',"anyof",categoryid));
			var search = nlapiSearchRecord('customrecord_cologix_crossconnect', null, filters, columns);
			if(search){
				inventory.network_nims += search.length;
			}
			inventory.network_bills += quantity;
		}
		
		// xc
		if(categoryid == 4){
			var columns = [];
	    	columns.push(new nlobjSearchColumn('internalid',null,null));
			var filters = [];
			filters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
			filters.push(new nlobjSearchFilter("custrecord_xconnect_service_order",null,"anyof",soid));
			filters.push(new nlobjSearchFilter("custrecord_cologix_xc_service",null,"anyof",serviceid));
			//filters.push(new nlobjSearchFilter("custcol_cologix_invoice_item_category",'custrecord_cologix_xc_service',"anyof",categoryid));
			var search = nlapiSearchRecord('customrecord_cologix_crossconnect', null, filters, columns);
			if(search){
				inventory.xc_nims += search.length;
			}
			inventory.xc_bills += quantity;
		}
		
		// vxc
		if(categoryid == 11){
			var columns = [];
	    	columns.push(new nlobjSearchColumn('internalid',null,null));
			var filters = [];
			filters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
			filters.push(new nlobjSearchFilter("custrecord_cologix_service_order",null,"anyof",soid));
			filters.push(new nlobjSearchFilter("custrecord_cologix_vxc_service",null,"anyof",serviceid));
			var search = nlapiSearchRecord('customrecord_cologix_vxc', null, filters, columns);
			if(search){
				inventory.vxc_nims += search.length;
			}
			inventory.vxc_bills += quantity;
		}
	}
	
	try {
		var rec = nlapiLoadRecord('salesorder', soid);
		rec.setFieldValue('custbody_clgx_cmd_network_bills', inventory.network_bills);
		rec.setFieldValue('custbody_clgx_cmd_network_nims', inventory.network_nims);
		rec.setFieldValue('custbody_clgx_cmd_power_bills', inventory.power_bills);
		rec.setFieldValue('custbody_clgx_cmd_power_nims', inventory.power_nims);
		rec.setFieldValue('custbody_clgx_cmd_space_bills', inventory.space_bills);
		rec.setFieldValue('custbody_clgx_cmd_space_nims', inventory.space_nims);
		rec.setFieldValue('custbody_clgx_cmd_vxc_bills', inventory.vxc_bills);
		rec.setFieldValue('custbody_clgx_cmd_vxc_nims', inventory.vxc_nims);
		rec.setFieldValue('custbody_clgx_cmd_xc_bills', inventory.xc_bills);
		rec.setFieldValue('custbody_clgx_cmd_xc_nims', inventory.xc_nims);
		nlapiSubmitRecord(rec, true, true);
	}
	catch (error) {
		
	}
	
	return true;
}