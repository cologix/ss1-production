nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CMD_Transactions.js
//	Script Name:	CLGX_SS_CMD_Transactions
//	Script Id:		customscript_clgx_ss_cmd_transactions
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/22/2016
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_cmd_transactions(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
    var context = nlapiGetContext();
    	
    var objFile = nlapiLoadFile(4830208);
    	var spaces = JSON.parse(objFile.getValue());
    	
    	var objFile = nlapiLoadFile(4833030);
    	var powers = JSON.parse(objFile.getValue());
    	
    	var objFile = nlapiLoadFile(4875312);
    	var net = JSON.parse(objFile.getValue());
    	
    	var objFile = nlapiLoadFile(4834334);
    	var xcs = JSON.parse(objFile.getValue());
    	
    	var objFile = nlapiLoadFile(4662283);
    	var vxcs = JSON.parse(objFile.getValue());
    	
    	var objFile = nlapiLoadFile(11063851);
    	var managed = JSON.parse(objFile.getValue());
    	
    	var services = [];
    	var search = nlapiLoadSearch('transaction', 'customsearch_clgx_cmd_customers_location');
    	var result = search.runSearch();
    	result.forEachResult(function(row) {
    		
    		var columns = row.getAllColumns();
    		var customerid = parseInt(row.getValue('mainname', null, 'GROUP')) || 0;
    		var customer = row.getText('mainname', null, 'GROUP') || '';
    		
    		var locationid = parseInt(row.getValue('location', null, 'GROUP')) || 0;
    		var name = (row.getText('location', null, 'GROUP')).split(":") || '';
    		var location = (name[name.length-1]).trim();
    		
    		var sos_space = parseInt(row.getValue(columns[4])) || 0;
    		var cages = parseInt(row.getValue(columns[10])) || 0;
    		
    		var inv_space = 0;
    		var selection = _.filter(spaces, function(arr){
				return (arr.customerid == customerid && arr.locationid == locationid);
			});
    		if(selection){
    			inv_space = selection.length;
    		}
    		if(cages > 0){
    			sos_space = inv_space;
    		}
    		
    		var inv_power = 0;
    		var selection = _.filter(powers, function(arr){
				return (arr.customerid == customerid && arr.locationid == locationid);
			});
    		if(selection){
    			inv_power = selection.length;
    		}
    		
    		var inv_net = 0;
    		var selection = _.filter(net, function(arr){
				return (arr.customerid == customerid && arr.locationid == locationid);
			});
    		if(selection){
    			inv_net = selection.length;
    		}
    		
    		var inv_xcs = 0;
    		var selection = _.filter(xcs, function(arr){
				return (arr.customerid == customerid && arr.locationid == locationid);
			});
    		if(selection){
    			inv_xcs = selection.length;
    		}
    		
    		var inv_vxcs = 0;
    		var selection = _.filter(vxcs, function(arr){
				return (arr.customerid == customerid && arr.locationid == locationid);
			});
    		if(selection){
    			inv_vxcs = selection.length;
    		}

    		var inv_managed = 0;
    		var selection = _.filter(managed, function(arr){
				return (arr.customerid == customerid && arr.locationid == locationid);
			});
    		if(selection){
    			inv_managed = selection.length;
    		}
    		
    		services.push({
    				"customerid": customerid,
    				"customer": customer,
    				"nodeid": locationid,
    				"node": location,
    				"type": "location",
    				"sos_count": parseInt(row.getValue('internalid', null, 'COUNT')) || 0,
    				"sos_space": sos_space,
    				"inv_space": inv_space,
    				"sos_power": parseInt(row.getValue(columns[5])) || 0,
    				"inv_power": inv_power,
    				"sos_net": parseInt(row.getValue(columns[6])) || 0,
    				"inv_net": inv_net,
    				"sos_xcs": parseInt(row.getValue(columns[7])) || 0,
    				"inv_xcs": inv_xcs,
    				"sos_vxcs": parseInt(row.getValue(columns[8])) || 0,
    				"inv_vxcs": inv_vxcs,
    				"sos_managed": parseInt(row.getValue(columns[9])) || 0,
    				"inv_managed": inv_managed,
    				"cages": cages,
    				"faicon": "building-o",
    				"leaf": true
    		});
    		return true;
    	});

    	var file = nlapiCreateFile('CLGX_JSON_CMD_Services_Transactions.json', 'PLAINTEXT', JSON.stringify(services));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
    	var sum_cust_count = 0;
    	var sum_sos_count = 0;
    	var sum_sos_space = 0;
    	var sum_inv_space = 0;
    	var sum_sos_power = 0;
    	var sum_inv_power = 0;
    	var sum_sos_net = 0;
    	var sum_inv_net = 0;
    	var sum_sos_xcs = 0;
    	var sum_inv_xcs = 0;
    	var sum_sos_vxcs = 0;
    	var sum_inv_vxcs = 0;
    	var sum_sos_managed = 0;
    	var sum_inv_managed = 0;
    	var sum_cages = 0;
    	
    	var locations = _.uniq(_.pluck(services, 'node'));
    	locations = _.sortBy(locations);
    	var ids = _.uniq(_.pluck(services, 'customerid'));

    	var sums = [];
    	for ( var i = 0; i < locations.length; i++ ) {
    		
    		var cust_count = 0;
    		var sos_count = 0;
    		var sos_space = 0;
    		var inv_space = 0;
    		var sos_power = 0;
    		var inv_power = 0;
    		var sos_net = 0;
    		var inv_net = 0;
    		var sos_xcs = 0;
    		var inv_xcs = 0;
    		var sos_vxcs = 0;
    		var inv_vxcs = 0;
    		var sos_managed = 0;
    		var inv_managed = 0;
    		var cages = 0;
    		
    		var selection = _.filter(services, function(arr){
    			return (arr.node == locations[i]);
    		});
    		
    		if(selection){
    			
    			var arr = _.uniq(_.pluck(selection, 'customerid'));
    			cust_count = arr.length;
    			sum_cust_count += arr.length;
    			
    			var arr = _.pluck(selection, 'sos_count');
    			sos_count = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_sos_count += sos_count;
    			
    			var arr = _.pluck(selection, 'sos_space');
    			sos_space = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_sos_space += sos_space;
    			
    			var arr = _.pluck(selection, 'inv_space');
    			inv_space = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_inv_space += inv_space;
    			
    			var arr = _.pluck(selection, 'sos_power');
    			sos_power = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_sos_power += sos_power;
    			
    			var arr = _.pluck(selection, 'inv_power');
    			inv_power = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_inv_power += inv_power;
    			
    			var arr = _.pluck(selection, 'sos_net');
    			sos_net = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_sos_net += sos_net;
    			
    			var arr = _.pluck(selection, 'inv_net');
    			inv_net = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_inv_net += inv_net;
    			
    			var arr = _.pluck(selection, 'sos_xcs');
    			sos_xcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_sos_xcs += sos_xcs;
    			
    			var arr = _.pluck(selection, 'inv_xcs');
    			inv_xcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_inv_xcs += inv_xcs;
    			
    			var arr = _.pluck(selection, 'sos_vxcs');
    			sos_vxcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_sos_vxcs += sos_vxcs;
    			
    			var arr = _.pluck(selection, 'inv_vxcs');
    			inv_vxcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_inv_vxcs += inv_vxcs;

    			var arr = _.pluck(selection, 'sos_managed');
    			sos_managed = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_sos_managed += sos_managed;
    			
    			var arr = _.pluck(selection, 'inv_managed');
    			inv_managed = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_inv_managed += inv_managed;

    			var arr = _.pluck(selection, 'cages');
    			cages = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_cages += cages;
    			
    		}

    		sums.push({
    			"locationid": selection[0].nodeid,
    			"location": locations[i],
    			"cust_count": cust_count,
    			"sos_count": sos_count,
    			"sos_space": sos_space,
    			"inv_space": inv_space,
    			"sos_power": sos_power,
    			"inv_power": inv_power,
    			"sos_net": sos_net,
    			"inv_net": inv_net,
    			"sos_xcs": sos_xcs,
    			"inv_xcs": inv_xcs,
    			"sos_vxcs": sos_vxcs,
    			"inv_vxcs": inv_vxcs,
    			"sos_managed": sos_managed,
    			"inv_managed": inv_managed,
    			"cages": cages
    		});
    		
    	}
    	/*
    	sums.push({
    		"locationid": 0,
    		"location": 'Totals',
    		"cust_count": ids.length,
    		"sos_count": sum_sos_count,
    		"sos_space": sum_sos_space,
    		"inv_space": sum_inv_space,
    		"sos_power": sum_sos_power,
    		"inv_power": sum_inv_power,
    		"sos_net": sum_sos_net,
    		"inv_net": sum_inv_net,
    		"sos_xcs": sum_sos_xcs,
    		"inv_xcs": sum_inv_xcs,
    		"sos_vxcs": sum_sos_vxcs,
    		"cages": sum_cages
    	});
    	*/
    	
    	var file = nlapiCreateFile('CLGX_JSON_CMD_Locations.json', 'PLAINTEXT', JSON.stringify(sums));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
    	var sum_cust_count = 0;
    	var sum_sos_count = 0;
    	var sum_sos_space = 0;
    	var sum_inv_space = 0;
    	var sum_sos_power = 0;
    	var sum_inv_power = 0;
    	var sum_sos_net = 0;
    	var sum_inv_net = 0;
    	var sum_sos_xcs = 0;
    	var sum_inv_xcs = 0;
    	var sum_sos_vxcs = 0;
    	var sum_inv_vxcs = 0;
    	var sum_sos_managed = 0;
    	var sum_inv_managed = 0;
    	var sum_cages = 0;
    	
    	var customers = [];
    	for ( var i = 0; i < ids.length; i++ ) {
    		
    		var cust_count = 0;
    		var sos_count = 0;
    		var sos_space = 0;
    		var inv_space = 0;
    		var sos_power = 0;
    		var inv_power = 0;
    		var sos_net = 0;
    		var inv_net = 0;
    		var sos_xcs = 0;
    		var inv_xcs = 0;
    		var sos_vxcs = 0;
    		var inv_vxcs = 0;
    		var sos_managed = 0;
    		var inv_managed = 0;
    		var cages = 0;

    		var selection = _.filter(services, function(arr){
    			return (arr.customerid == ids[i]);
    		});
    		
    		if(selection){
    			
    			_.map(selection, function(obj) { return _.omit(obj, ['customerid', 'customer', 'nodeid']); });
    			
    			var arr = _.pluck(selection, 'sos_count');
    			sos_count = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_sos_count += sos_count;
    			
    			var arr = _.pluck(selection, 'sos_space');
    			sos_space = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_sos_space += sos_space;
    			
    			var arr = _.pluck(selection, 'inv_space');
    			inv_space = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_inv_space += inv_space;
    			
    			var arr = _.pluck(selection, 'sos_power');
    			sos_power = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_sos_power += sos_power;
    			
    			var arr = _.pluck(selection, 'inv_power');
    			inv_power = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_inv_power += inv_power;
    			
    			var arr = _.pluck(selection, 'sos_net');
    			sos_net = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_sos_net += sos_net;
    			
    			var arr = _.pluck(selection, 'inv_net');
    			inv_net = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_inv_net += inv_net;
    			
    			var arr = _.pluck(selection, 'sos_xcs');
    			sos_xcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_sos_xcs += sos_xcs;
    			
    			var arr = _.pluck(selection, 'inv_xcs');
    			inv_xcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_inv_xcs += inv_xcs;
    			
    			var arr = _.pluck(selection, 'sos_vxcs');
    			sos_vxcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_sos_vxcs += sos_vxcs;
    			
    			var arr = _.pluck(selection, 'inv_vxcs');
    			inv_vxcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_inv_vxcs += inv_vxcs;

    			var arr = _.pluck(selection, 'sos_managed');
    			sos_managed = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_sos_managed += sos_managed;
    			
    			var arr = _.pluck(selection, 'inv_managed');
    			inv_managed = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_inv_managed += inv_managed;

    			var arr = _.pluck(selection, 'cages');
    			cages = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
    			sum_cages += cages;
    			
    		}

    		customers.push({
    			"nodeid": selection[0].customerid,
    			"node": selection[0].customer,
    			"type": "customer",
    			"sos_count": sos_count,
    			"sos_space": sos_space,
    			"inv_space": inv_space,
    			"sos_power": sos_power,
    			"inv_power": inv_power,
    			"sos_net": sos_net,
    			"inv_net": inv_net,
    			"sos_xcs": sos_xcs,
    			"inv_xcs": inv_xcs,
    			"sos_vxcs": sos_vxcs,
    			"inv_vxcs": inv_vxcs,
    			"sos_managed": sos_managed,
    			"inv_managed": inv_managed,
    			"cages": cages,
    			"faicon": "user-secret",
    			"expanded": false,
    			"leaf": false,
    			"children": selection
    		});
    		
    	}
    	/*
    	customers.push({
    		"nodeid": 0,
    		"node": 'Totals',
    		"type": "total",
    		"sos_count": sum_sos_count,
    		"sos_space": sum_sos_space,
    		"inv_space": sum_inv_space,
    		"sos_power": sum_sos_power,
    		"inv_power": sum_inv_power,
    		"sos_net": sum_sos_net,
    		"inv_net": sum_inv_net,
    		"sos_xcs": sum_sos_xcs,
    		"inv_xcs": sum_inv_xcs,
    		"sos_vxcs": sum_sos_vxcs,
    		"inv_vxcs": sum_inv_vxcs,
    		"cages": sum_cages,
			"faicon": "users",
    		"leaf": true
    	});
    	*/
    	var treeCustomers = {
    			"text": ".",
    			"children": customers
    	};
    	var file = nlapiCreateFile('CLGX_JSON_CMD_Customers.json', 'PLAINTEXT', JSON.stringify(treeCustomers));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
    	
    //=============================================================== Services  ===============================================================   

    	var children = [];
    	
    	// ========== spaces ==============================
    	
    	var ids = _.uniq(_.pluck(spaces, 'locationid'));
    	var locations = [];
    	for ( var i = 0; i < ids.length; i++ ) {
    		
    		var arr = _.filter(services, function(arr){
    			return (arr.nodeid == ids[i] && arr.sos_space > 0);
    		});
    		var counts = _.pluck(arr, 'sos_count');
    		sos = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		var counts = _.pluck(arr, 'sos_space');
    		bills = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		var counts = _.pluck(arr, 'inv_space');
    		nims = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		
    		var arr = _.filter(spaces, function(arr){
    			return (arr.locationid == ids[i]);
    		});
    		nims = arr.length;

    		locations.push({
    			"nodeid" : arr[0].locationid,
    			"node" : arr[0].location,
    			"type": "location",
    			"customers": arr.length,
    			"sos": sos,
    			"bills": bills,
    			"nims": nims,
    			"faicon": "building",
    			"leaf": true
    		});
    	}
    	locations = _.sortBy(locations, function(obj){ return obj.node;});
    	
    	var values = _.pluck(locations, 'customers');
    	customers = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'sos');
    	sos = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'bills');
    	bills = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'nims');
    	nims = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	
    	children.push({
    		"nodeid": 0,
    		"node": "Spaces",
    		"type": "spaces",
    		"customers": customers,
    		"sos": sos,
    		"bills": bills,
    		"nims": nims,
    		"faicon": "map-o",
    		"expanded": false,
    		"leaf": false,
    		"children": locations
    	});

    	// ========== powers ==============================
    	
    	var ids = _.uniq(_.pluck(powers, 'locationid'));
    	var locations = [];
    	for ( var i = 0; i < ids.length; i++ ) {
    		
    		var arr = _.filter(services, function(arr){
    			return (arr.nodeid == ids[i] && arr.sos_power > 0);
    		});
    		var counts = _.pluck(arr, 'sos_count');
    		sos = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		var counts = _.pluck(arr, 'sos_power');
    		bills = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		var counts = _.pluck(arr, 'inv_power');
    		nims = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		
    		var arr = _.filter(powers, function(arr){
    			return (arr.locationid == ids[i]);
    		});
    		nims = arr.length;

    		locations.push({
    			"nodeid" : arr[0].locationid,
    			"node" : arr[0].location,
    			"type": "location",
    			"customers": arr.length,
    			"sos": sos,
    			"bills": bills,
    			"nims": nims,
    			"faicon": "building",
    			"leaf": true
    		});
    	}
    	locations = _.sortBy(locations, function(obj){ return obj.node;});
    	
    	var values = _.pluck(locations, 'customers');
    	customers = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'sos');
    	sos = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'bills');
    	bills = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'nims');
    	nims = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	
    	children.push({
    		"nodeid": 0,
    		"node": "Powers",
    		"type": "powers",
    		"customers": customers,
    		"sos": sos,
    		"bills": bills,
    		"nims": nims,
    		"faicon": "plug",
    		"expanded": false,
    		"leaf": false,
    		"children": locations
    	});
    	
    	// ========== net ==============================
    	
    	var ids = _.uniq(_.pluck(net, 'locationid'));
    	var locations = [];
    	for ( var i = 0; i < ids.length; i++ ) {
    		
    		var arr = _.filter(services, function(arr){
    			return (arr.nodeid == ids[i] && arr.sos_net > 0);
    		});
    		var counts = _.pluck(arr, 'sos_count');
    		sos = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		var counts = _.pluck(arr, 'sos_net');
    		bills = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		var counts = _.pluck(arr, 'inv_net');
    		nims = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		
    		var arr = _.filter(net, function(arr){
    			return (arr.locationid == ids[i]);
    		});
    		nims = arr.length;

    		locations.push({
    			"nodeid" : arr[0].locationid,
    			"node" : arr[0].location,
    			"type": "location",
    			"customers": arr.length,
    			"sos": sos,
    			"bills": bills,
    			"nims": nims,
    			"faicon": "building",
    			"leaf": true
    		});
    	}
    	locations = _.sortBy(locations, function(obj){ return obj.node;});
    	
    	var values = _.pluck(locations, 'customers');
    	customers = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'sos');
    	sos = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'bills');
    	bills = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'nims');
    	nims = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	
    	children.push({
    		"nodeid": 0,
    		"node": "Network",
    		"type": "net",
    		"customers": customers,
    		"sos": sos,
    		"bills": bills,
    		"nims": nims,
    		"faicon": "share-alt-square",
    		"expanded": false,
    		"leaf": false,
    		"children": locations
    	});
    	
    	// ========== xcs ==============================
    	
    	var ids = _.uniq(_.pluck(xcs, 'locationid'));
    	var locations = [];
    	for ( var i = 0; i < ids.length; i++ ) {
    		
    		var arr = _.filter(services, function(arr){
    			return (arr.nodeid == ids[i] && arr.sos_xcs > 0);
    		});
    		var counts = _.pluck(arr, 'sos_count');
    		sos = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		var counts = _.pluck(arr, 'sos_xcs');
    		bills = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		var counts = _.pluck(arr, 'inv_xcs');
    		nims = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		
    		var arr = _.filter(xcs, function(arr){
    			return (arr.locationid == ids[i]);
    		});
    		nims = arr.length;

    		locations.push({
    			"nodeid" : arr[0].locationid,
    			"node" : arr[0].location,
    			"type": "location",
    			"customers": arr.length,
    			"sos": sos,
    			"bills": bills,
    			"nims": nims,
    			"faicon": "building",
    			"leaf": true
    		});
    	}
    	locations = _.sortBy(locations, function(obj){ return obj.node;});
    	
    	var values = _.pluck(locations, 'customers');
    	customers = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'sos');
    	sos = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'bills');
    	bills = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'nims');
    	nims = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	
    	children.push({
    		"nodeid": 0,
    		"node": "XCs",
    		"type": "xcs",
    		"customers": customers,
    		"sos": sos,
    		"bills": bills,
    		"nims": nims,
    		"faicon": "share-alt-square",
    		"expanded": false,
    		"leaf": false,
    		"children": locations
    	});
    	
    	// ========== vxcs ==============================
    	
    	var ids = _.uniq(_.pluck(vxcs, 'locationid'));
    	var locations = [];
    	for ( var i = 0; i < ids.length; i++ ) {
    		
    		var arr = _.filter(services, function(arr){
    			return (arr.nodeid == ids[i] && arr.sos_vxcs > 0);
    		});
    		var counts = _.pluck(arr, 'sos_count');
    		sos = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		var counts = _.pluck(arr, 'sos_vxcs');
    		bills = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		var counts = _.pluck(arr, 'inv_vxcs');
    		nims = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		
    		var arr = _.filter(vxcs, function(arr){
    			return (arr.locationid == ids[i]);
    		});
    		nims = arr.length;

    		locations.push({
    			"nodeid" : arr[0].locationid,
    			"node" : arr[0].location,
    			"type": "location",
    			"customers": arr.length,
    			"sos": sos,
    			"bills": bills,
    			"nims": nims,
    			"faicon": "building",
    			"leaf": true
    		});
    	}
    	locations = _.sortBy(locations, function(obj){ return obj.node;});
    	
    	var values = _.pluck(locations, 'customers');
    	customers = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'sos');
    	sos = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'bills');
    	bills = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'nims');
    	nims = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	
    	children.push({
    		"nodeid": 0,
    		"node": "VXCs",
    		"type": "vxcs",
    		"customers": customers,
    		"sos": sos,
    		"bills": bills,
    		"nims": nims,
    		"faicon": "share-alt",
    		"expanded": false,
    		"leaf": false,
    		"children": locations
    	});
    	
    	
    	
    	
    	
    	
    	
    	// ========== managed ==============================
    	
    	var ids = _.uniq(_.pluck(managed, 'locationid'));
    	var locations = [];
    	for ( var i = 0; i < ids.length; i++ ) {
    		
    		var arr = _.filter(services, function(arr){
    			return (arr.nodeid == ids[i] && arr.sos_managed > 0);
    		});
    		var counts = _.pluck(arr, 'sos_count');
    		sos = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		var counts = _.pluck(arr, 'sos_managed');
    		bills = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		var counts = _.pluck(arr, 'inv_managed');
    		nims = _.reduce(counts, function(memo, num){ return memo + num; }, 0);
    		
    		var arr = _.filter(managed, function(arr){
    			return (arr.locationid == ids[i]);
    		});
    		nims = arr.length;

    		locations.push({
    			"nodeid" : arr[0].locationid,
    			"node" : arr[0].location,
    			"type": "location",
    			"customers": arr.length,
    			"sos": sos,
    			"bills": bills,
    			"nims": nims,
    			"faicon": "building",
    			"leaf": true
    		});
    	}
    	locations = _.sortBy(locations, function(obj){ return obj.node;});
    	
    	var values = _.pluck(locations, 'customers');
    	customers = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'sos');
    	sos = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'bills');
    	bills = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	var values = _.pluck(locations, 'nims');
    	nims = _.reduce(values, function(memo, num){ return memo + num; }, 0);
    	
    	children.push({
    		"nodeid": 0,
    		"node": "Managed",
    		"type": "managed",
    		"customers": customers,
    		"sos": sos,
    		"bills": bills,
    		"nims": nims,
    		"faicon": "share-alt",
    		"expanded": false,
    		"leaf": false,
    		"children": locations
    	});
    	
    	
    		var treeServices = {
    		"text": ".",
    		"children": children
    	};
    	
    	var file = nlapiCreateFile('CLGX_JSON_CMD_Services.json', 'PLAINTEXT', JSON.stringify(treeServices));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
 
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
