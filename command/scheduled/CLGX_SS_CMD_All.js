//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CMD_All.js
//	Script Name:	CLGX_SS_CMD_All
//	Script Id:		customscript_clgx_ss_cmd_all
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		01/06/2017
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_cmd_all(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
    	nlapiScheduleScript('customscript_clgx_ss_cmd_spaces', 'customdeploy_clgx_ss_cmd_spaces');
    	nlapiScheduleScript('customscript_clgx_ss_cmd_powers', 'customdeploy_clgx_ss_cmd_powers');
    	nlapiScheduleScript('customscript_clgx_ss_cmd_xcs', 'customdeploy_clgx_ss_cmd_xcs');
    	nlapiScheduleScript('customscript_clgx_ss_cmd_vxcs', 'customdeploy_clgx_ss_cmd_vxcs');
    	//nlapiScheduleScript('customscript_clgx_ss_cmd_transactions', 'customdeploy_clgx_ss_cmd_transactions');
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
 
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

