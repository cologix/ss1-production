nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CMD_Managed_IPs.js
//	Script Name:	CLGX_SS_CMD_Managed_IPs
//	Script Id:		customscript_clgx_ss_cmd_managed_ips
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		02/20/2017
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_cmd_managed_ips(){
    try{

    	var requestURL = nlapiRequestURL(
    			  'https://nsapi1.dev.nac.net/v1.0/ips/assignments/?per_page=200',
    			  null,
    			  {
    			    'Content-type': 'application/json',
    			    'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
    			  },
    			  null,
  		  'GET');
      	var resp = JSON.parse( requestURL.body );
      	var pages = resp.pages;
      	
      	var arr = [];
      	for ( var i = 1; i < (pages + 1); i++ ) {
      		
          	var requestURL = nlapiRequestURL(
          			  'https://nsapi1.dev.nac.net/v1.0/ips/assignments/?per_page=100&page=' + i,
          			  null,
          			  {
          			    'Content-type': 'application/json',
          			    'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
          			  },
          			  null,
        		  'GET');
          	var resp = JSON.parse( requestURL.body );
            var temp = resp.data;
      		arr = _.union(arr,temp);
      		
      		nlapiLogExecution('DEBUG','debug', '| Page ' + i + ' / ' + pages + ' | ');
        
      	}
      	
      	for ( var i = 0; i < arr.length; i++ ) {
			arr[i].node = arr[i].name;
			arr[i].type = 'ip';
    		arr[i].faicon = 'tag';
    		arr[i].leaf = true;
    	}
    	var file = nlapiCreateFile('CLGX_JSON_CMD_Managed_IPs_Raw.json', 'PLAINTEXT', JSON.stringify(arr));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);

    	var customers = get_customers_nac ();

    	var ids = _.uniq(_.pluck(arr, 'client_id'));
    	
    	var clients = [];
    	for ( var i = 0; i < ids.length; i++ ) {
    		
    		var selection = _.filter(arr, function(arr){
				return (arr.client_id == ids[i]);
			});
    		
    		var nsid = 0;
			var nsname = '';
			var customer = _.find(customers, function(arr){ return (arr.nacid == ids[i]) ; });
			if(customer){
	    		nsid = customer.nsid;
	    		nsname = customer.nsname;
			}
			if(nsname == ''){
				nsname = selection[0].client_id;
			}
				
    		clients.push({
				"node": nsname,
				"nodeid": selection[0].client_id,
				"type": 'customer',
				"nsid": nsid,
				"nsname": nsname,
				"faicon": 'user',
    			"expanded": false,
    			"leaf": false,
    			"children":selection
            });
    	}
    	
    	var tree = {
				"node": '.',
				"type": 'ips',
    			"children":clients
        };

    	var file = nlapiCreateFile('CLGX_JSON_CMD_Managed_IPs.json', 'PLAINTEXT', JSON.stringify(tree));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

