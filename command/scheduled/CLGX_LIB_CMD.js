//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_LIB_CMD.js
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/11/2017
//-------------------------------------------------------------------------------------------------

function grid_to_tree (inventory, type, grid){
	
	/*
	var ids = _.uniq(_.pluck(grid, 'serviceid'));
	var services = [];
	for ( var i = 0; i < ids.length; i++ ) {
		var children = _.filter(grid, function(arr){
			return (arr.serviceid == ids[i]);
		});
		if(children){
			services.push({
				"nodeid": children[0].serviceid,
				"node": children[0].service,
				"type": "service",
				"customerid": children[0].customerid,
				"customer": children[0].customer,
				"locationid": children[0].locationid,
				"location": children[0].location,
				"soid": children[0].soid,
				"so": children[0].so,
				"count": children.length,
				"faicon": "gear",
				"expanded": false,
				"leaf": false,
				"children": children
			});
		}
	}
	
	var ids = _.uniq(_.pluck(services, 'soid'));
	var sos = [];
	for ( var i = 0; i < ids.length; i++ ) {
		var children = _.filter(services, function(arr){
			return (arr.soid == ids[i]);
		});
		var arr = _.pluck(children, 'count');
		var sum = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
		if(children){
			sos.push({
				"nodeid": children[0].soid,
				"node": children[0].so,
				"type": "so",
				"customerid": children[0].customerid,
				"customer": children[0].customer,
				"locationid": children[0].locationid,
				"location": children[0].location,
				"count": sum,
				"faicon": "gears",
				"expanded": false,
				"leaf": false,
				"children": children
			});
		}
	}
	
	var ids = _.uniq(_.pluck(sos, 'customerid'));
	var customers = [];
	for ( var i = 0; i < ids.length; i++ ) {
		var children = _.filter(sos, function(arr){
			return (arr.customerid == ids[i]);
		});
		var arr = _.pluck(children, 'count');
		var sum = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
		if(children){
			customers.push({
				"nodeid": children[0].customerid,
				"node": children[0].customer,
				"type": "customer",
				"count": sum,
				"faicon": "user",
				"expanded": false,
				"leaf": false,
				"children": children
			});
		}
	}
	
	*/
	
	
	if(inventory == 'Managed'){
		
		var ids = _.uniq(_.pluck(grid, 'customerid'));
		var customers = [];
		for ( var i = 0; i < ids.length; i++ ) {
			var children = _.filter(grid, function(arr){
				return (arr.customerid == ids[i]);
			});
			if(children){
				
				var ids2 = _.uniq(_.pluck(children, 'typeid'));
				var types = [];
				for ( var j = 0; j < ids2.length; j++ ) {
					var children2 = _.filter(children, function(arr){
						return (arr.typeid == ids2[j]);
					});
					types.push({
						"nodeid": children2[0].typeid,
						"node": children2[0].type,
						"customerid": children[0].customerid,
						"customer": children[0].customer,
						"type": "type",
						"count": children2.length,
						"faicon": "gears",
						"expanded": false,
						"leaf": false,
						"children": children2
					});
				}
				customers.push({
					"nodeid": children[0].customerid,
					"node": children[0].customer,
					"customerid": children[0].customerid,
					"customer": children[0].customer,
					"type": "customer",
					"count": children.length,
					"faicon": "user",
					"expanded": false,
					"leaf": false,
					"children": types
				});
			}
		}
		
	} else {
		
		var ids = _.uniq(_.pluck(grid, 'customerid'));
		var customers = [];
		for ( var i = 0; i < ids.length; i++ ) {
			var children = _.filter(grid, function(arr){
				return (arr.customerid == ids[i]);
			});
			if(children){
				customers.push({
					"nodeid": children[0].customerid,
					"node": children[0].customer,
					"customerid": children[0].customerid,
					"customer": children[0].customer,
					"type": "customer",
					"count": children.length,
					"faicon": "user",
					"expanded": false,
					"leaf": false,
					"children": children
				});
			}
		}
	}
	
	
	
	

	customers = _.sortBy(customers, function(obj){ return obj.node;});
	
	
	
	
	
	
	var tree = {
			"text": ".",
			"inventory": inventory,
			"type": type,
			"count": grid.length,
			"children": customers
	};
	return tree;
}


function spaces_topology (spaces){
	
	var objFile = nlapiLoadFile(4833030);
	var powers = JSON.parse(objFile.getValue());
	for ( var i = 0; i < spaces.length; i++ ) {
		
		var spacePowers = _.filter(powers, function(arr){
			return (arr.spaceid == spaces[i].nodeid);
		});
		if(spacePowers.length > 0){
			spaces[i].leaf = true;
			//spaces[i]['children'] = spacePowers;
		}
	}
	
	var facilitiesids = _.uniq(_.pluck(spaces, 'facilityid'));
	
	var facilities = [];
	for ( var i = 0; i < facilitiesids.length; i++ ) {
		
		var facilitySpaces = _.filter(spaces, function(arr){
			return (arr.facilityid == facilitiesids[i]);
		});
		
		var floorsids = _.uniq(_.pluck(facilitySpaces, 'floor'));
		var floors = [];
		for ( var j = 0; j < floorsids.length; j++ ) {
			
    		var facilityFloors = _.filter(facilitySpaces, function(arr){
				return (arr.floor == floorsids[j]);
			});
			
			var roomsids = _.uniq(_.pluck(facilityFloors, 'room'));
    		var rooms = [];
    		for ( var k = 0; k < roomsids.length; k++ ) {
    			
        		var floorRooms = _.filter(facilityFloors, function(arr){
    				return (arr.room == roomsids[k]);
    			});
    			
        		var cagesids = _.uniq(_.pluck(floorRooms, 'cage'));
        		var cages = [];
        		for ( var l = 0; l < cagesids.length; l++ ) {
        			
            		var roomCages = _.filter(floorRooms, function(arr){
        				return (arr.cage == cagesids[l]);
        			});
            		
            		var rowsids = _.uniq(_.pluck(roomCages, 'row'));
					var rows = [];
            		for ( var m = 0; m < rowsids.length; m++ ) {
            			
                		var cageRows = _.filter(roomCages, function(arr){
            				return (arr.row == rowsids[m]);
            			});
                		
                		var racksids = _.uniq(_.pluck(cageRows, 'rack'));
        				
                		var racks = [];
                		for ( var n = 0; n < racksids.length; n++ ) {
                    		var rackSpaces = _.filter(cageRows, function(arr){
                				return (arr.rack == racksids[n]);
                			});
                    		rackSpaces = _.sortBy(rackSpaces, function(obj){ return obj.node;});
                    		
                    		racks.push({
                    			"node": facilitySpaces[0].facility + '.' + facilityFloors[0].floor + '.' + floorRooms[0].room + '.' + roomCages[0].cage + '.' + cageRows[0].row + '.' + rackSpaces[0].rack,
                    			"type": 'rack',
                    			"facilityid": rackSpaces[0].facilityid,
                    			//"facility": rackSpaces[0].facility,
                    			"floor": rackSpaces[0].floor,
                    			"room": rackSpaces[0].room,
                    			"row": rackSpaces[0].row,
                    			"rack": rackSpaces[0].rack,
                    			"checked": false,
                    			"faicon": "map-o",
                    			"expanded": false,
                    			"leaf": false,
                    			"children": rackSpaces
                    		});
                		}
                		racks = _.sortBy(racks, function(obj){ return obj.node;});
                		
                		rows.push({
                			"node": facilitySpaces[0].facility + '.' + facilityFloors[0].floor + '.' + floorRooms[0].room + '.' + roomCages[0].cage + '.' + cageRows[0].row,
                			"type": 'row',
                			"facilityid": cageRows[0].facilityid,
                			//"facility": cageRows[0].facility,
                			"floor": cageRows[0].floor,
                			"room": cageRows[0].room,
                			"row": cageRows[0].row,
                			"checked": false,
                			"faicon": "map-o",
                			"expanded": true,
                			"leaf": false,
                			"children": racks
                		});
            		}
            		rows = _.sortBy(rows, function(obj){ return obj.node;});
            		
            		cages.push({
            			"node": facilitySpaces[0].facility + '.' + facilityFloors[0].floor + '.' + floorRooms[0].room + '.' + roomCages[0].cage,
            			"type": 'cage',
            			"facilityid": roomCages[0].facilityid,
            			//"facility": roomCages[0].facility,
            			"floor": roomCages[0].floor,
            			"room": roomCages[0].room,
            			"cage": roomCages[0].cage,
            			"checked": false,
            			"faicon": "map-o",
            			"expanded": true,
            			"leaf": false,
            			"children": rows
            		});
        		}
        		cages = _.sortBy(cages, function(obj){ return obj.node;});
        		
        		rooms.push({
        			"node": facilitySpaces[0].facility + '.' + facilityFloors[0].floor + '.' + floorRooms[0].room,
        			"type": 'room',
        			"facilityid": floorRooms[0].facilityid,
        			"facility": floorRooms[0].facility,
        			"floor": floorRooms[0].floor,
        			"room": floorRooms[0].room,
        			"checked": false,
        			"faicon": "map-o",
        			"expanded": true,
        			"leaf": false,
        			"children": cages
        		});
    		}
    		rooms = _.sortBy(rooms, function(obj){ return obj.node;});
    		
    		floors.push({
    			"node": facilitySpaces[0].facility + '.' + facilityFloors[0].floor,
    			"type": 'floor',
    			"facilityid": facilityFloors[0].facilityid,
    			"facility": facilityFloors[0].facility,
    			"floor": facilityFloors[0].floor,
    			"checked": false,
    			"faicon": "map-o",
    			"expanded": true,
    			"leaf": false,
    			"children": rooms
    		});
    		floors = _.sortBy(floors, function(obj){ return obj.node;});
    		
		}
		facilities.push({
			"node": facilitySpaces[0].facility,
			"facilityid": facilitySpaces[0].facilityid,
			"facility": facilitySpaces[0].facility,
			"type": 'facility',
			"checked": false,
			"faicon": "map-o",
			"expanded": true,
			"leaf": false,
			"children": floors
		});
		facilities = _.sortBy(facilities, function(obj){ return obj.node;});
		
	}

	var tree = {
			"text": ".",
			"inventory": 'Spaces Notifications',
			"type": 'notifications',
			"children": facilities
	};
	return tree;

}


function powers_topology (powers){
	
	var facids = _.uniq(_.pluck(powers, 'facilityid'));
	var Facilities = [];
	for ( var i = 0; i < facids.length; i++ ) {
		
		var generators = _.filter(powers, function(arr){
			return (arr.facilityid == facids[i]);
		});
		var genids = _.uniq(_.pluck(generators, 'genid'));
		var Generators = [];
		for ( var j = 0; j < genids.length; j++ ) {
			
    		var ups = _.filter(generators, function(arr){
				return (arr.genid == genids[j]);
			});
			var upsids = _.uniq(_.pluck(ups, 'upsid'));
    		var Ups = [];
    		for ( var k = 0; k < upsids.length; k++ ) {
    			
        		var panels = _.filter(ups, function(arr){
        			return (arr.upsid == upsids[k]);
        		});
        		var pnlids = _.uniq(_.pluck(panels, 'panelid'));
        		var Panels = [];
            	for ( var l = 0; l < pnlids.length; l++ ) {
        			
            		var Powers = _.filter(panels, function(arr){
            			return (arr.panelid == pnlids[l]);
            		});
        			Panels.push({
            			"node": Powers[0].panel + ' ' + Powers[0].panel_name,
            			"nodeid": Powers[0].panelid,
            			"type": 'panel',
            			"deviceid": Powers[0].panel_dev_id,
            			"device": Powers[0].panel_dev,
            			"checked": false,
            			"faicon": "bolt",
            			"expanded": false,
            			"leaf": false,
            			"children": Powers
            		});
        			Panels = _.sortBy(Panels, function(obj){ return obj.node;});
        		}
            	Ups.push({
        			"node": panels[0].ups + ' ' + panels[0].ups_name,
        			"nodeid": panels[0].upsid,
        			"type": 'ups',
        			"deviceid": panels[0].ups_dev_id,
        			"device": panels[0].ups_dev,
        			"checked": false,
        			"faicon": "bolt",
        			"expanded": false,
        			"leaf": false,
        			"children": Panels
        		});
            	Ups = _.sortBy(Ups, function(obj){ return obj.node;});
    		}
    		Generators.push({
    			"node": ups[0].gen + ' ' + ups[0].gen_name,
    			"nodeid": ups[0].genid,
    			"type": 'generator',
    			"deviceid": ups[0].gen_dev_id,
    			"device": ups[0].gen_dev,
    			"checked": false,
    			"faicon": "bolt",
    			"expanded": true,
    			"leaf": false,
    			"children": Ups
    		});
    		Generators = _.sortBy(Generators, function(obj){ return obj.node;});
    	}
    	Facilities.push({
			"node": generators[0].facility,
			"nodeid": generators[0].facilityid,
			"type": 'facility',
			"checked": false,
			"faicon": "building",
			"expanded": true,
			"leaf": false,
			"children": Generators
		});
	}

	var tree = {
			"text": ".",
			"inventory": 'Powers Notifications',
			"type": 'notifications',
			"children": Facilities
	};
	return tree;
	
}


function managed_topology (managed){
	
	var facids = _.uniq(_.pluck(managed, 'facilityid'));
	var facilities = [];
	for ( var i = 0; i < facids.length; i++ ) {
		
		var facility = _.filter(managed, function(arr){
			return (arr.facilityid == facids[i]);
		});
		var typeids = _.uniq(_.pluck(facility, 'typeid'));
		var types = [];
		for ( var j = 0; j < typeids.length; j++ ) {
			
			var equipments = _.filter(facility, function(arr){
				return (arr.typeid == typeids[j]);
			});
			types.push({
				"node": equipments[0].typeid,
				"nodeid": equipments[0].type,
				"type": 'equipment',
				"checked": false,
				"faicon": "gears",
				"expanded": true,
				"leaf": false,
				"children": equipments
			});

		}
		facilities.push({
			"node": facility[0].facility,
			"nodeid": facility[0].facilityid,
			"type": 'facility',
			"checked": false,
			"faicon": "building",
			"expanded": true,
			"leaf": false,
			"children": types
		});
	}
	var tree = {
			"text": ".",
			"inventory": 'Managed Notifications',
			"type": 'notifications',
			"children": facilities
	};
	return tree;
	
	
}
function get_customers_nac (){
	
	var customers = [];
	var search = nlapiLoadSearch('customer','customsearch_clgx_cmd_customers_nac');
	var resultSet = search.runSearch();
	resultSet.forEachResult(function(searchResult) {
		customers.push({
			"nsid": parseInt(searchResult.getValue('internalid',null,null)),
			"nacid": parseInt(searchResult.getValue('custentity_clgx_matrix_entity_id',null,null)),
			"nsname": searchResult.getValue('entityid',null,null)
	    });
		return true;
	});
	return customers;
}


