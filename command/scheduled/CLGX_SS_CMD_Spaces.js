nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CMD_Spaces.js
//	Script Name:	CLGX_SS_CMD_Spaces
//	Script Id:		customscript_clgx_ss_cmd_spaces
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/22/2016
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_cmd_spaces(){
    try{

    	
// =================================================== All spaces =================================================== 
    	 
    	var spaces = [];
    	var filters = [];
    	while (true) {
    		search = nlapiSearchRecord('customrecord_cologix_space', 'customsearch_clgx_cmd_spaces_2',filters);
    		if (!search) { break; }
    		for (var i in search) {
    			var id = parseInt(search[i].getValue('internalid',null,'GROUP'));
    			
        		var name = (search[i].getText('location', 'CUSTRECORD_SPACE_SERVICE_ORDER', 'GROUP')).split(":") || '';
        		var location = (name[name.length-1]).trim();
    			
        		var name = (search[i].getText('custrecord_cologix_space_project', null, 'GROUP')).split(":") || '';
        		var service = (name[name.length-1]).trim();

        		var node = search[i].getValue('name', null, 'GROUP') || '';
        		var name = node.split(".");
        		
        		var floor = name[1];
        		var room = name[2];
        		var cage = name[3];
        		var row = name[4];
        		var rack = name[5];
        		var unit = name[6];
        		
        		spaces.push({
    				"nodeid": parseInt(search[i].getValue('internalid',null,'GROUP')) || 0,
    				"node": node,
    				"type": 'space',
        			"floor": floor,
        			"room": room,
        			"cage": cage,
        			"row": row,
        			"rack": rack,
        			"unit": unit,
    				"categoryid": 0,
    				"customerid": parseInt(search[i].getValue('mainname', 'CUSTRECORD_SPACE_SERVICE_ORDER', 'GROUP')) || 0,
    				"customer": search[i].getText('mainname', 'CUSTRECORD_SPACE_SERVICE_ORDER', 'GROUP') || '',
    				"locationid": parseInt(search[i].getValue('location', 'CUSTRECORD_SPACE_SERVICE_ORDER', 'GROUP')) || 0,
    				"location": location,
    				"facilityid": parseInt(search[i].getValue('custrecord_cologix_space_location', null, 'GROUP')) || 0,
    				"facility": search[i].getText('custrecord_cologix_space_location', null, 'GROUP') || '',
    				"soid": parseInt(search[i].getValue('internalid', 'CUSTRECORD_SPACE_SERVICE_ORDER', 'GROUP')) || 0,
    				"so": search[i].getValue('number', 'CUSTRECORD_SPACE_SERVICE_ORDER', 'GROUP') || '',
        			"statusid": parseInt(search[i].getValue('custrecord_cologix_space_status', null, 'GROUP')) || 0,
    				"status": search[i].getText('custrecord_cologix_space_status', null, 'GROUP') || '',
    				"serviceid": parseInt(search[i].getValue('custrecord_cologix_space_project', null, 'GROUP')) || 0,
        			"service": service,
        			"itemid": 0,
    				"item": '',
    				"iscage": 0,
        			"faicon": "map",
        			"leaf": true
        		});
    		}
    		if (search.length < 1000) { break; }
    		filters[0] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", id);
    	}
    	
/*
    	
// =================================================== All spaces =================================================== 
         	 
    	var spaces = [];
    	var filters = [];
    	while (true) {
    		search = nlapiSearchRecord('customrecord_cologix_space', 'customsearch_clgx_cmd_spaces',filters);
    		if (!search) { break; }
    		for (var i in search) {
    			var id = parseInt(search[i].getValue('internalid',null,null));
    			
        		var name = (search[i].getText('location', 'CUSTRECORD_SPACE_SERVICE_ORDER', null)).split(":") || '';
        		var location = (name[name.length-1]).trim();
    			
        		var name = (search[i].getText('custrecord_space_service_order', null, null)).split("#") || '';
        		var so = (name[name.length-1]).trim();

        		var name = (search[i].getText('custrecord_cologix_space_project', null, null)).split(":") || '';
        		var service = (name[name.length-1]).trim();

        		var node = search[i].getValue('name', null, null) || '';
        		var name = node.split(".");
        		
        		var floor = name[1];
        		var room = name[2];
        		var cage = name[3];
        		var row = name[4];
        		var rack = name[5];
        		var unit = name[6];
        		
        		spaces.push({
    				"nodeid": parseInt(search[i].getValue('internalid',null,null)) || 0,
    				"node": node,
    				"type": 'space',
        			"floor": floor,
        			"room": room,
        			"cage": cage,
        			"row": row,
        			"rack": rack,
        			"unit": unit,
    				"categoryid": 0,
    				"customerid": parseInt(search[i].getValue('mainname', 'CUSTRECORD_SPACE_SERVICE_ORDER', null)) || 0,
    				"customer": search[i].getText('mainname', 'CUSTRECORD_SPACE_SERVICE_ORDER', null) || '',
    				"locationid": parseInt(search[i].getValue('location', 'CUSTRECORD_SPACE_SERVICE_ORDER', null)) || 0,
    				"location": location,
    				"facilityid": parseInt(search[i].getValue('custrecord_cologix_space_location', null, null)) || 0,
    				"facility": search[i].getText('custrecord_cologix_space_location', null, null) || '',
    				"soid": parseInt(search[i].getValue('custrecord_space_service_order', null, null)) || 0,
    				"so": so,
        			"statusid": parseInt(search[i].getValue('custrecord_cologix_space_status', null, null)) || 0,
    				"status": search[i].getText('custrecord_cologix_space_status', null, null) || '',
    				"serviceid": parseInt(search[i].getValue('custrecord_cologix_space_project', null, null)) || 0,
        			"service": service,
        			"itemid": 0,
    				"item": '',
    				"iscage": 0,
        			"faicon": "map",
        			"leaf": true
        		});
    		}
    		if (search.length < 1000) { break; }
    		filters[0] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", id);
    	}
    	*/
    	
    	
    	/*
    	var arr = [];
    	var ids = _.uniq(_.pluck(spaces, 'serviceid'));
    	for ( var i = 0; i < ids.length; i++ ) {
    		var selection = _.filter(spaces, function(arr){
				return (arr.serviceid == ids[i]);
			});
    		arr.push({
				"customerid": selection[0].customerid,
				"locationid": selection[0].locationid,
				"location": selection[0].location,
				"soid": selection[0].soid,
				"serviceid": selection[0].serviceid,
				"count": selection.length
    		});
    	}
    	var file = nlapiCreateFile('CLGX_JSON_CMD_Spaces_Counts.json', 'PLAINTEXT', JSON.stringify(arr));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	*/
// =================================================== Services with spaces =================================================== 
    	       
    	var services = [];
    	var filters = [];
    	while (true) {
    		search = nlapiSearchRecord('transaction', 'customsearch_clgx_cmd_spaces_jobs',filters);
    		if (!search) { break; }
    		for (var i in search) {
    			var id = parseInt(search[i].getValue('custcol_clgx_so_col_service_id',null,'GROUP'));
    			var columns = search[i].getAllColumns();
    			services.push({
    				"nodeid": parseInt(search[i].getValue('custcol_clgx_so_col_service_id', null, 'GROUP')) || 0,
    				"node": search[i].getText('custcol_clgx_so_col_service_id', null, 'GROUP') || '',
    				"type": 'service',
    				"categoryid": parseInt(search[i].getValue('custcol_cologix_invoice_item_category', null, 'GROUP')) || 0,
    				"customerid": parseInt(search[i].getValue('mainname', null, 'GROUP')) || 0,
    				"customer": search[i].getText('mainname', null, 'GROUP') || '',
    				"locationid": parseInt(search[i].getValue('location', null, 'GROUP')) || 0,
    				"location": search[i].getText('location', null, 'GROUP') || '',
    				"soid": parseInt(search[i].getValue('internalid',null,'GROUP')) || 0,
    				"so": search[i].getText('internalid',null,'GROUP') || '',
    				"itemid": parseInt(search[i].getValue('item', null, 'GROUP')) || 0,
    				"item": search[i].getText('item', null, 'GROUP') || '',
    				"iscage": parseInt(search[i].getValue(columns[6])) || 0,
        			"faicon": "gear",
        			"leaf": true
        		});
    		}
    		if (search.length < 1000) { break; }
    		filters[0] = new nlobjSearchFilter("internalIdNumber", 'custcol_clgx_so_col_service_id', "greaterthan", id);
    	}
    	var file = nlapiCreateFile('CLGX_JSON_CMD_Spaces_Services.json', 'PLAINTEXT', JSON.stringify(services));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
// =================================================== Merge services in spaces =================================================== 
    	       
    	for ( var i = 0; i < spaces.length; i++ ) {
    		var service = _.find(services, function(arr){ return (arr.soid == spaces[i].soid && arr.nodeid == spaces[i].serviceid) ; });
    		if(service){
    			spaces[i].categoryid = service.categoryid;
    			spaces[i].itemid = service.itemid;
    			spaces[i].item = service.item;
    			spaces[i].iscage = service.iscage;
    		}
    	}
    	var file = nlapiCreateFile('CLGX_JSON_CMD_Spaces_All.json', 'PLAINTEXT', JSON.stringify(spaces));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);

// =================================================== Spaces Topology =================================================== 
    	
    	var tree = spaces_topology(spaces);
		
    	var file = nlapiCreateFile('CLGX_JSON_CMD_Spaces_Topology.json', 'PLAINTEXT', JSON.stringify(tree));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
// =================================================== Provisioned Spaces =================================================== 
    	
    	var provisioned = _.filter(spaces, function(arr){
    		return (arr.categoryid > 0);
    	});
    	
    	var tree = grid_to_tree ('Spaces', 'Provisioned', provisioned);
    		
    	var file = nlapiCreateFile('CLGX_JSON_CMD_Spaces_Provisioned.json', 'PLAINTEXT', JSON.stringify(tree));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
// =================================================== Misconfig Spaces ===================================================  	
    	
    	var misconfigs = _.filter(spaces, function(arr){
    		return (arr.categoryid == 0);
    	});
    	var tree = grid_to_tree ('Spaces', 'Misconfigs', misconfigs);
    	
		var file = nlapiCreateFile('CLGX_JSON_CMD_Spaces_Misconfigs.json', 'PLAINTEXT', JSON.stringify(tree));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
// =================================================== Orphans (No SO or no Job) =================================================== 
    	
    	var orphans = [];
    	var search = nlapiLoadSearch('customrecord_cologix_space', 'customsearch_clgx_cmd_spaces_orphans');
    	var result = search.runSearch();
    	result.forEachResult(function(row) {
    		
			
    		var name = (row.getText('custrecord_space_service_order', null, null)).split("#") || '';
    		var so = (name[name.length-1]).trim();

    		var name = (row.getText('custrecord_cologix_space_project', null, null)).split(":") || '';
    		var service = (name[name.length-1]).trim();

    		orphans.push({
    			"nodeid": parseInt(row.getValue('internalid',null,null)) || 0,
    			"node": row.getValue('name',null,null) || '',
    			"type": 'space',
    			"facilityid": parseInt(row.getValue('custrecord_cologix_space_location', null, null)) || 0,
    			"facility": row.getText('custrecord_cologix_space_location', null, null) || '',
    			"soid": parseInt(row.getValue('custrecord_space_service_order', null, null)) || 0,
    			"so": so,
    			"serviceid": parseInt(row.getValue('custrecord_cologix_space_project', null, null)) || 0,
    			"service": service,
    			"statusid": parseInt(row.getValue('custrecord_cologix_space_status', null, null)) || 0,
				"status": row.getText('custrecord_cologix_space_status', null, null) || '',
				"faicon": "map",
    			"leaf": true
    		});
    		return true;
    	});
    	
    	var ids = _.uniq(_.pluck(orphans, 'facilityid'));
		var facilities = [];
		for ( var i = 0; i < ids.length; i++ ) {
			var children = _.filter(orphans, function(arr){
				return (arr.facilityid == ids[i]);
			});
			if(children){
				facilities.push({
					"nodeid": children[0].facilityid,
					"node": children[0].facility,
					"type": "facility",
					"faicon": "building",
					"expanded": false,
					"leaf": false,
					"count": children.length,
					"children": children
				});
			}
		}
		var tree = {
			"text": ".",
			"inventory": 'Spaces',
			"type": "Orphans",
			"count": orphans.length,
			"children": facilities
		};
    	var file = nlapiCreateFile('CLGX_JSON_CMD_Spaces_Orphans.json', 'PLAINTEXT', JSON.stringify(tree));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
    	nlapiScheduleScript('customscript_clgx_ss_cmd_xcs', 'customdeploy_clgx_ss_cmd_xcs');
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
 
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
