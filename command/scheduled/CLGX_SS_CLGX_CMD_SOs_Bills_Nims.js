nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CLGX_CMD_SOs_Bills_Nims.js
//	Script Name:	CLGX_SS_CLGX_CMD_SOs_Bills_Nims
//	Script Id:		customscript_clgx_ss_clgx_cmd_bills_nims
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		02/210/2017
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_cmd_bills_nims(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
    	
        var context = nlapiGetContext();
        var initial = moment();
        
        var deployment = context.getDeploymentId();
    	if(deployment == 'customdeploy_clgx_ss_clgx_cmd_bill_nim_4'){
    		var fileid = 5086060;
    		var deploy = 1;
    	}
    	if(deployment == 'customdeploy_clgx_ss_clgx_cmd_bill_nim_5'){
    		var fileid = 5086061;
    		var deploy = 2;
    	}
    	
    	var file = nlapiLoadFile(fileid);
    	var arr = JSON.parse(file.getValue());
    	
    	if(arr.length > 0){
    		
    		var arrNew = arr;
    		
	    	var file = nlapiLoadFile(4830208);
	    	var spaces = JSON.parse(file.getValue());
	    	
	    	var file = nlapiLoadFile(4833030);
	    	var powers = JSON.parse(file.getValue());
	    	
	    	var file = nlapiLoadFile(4875312);
	    	var network = JSON.parse(file.getValue());
	    	
	    	var file = nlapiLoadFile(4834334);
	    	var xcs = JSON.parse(file.getValue());
	    	
	    	var file = nlapiLoadFile(4662283);
	    	var vxcs = JSON.parse(file.getValue());
	    	
	    	var batch = 200;
	    	var loop = arr.length;
	    	if(arr.length > batch){
		        loop = batch;
			}
			
	    	for ( var l = 0; arr != null && l < loop; l++ ) {
	
	    		var soid = arr[l];
	    		
	        	var inventory = {
	        			"space_bills": 0,
	        			"space_nims": 0,
	        			"power_bills": 0,
	        			"power_nims": 0,
	        			"network_bills": 0,
	        			"network_nims": 0,
	        			"xc_bills": 0,
	        			"xc_nims": 0,
	        			"vxc_bills": 0,
	        			"vxc_nims": 0
	        	};
	        	
	        	var filters = [];
	        	filters.push(new nlobjSearchFilter("internalid",null,"anyof",soid));
	        	var search = nlapiSearchRecord('transaction', 'customsearch_clgx_cmd_so_bills_nims_2', filters);
	        	var columns = search[0].getAllColumns();
				
				var spacesSO = parseInt(search[0].getValue(columns[0])) || 0;
				var powersSO = parseInt(search[0].getValue(columns[1])) || 0;
				var networkSO = parseInt(search[0].getValue(columns[2])) || 0;
				var xcsSO = parseInt(search[0].getValue(columns[3])) || 0;
				var vxcsSO = parseInt(search[0].getValue(columns[4])) || 0;
				var cage = parseInt(search[0].getValue(columns[5])) || 0;

        		// spaces
    			var selection = _.filter(spaces, function(arr){
    	    		return (arr.soid == soid);
    	    	});
    			if(selection){
    				inventory.power_nims = selection.length;
    			}
				if(cage > 0){
					inventory.space_bills = selection.length;
				} else {
					inventory.space_bills = spacesSO;
				}
        		
        		// powers
				var selection = _.filter(powers, function(arr){
    	    		return (arr.soid == soid);
    	    	});
    			if(selection){
    				inventory.power_nims = selection.length;
    			}
    			inventory.power_bills = powersSO;

        		// network
    			var selection = _.filter(network, function(arr){
    	    		return (arr.soid == soid);
    	    	});
    			if(selection){
    				inventory.network_nims = selection.length;
    			}
    			inventory.network_bills = networkSO;
        		
        		// xcs
    			var selection = _.filter(xcs, function(arr){
    	    		return (arr.soid == soid);
    	    	});
    			if(selection){
    				inventory.xc_nims = selection.length;
    			}
    			inventory.xc_bills = xcsSO;
        		
        		// vxcs
    			var columns = [];
    			var selection = _.filter(vxcs, function(arr){
    	    		return (arr.soid == soid);
    	    	});
    			if(selection){
    				inventory.vxc_nims = selection.length;
    			}
    			inventory.vxc_bills = vxcsSO;

	        	try {
	        		var rec = nlapiLoadRecord('salesorder', soid);
	        		rec.setFieldValue('custbody_clgx_cmd_network_bills', inventory.network_bills);
	        		rec.setFieldValue('custbody_clgx_cmd_network_nims', inventory.network_nims);
	        		rec.setFieldValue('custbody_clgx_cmd_power_bills', inventory.power_bills);
	        		rec.setFieldValue('custbody_clgx_cmd_power_nims', inventory.power_nims);
	        		rec.setFieldValue('custbody_clgx_cmd_space_bills', inventory.space_bills);
	        		rec.setFieldValue('custbody_clgx_cmd_space_nims', inventory.space_nims);
	        		rec.setFieldValue('custbody_clgx_cmd_vxc_bills', inventory.vxc_bills);
	        		rec.setFieldValue('custbody_clgx_cmd_vxc_nims', inventory.vxc_nims);
	        		rec.setFieldValue('custbody_clgx_cmd_xc_bills', inventory.xc_bills);
	        		rec.setFieldValue('custbody_clgx_cmd_xc_nims', inventory.xc_nims);
	        		nlapiSubmitRecord(rec, true, true);
	        	}
	        	catch (error) {
	        		
	        	}
	        	
	        	arrNew = _.reject(arrNew, function(num){
			        return (num == soid);
				});
				
				var usage =  10000 - parseInt(context.getRemainingUsage());
	        	nlapiLogExecution('DEBUG', 'SO', '| SOID = ' + soid + ' | ' + l + '/' + arr.length +  ' | Usage = '+ usage + ' |');
	        }
	    	
	    	var file = nlapiCreateFile('CLGX_JSON_CMD_SOs_IDs_'+ deploy + '.json', 'PLAINTEXT', JSON.stringify(arrNew));
	    	file.setFolder(3824960);
			nlapiSubmitFile(file);
			
			status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
    	}
    	else{
    		
    		nlapiLogExecution('DEBUG','Debug', 'No more SOs to process'); 
    		nlapiSendEmail(71418,71418,'finished ' + deployment,'finished' + deployment,null,null,null,null);
    	}

		nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

