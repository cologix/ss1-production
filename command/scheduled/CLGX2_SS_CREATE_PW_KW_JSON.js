/**
 * @NApiVersion 2.x
 * @NScriptType ScheduledScript
 * @NModuleScope SameAccount
 *
 * @author Catalina Taran
 * @date   4/6/2010
 */
define(["N/task", "N/runtime", "N/search", "N/file", "N/record","/SuiteScripts/clgx/libraries/lodash.min"],
    function(task, runtime, search, file, record,_) {
        function execute(scriptContext) {
            var results = search.load({ type: 'customrecord_clgx_power_circuit', id: "customsearch_clgx_dcim_pwrs_kw_cust_pn_2" });

        var arrPowers=new Array();
        var resultSet = results.run();


        // now take the first portion of data.
        var currentRange = resultSet.getRange({
            start : 0,
            end : 1000
        });

        var i = 0;  // iterator for all search results
        var j = 0;  // iterator for current result range 0..999

        while ( j < currentRange.length ) {
            var objPower = new Object();
            // take the result row
            var result = currentRange[j];
            //var columns = result.getAllColumns();
            objPower["facilityid"] = parseInt(result.getValue(result.columns[0]));
            objPower["facility"] = result.getText(result.columns[0]);
            objPower["panelid"] = parseInt(result.getValue(result.columns[1]));
            objPower["panel"] = result.getText(result.columns[1]);
            objPower["powerid"] = parseInt(result.getValue(result.columns[2]));
            objPower["power"] = result.getValue(result.columns[3]);
            objPower["pairid"] = parseInt(result.getValue(result.columns[4]));
            objPower["pair"] = result.getText(result.columns[4]);
            objPower["spaceid"] = parseInt(result.getValue(result.columns[5]));
            objPower["space"] = result.getText(result.columns[5]);
            objPower["serviceid"] = parseInt(result.getValue(result.columns[6]));
            objPower["service"] = _.last((result.getText(result.columns[6])).split(" : "));
            objPower["soid"] = parseInt(result.getValue(result.columns[7]));
            objPower["so"] = (result.getText(result.columns[7])).replace("Service Order #", "");
            objPower["volts"] = result.getText(result.columns[8]);
            objPower["contract"] = result.getText(result.columns[9]);
            objPower["module"] = parseInt(result.getValue(result.columns[10]));
            objPower["breaker"] = parseInt(result.getValue(result.columns[11]));
            var kwavg = parseFloat(result.getValue(result.columns[12]));
            if(kwavg > 0){
                objPower["kwavg"] = kwavg;
            }
            else{
                objPower["kwavg"] = 0;
            }

            //var objPwrUsg = _.find(arrAllPowers, function(arr){ return (arr.powerid == parseInt(searchPower.getValue(columns[2]))); });
            /*   var objPwrUsg = _.find(arrAllPowers, function(arr){ return (arr.powerid == parseInt(result.getValue(result.columns[2]))); });
               if(objPwrUsg != null){
                   objPower["amps"] = round(objPwrUsg.amps);
                   objPower["ampspair"] = round(objPwrUsg.ampspair);
                   objPower["absum"] = round(objPwrUsg.absum);
                   objPower["usage"] = round(objPwrUsg.usage);
               }
               else{*/
            objPower["amps"] = 0;
            objPower["ampspair"] = 0;
            objPower["absum"] = 0;
            objPower["usage"] = 0;
            // }
            arrPowers.push(objPower);

            // finally:
            i++; j++;
            if( j==1000 ) {   // check if it reaches 1000
                j=0;          // reset j an reload the next portion
                currentRange = resultSet.getRange({
                    start : i,
                    end : i+1000
                });
            }
        }

var x=0;

            var fileObject = file.create({
                name: "pw_kw.json",
                fileType: file.Type.PLAINTEXT,
                folder: 3824960,
                contents: JSON.stringify(arrPowers)
            });
            var fileID = fileObject.save();

            log.debug({ title: "createIDFile - fileID", details: fileID });


        }





        return {
            execute: execute
        };

    });