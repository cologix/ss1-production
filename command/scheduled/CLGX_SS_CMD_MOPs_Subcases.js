nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CMD_MOPs_Subcases.js
//	Script Name:	CLGX_SS_CMD_MOPs_Subcases
//	Script Id:		customscript_clgx_ss_cmd_mops_subcases
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		02/27/2017
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_cmd_mops_subcases(){
    try{

        var context = nlapiGetContext();
        
        var caseid = context.getSetting('SCRIPT','custscript_cmd_mops_parent_caseid');
        var userid = context.getSetting('SCRIPT','custscript_cmd_mops_parent_userid');
        
        var rec = nlapiLoadRecord('supportcase', caseid);
        var json = JSON.parse(rec.getFieldValue('custevent_clgx_case_maintenance_json'));
        
    	nlapiLogExecution('DEBUG','debug', '| caseid = ' + caseid + ' | userid = ' + userid + ' | ');
    	nlapiLogExecution('DEBUG','debug', '| json = ' + JSON.stringify( json ) + ' | ');
        
        var casenumber = rec.getFieldValue('casenumber');
	    var customer = rec.getFieldValue('company');
	    var title = rec.getFieldValue('title');
	    var assigned = rec.getFieldText('assigned');
        
    	var subsidiary = rec.getFieldValue('subsidiary');
    	var facility = rec.getFieldValue('custevent_cologix_facility');
    	var facilityname = rec.getFieldText('custevent_cologix_facility');
    	var profile = rec.getFieldValue('profile');
    	
    	var casetype = rec.getFieldValue('category');
    	var casetypename = rec.getFieldText('category');
	    var subcasetype = rec.getFieldValue('custevent_cologix_sub_case_type');
	    
	    var priority = rec.getFieldText('priority');
	    var status = rec.getFieldText('status');
	    
        var notify = rec.getFieldValue('custevent_clgx_customer_notification');
	    var followup = rec.getFieldValue('custevent_cologix_case_sched_followup');
    	var starttime = rec.getFieldValue('custevent_cologix_sched_start_time');
    	
    	
    	if(status == 'Escalated'){
    		var subcasestatus = 'In Progress';
    	}
    	else{
    		subcasestatus = status;
    	}
    	if(status == 'CAB Approved'){
    		if(facilityname.indexOf("MTL") > -1){
    			assigned = 'Customer Care-MTL';
				nlapiSubmitField('supportcase', caseid, 'assigned', 317218);
    		}
    		else{
    			assigned = 'Customer Care-Non MTL';
    			nlapiSubmitField('supportcase', caseid, 'assigned', 317219);
    		}
    	}
    	if(priority == 'Urgent'){
    		priority = 'Medium';
    	}

    	if(json != null){
    		
	    	if(json.type == 'spaces'){
	    		var tree = get_spaces_mops (json);
	        } 
	    	if(json.type == 'equipments'){
	    		var tree = get_equipments_mops (json);
	        }
	    	if(json.type == 'powers'){
	    		var tree = get_powers_mops (json);
	        }
	    	
	    	var cases = tree.children[0].children;
	    	
	    	for ( var i = 0; i < cases.length; i++ ) {
	    		
	    		
	        	
	        	var casesubsidiary = nlapiLookupField('customer', cases[i].customerid, 'subsidiary');
	    		
				var record = nlapiCreateRecord('supportcase');
				var listEmails = '';
	        	if(cases[i].emails != null){
					listEmails = cases[i].emails.join();
					record.setFieldValue('custevent_acs_custom_email', listEmails);
	        	}
	        	if(listEmails.length > 300){
	        		while (listEmails.length > 300) {
	        			cases[i].emails.pop();
	        			listEmails = cases[i].emails.join();
	        		}
	        	}
	            record.setFieldValue('title', 'Childcase of ' + casenumber + ' - ' + casetypename + ' at ' + facilityname);
	            record.setFieldValue('company', cases[i].customerid);
	            record.setFieldValue('custevent_cologix_facility', facility);
	            record.setFieldValue('subsidiary', casesubsidiary);
	            if(assigned != null && assigned != ''){
	            	record.setFieldText('assigned', assigned);
	            }
	            
				record.setFieldValue('email', listEmails);
				
				var get_email = record.getFieldValue('email');
            //----------------------------------//
            var get_email_spillover = record.getFieldValue('custevent_acs_custom_email');
            if(get_email_spillover != null){
            
            
            var get_email_array = new Array();
            var get_email_spillover_array = new Array();
            get_email_array = get_email.split(',');
            get_email_spillover_array = get_email_spillover.split(',');

            nlapiLogExecution('DEBUG','--- get_email_spillover---', get_email_spillover)
            if(get_email_array.length == get_email_spillover_array.length){
                nlapiLogExecution('DEBUG','--- THERE IS NO SPILL OVER EMAIL ---')
                record.setFieldValue('custevent_acs_custom_email', ''); // use all emails if multiple contacts
            }else{
                nlapiLogExecution('DEBUG','--- THERE IS SPILL OVER EMAIL ---')
                get_email_spillover_array = get_email_spillover_array.filter(function(val) {
                    return get_email_array.indexOf(val) == -1;
				  });
				  
				//------------------Jedi Changes----------------//
                // for (var i = get_email_spillover_array.length - 1; i >= 0; i--) {
                //     for (var j = 0; j < get_email_array.length; j++) {
                //       if (get_email_spillover_array[i] === get_email_array[j]) {
                //         get_email_spillover_array.splice(i, 1);
                //         }
                //       }
                //     }
                record.setFieldValue('custevent_acs_custom_email', get_email_spillover_array.join()); // use all emails if multiple contacts
               
                //----------------------------------//
            }
            
            
            }
            
	            record.setFieldText('status', subcasestatus);
	            record.setFieldText('priority', priority);
	            
	            record.setFieldValue('category', casetype);
	            record.setFieldValue('custevent_cologix_sub_case_type', subcasetype);
	            
	            record.setFieldValue('custevent_cologix_case_sched_followup', followup);
	            record.setFieldValue('custevent_cologix_sched_start_time', starttime);
	            
	            record.setFieldValue('custevent_clgx_parent_id', caseid);
	            record.setFieldValue('origin', 6);
	            var idRec = nlapiSubmitRecord(record, false, true);
	
	            var index = i +1;
				var usage = 10000 - parseInt(context.getRemainingUsage());
	            
	            nlapiLogExecution('DEBUG','debug', '| Case = ' + index + ' / ' + cases.length + ' | Customer = ' + customer + ' | Usage - '+ usage + '  |');
	    		
	    	}
	    	
	    	nlapiSubmitField('supportcase', caseid, 'custevent_clgx_locked_case', 'F'); 
	    	
	    } else {
	    	nlapiSubmitField('supportcase', caseid, 'custevent_clgx_locked_case', 'F'); 
	    }
    	
    	nlapiSendEmail(432742,71418,'All subcases for case ' + casenumber + ' - ' + casetypename + ' at ' + facilityname + ' were created.','',null,null,null,null);
    	
    }
    catch (error){
    	
    	nlapiSubmitField('supportcase', caseid, 'custevent_clgx_locked_case', 'F');
    	
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

