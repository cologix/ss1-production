//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CMD_Managed_IPs_2.js
//	Script Name:	CLGX_SS_CMD_Managed_IPs_2
//	Script Id:		customscript_clgx_ss_cmd_managed_ips_2
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		02/20/2017
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_cmd_managed_ips_2(){
    try{


	    var file = nlapiLoadFile(5173116);
    	var arr = JSON.parse(file.getValue());
    	var customers = get_customers_nac ();

    	var ids = _.uniq(_.pluck(arr, 'client_id'));
    	var clients = [];
    	for ( var i = 0; i < ids.length; i++ ) {
    		
    		var selection = _.filter(arr, function(arr){
				return (arr.client_id == ids[i]);
			});
    		
    		var nsid = 0;
			var nsname = '';
			var customer = _.find(customers, function(arr){ return (arr.nacid == ids[i]) ; });
			if(customer){
	    		nsid = customer.nsid;
	    		nsname = customer.nsname;
			}
			if(nsname == ''){
				nsname = selection[0].client_id;
			}
				
    		clients.push({
				"node": nsname,
				"nodeid": selection[0].client_id,
				"type": 'customer',
				"nsid": nsid,
				"nsname": nsname,
				"faicon": 'user',
    			"expanded": false,
    			"leaf": false,
    			"children":selection
            });
    	}
    	clients = _.sortBy(clients, function(obj){ return obj.node;});
    	
    	var tree = {
				"node": '.',
				"type": 'ips',
    			"children":clients
        };

    	var file = nlapiCreateFile('CLGX_JSON_CMD_Managed_IPs.json', 'PLAINTEXT', JSON.stringify(tree));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

