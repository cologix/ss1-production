nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CMD_Powers.js
//	Script Name:	CLGX_SS_CMD_Powers
//	Script Id:		customscript_clgx_ss_cmd_powers
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/22/2016
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_cmd_powers(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        var context = nlapiGetContext();
    	var environment = context.getEnvironment();
        
// =================================================== All powers =================================================== 
    	if(environment == 'PRODUCTION'){
    		
	    	var powers = [];
	    	var filters = [];
	    	while (true) {
	    		search = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_cmd_powers',filters);
	    		if (!search) { break; }
	    		for (var i in search) {
	    			var id = parseInt(search[i].getValue('internalid',null,null));
	    			
	    			var name = (search[i].getText('location', 'CUSTRECORD_POWER_CIRCUIT_SERVICE_ORDER', null)).split(":") || '';
	        		var location = (name[name.length-1]).trim();
	    			
	        		var name = (search[i].getText('custrecord_power_circuit_service_order', null, null)).split("#") || '';
	        		var so = (name[name.length-1]).trim();
	
	        		var name = (search[i].getText('custrecord_cologix_power_service', null, null)).split(":") || '';
	        		var service = (name[name.length-1]).trim();
	
	        		powers.push({
	    				"nodeid": parseInt(search[i].getValue('internalid',null,null)) || 0,
	    				"node": search[i].getValue('name',null,null) || '',
	    				"type": 'power',
	    				"categoryid": 0,
	    				
	    				"customerid": parseInt(search[i].getValue('mainname', 'CUSTRECORD_POWER_CIRCUIT_SERVICE_ORDER', null)) || 0,
	    				"customer": search[i].getText('mainname', 'CUSTRECORD_POWER_CIRCUIT_SERVICE_ORDER', null) || '',
	    				
	    				"locationid": parseInt(search[i].getValue('location', 'CUSTRECORD_POWER_CIRCUIT_SERVICE_ORDER', null)) || 0,
	    				"location": location,
	    				
	    				"facilityid": parseInt(search[i].getValue('custrecord_cologix_power_facility', null, null)) || 0,
	    				"facility": search[i].getText('custrecord_cologix_power_facility', null, null) || '',
	    				
	    				"spaceid": parseInt(search[i].getValue('custrecord_cologix_power_space', null, null)) || 0,
	    				"space": search[i].getText('custrecord_cologix_power_space', null, null) || '',
	    				
	    				"soid": parseInt(search[i].getValue('custrecord_power_circuit_service_order', null, null)) || 0,
	        			"so": so,
	        			
	        			"serviceid": parseInt(search[i].getValue('custrecord_cologix_power_service', null, null)) || 0,
	        			"service": service,
	        			
	        			"genid": parseInt(search[i].getValue('custrecord_clgx_power_generator', null, null)) || 0,
	    				"gen": search[i].getText('custrecord_clgx_power_generator', null, null) || '',
	    				"gen_name": search[i].getValue('altname', 'custrecord_clgx_power_generator', null) || '',
	    				"gen_dev_id": parseInt(search[i].getValue('custrecord_clgx_od_fam_device', 'custrecord_clgx_power_generator', null)) || 0,
	    				"gen_dev": search[i].getText('custrecord_clgx_od_fam_device', 'custrecord_clgx_power_generator', null) || '',
	    				
	    				"upsid": parseInt(search[i].getValue('custrecord_cologix_power_ups_rect', null, null)) || 0,
	    				"ups": search[i].getText('custrecord_cologix_power_ups_rect', null, null) || '',
	    				"ups_name": search[i].getValue('altname', 'custrecord_cologix_power_ups_rect', null) || '',
	    				"ups_dev_id": parseInt(search[i].getValue('custrecord_clgx_od_fam_device', 'custrecord_cologix_power_ups_rect', null)) || 0,
	    				"ups_dev": search[i].getText('custrecord_clgx_od_fam_device', 'custrecord_cologix_power_ups_rect', null) || '',
	    				
	    				"panelid": parseInt(search[i].getValue('custrecord_clgx_power_panel_pdpm', null, null)) || 0,
	    				"panel": search[i].getText('custrecord_clgx_power_panel_pdpm', null, null) || '',
	    				"panel_name": search[i].getValue('altname', 'custrecord_clgx_power_panel_pdpm', null) || '',
	    				"panel_dev_id": parseInt(search[i].getValue('custrecord_clgx_dcim_device', null, null)) || 0,
	    				"panel_dev": search[i].getText('custrecord_clgx_dcim_device', null, null) || '',
	    				
	    				"itemid": 0,
	    				"item": '',
	        			"faicon": "plug",
	        			"leaf": true
	        		});
	    		}
	    		if (search.length < 1000) { break; }
	    		filters[0] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", id);
	    	}
	    	
	    	var arr = [];
	    	var ids = _.uniq(_.pluck(powers, 'serviceid'));
	    	for ( var i = 0; i < ids.length; i++ ) {
	    		var selection = _.filter(powers, function(arr){
					return (arr.serviceid == ids[i]);
				});
	    		arr.push({
					"customerid": selection[0].customerid,
					"locationid": selection[0].locationid,
					"location": selection[0].location,
					"soid": selection[0].soid,
					"serviceid": selection[0].serviceid,
					"count": selection.length
	    		});
	    	}
	    	var file = nlapiCreateFile('CLGX_JSON_CMD_Powers_Counts.json', 'PLAINTEXT', JSON.stringify(arr));
	    	file.setFolder(3824960);
	    	nlapiSubmitFile(file);
	    	
	// =================================================== Services with powers =================================================== 
	    	
	    	var services = [];
	    	var filters = [];
	    	while (true) {
	    		search = nlapiSearchRecord('transaction', 'customsearch_clgx_cmd_powers_jobs',filters);
	    		if (!search) { break; }
	    		for (var i in search) {
	    			var id = parseInt(search[i].getValue('custcol_clgx_so_col_service_id',null,'GROUP'));
	    			var columns = search[i].getAllColumns();
	    			services.push({
	    				"serviceid": parseInt(search[i].getValue('custcol_clgx_so_col_service_id', null, 'GROUP')) || 0,
	    				"soid": parseInt(search[i].getValue('internalid',null,'GROUP')) || 0,
	    				"categoryid": parseInt(search[i].getValue('custcol_cologix_invoice_item_category', null, 'GROUP')) || 0,
	    				"itemid": parseInt(search[i].getValue('item', null, 'GROUP')) || 0,
	    				"item": search[i].getText('item', null, 'GROUP') || ''
	        		});
	    		}
	    		if (search.length < 1000) { break; }
	    		filters[0] = new nlobjSearchFilter("internalIdNumber", 'custcol_clgx_so_col_service_id', "greaterthan", id);
	    	}
	    	var file = nlapiCreateFile('CLGX_JSON_CMD_Powers_Services.json', 'PLAINTEXT', JSON.stringify(services));
	    	file.setFolder(3824960);
	    	nlapiSubmitFile(file);
	
	// =================================================== Merge services in powers =================================================== 
	    	    
	    	for ( var i = 0; i < powers.length; i++ ) {
	    		var service = _.find(services, function(arr){ return (arr.soid == powers[i].soid && arr.serviceid == powers[i].serviceid) ; });
	    		if(service){
	    			powers[i].categoryid = service.categoryid;
	    			powers[i].itemid = service.itemid;
	    			powers[i].item = service.item;
	    		}
	    	}
	    	var file = nlapiCreateFile('CLGX_JSON_CMD_Powers_All.json', 'PLAINTEXT', JSON.stringify(powers));
	    	file.setFolder(3824960);
	    	nlapiSubmitFile(file);
	
	// =================================================== Powers Topology =================================================== 
	    	
	    	var tree = powers_topology (powers);
			
	    	var file = nlapiCreateFile('CLGX_JSON_CMD_Powers_Topology.json', 'PLAINTEXT', JSON.stringify(tree));
	    	file.setFolder(3824960);
	    	nlapiSubmitFile(file);
	    	
	// =================================================== Provisioned Powers =================================================== 
	    		
	    	var provisioned = _.filter(powers, function(arr){
	    		return (arr.categoryid > 0);
	    	});
	    	var tree = grid_to_tree ('Powers', 'Provisioned', provisioned);
			
	    	var file = nlapiCreateFile('CLGX_JSON_CMD_Powers_Provisioned.json', 'PLAINTEXT', JSON.stringify(tree));
	    	file.setFolder(3824960);
	    	nlapiSubmitFile(file);
	    	
	// =================================================== Miss configs (No match SO + Job) ===================================================  	
	    	    
	    	var misconfigs = _.filter(powers, function(arr){
	    		return (arr.categoryid == 0);
	    	});
	    	var tree = grid_to_tree ('Powers', 'Misconfigs', misconfigs);
	    	
			var file = nlapiCreateFile('CLGX_JSON_CMD_Powers_Misconfigs.json', 'PLAINTEXT', JSON.stringify(tree));
	    	file.setFolder(3824960);
	    	nlapiSubmitFile(file);
	    	
	// =================================================== Orphans (No SO or no Job) ===================================================   	
	    	
	    	var orphans = [];
	    	var search = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_cmd_powers_orphans');
	    	var result = search.runSearch();
	    	result.forEachResult(function(row) {
				
	    		var name = (row.getText('custrecord_power_circuit_service_order', null, null)).split("#") || '';
	    		var so = (name[name.length-1]).trim();
	
	    		var name = (row.getText('custrecord_cologix_power_service', null, null)).split(":") || '';
	    		var service = (name[name.length-1]).trim();
	
	    		orphans.push({
	    			"nodeid": parseInt(row.getValue('internalid',null,null)) || 0,
	    			"node": row.getValue('name',null,null) || '',
	    			"type": 'power',
	    			"facilityid": parseInt(row.getValue('custrecord_cologix_power_facility', null, null)) || 0,
	    			"facility": row.getText('custrecord_cologix_power_facility', null, null) || '',
	    			"soid": parseInt(row.getValue('custrecord_power_circuit_service_order', null, null)) || 0,
	    			"so": so,
	    			"serviceid": parseInt(row.getValue('custrecord_cologix_power_service', null, null)) || 0,
	    			"service": service,
	    			"faicon": "plug",
	    			"leaf": true
	    		});
	    		return true;
	    	});
	    	
	    	var ids = _.uniq(_.pluck(orphans, 'facilityid'));
			var facilities = [];
			for ( var i = 0; i < ids.length; i++ ) {
				var children = _.filter(orphans, function(arr){
					return (arr.facilityid == ids[i]);
				});
				if(children){
					facilities.push({
						"nodeid": children[0].facilityid,
						"node": children[0].facility,
						"type": "facility",
						"faicon": "building",
						"expanded": false,
						"leaf": false,
						"count": children.length,
						"children": children
					});
				}
			}
			var tree = {
				"text": ".",
				"inventory": 'Powers',
				"type": "Orphans",
				"count": orphans.length,
				"children": facilities
			};
	    	var file = nlapiCreateFile('CLGX_JSON_CMD_Powers_Orphans.json', 'PLAINTEXT', JSON.stringify(tree));
	    	file.setFolder(3824960);
	    	nlapiSubmitFile(file);
	    	
	    	
	    	nlapiScheduleScript('customscript_clgx_ss_cmd_spaces', 'customdeploy_clgx_ss_cmd_spaces');
	    	nlapiScheduleScript('customscript_clgx_ss_cmd_managed', 'customdeploy_clgx_ss_cmd_managed');
	    	
	    	/*
	    	// managed
	    	nlapiScheduleScript('customscript_clgx_ss_cmd_managed_15min', 'customdeploy_clgx_ss_cmd_managed_15min');
	    	nlapiScheduleScript('customscript_clgx_ss_cmd_managed_backups', 'customdeploy_clgx_ss_cmd_managed_backups');
	    	nlapiScheduleScript('customscript_clgx_ss_cmd_managed_ipmis', 'customdeploy_clgx_ss_cmd_managed_ipmis');
	    	nlapiScheduleScript('customscript_clgx_ss_cmd_managed_servers', 'customdeploy_clgx_ss_cmd_managed_servers');
	    	nlapiScheduleScript('customscript_clgx_ss_cmd_managed_devices', 'customdeploy_clgx_ss_cmd_managed_devices');
	    	
	    	nlapiScheduleScript('customscript_clgx_ss_cmd_managed_ips', 'customdeploy_clgx_ss_cmd_managed_ips');
	    	nlapiScheduleScript('customscript_clgx_ss_cmd_managed_domains', 'customdeploy_clgx_ss_cmd_managed_domains');
	    	nlapiScheduleScript('customscript_clgx_ss_cmd_managed_ports', 'customdeploy_clgx_ss_cmd_managed_ports');
	    	*/
	    	
	    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    	}
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

