nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CMD_XCs.js
//	Script Name:	CLGX_SS_CMD_XCs
//	Script Id:		customscript_clgx_ss_cmd_xcs
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/22/2016
//-------------------------------------------------------------------------------------------------

function scheduled_clgx_ss_cmd_xcs(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        var context = nlapiGetContext();
    	
// =================================================== All XCs =================================================== 
          
        var xcs = [];
    	var filters = new Array();
    	while (true) {
    		search = nlapiSearchRecord('customrecord_cologix_crossconnect', 'customsearch_clgx_cmd_xcs', filters);
    		if (!search) { break; }
    		for (var i in search) {
    			var id = parseInt(search[i].getValue('internalid',null,null));
       			
    			var name = (search[i].getText('location', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', null)).split(":") || '';
        		var location = (name[name.length-1]).trim();
    			
        		var name = (search[i].getText('custrecord_xconnect_service_order', null, null)).split("#") || '';
        		var so = (name[name.length-1]).trim();

        		var name = (search[i].getText('custrecord_cologix_xc_service', null, null)).split(":") || '';
        		var service = (name[name.length-1]).trim();

    			xcs.push({
        			"nodeid": parseInt(search[i].getValue('internalid',null,null)) || 0,
    				"node": search[i].getValue('name',null,null) || '',
    				"type": 'xc',
    				"categoryid": 0,
    				"customerid": parseInt(search[i].getValue('mainname', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', null)) || 0,
    				"customer": search[i].getText('mainname', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', null) || '',
    				"locationid": parseInt(search[i].getValue('location', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', null)) || 0,
    				"location": location,
    				"facilityid": parseInt(search[i].getValue('custrecord_clgx_xc_facility', null, null)) || 0,
    				"facility": search[i].getText('custrecord_clgx_xc_facility', null, null) || '',
    				"soid": parseInt(search[i].getValue('custrecord_xconnect_service_order', null, null)) || 0,
        			"so": so,
        			"serviceid": parseInt(search[i].getValue('custrecord_cologix_xc_service', null, null)) || 0,
        			"service": service,
        			"carrierid": parseInt(search[i].getValue('custrecord_cologix_carrier_name', null, null)) || 0,
        			"carrier": search[i].getText('custrecord_cologix_carrier_name', null, null) || '',
    				"itemid": 0,
    				"item": '',
        			"faicon": "share-alt-square",
        			"leaf": true
        		});
    		}
    		if (search.length < 1000) { break; }
    		filters[0] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", id);
    	}
    	
// =================================================== Services with xcs =================================================== 
    	    	
    	var services = [];
    	var filters = new Array();
    	while (true) {
    		search = nlapiSearchRecord('transaction', 'customsearch_clgx_cmd_xcs_jobs', filters);
    		if (!search) { break; }
    		for (var i in search) {
    			var id = parseInt(search[i].getValue('custcol_clgx_so_col_service_id',null,'GROUP'));
    			var columns = search[i].getAllColumns();
    			services.push({
    				"serviceid": parseInt(search[i].getValue('custcol_clgx_so_col_service_id', null, 'GROUP')) || 0,
    				"soid": parseInt(search[i].getValue('internalid',null,'GROUP')) || 0,
    				"categoryid": parseInt(search[i].getValue('custcol_cologix_invoice_item_category', null, 'GROUP')) || 0,
    				"itemid": parseInt(search[i].getValue('item', null, 'GROUP')) || 0,
    				"item": search[i].getText('item', null, 'GROUP') || ''
        		});
    		}
    		if (search.length < 1000) { break; }
    		filters[0] = new nlobjSearchFilter("internalIdNumber", 'custcol_clgx_so_col_service_id', "greaterthan", id);
    	}
    	var file = nlapiCreateFile('CLGX_JSON_CMD_XCs_Services.json', 'PLAINTEXT', JSON.stringify(services));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);

// =================================================== Merge services in xcs =================================================== 
    	    	
    	for ( var i = 0; i < xcs.length; i++ ) {
    		var service = _.find(services, function(arr){ return (arr.soid == xcs[i].soid && arr.serviceid == xcs[i].serviceid) ; });
    		if(service){
    			xcs[i].categoryid = service.categoryid;
    			xcs[i].itemid = service.itemid;
    			xcs[i].item = service.item;
    		}
    	}

// =================================================== Provisioned XCs =================================================== 
		
    	var provisioned = _.filter(xcs, function(arr){
    		return (arr.categoryid == 4);
    	});
    	
    	var file = nlapiCreateFile('CLGX_JSON_CMD_XCs_All.json', 'PLAINTEXT', JSON.stringify(provisioned));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
    	var arr = [];
    	var ids = _.uniq(_.pluck(provisioned, 'serviceid'));
    	for ( var i = 0; i < ids.length; i++ ) {
    		var selection = _.filter(provisioned, function(arr){
				return (arr.serviceid == ids[i]);
			});
    		arr.push({
				"customerid": selection[0].customerid,
				"locationid": selection[0].locationid,
				"location": selection[0].location,
				"soid": selection[0].soid,
				"serviceid": selection[0].serviceid,
				"count": selection.length
    		});
    	}
    	var file = nlapiCreateFile('CLGX_JSON_CMD_XCs_Counts.json', 'PLAINTEXT', JSON.stringify(arr));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
    	var tree = grid_to_tree ('XCs', 'Provisioned', provisioned);
		
    	var file = nlapiCreateFile('CLGX_JSON_CMD_XCs_Provisioned.json', 'PLAINTEXT', JSON.stringify(tree));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
// =================================================== Provisioned Network =================================================== 
		
    	var provisioned = _.filter(xcs, function(arr){
    		return (arr.categoryid == 5);
    	});
    	
    	var file = nlapiCreateFile('CLGX_JSON_CMD_Network_All.json', 'PLAINTEXT', JSON.stringify(provisioned));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
    	var arr = [];
    	var ids = _.uniq(_.pluck(provisioned, 'serviceid'));
    	for ( var i = 0; i < ids.length; i++ ) {
    		var selection = _.filter(provisioned, function(arr){
				return (arr.serviceid == ids[i]);
			});
    		arr.push({
				"customerid": selection[0].customerid,
				"locationid": selection[0].locationid,
				"location": selection[0].location,
				"soid": selection[0].soid,
				"serviceid": selection[0].serviceid,
				"count": selection.length
    		});
    	}
    	var file = nlapiCreateFile('CLGX_JSON_CMD_Network_Counts.json', 'PLAINTEXT', JSON.stringify(arr));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
    	var tree = grid_to_tree ('Network', 'Provisioned', provisioned);
		
    	var file = nlapiCreateFile('CLGX_JSON_CMD_Network_Provisioned.json', 'PLAINTEXT', JSON.stringify(tree));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
    	
// =================================================== Miss configs =================================================== 
		
    	var misconfigs = _.filter(xcs, function(arr){
    		return (arr.categoryid == 0);
    	});
    	var tree = grid_to_tree ('XCs', 'Misconfigs', misconfigs);
		
    	var file = nlapiCreateFile('CLGX_JSON_CMD_XCs_Misconfigs.json', 'PLAINTEXT', JSON.stringify(tree));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
// =================================================== Orphans (No SO or no Job) ===================================================   	
    	    	
    	var orphans = [];
    	var search = nlapiLoadSearch('customrecord_cologix_crossconnect', 'customsearch_clgx_cmd_xcs_orphans');
    	var result = search.runSearch();
    	result.forEachResult(function(row) {
			
    		var name = (row.getText('custrecord_xconnect_service_order', null, null)).split("#") || '';
    		var so = (name[name.length-1]).trim();

    		var name = (row.getText('custrecord_cologix_xc_service', null, null)).split(":") || '';
    		var service = (name[name.length-1]).trim();

    		orphans.push({
    			"nodeid": parseInt(row.getValue('internalid',null,null)) || 0,
    			"node": row.getValue('name',null,null) || '',
    			"type": 'xc',
    			"facilityid": parseInt(row.getValue('custrecord_clgx_xc_facility', null, null)) || 0,
    			"facility": row.getText('custrecord_clgx_xc_facility', null, null) || '',
    			"soid": parseInt(row.getValue('custrecord_xconnect_service_order', null, null)) || 0,
    			"so": so,
    			"serviceid": parseInt(row.getValue('custrecord_cologix_xc_service', null, null)) || 0,
    			"service": service,
    			"faicon": "share-alt-square",
    			"leaf": true
    		});
    		return true;
    	});

    	var ids = _.uniq(_.pluck(orphans, 'facilityid'));
		var facilities = [];
		for ( var i = 0; i < ids.length; i++ ) {
			var children = _.filter(orphans, function(arr){
				return (arr.facilityid == ids[i]);
			});
			if(children){
				facilities.push({
					"nodeid": children[0].facilityid,
					"node": children[0].facility,
					"type": "facility",
					"faicon": "building",
					"expanded": false,
					"leaf": false,
					"count": children.length,
					"children": children
				});
			}
		}
		var tree = {
			"text": ".",
			"inventory": 'XCs',
			"type": "Orphans",
			"count": orphans.length,
			"children": facilities
		};
    	var file = nlapiCreateFile('CLGX_JSON_CMD_XCs_Orphans.json', 'PLAINTEXT', JSON.stringify(tree));
    	file.setFolder(3824960);
    	nlapiSubmitFile(file);
    	
    	nlapiScheduleScript('customscript_clgx_ss_cmd_vxcs', 'customdeploy_clgx_ss_cmd_vxcs');
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
 
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
