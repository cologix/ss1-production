nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Power_Chain.js
//	Script Name:	CLGX_SL_CMD_Power_Chain
//	Script Id:		customscript_clgx_sl_cmd_power_chain
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/28/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=731&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_power_chain (request, response){
	try {
		
		var arrStrRoot = request.getParameter('root').split(',');
		var arrRequestIDs = new Array();
		for ( var i = 0; arrStrRoot != null && i < arrStrRoot.length; i++ ) {
			arrRequestIDs.push(parseInt(arrStrRoot[i]));
		}
		
		var objFile = nlapiLoadFile(4482933);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{dataChart}','g'), get_org_chart (arrRequestIDs));
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function get_org_chart (arrRequestIDs){
	// get all children devices for the requested root devices
	var arrDevices = get_devices (arrRequestIDs);
	var arrDevicesIDs = _.compact(_.pluck(arrDevices, 'deviceid'));
	
	// construct the orgChart
	var arrData = new Array();
	var arrNode = new Array();
    arrNode[0] = 'Child';
    arrNode[1] = 'Parent';
    arrData.push(arrNode);

	for ( var i = 0; arrDevices != null && i < arrDevices.length; i++ ) {
		var arrNode = new Array();
	    arrNode[0] = get_node_html (arrDevices[i]);
	    var objParent = _.find(arrDevices, function(arr){ return (arr.deviceid == arrDevices[i].parentid); });
	    if(objParent != null){
	    	arrNode[1] = get_node_html (objParent);
	    }
	    else{
	    	arrNode[1] = '';
	    }
	    arrData.push(arrNode);
	}
	return JSON.stringify(arrData);
}

function get_devices (arrRequestIDs){

	var arrDevices = new Array();
	var searchDevices = nlapiLoadSearch('customrecord_clgx_dcim_devices', 'customsearch_clgx_dcim_rpt_cap_devices');
	var resultSet = searchDevices.runSearch();
	resultSet.forEachResult(function(searchResult) {
		var columns = searchResult.getAllColumns();
		var objDevice = new Object();
		objDevice["parentid"] = parseInt(searchResult.getValue('custrecord_clgx_dcim_device_parent',null,null));
		objDevice["nodeid"] = parseInt(searchResult.getValue('internalid',null,null));
		arrDevices.push(objDevice);
		return true;
	});
	var arrDevicesIDs = _.pluck(arrDevices, 'nodeid');

	// extract only the requested tree nodes ---------------------------------------------------------------------------------------------------------------------------------
	var arrIDs = new Array();
	var arrRootNodes = _.filter(arrDevices, function(arr){
        return (arrRequestIDs.indexOf(arr.nodeid) > -1);
	});
	for ( var i = 0; arrRootNodes != null && i < arrRootNodes.length; i++ ) {
		arrIDs.push(arrRootNodes[i].nodeid);
		var arrNodes1 = _.filter(arrDevices, function(arr){
	        return (arr.parentid === arrRootNodes[i].nodeid);
		});
		for ( var j = 0; arrNodes1 != null && j < arrNodes1.length; j++ ) {
			arrIDs.push(arrNodes1[j].nodeid);
			var arrNodes2 = _.filter(arrDevices, function(arr){
    	        return (arr.parentid === arrNodes1[j].nodeid);
    		});
    		for ( var k = 0; arrNodes2 != null && k < arrNodes2.length; k++ ) {
    			arrIDs.push(arrNodes2[k].nodeid);
    			var arrNodes3 = _.filter(arrDevices, function(arr){
        	        return (arr.parentid === arrNodes2[k].nodeid);
        		});
        		for ( var l = 0; arrNodes3 != null && l < arrNodes3.length; l++ ) {
        			arrIDs.push(arrNodes3[l].nodeid);
        			var arrNodes4 = _.filter(arrDevices, function(arr){
            	        return (arr.parentid === arrNodes3[l].nodeid);
            		});
            		for ( var m = 0; arrNodes4 != null && m < arrNodes4.length; m++ ) {
            			arrIDs.push(arrNodes4[m].nodeid);
            			var arrNodes5 = _.filter(arrDevices, function(arr){
                	        return (arr.parentid === arrNodes4[m].nodeid);
                		});
                		for ( var n = 0; arrNodes5 != null && n < arrNodes5.length; n++ ) {
                			arrIDs.push(arrNodes5[n].nodeid);
                			/*
                			var arrNodes6 = _.filter(arrDevices, function(arr){
                    	        return (arr.parentid === arrNodes5[n].nodeid);
                    		});
                    		for ( var o = 0; arrNodes6 != null && o < arrNodes6.length; n++ ) {
                    			arrIDs.push(arrNodes6[o].nodeid);
                    			var arrNodes7 = _.filter(arrDevices, function(arr){
                        	        return (arr.parentid === arrNodes6[o].nodeid);
                        		});
                        		for ( var p = 0; arrNodes7 != null && p < arrNodes7.length; n++ ) {
                        			arrIDs.push(arrNodes7[p].nodeid);
                        			
                        			// add more levels here
                        			
                        		}
                    		}
                    		*/
                		}
            		}
        		}
    		}
		}
	}
	
	// build and return the devices grid array ---------------------------------------------------------------------------------------------------------------------------------
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('name',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_parent',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_pair',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_virtual',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_fam',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_category',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_panel_type','custrecord_clgx_dcim_device_fam',null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_avg','custrecord_clgx_dcim_device_load',null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_load_ow',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_points_day_avg','custrecord_clgx_dcim_device_current',null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_current_ow',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_capacity',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_failover',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_failover_ow',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_has_powers',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_day_kw_a',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_day_kw_ab',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_pwr_chains',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrIDs));
	arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	var searchDevices = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
	
	var arrDevices = new Array();
	for ( var i = 0; searchDevices != null && i < searchDevices.length; i++ ) {
		
		var deviceid = parseInt(searchDevices[i].getValue('internalid',null,null));
		
		var objDevice = new Object();
		objDevice["index"] = i;
		objDevice["parentid"] = parseInt(searchDevices[i].getValue('custrecord_clgx_dcim_device_parent',null,null));
		objDevice["parent"] = searchDevices[i].getText('custrecord_clgx_dcim_device_parent',null,null);
		objDevice["deviceid"] = deviceid;
		objDevice["device"] = searchDevices[i].getValue('name',null,null);
		objDevice["pairid"] = parseInt(searchDevices[i].getValue('custrecord_clgx_dcim_device_pair',null,null));
		objDevice["pair"] = searchDevices[i].getText('custrecord_clgx_dcim_device_pair',null,null);
		objDevice["virtual"] = searchDevices[i].getValue('custrecord_clgx_dcim_device_virtual',null,null);
		objDevice["famid"] = parseInt(searchDevices[i].getValue('custrecord_clgx_dcim_device_fam',null,null));
		
		var virtual = searchDevices[i].getValue('custrecord_clgx_dcim_device_virtual',null,null);
		var categoryid = parseInt(searchDevices[i].getValue('custrecord_clgx_dcim_device_category',null,null)) || 0;
		var category = searchDevices[i].getText('custrecord_clgx_dcim_device_category',null,null) || '';
		objDevice["categoryid"] = categoryid;
		objDevice["category"] = category;
		
		objDevice["count_pwrs"] = count_powers(deviceid, categoryid);
		
		if(categoryid == 3){
			var powers = get_powers(deviceid);
			objDevice["powers"] = powers.powers;
			objDevice["kws_a"] = powers.kws_a;
			objDevice["kws_ab"] = powers.kws_ab;
			objDevice["kvas_a"] = powers.kvas_a;
			objDevice["kvas_ab"] = powers.kvas_ab;
		} else {
			objDevice["powers"] = [];
			objDevice["kws_a"] = 0;
			objDevice["kws_ab"] = 0;
			objDevice["kvas_a"] = 0;
			objDevice["kvas_ab"] = 0;
		}
		
		var capacity = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_device_capacity',null,null)) || 0;
		objDevice["capacity"] = capacity;
		
		var kwsab = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_device_day_kw_ab',null,null)) || 0;
		objDevice["failow"] = kwsab;
		objDevice["kwsab"] = kwsab;
		
		var load = parseFloat(searchDevices[i].getValue('custrecord_clgx_dcim_points_day_avg','custrecord_clgx_dcim_device_load',null)) || 0;
		objDevice["load"] = load;

		var chains = searchDevices[i].getText('custrecord_clgx_dcim_device_pwr_chains',null,null);
		objDevice["chains"] = chains;
		
		if(categoryid == 1 || categoryid == 3){
			var available = capacity - kwsab;
			objDevice["available"] = available;
		} 
		else if (categoryid > 0 && categoryid != 1 && categoryid != 3) {
			var available = capacity - load;
			objDevice["available"] = available;
		} 
		else {
			objDevice["available"] = 0;
		}
		
		arrDevices.push(objDevice);
	}

	return arrDevices;
}

function count_powers(deviceid, categoryid){
	
	var count = 0;
	var famid = parseInt(nlapiLookupField('customrecord_clgx_dcim_devices', deviceid, 'custrecord_clgx_dcim_device_fam'));
	
	var field = "";
	if(categoryid == 3){ // panel
		field = "custrecord_clgx_power_panel_pdpm";
	}
	if(categoryid == 1){ // UPS
		field = "custrecord_cologix_power_ups_rect";
	}
	if(categoryid == 8 || categoryid == 12){ // Generator
		field = "custrecord_clgx_power_generator";
	}
	
	if(famid && field){
		var columns = [];
		columns.push(new nlobjSearchColumn('name',null,null));
		var filters = [];
		filters.push(new nlobjSearchFilter(field, null, "anyof",famid));
		filters.push(new nlobjSearchFilter("isinactive", null, "is", 'F'));
		var powers = nlapiSearchRecord('customrecord_clgx_power_circuit', null, filters, columns);
		if(powers){
			count = powers.length;
		}
	}
	return count;
}

function get_powers(deviceid){
	
	var famid = parseInt(nlapiLookupField('customrecord_clgx_dcim_devices', deviceid, 'custrecord_clgx_dcim_device_fam'));
	var powers = [];
	if(famid){
		var columns = [];
		columns.push(new nlobjSearchColumn('internalid',null,null));
		columns.push(new nlobjSearchColumn('name',null,null));
		columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_sum_day_kw_a',null,null));
		columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_sum_day_kw_ab',null,null));
		columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_inst_kva_a',null,null));
		columns.push(new nlobjSearchColumn('custrecord_clgx_power_chain',null,null));
		var filters = [];
		//filters.push(new nlobjSearchFilter("custrecord_clgx_power_panel_pdpm", null, "anyof",famid));
		filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_device", null, "anyof",deviceid));
		filters.push(new nlobjSearchFilter("isinactive", null, "is", 'F'));
		var search = nlapiSearchRecord('customrecord_clgx_power_circuit', null, filters, columns);
		var kws_a = 0;
		var kws_ab = 0;
		var kvas_a = 0;
		var kvas_ab = 0;
		for ( var i = 0; search != null && i < search.length; i++ ) {
			var kw_a = parseFloat(search[i].getValue('custrecord_clgx_dcim_sum_day_kw_a',null,null)) || 0;
			var kw_ab = parseFloat(search[i].getValue('custrecord_clgx_dcim_sum_day_kw_ab',null,null)) || 0;
			var kva_a = parseFloat(search[i].getValue('custrecord_clgx_dcim_inst_kva_a',null,null)) || 0;
			var chain = search[i].getText('custrecord_clgx_power_chain',null,null) || '';
			
			if(kw_a){kws_a += kw_a;}
			if(kw_ab){kws_ab += kw_ab;}
			if(kva_a){kvas_a += kva_a;}
			powers.push({
				"power_id":  parseInt(search[i].getValue('internalid',null,null)),
				"power":     search[i].getValue('name',null,null),
				"kw_a":      round(kw_a),
				"kw_ab":     round(kw_ab),
				"kva_a":     round(kva_a),
				"chain":     chain,
			});
		}
	}
	return {
		"kws_a":     round(kws_a),
		"kws_ab":    round(kws_ab),
		"kvas_a":    round(kvas_a),
		"kvas_ab":   round(kvas_ab),
		"powers":    powers,
	};
}

function get_node_html(objDevice){
    
	var device = objDevice.device;
	var deviceid = objDevice.deviceid;
	var capacity = round(objDevice.capacity);
	var load = round(objDevice.load);
	//var current = round(objDevice.current);
	//var fail = round(objDevice.fail);
	var failow = round(objDevice.failow);
	
	var available = round(objDevice.available);
	//var available = round(objDevice.capacity - objDevice.failow);
	var virtual = objDevice.virtual;

	var virtualcss = '';
	if(virtual == 'T'){
		virtualcss = ' class="virtual"';
	}
	
	var html = '';
	html += '<table align="center" width="100%"' + virtualcss + '><tr class="bb2 ac hd0">';
	html += '<td colspan="3" nowrap><a href="/app/common/custom/custrecordentry.nl?rectype=218&id=' + deviceid + '" " target="_blank">' + device + '</a></td>';
	
	html += '</tr><tr class="bb">';
	//html += '<td nowrap class="al">Category</td>';
	html += '<td class="ar" colspan="3" nowrap>' + objDevice.category + '</td>';

	html += '</tr><tr class="bb hd3">';
	html += '<td nowrap class="al" colspan="2">Power Chains</td>';
	html += '<td class="ar">' + objDevice.chains + '</td>';
	html += '</tr>';

	html += '</tr><tr class="bb">';
	html += '<td nowrap class="al" colspan="2">Derated Capacity</td>';
	html += '<td class="ar">' + capacity + '</td>';
	if(load > 0){
		html += '</tr><tr class="bb">';
		html += '<td nowrap class="al" colspan="2">Actual Load (OD)</td>';
		html += '<td class="ar">' + load + '</td>';
	}

	html += '</tr><tr class="bb hd1">';
	html += '<td nowrap class="al" colspan="2">Load w/ Failover</td>';
	html += '<td class="ar">' + failow + '</td>';
	
	if(available > 0){
		html += '</tr><tr class="bb hd2">';
	}
	else{
		html += '</tr><tr class="bb hd3">';
	}
	html += '<td nowrap class="al" colspan="2">Available Capacity</td>';
	html += '<td class="ar">' + available + '</td>';
	html += '</tr>';

	//if(objDevice.count_pwrs){
		html += '<tr class="bb hd3"></tr>';
		html += '<tr class="bb hd0">';
		html += '<td nowrap class="al" align="left">Powers (' + objDevice.count_pwrs + ')</td>';
		html += '<td nowrap class="al" align="right">Used</td>';
		html += '<td nowrap class="al" align="right">Sold</td>';
		html += '</tr>';
		
		html += '<tr class="bb hd3"></tr>';
		html += '<tr class="bb hd3">';
		html += '<td nowrap class="al" align="left">Max Single Failure</td>';
		html += '<td nowrap class="al" align="right"></td>';
		html += '<td nowrap class="al" align="right"></td>';
		html += '</tr>';
		
		html += '<tr class="bb hd3"></tr>';
		html += '<tr class="bb hd1">';
		html += '<td nowrap class="al" align="left">Rolled Up</td>';
		html += '<td nowrap class="al" align="right">' + objDevice.kws_a + '</td>';
		html += '<td nowrap class="al" align="right">' + objDevice.kvas_a + '</td>';
		html += '</tr>';
		for ( var i = 0; objDevice.powers != null && i < objDevice.powers.length; i++ ) {
			html += '<tr>';
			html += '<td nowrap class="al" align="left"><span class="hd3">' + objDevice.powers[i].chain + '</span> - <a href="/app/common/custom/custrecordentry.nl?rectype=17&id=' + objDevice.powers[i].power_id + '" " target="_blank">' + objDevice.powers[i].power + '</a></td>';
			html += '<td nowrap class="al" align="right">' + objDevice.powers[i].kw_a + '</td>';
			html += '<td nowrap class="al" align="right">' + objDevice.powers[i].kva_a + '</td>';
			html += '</tr>';
		}
	//}
	
	html += '</table>';

	return html;
}

function round(value) {
	  return Number(Math.round(value+'e'+2)+'e-'+2);
}
