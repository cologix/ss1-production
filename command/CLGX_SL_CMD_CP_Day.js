nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_CP_Day.js
//	Script Name:	CLGX_SL_CMD_CP_Day
//	Script Id:		customscript_clgx_sl_cmd_cp_day
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/08/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=750&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_clgx_sl_cmd_cp_day (request, response){
    try {

    	var date = request.getParameter('date') || '';
    	
		var request = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/customer_portal/get_logs_day.cfm?date=' + date);
        var dataCustomers = JSON.parse( request.body );
        
        var customers = _.uniq(_.pluck(dataCustomers, 'companyid'));
        var modifiers = _.uniq(_.pluck(dataCustomers, 'modifierid'));
        var contacts = _.uniq(_.pluck(dataCustomers, 'contactid'));
        
		var arr = _.pluck(dataCustomers, 'sessions');
		sessions = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
		var arr = _.pluck(dataCustomers, 'hits');
		hits = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
		
        var dataCustomersSums = {
    		"customers": customers.length,
    		"modifiers": modifiers.length,
    		"contacts": contacts.length,
    		"sessions": sessions,
    		"hits": hits
    		
        };
        
        var objFile = nlapiLoadFile(4591698);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{dataCustomers}','g'), JSON.stringify(dataCustomers));
		html = html.replace(new RegExp('{dataCustomersSums}','g'), JSON.stringify(dataCustomersSums));
		response.write( html );

    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
