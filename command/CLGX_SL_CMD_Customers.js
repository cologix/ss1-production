nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Customers.js
//	Script Name:	CLGX_SL_CMD_SCustomers
//	Script Id:		customscript_clgx_sl_cmd_customers
//	Script Runs:	On Server
//	Script Type:	Suitelet
// 	Link			/app/site/hosting/scriptlet.nl?script=723&deploy=1
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/08/2016
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_customers (request, response){
	
	try {
		
		var file = nlapiLoadFile(4510024);
		var customers = file.getValue();
		
		var file = nlapiLoadFile(4534402);
		var locations = file.getValue();
		
		var file = nlapiLoadFile(4642883);
		var services = file.getValue();
		
		var objFile = nlapiLoadFile(4483442);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{customers}','g'), customers);
		html = html.replace(new RegExp('{locations}','g'), locations);
		html = html.replace(new RegExp('{services}','g'), services);
		response.write(html);
		
	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}

