nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Alarms_OD.js
//	Script Name:	CLGX_SL_CMD_Alarms_OD
//	Script Id:		customscript_clgx_sl_cmd_alarms_od
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/26/2017
//	URL:			/app/site/hosting/scriptlet.nl?script=800&deploy=1  
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_alarms_od (request, response){
    try {
    	var fileObject = nlapiLoadFile(14250768);
        response.write(fileObject.getValue());
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
