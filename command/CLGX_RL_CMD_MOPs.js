nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	   CLGX_RL_CMD_MOPs.js
//	Script Name:	   CLGX_RL_CMD_MOPs
//	Script Id:     customscript_clgx_rl_cmd_mops
//	Script Type:	   RESTlet
//	@authors:      Dan Tansanu - dan.tansanu@cologix.com
//	Created:	       03/22/2018
//	RelativeURL:   /app/site/hosting/restlet.nl?script=1492&deploy=1
//-------------------------------------------------------------------------------------------------

function restlet_cmd_mops (datain){
    try {

		datain = JSON.parse(datain);
		var userid = nlapiGetUser();
	    	var today = moment().format('M/D/YYYY');
	    	var type = datain['type'];
	    		var tree = {
				"node": '.',
				"type": type,
				"userid": userid,
				"today": today,
				"customerid": parseInt(datain['customerid']) || 0,
                "facilityid": parseInt(datain['facilityid']) || 0,
	    			"customers":0,
	    			"contacts":0,
	    			"devices":[],
	    			"spaces":[],
	    			"equipments":[],
	    			"children":[]
	    };
	
	    	var mop = {};
	    	if(type == 'spaces'){
	        	var str = datain['names'].split(",");
	        var arr = [];
	        	for ( var i = 0; i < str.length; i++ ) {
	        		arr.push({
						"node": str[i].replace("||", "&"),
						"type": 'space',
		    			"faicon": 'map-o',
		    			"leaf": true
	                });
	        	}
	        	tree.children.push({
	    				"node": 'Selected Spaces',
	    				"type": 'spaces',
	        			"faicon": 'map',
	        			"expanded": false,
	        			"leaf": false,
	        			"children":arr
	            });
	        	tree.spaces = _.pluck(arr, 'node');
	        	tree = get_spaces_mops (tree);
	        }
	    	if(type == 'powers'){
	    		
	    		var generatorsids = JSON.parse(datain.generators);
	    		var upsids = JSON.parse(datain.ups);
	    		var panelsids = JSON.parse(datain.panels);
	    		var allids = _.union(generatorsids,upsids,panelsids);
	    		
	    		tree.devices = allids;
	    		
	    		var columns = [];
		    	columns.push(new nlobjSearchColumn('internalid',null,null));
		    	columns.push(new nlobjSearchColumn('name',null,null));
		    	columns.push(new nlobjSearchColumn('altname',null,null));
				var filters = [];
				filters.push(new nlobjSearchFilter("internalid",null,"anyof",allids));
				var search = nlapiSearchRecord('customrecord_ncfar_asset', null, filters, columns);
				var devices = [];
				for ( var i = 0; search != null && i < search.length; i++ ) {
					devices.push({
						"id": parseInt(search[i].getValue('internalid',null,null)),
						"name": search[i].getValue('name',null,null) + ' ' + search[i].getValue('altname',null,null)
	                });
				}
				
				var objDevices = {
					"node": 'Selected Devices',
					"type": 'devices',
					"faicon": 'bolt',
					"expanded": false,
					"leaf": false,
					"children":[]
				};
				if(generatorsids.length > 0){
					var generators = [];
					for ( var i = 0; i < generatorsids.length; i++ ) {
						var generator = _.find(devices, function(arr){ return (arr.id == generatorsids[i]) ; });
						generators.push({
							"node": generator.name,
							"nodeid": generator.id,
							"type": 'generator',
			    			"faicon": 'bolt',
			    			"leaf": true
		                });
					}
					objDevices.children.push({
						"node": 'Generators',
						"type": 'generators',
		    			"faicon": 'bolt',
		    			"expanded": false,
		    			"leaf": false,
		    			"children":generators
					});
				}
	
				
				if(upsids.length > 0){
					var upses = [];
					for ( var i = 0; i < upsids.length; i++ ) {
						var ups = _.find(devices, function(arr){ return (arr.id == upsids[i]) ; });
						upses.push({
							"node": ups.name,
							"nodeid": ups.id,
							"type": 'ups',
			    			"faicon": 'bolt',
			    			"leaf": true
		                });
					}
					objDevices.children.push({
						"node": 'UPS',
						"type": 'ups',
		    			"faicon": 'bolt',
		    			"expanded": false,
		    			"leaf": false,
		    			"children":upses
					});
				}
	
				
				if(panelsids.length > 0){
					var panels = [];
					for ( var i = 0; i < panelsids.length; i++ ) {
						var panel = _.find(devices, function(arr){ return (arr.id == panelsids[i]) ; });
						panels.push({
							"node": panel.name,
							"nodeid": panel.id,
							"type": 'generator',
			    			"faicon": 'bolt',
			    			"leaf": true
		                });
					}
					objDevices.children.push({
						"node": 'Panels',
						"type": 'panels',
		    			"faicon": 'bolt',
		    			"expanded": false,
		    			"leaf": false,
		    			"children":panels
					});
				}
	
				tree.children.push(objDevices);
	
				tree = get_powers_mops (tree);
	    	}
	    	
	    	if(type == 'equipments'){
	    		
	            var equipmentsids = JSON.parse(request.getParameter('equipments'));
	    		tree.equipments = equipmentsids;
	    		
	    		var equipments = [];
				var columns = [];
		    	columns.push(new nlobjSearchColumn('internalid',null,null).setSort(true));
		    	columns.push(new nlobjSearchColumn('custrecord_clgx_equipment_name',null,null));
				var filters = [];
				filters.push(new nlobjSearchFilter("internalid",null,"anyof",equipmentsids));
				var search = nlapiSearchRecord('customrecord_cologix_equipment', null, filters, columns);
				for ( var i = 0; search != null && i < search.length; i++ ) {
					equipments.push({
						"node"     : search[i].getValue('custrecord_clgx_equipment_name',null,null),
						"nodeid"   : parseInt(search[i].getValue('internalid',null,null)),
						"type"     : 'equipment',
		    			"faicon"   : 'gear',
		    			"leaf"    : true
	                });
				}
				
				
				var objEquipments = {
					"node": 'Selected Equipments',
					"type": 'equipments',
					"faicon": 'gears',
					"expanded": false,
					"leaf": false,
					"children":equipments
				};
				nlapiLogExecution('ERROR','debug', JSON.stringify(tree, null, 4));
				
				tree.children.push(objEquipments);
				nlapiLogExecution('ERROR','debug', JSON.stringify(tree, null, 4));
				tree = get_equipments_mops (tree);
				
				nlapiLogExecution('ERROR','debug', JSON.stringify(tree, null, 4));
				
	    	}
	    	
	    	var subtypes = get_case_subtypes (type);
	    	var employees = get_employees (tree.customerid);
	    	var contacts = get_contacts (tree.customerid);
	    	
	    	var file = nlapiLoadFile(5105512);
			html = file.getValue();
			html = html.replace(new RegExp('{tree}','g'), JSON.stringify(tree));
			html = html.replace(new RegExp('{employees}','g'), JSON.stringify(employees));
			html = html.replace(new RegExp('{contacts}','g'), JSON.stringify(contacts));
			html = html.replace(new RegExp('{subtypes}','g'), JSON.stringify(subtypes));

        return html;

    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


