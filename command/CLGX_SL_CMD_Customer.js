nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Customer.js
//	Script Name:	CLGX_SL_CMD_SCustomer
//	Script Id:		customscript_clgx_sl_cmd_customer
//	Script Runs:	On Server
//	Script Type:	Suitelet
// 	Link			/app/site/hosting/scriptlet.nl?script=749&deploy=1
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/06/2016
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_customer (request, response){
	
	try {
		
		var customerid = request.getParameter('customerid') || 0;
		var locationid = 0;

		var name = (nlapiLookupField('customer', customerid, 'entityid')).split(":") || '';
		var customer = (name[name.length-1]).trim();
		
    	var rows = get_rows(customerid, locationid);
    	var ids = _.uniq(_.pluck(rows, 'serviceid'));
    	
    	//var spaces = get_spaces(ids);
		//var powers = get_powers(ids);
		//var net = get_net(ids);
		//var xcs = get_xcs(ids);
		//var vxcs = get_vxcs(ids);
		
    	var spaces = get_inventory (customerid, locationid, 4830208);
		var powers = get_inventory (customerid, locationid, 4833030);
		var net = get_inventory (customerid, locationid, 4875312);
		var xcs = get_inventory (customerid, locationid, 4834334);
		var vxcs = get_inventory (customerid, locationid, 4662283);
		var managed = get_inventory (customerid, locationid, 11063851);
		
		var inventory = [];
		if(spaces.length > 0){
			inventory.push({
				"node":'Spaces',
				"type":'spaces',
				"faicon": "map-o",
				"expanded": false,
				"leaf": false,
				"children": spaces
			});
		}
		if(powers.length > 0){
			inventory.push({
				"node":'Powers',
				"type":'powers',
				"faicon": "plug",
				"expanded": false,
				"leaf": false,
				"children": powers
			});	
		}
		if(net.length > 0){
			inventory.push({
				"node":'Network',
				"type":'nets',
				"faicon": "share-alt-square",
				"expanded": false,
				"leaf": false,
				"children": net
			});
		}
		if(xcs.length > 0){
			inventory.push({
				"node":'XCs',
				"type":'xcs',
				"faicon": "share-alt-square",
				"expanded": false,
				"leaf": false,
				"children": xcs
			});
		}
		if(vxcs.length > 0){
			inventory.push({
				"node":'VXCs',
				"type":'vxcs',
				"faicon": "share-alt",
				"expanded": false,
				"leaf": false,
				"children": vxcs
			});
		}
		if(managed.length > 0){
			inventory.push({
				"node":'Managed',
				"type":'managed',
				"faicon": "gear",
				"expanded": false,
				"leaf": false,
				"children": managed
			});
		}
		
		for ( var i = 0; i < rows.length; i++ ) {
			
			var arr = _.filter(spaces, function(arr){
				return (arr.soid == rows[i].soid && arr.serviceid == rows[i].serviceid);
			});
			if(arr.length > 0){
				if(rows[i].cages > 0){
					rows[i].inv_space = arr.length;
					rows[i].sos_space = arr.length
				} else {
					rows[i].inv_space = arr.length;
				}
				rows[i].children.push({
					"node":'Spaces',
					"type":'spaces',
					"faicon": "map-o",
					"expanded": false,
					"leaf": false,
					"children": arr
				});
			}
			
			var arr = _.filter(powers, function(arr){
				return (arr.soid == rows[i].soid && arr.serviceid == rows[i].serviceid);
			});
			if(arr.length > 0){
				rows[i].inv_power = arr.length;
				rows[i].children.push({
					"node":'Powers',
					"type":'powers',
					"faicon": "plug",
					"expanded": false,
					"leaf": false,
					"children": arr
				});
			}
			
			var arr = _.filter(net, function(arr){
				return (arr.soid == rows[i].soid && arr.serviceid == rows[i].serviceid);
			});
			if(arr.length > 0){
				rows[i].inv_net = arr.length;
				rows[i].children.push({
					"node":'Network',
					"type":'nets',
					"faicon": "share-alt-square",
					"expanded": false,
					"leaf": false,
					"children": arr
				});
			}
			
			var arr = _.filter(xcs, function(arr){
				return (arr.soid == rows[i].soid && arr.serviceid == rows[i].serviceid);
			});
			if(arr.length > 0){
				rows[i].inv_xcs = arr.length;
				rows[i].children.push({
					"node":'XCs',
					"type":'xcs',
					"faicon": "share-alt-square",
					"expanded": false,
					"leaf": false,
					"children": arr
				});
			}
			
			var arr = _.filter(vxcs, function(arr){
				return (arr.soid == rows[i].soid && arr.serviceid == rows[i].serviceid);
			});
			if(arr.length > 0){
				rows[i].inv_vxcs = arr.length;
				rows[i].children.push({
					"node":'VXCs',
					"type":'vxcs',
					"faicon": "share-alt",
					"expanded": false,
					"leaf": false,
					"children": arr
				});
			}
			
			var arr = _.filter(managed, function(arr){
				return (arr.soid == rows[i].soid && arr.serviceid == rows[i].serviceid);
			});
			if(arr.length > 0){
				rows[i].inv_managed = arr.length;
				rows[i].children.push({
					"node":'Managed',
					"type":'managed',
					"faicon": "gear",
					"expanded": false,
					"leaf": false,
					"children": arr
				});
			}
		}
		
		var sos = [];
		var sosids = _.uniq(_.pluck(rows, 'soid'));
		for ( var i = 0; i < sosids.length; i++ ) {
			
			var arrSO = _.filter(rows, function(arr){
				return (arr.soid == sosids[i]);
			});
			
			var sos_count = 1;
			var arr = _.pluck(arrSO, 'serv_count');
			var serv_count = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
			
			var arr = _.pluck(arrSO, 'sos_space');
			var sos_space = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
			var arr = _.pluck(arrSO, 'sos_power');
			var sos_power = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
			var arr = _.pluck(arrSO, 'sos_net');
			var sos_net = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
			var arr = _.pluck(arrSO, 'sos_xcs');
			var sos_xcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
			var arr = _.pluck(arrSO, 'sos_vxcs');
			var sos_vxcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
			var arr = _.pluck(arrSO, 'sos_managed');
			var sos_managed = _.reduce(arr, function(memo, num){ return memo + num; }, 0);

			var arr = _.pluck(arrSO, 'inv_space');
			var inv_space = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
			var arr = _.pluck(arrSO, 'inv_power');
			var inv_power = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
			var arr = _.pluck(arrSO, 'inv_net');
			var inv_net = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
			var arr = _.pluck(arrSO, 'inv_xcs');
			var inv_xcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
			var arr = _.pluck(arrSO, 'inv_vxcs');
			var inv_vxcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
			var arr = _.pluck(arrSO, 'inv_managed');
			var inv_managed = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
			
			sos.push({
				"nodeid": arrSO[0].soid,
				"node": arrSO[0].so,
				"type": "so",
				"locationid": arrSO[0].locationid,
				"location": arrSO[0].location,
				"sos_count": sos_count,
				"serv_count": serv_count,
				"sos_space": sos_space,
				"inv_space": inv_space,
				"sos_power": sos_power,
				"inv_power": inv_power,
				"sos_net": sos_net,
				"inv_net": inv_net,
				"sos_xcs": sos_xcs,
				"inv_xcs": inv_xcs,
				"sos_vxcs": sos_vxcs,
				"inv_vxcs": inv_vxcs,
				"sos_managed": sos_managed,
				"inv_managed": inv_managed,
				"faicon": "th-list",
				"expanded": false,
				"leaf": false,
				"children": arrSO
			});
		}

		// sums at customer level
		var arr = _.pluck(sos, 'sos_count');
		var sos_count = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
		var arr = _.pluck(sos, 'serv_count');
		var serv_count = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
		
		var arr = _.pluck(sos, 'sos_space');
		var sos_space = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
		var arr = _.pluck(sos, 'sos_power');
		var sos_power = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
		var arr = _.pluck(sos, 'sos_net');
		var sos_net = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
		var arr = _.pluck(sos, 'sos_xcs');
		var sos_xcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
		var arr = _.pluck(sos, 'sos_vxcs');
		var sos_vxcs = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
		var arr = _.pluck(sos, 'sos_managed');
		var sos_managed = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
		
		
		var treeSOs = {
				"text": ".",
				"children": [{
					"nodeid": customerid,
					"node": customer,
					"type": "customer",
					"sos_count": sos_count,
					"serv_count": serv_count,
					"sos_power": sos_power,
					"inv_power": powers.length,
					"sos_space": sos_space,
					"inv_space": spaces.length,
					"sos_net": sos_net,
					"inv_net": net.length,
					"sos_xcs": sos_xcs,
					"inv_xcs": xcs.length,
					"sos_vxcs": sos_vxcs,
					"inv_vxcs": vxcs.length,
					"sos_managed": sos_managed,
					"inv_managed": managed.length,
					"faicon": "user-secret",
					"expanded": false,
					"leaf": false,
					"children": sos
				}]
		};
		
		var treeInv = {
				"text": ".",
				"children": inventory
		};
		
		var objFile = nlapiLoadFile(4577619);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{treeSOs}','g'), JSON.stringify(treeSOs));
		html = html.replace(new RegExp('{treeInv}','g'), JSON.stringify(treeInv));
		response.write(html);
		
	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}

function get_rows (customerid, locationid) {
	
	var rows = [];
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainname', null,'anyof', customerid));
	if(locationid > 0){
		filters.push(new nlobjSearchFilter('location', null,'anyof', locationid));
	}
	var search = nlapiLoadSearch('transaction', 'customsearch_clgx_cmd_customer');
	search.addColumns(columns);
	search.addFilters(filters);
	var result = search.runSearch();
	result.forEachResult(function(row) {
		
		var columns = row.getAllColumns();
		
		var name = (row.getText('location', null, 'GROUP')).split(":") || '';
		var location = (name[name.length-1]).trim();
		
		var name = (row.getText('custcol_clgx_so_col_service_id', null, 'GROUP')).split(":") || '';
		var service = (name[name.length-1]).trim();
		
		var name = (row.getValue('transactionname', null, 'GROUP')).split("#") || '';
		var so = (name[name.length-1]).trim();
		
		rows.push({
				"nodeid": parseInt(row.getValue('custcol_clgx_so_col_service_id', null, 'GROUP')) || 0,
				"node": service,
				"type": "service",
				"locationid": parseInt(row.getValue('location', null, 'GROUP')) || 0,
				"location": location,
				"soid": parseInt(row.getValue('internalid', null, 'GROUP')),
				"so": so,
				"serviceid": parseInt(row.getValue('custcol_clgx_so_col_service_id', null, 'GROUP')) || 0,
				"sos_count": 0,
				"serv_count": 1,
				"sos_space": parseInt(row.getValue(columns[4])) || 0,
				"inv_space": 0,
				"sos_power": parseInt(row.getValue(columns[5])) || 0,
				"inv_power": 0,
				"sos_net": parseInt(row.getValue(columns[6])) || 0,
				"inv_net": 0,
				"sos_xcs": parseInt(row.getValue(columns[7])) || 0,
				"inv_xcs": 0,
				"sos_vxcs": parseInt(row.getValue(columns[8])) || 0,
				"inv_vxcs": 0,
				"sos_managed": parseInt(row.getValue(columns[9])) || 0,
				"inv_managed": 0,
				"cages": parseInt(row.getValue(columns[10])) || 0,
				"faicon": "gears",
				"leaf": false,
				"children": []
		});
		return true;
	});
	return rows;
}

function get_inventory (customerid, locationid, fileid) {
	
	var objFile = nlapiLoadFile(fileid);
	var all = JSON.parse(objFile.getValue());
	if(locationid){
		var arr = _.filter(all, function(arr){
			return (arr.customerid == customerid && arr.locationid == locationid);
		});
	} else {
		var arr = _.filter(all, function(arr){
			return (arr.customerid == customerid);
		});
	}
	return arr;
}


function get_powers (ids) {
	var powers = [];
	var columns = [];
	var filters = [];
	filters.push(new nlobjSearchFilter('custrecord_cologix_power_service',null,'anyof', ids));
	var search = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_cmd_customer_powers');
	search.addColumns(columns);
	search.addFilters(filters);
	var result = search.runSearch();
	result.forEachResult(function(row) {
		powers.push({
			"nodeid": parseInt(row.getValue('internalid', null, null)) || 0,
			"node": row.getValue('name', null, null) || '',
			"type": 'power',
			"soid": parseInt(row.getValue('custrecord_power_circuit_service_order', null, null)) || 0,
			"so": row.getValue('custrecord_power_circuit_service_order', null, null) || '',
			"serviceid": parseInt(row.getValue('custrecord_cologix_power_service', null, null)) || 0,
			"service": row.getValue('custrecord_cologix_power_service', null, null) || '',
			"facilityid": parseInt(row.getValue('custrecord_cologix_power_facility', null, null)) || 0,
			"facility": row.getText('custrecord_cologix_power_facility', null, null) || '',
			"usage": parseFloat(row.getValue('custrecord_clgx_dcim_sum_day_amp_a_usg', null, null)) || 0,
			"faicon": "plug",
			"leaf": true
		});
		return true;
	});
	return powers;
}

function get_spaces (ids) {
	var spaces = [];
	var columns = [];
	var filters = [];
	filters.push(new nlobjSearchFilter('custrecord_cologix_space_project',null,'anyof', ids));
	var search = nlapiLoadSearch('customrecord_cologix_space', 'customsearch_clgx_cmd_customer_spaces');
	search.addColumns(columns);
	search.addFilters(filters);
	var result = search.runSearch();
	result.forEachResult(function(row) {
		spaces.push({
			"nodeid": parseInt(row.getValue('internalid', null, null)) || 0,
			"node": row.getValue('name', null, null) || '',
			"type": 'space',
			"soid": parseInt(row.getValue('custrecord_space_service_order', null, null)) || 0,
			"so": row.getValue('custrecord_space_service_order', null, null) || '',
			"serviceid": parseInt(row.getValue('custrecord_cologix_space_project', null, null)) || 0,
			"service": row.getValue('custrecord_cologix_space_project', null, null) || '',
			"facilityid": parseInt(row.getValue('custrecord_cologix_space_location', null, null)) || 0,
			"facility": row.getText('custrecord_cologix_space_location', null, null) || '',
			"faicon": "map-o",
			"leaf": true
		});
		return true;
	});
	return spaces;
}

function get_net (ids) {
	var net = [];
	var columns = [];
	var filters = [];
	filters.push(new nlobjSearchFilter('custrecord_cologix_xc_service',null,'anyof', ids));
	var search = nlapiLoadSearch('customrecord_cologix_crossconnect', 'customsearch_clgx_cmd_customer_net');
	search.addColumns(columns);
	search.addFilters(filters);
	var result = search.runSearch();
	result.forEachResult(function(row) {
		net.push({
			"nodeid": parseInt(row.getValue('internalid', null, 'GROUP')) || 0,
			"node": row.getValue('name', null, 'GROUP') || '',
			"type": 'net',
			"soid": parseInt(row.getValue('internalid', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', 'GROUP')) || 0,
			"so": row.getValue('tranid', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', 'GROUP') || '',
			"serviceid": parseInt(row.getValue('custrecord_cologix_xc_service', null, 'GROUP')) || 0,
			"service": row.getValue('custrecord_cologix_xc_service', null, 'GROUP') || '',
			"facilityid": parseInt(row.getValue('custrecord_clgx_xc_facility', null, 'GROUP')) || 0,
			"facility": row.getText('custrecord_clgx_xc_facility', null, 'GROUP') || '',
			"faicon": "share-alt-square",
			"leaf": true
		});
		return true;
	});
	return net;
}

function get_xcs(ids) {
	var xcs = [];
	var columns = [];
	var filters = [];
	filters.push(new nlobjSearchFilter('custrecord_cologix_xc_service',null,'anyof', ids));
	var search = nlapiLoadSearch('customrecord_cologix_crossconnect', 'customsearch_clgx_cmd_customer_xcs');
	search.addColumns(columns);
	search.addFilters(filters);
	var result = search.runSearch();
	result.forEachResult(function(row) {
		xcs.push({
			"nodeid": parseInt(row.getValue('internalid', null, 'GROUP')) || 0,
			"node": row.getValue('name', null, 'GROUP') || '',
			"type": 'xc',
			"soid": parseInt(row.getValue('internalid', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', 'GROUP')) || 0,
			"so": row.getValue('tranid', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', 'GROUP') || '',
			"serviceid": parseInt(row.getValue('custrecord_cologix_xc_service', null, 'GROUP')) || 0,
			"service": row.getValue('custrecord_cologix_xc_service', null, 'GROUP') || '',
			"facilityid": parseInt(row.getValue('custrecord_clgx_xc_facility', null, 'GROUP')) || 0,
			"facility": row.getText('custrecord_clgx_xc_facility', null, 'GROUP') || '',
			"faicon": "share-alt-square",
			"leaf": true
		});
		return true;
	});
	return xcs;
}

function get_vxcs(ids) {
	var vxcs = [];
	var columns = [];
	var filters = [];
	filters.push(new nlobjSearchFilter('custrecord_cologix_vxc_service',null,'anyof', ids));
	var search = nlapiLoadSearch('customrecord_cologix_vxc', 'customsearch_clgx_cmd_customer_vxcs');
	search.addColumns(columns);
	search.addFilters(filters);
	var result = search.runSearch();
	result.forEachResult(function(row) {
		vxcs.push({
			"nodeid": parseInt(row.getValue('internalid', null, null)) || 0,
			"node": row.getValue('name', null, null) || '',
			"type": 'vxc',
			"soid": parseInt(row.getValue('custrecord_cologix_service_order', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', null)) || 0,
			"so": row.getValue('custrecord_cologix_service_order', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', null) || '',
			"serviceid": parseInt(row.getValue('custrecord_cologix_vxc_service', null, null)) || 0,
			"service": row.getValue('custrecord_cologix_vxc_service', null, null) || '',
			"facilityid": parseInt(row.getValue('custrecord_cologix_vxc_facility', null, null)) || 0,
			"facility": row.getText('custrecord_cologix_vxc_facility', null, null) || '',
			"faicon": "share-alt",
			"leaf": true
		});
		return true;
	});
	return vxcs;
}

function get_managed(ids) {
	var managed = [];
	var columns = [];
	var filters = [];
	filters.push(new nlobjSearchFilter('custrecord_clgx_ms_service',null,'anyof', ids));
	var search = nlapiLoadSearch('customrecord_clgx_managed_services', 'customsearch_clgx_cmd_customer_managed');
	search.addColumns(columns);
	search.addFilters(filters);
	var result = search.runSearch();
	result.forEachResult(function(row) {
		managed.push({
			"nodeid": parseInt(row.getValue('internalid', null, null)) || 0,
			"node": row.getValue('name', null, null) || '',
			"type": 'managed',
			"soid": parseInt(row.getValue('custrecord_cologix_service_order', 'custrecord_clgx_ms_service_order', null)) || 0,
			"so": row.getValue('custrecord_cologix_service_order', 'custrecord_clgx_ms_service_order', null) || '',
			"serviceid": parseInt(row.getValue('custrecord_clgx_ms_service', null, null)) || 0,
			"service": row.getValue('custrecord_clgx_ms_service', null, null) || '',
			"facilityid": parseInt(row.getValue('custrecord_clgx_ms_facility', null, null)) || 0,
			"facility": row.getText('custrecord_clgx_ms_facility', null, null) || '',
			"faicon": "gear",
			"leaf": true
		});
		return true;
	});
	return managed;
}

