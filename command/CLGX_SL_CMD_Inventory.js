nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Inventory.js
//	Script Name:	CLGX_SL_CMD_Inventory
//	Script Id:		customscript_clgx_sl_cmd_inventory
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/06/2017
//	URL:			/app/site/hosting/scriptlet.nl?script=766&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_inventory (request, response){
	try {
		
		var type = request.getParameter('type');
		
		if (type == 'spaces'){
			var objFile = nlapiLoadFile(4830307);
	    	var complet = JSON.parse(objFile.getValue());
	    	var objFile = nlapiLoadFile(4830308);
	    	var misconf = JSON.parse(objFile.getValue());
	    	var objFile = nlapiLoadFile(4830309);
	    	var orphans = JSON.parse(objFile.getValue());
		}
		if (type == 'powers'){
			var objFile = nlapiLoadFile(4833031);
	    	var complet = JSON.parse(objFile.getValue());
	    	var objFile = nlapiLoadFile(4833032);
	    	var misconf = JSON.parse(objFile.getValue());
	    	var objFile = nlapiLoadFile(4833033);
	    	var orphans = JSON.parse(objFile.getValue());
		}
		if (type == 'xcs'){
			var objFile = nlapiLoadFile(4834634);
	    	var complet = JSON.parse(objFile.getValue());
	    	var objFile = nlapiLoadFile(4834636);
	    	var misconf = JSON.parse(objFile.getValue());
	    	var objFile = nlapiLoadFile(4834637);
	    	var orphans = JSON.parse(objFile.getValue());
		}
		if (type == 'network'){
			var objFile = nlapiLoadFile(4834635);
	    	var complet = JSON.parse(objFile.getValue());
	    	var objFile = nlapiLoadFile(4834636);
	    	var misconf = JSON.parse(objFile.getValue());
	    	var objFile = nlapiLoadFile(4834637);
	    	var orphans = JSON.parse(objFile.getValue());
		}
		if (type == 'vxcs'){
			var objFile = nlapiLoadFile(4835934);
	    	var complet = JSON.parse(objFile.getValue());
	    	var objFile = nlapiLoadFile(4835434);
	    	var misconf = JSON.parse(objFile.getValue());
	    	var objFile = nlapiLoadFile(4662285);
	    	var orphans = JSON.parse(objFile.getValue());
		}
		if (type == 'managed'){
			var objFile = nlapiLoadFile(11065154);
	    	var complet = JSON.parse(objFile.getValue());
	    	var objFile = nlapiLoadFile(11065255);
	    	var misconf = JSON.parse(objFile.getValue());
	    	var objFile = nlapiLoadFile(11065656);
	    	var orphans = JSON.parse(objFile.getValue());
		}
    	
		
    	var objFile = nlapiLoadFile(4830610);
		html = objFile.getValue();
		html = html.replace(new RegExp('{complet}','g'), JSON.stringify(complet));
		html = html.replace(new RegExp('{misconf}','g'), JSON.stringify(misconf));
		html = html.replace(new RegExp('{orphans}','g'), JSON.stringify(orphans));
		response.write( html );
		
	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}
