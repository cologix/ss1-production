nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_MOPs_Cases.js
//	Script Name:	CLGX_SL_CMD_MOPs_Cases
//	Script Id:		customscript_clgx_sl_cmd_mops_cases
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		02/23/2017
//	URL:			/app/site/hosting/scriptlet.nl?script=826&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_mops_cases (request, response){
    try {

    	var userid = nlapiGetUser();
    	var today = moment().format('M/D/YYYY');
    	
    	var type = request.getParameter('type') || '';
    	
    	var spaces = JSON.parse(request.getParameter('spaces')) || [];
    	for ( var i = 0; i < spaces.length; i++ ) {
        	spaces[i] = spaces[i].replace("||", "&");
        }
        
    	var customerid = parseInt(request.getParameter('customerid')) || 0;
    	if(customerid == 2790){
    		var subsidiaryid = 5;
    	} else {
    		var subsidiaryid = 6;
    	}
    	
    	var csubtype = parseInt(request.getParameter('subtype')) || 0;
    	var ctype = parseInt(nlapiLookupField('customrecord_cologix_sub_case_type', csubtype, 'custrecord_cologix_sub_case_type'));

    	var tree = {
				"message": '',
				"type": type,
				"userid": userid,
				"today": today,
				"customerid": customerid,
				"subsidiaryid": subsidiaryid,
                "facilityid": parseInt(request.getParameter('facilityid')) || 0,
                "subject": request.getParameter('subject') || '',
                "assigned": parseInt(request.getParameter('assigned')) || 0,
                "ctype": ctype,
                "csubtype": csubtype,
                "contact": parseInt(request.getParameter('contact')) || 0,
                "devices": JSON.parse(request.getParameter('devices')) || [],
                "spaces": spaces,
                "equipments": JSON.parse(request.getParameter('equipments')) || [],
                "children":[]
        };

    	// create parent case with JSON tree
    	var record = nlapiCreateRecord('supportcase');
        record.setFieldValue('title', tree.subject);
        record.setFieldValue('company', tree.customerid);
        record.setFieldValue('custevent_cologix_facility', tree.facilityid);
        record.setFieldValue('subsidiary', tree.subsidiaryid);
        record.setFieldValue('assigned', tree.assigned);
        record.setFieldValue('contact', tree.contact);
        
        if(tree.customerid == 2789){
        	record.setFieldValue('profile', 9);
        }
        
        //record.setFieldText('status', subcasestatus);
        //record.setFieldText('priority', priority);
        record.setFieldValue('origin', 8);
        record.setFieldValue('category', tree.ctype);
        record.setFieldValue('custevent_cologix_sub_case_type', tree.csubtype);
        record.setFieldValue('custevent_clgx_case_maintenance_json', JSON.stringify(tree));
        record.setFieldValue('custevent_clgx_customer_notification', 'T');
        //record.setFieldValue('custevent_cologix_case_sched_followup', followup);
        //record.setFieldValue('custevent_cologix_sched_start_time', starttime);
        //record.setFieldValue('origin', 6);
        var idRec = nlapiSubmitRecord(record, false, true);
    	
    	if(ctype == 2){
    		nlapiSubmitField('supportcase', idRec, 'custevent_clgx_locked_case', 'T');
    		
    		var message = 'A ' + type + ' incident parent case was created. Please <a href="/app/crm/support/supportcase.nl?id=' + idRec + '" target="_blank">click here</a> to access it. Customers subcases will be created in a few minutes. You will receive an email after all subcases were created. Please close this tab.';
    		var arrParam = [];
            arrParam['custscript_cmd_mops_parent_caseid'] = idRec;
            arrParam['custscript_cmd_mops_parent_userid'] = userid;
            
            nlapiScheduleScript('customscript_clgx_ss_cmd_mops_subcases', null ,arrParam);
    	} else {
    		var message = 'A ' + type + ' maintenance parent case was created. Please <a href="/app/crm/support/supportcase.nl?id=' + idRec + '" target="_blank">click here</a> to access it. Customers subcases will be created when the status of the parent case changes to CAB Approved. Please close this tab.';
    	}
    	tree.message = message;
    	
    	response.write(message);
    	
    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
