nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Devices_JSON.js
//	Script Name:	CLGX_SL_CMD_Devices_JSON
//	Script Id:		customscript_clgx_sl_cmd_devices_json
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/30/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=743&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_devices_json (request, response){
	try {

		// load devices_grid_add.json file and change it to array
		var objFile = nlapiLoadFile(1905426);
		var treeDevices = JSON.parse(objFile.getValue());
		response.write( JSON.stringify(treeDevices) );
		
	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}
