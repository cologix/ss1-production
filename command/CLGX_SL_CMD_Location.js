nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Location.js
//	Script Name:	CLGX_SL_CMD_Location
//	Script Id:		customscript_clgx_sl_cmd_location
//	Script Runs:	On Server
//	Script Type:	Suitelet
// 	Link			/app/site/hosting/scriptlet.nl?script=753&deploy=1
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/13/2016
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_location (request, response){
	
	try {
		
		var locationid = request.getParameter('locationid') || 0;
		var location = nlapiLookupField('location', locationid, 'name');

		var categoryid = request.getParameter('categoryid') || 0;
		
    	var services = get_services(locationid, categoryid);
    	var ids = _.uniq(_.pluck(services, 'serviceid'));
    	
    	var inventory = [];
		if(ids.length > 0 && categoryid == 10){ // space
			inventory = get_spaces(ids);
			var treetype = 'Spaces';
		}
		if(ids.length > 0 && categoryid == 8){ // power
			inventory = get_powers(ids);
			var treetype = 'Powers';
		}
		if(ids.length > 0 && categoryid == 5){ // net
			inventory = get_net(ids);
			var treetype = 'Network';
		}
		if(ids.length > 0 && categoryid == 4){ // xc
			inventory = get_xcs(ids);
			var treetype = 'XCs';
		}
		if(ids.length > 0 && categoryid == 11){ // vxc
			inventory = get_vxcs(ids);
			var treetype = 'VXCs';
		}
		if(ids.length > 0 && categoryid == 13){ // managed
			inventory = get_managed(ids);
			var treetype = 'Managed';
		}
		
    	for ( var i = 0; i < services.length; i++ ) {
			var arr = _.filter(inventory, function(arr){
				return (arr.serviceid == services[i].serviceid);
			});
			if(arr.length > 0){
				for ( var j = 0; j < arr.length; j++ ) {
					arr[j].customerid = services[i].customerid;
					arr[j].customer = services[i].customer;
				}
				services[i].nims = arr.length;
				services[i].children = arr;
			}
		}
		
		var sos = [];
		var ids = _.uniq(_.pluck(services, 'soid'));
		for ( var i = 0; i < ids.length; i++ ) {
			var arr = _.filter(services, function(arr){
				return (arr.soid == ids[i]);
			});
			sos.push({
				"nodeid": arr[0].soid,
				"node": arr[0].so,
				"type": "so",
				"customerid": arr[0].customerid,
				"customer": arr[0].customer,
				"soid": arr[0].soid,
				"so": arr[0].so,
				"faicon": "gears",
				"expanded": false,
				"leaf": false,
				"children": arr
			});
		}
		
		var customers = [];
		var ids = _.uniq(_.pluck(sos, 'customerid'));
		for ( var i = 0; i < ids.length; i++ ) {
			var arr = _.filter(sos, function(arr){
				return (arr.customerid == ids[i]);
			});
			customers.push({
				"nodeid": arr[0].customerid,
				"node": arr[0].customer,
				"type": "customer",
				"customerid": arr[0].customerid,
				"customer": arr[0].customer,
				"faicon": "user",
				"expanded": false,
				"leaf": false,
				"children": arr
			});
		}
		
		var tree = {
				"text": ".",
				"type": treetype,
				"location": location,
				"children": customers
		};
		
		
		var objFile = nlapiLoadFile(4617801);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{tree}','g'), JSON.stringify(tree));
		response.write(html);
		
	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}


function get_services (locationid, categoryid) {
	
	var services = [];
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('location', null,'anyof', locationid));
	filters.push(new nlobjSearchFilter('custcol_cologix_invoice_item_category', null,'anyof', categoryid));
	var search = nlapiLoadSearch('transaction', 'customsearch_clgx_cmd_location');
	search.addColumns(columns);
	search.addFilters(filters);
	var result = search.runSearch();
	result.forEachResult(function(row) {
		
		var columns = row.getAllColumns();
		
		var name = (row.getText('location', null, 'GROUP')).split(":") || '';
		var location = (name[name.length-1]).trim();
		
		var name = (row.getText('custcol_clgx_so_col_service_id', null, 'GROUP')).split(":") || '';
		var service = (name[name.length-1]).trim();
		
		if(categoryid == 10){
			var column = 6;
		}
		if(categoryid == 8){
			var column = 7;
		}
		if(categoryid == 5){
			var column = 8;
		}
		if(categoryid == 4){
			var column = 9;
		}
		if(categoryid == 11){
			var column = 10;
		}
		if(categoryid == 13){
			var column = 11;
		}
		services.push({
				"nodeid": parseInt(row.getValue('custcol_clgx_so_col_service_id', null, 'GROUP')) || 0,
				"node": service,
				"type": "service",
				
				"customerid": parseInt(row.getValue('mainname', null, 'GROUP')) || 0,
				"customer": row.getText('mainname', null, 'GROUP') || '',
				"locationid": parseInt(row.getValue('location', null, 'GROUP')) || 0,
				"location": location,
				"soid": parseInt(row.getValue('internalid', null, 'GROUP')),
				"so": row.getValue('transactionnumber', null, 'GROUP') || '',
				"serviceid": parseInt(row.getValue('custcol_clgx_so_col_service_id', null, 'GROUP')) || 0,
				"service": service,
				"itemid": parseInt(row.getValue('item', null, 'GROUP')) || 0,
				"item": row.getText('item', null, 'GROUP') || '',
				
				"bills": parseInt(row.getValue(columns[column])) || 0,
				"nims": 0,
				
				"faicon": "gear",
				"leaf": false,
				"children": []
		});
		return true;
	});
	return services;
}

function get_spaces (ids) {
	
	var arr = [];
	var columns = [];
	var filters = [];
	filters.push(new nlobjSearchFilter('custrecord_cologix_space_project',null,'anyof', ids));
	var search = nlapiLoadSearch('customrecord_cologix_space', 'customsearch_clgx_cmd_customer_spaces');
	search.addColumns(columns);
	search.addFilters(filters);
	var result = search.runSearch();
	result.forEachResult(function(row) {
		arr.push({
			"nodeid": parseInt(row.getValue('internalid', null, null)) || 0,
			"node": row.getValue('name', null, null) || '',
			"type": 'space',
			"customerid": 0,
			"customer": '',
			//"soid": parseInt(row.getValue('custrecord_space_service_order', null, null)) || 0,
			"so": row.getText('custrecord_space_service_order', null, null) || '',
			"serviceid": parseInt(row.getValue('custrecord_cologix_space_project', null, null)) || 0,
			"service": row.getText('custrecord_cologix_space_project', null, null) || '',
			//"facilityid": parseInt(row.getValue('custrecord_cologix_space_location', null, null)) || 0,
			//"facility": row.getText('custrecord_cologix_space_location', null, null) || '',
			"faicon": "map-o",
			"leaf": true
		});
		return true;
	});
	return arr;
}

function get_powers (ids) {
	
	var arr = [];
	var columns = [];
	var filters = [];
	filters.push(new nlobjSearchFilter('custrecord_cologix_power_service',null,'anyof', ids));
	var search = nlapiLoadSearch('customrecord_clgx_power_circuit', 'customsearch_clgx_cmd_customer_powers');
	search.addColumns(columns);
	search.addFilters(filters);
	var result = search.runSearch();
	result.forEachResult(function(row) {
		arr.push({
			"nodeid": parseInt(row.getValue('internalid', null, null)) || 0,
			"node": row.getValue('name', null, null) || '',
			"type": 'power',
			"customerid": 0,
			"customer": '',
			"soid": parseInt(row.getValue('custrecord_power_circuit_service_order', null, null)) || 0,
			"so": row.getValue('custrecord_power_circuit_service_order', null, null) || '',
			"serviceid": parseInt(row.getValue('custrecord_cologix_power_service', null, null)) || 0,
			"service": row.getValue('custrecord_cologix_power_service', null, null) || '',
			//"facilityid": parseInt(row.getValue('custrecord_cologix_power_facility', null, null)) || 0,
			//"facility": row.getText('custrecord_cologix_power_facility', null, null) || '',
			//"usage": parseFloat(row.getValue('custrecord_clgx_dcim_sum_day_amp_a_usg', null, null)) || 0,
			"faicon": "plug",
			"leaf": true
		});
		return true;
	});
	return arr;

}


function get_net (ids) {
	
	var arr = [];
	var columns = [];
	var filters = [];
	filters.push(new nlobjSearchFilter('custrecord_cologix_xc_service',null,'anyof', ids));
	var search = nlapiLoadSearch('customrecord_cologix_crossconnect', 'customsearch_clgx_cmd_customer_net');
	search.addColumns(columns);
	search.addFilters(filters);
	var result = search.runSearch();
	result.forEachResult(function(row) {
		arr.push({
			"nodeid": parseInt(row.getValue('internalid', null, 'GROUP')) || 0,
			"node": row.getValue('name', null, 'GROUP') || '',
			"type": 'net',
			"customerid": 0,
			"customer": '',
			"soid": parseInt(row.getValue('internalid', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', 'GROUP')) || 0,
			"so": row.getValue('tranid', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', 'GROUP') || '',
			"serviceid": parseInt(row.getValue('custrecord_cologix_xc_service', null, 'GROUP')) || 0,
			"service": row.getValue('custrecord_cologix_xc_service', null, 'GROUP') || '',
			"facilityid": parseInt(row.getValue('custrecord_clgx_xc_facility', null, 'GROUP')) || 0,
			"facility": row.getText('custrecord_clgx_xc_facility', null, 'GROUP') || '',
			"details": row.getText('custrecord_cologix_carrier_name', null, 'GROUP') || '',
			"faicon": "share-alt-square",
			"leaf": true
		});
		return true;
	});
	return arr;
}

function get_xcs(ids) {
	
	var arr = [];
	var columns = [];
	var filters = [];
	filters.push(new nlobjSearchFilter('custrecord_cologix_xc_service',null,'anyof', ids));
	var search = nlapiLoadSearch('customrecord_cologix_crossconnect', 'customsearch_clgx_cmd_customer_xcs');
	search.addColumns(columns);
	search.addFilters(filters);
	var result = search.runSearch();
	result.forEachResult(function(row) {
		arr.push({
			"nodeid": parseInt(row.getValue('internalid', null, 'GROUP')) || 0,
			"node": row.getValue('name', null, 'GROUP') || '',
			"type": 'xc',
			"customerid": 0,
			"customer": '',
			"soid": parseInt(row.getValue('internalid', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', 'GROUP')) || 0,
			"so": row.getValue('tranid', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', 'GROUP') || '',
			"serviceid": parseInt(row.getValue('custrecord_cologix_xc_service', null, 'GROUP')) || 0,
			"service": row.getValue('custrecord_cologix_xc_service', null, 'GROUP') || '',
			"facilityid": parseInt(row.getValue('custrecord_clgx_xc_facility', null, 'GROUP')) || 0,
			"facility": row.getText('custrecord_clgx_xc_facility', null, 'GROUP') || '',
			"details": row.getText('custrecord_cologix_carrier_name', null, 'GROUP') || '',
			"faicon": "share-alt-square",
			"leaf": true
		});
		return true;
	});
	return arr;
}

function get_vxcs(ids) {
	
	var arr = [];
	var columns = [];
	var filters = [];
	filters.push(new nlobjSearchFilter('custrecord_cologix_vxc_service',null,'anyof', ids));
	var search = nlapiLoadSearch('customrecord_cologix_vxc', 'customsearch_clgx_cmd_customer_vxcs');
	search.addColumns(columns);
	search.addFilters(filters);
	var result = search.runSearch();
	result.forEachResult(function(row) {
		arr.push({
			"nodeid": parseInt(row.getValue('internalid', null, null)) || 0,
			"node": row.getValue('name', null, null) || '',
			"type": 'vxc',
			"customerid": 0,
			"customer": '',
			"soid": parseInt(row.getValue('custrecord_cologix_service_order', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', null)) || 0,
			"so": row.getValue('custrecord_cologix_service_order', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', null) || '',
			"serviceid": parseInt(row.getValue('custrecord_cologix_vxc_service', null, null)) || 0,
			"service": row.getValue('custrecord_cologix_vxc_service', null, null) || '',
			"facilityid": parseInt(row.getValue('custrecord_cologix_vxc_facility', null, null)) || 0,
			"facility": row.getText('custrecord_cologix_vxc_facility', null, null) || '',
			"faicon": "share-alt",
			"leaf": true
		});
		return true;
	});
	return arr;
}


function get_managed(ids) {
	
	var arr = [];
	var columns = [];
	var filters = [];
	filters.push(new nlobjSearchFilter('custrecord_cologix_vxc_service',null,'anyof', ids));
	var search = nlapiLoadSearch('customrecord_cologix_vxc', 'customsearch_clgx_cmd_customer_vxcs');
	search.addColumns(columns);
	search.addFilters(filters);
	var result = search.runSearch();
	result.forEachResult(function(row) {
		arr.push({
			"nodeid": parseInt(row.getValue('internalid', null, null)) || 0,
			"node": row.getValue('name', null, null) || '',
			"type": 'vxc',
			"customerid": 0,
			"customer": '',
			"soid": parseInt(row.getValue('custrecord_cologix_service_order', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', null)) || 0,
			"so": row.getValue('custrecord_cologix_service_order', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', null) || '',
			"serviceid": parseInt(row.getValue('custrecord_cologix_vxc_service', null, null)) || 0,
			"service": row.getValue('custrecord_cologix_vxc_service', null, null) || '',
			"facilityid": parseInt(row.getValue('custrecord_cologix_vxc_facility', null, null)) || 0,
			"facility": row.getText('custrecord_cologix_vxc_facility', null, null) || '',
			"faicon": "share-alt",
			"leaf": true
		});
		return true;
	});
	return arr;
}