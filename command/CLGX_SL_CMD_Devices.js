nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Devices.js
//	Script Name:	CLGX_SL_CMD_Devices
//	Script Id:		customscript_clgx_sl_cmd_devices
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/30/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=742&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cmd_devices (request, response){
	try {
		
		var objFile = nlapiLoadFile(4514646);
		var html = objFile.getValue();
		response.write( html );
		
	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}
