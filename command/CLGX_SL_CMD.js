nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD.js
//	Script Name:	CLGX_SL_CMD
//	Script Id:		customscript_clgx_sl_cmd
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/15/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=726&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_clgx_cmd (request, response){
    try {

        var objFile = nlapiLoadFile(4435648);
        var html = objFile.getValue();
        html = html.replace(new RegExp('{strMenu}','g'), get_menus());
        response.write( html );
    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function get_menus(){

    var bar = ['<img valign=bottom width=75px height=30px src=/core/media/media.nl?id=4483948&c=1337135_SB1&h=feb3492970fffdba401a border=0/>',{
        "text": "Customers",
        "xtype": 'splitbutton',
        "iconCls": 'fa fa-users fa-lg blue1',
        "menu": [{
            "text": "Colo Services",
            "iconCls": 'fa fa-cogs fa-lg blue2',
            "bread": '/ Customers / ',
            "src": '/app/site/hosting/scriptlet.nl?script=723&deploy=1',
            "handler": 'menu'
        },{
            "text": "Inventory",
            "iconCls": 'fa fa-th fa-lg blue2',
            "menu": [{
                "text": "Spaces",
                "iconCls": 'fa fa-map fa-lg blue2',
                "bread": '/ Customers / Inventory / ',
                "src": '/app/site/hosting/scriptlet.nl?script=2203&deploy=1&type=spaces',
                "handler": 'menu'
            },{
                "text": "Powers",
                "iconCls": 'fa fa-plug fa-lg blue2',
                "bread": '/ Customers / Inventory / ',
                "src": '/app/site/hosting/scriptlet.nl?script=2203&deploy=1&type=powers',
                "handler": 'menu'
            },{
                "text": "Network",
                "iconCls": 'fa fa-share-alt-square fa-lg blue2',
                "bread": '/ Customers / Inventory / ',
                "src": '/app/site/hosting/scriptlet.nl?script=2203&deploy=1&type=network',
                "handler": 'menu'
            },{
                "text": "XCs",
                "iconCls": 'fa fa-share-alt-square fa-lg blue2',
                "bread": '/ Customers / Inventory / ',
                "src": '/app/site/hosting/scriptlet.nl?script=2203&deploy=1&type=xcs',
                "handler": 'menu'
            },{
                "text": "VXCs",
                "iconCls": 'fa fa-share-alt fa-lg blue2',
                "bread": '/ Customers / Inventory / ',
                "src": '/app/site/hosting/scriptlet.nl?script=2203&deploy=1&type=vxcs',
                "handler": 'menu'
            },'-',{
                "text": "Managed Services",
                "iconCls": 'fa fa-gears fa-lg blue2',
                "bread": '/ Customers / Inventory / Managed Services /',
                "src": '/app/site/hosting/scriptlet.nl?script=766&deploy=1&type=managed',
                "handler": 'menu',
                "menu": [{
                    "text": "Backup",
                    "iconCls": 'fa fa-gear fa-lg blue2',
                    "bread": '/ Customers / Inventory / Managed Services / ',
                    "src": '/app/site/hosting/scriptlet.nl?script=1581&deploy=1&type=1',
                    "handler": 'menu'
                },{
                    "text": "Licensing",
                    "iconCls": 'fa fa-gear fa-lg blue2',
                    "bread": '/ Customers / Inventory / Managed Services / ',
                    "src": '/app/site/hosting/scriptlet.nl?script=1581&deploy=1&type=2',
                    "handler": 'menu'
                },{
                    "text": "Load Balancer",
                    "iconCls": 'fa fa-gear fa-lg blue2',
                    "bread": '/ Customers / Inventory / Managed Services / ',
                    "src": '/app/site/hosting/scriptlet.nl?script=1581&deploy=1&type=3',
                    "handler": 'menu'
                },{
                    "text": "Router",
                    "iconCls": 'fa fa-gear fa-lg blue2',
                    "bread": '/ Customers / Inventory / Managed Services / ',
                    "src": '/app/site/hosting/scriptlet.nl?script=1581&deploy=1&type=4',
                    "handler": 'menu'
                },{
                    "text": "Router (Switch)",
                    "iconCls": 'fa fa-gear fa-lg blue2',
                    "bread": '/ Customers / Inventory / Managed Services / ',
                    "src": '/app/site/hosting/scriptlet.nl?script=1581&deploy=1&type=11',
                    "handler": 'menu'
                },{
                    "text": "Security",
                    "iconCls": 'fa fa-gear fa-lg blue2',
                    "bread": '/ Customers / Inventory / Managed Services / ',
                    "src": '/app/site/hosting/scriptlet.nl?script=1581&deploy=1&type=5',
                    "handler": 'menu'
                },{
                    "text": "Server",
                    "iconCls": 'fa fa-gear fa-lg blue2',
                    "bread": '/ Customers / Inventory / Managed Services / ',
                    "src": '/app/site/hosting/scriptlet.nl?script=1581&deploy=1&type=6',
                    "handler": 'menu'
                },{
                    "text": "Storage",
                    "iconCls": 'fa fa-gear fa-lg blue2',
                    "bread": '/ Customers / Inventory / Managed Services / ',
                    "src": '/app/site/hosting/scriptlet.nl?script=1581&deploy=1&type=7',
                    "handler": 'menu'
                },{
                    "text": "Virtual DC",
                    "iconCls": 'fa fa-gear fa-lg blue2',
                    "bread": '/ Customers / Inventory / Managed Services / ',
                    "src": '/app/site/hosting/scriptlet.nl?script=1581&deploy=1&type=8',
                    "handler": 'menu'
                },{
                    "text": "Virtual Server",
                    "iconCls": 'fa fa-gear fa-lg blue2',
                    "bread": '/ Customers / Inventory / Managed Services / ',
                    "src": '/app/site/hosting/scriptlet.nl?script=1581&deploy=1&type=9',
                    "handler": 'menu'
                }/*,'-',{
			    	"text": "DNS Management",
			    	"iconCls": 'fa fa-cubes fa-lg blue2',
			    	"menu": [{
			    		"text": "Forward DNS",
			    		"iconCls": 'fa fa-cube fa-lg blue2',
			    		"bread": '/ Customers / Inventory / Managed Services / ',
			    		"src": '/app/site/hosting/scriptlet.nl?script=824&deploy=1&type=vxcs',
			    		"handler": 'menu'
				    },{
			    		"text": "Reverse DNS",
			    		"iconCls": 'fa fa-tag fa-lg blue2',
			    		"bread": '/ Customers / Inventory / Managed Services / ',
			    		"src": '/app/site/hosting/scriptlet.nl?script=823&deploy=1&type=vxcs',
			    		"handler": 'menu'
				    }]
			    }*/]
            }]
        },/*{
    		"text": "Notifications",
    		"iconCls": 'fa fa-at fa-lg blue2',
    		"menu": [{
	    		"text": "Spaces",
	    		"iconCls": 'fa fa-map-o fa-lg blue2',
	    		"bread": '/ Customers / Notifications / Spaces / ',
	    		"menu": get_menu_notifications(776)
		    },{
	    		"text": "Powers",
	    		"iconCls": 'fa fa-plug fa-lg blue2',
	    		"bread": '/ Customers / Notifications / Powers / ',
	    		"menu": get_menu_notifications(808)
		    }]
	    },*/{
            "text": "Portal Stats",
            "iconCls": 'fa fa-calendar fa-lg blue2',
            "bread": '/ Customers / ',
            "src": '/app/site/hosting/scriptlet.nl?script=750&deploy=1',
            "handler": 'menu'
        }]
    },{
        "text": "DCIM",
        "xtype": 'splitbutton',
        "iconCls": 'fa fa-cogs fa-lg blue1',
        "menu": [{
            "text": "Devices",
            "iconCls": 'fa fa-cogs fa-lg blue1',
            "bread": '/ DCIM / ',
            "src": '/app/site/hosting/scriptlet.nl?script=742&deploy=1',
            "handler": 'menu'
        },{
            "text": "FAMs",
            //"xtype": 'splitbutton',
            "iconCls": 'fa fa-cog fa-lg blue1',
            "bread": '/ DCIM / ',
            "menu": get_menu_fams()
        },'-',{
            "text": "Power Chain",
            //"xtype": 'splitbutton',
            "iconCls": 'fa fa-sitemap fa-lg blue1',
            "bread": '/ DCIM / ',
            "menu": get_menu_pwr_trees(1515)
        },{
            "text": "Power Utilisation",
            //"xtype": 'splitbutton',
            "iconCls": 'fa fa-bolt fa-lg blue1',
            "bread": '/ DCIM / ',
            "menu": get_menu_pwr_trees(732)
        },{
            "text": "Power Reports",
            //"xtype": 'splitbutton',
            "iconCls": 'fa fa-plug fa-lg blue1',
            "menu": [{
                "text": "Draw Exceeded (Amps)",
                "iconCls": 'fa fa-plug fa-lg blue2',
                "bread": '/ DCIM / Power Reports / ',
                "src": '/app/site/hosting/scriptlet.nl?script=737&deploy=1',
                "handler": 'menu'
            },{
                "text": "Peak Exceeded (kW)",
                "iconCls": 'fa fa-plug fa-lg blue2',
                "bread": '/ DCIM / Power Reports / ',
                "src": '/app/site/hosting/restlet.nl?script=839&deploy=1',
                "handler": 'menu'
            }]
        },'-',{
            "text": "Active Alarms",
            "iconCls": 'fa fa-exclamation-circle fa-lg Crimson1',
            "bread": '/ ',
            "src": '/core/media/media.nl?id=4436551&c=1337135&h=7d5d7aa420a529019fc6&_xt=.html',
            "handler": 'menu'
        }]
    }/*,{
		"text": "CP Stats",
		"xtype": 'splitbutton',
		"iconCls": 'fa fa-bar-chart fa-lg blue1',
        "menu": [{
    		"text": "By Date",
    		"iconCls": 'fa fa-calendar  fa-lg blue2',
    		"bread": '/ CP Stats / ',
    	    "handler": 'menu'
	    },{
    		"text": "By Customers",
    		"iconCls": 'fa fa-users fa-lg blue2',
    		"bread": '/ CP Stats / ',
    		"handler": 'menu'
	    }]
	}*/];


    var currentContext = nlapiGetContext();
    role = currentContext.getRole();

    if(role == -5 || role == 3 || role == 18 || role == 1003 || role == 1004 || role == 1039 || role == 1054 || role == 1088){
        bar[1].menu.push({
            "text": "Notifications",
            "iconCls": 'fa fa-at fa-lg blue2',
            "menu": [{
                "text": "Spaces",
                "iconCls": 'fa fa-map-o fa-lg blue2',
                "bread": '/ Customers / Notifications / Spaces / ',
                "menu": get_menu_notifications(776)
            },{
                "text": "Powers",
                "iconCls": 'fa fa-plug fa-lg blue2',
                "bread": '/ Customers / Notifications / Powers / ',
                //"menu": get_menu_notifications(808)
                "menu": get_menu_notifications(2214)
            },{
                "text": "Equipment",
                "iconCls": 'fa fa-gears fa-lg blue2',
                "bread": '/ Customers / Notifications / Equipment / ',
                "menu": get_menu_notifications(1593)
            },{
                "text": "Network",
                "iconCls": 'fa fa-gears fa-lg blue2',
                "bread": '/ Customers / Notifications / Network/ ',
                "menu": get_menu_notifications(2175)
            }]
        });
    }

    var strBar = JSON.stringify(bar);
    strBar = strBar.replace(new RegExp('"menu"','g'),'menu');

    return strBar;

}


function get_menu_fams(){

    var columns = [];
    columns.push(new nlobjSearchColumn('custrecord_cologix_location_subsidiary','custrecord_assetfacility','GROUP').setSort(false));
    columns.push(new nlobjSearchColumn('custrecord_clgx_facilty_market','custrecord_assetfacility','GROUP').setSort(false));
    var filters = [];
    var searchMarkets = nlapiSearchRecord('customrecord_ncfar_asset', null, filters, columns);

    var markets = [];
    for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {

        var subsidiaryid = parseInt(searchMarkets[i].getValue('custrecord_cologix_location_subsidiary','custrecord_assetfacility','GROUP'));
        var marketid = parseInt(searchMarkets[i].getValue('custrecord_clgx_facilty_market','custrecord_assetfacility','GROUP'));
        var market = searchMarkets[i].getText('custrecord_clgx_facilty_market','custrecord_assetfacility','GROUP');

        var columns = [];
        columns.push(new nlobjSearchColumn('internalid','custrecord_assetfacility','GROUP'));
        columns.push(new nlobjSearchColumn('name','custrecord_assetfacility','GROUP').setSort(false));
        var filters = [];
        filters.push(new nlobjSearchFilter('custrecord_clgx_facilty_market','custrecord_assetfacility',"anyof",marketid));
        var searchFacilities = nlapiSearchRecord('customrecord_ncfar_asset', null, filters, columns);

        var facilities = new Array();
        for ( var j = 0; searchFacilities != null && j < searchFacilities.length; j++ ) {

            var facilityid = searchFacilities[j].getValue('internalid','custrecord_assetfacility','GROUP');
            var facility = searchFacilities[j].getValue('name','custrecord_assetfacility','GROUP');

            facilities.push({
                "text": facility,
                "node": parseInt(searchFacilities[j].getValue('internalid','custrecord_assetfacility','GROUP')),
                "bread": '/ DCIM / FAMs / ' + market + ' / ',
                "iconCls": 'fa fa-building-o fa-lg blue2',
                "src": '/app/site/hosting/scriptlet.nl?script=741&deploy=1&id=&root=' + facilityid,
                "handler": 'menu'
            });
        }
        if(subsidiaryid == 2789){
            var faicon = 'flag-icon flag-icon-ca';
        } else {
            var faicon = 'flag-icon flag-icon-us';
        }
        markets.push({
            "text": market,
            "iconCls": faicon,
            "menu": facilities
        });
    }
    return markets;
}


function get_menu_pwr_trees(script){

    if(script == 1515){
        var bread = '/ DCIM / Power Chain / ';
        var icon = 'sitemap';
    } else {
        var bread = '/ DCIM / BCM Usage / ';
        var icon = 'bolt';
    }

    var columns = [];
    columns.push(new nlobjSearchColumn('custrecord_cologix_location_subsidiary','custrecord_clgx_dcim_device_facility','GROUP').setSort(false));
    columns.push(new nlobjSearchColumn('custrecord_clgx_facilty_market','custrecord_clgx_dcim_device_facility','GROUP').setSort(false));
    var filters = [];
    var searchMarkets = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, filters, columns);

    var markets = [];
    for ( var i = 0; searchMarkets != null && i < searchMarkets.length; i++ ) {

        var subsidiaryid = parseInt(searchMarkets[i].getValue('custrecord_cologix_location_subsidiary','custrecord_clgx_dcim_device_facility','GROUP'));
        var marketid = parseInt(searchMarkets[i].getValue('custrecord_clgx_facilty_market','custrecord_clgx_dcim_device_facility','GROUP'));
        var market = searchMarkets[i].getText('custrecord_clgx_facilty_market','custrecord_clgx_dcim_device_facility','GROUP');

        var columns = [];
        columns.push(new nlobjSearchColumn('custrecord_clgx_dcim_device_pwr_tree',null,'GROUP').setSort(false));
        var filters = [];
        filters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device_pwr_tree',null,"noneof",'@NONE@'));
        filters.push(new nlobjSearchFilter('custrecord_clgx_facilty_market','custrecord_clgx_dcim_device_facility',"anyof",marketid));
        var searchFacilities = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, filters, columns);

        var facilities = new Array();
        for ( var j = 0; searchFacilities != null && j < searchFacilities.length; j++ ) {

            var treeid = parseInt(searchFacilities[j].getValue('custrecord_clgx_dcim_device_pwr_tree',null,'GROUP'));
            var tree = searchFacilities[j].getText('custrecord_clgx_dcim_device_pwr_tree',null,'GROUP');

            var columns = [];
            columns.push(new nlobjSearchColumn('internalid',null,null).setSort(false));
            var filters = [];
            filters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device_root',null,"is",'T'));
            filters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device_pwr_tree',null,"anyof",treeid));
            filters.push(new nlobjSearchFilter('custrecord_clgx_facilty_market','custrecord_clgx_dcim_device_facility',"anyof",marketid));
            var searchRoots = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, filters, columns);
            var roots = [];
            for ( var k = 0; searchRoots != null && k < searchRoots.length; k++ ) {
                roots.push(parseInt(searchRoots[k].getValue('internalid',null,null)));
            }

            if(script == 1515){
                var src = '/app/site/hosting/restlet.nl?script=1515&deploy=1&tree=' + treeid;
            } else {
                var src = '/app/site/hosting/scriptlet.nl?script=' + script + '&deploy=1&treeid=' + treeid + '&tree=' + tree + '&root=' + roots.join();
            }

            facilities.push({
                "text": tree,
                "bread": bread + market + ' / ',
                "src": src,
                "iconCls": 'fa fa-' + icon + ' fa-lg blue2',
                "handler": 'menu'
            });
        }

        if(subsidiaryid == 2789){
            var faicon = 'flag-icon flag-icon-ca';
        } else {
            var faicon = 'flag-icon flag-icon-us';
        }
        markets.push({
            "text": market,
            "iconCls": faicon,
            "menu": facilities
        });
    }
    return markets;
}

function get_menu_notifications(script){

    if(script == 776){
        var section = 'Spaces';
    }
    else if (script == 808) {
        var section = 'Powers';
    }
    else if (script == 2175) {
        var section = 'Network';
    }
    else {
        var section = 'Equipment';
    }
    var search = nlapiSearchRecord('customrecord_cologix_facility', 'customsearch_clgx_cmd_market_facilities');
    var rows = [];
    for ( var i = 0; search != null && i < search.length; i++ ) {
        rows.push({
            "subsidiaryid": parseInt(search[i].getValue('custrecord_cologix_location_subsidiary',null,null)) || 0,
            "marketid": parseInt(search[i].getValue('custrecord_clgx_facilty_market',null,null)) || 0,
            "market": search[i].getText('custrecord_clgx_facilty_market',null,null) || '',
            "facilityid": parseInt(search[i].getValue('internalid',null,null)) || 0,
            "facility": search[i].getValue('name',null,null) || '',
        });
    }
    var ids = _.uniq(_.pluck(rows, 'marketid'));
    var markets = [];
    for ( var i = 0; ids != null && i < ids.length; i++ ) {

        var marketFacilities = _.filter(rows, function(arr){
            return (arr.marketid == ids[i]);
        });
        var facilities = [];
        for ( var j = 0; marketFacilities != null && j < marketFacilities.length; j++ ) {

            facilities.push({
                "text": marketFacilities[j].facility,
                "node": marketFacilities[j].facility,
                "bread": '/ Customers / Notifications / ' + section + ' / ' +  marketFacilities[j].market + ' / ',
                "iconCls": 'fa fa-building-o fa-lg blue2',
                "src": '/app/site/hosting/scriptlet.nl?script=' + script + '&deploy=1&facilityid=' + marketFacilities[j].facilityid,
                "handler": 'menu'
            });
        }
        if(marketFacilities[0].subsidiaryid == 2789){
            var faicon = 'flag-icon flag-icon-ca';
        } else {
            var faicon = 'flag-icon flag-icon-us';
        }
        markets.push({
            "text": marketFacilities[0].market,
            "iconCls": faicon,
            "menu": facilities
        });

    }

    return markets;

}