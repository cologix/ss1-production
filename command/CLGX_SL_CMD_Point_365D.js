nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Point_365D.js
//	Script Name:	CLGX_SL_CMD_Point_365D
//	Script Id:		customscript_clgx_sl_cmd_point_365d
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/01/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=747&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_clgx_sl_cmd_point_365d (request, response){
	try {
		
		var pointid = request.getParameter('pointid');
		var html = '';
		
		var start = moment().subtract('days', 365).format('YYYY-MM-DD');
    	var end = moment().format('YYYY-MM-DD');
    	
		if(pointid > 0){
			
			var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/devices/get_point_hours_365_days.cfm?pointid=' + pointid + '&start=' + start + '&end=' + end);
			var strJSON = requestURL.body;
			var arrValues = JSON.parse(strJSON);
			
			var objFile = nlapiLoadFile(4528878);
			html = objFile.getValue();
			html = html.replace(new RegExp('{hoursJSON}','g'), JSON.stringify(arrValues));
		}

		response.write( html );
	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}
