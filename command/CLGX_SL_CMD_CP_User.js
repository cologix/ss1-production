nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_CP_User.js
//	Script Name:	CLGX_SL_CMD_CP_User
//	Script Id:		customscript_clgx_sl_cmd_cp_user
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/08/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=752&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_clgx_sl_cmd_cp_user (request, response){
    try {

    	var id = request.getParameter('id') || '';
    	
		var request = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/customer_portal/get_logs_user.cfm?id=' + id);
        var json = JSON.parse( request.body );
        
        var years = _.uniq(_.pluck(json, 'year'));
        
        var Y = [];
		for ( var y = 0; y < years.length; y++ ) {
			
			var MONTHS = _.filter(json, function(arr){
				return (arr.year == years[y]);
			});
			var months = _.uniq(_.pluck(MONTHS, 'month'));
			
			var M = [];
			for ( var m = 0; m < months.length; m++ ) {
				
				var DAYS = _.filter(MONTHS, function(arr){
					return (arr.month == months[m]);
				});
				var days = _.uniq(_.pluck(DAYS, 'day'));
				
				var D = [];
				for ( var d = 0; d < days.length; d++ ) {
					
					var ACTS = _.filter(DAYS, function(arr){
						return (arr.day == days[m]);
					});
					
					var A = [];
					for ( var a = 0; a < ACTS.length; a++ ) {
						
						A.push({
							"node": ACTS[a].act,
							"type": 'act',
							"date": months[m] + '/' + DAYS[d].day + '/' + years[y],
							"act": ACTS[a].act,
							"record": ACTS[a].type,
							"field": ACTS[a].field,
							"date": ACTS[a].inserted,
							"faicon": "pencil-square-o blue1",
							"leaf": true,
						});
					}
					
					D.push({
						"node": days[d],
						"type": 'day',
						"faicon": "calendar-o blue1",
						"expanded": false,
						"leaf": false,
						"children": A
					});
				}
				M.push({
					"node":months[m],
					"type": 'month',
					"faicon": "calendar blue1",
					"expanded": false,
					"leaf": false,
					"children": D
				});
			}
			Y.push({
				"node":years[y],
				"type": 'year',
				"faicon": "calendar blue2",
				"expanded": false,
				"leaf": false,
				"children": M
			});
		}
        
		var tree = {
				"text": ".",
				"children": Y
		};
		
		
        var objFile = nlapiLoadFile(4592612);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{treeUser}','g'), JSON.stringify(tree));
		response.write( html );

    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
