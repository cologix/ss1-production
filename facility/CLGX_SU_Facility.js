nlapiLogExecution("audit","FLOStart",new Date().getTime());
function afterSubmit (type) {
	try {
		//------------- NNJ SYNC -----------------------------------------------------------------------------------
		var currentContext = nlapiGetContext();
        var environment = currentContext.getEnvironment();
        var row = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
        if (environment == 'PRODUCTION' && (type == 'xedit'  || currentContext.getExecutionContext() == 'userinterface')) {
        	
			var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
			record.setFieldValue('custrecord_clgx_queue_ping_record_id', nlapiGetRecordId());
			record.setFieldValue('custrecord_clgx_queue_ping_record_type', 8);
			record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
			var idRec = nlapiSubmitRecord(record, false,true);
        	
        	/*
			try {
				flexapi('POST','/netsuite/update', {
					'type': nlapiGetRecordType(),
					'id': nlapiGetRecordId(),
					'action': type,
					'userid': nlapiGetUser(),
					'user': nlapiLookupField('employee', nlapiGetUser(), 'entityid'),
					'data': json_serialize(row)
				});
			}
			catch (error) {
				nlapiLogExecution('DEBUG', 'flexapi error: ', error);
				var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
				record.setFieldValue('custrecord_clgx_queue_ping_record_id', nlapiGetRecordId());
				record.setFieldValue('custrecord_clgx_queue_ping_record_type', 8);
				record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
				var idRec = nlapiSubmitRecord(record, false,true);
			}
			*/
    	}
	}
	catch (error) {
	    if (error.getDetails != undefined){
	        nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	        throw error;
	    }
	    else{
	        nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	        throw nlapiCreateError('99999', error.toString());
	    }
	} 
}