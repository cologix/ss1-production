nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Power_Usage.js
//	Script Name:	CLGX_SU_Power_Usage
//	Script Id:		customscript_clgx_su_power_usage
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Record
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		3/14/2012
//-------------------------------------------------------------------------------------------------

function beforeLoad (type) {
	try {
		nlapiLogExecution('DEBUG','User Event - beforeLoad','|--------------------STARTED---------------------|');
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 3/14/2012
// Details:	Search if the Power Circuit have already a last Power Usage reading related.  If it does, it will edit this last reading record and alert. If not, it will get SO details and calculate fields and allow to create the new/first power usage record.
//-----------------------------------------------------------------------------------------------------------------

		var currentContext = nlapiGetContext();
		if (currentContext.getExecutionContext() == 'userinterface') {
			
			var stSOID = nlapiGetFieldValue('custrecord_clgx_pwusg_service_order');
			var stPowerCircuitID = nlapiGetFieldValue('custrecord_clgx_pwusg_power_circuit');
			//var isLastRead = nlapiGetFieldValue('custrecord_clgx_pwusg_this_is_last_read');
			
			if (type == 'create') { // an attempt to create a new power usage record is done ---------------------------------------------------------
				if (stSOID == null || stSOID == ''){// if this power usage is not linked to a SO - send back to the power circuit and alert
					var params = new Array();
					params['so'] = 'F';
					params['ns'] = 'T';
					params['e'] = 'T';
					nlapiSetRedirectURL('RECORD','customrecord_clgx_power_circuit', stPowerCircuitID, null, params);
				}
				else if (stPowerCircuitID == null || stPowerCircuitID == ''){ // the record creation did not came from a power circuit
					nlapiSetRedirectURL('RECORD','customrecord_clgx_power_circuit');
				}
				else{
					// get details from existing SO
	  				var arrSODetails = nlapiLookupField('salesorder',stSOID,['custbody_clgx_pwusg_meter_power_usage','custbody_clgx_pwusg_commit_util_kwh','custbody_clgx_pwusg_commit_util_rate', 'custbody_clgx_pwusg_util_rate', 'custbody_clgx_pwusg_commit_gen_kwh', 'custbody_clgx_pwusg_commit_gen_rate', 'custbody_clgx_pwusg_gen_rate']);
	                var stSOMetered = arrSODetails['custbody_clgx_pwusg_meter_power_usage'];

	                // if stSOMetered is 'F' redirect and alert?
					if (stSOMetered == 'F'){// if this SO does not have a power usage monitoring - send back to the power circuit and alert
						var params = new Array();
						params['so'] = 'T';
						params['ns'] = 'T';
						params['e'] = 'T';
						nlapiSetRedirectURL('RECORD','customrecord_clgx_power_circuit', stPowerCircuitID, null, params);
					}
	                
					// get details from existing Power circuit
	  				var arrPwrCircuitDetails = nlapiLookupField('customrecord_clgx_power_circuit',stPowerCircuitID,['custrecord_cologix_init_power_meter_read']);
	                var stInitialReading = arrPwrCircuitDetails['custrecord_cologix_init_power_meter_read'];
	                
					//nlapiSetFieldValue('custrecord_clgx_pwusg_this_is_last_read', 'T');
					nlapiSetFieldValue('custrecord_clgx_pwusg_meter_read_date', getDateToday());
					nlapiSetFieldValue('custrecord_clgx_pwusg_commit_util_kwh', arrSODetails['custbody_clgx_pwusg_commit_util_kwh']);
					nlapiSetFieldValue('custrecord_clgx_pwusg_commit_util_rate', arrSODetails['custbody_clgx_pwusg_commit_util_rate']);
					nlapiSetFieldValue('custrecord_clgx_pwusg_util_rate', arrSODetails['custbody_clgx_pwusg_util_rate']);
					nlapiSetFieldValue('custrecord_clgx_pwusg_commit_gen_kwh', arrSODetails['custbody_clgx_pwusg_commit_gen_kwh']);
					nlapiSetFieldValue('custrecord_clgx_pwusg_commit_gen_rate', arrSODetails['custbody_clgx_pwusg_commit_gen_rate']);
					nlapiSetFieldValue('custrecord_clgx_pwusg_gen_rate', arrSODetails['custbody_clgx_pwusg_gen_rate']);
					
					//search if the Power Circuit has already a last Power Usage record related
					var arrColumns = new Array();
					var arrFilters = new Array();
					arrColumns[0] = new nlobjSearchColumn('internalid', null, null); // last power usage ID if any
					arrFilters[0] = new nlobjSearchFilter('custrecord_clgx_pwusg_power_circuit', null, 'is', stPowerCircuitID);
					var searchResults = nlapiSearchRecord('customrecord_clgx_power_usage', null, arrFilters, arrColumns);
					if (searchResults != null){ // if power usage records exist for this power circuit find the last one
						
						var stLastPwrUsageID = 0;
						for ( var i = 0; i<searchResults.length; i++ ) {
							var stRecordId = searchResults[i].getValue('internalid');
							if (parseInt(stRecordId) > parseInt(stLastPwrUsageID)){
								stLastPwrUsageID = stRecordId;
							}
						}
						
						// get details from existing last power usage
		  				var arrPwrUsgDetails = nlapiLookupField('customrecord_clgx_power_usage',stLastPwrUsageID,['custrecord_clgx_pwusg_current_read']);
		                var stPwrUsgLastRead = arrPwrUsgDetails['custrecord_clgx_pwusg_current_read'];

						nlapiSetFieldValue('custrecord_clgx_pwusg_previous_id', stLastPwrUsageID);
						nlapiSetFieldValue('custrecord_clgx_pwusg_previous_read', stPwrUsgLastRead);
						nlapiSetFieldValue('custrecord_clgx_pwusg_current_read', stPwrUsgLastRead);
					}
					else{ // this is the first power usage, so take the initial reading from the power circuit
						nlapiSetFieldValue('custrecord_clgx_pwusg_previous_id', '0'); // there was no previous power usage reading ID
						nlapiSetFieldValue('custrecord_clgx_pwusg_previous_read', stInitialReading);
						nlapiSetFieldValue('custrecord_clgx_pwusg_current_read', stInitialReading);
					}
				}
			}
			
			else if (type == 'edit') { // a power usage record is edited ---------------------------------------------------------------
				
				if (stPowerCircuitID != null){ // this is linked to a power circuit
				
					var stPwrUsgID = nlapiGetRecordId();

					
					//search if the Power Circuit has already a last Power Usage record related
					var arrColumns = new Array();
					var arrFilters = new Array();
					arrColumns[0] = new nlobjSearchColumn('internalid', null, null); // last power usage ID if any
					arrFilters[0] = new nlobjSearchFilter('custrecord_clgx_pwusg_power_circuit', null, 'is', stPowerCircuitID);
					var searchResults = nlapiSearchRecord('customrecord_clgx_power_usage', null, arrFilters, arrColumns);
					if (searchResults != null){ // if power usage records exist for this power circuit find the last one
						
						var stLastPwrUsageID = 0;
						for ( var i = 0; i<searchResults.length; i++ ) {
							var stRecordId = searchResults[i].getValue('internalid');
							if (parseInt(stRecordId) > parseInt(stLastPwrUsageID)){
								stLastPwrUsageID = stRecordId;
							}
						}
					
						if (stPwrUsgID != stLastPwrUsageID){ // this is not the last one - redirect to last power usage reading record and alert
							
							var params = new Array();
							params['pu'] = 'T';
							params['so'] = 'T';
							params['e'] = 'T';
							nlapiSetRedirectURL('RECORD','customrecord_clgx_power_usage', stLastPwrUsageID, null, params);
							
						}
						else{
							//this is the last one, do nothing - allow editing
						}
				}
				else{ // this is not linked to a power circuit and may not be edited - send to SO
					nlapiSetRedirectURL('RECORD','salesorder', stSOID, null, null);
				}

			}
			
		}
	}
//---------- End Section 1 ------------------------------------------------------------------------------------------------


		nlapiLogExecution('DEBUG','User Event - beforeLoad','|--------------------FINISHED---------------------|');  
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function afterSubmit () {
	try {
		nlapiLogExecution('DEBUG','User Event - afterSubmit','|--------------------STARTED---------------------|');
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 3/14/2012
// Details:	Create the next month power usage record when the technician will read and save the current month meter reading. It will also write the current month meter reading into the 'Previous month meter reading' field of the next month newly created record
//-----------------------------------------------------------------------------------------------------------------

		var currentContext = nlapiGetContext();
		if (currentContext.getExecutionContext() == 'userinterface') {
			
			var stLastPowerUsageID = nlapiGetFieldValue('custrecord_clgx_pwusg_previous_id');
			if (type == 'create') {
				
				if (stLastPowerUsageID != '') { // this is not the first power usage for this power circuit
					
					var myRecord = nlapiLoadRecord('customrecord_clgx_power_usage', stLastPowerUsageID);
					myRecord.setFieldValue('custrecord_clgx_pwusg_this_is_last_read', 'F');
					nlapiSubmitRecord(myRecord);
				}
			}
			else{}
		}

//---------- End Section 1 ------------------------------------------------------------------------------------------------


		nlapiLogExecution('DEBUG','User Event - afterSubmit','|--------------------FINISHED---------------------|');  
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function getDateToday(){
    var date = new Date();
    var month = parseInt(date.getMonth()) + 1;
    var day = date.getDate();
    var year = date.getFullYear();
    
    if(parseInt(month) < 10){
        month = '0' + month;
    }
    
    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = month + '/' + day + '/' + year;
    
    return formattedDate;
}