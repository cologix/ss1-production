//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_LIB_Power_Circuit_Update.js
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/15/2017
//-------------------------------------------------------------------------------------------------

function update_power (powerid){

	var deviceid = 0;
	var pdpm = '';
	
	var power = nlapiLoadRecord('customrecord_clgx_power_circuit',powerid);
	var panelid = power.getFieldValue('custrecord_clgx_power_panel_pdpm');
	var volts = power.getFieldText('custrecord_cologix_power_volts');
	
	if(panelid != null && panelid != ''){
		
		// go to FAM record and see if it's linked to a device and if pdpm or vertical
		var fields = ['custrecord_clgx_od_fam_device','custrecord_clgx_panel_type'];
		var columns = nlapiLookupField('customrecord_ncfar_asset', panelid, fields);
		var deviceonfam = columns.custrecord_clgx_od_fam_device;
		var paneltype = columns.custrecord_clgx_panel_type;
		
		if(deviceonfam != null && deviceonfam != ''){
			deviceid = deviceonfam;
		}
		
		if(deviceid > 0){ // the fam/panel is linked to a device
				
			var module = power.getFieldText('custrecord_cologix_power_upsbreaker');
			var breaker = power.getFieldText('custrecord_cologix_power_circuitbreaker');
			var line = power.getFieldValue('custrecord_cologix_power_subpanel');
			
			// Build Points Array ------------------------------------------------------------------------------------------------------------------------------------------------
			var arrPoints = new Array();
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_device",null,"anyof",deviceid));
			var searchPoints = nlapiLoadSearch('customrecord_clgx_dcim_points', 'customsearch_clgx_dcim_pwrs_update_point');
			searchPoints.addFilters(arrFilters);
			var resultSet = searchPoints.runSearch();
			resultSet.forEachResult(function(searchResult) {
				var columns = searchResult.getAllColumns();
				var objPoint = new Object();
				objPoint["pointid"] = parseInt(searchResult.getValue('internalid',null,null));
				objPoint["pointextid"] = parseInt(searchResult.getValue('externalid',null,null));
				objPoint["point"] = searchResult.getValue('name',null,null);
				arrPoints.push(objPoint);
				return true;
			});
			var arrAMPPoints = new Array();
			var arrKWPoints = new Array();
			var arrKWHPoints = new Array();
			var arrFCTPoints = new Array();
			var arrVLTPoints = new Array();
			
// =============================================== PDPM ======================================================================================
			
			if(paneltype == 2){

				// examples:
				//Module 01 Breaker 1 Current
				//Module 01 Breaker 1 Power
				//Module 01 Energy since Reset
				
				// these voltages  - only one breaker
				if(volts == '120V Single Phase' || volts == '240V Single Phase'){
					var ampspoint = 'Module ' + ('0' + module).slice(-2) + ' Breaker ' + line + ' Current';
					var objPoint = _.find(arrPoints, function(arr){ return (arr.point == ampspoint); });
					if(objPoint != null){
						arrAMPPoints.push(objPoint.pointid);
					}
					var kwpoint = 'Module ' + ('0' + module).slice(-2) + ' Breaker ' + line + ' Power';
					var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwpoint); });
					if(objPoint != null){
						arrKWPoints.push(objPoint.pointid);
					}
				}
				// this voltages - 2 breakers
				else if (volts == '208V Single Phase'){
					var arrBreakers = new Array();
					if (line == 8){ // Lines 1, 2
						arrBreakers = [1,2];
						var line2 = 1;
					}
					if (line == 9){ // Lines 2, 3
						arrBreakers = [2,3];
						var line2 = 2;
					}
					if (line == 10){ // Lines 1, 3
						arrBreakers = [1,3];
					}
					for ( var i = 0; arrBreakers != null && i < arrBreakers.length; i++ ) {
						var ampspoint = 'Module ' + ('0' + module).slice(-2) + ' Breaker ' + arrBreakers[i] + ' Current';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == ampspoint); });
						if(objPoint != null){
							arrAMPPoints.push(objPoint.pointid);
						}
						var kwpoint = 'Module ' + ('0' + module).slice(-2) + ' Breaker ' + arrBreakers[i] + ' Power';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwpoint); });
						if(objPoint != null){
							arrKWPoints.push(objPoint.pointid);
						}
					}
				}
				// these voltages - 3 breakers
				else if (volts == '208V Three Phase' || volts == '240V Three Phase'){
					for ( var i = 1;  i < 4; i++ ) {
						var ampspoint = 'Module ' + ('0' + module).slice(-2) + ' Breaker ' + i + ' Current';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == ampspoint); });
						if(objPoint != null){
							arrAMPPoints.push(objPoint.pointid);
						}
						var kwpoint = 'Module ' + ('0' + module).slice(-2) + ' Breaker ' + i + ' Power';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwpoint); });
						if(objPoint != null){
							arrKWPoints.push(objPoint.pointid);
						}
					}
				}
				else{}
				
				// when panel is PDPM, there is only one KWH point per module, not by breaker
				var kwhpoint = 'Module ' + ('0' + module).slice(-2) + ' Energy since Reset';
				var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwhpoint); });
				if(objPoint != null){
					arrKWHPoints.push(objPoint.pointid);
				}
				
			}
			

// =============================================== Virtual PDPM ======================================================================================
			
			if(paneltype == 12){

				// these voltages  - only one breaker
				if(volts == '120V Single Phase' || volts == '240V Single Phase'){
					
					var ampspoint = ('0' + module).slice(-2) + '.00' + line + '.AMP';
					var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == ampspoint); });
					if(objPoint != null){
						arrAMPPoints.push(objPoint.pointid);
					}
					var kwpoint = ('0' + module).slice(-2) + '.00' + line + '.RPW';
					var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == kwpoint); });
					if(objPoint != null){
						arrKWPoints.push(objPoint.pointid);
					}

				}
				// these voltages - 2 breakers
				else if (volts == '208V Single Phase'){
					
					var arrBreakers = [];
					if (line == 8){ // Lines 1, 2
						arrBreakers = [1,2];
						var line2 = 1;
					}
					if (line == 9){ // Lines 2, 3
						arrBreakers = [2,3];
						var line2 = 2;
					}
					if (line == 10){ // Lines 1, 3
						arrBreakers = [1,3];
					}
					
					for ( var i = 0; i < 2; i++ ) {
						var ampspoint = ('0' + module).slice(-2) + '.00' + arrBreakers[i] + '.AMP';
						var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == ampspoint); });
						if(objPoint != null){
							arrAMPPoints.push(objPoint.pointid);
						}
						var kwpoint = ('0' + module).slice(-2) + '.00' + arrBreakers[i] + '.RPW';
						var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == kwpoint); });
						if(objPoint != null){
							arrKWPoints.push(objPoint.pointid);
						}
					}
				}
				// these voltages - 3 breakers
				else if (volts == '208V Three Phase' || volts == '240V Three Phase' || volts == '600V Three Phase'){
					for ( var i = 1;  i < 4; i++ ) {
						var ampspoint = ('0' + module).slice(-2) + '.00' + i + '.AMP';
						var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == ampspoint); });
						if(objPoint != null){
							arrAMPPoints.push(objPoint.pointid);
						}
						var kwpoint = ('0' + module).slice(-2) + '.00' + i + '.RPW';
						var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == kwpoint); });
						if(objPoint != null){
							arrKWPoints.push(objPoint.pointid);
						}
					}
				}
				else{}
			
				// when panel is PDPM, there is only one KWH point per module, not by breaker
				var kwhpoint = ('0' + module).slice(-2) + '.000.KWH';
				var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwhpoint); });
				if(objPoint != null){
					arrKWHPoints.push(objPoint.pointid);
				}
				
			}
			
// =============================================== Traditional, Traditional-Amps or Vertical PDU ======================================================================================
				
			else if (paneltype == 1 || paneltype == 3 || paneltype == 5) {
				
				var v = 2; //  if Canara panel, increment breaker by 2
				if(paneltype == 3){ //  if vertical panel, increment breaker by 1
					v = 1;
				}
				
				// examples Traditional :
				//Breaker 01 - Current Reading
				//Breaker 01 - Power Total
				//Breaker 01 - Total Consumption Measured
				
				var breaker1 = parseInt(breaker);
				var breaker2 = parseInt(breaker) + v;
				var breaker3 = parseInt(breaker) + (v * 2);
				var arrBreakers = [];
				
				if(breaker1 > 9){
					arrBreakers.push(breaker1.toString());
				} else {
					arrBreakers.push('0' + breaker1.toString())
				}
				if(breaker2 > 9){
					arrBreakers.push(breaker2.toString());
				} else {
					arrBreakers.push('0' + breaker2.toString())
				}
				if(breaker3 > 9){
					arrBreakers.push(breaker3.toString());
				} else {
					arrBreakers.push('0' + breaker3.toString())
				}
				
				// these voltages  - only one breaker
				if(volts == '120V Single Phase' || volts == '240V Single Phase'){
					var ampspoint = 'Breaker ' + arrBreakers[0] + ' - Current Reading';
					var objPoint = _.find(arrPoints, function(arr){ return (arr.point == ampspoint); });								
					if(objPoint != null){
						arrAMPPoints.push(objPoint.pointid);
					}
					var kwpoint = 'Breaker ' + arrBreakers[0] + ' - Power Total';
					var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwpoint); });
					if(objPoint != null){
						arrKWPoints.push(objPoint.pointid);
					}
					var kwhpoint = 'Breaker ' + arrBreakers[0] + ' - Total Consumption Measured';
					var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwhpoint); });
					if(objPoint != null){
						arrKWHPoints.push(objPoint.pointid);
					}
				}
				// these voltages - 2 breakers
				else if (volts == '208V Single Phase'){
					for ( var i = 0; i < 2; i++ ) {
						var ampspoint = 'Breaker ' + arrBreakers[i] + ' - Current Reading';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == ampspoint); });
						if(objPoint != null){
							arrAMPPoints.push(objPoint.pointid);
						}
						var kwpoint = 'Breaker ' + arrBreakers[i] + ' - Power Total';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwpoint); });
						if(objPoint != null){
							arrKWPoints.push(objPoint.pointid);
						}
						var kwhpoint = 'Breaker ' + arrBreakers[i] + ' - Total Consumption Measured';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwhpoint); });
						if(objPoint != null){
							arrKWHPoints.push(objPoint.pointid);
						}
					}
				}
				// these voltages - 3 breakers
				else if (volts == '208V Three Phase' || volts == '240V Three Phase' || volts == '600V Three Phase'){
					for ( var i = 0;  i < 3; i++ ) {
						var ampspoint = 'Breaker ' + arrBreakers[i] + ' - Current Reading';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == ampspoint); });
						if(objPoint != null){
							arrAMPPoints.push(objPoint.pointid);
						}
						var kwpoint = 'Breaker ' + arrBreakers[i] + ' - Power Total';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwpoint); });
						if(objPoint != null){
							arrKWPoints.push(objPoint.pointid);
						}
						var kwhpoint = 'Breaker ' + arrBreakers[i] + ' - Total Consumption Measured';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == kwhpoint); });
						if(objPoint != null){
							arrKWHPoints.push(objPoint.pointid);
						}
					}
				}
				else{}
				
			}
			
// =============================================== Cyberex ======================================================================================
			
			else if (paneltype == 4) { // Cyberex Panel
				
				// examples Cyberex :
				// Panel - Voltage - Phase A
				// Panel - Voltage - Phase B
				// Panel - Voltage - Phase C
				// Breaker 01 - Amp Reading
				// Breaker 01 - Power Factor
				
				var arrPhases = ["A","B","C"];
				var v = 2; //  increment breaker by 2
				
				// these voltages  - only one breaker
				if(volts == '120V Single Phase' || volts == '240V Single Phase'){
					var ampspoint = 'Breaker ' + ('0' + breaker).slice(-2) + ' - Amp Reading';
					var objPoint = _.find(arrPoints, function(arr){ return (arr.point == ampspoint); });								
					if(objPoint != null){
						arrAMPPoints.push(objPoint.pointid);
					}
					var vltpoint = 'Panel - Voltage - Phase ' + arrPhases[0];
					var objPoint = _.find(arrPoints, function(arr){ return (arr.point == vltpoint); });
					if(objPoint != null){
						arrVLTPoints.push(objPoint.pointid);
					}
					var fctpoint = 'Breaker ' + ('0' + breaker).slice(-2) + ' - Power Factor';
					var objPoint = _.find(arrPoints, function(arr){ return (arr.point == fctpoint); });
					if(objPoint != null){
						arrFCTPoints.push(objPoint.pointid);
					}
				}
				// these voltages - 2 breakers
				else if (volts == '208V Single Phase'){
					for ( var i = 0; i < 2; i++ ) {
						var ampspoint = 'Breaker ' + ('0' + (parseInt(breaker) + i*v)).slice(-2) + ' - Amp Reading';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == ampspoint); });
						if(objPoint != null){
							arrAMPPoints.push(objPoint.pointid);
						}
						var vltpoint = 'Panel - Voltage - Phase ' + arrPhases[i];
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == vltpoint); });
						if(objPoint != null){
							arrVLTPoints.push(objPoint.pointid);
						}
						var fctpoint = 'Breaker ' + ('0' + (parseInt(breaker) + i*v)).slice(-2) + ' - Power Factor';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == fctpoint); });
						if(objPoint != null){
							arrFCTPoints.push(objPoint.pointid);
						}
					}
				}
				// these voltages - 3 breakers
				else if (volts == '208V Three Phase' || volts == '240V Three Phase'){
					for ( var i = 0;  i < 3; i++ ) {
						var ampspoint = 'Breaker ' + ('0' + (parseInt(breaker) + i*v)).slice(-2) + ' - Amp Reading';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == ampspoint); });
						if(objPoint != null){
							arrAMPPoints.push(objPoint.pointid);
						}
						var vltpoint = 'Panel - Voltage - Phase ' + arrPhases[i];
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == vltpoint); });
						if(objPoint != null){
							arrVLTPoints.push(objPoint.pointid);
						}
						var fctpoint = 'Breaker ' + ('0' + (parseInt(breaker) + i*v)).slice(-2) + ' - Power Factor';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == fctpoint); });
						if(objPoint != null){
							arrFCTPoints.push(objPoint.pointid);
						}
					}
				}
				else{}
				

			}
			


// =============================================== Virtual Cyberex ======================================================================================
			
			else if (paneltype == 8) { // Virtual Cyberex Panel
				
				// examples Virtual Cyberex points:
				// PN.001.AMP
				// PN.0AN.VLT
				// PN.0BN.VLT
				// PN.0CN.VLT
				// PN.001.PWF
				
				var arrPhases = ["A","B","C"];
				var v = 2; //  increment breaker by 2
				
				var arrBreakers = [];
				arrBreakers.push(('00' + (parseInt(breaker))).slice(-3));
				arrBreakers.push(('00' + (parseInt(breaker) + v)).slice(-3));
				arrBreakers.push(('00' + (parseInt(breaker) + v*2)).slice(-3));

				// these voltages  - only one breaker
				if(volts == '120V Single Phase' || volts == '240V Single Phase'){
					var ampspoint = '00.' + arrBreakers[0] + '.AMP';
					var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == ampspoint); });
					if(objPoint != null){
						arrAMPPoints.push(objPoint.pointid);
					}
					
					var m = arrBreakers[0] % 6;
					if(m == 1 || m == 2){
					var ndx = 0;
					}
					if(m == 3 || m == 4){
					var ndx = 1;
					}
					if(m == 0 || m == 5){
					var ndx = 2;
					}
					var vltpoint = 'PN.0' + arrPhases[ndx] + 'N.VLT';
					var objPoint = _.find(arrPoints, function(arr){ return (arr.point == vltpoint); });
					if(objPoint != null){
						arrVLTPoints.push(objPoint.pointid);
					}
					
					var fctpoint = '00.' + arrBreakers[0]  + '.PWF';
					var objPoint = _.find(arrPoints, function(arr){ return (arr.point == fctpoint); });
					if(objPoint != null){
						arrFCTPoints.push(objPoint.pointid);
					}
				}
				// these voltages - 2 breakers
				else if (volts == '208V Single Phase'){
					for ( var i = 0; i < 2; i++ ) {
						var ampspoint = '00.' + arrBreakers[i] + '.AMP';
						var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == ampspoint); });
						if(objPoint != null){
							arrAMPPoints.push(objPoint.pointid);
						}
						
						var m = arrBreakers[i] % 6;
						if(m == 1 || m == 2){
						var ndx = 0;
						}
						if(m == 3 || m == 4){
						var ndx = 1;
						}
						if(m == 0 || m == 5){
						var ndx = 2;
						}
						var vltpoint = 'PN.0' + arrPhases[ndx] + 'N.VLT';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == vltpoint); });
						if(objPoint != null){
							arrVLTPoints.push(objPoint.pointid);
						}
						
						var fctpoint = '00.' + arrBreakers[i] + '.PWF';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == fctpoint); });
						if(objPoint != null){
							arrFCTPoints.push(objPoint.pointid);
						}
					}
				}
				// these voltages - 3 breakers
				else if (volts == '208V Three Phase' || volts == '240V Three Phase'){
					for ( var i = 0;  i < 3; i++ ) {
						var ampspoint = '00.' + arrBreakers[i] + '.AMP';
						var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == ampspoint); });
						if(objPoint != null){
							arrAMPPoints.push(objPoint.pointid);
						}
						
						var m = arrBreakers[i] % 6;
						if(m == 1 || m == 2){
						var ndx = 0;
						}
						if(m == 3 || m == 4){
						var ndx = 1;
						}
						if(m == 0 || m == 5){
						var ndx = 2;
						}
						var vltpoint = 'PN.0' + arrPhases[ndx] + 'N.VLT';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == vltpoint); });
						if(objPoint != null){
							arrVLTPoints.push(objPoint.pointid);
						}
						
						var fctpoint = '00.' + arrBreakers[i] + '.PWF';
						var objPoint = _.find(arrPoints, function(arr){ return (arr.point == fctpoint); });
						if(objPoint != null){
							arrFCTPoints.push(objPoint.pointid);
						}
					}
				}
				else{}
				
			}
			
			
			
// =============================================== Virtual Amps ======================================================================================
						
			else if (paneltype == 6) {
				
				var v = 2;
				
				var arrBreakers = [];
				arrBreakers.push(('00' + (parseInt(breaker))).slice(-3));
				arrBreakers.push(('00' + (parseInt(breaker) + v)).slice(-3));
				arrBreakers.push(('00' + (parseInt(breaker) + v*2)).slice(-3));

				// these voltages  - only one breaker
				if(volts == '120V Single Phase' || volts == '240V Single Phase'){
					var ampspoint = '00.' + arrBreakers[0] + '.AMP';
					var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == ampspoint); });
					if(objPoint != null){
						arrAMPPoints.push(objPoint.pointid);
					}
				}
				// these voltages - 2 breakers
				else if (volts == '208V Single Phase'){
					for ( var i = 0; i < 2; i++ ) {
						var ampspoint = '00.' + arrBreakers[i] + '.AMP';
						var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == ampspoint); });
						if(objPoint != null){
							arrAMPPoints.push(objPoint.pointid);
						}
					}
				}
				// these voltages - 3 breakers
				else if (volts == '208V Three Phase' || volts == '240V Three Phase' || volts == '600V Three Phase'){
					for ( var i = 0;  i < 3; i++ ) {
						var ampspoint = '00.' + arrBreakers[i] + '.AMP';
						var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == ampspoint); });
						if(objPoint != null){
							arrAMPPoints.push(objPoint.pointid);
						}
					}
				}
				else{}
			}

// =============================================== Virtual Traditional & Virtual - Vertical ======================================================================================
							
			else if (paneltype == 7 || paneltype == 10) {
				
				if(paneltype == 10){
					var v = 1;
				}
				if(paneltype == 7){
					var v = 2;
				}
				var arrBreakers = [];
				arrBreakers.push(('00' + (parseInt(breaker))).slice(-3));
				arrBreakers.push(('00' + (parseInt(breaker) + v)).slice(-3));
				arrBreakers.push(('00' + (parseInt(breaker) + v*2)).slice(-3));

				// these voltages  - only one breaker
				if(volts == '120V Single Phase' || volts == '240V Single Phase'){
					var ampspoint = '00.' + arrBreakers[0] + '.AMP';
					var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == ampspoint); });
					if(objPoint != null){
						arrAMPPoints.push(objPoint.pointid);
					}
					var kwpoint = '00.' + arrBreakers[0] + '.RPW';
					var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == kwpoint); });
					if(objPoint != null){
						arrKWPoints.push(objPoint.pointid);
					}
					var kwhpoint = '00.' + arrBreakers[0] + '.KWH';
					var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == kwhpoint); });
					if(objPoint != null){
						arrKWHPoints.push(objPoint.pointid);
					}
				}
				// these voltages - 2 breakers
				else if (volts == '208V Single Phase'){
					for ( var i = 0; i < 2; i++ ) {
						var ampspoint = '00.' + arrBreakers[i] + '.AMP';
						var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == ampspoint); });
						if(objPoint != null){
							arrAMPPoints.push(objPoint.pointid);
						}
						var kwpoint = '00.' + arrBreakers[i] + '.RPW';
						var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == kwpoint); });
						if(objPoint != null){
							arrKWPoints.push(objPoint.pointid);
						}
						var kwhpoint = '00.' + arrBreakers[i] + '.KWH';
						var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == kwhpoint); });
						if(objPoint != null){
							arrKWHPoints.push(objPoint.pointid);
						}
					}
				}
				// these voltages - 3 breakers
				else if (volts == '208V Three Phase' || volts == '240V Three Phase' || volts == '600V Three Phase'){
					for ( var i = 0;  i < 3; i++ ) {
						var ampspoint = '00.' + arrBreakers[i] + '.AMP';
						var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == ampspoint); });
						if(objPoint != null){
							arrAMPPoints.push(objPoint.pointid);
						}
						var kwpoint = '00.' + arrBreakers[i] + '.RPW';
						var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == kwpoint); });
						if(objPoint != null){
							arrKWPoints.push(objPoint.pointid);
						}
						var kwhpoint = '00.' + arrBreakers[i] + '.KWH';
						var objPoint = _.find(arrPoints, function(arr){ return ((arr.point).substr((arr.point).length - 10) == kwhpoint); });
						if(objPoint != null){
							arrKWHPoints.push(objPoint.pointid);
						}
					}
				}
				else{}
			}

// =============================================== other panel types... ======================================================================================
								
			else{
				
			}
			
// =============================================== update device and points on power circuit ======================================================================================
			
			if(deviceid > 0){
				power.setFieldValue('custrecord_clgx_dcim_device',deviceid);
			}
			if(arrAMPPoints.length > 0){
				power.setFieldValues('custrecord_clgx_dcim_points_current',arrAMPPoints);
			}
			if(paneltype == 1 || paneltype == 2 || paneltype == 3 || paneltype == 7 || paneltype == 10 || paneltype == 12){
				if(arrKWPoints.length > 0){
					power.setFieldValues('custrecord_clgx_dcim_points_demand',arrKWPoints);
				}
				if(arrKWHPoints.length > 0){
					power.setFieldValues('custrecord_clgx_dcim_points_consumption',arrKWHPoints);
				}
			}
			else if (paneltype == 4 || paneltype == 8){
				if(arrVLTPoints.length > 0){
					power.setFieldValues('custrecord_clgx_dcim_points_voltage',arrVLTPoints);
				}
				if(arrFCTPoints.length > 0){
					power.setFieldValues('custrecord_clgx_dcim_points_pwr_factor',arrFCTPoints);
				}
			}
			else{}
			
			var idRec = nlapiSubmitRecord(power, false, true);
				
		}
		else{ // the fam is not linked to a device - do nothing 
			// TODO - search for a device with the same name as the FAM ?
		}

	}
	else{ // no panel  - maybe powers linked to circuits - MTL7
		
		var circuitid = parseInt(power.getFieldValue('custrecord_clgx_outlet_box_serial_no_pwr'));
		var circuit = power.getFieldText('custrecord_clgx_outlet_box_serial_no_pwr');
		
		if(circuitid > 0){ // a circuit is configured
		
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("name",null,"contains",circuit));
			arrFilters.push(new nlobjSearchFilter('custrecord_clgx_dcim_device_deleted',null,'is','F'));
			arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
			var searchDevice = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
			
			var deviceid = 0;
			if(searchDevice != null){
				deviceid = parseInt(searchDevice[0].getValue('internalid',null,null));
			}
			
			if(deviceid > 0){
				
				power.setFieldValue('custrecord_clgx_dcim_device',deviceid);
				
				// Build Points Array ------------------------------------------------------------------------------------------------------------------------------------------------
				var arrColumns = new Array();
				arrColumns.push(new nlobjSearchColumn('internalid',null,null));
				arrColumns.push(new nlobjSearchColumn('externalid',null,null));
				arrColumns.push(new nlobjSearchColumn('name',null,null));
				var arrFilters = new Array();
				arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_device",null,"anyof",deviceid));
				var searchPoints = nlapiSearchRecord('customrecord_clgx_dcim_points', null, arrFilters, arrColumns);
				var arrPoints = new Array();
				for ( var i = 0; searchPoints != null && i < searchPoints.length; i++ ) {
					var objPoint = new Object();
					objPoint["pointid"] = parseInt(searchPoints[i].getValue('internalid',null,null));
					objPoint["pointextid"] = parseInt(searchPoints[i].getValue('externalid',null,null));
					objPoint["point"] = searchPoints[i].getValue('name',null,null);
					arrPoints.push(objPoint);
				}
				
				var arrAMPPoints = new Array();
				var objPointAmps = _.find(arrPoints, function(arr){ return (arr.point == 'Current - Total'); });
				if(objPointAmps != null){
					arrAMPPoints.push(objPointAmps.pointid);
					power.setFieldValues('custrecord_clgx_dcim_points_current',arrAMPPoints);
				}
											
				var arrKWPoints = new Array();
				var objPointKW = _.find(arrPoints, function(arr){ return (arr.point == 'Power - Total Demand'); });
				if(objPointKW != null){
					arrKWPoints.push(objPointKW.pointid);
					power.setFieldValues('custrecord_clgx_dcim_points_demand',arrKWPoints);
				}
				
				var arrKWHPoints = new Array();
				var objPointKWH = _.find(arrPoints, function(arr){ return (arr.point == 'Power - Total Consumption Measured'); });
				if(objPointKWH != null){
					arrKWHPoints.push(objPointKWH.pointid);
					power.setFieldValues('custrecord_clgx_dcim_points_consumption',arrKWHPoints);
				}

				var idRec = nlapiSubmitRecord(power, false, true);
			}
		}
		else{ // no circuit either - do nothing }
		
	}
	}
	return 1;
}



function get_new_points (externalid){
	
	var request = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/devices/get_points_update.cfm?deviceid=' + externalid);
	var data = request.body;
	var arr = JSON.parse( data );
	
	var points = new Array();
	for ( var j = 0; arr != null && j < arr.length; j++ ) {
		var point = {
				"externalid": (arr[j].POINTID).toString() || '',
				"name": arr[j].NAME || '',
				"average": parseFloat(arr[j].DAVG) || 0,
		};
		points.push(point);
	}
	return points;
}

function get_old_points (deviceid){
	
	var columns = new Array();
	columns.push(new nlobjSearchColumn('internalid',null,null));
	columns.push(new nlobjSearchColumn('externalid',null,null));
	columns.push(new nlobjSearchColumn('name',null,null));
	var filters = new Array();
	filters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_device",null,"anyof",deviceid));
	filters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	var search = nlapiSearchRecord('customrecord_clgx_dcim_points', null, filters, columns);
	
	var points = new Array();
	for ( var j = 0; search != null && j < search.length; j++ ) {
		var point = {
				"internalid": search[j].getValue('internalid',null,null),
				"externalid": search[j].getValue('externalid',null,null),
				"name": search[j].getValue('name',null,null)
		};
		points.push(point);
	}
	return points;
}

function delete_points (arrDelete,arrOld){
	
	var deleted = new Array();
	var errors = new Array();
	for ( var i = 0; arrDelete != null && i < arrDelete.length; i++ ) {
		
		var point = _.find(arrOld, function(arr){ return (arr.externalid == arrDelete[i]) ; });
		if(point){
			try {
				nlapiDeleteRecord('customrecord_clgx_dcim_points', point.internalid);
				deleted.push(arrDelete[i]);
			}
			catch (error) {
				errors.push(arrDelete[i]);
				
				if(point.name.substr(point.name.length - 3) == 'DEL'){
					var delname = point.name;
				} else {
					var delname = (point.name + '-DEL').substring(0,299);
				}
				try {
					nlapiSubmitField('customrecord_clgx_dcim_points', point.internalid, ['name','custrecord_clgx_dcim_points_deleted'], [delname,'T']);
				} catch (error) {
					//errors.push(arrDelete[i]);
				}
			}
		}
	}
	return [deleted,errors];
}

function update_points (arrUpdate,arrNew,arrOld){
	
	var updated = new Array();
	var errors= new Array();
	
	for ( var i = 0; arrUpdate != null && i < arrUpdate.length; i++ ) {
		
		var objNew = _.find(arrNew, function(arr){ return (arr.externalid == arrUpdate[i]) ; });
		var objOld = _.find(arrOld, function(arr){ return (arr.externalid == arrUpdate[i]) ; });
		
		if(objNew && objOld){
			
			var fields = ['name', 'custrecord_clgx_dcim_points_day_avg', 'custrecord_clgx_dcim_points_deleted'];
			var values = [objNew.name, objNew.average, 'F'];
			
			try {
				nlapiSubmitField('customrecord_clgx_dcim_points', objOld.internalid, fields, values);
				updated.push(arrUpdate[i]);
			}
			catch (error) {
				errors.push(arrUpdate[i]);
			}
		}
	}
	return [updated,errors];
}

function add_points (deviceid,arrAdd,arrNew){
	
	var added = new Array();
	var errors= new Array();
	
	for ( var i = 0; arrAdd != null && i < arrAdd.length; i++ ) {
		
		var objNew = _.find(arrNew, function(arr){ return (arr.externalid == arrAdd[i]) ; });
		if(objNew){
			
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("name",null,"is", objNew.name));
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_device",null,"anyof",deviceid));
			arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_points_deleted",null,"is",'F'));
			var search = nlapiSearchRecord('customrecord_clgx_dcim_points', null, arrFilters, arrColumns);
			
			if(!search) {
				try {
		            var record = nlapiCreateRecord('customrecord_clgx_dcim_points');
		            record.setFieldValue('name', objNew.name);
		            record.setFieldValue('externalid', arrAdd[i].toString());
		            record.setFieldValue('custrecord_clgx_dcim_points_device', deviceid);
		            record.setFieldValue('custrecord_clgx_dcim_points_day_avg', objNew.average);
		            record.setFieldValue('custrecord_clgx_dcim_points_deleted', 'F');
		            var idRec = nlapiSubmitRecord(record, false, true);
		            added.push(arrAdd[i]);
				}
				catch (error) {
					errors.push(arrAdd[i]);
				}
			}
		}
	}
	return [added,errors];
}

