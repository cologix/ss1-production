nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_Power_Circuit.js
//	Script Name:	CLGX_CR_Power_Circuit
//	Script Id:		customscript_clgx_cr_power_circuit
//	Script Runs:	Client
//	Script Type:	Client Script
//	Deployments:	Case
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		02/10/2015
//-------------------------------------------------------------------------------------------------

function saveRecord(){
	try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Created:	02/10/2015
// Details:	Verify breakers conflicts
//-------------------------------------------------------------------------------------------------

        var allowSave = 1;
        var alertMsg = '';
		
        var power = nlapiGetRecordId();
		var volts = nlapiGetFieldText('custrecord_cologix_power_volts');
		var panel = nlapiGetFieldValue('custrecord_clgx_power_panel_pdpm');
		
		if(panel != null && panel != ''){
			
			// verify match volts / lines
			
			//var fields = ['custrecord_clgx_fam_pdpm','custrecord_clgx_vertical_pdu'];
			//var columns = nlapiLookupField('customrecord_ncfar_asset', panel, fields);
			//var pdpm = columns.custrecord_clgx_fam_pdpm;
			//var vertical = columns.custrecord_clgx_vertical_pdu;
			
			var paneltype = nlapiLookupField('customrecord_ncfar_asset', panel, 'custrecord_clgx_panel_type');
			
			if(paneltype == 2 ||paneltype == 12){ // pdpm
				
				var thismodule = parseInt(nlapiGetFieldText('custrecord_cologix_power_upsbreaker'));
				var lines = nlapiGetFieldText('custrecord_cologix_power_subpanel');
				
				// verify first the match between voltage/phases and lines
				if((lines == 'Line 1' || lines == 'Line 2' || lines == 'Line 3') && (volts == '208V Single Phase' || volts == '208V Three Phase' || volts == '240V Three Phase')){
					alertMsg += 'Volts/phase(s) has to match with module line(s). Please verify your selection(s).';
					allowSave=0;
				}
				if((lines == 'Lines 1, 2' || lines == 'Lines 1, 3' || lines == 'Lines 2, 3') && (volts == '120V Single Phase' || volts == '240V Single Phase' || volts == '208V Three Phase' || volts == '240V Three Phase')){
					alertMsg += 'Volts/phase(s) has to match with module line(s). Please verify your selection(s).';
					allowSave=0;
				}
				if((lines == 'Lines 1, 2, 3') && (volts == '120V Single Phase' || volts == '240V Single Phase' || volts == '208V Single Phase')){
					alertMsg += 'Volts/phase(s) has to match with module line(s). Please verify your selection(s).';
					allowSave=0;
				}
				
				
				if(allowSave == 1){ // if match between voltage/phases and lines and flag didin't changed, then verify breakers
					
					var arrPowerBreakers = new Array();
					if(lines == 'Line 1'){
						arrPowerBreakers.push(1);
					}
					if(lines == 'Line 2'){
						arrPowerBreakers.push(2);
					}
					if(lines == 'Line 3'){
						arrPowerBreakers.push(3);
					}
					if(lines == 'Lines 1, 2'){
						arrPowerBreakers.push(1);
						arrPowerBreakers.push(2);
					}
					if(lines == 'Lines 2, 3'){
						arrPowerBreakers.push(2);
						arrPowerBreakers.push(3);
					}
					if(lines == 'Lines 1, 3'){
						arrPowerBreakers.push(1);
						arrPowerBreakers.push(3);
					}
					if(lines == 'Lines 1, 2, 3'){
						arrPowerBreakers.push(1);
						arrPowerBreakers.push(2);
						arrPowerBreakers.push(3);
					}
					
					var arrColumns = new Array();
					arrColumns.push(new nlobjSearchColumn('internalid',null,null));
					arrColumns.push(new nlobjSearchColumn('name',null,null));
					arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_upsbreaker',null,null));
					arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_subpanel',null,null));
					var arrFilters = new Array();
					if(power > 0){
						arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",power));
					}
					arrFilters.push(new nlobjSearchFilter("custrecord_clgx_power_panel_pdpm",null,"anyof",panel));
					arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_upsbreaker",null,"anyof",thismodule));
					arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_service",null,"noneof",'@NONE@'));
					arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
					var searchBreakers = nlapiSearchRecord('customrecord_clgx_power_circuit', null, arrFilters, arrColumns);
					
					var arrPanelBreakers = new Array();
					for ( var i = 0; searchBreakers != null && i < searchBreakers.length; i++ ) {
			        	var searchBreaker = searchBreakers[i];
			        	
			        	var module = parseInt(searchBreaker.getText('custrecord_cologix_power_upsbreaker',null,null));
			        	var lines = searchBreaker.getText('custrecord_cologix_power_subpanel',null,null);
			        	
						if(lines == 'Line 1'){
							var objBreaker = new Object();
							objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
							objBreaker["power"] = searchBreaker.getValue('name',null,null);
							objBreaker["module"] = module;
							objBreaker["breaker"] = 1;
							arrPanelBreakers.push(objBreaker);
						}
						if(lines == 'Line 2'){
							var objBreaker = new Object();
							objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
							objBreaker["power"] = searchBreaker.getValue('name',null,null);
							objBreaker["module"] = module;
							objBreaker["breaker"] = 2;
							arrPanelBreakers.push(objBreaker);
						}
						if(lines == 'Line 3'){
							var objBreaker = new Object();
							objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
							objBreaker["power"] = searchBreaker.getValue('name',null,null);
							objBreaker["module"] = module;
							objBreaker["breaker"] = 3;
							arrPanelBreakers.push(objBreaker);
						}
						if(lines == 'Lines 1, 2'){
							var objBreaker = new Object();
							objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
							objBreaker["power"] = searchBreaker.getValue('name',null,null);
							objBreaker["module"] = module;
							objBreaker["breaker"] = 1;
							arrPanelBreakers.push(objBreaker);
							var objBreaker = new Object();
							objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
							objBreaker["power"] = searchBreaker.getValue('name',null,null);
							objBreaker["module"] = module;
							objBreaker["breaker"] = 2;
							arrPanelBreakers.push(objBreaker);
						}
						if(lines == 'Lines 2, 3'){
							var objBreaker = new Object();
							objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
							objBreaker["power"] = searchBreaker.getValue('name',null,null);
							objBreaker["module"] = module;
							objBreaker["breaker"] = 2;
							arrPanelBreakers.push(objBreaker);
							var objBreaker = new Object();
							objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
							objBreaker["power"] = searchBreaker.getValue('name',null,null);
							objBreaker["module"] = module;
							objBreaker["breaker"] = 3;
							arrPanelBreakers.push(objBreaker);
						}
						if(lines == 'Lines 1, 3'){
							var objBreaker = new Object();
							objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
							objBreaker["power"] = searchBreaker.getValue('name',null,null);
							objBreaker["module"] = module;
							objBreaker["breaker"] = 1;
							arrPanelBreakers.push(objBreaker);
							var objBreaker = new Object();
							objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
							objBreaker["power"] = searchBreaker.getValue('name',null,null);
							objBreaker["module"] = module;
							objBreaker["breaker"] = 3;
							arrPanelBreakers.push(objBreaker);
						}
						if(lines == 'Lines 1, 2, 3'){
							var objBreaker = new Object();
							objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
							objBreaker["power"] = searchBreaker.getValue('name',null,null);
							objBreaker["module"] = module;
							objBreaker["breaker"] = 1;
							arrPanelBreakers.push(objBreaker);
							var objBreaker = new Object();
							objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
							objBreaker["power"] = searchBreaker.getValue('name',null,null);
							objBreaker["module"] = module;
							objBreaker["breaker"] = 2;
							arrPanelBreakers.push(objBreaker);
							var objBreaker = new Object();
							objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
							objBreaker["power"] = searchBreaker.getValue('name',null,null);
							objBreaker["module"] = module;
							objBreaker["breaker"] = 3;
							arrPanelBreakers.push(objBreaker);
						}
					}
					
					var arrConflicts = new Array();
					for ( var i = 0; arrPanelBreakers != null && i < arrPanelBreakers.length; i++ ) {
						
						for ( var j = 0; arrPowerBreakers != null && j < arrPowerBreakers.length; j++ ) {
							if(arrPanelBreakers[i].module == thismodule && arrPanelBreakers[i].breaker == arrPowerBreakers[j]){
								arrConflicts.push(arrPanelBreakers[i]);
							}
						}
					}
					
					if(arrConflicts.length > 0){
						alertMsg += 'This power circuit is in conflict with:\n';
						for ( var i = 0; arrConflicts != null && i < arrConflicts.length; i++ ) {
							alertMsg += arrConflicts[i].power + ' on module ' + arrConflicts[i].module + ' / breaker ' + arrConflicts[i].breaker + '\n';
						}
						allowSave=0;
					}
				}
			}
			
			else{ 
				
				var breaker = parseInt(nlapiGetFieldText('custrecord_cologix_power_circuitbreaker'));
				var arrPowerBreakers = new Array();
				var v = 2;
				if(paneltype == 3 || paneltype == 10){ //  increment breaker by 1
					v = 1;
				}
				// these voltages  - only one breaker
				if(volts == '120V Single Phase' || volts == '240V Single Phase'){
					arrPowerBreakers.push(breaker);
				}
				// this voltages - 2 breakers
				else if (volts == '208V Single Phase'){
					arrPowerBreakers.push(breaker);
					arrPowerBreakers.push(breaker + v*1);
				}
				// these voltages - 3 breakers
				else if (volts == '208V Three Phase' || volts == '240V Three Phase' || volts == '600V Three Phase'){
					arrPowerBreakers.push(breaker);
					arrPowerBreakers.push(breaker + v*1);
					arrPowerBreakers.push(breaker + v*2);
				}
				else{
				}
				
				var arrColumns = new Array();
				arrColumns.push(new nlobjSearchColumn('internalid',null,null));
				arrColumns.push(new nlobjSearchColumn('name',null,null));
				arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_volts',null,null));
				arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_circuitbreaker',null,null).setSort(false));
				var arrFilters = new Array();
				if(power > 0){
					arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",power));
				}
				arrFilters.push(new nlobjSearchFilter("custrecord_clgx_power_panel_pdpm",null,"anyof",panel));
				arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_service",null,"noneof",'@NONE@'));
				arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_circuitbreaker",null,"noneof",'@NONE@'));
				arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
				var searchBreakers = nlapiSearchRecord('customrecord_clgx_power_circuit', null, arrFilters, arrColumns);
				
				var arrPanelBreakers = new Array();
				for ( var i = 0; searchBreakers != null && i < searchBreakers.length; i++ ) {
		        	var searchBreaker = searchBreakers[i];
		        	var volts = searchBreaker.getText('custrecord_cologix_power_volts',null,null);
		        	var breaker = parseInt(searchBreaker.getText('custrecord_cologix_power_circuitbreaker',null,null));
		        	
					if(volts == '120V Single Phase' || volts == '240V Single Phase'){
						var objBreaker = new Object();
						objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
						objBreaker["power"] = searchBreaker.getValue('name',null,null);
						objBreaker["breaker"] = breaker;
						arrPanelBreakers.push(objBreaker);
					}
					// this voltages - 2 breakers
					else if (volts == '208V Single Phase'){
						
						var objBreaker = new Object();
						objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
						objBreaker["power"] = searchBreaker.getValue('name',null,null);
						objBreaker["breaker"] = breaker;
						arrPanelBreakers.push(objBreaker);
						
						var objBreaker = new Object();
						objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
						objBreaker["power"] = searchBreaker.getValue('name',null,null);
						objBreaker["breaker"] = breaker + v*1;
						arrPanelBreakers.push(objBreaker);

					}
					// these voltages - 3 breakers
					else if (volts == '208V Three Phase' || volts == '240V Three Phase'){
						
						var objBreaker = new Object();
						objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
						objBreaker["power"] = searchBreaker.getValue('name',null,null);
						objBreaker["breaker"] = breaker;
						arrPanelBreakers.push(objBreaker);
						
						var objBreaker = new Object();
						objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
						objBreaker["power"] = searchBreaker.getValue('name',null,null);
						objBreaker["breaker"] = breaker + v*1;
						arrPanelBreakers.push(objBreaker);
						
						var objBreaker = new Object();
						objBreaker["powerid"] = parseInt(searchBreaker.getValue('internalid',null,null));
						objBreaker["power"] = searchBreaker.getValue('name',null,null);
						objBreaker["breaker"] = breaker + v*2;
						arrPanelBreakers.push(objBreaker);

					}
					else{
					}

				}
				
				var arrConflicts = new Array();
				for ( var i = 0; arrPanelBreakers != null && i < arrPanelBreakers.length; i++ ) {
					
					for ( var j = 0; arrPowerBreakers != null && j < arrPowerBreakers.length; j++ ) {
						if(arrPanelBreakers[i].breaker == arrPowerBreakers[j]){
							arrConflicts.push(arrPanelBreakers[i]);
						}
					}
				}
				
				if(arrConflicts.length > 0){
					alertMsg += 'This power circuit is in conflict with:\n';
					for ( var i = 0; arrConflicts != null && i < arrConflicts.length; i++ ) {
						alertMsg += arrConflicts[i].power + ' on breaker ' + arrConflicts[i].breaker + '\n';
					}
					allowSave=0;
				}
			}
		}
		else{ // there is no panel, look for OUTLET BOX SERIAL #
			
			var box = nlapiGetFieldValue('custrecord_clgx_outlet_box_serial_no_pwr');
			var phase = nlapiGetFieldText('custrecord_clgx_voltage_phase_pwr');
			
			if(box != null && box != ''){
				
				var arrColumns = new Array();
				arrColumns.push(new nlobjSearchColumn('internalid',null,null));
				arrColumns.push(new nlobjSearchColumn('name',null,null));
				var arrFilters = new Array();
				if(power > 0){
					arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",power));
				}
				arrFilters.push(new nlobjSearchFilter("custrecord_clgx_outlet_box_serial_no_pwr",null,"anyof",box));
				arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_service",null,"noneof",'@NONE@'));
				arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
				var searchBreakers = nlapiSearchRecord('customrecord_clgx_power_circuit', null, arrFilters, arrColumns);
				
				if(searchBreakers != null){
					alertMsg += 'Power circuit ' + searchBreakers[0].getValue('name',null,null) + ' is already configured on this outlet box. Please verify your selection.';
					allowSave=0;
				}
				
				// verify first the match between voltage/phases and lines
				if((phase == 'A' || phase == 'B' || phase == 'C') && (volts == '208V Single Phase' || volts == '208V Three Phase' || volts == '240V Three Phase')){
					alertMsg += 'Volts has to match with voltage phase(s). Please verify your selection(s).';
					allowSave=0;
				}
				if((phase == 'A, B' || phase == 'A, C' || phase == 'B, C') && (volts == '120V Single Phase' || volts == '240V Single Phase' || volts == '208V Three Phase' || volts == '240V Three Phase')){
					alertMsg += 'Volts has to match with voltage phase(s). Please verify your selection(s).';
					allowSave=0;
				}
				if((phase == 'A, B, C') && (volts == '120V Single Phase' || volts == '240V Single Phase' || volts == '208V Single Phase')){
					alertMsg += 'Volts has to match with voltage phase(s). Please verify your selection(s).';
					allowSave=0;
				}
			}

		}

        if (allowSave == 0) {
            alert(alertMsg);
            return false;
        }
        else{
            return true;
        }
		
//---------- End Section 1 ------------------------------------------------------------------------------------------------

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


