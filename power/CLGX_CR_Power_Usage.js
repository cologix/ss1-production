nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_Power_Usage.js
//	Script Name:	CLGX_CR_Power_Usage
//	Script Id:		customscript_clgx_cr_power_usage
//	Script Runs:	Client
//	Script Type:	Client Script
//	Deployments:	Power Usage
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		3/14/2012
//-------------------------------------------------------------------------------------------------

function fieldChanged(type, name, linenum){
	try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 3/14/2012
// Details:	Recalculate form fields depending on the current monthly reading.
//-------------------------------------------------------------------------------------------------

		var currentContext = nlapiGetContext();
		if (currentContext.getExecutionContext() == 'userinterface') {
			
			if (name == 'custrecord_clgx_pwusg_current_read') {
				
				var stCurentRead = nlapiGetFieldValue('custrecord_clgx_pwusg_current_read');
				var stPreviousRead = nlapiGetFieldValue('custrecord_clgx_pwusg_previous_read');
                var stCommitUtilKwh = nlapiGetFieldValue('custrecord_clgx_pwusg_commit_util_kwh');
                var stCommitUtilRate = nlapiGetFieldValue('custrecord_clgx_pwusg_commit_util_rate');
                var stUtilRate = nlapiGetFieldValue('custrecord_clgx_pwusg_util_rate');
                var stCommitGenKwh = nlapiGetFieldValue('custrecord_clgx_pwusg_commit_gen_kwh');
                var stCommitGenRate = nlapiGetFieldValue('custrecord_clgx_pwusg_commit_gen_rate');
                var stGenRate = nlapiGetFieldValue('custrecord_clgx_pwusg_gen_rate');
                var stLastPowerUsageID = nlapiGetFieldValue('custrecord_clgx_pwusg_previous_id');
                
                var stPwrUsgUtil = parseInt(stCurentRead) - parseInt(stPreviousRead);
                var stConsumtionUtil = parseInt(stPwrUsgUtil) - parseInt(stCommitUtilKwh);
                var stConsumtionGen = parseInt(stPwrUsgUtil) - parseInt(stCommitGenKwh);
                
                nlapiSetFieldValue('custrecord_clgx_pwusg_utility', parseInt(stPwrUsgUtil));
				if (stConsumtionUtil > 0){
					var amountUtil = parseFloat(stConsumtionUtil)*parseFloat(stUtilRate);
					nlapiSetFieldValue('custrecord_clgx_pwusg_overage_calc_util', parseInt(stConsumtionUtil));
					nlapiSetFieldValue('custrecord_clgx_pwusg_over_calc_amnt_uti', parseFloat(amountUtil).toFixed(2));
				}
				else{
					var amountUtil = 0;
					nlapiSetFieldValue('custrecord_clgx_pwusg_overage_calc_util', 0);
					nlapiSetFieldValue('custrecord_clgx_pwusg_over_calc_amnt_uti', 0);
				}
				if (stConsumtionGen > 0){
					var amountGen = parseFloat(stConsumtionGen)*parseFloat(stGenRate);
					nlapiSetFieldValue('custrecord_clgx_pwusg_overage_calc_gen', parseInt(stConsumtionGen));
					nlapiSetFieldValue('custrecord_clgx_pwusg_over_calc_amnt_gen', parseFloat(amountGen).toFixed(2));
				}
				else{
					var amountGen = 0;
					nlapiSetFieldValue('custrecord_clgx_pwusg_overage_calc_gen', 0);
					nlapiSetFieldValue('custrecord_clgx_pwusg_over_calc_amnt_gen', 0);
				}
				var amountTotal =  parseFloat(amountUtil) + parseFloat(amountGen);
				nlapiSetFieldValue('custrecord_clgx_power_usage_total_amount', parseFloat(amountTotal).toFixed(2));

			}
		}

		
//---------- End Section 1 ------------------------------------------------------------------------------------------------

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function validateField(type, name, linenum){
	try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 3/14/2012
// Details:	Current meter reading can not be smaller than previous meter reading 
//			unless first power usage is edit for modification and they have to be the same.
//-------------------------------------------------------------------------------------------------

		var currentContext = nlapiGetContext();
		if (currentContext.getExecutionContext() == 'userinterface') {
			
			if (name == 'custrecord_clgx_pwusg_current_read') {
				var stCurentRead = nlapiGetFieldValue('custrecord_clgx_pwusg_current_read');
				var stPreviousRead = nlapiGetFieldValue('custrecord_clgx_pwusg_previous_read');
				var stLastPowerUsageID = nlapiGetFieldValue('custrecord_clgx_pwusg_previous_id');
				
				if((parseInt(stCurentRead) < parseInt(stPreviousRead)) && stLastPowerUsageID != ''){
					alert('Current meter reading can not be smaller than previous meter reading.');
					return false;
				}
				else{
					return true;
				}
			}
		}

		
//---------- End Section 1 ------------------------------------------------------------------------------------------------

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}



