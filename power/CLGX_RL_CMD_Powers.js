 nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CMD_Powers.js
//	Script Name:	CLGX_SL_CMD_Powers
//	Script Id:		customscript_clgx_sl_cmd_powers
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/29/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=734&deploy=1
//-------------------------------------------------------------------------------------------------

    function suitelet_cmd_powers (datain){
        try {
            datain=JSON.parse(datain);
            nlapiLogExecution('DEBUG','datain', datain);
            var customerid = datain.customerid;
            var facilityid = datain.facilityid;
            var famid = datain.famid;
            var allpwrs = datain.allpwrs;
            var html = '';
            var arrPowers =  new Array();

            if(customerid > 0 || famid > 0){

                var arrColumns = new Array();
                var arrFilters = new Array();
                if(customerid > 0){
                    arrFilters.push(new nlobjSearchFilter("parent",'custrecord_cologix_power_service',"anyof",customerid));
                }
                if(famid > 0){
                    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_power_panel_pdpm",null,"anyof",famid));
                }
                arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
                if(allpwrs > 0){
                    var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pwrs_kw_cust_all', arrFilters, arrColumns);
                }
                else{
                    var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pwrs_kw_cust_pnl', arrFilters, arrColumns);
                }

                while (true) {

                    var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_dcim_pwrs_kw_cust_pnl', arrFilters, arrColumns);

                    if (!searchPowers) {
                        break;
                    }
                    for (var i in searchPowers) {

                        var searchPower = searchPowers[i];
                        var columns = searchPower.getAllColumns();
                        var objPower = new Object();
                        objPower["facilityid"] = parseInt(searchPowers[i].getValue(columns[0]));
                        objPower["facility"] = searchPower.getText(columns[0]);
                        objPower["panelid"] = parseInt(searchPower.getValue(columns[1]));
                        objPower["panel"] = searchPower.getText(columns[1]);
                        objPower["powerid"] = parseInt(searchPower.getValue(columns[2]));
                        var internalid = parseInt(searchPower.getValue(columns[2]));
                        objPower["power"] = searchPower.getValue(columns[3]);
                        objPower["pairid"] = parseInt(searchPower.getValue(columns[4]));
                        objPower["pair"] = searchPower.getText(columns[4]);
                        objPower["spaceid"] = parseInt(searchPower.getValue(columns[5]));
                        objPower["space"] = searchPower.getText(columns[5]);
                        objPower["serviceid"] = parseInt(searchPower.getValue(columns[6]));
                        objPower["service"] = _.last((searchPower.getText(columns[6])).split(" : "));
                        objPower["soid"] = parseInt(searchPower.getValue(columns[7]));
                        objPower["so"] = (searchPower.getText(columns[7])).replace("Service Order #", "");
                        objPower["volts"] = searchPower.getText(columns[8]);
                        objPower["contract"] = searchPower.getText(columns[9]);
                        objPower["module"] = parseInt(searchPower.getValue(columns[10]));
                        objPower["breaker"] = parseInt(searchPower.getValue(columns[11]));
                        var kwavg = parseFloat(searchPower.getValue(columns[12]));
                        if (kwavg > 0) {
                            objPower["kwavg"] = kwavg;
                        } else {
                            objPower["kwavg"] = 0;
                        }

                        //var objPwrUsg = _.find(arrAllPowers, function(arr){ return (arr.powerid == parseInt(searchPower.getValue(columns[2]))); });
                        /* var objPwrUsg = _.find(arrAllPowers, function (arr) {
                             return (arr.powerid == parseInt(searchPower.getValue(columns[2])));
                         });
                         if (objPwrUsg != null) {
                             objPower["amps"] = round(objPwrUsg.amps);
                             objPower["ampspair"] = round(objPwrUsg.ampspair);
                             objPower["absum"] = round(objPwrUsg.absum);
                             objPower["usage"] = round(objPwrUsg.usage);
                         } else {*/
                        objPower["amps"] = 0;
                        objPower["ampspair"] = 0;
                        objPower["absum"] = 0;
                        objPower["usage"] = 0;
                        // }
                        arrPowers.push(objPower);
                        if (searchPowers.length < 1000) {
                            break;
                        }
                        arrFilters.push(new nlobjSearchFilter("internalIdNumber", null, "greaterthan", internalid));
                    }
                }
}
            var objFile = nlapiLoadFile(4495118);
            html = objFile.getValue();
            html = html.replace(new RegExp('{powersJSON}','g'), JSON.stringify(arrPowers));

            response.write( html );
        }
        catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
            if (error.getDetails != undefined){
                nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
                throw error;
            }
            else{
                nlapiLogExecution('ERROR','Unexpected Error', error.toString());
                throw nlapiCreateError('99999', error.toString());
            }
        } // End Catch Errors Section ------------------------------------------------------------------------------------------
    }
    function round(value) {
        return Number(Math.round(value+'e'+2)+'e-'+2);
    }