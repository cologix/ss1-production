//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CF_Power_Circuit.js
//	Script Runs:	Client
//	Script Type:	Client Script / Form
//	Deployments:	Power Circuit Form
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		3/14/2012
//-------------------------------------------------------------------------------------------------

function pageInitForm(type){
	try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 3/14/2012
// Details:	Alert if a power usage exist already for this power circuit and redirect to last one.
//-------------------------------------------------------------------------------------------------

		var currentContext = nlapiGetContext();
		if (currentContext.getExecutionContext() == 'userinterface') {
			

			var pu = getUrlVars()['pu'];
			var so = getUrlVars()['so'];
			if (pu == 'T'){
				alert ('The power usage you are trying to edit is not the last record. You can not edit a past power usage record. This is the power usage record for the current period. You must edit and update this one.');
			}
		}

		
//---------- End Section 1 ------------------------------------------------------------------------------------------------

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}



// get the parameters from the URL
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}