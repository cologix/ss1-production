//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CF_Power_Circuit.js
//	Script Runs:	Client
//	Script Type:	Client Script / Form
//	Deployments:	Power Circuit Form
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		3/14/2012
//-------------------------------------------------------------------------------------------------

function pageInitForm(type){
	try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 3/14/2012
// Details:	Alert if a power usage exist already for this power circuit.
//-------------------------------------------------------------------------------------------------

		var currentContext = nlapiGetContext();
		if (currentContext.getExecutionContext() == 'userinterface') {
			
			var so = getUrlVars()['so'];
			var ns = getUrlVars()['ns'];

			if (so == 'F' && ns == 'T'){
				alert ('You can not create a power usage record for a power circuit that is not linked to a service order. You must add a service order and save the power circuit before being able to add a power usage.');
			}
			else if (so == 'T' && ns == 'T'){
				alert ('This power circuit is linked to a service order that does not have a power usage service. This service must be added to the service order prior to creating a power usage.');
			}
		}

		
//---------- End Section 1 ------------------------------------------------------------------------------------------------

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}



// get the parameters from the URL
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}