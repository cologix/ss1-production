nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Power_Circuit.js
//	Script Name:	CLGX_SU_Power_Circuit
//	Script Id:		customscript_clgx_su_power_circuit
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Project
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Date:			6/19/2014
//-------------------------------------------------------------------------------------------------

function beforeLoad(type, form) {
    try {

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Date:	03/12/2015
// Details:	No copy for power record
//-----------------------------------------------------------------------------------------------------------------

        var currentContext = nlapiGetContext();
        roleid = currentContext.getRole();
        var allow = 0;
        if (roleid == '-5' || roleid == '3' || roleid == '18') {
            allow = 1;
        }

        if ((type == 'copy' || type == 'create') && allow == 0) {
            var arrParam = new Array();
            arrParam['custscript_internal_message'] = 'You are not allowed to create or copy a power record.';
            nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
        }

//------------- Begin Section 2 -----------------------------------------------------------------------------------
// Date:	6/19/2014
// Details:	Hide device fields from other roles than admin or full or Keith
//-----------------------------------------------------------------------------------------------------------------

        var currentContext = nlapiGetContext();
        var powerid = nlapiGetRecordId();
        var userid = nlapiGetUser();
        var roleid = nlapiGetRole();

        if( (currentContext.getExecutionContext() == 'userinterface') && (type == 'edit' || type == 'view')){

            if (roleid != '-5' && roleid != '3' && roleid != '18' && roleid != '1052' && roleid != '1088') { // admin, full, cologix command

                form.removeButton('makecopy');
                form.removeButton('new');
                form.getField('name').setDisplayType('inline');

                if (nlapiGetField('custrecord_cologix_power_volts') != null && nlapiGetField('custrecord_cologix_power_volts') != ''){
                    form.getField('custrecord_cologix_power_volts').setDisplayType('inline');
                }
                if (nlapiGetField('custrecord_cologix_power_amps') != null && nlapiGetField('custrecord_cologix_power_amps') != ''){
                    form.getField('custrecord_cologix_power_amps').setDisplayType('inline');
                }

                if (nlapiGetField('custrecord_clgx_dcim_device') != null && nlapiGetField('custrecord_clgx_dcim_device') != ''){
                    form.getField('custrecord_clgx_dcim_device').setDisplayType('inline');
                }
                if (nlapiGetField('custrecord_clgx_dcim_points_consumption') != null && nlapiGetField('custrecord_clgx_dcim_points_consumption') != ''){
                    form.getField('custrecord_clgx_dcim_points_consumption').setDisplayType('inline');
                }
                if (nlapiGetField('custrecord_clgx_dcim_points_demand') != null && nlapiGetField('custrecord_clgx_dcim_points_demand') != ''){
                    form.getField('custrecord_clgx_dcim_points_demand').setDisplayType('inline');
                }
                if (nlapiGetField('custrecord_clgx_dcim_points_current') != null && nlapiGetField('custrecord_clgx_dcim_points_current') != ''){
                    form.getField('custrecord_clgx_dcim_points_current').setDisplayType('inline');
                }

                if (nlapiGetField('custrecord_clgx_dcim_points_voltage') != null && nlapiGetField('custrecord_clgx_dcim_points_voltage') != ''){
                    form.getField('custrecord_clgx_dcim_points_voltage').setDisplayType('inline');
                }
                if (nlapiGetField('custrecord_clgx_dcim_points_pwr_factor') != null && nlapiGetField('custrecord_clgx_dcim_points_pwr_factor') != ''){
                    form.getField('custrecord_clgx_dcim_points_pwr_factor').setDisplayType('inline');
                }

                if (nlapiGetField('custrecord_clgx_dcim_date_provision') != null && nlapiGetField('custrecord_clgx_dcim_date_provision') != ''){
                    form.getField('custrecord_clgx_dcim_date_provision').setDisplayType('inline');
                }
                if (nlapiGetField('custrecord_clgx_dcim_date_last_replicate') != null && nlapiGetField('custrecord_clgx_dcim_date_last_replicate') != ''){
                    form.getField('custrecord_clgx_dcim_date_last_replicate').setDisplayType('inline');
                }
                if (nlapiGetField('custrecord_clgx_dcim_date_disconnect') != null && nlapiGetField('custrecord_clgx_dcim_date_disconnect') != ''){
                    form.getField('custrecord_clgx_dcim_date_disconnect').setDisplayType('inline');
                }


                if (nlapiGetField('custrecord_clgx_dcim_sum_day_kw_a') != null && nlapiGetField('custrecord_clgx_dcim_sum_day_kw_a') != ''){
                    form.getField('custrecord_clgx_dcim_sum_day_kw_a').setDisplayType('inline');
                }
                if (nlapiGetField('custrecord_clgx_dcim_sum_day_kw_ab') != null && nlapiGetField('custrecord_clgx_dcim_sum_day_kw_ab') != ''){
                    form.getField('custrecord_clgx_dcim_sum_day_kw_ab').setDisplayType('inline');
                }
                if (nlapiGetField('custrecord_clgx_dcim_sum_day_amp_a') != null && nlapiGetField('custrecord_clgx_dcim_sum_day_amp_a') != ''){
                    form.getField('custrecord_clgx_dcim_sum_day_amp_a').setDisplayType('inline');
                }
                if (nlapiGetField('custrecord_clgx_dcim_sum_day_amp_ab') != null && nlapiGetField('custrecord_clgx_dcim_sum_day_amp_ab') != ''){
                    form.getField('custrecord_clgx_dcim_sum_day_amp_ab').setDisplayType('inline');
                }
            }

            var points = nlapiGetFieldValues('custrecord_clgx_dcim_points_current');
            if(points != null && points != '' && (roleid == '-5' || roleid == '3' || roleid == '18' || roleid == '1052')){
                var dcimTab = form.addTab('custpage_asset_tab_dcim', 'DCIM');
                var dcimField = form.addField('custpage_asset_dcim','inlinehtml', null, null, 'custpage_asset_tab_dcim');
                var dcimFrameHTML = '';
                dcimFrameHTML += '<iframe name="dcim" id="dcim" src="/app/site/hosting/scriptlet.nl?script=366&deploy=1&powerid=' + powerid + '" height="580px" width="1200px" frameborder="0" scrolling="no"></iframe>';
                dcimField.setDefaultValue(dcimFrameHTML);
            }

        }

//---------- End Sections ------------------------------------------------------------------------------------------------
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function beforeSubmit(type, form) {
    try {

        var currentContext = nlapiGetContext();
        var powerid = nlapiGetRecordId();
        var userid = nlapiGetUser();
        var roleid = nlapiGetRole();

        if (type == 'xedit' && (roleid == '1003' || roleid == '1039')){
            throw "This record may not be changed via inline editing. You will also receive an email error, no action is required.";
        }

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Date:	9/19/2014
// Details:	Manage provision, last update and disconnect dates for power
//-----------------------------------------------------------------------------------------------------------------

        if( (currentContext.getExecutionContext() == 'userinterface') && (type == 'edit' || type == 'create')){

            var d = new moment();
            var today = d.format('M/D/YYYY');

            var space = nlapiGetFieldValue('custrecord_cologix_power_space');
            var dateprovision = nlapiGetFieldValue('custrecord_clgx_dcim_date_provision');
            if((space != '' && space != null) && (dateprovision == '' || dateprovision == null)){
                nlapiSetFieldValue('custrecord_clgx_dcim_date_provision', today);
                nlapiSetFieldValue('custrecord_clgx_dcim_date_last_replicate', today);
            }

            var isinactive = nlapiGetFieldValue('isinactive');
            var datedisconnect = nlapiGetFieldValue('custrecord_clgx_dcim_date_disconnect');
            if(isinactive == 'T' && (datedisconnect == '' || datedisconnect == null)){
                nlapiSetFieldValue('custrecord_clgx_dcim_date_disconnect', today);
            }
        }

//------------- Begin Section 2 -----------------------------------------------------------------------------------
// Date:	03/12/2015
// Details:	Verify if pair was deleted - if yes change the name to '...P'
//-----------------------------------------------------------------------------------------------------------------

        if(currentContext.getExecutionContext() == 'userinterface' && type == 'edit'){

            var powerid = nlapiGetRecordId();
            var pairid = nlapiGetFieldValue('custrecord_clgx_dcim_pair_power');

            var oldPower = nlapiGetOldRecord();
            var oldpairid = oldPower.getFieldValue('custrecord_clgx_dcim_pair_power');

            if((pairid == null || pairid == '') && (oldpairid != null && oldpairid != '') ) {
                var powerstr = '0000000' + powerid + 'P';
                var length = powerstr.length;
                var powername = 'POWER' + powerstr.substr(length-8,length);
                nlapiSetFieldValue('name', powername);
            }
        }

//------------- Begin Section 3 -----------------------------------------------------------------------------------
// Date:	04/19/2018
// Details:	Populate Installed KVA and Power Chain
//-----------------------------------------------------------------------------------------------------------------
        if(currentContext.getExecutionContext() == 'userinterface' || (type == "edit" || type == "xedit")){
            try {
                nlapiSetFieldValue("custrecord_clgx_dcim_inst_kva_a", get_kva(nlapiGetFieldText("custrecord_cologix_power_volts"), nlapiGetFieldText("custrecord_clgx_power_amps_installed")));
                nlapiSetFieldValue("custrecord_clgx_dcim_cont_kva_a", get_kva(nlapiGetFieldText("custrecord_cologix_power_volts"), nlapiGetFieldText("custrecord_cologix_power_amps")));
                var chains = (nlapiLookupField('customrecord_clgx_dcim_devices', nlapiGetFieldValue("custrecord_clgx_dcim_device"), 'custrecord_clgx_dcim_device_pwr_chains')).split(",");
                nlapiSetFieldValue("custrecord_clgx_power_chain", chains[0]);
            } catch (ex) {
                nlapiLogExecution("DEBUG", "Device Power Chain Error", ex);
            }
        }
//---------- End Sections ------------------------------------------------------------------------------------------------

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}



function afterSubmit (type) {
    try {

        var currentContext = nlapiGetContext();
        var userid = nlapiGetUser();
        var roleid = nlapiGetRole();
        var recid  = nlapiGetRecordId();

//------------- Begin Section 2 -----------------------------------------------------------------------------------
// Date:	8/15/2014
// Details:	Update OD Device and points
//-----------------------------------------------------------------------------------------------------------------
        if(currentContext.getExecutionContext() == 'userinterface'&& type == 'edit'){
            var powerid = nlapiGetRecordId();
            var pwr = update_power (powerid);
        }

//------------- NNJ SYNC -----------------------------------------------------------------------------------

        var currentContext = nlapiGetContext();
        var environment = currentContext.getEnvironment();

        /*var facility = nlapiGetFieldValue('custrecord_cologix_power_facility');

        if (facility == 33 || facility == 34 || facility == 35 || facility == 36) {
        var row = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());

           var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
           record.setFieldValue('custrecord_clgx_queue_ping_record_id', nlapiGetRecordId());
           record.setFieldValue('custrecord_clgx_queue_ping_record_type', 6);
           record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
           var idRec = nlapiSubmitRecord(record, false,true);


           try {
               flexapi('POST','/netsuite/update', {
                'type': nlapiGetRecordType(),
                'id': nlapiGetRecordId(),
                'action': type,
                'userid': nlapiGetUser(),
                'user': nlapiLookupField('employee', nlapiGetUser(), 'entityid'),
                'data': json_serialize(row)
               });
           }
           catch (error) {
               nlapiLogExecution('DEBUG', 'flexapi error: ', error);
               var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
               record.setFieldValue('custrecord_clgx_queue_ping_record_id', nlapiGetRecordId());
               record.setFieldValue('custrecord_clgx_queue_ping_record_type', 6);
               record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
               var idRec = nlapiSubmitRecord(record, false,true);
           }

        }*/
        //}
//---------- End Sections ------------------------------------------------------------------------------------------------
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function get_kva (voltsStr, ampsStr) {

    var kva = 0.00;
    if(voltsStr){

        var volts = parseInt(voltsStr.substr(0, 3).replace("V", ""));
        var amps  = parseInt(ampsStr.replace("A", ""));

        switch(voltsStr) {
            case "120V Single Phase":
                kva = parseFloat((amps * 0.12).toFixed(2));
                break;
            case "208V Single Phase":
                kva = parseFloat((amps * 0.208).toFixed(2));
                break;
            case "208V Three Phase":
                kva = parseFloat((amps * Math.sqrt(3) * 0.208).toFixed(2));
                break;
            case "240V Single Phase":
                kva = parseFloat((amps * 0.24).toFixed(2));
                break;
            case "240V Three Phase":
                kva = parseFloat((amps * Math.sqrt(3) * 0.415).toFixed(2));
                break;
            case "600V Single Phase":
                kva = parseFloat((amps * 0.347).tofixed(2));
                break;
            case "600V Three Phase":
                kva = parseFloat((amps * 0.347).toFixed(2));
                break;
            default:
                kva = parseFloat(((volts * amps) / 1000).toFixed(2));
        }
    }
    kva = parseFloat((kva * 0.8).toFixed(2));
    return kva;
}