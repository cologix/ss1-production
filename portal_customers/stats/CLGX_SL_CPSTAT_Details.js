//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CPSTAT_Details.js
//	Script Name:	CLGX_SL_CPSTAT_Details
//	Script Id:		customscript_clgx_sl_clstat_details
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		4/25/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=297&deploy=1  
//-------------------------------------------------------------------------------------------------

function suitelet_clstat_details (request, response){
	try {
		
		var month = request.getParameter('monthid');
		var year = request.getParameter('yearid');
		var customerid = request.getParameter('customerid');
		
		if(month > 0 || customerid > 0){
			var objFile = nlapiLoadFile(1188537);
			var html = objFile.getValue();
			html = html.replace(new RegExp('{visits}','g'), get_visits (month, year, customerid));
		}
		else{
			var html = 'Please select a month or a customer from the left panel.';
		}
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function get_visits (month, year, customerid){

	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_year_display",null,"is",yearid));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_month_display",null,"anyof",monthid));
	var searchCustomers = nlapiLoadSearch('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_ci_customers');
	searchCustomers.addFilters(arrFilters);
	
	var resultSet = searchCustomers.runSearch();
	var arrCustomers = new Array();
	resultSet.forEachResult(function(searchResult) {
		var colObj = new Object();
		colObj = new Object();
		colObj["customerid"] = parseInt(searchResult.getValue('custrecord_clgx_consol_inv_customer', null, 'GROUP'));
		colObj["customer"] = searchResult.getText('custrecord_clgx_consol_inv_customer', null, 'GROUP');
		colObj["yearid"] = parseInt(yearid);
		colObj["monthid"] = monthid;
		arrCustomers.push(colObj);
		return true; // return true to keep iterating
	});

    return JSON.stringify(arrCustomers);
}
