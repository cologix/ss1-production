nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CPSTAT_Menu.js
//	Script Name:	CLGX_SL_CPSTAT_Menu
//	Script Id:		customscript_clgx_sl_cpstat_menu
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/14/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=264&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cpstat_menu (request, response){
	try {
		var objFile = nlapiLoadFile(3072989);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{customers}','g'), get_customers());
		html = html.replace(new RegExp('{years}','g'), get_years());
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function get_customers(){

	var searchCustomers = nlapiLoadSearch('customrecord_clgx_portal_sessions_archiv', 'customsearch_clgx_cpstat_customers');

	var resultSet = searchCustomers.runSearch();
	var arrCustomers = new Array();
	resultSet.forEachResult(function(searchResult) {
		var colObj = new Object();
		colObj["customerid"] = parseInt(searchResult.getValue('custrecord_clgx_portal_archive_cust_id', null, 'GROUP'));
		colObj["customer"] = searchResult.getValue('custrecord_clgx_portal_archive_cust_name', null, 'GROUP');
		colObj["visits"] = parseInt(searchResult.getValue('internalid', null, 'COUNT'));
		arrCustomers.push(colObj);

		return true; // return true to keep iterating
	});
	
    return JSON.stringify(arrCustomers);
}

function get_years(){	
	var arrColumns = new Array();
	var arrFilters = new Array();
	var searchResults = nlapiSearchRecord('customrecord_clgx_portal_sessions_archiv', 'customsearch_clgx_cpstat_years_months', arrFilters, arrColumns);

	var arrDates = new Array();
	for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
		var columns = searchResults[i].getAllColumns();
		var ymdate = searchResults[i].getValue(columns[0]);
		var visits = parseInt(searchResults[i].getValue(columns[1]));
		var objDate = new Object();
		objDate["year"] = ymdate.substring(0, 4);
		objDate["month"] = ymdate.substring(5, 8);
		objDate["visits"] = visits;
		arrDates.push(objDate);
	}
	
	var arrY = (_.uniq(_.pluck(arrDates, 'year')));
	
	var arrYears = new Array();
	for ( var i = 0; arrY != null && i < arrY.length; i++ ) {
		
		var objYear = new Object();
		var arrMonths = new Array();
		objYear["node"] = arrY[i];
		objYear["year"] = arrY[i];
		objYear["month"] = '';
		objYear["leaf"] = false;
		if(i == 0){
			objYear["expanded"] = true;
		}
		else{
			objYear["expanded"] = false;
		}
		
		objYear["iconCls"] = 'year';
		
		
	    var arrM = _.filter(arrDates, function(arr){
	        return (arr.year == arrY[i]);
		});
	    var yvisits = 0;
	    for ( var j = 0; arrM != null && j < arrM.length; j++ ) {
    		var objMonth = new Object();
    		objMonth["node"] = arrM[j].month;
    		objMonth["year"] = arrM[j].year;
    		objMonth["month"] = arrM[j].month;
    		objMonth["visits"] = arrM[j].visits;
    		yvisits += arrM[j].visits;
    		objMonth["leaf"] = true;
    		objMonth["iconCls"] = 'month';
    		arrMonths.push(objMonth);
	    }
	    objYear["visits"] = yvisits;
    	objYear["children"] = arrMonths;
    	arrYears.push(objYear);
	}
	
    var objTree = new Object();
    objTree["text"] = '.';
    objTree["children"] = arrYears;
	
	return JSON.stringify(objTree);}


