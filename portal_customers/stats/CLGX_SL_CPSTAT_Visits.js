nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CPSTAT_Visits.js
//	Script Name:	CLGX_SL_CPSTAT_Visits
//	Script Id:		customscript_clgx_sl_clstat_visits
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		10/15/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=558&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_clstat_visits (request, response){
	try {
		
		var month = request.getParameter('month');
		var year = request.getParameter('year');
		var customerid = request.getParameter('customerid');
		
		if(month > 0 || customerid > 0){
			var objFile = nlapiLoadFile(3074107);
			var html = objFile.getValue();
			html = html.replace(new RegExp('{visits}','g'), get_visits (month, year, customerid));
		}
		else{
			var html = 'Please select a month or a customer from the left panel.';
		}
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function get_visits (month, year, customerid){

	if(month != '0'){
		var startDate = new moment(month + '/01/' + year).format('M/D/YYYY');
		var endDate = new moment(month + '/01/' + year).endOf('month').format('M/D/YYYY');
	}
	
	var arrFilters = new Array();
	if(month != '0'){
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_portal_archive_date", null,'within',startDate,endDate));
	}
	if(customerid  > 0){
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_portal_archive_cust_id",null,"equalto",customerid));
	}
	var searchVisits = nlapiLoadSearch('customrecord_clgx_portal_sessions_archiv', 'customsearch_clgx_cpstat_visits');
	searchVisits.addFilters(arrFilters);
	
	var resultSet = searchVisits.runSearch();
	var arrVisits = new Array();
	resultSet.forEachResult(function(searchResult) {
		var objVisit = new Object();
		objVisit["date"] = searchResult.getValue('custrecord_clgx_portal_archive_date', null, null);
		objVisit["time"] = searchResult.getValue('custrecord_clgx_portal_archive_time', null, null);
		objVisit["customer"] = searchResult.getValue('custrecord_clgx_portal_archive_cust_name', null, null);
		objVisit["contact"] = searchResult.getValue('custrecord_clgx_portal_archive_user_name', null, null);
		objVisit["ip"] = searchResult.getValue('custrecord_clgx_portal_archive_user_ip', null, null);
		arrVisits.push(objVisit);
		return true; // return true to keep iterating
	});

    return JSON.stringify(arrVisits);
}
