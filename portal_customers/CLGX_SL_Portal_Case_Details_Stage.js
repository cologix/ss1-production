nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Portal_Case_Details.js
//	Script Name:	CLGX_SL_Portal_Case_Details
//	Script Id:		customscript_clgx_sl_portal_case_details
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		7/10/2012
//-------------------------------------------------------------------------------------------------

function suitelet_portal_case_details(request, response){
try{
	
	var sid = "";
	var userIP = "";
	
	sid = request.getParameter('sid');
	userIP = request.getHeader('NS-Client-IP');
	if (sid == null || sid == ''){
		response.write( 'Your session has expired.' );
	}
	else{
	
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
		arrColumns[1] = new nlobjSearchColumn('custrecord_clgx_portal_session_sid', null, null);
		arrColumns[2] = new nlobjSearchColumn('custrecord_clgx_portal_session_subsid', null, null);
		arrColumns[3] = new nlobjSearchColumn('custrecord_clgx_portal_session_customer', null, null);
		arrColumns[4] = new nlobjSearchColumn('custrecord_clgx_portal_session_contact', null, null);
		arrFilters[0] = new nlobjSearchFilter('custrecord_clgx_portal_session_sid',null,'is',sid);
		arrFilters[1] = new nlobjSearchFilter('custrecord_clgx_portal_session_user_ip',null,'is',userIP);
		var searchSession = nlapiSearchRecord('customrecord_clgx_portal_sessions_manage', null, arrFilters, arrColumns);
		
		if (searchSession == null) { // wrong session ID - probably expired, send login form back
			response.write( 'Your session has expired.' );
		}
		else{ // this is a valid and still active session
			
			var id = "";
			caseID = request.getParameter('id');
			
			var arrSearchFilters = new Array(); 
			var arrSearchColumns = new Array(); 
			arrSearchFilters[0] = new nlobjSearchFilter('internalid', null, 'is', caseID); 
			arrSearchFilters[1] = new nlobjSearchFilter('internalonly','messages', 'is', 'F');
			arrSearchColumns[0] = new nlobjSearchColumn('message','messages'); 
			arrSearchColumns[1] = new nlobjSearchColumn('messagedate','messages').setSort(true);   
			var arrSearchResults = nlapiSearchRecord('supportcase','null',arrSearchFilters,arrSearchColumns); 
			
			var stMessage = '=======================================================================================</br>';
			for ( var j = 0; arrSearchResults != null && j < arrSearchResults.length; j++ ) {
				var searchResult = arrSearchResults[j];
				var message= searchResult.getValue(arrSearchColumns[0]);
				var messagedate= searchResult.getValue(arrSearchColumns[1]);
				stMessage += 'Date :' + messagedate + '</br></br>';
				stMessage += message + '</br></br>';
				stMessage += '=======================================================================================</br>';
			}
			
			stMessage = stMessage.replace(/\n/g, '<br>')
			//stMessage = stMessage.replace(/\'/g," ");
			//stMessage = stMessage.replace(/\"/g," ");
			
			response.write( stMessage ); 
			
		}
	}
}
catch (error){
	response.write( 'An error has occurred. Please contact support and provide all the details.' );
}
}
