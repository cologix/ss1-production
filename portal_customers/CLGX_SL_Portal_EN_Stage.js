nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Portal_EN.js
//	Script Name:	CLGX_SL_Portal_EN
//	Script Id:		customscript_clgx_sl_portal_en
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		5/28/2012
//-------------------------------------------------------------------------------------------------

function suitelet_portal(request, response){
try{
	// staging/production switch
	var stage = 1;
	var baseurl = 'https://forms.na1.netsuite.com';
	if(stage == 1){ // staging
		var templateLogin = 114656;
		var templatePortal = 114658;
		var portalURL = baseurl + '/app/site/hosting/scriptlet.nl?script=127&deploy=1&compid=1337135&h=5a46cd55c1542a2a567a';
		var loginURL = baseurl + '/app/site/hosting/scriptlet.nl?script=126&deploy=1&compid=1337135&h=d577c0b8b1b0fb618761';
		var caseDetailsURL = baseurl + '/app/site/hosting/scriptlet.nl?script=125&deploy=1&compid=1337135&h=7a9734553c197d996735';
	}
	else{ // production
		var templateLogin = 114655;
		var templatePortal = 114657;
		var portalURL = baseurl + '/app/site/hosting/scriptlet.nl?script=107&deploy=1&compid=1337135&h=2cfbedb3f83533636ae1';
		var loginURL = baseurl + '/app/site/hosting/scriptlet.nl?script=108&deploy=1&compid=1337135&h=cc6e4cabefab4198bb56';
		var caseDetailsURL = baseurl + '/app/site/hosting/scriptlet.nl?script=118&deploy=1&compid=1337135&h=872feb3bed2f5fa95103';
	}
	
	var customerID = '';
	var act = "";
	var alertMessage = "";
	var passwordHTML = '';
	var disableHTML = '';
	var confirmHTML = '';
	var confirmColor = 'green';
	var confirmIcon = 'blank.gif';
	var confirmStyle = 'blank';
	var parentHTML = '';
	
	var sid = request.getParameter('sid');
	var tab = request.getParameter('tab');
	if (tab == null){
		tab = 0;
	}
	var userIP = request.getHeader('NS-Client-IP');
	act = request.getParameter('act');
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
	arrColumns[1] = new nlobjSearchColumn('custrecord_clgx_portal_session_terms', null, null);
	arrColumns[2] = new nlobjSearchColumn('custrecord_clgx_portal_session_contact', null, null);
	arrFilters[0] = new nlobjSearchFilter('custrecord_clgx_portal_session_sid',null,'is',sid);
	arrFilters[1] = new nlobjSearchFilter('custrecord_clgx_portal_session_user_ip',null,'is',userIP);
	var searchSession = nlapiSearchRecord('customrecord_clgx_portal_sessions_manage', null, arrFilters, arrColumns);
	
	if (searchSession == null) { // wrong session ID - probably expired, send login form back
		alertMessage = 'Your session has expired.';
		var objFile = nlapiLoadFile(templateLogin);
		var stMainHTML = objFile.getValue();
		stMainHTML = stMainHTML.replace(new RegExp('{alertMessage}','g'),alertMessage);
		stMainHTML = stMainHTML.replace(new RegExp('{passwordHTML}','g'),passwordHTML);
		stMainHTML = stMainHTML.replace(new RegExp('{loginURL}','g'),loginURL);
		stMainHTML = stMainHTML.replace(new RegExp('{disableHTML}','g'),disableHTML);
		response.write( stMainHTML );
	}

	else{ // this is a valid and still active session
		
		var resultRow = searchSession[0];
		var columns = resultRow.getAllColumns();
		var sidID = resultRow.getValue(columns[0]);
		var terms = resultRow.getValue(columns[1]);
		var contactID = resultRow.getValue(columns[2]);
		
		if (terms == 'F' && act != 'terms'){ // contact has to accept terms
			var objFile = nlapiLoadFile(169070);
			var stMainHTML = objFile.getValue();
			stMainHTML = stMainHTML.replace(new RegExp('{portalURL}','g'),portalURL);
		    stMainHTML = stMainHTML.replace(new RegExp('{sid}','g'),sid);
			response.write( stMainHTML );
		}
		else{
			// update session and contact
			if (act != 'logout'){
				nlapiSubmitField('customrecord_clgx_portal_sessions_manage', sidID, ['custrecord_clgx_portal_session_terms'], 'T');
				nlapiSubmitField('contact', contactID, ['custentity_clgx_portal_accepted_terms'], 'T');
			}
			// verify if it's a contact from a parent company asking to edit one of the companies
			var company = '';
			company = request.getParameter('company');
			if (company != null & act == 'company'){ // update on session record the company to be edited
				nlapiSubmitField('customrecord_clgx_portal_sessions_manage', sidID, ['custrecord_clgx_portal_session_company'], [company]);
			}
	
			var arrColumns = new Array();
			var arrFilters = new Array();
			arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
			arrColumns[1] = new nlobjSearchColumn('custrecord_clgx_portal_session_sid', null, null);
			arrColumns[2] = new nlobjSearchColumn('custrecord_clgx_portal_session_subsid', null, null);
			arrColumns[3] = new nlobjSearchColumn('custrecord_clgx_portal_session_customer', null, null);
			arrColumns[4] = new nlobjSearchColumn('custrecord_clgx_portal_session_contact', null, null);
			arrColumns[5] = new nlobjSearchColumn('custrecord_clgx_portal_session_company', null, null);
			arrColumns[6] = new nlobjSearchColumn('custrecord_clgx_portal_session_is_parent', null, null);
			arrColumns[7] = new nlobjSearchColumn('custrecord_clgx_portal_session_rights', null, null);
			arrColumns[8] = new nlobjSearchColumn('custrecord_clgx_portal_session_sales_rep', null, null);
			arrFilters[0] = new nlobjSearchFilter('custrecord_clgx_portal_session_sid',null,'is',sid);
			arrFilters[1] = new nlobjSearchFilter('custrecord_clgx_portal_session_user_ip',null,'is',userIP);
			var searchSession = nlapiSearchRecord('customrecord_clgx_portal_sessions_manage', null, arrFilters, arrColumns);
	
			var resultRow = searchSession[0];
			var columns = resultRow.getAllColumns();
			var sidID = resultRow.getValue(columns[0]);
			var sid = resultRow.getValue(columns[1]);
			var subsidiaryID = resultRow.getValue(columns[2]);
			var customerID = resultRow.getValue(columns[3]);
			var contactID = resultRow.getValue(columns[4]);
			var companyID = resultRow.getValue(columns[5]);
			var parent = resultRow.getValue(columns[6]);
			var sessionRights = resultRow.getValue(columns[7]);
			var salesRep = resultRow.getValue(columns[8]);
			
			//if(parent == 'T'){
			// construct companies selection lists
			var arrSearchColumns = new Array();
			var arrSearchFilters = new Array();
			arrSearchColumns[0] = new nlobjSearchColumn('internalid', 'customer', null);
			arrSearchColumns[1] = new nlobjSearchColumn('companyname', 'customer', null);
			arrSearchFilters[0] = new nlobjSearchFilter('internalid',null,'is',contactID);
			var searchCompanies = nlapiSearchRecord('contact', null, arrSearchFilters, arrSearchColumns);
			
			var selectCompanies = '<select id="company" name="company">';
			var companiesData = '';
			var selected = '';
			var disabled = '';
			var nbrCompanies = searchCompanies.length;
			if(nbrCompanies == 1){
				disabled = ' disabled="disabled"'
			}
			parentHTML += 'Manage Company : <select id="company" name="company" ' + disabled + '>';
			for ( var i = 0; searchCompanies != null && i < searchCompanies.length; i++ ) {
				var searchCompany = searchCompanies[i];
				if (searchCompany.getValue('internalid', 'customer') == companyID){
					selected = 'selected="selected"';
				}
				parentHTML += '<option value="'+ searchCompany.getValue('internalid', 'customer') +'" '+ selected + '>'+ searchCompany.getValue('companyname', 'customer') +'</option>';
				selectCompanies += '<option value="'+ searchCompany.getValue('internalid', 'customer') +'" '+ selected + '>'+ searchCompany.getValue('companyname', 'customer') +'</option>';
				selected = '';
			}
			parentHTML += '</select><input type="submit" value="Go"' + disabled + '>';
			selectCompanies += '</select>';
			//}
	
			// construct contacts selection lists
			var arrSearchColumns = new Array();
			var arrSearchFilters = new Array();
			arrSearchColumns[0] = new nlobjSearchColumn('internalid', null, null);
			arrSearchColumns[1] = new nlobjSearchColumn('entityid', null, null);
			//arrSearchFilters[0] = new nlobjSearchFilter('company',null,'anyof',companyID);
			arrSearchFilters[0] = new nlobjSearchFilter('internalid','customer','is',companyID);
			arrSearchFilters[1] = new nlobjSearchFilter('isinactive',null,'is','F');
			var searchContacts = nlapiSearchRecord('contact', null, arrSearchFilters, arrSearchColumns);
			
			var selectContactsHTML = '<select name="supervisor"><option></option>';
			for ( var i = 0; searchContacts != null && i < searchContacts.length; i++ ) {
				var searchContact = searchContacts[i];
				selectContactsHTML += '<option value="'+ searchContact.getValue('internalid', null) +'">'+ searchContact.getValue('entityid', null) +'</option>';
			}
			selectContactsHTML += '</select>';
	
			var arrCustomerDetails = nlapiLookupField('customer',companyID,['salesrep','companyname']);
			var salesRepID = arrCustomerDetails['salesrep'];
			var custCompName = arrCustomerDetails['companyname'];
	
			var arrContactDetails = nlapiLookupField('contact',contactID,['entityid']);
	        var custContactName = arrContactDetails['entityid'];
	        
			var arrRepDetails = nlapiLookupField('employee',salesRepID,['entityid']);
	        var salesRepName = arrRepDetails['entityid'];
	        
	        
			switch(act) { // verify if any action was asked
			case 'newCase': //------------------------------------------------create new case
				var subject = request.getParameter('subject');
				var casetype = request.getParameter('casetype');
				var priority = request.getParameter('priority');
				var facility = request.getParameter('facility');
				var casereqDate = request.getParameter('casereqdate');
				
				var date = new Date();
				var record = nlapiCreateRecord('supportcase');
				
				var message = request.getParameter('message');
				if(message !=null && message !=''){
					var stMessage = '';
					stMessage += 'Requested Date/Time: ' + casereqDate;
					stMessage +=  '\n================================\n\n';
					
					stMessage +=  message;	
					record.setFieldValue('incomingmessage', stMessage);
				}
	
				record.setFieldValue('title', subject);
				record.setFieldValue('company', companyID);
				record.setFieldValue('custevent_cologix_facility', facility);
				
				// 'Assign To' dependent on Facility
	        	switch(facility) {
				case '3': // Dallas
					record.setFieldValue('assigned', 2883);
					break;
		                case '18': // Dallas
					record.setFieldValue('assigned', 2883);
					break;
				case '2': // MTL1
					record.setFieldValue('assigned', 5241);
					break;
				case '5': // MTL2
					record.setFieldValue('assigned', 5393);
					break;
				case '4': // MTL3
					record.setFieldValue('assigned', 5394);
					break;
				case '9': // MTL4
					record.setFieldValue('assigned', 5395);
					break;
				case '7': // MTL5
					record.setFieldValue('assigned', 5396);
					break;
				case '6': // MTL6
					record.setFieldValue('assigned', 5400);
					break;
                               case '19': // MTL7
					record.setFieldValue('assigned', 179538);
					break;
                               case '21': // JAX
					record.setFieldValue('assigned', 231735);
					break;
				case '8': // Toronto
					record.setFieldValue('assigned', 12883);
					break;
				case '14': // Vancouver
					record.setFieldValue('assigned', 12884);
					break;
			      case '20': // Vancouver
					record.setFieldValue('assigned', 12884);
					break;
				case '15': // Toronto
					record.setFieldValue('assigned', 12883);
					break;
                               case '24': // Columbus
					record.setFieldValue('assigned', 232971);
					break;
				case '17': // Minneapolis
					record.setFieldValue('assigned', 10505);
					break;
	                        case '25': // Minneapolis
					record.setFieldValue('assigned', 10505);
					break;
				default:
					record.setFieldValue('assigned', '');
	        	}
				
	        	
				if (casetype == 'Remote Hands/On Demand'){
					record.setFieldText('category', 'Remote Hands');
					record.setFieldText('custevent_cologix_sub_case_type', 'On Demand');
				}
				else if (casetype == 'Remote Hands/Scheduled'){
					record.setFieldText('category', 'Remote Hands');
					record.setFieldText('custevent_cologix_sub_case_type', 'Scheduled');
				}
				else if (casetype == 'Incident'){
					record.setFieldText('category', 'Incident');
					record.setFieldText('custevent_cologix_sub_case_type', 'Customer');
				}
				else if (casetype == 'Billing/Information Request'){
					record.setFieldText('category', 'Finance Helpdesk');
					record.setFieldText('custevent_cologix_sub_case_type', 'Information Request');
				}
				else if (casetype == 'Billing/Payment Inquiry'){
					record.setFieldText('category', 'Finance Helpdesk');
					record.setFieldText('custevent_cologix_sub_case_type', 'Payment');
				}
				else if (casetype == 'Billing/Dispute'){
					record.setFieldText('category', 'Finance Helpdesk');
					record.setFieldText('custevent_cologix_sub_case_type', 'Dispute');
				}
				else{
					// set no category
				}
				
				if(customerID == companyID){
					record.setFieldValue('contact', contactID);
				}
				
				record.setFieldText('priority', priority);
				record.setFieldText('origin', 'Web');
				record.setFieldValue('startdate', nlapiDateToString(date,'date'));
				record.setFieldValue('starttime', nlapiDateToString(date,'timeofday'));
				
				var idRec = nlapiSubmitRecord(record, true,true);
				
				confirmHTML = 'Your case has been created successfully!';
				confirmIcon = 'ok.png';
				confirmStyle = 'ok';
				
				break;
			case 'updateCase': //------------------------------------------------update case
				var caseID = request.getParameter('id');
				var closeCase = request.getParameter('close');
				var newMessage = request.getParameter('message');
				
				if(closeCase == 'on'){
					var objCase = nlapiLoadRecord('supportcase', caseID);
					objCase.setFieldValue('messagenew', 'T');
					newMessage += '\n\nClosed By: ' + custContactName;
					objCase.setFieldValue('incomingmessage', newMessage);
					objCase.setFieldValue('status', 5);
					nlapiSubmitRecord(objCase, true,true);
				}
				else if (closeCase != 'on' && newMessage !=''){
					var objCase = nlapiLoadRecord('supportcase', caseID);
					objCase.setFieldValue('messagenew', 'T');
					newMessage += '\n\nUpdated By: ' + custContactName;
					objCase.setFieldValue('incomingmessage', newMessage);
					nlapiSubmitRecord(objCase, true,true);
				}
				else{}
	
				confirmHTML = 'Your case has been updated successfully!';
				confirmIcon = 'ok.png';
				confirmStyle = 'ok';
				
				break;
			case 'emailInvoices': //------------------------------------------------retrieve invoices by email
	
				invoices = request.getParameter('invoices');
				
				var record = nlapiCreateRecord('customrecord_clgx_portal_email_invoices');
				record.setFieldValue('custrecord_clgx_portal_invoices_customer', companyID);
				record.setFieldValue('custrecord_clgx_portal_invoices_contact', contactID);
				record.setFieldValue('custrecord_clgx_portal_invoices_list', invoices);
				var idEmail = nlapiSubmitRecord(record, true); // create record for invoice by email
				
				confirmHTML = 'The selected invoice(s) will be emailed to you within an hour.';
				confirmIcon = 'ok.png';
				confirmStyle = 'ok';
				
				break;
				
			case 'updateContact': //------------------------------------------------update existing contact
				
				var updateID = request.getParameter('id');
				
				if(updateID == null || updateID == ''){ // this is a demand to create a new contact
					
					var name = request.getParameter('name');
					var jobtitle = request.getParameter('jobtitle');
					var role = request.getParameter('role');
					var role2 = request.getParameter('role2');
					var supervisor = request.getParameter('supervisor');
					var phone = request.getParameter('phone');
					var email = request.getParameter('email');
					
					var newPassword = generatePassword(8,false);
					
					// verify first  if the email exist
					var arrFilters = new Array();
					arrFilters[0] = new nlobjSearchFilter('email',null,'is',email);
					var searchContactEmail = nlapiSearchRecord('contact', null, arrFilters, null);
			
					if (searchContactEmail != null) { // this email exist
						confirmHTML = 'This email exist. Please consider attaching the contact.';
						confirmColor = 'darkred';
						confirmIcon = 'cancel.png';
						confirmStyle = 'cancel';
					}
					else{ // this is a new and unique email, create user
						
						// verify if this is the only billing contact
						var arrFilters = new Array();
						arrFilters[0] = new nlobjSearchFilter('company',null,'anyof',companyID);
						arrFilters[1] = new nlobjSearchFilter('contactrole',null,'anyof',1);
						var searchBillingEmail = nlapiSearchRecord('contact', null, arrFilters, null);
						
						if (searchBillingEmail == null && role != 1) {
							confirmHTML = 'The contact was not created! You must have at least one contact with billing role, excluding attached contacts.';
							confirmColor = 'darkred';
							confirmIcon = 'cancel.png';
							confirmStyle = 'cancel';
						}
						else{
							// configure portal rights
							var rightsStr = new Array();
							// read only rights
							if(request.getParameter('cases') == 'on'){rightsStr.push(1);}
							else{rightsStr.push(0);}
							if(request.getParameter('invoices') == 'on'){rightsStr.push(1);}
							else{rightsStr.push(0);}
							if(request.getParameter('contacts') == 'on'){rightsStr.push(1);}
							else{rightsStr.push(0);}
							if(request.getParameter('service') == 'on'){rightsStr.push(1);}
							else{rightsStr.push(0);}
							// write rights
							if(request.getParameter('cases_edit') == 'on'){
								rightsStr.push(1);
								rightsStr[0] = 1;
							}
							else{rightsStr.push(0);}
							if(request.getParameter('invoices_edit') == 'on'){
								rightsStr.push(1);
								rightsStr[1] = 1;
							}
							else{rightsStr.push(0);}
							if(request.getParameter('contacts_edit') == 'on'){
								rightsStr.push(1);
								rightsStr[2] = 1;
							}
							else{rightsStr.push(0);}
		
							var record = nlapiCreateRecord('contact');
							record.setFieldValue('subsidiary', subsidiaryID);
							record.setFieldValue('company', companyID);
							record.setFieldValue('entityid', name);
							record.setFieldValue('title', jobtitle);
							record.setFieldValue('contactrole', role);
							record.setFieldValue('custentity_clgx_contact_secondary_role', role2);
							record.setFieldValue('supervisor', supervisor);
							record.setFieldValue('phone', phone);
							record.setFieldValue('email', email);
							record.setFieldValue('custentity_clgx_portal_password', newPassword);
							record.setFieldValue('custentity_clgx_portal_login_attempts', 0);
							record.setFieldValue('custentity_clgx_portal_rights', rightsStr.toString());
							var idRec = nlapiSubmitRecord(record, true,true);
			
							emailSubject = 'Cologix Customer Portal';
							emailSubjectConfirm = 'Cologix Customer Portal';
							emailBody = 'Dear Customer,\n\n' +
										'Here is your password:\n\n' + newPassword + '\n\n' +
										'Thank you for your business.\n\n' +
										'Sincerely,\n' +
										'Cologix\n\n';
							nlapiSendEmail(salesRepID,idRec,emailSubjectConfirm,emailBody,null,null,null,null);
							
							confirmHTML = 'Your new user has been created successfully!';
							confirmIcon = 'ok.png';
							confirmStyle = 'ok';
						}
					}
				}
				else{ // this is a contact update request
	
					var name = request.getParameter('name');
					var jobtitle = request.getParameter('jobtitle');
					var role = request.getParameter('role');
					var role2 = request.getParameter('role2');
					var supervisor = request.getParameter('supervisor');
					var phone = request.getParameter('phone');
					var email = request.getParameter('email');
					
					// verify first  if the email exist for another user
					var arrFilters = new Array();
					arrFilters[0] = new nlobjSearchFilter('internalid',null,'noneof',updateID);
					arrFilters[1] = new nlobjSearchFilter('email',null,'is',email);
					var searchContactEmail = nlapiSearchRecord('contact', null, arrFilters, null);
					
					if (searchContactEmail != null) { // this email exist
						confirmHTML = 'This email exist. Please consider attaching the contact.';
						confirmColor = 'darkred';
						confirmIcon = 'cancel.png';
						confirmStyle = 'cancel';
					}
					else{ // this is still an unique email, update user
						
						// verify if this is the only billing contact
						var arrFilters = new Array();
						arrFilters[0] = new nlobjSearchFilter('internalid',null,'noneof',updateID);
						arrFilters[1] = new nlobjSearchFilter('company',null,'anyof',companyID);
						arrFilters[2] = new nlobjSearchFilter('contactrole',null,'anyof',1);
						var searchBillingEmail = nlapiSearchRecord('contact', null, arrFilters, null);
						
						if (searchBillingEmail == null && role != 1) {
							confirmHTML = 'The contact was not updated! You must have at least one contact with billing role, excluding attached contacts.';
							confirmColor = 'darkred';
							confirmIcon = 'cancel.png';
							confirmStyle = 'cancel';
						}
						else{
							// configure portal rights
							var rightsStr = new Array();
							// read only rights
							if(request.getParameter('cases') == 'on'){rightsStr.push(1);}
							else{rightsStr.push(0);}
							if(request.getParameter('invoices') == 'on'){rightsStr.push(1);}
							else{rightsStr.push(0);}
							if(request.getParameter('contacts') == 'on'){rightsStr.push(1);}
							else{rightsStr.push(0);}
							if(request.getParameter('service') == 'on'){rightsStr.push(1);}
							else{rightsStr.push(0);}
							// write rights
							if(request.getParameter('cases_edit') == 'on'){
								rightsStr.push(1);
								rightsStr[0] = 1;
							}
							else{rightsStr.push(0);}
							if(request.getParameter('invoices_edit') == 'on'){
								rightsStr.push(1);
								rightsStr[1] = 1;
							}
							else{rightsStr.push(0);}
							if(request.getParameter('contacts_edit') == 'on'){
								rightsStr.push(1);
								rightsStr[2] = 1;
							}
							else{rightsStr.push(0);}
				
							var objContact = nlapiLoadRecord('contact', updateID);
							var askPassword = request.getParameter('password');
							if(askPassword == 'on'){
								var newPassword = generatePassword(8,false);
								objContact.setFieldValue('custentity_clgx_portal_password', newPassword);
								
								emailSubject = 'Cologix Customer Portal';
								emailSubjectConfirm = 'Cologix Customer Portal';
								emailBody = 'Dear Customer,\n\n' +
											'Here is your new password:\n\n' + newPassword + '\n\n' +
											'Thank you for your business.\n\n' +
											'Sincerely,\n' +
											'Cologix\n\n';
								nlapiSendEmail(salesRepID,updateID,emailSubject,emailBody,null,null,null,null);
								
							}
				
							objContact.setFieldValue('entityid', name);
							objContact.setFieldValue('title', jobtitle);
							objContact.setFieldValue('contactrole', role);
							objContact.setFieldValue('custentity_clgx_contact_secondary_role', role2);
							objContact.setFieldValue('supervisor', supervisor);
							objContact.setFieldValue('phone', phone);
							objContact.setFieldValue('email', email);
							objContact.setFieldValue('custentity_clgx_portal_rights', rightsStr.toString());
							objContact.setFieldValue('custentity_clgx_portal_accepted_terms', 'F');
							nlapiSubmitRecord(objContact, true, true);
				
							confirmHTML = 'Your user has been updated successfully!';
							confirmIcon = 'ok.png';
							confirmStyle = 'ok';
						}	
					}
				}
				
				break;
			case 'deleteContact': //------------------------------------------------delete contact
				
				var deleteID = request.getParameter('id');
				
				// verify if this is the only billing contact
				var arrFilters = new Array();
				arrFilters[0] = new nlobjSearchFilter('internalid',null,'noneof',deleteID);
				arrFilters[1] = new nlobjSearchFilter('company',null,'anyof',companyID);
				arrFilters[2] = new nlobjSearchFilter('contactrole',null,'anyof',1);
				var searchBillingEmail = nlapiSearchRecord('contact', null, arrFilters, null);
				
				if (searchBillingEmail == null) {
					confirmHTML = 'The contact was not deleted! You must have at least one contact with billing role, excluding attached contacts.';
					confirmColor = 'darkred';
					confirmIcon = 'cancel.png';
					confirmStyle = 'cancel';
				}
				else{
					
					var objContact = nlapiLoadRecord('contact', deleteID);
					objContact.setFieldValue('isinactive', 'T');
					nlapiSubmitRecord(objContact, true, true);
					//nlapiDeleteRecord('contact', deleteID );
					
					confirmHTML = 'Your user has been removed successfully!';
					confirmIcon = 'ok.png';
					confirmStyle = 'ok';
				}
	
				break;
				
			case 'attachContact': //------------------------------------------------attach contact
				
				var attachID = request.getParameter('id');
				var attachToID = request.getParameter('company');
				var role = request.getParameter('role');
				
				var arrSearchFilters = new Array();
				arrSearchFilters[0] = new nlobjSearchFilter('internalid',null,'is',attachID);
				arrSearchFilters[1] = new nlobjSearchFilter('internalid','customer','is',attachToID);
				var searchCompanies = nlapiSearchRecord('contact', null, arrSearchFilters, arrSearchColumns);
				
				if(searchCompanies != null){
					confirmHTML = 'This contact is already attached to this company.';
					confirmColor = 'darkred';
					confirmIcon = 'cancel.png';
					confirmStyle = 'cancel';
				}
				else{
					if(role == '0'){
						nlapiAttachRecord('contact', attachID, 'customer', attachToID, null);
						confirmHTML = 'Your user has been attached successfully!';
						confirmIcon = 'ok.png';
						confirmStyle = 'ok';
					}
					else{
						nlapiAttachRecord('contact', attachID, 'customer', attachToID, {'role':role});
						confirmHTML = 'Your user has been attached successfully!';
						confirmIcon = 'ok.png';
						confirmStyle = 'ok';
					}
				}
				
				break;
				
			case 'detachContact': //------------------------------------------------detach contact
				
				var detachID = request.getParameter('id');
				var detachFromID = request.getParameter('company');
				
				if(contactID == detachID){
					confirmHTML = 'You can not detach yourself from a company.';
					confirmColor = 'darkred';
					confirmIcon = 'cancel.png';
					confirmStyle = 'cancel';
				}
				else{
					var arrContactDetails = nlapiLookupField('contact',detachID,['company']);
					if(detachFromID == arrContactDetails['company']){
						confirmHTML = 'You can not detach a contact from his main company. Consider removing the contact.';
						confirmColor = 'darkred';
						confirmIcon = 'cancel.png';
						confirmStyle = 'cancel';
					}
					else{
						nlapiDetachRecord('contact', detachID, 'customer', detachFromID, null);
						confirmHTML = 'Your user has been detached successfully!';
						confirmIcon = 'ok.png';
						confirmStyle = 'ok';
					}
				}
				
				break;
	
			case 'newService': //------------------------------------------------create new service
				
				var reqdate = request.getParameter('reqdate');
				var type = request.getParameter('type');
				var aendcust = request.getParameter('aendcust');
				var zendtype = request.getParameter('zendtype');
				var acfa = request.getParameter('acfa');
				var arpa = request.getParameter('arpa');
				var notes = request.getParameter('notes');
	
				var facilityID = request.getParameter('facility');
				var arrFacilityDetails = nlapiLookupField('customrecord_cologix_facility',facilityID,['custrecord_cologix_location_address1','custrecord_clgx_facility_location']);
	            var facility = arrFacilityDetails['custrecord_cologix_location_address1'];
	            var location = arrFacilityDetails['custrecord_clgx_facility_location'];
	            /*
				if (salesRepID != null){
					var arrEmployeeDetails = nlapiLookupField('employee',salesRepID,['supervisor','entityid']);
					var superviserID = arrEmployeeDetails['supervisor'];
					var superviserName = arrEmployeeDetails['entityid'];
				}
	            */
	            var strMemo = 'A request for a new cross connect was received from:\n\n' +
								'Contact Name : ' + custContactName + '\n\n' +
								'Requested Delivery Date : ' + reqdate + '\n\n' +
								'Data Center : ' + facility + '\n\n' +
								'Cross Connect Type : ' +  type + '\n\n' +
								'A Loc Name : ' +  aendcust + '\n\n' +
								'Z-end Connection Type : ' +  zendtype + '\n\n' +
								'CFA (Carrier demarc point) : ' +  acfa + '\n\n' +
								'Requested Port Assignment : ' +  arpa + '\n\n' +
								'Notes : ' + notes;
	            
				// create a new opportunity
				var record = nlapiCreateRecord('opportunity');
				record.setFieldValue('entity', companyID);
				record.setFieldValue('title', 'Portal cross connect request');
				record.setFieldText('leadsource', 'Portal');
				record.setFieldValue('memo', strMemo);
				var idRec = nlapiSubmitRecord(record, true,true);
				
				var arrOpptyDetails = nlapiLookupField('opportunity',idRec,['tranid']);
	            var opptyNbr = arrOpptyDetails['tranid'];
							
	            var emailSubject = 'New Cross Connect Request';
	            var emailSubjectRep = 'New Cross Connect Request for ' + custCompName + ' (Asked by ' + custContactName + ')';
	            var emailBody = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
							'Your request for a new cross connect was received with the following info:\n\n' +
							'Reference Number : ' + opptyNbr + '\n' +
							'Requested Delivery Date : ' + reqdate + '\n' +
							'Data Center : ' + facility + '\n' +
							'Cross Connect Type : ' +  type + '\n' +
							'A Loc Name : ' +  aendcust + '\n' +
							'Z-end Connection Type : ' +  zendtype + '\n' +
							'CFA (Carrier demarc point) : ' +  acfa + '\n' +
							'Requested Port Assignment : ' +  arpa + '\n' +
							'Notes : ' + notes + '\n\n' +
							'This Service Order shall be effective as of the date emailed by Customer to Cologix and incorporates and is governed by the terms and conditions of the MSA and Colocation Services Schedule entered into by the parties.  If Customer has not entered into an MSA and/or Colocation Services Schedule with Cologix, then, as applicable, Cologix Standard MSA Terms and Conditions and Standard Colocation Services Schedule Terms and Conditions, each as available at cologix.com, shall be incorporated herein and shall govern this Service Order.\n\n' +
							'The initial term of this Serivce Order shall be one (1) month and shall automatically renew for successive one (1) month terms, unless terminated by either party, in writing, at least one (1) month prior to the end of the then current term.\n\n' +
							'Thank you for your business.  I will be following up with you shortly to finalize the details of this request.\n\n' +
							'Sincerely,\n' +
							salesRepName + '\n\n';
	
	            if (salesRepID != null && salesRepID != ''){
					nlapiSendEmail(salesRepID,salesRepID,emailSubjectRep,emailBody,null,null,null,null);
					nlapiSendEmail(salesRepID,contactID,emailSubject,emailBody,null,null,null,null);
					//if (superviserID != null){
					//	nlapiSendEmail(salesRepID,superviserID,emailSubjectRep,emailBody,null,null,null,null);
					//}
	            }
				confirmHTML = 'Your request for a new cross connect has been received successfully!';
				confirmIcon = 'ok.png';
				confirmStyle = 'ok';
				
				break;
				
			case 'addService': //------------------------------------------------create new service
				
				var arrCustomerDetails = nlapiLookupField('customer',companyID,['salesrep']);
				var salesRepID = arrCustomerDetails['salesrep'];
	
				var facilityID = request.getParameter('facility');
				var arrFacilityDetails = nlapiLookupField('customrecord_cologix_facility',facilityID,['custrecord_cologix_location_address1']);
	            var facility = arrFacilityDetails['custrecord_cologix_location_address1'];
				var reqdate = request.getParameter('reqdate');
				var notes = request.getParameter('notes');
	
				var arrCustomerDetails = nlapiLookupField('customer',companyID,['companyname']);
	            var custCompName = arrCustomerDetails['companyname'];
				var arrContactDetails = nlapiLookupField('contact',contactID,['entityid']);
	            var custContactName = arrContactDetails['entityid'];
	            
	            var strMemo = 'A request for additional service(s) was received from:\n\n' +
				'Contact Name : ' + custContactName + '\n\n' +
				'Request Date : ' + reqdate + '\n\n' +
				'Data Center : ' + facility + '\n\n' +
				'Notes : ' + notes;
	            
				// create a new opportunity
				var record = nlapiCreateRecord('opportunity');
				record.setFieldValue('entity', companyID);
				record.setFieldValue('title', 'Portal service request');
				record.setFieldValue('memo', strMemo);
				var idRec = nlapiSubmitRecord(record, true,true);
				
				var arrOpptyDetails = nlapiLookupField('opportunity',idRec,['tranid']);
	            var opptyNbr = arrOpptyDetails['tranid'];
	            
	            var emailSubject = 'Additional Services Request';
	            var emailSubjectRep = 'Additional Services Request for ' + custCompName + ' (Asked by ' + custContactName + ')';
	            var emailBody = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
							'Your request for additional services  was received with the following info:\n\n' +
							'Reference Nbr. : ' + opptyNbr + '\n' +
							'Request Date : ' + reqdate + '\n' +
							'Data Center : ' + facility + '\n' +
							'Notes : ' + notes + '\n\n' +
							'Thank you for your business.  I will be following up with you shortly to finalize the details of this request.\n\n' +
							'Sincerely,\n' +
							salesRepName + '\n\n';
	
				if (salesRepID != null && salesRepID != ''){
					nlapiSendEmail(salesRepID,salesRepID,emailSubjectRep,emailBody,null,null,null,null);
					nlapiSendEmail(salesRepID,contactID,emailSubject,emailBody,null,null,null,null);
					//if (superviserID != null){
					//	nlapiSendEmail(salesRepID,superviserID,emailSubjectRep,emailBody,null,null,null,null);
					//}
				}
		
				confirmHTML = 'Your request for a new service has been received successfully!';
				confirmIcon = 'ok.png';
				confirmStyle = 'ok';
				
				break;
				
			case 'discoService': //------------------------------------------------disconect service
				
				var facility = request.getParameter('facility');
				var reqdate = request.getParameter('reqdate');
				var order = request.getParameter('order');
				var circuit = request.getParameter('circuit');
				var invoice = request.getParameter('invoice');
				var reference = request.getParameter('reference');
				var description = request.getParameter('description');
				var notes = request.getParameter('notes');
				
				if(order == '' && circuit == '' && invoice == '' && reference == '' && description == ''){
					confirmHTML = 'Unsuccessful request! Please provide at least one of indicated fields.';
					confirmColor = 'darkred';
					confirmIcon = 'cancel.png';
					confirmStyle = 'cancel';
				}
				else{
	
					var arrCustomerDetails = nlapiLookupField('customer',companyID,['salesrep']);
					var salesRepID = arrCustomerDetails['salesrep'];
					
					var arrCustomerDetails = nlapiLookupField('customer',companyID,['companyname']);
		            var custCompName = arrCustomerDetails['companyname'];
					var arrContactDetails = nlapiLookupField('contact',contactID,['entityid']);
		            var custContactName = arrContactDetails['entityid'];
		            
					emailSubject = 'Disconnect Service Request';
					emailSubjectRep = 'Disconnect Service Request for ' + custCompName + ' (Asked by ' + custContactName + ')';
					emailBody = 'Hello ' + custContactName + ' (' + custCompName + '),\n\n' +
								'Your disconnect service request was received with the following info:\n\n' +
								'Request Date : ' + reqdate + '\n' +
								'Data Center : ' + facility + '\n' +
								'Order Number : ' + order + '\n' +
								'Circuit ID : ' + circuit + '\n' +
								'Invoice Number : ' + invoice + '\n' +
								'Reference# : ' + reference + '\n' +
								'Description : ' + description + '\n' +
								'Notes : ' + notes + '\n\n' +
								'This is a disconnect request and is subject to your in force contractual terms and conditions. You will receive further communication from a Cologix Billing representative who will follow up with you shortly to confirm the details of this request.\n\n' +
								'Thank you for your business.\n\n' +
								'Sincerely,\n' +
								salesRepName + '\n\n';
		
					if (salesRepID != null && salesRepID != ''){
						nlapiSendEmail(salesRepID,salesRepID,emailSubjectRep,emailBody,null,null,null,null);
						nlapiSendEmail(salesRepID,contactID,emailSubject,emailBody,null,null,null,null);
						
						clgx_send_employee_emails_from_savedsearch("customsearch_clgx_portal_en_stage", emailSubject, emailBody);
						//nlapiSendEmail(salesRepID,12073,emailSubject,emailBody,null,null,null,null); //  Kristen Schultz
						//if (superviserID != null){
						//	nlapiSendEmail(salesRepID,superviserID,emailSubjectRep,emailBody,null,null,null,null);
						//}
					}
					
					confirmHTML = 'Your request has been received successfully!';
					confirmIcon = 'ok.png';
					confirmStyle = 'ok';
				}
				break;
				
			case 'company': // if swiching company to edit
				
				confirmHTML = 'You are managing the "' + custCompName + '" company now.';
				confirmIcon = 'ok.png';
				confirmStyle = 'ok';
				
				break;
				
				
			case 'logout': // if logout send to login form
				nlapiSetRedirectURL('EXTERNAL', loginURL);
				break;
				
			default:
				// do nothing
		}
			// increase session's life
			var date = new Date(); 
			nlapiSubmitField('customrecord_clgx_portal_sessions_manage', sidID, ['custrecord_clgx_portal_session_last_date','custrecord_clgx_portal_session_last_time'], [nlapiDateToString(date,'date'),nlapiDateToString(date,'timeofday')]);
			
			// --------------- Configure portal rights -----------------------
			
			var rightsHTML='';
			if(sessionRights.charAt(0) == '0'){
				rightsHTML += '$("#tt").tabs("close", "Cases");';
			}
			if(sessionRights.charAt(2) == '0'){
				rightsHTML += '$("#tt").tabs("close", "Invoices");';
			}	
			if(sessionRights.charAt(4) == '0'){
				rightsHTML += '$("#tt").tabs("close", "Admin");';
			}
			if(sessionRights.charAt(6) == '0'){
				rightsHTML += '$("#tt").tabs("close", "Services");';
			}
			if(sessionRights.charAt(8) == '0'){
				rightsHTML += '$("#aCases").accordion("remove", "New Case");';
			}
			
			// display portal page after actions
			var objFilePortal = nlapiLoadFile(templatePortal); 
	    	var stMainHTML = objFilePortal.getValue();
	    	
			if(sessionRights.charAt(0) == '1'){
				stMainHTML = stMainHTML.replace(new RegExp('{stOpenCases}','g'),openCasesData(companyID,caseDetailsURL));
				stMainHTML = stMainHTML.replace(new RegExp('{stClosedCases}','g'),closedCasesData(companyID,caseDetailsURL));
			}
			else{
				stMainHTML = stMainHTML.replace(new RegExp('{stOpenCases}','g'),'var openCasesData = [];');
				stMainHTML = stMainHTML.replace(new RegExp('{stClosedCases}','g'),'var closedCasesData = [];');
			}
			if(sessionRights.charAt(2) == '1'){
				stMainHTML = stMainHTML.replace(new RegExp('{stInvoices}','g'),invoicesData(companyID));
			}	
			else{
				stMainHTML = stMainHTML.replace(new RegExp('{stInvoices}','g'),'var invoicesData = [];');
			}
			if(sessionRights.charAt(4) == '1'){
				stMainHTML = stMainHTML.replace(new RegExp('{stContacts}','g'),contactsData(companyID,sid));
				stMainHTML = stMainHTML.replace(new RegExp('{stAtachments}','g'),atachmentsData(companyID,sid));
			}
			else{
				stMainHTML = stMainHTML.replace(new RegExp('{stContacts}','g'),'var contactsData = [];');
				stMainHTML = stMainHTML.replace(new RegExp('{stAtachments}','g'),'var atachmentsData = [];');
			}
	    	
			var casesHTML = '';
			if(sessionRights.charAt(8) == '1'){
				casesHTML += '<div id="openCasesToolbar">';
				//casesHTML += '<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newCase()">New</a>';
				casesHTML += '<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editCase()">Edit</a>';
				//casesHTML += '<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="closeCase()">Close</a>';
				casesHTML += '</div>';
			}
			stMainHTML = stMainHTML.replace(new RegExp('{casesHTML}','g'),casesHTML);
			
			var invoicesHTML ='';
			if(sessionRights.charAt(10) == '1'){
				invoicesHTML = '<td align="left">| <a href="#" onclick="getSelections()">Email me all selected invoices.</a> |</td>';
			}
			stMainHTML = stMainHTML.replace(new RegExp('{invoicesHTML}','g'),invoicesHTML);
			
			var contactsHTML ='';
			if(sessionRights.charAt(12) == '1'){
				contactsHTML += '<div id="contactsToolbar">';
				contactsHTML += '<a href="#" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="newUser()">New</a>';
				contactsHTML += '<a href="#" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editUser()">Edit</a>';
				contactsHTML += '<a href="#" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="removeUser()">Remove</a>';
				if(nbrCompanies > 1){
					contactsHTML += '<a href="#" class="easyui-linkbutton" iconCls="icon-redo" plain="true" onclick="attachUser()">Attach</a>';
				}
				contactsHTML += '</div>';
			}
			var atachmentsHTML ='';
			if(sessionRights.charAt(12) == '1' && nbrCompanies > 1){
				atachmentsHTML += '<div id="atachmentsToolbar">';
				atachmentsHTML += '<a href="#" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="detachUser()">Detach</a>';
				atachmentsHTML += '</div>';
			}
			
			confirmHTML = '<td align="center"><img src=//www.cologix.com/portal/jquery-easyui-1.3.1/themes/icons/' + confirmIcon + ' alt=Confirmation height=12 width=12 /></td><td class="' + confirmStyle + '" align="left">&nbsp;' + confirmHTML + '&nbsp;</td>';
	
			stMainHTML = stMainHTML.replace(new RegExp('{contactsHTML}','g'),contactsHTML);
			stMainHTML = stMainHTML.replace(new RegExp('{atachmentsHTML}','g'),atachmentsHTML);
			stMainHTML = stMainHTML.replace(new RegExp('{portalURL}','g'),portalURL);
		    stMainHTML = stMainHTML.replace(new RegExp('{sid}','g'),sid);
		    stMainHTML = stMainHTML.replace(new RegExp('{tab}','g'),tab);
		    stMainHTML = stMainHTML.replace(new RegExp('{rights}','g'),rightsHTML);
		    stMainHTML = stMainHTML.replace(new RegExp('{discoDate}','g'),getDiscoDate());
		    stMainHTML = stMainHTML.replace(new RegExp('{custContactName}','g'),custContactName);
		    stMainHTML = stMainHTML.replace(new RegExp('{confirmHTML}','g'),confirmHTML);
		    stMainHTML = stMainHTML.replace(new RegExp('{parentHTML}','g'),parentHTML);
		    stMainHTML = stMainHTML.replace(new RegExp('{selectCompanies}','g'),selectCompanies);
		    stMainHTML = stMainHTML.replace(new RegExp('{selectContactsHTML}','g'),selectContactsHTML);
		    
		    
		    //stMainHTML = stMainHTML.replace(new RegExp('{remUsage}','g'),remUsage);
			response.write( stMainHTML );
		}
	}
}
catch (error){
	/*
	// if error terminate session and send to login
	var passwordHTML = '';
	alertMessage = 'An error has occured. Please contact support.';
	var objFile = nlapiLoadFile(templateLogin);
	var stMainHTML = objFile.getValue();
	stMainHTML = stMainHTML.replace(new RegExp('{alertMessage}','g'),alertMessage);
	stMainHTML = stMainHTML.replace(new RegExp('{passwordHTML}','g'),passwordHTML);
	stMainHTML = stMainHTML.replace(new RegExp('{loginURL}','g'),loginURL);
	response.write( stMainHTML );
	*/
	
	
    if (error.getDetails != undefined){
        nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
        throw error;
    }
    else{
        nlapiLogExecution('ERROR','Unexpected Error', error.toString());
        throw nlapiCreateError('99999', error.toString());
    }
	
}
}

function openCasesData(custID, caseDetails){	
	
	sid = request.getParameter('sid');
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
	arrColumns[1] = new nlobjSearchColumn('casenumber', null, null);
	arrColumns[2] = new nlobjSearchColumn('startdate', null, null);
	arrColumns[3] = new nlobjSearchColumn('title', null, null);
	arrColumns[4] = new nlobjSearchColumn('assigned', null, null);
	arrColumns[5] = new nlobjSearchColumn('category', null, null);
	arrColumns[6] = new nlobjSearchColumn('custevent_cologix_sub_case_type', null, null);
	arrColumns[7] = new nlobjSearchColumn('status', null, null);
	arrColumns[8] = new nlobjSearchColumn('priority', null, null);
	arrColumns[9] = new nlobjSearchColumn('enddate', null, null);
	arrColumns[10] = new nlobjSearchColumn('custevent_cologix_case_sched_followup', null, null);
	arrColumns[11] = new nlobjSearchColumn('custevent_cologix_sched_start_time', null, null);
	arrColumns[12] = new nlobjSearchColumn('contact', null, null);
	arrFilters[0] = new nlobjSearchFilter('internalid','customer','is',custID);
	arrFilters[1] = new nlobjSearchFilter('status',null,'noneof','5');
	
	var searchCases = nlapiSearchRecord('supportcase', null, arrFilters, arrColumns);
	
	var stCases = 'var openCasesData = [';
	for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
		var searchCase = searchCases[i];
		stCases += '{id:"' + searchCase.getValue('internalid') + 
				'",case:"' + searchCase.getValue('casenumber') +
				'",details:"' + '<a href=' + caseDetails + '&sid=' + sid + '&id=' + searchCase.getValue('internalid') + ' target=_blank><img src=//www.cologix.com/portal/jquery-easyui-1.2.6/themes/icons/search.png alt=Details height=16 width=16 /></a>' +
				'",casedate:"' + searchCase.getValue('startdate') + 
				'",schedate:"' + searchCase.getValue('custevent_cologix_case_sched_followup') + ' '  + searchCase.getValue('custevent_cologix_sched_start_time') +
				'",subject:"' + searchCase.getValue('title') + 
				'",contact:"' + searchCase.getText('contact') + 
				'",category:"' + searchCase.getText('category') + 
				'",priority:"' + searchCase.getText('priority') +
				'",status:"' + searchCase.getText('status') +  '"},';
	}
	
	var strLen = stCases.length;
	if (searchCases != null){
		stCases = stCases.slice(0,strLen-1);
	}
	stCases += '];';
    return stCases;
}

function closedCasesData(custID,caseDetails){	
	
	sid = request.getParameter('sid');
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
	arrColumns[1] = new nlobjSearchColumn('casenumber', null, null);
	arrColumns[2] = new nlobjSearchColumn('startdate', null, null);
	arrColumns[3] = new nlobjSearchColumn('title', null, null);
	arrColumns[4] = new nlobjSearchColumn('assigned', null, null);
	arrColumns[5] = new nlobjSearchColumn('category', null, null);
	arrColumns[6] = new nlobjSearchColumn('custevent_cologix_sub_case_type', null, null);
	arrColumns[7] = new nlobjSearchColumn('status', null, null);
	arrColumns[8] = new nlobjSearchColumn('priority', null, null);
	arrColumns[9] = new nlobjSearchColumn('enddate', null, null);
	arrColumns[10] = new nlobjSearchColumn('custevent_cologix_case_sched_followup', null, null);
	arrColumns[11] = new nlobjSearchColumn('custevent_cologix_sched_start_time', null, null);
	arrColumns[12] = new nlobjSearchColumn('contact', null, null);
	arrFilters[0] = new nlobjSearchFilter('internalid','customer','is',custID);
	arrFilters[1] = new nlobjSearchFilter('status',null,'anyof','5');
	var searchCases = nlapiSearchRecord('supportcase', null, arrFilters, arrColumns);
	
	var stCases = 'var closedCasesData = [';
	for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
		var searchCase = searchCases[i];
		
		stCases += '{id:"' + searchCase.getValue('internalid') + 
				'",case:"' + searchCase.getValue('casenumber') +	
				'",details:"' + '<a href=' + caseDetails + '&sid=' + sid + '&id=' + searchCase.getValue('internalid') + ' target=_blank><img src=//www.cologix.com/portal/jquery-easyui-1.2.6/themes/icons/search.png alt=Details height=16 width=16 /></a>' +
				'",casedate:"' + searchCase.getValue('startdate') + 
				'",schedate:"' + searchCase.getValue('custevent_cologix_case_sched_followup') + ' '  + searchCase.getValue('custevent_cologix_sched_start_time') +
				'",subject:"' + searchCase.getValue('title') + 
				'",contact:"' + searchCase.getText('contact') + 
				'",category:"' + searchCase.getText('category') + 
				'",priority:"' + searchCase.getText('priority') +
				'",status:"' + searchCase.getText('status') +  '"},';
	}
	var strLen = stCases.length;
	if (searchCases != null){
		stCases = stCases.slice(0,strLen-1);
	}
	stCases += '];';
    return stCases;
}

function invoicesData(custID){	
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
	arrColumns[1] = new nlobjSearchColumn('custrecord_clgx_consol_inv_pdf_file_id', null, null);
	arrColumns[2] = new nlobjSearchColumn('custrecord_clgx_consol_inv_batch', null, null);
	arrColumns[3] = new nlobjSearchColumn('custrecord_clgx_consol_inv_year_display', null, null);
	arrColumns[4] = new nlobjSearchColumn('custrecord_clgx_consol_inv_month_display', null, null);
	arrColumns[5] = new nlobjSearchColumn('custrecord_clgx_consol_inv_date', null, null);
	arrColumns[6] = new nlobjSearchColumn('custrecord_clgx_consol_inv_total', null, null);
	arrFilters[0] = new nlobjSearchFilter('custrecord_clgx_consol_inv_customer',null,'is',custID);
	arrFilters[1] = new nlobjSearchFilter('custrecord_clgx_consol_inv_output',null,'is',1);
	arrFilters[2] = new nlobjSearchFilter('custrecord_clgx_consol_inv_processed',null,'is','T');
	var searchInvoices = nlapiSearchRecord('customrecord_clgx_consolidated_invoices', null, arrFilters, arrColumns);
	
	var stInvoices = 'var invoicesData = [';
	for ( var i = 0; searchInvoices != null && i < searchInvoices.length; i++ ) {
		var searchInvoice = searchInvoices[i];
		
		stInvoices += '{id:"' + searchInvoice.getValue('internalid') + 
				'",nbr:"' + searchInvoice.getValue('custrecord_clgx_consol_inv_batch') + 
				'",icon:"' + '<img src=//www.cologix.com/portal/jquery-easyui-1.2.6/themes/icons/pdf.png alt=Details height=16 width=16 />' +
				'",year:"' + searchInvoice.getValue('custrecord_clgx_consol_inv_year_display') + 
				'",month:"' + searchInvoice.getText('custrecord_clgx_consol_inv_month_display') + 
				'",date:"' + searchInvoice.getValue('custrecord_clgx_consol_inv_date') + 
				'",total:"' + searchInvoice.getValue('custrecord_clgx_consol_inv_total') +  '"},';
	}
	var strLen = stInvoices.length;
	if (searchInvoices != null){
		stInvoices = stInvoices.slice(0,strLen-1);
	}
	stInvoices += '];';
	
    return stInvoices;
}

function contactsData(custID,session){	

	var arrColumns = new Array();
	var arrFilters = new Array();
	arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
	arrColumns[1] = new nlobjSearchColumn('entityid', null, null);
	arrColumns[2] = new nlobjSearchColumn('phone', null, null);
	arrColumns[3] = new nlobjSearchColumn('email', null, null);
	arrColumns[4] = new nlobjSearchColumn('title', null, null);
	arrColumns[5] = new nlobjSearchColumn('contactrole', null, null);
	arrColumns[6] = new nlobjSearchColumn('custentity_cologix_access_crd_num', null, null);
	arrColumns[7] = new nlobjSearchColumn('custentity_cologix_co_security_id', null, null);
	arrColumns[8] = new nlobjSearchColumn('custentity_clgx_access_sig', null, null);
	arrColumns[9] = new nlobjSearchColumn('custentity_cologix_data_cntr_accs', null, null);
	arrColumns[10] = new nlobjSearchColumn('custentity_clgx_portal_rights', null, null);
	arrColumns[11] = new nlobjSearchColumn('internalid', 'customer', null);
	arrColumns[12] = new nlobjSearchColumn('company', null, null);
	arrColumns[13] = new nlobjSearchColumn('supervisor', null, null);
	arrColumns[14] = new nlobjSearchColumn('custentity_clgx_contact_secondary_role', null, null);
	arrFilters[0] = new nlobjSearchFilter('internalid','customer','anyof',custID);
	arrFilters[1] = new nlobjSearchFilter('company',null,'anyof',custID);
	arrFilters[2] = new nlobjSearchFilter('isinactive',null,'is','F');
	var searchContacts = nlapiSearchRecord('contact', null, arrFilters, arrColumns);
	
	var stContacts = 'var contactsData = [';
	for ( var i = 0; searchContacts != null && i < searchContacts.length; i++ ) {
		var searchContact = searchContacts[i];
		
		var cardCheck = 'off';
		var securityIDCheck = 'off';
		var accessCheck = 'off';
		var centerCheck = 'off';
		if(searchContact.getValue('custentity_cologix_access_crd_num') != ''){cardCheck = 'on';}
		if(searchContact.getValue('custentity_cologix_co_security_id') != ''){securityIDCheck = 'on';}
		if(searchContact.getValue('custentity_clgx_access_sig') == 'T'){accessCheck = 'on';}
		if(searchContact.getValue('custentity_cologix_data_cntr_accs') == 'T'){centerCheck = 'on';}
		
		var casesRights = 'off';
		var invoicesRights = 'off';
		var contactsRights = 'off';
		var serviceRights = 'off';
		var cases_editRights = 'off';
		var invoices_editRights = 'off';
		var contacts_editRights = 'off';
		if(searchContact.getValue('custentity_clgx_portal_rights').charAt(0) == '1'){casesRights = 'on';}
		if(searchContact.getValue('custentity_clgx_portal_rights').charAt(2) == '1'){invoicesRights = 'on';}
		if(searchContact.getValue('custentity_clgx_portal_rights').charAt(4) == '1'){contactsRights = 'on';}
		if(searchContact.getValue('custentity_clgx_portal_rights').charAt(6) == '1'){serviceRights = 'on';}
		if(searchContact.getValue('custentity_clgx_portal_rights').charAt(8) == '1'){cases_editRights = 'on';}
		if(searchContact.getValue('custentity_clgx_portal_rights').charAt(10) == '1'){invoices_editRights = 'on';}
		if(searchContact.getValue('custentity_clgx_portal_rights').charAt(12) == '1'){contacts_editRights = 'on';}
		
		if(contacts_editRights == 'on'){
			var stAdmin = '<img src=//www.cologix.com/portal/jquery-easyui-1.2.6/themes/icons/ok.png alt=Admin height=16 width=16 />';
		}
		else{
			var stAdmin = '<img src=//www.cologix.com/portal/jquery-easyui-1.2.6/themes/icons/blank.gif alt=Admin height=16 width=16 />';
		}
		
		if(searchContact.getValue('internalid', 'customer') != searchContact.getValue('company')){
			var stAttached = '<img src=//www.cologix.com/portal/jquery-easyui-1.2.6/themes/icons/redo.png alt=Attached height=16 width=16 />';
		}
		else{
			var stAttached = '<img src=//www.cologix.com/portal/jquery-easyui-1.2.6/themes/icons/blank.gif alt=Attached height=16 width=16 />';
		}
		
		stContacts += '{id:"' + searchContact.getValue('internalid') + 
				'",sid:"' + session +		
				'",admin:"' + stAdmin +
				'",attached:"' + stAttached +
				'",name:"' + searchContact.getValue('entityid') +
				'",roleName:"' + searchContact.getText('contactrole') + 
				'",role2Name:"' + searchContact.getText('custentity_clgx_contact_secondary_role') + 
				'",role:"' + searchContact.getValue('contactrole') + 
				'",role2:"' + searchContact.getValue('custentity_clgx_contact_secondary_role') + 
				'",supervisor:"' + searchContact.getValue('supervisor') + 
				'",email:"' + searchContact.getValue('email') + 
				'",phone:"' + searchContact.getValue('phone') + 
				'",jobtitle:"' + searchContact.getValue('title') + 
				'",card:"' + cardCheck +
				'",securityID:"' + securityIDCheck +
				'",access:"' + accessCheck +
				'",center:"' + centerCheck +
				'",cases:"' + casesRights +
				'",invoices:"' + invoicesRights +
				'",contacts:"' + contactsRights +
				'",service:"' + serviceRights +
				'",cases_edit:"' + cases_editRights +
				'",invoices_edit:"' + invoices_editRights +
				'",contacts_edit:"' +  contacts_editRights + '"},';
	}
	var strLen = stContacts.length;
	if (searchContacts != null){
		stContacts = stContacts.slice(0,strLen-1);
	}
	stContacts += '];';
	
    return stContacts;
}

function atachmentsData(custID,session){	

	var arrColumns = new Array();
	var arrFilters = new Array();
	arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
	arrColumns[1] = new nlobjSearchColumn('entityid', null, null);
	arrColumns[2] = new nlobjSearchColumn('phone', null, null);
	arrColumns[3] = new nlobjSearchColumn('email', null, null);
	arrColumns[4] = new nlobjSearchColumn('title', null, null);
	arrColumns[5] = new nlobjSearchColumn('contactrole', null, null);
	arrColumns[6] = new nlobjSearchColumn('custentity_cologix_access_crd_num', null, null);
	arrColumns[7] = new nlobjSearchColumn('custentity_cologix_co_security_id', null, null);
	arrColumns[8] = new nlobjSearchColumn('custentity_clgx_access_sig', null, null);
	arrColumns[9] = new nlobjSearchColumn('custentity_cologix_data_cntr_accs', null, null);
	arrColumns[10] = new nlobjSearchColumn('custentity_clgx_portal_rights', null, null);
	arrColumns[11] = new nlobjSearchColumn('internalid', 'customer', null);
	arrColumns[12] = new nlobjSearchColumn('company', null, null);
	arrColumns[13] = new nlobjSearchColumn('supervisor', null, null);
	arrColumns[14] = new nlobjSearchColumn('custentity_clgx_contact_secondary_role', null, null);
	arrFilters[0] = new nlobjSearchFilter('internalid','customer','anyof',custID);
	arrFilters[1] = new nlobjSearchFilter('company',null,'noneof',custID);
	arrFilters[2] = new nlobjSearchFilter('isinactive',null,'is','F');
	var searchContacts = nlapiSearchRecord('contact', null, arrFilters, arrColumns);
	
	var stContacts = 'var atachmentsData = [';
	for ( var i = 0; searchContacts != null && i < searchContacts.length; i++ ) {
		var searchContact = searchContacts[i];
		
		var cardCheck = 'off';
		var securityIDCheck = 'off';
		var accessCheck = 'off';
		var centerCheck = 'off';
		if(searchContact.getValue('custentity_cologix_access_crd_num') != ''){cardCheck = 'on';}
		if(searchContact.getValue('custentity_cologix_co_security_id') != ''){securityIDCheck = 'on';}
		if(searchContact.getValue('custentity_clgx_access_sig') == 'T'){accessCheck = 'on';}
		if(searchContact.getValue('custentity_cologix_data_cntr_accs') == 'T'){centerCheck = 'on';}
		
		var casesRights = 'off';
		var invoicesRights = 'off';
		var contactsRights = 'off';
		var serviceRights = 'off';
		var cases_editRights = 'off';
		var invoices_editRights = 'off';
		var contacts_editRights = 'off';
		if(searchContact.getValue('custentity_clgx_portal_rights').charAt(0) == '1'){casesRights = 'on';}
		if(searchContact.getValue('custentity_clgx_portal_rights').charAt(2) == '1'){invoicesRights = 'on';}
		if(searchContact.getValue('custentity_clgx_portal_rights').charAt(4) == '1'){contactsRights = 'on';}
		if(searchContact.getValue('custentity_clgx_portal_rights').charAt(6) == '1'){serviceRights = 'on';}
		if(searchContact.getValue('custentity_clgx_portal_rights').charAt(8) == '1'){cases_editRights = 'on';}
		if(searchContact.getValue('custentity_clgx_portal_rights').charAt(10) == '1'){invoices_editRights = 'on';}
		if(searchContact.getValue('custentity_clgx_portal_rights').charAt(12) == '1'){contacts_editRights = 'on';}
		
		if(contacts_editRights == 'on'){
			var stAdmin = '<img src=//www.cologix.com/portal/jquery-easyui-1.2.6/themes/icons/ok.png alt=Admin height=16 width=16 />';
		}
		else{
			var stAdmin = '<img src=//www.cologix.com/portal/jquery-easyui-1.2.6/themes/icons/blank.gif alt=Admin height=16 width=16 />';
		}
		
		if(searchContact.getValue('internalid', 'customer') != searchContact.getValue('company')){
			var stAttached = '<img src=//www.cologix.com/portal/jquery-easyui-1.2.6/themes/icons/redo.png alt=Attached height=16 width=16 />';
		}
		else{
			var stAttached = '<img src=//www.cologix.com/portal/jquery-easyui-1.2.6/themes/icons/blank.gif alt=Attached height=16 width=16 />';
		}
		
		stContacts += '{id:"' + searchContact.getValue('internalid') + 
				'",sid:"' + session +		
				'",admin:"' + stAdmin +
				'",attached:"' + stAttached +
				'",name:"' + searchContact.getValue('entityid') +
				'",roleName:"' + searchContact.getText('contactrole') + 
				'",role2Name:"' + searchContact.getText('custentity_clgx_contact_secondary_role') + 
				'",role:"' + searchContact.getValue('contactrole') + 
				'",role2:"' + searchContact.getValue('custentity_clgx_contact_secondary_role') + 
				'",supervisor:"' + searchContact.getValue('supervisor') + 
				'",email:"' + searchContact.getValue('email') + 
				'",phone:"' + searchContact.getValue('phone') + 
				'",jobtitle:"' + searchContact.getValue('title') + 
				'",card:"' + cardCheck +
				'",securityID:"' + securityIDCheck +
				'",access:"' + accessCheck +
				'",center:"' + centerCheck +
				'",cases:"' + casesRights +
				'",invoices:"' + invoicesRights +
				'",contacts:"' + contactsRights +
				'",service:"' + serviceRights +
				'",cases_edit:"' + cases_editRights +
				'",invoices_edit:"' + invoices_editRights +
				'",contacts_edit:"' +  contacts_editRights + '"},';
	}
	var strLen = stContacts.length;
	if (searchContacts != null){
		stContacts = stContacts.slice(0,strLen-1);
	}
	stContacts += '];';
	
    return stContacts;
}

function generateSID() {
	  var s = [], itoh = '0123456789ABCDEF';
	  for (var i = 0; i <36; i++) s[i] = Math.floor(Math.random()*0x10);
	  s[14] = 4;
	  s[19] = (s[19] & 0x3) | 0x8;
	  for (var i = 0; i <36; i++) s[i] = itoh[s[i]];
	  s[8] = s[13] = s[18] = s[23] = '-';
	  return s.join('');
}

function generatePassword(length, special) {
	  var iteration = 0;
	  var password = "";
	  var randomNumber;
	  if(special == undefined){
	      var special = false;
	  }
	  while(iteration < length){
	    randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
	    if(!special){
	      if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
	      if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
	      if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
	      if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
	    }
	    iteration++;
	    password += String.fromCharCode(randomNumber);
	  }
	  return password;
}

function addCommas(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function getDiscoDate(){
    var todaydate = new Date();
    var date = nlapiAddMonths(todaydate, 1);
    var month = parseInt(date.getMonth()) + 1;
    var day = date.getDate();
    var year = date.getFullYear();
    
    if(parseInt(month) < 10){
        month = '0' + month;
    }
    
    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = month + '/' + day + '/' + year;
    
    return formattedDate;
}
