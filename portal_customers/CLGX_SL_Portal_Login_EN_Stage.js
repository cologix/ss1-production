nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Portal_Login_EN.js
//	Script Name:	CLGX_SL_Portal_Login_EN
//	Script Id:		customscript_clgx_sl_portal_login_en
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		5/31/2012
//-------------------------------------------------------------------------------------------------

function suitelet_portal_login(request, response){
try{
	
	// staging/production switch ---------------------------------------------------------------
	var stageSwitch = 1;
	var baseurl = 'https://forms.na1.netsuite.com';
	if(stageSwitch == 1){ // staging
		var templateLogin = 114656;
		var templatePortal = 114658;
		var portalURL = baseurl + '/app/site/hosting/scriptlet.nl?script=127&deploy=1&compid=1337135&h=5a46cd55c1542a2a567a';
		var loginURL = baseurl + '/app/site/hosting/scriptlet.nl?script=126&deploy=1&compid=1337135&h=d577c0b8b1b0fb618761';
		var caseDetailsURL = baseurl + '/app/site/hosting/scriptlet.nl?script=125&deploy=1&compid=1337135&h=7a9734553c197d996735';
	}
	else{ // production
		var templateLogin = 114655;
		var templatePortal = 114657;
		var portalURL = baseurl + '/app/site/hosting/scriptlet.nl?script=107&deploy=1&compid=1337135&h=2cfbedb3f83533636ae1';
		var loginURL = baseurl + '/app/site/hosting/scriptlet.nl?script=108&deploy=1&compid=1337135&h=cc6e4cabefab4198bb56';
		var caseDetailsURL = baseurl + '/app/site/hosting/scriptlet.nl?script=118&deploy=1&compid=1337135&h=872feb3bed2f5fa95103';
	}
	
	// maintenance switch ---------------------------------------------------------------
	var offSwitch = 0;
	if(offSwitch == 1){
		var disableHTML = ' disabled="disabled"';
		var alertMessage = 'The portal is closed for maintenance. We are sorry for this inconvenience.';
	}
	else{
		var disableHTML = '';
		var alertMessage = '';
	}

	var customerID = '';
	var sid = '';
	var act = '';
	var userIP = '';
	var submit = '';
	
	var passwordHTML = '';
	
	sid = request.getParameter('sid');
	act = request.getParameter('act');
	userIP = request.getHeader('NS-Client-IP');
	//submit = request.getHeader('submit');
	
	if (sid == '' || sid == null){ // no session ID - first display of login page
		var objFile = nlapiLoadFile(templateLogin);
		var stLoginHTML = objFile.getValue();
		stLoginHTML = stLoginHTML.replace(new RegExp('{alertMessage}','g'),alertMessage);
		stLoginHTML = stLoginHTML.replace(new RegExp('{passwordHTML}','g'),passwordHTML);
		stLoginHTML = stLoginHTML.replace(new RegExp('{loginURL}','g'),loginURL);
		stLoginHTML = stLoginHTML.replace(new RegExp('{disableHTML}','g'),disableHTML);
		response.write( stLoginHTML );
	}
	else if (sid == 'requestnew' && act == 'login') { // login attempt
		contactEmail = request.getParameter('email');
		contactPass = request.getParameter('password');
		
		// verify only if the account and email exist and contact is active
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
		arrFilters[0] = new nlobjSearchFilter('email',null,'is',contactEmail);
		arrFilters[1] = new nlobjSearchFilter('isinactive',null,'is','F');
		var searchContactEmail = nlapiSearchRecord('contact', null, arrFilters, arrColumns);

		passwordHTML += '<td valign="top">';
		passwordHTML += '<form id="password" method="post" action="' + loginURL + '">';
		passwordHTML += '<input type="hidden" name="sid" value="requestnew"></input>';
		passwordHTML += '<input type="hidden" name="act" value="password"></input>';
		passwordHTML += '<div style="padding:5px;">';
		passwordHTML += 'Account # :</br>';
		passwordHTML += '<input class="easyui-validatebox" type="text" name="account" required="true">';
		passwordHTML += '</div>';
		passwordHTML += '<div style="padding:5px;">';
		passwordHTML += 'Email :</br>';
		passwordHTML += '<input class="easyui-validatebox" type="text" name="email" required="true" validType="email">';
		passwordHTML += '</div>';
		passwordHTML += '<div style="padding:5px;"><input type="submit" name="submit" value="Reset Password"></div>';
		passwordHTML += '<div style="padding:5px;color:darkred;">Did you forget your password? Please enter your account number and your email above to receive a new password.</div>';
		passwordHTML += '</form>';
		passwordHTML += '</td>';
		
		if (searchContactEmail == null || searchContactEmail.length > 1) { // wrong login or duplicate email, send login form back
			passwordHTML = '';
			alertMessage = 'Incorrect login! Please try again.';
			var objFile = nlapiLoadFile(templateLogin);
			var stLoginHTML = objFile.getValue();
			stLoginHTML = stLoginHTML.replace(new RegExp('{alertMessage}','g'),alertMessage);
			stLoginHTML = stLoginHTML.replace(new RegExp('{passwordHTML}','g'),passwordHTML);
			stLoginHTML = stLoginHTML.replace(new RegExp('{loginURL}','g'),loginURL);
			stLoginHTML = stLoginHTML.replace(new RegExp('{disableHTML}','g'),disableHTML);
			response.write( stLoginHTML );
		}
		
		else{
			//email exist, verify login
			var arrColumns = new Array();
			var arrFilters = new Array();
			arrColumns[0] = new nlobjSearchColumn('subsidiary', 'customer', null);
			arrColumns[1] = new nlobjSearchColumn('internalid', 'customer', null);
			arrColumns[2] = new nlobjSearchColumn('internalid', null, null);
			arrColumns[3] = new nlobjSearchColumn('custentity_clgx_portal_login_attempts', null, null);
			arrColumns[4] = new nlobjSearchColumn('custentity_clgx_portal_rights', null, null);
			arrColumns[5] = new nlobjSearchColumn('salesrep', 'customer', null);
			arrColumns[6] = new nlobjSearchColumn('custentity_clgx_portal_accepted_terms', null, null);
			arrFilters[0] = new nlobjSearchFilter('email',null,'is',contactEmail);
			arrFilters[1] = new nlobjSearchFilter('custentity_clgx_portal_password',null,'is',contactPass);
			arrFilters[2] = new nlobjSearchFilter('isinactive',null,'is','F');
			var searchContact = nlapiSearchRecord('contact', null, arrFilters, arrColumns);
			
			if (searchContact == null) { // wrong login, send login form back
				
				var arrColumns = new Array();
				var arrFilters = new Array();
				arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
				arrColumns[1] = new nlobjSearchColumn('custentity_clgx_portal_login_attempts', null, null);
				arrFilters[0] = new nlobjSearchFilter('email',null,'is',contactEmail);
				arrFilters[1] = new nlobjSearchFilter('isinactive',null,'is','F');
				var searchContact = nlapiSearchRecord('contact', null, arrFilters, arrColumns);

				var resultRow = searchContact[0];
				var columns = resultRow.getAllColumns();
				var contactID = resultRow.getValue(columns[0]);
				var logins = resultRow.getValue(columns[1]);
				
				if(parseInt(logins) < 6){
					nlapiSubmitField('contact', contactID, ['custentity_clgx_portal_login_attempts'], [parseInt(logins) + 1]);
					var loginsLeft = 5 - parseInt(logins);
					alertMessage = 'Incorrect login! Please try again. You have ' + loginsLeft + ' attempts left.';
				}
				else{
					alertMessage = 'You had 6 consecutive failed logins attempts. Your account is disabled. Please contact Cologix support.';
				}

				var objFile = nlapiLoadFile(templateLogin);
				var stLoginHTML = objFile.getValue();
				stLoginHTML = stLoginHTML.replace(new RegExp('{alertMessage}','g'),alertMessage);
				stLoginHTML = stLoginHTML.replace(new RegExp('{passwordHTML}','g'),passwordHTML);
				stLoginHTML = stLoginHTML.replace(new RegExp('{loginURL}','g'),loginURL);
				stLoginHTML = stLoginHTML.replace(new RegExp('{disableHTML}','g'),disableHTML);
				response.write( stLoginHTML );
			}	
			
			else{ // good login, create a new session
				
				var resultRow = searchContact[0];
				var columns = resultRow.getAllColumns();
				var subsidiaryID = resultRow.getValue(columns[0]);
				var customerID = resultRow.getValue(columns[1]);
				var contactID = resultRow.getValue(columns[2]);
				var logins = resultRow.getValue(columns[3]);
				var rights = resultRow.getValue(columns[4]);
				var salesRep = resultRow.getValue(columns[5]);
				var terms = resultRow.getValue(columns[6]);
				
				if(rights == '0,0,0,0,0,0,0'){ // this contact does not have access to portal
					
					alertMessage = 'You do not have access to the portal. Please contact your portal admin.';
					passwordHTML = '';
					var objFile = nlapiLoadFile(templateLogin);
					var stLoginHTML = objFile.getValue();
					stLoginHTML = stLoginHTML.replace(new RegExp('{alertMessage}','g'),alertMessage);
					stLoginHTML = stLoginHTML.replace(new RegExp('{passwordHTML}','g'),passwordHTML);
					stLoginHTML = stLoginHTML.replace(new RegExp('{loginURL}','g'),loginURL);
					stLoginHTML = stLoginHTML.replace(new RegExp('{disableHTML}','g'),disableHTML);
					response.write( stLoginHTML );
					
				}
				else{
					// search if the contact is attached to more than 1 customer
					var arrSearchColumns = new Array();
					var arrSearchFilters = new Array();
					arrSearchColumns[0] = new nlobjSearchColumn('internalid', 'customer', null);
					arrSearchFilters[0] = new nlobjSearchFilter('internalid',null,'is',contactID);
					var arrSearchCustomer = nlapiSearchRecord('contact', null, arrSearchFilters, arrSearchColumns);
					
					if (arrSearchCustomer.length > 1) {var parent = 'T';}
					else{var parent = 'F';}
					
					if(parseInt(logins) < 6){
						
						// there are less then 6 attempts to login - lower flag to 0
						nlapiSubmitField('contact', contactID, ['custentity_clgx_portal_login_attempts'], [0]);
						 // create a new session
						date = new Date(); 
						var sid = generateSID();
						//var tempCode = generatePassword(15);
						
						var record = nlapiCreateRecord('customrecord_clgx_portal_sessions_manage');
						record.setFieldValue('custrecord_clgx_portal_session_sid', sid);
						record.setFieldValue('custrecord_clgx_portal_session_user_ip', userIP);
						record.setFieldValue('custrecord_clgx_portal_session_last_date', nlapiDateToString(date,'date'));
						record.setFieldValue('custrecord_clgx_portal_session_last_time', nlapiDateToString(date,'timeofday'));
						record.setFieldValue('custrecord_clgx_portal_session_subsid', subsidiaryID);
						record.setFieldValue('custrecord_clgx_portal_session_customer', customerID);
						record.setFieldValue('custrecord_clgx_portal_session_sales_rep', salesRep);
						record.setFieldValue('custrecord_clgx_portal_session_contact', contactID);
						record.setFieldValue('custrecord_clgx_portal_session_company', customerID);
						record.setFieldValue('custrecord_clgx_portal_session_terms', terms);
						record.setFieldValue('custrecord_clgx_portal_session_is_parent', parent);
						record.setFieldValue('custrecord_clgx_portal_session_rights', rights);
						var idSession = nlapiSubmitRecord(record, true);
	
						// login is good, display portal
						nlapiSetRedirectURL('EXTERNAL', portalURL + '&sid=' + sid);
					}
					else{
						// too late, there are already 6 failed login attempts
						alertMessage = 'You had 6 consecutive failed logins attempts. Your account is disabled. Please contact Cologix support.';
						var objFile = nlapiLoadFile(templateLogin);
						var stLoginHTML = objFile.getValue();
						stLoginHTML = stLoginHTML.replace(new RegExp('{alertMessage}','g'),alertMessage);
						stLoginHTML = stLoginHTML.replace(new RegExp('{passwordHTML}','g'),passwordHTML);
						stLoginHTML = stLoginHTML.replace(new RegExp('{loginURL}','g'),loginURL);
						stLoginHTML = stLoginHTML.replace(new RegExp('{disableHTML}','g'),disableHTML);
						response.write( stLoginHTML );
					}
				}
			}
		}
	}
	else if (sid == 'requestnew' && act == 'password') { // recuperate password attempt

		contactAccount = request.getParameter('account');
		contactEmail = request.getParameter('email');
		
		// verify  if the account and email exist
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
		arrFilters[0] = new nlobjSearchFilter('internalid','customer','is',contactAccount);
		arrFilters[1] = new nlobjSearchFilter('email',null,'is',contactEmail);
		arrFilters[2] = new nlobjSearchFilter('isinactive',null,'is','F');
		var searchContactEmail = nlapiSearchRecord('contact', null, arrFilters, arrColumns);
		
	
		if (searchContactEmail == null) { // wrong email, send login form back
			
			passwordHTML += '<td valign="top">';
			passwordHTML += '<form id="password" method="post" action="' + loginURL + '">';
			passwordHTML += '<input type="hidden" name="sid" value="requestnew"></input>';
			passwordHTML += '<input type="hidden" name="act" value="password"></input>';
			passwordHTML += '<div style="padding:10px;">';
			passwordHTML += 'Account # :</br>';
			passwordHTML += '<input class="easyui-validatebox" type="text" name="account" required="true">';
			passwordHTML += '</div>';
			passwordHTML += '<div style="padding:10px;">';
			passwordHTML += 'Email :</br>';
			passwordHTML += '<input class="easyui-validatebox" type="text" name="email" required="true" validType="email"></input>';
			passwordHTML += '</div>';
			passwordHTML += '<div style="padding:10px;"><input type="submit" name="submit" value="Reset Password"></div>';
			passwordHTML += '<div style="padding:10px;color:darkred;">Incorrect input! Please try again.</div>';
			passwordHTML += '</form>';
			passwordHTML += '</td>';

			var objFile = nlapiLoadFile(templateLogin);
			var stLoginHTML = objFile.getValue();
			stLoginHTML = stLoginHTML.replace(new RegExp('{alertMessage}','g'),alertMessage);
			stLoginHTML = stLoginHTML.replace(new RegExp('{passwordHTML}','g'),passwordHTML);
			stLoginHTML = stLoginHTML.replace(new RegExp('{loginURL}','g'),loginURL);
			stLoginHTML = stLoginHTML.replace(new RegExp('{disableHTML}','g'),disableHTML);
			response.write( stLoginHTML );
		}
		else{
			
			var resultRow = searchContactEmail[0];
			var columns = resultRow.getAllColumns();
			var contactID = resultRow.getValue(columns[0]);
			
			var objContact = nlapiLoadRecord('contact', contactID);
			var newPassword = generatePassword(8,false);
			objContact.setFieldValue('custentity_clgx_portal_password', newPassword);
			nlapiSubmitRecord(objContact, true, true);
			
			emailSubject = 'Cologix Customer Portal - Password Reset';
			emailSubjectConfirm = 'Cologix Customer Portal - Password Reset';
			emailBody = 'Dear Customer,\n\n' +
						'Here is your new password:\n\n' + newPassword + '\n\n' +
						'Thank you for your business.\n\n' +
						'Sincerely,\n' +
						'Cologix\n\n';
			nlapiSendEmail(2406,contactID,emailSubject,emailBody,null,null,null,null);
			
			passwordHTML += '<td valign="top">';
			passwordHTML += '<form id="password" method="post" action="' + loginURL + '">';
			passwordHTML += '<input type="hidden" name="sid" value="requestnew"></input>';
			passwordHTML += '<input type="hidden" name="act" value="password"></input>';
			passwordHTML += '<div style="padding:10px;">';
			passwordHTML += 'Account # :</br>';
			passwordHTML += '<input class="easyui-validatebox" type="text" name="account" required="true">';
			passwordHTML += '</div>';
			passwordHTML += '<div style="padding:10px;">';
			passwordHTML += 'Email :</br>';
			passwordHTML += '<input class="easyui-validatebox" type="text" name="email" required="true" validType="email">';
			passwordHTML += '</div>';
			passwordHTML += '<div style="padding:10px;"><input type="submit" name="submit" value="Reset Password"></div>';
			passwordHTML += '<div style="padding:10px;color:darkred;">A new password was sent to the email you provided.</div>';
			passwordHTML += '</form>';
			passwordHTML += '</td>';
			
			var objFile = nlapiLoadFile(templateLogin);
			var stLoginHTML = objFile.getValue();
			stLoginHTML = stLoginHTML.replace(new RegExp('{alertMessage}','g'),alertMessage);
			stLoginHTML = stLoginHTML.replace(new RegExp('{passwordHTML}','g'),passwordHTML);
			stLoginHTML = stLoginHTML.replace(new RegExp('{loginURL}','g'),loginURL);
			stLoginHTML = stLoginHTML.replace(new RegExp('{disableHTML}','g'),disableHTML);
			response.write( stLoginHTML );
		}
	}
	else{}
	
}
catch (error){
	/*
	// if error terminate session and send to login
	alertMessage = 'An error has occured. Please contact support.';
	var objFile = nlapiLoadFile(templateLogin);
	var stMainHTML = objFile.getValue();
	stMainHTML = stMainHTML.replace(new RegExp('{alertMessage}','g'),alertMessage);
	stMainHTML = stMainHTML.replace(new RegExp('{passwordHTML}','g'),passwordHTML);
	stLoginHTML = stLoginHTML.replace(new RegExp('{loginURL}','g'),loginURL);
	response.write( stMainHTML );
	*/
    if (error.getDetails != undefined){
        nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
        throw error;
    }
    else{
        nlapiLogExecution('ERROR','Unexpected Error', error.toString());
        throw nlapiCreateError('99999', error.toString());
    }
}
}

function generateSID() {
	  var s = [], itoh = '0123456789ABCDEF';
	  for (var i = 0; i <36; i++) s[i] = Math.floor(Math.random()*0x10);
	  s[14] = 4;
	  s[19] = (s[19] & 0x3) | 0x8;
	  for (var i = 0; i <36; i++) s[i] = itoh[s[i]];
	  s[8] = s[13] = s[18] = s[23] = '-';
	  return s.join('');
}

function generatePassword(length, special) {
	  var iteration = 0;
	  var password = "";
	  var randomNumber;
	  if(special == undefined){
	      var special = false;
	  }
	  while(iteration < length){
	    randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
	    if(!special){
	      if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
	      if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
	      if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
	      if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
	    }
	    iteration++;
	    password += String.fromCharCode(randomNumber);
	  }
	  return password;
}
