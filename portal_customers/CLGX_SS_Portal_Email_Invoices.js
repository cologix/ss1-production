nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Portal_Email_Invoices.js
//	Script Name:	CLGX_SS_Portal_Email_Invoices
//	Script Id:		customscript_clgx_portal_email_invoices
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	CLGX_Portal_Email_Invoices
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		5/29/2012
//-------------------------------------------------------------------------------------------------

function scheduled_portal_email_invoices () {
	try {

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 5/29/2012
// Details:	Send invoices to customer by email - demands came from portal
//-----------------------------------------------------------------------------------------------------------------
		
		var usageLimit = 100;
		
		var arrColumns = new Array();
		var arrFilters = new Array();

		arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
		arrColumns[1] = new nlobjSearchColumn('custrecord_clgx_portal_invoices_customer', null, null);
		arrColumns[2] = new nlobjSearchColumn('custrecord_clgx_portal_invoices_contact', null, null);
		arrColumns[3] = new nlobjSearchColumn('custrecord_clgx_portal_invoices_list', null, null);
		arrFilters[0] = new nlobjSearchFilter('custrecord_clgx_portal_invoices_sent',null,'is','F');
		var searchResults = nlapiSearchRecord('customrecord_clgx_portal_email_invoices', null, arrFilters, arrColumns);

		for ( var i = 0; searchResults != null && i<searchResults.length; i++ ) {
			
            if((nlapiGetContext().getRemainingUsage() < usageLimit) && ((i + 1) < searchResults.length)){
                var context = nlapiGetContext();
                var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                if(status == 'QUEUED'){
                    nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
    			    break;
                }
            }
			
			var resultRow = searchResults[i];
			var columns = resultRow.getAllColumns();

			var internalID = resultRow.getValue(columns[0]);
			var customerID = resultRow.getValue(columns[1]);
			var contactID = resultRow.getValue(columns[2]);
			var stInvoices = resultRow.getValue(columns[3]);
			
			var arrInvoicesIDs = new Array();
			arrInvoicesIDs = stInvoices.split(",");
			
			arrPDFs = new Array();
			for(var j=0; j<arrInvoicesIDs.length; j++){
				var fileID = parseInt(nlapiLookupField('customrecord_clgx_consolidated_invoices', arrInvoicesIDs[j], 'custrecord_clgx_consol_inv_pdf_file_id'));
				
				var eResult = '0';
		        try {
		        	var objFileTry = nlapiLoadFile(fileID);
		        } 
		        catch (e) {
		            var str = String(e);
		            if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
		            	eResult = '1';
		            }
		        }

				if (eResult == '0'){
					var objFile = nlapiLoadFile(fileID);
					arrPDFs.push(objFile);
				}
			}
			
			emailSubject = 'Cologix Invoices';
			emailSubjectConfirm = 'Cologix Invoice sent.';
			emailBody = 'Dear Customer,\n\n' +
						'A copy of your invoice(s) is(are) attached for your records.\n\n' +
						'Thank you for your business.\n\n' +
						'Sincerely,\n' +
						'Cologix\n' +
						'1-855-IX-BILLS (492-4557)\n';
			nlapiSendEmail(12827,contactID,emailSubjectConfirm,emailBody,null,null,null,arrPDFs,true);
			
			date = new Date(); 
			nlapiSubmitField('customrecord_clgx_portal_email_invoices', internalID, ['custrecord_clgx_portal_invoices_sent','custrecord_clgx_portal_invoices_sentdate','custrecord_clgx_portal_invoices_senttime'], ['T',nlapiDateToString(date,'date'),nlapiDateToString(date,'timeofday')]);
		}


//---------- End Section 1 ------------------------------------------------------------------------------------------------

		var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
		nlapiLogExecution('DEBUG','Scheduled Script - Email Invoices','| ' + i + ' emails sent / ' + usageConsumtion + ' points used from 10,000. |');  

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}