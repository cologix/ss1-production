nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Portal_Delete_Sessions.js
//	Script Name:	CLGX_SS_Portal_Delete_Sessions
//	Script Id:		customscript_clgx_portal_delete_sessions
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	CLGX_customer_portal_sessions
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		5/29/2012
//-------------------------------------------------------------------------------------------------

function scheduled_portal_delete_sessions () {
	try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 5/29/2012
// Details:	Deletes customer portal sessions older than 30 minutes
//-----------------------------------------------------------------------------------------------------------------
		
		var usageLimit = 100;

		var date = new Date();
		date.setMinutes(date.getMinutes() - 30);

		var arrColumns = new Array();
		var arrFilters = new Array();
		arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
		arrFilters[0] = new nlobjSearchFilter('custrecord_clgx_portal_session_last_date',null,'onorbefore',nlapiDateToString(date, 'date'));
		arrFilters[1] = new nlobjSearchFilter('custrecord_clgx_portal_session_last_time',null,'lessthan',nlapiDateToString(date, 'timeofday'));
		var searchResults = nlapiSearchRecord('customrecord_clgx_portal_sessions_manage', null, arrFilters, arrColumns);

		for ( var i = 0; searchResults != null && i<searchResults.length; i++ ) {
			
            if((nlapiGetContext().getRemainingUsage() < usageLimit) && ((i + 1) < searchResults.length)){
                var context = nlapiGetContext();
                var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                if(status == 'QUEUED'){
                    nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
    			    break;
                }
            }

		var sidID = searchResults[i].getValue('internalid');
		
		// read session record
		var myRecord = nlapiLoadRecord('customrecord_clgx_portal_sessions_manage', sidID);
		var stSID = myRecord.getFieldValue('custrecord_clgx_portal_session_sid');
		var stUserIP = myRecord.getFieldValue('custrecord_clgx_portal_session_user_ip');
		var stDate = myRecord.getFieldValue('custrecord_clgx_portal_session_last_date');
		var stTime = myRecord.getFieldValue('custrecord_clgx_portal_session_last_time');
		var stSub = myRecord.getFieldValue('custrecord_clgx_portal_session_subsid');
		var stCustomerID = myRecord.getFieldValue('custrecord_clgx_portal_session_customer');
		var stUserID = myRecord.getFieldValue('custrecord_clgx_portal_session_contact');
		
		var arrCustomerDetails = nlapiLookupField('customer',stCustomerID,['companyname']);
        var stCustomerName = arrCustomerDetails['companyname'];
        
		var arrContactDetails = nlapiLookupField('contact',stUserID,['entityid']);
        var stUserName = arrContactDetails['entityid'];

		// create archive record
		var record = nlapiCreateRecord('customrecord_clgx_portal_sessions_archiv');
		record.setFieldValue('custrecord_clgx_portal_archive_sid', stSID);
		record.setFieldValue('custrecord_clgx_portal_archive_user_ip', stUserIP);
		record.setFieldValue('custrecord_clgx_portal_archive_date', stDate);
		record.setFieldValue('custrecord_clgx_portal_archive_time', stTime);
		record.setFieldValue('custrecord_clgx_portal_archive_subsid', parseInt(stSub));
		record.setFieldValue('custrecord_clgx_portal_archive_cust_id', parseInt(stCustomerID));
		record.setFieldValue('custrecord_clgx_portal_archive_cust_name', stCustomerName);
		record.setFieldValue('custrecord_clgx_portal_archive_user_id', parseInt(stUserID));
		record.setFieldValue('custrecord_clgx_portal_archive_user_name', stUserName);
		var idArchive = nlapiSubmitRecord(record, true);
		
		nlapiDeleteRecord('customrecord_clgx_portal_sessions_manage', sidID );
		}

//---------- End Section 1 ------------------------------------------------------------------------------------------------
		
		var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
		nlapiLogExecution('DEBUG','Scheduled Script - Delete Portal Sessions','| ' + i + ' sessions deleted / ' + usageConsumtion + ' points used from 10,000. |');  
	
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}