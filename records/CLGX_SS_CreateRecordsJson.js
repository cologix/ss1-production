nlapiLogExecution("audit","FLOStart",new Date().getTime());

function scheduled_create_json () {
    try {
        var jsonNative=getNativeJson();
        var sublists=getSublist();
        var strFileObj=new Object();
        var objFields=new Object();
        var objSublists=new Object();
        objFields.name='Fields';
        objFields.id='fields';
        objFields.type='';
        objFields.leaf=false;
        objFields.children=jsonNative;
        var arraySup=new Array();
        objSublists.name='Sublists';
        objSublists.id='sublists';
        objSublists.type='';
        objSublists.leaf=false;
        objSublists.children=sublists;
        arraySup.push(objFields);
        arraySup.push(objSublists);
        var objFile = nlapiLoadFile(6336525);
        var fileValue=objFile.getValue();
        var json=JSON.parse(fileValue);
        var strCS=json['supportcase'];
        var strContact=json['contact'];
        var strCustomer=json['customer'];
        var strPayment=json['customerpayment'];
        var strRefund=json['customerrefund'];
        var strEmail=json['emailtemplate'];
        var strEmp=json['employee'];
        var strEx=json['expensereport'];
        var strInv=json['invoice'];
        var strJournal=json['journalentry'];
        var strSO=json['salesorder'];
        var strService=json['service'];
        strFileObj.supportcase=strCS;
        strFileObj.contact=strContact;
        strFileObj.customer=strCustomer;
        strFileObj.customerpayment=strPayment;
        strFileObj.customerrefund=strRefund;
        strFileObj.emailtemplate=strEmail;
        strFileObj.employee=strEmp;
        strFileObj.expensereport=strEx;
        strFileObj.invoice=strInv;
        strFileObj.journalentry=strJournal;
        strFileObj.salesorder=strSO;
        strFileObj.service=strService;
        strFileObj.lead=arraySup;


        var fileName = nlapiCreateFile('natives.json', 'PLAINTEXT', JSON.stringify(strFileObj));
        fileName.setFolder(5029097);
        nlapiSubmitFile(fileName);
        // nlapiLogExecution('DEBUG','Scheduled Script - Create_averageStandard_json','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function getNativeJson(){
    var columns = new Array();
    var filters = new Array();
    var nativeFields=new Array();
    var customFields=new Array();
    var arrayNativeObj=new Array();
    var arrayCustomObj=new Array();
    var search = nlapiLoadSearch('lead', 'customsearch_clgx_ss_lead_rb', filters, columns);
    var resultSet = search.runSearch();
    var start=1;
    resultSet.forEachResult(function(searchResult) {
        var internalid=searchResult.getValue('internalid', null, null);
        var record=nlapiLoadRecord('lead',searchResult.getValue('internalid', null, null));
        var fields=record.getAllFields();

        for (var i = 0; fields != null && i < fields.length; i++) {

            if( fields[i].match(/custpage/g)==null &&(fields[i].match(/custentity/g)==null) ){

                var value=record.getFieldValue(fields[i]);
                if(value!=null &&  (!in_array(fields[i], nativeFields))){
                    nativeFields.push(fields[i]);
                    var field=record.getField(fields[i]);
                    if(field!=null) {
                        var label = field.label;
                        var type = field.type;
                        var obj = new Object();
                        if (label == '') {
                            label = fields[i];
                        }
                        obj.name = label;
                        obj.id = fields[i];
                        obj.type = type;
                        obj.leaf = true;
                        arrayNativeObj.push(obj);
                    }

                }
            }else {

                var value=record.getFieldValue(fields[i]);
                if(value!=null &&(!in_array(fields[i],  customFields))){
                    customFields.push(fields[i]);
                    var field=record.getField(fields[i]);
                    if(field!=null) {
                        var label = field.label;
                        var type = field.type;
                        var obj = new Object();
                        if (label == '') {
                            label = fields[i];
                        }
                        obj.name = label;
                        obj.id = fields[i];
                        obj.type = type;
                        obj.leaf = true;
                        arrayCustomObj.push(obj);
                    }
                }
            }

        }
        // }
        // start++;
        return true;

    });
    var arrReturn=new Array();
    var objRNative=new Object();
    var objRCustom=new Object();
    objRNative.name='Native';
    objRNative.id='native';
    objRNative.type='';
    objRNative.leaf=false;
    objRNative.children=arrayNativeObj;
    objRCustom.name='Custom';
    objRCustom.id='custom';
    objRCustom.type='';
    objRCustom.leaf=false;
    objRCustom.children=arrayCustomObj;
    arrReturn.push(objRNative);
    arrReturn.push(objRCustom);
    return arrReturn


}

function getSublist() {
    var columns = new Array();
    var filters = new Array();
    var nativeFields = new Array();
    var customFields = new Array();
    var arrayNativeObj = new Array();
    var arrayCustomObj = new Array();
    var nativeFields1 = new Array();
    var customFields1 = new Array();
    var arrayNativeObj1 = new Array();
    var arrayCustomObj1 = new Array();
    var nativeFields2 = new Array();
    var customFields2 = new Array();
    var arrayNativeObj2 = new Array();
    var arrayCustomObj2 = new Array();
    var nativeFields3 = new Array();
    var customFields3 = new Array();
    var arrayNativeObj3 = new Array();
    var arrayCustomObj3 = new Array();
    var nativeFields4 = new Array();
    var customFields4 = new Array();
    var arrayNativeObj4 = new Array();
    var arrayCustomObj4 = new Array();
    var nativeFields5 = new Array();
    var customFields5 = new Array();
    var arrayNativeObj5 = new Array();
    var arrayCustomObj5 = new Array();
    var nativeFields6 = new Array();
    var customFields6 = new Array();
    var arrayNativeObj6 = new Array();
    var arrayCustomObj6 = new Array();
    var nativeFields7= new Array();
    var customFields7 = new Array();
    var arrayNativeObj7 = new Array();
    var arrayCustomObj7 = new Array();
    var nativeFields8 = new Array();
    var customFields8 = new Array();
    var arrayNativeObj8 = new Array();
    var arrayCustomObj8 = new Array();
    var arrReturn = new Array();
    var arrReturn1 = new Array();
    var arrReturn2 = new Array();
    var arrReturn3 = new Array();
    var arrReturn4 = new Array();
    var arrReturn5 = new Array();
    var arrReturn6 = new Array();
    var arrReturn7 = new Array();
    var arrReturn8 = new Array();
    var search = nlapiLoadSearch('lead', 'customsearch_clgx_ss_lead_rb', filters, columns);
    var resultSet = search.runSearch();
    var start=1;
    resultSet.forEachResult(function(searchResult) {
        var internalid=searchResult.getValue('internalid', null, null);
        var record=nlapiLoadRecord('lead',searchResult.getValue('internalid', null, null));
        var fields = record.getAllLineItemFields('subscriptions');

    for (var i = 0; fields != null && i < fields.length; i++) {

        if (((fields[i].match(/custrecord/g) == null) &&(fields[i].match(/custrecord/g) == null)) && (!in_array(fields[i], nativeFields))) {
            var itemcount=record.getLineItemCount('subscriptions');
            if(itemcount>0) {
                var value = record.getLineItemValue('subscriptions', fields[i], 1);
                if (value != null) {
                    nativeFields.push(fields[i]);


                    var field = record.getLineItemField('subscriptions', fields[i], 1);
                    var label = field.label;
                    var type = field.type;
                    var obj = new Object();
                    if (label == '') {
                        label = fields[i];
                    }
                    obj.name = label;
                    obj.id = fields[i];
                    obj.type = type;
                    obj.leaf = true;
                    arrayNativeObj.push(obj);
                }
            }


        } else if (fields[i].match(/custrecord/g) != null && !in_array(fields[i], customFields)) {
            customFields.push(fields[i]);
            var itemcount=record.getLineItemCount('subscriptions');
            if(itemcount>0) {
                var value = record.getLineItemValue('subscriptions', fields[i], 1);
                if (value != null) {
                    var label = field.label;
                    var type = field.type;
                    var obj = new Object();
                    if (label == '') {
                        label = fields[i];
                    }
                    obj.name = label;
                    obj.id = fields[i];
                    obj.type = type;
                    obj.leaf = true;
                    arrayCustomObj.push(obj);
                }}

        }

    }

    var fields = record.getAllLineItemFields('addressbook');

    for (var i = 0; fields != null && i < fields.length; i++) {
        if (((fields[i].match(/custrecord/g) == null) ||(fields[i].match(/custrecord/g) == null)) && (!in_array(fields[i], nativeFields1))) {
            var itemcount=record.getLineItemCount('addressbook');
            if(itemcount>0) {
                var value = record.getLineItemValue('addressbook', fields[i], 1);
                if (value != null) {
            nativeFields1.push(fields[i]);
            var field = record.getLineItemField('addressbook', fields[i], 1);
            if (field != null) {

                var label = field.label;
                var type = field.type;
                var obj = new Object();

                if (label == '') {
                    label = fields[i];
                }
                obj.name = label;
                obj.id = fields[i];
                obj.type = type;
                obj.leaf = true;
                arrayNativeObj1.push(obj);
            }}
            }


        } else if (fields[i].match(/custrecord/g) != null && !in_array(fields[i], customFields1)) {
            var itemcount=record.getLineItemCount('addressbook');
            if(itemcount>0) {
                var value = record.getLineItemValue('addressbook', fields[i], 1);
                if (value != null) {
                    customFields1.push(fields[i]);
                    var field = record.getLineItemField('addressbook', fields[i], 1);

                    if (field != null) {

                        var label = field.label;
                        var type = field.type;
                        var obj = new Object();
                        if (label == '') {
                            label = fields[i];
                        }
                        obj.name = label;
                        obj.id = fields[i];
                        obj.type = type;
                        obj.leaf = true;
                        arrayCustomObj1.push(obj);
                    }
                }}

        }

    }

    var fields = record.getAllLineItemFields('creditcards');

    for (var i = 0; fields != null && i < fields.length; i++) {

        if (((fields[i].match(/custrecord/g) == null) ||(fields[i].match(/custrecord/g) == null)) && (!in_array(fields[i], nativeFields2))) {
            var itemcount=record.getLineItemCount('creditcards');
            if(itemcount>0) {
                var value = record.getLineItemValue('creditcards', fields[i], 1);
                if (value != null) {
                    nativeFields2.push(fields[i]);


                    var field = record.getLineItemField('creditcards', fields[i], 1);
                    if (field != null) {

                        var label = field.label;
                        var type = field.type;
                        var obj = new Object();
                        if (label == '') {
                            label = fields[i];
                        }
                        obj.name = label;
                        obj.id = fields[i];
                        obj.type = type;
                        obj.leaf = true;
                        arrayNativeObj2.push(obj);
                    }
                }
            }


        } else if (fields[i].match(/custrecord/g) != null && !in_array(fields[i], customFields2)) {
            var itemcount=record.getLineItemCount('creditcards');
            if(itemcount>0) {
                var value = record.getLineItemValue('creditcards', fields[i], 1);
                if (value != null) {
                    customFields2.push(fields[i]);
                    var field = record.getLineItemField('creditcards', fields[i], 1);
                    if (field != null) {

                        var label = field.label;
                        var type = field.type;
                        var obj = new Object();
                        if (label == '') {
                            label = fields[i];
                        }
                        obj.name = label;
                        obj.id = fields[i];
                        obj.type = type;
                        obj.leaf = true;
                        arrayCustomObj2.push(obj);
                    }
                }
            }

        }
    }


    var fields = record.getAllLineItemFields('addressbook');

    for (var i = 0; fields != null && i < fields.length; i++) {

        if (((fields[i].match(/custrecord/g) == null) ||(fields[i].match(/custrecord/g) == null)) && (!in_array(fields[i], nativeFields3))) {
            var itemcount=record.getLineItemCount('addressbook');
            if(itemcount>0) {
                var value = record.getLineItemValue('addressbook', fields[i], 1);
                if (value != null) {
                    nativeFields3.push(fields[i]);


                    var field = record.getLineItemField('addressbook', fields[i], 1);
                    if (field != null) {

                        var label = field.label;
                        var type = field.type;
                        var obj = new Object();
                        if (label == '') {
                            label = fields[i];
                        }
                        obj.name = label;
                        obj.id = fields[i];
                        obj.type = type;
                        obj.leaf = true;
                        arrayNativeObj3.push(obj);
                    }
                }}


        } else if (fields[i].match(/custrecord/g) != null && !in_array(fields[i], customFields3)) {
                    var itemcount=record.getLineItemCount('addressbook');
                    if(itemcount>0) {
                        var value = record.getLineItemValue('addressbook', fields[i], 1);
                        if (value != null) {
            customFields3.push(fields[i]);
            var field = record.getLineItemField('addressbook', fields[i], 1);
            if (field != null) {

                var label = field.label;
                var type = field.type;
                var obj = new Object();
                if (label == '') {
                    label = fields[i];
                }
                obj.name = label;
                obj.id = fields[i];
                obj.type = type;
                obj.leaf = true;
                arrayCustomObj3.push(obj);
            }}
            }

        }
    }

    var fields = record.getAllLineItemFields('creditcards');
    for (var i = 0; fields != null && i < fields.length; i++) {

        if (((fields[i].match(/custrecord/g) == null) ||(fields[i].match(/custrecord/g) == null)) && (!in_array(fields[i], nativeFields4))) {
            var itemcount=record.getLineItemCount('creditcards');
            if(itemcount>0) {
                var value = record.getLineItemValue('creditcards', fields[i], 1);
                if (value != null) {
                    nativeFields4.push(fields[i]);
                    var field = record.getLineItemField('creditcards', fields[i], 1);
                    if (field != null) {

                        var label = field.label;
                        var type = field.type;
                        var obj = new Object();
                        if (label == '') {
                            label = fields[i];
                        }
                        obj.name = label;
                        obj.id = fields[i];
                        obj.type = type;
                        obj.leaf = true;
                        arrayNativeObj4.push(obj);
                    }
                }}

        } else if (fields[i].match(/custrecord/g) != null && !in_array(fields[i], customFields4)) {
            var itemcount=record.getLineItemCount('creditcards');
            if(itemcount>0) {
                var value = record.getLineItemValue('creditcards', fields[i], 1);
                if (value != null) {
                    customFields4.push(fields[i]);
                    var field = record.getLineItemField('creditcards', fields[i], 1);
                    if (field != null) {

                        var label = field.label;
                        var type = field.type;
                        var obj = new Object();
                        if (label == '') {
                            label = fields[i];
                        }
                        obj.name = label;
                        obj.id = fields[i];
                        obj.type = type;
                        obj.leaf = true;
                        arrayCustomObj4.push(obj);
                    }
                }}
        }
    }

    var fields = record.getAllLineItemFields('contactroles');
    for (var i = 0; fields != null && i < fields.length; i++) {

        if (((fields[i].match(/custrecord/g) == null) ||(fields[i].match(/custrecord/g) == null)) && (!in_array(fields[i], nativeFields5))) {
            var itemcount=record.getLineItemCount('contactroles');
            if(itemcount>0) {
                var value = record.getLineItemValue('contactroles', fields[i], 1);
                if (value != null) {
                    nativeFields5.push(fields[i]);
                    var field = record.getLineItemField('contactroles', fields[i], 1);
                    if (field != null) {

                        var label = field.label;
                        var type = field.type;
                        var obj = new Object();
                        if (label == '') {
                            label = fields[i];
                        }
                        obj.name = label;
                        obj.id = fields[i];
                        obj.type = type;
                        obj.leaf = true;
                        arrayNativeObj5.push(obj);
                    }
                }
            }


        } else if (fields[i].match(/custrecord/g) != null && !in_array(fields[i], customFields5)) {
            var itemcount=record.getLineItemCount('contactroles');
            if(itemcount>0) {
                var value = record.getLineItemValue('contactroles', fields[i], 1);
                if (value != null) {
                    customFields5.push(fields[i]);
                    var field = record.getLineItemField('contactroles', fields[i], 1);
                    if (field != null) {

                        var label = field.label;
                        var type = field.type;
                        var obj = new Object();
                        if (label == '') {
                            label = fields[i];
                        }
                        obj.name = label;
                        obj.id = fields[i];
                        obj.type = type;
                        obj.leaf = true;
                        arrayCustomObj5.push(obj);
                    }
                }
            }

        }
    }


 var fields = record.getAllLineItemFields('itempricing');
    for (var i = 0; fields != null && i < fields.length; i++) {

        if (((fields[i].match(/custrecord/g) == null) ||(fields[i].match(/custrecord/g) == null)) && (!in_array(fields[i], nativeFields6))) {
            var itemcount=record.getLineItemCount('itempricing');
            if(itemcount>0) {
                var value = record.getLineItemValue('itempricing', fields[i], 1);
                if (value != null) {
                    nativeFields6.push(fields[i]);
                    var field = record.getLineItemField('itempricing', fields[i], 1);
                    if (field != null) {

                        var label = field.label;
                        var type = field.type;
                        var obj = new Object();
                        if (label == '') {
                            label = fields[i];
                        }
                        obj.name = label;
                        obj.id = fields[i];
                        obj.type = type;
                        obj.leaf = true;
                        arrayNativeObj6.push(obj);
                    }
                }
            }


        } else if (fields[i].match(/custrecord/g) != null && !in_array(fields[i], customFields6)) {
            var itemcount=record.getLineItemCount('itempricing');
            if(itemcount>0) {
                var value = record.getLineItemValue('itempricing', fields[i], 1);
                if (value != null) {
                    customFields6.push(fields[i]);
                    var field = record.getLineItemField('itempricing', fields[i], 1);
                    if (field != null) {

                        var label = field.label;
                        var type = field.type;
                        var obj = new Object();
                        if (label == '') {
                            label = fields[i];
                        }
                        obj.name = label;
                        obj.id = fields[i];
                        obj.type = type;
                        obj.leaf = true;
                        arrayCustomObj6.push(obj);
                    }
                }
            }
        }
    }
    var fields = record.getAllLineItemFields('currency');
    for (var i = 0; fields != null && i < fields.length; i++) {

        if (((fields[i].match(/custrecord/g) == null) ||(fields[i].match(/custrecord/g) == null)) && (!in_array(fields[i], nativeFields7))) {
            var itemcount=record.getLineItemCount('currency');
            if(itemcount>0) {
                var value = record.getLineItemValue('currency', fields[i], 1);
                if (value != null) {
                    nativeFields7.push(fields[i]);
                    var field = record.getLineItemField('currency', fields[i], 1);
                    if (field != null) {

                        var label = field.label;
                        var type = field.type;
                        var obj = new Object();
                        if (label == '') {
                            label = fields[i];
                        }
                        obj.name = label;
                        obj.id = fields[i];
                        obj.type = type;
                        obj.leaf = true;
                        arrayNativeObj7.push(obj);
                    }
                }
            }


        } else if (fields[i].match(/custrecord/g) != null && !in_array(fields[i], customFields7)) {
            var itemcount=record.getLineItemCount('currency');
            if(itemcount>0) {
                var value = record.getLineItemValue('currency', fields[i], 1);
                if (value != null) {
                    customFields7.push(fields[i]);
                    var field = record.getLineItemField('currency', fields[i], 1);
                    if (field != null) {

                        var label = field.label;
                        var type = field.type;
                        var obj = new Object();
                        if (label == '') {
                            label = fields[i];
                        }
                        obj.name = label;
                        obj.id = fields[i];
                        obj.type = type;
                        obj.leaf = true;
                        arrayCustomObj7.push(obj);
                    }
                }
            }

        }
    }




    var fields = record.getAllLineItemFields('grouppricing');

    for (var i = 0; fields != null && i < fields.length; i++) {

        if (((fields[i].match(/custrecord/g) == null) ||(fields[i].match(/custrecord/g) == null)) && (!in_array(fields[i], nativeFields8))) {
            var itemcount=record.getLineItemCount('grouppricing');
            if(itemcount>0) {
                var value = record.getLineItemValue('grouppricing', fields[i], 1);
                if (value != null) {
                    nativeFields8.push(fields[i]);


                    var field = record.getLineItemField('grouppricing', fields[i], 1);
                    if (field != null) {

                        var label = field.label;
                        var type = field.type;
                        var obj = new Object();
                        if (label == '') {
                            label = fields[i];
                        }
                        obj.name = label;
                        obj.id = fields[i];
                        obj.type = type;
                        obj.leaf = true;
                        arrayNativeObj8.push(obj);
                    }
                }
            }

        } else if (fields[i].match(/custrecord/g) != null && !in_array(fields[i], customFields8)) {
            var itemcount=record.getLineItemCount('grouppricing');
            if(itemcount>0) {
                var value = record.getLineItemValue('grouppricing', fields[i], 1);
                if (value != null) {
                    customFields8.push(fields[i]);
                    var field = record.getLineItemField('grouppricing', fields[i], 1);
                    if (field != null) {

                        var label = field.label;
                        var type = field.type;
                        var obj = new Object();
                        if (label == '') {
                            label = fields[i];
                        }
                        obj.name = label;
                        obj.id = fields[i];
                        obj.type = type;
                        obj.leaf = true;
                        arrayCustomObj8.push(obj);
                    }
                }
            }

        }
    }


        return true;

    });


    var objRNative = new Object();
    var objRCustom = new Object();
    objRNative.name = 'Native';
    objRNative.id = 'native';
    objRNative.type = '';
    objRNative.leaf = false;
    objRNative.children = arrayNativeObj;
    objRCustom.name = 'Custom';
    objRCustom.id = 'custom';
    objRCustom.type = '';
    objRCustom.leaf = false;
    objRCustom.children = arrayCustomObj;
    arrReturn.push(objRNative);
    arrReturn.push(objRCustom);


    var objRNative = new Object();
    var objRCustom = new Object();
    objRNative.name = 'Native';
    objRNative.id = 'native';
    objRNative.type = '';
    objRNative.leaf = false;
    objRNative.children = arrayNativeObj1;
    objRCustom.name = 'Custom';
    objRCustom.id = 'custom';
    objRCustom.type = '';
    objRCustom.leaf = false;
    objRCustom.children = arrayCustomObj1;
    arrReturn1.push(objRNative);
    arrReturn1.push(objRCustom);

    var objRNative = new Object();
    var objRCustom = new Object();
    objRNative.name = 'Native';
    objRNative.id = 'native';
    objRNative.type = '';
    objRNative.leaf = false;
    objRNative.children = arrayNativeObj2;
    objRCustom.name = 'Custom';
    objRCustom.id = 'custom';
    objRCustom.type = '';
    objRCustom.leaf = false;
    objRCustom.children = arrayCustomObj2;
    arrReturn2.push(objRNative);
    arrReturn2.push(objRCustom);

    var objRNative = new Object();
    var objRCustom = new Object();
    objRNative.name = 'Native';
    objRNative.id = 'native';
    objRNative.type = '';
    objRNative.leaf = false;
    objRNative.children = arrayNativeObj3;
    objRCustom.name = 'Custom';
    objRCustom.id = 'custom';
    objRCustom.type = '';
    objRCustom.leaf = false;
    objRCustom.children = arrayCustomObj3;
    arrReturn3.push(objRNative);
    arrReturn3.push(objRCustom);

    var objRNative = new Object();
    var objRCustom = new Object();
    objRNative.name = 'Native';
    objRNative.id = 'native';
    objRNative.type = '';
    objRNative.leaf = false;
    objRNative.children = arrayNativeObj4;
    objRCustom.name = 'Custom';
    objRCustom.id = 'custom';
    objRCustom.type = '';
    objRCustom.leaf = false;
    objRCustom.children = arrayCustomObj4;
    arrReturn4.push(objRNative);
    arrReturn4.push(objRCustom);


    var objRNative = new Object();
    var objRCustom = new Object();
    objRNative.name = 'Native';
    objRNative.id = 'native';
    objRNative.type = '';
    objRNative.leaf = false;
    objRNative.children = arrayNativeObj5;
    objRCustom.name = 'Custom';
    objRCustom.id = 'custom';
    objRCustom.type = '';
    objRCustom.leaf = false;
    objRCustom.children = arrayCustomObj5;
    arrReturn5.push(objRNative);
    arrReturn5.push(objRCustom);

    var objRNative = new Object();
    var objRCustom = new Object();
    objRNative.name = 'Native';
    objRNative.id = 'native';
    objRNative.type = '';
    objRNative.leaf = false;
    objRNative.children = arrayNativeObj6;
    objRCustom.name = 'Custom';
    objRCustom.id = 'custom';
    objRCustom.type = '';
    objRCustom.leaf = false;
    objRCustom.children = arrayCustomObj6;
    arrReturn6.push(objRNative);
    arrReturn6.push(objRCustom);

    var objRNative = new Object();
    var objRCustom = new Object();
    objRNative.name = 'Native';
    objRNative.id = 'native';
    objRNative.type = '';
    objRNative.leaf = false;
    objRNative.children = arrayNativeObj7;
    objRCustom.name = 'Custom';
    objRCustom.id = 'custom';
    objRCustom.type = '';
    objRCustom.leaf = false;
    objRCustom.children = arrayCustomObj7;
    arrReturn7.push(objRNative);
    arrReturn7.push(objRCustom);

    var objRNative = new Object();
    var objRCustom = new Object();
    objRNative.name = 'Native';
    objRNative.id = 'native';
    objRNative.type = '';
    objRNative.leaf = false;
    objRNative.children = arrayNativeObj8;
    objRCustom.name = 'Custom';
    objRCustom.id = 'custom';
    objRCustom.type = '';
    objRCustom.leaf = false;
    objRCustom.children = arrayCustomObj8;
    arrReturn8.push(objRNative);
    arrReturn8.push(objRCustom);
    /*
    var fields = record.getAllLineItemFields('recmachcustrecord_space_service_order');
    var nativeFields = new Array();
    var customFields = new Array();
    var arrayNativeObj = new Array();
    var arrayCustomObj = new Array();
    for (var i = 0; fields != null && i < fields.length; i++) {

        if (((fields[i].match(/custrecord/g) == null) ||(fields[i].match(/custrecord/g) == null)) && (!in_array(fields[i], nativeFields))) {
            nativeFields.push(fields[i]);


            var field = record.getLineItemField('recmachcustrecord_space_service_order', fields[i], 1);
            if (field != null) {

                var label = field.label;
                var type = field.type;
                var obj = new Object();
                if(label==''){
                    label  =fields[i];
                }
                obj.name = label;
                obj.id = fields[i];
                obj.type = type;
                obj.leaf = true;
                arrayNativeObj.push(obj);
            }


        } else if (fields[i].match(/custrecord/g) != null && !in_array(fields[i], customFields)) {
            customFields.push(fields[i]);
            var field = record.getLineItemField('recmachcustrecord_space_service_order', fields[i], 1);
            if (field != null) {

                var label = field.label;
                var type = field.type;
                var obj = new Object();
                if(label==''){
                    label  =fields[i];
                }
                obj.name = label;
                obj.id = fields[i];
                obj.type = type;
                obj.leaf = true;
                arrayCustomObj.push(obj);
            }

        }
    }

    var arrReturn9 = new Array();
    var objRNative = new Object();
    var objRCustom = new Object();
    objRNative.name = 'Native';
    objRNative.id = 'native';
    objRNative.type = '';
    objRNative.leaf = false;
    objRNative.children = arrayNativeObj;
    objRCustom.name = 'Custom';
    objRCustom.id = 'custom';
    objRCustom.type = '';
    objRCustom.leaf = false;
    objRCustom.children = arrayCustomObj;
    arrReturn9.push(objRNative);
    arrReturn9.push(objRCustom);

    var fields = record.getAllLineItemFields('recmachcustrecord_clgx_oob_service_order');
    var nativeFields = new Array();
    var customFields = new Array();
    var arrayNativeObj = new Array();
    var arrayCustomObj = new Array();
    for (var i = 0; fields != null && i < fields.length; i++) {

        if (((fields[i].match(/custrecord/g) == null) ||(fields[i].match(/custrecord/g) == null)) && (!in_array(fields[i], nativeFields))) {
            nativeFields.push(fields[i]);


            var field = record.getLineItemField('recmachcustrecord_clgx_oob_service_order', fields[i], 1);
            if (field != null) {

                var label = field.label;
                var type = field.type;
                var obj = new Object();
                if(label==''){
                    label  =fields[i];
                }
                obj.name = label;
                obj.id = fields[i];
                obj.type = type;
                obj.leaf = true;
                arrayNativeObj.push(obj);
            }


        } else if (fields[i].match(/custrecord/g) != null && !in_array(fields[i], customFields)) {
            customFields.push(fields[i]);
            var field = record.getLineItemField('recmachcustrecord_clgx_oob_service_order', fields[i], 1);
            if (field != null) {

                var label = field.label;
                var type = field.type;
                var obj = new Object();
                if(label==''){
                    label  =fields[i];
                }
                obj.name = label;
                obj.id = fields[i];
                obj.type = type;
                obj.leaf = true;
                arrayCustomObj.push(obj);
            }

        }
    }

    var arrReturn10 = new Array();
    var objRNative = new Object();
    var objRCustom = new Object();
    objRNative.name = 'Native';
    objRNative.id = 'native';
    objRNative.type = '';
    objRNative.leaf = false;
    objRNative.children = arrayNativeObj;
    objRCustom.name = 'Custom';
    objRCustom.id = 'custom';
    objRCustom.type = '';
    objRCustom.leaf = false;
    objRCustom.children = arrayCustomObj;
    arrReturn10.push(objRNative);
    arrReturn10.push(objRCustom);

    var fields = record.getAllLineItemFields('recmachcustrecord_power_circuit_service_order');
    var nativeFields = new Array();
    var customFields = new Array();
    var arrayNativeObj = new Array();
    var arrayCustomObj = new Array();
    for (var i = 0; fields != null && i < fields.length; i++) {

        if (((fields[i].match(/custrecord/g) == null) ||(fields[i].match(/custrecord/g) == null)) && (!in_array(fields[i], nativeFields))) {
            nativeFields.push(fields[i]);


            var field = record.getLineItemField('recmachcustrecord_power_circuit_service_order', fields[i], 1);
            if (field != null) {

                var label = field.label;
                var type = field.type;
                var obj = new Object();
                if(label==''){
                    label  =fields[i];
                }
                obj.name = label;
                obj.id = fields[i];
                obj.type = type;
                obj.leaf = true;
                arrayNativeObj.push(obj);
            }


        } else if (fields[i].match(/custrecord/g) != null && !in_array(fields[i], customFields)) {
            customFields.push(fields[i]);
            var field = record.getLineItemField('recmachcustrecord_power_circuit_service_order', fields[i], 1);
            if (field != null) {

                var label = field.label;
                var type = field.type;
                var obj = new Object();
                if(label==''){
                    label  =fields[i];
                }
                obj.name = label;
                obj.id = fields[i];
                obj.type = type;
                obj.leaf = true;
                arrayCustomObj.push(obj);
            }

        }
    }

    var arrReturn11 = new Array();
    var objRNative = new Object();
    var objRCustom = new Object();
    objRNative.name = 'Native';
    objRNative.id = 'native';
    objRNative.type = '';
    objRNative.leaf = false;
    objRNative.children = arrayNativeObj;
    objRCustom.name = 'Custom';
    objRCustom.id = 'custom';
    objRCustom.type = '';
    objRCustom.leaf = false;
    objRCustom.children = arrayCustomObj;
    arrReturn11.push(objRNative);
    arrReturn11.push(objRCustom);
*/
    var returnstring = new Array();
    var obj1= new Object();
    var obj2 = new Object();
    var obj3 = new Object();
    var obj4 = new Object();
    var obj5 = new Object();
    var obj6 = new Object();
    var obj7 = new Object();
    var obj8 = new Object();
    var obj9 = new Object();
    var obj10 = new Object();
    var obj11 = new Object();
    var obj12 = new Object();
    obj1.name = 'Subscriptions';
    obj1.id = 'subscriptions';
    obj1.type = '';
    obj1.leaf = false;
    obj1.children = arrReturn;

    obj2.name = 'Addressbook';
    obj2.id = 'addressbook';
    obj2.type = '';
    obj2.leaf = false;
    obj2.children = arrReturn1;

    obj3.name = 'Credit Cards';
    obj3.id = 'creditcards';
    obj3.type = '';
    obj3.leaf = false;
    obj3.children = arrReturn2;

    obj4.name = 'Addressbook';
    obj4.id = 'addressbook';
    obj4.type = '';
    obj4.leaf = false;
    obj4.children = arrReturn3;

    obj5.name = 'Credit Cards';
    obj5.id = 'creditcards';
    obj5.type = '';
    obj5.leaf = false;
    obj5.children = arrReturn4;

    obj6.name = 'Contact Roles';
    obj6.id = 'contactroles';
    obj6.type = '';
    obj6.leaf = false;
    obj6.children = arrReturn5;

    obj7.name = 'Item Pricing';
    obj7.id = 'itempricing';
    obj7.type = '';
    obj7.leaf = false;
    obj7.children = arrReturn6;

    obj8.name = 'Currency';
    obj8.id = 'currency';
    obj8.type = '';
    obj8.leaf = false;
    obj8.children = arrReturn7;

    obj9.name = 'Group Pricing';
    obj9.id = 'grouppricing';
    obj9.type = '';
    obj9.leaf = false;
    obj9.children = arrReturn8;

   /* obj10.name = 'Space';
    obj10.id = 'recmachcustrecord_space_service_order';
    obj10.type = '';
    obj10.leaf = false;
    obj10.children = arrReturn9;

    obj11.name = 'Out of Band';
    obj11.id = 'recmachcustrecord_clgx_oob_service_order';
    obj11.type = '';
    obj11.leaf = false;
    obj11.children = arrReturn10;

    obj12.name = 'Power Circuit';
    obj12.id = 'recmachcustrecord_power_circuit_service_order';
    obj12.type = '';
    obj12.leaf = false;
    obj12.children = arrReturn11;*/



    returnstring.push(obj1);
    returnstring.push(obj2);
    returnstring.push(obj3);
   // returnstring.push(obj4);
   // returnstring.push(obj5);
    returnstring.push(obj6);
    returnstring.push(obj7);
    returnstring.push(obj8);
    returnstring.push(obj9);
   // returnstring.push(obj10);
    //returnstring.push(obj11);
 //   returnstring.push(obj12);

    return returnstring;

}

//check if a value is in the array
function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}