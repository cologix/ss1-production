nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Records.js
//	Script Name:	CLGX_SL_Records
//	Script Id:		customscript_clgx_sl_records
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Catalina Taran
//	Created:		07/10/2017
//	Includes:		CLGX_LIB_Global.js
//	URL:			/app/site/hosting/scriptlet.nl?script=212&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_charts(request, response){
    try {
       // var market = request.getParameter('market');
       // var departmentid = request.getParameter('departmentid');
      //  var repid = request.getParameter('repid');
      //  var month = request.getParameter('month');
       // var forecast = request.getParameter('forecast');
       // var type = request.getParameter('type');


        var objFile = nlapiLoadFile(6327245);
        var html = objFile.getValue();
     //   html = html.replace(new RegExp('{salesJSON}','g'), dataSales(forecast, type, market, repid, month, departmentid));
        html = html.replace(new RegExp('{supportcase}','g'), getJson('supportcase'));
        html = html.replace(new RegExp('{contact}','g'), getJson('contact'));
        html = html.replace(new RegExp('{customer}','g'), getJson('customer'));
        html = html.replace(new RegExp('{customerpayment}','g'), getJson('customerpayment'));
        html = html.replace(new RegExp('{customerrefund}','g'), getJson('customerrefund'));
        html = html.replace(new RegExp('{emailtemplate}','g'), getJson('emailtemplate'));
        html = html.replace(new RegExp('{employee}','g'), getJson('employee'));
        html = html.replace(new RegExp('{expensereport}','g'), getJson('expensereport'));
        html = html.replace(new RegExp('{invoice}','g'), getJson('invoice'));
        html = html.replace(new RegExp('{item}','g'), getJson('item'));
        html = html.replace(new RegExp('{journalentry}','g'), getJson('journalentry'));
        html = html.replace(new RegExp('{lead}','g'), getJson('lead'));
        html = html.replace(new RegExp('{location}','g'), getJson('location'));
        html = html.replace(new RegExp('{message}','g'), getJson('message'));
        html = html.replace(new RegExp('{opportunity}','g'), getJson('opportunity'));
        html = html.replace(new RegExp('{partner}','g'), getJson('partner'));
        html = html.replace(new RegExp('{phonecall}','g'), getJson('phonecall'));
        html = html.replace(new RegExp('{job}','g'), getJson('job'));
        html = html.replace(new RegExp('{prospect}','g'), getJson('prospect'));
        html = html.replace(new RegExp('{purchaseorder}','g'), getJson('purchaseorder'));
        html = html.replace(new RegExp('{role}','g'), getJson('role'));
        html = html.replace(new RegExp('{task}','g'), getJson('task'));
        html = html.replace(new RegExp('{salesorder}','g'), getJson('salesorder'));
        html = html.replace(new RegExp('{service}','g'), getJson('service'));
        html = html.replace(new RegExp('{taxitem}','g'), getJson('taxitem'));
        html = html.replace(new RegExp('{taxgroup}','g'), getJson('taxgroup'));
        html = html.replace(new RegExp('{taxtype}','g'), getJson('taxtype'));
        html = html.replace(new RegExp('{timebill}','g'), getJson('timebill'));
        html = html.replace(new RegExp('{vendor}','g'), getJson('vendor'));
        html = html.replace(new RegExp('{subsidiary}','g'), getJson('subsidiary'));
        html = html.replace(new RegExp('{activeport}','g'), getJson('activeport'));

        
        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function getJson(type){
    var objFile = nlapiLoadFile(6336525);
    var fileValue=objFile.getValue();
    var json=JSON.parse(fileValue);
    var str=json[type];//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Records.js
//	Script Name:	CLGX_SL_Records
//	Script Id:		customscript_clgx_sl_records
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Catalina Taran
//	Created:		07/10/2017
//	Includes:		CLGX_LIB_Global.js
//	URL:			/app/site/hosting/scriptlet.nl?script=212&deploy=1
//-------------------------------------------------------------------------------------------------

    function suitelet_charts(request, response){
        try {
            // var market = request.getParameter('market');
            // var departmentid = request.getParameter('departmentid');
            //  var repid = request.getParameter('repid');
            //  var month = request.getParameter('month');
            // var forecast = request.getParameter('forecast');
            // var type = request.getParameter('type');


            var objFile = nlapiLoadFile(6327245);
            var html = objFile.getValue();
            //   html = html.replace(new RegExp('{salesJSON}','g'), dataSales(forecast, type, market, repid, month, departmentid));
            html = html.replace(new RegExp('{supportcase}','g'), getJson('supportcase'));

            response.write( html );
        }
        catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
            if (error.getDetails != undefined){
                nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
                throw error;
            }
            else{
                nlapiLogExecution('ERROR','Unexpected Error', error.toString());
                throw nlapiCreateError('99999', error.toString());
            }
        } // End Catch Errors Section ------------------------------------------------------------------------------------------
    }

    function getJson(type){
        var objFile = nlapiLoadFile(6336525);
        var fileValue=objFile.getValue();
        var json=JSON.parse(fileValue);
        var str=json[type];
        return JSON.stringify(str);
    }
    return JSON.stringify(str);
}