nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_API_Customer_GET_One.js
//	ScriptID:	customscript_clgx_rl_api_customer_get_one
//	ScriptType:	RESTlet
//	ScriptURL:	
//	Includes:	CLGX_LIB_API_Global
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	03/28/2016
//------------------------------------------------------

var get = wrap(function get(datain) {
	
	if (datain['id']) {
		
		var id = datain['id'];
		
		var rec = nlapiLoadRecord('customer', id);
		
		var obj = new Object();
		//obj["url"] = '/customers/' + id + '/';
		obj["customer_id"] = parseInt(rec.getFieldValue('id'));
		obj["matrix_id"] = rec.getFieldValue('custentity_clgx_matrix_entity_id');
		obj["customer"] = rec.getFieldValue('entityid');
		
		
		// add other customer fields
		if (datain['contacts']) {
			obj["contacts"] = append_contacts_roles (id);
		}
		if (datain['addresses']) {
			obj["addresses"] = append_addresses (id);
		}
		if (datain['orders']) {
			obj["orders"] = append_orders (id);
		}
		if (datain['cases']) {
			obj["cases"] = append_cases (id);
		}
		
		return obj;
		
	}
	else{
		return 'id?';
	}
});




function post (datain){
	try {
		
		var start = moment();
		
		var is_json = (datain.constructor.name == 'Object');
		if (!is_json) {
			datain = JSON.parse(datain);
		}
		
		var lang = datain['lang'];
		var ip = datain['ip'];
		var username = datain['username'];
		var password = datain['password'];
		var companyid = datain['companyid'];
		var askpass = datain['askpass'];
		
		var uuid = datain['uuid'];
		var radix = datain['radix'];
		
		 if(!radix){
			 var radix = generate_radix();
		 }
		
		var obj = new Object();
		obj["error"] = 'T';
		obj["code"] = 'USERERROR';
		obj["msg"] = '';
		obj["fail"] = 0;
		//obj["uuid"] = '';
		//obj["ip"] = ip;
		//obj["radix"] = radix;
		obj["me"] = {};
		obj["companies"] = [];
		obj["company"] = {};
		obj["ns"] = {};
		//obj["req"] = datain;
	  
		if(username){
		  
			var columns = new Array();
			var filters = new Array();
			filters.push(new nlobjSearchFilter('custentity_clx_customer_portal_username',null,'is',username));
			var record = nlapiSearchRecord('contact', 'customsearch_clgx_api_contact_login', filters, columns);
	  
			if(record){
				
				var contactid = parseInt(record[0].getValue('internalid',null,null));
				
				if(username && askpass){
			  
					var email = record[0].getValue('email',null,null);
					if(email){
						
						obj.error = 'F';
						obj.code = 'SUCCESS';
						
						var newpass = generate_password(12, true);
						
						if(lang == 17){
							subject = 'Portail de Cologix';
							body = 'Madame, Monsieur,\n\n' +
									'Voici votre mot de passe:\n\n' + newpass + '\n\n' +
									'Merci de faire affaire avec Cologix.\n\n' +
									'Au plaisir de vous &ecirc;tre utile,\n' +
									'Cologix\n\n';
							obj.msg = 'Un nouveau mot de passe a été envoyé à votre e-mail.';
						} else {
							subject = 'Cologix Customer Portal - Password Reset';
							body = 'Dear Customer,\n\n' +
									'Here is your new password:\n\n' + newpass + '\n\n' +
									'Thank you for your business.\n\n' +
									'Sincerely,\n' +
									'Cologix\n\n';
							obj.msg = 'A new password was sent to the email we have on file.';
						}
						
						nlapiSendEmail(432742, email, subject, body, null, null, null, null, true, null, "support@cologix.com");
						nlapiSubmitField('contact', contactid, 'custentity_clx_customer_portal_password', newpass);

					} else {
						if(lang == 17){
							obj.msg = 'Il n\'y a pas d\'e-mail associée à ce nom d\'utilisateur. Veuillez contacter le soutien technique de Cologix';
						} else {
							obj.msg = 'There is no email associated with this user name. Please contact support.';
						}
					}
			  
				} else if (username && password)  {
			  
					var recPassword = record[0].getValue('custentity_clx_customer_portal_password',null,null);
					var attempts = parseInt(record[0].getValue('custentity_clgx_portal_login_attempts',null,null));
			  
					if(password == recPassword && attempts < 7){
				 
						var name = record[0].getValue('entityid',null,null);
						nlapiSubmitField('contact', contactid, 'custentity_clgx_portal_login_attempts', 0); // re-initiate login attempts

						//obj.uuid = generate_uuid();
				 
						var sdamin = parseInt(record[0].getValue('custentity_clx_customer_portal_superadm',null,null));
						obj["me"] = {};
						obj.me["id"] = contactid;
						var rid = contactid.toString(radix);
						obj.me["rid"] = rid;
						//obj.me["irid"] = parseInt(rid, radix);
						obj.me["name"] = name;
						obj.me["inactive"] = record[0].getValue('isinactive',null,null);
						obj.me["sadmin"] = sdamin;
						obj.me["terms"] = record[0].getValue('custentity_clgx_portal_accepted_terms',null,null);
						var clang = parseInt(record[0].getValue('custentity_clx_customer_portal_language',null,null));
						obj.me["lang"] = clang;
						// 13 - English
						//  1 - US English
						// 17 - French (Canada)
	
						
						obj.companies = get_companies(contactid,radix);
				 
						obj.ns["ip"] = ip;
						obj.ns["uuid"] = generate_uuid();
						obj.ns["radix"] = radix;
				 
						var from = '';
						if(companyid){ // if a companyid was requested, verify if customer exist and if user have access to it
							
							var columns = new Array();
							var filters = new Array();
							filters.push(new nlobjSearchFilter('internalid',null,'is',companyid));
							var exist = nlapiSearchRecord('customer', 'customsearch_clgx_api_customers', filters, columns);
							
							if(exist){ // if requested company exist
								if(sdamin){ // if super admin serve it
									obj.company = get_company(contactid,companyid,radix,sdamin);
									if(obj.company.id != obj.company.id){
										var current = new Object();
										current["id"] = obj.company.id;
										current["rid"] = obj.company.rid;
										current["name"] = obj.company.name;
										obj.companies.push(current);
										_.uniq(obj.companies);
									}
								}
								else{ // if not super admin
									// verify if contact is attached to company
									var objCustomer = _.find(obj.companies, function(arr){ return (arr.id == companyid);});
									if(objCustomer){ // company is on his list, serve it
										obj.company = get_company(contactid,companyid,radix,sdamin);
										from = obj.company.name;
									} else { // company not on list, so serve the first one on the list
										if(obj.companies.length > 0){ // if any company
											obj.company = get_company(contactid,obj.companies[0].id,radix,null);
											from = obj.company.name;
										} else {
											obj.company = [];
										}
									}
								}
							} else { // requested company does not exist, so serve the first one on the list
								if(obj.companies.length > 0){ // if any company on the list
									obj.company = get_company(contactid,obj.companies[0].id,radix,null);
									from = obj.company.name;
								} else {
									obj.company = [];
								}
							}
						} else { // no company was requested , so serve the first company on the list
							if(obj.companies.length > 0){ // if any company
								obj.company = get_company(contactid,obj.companies[0].id,radix,null);
								from = obj.company.name;
							} else {
								obj.company = [];
							}
						}
						
						obj.error = 'F';
						obj.code = 'SUCCESS';
						
						if(clang == 17){
							obj.msg = 'Bonjour ' + obj.me.name + '. Bienvenue sur le portail de Cologix.';
						} else {
							obj.msg = 'Hello ' + obj.me.name + '. You are managing - ' + obj.company.name;
						}
						
						var now = moment().format("M/D/YYYY h:mm:ss a");
						var record = nlapiCreateRecord('customrecord_clgx_api_cust_portal_logins');
						record.setFieldValue('custrecord_clgx_api_cust_portal_uuid', obj.ns.uuid);
						record.setFieldValue('custrecord_clgx_api_cust_portal_ip', obj.ns.ip);
						record.setFieldValue('custrecord_clgx_api_cust_portal_radix', obj.ns.radix);
						record.setFieldValue('custrecord_clgx_api_cust_portal_date', now);
						record.setFieldValue('custrecord_clgx_api_cust_portal_contact', obj.me.id);
						record.setFieldValue('custrecord_clgx_api_cust_portal_entity', obj.company.id);
						record.setFieldValue('custrecord_clgx_api_cust_portal_rights', JSON.stringify(obj.company.rights));
						record.setFieldValue('custrecord_clgx_api_cust_portal_entities', JSON.stringify(obj.companies));
						record.setFieldValue('custrecord_clgx_api_cust_portal_sadmin', obj.me.sadmin);
						record.setFieldValue('custrecord_clgx_api_cust_portal_language', obj.me.lang);
						nlapiSubmitRecord(record, false,true);
						
						
					} else {
						if(attempts > 5){
							if(lang == 17){
								obj.msg = 'Vous avez fait 6 tentatives de connexion infructueuses et votre compte est maintenant désactivé. Veuillez contacter le soutien technique de Cologix.';
							} else {
								obj.msg = 'You had 6 consecutive failed logins attempts. Your account is disabled. Please contact Cologix support.';
							}
							obj.attempts = 6;
						} else {
							var left = 5 - attempts;
							nlapiSubmitField('contact', contactid, 'custentity_clgx_portal_login_attempts', attempts + 1);
							
						    //var rec = nlapiLoadRecord('contact', contactid);
				            //rec.setFieldValue('custentity_clgx_portal_login_attempts', attempts + 1); 
							//nlapiSubmitRecord(rec, false,true);
							
							if(lang == 17){
								obj.msg = 'Info de connexion incorrecte! Veuillez r&eacute;essayer. Il vous reste ' + left + ' tentatives.';
							} else {
								obj.msg = 'Incorrect login! Please try again. You have ' + left + ' attempt(s) left.';
							}
							
							obj.fail = attempts + 1;
						}
					}
				} else {
					if(lang == 17){
						obj.msg = 'Info de connexion incorrecte! Veuillez réessayer.';
					} else {
						obj.msg = 'Incorrect login! Please try again.';
					}
				}
			} else {
				
				if(lang == 17){
					obj.msg = 'Vous n\'avez pas accés au portail.  Veuillez contacter l\'administrateur du portail.';
				} else {
					obj.msg = 'You do not have access to the portal. Please contact your portal admin.';
				}
			}
		} else {
			
			if(lang == 17){
				obj.msg = 'Info de connexion incorrecte! Veuillez réessayer.';
			} else {
				obj.msg = 'Incorrect login! Please try again.';
			}
		}
	  
	  
		var usage = 5000 - parseInt(nlapiGetContext().getRemainingUsage());
		obj["usage"] = usage;
		
		var end = moment();
		var msec = end.diff(start);
		obj["time"] = msec/1000;
		
		obj["start"] = start.format('M/D/YYYY h:mm:ss');
		obj["last"] = end.format('M/D/YYYY h:mm:ss');
		
		obj["kb"] = ((JSON.stringify(obj)).length/1000).toFixed(3);
	
		return obj;

	}
  
	catch (error) {
	  
	  if (error.getDetails != undefined){
	      var code = error.getCode();
	      var msg = error.getDetails();
	      //nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	      //throw error;
	  } else {
	      var code = 'ERROR';
	      var msg = error.toString();
	      nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		  //throw nlapiCreateError('99999', error.toString());
	  }
		var obj = new Object();
		obj["error"] = 'T';
		obj["code"] = code;
		obj["msg"] = msg;
		obj["me"] = {};
		return obj;
	}
}


function append_contacts_roles(id) {
	
	var array = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('internalid','customer','is',id));
	var search = nlapiSearchRecord('contact', 'customsearch_clgx_company_contacts_roles', filters);
	if(search){
		for ( var i = 0; search != null && i < search.length; i++ ) {
			var obj= new Object();
			var contactid = parseInt(search[i].getValue('internalid',null,null));
			//obj["url"] = '/customers/' + id + '/contacts/' + contactid + '/';
			obj["contactid"] = contactid;
			obj["contact"] = search[i].getValue('entityid',null,null);
			obj["roleid"] = parseInt(search[i].getValue('contactrole',null,null)) || 0;
			obj["role"] = search[i].getText('contactrole',null,null) || '';
			array.push(obj);
		}
	}
	return array;
}

function append_addresses (id) {
	
	var array = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter("internalid",null,"anyof",id));
	var search = nlapiSearchRecord('customer', 'customsearch_clgx_invs_cust_addressees', filters);
	
    for ( var i = 0; search != null && i < search.length; i++ ) {
		var obj= new Object();
		var addressid = parseInt(search[i].getValue('addressinternalid','address',null));
		//obj["url"] = '/customers/' + id + '/addresses/' + addressid + '/';
		obj["addressid"] = addressid;
		obj["address1"] = search[i].getValue('address1','address',null);
		obj["address2"] = search[i].getValue('address2','address',null);
		obj["address3"] = search[i].getValue('address3','address',null);
		obj["attention"] = search[i].getValue('attention','address',null);
		obj["addressee"] = search[i].getValue('addressee','address',null);
		obj["addresspon"] = search[i].getValue('custrecord_clgx_address_pon','address',null);
		obj["billing"] = search[i].getValue('isdefaultbilling','address',null) || 'F';
		obj["shipping"] = search[i].getValue('isdefaultshipping','address',null) || 'F';
		obj["city"] = search[i].getValue('city','address',null);
		obj["state"] = search[i].getValue('state','address',null);
		obj["zipcode"] = search[i].getValue('zipcode','address',null);
		obj["country"] = search[i].getValue('country','address',null);
		array.push(obj);
	}
	return array;
}

function append_orders (id) {
	
	var arr = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('entity',null,'is',id));
	var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders', filters);
	if(records){
		for ( var i = 0; records != null && i < records.length; i++ ) {
			var obj = new Object();
			var orderid = parseInt(records[i].getValue('internalid',null,'GROUP'));
			obj["id"] = orderid;
			obj["number"] = records[i].getValue('transactionnumber',null,'GROUP');
			obj["date"] = records[i].getValue('trandate',null,'GROUP') || '';
			//obj["typeid"] = parseInt(records[i].getValue('type',null,'GROUP')) || 0;
			obj["type"] = records[i].getText('type',null,'GROUP') || '';
			//obj["statid"] = parseInt(records[i].getValue('status',null,'GROUP')) || 0;
			obj["status"] = records[i].getText('status',null,'GROUP') || '';
			arr.push(obj);
		}
	}
	return arr;
}

function append_cases (id) {
	
	var arrCases = new Array();
	
	
	// get customer cases
	
	
	return arrCases;
}




function get_companies(contactid,radix) {
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('internalid',null,'is',contactid));
	var records = nlapiSearchRecord('contact', 'customsearch_clgx_api_contact_companies', filters, columns);
	var arr = new Array();
	for ( var i = 0; records != null && i < records.length; i++ ) {
		var obj = new Object();
		var companyid = parseInt(records[i].getValue('internalid','company',null));
		obj["id"] = companyid;
		obj["rid"] = companyid.toString(radix);
		obj["name"] = records[i].getValue('companyname','company',null);
		arr.push(obj);
	}
	return arr;
}

function get_company(contactid,companyid,radix,sdamin) {
	
	var rec = nlapiLoadRecord('customer', companyid);
    var arrName = (rec.getFieldValue('entityid')).split(":");
    
    var obj = new Object();
	obj["id"] = companyid;
	obj["rid"] = companyid.toString(radix);
	obj["name"] = (arrName[arrName.length-1]).trim();
	var nac = parseInt(rec.getFieldValue('custentity_clgx_matrix_entity_id')) || 0;
	obj["nac"] = nac;
	obj["services"] = get_services(companyid);
	obj["rights"] = get_rights(contactid,companyid,radix,nac,sdamin);
	obj["menu"] = get_menu(contactid,obj,radix,sdamin);
	
	var objRep = new Object();
	var repid = parseInt(rec.getFieldValue('salesrep')) || 0;
	objRep["id"] = repid;
	objRep["rid"] = repid.toString(radix);
	objRep["name"] = rec.getFieldText('salesrep') || '';
	
	if(repid > 0){
    	var fields = ['email','phone'];
		var columns = nlapiLookupField('employee', repid, fields);
		objRep["email"] = columns.email;
		objRep["phone"] = columns.phone;
	}
	else{
		objRep["email"] = '';
		objRep["phone"] = '';
	}
	obj["salesrep"] = objRep;
	
	obj["contacts"] = get_contacts(companyid,radix);
	
	obj["cases"] = get_cases(companyid,radix);
	
	var arrOrders = get_orders(companyid,radix);
	obj["orders"] = arrOrders;
	var sos = _.filter(arrOrders, function(arr){
		return (arr.type == 'Service Order');
	});
	var ids = _.compact(_.uniq(_.pluck(sos, 'id')));
	
	obj["invoices"] = get_invoices(companyid,radix);
	
	if(ids.length > 0){
		obj["powers"] = get_powers(ids,radix);
		obj["spaces"] = get_spaces(ids,radix);
		obj["xcs"] = get_xcs(ids,radix);
	}
	else{
		obj["powers"] = [];
		obj["spaces"] = [];
		obj["xcs"] = [];
	}

	if(obj.nac){
    	var managed = new Object();
    	managed["backups"] = [];
    	managed["dns"] = [];
    	managed["ips"] = [];
    	managed["devices"] = [];
    	managed["ports"] = [];
    	managed["ipmi"] = [];
    	managed["vms"] = [];
    	managed["15min"] = [];
    	obj["managed"] = managed;
		
    	obj["legacyinv"] = [];
	}
	return obj;
}

function get_contacts(companyid,radix){
	
	var columns = new Array();
	columns.push(new nlobjSearchColumn('internalid',null,null));
	columns.push(new nlobjSearchColumn('entityid',null,null));
	columns.push(new nlobjSearchColumn('contactrole',null,null));
	columns.push(new nlobjSearchColumn('custentity_clgx_contact_secondary_role',null,null));
	columns.push(new nlobjSearchColumn('email',null,null));
	columns.push(new nlobjSearchColumn('title',null,null));
	var filters = new Array();
	filters.push(new nlobjSearchFilter('internalid','customer','is',companyid));
	filters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	var records = nlapiSearchRecord('contact', null, filters, columns);
	var arr = new Array();
	for ( var i = 0; records != null && i < records.length; i++ ) {
		var obj = new Object();
		var contactid = parseInt(records[i].getValue('internalid',null,null));
		obj["id"] = contactid;
		obj["rid"] = contactid.toString(radix);
		obj["name"] = records[i].getValue('entityid',null,null);
		obj["title"] = records[i].getValue('title',null,null);
		obj["role1"] = parseInt(records[i].getValue('contactrole',null,null)) || 0;
		//obj["role"] = records[i].getText('contactrole',null,null) || '';
		obj["role2"] = parseInt(records[i].getValue('custentity_clgx_contact_secondary_role',null,null)) || 0;
		//obj["role2"] = records[i].getText('custentity_clgx_contact_secondary_role',null,null) || '';
		obj["email"] = records[i].getValue('email',null,null);
		obj["portal"] = 'F';
		obj["admin"] = 'F';
		obj["terms"] = 'F';
		arr.push(obj);
	}
	return arr;
}

function get_cases(id,radix){
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter("internalid","company","anyof",id));
	var records = nlapiSearchRecord('supportcase', 'customsearch_clgx_api_cases', filters, columns);
	var arr = new Array();
	for ( var i = 0; records != null && i < records.length; i++ ) {
		var obj = new Object();
		var caseid = parseInt(records[i].getValue('internalid',null,null));
		obj["id"] = caseid;
		obj["rid"] = caseid.toString(radix);
		obj["number"] = records[i].getValue('casenumber',null,null);
		obj["title"] = records[i].getValue('title',null,null) || '';
		obj["status"] = parseInt(records[i].getValue('status',null,null)) || 0;
		//obj["status"] = records[i].getText('status',null,null) || '';
		obj["priority"] = parseInt(records[i].getValue('priority',null,null)) || 0;
		//obj["priority"] = records[i].getText('priority',null,null) || '';
		arr.push(obj);
	}
	return arr;
}

function get_orders(id,radix){
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
	var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders', filters, columns);
	var arr = new Array();
	for ( var i = 0; records != null && i < records.length; i++ ) {
		var obj = new Object();
		var orderid = parseInt(records[i].getValue('internalid',null,'GROUP'));
		obj["id"] = orderid;
		obj["rid"] = orderid.toString(radix);
		obj["number"] = records[i].getValue('transactionnumber',null,'GROUP');
		obj["date"] = records[i].getValue('trandate',null,'GROUP') || '';
		//obj["typeid"] = parseInt(records[i].getValue('type',null,'GROUP')) || 0;
		obj["type"] = records[i].getText('type',null,'GROUP') || '';
		//obj["statid"] = parseInt(records[i].getValue('status',null,'GROUP')) || 0;
		obj["status"] = records[i].getText('status',null,'GROUP') || '';
		arr.push(obj);
	}
	return arr;
}

function get_services(id){
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter("entity",null,"anyof",id));
	var records = nlapiSearchRecord('transaction', 'customsearch_clgx_api_orders_categs', filters, columns);
	var arr = new Array();
	for ( var i = 0; records != null && i < records.length; i++ ) {
		var columns = records[i].getAllColumns();
		arr.push(records[i].getValue(columns[0]));
	}
	return arr;
}


function get_invoices(id,radix){
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_customer",null,"anyof",id));
	var records = nlapiSearchRecord('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_api_invoices', filters, columns);
	var arr = new Array();
	for ( var i = 0; records != null && i < records.length; i++ ) {
		var obj = new Object();
		var invoiceid = parseInt(records[i].getValue('internalid',null,null));
		obj["id"] = invoiceid;
		obj["rid"] = invoiceid.toString(radix);
		obj["number"] = records[i].getValue('name',null,null);
		obj["date"] = records[i].getValue('custrecord_clgx_consol_inv_date',null,null) || '';
		obj["total"] = parseFloat(records[i].getValue('custrecord_clgx_consol_inv_total',null,null)) || 0;
		obj["balance"] = parseFloat(records[i].getValue('custrecord_clgx_consol_inv_balance',null,null)) || 0;
		obj["cur"] = records[i].getValue('custrecord_clgx_consol_inv_currency',null,null);
		var pdfid = parseInt(records[i].getValue('custrecord_clgx_consol_inv_pdf_file_id',null,null)) || 0;
		obj["pdf"] = pdfid;
		obj["rpdf"] = pdfid.toString(radix);
		var jsonid = parseInt(records[i].getValue('custrecord_clgx_consol_inv_json_file_id',null,null)) || 0;
		obj["json"] = jsonid;
		obj["rjson"] = jsonid.toString(radix);
		obj["cm"] = parseInt(records[i].getValue('custrecord_clgx_consol_inv_month',null,null)) || 0;
		obj["cy"] = parseInt(records[i].getValue('custrecord_clgx_consol_inv_year',null,null)) || 0;
		obj["dm"] = parseInt(records[i].getValue('custrecord_clgx_consol_inv_month_display',null,null)) || 0;
		obj["dy"] = parseInt(records[i].getValue('custrecord_clgx_consol_inv_year_display',null,null)) || 0;
		arr.push(obj);
	}
	return arr;
}

function get_powers(ids,radix){
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter("custrecord_power_circuit_service_order",null,"anyof",ids));
	var records = nlapiSearchRecord('customrecord_clgx_power_circuit', 'customsearch_clgx_api_powers', filters, columns);
	var arr = new Array();
	for ( var i = 0; records != null && i < records.length; i++ ) {
		var obj = new Object();
		var powerid = parseInt(records[i].getValue('internalid',null,null));
		obj["id"] = powerid;
		obj["rid"] = powerid.toString(radix);
		obj["number"] = records[i].getValue('name',null,null);
		
		var pairid = parseInt(records[i].getValue('custrecord_clgx_dcim_pair_power',null,null)) || 0;
		obj["pairid"] = pairid;
		obj["rpairid"] = pairid.toString(radix);
		obj["pair"] = records[i].getText('custrecord_clgx_dcim_pair_power',null,null) || '';
		
		var soid = parseInt(records[i].getValue('custrecord_power_circuit_service_order',null,null)) || 0;
		obj["soid"] = soid;
		obj["rsoid"] = soid.toString(radix);
		var name = (records[i].getText('custrecord_power_circuit_service_order',null,null)).split("#");
		obj["so"] = (name[name.length-1]).trim();
		
		var servid = parseInt(records[i].getValue('custrecord_cologix_power_service',null,null)) || 0;
		obj["servid"] = servid;
		obj["rservid"] = servid.toString(radix);
		var name = (records[i].getText('custrecord_cologix_power_service',null,null)).split(":")
		obj["serv"] = (name[name.length-1]).trim();
		
		obj["facility"] = parseInt(records[i].getValue('custrecord_cologix_power_facility',null,null)) || 0;
		//obj["facility"] = records[i].getText('custrecord_cologix_power_facility',null,null) || '';
		obj["panelid"] = parseInt(records[i].getValue('custrecord_clgx_dcim_device',null,null)) || 0;
		obj["panel"] = records[i].getText('custrecord_clgx_dcim_device',null,null) || '';
		obj["spaceid"] = parseInt(records[i].getValue('custrecord_cologix_power_space',null,null)) || 0;
		obj["space"] = records[i].getText('custrecord_cologix_power_space',null,null) || '';
		arr.push(obj);
	}
	return arr;
}

function get_spaces(ids,radix){
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter("custrecord_space_service_order",null,"anyof",ids));
	var records = nlapiSearchRecord('customrecord_cologix_space', 'customsearch_clgx_api_spaces', filters, columns);
	var arr = new Array();
	for ( var i = 0; records != null && i < records.length; i++ ) {
		var obj = new Object();
		var spaceid = parseInt(records[i].getValue('internalid',null,null));
		obj["id"] = spaceid;
		obj["rid"] = spaceid.toString(radix);
		obj["number"] = records[i].getValue('name',null,null);
		
		var soid = parseInt(records[i].getValue('custrecord_space_service_order',null,null)) || 0;
		obj["soid"] = soid;
		obj["rsoid"] = soid.toString(radix);
		var name = (records[i].getText('custrecord_space_service_order',null,null)).split("#");
		obj["so"] = (name[name.length-1]).trim();
		
		var servid = parseInt(records[i].getValue('custrecord_cologix_space_project',null,null)) || 0;
		obj["servid"] = servid;
		obj["rservid"] = servid.toString(radix);
		var name = (records[i].getText('custrecord_cologix_space_project',null,null)).split(":")
		obj["serv"] = (name[name.length-1]).trim();
		
		obj["facility"] = parseInt(records[i].getValue('custrecord_cologix_space_location',null,null)) || 0;
		//obj["facility"] = records[i].getText('custrecord_cologix_space_location',null,null) || '';
		arr.push(obj);
	}
	return arr;
}

function get_xcs(ids,radix){
	var columns = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter("custrecord_xconnect_service_order",null,"anyof",ids));
	var records = nlapiSearchRecord('customrecord_cologix_crossconnect', 'customsearch_clgx_api_xcs', filters, columns);
	var arr = new Array();
	for ( var i = 0; records != null && i < records.length; i++ ) {
		var obj = new Object();
		var xcid = parseInt(records[i].getValue('internalid',null,null));
		obj["id"] = xcid;
		obj["rid"] = xcid.toString(radix);
		obj["number"] = records[i].getValue('name',null,null);
		
		var soid = parseInt(records[i].getValue('custrecord_xconnect_service_order',null,null)) || 0;
		obj["soid"] = soid;
		obj["rsoid"] = soid.toString(radix);
		var name = (records[i].getText('custrecord_xconnect_service_order',null,null)).split("#");
		obj["so"] = (name[name.length-1]).trim();
		
		var servid = parseInt(records[i].getValue('custrecord_cologix_xc_service',null,null)) || 0;
		obj["servid"] = servid;
		obj["rservid"] = servid.toString(radix);
		var name = (records[i].getText('custrecord_cologix_xc_service',null,null)).split(":")
		obj["serv"] = (name[name.length-1]).trim();
		
		obj["facility"] = parseInt(records[i].getValue('custrecord_clgx_xc_facility',null,null)) || 0;
		//obj["facility"] = records[i].getText('custrecord_clgx_xc_facility',null,null) || '';
		arr.push(obj);
	}
	return arr;
}

function get_rights(contactid,companyid,radix,nac,sdamin) {
	
	var full = null;
	var view = null;
	
	if(sdamin){
		full = sdamin;
		view = 1;
	}
	else{
		// fetch rights from modifier record
	}
	
	var obj = new Object();
	obj["companies"] = full;
	obj["contacts"] = full;
	obj["orders"] = full;
	obj["cases"] = full;
	obj["invoices"] = view;
	obj["maintenances"] = view;
	obj["syshealth"] = view;
	obj["logins"] = view;
	
	obj["services"] = {};
	obj.services["powers"] = view;
	obj.services["spaces"] = view;
	obj.services["xcs"] = view;
	/*
	obj.services["vxcs"] = null;
	obj.services["net"] = null;
	obj.services["disaster"] = null;
	obj.services["equips"] = null;
	obj.services["others"] = null;
	*/
	if(!nac){
		full = null;
		view = null;
	}
	obj.services["domains"] = full;
	obj.services["ips"] = full;
	obj.services["dns"] = full;
	obj.services["ports"] = full;
	obj.services["vms"] = full;
	obj.services["ipmis"] = full;
	obj.services["backups"] = full;
	obj.services["ddos"] = full;
	obj.services["serv15min"] = full;
	
	obj["incidents"] = view;
	obj["entries"] = view;
	obj["visits"] = full;
	obj["shipments"] = full;
	
	return obj;
}

function generate_uuid() {
	  var s = [], itoh = '0123456789ABCDEF';
	  for (var i = 0; i <36; i++) s[i] = Math.floor(Math.random()*0x10);
	  s[14] = 4;
	  s[19] = (s[19] & 0x3) | 0x8;
	  for (var i = 0; i <36; i++) s[i] = itoh[s[i]];
	  s[8] = s[13] = s[18] = s[23] = '-';
	  return s.join('');
}

function generate_password(length, special) {
	  var iteration = 0;
	  var password = "";
	  var randomNumber;
	  if(special == undefined){
	      var special = false;
	  }
	  while(iteration < length){
	    randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
	    if(!special){
	      if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
	      if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
	      if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
	      if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
	    }
	    iteration++;
	    password += String.fromCharCode(randomNumber);
	  }
	  return password;
}

function generate_radix() {
	return Math.floor(Math.random() * (35 - 21)) + 21;
}


function get_menu(contactid,obj,radix,sadmin) {
	
	var arr = [];
	
	if(obj.rights.cases || obj.rights.maintenances || obj.rights.incidents || obj.rights.syshealth || obj.rights.visits || obj.rights.shipments){
		
		var menu = {"name":"Support","children":[]};
		if(obj.rights.contacts){menu.children.push({"name":"Cases","route":"/company/support/cases","rights":obj.rights.cases});}
		if(obj.rights.maintenances){menu.children.push({"name":"Maintenances","route":"/company/support/maintenances","rights":obj.rights.maintenances});}
		if(obj.nac){
			if(obj.rights.incidents){menu.children.push({"name":"Incidents","route":"/company/support/incidents","rights":obj.rights.incidents});}
			if(obj.rights.syshealth){menu.children.push({"name":"System Health","route":"/company/support/syshealth","rights":obj.rights.syshealth});}
			if(obj.rights.visits){menu.children.push({"name":"Visits","route":"/company/support/visits","rights":obj.rights.visits});}
			if(obj.rights.visits){menu.children.push({"name":"Visitors","route":"/company/support/visitors","rights":obj.rights.visits});}
			if(obj.rights.shipments){menu.children.push({"name":"Shipments","route":"/company/support/shipments","rights":obj.rights.shipments});}
		}
		arr.push(menu);
	}
	
	if(obj.rights.orders || obj.rights.invoices || obj.rights.services.spaces || obj.rights.services.powers || obj.rights.services.xcs || obj.rights.services.serv15min || obj.rights.services.vms || obj.rights.services.ips || obj.rights.services.dns || obj.rights.services.domains || obj.rights.services.alarms || obj.rights.services.ipmi || obj.rights.services.ddos || obj.rights.services.backups){
		
		var menu = {"name":"Services","children":[]};
		if(obj.rights.orders){menu.children.push({"name":"Orders","route":"/company/services/orders","rights":obj.rights.orders});}
		if(obj.rights.invoices){menu.children.push({"name":"Invoices","route":"/company/services/invoices","rights":obj.rights.invoices});}
		if(obj.nac){
			if(obj.rights.invoices){menu.children.push({"name":"Invoices (Legacy)","route":"/company/services/invoices_legacy","rights":obj.rights.invoices});}
		}
		if(_.indexOf(obj.services, 'spaces') > -1 || _.indexOf(obj.services, 'powers') > -1 || _.indexOf(obj.services, 'xcs') > -1){
			if(obj.rights.services.spaces && _.indexOf(obj.services, 'spaces') > -1){menu.children.push({"name":"Spaces","route":"/company/services/spaces","rights":obj.rights.services.spaces});}
			if(obj.rights.services.powers && _.indexOf(obj.services, 'powers') > -1){menu.children.push({"name":"Powers","route":"/company/services/powers","rights":obj.rights.services.powers});}
			if(obj.rights.services.xcs && _.indexOf(obj.services, 'xcs') > -1){menu.children.push({"name":"Interconnections","route":"/company/services/xcs","rights":obj.rights.services.xcs});}
		}
		if(obj.nac){
			if(obj.rights.services.serv15min){menu.children.push({"name":"15MinServers","route":"/company/services/managed/serv15min","rights":obj.rights.services.serv15min});}
			if(obj.rights.services.vms){menu.children.push({"name":"VMs","route":"/company/services/managed/vms","rights":obj.rights.services.vms});}
			if(obj.rights.services.ips){menu.children.push({"name":"IPs","route":"/company/services/managed/ips","rights":obj.rights.services.ips});}
			if(obj.rights.services.dns){menu.children.push({"name":"DNS","route":"/company/services/managed/dns","rights":obj.rights.services.dns});}
			if(obj.rights.services.domains){menu.children.push({"name":"Domains","route":"/company/services/managed/domains","rights":obj.rights.services.domains});}
			if(obj.rights.services.alarms){menu.children.push({"name":"Alarms","route":"/company/services/managed/alarms","rights":obj.rights.services.alarms});}
			if(obj.rights.services.ipmi){menu.children.push({"name":"IPMI","route":"/company/services/managed/ipmi","rights":obj.rights.services.ipmi});}
			if(obj.rights.services.ddos){menu.children.push({"name":"DDOS","route":"/company/services/managed/ddos","rights":obj.rights.services.ddos});}
			if(obj.rights.services.backups){menu.children.push({"name":"Backups","route":"routeobj.obj.rights.pany/services/managed/backups","rights":obj.rights.services.backups});}
		}
		arr.push(menu);
	}

	
	var menu = {"name":"Help","children":[]};
	menu.children.push({"name":"Documents","route":"/company/help/documents","rights":1});
	menu.children.push({"name":"Downloads","route":"/company/help/downloads","rights":1});
	menu.children.push({"name":"Help","route":"/company/help/help","rights":1});
	menu.children.push({"name":"About","route":"/company/help/about","rights":1});
	arr.push(menu);
	
	var menu = {"name":"Administration","children":[]};
	menu.children.push({"name":"My Profile","route":"/company/admin/myprofile","rights":4});
	if(obj.rights.companies){
		var companies = get_companies(contactid,radix);
		for ( var i = 0; companies != null && i < companies.length; i++ ) {
			menu.children.push({"name": companies[i].name ,"route":"/company/admin/companies?rid=" + companies[i].rid,"rights":obj.rights.companies});
		}
		if(sadmin){
			menu.children.push({"name": obj.name ,"route":"/company/admin/companies?rid=" + obj.rid,"rights":sadmin});
			_.uniq(menu.children);
		}
	}
	if(obj.rights.contacts){menu.children.push({"name":"Contacts","route":"/company/admin/contacts","rights":obj.rights.contacts});}
	if(obj.rights.logins){menu.children.push({"name":"Portal Logins","route":"/company/admin/portal_logins","rights":obj.rights.logins});}
	if(obj.nac && obj.rights.entries){menu.children.push({"name":"Entry Access Log","route":"/company/admin/entry_access_logs","rights":obj.rights.entries});}
	
	arr.push(menu);
	
	return arr;
}