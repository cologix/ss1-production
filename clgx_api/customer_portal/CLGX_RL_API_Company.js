nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_API_Company.js
//	ScriptID:	customscript_clgx_rl_api_company
//	ScriptType:	RESTlet
//	ScriptURL:	
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	04/27/2016
//------------------------------------------------------

function post (datain){
	try {
		
		var start = moment();
		
		var is_json = (datain.constructor.name == 'Object');
		if (!is_json) {
			datain = JSON.parse(datain);
		}
		
		var uuid = datain['uuid'];
		var ip = datain['ip'];
		var radix = datain['radix'];
		
		var crid = datain['crid'];
		var erid = datain['erid'];
		var lang = datain['lang'];
		
		var obj = new Object();
		obj["error"] = 'T';
		obj["code"] = 'USERERROR';
		obj["msg"] = '';
		obj["fail"] = 0;
		obj["me"] = {};
		obj["companies"] = [];
		obj["company"] = {};
		obj["ns"] = {};
		
		if(uuid && ip && radix && crid && erid && lang){
			
			var contactid = parseInt(crid, radix);
			var companyid = parseInt(erid, radix);
			
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_api_cust_portal_entity',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_api_cust_portal_entities',null,null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_contact",null,"anyof",contactid));
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_uuid",null,"is",uuid));
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_ip",null,"is",ip));
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_radix",null,"equalto",radix));
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_status",null,"anyof",1));
			var session = nlapiSearchRecord('customrecord_clgx_api_cust_portal_logins', null, arrFilters, arrColumns);

			if(session){
				
				var sid = parseInt(session[0].getValue('internalid',null,null));
				var scompanyid = parseInt(session[0].getValue('custrecord_clgx_api_cust_portal_entity',null,null));
				var scompanies = JSON.parse(session[0].getValue('custrecord_clgx_api_cust_portal_entities',null,null));
				var scompany = _.find(scompanies, function(arr){ return (arr.id == companyid) ; });
				if(scompany){
					
					// get contact
					var filters = new Array();
					filters.push(new nlobjSearchFilter('internalid',null,'is',contactid));
					var record = nlapiSearchRecord('contact', 'customsearch_clgx_api_contact_login', filters);
					
					var name = record[0].getValue('entityid',null,null);
					var sdamin = parseInt(record[0].getValue('custentity_clx_customer_portal_superadm',null,null));
					
					if(scompanyid != companyid){ // if other company refresh uuid and radix
						uuid = get_uuid();
						radix = get_radix();
					}
					
					obj["me"] = {};
					obj.me["id"] = contactid;
					var rid = contactid.toString(radix);
					obj.me["rid"] = rid;
					obj.me["name"] = name;
					obj.me["inactive"] = record[0].getValue('isinactive',null,null);
					obj.me["sadmin"] = sdamin;
					obj.me["terms"] = record[0].getValue('custentity_clgx_portal_accepted_terms',null,null);
					obj.me["lang"] = lang;
					
					obj.companies = get_companies(contactid,radix);
			 
					obj.ns["ip"] = ip;
					obj.ns["uuid"] = uuid;
					obj.ns["radix"] = radix;
			 
					obj.company = get_company(contactid,companyid,radix,sdamin);
					
					obj.error = 'F';
					obj.code = 'SUCCESS';
					
					if(lang == 17){
						obj.msg = 'Bonjour ' + obj.me.name;
					} else {
						obj.msg = 'Hello ' + obj.me.name + '. You are managing - ' + obj.company.name;
					}
					
					var now = moment().format("M/D/YYYY h:mm:ss a");
					if(scompanyid != companyid){
						set_session(obj);
						nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last','custrecord_clgx_api_cust_portal_status'], [now,2]);
					} else {
						nlapiSubmitField('customrecord_clgx_api_cust_portal_logins', sid, ['custrecord_clgx_api_cust_portal_last','custrecord_clgx_api_cust_portal_language'], [now,parseInt(lang)]);
					}

				} else {
					obj.msg = 'No rights for this company';
				}

			} else {
				obj.msg = 'Session request not valid';
			}
		} else {
			obj.msg = 'Missing arguments';
		}
		
		var usage = 5000 - parseInt(nlapiGetContext().getRemainingUsage());
		obj["usage"] = usage;
		
		var end = moment();
		var msec = end.diff(start);
		obj["time"] = msec/1000;
		
		obj["start"] = start.format('M/D/YYYY h:mm:ss');
		obj["last"] = end.format('M/D/YYYY h:mm:ss');
		
		obj["kb"] = ((JSON.stringify(obj)).length/1000).toFixed(3);
		
		return obj;
	}
  
	catch (error) {
	  
	  if (error.getDetails != undefined){
	      var code = error.getCode();
	      var msg = error.getDetails();
	      //nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	      //throw error;
	  } else {
	      var code = 'ERROR';
	      var msg = error.toString();
	      nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		  //throw nlapiCreateError('99999', error.toString());
	  }
		var obj = new Object();
		obj["error"] = 'T';
		obj["code"] = code;
		obj["msg"] = msg;
		obj["me"] = {};
		return obj;
	}
}

