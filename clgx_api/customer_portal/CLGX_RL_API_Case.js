nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_API_Case.js
//	ScriptID:	customscript_clgx_rl_api_case
//	ScriptType:	RESTlet
//	ScriptURL:	
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	04/27/2016
//------------------------------------------------------

function post (datain){
	try {

		var is_json = (datain.constructor.name == 'Object');
		if (!is_json) {
			datain = JSON.parse(datain);
		}
		
		var id = datain['id'];
		
		var obj = new Object();
		obj["error"] = 'T';
		obj["code"] = 'USERERROR';
		obj["msg"] = '';
		obj["supportcase"] = {};
		
		if(id){
			
			obj.error = 'F';
			obj.code = 'SUCCESS';
			obj["msg"]  = ''
				
			var rec = nlapiLoadRecord('supportcase', id);
			obj.supportcase["id"] = parseInt(rec.getFieldValue('id'));
			obj.supportcase["number"] = rec.getFieldValue('casenumber');
			obj.supportcase["title"] = rec.getFieldValue('title');
			
		} else {
			obj.msg = 'Missing arguments';
		}
		
		return obj;

	}
  
	catch (error) {
	  
	  if (error.getDetails != undefined){
	      var code = error.getCode();
	      var msg = error.getDetails();
	      //nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	      //throw error;
	  } else {
	      var code = 'ERROR';
	      var msg = error.toString();
	      nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		  //throw nlapiCreateError('99999', error.toString());
	  }
		var obj = new Object();
		obj["error"] = 'T';
		obj["code"] = code;
		obj["msg"] = msg;
		obj["me"] = {};
		return obj;
	}
}

