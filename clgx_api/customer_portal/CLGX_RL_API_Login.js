nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_API_Login.js
//	ScriptID:	customscript_clgx_rl_api_login
//	ScriptType:	RESTlet
//	ScriptURL:	
//	Includes:	CLGX_LIB_API_Global
//	@authors:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	04/27/2016
//------------------------------------------------------

function post (datain){
	try {
		
		var start = moment();
		
		var is_json = (datain.constructor.name == 'Object');
		if (!is_json) {
			datain = JSON.parse(datain);
		}
		
		var lang = datain['lang'] || 1;
		var ip = datain['ip'];
		var username = datain['username'];
		var password = datain['password'];
		var companyid = datain['companyid'];
		var askpass = datain['askpass'];
		
		var uuid = datain['uuid'];
		var radix = datain['radix'];
		
		var obj = new Object();
		obj["error"] = 'T';
		obj["code"] = 'USERERROR';
		obj["msg"] = '';
		obj["fail"] = 0;
		obj["me"] = {};
		obj["companies"] = [];
		obj["company"] = {};
		obj["ns"] = {};
	  
		if(username){
		  
			var columns = new Array();
			var filters = new Array();
			filters.push(new nlobjSearchFilter('custentity_clx_customer_portal_username',null,'is',username));
			var record = nlapiSearchRecord('contact', 'customsearch_clgx_api_contact_login', filters, columns);
	  
			if(record){
				
				var contactid = parseInt(record[0].getValue('internalid',null,null));
				
				if(username && askpass){
			  
					var email = record[0].getValue('email',null,null);
					if(email){
						
						obj.error = 'F';
						obj.code = 'SUCCESS';
						obj.msg = send_new_password(contactid,email,lang);
						
					} else {
						
						if(lang == 17){
							obj.msg = 'Il n\'y a pas d\'e-mail associée à ce nom d\'utilisateur. Veuillez contacter le soutien technique de Cologix';
						} else {
							obj.msg = 'There is no email associated with this user name. Please contact support.';
						}
					}
			  
				} else if (username && password)  {
			  
					var recPassword = record[0].getValue('custentity_clx_customer_portal_password',null,null);
					var attempts = parseInt(record[0].getValue('custentity_clgx_portal_login_attempts',null,null));
			  
					if(password == recPassword && attempts < 7){
				 
						var name = record[0].getValue('entityid',null,null);
						nlapiSubmitField('contact', contactid, 'custentity_clgx_portal_login_attempts', 0); // re-initiate login attempts

						var sdamin = parseInt(record[0].getValue('custentity_clx_customer_portal_superadm',null,null));
						
						var radix = get_radix();
						
						obj["me"] = {};
						obj.me["id"] = contactid;
						var rid = contactid.toString(radix);
						obj.me["rid"] = rid;
						obj.me["name"] = name;
						obj.me["inactive"] = record[0].getValue('isinactive',null,null);
						obj.me["sadmin"] = sdamin;
						obj.me["terms"] = record[0].getValue('custentity_clgx_portal_accepted_terms',null,null);
						var clang = parseInt(record[0].getValue('custentity_clx_customer_portal_language',null,null));
						obj.me["lang"] = clang;
						
						obj.companies = get_companies(contactid,radix);
				 
						obj.ns["ip"] = ip;
						obj.ns["uuid"] = get_uuid();
						obj.ns["radix"] = radix;
				 
						if(companyid){ // if a companyid was requested, verify if customer exist and if user have access to it
							
							var columns = new Array();
							var filters = new Array();
							filters.push(new nlobjSearchFilter('internalid',null,'is',companyid));
							var exist = nlapiSearchRecord('customer', 'customsearch_clgx_api_customers', filters, columns);
							
							if(exist){ // if requested company exist
								if(sdamin){ // if super admin serve it
									obj.company = get_company(contactid,companyid,radix,sdamin);
									if(obj.company.id != obj.company.id){
										var current = new Object();
										current["id"] = obj.company.id;
										current["rid"] = obj.company.rid;
										current["name"] = obj.company.name;
										obj.companies.push(current);
										_.uniq(obj.companies);
									}
								}
								else{ // if not super admin
									// verify if contact is attached to company
									var objCustomer = _.find(obj.companies, function(arr){ return (arr.id == companyid);});
									if(objCustomer){ // company is on his list, serve it
										obj.company = get_company(contactid,companyid,radix,sdamin);
									} else { // company not on list, so serve the first one on the list
										if(obj.companies.length > 0){ // if any company
											obj.company = get_company(contactid,obj.companies[0].id,radix,null);
										} else {
											obj.company = [];
										}
									}
								}
							} else { // requested company does not exist, so serve the first one on the list
								if(obj.companies.length > 0){ // if any company on the list
									obj.company = get_company(contactid,obj.companies[0].id,radix,null);
								} else {
									obj.company = [];
								}
							}
						} else { // no company was requested , so serve the first company on the list
							if(obj.companies.length > 0){ // if any company
								obj.company = get_company(contactid,obj.companies[0].id,radix,null);
							} else {
								obj.company = [];
							}
						}
						
						obj.error = 'F';
						obj.code = 'SUCCESS';
						
						if(clang == 17){
							obj.msg = 'Bonjour ' + obj.me.name + '. Bienvenue sur le portail de Cologix.';
						} else {
							obj.msg = 'Hello ' + obj.me.name + '. You are managing - ' + obj.company.name;
						}
						
						var newsid = set_session(obj);
						
					} else {
						if(attempts > 5){
							if(lang == 17){
								obj.msg = 'Vous avez fait 6 tentatives de connexion infructueuses et votre compte est maintenant désactivé. Veuillez contacter le soutien technique de Cologix.';
							} else {
								obj.msg = 'You had 6 consecutive failed logins attempts. Your account is disabled. Please contact Cologix support.';
							}
							obj.attempts = 6;
						} else {
							var left = 5 - attempts;
							nlapiSubmitField('contact', contactid, 'custentity_clgx_portal_login_attempts', attempts + 1);
							
						    //var rec = nlapiLoadRecord('contact', contactid);
				            //rec.setFieldValue('custentity_clgx_portal_login_attempts', attempts + 1); 
							//nlapiSubmitRecord(rec, false,true);
							
							if(lang == 17){
								obj.msg = 'Info de connexion incorrecte! Veuillez r&eacute;essayer. Il vous reste ' + left + ' tentatives.';
							} else {
								obj.msg = 'Incorrect login! Please try again. You have ' + left + ' attempt(s) left.';
							}
							
							obj.fail = attempts + 1;
						}
					}
				} else {
					if(lang == 17){
						obj.msg = 'Info de connexion incorrecte! Veuillez réessayer.';
					} else {
						obj.msg = 'Incorrect login! Please try again.';
					}
				}
			} else {
				
				if(lang == 17){
					obj.msg = 'Vous n\'avez pas accés au portail.  Veuillez contacter l\'administrateur du portail.';
				} else {
					obj.msg = 'You do not have access to the portal. Please contact your portal admin.';
				}
			}
		} else {
			
			if(lang == 17){
				obj.msg = 'Info de connexion incorrecte! Veuillez réessayer.';
			} else {
				obj.msg = 'Incorrect login! Please try again.';
			}
		}
	  
	  
		var usage = 5000 - parseInt(nlapiGetContext().getRemainingUsage());
		obj["usage"] = usage;
		
		var end = moment();
		var msec = end.diff(start);
		obj["time"] = msec/1000;
		
		obj["start"] = start.format('M/D/YYYY h:mm:ss');
		obj["last"] = end.format('M/D/YYYY h:mm:ss');
		
		obj["kb"] = ((JSON.stringify(obj)).length/1000).toFixed(3);
	
		return obj;

	}
  
	catch (error) {
	  
	  if (error.getDetails != undefined){
	      var code = error.getCode();
	      var msg = error.getDetails();
	      //nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	      //throw error;
	  } else {
	      var code = 'ERROR';
	      var msg = error.toString();
	      nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		  //throw nlapiCreateError('99999', error.toString());
	  }
		var obj = new Object();
		obj["error"] = 'T';
		obj["code"] = code;
		obj["msg"] = msg;
		obj["me"] = {};
		return obj;
	}
}


