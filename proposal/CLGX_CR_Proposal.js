nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_Proposal.js
//	Script Name:	CLGX_CR_Proposal
//	Script Id:		customscript_clgx_cr_proposal
//	Script Runs:	On Client
//	Script Type:	Client Script
//	Deployments:	Proposal - Sandbox | Production
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		1/10/2012
//-------------------------------------------------------------------------------------------------

function pageInit(type){
    try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 1/10/2012
// Details:	Display Contract Terms fields disabled or not depending on role.
//-------------------------------------------------------------------------------------------------

        var currentContext = nlapiGetContext();
        var stRole = currentContext.getRole();
        nlapiDisableField('class', true);

        var customerID = nlapiGetFieldValue('entity');

        if(customerID != null && customerID != '' && (type =='create' || type =='copy')){
            var stSOLang = nlapiLookupField('customer', customerID,'language');

            var nbrItems = nlapiGetLineItemCount('item'); // count number of Items

            var isKingStreet = 0;
            for (var i = 0; i < nbrItems; i++) {
                if(nlapiGetLineItemValue('item', 'location', i + 1) == 15){ // // if location is 905 King Street West
                    isKingStreet = 1;
                }
            }

            if(isKingStreet == 1){ // if location is 905 King Street West
                if(stSOLang == 'en_US' || stSOLang == 'en'){
                    nlapiSetFieldValue('custbody_clgx_contract_terms_title', '11');
                }
                else{
                    nlapiSetFieldValue('custbody_clgx_contract_terms_title', '12');
                }
            }
            else{
                if(stSOLang == 'en_US' || stSOLang == 'en'){
                    nlapiSetFieldValue('custbody_clgx_contract_terms_title', '2');
                }
                else{
                    nlapiSetFieldValue('custbody_clgx_contract_terms_title', '3');
                }
            }
        }

        var contractType = nlapiGetFieldValue('custbody_clgx_contract_terms_title');
        if (stRole == '1017' || stRole == '3' || stRole == '-5' || stRole == '18' || stRole == '1027' || stRole == '1029' || stRole == '1088'){ // if role is Cologix Product Manager or Admin or full or legal
            if(contractType == 7 || contractType == 8){
                nlapiDisableField('custbody_clgx_contract_terms_ready', false);
                nlapiDisableField('custbody_clgx_contract_terms_contact', false);
                nlapiDisableField('custbody_clgx_contract_terms_notes', false);
                nlapiDisableField('custbody_clgx_contract_terms_title', false);
                nlapiDisableField('custbodyclgx_aa_type', false);
                nlapiDisableField('custbody_cologix_annual_accelerator', false);
                nlapiDisableField('custbody_clgx_contract_terms_body', false);
            }
            else{
                nlapiDisableField('custbody_clgx_contract_terms_ready', false);
                nlapiDisableField('custbody_clgx_contract_terms_contact', false);
                nlapiDisableField('custbody_clgx_contract_terms_notes', false);
                nlapiDisableField('custbody_clgx_contract_terms_title', false);
                nlapiDisableField('custbodyclgx_aa_type', true);
                nlapiDisableField('custbody_cologix_annual_accelerator', true);
                nlapiDisableField('custbody_clgx_contract_terms_body', true);
            }
        }
        else if (stRole == '1018' || stRole == '1019' || stRole == '1039' || stRole == '1040' || stRole == '1046' || stRole == '1047' || stRole == '1088') { // if role is Cologix Sales Person or Sales VP
            nlapiDisableField('custbody_clgx_contract_terms_ready', false);
            nlapiDisableField('custbody_clgx_contract_terms_contact', false);
            nlapiDisableField('custbody_clgx_contract_terms_notes', false);
            nlapiDisableField('custbody_clgx_contract_terms_title', false);
            nlapiDisableField('custbodyclgx_aa_type', true);
            nlapiDisableField('custbody_cologix_annual_accelerator', true);
            nlapiDisableField('custbody_clgx_contract_terms_body', true);
        }
        else { // everybody else
            nlapiDisableField('custbody_clgx_contract_terms_contact', true);
            nlapiDisableField('custbody_clgx_contract_terms_title', true);
            nlapiDisableField('custbody_clgx_contract_terms_body', true);
            nlapiDisableField('custbody_clgx_contract_terms_notes', true);
            nlapiDisableField('custbody_clgx_contract_terms_ready', true);
            nlapiDisableField('custbodyclgx_aa_type', true);
            nlapiDisableField('custbody_cologix_annual_accelerator', true);
        }

//---------- End Section 1 ------------------------------------------------------------------------------------------------


//------------- Begin Section 2 -------------------------------------------------------------------
// Version:	1.0 - 4/1/2013
// Details:	If this customer is on credit hold, do not allow creating quote or contract
//-------------------------------------------------------------------------------------------------

        if(customerID != null && customerID != ''){

            var arrColumns = new Array();
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
            arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof',customerID));
            var searchCustomers = nlapiSearchRecord('customer', null, arrFilters, arrColumns);

            if(searchCustomers != null){
                var recCustomer = nlapiLoadRecord('customer', customerID);
                var onHold = recCustomer.getFieldValue('creditholdoverride');
                if(onHold == 'ON'){
                    if(stRole == '1018' || stRole == '1019' || stRole == '1039' || stRole == '1040' || stRole == '1046'){
                        nlapiSetFieldValue('custbody_clgx_credit_hold_warning', '<div style="color:#FF0000">This customer is on credit hold!</div>');
                        nlapiDisableField('custbody_clgx_contract_terms_ready', true);
                        nlapiDisableField('custbody_clgx_uploaded_to_box', true);
                    }
                    else{
                        nlapiSetFieldValue('custbody_clgx_credit_hold_warning', '<div style="color:#FF0000">This customer is on credit hold!</div>');
                    }
                }
            }
        }

//---------- End Section 2 ------------------------------------------------------------------------------------------------

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function fieldChanged(type, name) {
    try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 1/10/2012
// Details:	Manage the Contract Terms fields on the proposal form.
//-----------------------------------------------------------------------------------------------------------------

        var currentContext = nlapiGetContext();
        var stRole = currentContext.getRole();
        if (currentContext.getExecutionContext() == 'userinterface') {
            if (name == 'custbody_clgx_prop_incremental_mrc') {
                var incremental = nlapiGetFieldValue('custbody_clgx_prop_incremental_mrc');
                if(parseFloat(incremental)<parseFloat(0))
                {
                    alert('Incremental MRC must be greater than zero!');
                    return false;
                }
            }

            if (name == 'entity') {
                var customerID = nlapiGetFieldValue('entity');
                var stSOLang = nlapiLookupField('customer', customerID,'language');
                if(stSOLang == 'en_US' || stSOLang == 'en'){
                    nlapiSetFieldValue('custbody_clgx_contract_terms_title', '2');
                }
                else{
                    nlapiSetFieldValue('custbody_clgx_contract_terms_title', '3');
                }
            }


            if (name == 'custbody_clgx_contract_terms_title') {
                var stTermsBody = nlapiLookupField('customrecord_clgx_contract_terms', nlapiGetFieldValue('custbody_clgx_contract_terms_title'), 'custrecord_clgx_contract_terms_body');
                nlapiSetFieldValue('custbody_clgx_contract_terms_body', stTermsBody);

                var contractType = nlapiGetFieldValue('custbody_clgx_contract_terms_title');


                if(contractType == 1){
                    nlapiSetFieldValue('custbodyclgx_aa_type', 1);
                    nlapiSetFieldValue('custbody_cologix_annual_accelerator', '3');
                }
                else if(contractType == 2){
                    nlapiSetFieldValue('custbodyclgx_aa_type', 3);
                    nlapiSetFieldValue('custbody_cologix_annual_accelerator', '3');
                }
                else if(contractType == 3){
                    nlapiSetFieldValue('custbodyclgx_aa_type', 3);
                    nlapiSetFieldValue('custbody_cologix_annual_accelerator', '3');
                }
                else if(contractType == 4){
                    nlapiSetFieldValue('custbodyclgx_aa_type', '');
                    nlapiSetFieldValue('custbody_cologix_annual_accelerator', '');
                }
                else if(contractType == 5){
                    nlapiSetFieldValue('custbodyclgx_aa_type', '');
                    nlapiSetFieldValue('custbody_cologix_annual_accelerator', '');
                }
                else if(contractType == 6){
                    nlapiSetFieldValue('custbodyclgx_aa_type', 1);
                    nlapiSetFieldValue('custbody_cologix_annual_accelerator', '3');
                }
                else if(contractType == 7){
                    nlapiSetFieldValue('custbodyclgx_aa_type', '');
                    nlapiSetFieldValue('custbody_cologix_annual_accelerator', '');
                }
                else if(contractType == 8){
                    nlapiSetFieldValue('custbodyclgx_aa_type', '');
                    nlapiSetFieldValue('custbody_cologix_annual_accelerator', '');
                }
                else if(contractType == 15){
                    nlapiSetFieldValue('custbodyclgx_aa_type', '');
                    nlapiSetFieldValue('custbody_cologix_annual_accelerator', '');
                }
                else{

                }

                if(contractType != 7 && contractType != 8){
                    nlapiDisableField('custbody_clgx_contract_terms_body', true);
                    nlapiDisableField('custbodyclgx_aa_type', true);
                    nlapiDisableField('custbody_cologix_annual_accelerator', true);
                }
                else{
                    if (stRole == '1017' || stRole == '3' || stRole == '-5' || stRole == '18' || stRole == '1027' || stRole == '1029'){ // if role is Cologix Product Manager or Admin or full or legal
                        nlapiDisableField('custbody_clgx_contract_terms_body', false);
                        nlapiDisableField('custbodyclgx_aa_type', false);
                        nlapiDisableField('custbody_cologix_annual_accelerator', false);
                    }
                    else{
                        nlapiDisableField('custbody_clgx_contract_terms_body', true);
                        nlapiDisableField('custbodyclgx_aa_type', true);
                        nlapiDisableField('custbody_cologix_annual_accelerator', true);
                    }
                }

            }
            if (name == 'custbody_clgx_contract_terms_contact') {
                var stAttention = nlapiLookupField('contact', nlapiGetFieldValue('custbody_clgx_contract_terms_contact'), 'entityid');
                nlapiSetFieldValue('custbody_clgx_contract_terms_attention', stAttention);
            }


            if (name == 'entity') {
                var customerID = nlapiGetFieldValue('entity');

                if(customerID != null && customerID != ''){

                    var currentContext = nlapiGetContext();
                    var stRole = currentContext.getRole();

                    var recCustomer = nlapiLoadRecord('customer', customerID);
                    var onHold = recCustomer.getFieldValue('creditholdoverride');
                    if(onHold == 'ON'){
                        if(stRole == '1018' || stRole == '1019' || stRole == '1039' || stRole == '1040' || stRole == '1046'){
                            nlapiSetFieldValue('custbody_clgx_credit_hold_warning', '<div style="color:#FF0000">This customer is on credit hold!</div>');
                            nlapiDisableField('custbody_clgx_contract_terms_ready', true);
                            nlapiDisableField('custbody_clgx_uploaded_to_box', true);
                        }
                        else{
                            nlapiSetFieldValue('custbody_clgx_credit_hold_warning', '<div style="color:#FF0000">This customer is on credit hold!</div>');
                        }
                    }
                    else{
                        nlapiSetFieldValue('custbody_clgx_credit_hold_warning', '<div style="color:#FF0000"></div>');
                    }
                }
            }
        }

//---------- End Section 1 ------------------------------------------------------------------------------------------------


//------------- Begin Section 2 -----------------------------------------------------------------------------------
// Version:	1.0 - 04/30/2013
// Details:	If any of the items lines has location '905 King Street West' choose specific terms
//-----------------------------------------------------------------------------------------------------------------
        if (type=='item' && name == 'location'){
            var customerID = nlapiGetFieldValue('entity');
            var terms = nlapiGetFieldValue('custbody_clgx_contract_terms_title');
            var stSOLang = nlapiLookupField('customer', customerID,'language');
            if(nlapiGetCurrentLineItemValue('item', 'location') == 15 && (terms == 2 ||  terms == 3)){ // if location is 905 King Street West and default terms
                if(stSOLang == 'en_US' || stSOLang == 'en'){
                    nlapiSetFieldValue('custbody_clgx_contract_terms_title', '11');
                }
                else{
                    nlapiSetFieldValue('custbody_clgx_contract_terms_title', '12');
                }
            }
            /*
             else{
             if(stSOLang == 'en_US' || stSOLang == 'en'){
             nlapiSetFieldValue('custbody_clgx_contract_terms_title', '2');
             }
             else{
             nlapiSetFieldValue('custbody_clgx_contract_terms_title', '3');
             }
             }
             */
        }

//---------- End Sections ------------------------------------------------------------------------------------------------

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function lineInit(type){
    try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 1/20/2012
// Details:	Disables field 'class' on 'item' sublist
//-----------------------------------------------------------------------------------------------------------------

        if (type=='item'){
            nlapiDisableLineItemField('item', 'class', true);
        }

//---------- End Section 1 ------------------------------------------------------------------------------------------------

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function saveRecord(){
    try {

        var currentContext = nlapiGetContext();
        stRole = currentContext.getRole();

//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 4/4/2013
// Details:	If Status is is Closed-Lost, Loss reason is mandatory
//-------------------------------------------------------------------------------------------------
        var allowSave = 1;
        var alertMsg = '';
        var currentContext = nlapiGetContext();
        var usrRole = currentContext.getRole();
        var status = nlapiGetFieldText('entitystatus');
        var lossReason = nlapiGetFieldValue('custbody_clgx_winlossreason');
        var itemsCount =nlapiGetLineItemCount('item');
        var order_notes= nlapiGetFieldValue('custbody_clgx_order_notes');
        //alert(itemsCount);
        //Only if user has admin, full or AR Clerk roles can add discount items to proposal
        // if ((usrRole != 1011)&&(itemsCount>0))
        if (itemsCount>0)
        {
            for ( var j = 1; j <= itemsCount; j++ ) {
                //  var itemId= nlapiGetLineItemValue('item', 'item', j);
                var itemType= nlapiGetLineItemValue('item', 'itemtype', j);
                var amount= nlapiGetLineItemValue('item', 'amount', j);
                /*
                 if((itemType=='Discount')&&(usrRole != 1011 && usrRole != 3 && usrRole != 18 && usrRole!=5))
                 {
                 alertMsg = 'Discount Item is not valid for this proposal';
                 allowSave=0;
                 }
                 */
                if((itemType!='Discount')&&(usrRole != 3 && usrRole != 18)&&(amount<0))
                {
                    alertMsg='Negative amounts are not permited';
                    allowSave=0;

                }
            }
        }

        if (status == 'Closed Lost' && (lossReason == null || lossReason == '')) {
            alertMsg = 'If status is "Closed-Lost" then you have to choose the Loss reason.';
            allowSave = 0;
        }
        if (status == 'Closed Lost' && (order_notes == null || order_notes == '')) {
            alertMsg = 'If status is "Closed-Lost" then you have to choose the Loss reason and enter an Order Note.';
            allowSave = 0;
        }
//------------- Begin Section 2 -------------------------------------------------------------------
// Date:	01/24/1014
// Details:	Restrict DAL1 and DAL2 for certain items
//-------------------------------------------------------------------------------------------------
        // 2  DAL1
        // 17 DAL2
        //ID – 222 Network Bandwidth"
        //ID – 523 "IP Services
        //ID – 355 “Fixed�
        //ID – 554 “Burstable Commit"
        //ID – 555 "Burstable Commit Redundant Port"
        //ID - 556 “Fixed Redundant Port"

        var nbrItems = nlapiGetLineItemCount('item');

        var strItems = '';
        for ( var i = 1; i < nbrItems + 1; i++ ) {
            var itemid = nlapiGetLineItemValue('item', 'item', i);
            var item = nlapiGetLineItemText('item', 'item', i);
            var locationid = nlapiGetLineItemValue('item', 'location', i);

            if((locationid == 2 || locationid == 17) && stRole != '3' && stRole != '-5' && stRole != '18'){
                if(itemid == 222 || itemid == 523 || itemid == 355 || itemid == 554 || itemid == 555 || itemid == 556){
                    strItems += item + ', ';
                    //allowSave = 0;
                    alertMsg = 'There are service(s) that cannot be sold at this facility';
                }
            }
        }

//-------------------------------------------------------------------------------------------------
        if (allowSave == 0) {
            alert(alertMsg);
            return false;
        }
        else{
            return true;
        }
//-------------------------------------------------------------------------------------------------

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function getDateTime(){
    var date = new Date();
    var month = parseInt(date.getMonth()) + 1;
    var day = date.getDate();
    var year = date.getFullYear();


    var hour = date.getHours();
    var minute = date.getMinutes();
    var second = date.getSeconds();

    if(parseInt(month) < 10){
        month = '0' + month;
    }
    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = month + '/' + day + '/' + year + ' ' + hour + ':' + minute + ':' + second;

    return formattedDate;
}

function stringToArray (str)
{
    //Use ChrCode 5 as a separator
    var strChar5 = String.fromCharCode(5);

    //Use the Split method to create an array,
    //where Chrcode 5 is the separator/delimiter
    var multiSelectStringArray = str.split(strChar5);
    return multiSelectStringArray;
}

function displayResult ()
{
    var str = stringToArray(nlapiGetFieldValue('custentity8'));
    alert (str);
}