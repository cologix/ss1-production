//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Proposal.js
//	Script Name:	CLGX_SU_Proposal
//	Script Id:		customscript_clgx_su_proposal
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Proposal - Sandbox |
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		1/12/2012
//	Includes:		CLGX_LIB_Transaction_Totals.js, underscore-min.js
//-------------------------------------------------------------------------------------------------

function beforeLoad (type, form) {
    try {
        nlapiSetFieldValue("custbody_clgx2_proposal_view_actions", "<script type=\"text/javascript\">var fieldObject = document.getElementById(\"createsalesord\");fieldObject.setAttribute(\"onclick\", \"process_estopp('salesord'); this.disabled = true; return false;\");</script>");

        var currentContext = nlapiGetContext();
        //------------- Begin Section  -------------------------------------------------------------------
        // Created:	08/21/2017 - Dan
        // Details:	Verify if proposal is processed to create PDF - if yes, don't allow editing.
        //-------------------------------------------------------------------------------------------------

        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'edit')) {
            var locked = nlapiGetFieldValue('custbody_clgx_transaction_locked');
            if (locked == 'T') {
                var arrParam = new Array();
                arrParam['custscript_internal_message'] = 'The PDF budgetary quote or contract is being created. Please wait until you have received the confirmation email.';
                nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
            }
        }

        //------------- Begin Section -------------------------------------------------------------------
        // Created:
        // Details:
        //-------------------------------------------------------------------------------------------------

        var  stRole = currentContext.getRole();
        var allow = 0;
        if (stRole == '-5' || stRole == '3' || stRole == '18') {
            allow = 1;
        }
        var renewedFromSos=nlapiGetFieldValues('custbody_clgx_renewed_from_sos');
        var opp=nlapiGetFieldValue('opportunity');
        if((opp!=null)&&(opp!=''))
        {
            var oppRecord=nlapiLoadRecord('opportunity',opp);
            var saletype=oppRecord.getFieldValue('custbody_cologix_opp_sale_type');
            if((stRole==1017)&&(saletype==1)&&((renewedFromSos=='')||(renewedFromSos==null)))
            {
                form.getField('custbody_clgx_prop_incremental_mrc').setDisplayType('normal');

            }
            if((stRole==18)||(stRole==1017))
            {
                form.getField('custbody_clgx_prop_incremental_mrc').setDisplayType('normal');
            }
        }


        var closed = 0;
        if(closed == 1 && allow == 0 && (type == 'create' || type == 'edit')){ // if module is closed and any other role then admin
            var arrParam = new Array();
            arrParam['custscript_internal_message'] = 'This module is closed for modifications. Sorry for the inconvenience. Please come back later.';
            nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
        }

        if (currentContext.getExecutionContext() == 'userinterface'){
//------------- Begin Section 1 -------------------------------------------------------------------
// Created:	4/17/2014
// Details:	No role can create a new proposal unless from an opportunity or admin or full roles
//-------------------------------------------------------------------------------------------------

            if(allow == 0 && type == 'create' && stRole != 1039){ // if any other role then admin or customer care
                var opportunity = nlapiGetFieldValue('opportunity');
                if(opportunity == null || opportunity == ''){
                    var arrParam = new Array();
                    arrParam['custscript_internal_message'] = 'You can\'t create a proposal directly. Please create an opportunity first.';
                    nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
                }
            }

//------------- Begin Section 2 -------------------------------------------------------------------
// Created:	4/1/2013
// Details:	If this customer is on credit hold, display an warning
//-------------------------------------------------------------------------------------------------

            if (type == 'view' || type == 'edit'){
                var customerID = nlapiGetFieldValue('entity');
                if(customerID != null && customerID != ''){
                    var recCustomer = nlapiLoadRecord('customer', customerID);
                    var onHold = recCustomer.getFieldValue('creditholdoverride');
                    if(onHold == 'ON'){
                        nlapiSetFieldValue('custbody_clgx_credit_hold_warning', '<div style="color:#FF0000">This customer is on credit hold!</div>');
                    }
                }
            }

//------------- Begin Section 3 -------------------------------------------------------------------
// Created:	4/15/2014
// Details:	Display download links for BQ and Contract
//-------------------------------------------------------------------------------------------------

            if (type == 'view' || type == 'edit'){
                var fileQuoteUrl = '';
                var fileQuoteID = nlapiGetFieldValue('custbody_clgx_prop_bq_file_id');
                if(fileQuoteID != null && fileQuoteID != ''){

                    try {
                        var objFileQuote = nlapiLoadFile(fileQuoteID);
                        fileQuoteUrl = objFileQuote.getURL();
                    }
                    catch (e) {
                        var str = String(e);
                        if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
                            fileQuoteID = 0;
                        }
                    }

                    if(fileQuoteUrl != ''){
                        nlapiSetFieldValue('custbody_clgx_download_budgetray_quote', '<a href="' + fileQuoteUrl + '" target="_blank"><img src="//www.cologix.com/images/icon/application_pdf.png" alt="BQ" height="16" width="16"></a>');
                    }

                }
                var fileContractUrl = '';
                var fileContractID = nlapiGetFieldValue('custbody_clgx_prop_contract_file_id');
                if(fileContractID != null && fileContractID != ''){
                    try {
                        var objFileContract = nlapiLoadFile(fileContractID);
                        fileContractUrl = objFileContract.getURL();
                    }
                    catch (e) {
                        var str = String(e);
                        if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
                            fileContractID = 0;
                        }
                    }

                    if(fileContractUrl != ''){
                        nlapiSetFieldValue('custbody_clgx_download_contract', '<a href="' + fileContractUrl + '" target="_blank"><img src="//www.cologix.com/images/icon/acrobat_reader_16x16.gif" alt="Contract" height="16" width="16"></a>');
                    }
                }
            }

//------------- Begin Section 4 -----------------------------------------------------------------------------------
// Created:	4/15/2014
// Details: Display transaction totals in the header in an inlineHTML field
//-----------------------------------------------------------------------------------------------------------------

            if (type == 'view' || type == 'edit') {

                var proposalid = nlapiGetRecordId();

                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_location',null,null).setSort(false));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_total',null,null));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_total_nrc',null,null));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_transaction",null,"anyof",proposalid));
                var searchTotals = nlapiSearchRecord('customrecord_clgx_totals_transaction', null, arrFilters, arrColumns);

                var html = '<table cellpadding="2" border="1" style="font-family: verdana,arial,sans-serif;font-size:11px;color:#333333;border-width: 1px;border-color: #999999;border-collapse: collapse;padding:5px;">';

                html += '<tr>';
                html += '<th style="padding: 5px;background-color: #dedede;">Location</th>';
                html += '<th style="padding: 5px;background-color: #dedede;">Total Recurring</th>';
                html += '<th style="padding: 5px;background-color: #dedede;">Total NRC</th>';
                html += '</tr>';

                for ( var i = 0; searchTotals != null && i < searchTotals.length; i++ ) {
                    var searchTotal = searchTotals[i];
                    html += '<tr>';
                    html += '<td style="padding: 5px;">' + searchTotal.getText('custrecord_clgx_totals_location',null,null) + '</td>';
                    html += '<td style="padding: 5px;">$' + addCommas(searchTotal.getValue('custrecord_clgx_totals_total',null,null)) + '</td>';
                    html += '<td style="padding: 5px;">$' + addCommas(searchTotal.getValue('custrecord_clgx_totals_total_nrc',null,null)) + '</td>';
                    html += '</tr>';

                }
                html += '</table>';

                nlapiSetFieldValue('custbody_clgx_transaction_totals', html);
            }

//------------- Begin Section 5 -----------------------------------------------------------------------------------
// Created:	6/22/2016
// Details: Display Cloud Connect Details on Box if the following items on the order; internal ID's: 768, 769, 770, 771.
//-----------------------------------------------------------------------------------------------------------------

            if (type == 'view' || type == 'edit') {

                var itemsCount = nlapiGetLineItemCount('item');

                if (itemsCount > 0) {
                    for (var j = 1; j <= itemsCount; j++) {
                        var itemID = nlapiGetLineItemValue('item', 'item', j);
                        if( itemID==792 || itemID==793 || itemID==794 || itemID==795 || itemID==796 || itemID==797 || itemID==798 || itemID==799 || itemID==800 || itemID==803)
                        {
                            form.getField('custbody_clgx_cloud_details').setDisplayType('normal');
                        }
                    }
                }
            }

//---------- End Sections ------------------------------------------------------------------------------------------------
        }
        //nlapiLogExecution('DEBUG','User Event - Befor Load','|-------------FINISHED--------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function beforeSubmit(type){
    try {

        var currentContext = nlapiGetContext();

//------------- Begin Section 1 -------------------------------------------------------------------
// Created:	2/7/2014
// Details:	When a proposal is deleted, update any SO that was used to it's renewal
//-------------------------------------------------------------------------------------------------

        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'delete')){
            var arrSOs = nlapiGetFieldValues('custbody_clgx_renewed_from_sos');
            for ( var i = 0; arrSOs != null && i < arrSOs.length; i++ ) {
                nlapiSubmitField('salesorder', arrSOs[i], 'custbody_clgx_so_renewed_on_proposal', '');
            }
        }

//------------- Begin Section 2 -------------------------------------------------------------------
// Created:	4/30/2015
// Details:	When a proposal is created, initialize SilverPOP fields
//-------------------------------------------------------------------------------------------------
        /*
                if (type == 'create' || type == 'copy'){
                    nlapiSetFieldValue('custbody_clgx_sp_json_sync', '');
                    nlapiSetFieldValue('custbody_clgx_sp_sync', 'F');
                }
        */
//---------- End Sections ------------------------------------------------------------------------------------------------

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function afterSubmit(type){
    try {

        var currentContext = nlapiGetContext();

        //------------- Begin Section 1 -------------------------------------------------------------------
        // Details:	incrementalMrc
        //-------------------------------------------------------------------------------------------------

        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'edit')) {

            var incrementalMrc=nlapiGetFieldValue('custbody_clgx_prop_incremental_mrc');
            var oppIDRecord=nlapiGetFieldValue('opportunity');
            var opRecord=nlapiLoadRecord('opportunity',oppIDRecord);
            opRecord.setFieldValue('custbody_cologix_opp_incremental_mrc',incrementalMrc);
            nlapiSubmitRecord(opRecord, false, true);
        }

//------------- Begin Section 2 -------------------------------------------------------------------
// Version:	1.0 - 4/23/2012
// Details:	Calculate NRC and MRC subtotals from items sublist on header fields.
//-------------------------------------------------------------------------------------------------

        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'edit' || type == 'create')){

            var proposalid = nlapiGetRecordId();
            var customerid = nlapiGetFieldValue('entity');
            var channelRep= nlapiGetFieldValue('custbody_clgx_channel_rep');
            var salesrep=nlapiGetFieldValue('salesrep');

            var stNbrItems = nlapiGetLineItemCount('item');
            var curNRC = 0;
            var curMRC = 0;
            var oppIDRecord=nlapiGetFieldValue('opportunity');
            if(oppIDRecord!=null && oppIDRecord!='') {
                var opRecord = nlapiLoadRecord('opportunity', oppIDRecord);
                opRecord.setFieldValue('custbody_clgx_channel_rep', channelRep);
                opRecord.setFieldValue('salesrep', salesrep);
                nlapiSubmitRecord(opRecord, false, true);
            }

            for (var i = 0; i < parseInt(stNbrItems); i++) {
                var currAmount = nlapiGetLineItemValue('item', 'amount', i + 1);
                if (currAmount == null || currAmount == ''){
                    currAmount = 0;
                }
                var stBS = nlapiGetLineItemText('item', 'class', i + 1);
                if (stBS.indexOf("Recurring") > -1) {
                    curMRC = curMRC + parseFloat(currAmount);
                }
                else if (stBS.indexOf("NRC") > -1){
                    curNRC = curNRC + parseFloat(currAmount);
                }
                else {
                    // ignore other classes - capital labor
                }
            }

            nlapiSubmitField('estimate', proposalid, ['custbody_clgx_total_recurring','custbody_clgx_total_non_recurring'], [curMRC,curNRC]);

            // Details:	Calculates NRC and MRC totals from items sublist to custom record 'Transaction Totals'
            var totals = clgx_transaction_totals (customerid, 'proposal', proposalid);

        }

//------------- Begin Section 3 -------------------------------------------------------------------
// Version:	1.0 - 4/24/2012
// Version:	2.0 - 7/24/2012
// Details:	Update any items lines changes to the coresponding opportunity + recalculates NRC & MRC totals of the opportunity
//-------------------------------------------------------------------------------------------------
        if (currentContext.getExecutionContext() == 'userinterface' && type != 'delete') {

            var optyID  = nlapiGetFieldValue('opportunity');
            var proposalid = nlapiGetRecordId();
            var customerid = nlapiGetFieldValue('entity');

            if(optyID != null && optyID != ''){
                // search if there are SOs created from any proposal created from the opportunity of this proposal
                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('internalid',null,null));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter('opportunity',null,'anyof',optyID));
                var searchSOs = nlapiSearchRecord('salesorder', null, arrFilters, arrColumns);

                // update only if there is no SO from any proposal created from the opportunity of this proposal
                if (searchSOs == null && type == 'edit'){

                    var optyID  = nlapiGetFieldValue('opportunity');
                    if (optyID != null){
                        var stProposalID = nlapiGetRecordId();
                        var optyID  = nlapiGetFieldValue('opportunity');
                        var stNbrItems = nlapiGetLineItemCount('item');
                        var stSubsidiary  = nlapiGetFieldText('subsidiary');

                        var objOpty = nlapiLoadRecord('opportunity', optyID);
                        var status = objOpty.getFieldValue('status');

                        if(status != 'Closed - Lost'){

                            var nbrItemsOppty = objOpty.getLineItemCount('item');

                            for (var i = 0; i < nbrItemsOppty; i++){
                                objOpty.removeLineItem('item',1);
                            }

                            var curNRC = 0;
                            var curMRC = 0;

                            for (var i = 0; i < stNbrItems; i++) {
                                objOpty.selectNewLineItem('item');
                                objOpty.setCurrentLineItemValue('item','item', nlapiGetLineItemValue('item', 'item', i + 1));
                                objOpty.setCurrentLineItemValue('item','quantity', nlapiGetLineItemValue('item', 'quantity', i + 1));
                                objOpty.setCurrentLineItemValue('item','description', nlapiGetLineItemValue('item', 'description', i + 1));
                                objOpty.setCurrentLineItemValue('item','price', nlapiGetLineItemValue('item', 'price', i + 1));
                                objOpty.setCurrentLineItemValue('item','rate', nlapiGetLineItemValue('item', 'rate', i + 1));
                                objOpty.setCurrentLineItemValue('item','amount', nlapiGetLineItemValue('item', 'amount', i + 1));
                                objOpty.setCurrentLineItemValue('item','altsalesamt', nlapiGetLineItemValue('item', 'altsalesamt', i + 1));
                                if (stSubsidiary == 'Cologix Canada'){
                                    objOpty.setCurrentLineItemValue('item','taxcode', nlapiGetLineItemValue('item', 'taxcode', i + 1));
                                    objOpty.setCurrentLineItemValue('item','taxrate1', nlapiGetLineItemValue('item', 'taxrate1', i + 1));
                                    objOpty.setCurrentLineItemValue('item','taxrate2', nlapiGetLineItemValue('item', 'taxrate2', i + 1));
                                }
                                objOpty.setCurrentLineItemValue('item','options', nlapiGetLineItemValue('item', 'options', i + 1));
                                objOpty.setCurrentLineItemValue('item','costestimatetype', nlapiGetLineItemValue('item', 'costestimatetype', i + 1));
                                objOpty.setCurrentLineItemValue('item','location', nlapiGetLineItemValue('item', 'location', i + 1));
                                objOpty.setCurrentLineItemValue('item','class', nlapiGetLineItemValue('item', 'class', i + 1));
                                objOpty.setCurrentLineItemValue('item','custcol_cologix_invoice_item_category', nlapiGetLineItemValue('item', 'custcol_cologix_invoice_item_category', i + 1));
                                objOpty.commitLineItem('item');

                                var currAmount = nlapiGetLineItemValue('item', 'amount', i + 1);
                                var stBS = nlapiGetLineItemText('item', 'class', i + 1);

                                if (currAmount == null || currAmount == ''){
                                    currAmount = 0;
                                }
                                if (stBS.indexOf("Recurring") > -1) {
                                    curMRC = curMRC + parseFloat(currAmount);
                                }
                                else if (stBS.indexOf("NRC") > -1){
                                    curNRC = curNRC + parseFloat(currAmount);
                                }
                                else {

                                }
                                objOpty.setFieldValue('custbody_clgx_total_recurring', curMRC);
                                objOpty.setFieldValue('custbody_clgx_total_non_recurring', curNRC);
                            }
                            var id = nlapiSubmitRecord(objOpty, true, true);
                        }
                    }

                    // Details:	Calculates NRC and MRC totals from items sublist to custom record 'Opportunity Totals'
                    var totals = clgx_transaction_totals (customerid, 'oppty', optyID);

                }

            }
        }

//------------- Begin Section 6 -------------------------------------------------------------------
// Version:	1.0 - 4/24/2012
// Details:	Update loss reason, ordernotes, forecasttype and competitors back on opportunity
//-------------------------------------------------------------------------------------------------

        if (type != 'delete') {

            //var opportunityID = nlapiGetFieldValue('opportunity');
            var proposalid = nlapiGetRecordId();
            var rec = nlapiLoadRecord('estimate', proposalid);
            var opportunityID = rec.getFieldValue('opportunity');

            if(opportunityID != null && opportunityID != ''){

                var recOpportunity = nlapiLoadRecord('opportunity', opportunityID);
                var update = 0;

                var forecastnote = nlapiGetFieldValue('custbody_clgx_custbody_forecastnotes');
                if ((forecastnote != null && forecastnote != '') && (opportunityID != null && opportunityID != '')) {
                    recOpportunity.setFieldValue('custbody_clgx_custbody_forecastnotes', forecastnote);
                    update = 1;
                }

                var forecasttype = nlapiGetFieldValue('forecasttype');
                if ((forecasttype != null && forecasttype != '') && (opportunityID != null && opportunityID != '')) {
                    recOpportunity.setFieldValue('forecasttype', forecasttype);
                    update = 1;
                }

                var lossReason = nlapiGetFieldValue('custbody_clgx_winlossreason');
                if ((lossReason != null && lossReason != '') && (opportunityID != null && opportunityID != '')) {
                    recOpportunity.setFieldValue('winlossreason', lossReason);
                    update = 1;
                }

                var competitorsIDs = new Array();
                competitorsIDs = nlapiGetFieldValues('custbody_clgx_competitors');
                if ((competitorsIDs != null && competitorsIDs != '') && (opportunityID != null && opportunityID != '')) {
                    recOpportunity.setFieldValue('custbody_clgx_competitors', competitorsIDs);
                    update = 1;
                }

                var leadsource = nlapiGetFieldValue('leadsource');
                if (leadsource != null && leadsource != '') {
                    recOpportunity.setFieldValue('leadsource', leadsource);
                    update = 1;
                }

                var expectedclose = nlapiGetFieldValue('expectedclosedate');
                if (expectedclose != null && expectedclose != '') {
                    recOpportunity.setFieldValue('expectedclosedate', expectedclose);
                    update = 1;
                }

                var ordernotes = nlapiGetFieldValue('custbody_clgx_order_notes');
                if (ordernotes != null && ordernotes != '') {
                    recOpportunity.setFieldValue('memo', ordernotes);
                    update = 1;
                }

                if(update == 1){
                    nlapiSubmitRecord(recOpportunity, false, true);
                }

            }
        }

//------------- Begin Section 7 -----------------------------------------------------------------------------------
// Created:	04/23/2015
// Details:	Queue transaction to syncronize with SilverPOP
//-----------------------------------------------------------------------------------------------------------------
        /*
                var currentContext = nlapiGetContext();
                var userid = nlapiGetUser();
                var roleid = currentContext.getRole();
                var transactid = nlapiGetRecordId();
                var inactive = nlapiGetFieldValue('isinactive');
                var json = nlapiGetFieldValue('custbody_clgx_sp_json_sync');

                if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'create' || type == 'xedit' || type == 'edit' || type == 'delete')) {

                    var action = 1;
                    if(type == 'create'){
                        action = 0;
                    }
                    if(type == 'delete'){
                        action = 2;
                    }
                    if(inactive == 'T'){
                        action = 3;
                    }

                    var arrColumns = new Array();
                    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
                    var arrFilters = new Array();
                    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_transact_to_sp_transact",null,"equalto",transactid));
                    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_transact_to_sp_type",null,"equalto",2));
                    arrFilters.push(new nlobjSearchFilter('custrecord_clgx_transact_to_sp_done',null,'is','F'));
                    var searchQueue = nlapiSearchRecord('customrecord_clgx_queue_transact_to_sp', null, arrFilters, arrColumns);

                    if(searchQueue == null){ // add contact to queue if not in it
                        var record = nlapiCreateRecord('customrecord_clgx_queue_transact_to_sp');
                        record.setFieldValue('custrecord_clgx_transact_to_sp_transact', transactid);
                        record.setFieldValue('custrecord_clgx_transact_to_sp_type', 2);
                        record.setFieldValue('custrecord_clgx_transact_to_sp_action', action);
                        record.setFieldValue('custrecord_clgx_transact_to_sp_done', 'F');
                        record.setFieldValue('custrecord_clgx_transact_to_sp_json', json);
                        var idRec = nlapiSubmitRecord(record, false,true);
                    }
                    else{ // if in queue change status in case it was different
                        nlapiSubmitField('customrecord_clgx_queue_transact_to_sp', searchQueue[0].getValue('internalid',null,null), 'custrecord_clgx_transact_to_sp_action', action);
                    }
                }
                */
        //------------- Begin Section 8 -------------------------------------------------------------------
        // Version:	1.0 - 1/12/2012
        // Details:	Create the PDF contract or quote from proposal.
        //-------------------------------------------------------------------------------------------------

        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'edit')) {

            var proposalid = nlapiGetRecordId();
            var contract = nlapiGetFieldValue('custbody_clgx_contract_terms_ready');
            var quote = nlapiGetFieldValue('custbody_clgx_print_budgetray_quote');

            if (quote == 'T' || contract == 'T') {
                if (quote == 'T') {
                    var fields = ['custbody_clgx_prop_bq_file_id','custbody_clgx_transaction_locked'];
                    var values = [null,'T'];
                    nlapiSubmitField('estimate', proposalid, fields, values);
                }
                if (contract == 'T') {
                    var fields = ['custbody_clgx_prop_contract_file_id','custbody_clgx_transaction_locked'];
                    var values = [null,'T'];
                    nlapiSubmitField('estimate', proposalid, fields, values);
                }

                var arrParam = [];
                arrParam['custscript_proposal_pdf_proposalid'] = proposalid;
                arrParam['custscript_proposal_pdf_userid'] = nlapiGetUser();
                arrParam['custscript_proposal_pdf_quote'] = quote;
                arrParam['custscript_proposal_pdf_contract'] = contract;
                var status = nlapiScheduleScript('customscript_clgx_ss_proposal_pdf', null ,arrParam);

                var arrParam = new Array();
                arrParam['custscript_internal_message'] = 'The PDF budgetary quote or contract is being created. Please wait until you have received the confirmation email.';
                nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
            }
        }

        //------------- Begin Section 8 -------------------------------------------------------------------
        // Version:	1.0 - 9/30/2019
        // Details:	Used to copy metered power information from the proposal to the opportunity.
        //-------------------------------------------------------------------------------------------------
        if ((currentContext.getExecutionContext() == "userinterface") && (type == "edit")) {
            var opportunityID = nlapiGetFieldValue("opportunity");
            var recordObject = nlapiLoadRecord("opportunity", opportunityID);
            recordObject.setFieldValue("custbody_clgx_pwusg_meter_power_usage", nlapiGetFieldValue("custbody_clgx_pwusg_meter_power_usage"));
            recordObject.setFieldValue("custbody_clgx_power_type_so", nlapiGetFieldValue("custbody_clgx_power_type_so"));
            recordObject.setFieldValue("custbody_clgx_pwusg_util_rate", nlapiGetFieldValue("custbody_clgx_pwusg_util_rate"));
            recordObject.setFieldValue("custbody_clgx_pwusg_commit_util_kwh", nlapiGetFieldValue("custbody_clgx_pwusg_commit_util_kwh"));
            recordObject.setFieldValue("custbody_clgx_pwusg_commit_util_rate", nlapiGetFieldValue("custbody_clgx_pwusg_commit_util_rate"));
            recordObject.setFieldValue("custbody_clgx_legacy_utilization", nlapiGetFieldValue("custbody_clgx_legacy_utilization"));
            recordObject.setFieldValue("custbody_clgx_item_kw_store", nlapiGetFieldValue("custbody_clgx_item_kw_store"));
            nlapiSubmitRecord(recordObject);
        }

//---------- End Sections  ------------------------------------------------------------------------------------------------
    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}