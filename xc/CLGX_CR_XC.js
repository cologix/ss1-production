nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_XC.js
//	Script Name:	CLGX_CR_XC
//	Script Id:		customscript_clgx_cr_xc
//	Script Runs:	Client
//	Script Type:	Client Script
//	Deployments:	Expense Report
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		9/3/2013
//-------------------------------------------------------------------------------------------------

function saveRecord() {
    try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Details:	Verify if any of the mandatory ports are from the same equipment or unavailable - if yes, display an alert and don't allow saving the record
//-------------------------------------------------------------------------------------------------

        var saveOK = 1;
        var response = '';
        var inactive = nlapiGetFieldValue('isinactive');
        
        var xcType = nlapiGetFieldValue('custrecord_cologix_xc_type');
        if(xcType!=28 && xcType!=29 && xcType!=30 && xcType!=31 && inactive == 'F') {
            var aPort = nlapiGetFieldValue('custrecord_clgx_a_end_port');
            var bPort = nlapiGetFieldValue('custrecord_clgx_b_end_port');
            var cPort = nlapiGetFieldValue('custrecord_clgx_c_end_port');
            var zPort = nlapiGetFieldValue('custrecord_clgx_z_end_port');
            var vlan = nlapiGetFieldValue('custrecord_clgx_xc_vlan');
            var speed = nlapiGetFieldValue('custrecord_clgx_speed');

            var fields = ['custrecord_clgx_active_port_equipment', 'custrecord_clgx_active_port_status']; // same for all 4 switches

            if (aPort != null && aPort != '') {
                var aColumns = nlapiLookupField('customrecord_clgx_active_port', aPort, fields);
                var aPortEquip = aColumns.custrecord_clgx_active_port_equipment;
                var aPortStatus = aColumns.custrecord_clgx_active_port_status;
            } else {
                var aPortStatus = 0;
                aPort = '';
            }

            if (bPort != null && bPort != '') {
                var bColumns = nlapiLookupField('customrecord_clgx_active_port', bPort, fields);
                var bPortEquip = bColumns.custrecord_clgx_active_port_equipment;
                var bPortStatus = bColumns.custrecord_clgx_active_port_status;
            } else {
                var bPortStatus = 0;
                bPort = '';
            }

            if (cPort != null && cPort != '') {
                var cColumns = nlapiLookupField('customrecord_clgx_active_port', cPort, fields);
                var cPortEquip = cColumns.custrecord_clgx_active_port_equipment;
                var cPortStatus = cColumns.custrecord_clgx_active_port_status;
            } else {
                var cPortStatus = 0;
                cPort = '';
            }

            if (zPort != null && zPort != '') {
                var zColumns = nlapiLookupField('customrecord_clgx_active_port', zPort, fields);
                var zPortEquip = zColumns.custrecord_clgx_active_port_equipment;
                var zPortStatus = zColumns.custrecord_clgx_active_port_status;
            } else {
                var zPortStatus = 0;
                zPort = '';
            }

            if (vlan != null && vlan != '') {
                var vFields = ['custrecord_clgx_vlan_status'];
                var vColumns = nlapiLookupField('customrecord_clgx_vlan', vlan, vFields);
                var vPortStatus = vColumns.custrecord_clgx_vlan_status;
            } else {
                var vPortStatus = 0;
                vlan = '';
            }


            /*
             // if any of the configured ports has other status then available, don't save
             if (aPortStatus != 2 && aPortStatus != 0){
             var response = 'The port you choosed for Access Switch 1 is not available. This is an uncommon situation. Please empty your browser cash before trying to configure the switch again. If after doing this you still see this message, please inform your network administrator.';
             var saveOK = 0;
             }
             if (bPortStatus != 2 && bPortStatus != 0){
             var response = 'The port you choosed for Distribution Switch 1 is not available. This is an uncommon situation. Please empty your browser cash before trying to configure the switch again. If after doing this you still see this message, please inform your network administrator.';
             var saveOK = 0;
             }
             if (cPortStatus != 2 && cPortStatus != 0){
             var response = 'The port you choosed for Distribution Switch 2 is not available. This is an uncommon situation. Please empty your browser cash before trying to configure the switch again. If after doing this you still see this message, please inform your network administrator.';
             }
             if (zPortStatus != 2 && zPortStatus != 0){
             var response = 'The port you choosed for Access Switch 2 is not available. This is an uncommon situation. Please empty your browser cash before trying to configure the switch again. If after doing this you still see this message, please inform your network administrator.';
             var saveOK = 0;
             }
             if (vPortStatus != 2 && vPortStatus != 0){
             var response = 'The port you choosed for VLAN is not available. This is an uncommon situation. Please empty your browser cash before trying to configure the switch again. If after doing this you still see this message, please inform your network administrator.';
             var saveOK = 0;
             }
             */


            // 2 hops --------------------------------------------------------------------------------------
            if (xcType == 19 || xcType == 20) {
                if (aPort != '' && zPort != '' && aPort != null && zPort != null) {
                    if (aPortEquip == zPortEquip) {
                        var response = 'All configured switches must be from different equipments.';
                        var saveOK = 0;
                    }
                }
                else {
                    var response = 'Please configure both access switches.';
                    var saveOK = 0;
                }
                if (bPort != '' || cPort != '') {
                    var response = 'Please set type to "4 hop" and remove both distribution switches.';
                    var saveOK = 0;
                }
            }
            // 3 hops --------------------------------------------------------------------------------------
            else if (xcType == 21 || xcType == 23) {
                if (aPort != '' && bPort != '' && zPort != '' && aPort != null && bPort != null && zPort != null) {
                    if (aPortEquip == zPortEquip || bPortEquip == zPortEquip || aPortEquip == bPortEquip) {
                        var response = 'All configured switches must be from different equipments.';
                        var saveOK = 0;
                    }
                }
                else {
                    var response = 'Please configure both access switches and the first distribution switch.';
                    var saveOK = 0;
                }
                if (cPort != '') {
                    var response = 'Please set type to "4 hop" and remove the second distribution switch.';
                    var saveOK = 0;
                }
            }
            // 4 hops --------------------------------------------------------------------------------------
            else if (xcType == 22 || xcType == 24) {
                if (aPort != '' && bPort != '' && cPort != '' && zPort != '' && aPort != null && bPort != null && cPort != null && zPort != null) {
                    if (aPortEquip == zPortEquip || bPortEquip == zPortEquip || cPortEquip == zPortEquip || aPortEquip == bPortEquip || aPortEquip == cPortEquip || bPortEquip == cPortEquip) {
                        var response = 'All configured switches must be from different equipments.';
                        var saveOK = 0;
                    }
                }
                else {
                    var response = 'Please configure both access switches and both distribution switches.';
                    var saveOK = 0;
                }
            }
            // no hops --------------------------------------------------------------------------------------
            else { // xcType == '' || xcType == 17 || xcType == 18
                if ((aPort != '' || bPort != '' || cPort != '' || zPort != '') && (xcType != 32) && (xcType != 25) && (xcType != 26) && (xcType != 27) && (xcType != 33) && (xcType != 34)) {
                    var response = 'Please set type to "4 hop" and remove all switches.';
                    var saveOK = 0;
                }
            }

            // verify Vlan and speed status ----------------------------------------------------------------
            if (xcType == '' || xcType == 17 || xcType == 18) {
                if (vlan != '' || speed != '') {
                    var response = 'Please set type to "4 hop" and remove both VLAN# and Speed.';
                    var saveOK = 0;
                }
            }
            else {
                if ((vlan == '' || speed == '') && (xcType != 32) && (xcType != 26) && (xcType != 27) && (xcType != 17) && (xcType != 18) && (xcType != 25) && (xcType != 34)) {
                    var response = 'Please configure both VLAN# and Speed.';
                    var saveOK = 0;
                }
            }

            // display alert and permit save or not ----------------------------------------------------------------
            if (saveOK == 0) {
                var alert = confirm(response);
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function fieldChanged(type, name, linenum){
    try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 10/09/2013
// Details:	Set market field depending on facility
//-------------------------------------------------------------------------------------------------
        if (name == 'custrecord_clgx_xc_facility') {

            var facility = nlapiGetFieldValue('custrecord_clgx_xc_facility');
            var market = nlapiLookupField('customrecord_cologix_facility', facility, 'custrecord_clgx_facilty_market');
            nlapiSetFieldValue('custrecord_cologix_xc_market',market);

        }
 //------------- Begin Section 2 -------------------------------------------------------------------
 // Version:	1.0 - 1/3/2018
 // Details:	If inactivated null all ports
 //-------------------------------------------------------------------------------------------------
		 if (name == 'isinactive') {
		 	var inactive = nlapiGetFieldValue('isinactive');
		 	if(inactive == 'T'){
		 		nlapiSetFieldValue('custrecord_clgx_a_end_port',null);
		 		nlapiSetFieldValue('custrecord_clgx_b_end_port',null);
		 		nlapiSetFieldValue('custrecord_clgx_c_end_port',null);
		 		nlapiSetFieldValue('custrecord_clgx_z_end_port',null);
		 		nlapiSetFieldValue('custrecord_clgx_xc_vlan',null);
		 		nlapiSetFieldValue('custrecord_clgx_speed','');
		 	}
		 	
		 }
//------------- End Section 1 -------------------------------------------------------------------
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


