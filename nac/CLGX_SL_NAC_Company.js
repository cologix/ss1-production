//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Test.js
//	Script Name:	CLGX_SL_Test
//	Script Id:		customscript_clgx_sl_test
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/11/2013
//-------------------------------------------------------------------------------------------------

function suitelet_test (request, response){
    try {

    	var entityid = 243468;
    	
        var recCompany = nlapiLoadRecord('customer', entityid);
        var internalid = recCompany.getFieldValue('id');
        
		var objCompany = new Object();
		
		objCompany["action"] = 'create';
		
		objCompany["entitynacid"] = 123456789;
		objCompany["entityid"] = parseInt(recCompany.getFieldValue('id'));
		objCompany["entity"] = recCompany.getFieldValue('entityid');
		
		objCompany["statusid"] = parseInt(recCompany.getFieldValue('entitystatus'));
		objCompany["status"] = recCompany.getFieldText('entitystatus');
		
		objCompany["salesrepid"] = parseInt(recCompany.getFieldValue('salesrep'));
		objCompany["salesrep"] = recCompany.getFieldText('salesrep');
		
		objCompany["currencyid"] = parseInt(recCompany.getFieldValue('currency'));
		objCompany["currency"] = recCompany.getFieldText('currency');
		
		objCompany["termsid"] = parseInt(recCompany.getFieldValue('terms'));
		objCompany["terms"] = recCompany.getFieldText('terms');
		
		var nbrAddresses = recCompany.getLineItemCount('addressbook');
		var arrAddresses = new Array();
		for ( var i = 0;  i < nbrAddresses; i++ ) {
			var objAddress = new Object();
			objAddress["addr1"] = recCompany.getLineItemValue('addressbook','addr1', i + 1);
			objAddress["addr2"] = recCompany.getLineItemValue('addressbook','addr2', i + 1);
			objAddress["addr3"] = recCompany.getLineItemValue('addressbook','addr3', i + 1);
			objAddress["city"] = recCompany.getLineItemValue('addressbook','city', i + 1);
			objAddress["state"] = recCompany.getLineItemValue('addressbook','state', i + 1);
			objAddress["zip"] = recCompany.getLineItemValue('addressbook','zip', i + 1);
			objAddress["phone"] = recCompany.getLineItemValue('addressbook','phone', i + 1);
			arrAddresses.push(objAddress);
		}
		objCompany["addresses"] = arrAddresses;
		
		
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,null));
		arrColumns.push(new nlobjSearchColumn('entityid',null,null));
		arrColumns.push(new nlobjSearchColumn('email',null,null));
		arrColumns.push(new nlobjSearchColumn('phone',null,null));
		arrColumns.push(new nlobjSearchColumn('mobilephone',null,null));
		arrColumns.push(new nlobjSearchColumn('custentity_cologix_esc_seq',null,null));
		arrColumns.push(new nlobjSearchColumn('custentity_clgx_portal_password',null,null));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("company",null,"anyof",entityid));
		var searchContacts = nlapiSearchRecord('contact', null, arrFilters, arrColumns);
		var arrContacts = new Array();
		for ( var i = 0; searchContacts != null && i < searchContacts.length; i++ ) {
			var objContact = new Object();
			objContact["contactnacid"] = 123456789;
			objContact["contactid"] = parseInt(searchContacts[i].getValue('internalid',null,null));
			objContact["contact"] = searchContacts[i].getValue('entityid',null,null);
			objContact["roleid"] = parseInt(searchContacts[i].getValue('contactrole',null,null));
			objContact["role"] = searchContacts[i].getText('contactrole',null,null);
			objContact["email"] = searchContacts[i].getValue('email',null,null);
			objContact["phone"] = searchContacts[i].getValue('phone',null,null);
			objContact["mobile"] = searchContacts[i].getValue('mobilephone',null,null);
			objContact["escalation"] = searchContacts[i].getValue('custentity_cologix_esc_seq',null,null);
			//objContact["password"] = searchContacts[i].getValue('custentity_clgx_portal_password',null,null);
			arrContacts.push(objContact);
		}
		objCompany["contacts"] = arrContacts;
		
		
		/*
		var nbrContacts = recCompany.getLineItemCount('contactroles');
		var arrContacts = new Array();
		for ( var i = 0;  i < nbrContacts; i++ ) {
			var objContact = new Object();
			objContact["contactid"] = parseInt(recCompany.getLineItemValue('contactroles','contact', i + 1));
			objContact["contact"] = recCompany.getLineItemValue('contactroles','contactname', i + 1);
			objContact["roleid"] = parseInt(recCompany.getLineItemValue('contactroles','role', i + 1));
			objContact["role"] = recCompany.getLineItemText('contactroles','role', i + 1);
			objContact["email"] = recCompany.getLineItemValue('contactroles','email', i + 1);
			arrContacts.push(objContact);
		}
		objCompany["contacts"] = arrContacts;
		*/
		
        response.write(JSON.stringify(objCompany));
        
        //response.write('<br/><br/>');
 		//response.write(JSON.stringify(rec));
    	
 		//response.write('<br/><br/>');
		//response.write(nlapiGetContext().getRemainingUsage());
		//response.write('<br/><br/>');
		//response.write(loops);
		
		
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}




function get_ops_reps (arrFacilitiesIDs){

    var arrOpsReps = new Array();
    for ( var i = 0; i < arrFacilitiesIDs.length; i++ ) {

        if(arrFacilitiesIDs[i] == 2 || arrFacilitiesIDs[i] == 4 || arrFacilitiesIDs[i] == 5 || arrFacilitiesIDs[i] == 6 || arrFacilitiesIDs[i] == 7 || arrFacilitiesIDs[i] == 9 || arrFacilitiesIDs[i] == 19){
                
            var objOpsRep = new Object();
            objOpsRep["opid"] = 168983;
            objOpsRep["op"] = 'Donato Di Rienzo';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 3118;
            objOpsRep["op"] = 'Sorin Arion';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 339723;
            objOpsRep["op"] = 'Rochelle Price';
            arrOpsReps.push(objOpsRep);

        }

        else{

            var objOpsRep = new Object();
            objOpsRep["opid"] = 212518;
            objOpsRep["op"] = 'Amy J Connell';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 9735;
            objOpsRep["op"] = 'Pauli Guild';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 339723;
            objOpsRep["op"] = 'Rochelle Price';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 8795;
            objOpsRep["op"] = 'Mario Novello';
            arrOpsReps.push(objOpsRep);

            //var objOpsRep = new Object();
            //objOpsRep["opid"] = 211767;
            //objOpsRep["op"] = 'Vlad Thiel';
            //arrOpsReps.push(objOpsRep);

        }
    }

    return arrOpsReps;
}
