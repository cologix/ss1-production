nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_NAC_FileExport.js
//	Script Name:	CLGX_RL_NAC_FileExport
//	Script Id:		customscript_clgx_rl_nac_file_export
//	Script Runs:	On Server
//	Script Type:	Restlet
//	@authors:		Dan Cech - dan.cech@cologix.com
//	Created:		12/11/2015
//-------------------------------------------------------------------------------------------------

var get = wrap(function get(datain) {
	var id = datain['id'];
	var include_data = datain.hasOwnProperty('data') && datain['data'];
	
	var file = nlapiLoadFile(id);
	
	var folder = nlapiLoadRecord('folder', file.getFolder());
	var path = folder.getFieldValue('name');
	
	var fld = null;
	var pid = folder.getFieldValue('parent');
	
	while (pid) {
		fld = nlapiLoadRecord('folder', pid);
		path = fld.getFieldValue('name') + '/' + path;
		pid = fld.getFieldValue('parent');
	}
	
	var out = {
		'id': file.getId(),
		'url': file.getURL(),
		'name': (file.getName()).replace(/,/g, ""),
		'folder_id': file.getFolder(),
		'folder_name': folder.getFieldValue('name'),
		'path': path,
		'description': file.getDescription(),
		'size': file.getSize(),
		'type': file.getType(),
		'inactive': file.isInactive(),
		'online': file.isOnline()
	};
	
	if (include_data) {
		out['data'] = file.getValue();
	}
	
	return out;
});
