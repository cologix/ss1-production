nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_NAC_Customers_Sync.js
//	Script Name:	CLGX_RL_NAC_Customers_Sync
//	Script Id:		customscript_clgx_rl_nac_customer_sync
//	Script Type:	Restlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/23/2016
//-------------------------------------------------------------------------------------------------

var get = wrap(function get(datain) {
	
	var recordtype = datain['type'];
	
	// get an individual record
	if (datain['id'] && !datain['criteria']) {
		var id = datain['id'];
		
		var row = nlapiLoadRecord(recordtype, id);
		
		var ret = json_serialize(row);
		
		return ret;
	}

	// get a list of records
	if (datain['criteria']) {
		var criteria = datain['criteria'];
	}
	else{
		var criteria = 'equalto';
	}
	
	var filters = new Array();
	for (var i in datain) {
		if (i == 'type' || i == 'criteria' ) {
			continue;
		}
		filters.push(new nlobjSearchFilter(i, null, criteria, datain[i]));
	}
	
	var cntfilters = filters.length;
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid').setSort(false /* bsortascending */);
	
	var rows = new Array();
	
	while (true) {
		res = nlapiSearchRecord(recordtype, null, filters, columns);
		if (!res) {
			break;
		}
		
		for (var i in res) {
			if (res[i].getRecordType() != recordtype) {
				continue;
			}
			
			rows.push({
				'id': res[i].getId(),
				'recordtype': res[i].getRecordType()
			});
		}
		
		if (res.length < 1000) {
			break;
		}
		
		var lastId = res[999].getId();
		filters[cntfilters] = new nlobjSearchFilter("internalIdNumber", null, "greaterThan", lastId);
	}
	
	return rows;
});

var post = wrap(function post(datain) {
	var recordtype = datain['type'];
	
	// get an individual record
	if (datain['id']) {
		var record = nlapiLoadRecord(recordtype, datain['id']);
	} else {
		var record = nlapiCreateRecord(recordtype);
	}
	
	var old = {};
	var updated = {};
	for (var k in datain) {
		if (k == 'id' || k == 'type') {
			continue;
		}
		
		old[k] = record.getFieldValue(k);
		if (datain[k] != old[k]) {
			record.setFieldValue(k, datain[k]);
			updated[k] = datain[k];
		}
	}
	
	//var id = nlapiSubmitRecord(record, true);
	var id = nlapiSubmitRecord(record, false, true);
	
	return {
		'id': id,
		'old': old,
		'new': updated
	};
});
