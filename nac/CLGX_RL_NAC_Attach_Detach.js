nlapiLogExecution("audit","FLOStart",new Date().getTime());
//----------------------------------------------------
//	Script:		CLGX_RL_NAC_Attach_Detach.js
//	ScriptID:	customscript_clgx_rl_nac_attach_detach
//	ScriptType:	RESTlet
//	@author:	Dan Tansanu - dan.tansanu@cologix.com
//	Created:	03/28/2016
//----------------------------------------------------

var put = wrap(function put(datain) {
	
	var act = datain['act'];
	var customer = datain['customer'];
	var contact = datain['contact'];
	var role = datain['role'];
	
	if(act== 'attach'){
		nlapiAttachRecord('contact', contact, 'customer', customer, {"role": role});
	} 
	else if (act == 'detach'){
		nlapiDetachRecord('contact', contact, 'customer', customer);
	}
	
	return true;

});
