//-------------------------------------------------------------------------------------------------
//	Script:		CLGX_RL_NAC_Library.js
//	@authors:	Dan Cech - dan.cech@cologix.com
//	Created:	1/14/2016
//-------------------------------------------------------------------------------------------------

function wrap(func) {
	return function (datain) {
		try {
			var startusage = parseInt(nlapiGetContext().getRemainingUsage());
			var starttime = new Date().getTime();
			
			var is_json = (datain.constructor.name == 'Object');
			
			if (!is_json) {
				datain = JSON.parse(datain);
			}
			
			var ret = func(datain);
			
			var usage = startusage - parseInt(nlapiGetContext().getRemainingUsage());
			var ts = (new Date().getTime() - starttime) / 1000;
			nlapiLogExecution('DEBUG', func.name + ' Usage = ' + usage + ' Time = ' + ts, JSON.stringify(datain));
			
			return is_json ? ret : JSON.stringify(ret);
		} catch (error) {
			var code = 'UNEXPECTED_ERROR';
			var msg = '';
			
			if (error.getDetails != undefined) {
				code = error.getCode();
				msg = error.getDetails();
			} else {
				msg = error.toString();
				throw error;
			}
			
			// nlapiLogExecution('ERROR', code, msg);
			throw nlapiCreateError(code, msg, false);
		}
	}
}

function flexapi(method, url, data) {
	// custrecord_clgx_queue_ping_record_type
	// 1: Customer
	// 2: Contact
	// 3: Service Order
	// 4: Consolidated Invoice
	// 5: FAM Asset
	// 6: Employee
	// 7: 
	//nlapiLogExecution('DEBUG', 'flexapi request: ' + method + ' ' + url, JSON.stringify(data));
	
	var requestURL = nlapiRequestURL(
		'https://nsapi1.dev.nac.net/v1.0' + url,
		data ? JSON.stringify(data) : null,
		{
			'Content-type': 'application/json',
			'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
		},
		null,
		method);
	
	var strResponse = requestURL.body;
	
	nlapiLogExecution('DEBUG', 'flexapi response: ' + method + ' ' + url, strResponse);
	
	var objResponse = JSON.parse(strResponse);
	return objResponse;
}

function json_serialize(row) {
	var ret = JSON.parse(JSON.stringify(row));
	
	if (ret['addressbook']) {
		for (var i = 0; i < ret['addressbook'].length; i++) {
			ret['addressbook'][i]['id'] = row.getLineItemValue('addressbook', 'id', i + 1)
		}
	}
	
	return ret;
}

function typeOf(obj) {
	var type = typeof obj;
	return type === "object" && !obj ? "null" : type;
}

function introspect(name, obj, levels, indent) {
	indent = indent || "";
	if (typeOf(levels) !== "number") {
		levels = 1;
	}
	
	var objType = typeOf(obj);
	
	var result = [indent, name, " ", objType, " :"].join('');
	var prop;
	
	if (objType === "object") {
		if (levels <= 0) {
			return [result, " ..."].join('');
		}
		
		indent = [indent, "  "].join('');
		for (var p in obj) {
			prop = p;
			// prop = obj[p];
			// prop = introspect(p, obj[p], levels - 1, indent);
			result = [result, "\n", prop].join('');
		}
		return result;
	}
	
	if (objType === "null") {
		return [result, " null"].join('');
	}
	
	return [result, " ", obj].join('');
}

function append_attached_contacts(ret, id) {
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('entityid',null,null));
	arrColumns.push(new nlobjSearchColumn('contactrole',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('internalid','customer','is',id));
	arrFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	var searchContacts = nlapiSearchRecord('contact', null, arrFilters, arrColumns);
	if(searchContacts){
		ret["attached"] = searchContacts;
	} else {
		ret["attached"] = [];
	}
	return ret;
}


function append_attached_companies(ret,id,companyid) {
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('contactrole',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('internalid',null,'is',id));
	if(companyid){
		arrFilters.push(new nlobjSearchFilter('internalid','company','is',companyid));
	}
	arrFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	arrFilters.push(new nlobjSearchFilter('isinactive','customer','is','F'));
	var searchRole = nlapiSearchRecord('contact', null, arrFilters, arrColumns);
	
	if(searchRole){
		var json = JSON.parse(JSON.stringify(searchRole));
		ret["contactrole"] = json[0].columns.contactrole;
	} else{
		ret["contactrole"] = null;
	}
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid', 'company', null));
	arrColumns.push(new nlobjSearchColumn('companyname', 'company', null));
	arrColumns.push(new nlobjSearchColumn('contactrole',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('internalid',null,'is',id));
	arrFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	arrFilters.push(new nlobjSearchFilter('isinactive','customer','is','F'));
	var searchCompanies = nlapiSearchRecord('contact', null, arrFilters, arrColumns);
	
	var searchCompanies = JSON.parse(JSON.stringify(searchCompanies));
	
	if(searchCompanies){
		ret["attached"] = searchCompanies;
	} else {
		return ret;
		//ret["attached"] = []
	}
	return ret;
}


function append_services_powers(ret, id, recordtype) {
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_service',null,null).setSort(false));
	arrColumns.push(new nlobjSearchColumn('custentity_clgx_matrix_service_id','custrecord_cologix_power_service',null));
	arrColumns.push(new nlobjSearchColumn('internalid',null,null).setSort(false));
	arrColumns.push(new nlobjSearchColumn('name',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_power_matrix_id',null,null));
	var arrFilters = new Array();
	if(recordtype == 'salesorder'){
		arrFilters.push(new nlobjSearchFilter("custrecord_power_circuit_service_order",null,"is",id));
	}
	else { //job
		arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_service",null,"is",id));
	}
	arrFilters.push(new nlobjSearchFilter('isinactive',null,'is','F'));
	var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', null, arrFilters, arrColumns);
	
	var arrPowers = new Array();
	for ( var i = 0; searchPowers != null && i < searchPowers.length; i++ ) {
		var row = nlapiLoadRecord(searchPowers[i].getRecordType(), searchPowers[i].getId());
		arrPowers.push(row);
	}
	
	/*
	var arrPowers = new Array();
	for ( var i = 0; searchPowers != null && i < searchPowers.length; i++ ) {
		var objPower = new Object();
		//objPower["service_id"] = parseInt(searchPowers[i].getValue('custrecord_cologix_power_service',null,null));
		//objPower["service"] = searchPowers[i].getText('custrecord_cologix_power_service',null,null);
		//objPower["service_matrix_id"] = searchPowers[i].getValue('custentity_clgx_matrix_service_id','custrecord_cologix_power_service',null);
		objPower["id"] = parseInt(searchPowers[i].getValue('internalid',null,null));
		objPower["name"] = searchPowers[i].getValue('name',null,null);
		objPower["custrecord_clgx_power_matrix_id"] = searchPowers[i].getValue('custrecord_clgx_power_matrix_id',null,null);
		arrPowers.push(objPower);
	}
	*/
	
	
	/*
	var arrServicesIDs = _.compact(_.uniq(_.pluck(arrPowers, 'service_id')));
	
	
	var arrServices = new Array();
	for ( var i = 0; arrServicesIDs != null && i < arrServicesIDs.length; i++ ) {
		var arrServPwrs = _.filter(arrPowers, function(arr){
			return (arr.service_id == arrServicesIDs[i]);
		});
		var objService = new Object();
		objService["service_id"] = arrServPwrs[0].service_id;
		objService["service"] = arrServPwrs[0].service;
		objService["service_matrix_id"] = arrServPwrs[0].service_matrix_id;
		
		var arrPwrs = new Array();
		for ( var j = 0; arrServPwrs != null && j < arrServPwrs.length; j++ ) {
			var objPwr = new Object();
			objPwr["id"] = arrServPwrs[j].id;
			objPwr["name"] = arrServPwrs[j].name;
			objPwr["custrecord_clgx_power_matrix_id"] = arrServPwrs[j].custrecord_clgx_power_matrix_id;
			arrPwrs.push(objPwr);
		}
		objService["powers"] = arrPwrs;
		arrServices.push(objService);
	}
	
	ret["services_powers"] = arrServices;
	*/
	ret["services_powers"] = arrPowers;
		
	return ret;

}