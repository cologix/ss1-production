//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_NAC_Test.js
//	Script Name:	CLGX_RL_NAC_Test
//	Script Id:		customscript_clgx_rl_nac_test
//	Script Runs:	On Server
//	Script Type:	Restlet
//	@authors:		Dan Cech - dan.cech@cologix.com
//	Created:		12/1/2015
//-------------------------------------------------------------------------------------------------

function get (datain) {
    try {
    	var is_json = (datain.constructor.name == 'Object');
    	if (!is_json) {
    		datain = JSON.parse(datain);
    	}
    	    	
    	var recordtype = datain['type'];
    	
    	// get an individual record
    	if (datain['id']) {
        	var id = datain['id'];
        	
            var record = nlapiLoadRecord(recordtype, id);
            
            return is_json ? record : JSON.stringify(record);
    	}
    	
    	// get a list of records
    	var filters = new Array();
    	for (var i in datain) {
    		if (i == 'id' || i == 'type') {
    			continue;
    		}
    		filters.push(new nlobjSearchFilter(i, null, "is", datain[i], null));
    	}
    	
    	var cntfilters = filters.length;
    	
    	var columns = new Array();
    	columns[0] = new nlobjSearchColumn('internalid').setSort(false /* bsortascending */);
    	
    	var rows = new Array();
    	
    	while (true) {
    		res = nlapiSearchRecord(recordtype, null, filters, columns);
    		if (!res) {
    			break;
    		}
	    	
    		for (var i in res) {
    			if (res[i].getRecordType() != recordtype) {
    				continue;
    			}
    			
        		rows.push({
        			'id': res[i].getId(),
        			'recordtype': res[i].getRecordType()
        		});
    		}
    		
    		if (res.length < 1000) {
    			break;
    		}
    		
	        var lastId = res[999].getId();
	        filters[cntfilters] = new nlobjSearchFilter("internalIdNumber", null, "greaterThan", lastId);
    	}
    	
    	return is_json ? rows : JSON.stringify(rows);
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function post (datain) {
    try {
    	var is_json = (datain.constructor.name == 'Object');
    	if (!is_json) {
    		datain = JSON.parse(datain);
    	}
    	    	
    	var recordtype = datain['type'];
    	
    	// get an individual record
    	if (datain['id']) {
            var record = nlapiLoadRecord(recordtype, datain['id']);
    	} else {
    		var record = nlapiCreateRecord(recordtype);
    	}
    	
    	var old = {};
    	var updated = {};
    	for (var k in datain) {
    		if (k == 'id' || k == 'type') {
    			continue;
    		}
    		
    		old[k] = record.getFieldValue(k);
    		if (datain[k] != old[k]) {
    			record.setFieldValue(k, datain[k]);
    			updated[k] = datain[k];
    		}
    	}
    	
		var id = nlapiSubmitRecord(record, true);
		var ret = {
			'id': id,
			'old': old,
			'new': updated
		};
		return is_json ? ret : JSON.stringify(ret);
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
