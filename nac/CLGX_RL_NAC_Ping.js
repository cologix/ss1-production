function NNJ_Ping(record_type, record_id, event_type) {
	try{
		var currentContext = nlapiGetContext();
        var environment = currentContext.getEnvironment();
		
		if (environment == 'PRODUCTION' && (type == 'xedit'  || currentContext.getExecutionContext() == 'userinterface')) {	
			var row = nlapiLoadRecord(record_type, record_id);
			var ret = null;
			
			if(recordtype == 'customer'){
				ret = json_serialize(row)
		        ret = append_attached_contacts (ret, record_id);
		        
		        
			} else if(recordtype == 'contact'){
				var companyid = row.getFieldValue('company');
				var arrColumns = new Array();
				var arrFilters = new Array();
				arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
				var searchSOs = nlapiSearchRecord('salesorder', 'customsearch_clgx_nj_sos_check', arrFilters, arrColumns);
				if(searchSOs != null){
					ret = json_serialize(row);
					var companyid = row.getFieldValue('company');
					ret = append_attached_companies(ret, record_id, companyid);
				}
				
			} else if(recordtype == 'salesorder'){
				var clocation = nlapiGetFieldValue('custbody_clgx_consolidate_locations');
				if(clocation == 29) {
					var row = nlapiLoadRecord('salesorder', record_id);
					var ret = json_serialize(row);
					ret = append_services_powers (ret, soid, 'salesorder');
					
					var second_id = row.getFieldValue('entity');
					if (second_id >= 1) {
						NNJ_Ping 'customer', second_id, event_type;
					}
				}
			} else if (recordtype == 'job') {
				var facility = row.getFieldValue('custentity_cologix_facility');
				if (facility == 33 || facility == 34 || facility == 35 || facility == 36) {
					ret = json_serialize(row)
					ret = append_services_powers (ret, soid, 'salesorder');

					var second_id = row.getFieldValue('salesorder', soid);
					NNJ_Ping 'salesorder', second_id, event_type;
				}
			} else if(recordtype == 'customrecord_clgx_consolidated_invoices'){
				ret = json_serialize(row)
				
			} else if(recordtype == 'customrecord_ncfar_asset'){
				var facility = row.getFieldValue('custrecord_assetfacility');
				if (facility == 33 || facility == 34 || facility == 35 || facility == 36) {
					var ret = json_serialize(row);
				}
			
			} else if(recordtype == 'customrecord_clgx_power_circuit'){
				var facility = row.getFieldValue('custrecord_cologix_power_facility');
				if (facility == 33 || facility == 34 || facility == 35 || facility == 36) {
					var ret = json_serialize(row);
					
			        var second_id = row.getFieldValue('custrecord_power_circuit_service_order');
					if (second_id >= 1) {
						NNJ_Ping 'salesorder', second_id, event_type;
					}
				}
			
			} else if(recordtype == 'employee'){
				ret = json_serialize(row)
				
			} else if(recordtype == 'customrecord_cologix_facility'){
				ret = json_serialize(row)
				
			} else if(recordtype == 'customrecord_cologix_space'){
				var facility = row.getFieldValue('custrecord_cologix_space_location');
				if (facility == 33 || facility == 34 || facility == 35 || facility == 36) {
					var ret = json_serialize(row);
					var second_id = row.getFieldValue('custrecord_space_service_order');
					if (second_id >= 1) {
						NNJ_Ping 'salesorder', second_id, event_type;
					}
				}
			} else if(recordtype == 'customrecord_cologix_crossconnect'){
				var facility = nlapiGetFieldValue('custrecord_clgx_xc_facility');
				if (facility == 33 || facility == 34 || facility == 35 || facility == 36) {
					var ret = json_serialize(row);
					var second_id = row.getFieldValue('custrecord_xconnect_service_order');
					if (second_id >= 1) {
						NNJ_Ping 'salesorder', second_id, event_type
					}
				}
			}
			
			
			if (ret != null) {
				try {
					flexapi('POST','/netsuite/update', {
						'type': record_type,
						'id': record_id,
						'action': event_type,
						'userid': nlapiGetUser(),
						'user': nlapiLookupField('employee', nlapiGetUser(), 'entityid'),
						'data': ret
					});
				}
				catch (error) {
					nlapiLogExecution('DEBUG', 'flexapi error: ', error);
					var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
					record.setFieldValue('custrecord_clgx_queue_ping_record_id', record_id);
					record.setFieldValue('custrecord_clgx_queue_ping_record_type', 0);
					record.setFieldValue('custrecord_clgx_queue_ping_type_name', record_type);
					record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
					var idRec = nlapiSubmitRecord(record, false,true);
				}
			}
		}
	} catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	}
}
