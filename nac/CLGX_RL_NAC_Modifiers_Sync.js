nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_NAC_Modifiers_Sync.js
//	Script Name:	CLGX_RL_NAC_Modifiers_Sync
//	Script Id:		customscript_clgx_rl_nac_modif_sync
//	Script Runs:	On Server
//	Script Type:	Restlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		18/1/2015
//-------------------------------------------------------------------------------------------------

var get = wrap(function get(datain) {
	var recordtype = datain['type'];
	
	// get an individual record
	if (datain['id'] && recordtype == 'customrecord_clgx_modifier') {
		var id = datain['id'];
		
		var row = nlapiLoadRecord('customrecord_clgx_modifier', id);
		var ret = json_serialize(row);
		
		return ret;
	}
	
	var rows = new Array();
	
	if(recordtype == 'customer' || recordtype == 'contact'){
		var filters = new Array();
		if(recordtype == 'customer'){
			filters.push(new nlobjSearchFilter('custrecord_clgx_modifier_customer', null, "anyof", datain['id']));
		}
		else{
			filters.push(new nlobjSearchFilter('custrecord_clgx_modifier_contact', null, "anyof", datain['id']));
		}
		
		var cntfilters = filters.length;
		
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('internalid').setSort(false /* bsortascending */);
		
		while (true) {
			res = nlapiSearchRecord('customrecord_clgx_modifier', null, filters, columns);
			if (!res) {
				break;
			}
			
			for (var i in res) {
				if (res[i].getRecordType() != 'customrecord_clgx_modifier') {
					continue;
				}
				
				rows.push({
					'id': res[i].getId(),
					'recordtype': res[i].getRecordType()
				});
			}
			
			if (res.length < 1000) {
				break;
			}
			
			var lastId = res[999].getId();
			filters[cntfilters] = new nlobjSearchFilter("internalIdNumber", null, "greaterThan", lastId);
		}
	
	}
	
	return rows;
});

