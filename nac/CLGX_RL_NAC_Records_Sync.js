nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_NAC_Records_Sync.js
//	Script Name:	CLGX_RL_NAC_Records_Sync
//	Script Id:		customscript_clgx_rl_nac_rec_sync
//	Script Runs:	On Server
//	Script Type:	Restlet
//	@authors:		ryan.pavely@cologix.com
//	Created:		12/1/2015
//-------------------------------------------------------------------------------------------------

var get = wrap(function get(datain) {
    try{

        var recordtype = datain['type'];

        // get an individual record
        if (datain['id'] && !datain['criteria']) {

            var id = datain['id'];

            var row = nlapiLoadRecord(recordtype, id);

            var ret = json_serialize(row);

            if(recordtype == 'customer'){

                ret = append_attached_contacts (ret, id);

            }
            else if(recordtype == 'contact'){

                var companyid = row.getFieldValue('company');
                ret = append_attached_companies(ret, id, companyid);

            } else if (recordtype == 'salesorder' || recordtype == 'job') {

                ret = append_services_powers (ret, id, recordtype);

            } else {}

            // ret['introspect'] = introspect(recordtype, row);
            // ret['rawdata'] = row.rawData;

            return ret;
        }

        // get a list of records
        if (datain['criteria']) {
            var criteria = datain['criteria'];
        }
        else{
            var criteria = 'equalto';
        }

        var filters = new Array();
        for (var i in datain) {
            if (i == 'type' || i == 'criteria' ) {
                continue;
            }
            filters.push(new nlobjSearchFilter(i, null, criteria, datain[i]));
        }

        var cntfilters = filters.length;

        var columns = new Array();
        columns[0] = new nlobjSearchColumn('internalid').setSort(false /* bsortascending */);

        var rows = new Array();

        while (true) {
            res = nlapiSearchRecord(recordtype, null, filters, columns);
            if (!res) {
                break;
            }

            for (var i in res) {
                if (res[i].getRecordType() != recordtype) {
                    continue;
                }

                rows.push({
                    'id': res[i].getId(),
                    'recordtype': res[i].getRecordType()
                });
            }

            if (res.length < 1000) {
                break;
            }

            var lastId = res[999].getId();
            filters[cntfilters] = new nlobjSearchFilter("internalIdNumber", null, "greaterThan", lastId);
        }

        return rows;
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            return error.getCode() + ': ' + error.getDetails();
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            return error.getCode() + ': ' + error.getDetails();
            throw nlapiCreateError('99999', error.toString());
        }
    }
});


var post = wrap(function post(datain) {
    try{

        var recordtype = datain['type'];

        // get an individual record
        if (datain['id']) {
            var record = nlapiLoadRecord(recordtype, datain['id']);
        } else {
            var record = nlapiCreateRecord(recordtype);
        }
        if(_.has(datain, 'assigned')) {
            if (datain['assigned'] == 600792) {
                datain['assigned'] =713181;
            }
            if(datain['email']=='laura.lore@cologix.com'){
                datain['email']='chad.cummins@cologix.com';
           }
        }

        var old = {};
        var updated = {};
        for (var k in datain) {
            if (k == 'id' || k == 'type') {
                continue;
            }

            old[k] = record.getFieldValue(k);
            if (datain[k] != old[k]) {
                record.setFieldValue(k, datain[k]);
                updated[k] = datain[k];
            }
        }

        //var id = nlapiSubmitRecord(record, true);
        var id = nlapiSubmitRecord(record, false, true);

        return {
            'id': id,
            'old': old,
            'new': updated
        };
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            return error.getCode() + ': ' + error.getDetails();
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            return error.getCode() + ': ' + error.getDetails();
            throw nlapiCreateError('99999', error.toString());
        }
    }
});

function deletepost(datain) {
    try{
        var recordtype = datain['type'];
        nlapiDeleteRecord(recordtype, datain['id']);

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            return error.getCode() + ': ' + error.getDetails();
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            return error.getCode() + ': ' + error.getDetails();
            throw nlapiCreateError('99999', error.toString());
        }
    }
};

