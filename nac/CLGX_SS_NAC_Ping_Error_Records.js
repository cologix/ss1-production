nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_NAC_Ping_Error_Records.js
//	Script Name:	CLGX_SS_NAC_Ping_Error_Records
//	Script Id:		customscript_clgx_ss_nac_ping_errors
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		03/03/2016
//-------------------------------------------------------------------------------------------------
function scheduled_clgx_ss_nac_ping_errors(){ 
    try{
    	
    	var currentContext = nlapiGetContext();
    	var environment = currentContext.getEnvironment();
    	
    	if (environment == 'PRODUCTION') {

	    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
	        
	        var context = nlapiGetContext();
	        var initialTime = moment();
	
	        var searchResults = nlapiSearchRecord('customrecord_clgx_queue_ping_matrix', 'customsearch_clgx_queue_ping_matrix');
	
	        //for ( var i = 0; searchResults != null && i < 10; i++ ) {
	        for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
	        	
	        	var startExecTime = moment();
	        	var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);
	        	
	            if ( context.getRemainingUsage() <= 1000 || totalMinutes > 50 || i > 990 ){
	            	var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
	                break; 
	            }
	            
	            var internalid = parseInt(searchResults[i].getValue('internalid',null,null));
	            var recordid = parseInt(searchResults[i].getValue('custrecord_clgx_queue_ping_record_id',null,null));
	            var recordtype = parseInt(searchResults[i].getValue('custrecord_clgx_queue_ping_record_type',null,null));
	            
	            if(recordtype == 1){
	            	var record = 'customer';
	            }
	            else if(recordtype == 2){
	            	var record = 'contact';
	            }
	            else if(recordtype == 3){
	            	var record = 'salesorder';
	            }
	            //else if(recordtype == 4){
	            //	var record = 'customrecord_clgx_consolidated_invoices';
	            //}
	            else if(recordtype == 5){
	            	var record = 'customrecord_ncfar_asset';
	            }
	            else if(recordtype == 6){
	            	var record = 'customrecord_clgx_power_circuit';
	            }
	            else if(recordtype == 7){
	            	var record = 'employee';
	            }
	            else if(recordtype == 8){
	            	var record = 'customrecord_cologix_facility';
	            }
	            else if(recordtype == 9){
	            	var record = 'customrecord_cologix_space';
	            }
	            else if(recordtype == 10){
	            	var record = 'customrecord_cologix_crossconnect';
	            }
	            else{
	            	nlapiSubmitField('customrecord_clgx_queue_ping_matrix', internalid, 'custrecord_clgx_queue_ping_processed', 'T');
	            	return;
	            }
	            
	            var usage = 10000 - parseInt(context.getRemainingUsage());
	            var index = i + 1;
	            nlapiLogExecution('DEBUG','Results ', ' | Record = ' +  index + '/'+ searchResults.length + ' | Record = '+ record + ' | ID = '+ recordid + ' | Usage = '+ usage + '  |');
	            
	            // If it does not exist, skip but mark complete.
	            try {
	            	var row = nlapiLoadRecord(record, recordid);
	            	
					try {
						/*
						flexapi('POST','/netsuite/update', {
							'type': record,
							'id': recordid,
							'action': 'edit',
							'userid': 0,
							'user': 'system',
							'data': json_serialize(row)
						});
						*/
						
				    	var data = {
								'type': record,
								'id': recordid,
								'action': 'edit',
								'userid': 0,
								'user': 'system',
								'data': json_serialize(row)
							}
						
						var requestURL = nlapiRequestURL(
								'https://nsapi1.dev.nac.net/v1.0/netsuite/update',
								data ? JSON.stringify(data) : null,
								{
									'Content-type': 'application/json',
									'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
								},
								null,
								'POST');
						
						nlapiSubmitField('customrecord_clgx_queue_ping_matrix', internalid, 'custrecord_clgx_queue_ping_processed', 'T');
						
					} catch (error) {
				        if (error.getDetails != undefined){
				            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
				        }
				        else{
				            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
				        }
				        nlapiSubmitField('customrecord_clgx_queue_ping_matrix', internalid, 'custrecord_clgx_queue_ping_processed', 'T');
					}
					nlapiSubmitField('customrecord_clgx_queue_ping_matrix', internalid, 'custrecord_clgx_queue_ping_processed', 'T');

	            } catch(error) {
			        if (error.getDetails != undefined){
			            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
			        }
			        else{
			            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
			        }
			        nlapiSubmitField('customrecord_clgx_queue_ping_matrix', internalid, 'custrecord_clgx_queue_ping_processed', 'T');
	            }
	
	        }
	        
	        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
	    }
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
