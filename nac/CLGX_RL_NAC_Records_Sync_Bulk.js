nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_NAC_Records_Sync.js
//	Script Name:	CLGX_RL_NAC_Records_Sync
//	Script Id:		customscript_clgx_rl_nac_rec_sync
//	Script Runs:	On Server
//	Script Type:	Restlet
//	@authors:		Dan Cech - dan.cech@cologix.com
//	Created:		12/1/2015
//-------------------------------------------------------------------------------------------------

var get = wrap(function get(datain) {
	var recordtype = datain['type'];
	
	// get an individual record
	if (datain['id'] && !datain['criteria']) {
		var id = datain['id'];
		
		var row = nlapiLoadRecord(recordtype, id);
		
		var ret = json_serialize(row);
		
		// ret['introspect'] = introspect(recordtype, row);
		// ret['rawdata'] = row.rawData;
		
		return ret;
	}

	// get a list of records
	if (datain['criteria']) {
		var criteria = datain['criteria'];
	}
	else{
		var criteria = 'equalto';
	}
	
	var filters = new Array();
	for (var i in datain) {
		if (i == 'type' || i == 'criteria' ) {
			continue;
		}
		//return criteria;
		//return datain[i];
		//return i;
		filters.push(new nlobjSearchFilter(i, null, criteria, datain[i]));
	}
	
	var cntfilters = filters.length;
	
	var columns = new Array();
	columns[0] = new nlobjSearchColumn('internalid').setSort(false /* bsortascending */);
	
	var rows = new Array();
	
	while (true) {
		res = nlapiSearchRecord(recordtype, null, filters, columns);
		if (!res) {
			break;
		}
		
		for (var i in res) {
			if (res[i].getRecordType() != recordtype) {
				continue;
			}
			
			var row = nlapiLoadRecord(res[i].getRecordType(), res[i].getId());
			//rows.push({
			//	'id': res[i].getId(),
			//	'recordtype': res[i].getRecordType()
			//});
			rows.push(row);
		}
		
		if (res.length < 1000) {
			break;
		}
		
		var lastId = res[999].getId();
		filters[cntfilters] = new nlobjSearchFilter("internalIdNumber", null, "greaterThan", lastId);
	}
	
	return rows;
});

var post = wrap(function post(datain) {
	var recordtype = datain['type'];
	
	// get an individual record
	if (datain['id']) {
		var record = nlapiLoadRecord(recordtype, datain['id']);
	} else {
		var record = nlapiCreateRecord(recordtype);
	}
	
	var old = {};
	var updated = {};
	for (var k in datain) {
		if (k == 'id' || k == 'type') {
			continue;
		}
		
		old[k] = record.getFieldValue(k);
		if (datain[k] != old[k]) {
			record.setFieldValue(k, datain[k]);
			updated[k] = datain[k];
		}
	}
	
	var id = nlapiSubmitRecord(record, true);
	
	return {
		'id': id,
		'old': old,
		'new': updated
	};
});
