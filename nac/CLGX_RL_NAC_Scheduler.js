nlapiLogExecution("audit","FLOStart",new Date().getTime());
function NAC_Scheduler(AsDate, Hour, LastRun, NextRun, StartDate, ScheduleType, ScheduleUnitsA, ScheduleUnitsB, ScheduleUnitsC, ScheduleOpt, ScheduleBitmask) {
	var StartDate = moment(StartDate);
	var NextRun = moment(NextRun);
	var LastRun = moment(LastRun);

	
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - AsDate',AsDate);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - Hour',Hour);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - LastRun',LastRun);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - NextRun',NextRun);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - StartDate',StartDate);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ScheduleType',ScheduleType);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ScheduleUnitsA',ScheduleUnitsA);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ScheduleUnitsB',ScheduleUnitsB);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ScheduleUnitsC',ScheduleUnitsC);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ScheduleOpt',ScheduleOpt);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ScheduleBitmask',ScheduleBitmask);
	/*
	Time - 
	NoEnd, End after x occurrences, or by x date
	
	Daily												ScheduleType    = 0
		()Every x days									ScheduleOpt     = 0, ScheduleUnitsA  = xX
		()Every day										ScheduleOpt     = 1
	
	Weekly												ScheduleType    = 1
		()Every X Weeks									ScheduleUnitsA  = X
	  	On Many_DayOfWeek (Sunday, monday, tuesday, wednesday, thursday, friday, saturday)	ScheduleBitmask = 0
	
	Monthly												ScheduleType    = 2
		Day X of every Y month							ScheduleOpt     = 0, ScheduleUnitsA  =X, ScheduleUnitsB = Y 
		The x(First, Second, Third, Fourth, Last) y(DayOfWeek) or every z months			ScheduleOpt     = 1, ScheduleUnitsA  =Xx, ScheduleUnitsB = Y, ScheduleUnitsC = Z
	
	Yearly												ScheduleType    = 3
		Every x(month) y(day)									ScheduleOpt     = 0, ScheduleUnitsA  = x, ScheduleUnitsB = y
	    The x(First, Second, Third, Fourth, Last) y(DayOfWeek) of z(Month)	ScheduleOpt     = 1, ScheduleUnitsA  =Xx, ScheduleUnitsB = Y, ScheduleUnitsc = Z
	
	Hourly												ScheduleType    = 4
		Every x(hours)									ScheduleUnitsA  = X

	BitMask; Saturday = 64, Wednesday=32, Tuesday=16, Friday=8, Monday=4, Thursday=2, Sunday=1
	
	EndDate DATE = null
	ScheduleType INT (1=daily, 2=weekly, 3=monthly, 4=yearly)
	ScheduleUnitsA INT = NULL
	ScheduleUnitsB INT = NULL
	ScheduleBitmask INT = 0
	*/
	
	/* Starting at ISNULL(LastRun, ISNULL(StartDate, AsDate)) loop for each run time until now */
	
	//var TAsDate = moment(AsDate).tz("America/New_York");
	//AsDate = TAsDate;
	
	
	
	/* Process Bit Mask */
	var ReoccurTest;
	var ScheduleBitmask_Left;
	var ScheduleBitmask_Sunday = 0;
	var ScheduleBitmask_Monday = 0;
	var ScheduleBitmask_Tuesday = 0;
	var ScheduleBitmask_Wednesday = 0;
	var ScheduleBitmask_Thursday = 0;
	var ScheduleBitmask_Friday = 0;
	var ScheduleBitmask_Saturday = 0;
	
	if (ScheduleType == 1 && ScheduleBitmask == null && LastRun != null) {
		var date_lastrun = new moment(LastRun);
		var dw = LastRun.day();
		switch(dw) {
			case 1:
				ScheduleBitmask = 1; 
				break;
			case 2:
				ScheduleBitmask = 4; 
				break;
			case 3:
				ScheduleBitmask = 16; 
				break;
			case 4:
				ScheduleBitmask = 32; 
				break;
			case 5:
				ScheduleBitmask = 2; 
				break;
			case 6:
				ScheduleBitmask = 8; 
				break;
			case 7:
				ScheduleBitmask = 64; 
				break;
		}
	}
	
	if (ScheduleBitmask > 0) {
		ScheduleBitmask_Left = ScheduleBitmask;
		if (ScheduleBitmask_Left >= 64) {
			ScheduleBitmask_Saturday = 1;
			ScheduleBitmask_Left = (ScheduleBitmask_Left-64);	
		}
		if (ScheduleBitmask_Left >= 32) { 
			ScheduleBitmask_Wednesday = 1;
			ScheduleBitmask_Left = (ScheduleBitmask_Left-32);
		}
		if (ScheduleBitmask_Left >= 16) { 
			ScheduleBitmask_Tuesday = 1;
			ScheduleBitmask_Left = (ScheduleBitmask_Left-16);
		}
		if (ScheduleBitmask_Left >= 8) { 
			ScheduleBitmask_Friday = 1;
			ScheduleBitmask_Left = (ScheduleBitmask_Left-8);
		}
		if (ScheduleBitmask_Left >= 4) { 
			ScheduleBitmask_Monday = 1;
			ScheduleBitmask_Left = (ScheduleBitmask_Left-4);
		}
		if (ScheduleBitmask_Left >= 2) { 
			ScheduleBitmask_Thursday = 1;
			ScheduleBitmask_Left = (ScheduleBitmask_Left-2);
		}
		if (ScheduleBitmask_Left >= 1) { 
			ScheduleBitmask_Sunday = 1;
			ScheduleBitmask_Left = (ScheduleBitmask_Left-1);
		}
	}
	/* Done with Bitmask */
	/*
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ScheduleBitmask_Sunday',ScheduleBitmask_Sunday);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ScheduleBitmask_Monday',ScheduleBitmask_Monday);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ScheduleBitmask_Tuesday',ScheduleBitmask_Tuesday);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ScheduleBitmask_Wednesday',ScheduleBitmask_Wednesday);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ScheduleBitmask_Thursday',ScheduleBitmask_Thursday);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ScheduleBitmask_Friday',ScheduleBitmask_Friday);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ScheduleBitmask_Saturday',ScheduleBitmask_Saturday);
	*/
	var TestDate='';
	
	if (LastRun == null) {
		//LastRun = moment(StartDate).add(Hour, 'hours');
		LastRun = moment(StartDate);
		TestDate = new moment(StartDate);
		nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - TEST DATE from 1',TestDate);
	} else {
		TestDate = new moment(NextRun);
		nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - TEST DATE from 2',TestDate);
	}
	
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - AsDate',AsDate);
	
	
	var Complete = 0;
	
	var iteration = 0;
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - TestDate',TestDate);

	
	//if TestDate == today and hour(AsDate) < Hours, TEstDate +=1 day
	
	
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - TestDAte Pre', moment(TestDate));
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - A=T', ((moment(AsDate).format('MM/DD/YYYY') == moment(TestDate).format('MM/DD/YYYY'))));
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - HH', moment(AsDate).format('HH'));
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - Hour', Hour);
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - HH<Hour', parseInt(moment(AsDate).format('HH')) > parseInt(Hour));
	//if (((moment(AsDate).format('MM/DD/YYYY') == moment(TestDate).format('MM/DD/YYYY')) == true) && (parseInt(moment(AsDate).format('HH')) > parseInt(Hour))) {
	//	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - Advance TestDate 3');
	//	TestDate = moment(moment(TestDate).add(1, 'days')).format('MM/DD/YYYY');
	//	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - TestDAte Post', moment(TestDate).format('MM/DD/YYYY'));
	//} 

	AsDate = moment(AsDate).format('MM/DD/YYYY')
	TestDate = moment(TestDate).format('MM/DD/YYYY')
	//TestDate = moment(moment(TestDate).format('MM/DD/YYYY')).add(Hour, 'hours')
	
	while (Complete == 0) {
		iteration++;
		if (iteration >= 1000) {
			nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ABORT', '');
			return 'error iter [' + iteration + '] [' + TestDate + '] [' + AsDate + ']';
		}
		
		//if ((TestDate >= AsDate) == true) {
		nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - TestDate', TestDate);
		nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - AsDate', AsDate);
		var ddiff = moment(TestDate).diff(AsDate, 'days');
		//var ddiff = moment(TestDate).diff(AsDate, 'hours');
		//nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - T/F', (TestDate >= AsDate));
		//TestDate2 = moment(moment(TestDate).add(1, 'days')).format('MM/DD/YYYY')
		//nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - T/F2', (TestDate >= AsDate));
		nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - diff', ddiff);
		if (ddiff > 0) {
	         if (ScheduleType == 0) { 			/* Daily */
				if (ScheduleOpt == 0) { 			/* Every [#] Days							ScheduleUnitsA */
					if (ScheduleUnitsA == 0) { 
						ScheduleUnitsA = 1; 
					}
					//ReoccurTest = moment(TestDate).diff(StartDate, 'days') / ScheduleUnitsA;
					//var re = MonthDiff(StartDate, TestDate);
					var re = moment(TestDate).diff(StartDate, 'days');
					
					ReoccurTest=re / ScheduleUnitsA;
					nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ReoccurTest D','[' + re + '] [' + ReoccurTest + '] [' + moment(StartDate).format("MM/DD/YYYY") + '] [' + moment(TestDate).format("MM/DD/YYYY") + ']');
					
					if (ReoccurTest == Math.floor(ReoccurTest) && ReoccurTest > 0) {
						Complete = 1;
					}
				} else {							/* Every Weekday */
					Complete = 1;
				}
	         } else if (ScheduleType == 1) { 	/* Weekly */
													/* Every [ScheduleUnitsA] Weeks on day [Day_Of_Week]   		ScheduleUnitsA, ScheduleBitmask */
				ReoccurTest = moment(TestDate).diff(StartDate, 'days') / ScheduleUnitsA;
				nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ReoccurTest W','[' + ReoccurTest + '] [' + moment(StartDate).format("MM/DD/YYYY HH:mm") + '] [' + moment(TestDate).format("MM/DD/YYYY HH:mm") + ']');
				nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ReoccurTest W','[' + moment(TestDate).day() + '] [' + ScheduleBitmask_Sunday + '] [' + ScheduleBitmask_Monday + '] [' + ScheduleBitmask_Tuesday + ']  [' + ScheduleBitmask_Wednesday + ']  [' + ScheduleBitmask_Thursday + ']  [' + ScheduleBitmask_Friday + ']  [' + ScheduleBitmask_Saturday + ']');
				if ( 
				   ReoccurTest == Math.floor(ReoccurTest)
					&& ( (moment(TestDate).day() == 0 && ScheduleBitmask_Sunday==1) 
					  || (moment(TestDate).day() == 1 && ScheduleBitmask_Monday==1) 
					  || (moment(TestDate).day() == 2 && ScheduleBitmask_Tuesday==1) 
					  || (moment(TestDate).day() == 3 && ScheduleBitmask_Wednesday==1) 
					  || (moment(TestDate).day() == 4 && ScheduleBitmask_Thursday==1) 
					  || (moment(TestDate).day() == 5 && ScheduleBitmask_Friday==1) 
					  || (moment(TestDate).day() == 6 && ScheduleBitmask_Saturday==1) 
					)) 
				{
					Complete = 1;
				}
			} else if (ScheduleType == 2) { 		/* Monthly */
				if (ScheduleOpt == 0) { 					/* Monthly [Day_Of_Month] day of every [#] month(s)			ScheduleUnitsA, ScheduleUnitsB */
					var re = MonthDiff(StartDate, TestDate);
					ReoccurTest=re / ScheduleUnitsB;
					nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ReoccurTest M0', re + '[' + ReoccurTest + '] [' + moment(StartDate).format("MM/DD/YYYY") + '] [' + moment(TestDate).format("MM/DD/YYYY") + '] [' + ScheduleUnitsA + '] [' + ScheduleUnitsB + '] [' + moment(TestDate).format('D') + '] [' + moment(TestDate).day() + ']');
					if ( moment(TestDate).format('D') == ScheduleUnitsA && Math.floor(ReoccurTest) == ReoccurTest) {
						Complete = 1;
					}
				} else {							/* Monthly The [First|Second|Third|Foruth|Last] [Day|Weekday|WeekendDay|Mon|Tue|Wed|Thu|Fri|Sat] of every [#] Months */
					nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ReoccurTest M1','[' + moment(TestDate).format("MM/DD/YYYY") + '] [' + ScheduleUnitsA + '] [' + moment(TestDate).day() + '] [' + ScheduleUnitsB + '] [' + ScheduleUnitsC + '] {' + moment(TestDate).format('D') + '} {' + moment((moment(TestDate).endOf('month').add(-6, 'days'))).format("D") + '} {' + moment((moment(TestDate).endOf('month'))).format("D") + '} [' + moment(TestDate).diff(LastRun, 'months', false) + '] [' + (1*ScheduleUnitsC) + ']');
					if (
							(
							       (ScheduleUnitsA == 0 /*First*/  && (moment(TestDate).format('D') >= 1 && moment(TestDate).format('D') <= 7)) 
								|| (ScheduleUnitsA == 1 /*Second*/ && (moment(TestDate).format('D') >= 8 && moment(TestDate).format('D') <=  14))
								|| (ScheduleUnitsA == 2 /*Third*/  && (moment(TestDate).format('D') >= 15 && moment(TestDate).format('D') <=  21)) 
								|| (ScheduleUnitsA == 3 /*Fourth*/ && (moment(TestDate).format('D') >= 22 && moment(TestDate).format('D') <=  28)) 
								|| (ScheduleUnitsA == 4 /*Last*/ && (moment(TestDate).format('D') >= (moment(TestDate).endOf('month').add(-6, 'days').format('D')) && moment(TestDate).format('D') <=  moment(TestDate).endOf('month').format('D')))
							) &&  (
							   (ScheduleUnitsB == 0 && moment(TestDate).day() == 0) /*Sunday*/
							|| (ScheduleUnitsB == 1 && moment(TestDate).day() == 1) /*Monday*/
							|| (ScheduleUnitsB == 2 && moment(TestDate).day() == 2) /*Tuesday*/
							|| (ScheduleUnitsB == 3 && moment(TestDate).day() == 3) /*Wednesday*/
							|| (ScheduleUnitsB == 4 && moment(TestDate).day() == 4) /*Thursday*/
							|| (ScheduleUnitsB == 5 && moment(TestDate).day() == 5) /*Friday*/
							|| (ScheduleUnitsB == 6 && moment(TestDate).day() == 6) /*Saturday*/
						)
							&& moment(TestDate).diff(LastRun, 'months', false) >= (1*ScheduleUnitsC-1) 
						)
					{
						Complete = 1;
					}
				}
			} else if (ScheduleType == 3) { 	/* Yearly */
				if (ScheduleOpt == 0) { 	/*Yearly Every [Month] [Day_Num_Of_Month]st				ScheduleUnitsA, ScheduleUnitsB */
					nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - ReoccurTest Y0','[' + moment(TestDate).format("MM/DD/YYYY") + '] [' + ScheduleUnitsA + '] [' + moment(TestDate).month() + '] [' + moment(TestDate).day() + '] [' + ScheduleUnitsB + ']');
					if (moment(TestDate).month() == (ScheduleUnitsA) && moment(TestDate).format('D') == ScheduleUnitsB) {
						Complete = 1;
					}
				} else {			/*Yearly The [First|Second|Third|Foruth|Last] [Day|Weekday|WeekendDay|Mon|Tue|Wed|Thu|Fri|Sat] of every [Month] */
					if  (
							(
							   (ScheduleUnitsA == 1 /*First*/  && (moment(TestDate).format('D') >= 1 && moment(TestDate).format('D') <= 7)) 
							|| (ScheduleUnitsA == 2 /*Second*/ && (moment(TestDate).format('D') >= 8 && moment(TestDate).format('D') <= 14)) 
						    || (ScheduleUnitsA == 3 /*Third*/  && (moment(TestDate).format('D') >= 15 && moment(TestDate).format('D') <= 21)) 
						    || (ScheduleUnitsA == 4 /*Fourth*/ && (moment(TestDate).format('D') >= 22 && moment(TestDate).format('D') <= 28)) 
							|| (ScheduleUnitsA == 5 /*Last*/ && (moment(TestDate).format('D') >= (moment(TestDate).endOf('month').add(-6, 'days').format('D')) && moment(TestDate).format('D') <=  moment(TestDate).endOf('month').format('D')))
						   ) && (
							   (ScheduleUnitsB == 0 && moment(TestDate).day() == 0) /*Sunday*/
							|| (ScheduleUnitsB == 1 && moment(TestDate).day() == 1) /*Monday*/
							|| (ScheduleUnitsB == 2 && moment(TestDate).day() == 2) /*Tuesday*/
							|| (ScheduleUnitsB == 3 && moment(TestDate).day() == 3) /*Wednesday*/
							|| (ScheduleUnitsB == 4 && moment(TestDate).day() == 4) /*Thursday*/
							|| (ScheduleUnitsB == 5 && moment(TestDate).day() == 5) /*Friday*/
							|| (ScheduleUnitsB == 6 && moment(TestDate).day() == 6) /*Saturday*/
						   )
						   && (moment(TestDate).Month() == (ScheduleUnitsC + 1))
						)
					{
						Complete = 1;
					}
				}
			} else if (ScheduleType == 4) { 	/* Hourly */
				var newDT = moment(TestDate).add(ScheduleUnitsA, 'hours');
				TestDate = newDT;
				
				if (TestDate > LastRun) {
					Complete = 1;
					return(TestDate);
				} else {
					Complete = 0;
				}
			} else {
				return('1902-01-1');
			}
		//} else {
		//	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - LOOP', '[' + (TestDate >= AsDate) + '] [' + moment(TestDate).format("MM/DD/YYYY") + '] [' + moment(AsDate).format("MM/DD/YYYY") + ']');
		}

		if (Complete == 0) {
			if (ScheduleType == 4) {
				TestDate = TestDate;
			} else {
				//var newDT = moment(moment(TestDate).add(1, 'days')).format('MM/DD/YYYY');
				//TestDate = moment(moment(moment(TestDate).format('MM/DD/YYYY')).add(Hour, 'hours')).format('MM/DD/YYYY HH:mm')
				//TestDate = newDT;
				newDT = moment(TestDate).add(1, 'days');
				TestDate = newDT; 
			}
		}
		/* Get Next TimeSlot */
	}
	NextRun = TestDate;
	if (parseInt(moment(TestDate).format('HH')) != parseInt(Hour)) {
		NextRun = moment(moment(TestDate).format('MM/DD/YYYY')).add(parseInt(Hour), 'hours');
	}
	
	if (NextRun < AsDate) { NextRun=NULL; } // Really Rut Roh!
	nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - NextRun',NextRun);
	return(NextRun);			
}

function MonthDiff(Start, End) {
	s_m = moment(Start).format('MM')
	s_y = moment(Start).format('YYYY')

	e_m = moment(End).format('MM')
	e_y = moment(End).format('YYYY')

	var s_t=moment(s_m + '/01/' + s_y)
	var e_t=moment(e_m + '/01/' + e_y)
	//nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - MonthDiff','[' + moment(s_t).format("MM/DD/YYYY") + '] [' + moment(e_t).format("MM/DD/YYYY") + ']')
	
	//nlapiLogExecution('DEBUG','Scheduled Script - Scheduler - MonthDiff','[' + s_m + '] [' + s_y + '] [' + e_m + '] [' + e_y + ']')
	var diff = 0;
	while (s_t <= e_t) {
		s_t.add(1, 'months')
		diff++;
	}
	return(diff-1);
}

function get (datain) {
	try {

		  //AsDate, Hour, LastRun, NextRun, StartDate, ScheduleType, ScheduleUnitsA, ScheduleUnitsB, ScheduleUnitsC, ScheduleOpt, ScheduleBitmask
		//var newdate = NAC_Scheduler(moment().add(4, 'hours'), sHour, sLastRun, sNextRun, sStartDate, sType, sUnitA, sUnitB, sUnitC, sOpt, sBit);		
    	//return NAC_Scheduler(moment().add(4, 'hours'), '22', '2016-09-07 22:00', '2016-09-01 22:00', '2016-09-01 22:00', 1, 1, 0, 0, 0, 64);
    	//return NAC_Scheduler(moment('2017-01-26 02:00:14'), '5', '2017-01-19 03:00:00', '2017-01-26 03:00:00', '2016-06-12', 1, 1, 0, 0, 0, 2);
    	return NAC_Scheduler(moment('2017-01-26 20:00:14'), '7', '2017-01-26 10:00:00', '2017-01-26 07:00:00', '2016-01-02', 1, 1, 0, 0, 0, 2);
    } catch (error) { // Start Catch Errors Section 
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        } else {
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


