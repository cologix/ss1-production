//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Test.js
//	Script Name:	CLGX_SL_Test
//	Script Id:		customscript_clgx_sl_test
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/11/2013
//-------------------------------------------------------------------------------------------------

function suitelet_test2 (request, response){
    try {

    	var contactid = 596465;
    	
        var recContact = nlapiLoadRecord('contact', contactid);
        var internalid = recContact.getFieldValue('id');
        
		var objContact = new Object();
		
		objContact["action"] = 'update';
		
		objContact["contactnacid"] = 123456789;
		objContact["contactid"] = parseInt(recContact.getFieldValue('id'));
		objContact["contact"] = recContact.getFieldValue('entityid');
		
		objContact["contact"] = recContact.getFieldValue('entityid');
		objContact["firstname"] = recContact.getFieldValue('firstname');
		objContact["lastname"] = recContact.getFieldValue('lastname');
		objContact["email"] = recContact.getFieldValue('email');
		objContact["phone"] = recContact.getFieldValue('phone');
		objContact["mobilephone"] = recContact.getFieldValue('mobilephone');
		
		objContact["companyid"] = parseInt(recContact.getFieldValue('company'));
		objContact["company"] = recContact.getFieldText('company');
		
		objContact["title"] = recContact.getFieldValue('title');
		
		objContact["roleid"] = parseInt(recContact.getFieldValue('contactrole'));

		/*
		var nbrAddresses = recCompany.getLineItemCount('addressbook');
		var arrAddresses = new Array();
		for ( var i = 0;  i < nbrAddresses; i++ ) {
			var objAddress = new Object();
			objAddress["addr1"] = recCompany.getLineItemValue('addressbook','addr1', i + 1);
			objAddress["addr2"] = recCompany.getLineItemValue('addressbook','addr2', i + 1);
			objAddress["addr3"] = recCompany.getLineItemValue('addressbook','addr3', i + 1);
			objAddress["city"] = recCompany.getLineItemValue('addressbook','city', i + 1);
			objAddress["state"] = recCompany.getLineItemValue('addressbook','state', i + 1);
			objAddress["zip"] = recCompany.getLineItemValue('addressbook','zip', i + 1);
			objAddress["phone"] = recCompany.getLineItemValue('addressbook','phone', i + 1);
			arrAddresses.push(objAddress);
		}
		objCompany["addresses"] = arrAddresses;
		*/
		
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid','company',null));
		arrColumns.push(new nlobjSearchColumn('entityid','company',null));
		arrColumns.push(new nlobjSearchColumn('entitystatus','company',null));
		arrColumns.push(new nlobjSearchColumn('salesrep','company',null));
		arrColumns.push(new nlobjSearchColumn('currency','company',null));
		//arrColumns.push(new nlobjSearchColumn('terms','company',null));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",contactid));
		var searchCompanies = nlapiSearchRecord('contact', null, arrFilters, arrColumns);
		var arrCompanies = new Array();
		for ( var i = 0; searchCompanies != null && i < searchCompanies.length; i++ ) {
			var objCompany = new Object();
			objCompany["entitynacid"] = 123456789;
			objCompany["companyid"] = parseInt(searchCompanies[i].getValue('internalid','company',null));
			objCompany["company"] = searchCompanies[i].getValue('entityid','company',null);
			objCompany["status"] = searchCompanies[i].getText('entitystatus','company',null);
			objCompany["salesrepid"] = parseInt(searchCompanies[i].getValue('salesrep','company',null));
			objCompany["salesrep"] = searchCompanies[i].getText('salesrep','company',null);
			arrCompanies.push(objCompany);
		}
		objContact["companies"] = arrCompanies;
		
		
		/*
		var nbrContacts = recCompany.getLineItemCount('contactroles');
		var arrContacts = new Array();
		for ( var i = 0;  i < nbrContacts; i++ ) {
			var objContact = new Object();
			objContact["contactid"] = parseInt(recCompany.getLineItemValue('contactroles','contact', i + 1));
			objContact["contact"] = recCompany.getLineItemValue('contactroles','contactname', i + 1);
			objContact["roleid"] = parseInt(recCompany.getLineItemValue('contactroles','role', i + 1));
			objContact["role"] = recCompany.getLineItemText('contactroles','role', i + 1);
			objContact["email"] = recCompany.getLineItemValue('contactroles','email', i + 1);
			arrContacts.push(objContact);
		}
		objCompany["contacts"] = arrContacts;
		*/
		
        response.write(JSON.stringify(objContact));
        
        //response.write('<br/><br/>');
 		//response.write(JSON.stringify(rec));
    	
 		//response.write('<br/><br/>');
		//response.write(nlapiGetContext().getRemainingUsage());
		//response.write('<br/><br/>');
		//response.write(loops);
		
		
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}




function get_ops_reps (arrFacilitiesIDs){

    var arrOpsReps = new Array();
    for ( var i = 0; i < arrFacilitiesIDs.length; i++ ) {

        if(arrFacilitiesIDs[i] == 2 || arrFacilitiesIDs[i] == 4 || arrFacilitiesIDs[i] == 5 || arrFacilitiesIDs[i] == 6 || arrFacilitiesIDs[i] == 7 || arrFacilitiesIDs[i] == 9 || arrFacilitiesIDs[i] == 19){
                
            var objOpsRep = new Object();
            objOpsRep["opid"] = 168983;
            objOpsRep["op"] = 'Donato Di Rienzo';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 3118;
            objOpsRep["op"] = 'Sorin Arion';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 339723;
            objOpsRep["op"] = 'Rochelle Price';
            arrOpsReps.push(objOpsRep);

        }

        else{

            var objOpsRep = new Object();
            objOpsRep["opid"] = 212518;
            objOpsRep["op"] = 'Amy J Connell';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 9735;
            objOpsRep["op"] = 'Pauli Guild';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 339723;
            objOpsRep["op"] = 'Rochelle Price';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 8795;
            objOpsRep["op"] = 'Mario Novello';
            arrOpsReps.push(objOpsRep);

            //var objOpsRep = new Object();
            //objOpsRep["opid"] = 211767;
            //objOpsRep["op"] = 'Vlad Thiel';
            //arrOpsReps.push(objOpsRep);

        }
    }

    return arrOpsReps;
}
