nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_NAC_Billing_Batch.js
//	Script Name:	CLGX_SL_NAC_Billing_Batch
//	Script Id:		customscript_clgx_sl_nac_billing_batch
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/16/2015
//-------------------------------------------------------------------------------------------------

function suitelet_nac_billing_batch (request, response){
    try {

    	
    	var startScript = moment();
    	
    	//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/nac/get_nac_billing.cfm');
		var strJSON = requestURL.body;
		var arrBilling = JSON.parse( strJSON );
    	
		var today = moment().format('YYYY-M-D');
		
		var jsonFile = nlapiCreateFile(today + '_nac_' + '.json', 'PLAINTEXT', JSON.stringify(arrBilling));
		jsonFile.setFolder(2614455);
		var fileid = nlapiSubmitFile(jsonFile);
		
    	var jsonFile = nlapiCreateFile(today + '_nac_queue' + '.json', 'PLAINTEXT', JSON.stringify(arrBilling));
		jsonFile.setFolder(2614455);
		var fileid = nlapiSubmitFile(jsonFile);
		
		
		var endScript = moment();
     	var execSeconds = (endScript.diff(startScript)/1000).toFixed(1); // execution time in seconds
     	var processed = '' + endScript.format('M/D/YYYY h:mm:ss a');
     	
		if(fileid > 0){
    		response.write('{"success":true,"fileid":' + fileid + ',"batchid":12345,"records":' + arrBilling.length + ',"processed":"' + processed + '","exectime":"' + execSeconds + ' sec","errorcode":"None","errordetails":""}');
    	}
    	else{
    		response.write('{"success":false,"fileid":0,"batchid":0,"batchid":0,"records":0,"processed":"' + processed + '","exectime":"' + execSeconds + ' sec","errorcode":"Wrong File","errordetails":"File creation error"}');
    	}
		
		
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}




function get_ops_reps (arrFacilitiesIDs){

    var arrOpsReps = new Array();
    for ( var i = 0; i < arrFacilitiesIDs.length; i++ ) {

        if(arrFacilitiesIDs[i] == 2 || arrFacilitiesIDs[i] == 4 || arrFacilitiesIDs[i] == 5 || arrFacilitiesIDs[i] == 6 || arrFacilitiesIDs[i] == 7 || arrFacilitiesIDs[i] == 9 || arrFacilitiesIDs[i] == 19){
                
            var objOpsRep = new Object();
            objOpsRep["opid"] = 168983;
            objOpsRep["op"] = 'Donato Di Rienzo';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 3118;
            objOpsRep["op"] = 'Sorin Arion';
            arrOpsReps.push(objOpsRep);
        }

        else{

            var objOpsRep = new Object();
            objOpsRep["opid"] = 212518;
            objOpsRep["op"] = 'Amy J Connell';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 9735;
            objOpsRep["op"] = 'Pauli Guild';
            arrOpsReps.push(objOpsRep);

            var objOpsRep = new Object();
            objOpsRep["opid"] = 8795;
            objOpsRep["op"] = 'Mario Novello';
            arrOpsReps.push(objOpsRep);

            //var objOpsRep = new Object();
            //objOpsRep["opid"] = 211767;
            //objOpsRep["op"] = 'Vlad Thiel';
            //arrOpsReps.push(objOpsRep);

        }
    }

    return arrOpsReps;
}
