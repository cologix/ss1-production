nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_VXC.js
//	Script Name:	CLGX_CR_VXC
//	Script Id:		customscript_clgx_cr_vxc
//	Script Runs:	Client
//	Script Type:	Client Script
//	Deployments:	Expense Report
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		04/18/2017
//-------------------------------------------------------------------------------------------------

function saveRecord() {
    try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Details:	Verify if any of the mandatory ports are from the same equipment or unavailable - if yes, display an alert and don't allow saving the record
//-------------------------------------------------------------------------------------------------

        var saveOK = 1;
        var response = '';

        var xcType = nlapiGetFieldValue('custrecord_cologix_vxc_type');
        var stag = nlapiGetFieldValue('custpage_clgx_vxc_stag');
        if (xcType == 28 || xcType == 29 || xcType == 30 || xcType == 31) {
            if (stag == '') {
                var response = 'Please enter value for: CLGX STAG';
                var saveOK = 0;
            }

            /* if(xcType!=28 && xcType!=29 && xcType!=30 && xcType!=31) {
             var aPort = nlapiGetFieldValue('custrecord_clgx_a_end_port');
             var bPort = nlapiGetFieldValue('custrecord_clgx_b_end_port');
             var cPort = nlapiGetFieldValue('custrecord_clgx_c_end_port');
             var zPort = nlapiGetFieldValue('custrecord_clgx_z_end_port');
             var vlan = nlapiGetFieldValue('custrecord_clgx_xc_vlan');
             var speed = nlapiGetFieldValue('custrecord_clgx_speed');

             var fields = ['custrecord_clgx_active_port_equipment', 'custrecord_clgx_active_port_status']; // same for all 4 switches

             if (aPort != null && aPort != '') {
             var aColumns = nlapiLookupField('customrecord_clgx_active_port', aPort, fields);
             var aPortEquip = aColumns.custrecord_clgx_active_port_equipment;
             var aPortStatus = aColumns.custrecord_clgx_active_port_status;
             } else {
             var aPortStatus = 0;
             aPort = '';
             }

             if (bPort != null && bPort != '') {
             var bColumns = nlapiLookupField('customrecord_clgx_active_port', bPort, fields);
             var bPortEquip = bColumns.custrecord_clgx_active_port_equipment;
             var bPortStatus = bColumns.custrecord_clgx_active_port_status;
             } else {
             var bPortStatus = 0;
             bPort = '';
             }

             if (cPort != null && cPort != '') {
             var cColumns = nlapiLookupField('customrecord_clgx_active_port', cPort, fields);
             var cPortEquip = cColumns.custrecord_clgx_active_port_equipment;
             var cPortStatus = cColumns.custrecord_clgx_active_port_status;
             } else {
             var cPortStatus = 0;
             cPort = '';
             }

             if (zPort != null && zPort != '') {
             var zColumns = nlapiLookupField('customrecord_clgx_active_port', zPort, fields);
             var zPortEquip = zColumns.custrecord_clgx_active_port_equipment;
             var zPortStatus = zColumns.custrecord_clgx_active_port_status;
             } else {
             var zPortStatus = 0;
             zPort = '';
             }

             if (vlan != null && vlan != '') {
             var vFields = ['custrecord_clgx_vlan_status'];
             var vColumns = nlapiLookupField('customrecord_clgx_vlan', vlan, vFields);
             var vPortStatus = vColumns.custrecord_clgx_vlan_status;
             } else {
             var vPortStatus = 0;
             vlan = '';
             }

             // 2 hops --------------------------------------------------------------------------------------
             if (xcType == 19 || xcType == 20) {
             if (aPort != '' && zPort != '' && aPort != null && zPort != null) {
             if (aPortEquip == zPortEquip) {
             var response = 'All configured switches must be from different equipments.';
             var saveOK = 0;
             }
             }
             else {
             var response = 'Please configure both access switches.';
             var saveOK = 0;
             }
             if (bPort != '' || cPort != '') {
             var response = 'Please set type to "4 hop" and remove both distribution switches.';
             var saveOK = 0;
             }
             }
             // 3 hops --------------------------------------------------------------------------------------
             else if (xcType == 21 || xcType == 23) {
             if (aPort != '' && bPort != '' && zPort != '' && aPort != null && bPort != null && zPort != null) {
             if (aPortEquip == zPortEquip || bPortEquip == zPortEquip || aPortEquip == bPortEquip) {
             var response = 'All configured switches must be from different equipments.';
             var saveOK = 0;
             }
             }
             else {
             var response = 'Please configure both access switches and the first distribution switch.';
             var saveOK = 0;
             }
             if (cPort != '') {
             var response = 'Please set type to "4 hop" and remove the second distribution switch.';
             var saveOK = 0;
             }
             }
             // 4 hops --------------------------------------------------------------------------------------
             else if (xcType == 22 || xcType == 24) {
             if (aPort != '' && bPort != '' && cPort != '' && zPort != '' && aPort != null && bPort != null && cPort != null && zPort != null) {
             if (aPortEquip == zPortEquip || bPortEquip == zPortEquip || cPortEquip == zPortEquip || aPortEquip == bPortEquip || aPortEquip == cPortEquip || bPortEquip == cPortEquip) {
             var response = 'All configured switches must be from different equipments.';
             var saveOK = 0;
             }
             }
             else {
             var response = 'Please configure both access switches and both distribution switches.';
             var saveOK = 0;
             }
             }
             // no hops --------------------------------------------------------------------------------------
             else { // xcType == '' || xcType == 17 || xcType == 18
             if (aPort != '' || bPort != '' || cPort != '' || zPort != '') {
             var response = 'Please set type to "4 hop" and remove all switches.';
             var saveOK = 0;
             }
             }

             // verify Vlan and speed status ----------------------------------------------------------------
             if (xcType == '' || xcType == 17 || xcType == 18) {
             if (vlan != '' || speed != '') {
             var response = 'Please set type to "4 hop" and remove both VLAN# and Speed.';
             var saveOK = 0;
             }
             }
             else {
             if (vlan == '' || speed == '') {
             var response = 'Please configure both VLAN# and Speed.';
             var saveOK = 0;
             }
             }
             */
            // display alert and permit save or not ----------------------------------------------------------------
            if (saveOK == 0) {
                var alert = confirm(response);
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return true;
        }
    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }

}

function pageInit(type)
{
    if(type=='view'){

        nlapiSetFieldValue('custpage_clgx_vxc_stag', nlapiGetFieldValue('custrecord_clgx_vxc_stag'));
    }
    if(type=='edit'){
        var vxctype = nlapiGetFieldValue('custrecord_cologix_vxc_type');
        var provider = nlapiGetFieldValue('custrecord_clgx_cloud_provider');
        var market=nlapiGetFieldValue('custrecord_cologix_vxc_market');
        var internalID=nlapiGetRecordId();
        var stag=nlapiGetFieldValue('custrecord_clgx_vxc_stag');
        var so=nlapiGetFieldValue('custrecord_cologix_service_order');
        var selected=false;
        if(vxctype!=null && provider!=null && market!=null) {
            var columns = new Array();
            var filters = new Array();
            if((market!=null && market!='') && (vxctype == 28 || vxctype == 29 || vxctype == 30)){
                filters.push(new nlobjSearchFilter("custrecord_cologix_vxc_market", null, "anyof", market));

            }
            if((vxctype!=null && vxctype!='')&&(vxctype == 28 || vxctype == 29 || vxctype == 30)) {
                var vxctypeArr=[28,29,30];
                filters.push(new nlobjSearchFilter("custrecord_cologix_vxc_type", null, "anyof", vxctypeArr));
            }
            if(provider!=null && provider!='') {
                filters.push(new nlobjSearchFilter("custrecord_clgx_cloud_provider", null, "anyof", provider));
            }
            if (internalID != null && internalID!='') {
                filters.push(new nlobjSearchFilter("internalid", null, "noneof", internalID));
            }
            var records = nlapiSearchRecord('customrecord_cologix_vxc', 'customsearch_clgx_ss_vxc_stag', filters, columns);
            var arrstag = new Array();
            var arrso = new Array();
            if (records != null) {
                for (var j = 0; records != null && j < records.length; j++) {
                    var search = records[j];
                    var columns = search.getAllColumns();
                    arrstag.push(search.getValue(columns[0]));
                    arrso.push(search.getValue(columns[1]));
                }
            }
            if ((provider == 3) && (vxctype == 28 || vxctype == 29 || vxctype == 30)) { // Amazon
                for (var j = 1500; j < 1833; j++) {
                    if (j == stag) {
                        selected = true;
                        // alert(j+';'+selected);
                    }
                    else{
                        selected=false;
                    }
                    if ((!in_array(j, arrstag))|| (in_array(so,arrso)&& so!=null)) {
                        nlapiInsertSelectOption('custpage_clgx_vxc_stag', j, j, selected);
                    }

                }
            }
            if ((provider == 2) && (vxctype == 28 || vxctype == 29 || vxctype == 30)) { // Microsoft
                nlapiRemoveSelectOption('custpage_clgx_vxc_stag', '1500');
                for (var j = 1833; j < 2167; j++) {
                    if (j == stag) {
                        selected = true;
                    }
                    else{
                        selected=false;
                    }
                    if ((!in_array(j, arrstag))|| (in_array(so,arrso)&& so!=null)) {
                        nlapiInsertSelectOption('custpage_clgx_vxc_stag', j, j, selected);
                    }

                }
            }
            if ((provider == 4) && (vxctype == 28 || vxctype == 29 || vxctype == 30)) { // Microsoft
                for (var j = 2167; j < 2500; j++) {
                    if (j == stag) {
                        selected = true;
                    }
                    else{
                        selected=false;
                    }
                    if ((!in_array(j, arrstag))|| (in_array(so,arrso)&& so!=null)) {
                        nlapiInsertSelectOption('custpage_clgx_vxc_stag', j, j, selected);
                    }

                }
            }
            if (vxctype == 31 ) { // Cloud Hop 4
                for (var j = 2500; j < 2750; j++) {
                    if (j == stag) {
                        selected = true;
                    }
                    else{
                        selected=false;
                    }
                    if ((!in_array(j, arrstag))|| (in_array(so,arrso)&& so!=null)) {
                        nlapiInsertSelectOption('custpage_clgx_vxc_stag', j, j, selected);
                    }

                }
            }
            if ((provider == 1) && (vxctype == 28 || vxctype == 29 || vxctype == 30)) { // Microsoft
                // SoftLayer
                for (var j = 2750; j < 3000; j++) {
                    if (j == stag) {
                        selected = true;
                    }
                    else{
                        selected=false;
                    }
                    if ((!in_array(j, arrstag))|| (in_array(so,arrso)&& so!=null)) {
                        nlapiInsertSelectOption('custpage_clgx_vxc_stag', j, j, selected);
                    }

                }
            }
        }
    }

}

function fieldChanged(type, name, linenum){
    try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 10/09/2013
// Details:	Set market field depending on facility
//-------------------------------------------------------------------------------------------------
        if (name == 'custrecord_cologix_vxc_type' || name == 'custrecord_clgx_cloud_provider' || name=='custrecord_cologix_vxc_market') {

            var vxctype = nlapiGetFieldValue('custrecord_cologix_vxc_type');
            var provider = nlapiGetFieldValue('custrecord_clgx_cloud_provider');
            var market=nlapiGetFieldValue('custrecord_cologix_vxc_market');
            var so=nlapiGetFieldValue('custrecord_cologix_service_order');
            // alert(vxctype+';'+provider+';'+market);
            var internalID=nlapiGetRecordId();
            var stag=nlapiGetFieldValue('custrecord_clgx_vxc_stag');
            var selected=false;
            for (var j = 1500; j < 3000; j++) {
                nlapiRemoveSelectOption('custpage_clgx_vxc_stag', j);
            }


            if(vxctype!=null && provider!=null && market!=null) {
                var columns = new Array();
                var filters = new Array();
                if((market!=null && market!='') && (vxctype == 28 || vxctype == 29 || vxctype == 30)){
                    filters.push(new nlobjSearchFilter("custrecord_cologix_vxc_market", null, "anyof", market));

                }
                if((vxctype!=null && vxctype!='')&&(vxctype == 28 || vxctype == 29 || vxctype == 30)) {
                    var vxctypeArr=[28,29,30];
                    filters.push(new nlobjSearchFilter("custrecord_cologix_vxc_type", null, "anyof", vxctypeArr));
                }
                if(provider!=null && provider!='') {
                    filters.push(new nlobjSearchFilter("custrecord_clgx_cloud_provider", null, "anyof", provider));
                }
                if (internalID != null && internalID!='') {
                    filters.push(new nlobjSearchFilter("internalid", null, "noneof", internalID));
                }
                var records = nlapiSearchRecord('customrecord_cologix_vxc', 'customsearch_clgx_ss_vxc_stag', filters, columns);
                var arrstag = new Array();
                var arrso = new Array();
                if (records != null) {
                    for (var j = 0; records != null && j < records.length; j++) {
                        var search = records[j];
                        var columns = search.getAllColumns();
                        arrstag.push(search.getValue(columns[0]));
                        arrso.push(search.getValue(columns[1]));
                    }
                }
                if ((provider == 3) && (vxctype == 28 || vxctype == 29 || vxctype == 30)) { // Amazon

                    for (var j = 1500; j < 1833; j++) {
                        if (j == stag) {
                            selected = true;
                        }
                        else{
                            selected=false;
                        }
                        if ((!in_array(j, arrstag))|| (in_array(so,arrso)&& so!=null)) {
                            nlapiInsertSelectOption('custpage_clgx_vxc_stag', j, j, selected);
                        }

                    }
                }
                if ((provider == 2) && (vxctype == 28 || vxctype == 29 || vxctype == 30)) { // Microsoft

                    for (var j = 1833; j < 2167; j++) {
                        if (j == stag) {
                            selected = true;
                        }
                        else{
                            selected=false;
                        }
                        if ((!in_array(j, arrstag))|| (in_array(so,arrso)&& so!=null)) {
                            nlapiInsertSelectOption('custpage_clgx_vxc_stag', j, j, selected);
                        }

                    }
                }
                if ((provider == 4) && (vxctype == 28 || vxctype == 29 || vxctype == 30)) { // Microsoft
                    for (var j = 2167; j < 2500; j++) {
                        if (j == stag) {
                            selected = true;
                        }
                        else{
                            selected=false;
                        }
                        if ((!in_array(j, arrstag))|| (in_array(so,arrso)&& so!=null)) {
                            nlapiInsertSelectOption('custpage_clgx_vxc_stag', j, j, selected);
                        }

                    }
                }
                if (vxctype == 31 ) { // Cloud Hop 4
                    for (var j = 2500; j < 2750; j++) {
                        if (j == stag) {
                            selected = true;
                        }
                        else{
                            selected=false;
                        }
                        if ((!in_array(j, arrstag))|| (in_array(so,arrso)&& so!=null)) {
                            nlapiInsertSelectOption('custpage_clgx_vxc_stag', j, j, selected);
                        }

                    }
                }
                if ((provider == 1) && (vxctype == 28 || vxctype == 29 || vxctype == 30)) { // Microsoft
                    // SoftLayer
                    for (var j = 2750; j < 3000; j++) {
                        if (j == stag) {
                            selected = true;
                        }
                        else{
                            selected=false;
                        }
                        if ((!in_array(j, arrstag))|| (in_array(so,arrso)&& so!=null)) {
                            nlapiInsertSelectOption('custpage_clgx_vxc_stag', j, j, selected);
                        }

                    }
                }
            }

        }
        if (name == 'custpage_clgx_vxc_stag'){
            var stag=nlapiGetFieldValue('custpage_clgx_vxc_stag');
            if(stag!='undefined'){
                nlapiSetFieldValue('custrecord_clgx_vxc_stag', nlapiGetFieldValue('custpage_clgx_vxc_stag'));
            }

        }
        
        if (name == 'isinactive') {
        	var inactive = nlapiGetFieldValue('isinactive');
        	if(inactive == 'T'){
        		nlapiSetFieldValue('custrecord_clgx_vxc_provider_switch_1',null);
        		nlapiSetFieldValue('custrecord_clgx_vxc_access_switch_1',null);
        		nlapiSetFieldValue('custrecord_clgx_vxc_access_switch_2',null);
        		nlapiSetFieldValue('custrecord_clgx_vxc_access_switch_3',null);
        		nlapiSetFieldValue('custrecord_clgx_vxc_speed',null);
        		nlapiSetFieldValue('custrecord_clgx_cloud_connect_port',null);
        		nlapiSetFieldValue('custrecord_clgx_cloud_connect_port_z',null);
        		nlapiSetFieldValue('custrecord_clgx_vxc_stag',null);
        	}
        	
        } 

//------------- End Section 1 -------------------------------------------------------------------
    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

//check if a value is in the array
function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}