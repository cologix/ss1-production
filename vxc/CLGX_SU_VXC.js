nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_VXC.js
//	Script Name:	CLGX_SU_VXC
//	Script Id:		customscript_clgx_su_vxc
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Virtual Cross Connect
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		04/18/2017
//-------------------------------------------------------------------------------------------------
function beforeLoad(type, form) {
    try {

        nlapiLogExecution('DEBUG', 'User Event - Before Load', '|-------------STARTED--------------|');

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Add Field STAG
//-----------------------------------------------------------------------------------------------------------------

     //   form.getField('custrecord_clgx_vxc_intermarket_stag').setDisplayType('hidden');
        if(type!='view') {
            //form.getField('custrecord_clgx_vxc_stag').setDisplayType('hidden');
            var stag = form.addField('custpage_clgx_vxc_stag', 'select', 'CLGX STAG');
           // stag.setMandatory(true);
        }
        //  if(type=='edit'){
        //     stag.addSelectOption(nlapiGetFieldValue('custrecord_clgx_vxc_stag'), nlapiGetFieldValue('custrecord_clgx_vxc_stag'));
        //    }
        if(type=='view') {
            var stag = nlapiGetFieldValue('custrecord_clgx_vxc_stag');
            nlapiSetFieldValue('custpage_clgx_vxc_stag', stag);
        }
    } catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function beforeSubmit (type) {
    try {
        var currentContext = nlapiGetContext();
        if (currentContext.getExecutionContext() == 'userinterface') {

            stRole = currentContext.getRole();
            var stag= nlapiGetFieldValue('custpage_clgx_vxc_stag');
            nlapiSetFieldValue('custrecord_clgx_vxc_stag', stag);

            // get new record ports to compare
            var aPort = nlapiGetFieldValue('custrecord_clgx_a_end_port');
            var bPort = nlapiGetFieldValue('custrecord_clgx_b_end_port');
            var cPort = nlapiGetFieldValue('custrecord_clgx_c_end_port');
            var zPort = nlapiGetFieldValue('custrecord_clgx_z_end_port');
            var vlan = nlapiGetFieldValue('custrecord_clgx_xc_vlan');
            var speed = nlapiGetFieldValue('custrecord_clgx_speed');

            var aPortName = nlapiGetFieldText('custrecord_clgx_a_end_port');
            var bPortName = nlapiGetFieldText('custrecord_clgx_b_end_port');
            var cPortName = nlapiGetFieldText('custrecord_clgx_c_end_port');
            var zPortName = nlapiGetFieldText('custrecord_clgx_z_end_port');
            var vlanName = nlapiGetFieldText('custrecord_clgx_xc_vlan');
            var speedName = nlapiGetFieldText('custrecord_clgx_speed');

            var xcType = nlapiGetFieldValue('custrecord_cologix_xc_circuit_type');
            var xcTypeName = nlapiGetFieldText('custrecord_cologix_xc_circuit_type');

            var xc = nlapiGetRecordId();
            var service = nlapiGetFieldValue('custrecord_cologix_xc_service');
            var so = nlapiGetFieldValue('custrecord_xconnect_service_order');
            var customer = nlapiGetFieldValue('custrecord_cologix_carrier_name');

            var xcName = nlapiGetFieldValue('name');
            var serviceName = nlapiGetFieldText('custrecord_cologix_xc_service');
            var soName= nlapiGetFieldText('custrecord_xconnect_service_order');
            var customerName = nlapiGetFieldText('custrecord_cologix_carrier_name');

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Details:	If XC is deleted, make selected ports available and clear XC, Service, SO and Customer
//-----------------------------------------------------------------------------------------------------------------

            if (type == 'delete') {
                if ((stRole == -5 || stRole == 3) ) {

                    if(aPort !=null && aPort != ''){
                        nlapiSubmitField('customrecord_clgx_active_port', aPort, ['custrecord_clgx_active_port_status','custrecord_clgx_active_port_xc','custrecord_clgx_active_port_sequence','custrecord_clgx_active_port_service','custrecord_clgx_active_port_so','custrecord_clgx_active_port_customer'], [2,'','','','','']);
                    }
                    if(bPort !=null && bPort != ''){
                        nlapiSubmitField('customrecord_clgx_active_port', bPort, ['custrecord_clgx_active_port_status','custrecord_clgx_active_port_xc','custrecord_clgx_active_port_sequence','custrecord_clgx_active_port_service','custrecord_clgx_active_port_so','custrecord_clgx_active_port_customer'], [2,'','','','','']);
                    }
                    if(cPort !=null && cPort != ''){
                        nlapiSubmitField('customrecord_clgx_active_port', cPort, ['custrecord_clgx_active_port_status','custrecord_clgx_active_port_xc','custrecord_clgx_active_port_sequence','custrecord_clgx_active_port_service','custrecord_clgx_active_port_so','custrecord_clgx_active_port_customer'], [2,'','','','','']);
                    }
                    if(zPort !=null && zPort != ''){
                        nlapiSubmitField('customrecord_clgx_active_port', zPort, ['custrecord_clgx_active_port_status','custrecord_clgx_active_port_xc','custrecord_clgx_active_port_sequence','custrecord_clgx_active_port_service','custrecord_clgx_active_port_so','custrecord_clgx_active_port_customer'], [2,'','','','','']);
                    }
                    if(vlan !=null && vlan != ''){
                        nlapiSubmitField('customrecord_clgx_vlan', vlan, ['custrecord_clgx_vlan_status','custrecord_clgx_vlan_xc','custrecord_clgx_vlan_service','custrecord_clgx_vlan_service_order','custrecord_clgx_vlan_customer'], [2,'','','','']);
                    }
                }
                else{ // if other role than admin
                    // if XC have or had switches do not allow delete
                    if(xcType==19 || xcType==20 || xcType==21 || xcType==22 || xcType==23 || xcType==24 || xcTypeOld==19 || xcTypeOld==20 || xcTypeOld==21 || xcTypeOld==22 || xcTypeOld==23 || xcTypeOld==24){
                        var arrParam = new Array();
                        arrParam['custscript_internal_message'] = 'You can\'t delete a cross connect that have or had configured switches and VLAN. Please contact your network administrator.';
                        nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
                    }
                }
            }


            if (type == 'create' || type == 'edit') {

// look for the market facility and populate the hidden field market on XC ============================================================================================================
                var facility = nlapiGetFieldValue('custrecord_clgx_xc_facility');
                var market = nlapiLookupField('customrecord_cologix_facility', facility, 'custrecord_clgx_facilty_market');
                nlapiSetFieldValue('custrecord_cologix_xc_market', market);

//  populate fields used by email templates ============================================================================================================
                if(aPort != null && aPort != ''){
                    var columns = nlapiLookupField('customrecord_clgx_active_port', aPort, ['custrecord_clgx_active_port_nbr','custrecord_clgx_active_port_equip_make']);
                    var portNbrA = columns.custrecord_clgx_active_port_nbr;
                    var equipTypeA = columns.custrecord_clgx_active_port_equip_make;
                    if(equipTypeA == 'Cisco'){
                        var strAend = 'interface FastEthernet0/' + portNbrA;
                    }
                    else{
                        portNbrA = portNbrA - 1;
                        var strAend = 'interfaces ge-0/0/' + portNbrA;
                    }
                    nlapiSetFieldValue('custrecord_clgx_a_end', strAend);
                }
                else{
                    nlapiSetFieldValue('custrecord_clgx_a_end', '');
                }

                if(bPort != null && bPort != ''){
                    var columns = nlapiLookupField('customrecord_clgx_active_port', bPort, ['custrecord_clgx_active_port_nbr','custrecord_clgx_active_port_equip_make']);
                    var portNbrB = columns.custrecord_clgx_active_port_nbr;
                    var equipTypeB = columns.custrecord_clgx_active_port_equip_make;
                    if(equipTypeB == 'Cisco'){
                        var strBend = 'interface FastEthernet0/' + portNbrB;
                    }
                    else{
                        portNbrB = portNbrB - 1;
                        var strBend = 'interfaces ge-0/0/' + portNbrB;
                    }
                    nlapiSetFieldValue('custrecord_clgx_b_end', strBend);
                }
                else{
                    nlapiSetFieldValue('custrecord_clgx_b_end', '');
                }

                if(cPort != null && cPort != ''){
                    var columns = nlapiLookupField('customrecord_clgx_active_port', cPort, ['custrecord_clgx_active_port_nbr','custrecord_clgx_active_port_equip_make']);
                    var portNbrC = columns.custrecord_clgx_active_port_nbr;
                    var equipTypeC = columns.custrecord_clgx_active_port_equip_make;
                    if(equipTypeC == 'Cisco'){
                        var strCend = 'interface FastEthernet0/' + portNbrC;
                    }
                    else{
                        portNbrC = portNbrC - 1;
                        var strCend = 'interfaces ge-0/0/' + portNbrC;
                    }
                    nlapiSetFieldValue('custrecord_clgx_c_end', strCend);
                }
                else{
                    nlapiSetFieldValue('custrecord_clgx_c_end', '');
                }

                if(zPort != null && zPort != ''){
                    var columns = nlapiLookupField('customrecord_clgx_active_port', zPort, ['custrecord_clgx_active_port_nbr','custrecord_clgx_active_port_equip_make']);
                    var portNbrZ = columns.custrecord_clgx_active_port_nbr;
                    var equipTypeZ = columns.custrecord_clgx_active_port_equip_make;
                    if(equipTypeZ == 'Cisco'){
                        var strZend = 'interface FastEthernet0/' + portNbrZ;
                    }
                    else{
                        portNbrZ = portNbrZ - 1;
                        var strZend = 'interfaces ge-0/0/' + portNbrZ;
                    }
                    nlapiSetFieldValue('custrecord_clgx_z_end', strZend);
                }
                else{
                    nlapiSetFieldValue('custrecord_clgx_z_end', '');
                }

// update XC, SO, Service and Customer on Ports  ============================================================================================================
                if(aPort !=null && aPort != ''){
                    nlapiSubmitField('customrecord_clgx_active_port', aPort, ['custrecord_clgx_active_port_status','custrecord_clgx_active_port_xc','custrecord_clgx_active_port_sequence','custrecord_clgx_active_port_service','custrecord_clgx_active_port_so','custrecord_clgx_active_port_customer'], [1,xc,1,service,so,customer]);
                }
                if(bPort !=null && bPort != ''){
                    nlapiSubmitField('customrecord_clgx_active_port', bPort, ['custrecord_clgx_active_port_status','custrecord_clgx_active_port_xc','custrecord_clgx_active_port_sequence','custrecord_clgx_active_port_service','custrecord_clgx_active_port_so','custrecord_clgx_active_port_customer'], [1,xc,2,service,so,customer]);
                }
                if(cPort !=null && cPort != ''){
                    nlapiSubmitField('customrecord_clgx_active_port', cPort, ['custrecord_clgx_active_port_status','custrecord_clgx_active_port_xc','custrecord_clgx_active_port_sequence','custrecord_clgx_active_port_service','custrecord_clgx_active_port_so','custrecord_clgx_active_port_customer'], [1,xc,3,service,so,customer]);
                }
                if(zPort !=null && zPort != ''){
                    nlapiSubmitField('customrecord_clgx_active_port', zPort, ['custrecord_clgx_active_port_status','custrecord_clgx_active_port_xc','custrecord_clgx_active_port_sequence','custrecord_clgx_active_port_service','custrecord_clgx_active_port_so','custrecord_clgx_active_port_customer'], [1,xc,10,service,so,customer]);
                }
                if(vlan !=null && vlan != ''){
                    nlapiSubmitField('customrecord_clgx_vlan', vlan, ['custrecord_clgx_vlan_status','custrecord_clgx_vlan_xc','custrecord_clgx_vlan_service','custrecord_clgx_vlan_service_order','custrecord_clgx_vlan_customer'], [1,xc,service,so,customer]);
                }

            }


            if (type == 'edit') {

                // get old record ports to compare
                var oldPort = nlapiGetOldRecord();

                var aPortOld = oldPort.getFieldValue('custrecord_clgx_a_end_port');
                var bPortOld = oldPort.getFieldValue('custrecord_clgx_b_end_port');
                var cPortOld = oldPort.getFieldValue('custrecord_clgx_c_end_port');
                var zPortOld = oldPort.getFieldValue('custrecord_clgx_z_end_port');
                var vlanOld = oldPort.getFieldValue('custrecord_clgx_xc_vlan');
                var speedOld = oldPort.getFieldValue('custrecord_clgx_speed');

                var aPortNameOld = oldPort.getFieldText('custrecord_clgx_a_end_port');
                var bPortNameOld = oldPort.getFieldText('custrecord_clgx_b_end_port');
                var cPortNameOld = oldPort.getFieldText('custrecord_clgx_c_end_port');
                var zPortNameOld = oldPort.getFieldText('custrecord_clgx_z_end_port');
                var vlanNameOld = oldPort.getFieldText('custrecord_clgx_xc_vlan');
                var speedNameOld = oldPort.getFieldText('custrecord_clgx_speed');

                var xcTypeOld = oldPort.getFieldValue('custrecord_cologix_xc_circuit_type');
                var xcTypeNameOld = oldPort.getFieldText('custrecord_cologix_xc_circuit_type');

                var serviceOld = oldPort.getFieldValue('custrecord_cologix_xc_service');
                var soOld = oldPort.getFieldValue('custrecord_xconnect_service_order');
                var customerOld = oldPort.getFieldValue('custrecord_cologix_carrier_name');

                var serviceNameOld = oldPort.getFieldText('custrecord_cologix_xc_service');
                var soNameOld = oldPort.getFieldText('custrecord_xconnect_service_order');
                var customerNameOld = oldPort.getFieldText('custrecord_cologix_carrier_name');

                // A PORT ==================================================================================================
                // if no new port and old port, or if different, make the old one available
                if(((aPort == null || aPort == '') && (aPortOld !=null && aPortOld != '')) || ((aPort !=null && aPort != '') && (aPortOld !=null && aPortOld != '') && (aPort != aPortOld))){
                    nlapiSubmitField('customrecord_clgx_active_port', aPortOld, ['custrecord_clgx_active_port_status','custrecord_clgx_active_port_xc','custrecord_clgx_active_port_sequence','custrecord_clgx_active_port_service','custrecord_clgx_active_port_so','custrecord_clgx_active_port_customer'], [2,'','','','','']);
                }
                // B PORT ==================================================================================================
                // if new port is different that the old one, make the old one available and the new one provisioned
                if(((bPort == null || bPort == '') && (bPortOld !=null && bPortOld != '')) || ((bPort !=null && bPort != '') && (bPortOld !=null && bPortOld != '') && (bPort != bPortOld))){
                    nlapiSubmitField('customrecord_clgx_active_port', bPortOld, ['custrecord_clgx_active_port_status','custrecord_clgx_active_port_xc','custrecord_clgx_active_port_sequence','custrecord_clgx_active_port_service','custrecord_clgx_active_port_so','custrecord_clgx_active_port_customer'], [2,'','','','','']);
                }
                // C PORT ==================================================================================================
                // if new port is different that the old one, make the old one available and the new one provisioned
                if(((cPort == null || cPort == '') && (cPortOld !=null && cPortOld != '')) || ((cPort !=null && cPort != '') && (cPortOld !=null && cPortOld != '') && (cPort != cPortOld))){
                    nlapiSubmitField('customrecord_clgx_active_port', cPortOld, ['custrecord_clgx_active_port_status','custrecord_clgx_active_port_xc','custrecord_clgx_active_port_sequence','custrecord_clgx_active_port_service','custrecord_clgx_active_port_so','custrecord_clgx_active_port_customer'], [2,'','','','','']);
                }
                // Z PORT ==================================================================================================
                // if new port is different that the old one, make the old one available and the new one unavailable
                if(((zPort == null || zPort == '') && (zPortOld !=null && zPortOld != '')) || ((zPort !=null && zPort != '') && (zPortOld !=null && zPortOld != '') && (zPort != zPortOld))){
                    nlapiSubmitField('customrecord_clgx_active_port', zPortOld, ['custrecord_clgx_active_port_status','custrecord_clgx_active_port_xc','custrecord_clgx_active_port_sequence','custrecord_clgx_active_port_service','custrecord_clgx_active_port_so','custrecord_clgx_active_port_customer'], [2,'','','','','']);
                }
                // VLAN PORT ==================================================================================================
                // if new port is different that the old one, make the old one available and the new one provisioned
                if(((vlan == null || vlan == '') && (vlanOld !=null && vlanOld != '')) || ((vlan !=null && vlan != '') && (vlanOld !=null && vlanOld != '') && (vlan != vlanOld))){
                    nlapiSubmitField('customrecord_clgx_vlan', vlanOld, ['custrecord_clgx_vlan_status','custrecord_clgx_vlan_xc','custrecord_clgx_vlan_service','custrecord_clgx_vlan_service_order','custrecord_clgx_vlan_customer'], [2,'','','','']);
                }

//------------- Begin Section 2 -----------------------------------------------------------------------------------
// Details:	Send email to Juniper and Cisco NOCs and to user if switches changed
//-----------------------------------------------------------------------------------------------------------------
                var arrCisco = new Array();
                var arrJuniper = new Array();
                var arrCiscoOld = new Array();
                var arrJuniperOld = new Array();

                var informCiscoNOC = 0;
                var informJuniperNOC = 0;

                var xcTypeModif = 'No';
                var aModif = 'No';
                var bModif = 'No';
                var cModif = 'No';
                var zModif = 'No';
                var vlanModif = 'No';
                var speedModif = 'No';

// verify equipment Port A ====================================================================================
                if(aPortOld != aPort){
                    aModif = 'Yes';
                }
                if(aPort != null && aPort != ''){
                    var columns = nlapiLookupField('customrecord_clgx_active_port', aPort, ['custrecord_clgx_active_port_equip_make']);
                    var equipMake = columns.custrecord_clgx_active_port_equip_make;
                    if(equipMake == 1){ // Cisco
                        arrCisco.push(aPort);
                    }
                    else{ // Juniper
                        arrJuniper.push(aPort);
                    }
                }
// verify equipment old Port A ====================================================================================
                if(aPortOld != null && aPortOld != ''){
                    var columns = nlapiLookupField('customrecord_clgx_active_port', aPortOld, ['custrecord_clgx_active_port_equip_make']);
                    var equipMake = columns.custrecord_clgx_active_port_equip_make;
                    if(equipMake == 1){ // Cisco
                        arrCiscoOld.push(aPortOld);
                    }
                    else{ // Juniper
                        arrJuniperOld.push(aPortOld);
                    }
                }

// verify equipment Port B ====================================================================================
                if(bPortOld != bPort){
                    bModif = 'Yes';
                }
                if(bPort != null && bPort != ''){
                    var columns = nlapiLookupField('customrecord_clgx_active_port', bPort, ['custrecord_clgx_active_port_equip_make']);
                    var equipMake = columns.custrecord_clgx_active_port_equip_make;
                    if(equipMake == 1){ // Cisco
                        arrCisco.push(bPort);
                    }
                    else{ // Juniper
                        arrJuniper.push(bPort);
                    }
                }
// verify equipment old Port B ====================================================================================
                if(bPortOld != null && bPortOld != ''){
                    var columns = nlapiLookupField('customrecord_clgx_active_port', bPortOld, ['custrecord_clgx_active_port_equip_make']);
                    var equipMake = columns.custrecord_clgx_active_port_equip_make;
                    if(equipMake == 1){ // Cisco
                        arrCiscoOld.push(bPortOld);
                    }
                    else{ // Juniper
                        arrJuniperOld.push(bPortOld);
                    }
                }
// verify equipment Port C ====================================================================================
                if(cPortOld != cPort){
                    cModif = 'Yes';
                }
                if(cPort != null && cPort != ''){
                    var columns = nlapiLookupField('customrecord_clgx_active_port', cPort, ['custrecord_clgx_active_port_equip_make']);
                    var equipMake = columns.custrecord_clgx_active_port_equip_make;
                    if(equipMake == 1){ // Cisco
                        arrCisco.push(cPort);
                    }
                    else{ // Juniper
                        arrJuniper.push(cPort);
                    }
                }
// verify equipment old Port C ====================================================================================
                if(cPortOld != null && cPortOld != ''){
                    var columns = nlapiLookupField('customrecord_clgx_active_port', cPortOld, ['custrecord_clgx_active_port_equip_make']);
                    var equipMake = columns.custrecord_clgx_active_port_equip_make;
                    if(equipMake == 1){ // Cisco
                        arrCiscoOld.push(cPortOld);
                    }
                    else{ // Juniper
                        arrJuniperOld.push(cPortOld);
                    }
                }
// verify equipment Port Z ====================================================================================
                if(zPortOld != zPort){
                    zModif = 'Yes';
                }
                // verify equipment type for new Z
                if(zPort != null && zPort != ''){
                    var columns = nlapiLookupField('customrecord_clgx_active_port', zPort, ['custrecord_clgx_active_port_equip_make']);
                    var equipMake = columns.custrecord_clgx_active_port_equip_make;
                    if(equipMake == 1){ // Cisco
                        arrCisco.push(zPort);
                    }
                    else{ // Juniper
                        arrJuniper.push(zPort);
                    }
                }
// verify equipment old Port Z ====================================================================================
                if(zPortOld != null && zPortOld != ''){
                    var columns = nlapiLookupField('customrecord_clgx_active_port', zPortOld, ['custrecord_clgx_active_port_equip_make']);
                    var equipMake = columns.custrecord_clgx_active_port_equip_make;
                    if(equipMake == 1){ // Cisco
                        arrCiscoOld.push(zPortOld);
                    }
                    else{ // Juniper
                        arrJuniperOld.push(zPortOld);
                    }
                }
// verify if xcType, VLAN and speed changed  ====================================================================================
                if(xcTypeOld != xcType){
                    xcTypeModif = 'Yes';
                }
                if(vlanOld != vlan){
                    vlanModif = 'Yes';
                }
                if(speedOld != speed){
                    speedModif = 'Yes';
                }
// if any modifs and arrCisco or arrJuniper no nulls  ====================================================================================
                if(xcTypeModif == 'Yes' || aModif == 'Yes' || bModif == 'Yes' || cModif == 'Yes' || zModif == 'Yes' || vlanModif == 'Yes' || speedModif == 'Yes'){
                    if(arrCisco.length > 0 || arrCiscoOld.length > 0 ){
                        informCiscoNOC = 1;
                    }
                    if(arrJuniper.length > 0 || arrJuniperOld.length > 0 ){
                        informJuniperNOC = 1;
                    }
                }
// if any NOC have to be informed, send emails  ====================================================================================
                if(informCiscoNOC == 1 || informJuniperNOC == 1){

                    var userId = nlapiGetUser();
                    var columns = nlapiLookupField('employee', userId, ['entityid']);
                    var userName = columns.entityid;

                    var reportSubject = 'Network Changes by ' + userName + ' to customer ' + customerName + ' ( XC : ' + xcName + ' / Service : ' + serviceName + ' / SO : ' + soName + ')';
                    var  reportBody = 'The following network changes were made by ' + userName + ' to customer ' + customerName + ' ( XC : ' + xcName + ' / Service : ' + serviceName + ' / SO : ' + soName + ')\n';
                    reportBody += '<br>';
                    reportBody += '<table border="1" cellpadding="5">';
                    reportBody += '<tr><td>Field</td><td>Changed</td><td>Old Value</td><td>New Value</td></tr>';
                    reportBody += '<tr><td>Circuit Type</td><td>' + xcTypeModif + '</td><td>' + xcTypeNameOld + '</td><td>' + xcTypeName + '</td></tr>';
                    reportBody += '<tr><td>Access Switch 1</td><td>' + aModif + '</td><td>' + aPortNameOld + '</td><td>' + aPortName + '</td></tr>';
                    reportBody += '<tr><td>Distribution Switch 1</td><td>' + bModif + '</td><td>' + bPortNameOld + '</td><td>' + bPortName + '</td></tr>';
                    reportBody += '<tr><td>Distribution Switch 2</td><td>' + cModif + '</td><td>' + cPortNameOld + '</td><td>' + cPortName + '</td></tr>';
                    reportBody += '<tr><td>Access Switch 2</td><td>' + zModif + '</td><td>' + zPortNameOld + '</td><td>' + zPortName + '</td></tr>';
                    reportBody += '<tr><td>VLAN#</td><td>' + vlanModif + '</td><td>' + vlanNameOld + '</td><td>' + vlanName + '</td></tr>';
                    reportBody += '<tr><td>Speed</td><td>' + speedModif + '</td><td>' + speedNameOld + '</td><td>' + speedName + '</td></tr>';
                    reportBody += '</table>';
                    reportBody += '<br>';

                    if(informCiscoNOC == 1){
                    	clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_vxc_admin", reportSubject, reportBody);
                        //nlapiSendEmail(userId,71418,reportSubject,reportBody,null,null,null,null,true);
                    }
                    if(informJuniperNOC == 1){
                    	clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_vxc_admin", reportSubject, reportBody);
                        //nlapiSendEmail(userId,-5,reportSubject,reportBody,null,null,null,null,true);
                    }

                    clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_vxc_admin", reportSubject, reportBody);
                    /*nlapiSendEmail(userId,71418,reportSubject,reportBody,null,null,null,null,true);
                    nlapiSendEmail(userId,2406,reportSubject,reportBody,null,null,null,null,true);*/
                    //nlapiSendEmail(userId,userId,reportSubject,reportBody,null,null,null,null);
                }
            }

        }

        var usageConsumtion = 1000 - parseInt(nlapiGetContext().getRemainingUsage());
        nlapiLogExecution('DEBUG', 'Finish XC Script', 'Usage - '+ usageConsumtion);


    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function afterSubmit (type) {
    try {
        var currentContext = nlapiGetContext();
        if (currentContext.getExecutionContext() == 'userinterface') {

            if (type == 'create') {

                var xc = nlapiGetRecordId();
                var recXC = nlapiLoadRecord('customrecord_cologix_crossconnect', xc);
                var aPort = recXC.getFieldValue('custrecord_clgx_a_end_port');
                var bPort = recXC.getFieldValue('custrecord_clgx_b_end_port');
                var cPort = recXC.getFieldValue('custrecord_clgx_c_end_port');
                var zPort = recXC.getFieldValue('custrecord_clgx_z_end_port');
                var vlan = recXC.getFieldValue('custrecord_clgx_xc_vlan');

                if(aPort !=null && aPort != ''){
                    nlapiSubmitField('customrecord_clgx_active_port', aPort, 'custrecord_clgx_active_port_xc', xc);
                }
                if(bPort !=null && bPort != ''){
                    nlapiSubmitField('customrecord_clgx_active_port', bPort, 'custrecord_clgx_active_port_xc', xc);
                }
                if(cPort !=null && cPort != ''){
                    nlapiSubmitField('customrecord_clgx_active_port', cPort, 'custrecord_clgx_active_port_xc', xc);
                }
                if(zPort !=null && zPort != ''){
                    nlapiSubmitField('customrecord_clgx_active_port', zPort, 'custrecord_clgx_active_port_xc', xc);
                }
                if(vlan !=null && vlan != ''){
                    nlapiSubmitField('customrecord_clgx_vlan', parseInt(vlan), ['custrecord_clgx_vlan_xc'], [xc]);
                }
            }
            if (type == 'create' || type == 'edit' ) {
                var xc = nlapiGetRecordId();
                var recXC = nlapiLoadRecord('customrecord_cologix_crossconnect', xc);
                var xc_type = recXC.getFieldValue('custrecord_cologix_xc_type');
                if(xc_type==20 || xc_type==21 || xc_type==22 || xc_type==19 ||xc_type==23 || xc_type==24)
                {
                    var aPort = recXC.getFieldValue('custrecord_clgx_a_end_port');
                    if(aPort !=null && aPort != ''){
                        var record = nlapiLoadRecord('customrecord_clgx_active_port', aPort);
                        //record.setFieldValue('custrecord_clgx_active_port_equip_type', 1);
                        record.setFieldValue('custrecord_clgx_active_port_status', 1);
                        nlapiSubmitRecord(record, false, true);
                    }
                }
                if(xc_type==28 || xc_type==29 || xc_type==30 || xc_type==31)
                {
                    var aPort = recXC.getFieldValue('custrecord_clgx_a_end_port');
                    if(aPort !=null && aPort != ''){
                        var record = nlapiLoadRecord('customrecord_clgx_active_port', aPort);
                        //record.setFieldValue('custrecord_clgx_active_port_equip_type', 3);
                        record.setFieldValue('custrecord_clgx_active_port_status', 5);
                        nlapiSubmitRecord(record, false, true);
                    }
                }
            }
        }

    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }

}