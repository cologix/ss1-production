nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Task.js
//	Script Name:	CLGX_SU_Task
//	Script Id:		customscript_clgx_su_task
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Sales Order - Sandbox | Production
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		10/05/2015
//-------------------------------------------------------------------------------------------------

function beforeLoad(type, form) {
    try {


    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function beforeSubmit(type) {
    try {
    	
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function afterSubmit(type){
    try {
    	nlapiLogExecution('DEBUG', 'flexapi error: ', 'nothiing');
		var currentContext = nlapiGetContext();
		var userid = nlapiGetUser();
		var roleid = currentContext.getRole();
		var taskid = nlapiGetRecordId();
		
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Details:	Update due date on activities custom record for KPI.
// Created:	10/05/2015
//-----------------------------------------------------------------------------------------------------------------

		var duedate = nlapiGetFieldValue('duedate');
		var tasktype = nlapiGetFieldText('custeventcustevent_clgx_task_type_field');
		
		if (type == 'xedit' || (currentContext.getExecutionContext() == 'userinterface' && type == 'edit')) {
		
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_sales_activ_id",null,"equalto",taskid));
			var searchActivity = nlapiSearchRecord('customrecord_cologix_sales_activities', null, arrFilters, arrColumns);
			
			if(searchActivity != null){
				var internalid = parseInt(searchActivity[0].getValue('internalid',null,null));
				nlapiSubmitField('customrecord_cologix_sales_activities', internalid, ['custrecord_clgx_sales_activ_date','custrecord_clgx_sales_activ_task_type'], [duedate,tasktype]);
	        }
		}
    	
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

