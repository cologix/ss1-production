nlapiLogExecution("audit","FLOStart",new Date().getTime());
function create_moratorium(){
    try{
        //------------- Begin Section 1 -------------------------------------------------------------------
        //	Details:	Get the values and create a new Moratorium
        //	
        //-------------------------------------------------------------------------------------------------
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');
        var context = nlapiGetContext();
        //retrieve script parameters
//nlapiSendEmail(206211, 206211,'date','bodyTEST:',null,null,null,null); // Send email to Catalina

        var eventName = nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_clgx_mor_event_name');
        var facility = nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_clgx_mor_facility');
        var dateS= nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_clgx_mor_start_date');
        var timeS = nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_clgx_mor_start_time');
//nlapiSendEmail(206211, 206211,'date',timeS,null,null,null,null);

        var dateE = nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_clgx_mor_end_date');
        var timeE = nlapiGetContext().getSetting('SCRIPT','custscriptcustscript_clgx_mor_end_time');
         var dateSArr=dateS.split('-');
        var dateEArr=dateE.split('-');
 
        //create a new Moratorium Record
                           var newRecordMoratorium = nlapiCreateRecord('customrecord_clgx_moratorium');
                            newRecordMoratorium.setFieldValue('custrecord_clgx_mor_event_name', eventName);
                            newRecordMoratorium.setFieldValue('custrecord_clgx_mor_facility', facility);
                            newRecordMoratorium.setFieldValue('custrecord_clgx_mor_start_date', dateSArr[1]+'/'+dateSArr[2]+'/'+dateSArr[0]);
                            newRecordMoratorium.setFieldValue('custrecord_clgx_mor_start_time', timeS);
                            newRecordMoratorium.setFieldValue('custrecord_clgx_mor_end_date', dateEArr[1]+'/'+dateEArr[2]+'/'+dateEArr[0]);
                            newRecordMoratorium.setFieldValue('custrecord_clgx_mor_end_time', timeE);
                            nlapiSubmitRecord(newRecordMoratorium, false,true);
           
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


