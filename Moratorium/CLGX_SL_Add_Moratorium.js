nlapiLogExecution("audit","FLOStart",new Date().getTime());
function addMoratorium(request, response){
        //  nlapiSendEmail(206211, 206211,'date','hereSL',null,null,null,null);
        var name = request.getParameter('name');
        var facility = request.getParameter('facility');
        var sdate = request.getParameter('sdate');
        var stime = request.getParameter('stime');

        var edate = request.getParameter('edate');
        var etime = request.getParameter('etime');
        var stimeArr=stime.split(":");
        if(parseInt(stimeArr[0])>12){
        	stime=(stimeArr[0]-12)+":"+stimeArr[1]+" pm";
        }else
        {
          if(stimeArr[0]==0)
          {
             stimeArr[0]=12;
          }
        	stime=stimeArr[0]+":"+stimeArr[1]+" am";
        }
         var etimeArr=etime.split(":");
        if(etimeArr[0]>12){
        	etime=(etimeArr[0]-12)+":"+etimeArr[1]+" pm";
        }else
        {
   if(etimeArr[0]==0)
          {
             etimeArr[0]=12;
          }
        	etime=etimeArr[0]+":"+etimeArr[1]+" am";
        }

         var params = {
                            custscriptcustscript_clgx_mor_event_name:name,
                            custscriptcustscript_clgx_mor_facility:facility,
                            custscriptcustscript_clgx_mor_start_date:sdate,
                            custscriptcustscript_clgx_mor_start_time:stime,
                            custscriptcustscript_clgx_mor_end_date:edate,
                            custscriptcustscript_clgx_mor_end_time:etime
                        };
         
         nlapiLogExecution("DEBUG", "params", JSON.stringify(params));
//nlapiSendEmail(206211, 206211,'datecustscript_clgx_mor_start_date','body:'+sdate,null,null,null,null); // Send email to Catalina
//hit the scheduled script for Moratorium
                        nlapiScheduleScript('customscript_clgx_ss_create_moratorium',null, params);


   }


