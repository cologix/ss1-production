nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	        CLGX_SL_ISC_PeriodA.js
//	Script Name:	CLGX_SL_ISC_PeriodA
//	Script Id:		customscript_clgx_sl_isc_perioda
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com, adapted by Derek Hitchman - derek@erpadvisorsgroup.com
//	Created:		06/16/2015, Live 06/25/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=515&deploy=1
//     Description:       This Suitelet is the 2nd level down Suitelet for the Invoice Stare & Compare report. It is embedded as an iframe within the CLGX_SL_ISC_Frame.js Suitelet. It displays the first list of periods to select from.
//-------------------------------------------------------------------------------------------------

function suitelet_isc_periods (request, response){
	try {
		var objFile = nlapiLoadFile(2747136);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{PeriodsJSON}','g'), getPeriodsJSON());
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}





function getPeriodsJSON(){
	

	var cutOffDate =  new Date();
	cutOffDate.setDate(cutOffDate.getDate()-500);
	
	var searchInvoices = Array();
	
	var arrColumns = new Array();
	col1= new nlobjSearchColumn('postingperiod',null,'GROUP');
	arrColumns.push(col1);
	

	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("trandate",null,"after",cutOffDate));

	
	var savedsearch = nlapiCreateSearch( 'invoice', arrFilters, arrColumns );

	var resultSetLoc = savedsearch.runSearch();
	


	//var resultSetLoc = searchLocations.runSearch();
	var arrPeriods = new Array();
	resultSetLoc.forEachResult(function(searchResult) {
		var colObj = new Object();
		

		colObj["period"] =  searchResult.getText('postingperiod', null, 'GROUP');
		colObj["periodid"] =  searchResult.getValue('postingperiod', null, 'GROUP');
		colObj["iconCls"] ='month';
		colObj["children"] = new Object();
		arrPeriods.push(colObj);
		return true; // return true to keep iterating
		});


	objTree = new Object();
	objTree["children"]= new Array();	

	for(var a in arrPeriods  ){
		objTree["children"].push(a);
	}
	
	
	
    return JSON.stringify(arrPeriods);
}
	


