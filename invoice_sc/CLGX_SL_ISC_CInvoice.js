nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_ISC_CInvoice.js
//	Script Name:	CLGX_SL_ISC_CInvoice
//	Script Id:		customscript_clgx_sl_isc_cinvoice
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Derek Hitchman - derek@erpadvisorsgroup.com
//	Created:		03/24/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=477&deploy=1
//     Description:       This Suitelet is the 5th level down Suitelet for the Invoice Stare & Compare report. It is embedded as an iframe within the CLGX_SL_ISC_Locations.js Suitelet. It generates the actual list of invoices to be viewed. It pulls the invoices to be displayed for period A and then for period B and then iterates through these lists in memory, joining the respective invoices across the two periods for each customer and forms a JSON data object which is then rendered to screen within the CLGX_HTML_ISC_CInvoice.html page or the CLGX_HTML_ISC_CInvoice_Print.html page depending on if the view=print parameter is set or not. 
//-------------------------------------------------------------------------------------------------

function suitelet_isc_cinvoice (request, response){
	try {

		var locationName = decodeURIComponent(request.getParameter('location_name'));
		var perioda = decodeURIComponent(request.getParameter('perioda'));
		var periodb = decodeURIComponent(request.getParameter('periodb'));
		var view = decodeURIComponent(request.getParameter('view'));

		
		if(locationName) {
			var objFile = nlapiLoadFile(2747134 );
                        if(view == 'print') objFile =  nlapiLoadFile(2990665 );
			var html = objFile.getValue();
			var invoiceData =  getInvoices(perioda, periodb, locationName);
			var invoices =invoiceData[0];

			var invoicesJSON = JSON.stringify(invoices);
			var invoicesCSV = encodeURI(invoiceData[1]);
			html = html.replace(new RegExp('{customerCount}','g'),invoices["children"].length);
			html = html.replace(new RegExp('{view}','g'),view);

			if(invoicesCSV)
				html = html.replace(new RegExp('{invoicesCSV}','g'), invoicesCSV);
			
			if(invoicesJSON)
				html = html.replace(new RegExp('{invoicesJSON}','g'), invoicesJSON);
			else html = 'No results found for this location.';
		}
		else{
			var html = 'Please select a location from the left panel.';
		}
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error 0', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}



function getInvoices(perioda, periodb, locationName){

	var periods = new Array();
	periods.push(perioda);
	periods.push(periodb);
	
	var objTree = new Object();
	objTree["text"] = '.';
	
	
	var currencyMultiplier = new Object();
	currencyMultiplier[1] = 1.0;  //Usd
	
		
	
	//define typical row
	reportRow = function(v) {
	
	//	if(val==null) val= 0.00;
	//	var v = parseFloat(val).toFixed(2);
		
		this.nodeid;
		this.node;
		this.customerid;
		this.cinvoiceid;
		this.cinvoicenbr;
		this.periodid;
		this.periodname;
		
		this.expanded = true;
		this.iconCls;
		this.currency='';
		this.leaf = false;
		this.total =v;
		this.totalDisaster = v;
		this.totalEquiptmentRental= v;
		this.totalEquiptmentSales=v;
		this.totalInstall= v;
		this.totalIntercon= v;
		this.totalNetwork= v;
		this.totalBandwidthBurst= v;
		this.totalOtherNonRec= v;
		this.totalOtherRec= v;
		this.totalPower= v;
		this.totalPowerUsage= v;
		this.totalRemoteHands= v;
		this.totalSpace= v;
		this.totalVirtualInterconn= v;
		this.children = new Array();
		this.sortval ='';
		
	
		
	}
	
	
	
	//Define hash objects

	var customerMap = new Object();
	var ciMap = new Object();
	var invMap = new Object();
	var ciBatches = new Array(); // list of CIs to pull
	var ciList = new Object();
	
	html='';
	
	
	
	//pull invoice header data
	var invoiceList = new Array();
	var invoiceBodyMap= new Object();
	
	
	
	//New invoice data period A
    var arrColumns = new Array();
    var arrFilters=new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    arrColumns.push(new nlobjSearchColumn('custbody_consolidate_inv_nbr',null,null));
    arrColumns.push(new nlobjSearchColumn('custbody_clgx_consolidate_locations',null,null));
    arrColumns.push(new nlobjSearchColumn('postingperiod',null,null));
    arrColumns.push(new nlobjSearchColumn('fxamount',null,null));
    arrColumns.push(new nlobjSearchColumn('tranid',null,null));
    arrColumns.push(new nlobjSearchColumn('entity',null,null));
    arrColumns.push(new nlobjSearchColumn('currency',null,null));
	arrFilters.push(new nlobjSearchFilter("mainline",null,"is",'T'));
	arrFilters.push(new nlobjSearchFilter("postingperiod",null,"is",perioda));
	arrFilters.push(new nlobjSearchFilter("custbody_clgx_consolidate_locations",null,"is",locationName));



	
    SCS.Query('invoice', arrFilters, arrColumns , function(ix, rec, recordset){
    	var id = recordset[rec].getValue( 'internalid', null,null) ;
    	invoiceList.push(  id );
    	

    	var ciBatch = recordset[rec].getValue( 'custbody_consolidate_inv_nbr', null,null);
    	if(ciBatch == null  || ciBatch =='') {
    		ciBatch = recordset[rec].getValue( 'entity', null,null)+ recordset[rec].getValue( 'postingperiod', null,null);
    	}
    	
    	newInvoiceRecord  = new Object();
    	
    	newInvoiceRecord.custbody_consolidate_inv_nbr = recordset[rec].getValue( 'custbody_consolidate_inv_nbr', null,null) ;
    	newInvoiceRecord.currency = recordset[rec].getText( 'currency', null,null) ;
    	newInvoiceRecord.custbody_clgx_consolidate_locations = recordset[rec].getText( 'custbody_clgx_consolidate_locations', null,null) ;
    	newInvoiceRecord.companyId = recordset[rec].getValue( 'entity', null,null) ;
    	newInvoiceRecord.companyName = recordset[rec].getText( 'entity', null,null) ;
    	newInvoiceRecord.companyUnique = newInvoiceRecord.companyId + newInvoiceRecord.custbody_consolidate_inv_nbr.split('.')[1] ; 
    	
    	
    	invoiceBodyMap[id] = newInvoiceRecord;

    	//add CI totals data to CI object 
    	if(!ciList.hasOwnProperty( ciBatch )) {
    		ciList[ciBatch] = new Object();
    		ciList[ciBatch]['total'] = 0.00;
    	}
    	ciList[ciBatch]['total'] = parseFloat(ciList[ciBatch]['total']) + parseFloat(recordset[rec].getValue( 'fxamount', null,null) ) ; 
    	

    	if(!invMap.hasOwnProperty( id )) {
    		invMap[id] = new reportRow(null);
    		invMap[id].iconCls = 'invoice';
    		invMap[id].node = recordset[rec].getValue( 'tranid', null,null);	
    		invMap[id].nodeid = recordset[rec].getValue( 'internalid', null,null);	
    		invMap[id].total =recordset[rec].getValue( 'fxamount', null,null) ; 
    		invMap[id].cinvoicenbr =ciBatch;
    		invMap[id].sortval = recordset[rec].getValue( 'tranid', null,null);
    		
    	}
    	
    });
	
    
	//New invoice data period B
    var arrColumns = new Array();
    var arrFilters=new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    arrColumns.push(new nlobjSearchColumn('custbody_consolidate_inv_nbr',null,null));
    arrColumns.push(new nlobjSearchColumn('custbody_clgx_consolidate_locations',null,null));
    arrColumns.push(new nlobjSearchColumn('postingperiod',null,null));
    arrColumns.push(new nlobjSearchColumn('currency',null,null));
    arrColumns.push(new nlobjSearchColumn('tranid',null,null));
    arrColumns.push(new nlobjSearchColumn('fxamount',null,null));
    arrColumns.push(new nlobjSearchColumn('entity',null,null));
	arrFilters.push(new nlobjSearchFilter("mainline",null,"is",'T'));
	arrFilters.push(new nlobjSearchFilter("postingperiod",null,"is",periodb));
	arrFilters.push(new nlobjSearchFilter("custbody_clgx_consolidate_locations",null,"is",locationName));


	
	
	
    SCS.Query('invoice', arrFilters, arrColumns , function(ix, rec, recordset){
    	var id = recordset[rec].getValue( 'internalid', null,null) ;
    	invoiceList.push(  id );

    	var ciBatch = recordset[rec].getValue( 'custbody_consolidate_inv_nbr', null,null);
    	if(ciBatch == null  || ciBatch =='') {
    		ciBatch = recordset[rec].getValue( 'entity', null,null)+ recordset[rec].getValue( 'postingperiod', null,null);
    	}
    	
    	
    	   	
    	
    	
    	newInvoiceRecord  = new Object();
    	
    	newInvoiceRecord.custbody_consolidate_inv_nbr = recordset[rec].getValue( 'custbody_consolidate_inv_nbr', null,null) ;
    	newInvoiceRecord.currency = recordset[rec].getText( 'currency', null,null) ;
    	newInvoiceRecord.custbody_clgx_consolidate_locations = recordset[rec].getText( 'custbody_clgx_consolidate_locations', null,null) ;
    	newInvoiceRecord.companyId = recordset[rec].getValue( 'entity', null,null) ;
    	newInvoiceRecord.companyName = recordset[rec].getText( 'entity', null,null) ;
    	newInvoiceRecord.companyUnique = newInvoiceRecord.companyId + newInvoiceRecord.custbody_consolidate_inv_nbr.split('.')[1] ; 
    	

    	invoiceBodyMap[id] = newInvoiceRecord;

    	//add CI totals data to CI object 
    	if(!ciList.hasOwnProperty( ciBatch )) {
    		ciList[ciBatch] = new Object();
    		ciList[ciBatch]['total'] = 0.00;
    	}
    	ciList[ciBatch]['total'] = parseFloat(ciList[ciBatch]['total']) + parseFloat(recordset[rec].getValue( 'fxamount', null,null) ) ; 
    	
    	
    	if(!invMap.hasOwnProperty( id )) {
    		invMap[id] = new reportRow(null);
    		invMap[id].iconCls = 'invoice';
    		invMap[id].node = recordset[rec].getValue( 'tranid', null,null);	
    		invMap[id].nodeid = recordset[rec].getValue( 'internalid', null,null);	
    		invMap[id].total =recordset[rec].getValue( 'fxamount', null,null) ; 
    		invMap[id].cinvoicenbr = ciBatch;
    		invMap[id].sortval = recordset[rec].getValue( 'tranid', null,null);
    		
    	}
    });
	
	


	
    
	if(invoiceList.length == 0) {
		objTree["children"]= new Array();	
		return objTree;
	}
	
	
	
	//All invoice line items data
    var arrColumns = new Array();
    var arrFilters=new Array();

    arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('item',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('custcol_cologix_invoice_item_category',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
  //  arrColumns.push(new nlobjSearchColumn('currency',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('fxamount',null,'SUM'));
    arrColumns.push(new nlobjSearchColumn('entity',null,'GROUP'));
   // arrColumns.push(new nlobjSearchColumn('custbody_clgx_consolidate_locations',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('mainline',null,'GROUP'));
	arrFilters.push(new nlobjSearchFilter("mainline",null,"is",'F'));
	
	
	
	//arrFilters.push(new nlobjSearchFilter("postingperiod",null,"anyof",periods));
//	arrFilters.push(new nlobjSearchFilter("custbody_clgx_consolidate_locations",null,"is",locationName));

	arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",invoiceList));
    
	//custbody_clgx_consolidate_locations

    SCS.Query('invoice', arrFilters, arrColumns , function(ix, rec, recordset){

    	var internalid = recordset[rec].getValue( 'internalid', null,'GROUP');
    	var itemid = recordset[rec].getValue( 'item', null,'GROUP');
    	var mainline = recordset[rec].getValue( 'mainline', null,'GROUP');
    	var postingperiod = recordset[rec].getValue('postingperiod', null, 'GROUP');
    	var postingperiodname = recordset[rec].getText('postingperiod', null, 'GROUP');
    	var category = recordset[rec].getValue('custcol_cologix_invoice_item_category', null, 'GROUP');
    	var currency = invoiceBodyMap[internalid].currency;
    	var consolidate_inv_nbr = invoiceBodyMap[internalid].custbody_consolidate_inv_nbr;
    	var location = invoiceBodyMap[internalid].custbody_clgx_consolidate_locations;
    	var companyId = invoiceBodyMap[internalid].companyId
    	var companyName = invoiceBodyMap[internalid].companyName;
    	var companyUnique = invoiceBodyMap[internalid].companyUnique;
    	
    	//var clgx_consolidate_locations = recordset[rec].getText('custbody_clgx_consolidate_locations', null, 'GROUP');

    	
    	var fxamount = recordset[rec].getValue('fxamount', null, 'SUM');

    	var direction = ((postingperiod ==perioda) ? -1.0 : 1.0); 

    	//if not already an entry for customer then create one
    	
    	if(!customerMap.hasOwnProperty( companyUnique )) {
    		customerMap[companyUnique] = new reportRow(0.00);
   		 customerMap[companyUnique].node = companyName;
		 customerMap[companyUnique].nodeid = companyId;
		 customerMap[companyUnique].customerid = companyId;
		 customerMap[companyUnique].iconCls = 'account';
		 customerMap[companyUnique].location = location;
		 customerMap[companyUnique].total = null;
   		 customerMap[companyUnique].sortval = companyName;		 
    	}
    	
    	
    	//html += clgx_consolidate_locations+ "<br>";
    	
    	
    	if(currency) {
    		customerMap[companyUnique].currency = currency;
    	}

    	var  amtSigned = parseFloat(fxamount) *parseFloat(direction);
    	
    	
    	
    	if (mainline!='*'){ //false
    		if(itemid==210) customerMap[companyUnique].totalPowerUsage += amtSigned;
    		else if(itemid==574) customerMap[companyUnique].totalBandwidthBurst += amtSigned;
    		else if(category==1)  customerMap[companyUnique].totalEquiptmentRental += amtSigned;
	    	else if(category==2) customerMap[companyUnique].totalEquiptmentSales += amtSigned;
	    	else if(category==3) customerMap[companyUnique].totalInstall += amtSigned;
	    	else if(category==4) customerMap[companyUnique].totalIntercon += amtSigned;
	    	else if(category==5) customerMap[companyUnique].totalNetwork += amtSigned;
	    	else if(category==6) customerMap[companyUnique].totalOtherNonRec += amtSigned;
	    	else if(category==7) customerMap[companyUnique].totalOtherRec += amtSigned;
	    	else if(category==8) customerMap[companyUnique].totalPower += amtSigned;
	    	else if(category==9) customerMap[companyUnique].totalRemoteHands += amtSigned;
	    	else if(category==10) customerMap[companyUnique].totalSpace += amtSigned;
	    	else if(category==11) customerMap[companyUnique].totalVirtualInterconn += amtSigned;
	    	else if(category==12) customerMap[companyUnique].totalDisaster += amtSigned;
	    	
    		
    		


    		// handle issues with float operations
    		customerMap[companyUnique].totalPowerUsage  = Number(Math.round(customerMap[companyUnique].totalPowerUsage+'e2')+'e-2') ;
    		customerMap[companyUnique].totalBandwidthBurst  = Number(Math.round(customerMap[companyUnique].totalBandwidthBurst+'e2')+'e-2') ;
    		customerMap[companyUnique].totalEquiptmentRental  = Number(Math.round(customerMap[companyUnique].totalEquiptmentRental+'e2')+'e-2') ;
    		customerMap[companyUnique].totalEquiptmentSales  = Number(Math.round(customerMap[companyUnique].totalEquiptmentSales+'e2')+'e-2') ;
    		customerMap[companyUnique].totalInstall  = Number(Math.round(customerMap[companyUnique].totalInstall+'e2')+'e-2') ;
    		customerMap[companyUnique].totalIntercon  = Number(Math.round(customerMap[companyUnique].totalIntercon+'e2')+'e-2') ;
    		customerMap[companyUnique].totalNetwork  = Number(Math.round(customerMap[companyUnique].totalNetwork+'e2')+'e-2') ;
    		customerMap[companyUnique].totalOtherNonRec  = Number(Math.round(customerMap[companyUnique].totalOtherNonRec+'e2')+'e-2') ;
    		customerMap[companyUnique].totalOtherRec  = Number(Math.round(customerMap[companyUnique].totalOtherRec+'e2')+'e-2') ;
    		customerMap[companyUnique].totalPower  = Number(Math.round(customerMap[companyUnique].totalPower+'e2')+'e-2') ;
    		customerMap[companyUnique].totalRemoteHands  = Number(Math.round(customerMap[companyUnique].totalRemoteHands+'e2')+'e-2') ;
    		customerMap[companyUnique].totalSpace  = Number(Math.round(customerMap[companyUnique].totalSpace+'e2')+'e-2') ;
    		customerMap[companyUnique].totalVirtualInterconn  = Number(Math.round(customerMap[companyUnique].totalVirtualInterconn+'e2')+'e-2') ;
    		customerMap[companyUnique].totalDisaster  = Number(Math.round(customerMap[companyUnique].totalDisaster+'e2')+'e-2') ;
    		
    		
	
	    	//unique identifier for CI
	    	var uq = consolidate_inv_nbr;
	    	if(consolidate_inv_nbr== null  || consolidate_inv_nbr=='' ) {
	    		consolidate_inv_nbr = 'Unassigned';
	    		uq =  companyId+postingperiod;
	    	}



	    	//if not already an entry for CI then create one
	    	if(!ciMap.hasOwnProperty( uq)) {
	    		ciMap[uq] = new reportRow(0.00);
	    		ciMap[uq].nodeid = null; //donthave the CI ID
	    		ciMap[uq].node = consolidate_inv_nbr;
	    		ciMap[uq].customerid = companyId;
	    		ciMap[uq].iconCls = 'cinvoice';
	    		ciMap[uq].periodname = postingperiodname;
	    		ciMap[uq].expanded= false; 		
	    		ciMap[uq].total= (ciList[uq]['total']).toFixed(2); 		
	    		ciMap[uq].sortval=  consolidate_inv_nbr.split('.')[1] ; 
	    		ciMap[uq].companyUnique=  companyUnique ; 
	    		

	    	}

	    	
	    	
	    	

	    	
    		if(itemid==210) ciMap[uq].totalPowerUsage += fxamount*1.0;
    		else if(itemid==574) ciMap[uq].totalBandwidthBurst += fxamount*1.0;
	    	else if(category==1)  ciMap[uq].totalEquiptmentRental += fxamount*1.0;
	    	else if(category==2) ciMap[uq].totalEquiptmentSales += fxamount*1.0;
	    	else if(category==3) ciMap[uq].totalInstall += fxamount*1.0;
	    	else if(category==4) ciMap[uq].totalIntercon += fxamount*1.0;
	    	else if(category==5) ciMap[uq].totalNetwork += fxamount*1.0;
	    	else if(category==6) ciMap[uq].totalOtherNonRec += fxamount*1.0;
	    	else if(category==7) ciMap[uq].totalOtherRec += fxamount*1.0;
	    	else if(category==8) ciMap[uq].totalPower += fxamount*1.0;
	    	else if(category==9) ciMap[uq].totalRemoteHands += fxamount*1.0;
	    	else if(category==10) ciMap[uq].totalSpace += fxamount*1.0;
	    	else if(category==11) ciMap[uq].totalVirtualInterconn += fxamount*1.0;
	    	else if(category==12) ciMap[uq].totalDisaster += fxamount*1.0;
	    	

    		
    		// handle issues with float operations
    		ciMap[uq].totalPowerUsage  = Number(Math.round(ciMap[uq].totalPowerUsage+'e2')+'e-2') ;
    		ciMap[uq].totalBandwidthBurst  = Number(Math.round(ciMap[uq].totalBandwidthBurst+'e2')+'e-2') ;
    		ciMap[uq].totalEquiptmentRental  = Number(Math.round(ciMap[uq].totalEquiptmentRental+'e2')+'e-2') ;
    		ciMap[uq].totalEquiptmentSales  = Number(Math.round(ciMap[uq].totalEquiptmentSales+'e2')+'e-2') ;
    		ciMap[uq].totalInstall  = Number(Math.round(ciMap[uq].totalInstall+'e2')+'e-2') ;
    		ciMap[uq].totalIntercon  = Number(Math.round(ciMap[uq].totalIntercon+'e2')+'e-2') ;
    		ciMap[uq].totalNetwork  = Number(Math.round(ciMap[uq].totalNetwork+'e2')+'e-2') ;
    		ciMap[uq].totalOtherNonRec  = Number(Math.round(ciMap[uq].totalOtherNonRec+'e2')+'e-2') ;
    		ciMap[uq].totalOtherRec  = Number(Math.round(ciMap[uq].totalOtherRec+'e2')+'e-2') ;
    		ciMap[uq].totalPower  = Number(Math.round(ciMap[uq].totalPower+'e2')+'e-2') ;
    		ciMap[uq].totalRemoteHands  = Number(Math.round(ciMap[uq].totalRemoteHands+'e2')+'e-2') ;
    		ciMap[uq].totalSpace  = Number(Math.round(ciMap[uq].totalSpace+'e2')+'e-2') ;
    		ciMap[uq].totalVirtualInterconn  = Number(Math.round(ciMap[uq].totalVirtualInterconn+'e2')+'e-2') ;
    		ciMap[uq].totalDisaster  = Number(Math.round(ciMap[uq].totalDisaster+'e2')+'e-2') ;
    		
    		
	    	
	    	
	    	
    	} //end if not mainline
    	
    	
    });
    	
    
    
    
    

    
    //add all invs as children of CI
	for (var i in invMap) {
		
		ciMap[invMap[i].cinvoicenbr].children.push(invMap[i]);
		
	}
    
    
    
    //add all CIs as children of the customer
	for (var c in ciMap) {
		
		 customerMap[ciMap[c].companyUnique].children.push(ciMap[c]);
		
	}
    

	//add all customers to obj tree
	objTree["children"]= new Array();	

	
	for (var cu in customerMap) {
		if(customerMap[cu].children.length > 0
				&& (
						 customerMap[cu].totalPowerUsage.toFixed(2) != 0.00
						|| customerMap[cu].totalBandwidthBurst.toFixed(2)  != 0.00
						|| customerMap[cu].totalDisaster.toFixed(2)  != 0.00
						|| customerMap[cu].totalEquiptmentRental.toFixed(2) != 0.00
						|| customerMap[cu].totalEquiptmentSales.toFixed(2) != 0.00
						|| customerMap[cu].totalInstall.toFixed(2) != 0.00
						|| customerMap[cu].totalIntercon.toFixed(2) != 0.00
						|| customerMap[cu].totalNetwork.toFixed(2) != 0.00
						|| customerMap[cu].totalOtherNonRec.toFixed(2) != 0.00
						|| customerMap[cu].totalOtherRec.toFixed(2) != 0.00
						|| customerMap[cu].totalPower.toFixed(2) != 0.00
						|| customerMap[cu].totalRemoteHands.toFixed(2) != 0.00
						|| customerMap[cu].totalSpace.toFixed(2) != 0.00
						|| customerMap[cu].totalVirtualInterconn.toFixed(2) != 0.00
					)
			){
		objTree["children"].push(customerMap[cu]);
		}
	}
	
	
	
	//-- HANDLE CSV FILE GENERATION --//
	
	function csvEscape(string) {
		if (string== null) string = '';
		return '"' +string.replace('"','""')+'"';
	}


	function csvEscapeForceString(string) {
		if (string== null) string = '';
		return '"=""' +string.replace('"','""')+'"""';
	}

	
	
	function makeCsvRow (rowObject) {
		var row = [
		                 'Customer'
		                 ,csvEscape(rowObject.node)
		                 ,''
		                 ,''
		                 ,csvEscape(rowObject.periodname)
		                 ,csvEscape(rowObject.location)
		                 ,(rowObject.currency)
		                 ,(rowObject.total)
		                 ,(rowObject.totalDisaster)
		                 ,(rowObject.totalEquiptmentRental)
		                 ,(rowObject.totalEquiptmentSales)
		                 ,(rowObject.totalInstall)
		                 ,(rowObject.totalIntercon)
		                 ,(rowObject.totalNetwork)
		                 ,(rowObject.totalOtherNonRec)
		                 ,(rowObject.totalOtherRec)
		                 ,(rowObject.totalPower)
		                 ,(rowObject.totalSpace)
		                 ,(rowObject.totalVirtualInterconn)
		                 ,(rowObject.totalRemoteHands)
		                 ,(rowObject.totalPowerUsage)
		                 ,(rowObject.totalBandwidthBurst)
		                 ];
		
		return row.join(",");
	}
	

	
	function makeCsvRowInvoice (rowObject, parent, grandparent) {
		var row = [
	                 'Invoice'
		                 ,csvEscape(grandparent.node)
	                , csvEscapeForceString(parent.node)
	                , csvEscape(rowObject.node)
		                 ,csvEscape(parent.periodname)
		                 ,csvEscape(grandparent.location)
		                 ,(grandparent.currency)
		                 ,(rowObject.total)
		                 ,(rowObject.totalDisaster)
		                 ,(rowObject.totalEquiptmentRental)
		                 ,(rowObject.totalEquiptmentSales)
		                 ,(rowObject.totalInstall)
		                 ,(rowObject.totalIntercon)
		                 ,(rowObject.totalNetwork)
		                 ,(rowObject.totalOtherNonRec)
		                 ,(rowObject.totalOtherRec)
		                 ,(rowObject.totalPower)
		                 ,(rowObject.totalSpace)
		                 ,(rowObject.totalVirtualInterconn)
		                 ,(rowObject.totalRemoteHands)
		                 ,(rowObject.totalPowerUsage)
		                 ,(rowObject.totalBandwidthBurst)
		                 ];
		
		return row.join(",");
	}
	

	function makeCsvRowCInvoice (rowObject, parent) {
		var row = [ 'CI'
	                , csvEscape(parent.node)
	                , csvEscapeForceString(rowObject.node)
	                , ''
		                 ,csvEscape(rowObject.periodname)
		                 ,csvEscape(parent.location)
		                 ,(parent.currency)
		                 ,(rowObject.total)
		                 ,(rowObject.totalDisaster)
		                 ,(rowObject.totalEquiptmentRental)
		                 ,(rowObject.totalEquiptmentSales)
		                 ,(rowObject.totalInstall)
		                 ,(rowObject.totalIntercon)
		                 ,(rowObject.totalNetwork)
		                 ,(rowObject.totalOtherNonRec)
		                 ,(rowObject.totalOtherRec)
		                 ,(rowObject.totalPower)
		                 ,(rowObject.totalSpace)
		                 ,(rowObject.totalVirtualInterconn)
		                 ,(rowObject.totalRemoteHands)
		                 ,(rowObject.totalPowerUsage)
		                 ,(rowObject.totalBandwidthBurst)
		                 ];
		
		return row.join(",");
	}
		
		csvData=['RowType,Customer,CI,Invoice,Period,Location,Currency,Total,Disaster,Equiptment Rental,Equiptment Sales,Install,Intercon,Network,Other Non Rec,Other Rec,Power,Space,Virtual Interconn,Remote Hands,Power Usage,Bandwidth Burst'];
	
	var customers = objTree.children;
	for (var c = 0; c < customers.length; c++) {

		csvData.push(makeCsvRow(customers[c]));

		var cis = customers[c].children;
		for (var ci = 0; ci < cis.length; ci++) {
			 
			//csvData.push(makeCsvRow(cis[ci]));	
			csvData.push(makeCsvRowCInvoice( cis[ci] ,customers[c]   ));

			
			var invss = cis[ci].children;
			for (var ii = 0; ii < invss.length; ii++) {
						//csvData.push(makeCsvRow(cis[ci]));
						csvData.push(makeCsvRowInvoice(invss[ii], cis[ci] ,customers[c]   ));
						
			}
					
		
				
		}
		
	}

	var csvDataString = csvData.join("\n");
		
	
	
	
	
	
	
	
	//Return results	
	var resultArray = new Array();
	resultArray.push(objTree);
	resultArray.push(csvDataString);
	return resultArray;
}
