nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	       CLGX_SL_ISC_Locations.js
//	Script Name:	CLGX_SL_ISC_Locations 
//	Script Id:		customscript_clgx_sl_isc_locations 
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com, adapted by Derek Hitchman - derek@erpadvisorsgroup.com
//	Created:		06/16/2015, Live 06/25/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=513&deploy=1
//     Description:       This Suitelet is the 4th level down Suitelet for the Invoice Stare & Compare report. It is embedded as an iframe within the CLGX_SL_ISC_PeriodB.js Suitelet. It displays the list of locations to choose from. When a location is selected it passes the selected location and date ranges to the CLGX_SL_ISC_CInvoice.js Suitelet to generate the actual invoice list. 
//-------------------------------------------------------------------------------------------------

function suitelet_isc_locations (request, response){
	
	

	var perioda = null;
	var periodb = null;
	try {
		perioda = request.getParameter('perioda');
		periodb = request.getParameter('periodb');
	} catch (error){}

	if(!periodb || !perioda ) {
		response.write( 'Please select a Period B' );
		return;
	}
	
    
	//-------------Get Locations JSON---------------
	

	var cutOffDate =  new Date();
	cutOffDate.setDate(cutOffDate.getDate()-160);
	
	var searchInvoices = Array();
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custbody_clgx_consolidate_locations',null,'GROUP'));
	

	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("trandate",null,"after",cutOffDate));
	arrFilters.push(new nlobjSearchFilter("mainline",null,"is",'T'));

	
	var savedsearch = nlapiCreateSearch( 'invoice', arrFilters, arrColumns );

	var resultSetLoc = savedsearch.runSearch();
	


	//var resultSetLoc = searchLocations.runSearch();
	var arrLocations = new Array();
	resultSetLoc.forEachResult(function(searchResult) {
		var colObj = new Object();
		
		if(searchResult.getValue('custbody_clgx_consolidate_locations', null, 'GROUP')) {
		
			colObj["location_name"] = searchResult.getText('custbody_clgx_consolidate_locations', null, 'GROUP');
			colObj["locationid"] = searchResult.getValue('custbody_clgx_consolidate_locations', null, 'GROUP');
			
			colObj["perioda"] = perioda;
			colObj["periodb"] = periodb;
			arrLocations.push(colObj);
		}	
		return true; // return true to keep iterating
		
		});
	
	var locationsJSON=  JSON.stringify(arrLocations);
    // -------------End get Locations JSON----------------
	
	
	try {
		var objFile = nlapiLoadFile(2747135);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{LocationsJSON}','g'), locationsJSON);
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}





