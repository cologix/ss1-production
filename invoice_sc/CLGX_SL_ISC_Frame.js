nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	        CLGX_SL_ISC_Frame.js
//	Script Name:	CLGX_SL_ISC_Frame
//	Script Id:		customscript_clgx_sl_isc_frame
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Derek Hitchman - derek@erpadvisorsgroup.com
//	Created:		06/16/2015, Live 06/25/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=516&deploy=1
//     Description:       This Suitelet is the top level Suitelet for the Invoice Stare & Compare which is viewed by the user. It renders the outermost frame which the user views. It automatically embeds the CLGX_SL_ISC_PeriodA.js Suitelet in an iframe as the next level down which displays a select column for the first period. 
//-------------------------------------------------------------------------------------------------

function suitelet_isc_frame (request, response){
	try {
		var formFrame = nlapiCreateForm('Invoice Stare and Compare');
		var fieldFrame = formFrame.addField('custpage_clgx_frame_html','inlinehtml', null, null, null);
		var scriptField = formFrame.addField('custpage_clgx_scripts','inlinehtml', null, null, null);
		var frameHTML = '<iframe name="isc1" id="isc1" src="/app/site/hosting/scriptlet.nl?script=515&deploy=1" height="600px" width="1235px" frameborder="0" scrolling="no"></iframe>';
		fieldFrame.setDefaultValue(frameHTML);
		
		var scripts ="<script>jQuery( document ).ready(function() {\
						jQuery('input#send-now-btn').each(function () { this.style.setProperty( 'background-color', '#fbb', 'important' ); }); \
						jQuery('#tbl_send-now-btn').each(function () { \
			 					this.style.setProperty( 'position', 'absolute' );\
			 					this.style.setProperty( 'left', '20px' );\
			 					this.style.setProperty( 'top','780px' );\
								});\
						});</script>";
		scriptField.setDefaultValue(scripts);
		
		

		   response.writePage( formFrame  );

		  
		} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}