nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_TimeBill.js
//	Script Name:	CLGX_CR_TimeBill
//	Script Id:		customscript_clgx_cr_timebill
//	Script Runs:	On Server
//	Script Type:	Client Script
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//-------------------------------------------------------------------------------------------------


function setBilled()
{

    var caseid=nlapiGetFieldValue('casetaskevent');
    var item= nlapiGetFieldValue('item');
    var id = nlapiGetRecordId();


    if((caseid!= null)&&(caseid!='')) {
        var supportcase = nlapiLoadRecord('supportcase', caseid);
        var arrTimes=new Array();
        var arrDTimes=new Array();
        var  arrHTimes=new Array();

        var searchColumnT = new Array();
        var searchFilterT=new Array();
        searchFilterT.push(new nlobjSearchFilter('internalid',null,'anyof',caseid));
        var searchTimes = nlapiSearchRecord('supportcase','customsearch7400',searchFilterT,searchColumnT);
        if (searchTimes != null) {

            for (var k = 0; searchTimes != null && k < searchTimes.length; k++) {
                var searchT = searchTimes[k];
                var columns = searchT.getAllColumns();
                var idTime = searchT.getValue(columns[0]);
                var hrTime=searchT.getValue(columns[1]);
                var dTime=searchT.getValue(columns[2]);

                if(!in_array(idTime,arrTimes)){
                    arrTimes.push(idTime);
                    arrDTimes.push(dTime);
                    arrHTimes.push(hrTime);

                }

            }
        }
        if(arrTimes.length>0) {
            var searchColumn = new Array();
            var searchFilter = new Array();
            searchFilter.push(new nlobjSearchFilter('custrecord_clgx_rh_times', 'custrecord_clgx_rh_casesids', 'anyof', arrTimes));
            var searchInvoices = nlapiSearchRecord('supportcase', 'customsearch7401', searchFilter, searchColumn);
            if (searchInvoices != null) {
                var searchInvoice = searchInvoices[0];
                var columns = searchInvoice.getAllColumns();
                var invDate = searchInvoice.getValue(columns[0]);
                //   var dateInv = moment(invDate);
                //  var dateToValidate = dateInv;
                nlapiSetFieldValue('custcol_clgx_rh_validationdate', invDate);
                //Get the button

            }
        }
    }

}
function saveRecord(){
    try {
//------------- Request -------------------------------------------------------------------
        // Prevent a user to create a churn record if there is already an active churn record for that customer for that market.
//-------------------------------------------------------------------------------------------------
        var allowSave = 1;
        var alertMsg = '';
        var id = nlapiGetRecordId();
        var validationDate=nlapiGetFieldValue('custcol_clgx_rh_validationdate');
        var item= nlapiGetFieldValue('item');
        var trandate= nlapiGetFieldValue('trandate');
        var hours= nlapiGetFieldValue('hours');
        var check = new moment();

        var month = check.format('MM');
        var year  = check.format('YYYY');
        var tenMonth = month+'/10/'+year;
        tenMonth = moment(tenMonth, 'MM/DD/YYYY');

        var firstMonth = month+'/01/'+year;
        firstMonth = moment(firstMonth, 'MM/DD/YYYY');
        // alert(validationDate);
        if(((item!= null)&&(item!='')&&(item == 544 || item == 545)))
        {
            if(validationDate!=null && validationDate!='') {
                if (moment(trandate).isSameOrBefore(moment(validationDate))) {
                    allowSave = 0;
                    alertMsg = "Billing for the selected month has already been invoiced. Please cancel and edit this order again with a valid DATE";
                }
            }
            else{
                if  ((moment(trandate).isSameOrBefore(moment(firstMonth))) && (moment(check).isSameOrAfter(moment(tenMonth)))) {

                    var allowSave = 0;
                    var alertMsg = "Billing for the selected month has already been invoiced. Please cancel and edit this order again with a valid DATE";
                }
            }

            if (allowSave == 0) {
                alert(alertMsg);

                return false;
            }
            else{

                return true;
            }

        } else{
            return true;
        }
//-------------------------------------------------------------------------------------------------

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
//check if a value is in the array
function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}