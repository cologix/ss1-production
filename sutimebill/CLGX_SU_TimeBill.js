nlapiLogExecution("audit","FLOStart",new Date().getTime());
/**
 * Created by catalinataran on 2014-09-10.
 */
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_TimeBill.js
//	Script Name:	CLGX_SU_TimeBill
//	Script Id:		customscript_clgx_su_timebill
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//-------------------------------------------------------------------------------------------------
function beforeLoad(type, form) {
    try {
        nlapiLogExecution('DEBUG','User Event - Before Load','|-------------STARTED--------------|');
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Details:	Verify if case is processed to create sub cases - if yes, don't allow editing.
// Created:	07/14/2014
//-----------------------------------------------------------------------------------------------------------------


        var caseid=nlapiGetFieldValue('casetaskevent');
        if((caseid!= null)&&(caseid!=''))
        {
            var searchColumn = new Array();
            var searchFilter=new Array();
            searchColumn.push(new nlobjSearchColumn('custcol_clgx_rh_invoice_item_case',null,'GROUP'));
            searchFilter.push(new nlobjSearchFilter('custcol_clgx_rh_invoice_item_case',null,'anyof',caseid));
            var searchInvoices = nlapiSearchRecord('invoice',null,searchFilter,searchColumn);
            if(searchInvoices!=null)
            {
                nlapiSetFieldValue('custcol_clgx_rh_billedtime', 'T');
                //Get the button

            }
//---------- End Section 1 ------------------------------------------------------------------------------------------------
            //var record = nlapiGetNewRecord();
            // Get the expected date for this Churn
            //var expecteddate = record.getFieldValue("custevent_clgx_exp_churn_dte");
            //expecteddate=parseInt(expecteddate)+parseInt(1);
            //form.getField('custevent_clgx_exp_churn_dte').setDefaultValue(expecteddate);
        }
        nlapiLogExecution('DEBUG','User Event - Befor Load','|-------------FINISHED--------------|');
    }
        //}
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function afterSubmit(type){
    try {
        /*
         * Details:	Update/Create Remote Hands
         * When a Case has a closed date a Scheduled Script will be launched. The Scheduled Script  wil create a  New Remote Hands will be created/updated. The Remote Hands will be added info about time tracking, rates...etc.
         * Catalina  10 Sept 2014
         */
//-------------------------------------------------------------------------------------------------
        /**
                      *
                      * get the closed date && CreatedDate
                      */
        if (type != 'delete') {
        var timeid = nlapiGetRecordId();
        if(timeid>2)
        {
            var timebillID=nlapiLoadRecord('timebill', timeid);
            var caseid=timebillID.getFieldValue('casetaskevent');

            //var arrayFilter=new Array();
            //var arrayCol=new Array();
            //arrayFilter.push(new nlobjSearchFilter("internalid",null,"anyof",caseid));
            //var searchCase = nlapiSearchRecord('supportcase', null, arrayFilter, arrayCol);
            if(caseid!= null)
            {
                //  for ( var z = 0; searchCase != null && z < searchCase.length; z++ ) {
                //    var searchCas = searchCase[z];
                //   var columns = searchCas.getAllColumns();
                //   var caseID=searchCas.getValue(columns[0]);
                // }
                var acCase=nlapiLoadRecord('supportcase',  caseid);
                var closeddate='';
                closeddate=acCase.getFieldValue('enddate');
                // var status=acCase.getFieldValue('status');
                var subcasetype=acCase.getFieldValue('custevent_cologix_sub_case_type');
                // if(category==1)
                var category=acCase.getFieldValue('category');
                if((category==1)&&((subcasetype==5)||(subcasetype==6)))
                {
                    if(closeddate!=null)
                    {
                        closeddate=new Date(closeddate);
                        var year=closeddate.getFullYear();
                        var month=closeddate.getMonth();
                        var createddate=acCase.getFieldValue('createddate');
                        createddate=new Date(createddate);
                        /**
                                          * get the SO from case
                                          *
                                          */
                        var so=acCase.getFieldValue('custevent2');




                        /**
                                          * get the total businessH, total rates and company and location
                                          */
                        var company=acCase.getFieldValue('companyid');
                        var location=acCase.getFieldValue('custevent_cologix_facility');
                        var casesnbr=acCase.getFieldValue('casenumber');

                        /**
                                          *
                                          * schedule the script execution and define script parameter values
                                          */
                        var params = {
                            custscript_ss_remote_hands_cus:company,
                            custscript_ss_remote_hands_customerlocat:location,
                            custscript_ss_remote_hands_casecloseddat:closeddate,
                            custscript_ss_remote_hands_caseclosedmon:month,
                            custscript_ss_remote_hands_caseclosedyr:year,
                            custscript_ss_remote_hands_caseid:caseid,
                            //custscript_ss_remote_hands_totbusinessH:totalTimeB,
                            //custscript_ss_remote_hands_totafterH:totalTimeA,
                            // custscript_ss_remote_hands_totratebus:0,
                            // custscript_ss_remote_hands_totrateafter:0,
                            custscript_ss_remote_hands_casecreateddt:createddate,
                            custscript_ss_remote_hands_cas:casesnbr,
                            custscript_ss_remote_hands_caseso:so
                            //,
                            //custscript_ss_remote_hands_casetotalhour: totalTimeHoursBandA
                        };
//hit the scheduled script for remote hands

                        //var emailSubject = 'SS Remote params2';
                        //var emailBody = 'stStartDate ' +company+ ' stEndDate  ' +location;
                        //nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null); // Send email to Catalina
                        nlapiScheduleScript('customscript_clgx_ss_remote_hands',null, params);

                    }
                }
            }
        }
        }
    }
        //---------- End Sections ------------------------------------------------------------------------------------------------
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

}