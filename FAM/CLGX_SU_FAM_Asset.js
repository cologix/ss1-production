nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_FAM_Asset.js
//	Script Name:	CLGX_SU_FAM_Asset
//	Script Id:		customscript_clgx_su_fam_asset
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Project
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Date:			9/12/2013
//-------------------------------------------------------------------------------------------------

function beforeLoad(type, form) {
	try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Date:	9/12/2013
// Details:	Add a sub tab to asset and displays customers/contacts depending on asset
//-----------------------------------------------------------------------------------------------------------------
		var currentContext = nlapiGetContext();
		var assetid = nlapiGetRecordId();
		var userid = nlapiGetUser();
		var roleid = nlapiGetRole();
		
		
		if( (currentContext.getExecutionContext() == 'userinterface') && (type == 'edit' || type == 'view')){
			
			var assetTab = form.addTab('custpage_asset_tab_customers', 'Customers');
			var assetField = form.addField('custpage_asset_customers','inlinehtml', null, null, 'custpage_asset_tab_customers');
			var assetFrameHTML = '';
			//assetFrameHTML += 'www.cologix.com';
			assetFrameHTML += '<iframe name="customers" id="customers" src="/app/site/hosting/scriptlet.nl?script=237&deploy=1&assetid=' + assetid + '&act=0" height="580px" width="1000px" frameborder="0" scrolling="no"></iframe>';
			nlapiLogExecution('DEBUG','debug', '| assetid = ' + assetid + ' | ');
			assetField.setDefaultValue(assetFrameHTML);
			
			if (roleid == '-5' || roleid == '3' || roleid == '18' ||  roleid == '1052') {
				var deviceid = nlapiGetFieldValue('custrecord_clgx_od_fam_device');
				if(deviceid != null && deviceid != ''){
					var dcimid = nlapiLookupField('customrecord_clgx_dcim_devices', deviceid, 'externalid');
					if(dcimid != null && dcimid != ''){
						var dcimTab = form.addTab('custpage_asset_tab_dcim', 'DCIM');
						var dcimField = form.addField('custpage_asset_dcim','inlinehtml', null, null, 'custpage_asset_tab_dcim');
						var dcimFrameHTML = '';
						dcimFrameHTML += '<iframe name="dcim" id="dcim" src="/app/site/hosting/scriptlet.nl?script=300&deploy=1&deviceid=' + dcimid + '&powerid=0" height="580px" width="1200px" frameborder="0" scrolling="no"></iframe>';
						dcimField.setDefaultValue(dcimFrameHTML);
					}
				}
			}
			
		}

//------------- Begin Section 3 -----------------------------------------------------------------------------------
// Date:	6/19/2014
// Details:	Hide device field from other roles than admin or full or Keith
//-----------------------------------------------------------------------------------------------------------------
		if( (currentContext.getExecutionContext() == 'userinterface') && (type == 'edit' || type == 'view')){
			
			if (roleid != '-5' && roleid != '3' && roleid != '18' &&  roleid != '1052') { // admin, full, Supreme Commander
				if (nlapiGetField('custrecord_clgx_od_fam_device') != null && nlapiGetField('custrecord_clgx_od_fam_device') != ''){
					form.getField('custrecord_clgx_od_fam_device').setDisplayType('inline');
				}
				if (nlapiGetField('custrecord_clgx_od_fam_measured_point') != null && nlapiGetField('custrecord_clgx_od_fam_measured_point') != ''){
					form.getField('custrecord_clgx_od_fam_measured_point').setDisplayType('inline');
				}
				if (nlapiGetField('custrecord_clgx_od_fam_capacity') != null && nlapiGetField('custrecord_clgx_od_fam_capacity') != ''){
					form.getField('custrecord_clgx_od_fam_capacity').setDisplayType('inline');
				}
				if (nlapiGetField('custrecord_clgx_od_fam_capacity_report') != null && nlapiGetField('custrecord_clgx_od_fam_capacity_report') != ''){
					form.getField('custrecord_clgx_od_fam_capacity_report').setDisplayType('inline');
				}
				if (nlapiGetField('custrecord_clgx_od_fam_capacity_report_r') != null && nlapiGetField('custrecord_clgx_od_fam_capacity_report_r') != ''){
					form.getField('custrecord_clgx_od_fam_capacity_report_r').setDisplayType('inline');
				}
			}
		}

//---------- End Sections ------------------------------------------------------------------------------------------------
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function beforeSubmit (type) {
	try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Date:	07/09/2014
// Details:	FAM was edited and saved - put device in queue to update it's points
//-----------------------------------------------------------------------------------------------------------------
		var currentContext = nlapiGetContext();
		var assetid = nlapiGetRecordId();
		var userid = nlapiGetUser();
		var roleid = nlapiGetRole();

		if(currentContext.getExecutionContext() == 'userinterface' && type == 'edit'){
			if (roleid == '-5' || roleid == '3' || roleid == '18' || roleid == '1052') { //  admin, full, Supreme Commander
				
				var famname = nlapiGetFieldValue('name');
				var deviceid = nlapiGetFieldValue('custrecord_clgx_od_fam_device');
				
				if(deviceid != null && deviceid != ''){
					nlapiSubmitField('customrecord_clgx_dcim_devices', deviceid, 'custrecord_clgx_dcim_device_fam', assetid);
				}
				
	        	var oldFAM = nlapiGetOldRecord();
	        	var olddeviceid = oldFAM.getFieldValue('custrecord_clgx_od_fam_device');
	        	
	        	if(deviceid != null && deviceid != ''){ // a device is configured
	        		var externalid = nlapiLookupField('customrecord_clgx_dcim_devices', deviceid, 'externalid');
	        		
	        		if(olddeviceid != null && olddeviceid != ''){ // there was a device before
		        		if(deviceid != olddeviceid){ // the device changed
		        			

							
		        			var oldexternalid = nlapiLookupField('customrecord_clgx_dcim_devices', olddeviceid, 'externalid');
		        			// update new FAM on OpenData
		        			var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/devices/set_externalid.cfm?famid=' + famname + '&deviceid=' + externalid);
		        			//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/setextid.cfm?famid=' + famname + '&deviceid=' + externalid);
		        			// clear old FAM on OpenData
		        			var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/devices/set_externalid.cfm?famid=DEL00000&deviceid=' + oldexternalid);
		        			//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/setextid.cfm?famid=DEL00000&deviceid=' + oldexternalid);
			        	}
		        		else{ // it's the same device as before
		        		}
	        		}
	        		else{ // before there was no device
	        			
	        			// update new FAM on OpenData
	        			var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/devices/set_externalid.cfm?famid=' + famname + '&deviceid=' + externalid);
	        			//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/setextid.cfm?famid=' + famname + '&deviceid=' + externalid);
		        	}
	        		
	        		// verify if device is in update queue
					var arrColumns = new Array();
					arrColumns.push(new nlobjSearchColumn('internalid',null,null));
					var arrFilters = new Array();
					arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",deviceid));
					arrFilters.push(new nlobjSearchFilter("custrecord_clgx_dcim_device_updated",null,"is",'T'));
					var searchDevicesIds = nlapiSearchRecord('customrecord_clgx_dcim_devices', null, arrFilters, arrColumns);
					
					if(searchDevicesIds == null){
						nlapiSubmitField('customrecord_clgx_dcim_devices', deviceid, 'custrecord_clgx_dcim_device_updated', 'T');
					}
				}
	        	else{ // no device is configured
	        		if(olddeviceid != null && olddeviceid != ''){ // there was a device before
	        			var oldexternalid = nlapiLookupField('customrecord_clgx_dcim_devices', olddeviceid, 'externalid');
	        			// clear old FAM on OpenData
	        			var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/devices/set_externalid.cfm?famid=DEL00000&deviceid=' + oldexternalid);
	        			//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/setextid.cfm?famid=DEL00000&deviceid=' + oldexternalid);
		        	}
	        	}
			}
		}

//---------- End Sections ------------------------------------------------------------------------------------------------
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function afterSubmit (type) {
	try {
		//------------- NNJ SYNC -----------------------------------------------------------------------------------
		var currentContext = nlapiGetContext();
        var environment = currentContext.getEnvironment();
        var row = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
        var facility = nlapiGetFieldValue('custrecord_assetfacility');
    	if (facility == 33 || facility == 34 || facility == 35 || facility == 36) {
	        if (environment == 'PRODUCTION' && (type == 'xedit'  || currentContext.getExecutionContext() == 'userinterface')) {			
				try {
					flexapi('POST','/netsuite/update', {
						'type': nlapiGetRecordType(),
						'id': nlapiGetRecordId(),
						'action': type,
						'userid': nlapiGetUser(),
						'user': nlapiLookupField('employee', nlapiGetUser(), 'entityid'),
						'data': json_serialize(row)
					});
				}
				catch (error) {
					nlapiLogExecution('DEBUG', 'flexapi error: ', error);
					var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
					record.setFieldValue('custrecord_clgx_queue_ping_record_id', nlapiGetRecordId());
					record.setFieldValue('custrecord_clgx_queue_ping_record_type', 5);
					record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
					var idRec = nlapiSubmitRecord(record, false,true);
				}
	    	}
    	}
	}
	catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} 
}