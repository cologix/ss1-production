nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_FAM_Asset.js
//	Script Name:	CLGX_RL_FAM_Asset
//	Script Id:		customscript_clgx_rl_fam_asset
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		9/12/2013
//-------------------------------------------------------------------------------------------------

function restlet_fam_asset (datain){
	try {
		var txtArg = datain;
		txtArg = txtArg.replace(/\"/g,"");
		txtArg = txtArg.replace(/\:/g,",");
		txtArg = txtArg.replace(/\{/g,"");
		txtArg = txtArg.replace(/\}/g,"");
		var arrArguments = new Array();
		arrArguments = txtArg.split( "," );
		//arrArguments.reverse();
		
		if(arrArguments[3] != '' ){
			var assetid = arrArguments[3];
			
			if(arrArguments[1] == 1 ){
				returnContacts (assetid);
				//emailContacts(assetid, contacts);
			}
			if(arrArguments[1] == 2 ){
				//updateContactsSearch(assetid);
			}

			var objFile = nlapiLoadFile(654236);
			var html = objFile.getValue();
			//html = html.replace(new RegExp('{datain}','g'),datain);
			html = html.replace(new RegExp('{dataCustomers}','g'),jsonDataCustomers(assetid));
			html = html.replace(new RegExp('{assetid}','g'),assetid);
		}
		else{
			var html = '';
		}
		
		return html;
	
//---------- End Sections ------------------------------------------------------------------------------------------------

} 
catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
	if (error.getDetails != undefined){
	    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	    throw error;
	}
	else{
	    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	    throw nlapiCreateError('99999', error.toString());
	}
} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function jsonDataCustomers(assetid){	
	
	var arrServices = new Array();
	arrServices.push(0);

	/*
	// all 3 power circuits searches in one
	var arrCustomers = new Array();
	var filterExpression = 	[['custrecord_cologix_power_service', 'noneof', '@NONE@'], 'and', [ [ 'custrecord_cologix_power_ups_rect', 'anyof', assetid ],'or',[ 'custrecord_clgx_power_panel_pdpm', 'anyof', assetid ],'or',[ 'custrecord_clgx_power_generator', 'anyof', assetid ]]];
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('parent','custrecord_cologix_power_service','GROUP').setSort(false));
	var searchPowers = nlapiCreateSearch('customrecord_clgx_power_circuit', filterExpression, searchColumns);
	var resultSet = searchPowers.runSearch()
	resultSet.forEachResult(function(result) {
		arrCustomers.push(result.getValue('parent', 'custrecord_cologix_power_service', 'GROUP'));
		return true;
	});
	*/

	// Search for Power Circuits linked to this FAM record and add them to services array
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('name',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('custrecord_cologix_power_service',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('custrecord_cologix_power_ups_rect',null,'anyof', assetid)); // UPS FAM
	searchFilter.push(new nlobjSearchFilter('custrecord_cologix_power_service',null,'noneof', '@NONE@'));
	arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit',null,searchFilter,searchColumns);
	for ( var i = 0; searchPowers != null && i < searchPowers.length; i++ ) {
		var searchPower = searchPowers[i];
		arrServices.push(searchPower.getValue('custrecord_cologix_power_service',null,'GROUP'));
	}
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('name',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('custrecord_cologix_power_service',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('custrecord_clgx_power_panel_pdpm',null,'anyof', assetid)); // Panel FAM
	searchFilter.push(new nlobjSearchFilter('custrecord_cologix_power_service',null,'noneof', '@NONE@'));
	arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	var searchPowers2 = nlapiSearchRecord('customrecord_clgx_power_circuit',null,searchFilter,searchColumns);
	for ( var i = 0; searchPowers2 != null && i < searchPowers2.length; i++ ) {
		var searchPower = searchPowers2[i];
		arrServices.push(searchPower.getValue('custrecord_cologix_power_service',null,'GROUP'));
	}
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('name',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('custrecord_cologix_power_service',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('custrecord_cologix_power_service',null,'noneof', '@NONE@'));
	searchFilter.push(new nlobjSearchFilter('custrecord_clgx_power_generator',null,'anyof', assetid)); // Generator FAM
	arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	var searchPowers3 = nlapiSearchRecord('customrecord_clgx_power_circuit',null,searchFilter,searchColumns);
	for ( var i = 0; searchPowers3 != null && i < searchPowers3.length; i++ ) {
		var searchPower = searchPowers3[i];
		arrServices.push(searchPower.getValue('custrecord_cologix_power_service',null,'GROUP'));
	}
	
	// Search for Spaces linked to this FAM record and add them to services array
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('name',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('custrecord_cologix_space_project',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('custrecord_cologix_space_fams',null,'anyof', assetid));
	arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	var searchSpaces = nlapiSearchRecord('customrecord_cologix_space',null,searchFilter,searchColumns);
	for ( var i = 0; searchSpaces != null && i < searchSpaces.length; i++ ) {
		var searchSpace = searchSpaces[i];
		arrServices.push(searchSpace.getValue('custrecord_cologix_space_project',null,'GROUP'));
	}
	
	// Search for Services linked to this FAM record and add them to services array
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('entityid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('customer',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('custentity_clgx_service_fams',null,'anyof', assetid));
	arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	var searchServices = nlapiSearchRecord('job',null,searchFilter,searchColumns);
	for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
		var searchService = searchServices[i];
		arrServices.push(searchService.getValue('internalid',null,'GROUP'));
	}
	
	// Search all customers of all found services
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('customer',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('internalid',null,'anyof', arrServices));
	arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	var searchCustomers = nlapiSearchRecord('job',null,searchFilter,searchColumns);

	// build JSON 
	var jsonCustomers = 'dataCustomers =  {"text":".","children": [\n';
	for ( var i = 0; searchCustomers != null && i < searchCustomers.length; i++ ) {
		var searchCustomer = searchCustomers[i];
		var customerId = searchCustomer.getValue('customer',null,'GROUP');
		var customerName = searchCustomer.getText('customer',null,'GROUP');
		
		jsonCustomers += '{entity:"' + customerName + '", entityid:' + customerId + ', type:"customer", expanded:false, iconCls:"customer", leaf:false, children:[\n';

		// Build contacts tree
		var searchColumns = new Array();
		searchColumns.push(new nlobjSearchColumn('internalid','contact',null));
		searchColumns.push(new nlobjSearchColumn('entityid','contact',null));
		searchColumns.push(new nlobjSearchColumn('custentity_cologix_esc_seq','contact',null));
		searchColumns.push(new nlobjSearchColumn('custentity_clgx_ops_outage_notification','contact',null));
		searchColumns.push(new nlobjSearchColumn('email','contact',null));
		searchColumns.push(new nlobjSearchColumn('contactrole','contact',null));
		searchColumns.push(new nlobjSearchColumn('custentity_clgx_contact_secondary_role','contact',null));
		var searchFilter = new Array();
		searchFilter.push(new nlobjSearchFilter('internalid',null,'anyof', customerId));
		var searchContacts = nlapiSearchRecord('customer',null,searchFilter,searchColumns);
		

		var noContacts = 0;
		jsonCustomers += '{entity:"Contacts", expanded:true, type:"category", iconCls:"group", leaf:false, children:[\n';
		for ( var j = 0; searchContacts != null && j < searchContacts.length; j++ ) {
			var searchContact = searchContacts[j];
			var contactId = searchContact.getValue('internalid','contact',null);
			var contactName = searchContact.getValue('entityid','contact',null);
			var contactEmail = searchContact.getValue('email','contact',null);
			var role1 = searchContact.getText('contactrole','contact',null);
			var role2 = searchContact.getText('custentity_clgx_contact_secondary_role','contact',null);
			if(searchContact.getValue('custentity_clgx_ops_outage_notification','contact',null) == 'T'){
				jsonCustomers += '{entity:"' + contactName + '", entityid:' + contactId + ', email:"' + contactEmail + '", role1:"' + role1 + '", role2:"' + role2 + '", type:"contact", iconCls:"user", leaf:true},';
				noContacts = 1;
			}
			
		}
		var strLen = jsonCustomers.length;
		if (searchContacts != null && noContacts == 1){
			jsonCustomers = jsonCustomers.slice(0,strLen-1);
		}
		jsonCustomers += '\n]},';
		
		
		
		// Build Power Circuits tree if any
		if(searchPowers != null || searchPowers2 != null || searchPowers3 != null){
			jsonCustomers += '{entity:"Power", expanded:false, type:"category", iconCls:"power", leaf:false, children:[\n';
			
			if(searchPowers != null){
				for ( var j = 0; j < searchPowers.length; j++ ) {
					var searchPower = searchPowers[j];
					var powerId = searchPower.getValue('internalid',null,'GROUP');
					var powerName = searchPower.getValue('name',null,'GROUP');
					var serviceid = searchPower.getValue('custrecord_cologix_power_service',null,'GROUP');
					var currentCust = nlapiLookupField('job', serviceid, 'customer');
					if(currentCust == customerId){
						jsonCustomers += '{entity:"' + powerName + '", entityid:' + powerId + ', type:"power", iconCls:"power", leaf:true},';
					}
				}
			}
			if(searchPowers2 != null){
				for ( var j = 0; j < searchPowers2.length; j++ ) {
					var searchPower = searchPowers2[j];
					var powerId = searchPower.getValue('internalid',null,'GROUP');
					var powerName = searchPower.getValue('name',null,'GROUP');
					var serviceid = searchPower.getValue('custrecord_cologix_power_service',null,'GROUP');
					var currentCust = nlapiLookupField('job', serviceid, 'customer');
					if(currentCust == customerId){
						jsonCustomers += '{entity:"' + powerName + '", entityid:' + powerId + ', type:"power", iconCls:"power", leaf:true},';
					}
				}
			}
			if(searchPowers3 != null){
				for ( var j = 0; j < searchPowers3.length; j++ ) {
					var searchPower = searchPowers3[j];
					var powerId = searchPower.getValue('internalid',null,'GROUP');
					var powerName = searchPower.getValue('name',null,'GROUP');
					var serviceid = searchPower.getValue('custrecord_cologix_power_service',null,'GROUP');
					var currentCust = nlapiLookupField('job', serviceid, 'customer');
					if(currentCust == customerId){
						jsonCustomers += '{entity:"' + powerName + '", entityid:' + powerId + ', type:"power", iconCls:"power", leaf:true},';
					}
				}
			}
			var strLen = jsonCustomers.length;
			if(searchPowers != null || searchPowers2 != null || searchPowers3 != null){
				jsonCustomers = jsonCustomers.slice(0,strLen-1);
			}
			jsonCustomers += '\n]},';
		}
		
		// Build Spaces tree  if any
		if(searchSpaces != null){
			jsonCustomers += '{entity:"Space", expanded:false, type:"category", iconCls:"space", leaf:false, children:[\n';
			for ( var j = 0; j < searchSpaces.length; j++ ) {
				var searchSpace = searchSpaces[j];
				var spaceId = searchSpace.getValue('internalid',null,'GROUP');
				var spaceName = searchSpace.getValue('name',null,'GROUP');
				var serviceid = searchSpace.getValue('custrecord_cologix_space_project',null,'GROUP');
				var currentCust = nlapiLookupField('job', serviceid, 'customer');
				if(currentCust == customerId){
					jsonCustomers += '{entity:"' + spaceName + '", entityid:' + spaceId + ', type:"space", iconCls:"power", leaf:true},';
				}
			}
			var strLen = jsonCustomers.length;
			jsonCustomers = jsonCustomers.slice(0,strLen-1);
			jsonCustomers += '\n]},';
		}
		
		// Build Services tree  if any
		if(searchServices != null){
			jsonCustomers += '{entity:"Services", expanded:false, type:"category", iconCls:"service", leaf:false, children:[\n';
			for ( var j = 0; j < searchServices.length; j++ ) {
				var searchService = searchServices[j];
				var serviceId = searchService.getValue('internalid',null,'GROUP');
				var serviceName = searchService.getValue('entityid',null,'GROUP');
				var currentCust = searchService.getValue('customer',null,'GROUP');
				if(currentCust == customerId){
					jsonCustomers += '{entity:"' + serviceName + '", entityid:' + serviceId + ', type:"service", iconCls:"power", leaf:true},';
				}
			}
			var strLen = jsonCustomers.length;
			jsonCustomers = jsonCustomers.slice(0,strLen-1);
			jsonCustomers += '\n]}';
		}

		jsonCustomers += '\n]},'
	}
	var strLen = jsonCustomers.length;
	if (searchCustomers != null){
		jsonCustomers = jsonCustomers.slice(0,strLen-1);
	}
	jsonCustomers += '\n]};';
    return jsonCustomers;
}


function updateContactsSearch (assetid){
	
	var fam = assetid; //
	var famName = nlapiLookupField('customrecord_ncfar_asset', assetid, 'name');
	
	var arrServices = new Array();
	arrServices.push(0);
	
	// Search for Power Circuits linked to this FAM record and add them to services array
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('name',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('custrecord_cologix_power_service',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('custrecord_cologix_power_ups_rect',null,'anyof', assetid));
	var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit',null,searchFilter,searchColumns);
	for ( var i = 0; searchPowers != null && i < searchPowers.length; i++ ) {
		var searchPower = searchPowers[i];
		arrServices.push(searchPower.getValue('custrecord_cologix_power_service',null,'GROUP'));
	}
	
	// Search for Spaces linked to this FAM record and add them to services array
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('name',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('custrecord_cologix_space_project',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('custrecord_cologix_space_fams',null,'anyof', assetid));
	var searchSpaces = nlapiSearchRecord('customrecord_cologix_space',null,searchFilter,searchColumns);
	for ( var i = 0; searchSpaces != null && i < searchSpaces.length; i++ ) {
		var searchSpace = searchSpaces[i];
		arrServices.push(searchSpace.getValue('custrecord_cologix_space_project',null,'GROUP'));
	}
	
	// Search for Services linked to this FAM record and add them to services array
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('entityid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('customer',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('custentity_clgx_service_fams',null,'anyof', assetid));
	var searchServices = nlapiSearchRecord('job',null,searchFilter,searchColumns);
	for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
		var searchService = searchServices[i];
		arrServices.push(searchService.getValue('internalid',null,'GROUP'));
	}
	
	// Search all customers of all found services
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('customer',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('internalid',null,'anyof', arrServices));
	var searchCustomers = nlapiSearchRecord('job',null,searchFilter,searchColumns);

	var filterExpression = new Array();
	var arrTemp2 = new Array();
	for ( var i = 0; searchCustomers != null && i < searchCustomers.length; i++ ) {
		var searchCustomer = searchCustomers[i];
		var customer = searchCustomer.getText('customer',null,'GROUP');
		
		var arrTemp1 = new Array();
		arrTemp1.push('customer.entityid');
		arrTemp1.push('is');
		arrTemp1.push(customer);

		arrTemp2.push(arrTemp1);
		if((i+1) < searchCustomers.length){
			arrTemp2.push('or');
		}
	}
	
	filterExpression.push(arrTemp2);
	filterExpression.push('and');

	var arrTemp1 = new Array();
	arrTemp1.push('custentity_cologix_esc_seq');
	arrTemp1.push('noneof');
	arrTemp1.push('@NONE@');
	filterExpression.push(arrTemp1);


	var searchName = 'CLGX_Contacts_' + famName;
	var results = nlapiSearchGlobal('search:' + searchName); 
	var nbrSearches = 0;
	if(results != null){
		var nbrSearches = results.length;
	}
	
	if(nbrSearches < 1){  // search does not exist - create it
		var columnsCustomers = new Array();
		columnsCustomers.push(new nlobjSearchColumn('internalid','customer',null));
		columnsCustomers.push(new nlobjSearchColumn('entityid','customer',null));
		columnsCustomers.push(new nlobjSearchColumn('internalid',null,null));
		columnsCustomers.push(new nlobjSearchColumn('entityid',null,null));
		columnsCustomers.push(new nlobjSearchColumn('email',null,null));
		columnsCustomers.push(new nlobjSearchColumn('contactrole',null,null));
		columnsCustomers.push(new nlobjSearchColumn('custentity_clgx_contact_secondary_role',null,null));
		var search = nlapiCreateSearch('contact', filterExpression, columnsCustomers);
		var searchId = search.saveSearch('CLGX_Contacts_' + famName, 'customsearch_clgx_contacts_' + famName);
	}
	var mySearch = nlapiLoadSearch('contact', 'customsearch_clgx_contacts_' + famName);
	
	/*
	try{
		var mySearch = nlapiLoadSearch('contact', 'customsearch_clgx_contacts_' + famName);
	}
	catch (error) {
		if(error.getDetails != undefined){ // search does not exist - create it
			var columnsCustomers = new Array();
			columnsCustomers.push(new nlobjSearchColumn('internalid','customer',null));
			columnsCustomers.push(new nlobjSearchColumn('entityid','customer',null));
			columnsCustomers.push(new nlobjSearchColumn('internalid',null,null));
			columnsCustomers.push(new nlobjSearchColumn('entityid',null,null));
			columnsCustomers.push(new nlobjSearchColumn('email',null,null));
			columnsCustomers.push(new nlobjSearchColumn('contactrole',null,null));
			columnsCustomers.push(new nlobjSearchColumn('custentity_clgx_contact_secondary_role',null,null));
			var search = nlapiCreateSearch('contact', filterExpression, columnsCustomers);
			var searchId = search.saveSearch('CLGX_Contacts_' + famName, 'customsearch_clgx_contacts_' + famName);
		}
		return;
	}
*/
	mySearch.setFilterExpression(filterExpression);
	mySearch.saveSearch('CLGX_Contacts_' + famName, 'customsearch_clgx_contacts_' + famName);

	return 'success';
	
}


function returnContacts (assetid){
	
	var fam = assetid; //
	var famName = nlapiLookupField('customrecord_ncfar_asset', assetid, 'name');
	
	var arrServices = new Array();
	arrServices.push(0);
	
	// Search for Power Circuits linked to this FAM record and add them to services array
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('name',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('custrecord_cologix_power_service',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('custrecord_cologix_power_ups_rect',null,'anyof', assetid));
	var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit',null,searchFilter,searchColumns);
	for ( var i = 0; searchPowers != null && i < searchPowers.length; i++ ) {
		var searchPower = searchPowers[i];
		arrServices.push(searchPower.getValue('custrecord_cologix_power_service',null,'GROUP'));
	}
	
	// Search for Spaces linked to this FAM record and add them to services array
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('name',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('custrecord_cologix_space_project',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('custrecord_cologix_space_fams',null,'anyof', assetid));
	var searchSpaces = nlapiSearchRecord('customrecord_cologix_space',null,searchFilter,searchColumns);
	for ( var i = 0; searchSpaces != null && i < searchSpaces.length; i++ ) {
		var searchSpace = searchSpaces[i];
		arrServices.push(searchSpace.getValue('custrecord_cologix_space_project',null,'GROUP'));
	}
	
	// Search for Services linked to this FAM record and add them to services array
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('entityid',null,'GROUP'));
	searchColumns.push(new nlobjSearchColumn('customer',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('custentity_clgx_service_fams',null,'anyof', assetid));
	var searchServices = nlapiSearchRecord('job',null,searchFilter,searchColumns);
	for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
		var searchService = searchServices[i];
		arrServices.push(searchService.getValue('internalid',null,'GROUP'));
	}
	
	// Search all customers of all found services
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('customer',null,'GROUP'));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('internalid',null,'anyof', arrServices));
	var searchCustomers = nlapiSearchRecord('job',null,searchFilter,searchColumns);
	
	var arrCustomers = new Array();
	arrCustomers.push(0);
	for ( var i = 0; searchCustomers != null && i < searchCustomers.length; i++ ) {
		var searchCustomer = searchCustomers[i];
		arrCustomers.push(searchCustomer.getValue('customer',null,'GROUP'));
	}

	// Search all contacts of all found customers
	var searchColumns = new Array();
	searchColumns.push(new nlobjSearchColumn('internalid','customer',null));
	searchColumns.push(new nlobjSearchColumn('entityid','customer',null));
	searchColumns.push(new nlobjSearchColumn('internalid',null,null));
	searchColumns.push(new nlobjSearchColumn('entityid',null,null));
	searchColumns.push(new nlobjSearchColumn('email',null,null));
	searchColumns.push(new nlobjSearchColumn('contactrole',null,null));
	searchColumns.push(new nlobjSearchColumn('custentity_clgx_contact_secondary_role',null,null));
	var searchFilter = new Array();
	searchFilter.push(new nlobjSearchFilter('internalid','customer','anyof', arrCustomers));
	searchFilter.push(new nlobjSearchFilter('custentity_clgx_ops_outage_notification',null,'is', 'T'));
	var searchContacts = nlapiSearchRecord('contact',null,searchFilter,searchColumns);
	
	var csvContacts = '';
	var txtEmails = '';
	for ( var i = 0; searchContacts != null && i < searchContacts.length; i++ ) {
		var searchContact = searchContacts[i];
		var customer = searchContact.getValue('entityid','customer',null);
		var contact = searchContact.getValue('entityid',null,null);
		var email = searchContact.getValue('email',null,null);
		var role1 = searchContact.getText('contactrole',null,null);
		var role2 = searchContact.getText('custentity_clgx_contact_secondary_role',null,null);
		csvContacts += 	customer + ',' +	contact + ',' +	email +  ',' +	role1 +  ',' +	role2 +  '\n';
		txtEmails += 	email +  ',';
	}

	var attachments = new Array();
	if(csvContacts != ''){
		csvContacts = 'Customer,Contact,Email,Role1,Role2\n' + csvContacts;
		var fileContacts = nlapiCreateFile('contacts_' + famName + '.csv', 'CSV', csvContacts);
		var fileEmails = nlapiCreateFile('contacts_emails_list_' + famName + '.txt', 'CSV', txtEmails);
		attachments.push(fileContacts);
		attachments.push(fileEmails);
		var userId = nlapiGetUser();
		emailSubject = 'Customers / Contacts for ' + famName;
		emailBody = '';
		nlapiSendEmail(userId,userId,emailSubject,emailBody,null,null,null,attachments,true);
	}
	return 'success';
}

function emailContacts (assetid, contacts){
	
	var csvContacts = '';
	var txtEmails = '';
	var famName = nlapiLookupField('customrecord_ncfar_asset', assetid, 'name');
	var searchContacts = nlapiLoadSearch('contact', 'customsearch_clgx_contacts_' + famName);
	
	var resultSet = searchContacts.runSearch();
	resultSet.forEachResult(function(searchResult) {
		var customer = searchResult.getValue('entityid','customer',null);
		var contact = searchResult.getValue('entityid',null,null);
		var email = searchResult.getValue('email',null,null);
		var role1 = searchResult.getText('contactrole',null,null);
		var role2 = searchResult.getText('custentity_clgx_contact_secondary_role',null,null);
		csvContacts += 	customer + ',' +	contact + ',' +	email +  ',' +	role1 +  ',' +	role2 +  '\n';
		txtEmails += 	email +  ',';
		return true;
	});

	var attachments = new Array();
	if(csvContacts != ''){
		csvContacts = 'Customer,Contact,Email,Role1,Role2\n' + csvContacts;
		var fileContacts = nlapiCreateFile('contacts_' + famName + '.csv', 'CSV', csvContacts);
		var fileEmails = nlapiCreateFile('contacts_emails_list_' + famName + '.txt', 'CSV', txtEmails);
		attachments.push(fileContacts);
		attachments.push(fileEmails);
		var userId = nlapiGetUser();
		emailSubject = 'Customers / Contacts for ' + famName;
		emailBody = '';
		nlapiSendEmail(userId,userId,emailSubject,emailBody,null,null,null,attachments,true);
	}
	return 'success';
}