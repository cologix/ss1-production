nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_JSON_CPI.js
//	Script Name:	CLGX_SL_JSON_CPI
//	Script Id:		customscript_clgx_sl_json_cpi
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/3/2013
//-------------------------------------------------------------------------------------------------
function suitelet_json_cpi (request, response){
	try {

		var searchObj = clgx_search_to_json_grid ('customrecord_clgx_cpi', 'customsearch_clgx_cpi', null, null);
		
// =======================================================================================
	}
	catch (error) {
		var searchObj = new Object();
		searchObj["success"] = false;
		var rowsArr = new Array();
		searchObj["total"] = "0";
		searchObj["rows"] = rowsArr;
		if (error.getDetails != undefined){
			searchObj["error"] = error.getCode() + ' / ' + error.getDetails();
		}
		else{
			searchObj["error"] = error.toString();
		}
	}
	// return the search in JSON format
	response.write(JSON.stringify(searchObj));
}

// =======================================================================================

function clgx_search_to_json_grid (recordtype, searchid, arrColumns, arrFilters){
	
	var searchRecords = nlapiSearchRecord(recordtype, searchid, arrFilters, arrColumns);
	var searchObj = new Object();
	searchObj["recordtype"] = recordtype;
	searchObj["success"] = true;
	var rowsArr = new Array();
	if(searchRecords != null){
		searchObj["total"] = searchRecords.length;
		for ( var i = 0; searchRecords != null && i < searchRecords.length; i++ ) {
			var colObj = new Object();
			var colsArr = searchRecords[i].getAllColumns();
			for ( var j = 0; j < colsArr.length; j++ ) {
				fieldObj = new Object();
				fieldObj["type"] = colsArr[j].type;
				fieldObj["value"] = searchRecords[i].getValue(colsArr[j]);
				fieldObj["text"] = searchRecords[i].getText(colsArr[j]);
				var colName = colsArr[j].name;
				colObj[colName] = fieldObj;
			}
			rowsArr.push(colObj);
		}
		searchObj["rows"] = rowsArr;
	}
	else{
		searchObj["total"] = "0";
		searchObj["rows"] = rowsArr;
	}
	searchObj["error"] = null;
	
	return searchObj;
}