nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SOs_Uplift.js
//	Script Name:	CLGX_SS_SOs_Uplift
//	Script Id:		customscript_clgx_ss_sos_uplift
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Service Order
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/3/2013
//	Includes:		moment.min.js
//-------------------------------------------------------------------------------------------------
function scheduled_sos_uplift (){
	
//------------- Begin Section 1 -------------------------------------------------------------------
// Details:	Uplift items rate with CPI at anniversary Annual Accelerator Date
//-------------------------------------------------------------------------------------------------
    try{
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
    	
        var context = nlapiGetContext();

        //retrieve script parameters
        var month = context.getSetting('SCRIPT','custscript_sos_uplift_month');
        var year = context.getSetting('SCRIPT','custscript_sos_uplift_year');
        var arrLocations = context.getSetting('SCRIPT','custscript_sos_uplift_locations');
        
        var today = moment().format('M/D/YYYY');
        var strDay = month + '/' + '1/' + year;
        var MstartDate = new moment(strDay);
        var MendDate = new moment(strDay);
        
        var startDate = MstartDate.format('M/D/YYYY');
        var endDate   = MendDate.endOf('month').format('M/D/YYYY');
        
        var listLocations = new Array();
        var itemsSCArr= new Array();
        if (arrLocations !=  null){
        	if(arrLocations.length > 2){
        		var listLocations = arrLocations.split(arrLocations.charAt(2));
        	}
        	else{
        		var listLocations = arrLocations;
        	}
        }
        
        // read the CPI 2 month prior to desired month
        var cpi = getCPI(strDay);
        var cpiFloat = parseFloat(cpi);
        var customSchedule;
        var itemSchedule;
      
        nlapiLogExecution('DEBUG', 'Value', '| month = ' + month + ' | year = ' + year  + ' | arrLocations = ' + arrLocations + ' | cpiFloat = ' + cpiFloat + ' |');
        
		if(cpiFloat > 0){
            nlapiLogExecution('DEBUG', 'Value', '| cpiFloat = ' + cpiFloat + ' |');
            var searchInactives = nlapiSearchRecord('transaction', 'customsearch_clgx_sos_with_inactives', null, null);
            var arrSOs = new Array();
            for ( var i = 0; searchInactives != null && i < searchInactives.length; i++ ) {
                var searchInactive = searchInactives[i];
                var soid = searchInactive.getValue('internalid', null, 'GROUP');
                arrSOs.push(soid);
            }
            // load all SOs ready for uplifting
            var arrColumns = new Array();
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter('custbody_clgx_next_aa_date',null,'within',startDate,endDate));
            arrFilters.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof',listLocations));
            if(arrSOs.length > 0){
                arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",arrSOs));
            }
            // arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",4716779));
            arrFilters.push(new nlobjSearchFilter('custbody_clgx_ready_to_uplift',null,'is','T'));
            var searchResults = nlapiSearchRecord('transaction', 'customsearch_clgx_cpi_sos_to_uplift', arrFilters, arrColumns);
            var initialTime = moment();
            for ( var j = 0; searchResults != null && j < searchResults.length; j++ ) {
                var startExecTime = moment();
                var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);
                if((context.getRemainingUsage() < 1000 || totalMinutes > 50) && ((j + 1) < searchResults.length)){
                    var params = new Array();
                    params['custscript_sos_uplift_month'] = month;
                    params['custscript_sos_uplift_year'] = year;
                    params['custscript_sos_uplift_locations'] = arrLocations;
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId(),params);
                    if(status == 'QUEUED'){
                        nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
                        break;
                    }
                }
                var searchResult = searchResults[j];
                var internalid = searchResult.getValue('internalid',null,'GROUP');
                //nlapiLogExecution('DEBUG', 'Value', '| internalid = ' + internalid + ' |');
                var recSO = nlapiLoadRecord('salesorder', internalid);
                var recSONew = nlapiLoadRecord('salesorder', internalid);
                var update = 0;
                var soname = recSO.getFieldValue('tranid');
                var AADate = recSO.getFieldValue('custbody_clgx_next_aa_date');
                var AAType = recSO.getFieldValue('custbodyclgx_aa_type');
                var AA = recSO.getFieldValue('custbody_cologix_annual_accelerator');
                var uplift = 0;
                var d1 = new moment(AADate);
                var nextAADate = d1.add('years', 1).format('M/D/YYYY');
                var d2 = new moment(AADate);
                var tranDate = d2.add('days', -1).format('M/D/YYYY');
                if(AAType == 1){ // Flat Percentage
                    uplift = AA;
                }
                else if(AAType == 3){ // Greater of CPI or Flat Percentage
                    if(cpi < AA){
                        uplift = AA;
                    }
                    else{
                        uplift = cpi;
                    }
                }
                else{
                    uplift = 0;
                }
                uplift = parseFloat(uplift);
                nlapiLogExecution('DEBUG', 'Value', '| uplift = ' + uplift + ' |');
                if(uplift > 0){
                    var nbrItems = recSO.getLineItemCount('item');
                    var curMRC = 0;
                    var curMonth = 0;
                    var custSched = true;
                    for (var i = 0; i < nbrItems; i++){
                        var bs = recSO.getLineItemText('item','billingschedule', i + 1);
                        var recurring = recSO.getLineItemText('item','class', i + 1);
                        //var addCPIToCustomItem = recSO.getLineItemValue('item', 'custcol_clgx_item_sched_expired', i + 1); // Added Hector Urrutia (RSM) 1/30/2020
                        //if(recurring.indexOf("Recurring") > -1 || addCPIToCustomItem === 'T'){ // if this item is recurring  - Edited Hector Urrutia (RSM) 1/30/2020
                        if(recurring.indexOf("Recurring") > -1){	//LS 2/17/2020
                            if(bs != null && bs != ''){ // if billing schedule is not null copy item to new line and make billing schedule 'Month to Month', then set rate to 0 and remove Bill Schedule on old line
                                update = 1;
                                var item = recSO.getLineItemValue('item','item', i + 1);
                                var rate = recSO.getLineItemValue('item','rate', i + 1);
                                var amount = recSO.getLineItemValue('item','amount', i + 1);
                                var qty = recSO.getLineItemValue('item','quantity', i + 1);
                                var qty2print = recSO.getLineItemValue('item','custcol_clgx_qty2print', i + 1);
                                var qtybilled = recSO.getLineItemValue('item','quantitybilled', i + 1);
                                var description = recSO.getLineItemValue('item','description', i + 1);
                                var rateNew = (parseFloat(rate) + (parseFloat(rate)*parseFloat(uplift)/100)).toFixed(2);
                                var amountNew = (parseFloat(rateNew) * parseFloat(qty)).toFixed(2);
                                customSchedule = recSO.getLineItemValue('item', 'custcol_custom_schedule', i + 1);	//LS 2/17/2020
                                itemSchedule = recSO.getLineItemValue('item', 'custcol_item_schedule', i + 1);	//LS 2/17/2020
                                //nlapiLogExecution('DEBUG', 'Value', '| customSchedule1 ' + customSchedule + ' |');	//LS 2/17/2020
                                // nlapiLogExecution('DEBUG', 'Value', '| itemSchedule initial ' + itemSchedule + ' |');	//LS 2/17/2020
                                //LS 2/17/2020 do not change rate and amount when there is a custom schedule
                                //the scheduled script CLGX_MR_CustItemSchedUplifts can be run to update these amounts correctly
                                if (customSchedule == 'T'){
                                    rateNew = rate;
                                    amountNew = amount;
                                }
                                nlapiLogExecution('DEBUG', 'Value',
                                    ' | item = ' + item +
                                    ' | rate = ' + rate +
                                    ' | amount = ' + amount +
                                    ' | qty = ' + qty +
                                    ' | qty2print = ' + qty2print +
                                    ' | qtybilled = ' + qtybilled +
                                    ' | description = ' + description +
                                    ' | rateNew = ' + rateNew +
                                    ' | amountNew = ' + amountNew +
                                    ' | itemSchedule = ' + itemSchedule +
                                    ' | customSchedule = ' + customSchedule +
                                    ' |');
                                var intCount = recSONew.getLineItemCount('item');
                                // set fields on new item line
                                recSONew.insertLineItem('item', intCount + 1);
                                recSONew.setLineItemValue('item','custcol_clgx_so_col_service_id', intCount + 1, recSO.getLineItemValue('item','custcol_clgx_so_col_service_id', i + 1));
                                recSONew.setLineItemValue('item','location', intCount + 1, recSO.getLineItemValue('item','location', i + 1));
                                recSONew.setLineItemValue('item','item', intCount + 1, item);
                                recSONew.setLineItemValue('item','description', intCount + 1, description);
                                recSONew.setLineItemValue('item','pricelevels', intCount + 1, recSO.getLineItemValue('item','pricelevels', i + 1));
                                recSONew.setLineItemValue('item','price', intCount + 1, recSO.getLineItemValue('item','price', i + 1));
                                recSONew.setLineItemValue('item','taxcode', intCount + 1, recSO.getLineItemValue('item','taxcode', i + 1));
                                recSONew.setLineItemValue('item','taxrate1', intCount + 1, recSO.getLineItemValue('item','taxrate1', i + 1));
                                recSONew.setLineItemValue('item','taxrate2', intCount + 1, recSO.getLineItemValue('item','taxrate2', i + 1));
                                recSONew.setLineItemValue('item','class', intCount + 1, recSO.getLineItemValue('item','class', i + 1));
                                recSONew.setLineItemValue('item','options', intCount + 1, recSO.getLineItemValue('item','options', i + 1));
                                recSONew.setLineItemValue('item','custcol_cologix_unevendaystobill', intCount + 1, recSO.getLineItemValue('item','custcol_cologix_unevendaystobill', i + 1));
                                recSONew.setLineItemValue('item','custcol_cologix_invoice_item_category', intCount + 1, recSO.getLineItemValue('item','custcol_cologix_invoice_item_category', i + 1));
                                recSONew.setLineItemValue('item','custcol_clgx_qty2print', intCount + 1, qty2print);
                                recSONew.setLineItemValue('item','quantity', intCount + 1, parseInt(qty));
                                recSONew.setLineItemValue('item','billingschedule', intCount + 1, recSO.getLineItemValue('item','billingschedule', i + 1));
                                recSONew.setLineItemValue('item','rate', intCount + 1, rateNew);
                                recSONew.setLineItemValue('item','amount', intCount + 1, amountNew);
                                recSONew.setLineItemValue('item','custcol_custom_schedule',  intCount + 1, customSchedule); //LS 2/17/2020
                                recSONew.setLineItemValue('item','custcol_item_schedule',  intCount + 1, itemSchedule);  //LS 2/17/2020
                              
                                curMRC += parseFloat(parseFloat(rateNew) * parseInt(qty));
                                curMonth += parseFloat(parseFloat(rateNew) * parseInt(qty2print));
                                nlapiLogExecution('DEBUG', 'Value recSO',
                                    ' | service = ' + recSO.getLineItemValue('item','custcol_clgx_so_col_service_id', i + 1) +
                                    ' | location = ' + recSO.getLineItemValue('item','location', i + 1) +
                                    ' | item = ' + recSO.getLineItemValue('item','item', i + 1) +
                                    ' | pricelevels = ' + recSO.getLineItemValue('item','pricelevels', i + 1) +
                                    ' | price = '+ recSO.getLineItemValue('item','price', i + 1) +
                                    ' | taxcode = ' + recSO.getLineItemValue('item','taxcode', i + 1) +
                                    ' | taxrate1 = ' + recSO.getLineItemValue('item','taxrate1', i + 1) +
                                    ' | taxrate2 = ' + recSO.getLineItemValue('item','taxrate2', i + 1) +
                                    ' | class = ' + recSO.getLineItemValue('item','class', i + 1) +
                                    ' | options = ' + recSO.getLineItemValue('item','options', i + 1) +
                                    ' | unevendaystobill = ' + recSO.getLineItemValue('item','custcol_cologix_unevendaystobill', i + 1) +
                                    ' | category = ' + recSO.getLineItemValue('item','custcol_cologix_invoice_item_category', i + 1) +
                                    ' | billingschedule = ' + recSO.getLineItemValue('item','billingschedule', i + 1) +
                                    ' | customschedule = '  + + recSO.getLineItemValue('item','custcol_custom_schedule', i + 1) +  //LS 2/17/2020
                                    ' | itemschedule = '  + + recSO.getLineItemValue('item','custcol_item_schedule', i + 1) +   //LS 2/17/2020
                                    ' |');
                                // update fields on old line
                                recSONew.setLineItemValue('item','quantity', i + 1, qtybilled);
                                recSONew.setLineItemValue('item','rate', i + 1, 0);
                                recSONew.setLineItemValue('item','amount', i + 1, 0);
                                recSONew.setLineItemValue('item','billingschedule', i + 1, null);
                                recSONew.setLineItemValue('item','custcol_clgx_old_rate', i + 1, rate);
                                recSONew.setLineItemValue('item','custcol_clgx_date_closed', i + 1, today);
								recSONew.setLineItemValue('item','custcol_custom_schedule', i + 1, 'F'); //BD 2/27/20
								recSONew.setLineItemValue('item','custcol_item_schedule', i + 1, ''); //BD 2/27/20
                            }
                        }
                        else{ // if this item is non recurring set rate to 0 and remove Bill Schedule
                            recSONew.setLineItemValue('item','rate', i + 1, 0);
                            recSONew.setLineItemValue('item','amount', i + 1, 0);
                            recSONew.setLineItemValue('item','billingschedule', i + 1, null);
                        }
                        nlapiLogExecution('DEBUG', 'Value', '| itemSchedule before update lineNum ' + itemSchedule + ' |');
                        //LS 2/17/2020  //update LineNumber in Custom Item Schedule record
                        if (itemSchedule != null && itemSchedule.trim() != '' && itemSchedule != 0){
                            var obj={'itmsc':itemSchedule,'lineNbr':intCount+1};
                            itemsSCArr.push(obj);
                            nlapiLogExecution('DEBUG', 'Value', '| Updated LineID on  item schedule ' +  itemSchedule  + ' to lineNum ' + (intCount+1) + ' |');	//LS 2/17/2020
                        }
                    }
                    recSONew.setFieldValue('custbody_clgx_next_aa_date',nextAADate);
                    recSONew.setFieldValue('trandate', tranDate);
                    recSONew.setFieldValue('custbody_clgx_total_non_recurring', 0);
                    recSONew.setFieldValue('custbody_clgx_total_recurring', curMRC.toFixed(2));
                    recSONew.setFieldValue('custbody_clgx_total_recurring_month', curMonth.toFixed(2));
                    var index = j + 1;
                    var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                    nlapiLogExecution('DEBUG', 'Value', '| SO = ' + soname + ' | ID = ' + internalid + ' | Index = ' + index + ' of ' + searchResults.length +  ' | Usage = '+ usageConsumtion + ' | Minutes = ' + totalMinutes + ' |');
                    if(update == 1){
                        var customerid = recSONew.getFieldValue('entity');
                        nlapiLogExecution('DEBUG', 'Value', '| beforeSubmit  |');
                        var idAfter = nlapiSubmitRecord(recSONew, false, true);
                        nlapiLogExecution('DEBUG', 'Value', '| afterSubmit |');
                        var recSONewAF = nlapiLoadRecord('salesorder', idAfter);
                        for (var k = 0; itemsSCArr != null && k < itemsSCArr.length; k++){
                            var recCustomItemSchedule = nlapiLoadRecord('customrecord_custom_item_schedule', parseInt(itemsSCArr[k].itmsc));
                            recCustomItemSchedule.setFieldValue('custrecord_line_id', parseInt(recSONewAF.getLineItemValue('item', 'line', parseInt(itemsSCArr[k].lineNbr))));
                            nlapiSubmitRecord(recCustomItemSchedule, false, true);	//LS 2/17/2020
                        }
                        var totals = clgx_transaction_totals (customerid, 'so', internalid);
                    }
                    nlapiLogExecution('DEBUG', 'Value recSONew ',
                        ' | service = ' + recSONew.getLineItemValue('item','custcol_clgx_so_col_service_id', i + 1) +
                        ' | location = ' + recSONew.getLineItemValue('item','location', i + 1) +
                        ' | item = ' + recSONew.getLineItemValue('item','item', i + 1) +
                        ' | pricelevels = ' + recSONew.getLineItemValue('item','pricelevels', i + 1) +
                        ' | price = '+ recSONew.getLineItemValue('item','price', i + 1) +
                        ' | taxcode = ' + recSONew.getLineItemValue('item','taxcode', i + 1) +
                        ' | taxrate1 = ' + recSONew.getLineItemValue('item','taxrate1', i + 1) +
                        ' | taxrate2 = ' + recSONew.getLineItemValue('item','taxrate2', i + 1) +
                        ' | class = ' + recSONew.getLineItemValue('item','class', i + 1) +
                        ' | options = ' + recSONew.getLineItemValue('item','options', i + 1) +
                        ' | unevendaystobill = ' + recSONew.getLineItemValue('item','custcol_cologix_unevendaystobill', i + 1) +
                        ' | category = ' + recSONew.getLineItemValue('item','custcol_cologix_invoice_item_category', i + 1) +
                        ' | billingschedule = ' + recSONew.getLineItemValue('item','billingschedule', i + 1) +
                        ' | customschedule = '  + + recSONew.getLineItemValue('item','custcol_custom_schedule', i + 1) +
                        ' | itemschedule = '  + + recSONew.getLineItemValue('item','custcol_item_schedule', i + 1) +
                        ' |');
                }
                //var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                //nlapiLogExecution('DEBUG', 'Value', 'ID = ' + j + '/' + searchResults.length +  ' / Usage - '+ usageConsumtion);
            }
        }
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|');
    }
// ========================================================================================================================
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

//========================================================================================================================

function getCPI(strDay){
    var month2 = moment(strDay).subtract('months', 2).format('M');
    var year2 = moment(strDay).subtract('months', 2).format('YYYY');
    var searchColumn = new Array();
    searchColumn.push(new nlobjSearchColumn('custrecord_clgx_cpi_cpi',null,null));
    var searchFilter = new Array();
    searchFilter.push(new nlobjSearchFilter('custrecord_clgx_cpi_year',null,'equalto',year2));
    searchFilter.push(new nlobjSearchFilter('custrecord_clgx_cpi_month',null,'equalto',month2));
    var searchResult2 = nlapiSearchRecord('customrecord_clgx_cpi',null,searchFilter,searchColumn);
    if(searchResult2 != null){
        var result = searchResult2[0].getValue('custrecord_clgx_cpi_cpi',null,null)
    }
    else{
        var month3 = moment(strDay).subtract('months', 3).format('M');
        var year3 = moment(strDay).subtract('months', 3).format('YYYY');
        var searchColumn = new Array();
        searchColumn.push(new nlobjSearchColumn('custrecord_clgx_cpi_cpi',null,null));
        var searchFilter = new Array();
        searchFilter.push(new nlobjSearchFilter('custrecord_clgx_cpi_year',null,'equalto',year3));
        searchFilter.push(new nlobjSearchFilter('custrecord_clgx_cpi_month',null,'equalto',month3));
        var searchResult3 = nlapiSearchRecord('customrecord_clgx_cpi',null,searchFilter,searchColumn);
        if(searchResult3 != null){
            var result = searchResult3[0].getValue('custrecord_clgx_cpi_cpi',null,null)
        }
        else{
            var result = 0;
        }
    }
    return result;
}
