//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_AA_Dates_Uplift.js
//	Script Name:	CLGX_SS_AA_Dates_Uplift
//	Script Id:		customscript_clgx_ss_aa_dates_uplift
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Service Order
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/4/2013
//	Includes:		moment.min.js
//-------------------------------------------------------------------------------------------------
function scheduled_aa_dates_uplift (){
	
//------------- Begin Section 1 -------------------------------------------------------------------
// Details:	Uplift AA Date for SOs with AA Date 2 month back
//-------------------------------------------------------------------------------------------------
    try{
        var context = nlapiGetContext();

		var date = new Date();
		var d = new moment(date);
		var month = d.add('months', -2).format('M');
		var year = d.add('months', -2).format('YYYY');

		nlapiLogExecution('DEBUG', 'Value', 'month = ' + month + '/ year = ' + year);
		
        var arrDates  = getDateRange(month,year);
        var startDate = arrDates[0];
        var endDate   = arrDates[1];
        
        // load all SOs ready for uplifting
		nlapiLogExecution('DEBUG', 'Value', 'startDate = ' + startDate + '/ endDate = ' + endDate);
		
    	var arrColumns = new Array();
    	var arrFilters = new Array();
    	arrFilters.push(new nlobjSearchFilter('custbody_clgx_next_aa_date',null,'within',startDate,endDate));
    	arrFilters.push(new nlobjSearchFilter("custbody_clgx_ready_to_uplift",null,"is",'F'));
    	var searchResults = nlapiSearchRecord("transaction", 'customsearch_clgx_cpi_sos_to_uplift', arrFilters, arrColumns);

        for ( var j = 0; searchResults != null && j < searchResults.length; j++ ) {
            
        	nlapiLogExecution('DEBUG', 'Value', 'j = ' + j);
        	
            if((context.getRemainingUsage() < 1000) && ((j + 1) < searchResults.length)){
                var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId(),null);
                if(status == 'QUEUED'){
                    nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
    			    break;
                }
            }
            
        	var searchResult = searchResults[j];
        	var internalid = searchResult.getValue('internalid',null,'GROUP');

        	var recSO = nlapiLoadRecord('salesorder', internalid);
        	var AADate = recSO.getFieldValue('custbody_clgx_next_aa_date');
        	
			var d = new moment(AADate);
			var nextAADate = d.add('years', 1).format('M/D/YYYY');

			nlapiLogExecution('DEBUG', 'Value', 'nextAADate = ' + nextAADate);
			
			recSO.setFieldValue('custbody_clgx_next_aa_date',nextAADate);
			recSO.setFieldValue('custbody_clgx_ready_to_uplift','T');
    		nlapiSubmitRecord(recSO, false, true);
    		
        	var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
        	nlapiLogExecution('DEBUG', 'Value', 'ID = ' + j + '/' + searchResults.length +  ' / Usage - '+ usageConsumtion);
        }
	
    }
// ========================================================================================================================
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

//========================================================================================================================

//get the last day of the month
function daysInMonth(intMonth, intYear){
  if (intMonth < 0 || intMonth > 11){
      throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
  }
  var lastDayArray = [
      31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
  ];
  if (intMonth != 1){
      return lastDayArray[intMonth];
  }
  if (intYear % 4 != 0){
      return lastDayArray[1];
  }
  if (intYear % 100 == 0 && intYear % 400 != 0){
      return lastDayArray[1];
  }
  return lastDayArray[1] + 1;
}

//get the start date and end date
function getDateRange(month,year){
  var stMonth = month - 1;
  var stDays  = daysInMonth(parseInt(stMonth),parseInt(year));
  var stYear  = year;
  
  var stStartDate = month + '/1/' + stYear;
  var stEndDate   = month + '/' + stDays + '/' + stYear;
  
  var arrDateRange = new Array();
      arrDateRange[0] = stStartDate;
      arrDateRange[1] =stEndDate;
  
  return arrDateRange;
}
