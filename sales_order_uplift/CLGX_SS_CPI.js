nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_CPI.js
//	Script Name:	CLGX_SS_CPI
//	Script Id:		customscript_clgx_ss_cpi
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/2/2013
//	Includes:		MomentJS
//-------------------------------------------------------------------------------------------------

function scheduled_cpi () {
	try {
		nlapiLogExecution('DEBUG','Scheduled Script - CPI','|--------------------STARTED---------------------|');
		
        var environment = nlapiGetContext().getEnvironment();
    	if(environment == 'PRODUCTION'){

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Details:	Updates CPI rates from government site and send email when current month was updated
//-----------------------------------------------------------------------------------------------------------------
	
			//var dataStr = nlapiRequestURL('http://api.bls.gov/publicAPI/v1/timeseries/data/CUUR0000SA0L1E');
			//var dataStr = nlapiRequestURL('http://api.bls.gov/publicAPI/v1/timeseries/data/CUUR0000SA0');
			//var str = '{"status":"REQUEST_SUCCEEDED","responseTime":53,"message":[],"Results":{ "series": [{"seriesID":"CUUR0000SA0L1E","data":[{"year":"2015","period":"M06","periodName":"June","value":"242.354","footnotes":[{}]},{"year":"2015","period":"M05","periodName":"May","value":"242.119","footnotes":[{}]},{"year":"2015","period":"M04","periodName":"April","value":"241.802","footnotes":[{}]},{"year":"2015","period":"M03","periodName":"March","value":"241.067","footnotes":[{}]},{"year":"2015","period":"M02","periodName":"February","value":"240.083","footnotes":[{}]},{"year":"2015","period":"M01","periodName":"January","value":"239.248","footnotes":[{}]},{"year":"2014","period":"M13","periodName":"Annual","value":"237.897","footnotes":[{}]},{"year":"2014","period":"M12","periodName":"December","value":"238.775","footnotes":[{}]},{"year":"2014","period":"M11","periodName":"November","value":"239.248","footnotes":[{}]},{"year":"2014","period":"M10","periodName":"October","value":"239.413","footnotes":[{}]},{"year":"2014","period":"M09","periodName":"September","value":"238.841","footnotes":[{}]},{"year":"2014","period":"M08","periodName":"August","value":"238.296","footnotes":[{}]},{"year":"2014","period":"M07","periodName":"July","value":"238.138","footnotes":[{}]},{"year":"2014","period":"M06","periodName":"June","value":"238.157","footnotes":[{}]},{"year":"2014","period":"M05","periodName":"May","value":"238.029","footnotes":[{}]},{"year":"2014","period":"M04","periodName":"April","value":"237.509","footnotes":[{}]},{"year":"2014","period":"M03","periodName":"March","value":"236.913","footnotes":[{}]},{"year":"2014","period":"M02","periodName":"February","value":"236.075","footnotes":[{}]},{"year":"2014","period":"M01","periodName":"January","value":"235.367","footnotes":[{}]},{"year":"2013","period":"M13","periodName":"Annual","value":"233.806","footnotes":[{}]},{"year":"2013","period":"M12","periodName":"December","value":"235.000","footnotes":[{}]},{"year":"2013","period":"M11","periodName":"November","value":"235.243","footnotes":[{}]},{"year":"2013","period":"M10","periodName":"October","value":"235.162","footnotes":[{}]},{"year":"2013","period":"M09","periodName":"September","value":"234.782","footnotes":[{}]},{"year":"2013","period":"M08","periodName":"August","value":"234.258","footnotes":[{}]},{"year":"2013","period":"M07","periodName":"July","value":"233.792","footnotes":[{}]},{"year":"2013","period":"M06","periodName":"June","value":"233.640","footnotes":[{}]},{"year":"2013","period":"M05","periodName":"May","value":"233.462","footnotes":[{}]},{"year":"2013","period":"M04","periodName":"April","value":"233.236","footnotes":[{}]},{"year":"2013","period":"M03","periodName":"March","value":"233.052","footnotes":[{}]},{"year":"2013","period":"M02","periodName":"February","value":"232.432","footnotes":[{}]},{"year":"2013","period":"M01","periodName":"January","value":"231.612","footnotes":[{}]}]}] }}';
			//var dataObj = JSON.parse(str);
			
			var yearRec = moment().subtract('months', 1).format('YYYY');
		    var monthRec = moment().subtract('months', 1).format('M');
		    var day = moment().format('D');
		    
			var searchColumn = new Array();
			var searchFilter = new Array();
			searchFilter.push(new nlobjSearchFilter('custrecord_clgx_cpi_year',null,'equalto',yearRec));
			searchFilter.push(new nlobjSearchFilter('custrecord_clgx_cpi_month',null,'equalto',monthRec));
			var searchRecord = nlapiSearchRecord('customrecord_clgx_cpi',null,searchFilter,searchColumn);
			
			if(searchRecord == null && day > 10){ // the last month does not exist and is after the 10th of current month
			
				var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/cpi/get_cpi.cfm');
		    	//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/cpi/get_cpi.cfm');
		    	//var requestURL = nlapiRequestURL('https://command1.nnj2.cologix.net:10313/cpi',null,{'Content-type': 'application/json'},null,'GET');
				var jsonCPI = requestURL.body;
				var dataObj = JSON.parse( jsonCPI );
				
				var monthsArr = dataObj.Results.series[0].data;
		
				var sendEmail = 0;
				
				var reportSubject = 'A CPI Record has been added';
		        var  reportBody = '<table border="1" cellpadding="5">';
		        reportBody += '<tr><td>Year</td><td>Month</td><td>Previous Value</td><td>New Value</td><td>CPI</td></tr>';
		        
				for ( var i = 0; i < monthsArr.length; i++ ) {
					var year = parseInt(monthsArr[i].year);
					var monthStr = monthsArr[i].period.slice( 1 );
					if(monthStr.charAt(0) == '0'){
						monthStr = monthStr.slice( 1 );
					}
					var month = parseInt(monthStr);
					var value = parseFloat(monthsArr[i].value);
		
					if( month < 13){
						var searchColumn = new Array();
						var searchFilter = new Array();
						searchFilter.push(new nlobjSearchFilter('custrecord_clgx_cpi_year',null,'equalto',year));
						searchFilter.push(new nlobjSearchFilter('custrecord_clgx_cpi_month',null,'equalto',month));
						var searchResult = nlapiSearchRecord('customrecord_clgx_cpi',null,searchFilter,searchColumn);
						
						if(searchResult == null){
							
							var prevYear = year - 1;
							
							var searchColumn = new Array();
							searchColumn.push(new nlobjSearchColumn('custrecord_clgx_cpi_value_new',null,null));
							var searchFilter = new Array();
							searchFilter.push(new nlobjSearchFilter('custrecord_clgx_cpi_year',null,'equalto',prevYear));
							searchFilter.push(new nlobjSearchFilter('custrecord_clgx_cpi_month',null,'equalto',month));
							var searchResultOld = nlapiSearchRecord('customrecord_clgx_cpi',null,searchFilter,searchColumn);
							
							var prevValue = parseFloat(searchResultOld[0].getValue('custrecord_clgx_cpi_value_new',null,null));
							var cpi = ((value - prevValue)/prevValue)*100;
							if(cpi < 0){
								cpi = 0;
							}
							var cpi = cpi.toFixed(1);
		
							var record = nlapiCreateRecord('customrecord_clgx_cpi');
							record.setFieldValue('custrecord_clgx_cpi_year', year);
							record.setFieldValue('custrecord_clgx_cpi_month', month);
							record.setFieldValue('custrecord_clgx_cpi_value_old', prevValue);
							record.setFieldValue('custrecord_clgx_cpi_value_new', value);
							record.setFieldValue('custrecord_clgx_cpi_cpi', cpi);
							var idRec = nlapiSubmitRecord(record, false, true);
							
							reportBody += '<tr><td>' + year + '</td><td>' + month + '</td><td>' + prevValue + '</td><td>' + value + '</td><td>' + cpi + '</td></tr>';
							sendEmail = 1;
						}
					}
				}
				
				reportBody += '</table>';
				
				if (sendEmail == 1){
					clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_cpi_admin", reportSubject, reportBody);
					//nlapiSendEmail(71418,71418,reportSubject,reportBody,null,null,null,null,true);
					//nlapiSendEmail(71418,12073,reportSubject,reportBody,null,null,null,null);
					nlapiLogExecution('DEBUG','Scheduled Script - CPI','Did send email');
				}
			}
			else{
				if(searchRecord != null){
					nlapiLogExecution('DEBUG','Notification','| Record for last month exist already |');  
				}
				if(day < 11){
					nlapiLogExecution('DEBUG','Notification','| Not the 10 of the curent month yet |');  
				}
			}
//---------- End Section 1 ------------------------------------------------------------------------------------------------
			nlapiLogExecution('DEBUG','Scheduled Script - CPI','|--------------------FINISHED---------------------|');  
    	}
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

