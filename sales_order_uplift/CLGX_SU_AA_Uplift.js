nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_AA_Uplift.js
//	Script Name:	CLGX_SU_AA_Uplift.js
//	Script Id:		customscript_clgx_su_aa_uplift
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Invoice
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/4/2013
//-------------------------------------------------------------------------------------------------

function afterSubmit(type){
    try{
		var currentContext = nlapiGetContext();
		if (currentContext.getExecutionContext() == 'userinterface' && type == 'create') {

	        var rec = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
	        var month = rec.getFieldValue('custrecord_clgx_aa_uplift_month');
	        var year = rec.getFieldValue('custrecord_clgx_aa_uplift_year');
	        var locations = rec.getFieldValue('custrecord_clgx_aa_uplift_locations');

	        var arrParam = new Array();
	            arrParam['custscript_sos_uplift_month'] = month;
	            arrParam['custscript_sos_uplift_year'] = year;
	            arrParam['custscript_sos_uplift_locations'] = locations;

	        nlapiScheduleScript('customscript_clgx_ss_sos_uplift', 'customdeploy_clgx_ss_sos_uplift' ,arrParam);       
        }
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}