nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SP_CPI_Frame.js
//	Script Name:	CLGX_SP_CPI_Frame
//	Script Id:		customscript_clgx_sp_cpi_frame
//	Script Runs:	On Server
//	Script Type:	Portlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		12/3/2013
//-------------------------------------------------------------------------------------------------

function portlet_cpi_frame (portlet, column){
	portlet.setTitle('CPI');
	var html = '<iframe name="cpiFrame" id="cpiFrame" src="/core/media/media.nl?id=881796&c=1337135&h=18f6a70a3392a8476594&_xt=.html" height="200px" width="200px" frameborder="0" scrolling="no"></iframe>';
	portlet.setHtml(html);
}
