nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_Service_Inventory_Service_Details.js
//	Script Name:	CLGX_RL_Service_Inventory_Service_Details
//	Script Id:		customscript_clgx_rl_serv_inv_service_de
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		05/10/2013
//-------------------------------------------------------------------------------------------------

function restlet_serv_inv_service_details (datain){
	try {
		var txtArg = datain;
		txtArg = txtArg.replace(/\"/g,"");
		txtArg = txtArg.replace(/\:/g,",");
		txtArg = txtArg.replace(/\{/g,"");
		txtArg = txtArg.replace(/\}/g,"");
		var arrArguments = [];
		var arrArguments = txtArg.split( "," );

		if(arrArguments[0] != '' ){
			
			switch(arrArguments[3]) {
			case 'xconnect' : 
				var objFile = nlapiLoadFile(434514);
				var html = objFile.getValue();
				html = html.replace(new RegExp('{xcpathJSON}','g'),xcpathJSON(arrArguments[1]));
				html = html.replace(new RegExp('{title}','g'), 'XC Paths');
				break;
			case 'space': 
				var objFile = nlapiLoadFile(434615);
				var html = objFile.getValue();
				html = html.replace(new RegExp('{spaceJSON}','g'),spaceJSON(arrArguments[1]));
				html = html.replace(new RegExp('{title}','g'), 'Space');
				break;
			default:
				var html = 'Please select a record from the above panel and click on "View" to see details.';
			}
		}
		else{
			var html = 'Please select a record from the above panel and click on "View" to see details.';
		}
		return html;
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function xcpathJSON (xcid) {

	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_xcpath_seg_seq',null,null).setSort(false));
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_xcpath_space',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_xcpath_port',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_xcpath_connectortype',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_xcpath_tiecable',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_xcpath_panelname',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_cologix_xcpath_crossconnect",null,"anyof",xcid));
	var searchXCpaths = nlapiSearchRecord('customrecord_cologix_xcpath', null, arrFilters, arrColumns);
	
	var stXCpath = 'var dataXCpath =  [';
              
	for ( var i = 0; searchXCpaths != null && i < searchXCpaths.length; i++ ) {
		var searchXCpath = searchXCpaths[i];

		var space = searchXCpath.getText('custrecord_cologix_xcpath_space', null, null);
		var spaceid = searchXCpath.getValue('custrecord_cologix_xcpath_space', null, null);
		
		if (spaceid == ''){
			spaceid = 0;
		}
		stXCpath += '{seq:"' + searchXCpath.getText('custrecord_cologix_xcpath_seg_seq', null, null) + 
						'",pathid:' +  searchXCpath.getValue('internalid', null, null) + 
						',space:"' +  space + 
						'",spaceid:' +  spaceid + 
						',port:"' +  searchXCpath.getValue('custrecord_cologix_xcpath_port', null, null) +
						'",panel:"' +  searchXCpath.getText('custrecord_cologix_xcpath_panelname', null, null) +
						'",connector:"' +  searchXCpath.getText('custrecord_cologix_xcpath_connectortype', null, null) +
						'",cable:"' +  searchXCpath.getValue('custrecord_cologix_xcpath_tiecable', null, null) + '"},';
	}
	var strLen = stXCpath.length;
	if (searchXCpaths != null){
		stXCpath = stXCpath.slice(0,strLen-1);
	}
	stXCpath += '];';
    return stXCpath;
}


function spaceJSON (spaceid) {

	var recSpace = nlapiLoadRecord('customrecord_cologix_space', spaceid, null, null);
	
	var stSpaces = 'var dataSpace =  {"text":".","children": [';
		
		// Cross connects on this space
		var nbrXCs = recSpace.getLineItemCount('recmachcustrecord_cologix_xcpath_space');
		if(nbrXCs != null && nbrXCs != 0){
			stSpaces += '{name:"Cross Connect' + 
						'",expanded:false, iconCls:"xconnect", leaf:false, children:['; 
			for ( var j = 0; j < nbrXCs; j++ ) {
				var recPath = nlapiLoadRecord('customrecord_cologix_xcpath',recSpace.getLineItemValue('recmachcustrecord_cologix_xcpath_space', 'id', j + 1));

				stSpaces += '{name:"' + recPath.getFieldText('custrecord_cologix_xcpath_seg_seq') + ' / ' + recPath.getFieldText('custrecord_cologix_xcpath_connectortype') + ' / ' + recPath.getFieldValue('custrecord_cologix_xcpath_port') + ' / ' + recPath.getFieldText('custrecord_cologix_xcpath_utilstatus') +
							'",nameid:' +  recPath.getFieldValue('id') + 
							',name2:"' +  recPath.getFieldText('custrecord_cologix_xcpath_crossconnect') + 
							'",name2id:' +  recPath.getFieldValue('custrecord_cologix_xcpath_crossconnect') + 
							', iconCls: "xconnect' +
							'",leaf:true},'; 
			}
			
			var strLen = stSpaces.length;
			stSpaces = stSpaces.slice(0,strLen-1);
			stSpaces += ']},'; 
		}

		// Power circuits on this space
		var nbrPowers = recSpace.getLineItemCount('recmachcustrecord_cologix_power_space');
		if(nbrPowers != null && nbrPowers != 0){
			stSpaces += '{name:"Power' + 
						'",expanded:false, iconCls:"power", leaf:false, children:['; 
			for ( var j = 0; j < nbrPowers; j++ ) {
				var recPower = nlapiLoadRecord('customrecord_clgx_power_circuit',recSpace.getLineItemValue('recmachcustrecord_cologix_power_space', 'id', j + 1));

				
				
				stSpaces += '{name:"' + recPower.getFieldValue('name') + ' / ' + recPower.getFieldText('custrecord_cologix_power_volts') + ' / ' + recPower.getFieldText('custrecord_cologix_power_amps') +
							'",nameid:' +  recPower.getFieldValue('id') + 
							', iconCls: "power' +
							'",leaf:true},'; 
			}

			var strLen = stSpaces.length;
			stSpaces = stSpaces.slice(0,strLen-1);
			stSpaces += ']},'; 
		}

		/*
		// Equipments on this space
		var nbrNetworks = recSpace.getLineItemCount('recmachcustrecord_cologix_equipment_space');
		if(nbrNetworks != null && nbrNetworks != 0){
			stSpaces += '{name:"Network' + 
						'",expanded:false, iconCls:"network", leaf:false, children:['; 
			for ( var j = 0; j < nbrNetworks; j++ ) {
				stSpaces += '{name:"' + recSpace.getLineItemValue('recmachcustrecord_cologix_equipment_space', 'id', j + 1) + 
							'",nameid:' +  recSpace.getLineItemValue('recmachcustrecord_cologix_equipment_space', 'id', j + 1) + 
							', iconCls: "network' +
							'",leaf:true},'; 
			}
			var strLen = stSpaces.length;
			stSpaces = stSpaces.slice(0,strLen-1);
			stSpaces += ']},'; 
		}
		*/
		
		var strLen = stSpaces.length;
		if (parseInt(nbrXCs) > 0 ||parseInt(nbrPowers) > 0){
		//if (parseInt(nbrXCs) > 0 ||parseInt(nbrPowers) > 0 ||parseInt(nbrNetworks) > 0){
			stSpaces = stSpaces.slice(0,strLen-1);
		}
		
	stSpaces += ']};';
    return stSpaces;
}