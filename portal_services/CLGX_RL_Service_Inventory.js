nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_Service_Inventory.js
//	Script Name:	CLGX_RL_Service_Inventory
//	Script Id:		customscript_clgx_rl_serv_inv
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		02/21/2013
//-------------------------------------------------------------------------------------------------

function restlet_serv_inv (){
	try {
		var objFile = nlapiLoadFile(340902);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{customersJSON}','g'),customersJSON());
		html = html.replace(new RegExp('{locationsJSON}','g'),locationsJSON());
		html = html.replace(new RegExp('{servicesJSON}','g'),servicesJSON());
		return html;
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function customersJSON(){

	var stCustomers = 'var dataCustomers =  [';
	
	var searchCustomers = nlapiLoadSearch('transaction', 'customsearch_clgx_servinv_customers');
	var resultSet = searchCustomers.runSearch();
	resultSet.forEachResult(function(searchResult) {

	var columnsCustomer = searchResult.getAllColumns();
	
	stCustomers += '{customer:"' + searchResult.getText('entity', null, 'GROUP') + 
					'",customerid:' +  searchResult.getValue('entity', null, 'GROUP') + 
					',orders:' + searchResult.getValue('internalid', null, 'COUNT') + 
					',space:' + parseInt(searchResult.getValue(columnsCustomer[2])) + 
					',power:' + parseInt(searchResult.getValue(columnsCustomer[3])) + 
					',network:' + parseInt(searchResult.getValue(columnsCustomer[4])) + 
					',xcs:' + parseInt(searchResult.getValue(columnsCustomer[5])) + 
					',vxcs:' + parseInt(searchResult.getValue(columnsCustomer[6])) + 
					',other:' + parseInt(searchResult.getValue(columnsCustomer[7])) + '},'; 
	
	return true; // return true to keep iterating
	});

	var strLen = stCustomers.length;
	if (searchCustomers != null){
		stCustomers = stCustomers.slice(0,strLen-1);
	}
	stCustomers += '];';
    return stCustomers;
}

function locationsJSON(){

	var stLocations = 'var dataLocations =  [';
	
	var searchLocations = nlapiLoadSearch('transaction', 'customsearch_clgx_servinv_locations');
	var resultSet = searchLocations.runSearch();
	resultSet.forEachResult(function(searchResult) {

	var columnsLocation = searchResult.getAllColumns();
	
	stLocations += '{location:"' + searchResult.getValue('locationnohierarchy', null, 'GROUP') + 
					'",locationid:' +  searchResult.getValue('location', null, 'GROUP') + 
					',customer:"' +  searchResult.getText('entity', null, 'GROUP') + 
					'",customerid:' +  searchResult.getValue('entity', null, 'GROUP') + 
					',orders:' + searchResult.getValue('internalid', null, 'COUNT') + 
					',space:' + parseInt(searchResult.getValue(columnsLocation[4])) + 
					',power:' + parseInt(searchResult.getValue(columnsLocation[5])) + 
					',network:' + parseInt(searchResult.getValue(columnsLocation[6])) + 
					',xcs:' + parseInt(searchResult.getValue(columnsLocation[7])) + 
					',vxcs:' + parseInt(searchResult.getValue(columnsLocation[8])) + 
					',other:' + parseInt(searchResult.getValue(columnsLocation[9])) + '},'; 
	
	return true; // return true to keep iterating
	});

	var strLen = stLocations.length;
	if (searchLocations != null){
		stLocations = stLocations.slice(0,strLen-1);
	}
	stLocations += '];';
    return stLocations;
}

function servicesJSON () {

	var stServices = 'var dataFacilities =  {"text":".","children": [';

	// ----------------------------------  Cross Connects ---------------------------------------------

	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custcol_cologix_invoice_item_category",null,"anyof",4));
	var searchSOs = nlapiSearchRecord("salesorder", 'customsearch_clgx_servinv_locations_only', arrFilters, arrColumns);
	var searchSO = searchSOs[0];
	var columnsSO = searchSO.getAllColumns();
	var TnbrCustomers = parseInt(searchSO.getValue(columnsSO[0]));
	var TnbrSOs = parseInt(searchSO.getValue(columnsSO[1]));
	var TnbrXCs = parseInt(searchSO.getValue(columnsSO[5]));
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_xc_facility',null,'GROUP').setSort(false));
	arrColumns.push(new nlobjSearchColumn('internalid',null,'COUNT'));
	var arrFilters = new Array();
	//arrFilters.push(new nlobjSearchFilter("parent",'job',"noneof",[2763,2790,2798]));
	var searchServices = nlapiSearchRecord("customrecord_cologix_crossconnect", 'customsearch_clgx_servinv_xcs', arrFilters, arrColumns);

	if(searchServices != null){
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,'COUNT'));
		var arrFilters = new Array();
		var searchServicesTotal = nlapiSearchRecord("customrecord_cologix_crossconnect", 'customsearch_clgx_servinv_xcs', arrFilters, arrColumns);
		var nbrServices = searchServicesTotal[0].getValue('internalid', null, 'COUNT');
	}
	else{
		var nbrServices = 0;
	}

	stServices += '{entity:"Cross Connect"' + 
					', customers:' + TnbrCustomers  +
					', sos:' + TnbrSOs  +
					', bill:' + TnbrXCs  +
					', nim:' + nbrServices  +
					', expanded:false, iconCls:"xconnect", leaf:false, children:['; 
	
	for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
		var searchService = searchServices[i];
		var facility = searchService.getText('custrecord_clgx_xc_facility', null, 'GROUP');
		var facilityid = searchService.getValue('custrecord_clgx_xc_facility', null, 'GROUP');
		nbrNIMs = searchService.getValue('internalid', null, 'COUNT');
		if (facilityid == ''){
			facilityid = 0;
		}
		
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("location",null,"anyof",clgx_return_locationid(facilityid)));
		arrFilters.push(new nlobjSearchFilter("custcol_cologix_invoice_item_category",null,"anyof",4));
		var searchSOs = nlapiSearchRecord("salesorder", 'customsearch_clgx_servinv_locations_only', arrFilters, arrColumns);
		var searchSO = searchSOs[0];
		var columnsSO = searchSO.getAllColumns();
		var nbrCustomers = parseInt(searchSO.getValue(columnsSO[0]));
		var nbrSOs = parseInt(searchSO.getValue(columnsSO[1]));
		var nbrXCs = parseInt(searchSO.getValue(columnsSO[5]));

		stServices += '{entity:"' + facility + 
						'", entityid:' + facilityid  +	
						', customers:' + nbrCustomers  +
						', sos:' + nbrSOs  +
						', bill:' + nbrXCs  +
						', nim:' + nbrNIMs  +
						',service: "xconnect", iconCls:"facility", leaf:true},'; 
	}
	var strLen = stServices.length;
	if (searchServices != null){
		stServices = stServices.slice(0,strLen-1);
	}
	stServices += ']},';
	
	// ----------------------------------  Virtual Cross Connects ---------------------------------------------

	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custcol_cologix_invoice_item_category",null,"anyof",11));
	var searchSOs = nlapiSearchRecord("salesorder", 'customsearch_clgx_servinv_locations_only', arrFilters, arrColumns);
	var searchSO = searchSOs[0];
	var columnsSO = searchSO.getAllColumns();
	var TnbrCustomers = parseInt(searchSO.getValue(columnsSO[0]));
	var TnbrSOs = parseInt(searchSO.getValue(columnsSO[1]));
	var TnbrVXCs = parseInt(searchSO.getValue(columnsSO[6]));
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_vxc_facility',null,'GROUP').setSort(false));
	arrColumns.push(new nlobjSearchColumn('internalid',null,'COUNT'));
	var arrFilters = new Array();
	var searchServices = nlapiSearchRecord("customrecord_cologix_vxc", 'customsearch_clgx_servinv_vxcs', arrFilters, arrColumns);

	if(searchServices != null){
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,'COUNT'));
		var arrFilters = new Array();
		var searchServicesTotal = nlapiSearchRecord("customrecord_cologix_vxc", 'customsearch_clgx_servinv_vxcs', arrFilters, arrColumns);
		var nbrServices = searchServicesTotal[0].getValue('internalid', null, 'COUNT');
	}
	else{
		var nbrServices = 0;
	}

	stServices += '{entity:"Virtual Cross Connect"' + 
					', customers:' + TnbrCustomers  +
					', sos:' + TnbrSOs  +
					', bill:' + TnbrVXCs  +
					', nim:' + nbrServices  +
					', expanded:false, iconCls:"vxconnect", leaf:false, children:['; 
	
	for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
		var searchService = searchServices[i];
		var facility = searchService.getText('custrecord_cologix_vxc_facility', null, 'GROUP');
		var facilityid = searchService.getValue('custrecord_cologix_vxc_facility', null, 'GROUP');
		nbrNIMs = searchService.getValue('internalid', null, 'COUNT');
		if (facilityid == ''){
			facilityid = 0;
		}
		
		var arrColumns = new Array();
		var arrFilters = new Array();
		var locationid = clgx_return_locationid(facilityid);
		if(locationid == ''){
			locationid = 0;
		}
		arrFilters.push(new nlobjSearchFilter("location",null,"anyof",clgx_return_locationid(facilityid)));
		arrFilters.push(new nlobjSearchFilter("custcol_cologix_invoice_item_category",null,"anyof",11));
		var searchSOs = nlapiSearchRecord("salesorder", 'customsearch_clgx_servinv_locations_only', arrFilters, arrColumns);
		var searchSO = searchSOs[0];
		var columnsSO = searchSO.getAllColumns();
		var nbrCustomers = parseInt(searchSO.getValue(columnsSO[0]));
		var nbrSOs = parseInt(searchSO.getValue(columnsSO[1]));
		var nbrVXCs = parseInt(searchSO.getValue(columnsSO[6]));

		stServices += '{entity:"' + facility + 
						'", entityid:' + facilityid  +	
						', customers:' + nbrCustomers  +
						', sos:' + nbrSOs  +
						', bill:' + nbrVXCs  +
						', nim:' + nbrNIMs  +
						',service: "vxconnect", iconCls:"facility", leaf:true},'; 
		
	}
	var strLen = stServices.length;
	if (searchServices != null){
		stServices = stServices.slice(0,strLen-1);
	}

	stServices += ']},';
	
	// ----------------------------------  Power ---------------------------------------------
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custcol_cologix_invoice_item_category",null,"anyof",8));
	var searchSOs = nlapiSearchRecord("salesorder", 'customsearch_clgx_servinv_locations_only', arrFilters, arrColumns);
	var searchSO = searchSOs[0];
	var columnsSO = searchSO.getAllColumns();
	var TnbrCustomers = parseInt(searchSO.getValue(columnsSO[0]));
	var TnbrSOs = parseInt(searchSO.getValue(columnsSO[1]));
	var TnbrPowers = parseInt(searchSO.getValue(columnsSO[3]));
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_facility',null,'GROUP').setSort(false));
	arrColumns.push(new nlobjSearchColumn('internalid',null,'COUNT'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	var searchServices = nlapiSearchRecord("customrecord_clgx_power_circuit", 'customsearch_clgx_servinv_powers', arrFilters, arrColumns);

	if(searchServices != null){
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,'COUNT'));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
		var searchServicesTotal = nlapiSearchRecord("customrecord_clgx_power_circuit", 'customsearch_clgx_servinv_powers', arrFilters, arrColumns);
		var nbrServices = searchServicesTotal[0].getValue('internalid', null, 'COUNT');
	}
	else{
		var nbrServices = 0;
	}

	stServices += '{entity:"Power"' + 
					', customers:' + TnbrCustomers  +
					', sos:' + TnbrSOs  +
					', bill:' + TnbrPowers  +
					', nim:' + nbrServices  +
					', expanded:false, iconCls:"power", leaf:false, children:['; 
	
	for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
		var searchService = searchServices[i];
		var facility = searchService.getText('custrecord_cologix_power_facility', null, 'GROUP');
		var facilityid = searchService.getValue('custrecord_cologix_power_facility', null, 'GROUP');
		nbrNIMs = searchService.getValue('internalid', null, 'COUNT');
		if (facilityid == ''){
			facilityid = 0;
		}
		
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("location",null,"anyof",clgx_return_locationid(facilityid)));
		arrFilters.push(new nlobjSearchFilter("custcol_cologix_invoice_item_category",null,"anyof",8));
		var searchSOs = nlapiSearchRecord("salesorder", 'customsearch_clgx_servinv_locations_only', arrFilters, arrColumns);
		var searchSO = searchSOs[0];
		var columnsSO = searchSO.getAllColumns();
		var nbrCustomers = parseInt(searchSO.getValue(columnsSO[0]));
		var nbrSOs = parseInt(searchSO.getValue(columnsSO[1]));
		var nbrPowers = parseInt(searchSO.getValue(columnsSO[3]));

		stServices += '{entity:"' + facility + 
						'", entityid:' + facilityid  +	
						', customers:' + nbrCustomers  +
						', sos:' + nbrSOs  +
						', bill:' + nbrPowers  +
						', nim:' + nbrNIMs  +
						',service: "power", iconCls:"facility", leaf:true},'; 
		
	}
	var strLen = stServices.length;
	if (searchServices != null){
		stServices = stServices.slice(0,strLen-1);
	}

	stServices += ']},';
	
	// ----------------------------------  Space ---------------------------------------------

	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custcol_cologix_invoice_item_category",null,"anyof",10));
	var searchSOs = nlapiSearchRecord("salesorder", 'customsearch_clgx_servinv_locations_only', arrFilters, arrColumns);
	var searchSO = searchSOs[0];
	var columnsSO = searchSO.getAllColumns();
	var TnbrCustomers = parseInt(searchSO.getValue(columnsSO[0]));
	var TnbrSOs = parseInt(searchSO.getValue(columnsSO[1]));
	var TnbrSpaces = parseInt(searchSO.getValue(columnsSO[2]));
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_space_location',null,'GROUP').setSort(false));
	arrColumns.push(new nlobjSearchColumn('internalid',null,'COUNT'));
	var arrFilters = new Array();
	var searchServices = nlapiSearchRecord("customrecord_cologix_space", 'customsearch_clgx_servinv_spaces', arrFilters, arrColumns);

	if(searchServices != null){
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,'COUNT'));
		var arrFilters = new Array();
		var searchServicesTotal = nlapiSearchRecord("customrecord_cologix_space", 'customsearch_clgx_servinv_spaces', arrFilters, arrColumns);
		var nbrServices = searchServicesTotal[0].getValue('internalid', null, 'COUNT');
	}
	else{
		var nbrServices = 0;
	}

	stServices += '{entity:"Space"' + 
					', customers:' + TnbrCustomers  +
					', sos:' + TnbrSOs  +
					', bill:' + TnbrSpaces  +
					', nim:' + nbrServices  +
					', expanded:false, iconCls:"space", leaf:false, children:['; 
	
	for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
		var searchService = searchServices[i];
		var facility = searchService.getText('custrecord_cologix_space_location', null, 'GROUP');
		var facilityid = searchService.getValue('custrecord_cologix_space_location', null, 'GROUP');
		nbrNIMs = searchService.getValue('internalid', null, 'COUNT');
		if (facilityid == ''){
			facilityid = 0;
		}
		
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("location",null,"anyof",clgx_return_locationid(facilityid)));
		arrFilters.push(new nlobjSearchFilter("custcol_cologix_invoice_item_category",null,"anyof",10));
		var searchSOs = nlapiSearchRecord("salesorder", 'customsearch_clgx_servinv_locations_only', arrFilters, arrColumns);
		var searchSO = searchSOs[0];
		var columnsSO = searchSO.getAllColumns();
		var nbrCustomers = parseInt(searchSO.getValue(columnsSO[0]));
		var nbrSOs = parseInt(searchSO.getValue(columnsSO[1]));
		var nbrSpaces = parseInt(searchSO.getValue(columnsSO[2]));

		stServices += '{entity:"' + facility + 
						'", entityid:' + facilityid  +	
						', customers:' + nbrCustomers  +
						', sos:' + nbrSOs  +
						', bill:' + nbrSpaces  +
						', nim:' + nbrNIMs  +
						',service: "space", iconCls:"facility", leaf:true},'; 
		
	}
	var strLen = stServices.length;
	if (searchServices != null){
		stServices = stServices.slice(0,strLen-1);
	}

	stServices += ']},';
	
	// ----------------------------------  Network ---------------------------------------------
	/*
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_eq_service_facility',null,'GROUP').setSort(false));
	arrColumns.push(new nlobjSearchColumn('internalid',null,'COUNT'));
	var arrFilters = new Array();
	var searchServices = nlapiSearchRecord("customrecord_cologix_equipment", null, arrFilters, arrColumns);

	if(searchServices != null){
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,'COUNT'));
		var arrFilters = new Array();
		var searchServicesTotal = nlapiSearchRecord("customrecord_cologix_equipment", null, arrFilters, arrColumns);
		var nbrServices = searchServicesTotal[0].getValue('internalid', null, 'COUNT');
	}
	else{
		var nbrServices = 0;
	}
	stServices += '{entity:"Network", nim:'+ nbrServices +', bill:'+ nbrNets +', expanded:false, iconCls:"network", leaf:false, children:['; 
	
	for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
		var searchService = searchServices[i];
		var facility = searchService.getText('custrecord_cologix_eq_service_facility', null, 'GROUP');
		var facilityid = searchService.getValue('custrecord_cologix_eq_service_facility', null, 'GROUP');
		nbrNIMs = searchService.getValue('internalid', null, 'COUNT');
		if (facilityid == ''){
			facilityid = 0;
		}
		stServices += '{entity:"' + facility + 
						'", entityid:' + facilityid  +		
						', nim:' + nbrNIMs  +
						',service: "network", iconCls:"facility", leaf:true},'; 
	}
	var strLen = stServices.length;
	if (searchServices != null){
		stServices = stServices.slice(0,strLen-1);
	}

	stServices += ']},';
	*/
	// ----------------------------------  END ---------------------------------------------

	var strLen = stServices.length;
	stServices = stServices.slice(0,strLen-1);
	stServices += ']};';
    return stServices;
}

