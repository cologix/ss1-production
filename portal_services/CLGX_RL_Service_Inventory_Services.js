nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_Service_Inventory_Services.js
//	Script Name:	CLGX_RL_Service_Inventory_Services
//	Script Id:		customscript_clgx_rl_serv_inv_services
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		05/15/2013
//-------------------------------------------------------------------------------------------------

function restlet_serv_inv_services (datain){
	try {
		var txtArg = datain;
		txtArg = txtArg.replace(/\"/g,"");
		txtArg = txtArg.replace(/\:/g,",");
		txtArg = txtArg.replace(/\{/g,"");
		txtArg = txtArg.replace(/\}/g,"");
		var arrArguments = [];
		var arrArguments = txtArg.split( "," );

		if(arrArguments[0] != '' ){

			var facility = nlapiLookupField('customrecord_cologix_facility', arrArguments[1], 'name');
			
			switch(arrArguments[3]) {
			case 'xconnect' : 
				var objFile = nlapiLoadFile(431480);
				var html = objFile.getValue();
				html = html.replace(new RegExp('{xconnectJSON}','g'),xconnectJSON(arrArguments[1]));
				html = html.replace(new RegExp('{title}','g'), facility + ' | Cross Connect');
				break;
			case 'vxconnect': 
				var objFile = nlapiLoadFile(430403);
				var html = objFile.getValue();
				html = html.replace(new RegExp('{servicesJSON}','g'),vxconnectJSON(arrArguments[1]));
				html = html.replace(new RegExp('{title}','g'), facility + ' | Virtual Cross Connect');
				break;
			case 'space': 
				var objFile = nlapiLoadFile(433607);
				var html = objFile.getValue();
				html = html.replace(new RegExp('{spaceJSON}','g'),spaceJSON(arrArguments[1]));
				html = html.replace(new RegExp('{title}','g'), facility + ' | Space');
				break;
			case 'power': 
				var objFile = nlapiLoadFile(431380);
				var html = objFile.getValue();
				html = html.replace(new RegExp('{powerJSON}','g'),powerJSON(arrArguments[1]));
				html = html.replace(new RegExp('{title}','g'), facility + ' | Power');
				break;
			case 'network': 
				var objFile = nlapiLoadFile(430403);
				var html = objFile.getValue();
				html = html.replace(new RegExp('{servicesJSON}','g'),networkJSON(arrArguments[1]));
				html = html.replace(new RegExp('{titleServices}','g'), facility + ' | Network');
				break;
			default:
				var html = 'Please select a record from the above panel to see details.';
			}
		}
		else{
			var html = 'Please select a record from the above panel to see details.';
		}
		return html;
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function xconnectJSON (facilityid){
	var stServices = 'var dataXC =  [';
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('name',null,null).setSort(false));
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_xc_service',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_xconnect_service_order',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_carrier_name',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_xc_circuit_type',null,null));
	var arrFilters = new Array();
	if (facilityid == 0){
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_xc_facility",null,"anyof",'@NONE@'));
	}
	else{
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_xc_facility",null,"anyof",facilityid));
	}
	//var searchServices = nlapiSearchRecord("customrecord_cologix_crossconnect", 'customsearch_clgx_servinv_xcs', arrFilters, arrColumns);
	
	var searchServices = nlapiLoadSearch('customrecord_cologix_crossconnect', 'customsearch_clgx_servinv_xcs');
	searchServices.addFilters(arrFilters);
	searchServices.addColumns(arrColumns);

	var resultSet = searchServices.runSearch();
	resultSet.forEachResult(function(searchResult) {

		var xc = searchResult.getValue('name', null, null);
		var xcid = searchResult.getValue('internalid', null, null);
		var service = searchResult.getText('custrecord_cologix_xc_service', null, null);
		var serviceid = searchResult.getValue('custrecord_cologix_xc_service', null, null);
		var so = searchResult.getText('custrecord_xconnect_service_order', null, null);
		var soid = searchResult.getValue('custrecord_xconnect_service_order', null, null);
		var carrier = searchResult.getText('custrecord_cologix_carrier_name', null, null);
		var circuit = searchResult.getText('custrecord_cologix_xc_circuit_type', null, null);
		
		if (serviceid == ''){
			serviceid = 0;
		}
		if (soid == ''){
			soid = 0;
		}
		stServices += '{xc:"' + xc + 
				'",xcid:' +  xcid + 
				',service:"' +  service + 
				'",serviceid:' +  serviceid + 
				',so:"' +  so.replace(/\Service Order #/g,"") + 
				'",soid:' + soid + 
				',carrier:"' +  carrier +
				'",circuit:"' +  circuit + '"},';
		return true;                // return true to keep iterating
		});
	
	
	
	
	/*
	for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
		var searchService = searchServices[i];

		var xc = searchService.getValue('name', null, null);
		var xcid = searchService.getValue('internalid', null, null);
		var service = searchService.getText('custrecord_cologix_xc_service', null, null);
		var serviceid = searchService.getValue('custrecord_cologix_xc_service', null, null);
		var so = searchService.getText('custrecord_xconnect_service_order', null, null);
		var soid = searchService.getValue('custrecord_xconnect_service_order', null, null);
		
		if (serviceid == ''){
			serviceid = 0;
		}
		if (soid == ''){
			soid = 0;
		}
		stServices += '{xc:"' + xc + 
						'",xcid:' +  xcid + 
						',service:"' +  service + 
						'",serviceid:' +  serviceid + 
						',so:"' +  so.replace(/\Service Order #/g,"") + 
						'",soid:' + soid + 
						',carrier:"' +  searchService.getText('custrecord_cologix_carrier_name', null, null) +
						'",circuit:"' +  searchService.getText('custrecord_cologix_xc_circuit_type', null, null) + '"},';
	}
	*/
	
	
	
	
	var strLen = stServices.length;
	//if (searchService != null){
		stServices = stServices.slice(0,strLen-1);
	//}
	stServices += '];';
    return stServices;
}

function vxconnectJSON (facilityid){
	var stServices = 'var dataServices =  [';
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('name',null,null).setSort(false));
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_vxc_service',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_service_order',null,null));
	var arrFilters = new Array();
	if (facilityid == 0){
		arrFilters.push(new nlobjSearchFilter("custrecord_cologix_vxc_facility",null,"anyof",'@NONE@'));
	}
	else{
		arrFilters.push(new nlobjSearchFilter("custrecord_cologix_vxc_facility",null,"anyof",facilityid));
	}
	var searchServices = nlapiSearchRecord("customrecord_cologix_vxc", 'customsearch_clgx_servinv_vxcs', arrFilters, arrColumns);
	
	for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
		var searchService = searchServices[i];

		var name = searchService.getValue('name', null, null);
		var nameid = searchService.getValue('internalid', null, null);
		var service = searchService.getText('custrecord_cologix_vxc_service', null, null);
		var serviceid = searchService.getValue('custrecord_cologix_vxc_service', null, null);
		var so = searchService.getText('custrecord_cologix_service_order', null, null);
		var soid = searchService.getValue('custrecord_cologix_service_order', null, null);
		
		if (serviceid == ''){
			serviceid = 0;
		}
		if (soid == ''){
			soid = 0;
		}
		
		stServices += '{name:"' + name + 
						'",nameid:' +  nameid + 
						',service:"' +  service + 
						'",serviceid:' +  serviceid + 
						',so:"' +  so.replace(/\Service Order #/g,"") + 
						'",soid:' + soid + 
						',rectype:54},';
	}
	var strLen = stServices.length;
	if (searchService != null){
		stServices = stServices.slice(0,strLen-1);
	}
	stServices += '];';
    return stServices;
}

function spaceJSON (facilityid){
	var stServices = 'var dataSpace =  [';
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('name',null,null).setSort(false));
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_space_project',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_space_service_order',null,null));
	
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_space_square_feet',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_space_status',null,null));
	var arrFilters = new Array();
	if (facilityid == 0){
		arrFilters.push(new nlobjSearchFilter("custrecord_cologix_space_location",null,"anyof",'@NONE@'));
	}
	else{
		arrFilters.push(new nlobjSearchFilter("custrecord_cologix_space_location",null,"anyof",facilityid));
	}
	//var searchServices = nlapiSearchRecord("customrecord_cologix_space", 'customsearch_clgx_servinv_spaces', arrFilters, arrColumns);
	

	var searchServices = nlapiLoadSearch('customrecord_cologix_space', 'customsearch_clgx_servinv_spaces');
	searchServices.addFilters(arrFilters);
	searchServices.addColumns(arrColumns);

	var resultSet = searchServices.runSearch();
	resultSet.forEachResult(function(searchResult) {

		var name = searchResult.getValue('name', null, null);
		var nameid = searchResult.getValue('internalid', null, null);
		var service = searchResult.getText('custrecord_cologix_space_project', null, null);
		var serviceid = searchResult.getValue('custrecord_cologix_space_project', null, null);
		var so = searchResult.getText('custrecord_space_service_order', null, null);
		var soid = searchResult.getValue('custrecord_space_service_order', null, null);
		var sqfeet = searchResult.getValue('custrecord_cologix_space_square_feet', null, null);
		var provisioning = searchResult.getText('custrecord_cologix_space_status', null, null);
		
		if (serviceid == ''){
			serviceid = 0;
		}
		if (soid == ''){
			soid = 0;
		}
		if (sqfeet == ''){
			sqfeet = 0;
		}
		
		stServices += '{space:"' + name + 
					'",spaceid:' +  nameid + 
					',service:"' +  service + 
					'",serviceid:' +  serviceid + 
					',so:"' +  so.replace(/\Service Order #/g,"") + 
					'",soid:' + soid + 
					',sqfeet:' + sqfeet + 
					',provisioning:"' + provisioning + '"},';

		return true;                // return true to keep iterating
		});

	
	/*
	for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
		var searchService = searchServices[i];

		var name = searchService.getValue('name', null, null);
		var nameid = searchService.getValue('internalid', null, null);
		var service = searchService.getText('custrecord_cologix_space_project', null, null);
		var serviceid = searchService.getValue('custrecord_cologix_space_project', null, null);
		var so = searchService.getText('custrecord_space_service_order', null, null);
		var soid = searchService.getValue('custrecord_space_service_order', null, null);
		var sqfeet = searchService.getValue('custrecord_cologix_space_square_feet', null, null);
		var provisioning = searchService.getText('custrecord_cologix_space_status', null, null);
		
		if (serviceid == ''){
			serviceid = 0;
		}
		if (soid == ''){
			soid = 0;
		}
		if (sqfeet == ''){
			sqfeet = 0;
		}
		stServices += '{space:"' + name + 
						'",spaceid:' +  nameid + 
						',service:"' +  service + 
						'",serviceid:' +  serviceid + 
						',so:"' +  so.replace(/\Service Order #/g,"") + 
						'",soid:' + soid + 
						',sqfeet:' + sqfeet + 
						',provisioning:"' + provisioning + '"},';
	}
	*/

	var strLen = stServices.length;
	//if (searchService != null){
		stServices = stServices.slice(0,strLen-1);
	//}
	stServices += '];';
    return stServices;
}

function powerJSON (facilityid){
	var stServices = 'var dataPower =  [';

	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('name',null,null).setSort(false));
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_space',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_ups_rect',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_upsbreaker',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_subpanel',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_circuitbreaker',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_status',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_power_service',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_power_circuit_service_order',null,null));
	var arrFilters = new Array();
	if (facilityid == 0){
		arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_facility",null,"anyof",'@NONE@'));
	}
	else{
		arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_facility",null,"anyof",facilityid));
	}
	arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	var searchServices = nlapiSearchRecord("customrecord_clgx_power_circuit", 'customsearch_clgx_servinv_powers', arrFilters, arrColumns);
	
	for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
		var searchService = searchServices[i];

		var serviceid = searchService.getValue('custrecord_cologix_power_service', null, null);
		var soid = searchService.getValue('custrecord_power_circuit_service_order', null, null);
		var spaceid = searchService.getValue('custrecord_cologix_power_space', null, null);
		var upsid = searchService.getValue('custrecord_cologix_power_ups_rect', null, null);
		
		if (serviceid == ''){serviceid = 0;}
		if (soid == ''){soid = 0;}
		if (spaceid == ''){spaceid = 0;}
		if (upsid == ''){upsid = 0;}
		
		stServices += '{power:"' + searchService.getValue('name', null, null) + 
					'", powerid:' + searchService.getValue('internalid', null, null) +	
					',service:"' +  searchService.getText('custrecord_cologix_power_service', null, null) + 
					'",serviceid:' +  serviceid + 
					',so:"' +  searchService.getText('custrecord_power_circuit_service_order', null, null).replace(/\Service Order #/g,"") + 
					'",soid:' + soid + 
					', space:"' + searchService.getText('custrecord_cologix_power_space', null, null) +
					'", spaceid:' + spaceid +
					', ups:"' + searchService.getText('custrecord_cologix_power_ups_rect', null, null) +
					'", upsid:' + upsid +
					', upsbreak:"' + searchService.getValue('custrecord_cologix_power_upsbreaker', null, null) +
					'", subpanel:"' + searchService.getValue('custrecord_cologix_power_subpanel', null, null) +
					'", circuit:"' + searchService.getValue('custrecord_cologix_power_circuitbreaker', null, null) +
					'", provisioning:"' + searchService.getText('custrecord_cologix_power_status', null, null) + 
					'",rectype:17},';
	}
	var strLen = stServices.length;
	if (searchServices != null){
		stServices = stServices.slice(0,strLen-1);
	}
	stServices += '];';
    return stServices;
}

function networkJSON (facilityid){
	var stServices = 'var dataServices =  [';
	
	var arrColumns = new Array();
	//arrColumns.push(new nlobjSearchColumn('name',null,null).setSort(false));
	arrColumns.push(new nlobjSearchColumn('internalid',null,null).setSort(true));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_equipment_service',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_cologix_eq_service_order',null,null));
	var arrFilters = new Array();
	if (facilityid == 0){
		arrFilters.push(new nlobjSearchFilter("custrecord_cologix_eq_service_facility",null,"anyof",'@NONE@'));
	}
	else{
		arrFilters.push(new nlobjSearchFilter("custrecord_cologix_eq_service_facility",null,"anyof",facilityid));
	}
	var searchServices = nlapiSearchRecord("customrecord_cologix_equipment", null, arrFilters, arrColumns);
	
	for ( var i = 0; searchServices != null && i < searchServices.length; i++ ) {
		var searchService = searchServices[i];

		var name = searchService.getValue('internalid', null, null);
		var nameid = searchService.getValue('internalid', null, null);
		var service = searchService.getText('custrecord_cologix_equipment_service', null, null);
		var serviceid = searchService.getValue('custrecord_cologix_equipment_service', null, null);
		var so = searchService.getText('custrecord_cologix_eq_service_order', null, null);
		var soid = searchService.getValue('custrecord_cologix_eq_service_order', null, null);
		
		if (serviceid == ''){
			serviceid = 0;
		}
		if (soid == ''){
			soid = 0;
		}
		
		stServices += '{name:"' + name + 
						'",nameid:' +  nameid + 
						',service:"' +  service + 
						'",serviceid:' +  serviceid + 
						',so:"' +  so.replace(/\Service Order #/g,"") + 
						'",soid:' + soid + 
						',rectype:20},';
	}
	var strLen = stServices.length;
	if (searchService != null){
		stServices = stServices.slice(0,strLen-1);
	}
	stServices += '];';
    return stServices;
}