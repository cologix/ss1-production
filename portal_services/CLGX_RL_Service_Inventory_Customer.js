nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_Service_Inventory_Customer.js
//	Script Name:	CLGX_RL_Service_Inventory_Customer
//	Script Id:		customscript_clgx_rl_serv_inv_customer
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/31/2013
//-------------------------------------------------------------------------------------------------

function restlet_serv_inv_customer (datain){
	try {
		var txtArg = datain;
                txtArg = txtArg.replace(/\"/g,"");
		txtArg = txtArg.replace(/\:/g,",");
		txtArg = txtArg.replace(/\{/g,"");
		txtArg = txtArg.replace(/\}/g,"");
		var arrArguments = new Array();
		arrArguments = txtArg.split( "," );

		// ["locationid","0","customerid","217943"]
		
		if((arrArguments.length==4)&&(arrArguments[3] != '' )){
                  	var objFile = nlapiLoadFile(340903);
			var html = objFile.getValue();
			html = html.replace(new RegExp('{customerJSON}','g'),customerJSON(arrArguments[3],arrArguments[1]));
			html = html.replace(new RegExp('{customerName}','g'),nlapiLookupField('customer', arrArguments[3], 'companyname').replace(/\'/g," "));
		}
		else{
			var html = 'Please select a record from the left panel and click on "View" to see details.';
		}
		return html;
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function customerJSON(customerid, locationid){

	var arrServices = new Array();
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
	if(parseInt(locationid) > 0 ){
		arrFilters.push(new nlobjSearchFilter("location",null,"anyof",locationid));
	}
	var searchLocations = nlapiSearchRecord('transaction', 'customsearch_clgx_servinv_customer_locat', arrFilters, arrColumns);
	
	var stCustomer = 'var dataCustomer =  {"text":".","children": [';
              
	for ( var i = 0; searchLocations != null && i < searchLocations.length; i++ ) {
		var searchLocation = searchLocations[i];
		var currentLocation = searchLocation.getValue('location', null, 'GROUP');
		
		var totLocBill = 0;
		var totLocNim = 0;
		
		stCustomer += '{entity:"' + searchLocation.getValue('locationnohierarchy', null, 'GROUP') + 
						'",expanded:false, iconCls:"facility", leaf:false, children:['; 
		
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
		arrFilters.push(new nlobjSearchFilter("location",null,"anyof",currentLocation));
		var searchCategories = nlapiSearchRecord("transaction", 'customsearch_clgx_servinv_customer_categ', arrFilters, arrColumns);
		
		for ( var j = 0; searchCategories != null && j < searchCategories.length; j++ ) {
			var searchCategory = searchCategories[j];
			var currentCategory = searchCategory.getValue('custcol_cologix_invoice_item_category', null, 'GROUP');

			var totCategBill = 0;
			var totCategNim = 0;
			
			var category = searchCategory.getText('custcol_cologix_invoice_item_category', null, 'GROUP');
			var icon = function_get_icon (category);
				
			stCustomer += '{entity:"' + category + 
							'", iconCls:"' + icon +
							'",leaf:false' + 
							', children:['; 
			
			var arrColumns = new Array();
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
			arrFilters.push(new nlobjSearchFilter("location",null,"anyof",currentLocation));
			arrFilters.push(new nlobjSearchFilter("custcol_cologix_invoice_item_category",null,"anyof",currentCategory));
			var searchItems = nlapiSearchRecord("transaction", 'customsearch_clgx_servinv_customer_items', arrFilters, arrColumns);
			
			
			for ( var k = 0; searchItems != null && k < searchItems.length; k++ ) {
				var searchItem = searchItems[k];
				var columnsItem = searchItem.getAllColumns();

				//var quantity = searchItem.getValue('custcol_clgx_qty2print', null, 'SUM');	
				var quantity = parseInt(searchItem.getValue(columnsItem[8]))
				var serviceName = searchItem.getText('custcol_clgx_so_col_service_id', null, 'GROUP');
				var serviceID = searchItem.getValue('custcol_clgx_so_col_service_id', null, 'GROUP');
				arrServices.push(serviceID);
				
				if (serviceName != '- None -'){
					var myRecord = nlapiLoadRecord('job', parseInt(serviceID), null, null);

					switch(category) {
					case 'Interconnection' : 
						var nbrNIMs = myRecord.getLineItemCount('recmachcustrecord_cologix_xc_service');
						break;
					case 'Virtual Interconnection': 
						var nbrNIMs = myRecord.getLineItemCount('recmachcustrecord_cologix_vxc_service');
						break;
					case 'Space': 
						var nbrNIMs = myRecord.getLineItemCount('recmachcustrecord_cologix_space_project');
						break;
					case 'Power': 
						//var nbrNIMs = myRecord.getLineItemCount('recmachcustrecord_cologix_power_service');
						
						var arrColumns = new Array();
						arrColumns.push(new nlobjSearchColumn('internalid',null,null));
						var arrFilters = new Array();
						arrFilters.push(new nlobjSearchFilter("custrecord_cologix_power_service",null,"anyof",parseInt(serviceID)));
						arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
						var searchPowers = nlapiSearchRecord('customrecord_clgx_power_circuit', null, arrFilters, arrColumns);
						
						if(searchPowers != null){
							var nbrNIMs = searchPowers.length;
						}
						else{
							var nbrNIMs = 0;
						}
				
						
						break;
					case 'Network': 
						var nbrNIMs = parseInt(myRecord.getLineItemCount('recmachcustrecord_cologix_xc_service')) + parseInt(myRecord.getLineItemCount('recmachcustrecord_clgx_oob_service_id'));
						break;
					default:
						var nbrNIMs = 0;
					}
					if(nbrNIMs == -1){nbrNIMs = 0}
				}
				else{
					var nbrNIMs = 0;
					var serviceID = 0;
				}
			
				stCustomer += '\n{entity:"' + searchItem.getText('item', null, 'GROUP') + 
								'",so:"' + searchItem.getValue('number', null, 'GROUP') + 
								'",soid:' + searchItem.getValue('internalid', null, 'GROUP') + 
								',service:"' + serviceName + 
								'",serviceid:' + serviceID + 
								', iconCls:"' + icon +
								'",leaf:true' + 
								',bill:' + quantity + 
								',nim:'+ nbrNIMs +'},'; 
				
				totCategBill += parseInt(quantity);
				totCategNim += parseInt(nbrNIMs);
				totLocBill += parseInt(quantity);
				totLocNim += parseInt(nbrNIMs);
				
			}
			var strLen = stCustomer.length;
			if (searchItems != null){
				stCustomer = stCustomer.slice(0,strLen-1);
			}
			stCustomer += '],'; 
			stCustomer += 'bill:' + totCategBill + ','; 
			stCustomer += 'nim:' + totCategNim; 
			stCustomer += '},'; 
		}
		var strLen = stCustomer.length;
		if (searchCategories != null){
			stCustomer = stCustomer.slice(0,strLen-1);
		}
		stCustomer += '],'; 
		stCustomer += 'bill:' + totLocBill + ','; 
		stCustomer += 'nim:' + totLocNim; 
		stCustomer += '},'; 
	}
	
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn("internalid",null));
	arrColumns.push(new nlobjSearchColumn("entityid",null));
	arrColumns.push(new nlobjSearchColumn("custentity_cologix_service_order",null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("parent",null,"anyof",customerid));
	arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
	//arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",arrServices));
	if(parseInt(locationid) > 0 ){
		arrFilters.push(new nlobjSearchFilter("custentity_cologix_facility",null,"anyof",returnFacility(locationid)));
	}
	var searchServices = nlapiSearchRecord("job", null, arrFilters, arrColumns);
	
	if(searchServices != null){
		
		stCustomer += '{entity:"Orphan Services' + 
						'", iconCls:"attention' + 
						'", expanded:false' + 
						',leaf:false' + 
						',nim:9999999999' + 
						//',bill:' + searchServices.length +
						', children:['; 
		
		var nbrServicesTotal = 0;
		for ( var k = 0; searchServices != null && k < searchServices.length; k++ ) {
			var searchService = searchServices[k];
			
			var sid = searchService.getValue('internalid', null, null);
			
			var servRec = nlapiLoadRecord('job',sid);
			
			var nbrXCs = servRec.getLineItemCount('recmachcustrecord_cologix_xc_service');
			var nbrVXCs = servRec.getLineItemCount('recmachcustrecord_cologix_vxc_service');
			var nbrPowers = servRec.getLineItemCount('recmachcustrecord_cologix_power_service');
			var nbrSpaces = servRec.getLineItemCount('recmachcustrecord_cologix_space_project');
			if(parseInt(nbrXCs) == -1){nbrXCs = 0}
			if(parseInt(nbrVXCs) == -1){nbrVXCs = 0}
			if(parseInt(nbrPowers) == -1){nbrPowers = 0}
			if(parseInt(nbrSpaces) == -1){nbrSpaces = 0}
			
			var nbrServices = parseInt(nbrXCs) + parseInt(nbrVXCs) + parseInt(nbrPowers) + parseInt(nbrSpaces);
			
			var soid = searchService.getValue('custentity_cologix_service_order', null, null);
			if (soid == null || soid == ''){
				soid = 0;
				var so = 'none';
			}
			else{
				var so = searchService.getText('custentity_cologix_service_order', null, null);
			}
			
			if(!inArray(sid, arrServices)){
				nbrServicesTotal += parseInt(nbrServices);
				stCustomer += '\n{entity:"' + searchService.getValue('entityid', null, null) + 
							'",so:"' + so.replace(/\Service Order #/g,"") + 
							'",soid:' + soid + 
							',service:"' + searchService.getValue('entityid', null, null) + 
							'",serviceid:' + sid +  
							', iconCls:"attention' + 
							'",leaf:true' + 
							',bill:0' + 
							',nim:'+ nbrServices + '},'; 
			}
		}
		var strLen = stCustomer.length;
		if (searchServices != null && parseInt(nbrServicesTotal) > 0){
			stCustomer = stCustomer.slice(0,strLen-1);
		}
		
		stCustomer = stCustomer.replace('9999999999',nbrServicesTotal);
	
	stCustomer += ']},';
		
	}

	
	
	var strLen = stCustomer.length;
	if (searchLocations != null){
		stCustomer = stCustomer.slice(0,strLen-1);
	}
	stCustomer += ']};';
    return stCustomer;
}


function function_get_icon (categ){
	var icon = 'other';
	switch(categ) {
	case 'Interconnection' : 
		icon = 'xconnect';
		break;
	case 'Virtual Interconnection': 
		icon = 'vxconnect';
		break;
	case 'Space': 
		icon = 'space';
		break;
	case 'Power': 
		icon = 'power';
		break;
	case 'Network': 
		icon = 'network';
		break;
	default:
		icon = 'other';
	}
	return icon;
}

function inArray(val, arr){	
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}


function returnFacility(locationID){	
	var facilityID = 0;
	switch(locationID) {
	case 31:
		facilityID = 21;
		break;
	case '6':
		facilityID = 8;
		break;
	case '18':
		facilityID = 8;
		break;
	case '13':
		facilityID = 8;
		break;
	case '15':
		facilityID = 15;
		break;
	case '14':
		facilityID = 8;
		break;
	case '1':
		facilityID = 16;
		break;
	case '2':
		facilityID = 3;
		break;
	case '17':
		facilityID = 3;
		break;
	case '7':
		facilityID = 14;
		break;
	case '16':
		facilityID = 17;
		break;
	case '5':
		facilityID = 2;
		break;
	case '8':
		facilityID = 5;
		break;
	case '9':
		facilityID = 4;
		break;
	case '10':
		facilityID = 9;
		break;
	case '11':
		facilityID = 6;
		break;
	case '12':
		facilityID = 7;
		break;
	default:
		facilityID = 0;
	}
	return facilityID;
}