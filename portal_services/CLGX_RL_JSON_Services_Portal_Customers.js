//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_JSON_Services_Portal_Customers.js
//	Script Name:	CLGX_RL_JSON_Services_Portal_Customers
//	Script Id:		customscript_clgx_rl_json_sportal_customers
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/13/2012
//-------------------------------------------------------------------------------------------------

function restlet_json_customers_get(datain){
try {
	/*
	var arrColumns = new Array();
	var arrFilters = new Array();
	var searchCustomers = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_serv_customers', arrFilters, arrColumns);
	
	var stCustomers = '[';

	for ( var i = 0; searchCustomers != null && i < searchCustomers.length; i++ ) {
		var searchCustomer = searchCustomers[i];
		var columnsCustomer = searchCustomer.getAllColumns();
		
		stCustomers += '{customer:"' + searchCustomer.getText('entity', null, 'GROUP') + 
						'",customerid:' +  searchCustomer.getValue('entity', null, 'GROUP') + 
						',orders:' + searchCustomer.getValue('internalid', null, 'COUNT') + 
						',space:' + searchCustomer.getValue(columnsCustomer[2]) + 
						',power:' + searchCustomer.getValue(columnsCustomer[3]) + 
						',network:' + searchCustomer.getValue(columnsCustomer[4]) + 
						',xcs:' + searchCustomer.getValue(columnsCustomer[5]) + 
						',vxcs:' + searchCustomer.getValue(columnsCustomer[6]) + 
						',other:' + searchCustomer.getValue(columnsCustomer[7]) + '},'; 
	}
	var strLen = stCustomers.length;
	if (searchCustomers != null){
		stCustomers = stCustomers.slice(0,strLen-1);
	}
	stCustomers += ']';
	*/
	
	return '{"total":"27","data":[{"id":"1","price":"71.72","company":"3m Co","date":"2007-09-01","size":"large","visible":"1"},{"id":"2","price":"31.61","company":"AT&T Inc.","date":"2008-02-01","size":"extra large","visible":"0"},{"id":"3","price":"29.01","company":"Aloca Inc","date":"2007-08-01","size":"medium","visible":"0"}]}';
	
	
	
    //return stCustomers;

} 
catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
	if (error.getDetails != undefined){
	    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	    throw error;
	}
	else{
	    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	    throw nlapiCreateError('99999', error.toString());
	}
} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

