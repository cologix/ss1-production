nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_Service_Inventory_Service.js
//	Script Name:	CLGX_RL_Service_Inventory_Service
//	Script Id:		customscript_clgx_rl_serv_inv_service
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/31/2013
//-------------------------------------------------------------------------------------------------

function restlet_serv_inv_service (datain){
	try {
		var txtArg = datain;
		txtArg = txtArg.replace(/\"/g,"");
		txtArg = txtArg.replace(/\:/g,",");
		txtArg = txtArg.replace(/\{/g,"");
		txtArg = txtArg.replace(/\}/g,"");
		var arrArguments = [];
		var arrArguments = txtArg.split( "," );

		if(arrArguments[0] != '' ){
			
			var recJob = nlapiLoadRecord('job', arrArguments[1], null, null);
			var service = recJob.getFieldValue('entityid');
			var customer = recJob.getFieldText('parent');
			
			switch(arrArguments[3]) {
			case 'xconnect' : 
				var objFile = nlapiLoadFile(422283);
				var html = objFile.getValue();
				html = html.replace(new RegExp('{xconnectJSON}','g'),xconnectJSON(arrArguments[1]));
				html = html.replace(new RegExp('{title}','g'), '| ' + customer + ' | ' + service + ' | XC |');
				break;
			case 'vxconnect' : 
				var objFile = nlapiLoadFile(429579);
				var html = objFile.getValue();
				html = html.replace(new RegExp('{vxconnectJSON}','g'),vxconnectJSON(arrArguments[1]));
				html = html.replace(new RegExp('{title}','g'), '| ' + customer + ' | ' + service + ' | VXC |');
				break;
			case 'space': 
				var objFile = nlapiLoadFile(430832);
				var html = objFile.getValue();
				html = html.replace(new RegExp('{spaceJSON}','g'),spaceJSON(arrArguments[1]));
				html = html.replace(new RegExp('{title}','g'), '| ' + customer + ' | ' + service + ' | SPACE |');
				break;
			case 'power': 
				var objFile = nlapiLoadFile(430933);
				var html = objFile.getValue();
				html = html.replace(new RegExp('{powerJSON}','g'),powerJSON(arrArguments[1]));
				html = html.replace(new RegExp('{title}','g'), '| ' + customer + ' | ' + service + ' | POWER |');
				break;
			case 'network': 
				var objFile = nlapiLoadFile(431038);
				var html = objFile.getValue();
				html = html.replace(new RegExp('{networkJSON}','g'),networkJSON(arrArguments[1]));
				html = html.replace(new RegExp('{title}','g'), '| ' + customer + ' | ' + service + ' | NETWORK |');
				break;
			default:
				var html = 'Please select a record from the above panel and click on "View" to see details.';
			}
		}
		else{
			var html = 'Please select a record from the above panel and click on "View" to see details.';
		}
		return html;
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function xconnectJSON (serviceid) {

	var recJob = nlapiLoadRecord('job', serviceid, null, null);
	var nbrXCs = recJob.getLineItemCount('recmachcustrecord_cologix_xc_service');
	
	var stXCs = 'var dataService =  {"text":".","children": [';
              
	for ( var i = 0;nbrXCs != null && nbrXCs != 0 && i < nbrXCs; i++ ) {
		
		var xcid = recJob.getLineItemValue('recmachcustrecord_cologix_xc_service', 'id', i + 1);
		var recXC = nlapiLoadRecord('customrecord_cologix_crossconnect', xcid, null, null);

		stXCs += '{xc:"' + recXC.getFieldValue('name') + 
				'", xcid:' + recXC.getFieldValue('id') +		
				', carrier:"' + recXC.getFieldText('custrecord_cologix_carrier_name') +
				'", circuit:"' + recXC.getFieldText('custrecord_cologix_xc_circuit_type') +
				'",expanded:false, iconCls:"xconnect", leaf:false, children:['; 
		
		var nbrXCPaths = recXC.getLineItemCount('recmachcustrecord_cologix_xcpath_crossconnect');

		for ( var j = 0;nbrXCPaths != null && nbrXCPaths != 0 && j < nbrXCPaths; j++ ) {
		
			stXCs += '{seq:"' + recXC.getLineItemText('recmachcustrecord_cologix_xcpath_crossconnect', 'custrecord_cologix_xcpath_seg_seq', j + 1) + 
						'", space:"' + recXC.getLineItemText('recmachcustrecord_cologix_xcpath_crossconnect', 'custrecord_cologix_xcpath_space', j + 1) +
						'", spaceid:' + recXC.getLineItemValue('recmachcustrecord_cologix_xcpath_crossconnect', 'custrecord_cologix_xcpath_space', j + 1) +
						', port:"' + recXC.getLineItemValue('recmachcustrecord_cologix_xcpath_crossconnect', 'custrecord_cologix_xcpath_port', j + 1) +
						'", panel:"' + recXC.getLineItemText('recmachcustrecord_cologix_xcpath_crossconnect', 'custrecord_cologix_xcpath_panelname', j + 1) +
						'", pathid:' + recXC.getLineItemValue('recmachcustrecord_cologix_xcpath_crossconnect', 'id', j + 1) +	
						', connector:"' + recXC.getLineItemText('recmachcustrecord_cologix_xcpath_crossconnect', 'custrecord_cologix_xcpath_connectortype', j + 1) +
						'", cable:"' + recXC.getLineItemValue('recmachcustrecord_cologix_xcpath_crossconnect', 'custrecord_cologix_xcpath_tiecable', j + 1) +
						'", iconCls:"xcpath' +
						'",leaf:true},'; 
		}
		var strLen = stXCs.length;
		if (parseInt(nbrXCPaths) > 0){
			stXCs = stXCs.slice(0,strLen-1);
		}
		stXCs += ']},'; 
	}
	var strLen = stXCs.length;
	if (parseInt(nbrXCs) > 0){
		stXCs = stXCs.slice(0,strLen-1);
	}
	stXCs += ']};';
    return stXCs;
}

function vxconnectJSON (serviceid) {

	var recJob = nlapiLoadRecord('job', serviceid, null, null);
	var nbrXCs = recJob.getLineItemCount('recmachcustrecord_cologix_xc_service');
	
	var stXCs = 'var dataService =  {"text":".","children": [';
              
	for ( var i = 0;nbrXCs != null && nbrXCs != 0 && i < nbrXCs; i++ ) {
		
		var xcid = recJob.getLineItemValue('recmachcustrecord_cologix_xc_service', 'id', i + 1);
		var recXC = nlapiLoadRecord('customrecord_cologix_crossconnect', xcid, null, null);

		stXCs += '{xc:"' + recXC.getFieldValue('name') + 
				'", xcid:' + recXC.getFieldValue('id') +		
				', carrier:"' + recXC.getFieldText('custrecord_cologix_carrier_name') +
				'", circuit:"' + recXC.getFieldText('custrecord_cologix_xc_circuit_type') +
				'",expanded:false, iconCls:"xconnect", leaf:false, children:['; 
		
		var nbrXCPaths = recXC.getLineItemCount('recmachcustrecord_cologix_xcpath_crossconnect');

		for ( var j = 0;nbrXCPaths != null && nbrXCPaths != 0 && j < nbrXCPaths; j++ ) {
		
			stXCs += '{seq:"' + recXC.getLineItemText('recmachcustrecord_cologix_xcpath_crossconnect', 'custrecord_cologix_xcpath_seg_seq', j + 1) + 
						'", space:"' + recXC.getLineItemText('recmachcustrecord_cologix_xcpath_crossconnect', 'custrecord_cologix_xcpath_space', j + 1) +
						'", port:"' + recXC.getLineItemValue('recmachcustrecord_cologix_xcpath_crossconnect', 'custrecord_cologix_xcpath_port', j + 1) +
						'", pathid:' + recXC.getLineItemValue('recmachcustrecord_cologix_xcpath_crossconnect', 'id', j + 1) +	
						', connector:"' + recXC.getLineItemText('recmachcustrecord_cologix_xcpath_crossconnect', 'custrecord_cologix_xcpath_connectortype', j + 1) +
						'", cable:"' + recXC.getLineItemValue('recmachcustrecord_cologix_xcpath_crossconnect', 'custrecord_cologix_xcpath_tiecable', j + 1) +
						'", iconCls:"xcpath' +
						'",leaf:true},'; 
		}
		var strLen = stXCs.length;
		if (parseInt(nbrXCPaths) > 0){
			stXCs = stXCs.slice(0,strLen-1);
		}
		stXCs += ']},'; 
	}
	var strLen = stXCs.length;
	if (parseInt(nbrXCs) > 0){
		stXCs = stXCs.slice(0,strLen-1);
	}
	stXCs += ']};';
    return stXCs;
}

function powerJSON (serviceid) {

	var recJob = nlapiLoadRecord('job', serviceid, null, null);
	var nbrServices = recJob.getLineItemCount('recmachcustrecord_cologix_power_service');
	
	var stServices = 'var dataPower =  [';
              
	for ( var i = 0; nbrServices != null && nbrServices != 0 && i < nbrServices; i++ ) {
		
		var xcid = recJob.getLineItemValue('recmachcustrecord_cologix_power_service', 'id', i + 1);
		var recService = nlapiLoadRecord('customrecord_clgx_power_circuit', xcid, null, null);
		
		var inactive = recService.getFieldValue('isinactive');
		
		if(inactive == 'F'){
			stServices += '{power:"' + recService.getFieldValue('name') + 
			'", powerid:' + recService.getFieldValue('id') +	
			', space:"' + recService.getFieldText('custrecord_cologix_power_space') +
			'", spaceid:' + recService.getFieldValue('custrecord_cologix_power_space') +
			//', msb:"' + recService.getFieldValue('custrecord_cologix_power_msbbreaker') +
			', ups:"' + recService.getFieldText('custrecord_cologix_power_ups_rect') +
			'", upsid:' + recService.getFieldValue('custrecord_cologix_power_ups_rect') +
			', upsbreak:"' + recService.getFieldValue('custrecord_cologix_power_upsbreaker') +
			'", gen:"' + recService.getFieldText('custrecord_clgx_power_generator') +
			'", genid:' + recService.getFieldValue('custrecord_clgx_power_generator') +
			', panel:"' + recService.getFieldText('custrecord_clgx_power_panel_pdpm') +
			'", panelid:' + recService.getFieldValue('custrecord_clgx_power_panel_pdpm') +
			//'", pdu:"' + recService.getFieldValue('custrecord_cologix_power_pdubdfb') +
			', subpanel:"' + recService.getFieldValue('custrecord_cologix_power_subpanel') +
			'", circuit:"' + recService.getFieldValue('custrecord_cologix_power_circuitbreaker') +
			//'", phasea:"' + recService.getFieldText('custrecord_cologix_power_phase1') +
			//'", phaseb:"' + recService.getFieldText('custrecord_cologix_power_phase2') +
			//'", phasec:"' + recService.getFieldText('custrecord_cologix_power_phase3') +
			'", provisioning:"' + recService.getFieldText('custrecord_cologix_power_status') +  '"},'; 
		}

	}
	var strLen = stServices.length;
	if (parseInt(nbrServices) > 0){
		stServices = stServices.slice(0,strLen-1);
	}
	stServices += '];';
    return stServices;
}

function spaceJSON (serviceid) {

	var recJob = nlapiLoadRecord('job', serviceid, null, null);
	var nbrXCs = recJob.getLineItemCount('recmachcustrecord_cologix_space_project');
	
	var stSpaces = 'var dataService =  {"text":".","children": [';
              
	for ( var i = 0;nbrXCs != null && nbrXCs != 0 && i < nbrXCs; i++ ) {
		
		var xcid = recJob.getLineItemValue('recmachcustrecord_cologix_space_project', 'id', i + 1);
		var recXC = nlapiLoadRecord('customrecord_cologix_space', xcid, null, null);

		stSpaces += '{space:"' + recXC.getFieldValue('name') + 
				'", spaceid:' + recXC.getFieldValue('id') +		
				', sqfeet:"' + recXC.getFieldText('custrecord_cologix_space_square_feet') +
				'", provisionong:"' + recXC.getFieldText('custrecord_cologix_space_status') +
				'",expanded:false, iconCls:"space", leaf:false, children:['; 
		
		// Cross connects on this space
		var nbrXCs = recXC.getLineItemCount('recmachcustrecord_cologix_xcpath_space');
		if(nbrXCs != null && nbrXCs != 0){
			stSpaces += '{space:"Cross Connect' + 
						'",expanded:false, iconCls:"xconnect", leaf:false, children:['; 
			for ( var j = 0; j < nbrXCs; j++ ) {
				stSpaces += '{space:"' + recXC.getLineItemValue('recmachcustrecord_cologix_xcpath_crossconnect', 'name', j + 1) + 
							'", iconCls: "xconnect' +
							'",leaf:true},'; 
			}
			var strLen = stSpaces.length;
			stSpaces = stSpaces.slice(0,strLen-1);
			stSpaces += ']},'; 
		}

		// Power circuits on this space
		var nbrPowers = recXC.getLineItemCount('recmachcustrecord_cologix_power_space');
		if(nbrPowers != null && nbrPowers != 0){
			stSpaces += '{space:"Power' + 
						'",expanded:false, iconCls:"power", leaf:false, children:['; 
			for ( var j = 0; j < nbrPowers; j++ ) {
				stSpaces += '{space:"' + recXC.getLineItemValue('recmachcustrecord_cologix_power_space', 'name', j + 1) + 
							'", iconCls: "power' +
							'",leaf:true},'; 
			}
			var strLen = stSpaces.length;
			stSpaces = stSpaces.slice(0,strLen-1);
			stSpaces += ']},'; 
		}

		// Equipments on this space
		var nbrNetworks = recXC.getLineItemCount('recmachcustrecord_cologix_equipment_space');
		if(nbrNetworks != null && nbrNetworks != 0){
			stSpaces += '{space:"Network' + 
						'",expanded:false, iconCls:"network", leaf:false, children:['; 
			for ( var j = 0; j < nbrNetworks; j++ ) {
				stSpaces += '{space:"' + recXC.getLineItemValue('recmachcustrecord_cologix_equipment_space', 'id', j + 1) + 
							'", iconCls: "network' +
							'",leaf:true},'; 
			}
			var strLen = stSpaces.length;
			stSpaces = stSpaces.slice(0,strLen-1);
			stSpaces += ']},'; 
		}

	}
	var strLen = stSpaces.length;
	if (parseInt(nbrXCs) > 0 ||parseInt(nbrPowers) > 0 ||parseInt(nbrNetworks) > 0){
		//stSpaces = stSpaces.slice(0,strLen-1);
	}
	stSpaces += ']};';
    return stSpaces;
}