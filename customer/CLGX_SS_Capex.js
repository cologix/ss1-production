nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Capex_Mini.js
//	Script Name:	CLGX_SS_Capex_Mini
//	Script Id:		customscript_clgx_ss_rep_cpx_mini
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		02/15/2013
//-------------------------------------------------------------------------------------------------

function suiteletProjectCapex(request, response){
	try {
		nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------STARTED---------------------|');

		var projid = request.getParameter('projid');
		var csv = request.getParameter('csv');
		
		if(csv == 1){
			
			var attachments = new Array();
			
			var csvProject = csvDataProject (projid);
			if(csvProject != ''){
				var fileProject = nlapiCreateFile('project.csv', 'CSV', csvProject);
				attachments.push(fileProject);
			}
			var csvPOs = csvDataPOs(projid);
			if(csvPOs != ''){
				var filePOs = nlapiCreateFile('pos.csv', 'CSV', csvPOs);
				attachments.push(filePOs);
			}
			var csvBills = csvDataBills(projid);
			if(csvBills != ''){
				var fileBills = nlapiCreateFile('bills.csv', 'CSV', csvBills);
				attachments.push(fileBills);
			}
			var csvCredits = csvDataCredits(projid);
			if(csvCredits != ''){
				var fileCredits = nlapiCreateFile('credits.csv', 'CSV', csvCredits);
				attachments.push(fileCredits);
			}
			var csvJournals = csvDataJournals(projid);
			if(csvJournals != ''){
				var fileJournals = nlapiCreateFile('journals.csv', 'CSV', csvJournals);
				attachments.push(fileJournals);
			}
			var csvCapLabor = csvDataCapLabor(projid);
			if(csvCapLabor != ''){
				var fileCapLabor = nlapiCreateFile('caplabor.csv', 'CSV', csvCapLabor);
				attachments.push(fileCapLabor);
			}

			var userId = nlapiGetUser();
			
			var project = nlapiLookupField('job', projid, 'entityid');
			
			emailSubject = 'Export CSVs for project - ' + project;
			emailBody = '';
			nlapiSendEmail(userId,userId,emailSubject,emailBody,null,null,null,attachments,true);
		}
		
		var objFile = nlapiLoadFile(302399);
		var capexHTML = objFile.getValue();
		
		capexHTML = capexHTML.replace(new RegExp('{dataPOs}','g'),jsonDataPOs(projid));
		capexHTML = capexHTML.replace(new RegExp('{dataBills}','g'),jsonDataBills(projid));
		capexHTML = capexHTML.replace(new RegExp('{dataBillsCredits}','g'),jsonDataBillsCredits(projid));
		capexHTML = capexHTML.replace(new RegExp('{dataJournals}','g'),jsonDataJournals(projid));
		capexHTML = capexHTML.replace(new RegExp('{dataJournalsCapLabor}','g'),jsonDataJournalsCapLabor(projid));
		capexHTML = capexHTML.replace(new RegExp('{dataPeriods}','g'),jsonDataProject(projid));
		capexHTML = capexHTML.replace(new RegExp('{projid}','g'),projid);
		
		response.write( capexHTML );
	
//---------- End Section 1 ------------------------------------------------------------------------------------------------


nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------FINISHED---------------------|');  
} 
catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
	if (error.getDetails != undefined){
	    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	    throw error;
	}
	else{
	    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	    throw nlapiCreateError('99999', error.toString());
	}
} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function jsonDataPOs(projid){	
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('companyname','vendor','GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid','vendor','GROUP'));
	//arrColumns.push(new nlobjSearchColumn('amount',null,'SUM'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('custbody_cologix_project_name',null,'is',projid));
	var searchPOs = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_pos', arrFilters, arrColumns);              
	               
	var jsonPOs = 'var dataPOs = [';
	for ( var i = 0; searchPOs != null && i < searchPOs.length; i++ ) {
		var searchPO = searchPOs[i];
		var columns = searchPO.getAllColumns();
		
		jsonPOs += '\n{total:"Total"' + 
					',periodid:' + searchPO.getValue('postingperiod',null,'GROUP') +				
					',period:"' + searchPO.getText('postingperiod',null,'GROUP') +		
					'",poNbr:' + searchPO.getValue('tranid',null,'GROUP') +
					',poId:' + searchPO.getValue('internalid',null,'GROUP') + 
					',vendor:"' + cleanStr (searchPO.getValue('companyname','vendor','GROUP')) + 
					'",vendorid:' + searchPO.getValue('internalid','vendor','GROUP') + 
					',billed:' + searchPO.getValue(columns[6]) + 
					',unbilled:' + searchPO.getValue(columns[7]) + 
					',totalPOs:' + searchPO.getValue(columns[8]) +  '},';
		}
		
	var strLen = jsonPOs.length;
	if (searchPOs != null){
		jsonPOs = jsonPOs.slice(0,strLen-1);
	}
	jsonPOs += '\n];';
    return jsonPOs;
}

function jsonDataBills(projid){
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('companyname','vendor','GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid','vendor','GROUP'));
	arrColumns.push(new nlobjSearchColumn('tranid','appliedToTransaction','GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid','appliedToTransaction','GROUP'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('recordType',null,'is','vendorbill'));
	arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	//arrFilters.push(new nlobjSearchFilter('taxline','appliedToTransaction','is','F'));
	//var searchBills = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals2', arrFilters, arrColumns);
	var searchBills = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);
              
	var jsonBills = 'var dataBills = [';
	for ( var i = 0; searchBills != null && i < searchBills.length; i++ ) {
		var searchBill = searchBills[i];

		//var poid = 1;
		var poid = searchBill.getValue('internalid','appliedToTransaction','GROUP');
		if (poid == null || poid ==''){poid = 1;}
		
		//var ponbr = '';
		var ponbr = searchBill.getValue('tranid','appliedToTransaction','GROUP');
		if (ponbr == null || ponbr =='' || ponbr == '- None -'){ponbr = '';}

		jsonBills += '\n{total:"Total"' + 
					',periodid:' + searchBill.getValue('postingperiod',null,'GROUP') + 	
					',period:"' + searchBill.getText('postingperiod',null,'GROUP') +		
					'",billNbr:"' + searchBill.getValue('tranid',null,'GROUP') +
					'",billId:' + searchBill.getValue('internalid',null,'GROUP') + 
					',vendor:"' + cleanStr (searchBill.getValue('companyname','vendor','GROUP')) + 
					'",vendorid:' + searchBill.getValue('internalid','vendor','GROUP') + 
					',poNbr:"' + ponbr +
					'",poId:' + poid +
					',amount:' + searchBill.getValue('amount',null,'SUM') +  '},';
					//',amount:' + searchBill.getValue('totalamount',null,'SUM') +  '},';
			}
	var strLen = jsonBills.length;
	if (searchBills != null){
		jsonBills = jsonBills.slice(0,strLen-1);
	}
	jsonBills += '\n];';
    return jsonBills;
}

function jsonDataBillsCredits(projid){
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('companyname','vendor','GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid','vendor','GROUP'));
	arrColumns.push(new nlobjSearchColumn('tranid','appliedToTransaction','GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid','appliedToTransaction','GROUP'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('recordType',null,'is','vendorcredit'));
	arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchBills = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);
              
	var jsonBills = 'var dataBillsCredits = [';
	for ( var i = 0; searchBills != null && i < searchBills.length; i++ ) {
		var searchBill = searchBills[i];

		var poid = searchBill.getValue('internalid','appliedToTransaction','GROUP');
		if (poid == null || poid ==''){poid = 1;}
		var ponbr = searchBill.getValue('tranid','appliedToTransaction','GROUP');
		if (ponbr == null || ponbr =='' || ponbr == '- None -'){ponbr = '';}

		jsonBills += '\n{total:"Total"' + 
					',periodid:' + searchBill.getValue('postingperiod',null,'GROUP') + 	
					',period:"' + searchBill.getText('postingperiod',null,'GROUP') +		
					'",billCreditNbr:"' + searchBill.getValue('tranid',null,'GROUP') +
					'",billCreditId:' + searchBill.getValue('internalid',null,'GROUP') + 
					',vendor:"' + cleanStr (searchBill.getValue('companyname','vendor','GROUP')) + 
					'",vendorid:' + searchBill.getValue('internalid','vendor','GROUP') + 
					',poNbr:"' + ponbr +
					'",poId:' + poid +
					',amount:' + searchBill.getValue('amount',null,'SUM') +  '},';
			}
	var strLen = jsonBills.length;
	if (searchBills != null){
		jsonBills = jsonBills.slice(0,strLen-1);
	}
	jsonBills += '\n];';
    return jsonBills;
}

function jsonDataJournals(projid){
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('memo',null,'GROUP'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('recordType',null,'is','journalentry'));
	arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchJournals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);
              
	var jsonJournals = 'var dataJournals = [';
	for ( var i = 0; searchJournals != null && i < searchJournals.length; i++ ) {
		var searchJournal = searchJournals[i];

		jsonJournals += '\n{total:"Total"' + 
					',periodid:' + searchJournal.getValue('postingperiod',null,'GROUP') + 
					',period:"' + searchJournal.getText('postingperiod',null,'GROUP') +		
					'",journalNbr:"' + searchJournal.getValue('tranid',null,'GROUP') +
					'",journalId:' + searchJournal.getValue('internalid',null,'GROUP') + 
					',memo:"' + cleanStr (searchJournal.getValue('memo',null,'GROUP')) + 
					//',memo:"' + 
					'",amount:' + searchJournal.getValue('amount',null,'SUM') +  '},';
			}
	var strLen = jsonJournals.length;
	if (searchJournals != null){
		jsonJournals = jsonJournals.slice(0,strLen-1);
	}
	jsonJournals += '\n];';
    return jsonJournals;
}

function jsonDataJournalsCapLabor(projid){
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('memo',null,'GROUP'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('recordType',null,'is','journalentry'));
	arrFilters.push(new nlobjSearchFilter('class',null,'anyof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchJournals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);
              
	var jsonJournals = 'var dataJournalsCapLabor = [';
	for ( var i = 0; searchJournals != null && i < searchJournals.length; i++ ) {
		var searchJournal = searchJournals[i];

		jsonJournals += '\n{total:"Total"' + 
					',periodid:' + searchJournal.getValue('postingperiod',null,'GROUP') + 
					',period:"' + searchJournal.getText('postingperiod',null,'GROUP') +		
					'",journalCLNbr:"' + searchJournal.getValue('tranid',null,'GROUP') +
					'",journalCLId:' + searchJournal.getValue('internalid',null,'GROUP') + 
					',memo:"' + cleanStr (searchJournal.getValue('memo',null,'GROUP')) + 
					//',memo:"' + 
					'",amount:' + searchJournal.getValue('amount',null,'SUM') +  '},';
			}
	var strLen = jsonJournals.length;
	if (searchJournals != null){
		jsonJournals = jsonJournals.slice(0,strLen-1);
	}
	jsonJournals += '\n];';
    return jsonJournals;
}

function jsonDataProject (projid){	

	var projectName = nlapiLookupField('job', projid, 'entityid');
	
	var stPeriods = 'var dataPeriods = [';
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('recordType',null,'is','vendorbill'));
	arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchBills = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

	if (searchBills != null) {
		var searchBill = searchBills[0];
		var columns = searchBill.getAllColumns();
		
		stPeriods += '\n{trcls:"row' + 
					'",type:"Bills' + 
					'",TOT2011:' + searchBill.getValue(columns[2]) + 
					',JAN2012:' + searchBill.getValue(columns[3]) + 
					',FEB2012:' + searchBill.getValue(columns[4]) + 
					',MAR2012:' + searchBill.getValue(columns[5]) + 
					',APR2012:' + searchBill.getValue(columns[6]) + 
					',MAY2012:' + searchBill.getValue(columns[7]) + 
					',JUN2012:' + searchBill.getValue(columns[8]) + 
					',JUL2012:' + searchBill.getValue(columns[9]) + 
					',AUG2012:' + searchBill.getValue(columns[10]) + 
					',SEP2012:' + searchBill.getValue(columns[11]) + 
					',OCT2012:' + searchBill.getValue(columns[12]) + 
					',NOV2012:' + searchBill.getValue(columns[13]) + 
					',DEC2012:' + searchBill.getValue(columns[14]) + 
					',TOT2012:' + searchBill.getValue(columns[15]) + 
					',JAN2013:' + searchBill.getValue(columns[16]) + 
					',FEB2013:' + searchBill.getValue(columns[17]) + 
					',MAR2013:' + searchBill.getValue(columns[18]) + 
					',APR2013:' + searchBill.getValue(columns[19]) + 
					',MAY2013:' + searchBill.getValue(columns[20]) + 
					',JUN2013:' + searchBill.getValue(columns[21]) + 
					',JUL2013:' + searchBill.getValue(columns[22]) + 
					',AUG2013:' + searchBill.getValue(columns[23]) + 
					',SEP2013:' + searchBill.getValue(columns[24]) + 
					',OCT2013:' + searchBill.getValue(columns[25]) + 
					',NOV2013:' + searchBill.getValue(columns[26]) + 
					',DEC2013:' + searchBill.getValue(columns[27]) + 
					',TOT2013:' + searchBill.getValue(columns[28]) + 
					',TOTAL:' + searchBill.getValue(columns[1]) +  '},';
	}
	else{
		stPeriods += '\n{trcls:"row",type:"Bills", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, TOTAL:0},';
	}

	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('recordType',null,'is','vendorcredit'));
	arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchBillsCredits = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

	if (searchBillsCredits != null) {
		var searchBillsCredit = searchBillsCredits[0];
		var columns = searchBillsCredit.getAllColumns();
		
		stPeriods += '\n{trcls:"row' + 
					'",type:"Bills Credit' + 
					'",TOT2011:' + searchBillsCredit.getValue(columns[2]) + 
					',JAN2012:' + searchBillsCredit.getValue(columns[3]) + 
					',FEB2012:' + searchBillsCredit.getValue(columns[4]) + 
					',MAR2012:' + searchBillsCredit.getValue(columns[5]) + 
					',APR2012:' + searchBillsCredit.getValue(columns[6]) + 
					',MAY2012:' + searchBillsCredit.getValue(columns[7]) + 
					',JUN2012:' + searchBillsCredit.getValue(columns[8]) + 
					',JUL2012:' + searchBillsCredit.getValue(columns[9]) + 
					',AUG2012:' + searchBillsCredit.getValue(columns[10]) + 
					',SEP2012:' + searchBillsCredit.getValue(columns[11]) + 
					',OCT2012:' + searchBillsCredit.getValue(columns[12]) + 
					',NOV2012:' + searchBillsCredit.getValue(columns[13]) + 
					',DEC2012:' + searchBillsCredit.getValue(columns[14]) + 
					',TOT2012:' + searchBillsCredit.getValue(columns[15]) + 
					',JAN2013:' + searchBillsCredit.getValue(columns[16]) + 
					',FEB2013:' + searchBillsCredit.getValue(columns[17]) + 
					',MAR2013:' + searchBillsCredit.getValue(columns[18]) + 
					',APR2013:' + searchBillsCredit.getValue(columns[19]) + 
					',MAY2013:' + searchBillsCredit.getValue(columns[20]) + 
					',JUN2013:' + searchBillsCredit.getValue(columns[21]) + 
					',JUL2013:' + searchBillsCredit.getValue(columns[22]) + 
					',AUG2013:' + searchBillsCredit.getValue(columns[23]) + 
					',SEP2013:' + searchBillsCredit.getValue(columns[24]) + 
					',OCT2013:' + searchBillsCredit.getValue(columns[25]) + 
					',NOV2013:' + searchBillsCredit.getValue(columns[26]) + 
					',DEC2013:' + searchBillsCredit.getValue(columns[27]) + 
					',TOT2013:' + searchBillsCredit.getValue(columns[28]) + 
					',TOTAL:' + searchBillsCredit.getValue(columns[1]) +  '},';
	}
	else{
		stPeriods += '\n{trcls:"row",type:"Bills Credit", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, TOTAL:0},';
	}
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('recordType',null,'is','journalentry'));
	arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchJournals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

	if (searchJournals != null) {
		var searchJournal = searchJournals[0];
		var columns = searchJournal.getAllColumns();
		
		stPeriods += '\n{trcls:"row' + 
					'",type:"Journal' + 
					'",TOT2011:' + searchJournal.getValue(columns[2]) + 
					',JAN2012:' + searchJournal.getValue(columns[3]) + 
					',FEB2012:' + searchJournal.getValue(columns[4]) + 
					',MAR2012:' + searchJournal.getValue(columns[5]) + 
					',APR2012:' + searchJournal.getValue(columns[6]) + 
					',MAY2012:' + searchJournal.getValue(columns[7]) + 
					',JUN2012:' + searchJournal.getValue(columns[8]) + 
					',JUL2012:' + searchJournal.getValue(columns[9]) + 
					',AUG2012:' + searchJournal.getValue(columns[10]) + 
					',SEP2012:' + searchJournal.getValue(columns[11]) + 
					',OCT2012:' + searchJournal.getValue(columns[12]) + 
					',NOV2012:' + searchJournal.getValue(columns[13]) + 
					',DEC2012:' + searchJournal.getValue(columns[14]) + 
					',TOT2012:' + searchJournal.getValue(columns[15]) + 
					',JAN2013:' + searchJournal.getValue(columns[16]) + 
					',FEB2013:' + searchJournal.getValue(columns[17]) + 
					',MAR2013:' + searchJournal.getValue(columns[18]) + 
					',APR2013:' + searchJournal.getValue(columns[19]) + 
					',MAY2013:' + searchJournal.getValue(columns[20]) + 
					',JUN2013:' + searchJournal.getValue(columns[21]) + 
					',JUL2013:' + searchJournal.getValue(columns[22]) + 
					',AUG2013:' + searchJournal.getValue(columns[23]) + 
					',SEP2013:' + searchJournal.getValue(columns[24]) + 
					',OCT2013:' + searchJournal.getValue(columns[25]) + 
					',NOV2013:' + searchJournal.getValue(columns[26]) + 
					',DEC2013:' + searchJournal.getValue(columns[27]) + 
					',TOT2013:' + searchJournal.getValue(columns[28]) + 
					',TOTAL:' + searchJournal.getValue(columns[1]) +  '},';
	}
	else{
		stPeriods += '\n{trcls:"row",type:"Journal", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, TOTAL:0},';
	}
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('recordType',null,'is','journalentry'));
	arrFilters.push(new nlobjSearchFilter('class',null,'anyof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchCapLabors = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);
	
	if (searchCapLabors != null) {
		var searchCapLabor = searchCapLabors[0];
		var columns = searchCapLabor.getAllColumns();
		
		stPeriods += '\n{trcls:"row' + 
					'",type:"Cap Labor' + 
					'",TOT2011:' + searchCapLabor.getValue(columns[2]) + 
					',JAN2012:' + searchCapLabor.getValue(columns[3]) + 
					',FEB2012:' + searchCapLabor.getValue(columns[4]) + 
					',MAR2012:' + searchCapLabor.getValue(columns[5]) + 
					',APR2012:' + searchCapLabor.getValue(columns[6]) + 
					',MAY2012:' + searchCapLabor.getValue(columns[7]) + 
					',JUN2012:' + searchCapLabor.getValue(columns[8]) + 
					',JUL2012:' + searchCapLabor.getValue(columns[9]) + 
					',AUG2012:' + searchCapLabor.getValue(columns[10]) + 
					',SEP2012:' + searchCapLabor.getValue(columns[11]) + 
					',OCT2012:' + searchCapLabor.getValue(columns[12]) + 
					',NOV2012:' + searchCapLabor.getValue(columns[13]) + 
					',DEC2012:' + searchCapLabor.getValue(columns[14]) + 
					',TOT2012:' + searchCapLabor.getValue(columns[15]) + 
					',JAN2013:' + searchCapLabor.getValue(columns[16]) + 
					',FEB2013:' + searchCapLabor.getValue(columns[17]) + 
					',MAR2013:' + searchCapLabor.getValue(columns[18]) + 
					',APR2013:' + searchCapLabor.getValue(columns[19]) + 
					',MAY2013:' + searchCapLabor.getValue(columns[20]) + 
					',JUN2013:' + searchCapLabor.getValue(columns[21]) + 
					',JUL2013:' + searchCapLabor.getValue(columns[22]) + 
					',AUG2013:' + searchCapLabor.getValue(columns[23]) + 
					',SEP2013:' + searchCapLabor.getValue(columns[24]) + 
					',OCT2013:' + searchCapLabor.getValue(columns[25]) + 
					',NOV2013:' + searchCapLabor.getValue(columns[26]) + 
					',DEC2013:' + searchCapLabor.getValue(columns[27]) + 
					',TOT2013:' + searchCapLabor.getValue(columns[28]) + 
					',TOTAL:' + searchCapLabor.getValue(columns[1]) +  '},';
	}
	else{
		stPeriods += '\n{trcls:"row",type:"Cap Labor", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, TOTAL:0},';
	}
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	//arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchActuals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

	if (searchActuals != null) {
		var searchActual = searchActuals[0];
		var columns = searchActual.getAllColumns();
		stPeriods += '\n{trcls:"tot' + 
					'",type:"Total Actuals' + 
					'",TOT2011:' + searchActual.getValue(columns[2]) + 
					',JAN2012:' + searchActual.getValue(columns[3]) + 
					',FEB2012:' + searchActual.getValue(columns[4]) + 
					',MAR2012:' + searchActual.getValue(columns[5]) + 
					',APR2012:' + searchActual.getValue(columns[6]) + 
					',MAY2012:' + searchActual.getValue(columns[7]) + 
					',JUN2012:' + searchActual.getValue(columns[8]) + 
					',JUL2012:' + searchActual.getValue(columns[9]) + 
					',AUG2012:' + searchActual.getValue(columns[10]) + 
					',SEP2012:' + searchActual.getValue(columns[11]) + 
					',OCT2012:' + searchActual.getValue(columns[12]) + 
					',NOV2012:' + searchActual.getValue(columns[13]) + 
					',DEC2012:' + searchActual.getValue(columns[14]) + 
					',TOT2012:' + searchActual.getValue(columns[15]) + 
					',JAN2013:' + searchActual.getValue(columns[16]) + 
					',FEB2013:' + searchActual.getValue(columns[17]) + 
					',MAR2013:' + searchActual.getValue(columns[18]) + 
					',APR2013:' + searchActual.getValue(columns[19]) + 
					',MAY2013:' + searchActual.getValue(columns[20]) + 
					',JUN2013:' + searchActual.getValue(columns[21]) + 
					',JUL2013:' + searchActual.getValue(columns[22]) + 
					',AUG2013:' + searchActual.getValue(columns[23]) + 
					',SEP2013:' + searchActual.getValue(columns[24]) + 
					',OCT2013:' + searchActual.getValue(columns[25]) + 
					',NOV2013:' + searchActual.getValue(columns[26]) + 
					',DEC2013:' + searchActual.getValue(columns[27]) + 
					',TOT2013:' + searchActual.getValue(columns[28]) + 
					',TOTAL:' + searchActual.getValue(columns[1]) +  '},';
	}
	else{
		stPeriods += '\n{trcls:"tot",type:"Total Actuals", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, TOTAL:0},';
	}
	
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('custbody_cologix_project_name',null,'is',projid));
	var searchPOs = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_pos', arrFilters, arrColumns); 
	
	if (searchPOs != null) {
		var searchPO = searchPOs[0];
		var columns = searchPO.getAllColumns();
		
		stPeriods += '\n{trcls:"row' + 
					'",type:"Unbilled POs' + 
					'",TOT2011:' + searchPO.getValue(columns[4]) + 
					',JAN2012:' + searchPO.getValue(columns[5]) + 
					',FEB2012:' + searchPO.getValue(columns[6]) + 
					',MAR2012:' + searchPO.getValue(columns[7]) + 
					',APR2012:' + searchPO.getValue(columns[8]) + 
					',MAY2012:' + searchPO.getValue(columns[9]) + 
					',JUN2012:' + searchPO.getValue(columns[10]) + 
					',JUL2012:' + searchPO.getValue(columns[11]) + 
					',AUG2012:' + searchPO.getValue(columns[12]) + 
					',SEP2012:' + searchPO.getValue(columns[13]) + 
					',OCT2012:' + searchPO.getValue(columns[14]) + 
					',NOV2012:' + searchPO.getValue(columns[15]) + 
					',DEC2012:' + searchPO.getValue(columns[16]) + 
					',TOT2012:' + searchPO.getValue(columns[17]) + 
					',JAN2013:' + searchPO.getValue(columns[18]) + 
					',FEB2013:' + searchPO.getValue(columns[19]) + 
					',MAR2013:' + searchPO.getValue(columns[20]) + 
					',APR2013:' + searchPO.getValue(columns[21]) + 
					',MAY2013:' + searchPO.getValue(columns[22]) + 
					',JUN2013:' + searchPO.getValue(columns[23]) + 
					',JUL2013:' + searchPO.getValue(columns[24]) + 
					',AUG2013:' + searchPO.getValue(columns[25]) + 
					',SEP2013:' + searchPO.getValue(columns[26]) + 
					',OCT2013:' + searchPO.getValue(columns[27]) + 
					',NOV2013:' + searchPO.getValue(columns[28]) + 
					',DEC2013:' + searchPO.getValue(columns[29]) + 
					',TOT2013:' + searchPO.getValue(columns[30]) + 
					',TOTAL:' + searchPO.getValue(columns[2]) +  '},';
		
		stPeriods += '\n{trcls:"row' + 
					'",type:"Billed POs' + 
					'",TOT2011:0' + 
					',JAN2012:0' + 
					',FEB2012:0' + 
					',MAR2012:0' + 
					',APR2012:0' + 
					',MAY2012:0' + 
					',JUN2012:0' + 
					',JUL2012:0' + 
					',AUG2012:0' + 
					',SEP2012:0' + 
					',OCT2012:0' + 
					',NOV2012:0' + 
					',DEC2012:0' + 
					',TOT2012:0' + 
					',JAN2013:0' + 
					',FEB2013:0' + 
					',MAR2013:0' + 
					',APR2013:0' + 
					',MAY2013:0' + 
					',JUN2013:0' + 
					',JUL2013:0' + 
					',AUG2013:0' + 
					',SEP2013:0' + 
					',OCT2013:0' + 
					',NOV2013:0' + 
					',DEC2013:0' + 
					',TOT2013:0' + 
					',TOTAL:' + searchPO.getValue(columns[1]) +  '},';
		
		stPeriods += '\n{trcls:"tot' + 
					'",type:"Total POs' + 
					'",TOT2011:0' + 
					',JAN2012:0' + 
					',FEB2012:0' + 
					',MAR2012:0' + 
					',APR2012:0' + 
					',MAY2012:0' + 
					',JUN2012:0' + 
					',JUL2012:0' + 
					',AUG2012:0' + 
					',SEP2012:0' + 
					',OCT2012:0' + 
					',NOV2012:0' + 
					',DEC2012:0' + 
					',TOT2012:0' + 
					',JAN2013:0' + 
					',FEB2013:0' + 
					',MAR2013:0' + 
					',APR2013:0' + 
					',MAY2013:0' + 
					',JUN2013:0' + 
					',JUL2013:0' + 
					',AUG2013:0' + 
					',SEP2013:0' + 
					',OCT2013:0' + 
					',NOV2013:0' + 
					',DEC2013:0' + 
					',TOT2013:0' + 
					',TOTAL:' + searchPO.getValue(columns[3]) +  '},';
	}
	else{
		stPeriods += '\n{trcls:"row",type:"Unbilled POs", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, TOTAL:0},';
		stPeriods += '\n{trcls:"row",type:"Billed POs", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, TOTAL:0},';
		stPeriods += '\n{trcls:"tot",type:"Total POs", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, TOTAL:0},';
	}
	
	if (searchActuals != null && searchPOs != null) {
		
		stPeriods += '\n{trcls:"cal' + 
					'",type:"Committed Capital' + 
					'",TOT2011:0' + 
					',JAN2012:0' + 
					',FEB2012:0' + 
					',MAR2012:0' + 
					',APR2012:0' + 
					',MAY2012:0' + 
					',JUN2012:0' + 
					',JUL2012:0' + 
					',AUG2012:0' + 
					',SEP2012:0' + 
					',OCT2012:0' + 
					',NOV2012:0' + 
					',DEC2012:0' + 
					',TOT2012:0' + 
					',JAN2013:0' + 
					',FEB2013:0' + 
					',MAR2013:0' + 
					',APR2013:0' + 
					',MAY2013:0' + 
					',JUN2013:0' + 
					',JUL2013:0' + 
					',AUG2013:0' + 
					',SEP2013:0' + 
					',OCT2013:0' + 
					',NOV2013:0' + 
					',DEC2013:0' + 
					',TOT2013:0' + 
					',TOTAL:' + (parseFloat(searchActual.getValue(columns[1])) +  parseFloat(searchPO.getValue(columns[2]))) + '},';
	}
	else{
		stPeriods += '\n{trcls:"cal",type:"Committed Capital", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, TOTAL:0},';
	}
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_budget_project',null,'is',projectName));
	var searchBudgets = nlapiSearchRecord('customrecord_clgx_budget', 'customsearch_clgx_rep_cpx_budget', arrFilters, arrColumns); 
	
	if (searchBudgets != null) {
		var searchBudget = searchBudgets[0];
		var columns = searchBudget.getAllColumns();
		stPeriods += '\n{trcls:"row' + 
					'",type:"Budget' + 
					'",TOT2011:' + searchBudget.getValue(columns[2]) +
					',JAN2012:0' + 
					',FEB2012:0' + 
					',MAR2012:0' + 
					',APR2012:0' + 
					',MAY2012:0' + 
					',JUN2012:0' + 
					',JUL2012:0' + 
					',AUG2012:0' + 
					',SEP2012:0' + 
					',OCT2012:0' + 
					',NOV2012:0' + 
					',DEC2012:0' + 
					',TOT2012:' + searchBudget.getValue(columns[3]) +
					',JAN2013:0' + 
					',FEB2013:0' + 
					',MAR2013:0' + 
					',APR2013:0' + 
					',MAY2013:0' + 
					',JUN2013:0' + 
					',JUL2013:0' + 
					',AUG2013:0' + 
					',SEP2013:0' + 
					',OCT2013:0' + 
					',NOV2013:0' + 
					',DEC2013:0' + 
					',TOT2013:' + searchBudget.getValue(columns[4]) +
					',TOTAL:' + searchBudget.getValue(columns[1]) + '},';
	}
	else{
		stPeriods += '\n{trcls:"row",type:"Budget", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, TOTAL:0},';
	}
	
	
	if (searchBudgets != null && searchActuals != null && searchPOs != null) {

		stPeriods += '\n{trcls:"cal' + 
					'",type:"Committed Capital Variance' + 
					'",TOT2011:0' + 
					',JAN2012:0' + 
					',FEB2012:0' + 
					',MAR2012:0' + 
					',APR2012:0' + 
					',MAY2012:0' + 
					',JUN2012:0' + 
					',JUL2012:0' + 
					',AUG2012:0' + 
					',SEP2012:0' + 
					',OCT2012:0' + 
					',NOV2012:0' + 
					',DEC2012:0' + 
					',TOT2012:0' + 
					',JAN2013:0' + 
					',FEB2013:0' + 
					',MAR2013:0' + 
					',APR2013:0' + 
					',MAY2013:0' + 
					',JUN2013:0' + 
					',JUL2013:0' + 
					',AUG2013:0' + 
					',SEP2013:0' + 
					',OCT2013:0' + 
					',NOV2013:0' + 
					',DEC2013:0' + 
					',TOT2013:0' + 
					',TOTAL:' + (parseFloat(searchBudget.getValue(columns[1])) - (parseFloat(searchActual.getValue(columns[1])) +  parseFloat(searchPO.getValue(columns[2])))) + '},';
	}
	else{
		stPeriods += '\n{trcls:"cal",type:"Committed Capital Variance", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, TOTAL:0},';
	}
	
	if (searchActuals != null) {
		var searchActual = searchActuals[0];
		var columns = searchActual.getAllColumns();
		stPeriods += '\n{trcls:"row' + 
					'",type:"Forecasts' + 
					'",TOT2011:' + searchActual.getValue(columns[2]) + 
					',JAN2012:' + searchActual.getValue(columns[3]) + 
					',FEB2012:' + searchActual.getValue(columns[4]) + 
					',MAR2012:' + searchActual.getValue(columns[5]) + 
					',APR2012:' + searchActual.getValue(columns[6]) + 
					',MAY2012:' + searchActual.getValue(columns[7]) + 
					',JUN2012:' + searchActual.getValue(columns[8]) + 
					',JUL2012:' + searchActual.getValue(columns[9]) + 
					',AUG2012:' + searchActual.getValue(columns[10]) + 
					',SEP2012:' + searchActual.getValue(columns[11]) + 
					',OCT2012:' + searchActual.getValue(columns[12]) + 
					',NOV2012:' + searchActual.getValue(columns[13]) + 
					',DEC2012:' + searchActual.getValue(columns[14]) + 
					',TOT2012:' + searchActual.getValue(columns[15]) + 
					',JAN2013:' + searchActual.getValue(columns[16]) + 
					',FEB2013:' + searchActual.getValue(columns[17]) + 
					',MAR2013:' + searchActual.getValue(columns[18]) + 
					',APR2013:' + searchActual.getValue(columns[19]) + 
					',MAY2013:' + searchActual.getValue(columns[20]) + 
					',JUN2013:' + searchActual.getValue(columns[21]) + 
					',JUL2013:' + searchActual.getValue(columns[22]) + 
					',AUG2013:' + searchActual.getValue(columns[23]) + 
					',SEP2013:' + searchActual.getValue(columns[24]) + 
					',OCT2013:' + searchActual.getValue(columns[25]) + 
					',NOV2013:' + searchActual.getValue(columns[26]) + 
					',DEC2013:' + searchActual.getValue(columns[27]) + 
					',TOT2013:' + searchActual.getValue(columns[28]) + 
					',TOTAL:' + searchActual.getValue(columns[1]) +  '},';
	}
	else{
		stPeriods += '\n{trcls:"row",type:"Forecasts", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, TOTAL:0},';
	}
	
	if (searchActuals != null && searchPOs != null) {

	stPeriods += '\n{trcls:"cal' + 
				'",type:"Total Committed and Projected' + 
				'",TOT2011:0' + 
				',JAN2012:0' + 
				',FEB2012:0' + 
				',MAR2012:0' + 
				',APR2012:0' + 
				',MAY2012:0' + 
				',JUN2012:0' + 
				',JUL2012:0' + 
				',AUG2012:0' + 
				',SEP2012:0' + 
				',OCT2012:0' + 
				',NOV2012:0' + 
				',DEC2012:0' + 
				',TOT2012:0' + 
				',JAN2013:0' + 
				',FEB2013:0' + 
				',MAR2013:0' + 
				',APR2013:0' + 
				',MAY2013:0' + 
				',JUN2013:0' + 
				',JUL2013:0' + 
				',AUG2013:0' + 
				',SEP2013:0' + 
				',OCT2013:0' + 
				',NOV2013:0' + 
				',DEC2013:0' + 
				',TOT2013:0' + 
				',TOTAL:' + (parseFloat(searchActual.getValue(columns[1])) + (parseFloat(searchActual.getValue(columns[1])) +  parseFloat(searchPO.getValue(columns[2])))) + '},';
	}
	
	if (searchBudgets != null && searchActuals != null && searchPOs != null) {
	
	stPeriods += '\n{trcls:"cal' + 
				'",type:"Total Project Variance' + 
				'",TOT2011:0' + 
				',JAN2012:0' + 
				',FEB2012:0' + 
				',MAR2012:0' + 
				',APR2012:0' + 
				',MAY2012:0' + 
				',JUN2012:0' + 
				',JUL2012:0' + 
				',AUG2012:0' + 
				',SEP2012:0' + 
				',OCT2012:0' + 
				',NOV2012:0' + 
				',DEC2012:0' + 
				',TOT2012:0' + 
				',JAN2013:0' + 
				',FEB2013:0' + 
				',MAR2013:0' + 
				',APR2013:0' + 
				',MAY2013:0' + 
				',JUN2013:0' + 
				',JUL2013:0' + 
				',AUG2013:0' + 
				',SEP2013:0' + 
				',OCT2013:0' + 
				',NOV2013:0' + 
				',DEC2013:0' + 
				',TOT2013:0' + 
				',TOTAL:' + (parseFloat(searchBudget.getValue(columns[1])) - (parseFloat(searchActual.getValue(columns[1])) + (parseFloat(searchActual.getValue(columns[1])) +  parseFloat(searchPO.getValue(columns[2]))))) + '},';
	}
	
	if (searchBudgets != null && searchActuals != null && searchPOs != null) {
	stPeriods += '\n{trcls:"cal' + 
				'",type:"Budget Variance' + 
				'",TOT2011:' + (parseFloat(searchBudget.getValue(columns[2])) - parseFloat(searchActual.getValue(columns[2]))) + 
				',JAN2012:0' + 
				',FEB2012:0' + 
				',MAR2012:0' + 
				',APR2012:0' + 
				',MAY2012:0' + 
				',JUN2012:0' + 
				',JUL2012:0' + 
				',AUG2012:0' + 
				',SEP2012:0' + 
				',OCT2012:0' + 
				',NOV2012:0' + 
				',DEC2012:0' + 
				',TOT2012:' + (parseFloat(searchBudget.getValue(columns[3])) - parseFloat(searchActual.getValue(columns[15]))) + 
				',JAN2013:0' + 
				',FEB2013:0' + 
				',MAR2013:0' + 
				',APR2013:0' + 
				',MAY2013:0' + 
				',JUN2013:0' + 
				',JUL2013:0' + 
				',AUG2013:0' + 
				',SEP2013:0' + 
				',OCT2013:0' + 
				',NOV2013:0' + 
				',DEC2013:0' + 
				',TOT2013:' + (parseFloat(searchBudget.getValue(columns[4])) - parseFloat(searchActual.getValue(columns[28]))) + 
				',TOTAL:' + (parseFloat(searchBudget.getValue(columns[1])) - parseFloat(searchActual.getValue(columns[1]))) + '},';
	}
	
	
	
	
	var strLen = stPeriods.length;
	stPeriods = stPeriods.slice(0,strLen-1);
	stPeriods += '\n];';
    return stPeriods;
}

// export to CSV functions --------------------------------------------------------------------

function csvDataPOs(projid){
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('companyname','vendor','GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid','vendor','GROUP'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('custbody_cologix_project_name',null,'is',projid));
	var searchPOs = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_pos', arrFilters, arrColumns);              
	
	var csvPOs = '';
	if(searchPOs != null){
		csvPOs += 'Period,PO#,Vendor,Billed,Unbilled,Total\n';
		for ( var i = 0;  i < searchPOs.length; i++ ) {
			var searchPO = searchPOs[i];
			var columns = searchPO.getAllColumns();
			
			csvPOs += 	searchPO.getText('postingperiod',null,'GROUP').replace(/\,/g," ") + ',' +	
						searchPO.getValue('tranid',null,'GROUP').replace(/\,/g," ") + ',' +	
						searchPO.getValue('companyname','vendor','GROUP').replace(/\,/g," ") +  ',' +	
						searchPO.getValue(columns[6]).replace(/\,/g," ") +  ',' +	
						searchPO.getValue(columns[7]).replace(/\,/g," ") +  ',' +	
						searchPO.getValue(columns[8]).replace(/\,/g," ") +  '\n';
		}
	}
    return csvPOs;
}

function csvDataBills(projid){
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('companyname','vendor','GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid','vendor','GROUP'));
	arrColumns.push(new nlobjSearchColumn('tranid','appliedToTransaction','GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid','appliedToTransaction','GROUP'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('recordType',null,'is','vendorbill'));
	arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchBills = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);
    
	var csvBills = '';
	if(searchBills != null){
		csvBills += 'Period,Bill#,Vendor,PO#,Amount\n';
		for ( var i = 0;  i < searchBills.length; i++ ) {
			var searchBill = searchBills[i];
			
			csvBills += searchBill.getText('postingperiod',null,'GROUP').replace(/\,/g," ") + ',' +	
						searchBill.getValue('tranid',null,'GROUP').replace(/\,/g," ") + ',' +	
						searchBill.getValue('companyname','vendor','GROUP').replace(/\,/g," ") +  ',' +	
						searchBill.getValue('tranid','appliedToTransaction','GROUP').replace(/\,/g," ") +  ',' +	
						searchBill.getValue('amount',null,'SUM').replace(/\,/g," ") +  '\n';
		}
	}
    return csvBills;
}

function csvDataCredits(projid){
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('companyname','vendor','GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid','vendor','GROUP'));
	arrColumns.push(new nlobjSearchColumn('tranid','appliedToTransaction','GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid','appliedToTransaction','GROUP'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('recordType',null,'is','vendorcredit'));
	arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchCredits = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

	var csvCredits = '';
	if(searchCredits != null){
		csvCredits += 'Period,Bill#,Vendor,PO#,Amount\n';
		for ( var i = 0;  i < searchCredits.length; i++ ) {
			var searchCredit = searchCredits[i];
			
			csvCredits += searchCredit.getText('postingperiod',null,'GROUP').replace(/\,/g," ") + ',' +	
						searchCredit.getValue('tranid',null,'GROUP').replace(/\,/g," ") + ',' +	
						searchCredit.getValue('companyname','vendor','GROUP').replace(/\,/g," ") +  ',' +	
						searchCredit.getValue('tranid','appliedToTransaction','GROUP').replace(/\,/g," ") +  ',' +	
						searchCredit.getValue('amount',null,'SUM').replace(/\,/g," ") +  '\n';
		}
	}
    return csvCredits;
}

function csvDataJournals(projid){
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('memo',null,'GROUP'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('recordType',null,'is','journalentry'));
	arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchJournals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);
    
	var csvJournals = '';
	if(searchJournals != null){
		csvJournals += 'Period,Journal#,Memo,Amount\n';
		for ( var i = 0;  i < searchJournals.length; i++ ) {
			var searchJournal = searchJournals[i];
			
			csvJournals += searchJournal.getText('postingperiod',null,'GROUP').replace(/\,/g," ") + ',' +	
						searchJournal.getValue('tranid',null,'GROUP').replace(/\,/g," ") + ',' +	
						searchJournal.getValue('memo',null,'GROUP').replace(/\,/g," ") +  ',' +	
						searchJournal.getValue('amount',null,'SUM').replace(/\,/g," ") +  '\n';
		}
	}
    return csvJournals;
}

function csvDataCapLabor(projid){
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('memo',null,'GROUP'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('recordType',null,'is','journalentry'));
	arrFilters.push(new nlobjSearchFilter('class',null,'anyof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchCapLabors = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);
    
	var csvCapLabor = '';
	if(searchCapLabors != null){
		csvCapLabor += 'Period,Journal#,Memo,Amount\n';
		for ( var i = 0;  i < searchCapLabors.length; i++ ) {
			var searchCapLabor = searchCapLabors[i];
			
			csvCapLabor += searchCapLabor.getText('postingperiod',null,'GROUP').replace(/\,/g," ") + ',' +	
							searchCapLabor.getValue('tranid',null,'GROUP').replace(/\,/g," ") + ',' +	
							searchCapLabor.getValue('memo',null,'GROUP').replace(/\,/g," ") +  ',' +	
							searchCapLabor.getValue('amount',null,'SUM').replace(/\,/g," ") +  '\n';
		}
	}
    return csvCapLabor;
}

function csvDataProject (projid){

	var projectName = nlapiLookupField('job', projid, 'entityid');
	
	var stPeriods = 'TYPE,TOT2011,JAN2012,FEB2012,MAR2012,APR2012,MAY2012,JUN2012,JUL2012,AUG2012,SEP2012,OCT2012,NOV2012,DEC2012,TOT2012,JAN2013,FEB2013,MAR2013,APR2013,MAY2013,JUN2013,JUL2013,AUG2013,SEP2013,OCT2013,NOV2013,DEC2013,TOT2013,TOTAL\n';
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('recordType',null,'is','vendorbill'));
	arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchBills = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

	if (searchBills != null) {
		var searchBill = searchBills[0];
		var columns = searchBill.getAllColumns();
		
		stPeriods += 'Bills,' + 
					searchBill.getValue(columns[2]) +  ',' +	
					searchBill.getValue(columns[3]) +  ',' +	
					searchBill.getValue(columns[4]) +  ',' +	
					searchBill.getValue(columns[5]) +  ',' +	
					searchBill.getValue(columns[6]) +  ',' +	
					searchBill.getValue(columns[7]) +  ',' +	
					searchBill.getValue(columns[8]) +  ',' +	
					searchBill.getValue(columns[9]) +  ',' +	
					searchBill.getValue(columns[10]) +  ',' +	
					searchBill.getValue(columns[11]) +  ',' +	
					searchBill.getValue(columns[12]) +  ',' +	
					searchBill.getValue(columns[13]) +  ',' +	
					searchBill.getValue(columns[14]) +  ',' +	
					searchBill.getValue(columns[15]) +  ',' +	
					searchBill.getValue(columns[16]) +  ',' +	
					searchBill.getValue(columns[17]) +  ',' +	
					searchBill.getValue(columns[18]) +  ',' +	
					searchBill.getValue(columns[19]) +  ',' +	
					searchBill.getValue(columns[20]) +  ',' +	
					searchBill.getValue(columns[21]) +  ',' +	
					searchBill.getValue(columns[22]) +  ',' +	
					searchBill.getValue(columns[23]) +  ',' +	
					searchBill.getValue(columns[24]) +  ',' +	
					searchBill.getValue(columns[25]) +  ',' +	
					searchBill.getValue(columns[26]) +  ',' +	
					searchBill.getValue(columns[27]) +  ',' +	
					searchBill.getValue(columns[28]) +  ',' +	
					searchBill.getValue(columns[1]) +  '\n';
	}
	else{
		stPeriods += 'Bills,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
	}

	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('recordType',null,'is','vendorcredit'));
	arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchBillsCredits = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

	if (searchBillsCredits != null) {
		var searchBillsCredit = searchBillsCredits[0];
		var columns = searchBillsCredit.getAllColumns();
		
		stPeriods += 'Bills Credit,' + 
					searchBillsCredit.getValue(columns[2]) +  ',' +	
					searchBillsCredit.getValue(columns[3]) +  ',' +	
					searchBillsCredit.getValue(columns[4]) +  ',' +	
					searchBillsCredit.getValue(columns[5]) +  ',' +	
					searchBillsCredit.getValue(columns[6]) +  ',' +	
					searchBillsCredit.getValue(columns[7]) +  ',' +	
					searchBillsCredit.getValue(columns[8]) +  ',' +	
					searchBillsCredit.getValue(columns[9]) +  ',' +	
					searchBillsCredit.getValue(columns[10]) +  ',' +	
					searchBillsCredit.getValue(columns[11]) +  ',' +	
					searchBillsCredit.getValue(columns[12]) +  ',' +	
					searchBillsCredit.getValue(columns[13]) +  ',' +	
					searchBillsCredit.getValue(columns[14]) +  ',' +	
					searchBillsCredit.getValue(columns[15]) +  ',' +	
					searchBillsCredit.getValue(columns[16]) +  ',' +	
					searchBillsCredit.getValue(columns[17]) +  ',' +	
					searchBillsCredit.getValue(columns[18]) +  ',' +	
					searchBillsCredit.getValue(columns[19]) +  ',' +	
					searchBillsCredit.getValue(columns[20]) +  ',' +	
					searchBillsCredit.getValue(columns[21]) +  ',' +	
					searchBillsCredit.getValue(columns[22]) +  ',' +	
					searchBillsCredit.getValue(columns[23]) +  ',' +	
					searchBillsCredit.getValue(columns[24]) +  ',' +	
					searchBillsCredit.getValue(columns[25]) +  ',' +	
					searchBillsCredit.getValue(columns[26]) +  ',' +	
					searchBillsCredit.getValue(columns[27]) +  ',' +	
					searchBillsCredit.getValue(columns[28]) +  ',' +	
					searchBillsCredit.getValue(columns[1]) +  '\n';
	}
	else{
		stPeriods += 'Bills Credit,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
	}
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('recordType',null,'is','journalentry'));
	arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchJournals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

	if (searchJournals != null) {
		var searchJournal = searchJournals[0];
		var columns = searchJournal.getAllColumns();

		stPeriods += 'Journal,' + 
					searchJournal.getValue(columns[2]) +  ',' +	
					searchJournal.getValue(columns[3]) +  ',' +	
					searchJournal.getValue(columns[4]) +  ',' +	
					searchJournal.getValue(columns[5]) +  ',' +	
					searchJournal.getValue(columns[6]) +  ',' +	
					searchJournal.getValue(columns[7]) +  ',' +	
					searchJournal.getValue(columns[8]) +  ',' +	
					searchJournal.getValue(columns[9]) +  ',' +	
					searchJournal.getValue(columns[10]) +  ',' +	
					searchJournal.getValue(columns[11]) +  ',' +	
					searchJournal.getValue(columns[12]) +  ',' +	
					searchJournal.getValue(columns[13]) +  ',' +	
					searchJournal.getValue(columns[14]) +  ',' +	
					searchJournal.getValue(columns[15]) +  ',' +	
					searchJournal.getValue(columns[16]) +  ',' +	
					searchJournal.getValue(columns[17]) +  ',' +	
					searchJournal.getValue(columns[18]) +  ',' +	
					searchJournal.getValue(columns[19]) +  ',' +	
					searchJournal.getValue(columns[20]) +  ',' +	
					searchJournal.getValue(columns[21]) +  ',' +	
					searchJournal.getValue(columns[22]) +  ',' +	
					searchJournal.getValue(columns[23]) +  ',' +	
					searchJournal.getValue(columns[24]) +  ',' +	
					searchJournal.getValue(columns[25]) +  ',' +	
					searchJournal.getValue(columns[26]) +  ',' +	
					searchJournal.getValue(columns[27]) +  ',' +	
					searchJournal.getValue(columns[28]) +  ',' +	
					searchJournal.getValue(columns[1]) +  '\n';
	}
	else{
		stPeriods += 'Journal,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
	}
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('recordType',null,'is','journalentry'));
	arrFilters.push(new nlobjSearchFilter('class',null,'anyof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchCapLabors = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);
	
	if (searchCapLabors != null) {
		var searchCapLabor = searchCapLabors[0];
		var columns = searchCapLabor.getAllColumns();
		
		stPeriods += 'Cap Labor,' + 
					searchCapLabor.getValue(columns[2]) +  ',' +	
					searchCapLabor.getValue(columns[3]) +  ',' +	
					searchCapLabor.getValue(columns[4]) +  ',' +	
					searchCapLabor.getValue(columns[5]) +  ',' +	
					searchCapLabor.getValue(columns[6]) +  ',' +	
					searchCapLabor.getValue(columns[7]) +  ',' +	
					searchCapLabor.getValue(columns[8]) +  ',' +	
					searchCapLabor.getValue(columns[9]) +  ',' +	
					searchCapLabor.getValue(columns[10]) +  ',' +	
					searchCapLabor.getValue(columns[11]) +  ',' +	
					searchCapLabor.getValue(columns[12]) +  ',' +	
					searchCapLabor.getValue(columns[13]) +  ',' +	
					searchCapLabor.getValue(columns[14]) +  ',' +	
					searchCapLabor.getValue(columns[15]) +  ',' +	
					searchCapLabor.getValue(columns[16]) +  ',' +	
					searchCapLabor.getValue(columns[17]) +  ',' +	
					searchCapLabor.getValue(columns[18]) +  ',' +	
					searchCapLabor.getValue(columns[19]) +  ',' +	
					searchCapLabor.getValue(columns[20]) +  ',' +	
					searchCapLabor.getValue(columns[21]) +  ',' +	
					searchCapLabor.getValue(columns[22]) +  ',' +	
					searchCapLabor.getValue(columns[23]) +  ',' +	
					searchCapLabor.getValue(columns[24]) +  ',' +	
					searchCapLabor.getValue(columns[25]) +  ',' +	
					searchCapLabor.getValue(columns[26]) +  ',' +	
					searchCapLabor.getValue(columns[27]) +  ',' +	
					searchCapLabor.getValue(columns[28]) +  ',' +	
					searchCapLabor.getValue(columns[1]) +  '\n';
	}
	else{
		stPeriods += 'Cap Labor,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
	}
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	//arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
	arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
	var searchActuals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

	if (searchActuals != null) {
		var searchActual = searchActuals[0];
		var columns = searchActual.getAllColumns();		
		
		stPeriods += 'Total Actuals,' + 
					searchActual.getValue(columns[2]) +  ',' +	
					searchActual.getValue(columns[3]) +  ',' +	
					searchActual.getValue(columns[4]) +  ',' +	
					searchActual.getValue(columns[5]) +  ',' +	
					searchActual.getValue(columns[6]) +  ',' +	
					searchActual.getValue(columns[7]) +  ',' +	
					searchActual.getValue(columns[8]) +  ',' +	
					searchActual.getValue(columns[9]) +  ',' +	
					searchActual.getValue(columns[10]) +  ',' +	
					searchActual.getValue(columns[11]) +  ',' +	
					searchActual.getValue(columns[12]) +  ',' +	
					searchActual.getValue(columns[13]) +  ',' +	
					searchActual.getValue(columns[14]) +  ',' +	
					searchActual.getValue(columns[15]) +  ',' +	
					searchActual.getValue(columns[16]) +  ',' +	
					searchActual.getValue(columns[17]) +  ',' +	
					searchActual.getValue(columns[18]) +  ',' +	
					searchActual.getValue(columns[19]) +  ',' +	
					searchActual.getValue(columns[20]) +  ',' +	
					searchActual.getValue(columns[21]) +  ',' +	
					searchActual.getValue(columns[22]) +  ',' +	
					searchActual.getValue(columns[23]) +  ',' +	
					searchActual.getValue(columns[24]) +  ',' +	
					searchActual.getValue(columns[25]) +  ',' +	
					searchActual.getValue(columns[26]) +  ',' +	
					searchActual.getValue(columns[27]) +  ',' +	
					searchActual.getValue(columns[28]) +  ',' +	
					searchActual.getValue(columns[1]) +  '\n';
	}
	else{
		stPeriods += 'Total Actuals,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
	}
	
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('custbody_cologix_project_name',null,'is',projid));
	var searchPOs = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_pos', arrFilters, arrColumns); 
	
	if (searchPOs != null) {
		var searchPO = searchPOs[0];
		var columns = searchPO.getAllColumns();
		
		stPeriods += 'Unbilled POs,' + 
					searchPO.getValue(columns[4]) +  ',' +	
					searchPO.getValue(columns[5]) +  ',' +	
					searchPO.getValue(columns[6]) +  ',' +	
					searchPO.getValue(columns[7]) +  ',' +	
					searchPO.getValue(columns[8]) +  ',' +	
					searchPO.getValue(columns[9]) +  ',' +	
					searchPO.getValue(columns[10]) +  ',' +	
					searchPO.getValue(columns[11]) +  ',' +	
					searchPO.getValue(columns[12]) +  ',' +	
					searchPO.getValue(columns[13]) +  ',' +	
					searchPO.getValue(columns[14]) +  ',' +	
					searchPO.getValue(columns[15]) +  ',' +	
					searchPO.getValue(columns[16]) +  ',' +	
					searchPO.getValue(columns[17]) +  ',' +	
					searchPO.getValue(columns[18]) +  ',' +	
					searchPO.getValue(columns[19]) +  ',' +	
					searchPO.getValue(columns[20]) +  ',' +	
					searchPO.getValue(columns[21]) +  ',' +	
					searchPO.getValue(columns[22]) +  ',' +	
					searchPO.getValue(columns[23]) +  ',' +	
					searchPO.getValue(columns[24]) +  ',' +	
					searchPO.getValue(columns[25]) +  ',' +	
					searchPO.getValue(columns[26]) +  ',' +	
					searchPO.getValue(columns[27]) +  ',' +	
					searchPO.getValue(columns[28]) +  ',' +	
					searchPO.getValue(columns[29]) +  ',' +	
					searchPO.getValue(columns[30]) +  ',' +	
					searchPO.getValue(columns[2]) +  '\n';

		stPeriods += 'Billed POs,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,' + searchPO.getValue(columns[1]) +  '\n';
		stPeriods += 'Total POs,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,' + searchPO.getValue(columns[3]) +  '\n';

	}
	else{
		stPeriods += 'Unbilled POs,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
		stPeriods += 'Billed POs,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
		stPeriods += 'Total POs,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
	}
	
	if (searchActuals != null && searchPOs != null) {
		stPeriods += 'Total POs,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,' + (parseFloat(searchActual.getValue(columns[1])) +  parseFloat(searchPO.getValue(columns[2]))) +  '\n';
	}
	else{
		stPeriods += 'Committed Capital,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
	}
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_budget_project',null,'is',projectName));
	var searchBudgets = nlapiSearchRecord('customrecord_clgx_budget', 'customsearch_clgx_rep_cpx_budget', arrFilters, arrColumns); 
	
	if (searchBudgets != null && searchPOs != null) {
		var searchBudget = searchBudgets[0];
		var columns = searchBudget.getAllColumns();

		stPeriods += 'Budget,' + searchPO.getValue(columns[2]) +  ',0,0,0,0,0,0,0,0,0,0,0,0,' + searchPO.getValue(columns[3]) +  ',0,0,0,0,0,0,0,0,0,0,0,0,' + searchPO.getValue(columns[4]) +  ',' + searchPO.getValue(columns[1]) +  '\n';
	}
	else{
		stPeriods += 'Budget,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
	}
	
	
	if (searchBudgets != null && searchActuals != null && searchPOs != null) {

		stPeriods += 'Committed Capital Variance,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,' + (parseFloat(searchBudget.getValue(columns[1])) - (parseFloat(searchActual.getValue(columns[1])) +  parseFloat(searchPO.getValue(columns[2])))) +  '\n';
		}
	else{
		stPeriods += 'Committed Capital Variance,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
	}
	
	if (searchActuals != null) {
		var searchActual = searchActuals[0];
		var columns = searchActual.getAllColumns();
		
		stPeriods += 'Forecasts,' + 
						searchActual.getValue(columns[2]) +  ',' +	
						searchActual.getValue(columns[3]) +  ',' +	
						searchActual.getValue(columns[4]) +  ',' +	
						searchActual.getValue(columns[5]) +  ',' +	
						searchActual.getValue(columns[6]) +  ',' +	
						searchActual.getValue(columns[7]) +  ',' +	
						searchActual.getValue(columns[8]) +  ',' +	
						searchActual.getValue(columns[9]) +  ',' +	
						searchActual.getValue(columns[10]) +  ',' +	
						searchActual.getValue(columns[11]) +  ',' +	
						searchActual.getValue(columns[12]) +  ',' +	
						searchActual.getValue(columns[13]) +  ',' +	
						searchActual.getValue(columns[14]) +  ',' +	
						searchActual.getValue(columns[15]) +  ',' +	
						searchActual.getValue(columns[16]) +  ',' +	
						searchActual.getValue(columns[17]) +  ',' +	
						searchActual.getValue(columns[18]) +  ',' +	
						searchActual.getValue(columns[19]) +  ',' +	
						searchActual.getValue(columns[20]) +  ',' +	
						searchActual.getValue(columns[21]) +  ',' +	
						searchActual.getValue(columns[22]) +  ',' +	
						searchActual.getValue(columns[23]) +  ',' +	
						searchActual.getValue(columns[24]) +  ',' +	
						searchActual.getValue(columns[25]) +  ',' +	
						searchActual.getValue(columns[26]) +  ',' +	
						searchActual.getValue(columns[27]) +  ',' +	
						searchActual.getValue(columns[28]) +  ',' +	
						searchActual.getValue(columns[1]) +  '\n';
	}
	else{
		stPeriods += 'Forecasts,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
	}
	
	if (searchActuals != null && searchPOs != null) {
		stPeriods += 'Total Committed and Projected,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,' + (parseFloat(searchActual.getValue(columns[1])) + (parseFloat(searchActual.getValue(columns[1])) +  parseFloat(searchPO.getValue(columns[2])))) +  '\n';
	}
	
	if (searchBudgets != null && searchActuals != null && searchPOs != null) {
		stPeriods += 'Total Project Variance,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,' + (parseFloat(searchBudget.getValue(columns[1])) - (parseFloat(searchActual.getValue(columns[1])) + (parseFloat(searchActual.getValue(columns[1])) +  parseFloat(searchPO.getValue(columns[2]))))) +  '\n';
	}
	
	if (searchBudgets != null && searchActuals != null && searchPOs != null) {
		stPeriods += 'Budget Variance,' + (parseFloat(searchBudget.getValue(columns[2])) - parseFloat(searchActual.getValue(columns[2]))) +  ',0,0,0,0,0,0,0,0,0,0,0,0,' + (parseFloat(searchBudget.getValue(columns[3])) - parseFloat(searchActual.getValue(columns[15]))) +  ',0,0,0,0,0,0,0,0,0,0,0,0,' + (parseFloat(searchBudget.getValue(columns[4])) - parseFloat(searchActual.getValue(columns[28]))) +  ',' + (parseFloat(searchBudget.getValue(columns[1])) - parseFloat(searchActual.getValue(columns[1]))) +  '\n';
	}

    return stPeriods;
}

function cleanStr (str){
	var string = '';
	string = str.replace(/\'/g," ");
	string = string.replace(/\"/g," ");
	return string;
}
