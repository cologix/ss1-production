
{stReportData}

$(function(){
	$('#reportGrid').datagrid({
		width:800,
		height:440,
		singleSelect:true,
		nowrap: false,
		striped: true,
		collapsible:false,
		sortName: 'case',
		sortOrder: 'desc',
		remoteSort: false,
		idField:'id',
		columns:[[
		    {field:'contact',title:'Description',width:150},
			{field:'date',title:'Open POs',width:70},
			{field:'time',title:'2011 Actual',width:70},
			{field:'ip',title:'Jan-2012 Actual',width:100}
		]],
		fit:true
	});
	$('#reportGrid').datagrid('loadData', reportData);
});
