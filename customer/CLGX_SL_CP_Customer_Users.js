nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CP_Customer_Users.js
//	Script Name:	CLGX_SL_CP_Customer_Users
//	Script Id:		customscript_clgx_sl_cp_customer_users
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		10/06/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=710&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cp_customer_users (request, response){
	try {
		
		var context = nlapiGetContext();
	    //nlapiLogExecution('DEBUG', 'Remaining usage at script beginning', context.getRemainingUsage());
	
		var id = request.getParameter('id') || 0;
	    var filters = [];
	    filters.push(new nlobjSearchFilter("company",null,"is",id));
	    var searchUsers = nlapiSearchRecord('contact', 'customsearch_clgx_cp_contacts', filters);
	    
	    var filters2 = new Array();
	    filters2.push(new nlobjSearchFilter('custrecord_clgx_api_cust_portal_entity',null,'anyof',id));
	    var logins = nlapiSearchRecord('customrecord_clgx_api_cust_portal_logins', 'customsearch_clgx_ss_cp_last_login_2', filters2);
	    
	    
	    var thisContact = '';
	    var lastLogin = '';
	    var users = [];
	    for ( var i = 0; searchUsers != null && i < searchUsers.length; i++ ) {

	            var customerid = parseInt(searchUsers[i].getValue('company',null,null));
	            var modifierid = parseInt(searchUsers[i].getValue('custentity_clgx_modifier',null,null)) || 0;
	            var contactid = parseInt(searchUsers[i].getValue('internalid',null,null));

	            var rigthsStr = searchUsers[i].getValue('custentity_clgx_cp_user_rights_json',null,null) || '';
	            if(rigthsStr != ''){
	                var rights = JSON.parse(rigthsStr);
	            } else {
	                rights = get_min_rights();
	            }
	            lastLogin = '';
	            if (logins != null){
		            for (var a = 0; a < logins.length; a++){
		            	thisContact = logins[a].getValue('custrecord_clgx_api_cust_portal_contact',null,null) || '';
		    	    	if (thisContact == contactid){
		    	    		lastLogin = logins[a].getValue('custrecord_clgx_api_cust_portal_date',null,null) || '';
		    	    		if (lastLogin != '')
		    	    			break;
		    	    	}
		            }
	            }

	            //11/192019 Liz; REMOVED CALL TO customsearch_clgx_ss_cp_last_login as it overflows governance
//	            var filtersLL=new Array();
//	            filtersLL.push(new nlobjSearchFilter("custrecord_clgx_api_cust_portal_contact",null,"is",contactid));
//	            var searchLastLogins = nlapiSearchRecord('customrecord_clgx_api_cust_portal_logins', 'customsearch_clgx_ss_cp_last_login', filtersLL);
//	            var lastLogin='';
//	            if(searchLastLogins != null)
//	            {
//	                var searchLastLogin = searchLastLogins[0];
//	                var columnsLL = searchLastLogin.getAllColumns();
//	                var lastLogin = searchLastLogin.getValue(columnsLL[0]);
//	            }
	            
	            var contact = {
	                "name": searchUsers[i].getValue('entityid',null,null),
	                "username": searchUsers[i].getText('custentity_clgx_modifier',null,null) || '',
	                "title": searchUsers[i].getValue('title',null,null),
	                "email": searchUsers[i].getValue('email',null,null),
	                "phone": searchUsers[i].getValue('phone',null,null),
	                "last_login": lastLogin,
	                "role1": searchUsers[i].getText('contactrole',null,null) || '',
	                "role2": searchUsers[i].getText('custentity_clgx_contact_secondary_role',null,null) || '',
	                "users": parseInt(rights.users),
	                "security": parseInt(rights.security),
	                "cases": parseInt(rights.cases),
	                "casesfin": parseInt(rights.casesfin),
	                "reports": parseInt(rights.reports),
	                "visits": parseInt(rights.visits),
	                "orders": parseInt(rights.orders),
	                "invoices": parseInt(rights.invoices),
	                "colocation": parseInt(rights.colocation),
	                "network": parseInt(rights.network),
	                "managed": parseInt(rights.managed),
	                "domains": parseInt(rights.domains),
	                "shipments": parseInt(rights.shipments)
	            };
	            users.push(contact);
	        }
		
        var objFile = nlapiLoadFile(4148949);
        var html = objFile.getValue();
		html = html.replace(new RegExp('{dataUsers}','g'), JSON.stringify(users));
		
		response.write( html );
		
	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}

function get_min_rights() {
    return {
        "users":0,
        "security":0,
        "cases":0,
        "casesfin":0,
        "reports":0,
        "visits":0,
        "orders":0,
        "invoices":0,
        "ccards":0,
        "colocation":0,
        "network":0,
        "domains":0,
        "managed":0,
        "shipments":0
    };
}

