nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Project.js
//	Script Name:	CLGX_SU_Project
//	Script Id:		customscript_clgx_su_project
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Project
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		04/12/2012
//-------------------------------------------------------------------------------------------------

function beforeLoad(type, form) {
	try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 04/12/2012
// Details:	Add a sub tab to the project form and displays the HTML Capex report.
//-----------------------------------------------------------------------------------------------------------------

//------------- Begin Section 2 -----------------------------------------------------------------------------------
// Version:	1.0 - 04/12/2012
// Details:	Add a sub tab to the project form and displays the ExtJS Capex report.
//-----------------------------------------------------------------------------------------------------------------

		var currentContext = nlapiGetContext();
		stRole = currentContext.getRole();
		
		var isProject = nlapiGetFieldValue('custentity_cologix_int_prj');
		var project_id = nlapiGetRecordId();
		
			if( (currentContext.getExecutionContext() == 'userinterface') && (isProject == 1) && (type == 'edit' | type == 'view')){
				
				var capexTab = form.addTab('custpage_capex_tab', 'Capex');
				var capexField = form.addField('custpage_mini_capex','inlinehtml', null, null, 'custpage_capex_tab');
				var capexFrameHTML = '';
				//capexFrameHTML += '<iframe name="capex" id="capex" src="/app/site/hosting/restlet.nl?script=226&deploy=1&csv=0&projid=' + projID + '" height="510px" width="1010px" frameborder="0" scrolling="no"></iframe>';
				capexFrameHTML += '<iframe name="capex" id="capex" src="/app/site/hosting/scriptlet.nl?script=1481&deploy=1&project_id=' + project_id + '" height="625px" width="1200px" frameborder="0" scrolling="no"></iframe>';
				capexField.setDefaultValue(capexFrameHTML);
			}
			
//---------- End Section 2 ------------------------------------------------------------------------------------------------

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function addCommas(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}


function sumArray (arr){
	var sum = 0;
	for (var i=0; i<arr.length; ++i) {
		sum += parseFloat(arr[i]);
	}
	return addCommas(parseFloat(sum).toFixed(2));
}

