nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Update_Balances.js
//	Script Name:	CLGX_SS_Update_Balances
//	Script Id:		customscript_clgx_ss_update_balances
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Service Order
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		06/01/2017
//-------------------------------------------------------------------------------------------------

function scheduled_update_balances(){
    try{

    	var customerid = nlapiGetContext().getSetting('SCRIPT','custscript_customerid');
    	var balance = clgx_update_balances (customerid);
        
    	nlapiLogExecution('DEBUG','debug', '| customerid = ' + customerid + ' | balance = ' + balance + ' | ');

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
