//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Project.js
//	Script Name:	CLGX_SU_Project
//	Script Id:		customscript_clgx_su_project
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Project
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		04/12/2012
//-------------------------------------------------------------------------------------------------

function beforeLoad(type, form) {
	try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 04/12/2012
// Details:	Add a sub tab to the project form and display a report.
//-----------------------------------------------------------------------------------------------------------------

		var currentContext = nlapiGetContext();
		
		isProject = nlapiGetFieldValue('custentity_cologix_int_prj');
		
		if( (currentContext.getExecutionContext() == 'userinterface') && (isProject == 1) && (type == 'edit' | type == 'view')){
			
			var capexTab = form.addTab('custpage_report_tab', 'CAPEX');
			var capexField = form.addField('custpage_mini_report','inlinehtml', null, null, 'custpage_report_tab');
			var capexFrameHTML = '';
			capexFrameHTML += '<iframe name="capex" id="capex" src="/app/site/hosting/scriptlet.nl?script=144&deploy=1" height="510px" width="1010px" frameborder="0" scrolling="no"></iframe>';
			capexField.setDefaultValue(capexFrameHTML);
		}
		
//---------- End Section 1 ------------------------------------------------------------------------------------------------


	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
