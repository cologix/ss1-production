//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Lead.js
//	Script Name:	CLGX_SU_Lead
//	Script Id:		customscript_clgx_su_lead
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Customer
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		4/28/2015
//-------------------------------------------------------------------------------------------------

function beforeSubmit (type) {
	try {

		var currentContext = nlapiGetContext();
		var userid = nlapiGetUser();
		var roleid = currentContext.getRole(); 
		var customerid = nlapiGetRecordId();

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Created:	4/28/2015
// Details:	Queue contacts/customer to syncronize with SilverPOP
//-----------------------------------------------------------------------------------------------------------------
		/*
		if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'delete')) {
			
			var arrColumns = new Array();
	    	var arrFilters = new Array();
	    	arrFilters.push(new nlobjSearchFilter("internalid","company","anyof",customerid));
	    	var searchContacts = nlapiSearchRecord('contact', 'customsearch_clgx_sp_sync_contacts_ids', arrFilters, arrColumns);
			
	    	for ( var i = 0; searchContacts != null && i < searchContacts.length; i++ ) {
	        	
	        	var contactid = searchContacts[i].getValue('internalid',null,null);
			
	        	var arrColumns = new Array();
		    	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
		    	var arrFilters = new Array();
		    	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_contact_to_sp_contact",null,"equalto",contactid));
		    	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_contact_to_sp_done',null,'is','F'));
		    	var searchQueue = nlapiSearchRecord('customrecord_clgx_queue_contacts_to_sp', null, arrFilters, arrColumns);
		    	
	    		if(searchQueue == null){
		    		var record = nlapiCreateRecord('customrecord_clgx_queue_contacts_to_sp');
					record.setFieldValue('custrecord_clgx_contact_to_sp_contact', contactid);
					record.setFieldValue('custrecord_clgx_contact_to_sp_action', 2);
					record.setFieldValue('custrecord_clgx_contact_to_sp_done', 'F');
					var idRec = nlapiSubmitRecord(record, false,true);
	    		}
	    		else{
	            	nlapiSubmitField('customrecord_clgx_queue_contacts_to_sp', searchQueue[0].getValue('internalid',null,null), 'custrecord_clgx_contact_to_sp_action', 2);
	    		}
			}
		}
		*/	
//---------- End Section 1 ------------------------------------------------------------------------------------------------


		nlapiLogExecution('DEBUG','User Event - beforeSubmit','|--------------------FINISHED---------------------|');  
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function afterSubmit (type) {
	try {

		var currentContext = nlapiGetContext();
		var userid = nlapiGetUser();
		var roleid = currentContext.getRole();
		var customerid = nlapiGetRecordId();
		var inactive = nlapiGetFieldValue('isinactive');

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Created:	4/28/2015
// Details:	Queue contacts/customer to syncronize with SilverPOP
//-----------------------------------------------------------------------------------------------------------------
		/*
		//if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'xedit' || type == 'edit')) {
		
		var action = 1;
		if(type == 'create'){
			action = 0;
		}
		if(type == 'delete'){
			action = 2;
		}
		if(inactive == 'T'){
			action = 3;
		}
	
		var arrColumns = new Array();
    	var arrFilters = new Array();
    	arrFilters.push(new nlobjSearchFilter("internalid","company","anyof",customerid));
    	var searchContacts = nlapiSearchRecord('contact', 'customsearch_clgx_sp_sync_contacts_ids', arrFilters, arrColumns);
    	
    	for ( var i = 0; searchContacts != null && i < searchContacts.length; i++ ) {
        	
        	var contactid = searchContacts[i].getValue('internalid',null,null);
		
        	var arrColumns = new Array();
	    	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	    	var arrFilters = new Array();
	    	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_contact_to_sp_contact",null,"equalto",contactid));
	    	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_contact_to_sp_done',null,'is','F'));
	    	var searchQueue = nlapiSearchRecord('customrecord_clgx_queue_contacts_to_sp', null, arrFilters, arrColumns);
	    	
    		if(searchQueue == null){
	    		var record = nlapiCreateRecord('customrecord_clgx_queue_contacts_to_sp');
				record.setFieldValue('custrecord_clgx_contact_to_sp_contact', contactid);
				record.setFieldValue('custrecord_clgx_contact_to_sp_action', action);
				record.setFieldValue('custrecord_clgx_contact_to_sp_done', 'F');
				var idRec = nlapiSubmitRecord(record, false,true);
    		}
		}
			
		//}
*/
//---------- End Sections ------------------------------------------------------------------------------------------------


		nlapiLogExecution('DEBUG','User Event - beforeSubmit','|--------------------FINISHED---------------------|');  
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
