nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Customers_Delete_Email.js
//	Script Name:	CLGX_SS_Customers_Delete_Email
//	Script Id:		customscript_clgx_customers_delete_email
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Case
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		8/21/2013
//-------------------------------------------------------------------------------------------------

function scheduled_customers_delete_emails () {
	try {

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.1 - 2/8/2012
// Details:	Delete email and phone from customer records
//-----------------------------------------------------------------------------------------------------------------

		nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
		
		var search = nlapiSearchRecord('customer','customsearch_clgx_cust_del_email_phone');
		
		for ( var i = 0; search != null && i < search.length; i++ ) {
			
			var id = search[i].getValue('internalid', null, null);
			
			   var rec = nlapiLoadRecord('customer', id);
			   rec.setFieldValue('email', '');
			   rec.setFieldValue('phone', '');
			   nlapiSubmitRecord(rec, false, true);
			   
			   nlapiLogExecution('DEBUG','customer', id); 
		}
	} 
	catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	}
}


