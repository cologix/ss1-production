nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Customer_NAC_Contacts.js
//	Script Name:	CLGX_SL_Customer_NAC_Contacts
//	Script Id:		customscript_clgx_sl_cust_nac_contacts
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/17/2016
//-------------------------------------------------------------------------------------------------

function suitelet_clgx_sl_cust_nac_contacts (request, response){
    try {

    	var customerid = request.getParameter('customerid');
    	var client_id = nlapiLookupField('customer', customerid, 'custentity_cologix_legacy_customerid');
        
    	var requestURL = nlapiRequestURL(
    			  'https://nsapi1.dev.nac.net/v1.0/clients/' + client_id,
    			  null,
    			  {
    			    'Content-type': 'application/json',
    			    'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
    			  },
    			  null,
    			  'GET');
    	
      	var objRequest = JSON.parse( requestURL.body );
      	
      	var objNACCustomer = new Object();
      	objNACCustomer["text"] = '.';
      	objNACCustomer["client_id"] = client_id;
      	objNACCustomer["netsuite_id"] = objRequest.netsuite_id;
      	objNACCustomer["netsuite_update"] = objRequest.netsuite_update_date;
		
		var arrContacts = new Array();
		// get billing contact from client record
		var objContact = new Object();
		objContact["node"] = objRequest.first_name + ' ' + objRequest.last_name;
		objContact["nodetype"] = 'bcontact';
		objContact["contact_id"] = 0;
		objContact["person_id"] = 0;
		objContact["netsuite_id"] = 0;
		objContact["netsuite_update"] = '';
		objContact["first_name"] = objRequest.first_name;
		objContact["last_name"] = objRequest.last_name;
		objContact["email"] = objRequest.email_address;
		objContact["faicon"] = 'fa fa-user Green1';
		objContact["leaf"] = true;
		arrContacts.push(objContact);
  		
  		// first call to API - contacts of a customer
    	var requestURL = nlapiRequestURL(
  			  'https://nsapi1.dev.nac.net/v1.0/clients/' + client_id + '/contacts/',
  			  null,
  			  {
  			    'Content-type': 'application/json',
  			    'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
  			  },
  			  null,
  			  'GET');
    	
    	var objRequest = JSON.parse( requestURL.body );
    	
		// how many contacts?
		var nbrNACContacts = objRequest.total;
		
		// get all contacts of a client
    	var requestURL = nlapiRequestURL(
    			  'https://nsapi1.dev.nac.net/v1.0/clients/' + client_id + '/contacts/?per_page=' + nbrNACContacts,
    			  null,
    			  {
    			    'Content-type': 'application/json',
    			    'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
    			  },
    			  null,
    			  'GET');
      	
    	var objRequest = JSON.parse( requestURL.body );
		
  		for ( var c = 0; objRequest.data != null && c < objRequest.data.length; c++ ) {
			var objContact = new Object();
			objContact["node"] = objRequest.data[c].first_name + ' ' + objRequest.data[c].last_name;
			objContact["nodetype"] = 'contact';
			objContact["contact_id"] = objRequest.data[c].id;
			objContact["person_id"] = objRequest.data[c].person_id;
			var netsuiteid = parseInt(objRequest.data[c].netsuite_id);
			objContact["netsuite_id"] = netsuiteid;
			objContact["netsuite_update"] = objRequest.data[c].netsuite_update_date;
			objContact["first_name"] = objRequest.data[c].first_name;
			objContact["last_name"] = objRequest.data[c].last_name;
			objContact["email"] = objRequest.data[c].email_address;
			if(netsuiteid > 0){
				objContact["faicon"] = 'fa fa-user SteelBlue2';
			}
			else{
				objContact["faicon"] = 'fa fa-user Crimson1';
			}
			objContact["leaf"] = true;
			arrContacts.push(objContact);
  		}
  		arrContacts = _.sortBy(arrContacts, function(obj){ return obj.node;});
  		
  		
  		var arrPersonsIDs = _.compact(_.uniq(_.pluck(arrContacts, 'person_id')));
  		
  		var arrPersons = new Array();
  		for ( var p = 0; arrPersonsIDs != null && p < arrPersonsIDs.length; p++ ) {
  		
  			// get person
  		    var requestURL = nlapiRequestURL(
  			  'https://nsapi1.dev.nac.net/v1.0/people/' + arrPersonsIDs[p],
  			  null,
  			  {
  			    'Content-type': 'application/json',
  			    'Authorization': 'Basic ' + nlapiEncrypt('4SFHVCO4QJBBOX46:FSHPJVQ3KIKN4TQJ','base64')
  			  },
  			  null,
  			  'GET');
    	
	      	var objRequest = JSON.parse( requestURL.body );
  		
	      	var nbrClients = objRequest.client_ids.length;
	      	
  			var objPerson = new Object();
  			if(nbrClients > 1){
  				objPerson["node"] = objRequest.username + ' (' + nbrClients + ')';
  			}
  			else{
  				objPerson["node"] = objRequest.username
  			}
  			
  			objPerson["nodetype"] = 'user';
  			objPerson["person_id"] = arrPersonsIDs[p];
  			var netsuite_id = parseInt(objRequest.netsuite_id);
  			objPerson["netsuite_id"] = netsuite_id;
  			objPerson["netsuite_update"] = objRequest.netsuite_update_date;
  			objPerson["username"] = objRequest.username;
  			objPerson["client_ids"] = objRequest.client_ids;
  			objPerson["contact_ids"] = objRequest.contact_ids;
  			
  			if(nbrClients > 1){
  				if(netsuiteid > 0){
  					objPerson["faicon"] = 'fa fa-users SteelBlue2';
  				}
  				else{
  					objPerson["faicon"] = 'fa fa-users Crimson1';
  				}
  			}
  			else{
  				if(netsuiteid > 0){
  					objPerson["faicon"] = 'fa fa-user SteelBlue2';
  				}
  				else{
  					objPerson["faicon"] = 'fa fa-user Crimson1';
  				}
  			}
  			objPerson["leaf"] = true;
  			arrPersons.push(objPerson);
  		}
  		arrPersons = _.sortBy(arrPersons, function(obj){ return obj.node;});
  		
		var objContacts = new Object();
		objContacts["node"] = 'NAC Contacts';
		objContacts["nodetype"] = 'naccontacts';
		objContacts["children"] = arrContacts;
		objContacts["expanded"] = true;
		objContacts["faicon"] = 'fa fa-users SteelBlue1';
		objContacts["leaf"] = false;
  		
		
		var objPersons = new Object();
		objPersons["node"] = 'NAC Modifiers (Persons)';
		objPersons["nodetype"] = 'nacpersons';
		objPersons["children"] = arrPersons;
		objPersons["expanded"] = true;
		objPersons["faicon"] = 'fa fa-users SteelBlue1';
		objPersons["leaf"] = false;
		
		var arrNACNodes =  new Array();
		arrNACNodes.push(objContacts);
		arrNACNodes.push(objPersons);
		objNACCustomer["children"] = arrNACNodes;
		
// Netsuite Contacts ===========================================================================================================================	
  		
		var arrFilters = new Array();
    	arrFilters.push(new nlobjSearchFilter('company',null,'anyof', customerid));
    	var searchContacts = nlapiLoadSearch('contact', 'customsearch_clgx_nac_contacts');
    	searchContacts.addFilters(arrFilters);
		var resultSet = searchContacts.runSearch();
		var arrNSContacts = new Array();
		resultSet.forEachResult(function(searchResult) {
			var objContact = new Object();
			objContact["node"] = searchResult.getValue('entityid',null,null);
			//objContact["node"] = searchResult.getValue('firstname',null,null) + ' ' + searchResult.getValue('lastname',null,null);
			objContact["nodetype"] = 'nscontact';
			objContact["contactid"] = parseInt(searchResult.getValue('internalid',null,null));
			//objContact["customerid"] = parseInt(searchResult.getValue('company',null,null));
			//objContact["customer"] = searchResult.getText('company',null,null);
			var nacid = parseInt(searchResult.getValue('custentity_clgx_legacy_contact_id',null,null));
			objContact["nacid"] = nacid;
			objContact["nacpersonid"] = parseInt(searchResult.getValue('custentity_cologix_legacy_modifierid',null,null));
			objContact["roleid"] = parseInt(searchResult.getValue('contactrole',null,null));
			objContact["role"] = searchResult.getText('contactrole',null,null);
			objContact["email"] = searchResult.getValue('email',null,null);
			if(nacid > 0){
				objContact["faicon"] = 'fa fa-user SteelBlue2';
			}
			else{
				objContact["faicon"] = 'fa fa-user Crimson1';
			}
			objContact["leaf"] = true;
			arrNSContacts.push(objContact);
			return true;
		});

		
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_modifier_contact',null,null));
		arrColumns.push(new nlobjSearchColumn('entityid','custrecord_clgx_modifier_contact',null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_modifier_role',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_modifier_title',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_modifier_email',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_modifier_nac_contact_id',null,null));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("custrecord_clgx_modifier_customer",null,"anyof",customerid));
		var searchModifiers = nlapiSearchRecord('customrecord_clgx_modifier', null, arrFilters, arrColumns);
		var arrNSModifiers = new Array();
		for ( var m = 0; searchModifiers != null && m < searchModifiers.length; m++ ) {
			var objNSModifier = new Object();
			//objNSModifier["node"] = searchModifiers[m].getText('custrecord_clgx_modifier_contact',null,null);
			objNSModifier["node"] = searchModifiers[m].getValue('entityid','custrecord_clgx_modifier_contact',null);
			objNSModifier["nodetype"] = 'nsmodifier';
			objNSModifier["contactid"] = parseInt(searchModifiers[m].getValue('custrecord_clgx_modifier_contact',null,null));
			objNSModifier["modifierid"] = parseInt(searchModifiers[m].getValue('internalid',null,null));
			var nacid = parseInt(searchModifiers[m].getValue('custrecord_clgx_modifier_nac_contact_id',null,null));
			objNSModifier["nacid"] = nacid;
			objNSModifier["roleid"] = parseInt(searchModifiers[m].getValue('custrecord_clgx_modifier_role',null,null));
			objNSModifier["role"] = searchModifiers[m].getText('custrecord_clgx_modifier_role',null,null);
			objNSModifier["title"] = searchModifiers[m].getValue('custrecord_clgx_modifier_title',null,null);
			objNSModifier["email"] = searchModifiers[m].getValue('custrecord_clgx_modifier_email',null,null);
			objNSModifier["faicon"] = 'fa fa-user SteelBlue2';
			if(nacid > 0){
				objNSModifier["faicon"] = 'fa fa-user SteelBlue2';
			}
			else{
				objNSModifier["faicon"] = 'fa fa-user Crimson1';
			}
			objNSModifier["leaf"] = true;
			arrNSModifiers.push(objNSModifier);
		}
		//arrNSModifiers = _.sortBy(arrNSModifiers, function(obj){ return obj.node;});
		
		var objNSContacts = new Object();
		objNSContacts["node"] = 'Nestuite Contacts';
		objNSContacts["nodetype"] = 'nscontacts';
		objNSContacts["children"] = arrNSContacts;
		objNSContacts["expanded"] = true;
		objNSContacts["faicon"] = 'fa fa-users SteelBlue1';
		objNSContacts["leaf"] = false;
		
		var objNSModifiers = new Object();
		objNSModifiers["node"] = 'Nestuite Modifiers';
		objNSModifiers["nodetype"] = 'nsmodifiers';
		//objNSModifiers["children"] = arrNSModifiers;
		objNSModifiers["children"] = [];
		objNSModifiers["expanded"] = false;
		objNSModifiers["faicon"] = 'fa fa-users SteelBlue1';
		objNSModifiers["leaf"] = false;
		
		var arrNSNodes =  new Array();
		arrNSNodes.push(objNSContacts);
		arrNSNodes.push(objNSModifiers);
		arrNSNodes["children"] = arrNSNodes;
		
		
      	var objNSCustomer = new Object();
      	objNSCustomer["text"] = '.';
      	objNSCustomer["customerid"] = client_id;
      	objNSCustomer["nacid"] = client_id;
      	objNSCustomer["children"] = arrNSNodes;

		
		
    	var objFile = nlapiLoadFile(3257767);
        var html = objFile.getValue();
		
		html = html.replace(new RegExp('{naccontacts}','g'), JSON.stringify(objNACCustomer));
		html = html.replace(new RegExp('{nscontacts}','g'), JSON.stringify(objNSCustomer));
		
		response.write( html );
		//response.write(JSON.stringify(objClient, null, 4));
		
		
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


