//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Customer_NAC_Sync.js
//	Script Name:	CLGX_SU_Customer_NAC_Sync
//	Script Id:		customscript_clgx_su_customer_nac_sync
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Customer
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		1/13/2016
//-------------------------------------------------------------------------------------------------

function afterSubmit (type) {
	try {

		var record_id = nlapiGetRecordId();
		var record_type = nlapiGetRecordType();
		var row = nlapiLoadRecord(record_type, record_id);
		flexapi('POST','/netsuite/update', {
			'type': record_type,
			'id': record_id,
			'data': json_serialize(row)
		});
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
