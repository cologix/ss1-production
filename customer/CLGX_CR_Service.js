nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_Service.js
//	Script Name:	CLGX_CR_Service
//	Script Id:		customscript_clgx_cr_service
//	Script Runs:	On Client
//	Script Type:	Client Script
//	Deployments:	Service / Project(Job) - Sandbox | Production
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		1/10/2012
//-------------------------------------------------------------------------------------------------

function pageInit(type){
	try {
		nlapiLogExecution('DEBUG','Client/Record - PageInit','|-------------STARTED--------------|');
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 1/10/2012
// Details:	Generate custom managed auto incremented number for a new service.
//-------------------------------------------------------------------------------------------------

		var currentContext = nlapiGetContext();
		stRole = currentContext.getRole();
		if ((currentContext.getExecutionContext() == 'userinterface') && ((type == 'create') || (type == 'copy'))) {
			
			var objNewId = nlapiLoadRecord('customrecord_clgx_auto_generated_numbers', 1);
			var stLastId = objNewId.getFieldValue('custrecord_clgx_auto_generated_number');
			var intNextId = parseInt(stLastId) + 1;
			objNewId.setFieldValue('custrecord_clgx_auto_generated_number', parseInt(intNextId));
			nlapiSubmitRecord(objNewId,true,true);
			
			var stId = new String(intNextId);
			var stPrefix = 'S00000000';
			var stNextId = stPrefix.substring(0,9-parseInt(stId.length)) + stId;
			
			if (stRole == '1063' || stRole == '1021' || stRole == '3' || stRole == '-5' || stRole == '18') { // if role is Cologix Accounting  Manager or Admin or full
				nlapiSetFieldValue('autoname', false);
				nlapiDisableField('autoname', false);
				nlapiSetFieldValue('entityid', stNextId);
				nlapiDisableField('entityid', false);
			}
			else {
				nlapiSetFieldValue('autoname', false);
				nlapiDisableField('autoname', true);
				nlapiSetFieldValue('entityid', stNextId);
				nlapiDisableField('entityid', true);
			}
			
		}
		else if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'edit')){
			
			if (stRole == '1063' || stRole == '1021' || stRole == '3' || stRole == '-5' || stRole == '18') { // if role is Cologix Accounting  Manager or Admin or full
			nlapiDisableField('autoname', false);
			nlapiDisableField('entityid', false);
			}
			else {
			nlapiDisableField('autoname', true);
			nlapiDisableField('entityid', true);
			}
		}
//---------- End Section 1 ------------------------------------------------------------------------------------------------


		nlapiLogExecution('DEBUG','Client/Record - PageInit','|-------------FINISHED--------------|');  
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
