nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Service.js
//	Script Name:	CLGX_SU_Service
//	Script Id:		customscript_clgx_su_service
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Sales Order - Sandbox | Production
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		9/18/2013
//-------------------------------------------------------------------------------------------------

function beforeLoad (type, form) {
	try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Created:		9/18/2013
// Details:	Hide field FAMs if not a legacy Service (starts with S)
//-----------------------------------------------------------------------------------------------------------------
		
		var currentContext = nlapiGetContext();
		if (currentContext.getExecutionContext() == 'userinterface') {
			stRole = currentContext.getRole();
			var serviceId = nlapiGetFieldValue('entityid');
			if(serviceId.charAt(0) == 'S' && stRole != '-5' && stRole != '3' && stRole != '18' && stRole != '1039'){
				form.getField('custentity_clgx_service_fams').setDisplayType('hidden');
			}
		}
		
//---------- End Sections ------------------------------------------------------------------------------------------------
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function beforeSubmit (type) {
	try {
		var currentContext = nlapiGetContext();
		var environment = currentContext.getEnvironment();
	    
		if( type == 'delete'){
			throw nlapiCreateError('99999', 'You can not delete a service record. Please inactivate it.');
		}
	}
	catch (error) {
	    if (error.getDetails != undefined){
	        nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	        throw error;
	    }
	    else{
	        nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	        throw nlapiCreateError('99999', error.toString());
	    }
	} 
}


function afterSubmit (type) {
	try {
		
	}
	catch (error) {
	    if (error.getDetails != undefined){
	        nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	        throw error;
	    }
	    else{
	        nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	        throw nlapiCreateError('99999', error.toString());
	    }
	} 
}



