nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Customer_kW_Peak.js
//	Script Name:	CLGX_SU_Customer_kW_Peak
//	Script Id:		customscript_clgx_su_cust_kw_peak
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/30/2014
//-------------------------------------------------------------------------------------------------

function beforeLoad (type, form) {
	try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Created:	4/15/2014
// Details:	Display download links for BQ and Contract
//-------------------------------------------------------------------------------------------------
		
		if (type == 'view' || type == 'edit'){
			var fileID = nlapiGetFieldValue('custrecord_clgx_dcim_peak_file');
			if(fileID != null && fileID != ''){
				nlapiSetFieldValue('custrecord_clgx_dcim_peak_file_url', '<a href="/app/common/media/mediaitem.nl?id=' + fileID + '&e=T" target="_blank"><img src="//www.cologix.com/images/icon/excel.png" alt="CSV" height="16" width="16"></a>');
			}
		}
	
//---------- End Sections ------------------------------------------------------------------------------------------------
		
		nlapiLogExecution('DEBUG','User Event - Befor Load','|-------------FINISHED--------------|');  
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
