nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_Capex.js
//	Script Name:	CLGX_RL_Capex
//	Script Id:		customscript_clgx_rl_capex
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		9/25/2013
//-------------------------------------------------------------------------------------------------

function restlet_capex(datain){
    try {
        nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------STARTED---------------------|');

        var txtArg = datain;
        txtArg = txtArg.replace(/\"/g,"");
        txtArg = txtArg.replace(/\:/g,",");
        txtArg = txtArg.replace(/\{/g,"");
        txtArg = txtArg.replace(/\}/g,"");
        var arrArguments = new Array();
        arrArguments = txtArg.split( "," );

        var projid = arrArguments[1];
        var csv = arrArguments[3];

        if(csv == 1){

            var attachments = new Array();

            var csvProject = csvDataProject (projid);
            if(csvProject != ''){
                var fileProject = nlapiCreateFile('project.csv', 'CSV', csvProject);
                attachments.push(fileProject);
            }
            var csvPOs = csvDataPOs(projid);
            if(csvPOs != ''){
                var filePOs = nlapiCreateFile('pos.csv', 'CSV', csvPOs);
                attachments.push(filePOs);
            }
            var csvBills = csvDataBills(projid);
            if(csvBills != ''){
                var fileBills = nlapiCreateFile('bills.csv', 'CSV', csvBills);
                attachments.push(fileBills);
            }
            var csvCredits = csvDataCredits(projid);
            if(csvCredits != ''){
                var fileCredits = nlapiCreateFile('credits.csv', 'CSV', csvCredits);
                attachments.push(fileCredits);
            }
            var csvJournals = csvDataJournals(projid);
            if(csvJournals != ''){
                var fileJournals = nlapiCreateFile('journals.csv', 'CSV', csvJournals);
                attachments.push(fileJournals);
            }
            var csvCapLabor = csvDataCapLabor(projid);
            if(csvCapLabor != ''){
                var fileCapLabor = nlapiCreateFile('caplabor.csv', 'CSV', csvCapLabor);
                attachments.push(fileCapLabor);
            }

            var userId = nlapiGetUser();

            var project = nlapiLookupField('job', projid, 'entityid');

            emailSubject = 'Export CSVs for project - ' + project;
            emailBody = '';
            nlapiSendEmail(userId,userId,emailSubject,emailBody,null,null,null,attachments,true);
        }

        var objFile = nlapiLoadFile(302399);
        var capexHTML = objFile.getValue();

        capexHTML = capexHTML.replace(new RegExp('{dataPOs}','g'),jsonDataPOs(projid));
        capexHTML = capexHTML.replace(new RegExp('{dataBills}','g'),jsonDataBills(projid));
        capexHTML = capexHTML.replace(new RegExp('{dataBillsCredits}','g'),jsonDataBillsCredits(projid));
        capexHTML = capexHTML.replace(new RegExp('{dataJournals}','g'),jsonDataJournals(projid));
        capexHTML = capexHTML.replace(new RegExp('{dataJournalsCapLabor}','g'),jsonDataJournalsCapLabor(projid));
        capexHTML = capexHTML.replace(new RegExp('{dataPeriods}','g'),jsonDataProject(projid));
        //capexHTML = capexHTML.replace(new RegExp('{dataPeriods}','g'),'var dataPeriods = []');

        capexHTML = capexHTML.replace(new RegExp('{projid}','g'),projid);

        return capexHTML;

//---------- End Section 1 ------------------------------------------------------------------------------------------------


        nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function jsonDataPOs(projid){

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('companyname','vendor','GROUP'));
    arrColumns.push(new nlobjSearchColumn('internalid','vendor','GROUP'));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('custbody_cologix_project_name',null,'is',projid));
    var searchPOs = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_pos', arrFilters, arrColumns);

    var arrPOs = new Array();
    for ( var i = 0; searchPOs != null && i < searchPOs.length; i++ ) {
        var searchPO = searchPOs[i];
        var columns = searchPO.getAllColumns();

        var objPO = new Object();
        objPO["total"] = 'Total';
        objPO["periodid"] = parseInt(searchPO.getValue('postingperiod',null,'GROUP'));
        objPO["period"] = searchPO.getText('postingperiod',null,'GROUP');
        objPO["poNbr"] = parseInt(searchPO.getValue('tranid',null,'GROUP'));
        objPO["poId"] = parseInt(searchPO.getValue('internalid',null,'GROUP'));
        objPO["vendor"] = cleanStr (searchPO.getValue('companyname','vendor','GROUP'));
        objPO["vendorid"] = parseInt(searchPO.getValue('internalid','vendor','GROUP'));
        objPO["billed"] = parseFloat(searchPO.getValue(columns[6]));
        objPO["unbilled"] = parseFloat(searchPO.getValue(columns[7]));
        objPO["totalPOs"] = parseFloat(searchPO.getValue(columns[8]));
        arrPOs.push(objPO);
    }
    return JSON.stringify(arrPOs);
}

function jsonDataBills(projid){

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('companyname','vendor','GROUP'));
    arrColumns.push(new nlobjSearchColumn('internalid','vendor','GROUP'));

    arrColumns.push(new nlobjSearchColumn('internalid','appliedtotransaction','MAX'));
    arrColumns.push(new nlobjSearchColumn('tranid','appliedtotransaction','MAX'));

    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('recordType',null,'is','vendorbill'));
    arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    //var searchBills = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals2', arrFilters, arrColumns);
    var searchBills = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

    var arrBills = new Array();
    for ( var i = 0; searchBills != null && i < searchBills.length; i++ ) {
        var searchBill = searchBills[i];

        /*
         billid = searchBill.getValue('internalid',null,'GROUP');
         var recBill = nlapiLoadRecord('vendorbill',billid);
         var poid = recBill.getLineItemValue('purchaseorders', 'id', 1);
         if (poid == null || poid ==''){poid = 1;}
         var ponbr = recBill.getLineItemValue('purchaseorders', 'poid', 1);
         if (ponbr == null || ponbr =='' || ponbr == '- None -'){ponbr = '';}
         */

        var objBill = new Object();
        objBill["total"] = 'Total';
        objBill["periodid"] = parseInt(searchBill.getValue('postingperiod',null,'GROUP'));
        objBill["period"] = searchBill.getText('postingperiod',null,'GROUP');
        objBill["billNbr"] = parseInt(searchBill.getValue('tranid',null,'GROUP'));
        objBill["billId"] = parseInt(searchBill.getValue('internalid',null,'GROUP'));
        objBill["vendor"] = cleanStr (searchBill.getValue('companyname','vendor','GROUP'));
        objBill["vendorid"] = parseInt(searchBill.getValue('internalid','vendor','GROUP'));

        //objBill["poNbr"] = parseInt (ponbr);
        //objBill["poId"] = parseInt (poid);
        objBill["poNbr"] = parseInt (searchBill.getValue('tranid','appliedtotransaction','MAX'));
        objBill["poId"] = parseInt (searchBill.getValue('internalid','appliedtotransaction','MAX'));

        objBill["amount"] = parseFloat(searchBill.getValue('fxamount',null,'SUM'));
        arrBills.push(objBill);
    }
    return JSON.stringify(arrBills);
}

function jsonDataBillsCredits(projid){

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('companyname','vendor','GROUP'));
    arrColumns.push(new nlobjSearchColumn('internalid','vendor','GROUP'));

    arrColumns.push(new nlobjSearchColumn('internalid','appliedtotransaction','MAX'));
    arrColumns.push(new nlobjSearchColumn('tranid','appliedtotransaction','MAX'));

    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('recordType',null,'is','vendorcredit'));
    arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchBills = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

    var arrBills = new Array();
    for ( var i = 0; searchBills != null && i < searchBills.length; i++ ) {
        var searchBill = searchBills[i];

        /*
         billid = searchBill.getValue('internalid',null,'GROUP');
         var recBill = nlapiLoadRecord('vendorcredit',billid);
         var poid = recBill.getLineItemValue('purchaseorders', 'id', 1);
         if (poid == null || poid ==''){poid = 1;}
         var ponbr = recBill.getLineItemValue('purchaseorders', 'poid', 1);
         if (ponbr == null || ponbr =='' || ponbr == '- None -'){ponbr = '';}
         */

        var objBill = new Object();
        objBill["total"] = 'Total';
        objBill["periodid"] = parseInt(searchBill.getValue('postingperiod',null,'GROUP'));
        objBill["period"] = searchBill.getText('postingperiod',null,'GROUP');
        objBill["billCreditNbr"] = parseInt(searchBill.getValue('tranid',null,'GROUP'));
        objBill["billCreditId"] = parseInt(searchBill.getValue('internalid',null,'GROUP'));
        objBill["vendor"] = cleanStr (searchBill.getValue('companyname','vendor','GROUP'));
        objBill["vendorid"] = parseInt(searchBill.getValue('internalid','vendor','GROUP'));

        //objBill["poNbr"] = parseInt (ponbr);
        //objBill["poId"] = parseInt (poid);

        objBill["poNbr"] = parseInt (searchBill.getValue('tranid','appliedtotransaction','MAX'));
        objBill["poId"] = parseInt (searchBill.getValue('internalid','appliedtotransaction','MAX'));
        objBill["amount"] = parseFloat(searchBill.getValue('fxamount',null,'SUM'));
        arrBills.push(objBill);
    }
    return JSON.stringify(arrBills);
}

function jsonDataJournals(projid){

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('memo',null,'GROUP'));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchJournals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actual_j', arrFilters, arrColumns);

    var arrJournals = new Array();
    for ( var i = 0; searchJournals != null && i < searchJournals.length; i++ ) {
        var searchJournal = searchJournals[i];

        var objJournal = new Object();
        objJournal["total"] = 'Total';
        objJournal["periodid"] = parseInt(searchJournal.getValue('postingperiod',null,'GROUP'));
        objJournal["period"] = searchJournal.getText('postingperiod',null,'GROUP');
        objJournal["journalNbr"] = parseInt(searchJournal.getValue('tranid',null,'GROUP'));
        objJournal["journalId"] = parseInt(searchJournal.getValue('internalid',null,'GROUP'));
        objJournal["memo"] = cleanStr (searchJournal.getValue('memo',null,'GROUP'));
        objJournal["amount"] = parseFloat(searchJournal.getValue('fxamount',null,'SUM'));
        arrJournals.push(objJournal);
    }
    return JSON.stringify(arrJournals);
}

function jsonDataJournalsCapLabor(projid){

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('memo',null,'GROUP'));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('class',null,'anyof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchJournals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actual_j', arrFilters, arrColumns);

    var arrJournals = new Array();
    for ( var i = 0; searchJournals != null && i < searchJournals.length; i++ ) {
        var searchJournal = searchJournals[i];

        var objJournal = new Object();
        objJournal["total"] = 'Total';
        objJournal["periodid"] = parseInt(searchJournal.getValue('postingperiod',null,'GROUP'));
        objJournal["period"] = searchJournal.getText('postingperiod',null,'GROUP');
        objJournal["journalCLNbr"] = parseInt(searchJournal.getValue('tranid',null,'GROUP'));
        objJournal["journalCLId"] = parseInt(searchJournal.getValue('internalid',null,'GROUP'));
        objJournal["memo"] = cleanStr (searchJournal.getValue('memo',null,'GROUP'));
        objJournal["amount"] = parseFloat(searchJournal.getValue('fxamount',null,'SUM'));
        arrJournals.push(objJournal);
    }
    return JSON.stringify(arrJournals);
}

function jsonDataProject (projid){

    var projectName = nlapiLookupField('job', projid, 'entityid');

    var stPeriods = 'var dataPeriods = [';

    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('recordType',null,'is','vendorbill'));
    arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchBills = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

    if (searchBills != null) {
        var searchBill = searchBills[0];
        var columns = searchBill.getAllColumns();

        stPeriods += '\n{trcls:"row' +
            '",type:"Bills' +
            '",TOT2011:' + searchBill.getValue(columns[2]) +
            ',JAN2012:' + searchBill.getValue(columns[3]) +
            ',FEB2012:' + searchBill.getValue(columns[4]) +
            ',MAR2012:' + searchBill.getValue(columns[5]) +
            ',APR2012:' + searchBill.getValue(columns[6]) +
            ',MAY2012:' + searchBill.getValue(columns[7]) +
            ',JUN2012:' + searchBill.getValue(columns[8]) +
            ',JUL2012:' + searchBill.getValue(columns[9]) +
            ',AUG2012:' + searchBill.getValue(columns[10]) +
            ',SEP2012:' + searchBill.getValue(columns[11]) +
            ',OCT2012:' + searchBill.getValue(columns[12]) +
            ',NOV2012:' + searchBill.getValue(columns[13]) +
            ',DEC2012:' + searchBill.getValue(columns[14]) +
            ',TOT2012:' + searchBill.getValue(columns[15]) +
            ',JAN2013:' + searchBill.getValue(columns[16]) +
            ',FEB2013:' + searchBill.getValue(columns[17]) +
            ',MAR2013:' + searchBill.getValue(columns[18]) +
            ',APR2013:' + searchBill.getValue(columns[19]) +
            ',MAY2013:' + searchBill.getValue(columns[20]) +
            ',JUN2013:' + searchBill.getValue(columns[21]) +
            ',JUL2013:' + searchBill.getValue(columns[22]) +
            ',AUG2013:' + searchBill.getValue(columns[23]) +
            ',SEP2013:' + searchBill.getValue(columns[24]) +
            ',OCT2013:' + searchBill.getValue(columns[25]) +
            ',NOV2013:' + searchBill.getValue(columns[26]) +
            ',DEC2013:' + searchBill.getValue(columns[27]) +
            ',TOT2013:' + searchBill.getValue(columns[28]) +
            ',JAN2014:' + searchBill.getValue(columns[29]) +
            ',FEB2014:' + searchBill.getValue(columns[30]) +
            ',MAR2014:' + searchBill.getValue(columns[31]) +
            ',APR2014:' + searchBill.getValue(columns[32]) +
            ',MAY2014:' + searchBill.getValue(columns[33]) +
            ',JUN2014:' + searchBill.getValue(columns[34]) +
            ',JUL2014:' + searchBill.getValue(columns[35]) +
            ',AUG2014:' + searchBill.getValue(columns[36]) +
            ',SEP2014:' + searchBill.getValue(columns[37]) +
            ',OCT2014:' + searchBill.getValue(columns[38]) +
            ',NOV2014:' + searchBill.getValue(columns[39]) +
            ',DEC2014:' + searchBill.getValue(columns[40]) +
            ',TOT2014:' + searchBill.getValue(columns[41]) +
            ',JAN2015:' + searchBill.getValue(columns[42]) +
            ',FEB2015:' + searchBill.getValue(columns[43]) +
            ',MAR2015:' + searchBill.getValue(columns[44]) +
            ',APR2015:' + searchBill.getValue(columns[45]) +
            ',MAY2015:' + searchBill.getValue(columns[46]) +
            ',JUN2015:' + searchBill.getValue(columns[47]) +
            ',JUL2015:' + searchBill.getValue(columns[48]) +
            ',AUG2015:' + searchBill.getValue(columns[49]) +
            ',SEP2015:' + searchBill.getValue(columns[50]) +
            ',OCT2015:' + searchBill.getValue(columns[51]) +
            ',NOV2015:' + searchBill.getValue(columns[52]) +
            ',DEC2015:' + searchBill.getValue(columns[53]) +
            ',TOT2015:' + searchBill.getValue(columns[54]) +
            ',JAN2016:' + searchBill.getValue(columns[55]) +
            ',FEB2016:' + searchBill.getValue(columns[56]) +
            ',MAR2016:' + searchBill.getValue(columns[57]) +
            ',APR2016:' + searchBill.getValue(columns[58]) +
            ',MAY2016:' + searchBill.getValue(columns[59]) +
            ',JUN2016:' + searchBill.getValue(columns[60]) +
            ',JUL2016:' + searchBill.getValue(columns[61]) +
            ',AUG2016:' + searchBill.getValue(columns[62]) +
            ',SEP2016:' + searchBill.getValue(columns[63]) +
            ',OCT2016:' + searchBill.getValue(columns[64]) +
            ',NOV2016:' + searchBill.getValue(columns[65]) +
            ',DEC2016:' + searchBill.getValue(columns[66]) +
            ',TOT2016:' + searchBill.getValue(columns[67]) +
            ',TOTAL:' + searchBill.getValue(columns[1]) +  '},';
    }
    else{
        stPeriods += '\n{trcls:"row",type:"Bills", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, JAN2014:0, FEB2014:0, MAR2014:0, APR2014:0, MAY2014:0, JUN2014:0, JUL2014:0, AUG2014:0, SEP2014:0, OCT2014:0, NOV2014:0, DEC2014:0, TOT2014:0,JAN2015:0, FEB2015:0, MAR2015:0, APR2015:0, MAY2015:0, JUN2015:0, JUL2015:0, AUG2015:0, SEP2015:0, OCT2015:0, NOV2015:0, DEC2015:0, TOT2015:0,JAN2016:0, FEB2016:0, MAR2016:0, APR2016:0, MAY2016:0, JUN2016:0, JUL2016:0, AUG2016:0, SEP2016:0, OCT2016:0, NOV2016:0, DEC2016:0, TOT2016:0, TOTAL:0},';
    }

    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('recordType',null,'is','vendorcredit'));
    arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchBillsCredits = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

    if (searchBillsCredits != null) {
        var searchBillsCredit = searchBillsCredits[0];
        var columns = searchBillsCredit.getAllColumns();

        stPeriods += '\n{trcls:"row' +
            '",type:"Bills Credit' +
            '",TOT2011:' + searchBillsCredit.getValue(columns[2]) +
            ',JAN2012:' + searchBillsCredit.getValue(columns[3]) +
            ',FEB2012:' + searchBillsCredit.getValue(columns[4]) +
            ',MAR2012:' + searchBillsCredit.getValue(columns[5]) +
            ',APR2012:' + searchBillsCredit.getValue(columns[6]) +
            ',MAY2012:' + searchBillsCredit.getValue(columns[7]) +
            ',JUN2012:' + searchBillsCredit.getValue(columns[8]) +
            ',JUL2012:' + searchBillsCredit.getValue(columns[9]) +
            ',AUG2012:' + searchBillsCredit.getValue(columns[10]) +
            ',SEP2012:' + searchBillsCredit.getValue(columns[11]) +
            ',OCT2012:' + searchBillsCredit.getValue(columns[12]) +
            ',NOV2012:' + searchBillsCredit.getValue(columns[13]) +
            ',DEC2012:' + searchBillsCredit.getValue(columns[14]) +
            ',TOT2012:' + searchBillsCredit.getValue(columns[15]) +
            ',JAN2013:' + searchBillsCredit.getValue(columns[16]) +
            ',FEB2013:' + searchBillsCredit.getValue(columns[17]) +
            ',MAR2013:' + searchBillsCredit.getValue(columns[18]) +
            ',APR2013:' + searchBillsCredit.getValue(columns[19]) +
            ',MAY2013:' + searchBillsCredit.getValue(columns[20]) +
            ',JUN2013:' + searchBillsCredit.getValue(columns[21]) +
            ',JUL2013:' + searchBillsCredit.getValue(columns[22]) +
            ',AUG2013:' + searchBillsCredit.getValue(columns[23]) +
            ',SEP2013:' + searchBillsCredit.getValue(columns[24]) +
            ',OCT2013:' + searchBillsCredit.getValue(columns[25]) +
            ',NOV2013:' + searchBillsCredit.getValue(columns[26]) +
            ',DEC2013:' + searchBillsCredit.getValue(columns[27]) +
            ',TOT2013:' + searchBillsCredit.getValue(columns[28]) +
            ',JAN2014:' + searchBillsCredit.getValue(columns[29]) +
            ',FEB2014:' + searchBillsCredit.getValue(columns[30]) +
            ',MAR2014:' + searchBillsCredit.getValue(columns[31]) +
            ',APR2014:' + searchBillsCredit.getValue(columns[32]) +
            ',MAY2014:' + searchBillsCredit.getValue(columns[33]) +
            ',JUN2014:' + searchBillsCredit.getValue(columns[34]) +
            ',JUL2014:' + searchBillsCredit.getValue(columns[35]) +
            ',AUG2014:' + searchBillsCredit.getValue(columns[36]) +
            ',SEP2014:' + searchBillsCredit.getValue(columns[37]) +
            ',OCT2014:' + searchBillsCredit.getValue(columns[38]) +
            ',NOV2014:' + searchBillsCredit.getValue(columns[39]) +
            ',DEC2014:' + searchBillsCredit.getValue(columns[40]) +
            ',TOT2014:' + searchBillsCredit.getValue(columns[41]) +
            ',JAN2015:' + searchBillsCredit.getValue(columns[42]) +
            ',FEB2015:' + searchBillsCredit.getValue(columns[43]) +
            ',MAR2015:' + searchBillsCredit.getValue(columns[44]) +
            ',APR2015:' + searchBillsCredit.getValue(columns[45]) +
            ',MAY2015:' + searchBillsCredit.getValue(columns[46]) +
            ',JUN2015:' + searchBillsCredit.getValue(columns[47]) +
            ',JUL2015:' + searchBillsCredit.getValue(columns[48]) +
            ',AUG2015:' + searchBillsCredit.getValue(columns[49]) +
            ',SEP2015:' + searchBillsCredit.getValue(columns[50]) +
            ',OCT2015:' + searchBillsCredit.getValue(columns[51]) +
            ',NOV2015:' + searchBillsCredit.getValue(columns[52]) +
            ',DEC2015:' + searchBillsCredit.getValue(columns[53]) +
            ',TOT2015:' + searchBillsCredit.getValue(columns[54]) +
            ',JAN2016:' + searchBillsCredit.getValue(columns[55]) +
            ',FEB2016:' + searchBillsCredit.getValue(columns[56]) +
            ',MAR2016:' + searchBillsCredit.getValue(columns[57]) +
            ',APR2016:' + searchBillsCredit.getValue(columns[58]) +
            ',MAY2016:' + searchBillsCredit.getValue(columns[59]) +
            ',JUN2016:' + searchBillsCredit.getValue(columns[60]) +
            ',JUL2016:' + searchBillsCredit.getValue(columns[61]) +
            ',AUG2016:' + searchBillsCredit.getValue(columns[62]) +
            ',SEP2016:' + searchBillsCredit.getValue(columns[63]) +
            ',OCT2016:' + searchBillsCredit.getValue(columns[64]) +
            ',NOV2016:' + searchBillsCredit.getValue(columns[65]) +
            ',DEC2016:' + searchBillsCredit.getValue(columns[66]) +
            ',TOT2016:' + searchBillsCredit.getValue(columns[67]) +
            ',TOTAL:' + searchBillsCredit.getValue(columns[1]) +  '},';
    }
    else{
        stPeriods += '\n{trcls:"row",type:"Bills Credit", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, JAN2014:0, FEB2014:0, MAR2014:0, APR2014:0, MAY2014:0, JUN2014:0, JUL2014:0, AUG2014:0, SEP2014:0, OCT2014:0, NOV2014:0, DEC2014:0, TOT2014:0,JAN2015:0, FEB2015:0, MAR2015:0, APR2015:0, MAY2015:0, JUN2015:0, JUL2015:0, AUG2015:0, SEP2015:0, OCT2015:0, NOV2015:0, DEC2015:0, TOT2015:0,JAN2016:0, FEB2016:0, MAR2016:0, APR2016:0, MAY2016:0, JUN2016:0, JUL2016:0, AUG2016:0, SEP2016:0, OCT2016:0, NOV2016:0, DEC2016:0, TOT2016:0, TOTAL:0},';
    }

    var arrColumns = new Array();
    var arrFilters = new Array();
    //arrFilters.push(new nlobjSearchFilter('recordType',null,'is','journalentry'));
    arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchJournals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actual_j', arrFilters, arrColumns);

    if (searchJournals != null) {
        var searchJournal = searchJournals[0];
        var columns = searchJournal.getAllColumns();

        stPeriods += '\n{trcls:"row' +
            '",type:"Journal' +
            '",TOT2011:' + searchJournal.getValue(columns[2]) +
            ',JAN2012:' + searchJournal.getValue(columns[3]) +
            ',FEB2012:' + searchJournal.getValue(columns[4]) +
            ',MAR2012:' + searchJournal.getValue(columns[5]) +
            ',APR2012:' + searchJournal.getValue(columns[6]) +
            ',MAY2012:' + searchJournal.getValue(columns[7]) +
            ',JUN2012:' + searchJournal.getValue(columns[8]) +
            ',JUL2012:' + searchJournal.getValue(columns[9]) +
            ',AUG2012:' + searchJournal.getValue(columns[10]) +
            ',SEP2012:' + searchJournal.getValue(columns[11]) +
            ',OCT2012:' + searchJournal.getValue(columns[12]) +
            ',NOV2012:' + searchJournal.getValue(columns[13]) +
            ',DEC2012:' + searchJournal.getValue(columns[14]) +
            ',TOT2012:' + searchJournal.getValue(columns[15]) +
            ',JAN2013:' + searchJournal.getValue(columns[16]) +
            ',FEB2013:' + searchJournal.getValue(columns[17]) +
            ',MAR2013:' + searchJournal.getValue(columns[18]) +
            ',APR2013:' + searchJournal.getValue(columns[19]) +
            ',MAY2013:' + searchJournal.getValue(columns[20]) +
            ',JUN2013:' + searchJournal.getValue(columns[21]) +
            ',JUL2013:' + searchJournal.getValue(columns[22]) +
            ',AUG2013:' + searchJournal.getValue(columns[23]) +
            ',SEP2013:' + searchJournal.getValue(columns[24]) +
            ',OCT2013:' + searchJournal.getValue(columns[25]) +
            ',NOV2013:' + searchJournal.getValue(columns[26]) +
            ',DEC2013:' + searchJournal.getValue(columns[27]) +
            ',TOT2013:' + searchJournal.getValue(columns[28]) +
            ',JAN2014:' + searchJournal.getValue(columns[29]) +
            ',FEB2014:' + searchJournal.getValue(columns[30]) +
            ',MAR2014:' + searchJournal.getValue(columns[31]) +
            ',APR2014:' + searchJournal.getValue(columns[32]) +
            ',MAY2014:' + searchJournal.getValue(columns[33]) +
            ',JUN2014:' + searchJournal.getValue(columns[34]) +
            ',JUL2014:' + searchJournal.getValue(columns[35]) +
            ',AUG2014:' + searchJournal.getValue(columns[36]) +
            ',SEP2014:' + searchJournal.getValue(columns[37]) +
            ',OCT2014:' + searchJournal.getValue(columns[38]) +
            ',NOV2014:' + searchJournal.getValue(columns[39]) +
            ',DEC2014:' + searchJournal.getValue(columns[40]) +
            ',TOT2014:' + searchJournal.getValue(columns[41]) +
            ',JAN2015:' + searchJournal.getValue(columns[42]) +
            ',FEB2015:' + searchJournal.getValue(columns[43]) +
            ',MAR2015:' + searchJournal.getValue(columns[44]) +
            ',APR2015:' + searchJournal.getValue(columns[45]) +
            ',MAY2015:' + searchJournal.getValue(columns[46]) +
            ',JUN2015:' + searchJournal.getValue(columns[47]) +
            ',JUL2015:' + searchJournal.getValue(columns[48]) +
            ',AUG2015:' + searchJournal.getValue(columns[49]) +
            ',SEP2015:' + searchJournal.getValue(columns[50]) +
            ',OCT2015:' + searchJournal.getValue(columns[51]) +
            ',NOV2015:' + searchJournal.getValue(columns[52]) +
            ',DEC2015:' + searchJournal.getValue(columns[53]) +
            ',TOT2015:' + searchJournal.getValue(columns[54]) +
            ',JAN2016:' + searchJournal.getValue(columns[55]) +
            ',FEB2016:' + searchJournal.getValue(columns[56]) +
            ',MAR2016:' + searchJournal.getValue(columns[57]) +
            ',APR2016:' + searchJournal.getValue(columns[58]) +
            ',MAY2016:' + searchJournal.getValue(columns[59]) +
            ',JUN2016:' + searchJournal.getValue(columns[60]) +
            ',JUL2016:' + searchJournal.getValue(columns[61]) +
            ',AUG2016:' + searchJournal.getValue(columns[62]) +
            ',SEP2016:' + searchJournal.getValue(columns[63]) +
            ',OCT2016:' + searchJournal.getValue(columns[64]) +
            ',NOV2016:' + searchJournal.getValue(columns[65]) +
            ',DEC2016:' + searchJournal.getValue(columns[66]) +
            ',TOT2016:' + searchJournal.getValue(columns[67]) +
            ',TOTAL:' + searchJournal.getValue(columns[1]) +  '},';
    }
    else{
        stPeriods += '\n{trcls:"row",type:"Journal", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, JAN2014:0, FEB2014:0, MAR2014:0, APR2014:0, MAY2014:0, JUN2014:0, JUL2014:0, AUG2014:0, SEP2014:0, OCT2014:0, NOV2014:0, DEC2014:0, TOT2014:0,JAN2015:0, FEB2015:0, MAR2015:0, APR2015:0, MAY2015:0, JUN2015:0, JUL2015:0, AUG2015:0, SEP2015:0, OCT2015:0, NOV2015:0, DEC2015:0, TOT2015:0,JAN2016:0, FEB2016:0, MAR2016:0, APR2016:0, MAY2016:0, JUN2016:0, JUL2016:0, AUG2016:0, SEP2016:0, OCT2016:0, NOV2016:0, DEC2016:0, TOT2016:0, TOTAL:0},';
    }
    var arrColumns = new Array();
    var arrFilters = new Array();
    //arrFilters.push(new nlobjSearchFilter('recordType',null,'is','journalentry'));
    arrFilters.push(new nlobjSearchFilter('class',null,'anyof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchCapLabors = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actual_j', arrFilters, arrColumns);

    if (searchCapLabors != null) {
        var searchCapLabor = searchCapLabors[0];
        var columns = searchCapLabor.getAllColumns();

        stPeriods += '\n{trcls:"row' +
            '",type:"Cap Labor' +
            '",TOT2011:' + searchCapLabor.getValue(columns[2]) +
            ',JAN2012:' + searchCapLabor.getValue(columns[3]) +
            ',FEB2012:' + searchCapLabor.getValue(columns[4]) +
            ',MAR2012:' + searchCapLabor.getValue(columns[5]) +
            ',APR2012:' + searchCapLabor.getValue(columns[6]) +
            ',MAY2012:' + searchCapLabor.getValue(columns[7]) +
            ',JUN2012:' + searchCapLabor.getValue(columns[8]) +
            ',JUL2012:' + searchCapLabor.getValue(columns[9]) +
            ',AUG2012:' + searchCapLabor.getValue(columns[10]) +
            ',SEP2012:' + searchCapLabor.getValue(columns[11]) +
            ',OCT2012:' + searchCapLabor.getValue(columns[12]) +
            ',NOV2012:' + searchCapLabor.getValue(columns[13]) +
            ',DEC2012:' + searchCapLabor.getValue(columns[14]) +
            ',TOT2012:' + searchCapLabor.getValue(columns[15]) +
            ',JAN2013:' + searchCapLabor.getValue(columns[16]) +
            ',FEB2013:' + searchCapLabor.getValue(columns[17]) +
            ',MAR2013:' + searchCapLabor.getValue(columns[18]) +
            ',APR2013:' + searchCapLabor.getValue(columns[19]) +
            ',MAY2013:' + searchCapLabor.getValue(columns[20]) +
            ',JUN2013:' + searchCapLabor.getValue(columns[21]) +
            ',JUL2013:' + searchCapLabor.getValue(columns[22]) +
            ',AUG2013:' + searchCapLabor.getValue(columns[23]) +
            ',SEP2013:' + searchCapLabor.getValue(columns[24]) +
            ',OCT2013:' + searchCapLabor.getValue(columns[25]) +
            ',NOV2013:' + searchCapLabor.getValue(columns[26]) +
            ',DEC2013:' + searchCapLabor.getValue(columns[27]) +
            ',TOT2013:' + searchCapLabor.getValue(columns[28]) +
            ',JAN2014:' + searchCapLabor.getValue(columns[29]) +
            ',FEB2014:' + searchCapLabor.getValue(columns[30]) +
            ',MAR2014:' + searchCapLabor.getValue(columns[31]) +
            ',APR2014:' + searchCapLabor.getValue(columns[32]) +
            ',MAY2014:' + searchCapLabor.getValue(columns[33]) +
            ',JUN2014:' + searchCapLabor.getValue(columns[34]) +
            ',JUL2014:' + searchCapLabor.getValue(columns[35]) +
            ',AUG2014:' + searchCapLabor.getValue(columns[36]) +
            ',SEP2014:' + searchCapLabor.getValue(columns[37]) +
            ',OCT2014:' + searchCapLabor.getValue(columns[38]) +
            ',NOV2014:' + searchCapLabor.getValue(columns[39]) +
            ',DEC2014:' + searchCapLabor.getValue(columns[40]) +
            ',TOT2014:' + searchCapLabor.getValue(columns[41]) +
            ',JAN2015:' + searchCapLabor.getValue(columns[42]) +
            ',FEB2015:' + searchCapLabor.getValue(columns[43]) +
            ',MAR2015:' + searchCapLabor.getValue(columns[44]) +
            ',APR2015:' + searchCapLabor.getValue(columns[45]) +
            ',MAY2015:' + searchCapLabor.getValue(columns[46]) +
            ',JUN2015:' + searchCapLabor.getValue(columns[47]) +
            ',JUL2015:' + searchCapLabor.getValue(columns[48]) +
            ',AUG2015:' + searchCapLabor.getValue(columns[49]) +
            ',SEP2015:' + searchCapLabor.getValue(columns[50]) +
            ',OCT2015:' + searchCapLabor.getValue(columns[51]) +
            ',NOV2015:' + searchCapLabor.getValue(columns[52]) +
            ',DEC2015:' + searchCapLabor.getValue(columns[53]) +
            ',TOT2015:' + searchCapLabor.getValue(columns[54]) +
            ',JAN2016:' + searchCapLabor.getValue(columns[55]) +
            ',FEB2016:' + searchCapLabor.getValue(columns[56]) +
            ',MAR2016:' + searchCapLabor.getValue(columns[57]) +
            ',APR2016:' + searchCapLabor.getValue(columns[58]) +
            ',MAY2016:' + searchCapLabor.getValue(columns[59]) +
            ',JUN2016:' + searchCapLabor.getValue(columns[60]) +
            ',JUL2016:' + searchCapLabor.getValue(columns[61]) +
            ',AUG2016:' + searchCapLabor.getValue(columns[62]) +
            ',SEP2016:' + searchCapLabor.getValue(columns[63]) +
            ',OCT2016:' + searchCapLabor.getValue(columns[64]) +
            ',NOV2016:' + searchCapLabor.getValue(columns[65]) +
            ',DEC2016:' + searchCapLabor.getValue(columns[66]) +
            ',TOT2016:' + searchCapLabor.getValue(columns[67]) +
            ',TOTAL:' + searchCapLabor.getValue(columns[1]) +  '},';
    }
    else{
        stPeriods += '\n{trcls:"row",type:"Cap Labor", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, JAN2014:0, FEB2014:0, MAR2014:0, APR2014:0, MAY2014:0, JUN2014:0, JUL2014:0, AUG2014:0, SEP2014:0, OCT2014:0, NOV2014:0, DEC2014:0, TOT2014:0,JAN2015:0, FEB2015:0, MAR2015:0, APR2015:0, MAY2015:0, JUN2015:0, JUL2015:0, AUG2015:0, SEP2015:0, OCT2015:0, NOV2015:0, DEC2015:0, TOT2015:0,JAN2016:0, FEB2016:0, MAR2016:0, APR2016:0, MAY2016:0, JUN2016:0, JUL2016:0, AUG2016:0, SEP2016:0, OCT2016:0, NOV2016:0, DEC2016:0, TOT2016:0, TOTAL:0},';
    }

    var arrColumns = new Array();
    var arrFilters = new Array();
    //arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchActuals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

    if (searchActuals != null) {
        var searchActual = searchActuals[0];
        var columns = searchActual.getAllColumns();
        stPeriods += '\n{trcls:"tot' +
            '",type:"Total Actuals' +
            '",TOT2011:' + searchActual.getValue(columns[2]) +
            ',JAN2012:' + searchActual.getValue(columns[3]) +
            ',FEB2012:' + searchActual.getValue(columns[4]) +
            ',MAR2012:' + searchActual.getValue(columns[5]) +
            ',APR2012:' + searchActual.getValue(columns[6]) +
            ',MAY2012:' + searchActual.getValue(columns[7]) +
            ',JUN2012:' + searchActual.getValue(columns[8]) +
            ',JUL2012:' + searchActual.getValue(columns[9]) +
            ',AUG2012:' + searchActual.getValue(columns[10]) +
            ',SEP2012:' + searchActual.getValue(columns[11]) +
            ',OCT2012:' + searchActual.getValue(columns[12]) +
            ',NOV2012:' + searchActual.getValue(columns[13]) +
            ',DEC2012:' + searchActual.getValue(columns[14]) +
            ',TOT2012:' + searchActual.getValue(columns[15]) +
            ',JAN2013:' + searchActual.getValue(columns[16]) +
            ',FEB2013:' + searchActual.getValue(columns[17]) +
            ',MAR2013:' + searchActual.getValue(columns[18]) +
            ',APR2013:' + searchActual.getValue(columns[19]) +
            ',MAY2013:' + searchActual.getValue(columns[20]) +
            ',JUN2013:' + searchActual.getValue(columns[21]) +
            ',JUL2013:' + searchActual.getValue(columns[22]) +
            ',AUG2013:' + searchActual.getValue(columns[23]) +
            ',SEP2013:' + searchActual.getValue(columns[24]) +
            ',OCT2013:' + searchActual.getValue(columns[25]) +
            ',NOV2013:' + searchActual.getValue(columns[26]) +
            ',DEC2013:' + searchActual.getValue(columns[27]) +
            ',TOT2013:' + searchActual.getValue(columns[28]) +
            ',JAN2014:' + searchActual.getValue(columns[29]) +
            ',FEB2014:' + searchActual.getValue(columns[30]) +
            ',MAR2014:' + searchActual.getValue(columns[31]) +
            ',APR2014:' + searchActual.getValue(columns[32]) +
            ',MAY2014:' + searchActual.getValue(columns[33]) +
            ',JUN2014:' + searchActual.getValue(columns[34]) +
            ',JUL2014:' + searchActual.getValue(columns[35]) +
            ',AUG2014:' + searchActual.getValue(columns[36]) +
            ',SEP2014:' + searchActual.getValue(columns[37]) +
            ',OCT2014:' + searchActual.getValue(columns[38]) +
            ',NOV2014:' + searchActual.getValue(columns[39]) +
            ',DEC2014:' + searchActual.getValue(columns[40]) +
            ',TOT2014:' + searchActual.getValue(columns[41]) +
            ',JAN2015:' + searchActual.getValue(columns[42]) +
            ',FEB2015:' + searchActual.getValue(columns[43]) +
            ',MAR2015:' + searchActual.getValue(columns[44]) +
            ',APR2015:' + searchActual.getValue(columns[45]) +
            ',MAY2015:' + searchActual.getValue(columns[46]) +
            ',JUN2015:' + searchActual.getValue(columns[47]) +
            ',JUL2015:' + searchActual.getValue(columns[48]) +
            ',AUG2015:' + searchActual.getValue(columns[49]) +
            ',SEP2015:' + searchActual.getValue(columns[50]) +
            ',OCT2015:' + searchActual.getValue(columns[51]) +
            ',NOV2015:' + searchActual.getValue(columns[52]) +
            ',DEC2015:' + searchActual.getValue(columns[53]) +
            ',TOT2015:' + searchActual.getValue(columns[54]) +
            ',JAN2016:' + searchActual.getValue(columns[55]) +
            ',FEB2016:' + searchActual.getValue(columns[56]) +
            ',MAR2016:' + searchActual.getValue(columns[57]) +
            ',APR2016:' + searchActual.getValue(columns[58]) +
            ',MAY2016:' + searchActual.getValue(columns[59]) +
            ',JUN2016:' + searchActual.getValue(columns[60]) +
            ',JUL2016:' + searchActual.getValue(columns[61]) +
            ',AUG2016:' + searchActual.getValue(columns[62]) +
            ',SEP2016:' + searchActual.getValue(columns[63]) +
            ',OCT2016:' + searchActual.getValue(columns[64]) +
            ',NOV2016:' + searchActual.getValue(columns[65]) +
            ',DEC2016:' + searchActual.getValue(columns[66]) +
            ',TOT2016:' + searchActual.getValue(columns[67]) +
            ',TOTAL:' + searchActual.getValue(columns[1]) +  '},';
    }
    else{
        stPeriods += '\n{trcls:"tot",type:"Total Actuals", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, JAN2014:0, FEB2014:0, MAR2014:0, APR2014:0, MAY2014:0, JUN2014:0, JUL2014:0, AUG2014:0, SEP2014:0, OCT2014:0, NOV2014:0, DEC2014:0, TOT2014:0,JAN2015:0, FEB2015:0, MAR2015:0, APR2015:0, MAY2015:0, JUN2015:0, JUL2015:0, AUG2015:0, SEP2015:0, OCT2015:0, NOV2015:0, DEC2015:0, TOT2015:0,JAN2016:0, FEB2016:0, MAR2016:0, APR2016:0, MAY2016:0, JUN2016:0, JUL2016:0, AUG2016:0, SEP2016:0, OCT2016:0, NOV2016:0, DEC2016:0, TOT2016:0, TOTAL:0},';
    }
    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('custbody_cologix_project_name',null,'is',projid));
    var searchPOs = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_pos', arrFilters, arrColumns);

    if (searchPOs != null) {
        var searchPO = searchPOs[0];
        var columns = searchPO.getAllColumns();

        stPeriods += '\n{trcls:"row' +
            '",type:"Unbilled POs' +
            '",TOT2011:' + searchPO.getValue(columns[4]) +
            ',JAN2012:' + searchPO.getValue(columns[5]) +
            ',FEB2012:' + searchPO.getValue(columns[6]) +
            ',MAR2012:' + searchPO.getValue(columns[7]) +
            ',APR2012:' + searchPO.getValue(columns[8]) +
            ',MAY2012:' + searchPO.getValue(columns[9]) +
            ',JUN2012:' + searchPO.getValue(columns[10]) +
            ',JUL2012:' + searchPO.getValue(columns[11]) +
            ',AUG2012:' + searchPO.getValue(columns[12]) +
            ',SEP2012:' + searchPO.getValue(columns[13]) +
            ',OCT2012:' + searchPO.getValue(columns[14]) +
            ',NOV2012:' + searchPO.getValue(columns[15]) +
            ',DEC2012:' + searchPO.getValue(columns[16]) +
            ',TOT2012:' + searchPO.getValue(columns[17]) +
            ',JAN2013:' + searchPO.getValue(columns[18]) +
            ',FEB2013:' + searchPO.getValue(columns[19]) +
            ',MAR2013:' + searchPO.getValue(columns[20]) +
            ',APR2013:' + searchPO.getValue(columns[21]) +
            ',MAY2013:' + searchPO.getValue(columns[22]) +
            ',JUN2013:' + searchPO.getValue(columns[23]) +
            ',JUL2013:' + searchPO.getValue(columns[24]) +
            ',AUG2013:' + searchPO.getValue(columns[25]) +
            ',SEP2013:' + searchPO.getValue(columns[26]) +
            ',OCT2013:' + searchPO.getValue(columns[27]) +
            ',NOV2013:' + searchPO.getValue(columns[28]) +
            ',DEC2013:' + searchPO.getValue(columns[29]) +
            ',TOT2013:' + searchPO.getValue(columns[30]) +
            ',JAN2014:' + searchPO.getValue(columns[31]) +
            ',FEB2014:' + searchPO.getValue(columns[32]) +
            ',MAR2014:' + searchPO.getValue(columns[33]) +
            ',APR2014:' + searchPO.getValue(columns[34]) +
            ',MAY2014:' + searchPO.getValue(columns[35]) +
            ',JUN2014:' + searchPO.getValue(columns[36]) +
            ',JUL2014:' + searchPO.getValue(columns[37]) +
            ',AUG2014:' + searchPO.getValue(columns[38]) +
            ',SEP2014:' + searchPO.getValue(columns[39]) +
            ',OCT2014:' + searchPO.getValue(columns[40]) +
            ',NOV2014:' + searchPO.getValue(columns[41]) +
            ',DEC2014:' + searchPO.getValue(columns[42]) +
            ',TOT2014:' + searchPO.getValue(columns[43]) +
            ',JAN2015:' + searchPO.getValue(columns[44]) +
            ',FEB2015:' + searchPO.getValue(columns[45]) +
            ',MAR2015:' + searchPO.getValue(columns[46]) +
            ',APR2015:' + searchPO.getValue(columns[47]) +
            ',MAY2015:' + searchPO.getValue(columns[48]) +
            ',JUN2015:' + searchPO.getValue(columns[49]) +
            ',JUL2015:' + searchPO.getValue(columns[50]) +
            ',AUG2015:' + searchPO.getValue(columns[51]) +
            ',SEP2015:' + searchPO.getValue(columns[52]) +
            ',OCT2015:' + searchPO.getValue(columns[53]) +
            ',NOV2015:' + searchPO.getValue(columns[54]) +
            ',DEC2015:' + searchPO.getValue(columns[55]) +
            ',TOT2015:' + searchPO.getValue(columns[56]) +
            ',JAN2016:' + searchPO.getValue(columns[57]) +
            ',FEB2016:' + searchPO.getValue(columns[58]) +
            ',MAR2016:' + searchPO.getValue(columns[59]) +
            ',APR2016:' + searchPO.getValue(columns[60]) +
            ',MAY2016:' + searchPO.getValue(columns[61]) +
            ',JUN2016:' + searchPO.getValue(columns[62]) +
            ',JUL2016:' + searchPO.getValue(columns[63]) +
            ',AUG2016:' + searchPO.getValue(columns[64]) +
            ',SEP2016:' + searchPO.getValue(columns[65]) +
            ',OCT2016:' + searchPO.getValue(columns[66]) +
            ',NOV2016:' + searchPO.getValue(columns[67]) +
            ',DEC2016:' + searchPO.getValue(columns[68]) +
            ',TOT2016:' + searchPO.getValue(columns[69]) +
            ',TOTAL:' + searchPO.getValue(columns[2]) +  '},';

        stPeriods += '\n{trcls:"row' +
            '",type:"Billed POs' +
            '",TOT2011:0' +
            ',JAN2012:0' +
            ',FEB2012:0' +
            ',MAR2012:0' +
            ',APR2012:0' +
            ',MAY2012:0' +
            ',JUN2012:0' +
            ',JUL2012:0' +
            ',AUG2012:0' +
            ',SEP2012:0' +
            ',OCT2012:0' +
            ',NOV2012:0' +
            ',DEC2012:0' +
            ',TOT2012:0' +
            ',JAN2013:0' +
            ',FEB2013:0' +
            ',MAR2013:0' +
            ',APR2013:0' +
            ',MAY2013:0' +
            ',JUN2013:0' +
            ',JUL2013:0' +
            ',AUG2013:0' +
            ',SEP2013:0' +
            ',OCT2013:0' +
            ',NOV2013:0' +
            ',DEC2013:0' +
            ',TOT2013:0' +
            ',JAN2014:0' +
            ',FEB2014:0' +
            ',MAR2014:0' +
            ',APR2014:0' +
            ',MAY2014:0' +
            ',JUN2014:0' +
            ',JUL2014:0' +
            ',AUG2014:0' +
            ',SEP2014:0' +
            ',OCT2014:0' +
            ',NOV2014:0' +
            ',DEC2014:0' +
            ',TOT2014:0' +
            ',JAN2015:0' +
            ',FEB2015:0' +
            ',MAR2015:0' +
            ',APR2015:0' +
            ',MAY2015:0' +
            ',JUN2015:0' +
            ',JUL2015:0' +
            ',AUG2015:0' +
            ',SEP2015:0' +
            ',OCT2015:0' +
            ',NOV2015:0' +
            ',DEC2015:0' +
            ',TOT2015:0' +
            ',JAN2016:0' +
            ',FEB2016:0' +
            ',MAR2016:0' +
            ',APR2016:0' +
            ',MAY2016:0' +
            ',JUN2016:0' +
            ',JUL2016:0' +
            ',AUG2016:0' +
            ',SEP2016:0' +
            ',OCT2016:0' +
            ',NOV2016:0' +
            ',DEC2016:0' +
            ',TOT2016:0' +
            ',TOTAL:' + searchPO.getValue(columns[1]) +  '},';

        stPeriods += '\n{trcls:"tot' +
            '",type:"Total POs' +
            '",TOT2011:0' +
            ',JAN2012:0' +
            ',FEB2012:0' +
            ',MAR2012:0' +
            ',APR2012:0' +
            ',MAY2012:0' +
            ',JUN2012:0' +
            ',JUL2012:0' +
            ',AUG2012:0' +
            ',SEP2012:0' +
            ',OCT2012:0' +
            ',NOV2012:0' +
            ',DEC2012:0' +
            ',TOT2012:0' +
            ',JAN2013:0' +
            ',FEB2013:0' +
            ',MAR2013:0' +
            ',APR2013:0' +
            ',MAY2013:0' +
            ',JUN2013:0' +
            ',JUL2013:0' +
            ',AUG2013:0' +
            ',SEP2013:0' +
            ',OCT2013:0' +
            ',NOV2013:0' +
            ',DEC2013:0' +
            ',TOT2013:0' +
            ',JAN2014:0' +
            ',FEB2014:0' +
            ',MAR2014:0' +
            ',APR2014:0' +
            ',MAY2014:0' +
            ',JUN2014:0' +
            ',JUL2014:0' +
            ',AUG2014:0' +
            ',SEP2014:0' +
            ',OCT2014:0' +
            ',NOV2014:0' +
            ',DEC2014:0' +
            ',TOT2014:0' +
            ',JAN2015:0' +
            ',FEB2015:0' +
            ',MAR2015:0' +
            ',APR2015:0' +
            ',MAY2015:0' +
            ',JUN2015:0' +
            ',JUL2015:0' +
            ',AUG2015:0' +
            ',SEP2015:0' +
            ',OCT2015:0' +
            ',NOV2015:0' +
            ',DEC2015:0' +
            ',TOT2015:0' +
            ',JAN2016:0' +
            ',FEB2016:0' +
            ',MAR2016:0' +
            ',APR2016:0' +
            ',MAY2016:0' +
            ',JUN2016:0' +
            ',JUL2016:0' +
            ',AUG2016:0' +
            ',SEP2016:0' +
            ',OCT2016:0' +
            ',NOV2016:0' +
            ',DEC2016:0' +
            ',TOT2016:0' +
            ',TOTAL:' + searchPO.getValue(columns[3]) +  '},';
    }
    else{
        stPeriods += '\n{trcls:"row",type:"Unbilled POs", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, JAN2014:0, FEB2014:0, MAR2014:0, APR2014:0, MAY2014:0, JUN2014:0, JUL2014:0, AUG2014:0, SEP2014:0, OCT2014:0, NOV2014:0, DEC2014:0, TOT2014:0, JAN2015:0, FEB2015:0, MAR2015:0, APR2015:0, MAY2015:0, JUN2015:0, JUL2015:0, AUG2015:0, SEP2015:0, OCT2015:0, NOV2015:0, DEC2015:0, TOT2015:0,JAN2016:0, FEB2016:0, MAR2016:0, APR2016:0, MAY2016:0, JUN2016:0, JUL2016:0, AUG2016:0, SEP2016:0, OCT2016:0, NOV2016:0, DEC2016:0, TOT2016:0,TOTAL:0},';
        stPeriods += '\n{trcls:"row",type:"Billed POs", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, JAN2014:0, FEB2014:0, MAR2014:0, APR2014:0, MAY2014:0, JUN2014:0, JUL2014:0, AUG2014:0, SEP2014:0, OCT2014:0, NOV2014:0, DEC2014:0, TOT2014:0,JAN2015:0, FEB2015:0, MAR2015:0, APR2015:0, MAY2015:0, JUN2015:0, JUL2015:0, AUG2015:0, SEP2015:0, OCT2015:0, NOV2015:0, DEC2015:0, TOT2015:0,JAN2016:0, FEB2016:0, MAR2016:0, APR2016:0, MAY2016:0, JUN2016:0, JUL2016:0, AUG2016:0, SEP2016:0, OCT2016:0, NOV2016:0, DEC2016:0, TOT2016:0, TOTAL:0},';
        stPeriods += '\n{trcls:"tot",type:"Total POs", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, JAN2014:0, FEB2014:0, MAR2014:0, APR2014:0, MAY2014:0, JUN2014:0, JUL2014:0, AUG2014:0, SEP2014:0, OCT2014:0, NOV2014:0, DEC2014:0, TOT2014:0,JAN2015:0, FEB2015:0, MAR2015:0, APR2015:0, MAY2015:0, JUN2015:0, JUL2015:0, AUG2015:0, SEP2015:0, OCT2015:0, NOV2015:0, DEC2015:0, TOT2015:0,JAN2016:0, FEB2016:0, MAR2016:0, APR2016:0, MAY2016:0, JUN2016:0, JUL2016:0, AUG2016:0, SEP2016:0, OCT2016:0, NOV2015:0, DEC2016:0, TOT2016:0, TOTAL:0},';
    }

    if (searchActuals != null && searchPOs != null) {

        stPeriods += '\n{trcls:"cal' +
            '",type:"Committed Capital' +
            '",TOT2011:0' +
            ',JAN2012:0' +
            ',FEB2012:0' +
            ',MAR2012:0' +
            ',APR2012:0' +
            ',MAY2012:0' +
            ',JUN2012:0' +
            ',JUL2012:0' +
            ',AUG2012:0' +
            ',SEP2012:0' +
            ',OCT2012:0' +
            ',NOV2012:0' +
            ',DEC2012:0' +
            ',TOT2012:0' +
            ',JAN2013:0' +
            ',FEB2013:0' +
            ',MAR2013:0' +
            ',APR2013:0' +
            ',MAY2013:0' +
            ',JUN2013:0' +
            ',JUL2013:0' +
            ',AUG2013:0' +
            ',SEP2013:0' +
            ',OCT2013:0' +
            ',NOV2013:0' +
            ',DEC2013:0' +
            ',TOT2013:0' +
            ',JAN2014:0' +
            ',FEB2014:0' +
            ',MAR2014:0' +
            ',APR2014:0' +
            ',MAY2014:0' +
            ',JUN2014:0' +
            ',JUL2014:0' +
            ',AUG2014:0' +
            ',SEP2014:0' +
            ',OCT2014:0' +
            ',NOV2014:0' +
            ',DEC2014:0' +
            ',TOT2014:0' +
            ',JAN2015:0' +
            ',FEB2015:0' +
            ',MAR2015:0' +
            ',APR2015:0' +
            ',MAY2015:0' +
            ',JUN2015:0' +
            ',JUL2015:0' +
            ',AUG2015:0' +
            ',SEP2015:0' +
            ',OCT2015:0' +
            ',NOV2015:0' +
            ',DEC2015:0' +
            ',TOT2015:0' +
            ',JAN2016:0' +
            ',FEB2016:0' +
            ',MAR2016:0' +
            ',APR2016:0' +
            ',MAY2016:0' +
            ',JUN2016:0' +
            ',JUL2016:0' +
            ',AUG2016:0' +
            ',SEP2016:0' +
            ',OCT2016:0' +
            ',NOV2016:0' +
            ',DEC2016:0' +
            ',TOT2016:0' +
            ',TOTAL:' + (parseFloat(searchActual.getValue(columns[1])) +  parseFloat(searchPO.getValue(columns[2]))) + '},';
    }
    else{
        stPeriods += '\n{trcls:"cal",type:"Committed Capital", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, JAN2014:0, FEB2014:0, MAR2014:0, APR2014:0, MAY2014:0, JUN2014:0, JUL2014:0, AUG2014:0, SEP2014:0, OCT2014:0, NOV2014:0, DEC2014:0, TOT2014:0,JAN2015:0, FEB2015:0, MAR2015:0, APR2015:0, MAY2015:0, JUN2015:0, JUL2015:0, AUG2015:0, SEP2015:0, OCT2015:0, NOV2015:0, DEC2015:0, TOT2015:0, JAN2016:0, FEB2016:0, MAR2016:0, APR2016:0, MAY2016:0, JUN2016:0, JUL2016:0, AUG2016:0, SEP2016:0, OCT2016:0, NOV2016:0, DEC2016:0, TOT2016:0,TOTAL:0},';
    }

    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('custrecord_clgx_budget_project',null,'is',projectName));
    var searchBudgets = nlapiSearchRecord('customrecord_clgx_budget', 'customsearch_clgx_rep_cpx_budget', arrFilters, arrColumns);

    if (searchBudgets != null) {
        var searchBudget = searchBudgets[0];
        var columns = searchBudget.getAllColumns();
        stPeriods += '\n{trcls:"row' +
            '",type:"Budget' +
            '",TOT2011:' + searchBudget.getValue(columns[2]) +
            ',JAN2012:0' +
            ',FEB2012:0' +
            ',MAR2012:0' +
            ',APR2012:0' +
            ',MAY2012:0' +
            ',JUN2012:0' +
            ',JUL2012:0' +
            ',AUG2012:0' +
            ',SEP2012:0' +
            ',OCT2012:0' +
            ',NOV2012:0' +
            ',DEC2012:0' +
            ',TOT2012:' + searchBudget.getValue(columns[3]) +
            ',JAN2013:0' +
            ',FEB2013:0' +
            ',MAR2013:0' +
            ',APR2013:0' +
            ',MAY2013:0' +
            ',JUN2013:0' +
            ',JUL2013:0' +
            ',AUG2013:0' +
            ',SEP2013:0' +
            ',OCT2013:0' +
            ',NOV2013:0' +
            ',DEC2013:0' +
            ',TOT2013:' + searchBudget.getValue(columns[4]) +
            ',JAN2014:0' +
            ',FEB2014:0' +
            ',MAR2014:0' +
            ',APR2014:0' +
            ',MAY2014:0' +
            ',JUN2014:0' +
            ',JUL2014:0' +
            ',AUG2014:0' +
            ',SEP2014:0' +
            ',OCT2014:0' +
            ',NOV2014:0' +
            ',DEC2014:0' +
            ',TOT2014:0' + searchBudget.getValue(columns[5]) +
            ',JAN2015:0' +
            ',FEB2015:0' +
            ',MAR2015:0' +
            ',APR2015:0' +
            ',MAY2015:0' +
            ',JUN2015:0' +
            ',JUL2015:0' +
            ',AUG2015:0' +
            ',SEP2015:0' +
            ',OCT2015:0' +
            ',NOV2015:0' +
            ',DEC2015:0' +
            ',TOT2015:0' + searchBudget.getValue(columns[6]) +
            ',JAN2016:0' +
            ',FEB2016:0' +
            ',MAR2016:0' +
            ',APR2016:0' +
            ',MAY2016:0' +
            ',JUN2016:0' +
            ',JUL2016:0' +
            ',AUG2016:0' +
            ',SEP2016:0' +
            ',OCT2016:0' +
            ',NOV2016:0' +
            ',DEC2016:0' +
            ',TOT2016:0' + searchBudget.getValue(columns[7]) +
            ',TOTAL:' + searchBudget.getValue(columns[1]) + '},';
    }
    else{
        stPeriods += '\n{trcls:"row",type:"Budget", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, JAN2014:0, FEB2014:0, MAR2014:0, APR2014:0, MAY2014:0, JUN2014:0, JUL2014:0, AUG2014:0, SEP2014:0, OCT2014:0, NOV2014:0, DEC2014:0, TOT2014:0,JAN2015:0, FEB2015:0, MAR2015:0, APR2015:0, MAY2015:0, JUN2015:0, JUL2015:0, AUG2015:0, SEP2015:0, OCT2015:0, NOV2015:0, DEC2015:0, TOT2015:0,JAN2016:0, FEB2016:0, MAR2016:0, APR2016:0, MAY2016:0, JUN2016:0, JUL2016:0, AUG2016:0, SEP2016:0, OCT2016:0, NOV2016:0, DEC2016:0, TOT2016:0, TOTAL:0},';
    }


    if (searchBudgets != null && searchActuals != null && searchPOs != null) {

        stPeriods += '\n{trcls:"cal' +
            '",type:"Committed Capital Variance' +
            '",TOT2011:0' +
            ',JAN2012:0' +
            ',FEB2012:0' +
            ',MAR2012:0' +
            ',APR2012:0' +
            ',MAY2012:0' +
            ',JUN2012:0' +
            ',JUL2012:0' +
            ',AUG2012:0' +
            ',SEP2012:0' +
            ',OCT2012:0' +
            ',NOV2012:0' +
            ',DEC2012:0' +
            ',TOT2012:0' +
            ',JAN2013:0' +
            ',FEB2013:0' +
            ',MAR2013:0' +
            ',APR2013:0' +
            ',MAY2013:0' +
            ',JUN2013:0' +
            ',JUL2013:0' +
            ',AUG2013:0' +
            ',SEP2013:0' +
            ',OCT2013:0' +
            ',NOV2013:0' +
            ',DEC2013:0' +
            ',TOT2013:0' +
            ',JAN2014:0' +
            ',FEB2014:0' +
            ',MAR2014:0' +
            ',APR2014:0' +
            ',MAY2014:0' +
            ',JUN2014:0' +
            ',JUL2014:0' +
            ',AUG2014:0' +
            ',SEP2014:0' +
            ',OCT2014:0' +
            ',NOV2014:0' +
            ',DEC2014:0' +
            ',TOT2014:0' +
            ',JAN2015:0' +
            ',FEB2015:0' +
            ',MAR2015:0' +
            ',APR2015:0' +
            ',MAY2015:0' +
            ',JUN2015:0' +
            ',JUL2015:0' +
            ',AUG2015:0' +
            ',SEP2015:0' +
            ',OCT2015:0' +
            ',NOV2015:0' +
            ',DEC2015:0' +
            ',TOT2015:0' +
            ',JAN2016:0' +
            ',FEB2016:0' +
            ',MAR2016:0' +
            ',APR2016:0' +
            ',MAY2016:0' +
            ',JUN2016:0' +
            ',JUL2016:0' +
            ',AUG2016:0' +
            ',SEP2016:0' +
            ',OCT2016:0' +
            ',NOV2016:0' +
            ',DEC2016:0' +
            ',TOT2016:0' +
            ',TOTAL:' + (parseFloat(searchBudget.getValue(columns[1])) - (parseFloat(searchActual.getValue(columns[1])) +  parseFloat(searchPO.getValue(columns[2])))) + '},';
    }
    else{
        stPeriods += '\n{trcls:"cal",type:"Committed Capital Variance", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, JAN2014:0, FEB2014:0, MAR2014:0, APR2014:0, MAY2014:0, JUN2014:0, JUL2014:0, AUG2014:0, SEP2014:0, OCT2014:0, NOV2014:0, DEC2014:0, TOT2014:0,JAN2015:0, FEB2015:0, MAR2015:0, APR2015:0, MAY2015:0, JUN2015:0, JUL2015:0, AUG2015:0, SEP2015:0, OCT2015:0, NOV2015:0, DEC2015:0, TOT2015:0, JAN2016:0, FEB2016:0, MAR2016:0, APR2016:0, MAY2016:0, JUN2016:0, JUL2016:0, AUG2016:0, SEP2016:0, OCT2016:0, NOV2016:0, DEC2016:0, TOT2016:0,TOTAL:0},';
    }

    if (searchActuals != null) {
        var searchActual = searchActuals[0];
        var columns = searchActual.getAllColumns();
        stPeriods += '\n{trcls:"row' +
            '",type:"Forecasts' +
            '",TOT2011:' + searchActual.getValue(columns[2]) +
            ',JAN2012:' + searchActual.getValue(columns[3]) +
            ',FEB2012:' + searchActual.getValue(columns[4]) +
            ',MAR2012:' + searchActual.getValue(columns[5]) +
            ',APR2012:' + searchActual.getValue(columns[6]) +
            ',MAY2012:' + searchActual.getValue(columns[7]) +
            ',JUN2012:' + searchActual.getValue(columns[8]) +
            ',JUL2012:' + searchActual.getValue(columns[9]) +
            ',AUG2012:' + searchActual.getValue(columns[10]) +
            ',SEP2012:' + searchActual.getValue(columns[11]) +
            ',OCT2012:' + searchActual.getValue(columns[12]) +
            ',NOV2012:' + searchActual.getValue(columns[13]) +
            ',DEC2012:' + searchActual.getValue(columns[14]) +
            ',TOT2012:' + searchActual.getValue(columns[15]) +
            ',JAN2013:' + searchActual.getValue(columns[16]) +
            ',FEB2013:' + searchActual.getValue(columns[17]) +
            ',MAR2013:' + searchActual.getValue(columns[18]) +
            ',APR2013:' + searchActual.getValue(columns[19]) +
            ',MAY2013:' + searchActual.getValue(columns[20]) +
            ',JUN2013:' + searchActual.getValue(columns[21]) +
            ',JUL2013:' + searchActual.getValue(columns[22]) +
            ',AUG2013:' + searchActual.getValue(columns[23]) +
            ',SEP2013:' + searchActual.getValue(columns[24]) +
            ',OCT2013:' + searchActual.getValue(columns[25]) +
            ',NOV2013:' + searchActual.getValue(columns[26]) +
            ',DEC2013:' + searchActual.getValue(columns[27]) +
            ',TOT2013:' + searchActual.getValue(columns[28]) +
            ',JAN2014:' + searchActual.getValue(columns[29]) +
            ',FEB2014:' + searchActual.getValue(columns[30]) +
            ',MAR2014:' + searchActual.getValue(columns[31]) +
            ',APR2014:' + searchActual.getValue(columns[32]) +
            ',MAY2014:' + searchActual.getValue(columns[33]) +
            ',JUN2014:' + searchActual.getValue(columns[34]) +
            ',JUL2014:' + searchActual.getValue(columns[35]) +
            ',AUG2014:' + searchActual.getValue(columns[36]) +
            ',SEP2014:' + searchActual.getValue(columns[37]) +
            ',OCT2014:' + searchActual.getValue(columns[38]) +
            ',NOV2014:' + searchActual.getValue(columns[39]) +
            ',DEC2014:' + searchActual.getValue(columns[40]) +
            ',TOT2014:' + searchActual.getValue(columns[41]) +
            ',JAN2015:' + searchActual.getValue(columns[42]) +
            ',FEB2015:' + searchActual.getValue(columns[43]) +
            ',MAR2015:' + searchActual.getValue(columns[44]) +
            ',APR2015:' + searchActual.getValue(columns[45]) +
            ',MAY2015:' + searchActual.getValue(columns[46]) +
            ',JUN2015:' + searchActual.getValue(columns[47]) +
            ',JUL2015:' + searchActual.getValue(columns[48]) +
            ',AUG2015:' + searchActual.getValue(columns[49]) +
            ',SEP2015:' + searchActual.getValue(columns[50]) +
            ',OCT2015:' + searchActual.getValue(columns[51]) +
            ',NOV2015:' + searchActual.getValue(columns[52]) +
            ',DEC2015:' + searchActual.getValue(columns[53]) +
            ',TOT2015:' + searchActual.getValue(columns[54]) +
            ',JAN2016:' + searchActual.getValue(columns[55]) +
            ',FEB2016:' + searchActual.getValue(columns[56]) +
            ',MAR2016:' + searchActual.getValue(columns[57]) +
            ',APR2016:' + searchActual.getValue(columns[58]) +
            ',MAY2016:' + searchActual.getValue(columns[59]) +
            ',JUN2016:' + searchActual.getValue(columns[60]) +
            ',JUL2016:' + searchActual.getValue(columns[61]) +
            ',AUG2016:' + searchActual.getValue(columns[62]) +
            ',SEP2016:' + searchActual.getValue(columns[63]) +
            ',OCT2016:' + searchActual.getValue(columns[64]) +
            ',NOV2016:' + searchActual.getValue(columns[65]) +
            ',DEC2016:' + searchActual.getValue(columns[66]) +
            ',TOT2016:' + searchActual.getValue(columns[67]) +
            ',TOTAL:' + searchActual.getValue(columns[1]) +  '},';
    }
    else{
        stPeriods += '\n{trcls:"row",type:"Forecasts", TOT2011:0, JAN2012:0, FEB2012:0, MAR2012:0, APR2012:0, MAY2012:0, JUN2012:0, JUL2012:0, AUG2012:0, SEP2012:0, OCT2012:0, NOV2012:0, DEC2012:0, TOT2012:0, JAN2013:0, FEB2013:0, MAR2013:0, APR2013:0, MAY2013:0, JUN2013:0, JUL2013:0, AUG2013:0, SEP2013:0, OCT2013:0, NOV2013:0, DEC2013:0, TOT2013:0, JAN2014:0, FEB2014:0, MAR2014:0, APR2014:0, MAY2014:0, JUN2014:0, JUL2014:0, AUG2014:0, SEP2014:0, OCT2014:0, NOV2014:0, DEC2014:0, TOT2014:0,JAN2015:0, FEB2015:0, MAR2015:0, APR2015:0, MAY2015:0, JUN2015:0, JUL2015:0, AUG2015:0, SEP2015:0, OCT2015:0, NOV2015:0, DEC2015:0, TOT2015:0, JAN2016:0, FEB2016:0, MAR2016:0, APR2016:0, MAY2016:0, JUN2016:0, JUL2016:0, AUG2016:0, SEP2016:0, OCT2016:0, NOV2016:0, DEC2016:0, TOT2016:0,TOTAL:0},';
    }

    if (searchActuals != null && searchPOs != null) {

        stPeriods += '\n{trcls:"cal' +
            '",type:"Total Committed and Projected' +
            '",TOT2011:0' +
            ',JAN2012:0' +
            ',FEB2012:0' +
            ',MAR2012:0' +
            ',APR2012:0' +
            ',MAY2012:0' +
            ',JUN2012:0' +
            ',JUL2012:0' +
            ',AUG2012:0' +
            ',SEP2012:0' +
            ',OCT2012:0' +
            ',NOV2012:0' +
            ',DEC2012:0' +
            ',TOT2012:0' +
            ',JAN2013:0' +
            ',FEB2013:0' +
            ',MAR2013:0' +
            ',APR2013:0' +
            ',MAY2013:0' +
            ',JUN2013:0' +
            ',JUL2013:0' +
            ',AUG2013:0' +
            ',SEP2013:0' +
            ',OCT2013:0' +
            ',NOV2013:0' +
            ',DEC2013:0' +
            ',TOT2013:0' +
            ',JAN2014:0' +
            ',FEB2014:0' +
            ',MAR2014:0' +
            ',APR2014:0' +
            ',MAY2014:0' +
            ',JUN2014:0' +
            ',JUL2014:0' +
            ',AUG2014:0' +
            ',SEP2014:0' +
            ',OCT2014:0' +
            ',NOV2014:0' +
            ',DEC2014:0' +
            ',TOT2014:0' +
            ',JAN2015:0' +
            ',FEB2015:0' +
            ',MAR2015:0' +
            ',APR2015:0' +
            ',MAY2015:0' +
            ',JUN2015:0' +
            ',JUL2015:0' +
            ',AUG2015:0' +
            ',SEP2015:0' +
            ',OCT2015:0' +
            ',NOV2015:0' +
            ',DEC2015:0' +
            ',TOT2015:0' +
            ',JAN2016:0' +
            ',FEB2016:0' +
            ',MAR2016:0' +
            ',APR2016:0' +
            ',MAY2016:0' +
            ',JUN2016:0' +
            ',JUL2016:0' +
            ',AUG2016:0' +
            ',SEP2016:0' +
            ',OCT2016:0' +
            ',NOV2016:0' +
            ',DEC2016:0' +
            ',TOT2016:0' +
            ',TOTAL:' + (parseFloat(searchActual.getValue(columns[1])) + (parseFloat(searchActual.getValue(columns[1])) +  parseFloat(searchPO.getValue(columns[2])))) + '},';
    }

    if (searchBudgets != null && searchActuals != null && searchPOs != null) {

        stPeriods += '\n{trcls:"cal' +
            '",type:"Total Project Variance' +
            '",TOT2011:0' +
            ',JAN2012:0' +
            ',FEB2012:0' +
            ',MAR2012:0' +
            ',APR2012:0' +
            ',MAY2012:0' +
            ',JUN2012:0' +
            ',JUL2012:0' +
            ',AUG2012:0' +
            ',SEP2012:0' +
            ',OCT2012:0' +
            ',NOV2012:0' +
            ',DEC2012:0' +
            ',TOT2012:0' +
            ',JAN2013:0' +
            ',FEB2013:0' +
            ',MAR2013:0' +
            ',APR2013:0' +
            ',MAY2013:0' +
            ',JUN2013:0' +
            ',JUL2013:0' +
            ',AUG2013:0' +
            ',SEP2013:0' +
            ',OCT2013:0' +
            ',NOV2013:0' +
            ',DEC2013:0' +
            ',TOT2013:0' +
            ',JAN2014:0' +
            ',FEB2014:0' +
            ',MAR2014:0' +
            ',APR2014:0' +
            ',MAY2014:0' +
            ',JUN2014:0' +
            ',JUL2014:0' +
            ',AUG2014:0' +
            ',SEP2014:0' +
            ',OCT2014:0' +
            ',NOV2014:0' +
            ',DEC2014:0' +
            ',TOT2014:0' +
            ',JAN2015:0' +
            ',FEB2015:0' +
            ',MAR2015:0' +
            ',APR2015:0' +
            ',MAY2015:0' +
            ',JUN2015:0' +
            ',JUL2015:0' +
            ',AUG2015:0' +
            ',SEP2015:0' +
            ',OCT2015:0' +
            ',NOV2015:0' +
            ',DEC2015:0' +
            ',TOT2015:0' +
            ',JAN2016:0' +
            ',FEB2016:0' +
            ',MAR2016:0' +
            ',APR2016:0' +
            ',MAY2016:0' +
            ',JUN2016:0' +
            ',JUL2016:0' +
            ',AUG2016:0' +
            ',SEP2016:0' +
            ',OCT2016:0' +
            ',NOV2016:0' +
            ',DEC2016:0' +
            ',TOT2016:0' +
            ',TOTAL:' + (parseFloat(searchBudget.getValue(columns[1])) - (parseFloat(searchActual.getValue(columns[1])) + (parseFloat(searchActual.getValue(columns[1])) +  parseFloat(searchPO.getValue(columns[2]))))) + '},';
    }

    if (searchBudgets != null && searchActuals != null && searchPOs != null) {
        stPeriods += '\n{trcls:"cal' +
            '",type:"Budget Variance' +
            '",TOT2011:' + (parseFloat(searchBudget.getValue(columns[2])) - parseFloat(searchActual.getValue(columns[2]))) +
            ',JAN2012:0' +
            ',FEB2012:0' +
            ',MAR2012:0' +
            ',APR2012:0' +
            ',MAY2012:0' +
            ',JUN2012:0' +
            ',JUL2012:0' +
            ',AUG2012:0' +
            ',SEP2012:0' +
            ',OCT2012:0' +
            ',NOV2012:0' +
            ',DEC2012:0' +
            ',TOT2012:' + (parseFloat(searchBudget.getValue(columns[3])) - parseFloat(searchActual.getValue(columns[15]))) +
            ',JAN2013:0' +
            ',FEB2013:0' +
            ',MAR2013:0' +
            ',APR2013:0' +
            ',MAY2013:0' +
            ',JUN2013:0' +
            ',JUL2013:0' +
            ',AUG2013:0' +
            ',SEP2013:0' +
            ',OCT2013:0' +
            ',NOV2013:0' +
            ',DEC2013:0' +
            ',TOT2013:' + (parseFloat(searchBudget.getValue(columns[4])) - parseFloat(searchActual.getValue(columns[28]))) +
            ',JAN2014:0' +
            ',FEB2014:0' +
            ',MAR2014:0' +
            ',APR2014:0' +
            ',MAY2014:0' +
            ',JUN2014:0' +
            ',JUL2014:0' +
            ',AUG2014:0' +
            ',SEP2014:0' +
            ',OCT2014:0' +
            ',NOV2014:0' +
            ',DEC2014:0' +
            ',TOT2014:0' + (parseFloat(searchBudget.getValue(columns[5])) - parseFloat(searchActual.getValue(columns[41]))) +
            ',JAN2015:0' +
            ',FEB2015:0' +
            ',MAR2015:0' +
            ',APR2015:0' +
            ',MAY2015:0' +
            ',JUN2015:0' +
            ',JUL2015:0' +
            ',AUG2015:0' +
            ',SEP2015:0' +
            ',OCT2015:0' +
            ',NOV2015:0' +
            ',DEC2015:0' +
            ',TOT2015:0' + (parseFloat(searchBudget.getValue(columns[6])) - parseFloat(searchActual.getValue(columns[54]))) +
            ',JAN2016:0' +
            ',FEB2016:0' +
            ',MAR2016:0' +
            ',APR2016:0' +
            ',MAY2016:0' +
            ',JUN2016:0' +
            ',JUL2016:0' +
            ',AUG2016:0' +
            ',SEP2016:0' +
            ',OCT2016:0' +
            ',NOV2016:0' +
            ',DEC2016:0' +
            ',TOT2016:0' + (parseFloat(searchBudget.getValue(columns[7])) - parseFloat(searchActual.getValue(columns[67]))) +
            ',TOTAL:' + (parseFloat(searchBudget.getValue(columns[1])) - parseFloat(searchActual.getValue(columns[1]))) + '},';
    }

    var strLen = stPeriods.length;
    stPeriods = stPeriods.slice(0,strLen-1);
    stPeriods += '\n];';
    return stPeriods;
}

// export to CSV functions --------------------------------------------------------------------

function csvDataPOs(projid){

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('companyname','vendor','GROUP'));
    arrColumns.push(new nlobjSearchColumn('internalid','vendor','GROUP'));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('custbody_cologix_project_name',null,'is',projid));
    var searchPOs = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_pos', arrFilters, arrColumns);

    var csvPOs = '';
    if(searchPOs != null){
        csvPOs += 'Period,PO#,Vendor,Billed,Unbilled,Total\n';
        for ( var i = 0;  i < searchPOs.length; i++ ) {
            var searchPO = searchPOs[i];
            var columns = searchPO.getAllColumns();

            csvPOs += 	searchPO.getText('postingperiod',null,'GROUP').replace(/\,/g," ") + ',' +
                searchPO.getValue('tranid',null,'GROUP').replace(/\,/g," ") + ',' +
                searchPO.getValue('companyname','vendor','GROUP').replace(/\,/g," ") +  ',' +
                searchPO.getValue(columns[6]).replace(/\,/g," ") +  ',' +
                searchPO.getValue(columns[7]).replace(/\,/g," ") +  ',' +
                searchPO.getValue(columns[8]).replace(/\,/g," ") +  '\n';
        }
    }
    return csvPOs;
}

function csvDataBills(projid){

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('companyname','vendor','GROUP'));
    arrColumns.push(new nlobjSearchColumn('internalid','vendor','GROUP'));
    arrColumns.push(new nlobjSearchColumn('tranid','appliedtotransaction','MAX'));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('recordType',null,'is','vendorbill'));
    arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchBills = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

    var csvBills = '';
    if(searchBills != null){
        csvBills += 'Period,Bill#,Vendor,PO#,Amount\n';
        for ( var i = 0;  i < searchBills.length; i++ ) {
            var searchBill = searchBills[i];
            /*
             billid = searchBill.getValue('internalid',null,'GROUP');
             var recBill = nlapiLoadRecord('vendorbill',billid);
             var ponbr = recBill.getLineItemValue('purchaseorders', 'poid', 1);
             */
            csvBills += searchBill.getText('postingperiod',null,'GROUP').replace(/\,/g," ") + ',' +
                searchBill.getValue('tranid',null,'GROUP').replace(/\,/g," ") + ',' +
                searchBill.getValue('companyname','vendor','GROUP').replace(/\,/g," ") +  ',' +
                searchBill.getValue('tranid','appliedtotransaction','MAX').replace(/\,/g," ") +  ',' +
                searchBill.getValue('fxamount',null,'SUM').replace(/\,/g," ") +  '\n';
        }
    }
    return csvBills;
}

function csvDataCredits(projid){

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('companyname','vendor','GROUP'));
    arrColumns.push(new nlobjSearchColumn('internalid','vendor','GROUP'));
    arrColumns.push(new nlobjSearchColumn('tranid','appliedToTransaction','MAX'));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('recordType',null,'is','vendorcredit'));
    arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchCredits = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

    var csvCredits = '';
    if(searchCredits != null){
        csvCredits += 'Period,Bill#,Vendor,PO#,Amount\n';
        for ( var i = 0;  i < searchCredits.length; i++ ) {
            var searchCredit = searchCredits[i];
            /*
             billid = searchCredit.getValue('internalid',null,'GROUP');
             var recBill = nlapiLoadRecord('vendorcredit',billid);
             var ponbr = recBill.getLineItemValue('purchaseorders', 'poid', 1);
             */
            csvCredits += searchCredit.getText('postingperiod',null,'GROUP').replace(/\,/g," ") + ',' +
                searchCredit.getValue('tranid',null,'GROUP').replace(/\,/g," ") + ',' +
                searchCredit.getValue('companyname','vendor','GROUP').replace(/\,/g," ") +  ',' +
                searchCredit.getValue('tranid','appliedtotransaction','MAX').replace(/\,/g," ") +  ',' +
                searchCredit.getValue('fxamount',null,'SUM').replace(/\,/g," ") +  '\n';
        }
    }
    return csvCredits;
}

function csvDataJournals(projid){

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('memo',null,'GROUP'));
    var arrFilters = new Array();
    //arrFilters.push(new nlobjSearchFilter('recordType',null,'is','journalentry'));
    arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchJournals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actual_j', arrFilters, arrColumns);

    var csvJournals = '';
    if(searchJournals != null){
        csvJournals += 'Period,Journal#,Memo,Amount\n';
        for ( var i = 0;  i < searchJournals.length; i++ ) {
            var searchJournal = searchJournals[i];

            csvJournals += searchJournal.getText('postingperiod',null,'GROUP').replace(/\,/g," ") + ',' +
                searchJournal.getValue('tranid',null,'GROUP').replace(/\,/g," ") + ',' +
                searchJournal.getValue('memo',null,'GROUP').replace(/\,/g," ") +  ',' +
                searchJournal.getValue('fxamount',null,'SUM').replace(/\,/g," ") +  '\n';
        }
    }
    return csvJournals;
}

function csvDataCapLabor(projid){

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('postingperiod',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('memo',null,'GROUP'));
    var arrFilters = new Array();
    //arrFilters.push(new nlobjSearchFilter('recordType',null,'is','journalentry'));
    arrFilters.push(new nlobjSearchFilter('class',null,'anyof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchCapLabors = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actual_j', arrFilters, arrColumns);

    var csvCapLabor = '';
    if(searchCapLabors != null){
        csvCapLabor += 'Period,Journal#,Memo,Amount\n';
        for ( var i = 0;  i < searchCapLabors.length; i++ ) {
            var searchCapLabor = searchCapLabors[i];

            csvCapLabor += searchCapLabor.getText('postingperiod',null,'GROUP').replace(/\,/g," ") + ',' +
                searchCapLabor.getValue('tranid',null,'GROUP').replace(/\,/g," ") + ',' +
                searchCapLabor.getValue('memo',null,'GROUP').replace(/\,/g," ") +  ',' +
                searchCapLabor.getValue('fxamount',null,'SUM').replace(/\,/g," ") +  '\n';
        }
    }
    return csvCapLabor;
}

function csvDataProject (projid){

    var projectName = nlapiLookupField('job', projid, 'entityid');

    var stPeriods = 'TYPE,TOT2011,JAN2012,FEB2012,MAR2012,APR2012,MAY2012,JUN2012,JUL2012,AUG2012,SEP2012,OCT2012,NOV2012,DEC2012,TOT2012,JAN2013,FEB2013,MAR2013,APR2013,MAY2013,JUN2013,JUL2013,AUG2013,SEP2013,OCT2013,NOV2013,DEC2013,TOT2013,JAN2014,FEB2014,MAR2014,APR2014,MAY2014,JUN2014,JUL2014,AUG2014,SEP2014,OCT2014,NOV2014,DEC2014,TOT2014,JAN2015,FEB2015,MAR2015,APR2015,MAY2015,JUN2015,JUL2015,AUG2015,SEP2015,OCT2015,NOV2015,DEC2015,TOT2015,JAN2016,FEB2016,MAR2016,APR2016,MAY2016,JUN2016,JUL2016,AUG2016,SEP2016,OCT2016,NOV2016,DEC2016,TOT2016,TOTAL\n';

    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('recordType',null,'is','vendorbill'));
    arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchBills = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

    if (searchBills != null) {
        var searchBill = searchBills[0];
        var columns = searchBill.getAllColumns();

        stPeriods += 'Bills,' +
            searchBill.getValue(columns[2]) +  ',' +
            searchBill.getValue(columns[3]) +  ',' +
            searchBill.getValue(columns[4]) +  ',' +
            searchBill.getValue(columns[5]) +  ',' +
            searchBill.getValue(columns[6]) +  ',' +
            searchBill.getValue(columns[7]) +  ',' +
            searchBill.getValue(columns[8]) +  ',' +
            searchBill.getValue(columns[9]) +  ',' +
            searchBill.getValue(columns[10]) +  ',' +
            searchBill.getValue(columns[11]) +  ',' +
            searchBill.getValue(columns[12]) +  ',' +
            searchBill.getValue(columns[13]) +  ',' +
            searchBill.getValue(columns[14]) +  ',' +
            searchBill.getValue(columns[15]) +  ',' +
            searchBill.getValue(columns[16]) +  ',' +
            searchBill.getValue(columns[17]) +  ',' +
            searchBill.getValue(columns[18]) +  ',' +
            searchBill.getValue(columns[19]) +  ',' +
            searchBill.getValue(columns[20]) +  ',' +
            searchBill.getValue(columns[21]) +  ',' +
            searchBill.getValue(columns[22]) +  ',' +
            searchBill.getValue(columns[23]) +  ',' +
            searchBill.getValue(columns[24]) +  ',' +
            searchBill.getValue(columns[25]) +  ',' +
            searchBill.getValue(columns[26]) +  ',' +
            searchBill.getValue(columns[27]) +  ',' +
            searchBill.getValue(columns[28]) +  ',' +
            searchBill.getValue(columns[29]) +  ',' +
            searchBill.getValue(columns[30]) +  ',' +
            searchBill.getValue(columns[31]) +  ',' +
            searchBill.getValue(columns[32]) +  ',' +
            searchBill.getValue(columns[33]) +  ',' +
            searchBill.getValue(columns[34]) +  ',' +
            searchBill.getValue(columns[35]) +  ',' +
            searchBill.getValue(columns[36]) +  ',' +
            searchBill.getValue(columns[37]) +  ',' +
            searchBill.getValue(columns[38]) +  ',' +
            searchBill.getValue(columns[39]) +  ',' +
            searchBill.getValue(columns[40]) +  ',' +
            searchBill.getValue(columns[41]) +  ',' +
            searchBill.getValue(columns[42]) +  ',' +
            searchBill.getValue(columns[43]) +  ',' +
            searchBill.getValue(columns[44]) +  ',' +
            searchBill.getValue(columns[45]) +  ',' +
            searchBill.getValue(columns[46]) +  ',' +
            searchBill.getValue(columns[47]) +  ',' +
            searchBill.getValue(columns[48]) +  ',' +
            searchBill.getValue(columns[49]) +  ',' +
            searchBill.getValue(columns[50]) +  ',' +
            searchBill.getValue(columns[51]) +  ',' +
            searchBill.getValue(columns[52]) +  ',' +
            searchBill.getValue(columns[53]) +  ',' +
            searchBill.getValue(columns[54]) +  ',' +
            searchBill.getValue(columns[55]) +  ',' +
            searchBill.getValue(columns[56]) +  ',' +
            searchBill.getValue(columns[57]) +  ',' +
            searchBill.getValue(columns[58]) +  ',' +
            searchBill.getValue(columns[59]) +  ',' +
            searchBill.getValue(columns[60]) +  ',' +
            searchBill.getValue(columns[61]) +  ',' +
            searchBill.getValue(columns[62]) +  ',' +
            searchBill.getValue(columns[63]) +  ',' +
            searchBill.getValue(columns[64]) +  ',' +
            searchBill.getValue(columns[65]) +  ',' +
            searchBill.getValue(columns[66]) +  ',' +
            searchBill.getValue(columns[67]) +  ',' +
            searchBill.getValue(columns[1]) +  '\n';
    }
    else{
        stPeriods += 'Bills,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
    }

    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('recordType',null,'is','vendorcredit'));
    arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchBillsCredits = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

    if (searchBillsCredits != null) {
        var searchBillsCredit = searchBillsCredits[0];
        var columns = searchBillsCredit.getAllColumns();

        stPeriods += 'Bills Credit,' +
            searchBillsCredit.getValue(columns[2]) +  ',' +
            searchBillsCredit.getValue(columns[3]) +  ',' +
            searchBillsCredit.getValue(columns[4]) +  ',' +
            searchBillsCredit.getValue(columns[5]) +  ',' +
            searchBillsCredit.getValue(columns[6]) +  ',' +
            searchBillsCredit.getValue(columns[7]) +  ',' +
            searchBillsCredit.getValue(columns[8]) +  ',' +
            searchBillsCredit.getValue(columns[9]) +  ',' +
            searchBillsCredit.getValue(columns[10]) +  ',' +
            searchBillsCredit.getValue(columns[11]) +  ',' +
            searchBillsCredit.getValue(columns[12]) +  ',' +
            searchBillsCredit.getValue(columns[13]) +  ',' +
            searchBillsCredit.getValue(columns[14]) +  ',' +
            searchBillsCredit.getValue(columns[15]) +  ',' +
            searchBillsCredit.getValue(columns[16]) +  ',' +
            searchBillsCredit.getValue(columns[17]) +  ',' +
            searchBillsCredit.getValue(columns[18]) +  ',' +
            searchBillsCredit.getValue(columns[19]) +  ',' +
            searchBillsCredit.getValue(columns[20]) +  ',' +
            searchBillsCredit.getValue(columns[21]) +  ',' +
            searchBillsCredit.getValue(columns[22]) +  ',' +
            searchBillsCredit.getValue(columns[23]) +  ',' +
            searchBillsCredit.getValue(columns[24]) +  ',' +
            searchBillsCredit.getValue(columns[25]) +  ',' +
            searchBillsCredit.getValue(columns[26]) +  ',' +
            searchBillsCredit.getValue(columns[27]) +  ',' +
            searchBillsCredit.getValue(columns[28]) +  ',' +
            searchBillsCredit.getValue(columns[29]) +  ',' +
            searchBillsCredit.getValue(columns[30]) +  ',' +
            searchBillsCredit.getValue(columns[31]) +  ',' +
            searchBillsCredit.getValue(columns[32]) +  ',' +
            searchBillsCredit.getValue(columns[33]) +  ',' +
            searchBillsCredit.getValue(columns[34]) +  ',' +
            searchBillsCredit.getValue(columns[35]) +  ',' +
            searchBillsCredit.getValue(columns[36]) +  ',' +
            searchBillsCredit.getValue(columns[37]) +  ',' +
            searchBillsCredit.getValue(columns[38]) +  ',' +
            searchBillsCredit.getValue(columns[39]) +  ',' +
            searchBillsCredit.getValue(columns[40]) +  ',' +
            searchBillsCredit.getValue(columns[41]) +  ',' +
            searchBillsCredit.getValue(columns[42]) +  ',' +
            searchBillsCredit.getValue(columns[43]) +  ',' +
            searchBillsCredit.getValue(columns[44]) +  ',' +
            searchBillsCredit.getValue(columns[45]) +  ',' +
            searchBillsCredit.getValue(columns[46]) +  ',' +
            searchBillsCredit.getValue(columns[47]) +  ',' +
            searchBillsCredit.getValue(columns[48]) +  ',' +
            searchBillsCredit.getValue(columns[49]) +  ',' +
            searchBillsCredit.getValue(columns[50]) +  ',' +
            searchBillsCredit.getValue(columns[51]) +  ',' +
            searchBillsCredit.getValue(columns[52]) +  ',' +
            searchBillsCredit.getValue(columns[53]) +  ',' +
            searchBillsCredit.getValue(columns[54]) +  ',' +
            searchBillsCredit.getValue(columns[55]) +  ',' +
            searchBillsCredit.getValue(columns[56]) +  ',' +
            searchBillsCredit.getValue(columns[57]) +  ',' +
            searchBillsCredit.getValue(columns[58]) +  ',' +
            searchBillsCredit.getValue(columns[59]) +  ',' +
            searchBillsCredit.getValue(columns[60]) +  ',' +
            searchBillsCredit.getValue(columns[61]) +  ',' +
            searchBillsCredit.getValue(columns[62]) +  ',' +
            searchBillsCredit.getValue(columns[63]) +  ',' +
            searchBillsCredit.getValue(columns[64]) +  ',' +
            searchBillsCredit.getValue(columns[65]) +  ',' +
            searchBillsCredit.getValue(columns[66]) +  ',' +
            searchBillsCredit.getValue(columns[67]) +  ',' +
            searchBillsCredit.getValue(columns[1]) +  '\n';
    }
    else{
        stPeriods += 'Bills Credit,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
    }

    var arrColumns = new Array();
    var arrFilters = new Array();
    //arrFilters.push(new nlobjSearchFilter('recordType',null,'is','journalentry'));
    arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchJournals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actual_j', arrFilters, arrColumns);

    if (searchJournals != null) {
        var searchJournal = searchJournals[0];
        var columns = searchJournal.getAllColumns();

        stPeriods += 'Journal,' +
            searchJournal.getValue(columns[2]) +  ',' +
            searchJournal.getValue(columns[3]) +  ',' +
            searchJournal.getValue(columns[4]) +  ',' +
            searchJournal.getValue(columns[5]) +  ',' +
            searchJournal.getValue(columns[6]) +  ',' +
            searchJournal.getValue(columns[7]) +  ',' +
            searchJournal.getValue(columns[8]) +  ',' +
            searchJournal.getValue(columns[9]) +  ',' +
            searchJournal.getValue(columns[10]) +  ',' +
            searchJournal.getValue(columns[11]) +  ',' +
            searchJournal.getValue(columns[12]) +  ',' +
            searchJournal.getValue(columns[13]) +  ',' +
            searchJournal.getValue(columns[14]) +  ',' +
            searchJournal.getValue(columns[15]) +  ',' +
            searchJournal.getValue(columns[16]) +  ',' +
            searchJournal.getValue(columns[17]) +  ',' +
            searchJournal.getValue(columns[18]) +  ',' +
            searchJournal.getValue(columns[19]) +  ',' +
            searchJournal.getValue(columns[20]) +  ',' +
            searchJournal.getValue(columns[21]) +  ',' +
            searchJournal.getValue(columns[22]) +  ',' +
            searchJournal.getValue(columns[23]) +  ',' +
            searchJournal.getValue(columns[24]) +  ',' +
            searchJournal.getValue(columns[25]) +  ',' +
            searchJournal.getValue(columns[26]) +  ',' +
            searchJournal.getValue(columns[27]) +  ',' +
            searchJournal.getValue(columns[28]) +  ',' +
            searchJournal.getValue(columns[29]) +  ',' +
            searchJournal.getValue(columns[30]) +  ',' +
            searchJournal.getValue(columns[31]) +  ',' +
            searchJournal.getValue(columns[32]) +  ',' +
            searchJournal.getValue(columns[33]) +  ',' +
            searchJournal.getValue(columns[34]) +  ',' +
            searchJournal.getValue(columns[35]) +  ',' +
            searchJournal.getValue(columns[36]) +  ',' +
            searchJournal.getValue(columns[37]) +  ',' +
            searchJournal.getValue(columns[38]) +  ',' +
            searchJournal.getValue(columns[39]) +  ',' +
            searchJournal.getValue(columns[40]) +  ',' +
            searchJournal.getValue(columns[41]) +  ',' +
            searchJournal.getValue(columns[42]) +  ',' +
            searchJournal.getValue(columns[43]) +  ',' +
            searchJournal.getValue(columns[44]) +  ',' +
            searchJournal.getValue(columns[45]) +  ',' +
            searchJournal.getValue(columns[46]) +  ',' +
            searchJournal.getValue(columns[47]) +  ',' +
            searchJournal.getValue(columns[48]) +  ',' +
            searchJournal.getValue(columns[49]) +  ',' +
            searchJournal.getValue(columns[50]) +  ',' +
            searchJournal.getValue(columns[51]) +  ',' +
            searchJournal.getValue(columns[52]) +  ',' +
            searchJournal.getValue(columns[53]) +  ',' +
            searchJournal.getValue(columns[54]) +  ',' +
            searchJournal.getValue(columns[55]) +  ',' +
            searchJournal.getValue(columns[56]) +  ',' +
            searchJournal.getValue(columns[57]) +  ',' +
            searchJournal.getValue(columns[58]) +  ',' +
            searchJournal.getValue(columns[59]) +  ',' +
            searchJournal.getValue(columns[60]) +  ',' +
            searchJournal.getValue(columns[61]) +  ',' +
            searchJournal.getValue(columns[62]) +  ',' +
            searchJournal.getValue(columns[63]) +  ',' +
            searchJournal.getValue(columns[64]) +  ',' +
            searchJournal.getValue(columns[65]) +  ',' +
            searchJournal.getValue(columns[66]) +  ',' +
            searchJournal.getValue(columns[67]) +  ',' +
            searchJournal.getValue(columns[1]) +  '\n';
    }
    else{
        stPeriods += 'Journal,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
    }

    var arrColumns = new Array();
    var arrFilters = new Array();
    //arrFilters.push(new nlobjSearchFilter('recordType',null,'is','journalentry'));
    arrFilters.push(new nlobjSearchFilter('class',null,'anyof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchCapLabors = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actual_j', arrFilters, arrColumns);

    if (searchCapLabors != null) {
        var searchCapLabor = searchCapLabors[0];
        var columns = searchCapLabor.getAllColumns();

        stPeriods += 'Cap Labor,' +
            searchCapLabor.getValue(columns[2]) +  ',' +
            searchCapLabor.getValue(columns[3]) +  ',' +
            searchCapLabor.getValue(columns[4]) +  ',' +
            searchCapLabor.getValue(columns[5]) +  ',' +
            searchCapLabor.getValue(columns[6]) +  ',' +
            searchCapLabor.getValue(columns[7]) +  ',' +
            searchCapLabor.getValue(columns[8]) +  ',' +
            searchCapLabor.getValue(columns[9]) +  ',' +
            searchCapLabor.getValue(columns[10]) +  ',' +
            searchCapLabor.getValue(columns[11]) +  ',' +
            searchCapLabor.getValue(columns[12]) +  ',' +
            searchCapLabor.getValue(columns[13]) +  ',' +
            searchCapLabor.getValue(columns[14]) +  ',' +
            searchCapLabor.getValue(columns[15]) +  ',' +
            searchCapLabor.getValue(columns[16]) +  ',' +
            searchCapLabor.getValue(columns[17]) +  ',' +
            searchCapLabor.getValue(columns[18]) +  ',' +
            searchCapLabor.getValue(columns[19]) +  ',' +
            searchCapLabor.getValue(columns[20]) +  ',' +
            searchCapLabor.getValue(columns[21]) +  ',' +
            searchCapLabor.getValue(columns[22]) +  ',' +
            searchCapLabor.getValue(columns[23]) +  ',' +
            searchCapLabor.getValue(columns[24]) +  ',' +
            searchCapLabor.getValue(columns[25]) +  ',' +
            searchCapLabor.getValue(columns[26]) +  ',' +
            searchCapLabor.getValue(columns[27]) +  ',' +
            searchCapLabor.getValue(columns[28]) +  ',' +
            searchCapLabor.getValue(columns[29]) +  ',' +
            searchCapLabor.getValue(columns[30]) +  ',' +
            searchCapLabor.getValue(columns[31]) +  ',' +
            searchCapLabor.getValue(columns[32]) +  ',' +
            searchCapLabor.getValue(columns[33]) +  ',' +
            searchCapLabor.getValue(columns[34]) +  ',' +
            searchCapLabor.getValue(columns[35]) +  ',' +
            searchCapLabor.getValue(columns[36]) +  ',' +
            searchCapLabor.getValue(columns[37]) +  ',' +
            searchCapLabor.getValue(columns[38]) +  ',' +
            searchCapLabor.getValue(columns[39]) +  ',' +
            searchCapLabor.getValue(columns[40]) +  ',' +
            searchCapLabor.getValue(columns[41]) +  ',' +
            searchCapLabor.getValue(columns[42]) +  ',' +
            searchCapLabor.getValue(columns[43]) +  ',' +
            searchCapLabor.getValue(columns[44]) +  ',' +
            searchCapLabor.getValue(columns[45]) +  ',' +
            searchCapLabor.getValue(columns[46]) +  ',' +
            searchCapLabor.getValue(columns[47]) +  ',' +
            searchCapLabor.getValue(columns[48]) +  ',' +
            searchCapLabor.getValue(columns[49]) +  ',' +
            searchCapLabor.getValue(columns[50]) +  ',' +
            searchCapLabor.getValue(columns[51]) +  ',' +
            searchCapLabor.getValue(columns[52]) +  ',' +
            searchCapLabor.getValue(columns[53]) +  ',' +
            searchCapLabor.getValue(columns[54]) +  ',' +
            searchCapLabor.getValue(columns[55]) +  ',' +
            searchCapLabor.getValue(columns[56]) +  ',' +
            searchCapLabor.getValue(columns[57]) +  ',' +
            searchCapLabor.getValue(columns[58]) +  ',' +
            searchCapLabor.getValue(columns[59]) +  ',' +
            searchCapLabor.getValue(columns[60]) +  ',' +
            searchCapLabor.getValue(columns[61]) +  ',' +
            searchCapLabor.getValue(columns[62]) +  ',' +
            searchCapLabor.getValue(columns[63]) +  ',' +
            searchCapLabor.getValue(columns[64]) +  ',' +
            searchCapLabor.getValue(columns[65]) +  ',' +
            searchCapLabor.getValue(columns[66]) +  ',' +
            searchCapLabor.getValue(columns[67]) +  ',' +
            searchCapLabor.getValue(columns[1]) +  '\n';
    }
    else{
        stPeriods += 'Cap Labor,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
    }

    var arrColumns = new Array();
    var arrFilters = new Array();
    //arrFilters.push(new nlobjSearchFilter('class',null,'noneof',3));
    arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projid));
    var searchActuals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFilters, arrColumns);

    if (searchActuals != null) {
        var searchActual = searchActuals[0];
        var columns = searchActual.getAllColumns();

        stPeriods += 'Total Actuals,' +
            searchActual.getValue(columns[2]) +  ',' +
            searchActual.getValue(columns[3]) +  ',' +
            searchActual.getValue(columns[4]) +  ',' +
            searchActual.getValue(columns[5]) +  ',' +
            searchActual.getValue(columns[6]) +  ',' +
            searchActual.getValue(columns[7]) +  ',' +
            searchActual.getValue(columns[8]) +  ',' +
            searchActual.getValue(columns[9]) +  ',' +
            searchActual.getValue(columns[10]) +  ',' +
            searchActual.getValue(columns[11]) +  ',' +
            searchActual.getValue(columns[12]) +  ',' +
            searchActual.getValue(columns[13]) +  ',' +
            searchActual.getValue(columns[14]) +  ',' +
            searchActual.getValue(columns[15]) +  ',' +
            searchActual.getValue(columns[16]) +  ',' +
            searchActual.getValue(columns[17]) +  ',' +
            searchActual.getValue(columns[18]) +  ',' +
            searchActual.getValue(columns[19]) +  ',' +
            searchActual.getValue(columns[20]) +  ',' +
            searchActual.getValue(columns[21]) +  ',' +
            searchActual.getValue(columns[22]) +  ',' +
            searchActual.getValue(columns[23]) +  ',' +
            searchActual.getValue(columns[24]) +  ',' +
            searchActual.getValue(columns[25]) +  ',' +
            searchActual.getValue(columns[26]) +  ',' +
            searchActual.getValue(columns[27]) +  ',' +
            searchActual.getValue(columns[28]) +  ',' +
            searchActual.getValue(columns[29]) +  ',' +
            searchActual.getValue(columns[30]) +  ',' +
            searchActual.getValue(columns[31]) +  ',' +
            searchActual.getValue(columns[32]) +  ',' +
            searchActual.getValue(columns[33]) +  ',' +
            searchActual.getValue(columns[34]) +  ',' +
            searchActual.getValue(columns[35]) +  ',' +
            searchActual.getValue(columns[36]) +  ',' +
            searchActual.getValue(columns[37]) +  ',' +
            searchActual.getValue(columns[38]) +  ',' +
            searchActual.getValue(columns[39]) +  ',' +
            searchActual.getValue(columns[40]) +  ',' +
            searchActual.getValue(columns[41]) +  ',' +
            searchActual.getValue(columns[42]) +  ',' +
            searchActual.getValue(columns[43]) +  ',' +
            searchActual.getValue(columns[44]) +  ',' +
            searchActual.getValue(columns[45]) +  ',' +
            searchActual.getValue(columns[46]) +  ',' +
            searchActual.getValue(columns[47]) +  ',' +
            searchActual.getValue(columns[48]) +  ',' +
            searchActual.getValue(columns[49]) +  ',' +
            searchActual.getValue(columns[50]) +  ',' +
            searchActual.getValue(columns[51]) +  ',' +
            searchActual.getValue(columns[52]) +  ',' +
            searchActual.getValue(columns[53]) +  ',' +
            searchActual.getValue(columns[54]) +  ',' +
            searchActual.getValue(columns[55]) +  ',' +
            searchActual.getValue(columns[56]) +  ',' +
            searchActual.getValue(columns[57]) +  ',' +
            searchActual.getValue(columns[58]) +  ',' +
            searchActual.getValue(columns[59]) +  ',' +
            searchActual.getValue(columns[60]) +  ',' +
            searchActual.getValue(columns[61]) +  ',' +
            searchActual.getValue(columns[62]) +  ',' +
            searchActual.getValue(columns[63]) +  ',' +
            searchActual.getValue(columns[64]) +  ',' +
            searchActual.getValue(columns[65]) +  ',' +
            searchActual.getValue(columns[66]) +  ',' +
            searchActual.getValue(columns[67]) +  ',' +
            searchActual.getValue(columns[1]) +  '\n';
    }
    else{
        stPeriods += 'Total Actuals,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
    }


    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('custbody_cologix_project_name',null,'is',projid));
    var searchPOs = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_pos', arrFilters, arrColumns);

    if (searchPOs != null) {
        var searchPO = searchPOs[0];
        var columns = searchPO.getAllColumns();

        stPeriods += 'Unbilled POs,' +
            searchPO.getValue(columns[4]) +  ',' +
            searchPO.getValue(columns[5]) +  ',' +
            searchPO.getValue(columns[6]) +  ',' +
            searchPO.getValue(columns[7]) +  ',' +
            searchPO.getValue(columns[8]) +  ',' +
            searchPO.getValue(columns[9]) +  ',' +
            searchPO.getValue(columns[10]) +  ',' +
            searchPO.getValue(columns[11]) +  ',' +
            searchPO.getValue(columns[12]) +  ',' +
            searchPO.getValue(columns[13]) +  ',' +
            searchPO.getValue(columns[14]) +  ',' +
            searchPO.getValue(columns[15]) +  ',' +
            searchPO.getValue(columns[16]) +  ',' +
            searchPO.getValue(columns[17]) +  ',' +
            searchPO.getValue(columns[18]) +  ',' +
            searchPO.getValue(columns[19]) +  ',' +
            searchPO.getValue(columns[20]) +  ',' +
            searchPO.getValue(columns[21]) +  ',' +
            searchPO.getValue(columns[22]) +  ',' +
            searchPO.getValue(columns[23]) +  ',' +
            searchPO.getValue(columns[24]) +  ',' +
            searchPO.getValue(columns[25]) +  ',' +
            searchPO.getValue(columns[26]) +  ',' +
            searchPO.getValue(columns[27]) +  ',' +
            searchPO.getValue(columns[28]) +  ',' +
            searchPO.getValue(columns[29]) +  ',' +
            searchPO.getValue(columns[30]) +  ',' +
            searchPO.getValue(columns[31]) +  ',' +
            searchPO.getValue(columns[32]) +  ',' +
            searchPO.getValue(columns[33]) +  ',' +
            searchPO.getValue(columns[34]) +  ',' +
            searchPO.getValue(columns[35]) +  ',' +
            searchPO.getValue(columns[36]) +  ',' +
            searchPO.getValue(columns[37]) +  ',' +
            searchPO.getValue(columns[38]) +  ',' +
            searchPO.getValue(columns[39]) +  ',' +
            searchPO.getValue(columns[40]) +  ',' +
            searchPO.getValue(columns[41]) +  ',' +
            searchPO.getValue(columns[42]) +  ',' +
            searchPO.getValue(columns[43]) +  ',' +
            searchPO.getValue(columns[44]) +  ',' +
            searchPO.getValue(columns[45]) +  ',' +
            searchPO.getValue(columns[46]) +  ',' +
            searchPO.getValue(columns[47]) +  ',' +
            searchPO.getValue(columns[48]) +  ',' +
            searchPO.getValue(columns[49]) +  ',' +
            searchPO.getValue(columns[50]) +  ',' +
            searchPO.getValue(columns[51]) +  ',' +
            searchPO.getValue(columns[52]) +  ',' +
            searchPO.getValue(columns[53]) +  ',' +
            searchPO.getValue(columns[54]) +  ',' +
            searchPO.getValue(columns[55]) +  ',' +
            searchPO.getValue(columns[56]) +  ',' +
            searchPO.getValue(columns[57]) +  ',' +
            searchPO.getValue(columns[58]) +  ',' +
            searchPO.getValue(columns[59]) +  ',' +
            searchPO.getValue(columns[60]) +  ',' +
            searchPO.getValue(columns[61]) +  ',' +
            searchPO.getValue(columns[62]) +  ',' +
            searchPO.getValue(columns[63]) +  ',' +
            searchPO.getValue(columns[64]) +  ',' +
            searchPO.getValue(columns[65]) +  ',' +
            searchPO.getValue(columns[66]) +  ',' +
            searchPO.getValue(columns[67]) +  ',' +
            searchPO.getValue(columns[68]) +  ',' +
            searchPO.getValue(columns[69]) +  ',' +
            searchPO.getValue(columns[2]) +  '\n';

        stPeriods += 'Billed POs,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,' + searchPO.getValue(columns[1]) +  '\n';
        stPeriods += 'Total POs,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,' + searchPO.getValue(columns[3]) +  '\n';

    }
    else{
        stPeriods += 'Unbilled POs,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
        stPeriods += 'Billed POs,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
        stPeriods += 'Total POs,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
    }

    if (searchActuals != null && searchPOs != null) {
        stPeriods += 'Total POs,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,' + (parseFloat(searchActual.getValue(columns[1])) +  parseFloat(searchPO.getValue(columns[2]))) +  '\n';
    }
    else{
        stPeriods += 'Committed Capital,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
    }

    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('custrecord_clgx_budget_project',null,'is',projectName));
    var searchBudgets = nlapiSearchRecord('customrecord_clgx_budget', 'customsearch_clgx_rep_cpx_budget', arrFilters, arrColumns);

    if (searchBudgets != null && searchPOs != null) {
        var searchBudget = searchBudgets[0];
        var columns = searchBudget.getAllColumns();

        stPeriods += 'Budget,' + searchPO.getValue(columns[2]) +  ',0,0,0,0,0,0,0,0,0,0,0,0,' + searchPO.getValue(columns[3]) +  ',0,0,0,0,0,0,0,0,0,0,0,0,' + searchPO.getValue(columns[4]) +  ',0,0,0,0,0,0,0,0,0,0,0,0,' + searchPO.getValue(columns[5])+  ',0,0,0,0,0,0,0,0,0,0,0,0,' + searchPO.getValue(columns[6]) + ',0,0,0,0,0,0,0,0,0,0,0,0,' + searchPO.getValue(columns[7]) + ',' + searchPO.getValue(columns[1]) +  '\n';
    }
    else{
        stPeriods += 'Budget,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,\n';
    }


    if (searchBudgets != null && searchActuals != null && searchPOs != null) {

        stPeriods += 'Committed Capital Variance,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,' + (parseFloat(searchBudget.getValue(columns[1])) - (parseFloat(searchActual.getValue(columns[1])) +  parseFloat(searchPO.getValue(columns[2])))) +  '\n';
    }
    else{
        stPeriods += 'Committed Capital Variance,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
    }

    if (searchActuals != null) {
        var searchActual = searchActuals[0];
        var columns = searchActual.getAllColumns();

        stPeriods += 'Forecasts,' +
            searchActual.getValue(columns[2]) +  ',' +
            searchActual.getValue(columns[3]) +  ',' +
            searchActual.getValue(columns[4]) +  ',' +
            searchActual.getValue(columns[5]) +  ',' +
            searchActual.getValue(columns[6]) +  ',' +
            searchActual.getValue(columns[7]) +  ',' +
            searchActual.getValue(columns[8]) +  ',' +
            searchActual.getValue(columns[9]) +  ',' +
            searchActual.getValue(columns[10]) +  ',' +
            searchActual.getValue(columns[11]) +  ',' +
            searchActual.getValue(columns[12]) +  ',' +
            searchActual.getValue(columns[13]) +  ',' +
            searchActual.getValue(columns[14]) +  ',' +
            searchActual.getValue(columns[15]) +  ',' +
            searchActual.getValue(columns[16]) +  ',' +
            searchActual.getValue(columns[17]) +  ',' +
            searchActual.getValue(columns[18]) +  ',' +
            searchActual.getValue(columns[19]) +  ',' +
            searchActual.getValue(columns[20]) +  ',' +
            searchActual.getValue(columns[21]) +  ',' +
            searchActual.getValue(columns[22]) +  ',' +
            searchActual.getValue(columns[23]) +  ',' +
            searchActual.getValue(columns[24]) +  ',' +
            searchActual.getValue(columns[25]) +  ',' +
            searchActual.getValue(columns[26]) +  ',' +
            searchActual.getValue(columns[27]) +  ',' +
            searchActual.getValue(columns[28]) +  ',' +
            searchActual.getValue(columns[29]) +  ',' +
            searchActual.getValue(columns[30]) +  ',' +
            searchActual.getValue(columns[31]) +  ',' +
            searchActual.getValue(columns[32]) +  ',' +
            searchActual.getValue(columns[33]) +  ',' +
            searchActual.getValue(columns[34]) +  ',' +
            searchActual.getValue(columns[35]) +  ',' +
            searchActual.getValue(columns[36]) +  ',' +
            searchActual.getValue(columns[37]) +  ',' +
            searchActual.getValue(columns[38]) +  ',' +
            searchActual.getValue(columns[39]) +  ',' +
            searchActual.getValue(columns[40]) +  ',' +
            searchActual.getValue(columns[41]) +  ',' +
            searchActual.getValue(columns[42]) +  ',' +
            searchActual.getValue(columns[43]) +  ',' +
            searchActual.getValue(columns[44]) +  ',' +
            searchActual.getValue(columns[45]) +  ',' +
            searchActual.getValue(columns[46]) +  ',' +
            searchActual.getValue(columns[47]) +  ',' +
            searchActual.getValue(columns[48]) +  ',' +
            searchActual.getValue(columns[49]) +  ',' +
            searchActual.getValue(columns[50]) +  ',' +
            searchActual.getValue(columns[51]) +  ',' +
            searchActual.getValue(columns[52]) +  ',' +
            searchActual.getValue(columns[53]) +  ',' +
            searchActual.getValue(columns[54]) +  ',' +
            searchActual.getValue(columns[55]) +  ',' +
            searchActual.getValue(columns[56]) +  ',' +
            searchActual.getValue(columns[57]) +  ',' +
            searchActual.getValue(columns[58]) +  ',' +
            searchActual.getValue(columns[59]) +  ',' +
            searchActual.getValue(columns[60]) +  ',' +
            searchActual.getValue(columns[61]) +  ',' +
            searchActual.getValue(columns[62]) +  ',' +
            searchActual.getValue(columns[63]) +  ',' +
            searchActual.getValue(columns[64]) +  ',' +
            searchActual.getValue(columns[65]) +  ',' +
            searchActual.getValue(columns[66]) +  ',' +
            searchActual.getValue(columns[67]) +  ',' +
            searchActual.getValue(columns[1]) +  '\n';
    }
    else{
        stPeriods += 'Forecasts,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n';
    }

    if (searchActuals != null && searchPOs != null) {
        stPeriods += 'Total Committed and Projected,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,' + (parseFloat(searchActual.getValue(columns[1])) + (parseFloat(searchActual.getValue(columns[1])) +  parseFloat(searchPO.getValue(columns[2])))) +  '\n';
    }

    if (searchBudgets != null && searchActuals != null && searchPOs != null) {
        stPeriods += 'Total Project Variance,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,' + (parseFloat(searchBudget.getValue(columns[1])) - (parseFloat(searchActual.getValue(columns[1])) + (parseFloat(searchActual.getValue(columns[1])) +  parseFloat(searchPO.getValue(columns[2]))))) +  '\n';
    }

    if (searchBudgets != null && searchActuals != null && searchPOs != null) {
        stPeriods += 'Budget Variance,' + (parseFloat(searchBudget.getValue(columns[2])) - parseFloat(searchActual.getValue(columns[2]))) +  ',0,0,0,0,0,0,0,0,0,0,0,0,' + (parseFloat(searchBudget.getValue(columns[3])) - parseFloat(searchActual.getValue(columns[15]))) +  ',0,0,0,0,0,0,0,0,0,0,0,0,' + (parseFloat(searchBudget.getValue(columns[4])) - parseFloat(searchActual.getValue(columns[28]))) +  ',0,0,0,0,0,0,0,0,0,0,0,0,' + (parseFloat(searchBudget.getValue(columns[5])) - parseFloat(searchActual.getValue(columns[41]))) +  ',0,0,0,0,0,0,0,0,0,0,0,0,' + (parseFloat(searchBudget.getValue(columns[6])) - parseFloat(searchActual.getValue(columns[54]))) +   ',0,0,0,0,0,0,0,0,0,0,0,0,' + (parseFloat(searchBudget.getValue(columns[7])) - parseFloat(searchActual.getValue(columns[67]))) + ',' + (parseFloat(searchBudget.getValue(columns[1])) - parseFloat(searchActual.getValue(columns[1]))) +  '\n';
    }

    return stPeriods;
}

function cleanStr (str){
    var string = '';
    string = str.replace(/\'/g," ");
    string = string.replace(/\"/g," ");
    return string;
}

