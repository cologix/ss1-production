nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Customer.js
//	Script Name:	CLGX_SU_Customer
//	Script Id:		customscript_clgx_su_customer
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Customer
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		2/28/2012
//	Includes:		CLGX_RL_NAC_Library.js
//-------------------------------------------------------------------------------------------------

function beforeLoad(type, form) {
    try {

        var start = moment();
        var body = '| ';
        
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 09/13/2012
// Details:	Add a sub tab to the customer form and display organizational chart
//-----------------------------------------------------------------------------------------------------------------

        var currentContext = nlapiGetContext();
        var userid = nlapiGetUser();
        var roleid = nlapiGetRole();
        var customerid = nlapiGetRecordId();

        if( (currentContext.getExecutionContext() == 'userinterface') && (type == 'edit' || type == 'view')){
            /*
             var customerID = nlapiGetRecordId();
             var orgChartHTML = '';
             var orgChartField = form.getField('custentity_clgx_customer_org_chart');
             orgChartField.setLayoutType('outside','startcol');

             var arrColumns = new Array();
             var arrFilters = new Array();
             arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
             arrColumns[1] = new nlobjSearchColumn('entityid', null, null);
             arrColumns[2] = new nlobjSearchColumn('contactrole', null, null);
             arrColumns[3] = new nlobjSearchColumn('supervisor', null, null);
             arrColumns[4] = new nlobjSearchColumn('email', null, null);
             //arrFilters[0] = new nlobjSearchFilter('company',null,'is',customerID);
             arrFilters[0] = new nlobjSearchFilter('internalid','customer','is',customerID);
             var searchContacts = nlapiSearchRecord('contact', null, arrFilters, arrColumns);

             orgChartHTML += '<style type="text/css">div.title {color:navy; font-style:italic;text-decoration: none;} a.chart{text-decoration: none;color:darkred;}</style>';
             orgChartHTML += '<script type="text/javascript" src="https://www.google.com/jsapi"></script>';
             orgChartHTML += '<script type="text/javascript">';
             orgChartHTML += '      google.load("visualization", "1", {packages:["orgchart"]});';
             orgChartHTML += '      google.setOnLoadCallback(drawChart);';
             orgChartHTML += '      function drawChart() {';
             orgChartHTML += '        var data = new google.visualization.DataTable();';
             orgChartHTML += '        data.addColumn("string", "Name");';
             orgChartHTML += '        data.addColumn("string", "Manager");';
             orgChartHTML += '        data.addColumn("string", "ToolTip");';
             orgChartHTML += '        data.addRows([';


             for(var i=0; searchContacts != null && i<searchContacts.length; i++){
             var stID = searchContacts[i].getValue('internalid',null,null);
             var stName = searchContacts[i].getValue('entityid',null,null);
             var stEmail = searchContacts[i].getValue('email',null,null);
             var stRole = searchContacts[i].getText('contactrole',null,null);
             var stSuperviser = searchContacts[i].getText('supervisor',null,null);

             orgChartHTML += '          [{v:"' + stName + '", f:"<a class=chart href=/app/common/entity/contact.nl?id=' + stID + '&e=T target=_blank>' + stName + '</a><div class=title>' + stRole + '</br><a class=chart href=mailto:' + stEmail + ' target=_blank>' + stEmail + '</a></div>"}, "' + stSuperviser + '", "' + stRole + '"],';
             }

             //<a class="report" href="/app/common/entity/custjob.nl?id=' + stID + '" target="_blank">' + stTitle + '</a>

             orgChartHTML += '        ]);';
             orgChartHTML += '        var chart = new google.visualization.OrgChart(document.getElementById("chart_div"));';
             orgChartHTML += '        chart.draw(data, {allowHtml:true, allowCollapse: true, size:"medium"});';
             orgChartHTML += '      }';
             orgChartHTML += '</script>';

             //html += '<div style="width:1320px;height:700px;overflow:auto;">';
             orgChartHTML += '<div id="chart_div"></div>';
             //html += '</div>';

             orgChartField.setDefaultValue(orgChartHTML);
             */
        }
//----------------------------------------------------------------------------------------------------------

        if (currentContext.getExecutionContext() == 'userinterface'){
            var cpTab = form.addTab('custpage_tab_cp_users', 'CP Users');
            var cpField = form.addField('custpage_cp_users','inlinehtml', null, null, 'custpage_tab_cp_users');
            cpField.setDefaultValue('<iframe name="_cp_rights" id="cp_rights" src="/app/site/hosting/scriptlet.nl?script=710&deploy=1&act=edit&id=' + customerid + '" height="480px" width="1030px" frameborder="0" scrolling="no"></iframe>');
        }
        
//----------------------------------------------------------------------------------------------------------
        /*
         var customerid = nlapiGetRecordId();
         var recCompany = nlapiLoadRecord('customer', customerid);
         var dBalance = recCompany.getFieldValue('custentity_clgx_customer_balance');
         var bCC = recCompany.getFieldValue('custentity_clgx_cc_enabled');
         var roleid = currentContext.getRole();

         if( (currentContext.getExecutionContext() == 'userinterface')  && (type == 'view') && (roleid == 18 || roleid == 1011 || roleid == 1061)){

         if (dBalance > 0 && bCC == 'T') {
         form.setScript('customscript_clgx_lib_creditcards');
         form.addButton('custpage_add_createlead_button', 'Charge Credit Card', 'UserEvent_ChargeCard()');
         }
         }
         */
        
        body += '01 - ' + moment().diff(start) + ' | ';
        
        var record = nlapiCreateRecord('customrecord_clgx_script_exec_time');
        record.setFieldValue('custrecord_clgx_script_exec_time_user', nlapiGetUser());
        record.setFieldValue('custrecord_clgx_script_exec_time_rec', "customer");
        record.setFieldValue('custrecord_clgx_script_exec_time_id', nlapiGetRecordId());
        record.setFieldValue('custrecord_clgx_script_exec_time_context', 1);
        record.setFieldValue('custrecord_clgx_script_exec_time', moment().diff(start));
        record.setFieldValue('custrecord_clgx_script_exec_time_details', body);
        try {
            nlapiSubmitRecord(record, false, true);
        }
        catch (error) {
        }
        
        
    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function beforeSubmit (type) {
	

    var start = moment();
    var body = '';
    
    
    try {

        var currentContext = nlapiGetContext();
        var userid = nlapiGetUser();
        var roleid = currentContext.getRole();
        var customerid = nlapiGetRecordId();

//------------- Begin Section 2 -----------------------------------------------------------------------------------
// Version:	1.1 - 4/30/2013
// Details:	Empty email and phone form customer header
//-----------------------------------------------------------------------------------------------------------------

        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'create' || type == 'edit')) {
            nlapiSetFieldValue('email', '');
            nlapiSetFieldValue('phone', '');
        }

//------------- Begin Section 3 -----------------------------------------------------------------------------------
// Created:	04/06/2015
// Details:	Queue contacts/customer to syncronize with SilverPOP
//-----------------------------------------------------------------------------------------------------------------
        /*
         if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'delete')) {

         var arrColumns = new Array();
         var arrFilters = new Array();
         arrFilters.push(new nlobjSearchFilter("internalid","company","anyof",customerid));
         var searchContacts = nlapiSearchRecord('contact', 'customsearch_clgx_sp_sync_contacts_ids', arrFilters, arrColumns);

         for ( var i = 0; searchContacts != null && i < searchContacts.length; i++ ) {

         var contactid = searchContact[i].getValue('internalid',null,null);

         var arrColumns = new Array();
         arrColumns.push(new nlobjSearchColumn('internalid',null,null));
         var arrFilters = new Array();
         arrFilters.push(new nlobjSearchFilter("custrecord_clgx_contact_to_sp_contact",null,"equalto",contactid));
         arrFilters.push(new nlobjSearchFilter('custrecord_clgx_contact_to_sp_done',null,'is','F'));
         var searchQueue = nlapiSearchRecord('customrecord_clgx_queue_contacts_to_sp', null, arrFilters, arrColumns);

         if(searchQueue == null){
         var record = nlapiCreateRecord('customrecord_clgx_queue_contacts_to_sp');
         record.setFieldValue('custrecord_clgx_contact_to_sp_contact', contactid);
         record.setFieldValue('custrecord_clgx_contact_to_sp_action', 2);
         record.setFieldValue('custrecord_clgx_contact_to_sp_done', 'F');
         var idRec = nlapiSubmitRecord(record, false,true);
         }
         else{
         nlapiSubmitField('customrecord_clgx_queue_contacts_to_sp', searchQueue[0].getValue('internalid',null,null), 'custrecord_clgx_contact_to_sp_action', 2);
         }
         }
         }
         */
//---------- End Section 1 ------------------------------------------------------------------------------------------------

        body += '01 - ' + moment().diff(start) + ' | ';
        
        var record = nlapiCreateRecord('customrecord_clgx_script_exec_time');
        record.setFieldValue('custrecord_clgx_script_exec_time_user', nlapiGetUser());
        record.setFieldValue('custrecord_clgx_script_exec_time_rec', "customer");
        record.setFieldValue('custrecord_clgx_script_exec_time_id', nlapiGetRecordId());
        record.setFieldValue('custrecord_clgx_script_exec_time_context', 2);
        record.setFieldValue('custrecord_clgx_script_exec_time', moment().diff(start));
        record.setFieldValue('custrecord_clgx_script_exec_time_details', body);
        try {
            nlapiSubmitRecord(record, false, true);
        }
        catch (error) {
        }
        
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function afterSubmit (type) {

    var start = moment();
    var body = '';
    
    try {

        var currentContext = nlapiGetContext();
        var userid = nlapiGetUser();
        var roleid = currentContext.getRole();
        var customerid = nlapiGetRecordId();
        var inactive = nlapiGetFieldValue('isinactive');

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.1 - 7/12/2012
// Details:	Verify if there is a billing contact for the customer. If yes, modify the custom flag 'Has Billing Contact'.
//-----------------------------------------------------------------------------------------------------------------

        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'create' || type == 'edit')) {

            // search all billing contacts of this customer
            var arrColumns = new Array();
            var arrFilters = new Array();
            arrColumns[0] = new nlobjSearchColumn('internalid', 'contact', null);
            arrFilters[0] = new nlobjSearchFilter('internalid',null,'is',customerid);
            arrFilters[1] = new nlobjSearchFilter('contactrole','contact','is',1);
            var searchResults = nlapiSearchRecord('customer', null, arrFilters, arrColumns);

            if (searchResults != null) { // no billing contact
                nlapiSubmitField('customer',customerid,['custentity_clgx_has_billing_contact'],['T']);
            }
            else{
                nlapiSubmitField('customer',customerid,['custentity_clgx_has_billing_contact'],['F']);
            }
        }
        body += '01 - ' + moment().diff(start) + ' | ';

//------------- Begin Section 2 -----------------------------------------------------------------------------------
// Created:	01/27/2016
// Details:	Syncronize with NAC
//-----------------------------------------------------------------------------------------------------------------

        var environment = currentContext.getEnvironment();
        if (environment == 'PRODUCTION' && (type == 'xedit'  || currentContext.getExecutionContext() == 'userinterface')) {
            var customerid = nlapiGetRecordId();
            var userid = nlapiGetUser();
            var user = nlapiLookupField('employee', userid, 'entityid');
            var row = nlapiLoadRecord('customer', customerid);

            var matrix = row.getFieldValue('custentity_clgx_matrix_entity_id');


            var arrColumns = new Array();
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
            var searchSOs = nlapiSearchRecord('salesorder', 'customsearch_clgx_nj_sos_check', arrFilters, arrColumns);

            if(searchSOs != null || matrix != null){
                var ret = json_serialize(row)
                ret = append_attached_contacts (ret, customerid);

                var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
                record.setFieldValue('custrecord_clgx_queue_ping_record_id', customerid);
                record.setFieldValue('custrecord_clgx_queue_ping_record_type', 1);
                record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
                var idRec = nlapiSubmitRecord(record, false,true);
                
                /*
                try {
                    flexapi('POST','/netsuite/update', {
                        'type': 'customer',
                        'id': customerid,
                        'action': type,
                        'userid': userid,
                        'user': user,
                        'error': false,
                        'errorcode': '',
                        'errordetails': '',
                        'data': ret
                    });
                    nlapiLogExecution('DEBUG', 'flexapi request: ', customerid);
                }
                catch (error) {
                    var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
                    record.setFieldValue('custrecord_clgx_queue_ping_record_id', customerid);
                    record.setFieldValue('custrecord_clgx_queue_ping_record_type', 1);
                    record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
                    var idRec = nlapiSubmitRecord(record, false,true);
                }
                */
            }
        }
        body += '02 - ' + moment().diff(start) + ' | ';
        
//------------- Begin Section 3 -----------------------------------------------------------------------------------
// Created:	04/06/2015
// Details:	Queue contacts/customer to syncronize with SilverPOP
//-----------------------------------------------------------------------------------------------------------------
        /*
         //if (type == 'xedit' || type == 'delete' || (currentContext.getExecutionContext() == 'userinterface' && type == 'edit')) {

         var action = 1;
         if(type == 'create'){
         action = 0;
         }
         if(type == 'delete'){
         action = 2;
         }
         if(inactive == 'T'){
         action = 3;
         }

         var arrColumns = new Array();
         var arrFilters = new Array();
         arrFilters.push(new nlobjSearchFilter("internalid","company","anyof",customerid));
         var searchContacts = nlapiSearchRecord('contact', 'customsearch_clgx_sp_sync_contacts_ids', arrFilters, arrColumns);

         for ( var i = 0; searchContacts != null && i < searchContacts.length; i++ ) {

         var contactid = searchContacts[i].getValue('internalid',null,null);

         var arrColumns = new Array();
         arrColumns.push(new nlobjSearchColumn('internalid',null,null));
         var arrFilters = new Array();
         arrFilters.push(new nlobjSearchFilter("custrecord_clgx_contact_to_sp_contact",null,"equalto",contactid));
         arrFilters.push(new nlobjSearchFilter('custrecord_clgx_contact_to_sp_done',null,'is','F'));
         var searchQueue = nlapiSearchRecord('customrecord_clgx_queue_contacts_to_sp', null, arrFilters, arrColumns);

         if(searchQueue == null){
         var record = nlapiCreateRecord('customrecord_clgx_queue_contacts_to_sp');
         record.setFieldValue('custrecord_clgx_contact_to_sp_contact', contactid);
         record.setFieldValue('custrecord_clgx_contact_to_sp_action', action);
         record.setFieldValue('custrecord_clgx_contact_to_sp_done', 'F');
         var idRec = nlapiSubmitRecord(record, false,true);
         }
         else{ // if in queue change status in case it was different
         nlapiSubmitField('customrecord_clgx_queue_contacts_to_sp', searchQueue[0].getValue('internalid',null,null), 'custrecord_clgx_contact_to_sp_action', 1);
         }
         }

         //}
         */

//------------- Begin Section 4 -----------------------------------------------------------------------------------
//Create a Customer Profile in Authorize-Catalina Taran -01Feb 2017
//-----------------------------------------------------------------------------------------------------------------

        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'create' || type == 'edit')) {

            var ccEnabled = nlapiGetFieldValue('custentity_clgx_cc_enabled');
            var customerREC= nlapiLoadRecord('customer',customerid);
            var customer=customerREC.getFieldValue('entityid');
            var authid=customerREC.getFieldValue('custentity_clgx_customer_authorize_id');
            var subsidiary=nlapiGetFieldValue('subsidiary');
            var ccPaused = nlapiGetFieldValue('custentity_clgx_cc_paused');
            nlapiLogExecution('DEBUG','customer1', customer);
            // nlapiLogExecution('DEBUG','authid', authid);

            if (ccEnabled == 'T' && ccPaused=='F' && (authid = '' || authid == null)) {
                var resp = create_customer_profile( customerid, customer,subsidiary);
                // nlapiLogExecution('DEBUG','customer1', customer);
                // nlapiLogExecution('DEBUG','customer2', customerid);
                // nlapiLogExecution('DEBUG','customer3', subsidiary);
                var authid = 0;

                //nlapiLogExecution('DEBUG','resp.messages.resultCode', resp.messages.resultCode);
                if (resp.messages.resultCode == 'Ok') {
                    authid = resp.customerProfileId;
                    nlapiSubmitField('customer', customerid, 'custentity_clgx_customer_authorize_id', authid);
                } else {
                    // ??
                }
            }
        }
        
        body += '03 - ' + moment().diff(start) + ' | ';
        
//------------- Begin Section 5 -----------------------------------------------------------------------------------
//Inactivate the Portal Users 23-March-2017 C Taran
//-----------------------------------------------------------------------------------------------------------------

        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'create' || type == 'edit')) {

            var entitystatus = nlapiGetFieldValue('entitystatus');

            if (inactive== 'T' || entitystatus==16) {



                var filters = new Array();
                var columns = new Array();
                filters.push(new nlobjSearchFilter("company", "custentity_clgx_modifier", "anyof", customerid));
                var rows = nlapiSearchRecord('customrecord_clgx_modifier', 'customsearch4000', filters, columns);
                if(rows.length>0) {

                    for (var i = 0; rows != null && i < rows.length; i++) {
                        // PUR
                        var row = rows[i];
                        var columns = row.getAllColumns();
                        var id = row.getValue(columns[1]);

                        var filters1 = new Array();
                        var columns1 = new Array();
                        filters1.push(new nlobjSearchFilter("internalid", null, "anyof", id));
                        var rows1 = nlapiSearchRecord('customrecord_clgx_modifier', 'customsearch4000', filters1, columns1);
                        var row1 = rows1[0];
                        var columns1 = row1.getAllColumns();
                        var noCompanies = row1.getValue(columns1[0]);
                        if(noCompanies==1) {
                            nlapiSubmitField('customrecord_clgx_modifier', id, ['isinactive'], 'T');
                        }



                    }
                }

            }
        }
        
        body += '04 - ' + moment().diff(start) + ' | ';
        
//---------- End Sections ------------------------------------------------------------------------------------------------

        
        var record = nlapiCreateRecord('customrecord_clgx_script_exec_time');
        record.setFieldValue('custrecord_clgx_script_exec_time_user', nlapiGetUser());
        record.setFieldValue('custrecord_clgx_script_exec_time_rec', "customer");
        record.setFieldValue('custrecord_clgx_script_exec_time_id', nlapiGetRecordId());
        record.setFieldValue('custrecord_clgx_script_exec_time_context', 3);
        record.setFieldValue('custrecord_clgx_script_exec_time', moment().diff(start));
        record.setFieldValue('custrecord_clgx_script_exec_time_details', body);
        try {
            nlapiSubmitRecord(record, false, true);
        }
        catch (error) {
        }
        
        
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function autorize_net_request (data){

    var req = nlapiRequestURL('https://api.authorize.net/xml/v1/request.api',JSON.stringify(data),{'Content-type': 'application/json'},null,'POST');
    var str = req.body;
    var resp = JSON.parse(str.slice(1));
    return resp;
}


function create_customer_profile (customerid, customer,subsidiary){

    if(subsidiary>0) {
        // define search filters
        var filters = new Array();

        filters[0] = new nlobjSearchFilter('custrecord_clgx_auth_net_subsidiary', null, 'anyof', subsidiary);
        filters[1] = new nlobjSearchFilter('custrecord_clgx_auth_net_type', null, 'anyof', 2);

// return keys
        var columns = new Array();
        columns[0] = new nlobjSearchColumn('custrecord_clgx_auth_net_api_id');
        columns[1] = new nlobjSearchColumn('custrecord_clgx_auth_net_client_key');
        columns[2] = new nlobjSearchColumn('custrecord_clgx_auth_net_transaction_key');

// execute the customrecord_clgx_authorize_net search, passing all filters and return columns
        var searchresults = nlapiSearchRecord('customrecord_clgx_authorize_net', null, filters, columns);

// loop through the results
        for (var i = 0; searchresults != null && i < searchresults.length; i++) {
            // get result values
            var searchresult = searchresults[i];
            var name = searchresult.getValue('custrecord_clgx_auth_net_api_id');
            var key = searchresult.getValue('custrecord_clgx_auth_net_client_key');
            var keyT = searchresult.getValue('custrecord_clgx_auth_net_transaction_key');
        }

//         nlapiLogExecution('DEBUG','name', name);
        //  nlapiLogExecution('DEBUG','keyT', keyT);

        var data = {
            "createCustomerProfileRequest": {
                "merchantAuthentication": {
                    "name": name,
                    "transactionKey": keyT
                },
                "profile": {
                    "merchantCustomerId": customerid,
                    "description": customer
                }
            }

        };

        var resp = autorize_net_request(data);

        return resp;

    } else {
        return true;
    }

}

