nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CHARTS_Maintenance_Details.js
//	Script Name:	CLGX_SL_CHARTS_Maintenance_Details
//	Script Id:		customscript_clgx_sl_chrt_maintenance_details
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		06/03/2015
//-------------------------------------------------------------------------------------------------

function suitelet_charts_maintenance_details (request, response){
    try {
        var market = request.getParameter('market');
        var date = request.getParameter('date');
        var type = request.getParameter('type');
        var mark=request.getParameter('mark');
        var objFile = nlapiLoadFile(2764926);
        var html = objFile.getValue();
        var title='';
        // if(market!=0)
        // {
        // var facilityRecord=nlapiLoadRecord('customrecord_cologix_facility',market);
        if(type==0)
        {
            var titlestatus='Approved';
        }
        if(type==1)
        {
            var titlestatus='Pending';
        }
        if(type==2)
        {
            var titlestatus='Pending';
        }
        if(type==3)
        {
            var titlestatus='Closed';
        }
        if(type==4)
        {
            var titlestatus='In Progress';
        }

        if(type==5)
        {
            var titlestatus='Resolved';
        }
        if(type==6)
        {
            var titlestatus='Not Started';
        }

        html = html.replace(new RegExp('{titlestatus}','g'), titlestatus);
        html = html.replace(new RegExp('{titledate}','g'), date);
        // }

        html = html.replace(new RegExp('{maintenanceJSON}','g'), dataMaintenance( type, market, date,mark));


        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function dataMaintenance (type, market, date,mark) {
    var series='var dataMaintenance =  [';
    var arrFilters = new Array();
    if(market!=0)
    {
        arrFilters.push(new nlobjSearchFilter("custevent_cologix_facility",null,"anyof",market));
    }
    if(mark!=0)
    {
        var facArray=getFacilityByMarket(mark);
        arrFilters.push(new nlobjSearchFilter("custevent_cologix_facility",null,"anyof",facArray));
    }
    arrFilters.push(new nlobjSearchFilter("custevent_cologix_case_sched_followup",null,"on",date));
    arrFilters.push(new nlobjSearchFilter("custevent_cologix_sub_case_type",null,"noneof",86));
    var arrColumns = new Array();
    if(type==0)
    {
        var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_3', arrFilters, arrColumns);
        for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
            var searchCase = searchCases[i];
            var columns = searchCase.getAllColumns();
            var number = searchCase.getValue(columns[0]);
            var follow = searchCase.getValue(columns[1]);
            var id = searchCase.getValue(columns[2]);
            var customer = searchCase.getValue(columns[3]);
            var customerName = searchCase.getText(columns[3]);
            var fac = searchCase.getText(columns[4]);
            var status = searchCase.getText(columns[5]);
            var time = searchCase.getValue(columns[6]);
            if((time==null)||(time==''))
            {
                time='';
            }
            follow = follow.replace("/","");
            follow = follow.replace(/\\/, "");
            follow = follow.replace(/"/g, '\\"');
            series += '\n{"number":"' + number  +
                '","time":"' + time+
                '","internalid":' +  id +
                ',"subject":"' + follow +
                '","customer":"' +  customerName +
                '","facility":"' + fac +
                '","status":"' + status +
                '"},';
        }
    }
    if((type==1)||(type==2))
    {
        var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_3_2', arrFilters, arrColumns);
        for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
            var searchCase = searchCases[i];
            var columns = searchCase.getAllColumns();
            var number = searchCase.getValue(columns[0]);
            var follow = searchCase.getValue(columns[1]);
            var id = searchCase.getValue(columns[2]);
            var customer = searchCase.getValue(columns[3]);
            var customerName = searchCase.getText(columns[3]);
            var fac = searchCase.getText(columns[4]);
            var status = searchCase.getText(columns[5]);
            var time = searchCase.getValue(columns[6]);
            if((time==null)||(time==''))
            {
                time='';
            }
            follow = follow.replace("/","");
            follow = follow.replace(/\\/, "");
            follow = follow.replace(/"/g, '\\"');
            series += '\n{"number":"' + number  +
                '","time":"' + time +
                '","internalid":' +  id +
                ',"subject":"' + follow +
                '","customer":"' +  customerName +
                '","facility":"' + fac +
                '","status":"' + status +
                '"},';
        }
    }
    if(type==3)
    {
        var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_3_2_2', arrFilters, arrColumns);
        for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
            var searchCase = searchCases[i];
            var columns = searchCase.getAllColumns();
            var number = searchCase.getValue(columns[0]);
            var follow = searchCase.getValue(columns[1]);
            var id = searchCase.getValue(columns[2]);
            var customer = searchCase.getValue(columns[3]);
            var customerName = searchCase.getText(columns[3]);
            var fac = searchCase.getText(columns[4]);
            var status = searchCase.getText(columns[5]);
            var time = searchCase.getValue(columns[6]);
            if((time==null)||(time==''))
            {
                time='';
            }
            follow = follow.replace("/","");
            follow = follow.replace(/\\/, "");
            follow = follow.replace(/"/g, '\\"');
            series += '\n{"number":"' + number  +
                '","time":"' + time +
                '","internalid":' +  id +
                ',"subject":"' + follow +
                '","customer":"' +  customerName +
                '","facility":"' + fac +
                '","status":"' + status +
                '"},';
        }
    }
    if(type==4)
    {
        var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_3_2_2_2', arrFilters, arrColumns);
        for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
            var searchCase = searchCases[i];
            var columns = searchCase.getAllColumns();
            var number = searchCase.getValue(columns[0]);
            var follow = searchCase.getValue(columns[1]);
            var id = searchCase.getValue(columns[2]);
            var customer = searchCase.getValue(columns[3]);
            var customerName = searchCase.getText(columns[3]);
            var fac = searchCase.getText(columns[4]);
            var status = searchCase.getText(columns[5]);
            var time = searchCase.getValue(columns[6]);
            if((time==null)||(time==''))
            {
                time='';
            }
            follow = follow.replace("/","");
            follow = follow.replace(/\\/, "");
            follow = follow.replace(/"/g, '\\"');
            series += '\n{"number":"' + number  +
                '","time":"' + time +
                '","internalid":' +  id +
                ',"subject":"' + follow +
                '","customer":"' +  customerName +
                '","facility":"' + fac +
                '","status":"' + status +
                '"},';
        }
    }

    if(type==5)
    {
        var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_3_2_2_3', arrFilters, arrColumns);
        for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
            var searchCase = searchCases[i];
            var columns = searchCase.getAllColumns();
            var number = searchCase.getValue(columns[0]);
            var follow = searchCase.getValue(columns[1]);
            var id = searchCase.getValue(columns[2]);
            var customer = searchCase.getValue(columns[3]);
            var customerName = searchCase.getText(columns[3]);
            var fac = searchCase.getText(columns[4]);
            var status = searchCase.getText(columns[5]);
            var time = searchCase.getValue(columns[6]);
            if((time==null)||(time==''))
            {
                time='';
            }
            follow = follow.replace("/","");
            follow = follow.replace(/\\/, "");
            follow = follow.replace(/"/g, '\\"');
            series += '\n{"number":"' + number  +
                '","time":"' + time +
                '","internalid":' +  id +
                ',"subject":"' + follow +
                '","customer":"' +  customerName +
                '","facility":"' + fac +
                '","status":"' + status +
                '"},';
        }
    }
    if(type==6)
    {
        var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_3_2_2__2', arrFilters, arrColumns);
        for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
            var searchCase = searchCases[i];
            var columns = searchCase.getAllColumns();
            var number = searchCase.getValue(columns[0]);
            var follow = searchCase.getValue(columns[1]);
            var id = searchCase.getValue(columns[2]);
            var customer = searchCase.getValue(columns[3]);
            var customerName = searchCase.getText(columns[3]);
            var fac = searchCase.getText(columns[4]);
            var status = searchCase.getText(columns[5]);
            var time = searchCase.getValue(columns[6]);
            if((time==null)||(time==''))
            {
                time='';
            }
            follow = follow.replace("/","");
            follow = follow.replace(/\\/, "");
            follow = follow.replace(/"/g, '\\"');
            series += '\n{"number":"' + number  +
                '","time":"' + time +
                '","internalid":' +  id +
                ',"subject":"' + follow +
                '","customer":"' +  customerName +
                '","facility":"' + fac +
                '","status":"' + status +
                '"},';
        }
    }


    var strLen = series.length;
    if (searchCases != null){
        series = series.slice(0,strLen-1);
    }
    series += '];';
    return series;



}
function getFacilityByMarket(market){
    var facility=new Array();
    switch(parseInt(market)) {
        case 2: // Dallas
            facility=[3, 18, 44];
            break;
        case 8: // COL
            facility=[24, 29];
            break;
        case 4: // JAK
            facility=[21, 27];
            break;
        case 9: // LAK
            facility=[28];
            break;
        case 3: // MIN
            facility=[17,25];
            break;
        case 6: // MTL
            facility=[2,5,4,9,6,7,19];

            break;
        case 10: // PAR
            facility=[34,33,35,36];

            break;
        case 5: // TOR
            facility=[23,8,15];
            break;
        case 7: // VAN
            facility=[14,20,42];

            break;
        default:
            facility = [];

    }
    return facility;
}
