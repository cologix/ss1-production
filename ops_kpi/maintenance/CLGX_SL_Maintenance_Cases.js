nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Maintenance_Cases.js
//	Script Name:	CLGX_SL_Maintenance_Cases
//	Script Id:		customscript_clgx_sl_maintenance_cases
// 	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//-------------------------------------------------------------------------------------------------

function suitelet_charts_maintenance_chart(request, response){
    try {

        var facilityid = request.getParameter('location');
        var marketid = request.getParameter('market');
        var title = request.getParameter('title');
        if(facilityid!= '' ){
            var objFile = nlapiLoadFile(2764925);
            var html = objFile.getValue();
            html = html.replace(new RegExp('{facility}','g'),facilityid);
            html = html.replace(new RegExp('{seriesgreen}','g'),returnCasesCount(marketid,facilityid ,0));
            html = html.replace(new RegExp('{seriesgred}','g'),returnCasesCount(marketid,facilityid ,2));
            html = html.replace(new RegExp('{seriesgyellow}','g'),returnCasesCount(marketid,facilityid ,1));
            html = html.replace(new RegExp('{seriesblue}','g'),returnCasesCount(marketid,facilityid ,3));
            html = html.replace(new RegExp('{seriesgrey}','g'),returnCasesCount(marketid,facilityid ,4));
            html = html.replace(new RegExp('{seriespurple}','g'),returnCasesCount(marketid,facilityid ,5));
            html = html.replace(new RegExp('{seriesbrown}','g'),returnCasesCount(marketid,facilityid ,6));
            html = html.replace(new RegExp('{seriesblocked}','g'),returnCasesBlocked(title,facilityid,marketid));
            var userid = nlapiGetUser();
            var roleid = nlapiGetContext().getRole();
            //  nlapiSendEmail(206211, 206211,'role','body:'+roleid,null,null,null,null); // Send email to Catalina
            var button='';
            if (roleid == -5 || roleid == 3 || roleid == 18 || roleid == 1033 || roleid==1039 || roleid==1004)
            {
                var button='<button  class="btn btn-primary btn-lg btn-block" id="create-user" type="button" data-toggle="modal" data-target="#createEventModal">Add Moratorium</button>';

            }
            html = html.replace(new RegExp('{button}','g'),button);



        }
        else{
            var html = 'Please select a report from the left panel.';
        }
        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
function returnCasesBlocked(title,facility,market)
{
    var series='';
    var titleIn=title;
    if(title!='')
    {
        var arrFilters = new Array();
        var titleArr=getFacilityByTitle(title);
        if((facility!=0)&&(market==0))
        {
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_mor_facility",null,"anyof",facility));
        }
        if((facility==0)&&(market!=0))
        {
            var facilityArray=getFacilityByMarket(market);
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_mor_facility",null,"anyof",facilityArray));
        }

        var arrColumns = new Array();
        var arrFac = new Array();

        var searchCases = nlapiSearchRecord('customrecord_clgx_moratorium','customsearch_clgx_moratorium_ops_search', arrFilters, arrColumns);
        for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {

                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();

                var sdate = searchCase.getValue(columns[0]);
                var edate = searchCase.getValue(columns[1]);
                var islastDay=isLastDay(edate);
                var title = searchCase.getValue(columns[2]);
                var fac = searchCase.getValue(columns[2]);
                arrFac.push(fac);
                var datesplitS = sdate.split('/');
                var datesplitE = edate.split('/');
                if (datesplitS[0] < 10) {
                    var monthS = '0' + datesplitS[0];
                }
                else {
                    var monthS = datesplitS[0];
                }
                if (datesplitS[1] < 10) {
                    var dayS = '0' + datesplitS[1];
                }
                else {
                    var dayS = datesplitS[1];
                }
                var datestring = datesplitS[2] + '-' + monthS + '-' + dayS;

                if (datesplitE[0] < 10) {
                    var monthE = '0' + datesplitE[0];
                }
                else {
                    var monthE = datesplitE[0];
                }
                if(!islastDay) {
                    datesplitE[1] = parseInt(datesplitE[1]) + parseInt(1);
                }
                if (datesplitE[1] < 10) {
                    var dayE = '0' + datesplitE[1];
                }
                else {
                    var dayE = datesplitE[1];
                }
                if(!islastDay) {
                    var datestringF = datesplitE[2] + '-' + monthE + '-' + dayE;
                 }
               else{
                    if((parseInt(monthE)+parseInt(1))<10)
                    {
                        var mth='0'+(parseInt(monthE)+parseInt(1));
                    }
                    else
                    {
                        var mth=(parseInt(monthE)+parseInt(1));
                    }
                    var datestringF = datesplitE[2] + '-' + mth + '-01';
                }

                series += "{ start: '" + datestring + "',end:'" + datestringF + "',color:'#A8A8A8',overlap: true,rendering: 'background'},";
            }
        }
   
    /*if(titleIn=='Parsippany')
     {
     arrFac=_.uniq(arrFac);
     if(arrFac.length<4)
     {
     series='';
     }
     }
     if(titleIn=='Montreal')
     {
     arrFac=_.uniq(arrFac);
     if(arrFac.length<7)
     {
     series='';
     }
     }
     var body='';
     if(titleIn=='Vancouver'||titleIn=='Dallas'||titleIn=='Toronto'||titleIn=='Columbus'||titleIn=='Minneapolis'||titleIn=='Jacksonville')
     {
     arrFac=_.uniq(arrFac);
     for ( var i = 0; arrFac != null && i < arrFac.length; i++ )
     {
     body=body+ ';'+arrFac[i];
     }
     if(arrFac.length<2)
     {
     series='';
     }
     }*/
    //nlapiSendEmail(206211, 206211,'date','body:'+body,null,null,null,null); // Send email to Catalina
    return series;
}
function returnCasesCount(market, facility,color)
{
    if((facility!=0)&&(market==0))
    {

        var series='';
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("custevent_cologix_facility",null,"anyof",facility));
        arrFilters.push(new nlobjSearchFilter("custevent_cologix_sub_case_type",null,"noneof",86));
        var arrColumns = new Array();
        if(color==0)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');

                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }   if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                //+searchCase.getValue(columns[2]);
                series +="{ title:'"+ count+" approved', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=0&mark="+market+"',start: '"+datestring+"',color:'#9DC209'},";


            }
        }


        if(color==2)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_2', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" pending', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=2&mark="+market+"', start: '"+datestring+"',color:'red'},";


            }
        }

        if(color==1)
        {
            var arrayDateRed=new Array();
            var searchCasesRed = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_2', arrFilters, arrColumns);
            for ( var i = 0; searchCasesRed != null && i < searchCasesRed.length; i++ ) {
                var searchCase = searchCasesRed[i];
                var columns = searchCase.getAllColumns();
                var date = searchCase.getValue(columns[1]);
                arrayDateRed.push(date);
            }
            if(arrayDateRed.length>0) {
                for (var j = 0; j < arrayDateRed.length; j++) {
                    arrFilters.push(new nlobjSearchFilter("custevent_cologix_case_sched_followup", null, "noton", arrayDateRed[j]));
                }
            }
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" pending', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=1&mark="+market+"', start: '"+datestring+"',color:'#F88017'},";

            }

        }
        if(color==3)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_3', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" closed', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=3&mark="+market+"', start: '"+datestring+"',color:'#6495ED'},";


            }
        }
        if(color==4)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_3_2', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" in progress', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=4&mark="+market+"', start: '"+datestring+"',color:'#463E3F'},";


            }
        }
        if(color==5)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_3_2_2', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" resolved', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=5&mark="+market+"', start: '"+datestring+"',color:'#7D0552'},";


            }
        }
        if(color==6)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_3_2__2', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" not started', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=6&mark="+market+"', start: '"+datestring+"',color:'#7F462C'},";


            }
        }





    }
    if((facility==0)&&(market!=0))
    {

        var series='';
        var arrFilters = new Array();
        var facilityArray=getFacilityByMarket(market);
        arrFilters.push(new nlobjSearchFilter("custevent_cologix_facility",null,"anyof",facilityArray));
        arrFilters.push(new nlobjSearchFilter("custevent_cologix_sub_case_type",null,"noneof",86));
        var arrColumns = new Array();
        if(color==0)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');

                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }   if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" approved', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=0&mark="+market+"', start: '"+datestring+"',color:'#9DC209'},";


            }
        }

        if(color==2)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_2', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" pending', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=2&mark="+market+"', start: '"+datestring+"',color:'red'},";


            }
        }
        if(color==1)
        {
            var arrayDateRed=new Array();
            var searchCasesRed = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_2', arrFilters, arrColumns);
            for ( var i = 0; searchCasesRed != null && i < searchCasesRed.length; i++ ) {
                var searchCase = searchCasesRed[i];
                var columns = searchCase.getAllColumns();
                var date = searchCase.getValue(columns[1]);
                arrayDateRed.push(date);
            }
            if(arrayDateRed.length>0) {
                for (var j = 0; j < arrayDateRed.length; j++) {
                    arrFilters.push(new nlobjSearchFilter("custevent_cologix_case_sched_followup", null, "noton", arrayDateRed[j]));
                }
            }

            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" pending', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=1&mark="+market+"', start: '"+datestring+"',color:'#F88017'},";

            }

        }

        if(color==3)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_3', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" closed', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=3&mark="+market+"', start: '"+datestring+"',color:'#6495ED'},";


            }
        }
        if(color==4)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_3_2', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" in progress', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=4&mark="+market+"', start: '"+datestring+"',color:'#463E3F'},";


            }
        }
        if(color==5)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_3_2_2', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" resolved', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=5&mark="+market+"', start: '"+datestring+"',color:'#7D0552'},";


            }
        }
        if(color==6)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_3_2__2', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" not started', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=6&mark="+market+"', start: '"+datestring+"',color:'#7F462C'},";


            }
        }

    }
    if((facility==0)&&(market==0))
    {
        var series='';
        var arrFilters = new Array();
        var arrColumns = new Array();
        arrFilters.push(new nlobjSearchFilter("custevent_cologix_sub_case_type",null,"noneof",86));
        if(color==0)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');

                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }   if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" approved', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=0&mark="+market+"', start: '"+datestring+"',color:'#9DC209'},";


            }
        }

        if(color==2)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_2', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" pending', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=2&mark="+market+"', start: '"+datestring+"',color:'red'},";

            }
        }
        if(color==1)
        {
            var arrayDateRed=new Array();
            var searchCasesRed = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_2', arrFilters, arrColumns);
            for ( var i = 0; searchCasesRed != null && i < searchCasesRed.length; i++ ) {
                var searchCase = searchCasesRed[i];
                var columns = searchCase.getAllColumns();
                var date = searchCase.getValue(columns[1]);
                arrayDateRed.push(date);
            }
            if(arrayDateRed.length>0) {
                for (var j = 0; j < arrayDateRed.length; j++) {
                    arrFilters.push(new nlobjSearchFilter("custevent_cologix_case_sched_followup", null, "noton", arrayDateRed[j]));
                }
            }
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" pending', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=1&mark="+market+"', start: '"+datestring+"',color:'#F88017'},";

            }

        }

        if(color==3)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_3', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit = date.split('/');
                if (datesplit[0] < 10) {
                    var month = '0' + datesplit[0];
                }
                else {
                    var month = datesplit[0];
                }
                if (datesplit[1] < 10) {
                    var day = '0' + datesplit[1];
                }
                else {
                    var day = datesplit[1];
                }
                var datestring = datesplit[2] + '-' + month + '-' + day;
                series += "{ title:'" + count + " closed', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market=" + facility + "&date=" + date + "&type=3&mark=" + market + "', start: '" + datestring + "',color:'#6495ED'},";
            }
        }
        if(color==4)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_3_2', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" in progress', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=4&mark="+market+"', start: '"+datestring+"',color:'#463E3F'},";


            }
        }
        if(color==5)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_3_2_2', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" resolved', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=5&mark="+market+"', start: '"+datestring+"',color:'#7D0552'},";


            }
        }
        if(color==6)
        {
            var searchCases = nlapiSearchRecord('supportcase', 'customsearch_ss_maint_cases_app_2_3_2__2', arrFilters, arrColumns);
            for ( var i = 0; searchCases != null && i < searchCases.length; i++ ) {
                var searchCase = searchCases[i];
                var columns = searchCase.getAllColumns();
                var count = searchCase.getValue(columns[0]);
                var date = searchCase.getValue(columns[1]);
                var datesplit=date.split('/');
                if(datesplit[0]<10)
                {
                    var month='0'+datesplit[0];
                }
                else{
                    var month=datesplit[0];
                }
                if(datesplit[1]<10)
                {
                    var day='0'+datesplit[1];
                }
                else{
                    var day=datesplit[1];
                }
                var datestring=datesplit[2]+'-'+month+'-'+day;
                series +="{ title:'"+ count+" not started', url: '/app/site/hosting/scriptlet.nl?script=518&deploy=1&market="+facility+"&date="+date+"&type=6&mark="+market+"', start: '"+datestring+"',color:'#7F462C'},";


            }
        }


    }
    return series;
}

function getFacilityByTitle(title){
    var filter='is';
    if((title.indexOf("&") > -1))
    {
        title=title.replace("&", " and ");
    }

    var facility=title;
    https://debugger.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=517&deploy=1&market=8&location=0&title=Columbus
        if(title=='Columbus'){
            facility='COL';
            filter='startswith';
        }
    if(title=='COL1'){
        facility='COL1 and 2';
        filter='is';
    }
    if(title=='MIN1'){
        facility='MIN1 and 2';
        filter='is';
    }
    if(title=='All Markets'){
        facility='';
        filter='';
    }
    if(title=='Dallas'){
        facility='DAL';
        filter='startswith';
    }
    if(title=='Jacksonville'){
        facility='JAX';
        filter='startswith';
    }
    if(title=='Lakeland'){
        facility='LAK';
        filter='startswith';
    }
    if(title=='Minneapolis'){
        facility='MIN';
        filter='startswith';
    }
    if(title=='Montreal'){
        facility='MTL';
        filter='startswith';
    }
    if(title=='Parsippany'){
        facility='NNJ';
        filter='startswith';
    }
    if(title=='Toronto'){
        facility='TOR';
        filter='startswith';
    }
    if(title=='Vancouver'){
        facility='VAN';
        filter='startswith';
    }
    var arr=[facility,filter]
    return arr;
}
function getFacilityByMarket(market){
    var facility=new Array();
    switch(parseInt(market)) {
        case 2: // Dallas
            facility=[3, 18, 44];
            break;
        case 8: // COL
            facility=[24, 29];
            break;
        case 4: // JAK
            facility=[21, 27];
            break;
        case 9: // LAK
            facility=[28];
            break;
        case 3: // MIN
            facility=[17,25];
            break;
        case 6: // MTL
            facility=[2,5,4,9,6,7,19];

            break;
        case 10: // PAR
            facility=[34,33,35,36];

            break;
        case 5: // TOR
            facility=[23,8,15];
            break;
        case 7: // VAN
            facility=[14,20,42];

            break;
        default:
            facility = [];

    }
    return facility;
}

function isLastDay(dt) {
    var test = new Date(dt);
    test.setDate(test.getDate() + 1);
    return test.getDate() === 1;
}

