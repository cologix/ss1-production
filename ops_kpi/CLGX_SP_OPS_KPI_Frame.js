nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SP_OPS_KPI_Frame.js
//	Script Name:	CLGX_SP_OPS_KPI_Frame
//	Script Id:		customscript_clgx_sp_ops_kpi_frame
//	Script Runs:	On Server
//	Script Type:	Portlet
//	Created:		23/06/2015
//-------------------------------------------------------------------------------------------------

function portlet_ops_kpi_frame (portlet, column){
	
	var userid = nlapiGetUser();
	var roleid = nlapiGetContext().getRole();
	var closed = 0;
	
	if (closed == 0) {
    	var html = '<iframe name="chartsFrame" id="chartsFrame" src="/app/site/hosting/scriptlet.nl?script=502&deploy=1" height="540px" width="850px" frameborder="0" scrolling="no"></iframe>';
    }
    else{
    	var html = 'This module is closed for modifications. Sorry for the inconvenience. Please come back later.';
    }
    portlet.setTitle('Operations KPI');
    portlet.setHtml(html);
    
}

