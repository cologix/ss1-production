nlapiLogExecution("audit","FLOStart",new Date().getTime());
function getGridSeries(request, response)
{

    var title = request.getParameter('title');
    var location = request.getParameter('location');
    var type=request.getParameter('type');
    var subtype = request.getParameter('subtype');
    var origin = request.getParameter('origin');
    var priority = request.getParameter('priority');
    var open = request.getParameter('open');
    var from = request.getParameter('from');
    var to = request.getParameter('to');
    var month = request.getParameter('month');
    var objFile = nlapiLoadFile(3063831);
     if(open==1)
    {
    if(priority==4)
    {
    	var objFile = nlapiLoadFile(3083174);
    }
    else{
      	var objFile = nlapiLoadFile(3080865);
      }
    }
    else{
    if(priority==4)
    {
    var objFile = nlapiLoadFile(3083173);
    }
    else{
    var objFile = nlapiLoadFile(3063831);
    }
  
    }
    var html = objFile.getValue();
    html = html.replace(new RegExp('{title}','g'), title);
    html = html.replace(new RegExp('{series}','g'), getSeries(title,location,type,subtype,origin,priority,from,to,open,month));
    response.write( html );
}
function getSeries(title,location,type,subtype,origin,priority,from,to,open,month)
{
    var series='';
    var arrFilters = new Array();
    var arrColumns = new Array();
    var statusArray=[5,7];
    if(open==1)
    {
arrFilters.push(new nlobjSearchFilter("createddate",null,"within",from,to));
arrFilters.push(new nlobjSearchFilter("status",null,"noneof",statusArray));
}
else{

   // arrFilters.push(new nlobjSearchFilter("closeddate",null,"within",from,to));
         arrFilters.push(new nlobjSearchFilter("status",null,"anyof",statusArray));
   
}

    if(location!=0)
    {
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_reporting_locations","custevent_clgx_case_location","anyof",location));
    }
    var arrpriorityall=[1,2,3];
    arrFilters.push(new nlobjSearchFilter("origin",null,"anyof",origin));
    if(priority==4)
    {
    	arrFilters.push(new nlobjSearchFilter("priority",null,"anyof",arrpriorityall));
    }
    else
    {
    	arrFilters.push(new nlobjSearchFilter("priority",null,"anyof",priority));
    }
    if(type!=0)
    {
        if(type=='incident')
        {
            arrFilters.push(new nlobjSearchFilter("type",null,"anyof",2));
        }
        if(type=='rh')
        {
            arrFilters.push(new nlobjSearchFilter("type",null,"anyof",1));
        }
    }
    if(subtype!=0)
    {

        arrFilters.push(new nlobjSearchFilter("custevent_cologix_sub_case_type",null,"anyof",subtype));

    }
    if(open==1)
    {
      var searchIns = nlapiSearchRecord('supportcase', 'customsearch_clgx_ss_cases_highpr_2_4', arrFilters, arrColumns);
    
    }
    else
    {
     if(month==3)
     {
       var searchIns = nlapiSearchRecord('supportcase', 'customsearch_clgx_ss_cases_highpr_2_4_2', arrFilters, arrColumns);
       
      }
          if(month==2)
     	{
       		var searchIns = nlapiSearchRecord('supportcase', 'customsearch_clgx_ss_cases_highpr_2_4__2', arrFilters, arrColumns);
        }
          if(month==1)
     	{
       		var searchIns = nlapiSearchRecord('supportcase', 'customsearch_clgx_ss_cases_highpr_2_4__3', arrFilters, arrColumns);
        }
          if(month==0)
     	{
       		var searchIns = nlapiSearchRecord('supportcase', 'customsearch_clgx_ss_cases_highpr_2_4__4', arrFilters, arrColumns);
        }
    }
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var nbr = searchIn.getValue(columns[0]);
        var id = searchIn.getValue(columns[1]);
        var customer = searchIn.getValue(columns[2]);
        var date = searchIn.getValue(columns[3]);
        var time = searchIn.getValue(columns[4]);
        var sub = searchIn.getText(columns[5]);
        var res = searchIn.getValue(columns[8]);
        var type=searchIn.getText(columns[9]);
        var timer = searchIn.getValue(columns[10]);
        var dateResolve=searchIn.getValue(columns[11]);
        var minutesResolve=searchIn.getValue(columns[12]);
        if(dateResolve!=null && dateResolve!='')
        {
        	time=minutesResolve;
        	date=dateResolve;
        }
        if(open==1)
        {
          var date = searchIn.getValue(columns[6]);
          var time = searchIn.getText(columns[7]);
        }
        if(open==1)
        {
          if(priority==4)
        {
              series += '\n{"NBR":"' + nbr +
            '","ID":' + id+
            ',"CUST":"' +  customer +
            '","DATECL":"' + date+
            '","TIME":"' + time+
            '","SUB":"' + sub+
            '","RES":"' + res+
            '","TYPE":"' + type+
             '","TIMER":"' + timer+
            '"},';
        }
        else
        {
          series += '\n{"NBR":"' + nbr +
            '","ID":' + id+
            ',"CUST":"' +  customer +
            '","DATECL":"' + date+
            '","TIME":"' + time+
            '","SUB":"' + sub+
            '","TYPE":"' + type+
            '"},';
        }
        }
        else
        {
        if(priority==4)
        {
              series += '\n{"NBR":"' + nbr +
            '","ID":' + id+
            ',"CUST":"' +  customer +
            '","DATECL":"' + date+
            '","TIME":"' + time+
            '","SUB":"' + sub+
            '","RES":"' + res+
             '","TIMER":"' + timer+
            '"},';
        }
        else
        {
        series += '\n{"NBR":"' + nbr +
            '","ID":' + id+
            ',"CUST":"' +  customer +
            '","DATECL":"' + date+
            '","TIME":"' + time+
            '","SUB":"' + sub+
            '"},';
            }
        }
    }

    var strLen = series.length;
    if (searchIns != null){
        series = series.slice(0,strLen-1);
    }
    return series;
}
