nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//  Script File:    CLGX_SL_OPS_KPI_Cases.js
//  Script Name:    CLGX_SL_OPS_KPI_Cases
//  Script Id:      customscript_clgx_sl_ops_kpi_cases
//  Script Runs:    On Server
//  Script Type:    Suitelet
//-------------------------------------------------------------------------------------------------

function suitelet_ops_kpi_cases_prod (request, response){
    try {
        var title = request.getParameter('title');
        var location = request.getParameter('location');
        var type=request.getParameter('type');
        var subtype = request.getParameter('subtype');
        var open= request.getParameter('open');
        var objFile = nlapiLoadFile(3060648);
        var html = objFile.getValue();


        if(open==1)
        {
            var returnSeries=returnSeriesHigh1(title, location,type,subtype);
            var avtitle='';
            var casetitle='Open Cases';
        }
        else
        {
            var returnSeries=returnSeriesHigh(title, location,type,subtype);
            var avtitle='Average Resolved Time in Minutes';
            var casetitle='Case Resolved';
        }
        if(type=='rh' && open!=1 && subtype==5)
        {
            var widthinc=2;
            var valinchigh=24;
            var valincmedium=24;
            var valinclow=24;

        }
        else
        {
            var widthinc=0;
            var valinchigh=0;
            var valincmedium=0;
            var valinclow=0;
        }
        var prevMonth1 = new moment().subtract(1, 'months').format('MMMM');
        var prevMonth2 = new moment().subtract(2, 'months').format('MMMM');
        var prevMonth3 = new moment().subtract(3, 'months').format('MMMM');
        var currMonth = new moment().subtract(0, 'months').format('MMMM');
        var stringMTHs="['"+prevMonth3+"', '"+prevMonth2+"', '"+prevMonth1+"', '"+currMonth+"']";
        html = html.replace(new RegExp('{months}','g'), stringMTHs);
        html = html.replace(new RegExp('{serieshigh}','g'), returnSeries[0]);
        html = html.replace(new RegExp('{seriesmedium}','g'),returnSeries[1]);
        html = html.replace(new RegExp('{serieslow}','g'),returnSeries[2]);
        html = html.replace(new RegExp('{seriesresponse}','g'),returnSeries[3]);
        html = html.replace(new RegExp('{averagetitley}','g'),avtitle);
        html = html.replace(new RegExp('{widthdaslow}','g'),widthinc);
        html = html.replace(new RegExp('{valuedaslow}','g'),valinclow);
        html = html.replace(new RegExp('{widthdasmedium}','g'),widthinc);
        html = html.replace(new RegExp('{valuedasmedium}','g'),valincmedium);
        html = html.replace(new RegExp('{widthdashigh}','g'),widthinc);
        html = html.replace(new RegExp('{valuedashigh}','g'),valinchigh);
        html = html.replace(new RegExp('{casetitle}','g'),casetitle);


        response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
function returnSeriesHigh(title, market,type,subtype)
{
    var series='';
    var seriesmedium='';
    var serieslow='';
    var seriesresponse ='';
    var arrFilters = new Array();
    var arrColumns = new Array();
    var arrFiltersAv = new Array();
    var arrColumnsAv = new Array();
    var arrDatesCurr  = getDateRange(0);
    var arrDatesBf1  = getDateRange(-1);
    var arrDatesBf2  = getDateRange(-2);
    var arrDatesBf3  = getDateRange(-3);
    var from0= arrDatesCurr[0];
    var to0  = arrDatesCurr[1];
    var from1 = arrDatesBf1[0];
    var to1  = arrDatesBf1[1];
    var from2 = arrDatesBf2[0];
    var to2  = arrDatesBf2[1];
    var from3 = arrDatesBf3[0];
    var to3  = arrDatesBf3[1];

    var av0H=0;
    var av0M=0;
    var av0L=0;
    var av1H=0;
    var av1M=0;
    var av1L=0;
    var av2H=0;
    var av2M=0;
    var av2L=0;
    var av3H=0;
    var av3M=0;
    var av3L=0;

    var av0R=0;
    var av1R=0;
    var av2R=0;
    var av3R=0;

    var av0HA=0;
    var av0MA=0;
    var av0LA=0;
    var av1HA=0;
    var av1MA=0;
    var av1LA=0;
    var av2HA=0;
    var av2MA=0;
    var av2LA=0;
    var av3HA=0;
    var av3MA=0;
    var av3LA=0;

    var av0RA=0;
    var av1RA=0;
    var av2RA=0;
    var av3RA=0;

    var av0HN=0;
    var av0MN=0;
    var av0LN=0;
    var av1HN=0;
    var av1MN=0;
    var av1LN=0;
    var av2HN=0;
    var av2MN=0;
    var av2LN=0;
    var av3HN=0;
    var av3MN=0;
    var av3LN=0;

    var av0HR=0;
    var av1HR=0;
    var av2HR=0;
    var av3HR=0;

    var curremailRHU = 0;
    var Mth1emailRHU = 0;
    var Mth2emailRHU = 0;
    var Mth3emailRHU = 0;
//portal
    var currwebRHU = 0;
    var Mth1webRHU =0;
    var Mth2webRHU = 0;
    var Mth3webRHU = 0;
//phone
    var currphoneRHU = 0;
    var Mth1phoneRHU = 0;
    var Mth2phoneRHU = 0;
    var Mth3phoneRHU = 0;
    //matrix
    var currmatrixRHU = 0;
    var Mth1matrixRHU = 0;
    var Mth2matrixRHU = 0;
    var Mth3matrixRHU = 0;

    var curremailRHM = 0;
    var Mth1emailRHM = 0;
    var Mth2emailRHM = 0;
    var Mth3emailRHM = 0;
//portal
    var currwebRHM = 0;
    var Mth1webRHM = 0;
    var Mth2webRHM = 0;
    var Mth3webRHM = 0;
//phone
    var currphoneRHM = 0;
    var Mth1phoneRHM = 0;
    var Mth2phoneRHM = 0;
    var Mth3phoneRHM = 0;
    //matrix
    var currmatrixRHM = 0;
    var Mth1matrixRHM = 0;
    var Mth2matrixRHM = 0;
    var Mth3matrixRHM = 0;
    //LOW
    var curremailRHL = 0;
    var Mth1emailRHL = 0;
    var Mth2emailRHL = 0;
    var Mth3emailRHL = 0;
//portal
    var currwebRHL = 0;
    var Mth1webRHL = 0;
    var Mth2webRHL = 0;
    var Mth3webRHL = 0;
//phone
    var currphoneRHL = 0;
    var Mth1phoneRHL = 0;
    var Mth2phoneRHL = 0;
    var Mth3phoneRHL = 0;

    //phone
    var currmatrixRHL = 0;
    var Mth1matrixRHL = 0;
    var Mth2matrixRHL = 0;
    var Mth3matrixRHL = 0;

    //RESPONSE
    var curremailRHR = 0;
    var Mth1emailRHR = 0;
    var Mth2emailRHR = 0;
    var Mth3emailRHR = 0;
//portal
    var currwebRHR = 0;
    var Mth1webRHR = 0;
    var Mth2webRHR = 0;
    var Mth3webRHR = 0;
//phone
    var currphoneRHR = 0;
    var Mth1phoneRHR = 0;
    var Mth2phoneRHR = 0;
    var Mth3phoneRHR = 0;
    //matrix
    var currmatrixRHR = 0;
    var Mth1matrixRHR = 0;
    var Mth2matrixRHR = 0;
    var Mth3matrixRHR = 0;
    //1 RH
    //2 Incident
    
    arrFilters.push(new nlobjSearchFilter("internalIdNumber", null, "greaterthan", 0));
    
    if(market!=0)
    {
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_reporting_locations","custevent_clgx_case_location","anyof",market));
    }
    if(type!=0)
    {
        if(type=='incident')
        {
            arrFilters.push(new nlobjSearchFilter("type",null,"anyof",2));
        }
        if(type=='rh')
        {
            arrFilters.push(new nlobjSearchFilter("type",null,"anyof",1));
        }
    }
    if(subtype!=0)
    {
        arrFilters.push(new nlobjSearchFilter("custevent_cologix_sub_case_type",null,"anyof",subtype));
    }
    var start0= moment(from0, "MM/DD/YYYY");
    var end0= moment(to0, "MM/DD/YYYY");
    var start1= moment(from1, "MM/DD/YYYY");
    var end1= moment(to1, "MM/DD/YYYY");
    var start2= moment(from2, "MM/DD/YYYY");
    var end2= moment(to2, "MM/DD/YYYY");
    var start3= moment(from3, "MM/DD/YYYY");
    var end3= moment(to3, "MM/DD/YYYY");
    // arrFilters.push(new nlobjSearchFilter("closeddate",null,"within",from3,to0));
    
	while (true) {
		searchCases = nlapiSearchRecord('supportcase', 'customsearch_clgx_ss_cases_highpr_3_3', arrFilters, arrColumns);
		if (!searchCases) {
			break;
		}
		for (var i in searchCases) {
			
			var columns = searchCases[i].getAllColumns();
        	var caseid=searchCases[i].getValue(columns[0]);
        	
            var date=new Date(searchCases[i].getValue(columns[1]));
            var priority=searchCases[i].getText(columns[2]);
            var origin=searchCases[i].getText(columns[3]);
            var av=searchCases[i].getValue(columns[4]);
            var avR=searchCases[i].getValue(columns[5]);
            var dateResolve1=searchCases[i].getValue(columns[6]);
            var dateResolve2=new Date(searchCases[i].getValue(columns[6]));
            var minutesResolve=searchCases[i].getValue(columns[7]);

        
        if(subtype==6){
            var createddate=searchCases[i].getValue(columns[8])+' '+searchCases[i].getValue(columns[9]);
            var newdate= createddate.replace('-',':');
            var now = moment(date);
            var mid = moment(newdate);

            var minutesResolve= now.diff(mid, 'minutes');
        }

        var datestr=(parseInt(date.getMonth())+parseInt(1))+'/'+date.getDate()+'/'+date.getFullYear();
        var date = moment(datestr, "MM/DD/YYYY");
        var datestrR=(parseInt(dateResolve2.getMonth())+parseInt(1))+'/'+dateResolve2.getDate()+'/'+dateResolve2.getFullYear();
        var dateResolve = moment(datestrR, "MM/DD/YYYY");

        if(priority=='Urgent')
        {
            if(dateResolve1!=null && dateResolve1!='')
            {
                if((dateResolve >= start0) && (dateResolve <= end0))
                {
                    if(av!=''){
                        av0HA +=parseFloat(minutesResolve);
                        av0HN +=parseInt(1);
                        av0RA +=parseFloat(avR);
                        av0HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        curremailRHR++;
                        curremailRHU++;
                    }
                    if(origin=="Phone")
                    {
                        currphoneRHR++;
                        currphoneRHU++;
                    }
                    if(origin=="Matrix")
                    {
                        currmatrixRHR++;
                        currmatrixRHU++;
                    }
                    if(origin=="Web")
                    {
                        currwebRHR++;
                        currwebRHU++;
                    }
                }
                if((dateResolve >= start1) && (dateResolve <= end1))
                {
                    if(av!=''){
                        av1HA +=parseFloat(minutesResolve);
                        av1HN +=parseInt(1);
                        av1RA +=parseFloat(avR);
                        av1HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth1emailRHU++;
                        Mth1emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth1phoneRHU++;
                        Mth1phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth1matrixRHU++;
                        Mth1matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth1webRHU++;
                        Mth1webRHR++;
                    }
                }
                if((dateResolve >= start2) && (dateResolve <= end2))
                {
                    if(av!=''){
                        av2HA +=parseFloat(minutesResolve);
                        av2HN +=parseInt(1);
                        av2RA +=parseFloat(avR);
                        av2HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth2emailRHU++;
                        Mth2emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth2phoneRHU++;
                        Mth2phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth2matrixRHU++;
                        Mth2matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth2webRHU++;
                        Mth2webRHR++;
                    }
                }
                if((dateResolve >= start3) && (dateResolve <= end3))
                {
                    if(av!=''){
                        av3HA +=parseFloat(minutesResolve);
                        av3HN +=parseInt(1);
                        av3RA +=parseFloat(avR);
                        av3HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth3emailRHU++;
                        Mth3emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth3phoneRHU++;
                        Mth3phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth3matrixRHU++;
                        Mth3matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth3webRHU++;
                        Mth3webRHR++;
                    }
                }
            }

            else
            {
                if((date >= start0) && (date <= end0))
                {
                    if(av!=''){
                        av0HA +=parseFloat(av);
                        av0HN +=parseInt(1);
                        av0RA +=parseFloat(avR);
                        av0HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        curremailRHR++;
                        curremailRHU++;
                    }
                    if(origin=="Phone")
                    {
                        currphoneRHR++;
                        currphoneRHU++;
                    }
                    if(origin=="Matrix")
                    {
                        currmatrixRHR++;
                        currmatrixRHU++;
                    }
                    if(origin=="Web")
                    {
                        currwebRHR++;
                        currwebRHU++;
                    }
                }
                if((date >= start1) && (date <= end1))
                {
                    if(av!=''){
                        av1HA +=parseFloat(av);
                        av1HN +=parseInt(1);
                        av1RA +=parseFloat(avR);
                        av1HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth1emailRHU++;
                        Mth1emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth1phoneRHU++;
                        Mth1phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth1matrixRHU++;
                        Mth1matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth1webRHU++;
                        Mth1webRHR++;
                    }
                }
                if((date >= start2) && (date <= end2))
                {
                    if(av!=''){
                        av2HA +=parseFloat(av);
                        av2HN +=parseInt(1);
                        av2RA +=parseFloat(avR);
                        av2HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth2emailRHU++;
                        Mth2emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth2phoneRHU++;
                        Mth2phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth2matrixRHU++;
                        Mth2matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth2webRHU++;
                        Mth2webRHR++;
                    }
                }
                if((date >= start3) && (date <= end3))
                {
                    if(av!=''){
                        av3HA +=parseFloat(av);
                        av3HN +=parseInt(1);
                        av3RA +=parseFloat(avR);
                        av3HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth3emailRHU++;
                        Mth3emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth3phoneRHU++;
                        Mth3phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth3matrixRHU++;
                        Mth3matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth3webRHU++;
                        Mth3webRHR++;
                    }
                }
            }
        }
//Medium
        if(priority=='Medium')
        {
            if(dateResolve1!=null && dateResolve1!='')
            {
                if((dateResolve >= start0) && (dateResolve <= end0))
                {
                    if(av!=''){
                        av0MA +=parseFloat(minutesResolve);
                        av0MN +=parseInt(1);
                        av0RA +=parseFloat(avR);
                        av0HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        curremailRHM++;
                        curremailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        currphoneRHM++;
                        currphoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        currmatrixRHM++;
                        currmatrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        currwebRHM++;
                        currwebRHR++;
                    }
                }
                if((dateResolve >= start1) && (dateResolve <= end1))
                {
                    if(av!=''){
                        av1MA +=parseFloat(minutesResolve);
                        av1MN +=parseInt(1);
                        av1RA +=parseFloat(avR);
                        av1HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth1emailRHM++;
                        Mth1emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth1phoneRHM++;
                        Mth1phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth1matrixRHM++;
                        Mth1matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth1webRHM++;
                        Mth1webRHR++;
                    }
                }

                if((dateResolve >= start2) && (dateResolve <= end2))
                {
                    if(av!=''){
                        av2MA +=parseFloat(minutesResolve);
                        av2MN +=parseInt(1);
                        av2RA +=parseFloat(avR);
                        av2HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth2emailRHM++;
                        Mth2emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth2phoneRHM++;
                        Mth2phoneRHR++;
                    }
                    if(origin=="Matrix  ")
                    {
                        Mth2matrixRHM++;
                        Mth2matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth2webRHM++;
                        Mth2webRHR++;

                    }
                }
                if((dateResolve >= start3) && (dateResolve <= end3))
                {
                    if(av!=''){
                        av3MA +=parseFloat(minutesResolve);
                        av3MN +=parseInt(1);
                        av3RA +=parseFloat(avR);
                        av3HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth3emailRHM++;
                        Mth3emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth3phoneRHM++;
                        Mth3phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth3matrixRHM++;
                        Mth3matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth3webRHM++;
                        Mth3webRHR++;
                    }
                }

            }
            else{
                if((date >= start0) && (date <= end0))
                {
                    if(av!=''){
                        av0MA +=parseFloat(av);
                        av0MN +=parseInt(1);
                        av0RA +=parseFloat(avR);
                        av0HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        curremailRHM++;
                        curremailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        currphoneRHM++;
                        currphoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        currmatrixRHM++;
                        currmatrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        currwebRHM++;
                        currwebRHR++;
                    }
                }
                if((date >= start1) && (date <= end1))
                {
                    if(av!=''){
                        av1MA +=parseFloat(av);
                        av1MN +=parseInt(1);
                        av1RA +=parseFloat(avR);
                        av1HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth1emailRHM++;
                        Mth1emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth1phoneRHM++;
                        Mth1phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth1matrixRHM++;
                        Mth1matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth1webRHM++;
                        Mth1webRHR++;
                    }
                }

                if((date >= start2) && (date <= end2))
                {
                    if(av!=''){
                        av2MA +=parseFloat(av);
                        av2MN +=parseInt(1);
                        av2RA +=parseFloat(avR);
                        av2HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth2emailRHM++;
                        Mth2emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth2phoneRHM++;
                        Mth2phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth2matrixRHM++;
                        Mth2matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth2webRHM++;
                        Mth2webRHR++;

                    }
                }
                if((date >= start3) && (date <= end3))
                {
                    if(av!=''){
                        av3MA +=parseFloat(av);
                        av3MN +=parseInt(1);
                        av3RA +=parseFloat(avR);
                        av3HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth3emailRHM++;
                        Mth3emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth3phoneRHM++;
                        Mth3phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth3matrixRHM++;
                        Mth3matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth3webRHM++;
                        Mth3webRHR++;
                    }
                }
            }
        }
//Low
        if(priority=='Low')
        {
            if(dateResolve1!=null && dateResolve1!='')
            {
                if((dateResolve >= start0) && (dateResolve <= end0))
                {
                    if(av!=''){
                        av0LA +=parseFloat(minutesResolve);
                        av0LN +=parseInt(1);
                        av0RA +=parseFloat(avR);
                        av0HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        curremailRHL++;
                        curremailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        currphoneRHL++;
                        currphoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        currmatrixRHL++;
                        currmatrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        currwebRHL++;
                        currwebRHR++;
                    }
                }
                if((dateResolve >= start1) && (dateResolve <= end1))
                {
                    if(av!=''){
                        av1LA +=parseFloat(minutesResolve);
                        av1LN +=parseInt(1);
                        av1RA +=parseFloat(avR);
                        av1HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth1emailRHL++;
                        Mth1emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth1phoneRHL++;
                        Mth1phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth1matrixRHL++;
                        Mth1matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth1webRHL++;
                        Mth1webRHR++;
                    }
                }
                if((dateResolve >= start2) && (dateResolve <= end2))
                {
                    if(av!=''){
                        av2LA +=parseFloat(minutesResolve);
                        av2LN +=parseInt(1);
                        av2RA +=parseFloat(avR);
                        av2HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth2emailRHL++;
                        Mth2emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth2phoneRHL++;
                        Mth2phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth2matrixRHL++;
                        Mth2matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth2webRHL++;
                        Mth2webRHR++;
                    }
                }
                if((dateResolve >= start3) && (dateResolve <= end3))
                {
                    if(av!=''){
                        av3LA +=parseFloat(minutesResolve);
                        av3LN +=parseInt(1);
                        av3RA +=parseFloat(avR);
                        av3HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth3emailRHL++;
                        Mth3emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth3phoneRHL++;
                        Mth3phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth3matrixRHL++;
                        Mth3matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth3webRHL++;
                        Mth3webRHR++;
                    }
                }
            }
            else{
                if((date >= start0) && (date <= end0))
                {
                    if(av!=''){
                        av0LA +=parseFloat(av);
                        av0LN +=parseInt(1);
                        av0RA +=parseFloat(avR);
                        av0HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        curremailRHL++;
                        curremailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        currphoneRHL++;
                        currphoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        currmatrixRHL++;
                        currmatrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        currwebRHL++;
                        currwebRHR++;
                    }
                }
                if((date >= start1) && (date <= end1))
                {
                    if(av!=''){
                        av1LA +=parseFloat(av);
                        av1LN +=parseInt(1);
                        av1RA +=parseFloat(avR);
                        av1HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth1emailRHL++;
                        Mth1emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth1phoneRHL++;
                        Mth1phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth1matrixRHL++;
                        Mth1matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth1webRHL++;
                        Mth1webRHR++;
                    }
                }
                if((date >= start2) && (date <= end2))
                {
                    if(av!=''){
                        av2LA +=parseFloat(av);
                        av2LN +=parseInt(1);
                        av2RA +=parseFloat(avR);
                        av2HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth2emailRHL++;
                        Mth2emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth2phoneRHL++;
                        Mth2phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth2matrixRHL++;
                        Mth2matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth2webRHL++;
                        Mth2webRHR++;
                    }
                }
                if((date >= start3) && (date <= end3))
                {
                    if(av!=''){
                        av3LA +=parseFloat(av);
                        av3LN +=parseInt(1);
                        av3RA +=parseFloat(avR);
                        av3HR +=parseInt(1);
                    }
                    if(origin=="E-mail")
                    {
                        Mth3emailRHL++;
                        Mth3emailRHR++;
                    }
                    if(origin=="Phone")
                    {
                        Mth3phoneRHL++;
                        Mth3phoneRHR++;
                    }
                    if(origin=="Matrix")
                    {
                        Mth3matrixRHL++;
                        Mth3matrixRHR++;
                    }
                    if(origin=="Web")
                    {
                        Mth3webRHL++;
                        Mth3webRHR++;
                    }
                }
            }
        }
        

	}
	if (searchCases.length < 1000) {
		break;
	}
	arrFilters[0] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", caseid);
}
    
    
    
    
    
    

    if(av0HA>0)
    {
        av0H=parseFloat(av0HA)/parseFloat(av0HN);
    }
    if(av1HA>0)
    {
        av1H=parseFloat(av1HA)/parseFloat(av1HN);
    }
    if(av2HA>0)
    {
        av2H=parseFloat(av2HA)/parseFloat(av2HN);
    }
    if(av3HA>0)
    {
        av3H=parseFloat(av3HA)/parseFloat(av3HN);
    }
    if(av0MA>0)
    {
        av0M=parseFloat(av0MA)/parseFloat(av0MN);
    }
    if(av1MA>0)
    {
        av1M=parseFloat(av1MA)/parseFloat(av1MN);
    }
    if(av2MA>0)
    {
        av2M=parseFloat(av2MA)/parseFloat(av2MN);
    }
    if(av3MA>0)
    {
        av3M=parseFloat(av3MA)/parseFloat(av3MN);
    }

    //Low
    if(av0LA>0)
    {
        av0L=parseFloat(av0LA)/parseFloat(av0LN);
    }
    if(av1LA>0)
    {
        av1L=parseFloat(av1LA)/parseFloat(av1LN);
    }
    if(av2LA>0)
    {
        av2L=parseFloat(av2LA)/parseFloat(av2LN);
    }
    if(av3LA>0)
    {
        av3L=parseFloat(av3LA)/parseFloat(av3LN);
    }

    //Response
    if(av0RA>0)
    {
        av0R=parseFloat(av0RA)/parseFloat(av0HR);
    }
    if(av1RA>0)
    {
        av1R=parseFloat(av1RA)/parseFloat(av1HR);
    }
    if(av2RA>0)
    {
        av2R=parseFloat(av2RA)/parseFloat(av2HR);
    }
    if(av3RA>0)
    {
        av3R=parseFloat(av3RA)/parseFloat(av3HR);
    }
    if(subtype==5)
    {
        av3H=av3H/24;
        av2H=av2H/24;
        av1H=av1H/24;
        av0H=av0H/24;
        av3M=av3M/24;
        av2M=av2M/24;
        av1M=av1M/24;
        av0M=av0M/24;
        av3L=av3L/24;
        av2L=av2L/24;
        av1L=av1L/24;
        av0L=av0L/24;
        av3R=av3R/24;
        av2R=av2R/24;
        av1R=av1R/24;
        av0R=av0R/24;


    }
    series +='{"type":"column", yAxis: 1,"name":"Email",data: [{y:'+Mth3emailRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - High Priority Case Closure&month=3&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=1&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2emailRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - High Priority Case Closure&month=2&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=1&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1emailRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - High Priority Case Closure&month=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=1&from='+from1+'&to='+to1+'"},' +
        '{y:'+curremailRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - High Priority Case Closure&month=0&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=1&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Phone",data: [{y:'+Mth3phoneRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - High Priority Case Closure&month=3&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=1&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2phoneRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - High Priority Case Closure&month=2&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=1&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1phoneRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - High Priority Case Closure&month=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=1&from='+from1+'&to='+to1+'"},' +
        '{y:'+currphoneRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - High Priority Case Closure&month=0&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=1&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Portal",data: [{y:'+Mth3webRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - High Priority Case Closure&month=3&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=1&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2webRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - High Priority Case Closure&month=2&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=1&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1webRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - High Priority Case Closure&month=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=1&from='+from1+'&to='+to1+'"},' +
        '{y:'+currwebRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - High Priority Case Closure&month=0&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=1&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Matrix",data: [{y:'+Mth3matrixRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - High Priority Case Closure&month=3&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=1&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2matrixRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - High Priority Case Closure&month=2&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=1&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1matrixRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - High Priority Case Closure&month=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=1&from='+from1+'&to='+to1+'"},' +
        '{y:'+currmatrixRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - High Priority Case Closure&month=0&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=1&from='+from0+'&to='+to0+'"}]},' +
        '{"name": "Average Closure Time in Minutes","type": "spline","showInLegend": false,data: [{y:'+parseFloat(av3H).toFixed(1)+',url:"#"}, {y:'+parseFloat(av2H).toFixed(1)+',url:"#"}, {y:'+parseFloat(av1H).toFixed(1)+',url:"#"}, {y:'+parseFloat(av0H).toFixed(1)+',url:"#"}]}';

    seriesmedium +='{"type":"column", yAxis: 1,"name":"Email",data: [{y:'+Mth3emailRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - Medium Priority Case Closure&month=3&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=2&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2emailRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - Medium Priority Case Closure&month=2&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=2&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1emailRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - Medium Priority Case Closure&month=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=2&from='+from1+'&to='+to1+'"},' +
        '{y:'+curremailRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - Medium Priority Case Closure&month=0&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=2&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Phone",data: [{y:'+Mth3phoneRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - Medium Priority Case Closure&month=3&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=2&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2phoneRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - Medium Priority Case priority=4Closure&month=2&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=2&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1phoneRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - Medium Priority Case Closure&month=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=2&from='+from1+'&to='+to1+'"},' +
        '{y:'+currphoneRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - Medium Priority Case Closure&month=0&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=2&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Portal",data: [{y:'+Mth3webRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - Medium Priority Case Closure&month=3&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=2&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2webRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - Medium Priority Case Closure&month=2&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=2&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1webRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - Medium Priority Case Closure&month=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=2&from='+from1+'&to='+to1+'"},' +
        '{y:'+currwebRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - Medium Priority Case Closure&month=0&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=2&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Matrix",data: [{y:'+Mth3matrixRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - Medium Priority Case Closure&month=3&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=2&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2matrixRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - Medium Priority Case priority=4Closure&month=2&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=2&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1matrixRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - Medium Priority Case Closure&month=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=2&from='+from1+'&to='+to1+'"},' +
        '{y:'+currmatrixRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - Medium Priority Case Closure&month=0&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=2&from='+from0+'&to='+to0+'"}]},' +
        '{"name": "Average Closure Time in Minutes","type": "spline","showInLegend": false,data: [{y:'+parseFloat(av3M).toFixed(1)+',url:"#"}, {y:'+parseFloat(av2M).toFixed(1)+',url:"#"}, {y:'+parseFloat(av1M).toFixed(1)+',url:"#"}, {y:'+parseFloat(av0M).toFixed(1)+',url:"#"}]}';

    serieslow +='{"type":"column", yAxis: 1,"name":"Email",data: [{y:'+Mth3emailRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - Low Priority Case Closure&month=3&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=3&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2emailRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - Low Priority Case Closure&month=2&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=3&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1emailRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - Low Priority Case Closure&month=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=3&from='+from1+'&to='+to1+'"},' +
        '{y:'+curremailRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - Low Priority Case Closure&month=0&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=3&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Phone",data: [{y:'+Mth3phoneRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - Low Priority Case Closure&month=3&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=3&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2phoneRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - Low Priority Case Closure&month=2&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=3&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1phoneRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - Low Priority Case Closure&month=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=3&from='+from1+'&to='+to1+'"},' +
        '{y:'+currphoneRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - Low Priority Case Closure&month=0&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=3&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Portal",data: [{y:'+Mth3webRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - Low Priority Case Closure&month=3&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=3&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2webRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - Low Priority Case Closure&month=2&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=3&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1webRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - Low Priority Case Closure&month=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=3&from='+from1+'&to='+to1+'"},' +
        '{y:'+currwebRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - Low Priority Case Closure&month=0&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=3&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Matrix",data: [{y:'+Mth3matrixRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - Low Priority Case Closure&month=3&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=3&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2matrixRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - Low Priority Case Closure&month=2&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=3&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1matrixRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - Low Priority Case Closure&month=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=3&from='+from1+'&to='+to1+'"},' +
        '{y:'+currmatrixRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - Low Priority Case Closure&month=0&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=3&from='+from0+'&to='+to0+'"}]},' +
        '{"name": "Average Closure Time in Minutes","type": "spline","showInLegend": false,data: [{y:'+parseFloat(av3L).toFixed(1)+',url:"#"}, {y:'+parseFloat(av2L).toFixed(1)+',url:"#"}, {y:'+parseFloat(av1L).toFixed(1)+',url:"#"}, {y:'+parseFloat(av0L).toFixed(1)+',url:"#"}]}';

    seriesresponse +='{"type":"column", yAxis: 1,"name":"Email",data: [{y:'+Mth3emailRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&month=3&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=4&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2emailRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&month=2&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=4&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1emailRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&month=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=4&from='+from1+'&to='+to1+'"},' +
        '{y:'+curremailRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&month=0&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=4&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Phone",data: [{y:'+Mth3phoneRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&month=3&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=4&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2phoneRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&month=2&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=4&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1phoneRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&month=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=4&from='+from1+'&to='+to1+'"},' +
        '{y:'+currphoneRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&month=0&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=4&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Portal",data: [{y:'+Mth3webRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&month=3&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=4&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2webRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&month=2&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=4&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1webRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&month=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=4&from='+from1+'&to='+to1+'"},' +
        '{y:'+currwebRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&month=0&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=4&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Matrix",data: [{y:'+Mth3matrixRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&month=3&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=4&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2matrixRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&month=2&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=4&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1matrixRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&month=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=4&from='+from1+'&to='+to1+'"},' +
        '{y:'+currmatrixRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&month=0&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=4&from='+from0+'&to='+to0+'"}]},' +
        '{"name": "Average Closure Time in Minutes","type": "spline","showInLegend": false,data: [{y:'+parseFloat(av3R).toFixed(1)+',url:"#"}, {y:'+parseFloat(av2R).toFixed(1)+',url:"#"}, {y:'+parseFloat(av1R).toFixed(1)+',url:"#"}, {y:'+parseFloat(av0R).toFixed(1)+',url:"#"}]}';


    //incident customsearch_clgx_ss_cases_highppriority=4r
    //customsearch_clgx_ss_cases_highpr_2 RH
// 1 -urgent
    //medium 2Response Time
    //low 3
    //custevent_cologix_sub_case_type
    //  5
    //    [1] = {number} 6

    var seriesArr=[series,seriesmedium,serieslow,seriesresponse];
    return seriesArr;



}
function returnSeriesHigh1(title, market,type,subtype)
{
    var series='';
    var seriesmedium='';
    var serieslow='';
    var seriesresponse='';
    var arrFilters = new Array();
    var arrColumns = new Array();
    var arrFiltersAv = new Array();
    var arrColumnsAv = new Array();
    var arrDatesCurr  = getDateRange(0);
    var arrDatesBf1  = getDateRange(-1);
    var arrDatesBf2  = getDateRange(-2);
    var arrDatesBf3  = getDateRange(-3);
    var from0= arrDatesCurr[0];
    var to0  = arrDatesCurr[1];
    var from1 = arrDatesBf1[0];
    var to1  = arrDatesBf1[1];
    var from2 = arrDatesBf2[0];
    var to2  = arrDatesBf2[1];
    var from3 = arrDatesBf3[0];
    var to3  = arrDatesBf3[1];

    var av0H=0;
    var av0M=0;
    var av0L=0;
    var av1H=0;
    var av1M=0;
    var av1L=0;
    var av2H=0;
    var av2M=0;
    var av2L=0;
    var av3H=0;
    var av3M=0;
    var av3L=0;

    var av0HA=0;
    var av0MA=0;
    var av0LA=0;
    var av1HA=0;
    var av1MA=0;
    var av1LA=0;
    var av2HA=0;
    var av2MA=0;
    var av2LA=0;
    var av3HA=0;
    var av3MA=0;
    var av3LA=0;

    var av0HN=0;
    var av0MN=0;
    var av0LN=0;
    var av1HN=0;
    var av1MN=0;
    var av1LN=0;
    var av2HN=0;
    var av2MN=0;
    var av2LN=0;
    var av3HN=0;
    var av3MN=0;
    var av3LN=0;

    var curremailRHU = 0;
    var Mth1emailRHU = 0;
    var Mth2emailRHU = 0;
    var Mth3emailRHU = 0;
//portal
    var currwebRHU = 0;
    var Mth1webRHU =0;
    var Mth2webRHU = 0;
    var Mth3webRHU = 0;
//phone
    var currphoneRHU = 0;
    var Mth1phoneRHU = 0;
    var Mth2phoneRHU = 0;
    var Mth3phoneRHU = 0;
    //matrix
    var currmatrixRHU = 0;
    var Mth1matrixRHU = 0;
    var Mth2matrixRHU = 0;
    var Mth3matrixRHU = 0;

    var curremailRHM = 0;
    var Mth1emailRHM = 0;
    var Mth2emailRHM = 0;
    var Mth3emailRHM = 0;
//portal
    var currwebRHM = 0;
    var Mth1webRHM = 0;
    var Mth2webRHM = 0;
    var Mth3webRHM = 0;
//phone
    var currphoneRHM = 0;
    var Mth1phoneRHM = 0;
    var Mth2phoneRHM = 0;
    var Mth3phoneRHM = 0;

    //matrix
    var currmatrixRHM = 0;
    var Mth1matrixRHM = 0;
    var Mth2matrixRHM = 0;
    var Mth3matrixRHM = 0;

    var curremailRHL = 0;
    var Mth1emailRHL = 0;
    var Mth2emailRHL = 0;
    var Mth3emailRHL = 0;
//portal
    var currwebRHL = 0;
    var Mth1webRHL = 0;
    var Mth2webRHL = 0;
    var Mth3webRHL = 0;
//phone
    var currphoneRHL = 0;
    var Mth1phoneRHL = 0;
    var Mth2phoneRHL = 0;
    var Mth3phoneRHL = 0;
    //matrix
    var currmatrixRHL = 0;
    var Mth1matrixRHL = 0;
    var Mth2matrixRHL = 0;
    var Mth3matrixRHL = 0;


    var curremailRHR = 0;
    var Mth1emailRHR = 0;
    var Mth2emailRHR = 0;
    var Mth3emailRHR = 0;
//portal
    var currwebRHR = 0;
    var Mth1webRHR = 0;
    var Mth2webRHR = 0;
    var Mth3webRHR = 0;
//matrix
    var currphoneRHR = 0;
    var Mth1phoneRHR = 0;
    var Mth2phoneRHR = 0;
    var Mth3phoneRHR = 0;
    //matrix
    var currmatrixRHR = 0;
    var Mth1matrixRHR = 0;
    var Mth2matrixRHR = 0;
    var Mth3matrixRHR = 0;
    //1 RH
    //2 Incident
    if(market!=0)
    {
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_reporting_locations","custevent_clgx_case_location","anyof",market));
    }
    if(type!=0)
    {
        if(type=='incident')
        {
            arrFilters.push(new nlobjSearchFilter("type",null,"anyof",2));
        }
        if(type=='rh')
        {
            arrFilters.push(new nlobjSearchFilter("type",null,"anyof",1));
        }
    }
    if(subtype!=0)
    {

        arrFilters.push(new nlobjSearchFilter("custevent_cologix_sub_case_type",null,"anyof",subtype));

    }
    var start0= moment(from0, "MM/DD/YYYY");
    var end0= moment(to0, "MM/DD/YYYY");
    var start1= moment(from1, "MM/DD/YYYY");
    var end1= moment(to1, "MM/DD/YYYY");
    var start2= moment(from2, "MM/DD/YYYY");
    var end2= moment(to2, "MM/DD/YYYY");
    var start3= moment(from3, "MM/DD/YYYY");
    var end3= moment(to3, "MM/DD/YYYY");
    arrFilters.push(new nlobjSearchFilter("createddate",null,"within",from3,to0));
    var searchCases = nlapiLoadSearch('supportcase', 'customsearch_clgx_ss_cases_highpr_3_2');
    searchCases.addColumns(arrColumns);
    searchCases.addFilters(arrFilters);
    var resultCS = searchCases.runSearch();
    var idArray=new Array();
    resultCS.forEachResult(function(searchResult) {
        var columns = searchResult.getAllColumns();
        var date=new Date(searchResult.getValue(columns[1]));
        var priority=searchResult.getText(columns[2]);
        var origin=searchResult.getText(columns[3]);
        var datestr=(parseInt(date.getMonth())+parseInt(1))+'/'+date.getDate()+'/'+date.getFullYear();
        var date = moment(datestr, "MM/DD/YYYY");
        if(priority=='Urgent')
        {
            if((date >= start0) && (date <= end0))
            {

                if(origin=="E-mail")
                {
                    curremailRHU++;
                    curremailRHR++;
                }
                if(origin=="Phone")
                {
                    currphoneRHU++;
                    currphoneRHR++;
                }
                if(origin=="Matrix")
                {
                    currmatrixRHU++;
                    currmatrixRHR++;
                }
                if(origin=="Web")
                {
                    currwebRHU++;
                    currwebRHR++;
                }
            }
            if((date >= start1) && (date <= end1))
            {

                if(origin=="E-mail")
                {
                    Mth1emailRHU++;
                    Mth1emailRHR++;
                }
                if(origin=="Phone")
                {
                    Mth1phoneRHU++;
                    Mth1phoneRHR++;
                }
                if(origin=="Matrix")
                {
                    Mth1matrixRHU++;
                    Mth1matrixRHR++;
                }
                if(origin=="Web")
                {
                    Mth1webRHU++;
                    Mth1webRHR++;
                }
            }
            if((date >= start2) && (date <= end2))
            {

                if(origin=="E-mail")
                {
                    Mth2emailRHU++;
                    Mth2emailRHR++;
                }
                if(origin=="Phone")
                {
                    Mth2phoneRHU++;
                    Mth2phoneRHR++;
                }
                if(origin=="Matrix")
                {
                    Mth2matrixRHU++;
                    Mth2matrixRHR++;
                }
                if(origin=="Web")
                {
                    Mth2webRHU++;
                    Mth2webRHR++;
                }
            }
            if((date >= start3) && (date <= end3))
            {

                if(origin=="E-mail")
                {
                    Mth3emailRHU++;
                    Mth3emailRHR++;
                }
                if(origin=="Phone")
                {
                    Mth3phoneRHU++;
                    Mth3phoneRHR++;
                }
                if(origin=="Matrix")
                {
                    Mth3matrixRHU++;
                    Mth3matrixRHR++;
                }
                if(origin=="Web")
                {
                    Mth3webRHU++;
                    Mth3webRHR++;
                }
            }
        }
//Medium
        if(priority=='Medium')
        {
            if((date >= start0) && (date <= end0))
            {

                if(origin=="E-mail")
                {
                    curremailRHM++;
                    curremailRHR++;
                }
                if(origin=="Phone")
                {
                    currphoneRHM++;
                    currphoneRHR++;
                }
                if(origin=="Matrix")
                {
                    currmatrixRHM++;
                    currmatrixRHR++;
                }
                if(origin=="Web")
                {
                    currwebRHM++;
                    currwebRHR++;
                }
            }
            if((date >= start1) && (date <= end1))
            {

                if(origin=="E-mail")
                {
                    Mth1emailRHM++;
                    Mth1emailRHR++;
                }
                if(origin=="Phone")
                {
                    Mth1phoneRHM++;
                    Mth1phoneRHR++;
                }
                if(origin=="Matrix")
                {
                    Mth1matrixRHM++;
                    Mth1matrixRHR++;
                }
                if(origin=="Web")
                {
                    Mth1webRHM++;
                    Mth1webRHR++;
                }
            }

            if((date >= start2) && (date <= end2))
            {

                if(origin=="E-mail")
                {
                    Mth2emailRHM++;
                    Mth2emailRHR++;
                }
                if(origin=="Phone")
                {
                    Mth2phoneRHM++;
                    Mth2phoneRHR++;
                }
                if(origin=="Matrix")
                {
                    Mth2matrixRHM++;
                    Mth2matrixRHR++;
                }
                if(origin=="Web")
                {
                    Mth2webRHM++;
                    Mth2webRHR++;
                }
            }
            if((date >= start3) && (date <= end3))
            {

                if(origin=="E-mail")
                {
                    Mth3emailRHM++;
                    Mth3emailRHR++;
                }
                if(origin=="Phone")
                {
                    Mth3phoneRHM++;
                    Mth3phoneRHR++;
                }
                if(origin=="Matrix")
                {
                    Mth3matrixRHM++;
                    Mth3matrixRHR++;
                }
                if(origin=="Web")
                {
                    Mth3webRHM++;
                    Mth3webRHR++;
                }
            }
        }
//Low
        if(priority=='Low')
        {
            if((date >= start0) && (date <= end0))
            {

                if(origin=="E-mail")
                {
                    curremailRHL++;
                    curremailRHR++;
                }
                if(origin=="matrix")
                {
                    currphoneRHL++;
                    currphoneRHR++;
                }
                if(origin=="Matrix")
                {
                    currmatrixRHL++;
                    currmatrixRHR++;
                }
                if(origin=="Web")
                {
                    currwebRHL++;
                    currwebRHR++;
                }
            }
            if((date >= start1) && (date <= end1))
            {

                if(origin=="E-mail")
                {
                    Mth1emailRHL++;
                    Mth1emailRHR++;
                }
                if(origin=="Phone")
                {
                    Mth1phoneRHL++;
                    Mth1phoneRHR++;
                }
                if(origin=="Matrix")
                {
                    Mth1matrixRHL++;
                    Mth1matrixRHR++;
                }
                if(origin=="Web")
                {
                    Mth1webRHL++;
                    Mth1webRHR++;
                }
            }
            if((date >= start2) && (date <= end2))
            {

                if(origin=="E-mail")
                {
                    Mth2emailRHL++;
                    Mth2emailRHR++;
                }
                if(origin=="Phone")
                {
                    Mth2phoneRHL++;
                    Mth2phoneRHR++;
                }
                if(origin=="Matrix")
                {
                    Mth2matrixRHL++;
                    Mth2matrixRHR++;
                }
                if(origin=="Web")
                {
                    Mth2webRHL++;
                    Mth2webRHR++;
                }
            }
            if((date >= start3) && (date <= end3))
            {

                if(origin=="E-mail")
                {
                    Mth3emailRHL++;
                    Mth3emailRHR++;
                }
                if(origin=="Phone")
                {
                    Mth3phoneRHL++;
                    Mth3phoneRHR++;
                }
                if(origin=="Matrix")
                {
                    Mth3matrixRHL++;
                    Mth3matrixRHR++;
                }
                if(origin=="Web")
                {
                    Mth3webRHL++;
                    Mth3webRHR++;
                }
            }
        }
        return true; // return true to keep iterating

    });


    series +='{"type":"column", yAxis: 1,"name":"Email",data: [{y:'+Mth3emailRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - High Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=1&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2emailRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - High Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=1&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1emailRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - High Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=1&from='+from1+'&to='+to1+'"},' +
        '{y:'+curremailRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - High Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=1&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Phone",data: [{y:'+Mth3phoneRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - High Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=1&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2phoneRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - High Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=1&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1phoneRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - High Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=1&from='+from1+'&to='+to1+'"},' +
        '{y:'+currphoneRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - High Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=1&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Portal",data: [{y:'+Mth3webRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - High Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=1&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2webRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - High Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=1&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1webRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - High Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=1&from='+from1+'&to='+to1+'"},' +
        '{y:'+currwebRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - High Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=1&from='+from0+'&to='+to0+'"}]},'+
        '{"type":"column", yAxis: 1,"name":"Matrix",data: [{y:'+Mth3matrixRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - High Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=1&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2matrixRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - High Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=1&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1matrixRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - High Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=1&from='+from1+'&to='+to1+'"},' +
        '{y:'+currmatrixRHU+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - High Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=1&from='+from0+'&to='+to0+'"}]}' ;

    seriesmedium +='{"type":"column", yAxis: 1,"name":"Email",data: [{y:'+Mth3emailRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - Medium Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=2&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2emailRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - Medium Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=2&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1emailRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - Medium Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=2&from='+from1+'&to='+to1+'"},' +
        '{y:'+curremailRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - Medium Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=2&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Phone",data: [{y:'+Mth3phoneRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - Medium Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=2&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2phoneRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - Medium Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=2&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1phoneRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - Medium Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=2&from='+from1+'&to='+to1+'"},' +
        '{y:'+currphoneRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - Medium Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=2&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Portal",data: [{y:'+Mth3webRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - Medium Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=2&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2webRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - Medium Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=2&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1webRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - Medium Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=2&from='+from1+'&to='+to1+'"},' +
        '{y:'+currwebRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - Medium Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=2&from='+from0+'&to='+to0+'"}]},'+
        '{"type":"column", yAxis: 1,"name":"Matrix",data: [{y:'+Mth3matrixRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - Medium Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=2&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2matrixRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - Medium Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=2&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1matrixRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - Medium Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=2&from='+from1+'&to='+to1+'"},' +
        '{y:'+currmatrixRHM+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - Medium Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=2&from='+from0+'&to='+to0+'"}]}';


    serieslow +='{"type":"column", yAxis: 1,"name":"Email",data: [{y:'+Mth3emailRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - Low Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=3&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2emailRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - Low Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=3&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1emailRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - Low Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=3&from='+from1+'&to='+to1+'"},' +
        '{y:'+curremailRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Email - Low Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=3&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Phone",data: [{y:'+Mth3phoneRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - Low Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=3&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2phoneRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - Low Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=3&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1phoneRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - Low Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=3&from='+from1+'&to='+to1+'"},' +
        '{y:'+currphoneRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Phone - Low Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=3&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Portal",data: [{y:'+Mth3webRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - Low Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=3&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2webRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - Low Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=3&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1webRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - Low Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=3&from='+from1+'&to='+to1+'"},' +
        '{y:'+currwebRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Portal - Low Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=3&from='+from0+'&to='+to0+'"}]},'+
        '{"type":"column", yAxis: 1,"name":"Matrix",data: [{y:'+Mth3matrixRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - Low Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=3&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2matrixRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - Low Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=3&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1matrixRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - Low Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=3&from='+from1+'&to='+to1+'"},' +
        '{y:'+currmatrixRHL+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Matrix - Low Priority Case Closure&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=3&from='+from0+'&to='+to0+'"}]}' ;

    seriesresponse +='{"type":"column", yAxis: 1,"name":"Email",data: [{y:'+Mth3emailRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=4&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2emailRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=4&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1emailRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=4&from='+from1+'&to='+to1+'"},' +
        '{y:'+curremailRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=1&priority=4&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Phone",data: [{y:'+Mth3phoneRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=4&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2phoneRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=4&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1phoneRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=4&from='+from1+'&to='+to1+'"},' +
        '{y:'+currphoneRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=2&priority=4&from='+from0+'&to='+to0+'"}]},' +
        '{"type":"column", yAxis: 1,"name":"Portal",data: [{y:'+Mth3webRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=4&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2webRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=4&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1webRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=4&from='+from1+'&to='+to1+'"},' +
        '{y:'+currwebRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=-5&priority=4&from='+from0+'&to='+to0+'"}]},'+
         '{"type":"column", yAxis: 1,"name":"Matrix",data: [{y:'+Mth3matrixRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=4&from='+from3+'&to='+to3+'"},' +
        '{y:'+Mth2matrixRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=4&from='+from2+'&to='+to2+'"},' +
        '{y:'+Mth1matrixRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=4&from='+from1+'&to='+to1+'"},' +
        '{y:'+currmatrixRHR+',url:"/app/site/hosting/scriptlet.nl?script=554&deploy=1&title=Response Time&open=1&location='+market+'&type='+type+'&subtype='+subtype+'&origin=7&priority=4&from='+from0+'&to='+to0+'"}]}' ;



    //incident customsearch_clgx_ss_cases_highpr
    //customsearch_clgx_ss_cases_highpr_2 RH
// 1 -urgent
    //medium 2
    //low 3
    //custevent_cologix_sub_case_type
    //  5
    //    [1] = {number} 6

    var seriesArr=[series,seriesmedium,serieslow,seriesresponse];
    return seriesArr;



}

function getDateRange(monthsago){

    var today = new Date();
    var date = nlapiAddMonths(today, monthsago);

    var month = parseInt(date.getMonth());
    var year = date.getFullYear();

    var stDays  = daysInMonth(parseInt(month),parseInt(year));
    var stYear  = year;

    var stMonth = parseInt(month) + 1;

    var stStartDate = stMonth + '/1/' + stYear;
    var stEndDate   = stMonth + '/' + stDays + '/' + stYear;

    var arrDateRange = new Array();
    arrDateRange[0] = stStartDate;
    arrDateRange[1] =stEndDate;

    return arrDateRange;
}


function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}