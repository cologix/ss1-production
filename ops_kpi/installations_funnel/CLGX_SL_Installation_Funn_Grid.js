nlapiLogExecution("audit","FLOStart",new Date().getTime());
function getseries(request, response)
{

    var title = request.getParameter('title');
    var location = request.getParameter('location');
    var cattype=request.getParameter('cattype');
    var product = request.getParameter('product');
    var from = request.getParameter('from');
    var to = request.getParameter('to');
    // product=10 //space
    // product=5//net
    // product=11//vxc
    // product=4//xc
    // product=8//power
    var target=5;
    var targetP="F";
    if(product==11)//vxc
    {
        targetP="F";
        target=0;
    }
    if(product==10 || product==5 || product==4 ||product==8)
    {
        targetP="T";
    }
    if(product==4)//xc
    {

        target=2;
    }
    var productarr=[product];
    if(product==0)
    {
        var productarr=[10,11,4,5,8];
    }
    var cattypearr=[cattype];
    if(cattype=='all')
    {
        var cattypearr=[1,2];
    }

    var objFile = nlapiLoadFile(3097429);
    var html = objFile.getValue();
    html = html.replace(new RegExp('{title}','g'), 'Installations Funnel Details- ' + title);
    html = html.replace(new RegExp('{series}','g'), getSeriesGrid(cattype,location,productarr,from,to, targetP,target));
    response.write( html );
}
function getSeriesGrid(cattype,location,productarr,from,to, targetP,target)
{
    var  arrFilters=new Array();
    var  arrColumns=new Array();
    var series='';
   if(from!="past") {
       arrFilters.push(new nlobjSearchFilter("custbody_clgx_target_install_date", null, "within", from, to));
   }
      if(productarr.length>0)
     {
       arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category","item","anyof",productarr));
     }

    if(location>1)
    {
        var locarr=clgx_return_child_locations_of_marketid (location);
        if(locarr.length==0)
        {
            locarr.push(location);
        }
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",locarr));
    }


    if(from=="past") {
        var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_so_install_vol_loc__11', arrFilters, arrColumns);
    }
    else
    {
        var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_so_install_vol_loc___2', arrFilters, arrColumns);
    }
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var customer = searchIn.getText(columns[0]);
        var so = searchIn.getValue(columns[1]);
        var soid = searchIn.getValue(columns[2]);
        var assi = searchIn.getText(columns[3]);
        var totalmmr = searchIn.getValue(columns[4]);
        var totalnrc = searchIn.getValue(columns[5]);
        var loc = searchIn.getValue(columns[6]);
        var actualinstd = searchIn.getValue(columns[7]);
        var nStandard = searchIn.getValue(columns[8]);
        var standard = searchIn.getValue(columns[9]);
        var itemcat = searchIn.getValue(columns[10]);
        //   var installdur=searchIn.getValue(columns[11]);

        var itemcatArr = itemcat.split(',');

        if (cattype == "all") {
            series += '\n{"CUSTOMER":"' + customer +
                '","SO":"' + so +
                '","SOID":' + soid +
                ',"TARGETP":"' + targetP +
                '","TARGET":' + target +
                ',"ASSIGNEDTECHNICIAN":"' + assi +
                '","TOTALMMR":' + totalmmr +
                ',"TOTALNRC":' + totalnrc +
                ',"LOCATION":"' + loc +
                '","ACTUALINSTALLDATE":"' + actualinstd +
                '"},';
        }
        else if (cattype == "standard") {
            if (nStandard == 0) {
                series += '\n{"CUSTOMER":"' + customer +
                    '","SO":"' + so +
                    '","SOID":' + soid +
                    ',"TARGETP":"' + targetP +
                    '","TARGET":' + target +
                    ',"ASSIGNEDTECHNICIAN":"' + assi +
                    '","TOTALMMR":' + totalmmr +
                    ',"TOTALNRC":' + totalnrc +
                    ',"LOCATION":"' + loc +
                    '","ACTUALINSTALLDATE":"' + actualinstd +
                    '"},';
            }

        }
        else if (cattype == "nstandard") {
            if (((nStandard > 0) && (standard > 0)) || (nStandard > 0)) {
                series += '\n{"CUSTOMER":"' + customer +
                    '","SO":"' + so +
                    '","SOID":' + soid +
                    ',"TARGETP":"' + targetP +
                    '","TARGET":' + target +
                    ',"ASSIGNEDTECHNICIAN":"' + assi +
                    '","TOTALMMR":' + totalmmr +
                    ',"TOTALNRC":' + totalnrc +
                    ',"LOCATION":"' + loc +
                    '","ACTUALINSTALLDATE":"' + actualinstd +
                    '"},';
            }

        }
    }


    var strLen = series.length;
    if (searchIns != null){
        series = series.slice(0,strLen-1);
    }
    return series;
}

function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}