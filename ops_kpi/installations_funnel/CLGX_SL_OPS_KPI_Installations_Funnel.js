nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_OPS_KPI_Installations_Locations.js
//	Script Name:	CLGX_SL_OPS_KPI_Installations_Locations
//	Script Id:		customscript_clgx_sl_ops_kpi_inst_loc
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		06/24/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=510&deploy=1
//-------------------------------------------------------------------------------------------------

function get_series_funnel (request, response){
    try {

        var script = request.getParameter('script');
        var type = request.getParameter('type');
        var category = request.getParameter('category');
        var tarw=0;
        var tarv=0;

        var chartype = 'column';
        if(type == 'avg'){
            chartype = 'spline';
            tarw=1;
        }


        var title = request.getParameter('title');
        var title1 = request.getParameter('title');
        var location = request.getParameter('location');
        var cattype=request.getParameter('cattype');
        var product = request.getParameter('product');
        var titleav="Standard";
        if(cattype=='standard')
        {
            titleav="Non-Standard";
        }
        if(cattype=='standard')
        {
            if(product=="space"||product=="power"||product=="network")
            {
                tarv=5;
            }
            if(product=="xc")
            {
                tarv=2;
            }
        }
        var objFile = nlapiLoadFile(3094644);

        var html = objFile.getValue();
        html = html.replace(new RegExp('{weeks}','g'), return_weeks());
        html = html.replace(new RegExp('{chartype}','g'), chartype);
        html = html.replace(new RegExp('{averagetitle}','g'), titleav);
        html = html.replace(new RegExp('{targetwidth}','g'), tarw);
        html = html.replace(new RegExp('{targetvalue}','g'), tarv);
        html = html.replace(new RegExp('{title}','g'), 'Installation Funnel - ' + title);
        if(title=='Standard'){
            title='ALL';
        }
        if(title=='By Products'){
            title='ALL';
        }
        if(title=='Non-Standard'){
            title='ALL';
        }

        html = html.replace(new RegExp('{series}','g'), return_series(title1,location,cattype,product));

        response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function return_series(title,location,cattype,product){
    var series='';
    var arrFilters = new Array();
    var arrColumns = new Array();
    var arrFilters1 = new Array();
    var arrColumns1 = new Array();
    var arrLocations=['Columbus','Dallas','Jacksonville','Jacksonville2','Lakeland','Minneapolis ','Montreal','Toronto','Vancouver','COL1','COL3','DAL1','DAL2','JAX1','JAX2','LAK1','MIN1','MIN3','MTL1','MTL2','MTL3','MTL4','MTL5','MTL6','MTL7','TOR1','TOR2','VAN1','VAN2'];
    var arrProducts=['Space','Power','XC','Virtual XC','Network'];
    var location1=0;
    var catGrid=cattype;
    var locGrid=location;
    if((in_array(title,arrProducts))||(title=="By Products")||(product=="all")) {
        location1=1;
        var cat=0;
        if(product=="space"){
            cat=10;
        }
        if(product=="power"){
            cat=8;
        }
        if(product=="xc"){
            cat=4;
        }
        if(product=="vxc"){
            cat=11;
        }
        if(product=="network"){
            cat=5;
        }
        var productCat=cat;

        if(cat!=0) {
            arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category", "item", "anyof", cat));
            arrFilters1.push(new nlobjSearchFilter("custitem_cologix_item_category", "item", "anyof", cat));
        }
    }


    if((in_array(title,arrLocations))||(title=="By Locations")) {
        if(location>1)
        {
            var locationarr=clgx_return_child_locations_of_marketid (location);
            if(locationarr.length==0)
            {
                locationarr.push(location);
            }
            arrFilters.push(new nlobjSearchFilter("location", null, "anyof", locationarr));
            arrFilters1.push(new nlobjSearchFilter("location", null, "anyof", locationarr));
        }
    }


    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_so_install_vol_loc_s_9', arrFilters, arrColumns);
    var searchIns1 = nlapiSearchRecord('transaction', 'customsearch_clgx_so_install_vol_loc__10', arrFilters1, arrColumns1);

    //Columbus
    var volCol_0=0;
    var volCol_1=0;
    var volCol_3=0;
    var volCol_4=0;
    var volCol_5=0;
    var volCol_6=0;
    var volCol_7=0;
    var volCol_8=0;
    var volCol_9=0;
    var volCol_10=0;
    var volCol_11=0;
    var volCol_12=0;
    //Dallas
    var volDal_0=0;
    var volDal_1=0;
    var volDal_3=0;
    var volDal_4=0;
    var volDal_5=0;
    var volDal_6=0;
    var volDal_7=0;
    var volDal_8=0;
    var volDal_9=0;
    var volDal_10=0;
    var volDal_11=0;
    var volDal_12=0;
    //JaK
    var volJAK_0=0;
    var volJAK_1=0;
    var volJAK_3=0;
    var volJAK_4=0;
    var volJAK_5=0;
    var volJAK_6=0;
    var volJAK_7=0;
    var volJAK_8=0;
    var volJAK_9=0;
    var volJAK_10=0;
    var volJAK_11=0;
    var volJAK_12=0;
    //JaK2
    var volJAK2_0=0;
    var volJAK2_1=0;
    var volJAK2_3=0;
    var volJAK2_4=0;
    var volJAK2_5=0;
    var volJAK2_6=0;
    var volJAK2_7=0;
    var volJAK2_8=0;
    var volJAK2_9=0;
    var volJAK2_10=0;
    var volJAK2_11=0;
    var volJAK2_12=0;
    //LAK
    var volLAK_0=0;
    var volLAK_1=0;
    var volLAK_3=0;
    var volLAK_4=0;
    var volLAK_5=0;
    var volLAK_6=0;
    var volLAK_7=0;
    var volLAK_8=0;
    var volLAK_9=0;
    var volLAK_10=0;
    var volLAK_11=0;
    var volLAK_12=0;
    //MIN
    var volMIN_0=0;
    var volMIN_1=0;
    var volMIN_3=0;
    var volMIN_4=0;
    var volMIN_5=0;
    var volMIN_6=0;
    var volMIN_7=0;
    var volMIN_8=0;
    var volMIN_9=0;
    var volMIN_10=0;
    var volMIN_11=0;
    var volMIN_12=0;
    //MTL
    var volMTL_0=0;
    var volMTL_1=0;
    var volMTL_3=0;
    var volMTL_4=0;
    var volMTL_5=0;
    var volMTL_6=0;
    var volMTL_7=0;
    var volMTL_8=0;
    var volMTL_9=0;
    var volMTL_10=0;
    var volMTL_11=0;
    var volMTL_12=0;

    //NNJ
    var volNNJ_0=0;
    var volNNJ_1=0;
    var volNNJ_3=0;
    var volNNJ_4=0;
    var volNNJ_5=0;
    var volNNJ_6=0;
    var volNNJ_7=0;
    var volNNJ_8=0;
    var volNNJ_9=0;
    var volNNJ_10=0;
    var volNNJ_11=0;
    var volNNJ_12=0;
    //TOR
    var volTOR_0=0;
    var volTOR_1=0;
    var volTOR_3=0;
    var volTOR_4=0;
    var volTOR_5=0;
    var volTOR_6=0;
    var volTOR_7=0;
    var volTOR_8=0;
    var volTOR_9=0;
    var volTOR_10=0;
    var volTOR_11=0;
    var volTOR_12=0;
    //VAN
    var volVAN_0=0;
    var volVAN_1=0;
    var volVAN_3=0;
    var volVAN_4=0;
    var volVAN_5=0;
    var volVAN_6=0;
    var volVAN_7=0;
    var volVAN_8=0;
    var volVAN_9=0;
    var volVAN_10=0;
    var volVAN_11=0;
    var volVAN_12=0;
    //Space
    var volSP_0=0;
    var volSP_1=0;
    var volSP_3=0;
    var volSP_4=0;
    var volSP_5=0;
    var volSP_6=0;
    var volSP_7=0;
    var volSP_8=0;
    var volSP_9=0;
    var volSP_10=0;
    var volSP_11=0;
    var volSP_12=0;
    //Power
    var volPW_0=0;
    var volPW_1=0;
    var volPW_3=0;
    var volPW_4=0;
    var volPW_5=0;
    var volPW_6=0;
    var volPW_7=0;
    var volPW_8=0;
    var volPW_9=0;
    var volPW_10=0;
    var volPW_11=0;
    var volPW_12=0;
    //XC
    var volXC_0=0;
    var volXC_1=0;
    var volXC_3=0;
    var volXC_4=0;
    var volXC_5=0;
    var volXC_6=0;
    var volXC_7=0;
    var volXC_8=0;
    var volXC_9=0;
    var volXC_10=0;
    var volXC_11=0;
    var volXC_12=0;
    //VXC
    var volVXC_0=0;
    var volVXC_1=0;
    var volVXC_3=0;
    var volVXC_4=0;
    var volVXC_5=0;
    var volVXC_6=0;
    var volVXC_7=0;
    var volVXC_8=0;
    var volVXC_9=0;
    var volVXC_10=0;
    var volVXC_11=0;
    var volVXC_12=0;
    //Network
    var volNW_0=0;
    var volNW_1=0;
    var volNW_3=0;
    var volNW_4=0;
    var volNW_5=0;
    var volNW_6=0;
    var volNW_7=0;
    var volNW_8=0;
    var volNW_9=0;
    var volNW_10=0;
    var volNW_11=0;
    var volNW_12=0;
    var ids=new Array();
    var ids1=new Array();
    var idsST=new Array();
    var ids1ST=new Array();
    var idsNST=new Array();
    var ids1NST=new Array();





    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var cat = searchIn.getText(columns[0]);
        //var date= new Date(searchIn.getValue(columns[2]));
        var id = searchIn.getValue(columns[1]);
        var nstandard = searchIn.getValue(columns[3]);
        var itemcat = searchIn.getValue(columns[4]);
        var itemcatArr = itemcat.split(',');

        //BY LOCATIONS
        if(cattype=='all' && location1==1) {
            if (cat == 'Columbus') {
                volCol_0++;
            }
            if(cat=='Dallas')
            {
                volDal_0++;
            }
            if(cat=='Jacksonville')
            {
                volJAK_0++;
            }
            if(cat=='Jacksonville2')
            {
                volJAK2_0++;
            }
            if(cat=='Lakeland')
            {
                volLAK_0++;
            }
            if(cat=='Minneapolis ')
            {
                volMIN_0++;
            }
            if(cat=='Montreal')
            {
                volMTL_0++;
            }
            if(cat=='New Jersey')
            {
                volNNJ_0++;
            }
            if(cat=='Toronto')
            {
                volTOR_0++;
            }
            if(cat=='Vancouver')
            {
                volVAN_0++;
            }
        }
        //standard
        if(cattype=='standard' && location1==1) {
            if (cat == 'Columbus' && nstandard==0) {
                volCol_0++;
            }
            if(cat=='Dallas' && nstandard==0)
            {
                volDal_0++;
            }
            if(cat=='Jacksonville' && nstandard==0)
            {
                volJAK_0++;
            }
            if(cat=='Jacksonville2' && nstandard==0)
            {
                volJAK2_0++;
            }
            if(cat=='Lakeland' && nstandard==0)
            {
                volLAK_0++;
            }
            if(cat=='Minneapolis ' && nstandard==0)
            {
                volMIN_0++;
            }
            if(cat=='Montreal' && nstandard==0)
            {
                volMTL_0++;
            }
            if(cat=='New Jersey' && nstandard==0)
            {
                volNNJ_0++;
            }
            if(cat=='Toronto' && nstandard==0)
            {
                volTOR_0++;
            }
            if(cat=='Vancouver' && nstandard==0)
            {
                volVAN_0++;
            }
        }
        //standard
        if(cattype=='nstandard' && location1==1) {
            if (cat == 'Columbus' && nstandard>0) {
                volCol_0++;
            }
            if(cat=='Dallas' && nstandard>0)
            {
                volDal_0++;
            }
            if(cat=='Jacksonville' && nstandard>0)
            {
                volJAK_0++;
            }
            if(cat=='Jacksonville2' && nstandard>0)
            {
                volJAK2_0++;
            }
            if(cat=='Lakeland' && nstandard>0)
            {
                volLAK_0++;
            }
            if(cat=='Minneapolis ' && nstandard>0)
            {
                volMIN_0++;
            }
            if(cat=='Montreal' && nstandard>0)
            {
                volMTL_0++;
            }
            if(cat=='New Jersey' && nstandard>0)
            {
                volNNJ_0++;
            }
            if(cat=='Toronto' && nstandard>0)
            {
                volTOR_0++;
            }
            if(cat=='Vancouver' && nstandard>0)
            {
                volVAN_0++;
            }
        }

        //BY PRODUCTS

        if(cattype=='all' && location1==0) {

            if(in_array("Space",itemcatArr) && !in_array(id,ids))
            {
                volSP_0++;
                if(cat==0)
                {
                    ids.push(id);
                }
            }
            if(in_array("Network",itemcatArr) && !in_array(id,ids))
            {
                volNW_0++;
                if(cat==0)
                {
                    ids.push(id);
                }
            }
            if(in_array("Power",itemcatArr) && !in_array(id,ids))
            {
                volPW_0++;
                if(cat==0)
                {
                    ids.push(id);
                }
            }
            if(in_array("Interconnection",itemcatArr) && !in_array(id,ids))
            {
                volXC_0++;
                if(cat==0)
                {
                    ids.push(id);
                }
            }
            if(in_array("Virtual Interconnection",itemcatArr) && !in_array(id,ids))
            {
                volVXC_0++;
                if(cat==0)
                {
                    ids.push(id);
                }
            }


        }
        if(cattype=='standard' && location1==0) {

            if(in_array("Space",itemcatArr) && nstandard==0 && !in_array(id,idsST))
            {
                volSP_0++;
                if(cat==0)
                {
                    idsST.push(id);
                }
            }
            if(in_array("Network",itemcatArr) && nstandard==0 && !in_array(id,idsST))
            {
                volNW_0++;
                if(cat==0)
                {
                    idsST.push(id);
                }
            }
            if(in_array("Power",itemcatArr) && nstandard==0 && !in_array(id,idsST))
            {
                volPW_0++;
                if(cat==0)
                {
                    idsST.push(id);
                }
            }
            if(in_array("Interconnection",itemcatArr) && nstandard==0 && !in_array(id,idsST))
            {
                volXC_0++;
                if(cat==0)
                {
                    idsST.push(id);
                }
            }
            if(in_array("Virtual Interconnection",itemcatArr) && nstandard==0 && !in_array(id,idsST))
            {
                volVXC_0++;
                if(cat==0)
                {
                    idsST.push(id);
                }
            }


        }
        if(cattype=='nstandard' && location1==0) {

            if(in_array("Space",itemcatArr) && nstandard>0 && !in_array(id,idsNST))
            {
                volSP_0++;
                if(cat==0)
                {
                    idsNST.push(id);
                }
            }
            if(in_array("Network",itemcatArr) && nstandard>0 && !in_array(id,idsNST))
            {
                volNW_0++;
                if(cat==0)
                {
                    idsNST.push(id);
                }
            }
            if(in_array("Power",itemcatArr) && nstandard>0 && !in_array(id,idsNST))
            {
                volPW_0++;
                if(cat==0)
                {
                    idsNST.push(id);
                }
            }
            if(in_array("Interconnection",itemcatArr) && nstandard>0 && !in_array(id,idsNST))
            {
                volXC_0++;
                if(cat==0)
                {
                    idsNST.push(id);
                }
            }
            if(in_array("Virtual Interconnection",itemcatArr) && nstandard>0 && !in_array(id,idsNST))
            {
                volVXC_0++;
                if(cat==0)
                {
                    idsNST.push(id);
                }
            }


        }
    }
    var getWeeks=return_weeksforSearch ();


    for ( var i = 0; searchIns1 != null && i < searchIns1.length; i++ ) {
        var searchIn = searchIns1[i];
        var columns = searchIn.getAllColumns();
        var cat = searchIn.getText(columns[0]);
        var date= new Date(searchIn.getValue(columns[2]));
        var datestr=(parseInt(date.getMonth())+parseInt(1))+'/'+date.getDate()+'/'+date.getFullYear();
        var date = moment(datestr, "MM/DD/YYYY");
        var nstandard = searchIn.getValue(columns[3]);
        var itemcat = searchIn.getValue(columns[4]);
        var itemcatArr = itemcat.split(',');
        if((date >= moment(getWeeks[0], "MM/DD/YYYY")) && (date <= moment(getWeeks[1], "MM/DD/YYYY")))
        {
            //BY LOCATIONS
            if(cattype=='all' && location1==1) {
                if (cat == 'Columbus') {
                    volCol_1++;
                }
                if(cat=='Dallas')
                {
                    volDal_1++;
                }
                if(cat=='Jacksonville')
                {
                    volJAK_1++;
                }
                if(cat=='Jacksonville2')
                {
                    volJAK2_1++;
                }
                if(cat=='Lakeland')
                {
                    volLAK_1++;
                }
                if(cat=='Minneapolis ')
                {
                    volMIN_1++;
                }
                if(cat=='Montreal')
                {
                    volMTL_1++;
                }
                if(cat=='New Jersey')
                {
                    volNNJ_1++;
                }
                if(cat=='Toronto')
                {
                    volTOR_1++;
                }
                if(cat=='Vancouver')
                {
                    volVAN_1++;
                }
            }
            //standard
            if(cattype=='standard' && location1==1) {
                if (cat == 'Columbus' && nstandard==0) {
                    volCol_1++;
                }
                if(cat=='Dallas' && nstandard==0)
                {
                    volDal_1++;
                }
                if(cat=='Jacksonville' && nstandard==0)
                {
                    volJAK_1++;
                }
                if(cat=='Jacksonville2' && nstandard==0)
                {
                    volJAK2_1++;
                }
                if(cat=='Lakeland' && nstandard==0)
                {
                    volLAK_1++;
                }
                if(cat=='Minneapolis ' && nstandard==0)
                {
                    volMIN_1++;
                }
                if(cat=='Montreal' && nstandard==0)
                {
                    volMTL_1++;
                }
                if(cat=='New Jersey' && nstandard==0)
                {
                    volNNJ_1++;
                }
                if(cat=='Toronto' && nstandard==0)
                {
                    volTOR_1++;
                }
                if(cat=='Vancouver' && nstandard==0)
                {
                    volVAN_1++;
                }
            }
            //nstandard
            if(cattype=='nstandard' && location1==1) {
                if (cat == 'Columbus' && nstandard>0) {
                    volCol_1++;
                }
                if(cat=='Dallas' && nstandard>0)
                {
                    volDal_1++;
                }
                if(cat=='Jacksonville' && nstandard>0)
                {
                    volJAK_1++;
                }
                if(cat=='Jacksonville2' && nstandard>0)
                {
                    volJAK2_1++;
                }
                if(cat=='Lakeland' && nstandard>0)
                {
                    volLAK_1++;
                }
                if(cat=='Minneapolis ' && nstandard>0)
                {
                    volMIN_1++;
                }
                if(cat=='Montreal' && nstandard>0)
                {
                    volMTL_1++;
                }
                if(cat=='New Jersey' && nstandard>0)
                {
                    volNNJ_1++;
                }
                if(cat=='Toronto' && nstandard>0)
                {
                    volTOR_1++;
                }
                if(cat=='Vancouver' && nstandard>0)
                {
                    volVAN_1++;
                }
            }
            //BY PRODUCTS

            if(cattype=='all' && location1==0) {

                if(in_array("Space",itemcatArr) && !in_array(id,ids1))
                {
                    volSP_1++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && !in_array(id,ids1))
                {
                    volNW_1++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && !in_array(id,ids1))
                {
                    volPW_1++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volXC_1++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volVXC_1++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }


            }
            if(cattype=='standard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volSP_1++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volNW_1++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volPW_1++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volXC_1++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volVXC_1++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }


            }
            if(cattype=='nstandard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volSP_1++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volNW_1++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volPW_1++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volXC_1++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volVXC_1++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
            }


        }

        if((date >= moment(getWeeks[2], "MM/DD/YYYY")) && (date <= moment(getWeeks[3], "MM/DD/YYYY")))
        {
            //BY LOCATIONS
            if(cattype=='all' && location1==1) {
                if (cat == 'Columbus') {
                    volCol_3++;
                }
                if(cat=='Dallas')
                {
                    volDal_3++;
                }
                if(cat=='Jacksonville')
                {
                    volJAK_3++;
                }
                if(cat=='Jacksonville2')
                {
                    volJAK2_3++;
                }
                if(cat=='Lakeland')
                {
                    volLAK_3++;
                }
                if(cat=='Minneapolis ')
                {
                    volMIN_3++;
                }
                if(cat=='Montreal')
                {
                    volMTL_3++;
                }
                if(cat=='New Jersey')
                {
                    volNNJ_3++;
                }
                if(cat=='Toronto')
                {
                    volTOR_3++;
                }
                if(cat=='Vancouver')
                {
                    volVAN_3++;
                }
            }
            //standard
            if(cattype=='standard' && location1==1) {
                if (cat == 'Columbus' && nstandard==0) {
                    volCol_3++;
                }
                if(cat=='Dallas' && nstandard==0)
                {
                    volDal_3++;
                }
                if(cat=='Jacksonville' && nstandard==0)
                {
                    volJAK_3++;
                }
                if(cat=='Jacksonville2' && nstandard==0)
                {
                    volJAK2_3++;
                }
                if(cat=='Lakeland' && nstandard==0)
                {
                    volLAK_3++;
                }
                if(cat=='Minneapolis ' && nstandard==0)
                {
                    volMIN_3++;
                }
                if(cat=='Montreal' && nstandard==0)
                {
                    volMTL_3++;
                }
                if(cat=='New Jersey' && nstandard==0)
                {
                    volNNJ_3++;
                }
                if(cat=='Toronto' && nstandard==0)
                {
                    volTOR_3++;
                }
                if(cat=='Vancouver' && nstandard==0)
                {
                    volVAN_3++;
                }
            }
            //nstandard
            if(cattype=='nstandard' && location1==1) {
                if (cat == 'Columbus' && nstandard>0) {
                    volCol_3++;
                }
                if(cat=='Dallas' && nstandard>0)
                {
                    volDal_3++;
                }
                if(cat=='Jacksonville' && nstandard>0)
                {
                    volJAK_3++;
                }
                if(cat=='Jacksonville2' && nstandard>0)
                {
                    volJAK2_3++;
                }
                if(cat=='Lakeland' && nstandard>0)
                {
                    volLAK_3++;
                }
                if(cat=='Minneapolis ' && nstandard>0)
                {
                    volMIN_3++;
                }
                if(cat=='Montreal' && nstandard>0)
                {
                    volMTL_3++;
                }
                if(cat=='New Jersey' && nstandard>0)
                {
                    volNNJ_3++;
                }
                if(cat=='Toronto' && nstandard>0)
                {
                    volTOR_3++;
                }
                if(cat=='Vancouver' && nstandard>0)
                {
                    volVAN_3++;
                }
            }
            //BY PRODUCTS

            if(cattype=='all' && location1==0) {

                if(in_array("Space",itemcatArr) && !in_array(id,ids1))
                {
                    volSP_3++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && !in_array(id,ids1))
                {
                    volNW_3++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && !in_array(id,ids1))
                {
                    volPW_3++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volXC_3++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volVXC_3++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }


            }
            if(cattype=='standard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volSP_3++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volNW_3++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volPW_3++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volXC_3++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volVXC_3++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }


            }
            if(cattype=='nstandard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volSP_3++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volNW_3++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volPW_3++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volXC_3++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volVXC_3++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
            }


        }
        if((date >= moment(getWeeks[4], "MM/DD/YYYY")) && (date <= moment(getWeeks[5], "MM/DD/YYYY")))
        {
            //BY LOCATIONS
            if(cattype=='all' && location1==1) {
                if (cat == 'Columbus') {
                    volCol_4++;
                }
                if(cat=='Dallas')
                {
                    volDal_4++;
                }
                if(cat=='Jacksonville')
                {
                    volJAK_4++;
                }
                if(cat=='Jacksonville2')
                {
                    volJAK2_4++;
                }
                if(cat=='Lakeland')
                {
                    volLAK_4++;
                }
                if(cat=='Minneapolis ')
                {
                    volMIN_4++;
                }
                if(cat=='Montreal')
                {
                    volMTL_4++;
                }
                if(cat=='New Jersey')
                {
                    volNNJ_4++;
                }
                if(cat=='Toronto')
                {
                    volTOR_4++;
                }
                if(cat=='Vancouver')
                {
                    volVAN_4++;
                }
            }
            //standard
            if(cattype=='standard' && location1==1) {
                if (cat == 'Columbus' && nstandard==0) {
                    volCol_4++;
                }
                if(cat=='Dallas' && nstandard==0)
                {
                    volDal_4++;
                }
                if(cat=='Jacksonville' && nstandard==0)
                {
                    volJAK_4++;
                }
                if(cat=='Jacksonville2' && nstandard==0)
                {
                    volJAK2_4++;
                }
                if(cat=='Lakeland' && nstandard==0)
                {
                    volLAK_4++;
                }
                if(cat=='Minneapolis ' && nstandard==0)
                {
                    volMIN_4++;
                }
                if(cat=='Montreal' && nstandard==0)
                {
                    volMTL_4++;
                }
                if(cat=='New Jersey' && nstandard==0)
                {
                    volNNJ_4++;
                }
                if(cat=='Toronto' && nstandard==0)
                {
                    volTOR_4++;
                }
                if(cat=='Vancouver' && nstandard==0)
                {
                    volVAN_4++;
                }
            }
            //nstandard
            if(cattype=='nstandard' && location1==1) {
                if (cat == 'Columbus' && nstandard>0) {
                    volCol_4++;
                }
                if(cat=='Dallas' && nstandard>0)
                {
                    volDal_4++;
                }
                if(cat=='Jacksonville' && nstandard>0)
                {
                    volJAK_4++;
                }
                if(cat=='Jacksonville2' && nstandard>0)
                {
                    volJAK2_4++;
                }
                if(cat=='Lakeland' && nstandard>0)
                {
                    volLAK_4++;
                }
                if(cat=='Minneapolis ' && nstandard>0)
                {
                    volMIN_4++;
                }
                if(cat=='Montreal' && nstandard>0)
                {
                    volMTL_4++;
                }
                if(cat=='Toronto' && nstandard>0)
                {
                    volTOR_4++;
                }
                if(cat=='Vancouver' && nstandard>0)
                {
                    volVAN_4++;
                }
            }
            //BY PRODUCTS

            if(cattype=='all' && location1==0) {

                if(in_array("Space",itemcatArr) && !in_array(id,ids1))
                {
                    volSP_4++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && !in_array(id,ids1))
                {
                    volNW_4++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && !in_array(id,ids1))
                {
                    volPW_4++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volXC_4++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volVXC_4++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }


            }
            if(cattype=='standard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volSP_4++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volNW_4++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volPW_4++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volXC_4++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volVXC_4++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }


            }
            if(cattype=='nstandard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volSP_4++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volNW_4++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volPW_4++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volXC_4++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volVXC_4++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
            }


        }
        if((date >= moment(getWeeks[6], "MM/DD/YYYY")) && (date <= moment(getWeeks[7], "MM/DD/YYYY")))
        {
            //BY LOCATIONS
            if(cattype=='all' && location1==1) {
                if (cat == 'Columbus') {
                    volCol_5++;
                }
                if(cat=='Dallas')
                {
                    volDal_5++;
                }
                if(cat=='Jacksonville')
                {
                    volJAK_5++;
                }
                if(cat=='Jacksonville2')
                {
                    volJAK2_5++;
                }
                if(cat=='Lakeland')
                {
                    volLAK_5++;
                }
                if(cat=='Minneapolis ')
                {
                    volMIN_5++;
                }
                if(cat=='Montreal')
                {
                    volMTL_5++;
                }
                if(cat=='New Jersey')
                {
                    volNNJ_5++;
                }
                if(cat=='Toronto')
                {
                    volTOR_5++;
                }
                if(cat=='Vancouver')
                {
                    volVAN_5++;
                }
            }
            //standard
            if(cattype=='standard' && location1==1) {
                if (cat == 'Columbus' && nstandard==0) {
                    volCol_5++;
                }
                if(cat=='Dallas' && nstandard==0)
                {
                    volDal_5++;
                }
                if(cat=='Jacksonville' && nstandard==0)
                {
                    volJAK_5++;
                }
                if(cat=='Jacksonville2' && nstandard==0)
                {
                    volJAK2_5++;
                }
                if(cat=='Lakeland' && nstandard==0)
                {
                    volLAK_5++;
                }
                if(cat=='Minneapolis ' && nstandard==0)
                {
                    volMIN_5++;
                }
                if(cat=='Montreal' && nstandard==0)
                {
                    volMTL_5++;
                }
                if(cat=='New Jersey' && nstandard==0)
                {
                    volNNJ_5++;
                }
                if(cat=='Toronto' && nstandard==0)
                {
                    volTOR_5++;
                }
                if(cat=='Vancouver' && nstandard==0)
                {
                    volVAN_5++;
                }
            }
            //nstandard
            if(cattype=='nstandard' && location1==1) {
                if (cat == 'Columbus' && nstandard>0) {
                    volCol_5++;
                }
                if(cat=='Dallas' && nstandard>0)
                {
                    volDal_5++;
                }
                if(cat=='Jacksonville' && nstandard>0)
                {
                    volJAK_5++;
                }
                if(cat=='Jacksonville2' && nstandard>0)
                {
                    volJAK2_5++;
                }
                if(cat=='Lakeland' && nstandard>0)
                {
                    volLAK_5++;
                }
                if(cat=='Minneapolis ' && nstandard>0)
                {
                    volMIN_5++;
                }
                if(cat=='Montreal' && nstandard>0)
                {
                    volMTL_5++;
                }
                if(cat=='New Jersey' && nstandard>0)
                {
                    volNNJ_5++;
                }
                if(cat=='Toronto' && nstandard>0)
                {
                    volTOR_5++;
                }
                if(cat=='Vancouver' && nstandard>0)
                {
                    volVAN_5++;
                }
            }
            //BY PRODUCTS

            if(cattype=='all' && location1==0) {

                if(in_array("Space",itemcatArr) && !in_array(id,ids1))
                {
                    volSP_5++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && !in_array(id,ids1))
                {
                    volNW_5++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && !in_array(id,ids1))
                {
                    volPW_5++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volXC_5++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volVXC_5++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }


            }
            if(cattype=='standard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volSP_5++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volNW_5++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volPW_5++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volXC_5++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volVXC_5++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }


            }
            if(cattype=='nstandard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volSP_5++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volNW_5++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volPW_5++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volXC_5++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volVXC_5++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
            }


        }

        if((date >= moment(getWeeks[8], "MM/DD/YYYY")) && (date <= moment(getWeeks[9], "MM/DD/YYYY")))
        {
            //BY LOCATIONS
            if(cattype=='all' && location1==1) {
                if (cat == 'Columbus') {
                    volCol_6++;
                }
                if(cat=='Dallas')
                {
                    volDal_6++;
                }
                if(cat=='Jacksonville')
                {
                    volJAK_6++;
                }
                if(cat=='Jacksonville2')
                {
                    volJAK2_6++;
                }
                if(cat=='Lakeland')
                {
                    volLAK_6++;
                }
                if(cat=='Minneapolis ')
                {
                    volMIN_6++;
                }
                if(cat=='Montreal')
                {
                    volMTL_6++;
                }
                if(cat=='New Jersey')
                {
                    volNNJ_6++;
                }
                if(cat=='Toronto')
                {
                    volTOR_6++;
                }
                if(cat=='Vancouver')
                {
                    volVAN_6++;
                }
            }
            //standard
            if(cattype=='standard' && location1==1) {
                if (cat == 'Columbus' && nstandard==0) {
                    volCol_6++;
                }
                if(cat=='Dallas' && nstandard==0)
                {
                    volDal_6++;
                }
                if(cat=='Jacksonville' && nstandard==0)
                {
                    volJAK_6++;
                }
                if(cat=='Jacksonville2' && nstandard==0)
                {
                    volJAK2_6++;
                }
                if(cat=='Lakeland' && nstandard==0)
                {
                    volLAK_6++;
                }
                if(cat=='Minneapolis ' && nstandard==0)
                {
                    volMIN_6++;
                }
                if(cat=='Montreal' && nstandard==0)
                {
                    volMTL_6++;
                }
                if(cat=='New Jersey' && nstandard==0)
                {
                    volNNJ_6++;
                }
                if(cat=='Toronto' && nstandard==0)
                {
                    volTOR_6++;
                }
                if(cat=='Vancouver' && nstandard==0)
                {
                    volVAN_6++;
                }
            }
            //nstandard
            if(cattype=='nstandard' && location1==1) {
                if (cat == 'Columbus' && nstandard>0) {
                    volCol_6++;
                }
                if(cat=='Dallas' && nstandard>0)
                {
                    volDal_6++;
                }
                if(cat=='Jacksonville' && nstandard>0)
                {
                    volJAK_6++;
                }
                if(cat=='Jacksonville2' && nstandard>0)
                {
                    volJAK2_6++;
                }
                if(cat=='Lakeland' && nstandard>0)
                {
                    volLAK_6++;
                }
                if(cat=='Minneapolis ' && nstandard>0)
                {
                    volMIN_6++;
                }
                if(cat=='Montreal' && nstandard>0)
                {
                    volMTL_6++;
                }
                if(cat=='NNJ' && nstandard>0)
                {
                    volNNJ_6++;
                }
                if(cat=='Toronto' && nstandard>0)
                {
                    volTOR_6++;
                }
                if(cat=='Vancouver' && nstandard>0)
                {
                    volVAN_6++;
                }
            }
            //BY PRODUCTS

            if(cattype=='all' && location1==0 ) {

                if(in_array("Space",itemcatArr) && !in_array(id,ids1))
                {
                    volSP_6++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && !in_array(id,ids1))
                {
                    volNW_6++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && !in_array(id,ids1))
                {
                    volPW_6++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volXC_6++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volVXC_6++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }


            }
            if(cattype=='standard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volSP_6++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volNW_6++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volPW_6++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volXC_6++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volVXC_6++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }


            }
            if(cattype=='nstandard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volSP_6++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volNW_6++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volPW_6++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volXC_6++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volVXC_6++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
            }


        }
        if((date >= moment(getWeeks[10], "MM/DD/YYYY")) && (date <= moment(getWeeks[11], "MM/DD/YYYY")))
        {
            //BY LOCATIONS
            if(cattype=='all' && location1==1) {
                if (cat == 'Columbus') {
                    volCol_7++;
                }
                if(cat=='Dallas')
                {
                    volDal_7++;
                }
                if(cat=='Jacksonville')
                {
                    volJAK_7++;
                }
                if(cat=='Jacksonville2')
                {
                    volJAK2_7++;
                }
                if(cat=='Lakeland')
                {
                    volLAK_7++;
                }
                if(cat=='Minneapolis ')
                {
                    volMIN_7++;
                }
                if(cat=='Montreal')
                {
                    volMTL_7++;
                }
                if(cat=='New Jersey')
                {
                    volNNJ_7++;
                }
                if(cat=='Toronto')
                {
                    volTOR_7++;
                }
                if(cat=='Vancouver')
                {
                    volVAN_7++;
                }
            }
            //standard
            if(cattype=='standard' && location1==1) {
                if (cat == 'Columbus' && nstandard==0) {
                    volCol_7++;
                }
                if(cat=='Dallas' && nstandard==0)
                {
                    volDal_7++;
                }
                if(cat=='Jacksonville' && nstandard==0)
                {
                    volJAK_7++;
                }
                if(cat=='Jacksonville2' && nstandard==0)
                {
                    volJAK2_7++;
                }
                if(cat=='Lakeland' && nstandard==0)
                {
                    volLAK_7++;
                }
                if(cat=='Minneapolis ' && nstandard==0)
                {
                    volMIN_7++;
                }
                if(cat=='Montreal' && nstandard==0)
                {
                    volMTL_7++;
                }
                if(cat=='New Jersey' && nstandard==0)
                {
                    volNNJ_7++;
                }
                if(cat=='Toronto' && nstandard==0)
                {
                    volTOR_7++;
                }
                if(cat=='Vancouver' && nstandard==0)
                {
                    volVAN_7++;
                }
            }
            //nstandard
            if(cattype=='nstandard' && location1==1) {
                if (cat == 'Columbus' && nstandard>0) {
                    volCol_7++;
                }
                if(cat=='Dallas' && nstandard>0)
                {
                    volDal_7++;
                }
                if(cat=='Jacksonville' && nstandard>0)
                {
                    volJAK_7++;
                }
                if(cat=='Jacksonville2' && nstandard>0)
                {
                    volJAK2_7++;
                }
                if(cat=='Lakeland' && nstandard>0)
                {
                    volLAK_7++;
                }
                if(cat=='Minneapolis ' && nstandard>0)
                {
                    volMIN_7++;
                }
                if(cat=='Montreal' && nstandard>0)
                {
                    volMTL_7++;
                }
                if(cat=='New Jersey' && nstandard>0)
                {
                    volNNJ_7++;
                }
                if(cat=='Toronto' && nstandard>0)
                {
                    volTOR_7++;
                }
                if(cat=='Vancouver' && nstandard>0)
                {
                    volVAN_7++;
                }
            }
            //BY PRODUCTS

            if(cattype=='all' && location1==0) {

                if(in_array("Space",itemcatArr) && !in_array(id,ids1))
                {
                    volSP_7++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && !in_array(id,ids1))
                {
                    volNW_7++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && !in_array(id,ids1))
                {
                    volPW_7++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volXC_7++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volVXC_7++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }


            }
            if(cattype=='standard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volSP_7++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volNW_7++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volPW_7++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volXC_7++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volVXC_7++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }


            }
            if(cattype=='nstandard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volSP_7++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volNW_7++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volPW_7++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volXC_7++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volVXC_7++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }


            }
        }

        if((date >= moment(getWeeks[12], "MM/DD/YYYY")) && (date <= moment(getWeeks[13], "MM/DD/YYYY")))
        {
            //BY LOCATIONS
            if(cattype=='all' && location1==1) {
                if (cat == 'Columbus') {
                    volCol_8++;
                }
                if(cat=='Dallas')
                {
                    volDal_8++;
                }
                if(cat=='Jacksonville')
                {
                    volJAK_8++;
                }
                if(cat=='Jacksonville2')
                {
                    volJAK2_8++;
                }
                if(cat=='Lakeland')
                {
                    volLAK_8++;
                }
                if(cat=='Minneapolis ')
                {
                    volMIN_8++;
                }
                if(cat=='Montreal')
                {
                    volMTL_8++;
                }
                if(cat=='New Jersey')
                {
                    volNNJ_8++;
                }
                if(cat=='Toronto')
                {
                    volTOR_8++;
                }
                if(cat=='Vancouver')
                {
                    volVAN_8++;
                }
            }
            //standard
            if(cattype=='standard' && location1==1) {
                if (cat == 'Columbus' && nstandard==0) {
                    volCol_8++;
                }
                if(cat=='Dallas' && nstandard==0)
                {
                    volDal_8++;
                }
                if(cat=='Jacksonville' && nstandard==0)
                {
                    volJAK_8++;
                }
                if(cat=='Jacksonville2' && nstandard==0)
                {
                    volJAK2_8++;
                }
                if(cat=='Lakeland' && nstandard==0)
                {
                    volLAK_8++;
                }
                if(cat=='Minneapolis ' && nstandard==0)
                {
                    volMIN_8++;
                }
                if(cat=='Montreal' && nstandard==0)
                {
                    volMTL_8++;
                }
                if(cat=='New Jersey' && nstandard==0)
                {
                    volNNJ_8++;
                }
                if(cat=='Toronto' && nstandard==0)
                {
                    volTOR_8++;
                }
                if(cat=='Vancouver' && nstandard==0)
                {
                    volVAN_8++;
                }
            }
            //nstandard
            if(cattype=='nstandard' && location1==1) {
                if (cat == 'Columbus' && nstandard>0) {
                    volCol_8++;
                }
                if(cat=='Dallas' && nstandard>0)
                {
                    volDal_8++;
                }
                if(cat=='Jacksonville' && nstandard>0)
                {
                    volJAK_8++;
                }
                if(cat=='Jacksonville2' && nstandard>0)
                {
                    volJAK2_8++;
                }
                if(cat=='Lakeland' && nstandard>0)
                {
                    volLAK_8++;
                }
                if(cat=='Minneapolis ' && nstandard>0)
                {
                    volMIN_8++;
                }
                if(cat=='Montreal' && nstandard>0)
                {
                    volMTL_8++;
                }
                if(cat=='New Jersey' && nstandard>0)
                {
                    volNNJ_8++;
                }
                if(cat=='Toronto' && nstandard>0)
                {
                    volTOR_8++;
                }
                if(cat=='Vancouver' && nstandard>0)
                {
                    volVAN_8++;
                }
            }
            //BY PRODUCTS

            if(cattype=='all' && location1==0) {

                if(in_array("Space",itemcatArr) && !in_array(id,ids1))
                {
                    volSP_8++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && !in_array(id,ids1))
                {
                    volNW_8++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && !in_array(id,ids1))
                {
                    volPW_8++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volXC_8++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volVXC_8++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }


            }
            if(cattype=='standard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volSP_8++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volNW_8++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volPW_8++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volXC_8++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volVXC_8++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }


            }
            if(cattype=='nstandard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volSP_8++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volNW_8++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volPW_8++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volXC_8++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volVXC_8++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }


            }
        }

        if((date >= moment(getWeeks[14], "MM/DD/YYYY")) && (date <= moment(getWeeks[15], "MM/DD/YYYY")))
        {
            //BY LOCATIONS
            if(cattype=='all' && location1==1) {
                if (cat == 'Columbus') {
                    volCol_9++;
                }
                if(cat=='Dallas')
                {
                    volDal_9++;
                }
                if(cat=='Jacksonville')
                {
                    volJAK_9++;
                }
                if(cat=='Jacksonville2')
                {
                    volJAK2_9++;
                }
                if(cat=='Lakeland')
                {
                    volLAK_9++;
                }
                if(cat=='Minneapolis ')
                {
                    volMIN_9++;
                }
                if(cat=='Montreal')
                {
                    volMTL_9++;
                }
                if(cat=='New Jersey')
                {
                    volNNJ_9++;
                }
                if(cat=='Toronto')
                {
                    volTOR_9++;
                }
                if(cat=='Vancouver')
                {
                    volVAN_9++;
                }
            }
            //standard
            if(cattype=='standard' && location1==1) {
                if (cat == 'Columbus' && nstandard==0) {
                    volCol_9++;
                }
                if(cat=='Dallas' && nstandard==0)
                {
                    volDal_9++;
                }
                if(cat=='Jacksonville' && nstandard==0)
                {
                    volJAK_9++;
                }
                if(cat=='Jacksonville2' && nstandard==0)
                {
                    volJAK2_9++;
                }
                if(cat=='Lakeland' && nstandard==0)
                {
                    volLAK_9++;
                }
                if(cat=='Minneapolis ' && nstandard==0)
                {
                    volMIN_9++;
                }
                if(cat=='Montreal' && nstandard==0)
                {
                    volMTL_9++;
                }
                if(cat=='New Jersey' && nstandard==0)
                {
                    volNNJ_9++;
                }
                if(cat=='Toronto' && nstandard==0)
                {
                    volTOR_9++;
                }
                if(cat=='Vancouver' && nstandard==0)
                {
                    volVAN_9++;
                }
            }
            //nstandard
            if(cattype=='nstandard' && location1==1) {
                if (cat == 'Columbus' && nstandard>0) {
                    volCol_9++;
                }
                if(cat=='Dallas' && nstandard>0)
                {
                    volDal_9++;
                }
                if(cat=='Jacksonville' && nstandard>0)
                {
                    volJAK_9++;
                }
                if(cat=='Jacksonville2' && nstandard>0)
                {
                    volJAK2_9++;
                }
                if(cat=='Lakeland' && nstandard>0)
                {
                    volLAK_9++;
                }
                if(cat=='Minneapolis ' && nstandard>0)
                {
                    volMIN_9++;
                }
                if(cat=='Montreal' && nstandard>0)
                {
                    volMTL_9++;
                }
                if(cat=='New Jersey' && nstandard>0)
                {
                    volNNJ_9++;
                }
                if(cat=='Toronto' && nstandard>0)
                {
                    volTOR_9++;
                }
                if(cat=='Vancouver' && nstandard>0)
                {
                    volVAN_9++;
                }
            }
            //BY PRODUCTS

            if(cattype=='all' && location1==0) {

                if(in_array("Space",itemcatArr) && !in_array(id,ids1))
                {
                    volSP_9++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && !in_array(id,ids1))
                {
                    volNW_9++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && !in_array(id,ids1))
                {
                    volPW_9++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volXC_9++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volVXC_9++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }


            }
            if(cattype=='standard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volSP_9++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volNW_9++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volPW_9++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volXC_9++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volVXC_9++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }


            }
            if(cattype=='nstandard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volSP_9++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volNW_9++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volPW_9++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volXC_9++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volVXC_9++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }


            }
        }

        if((date >= moment(getWeeks[16], "MM/DD/YYYY")) && (date <= moment(getWeeks[17], "MM/DD/YYYY")))
        {
            //BY LOCATIONS
            if(cattype=='all' && location1==1) {
                if (cat == 'Columbus') {
                    volCol_10++;
                }
                if(cat=='Dallas')
                {
                    volDal_10++;
                }
                if(cat=='Jacksonville')
                {
                    volJAK_10++;
                }
                if(cat=='Jacksonville2')
                {
                    volJAK2_10++;
                }
                if(cat=='Lakeland')
                {
                    volLAK_10++;
                }
                if(cat=='Minneapolis ')
                {
                    volMIN_10++;
                }
                if(cat=='Montreal')
                {
                    volMTL_10++;
                }
                if(cat=='New Jersey')
                {
                    volNNJ_10++;
                }
                if(cat=='Toronto')
                {
                    volTOR_10++;
                }
                if(cat=='Vancouver')
                {
                    volVAN_10++;
                }
            }
            //standard
            if(cattype=='standard' && location1==1) {
                if (cat == 'Columbus' && nstandard==0) {
                    volCol_10++;
                }
                if(cat=='Dallas' && nstandard==0)
                {
                    volDal_10++;
                }
                if(cat=='Jacksonville' && nstandard==0)
                {
                    volJAK_10++;
                }
                if(cat=='Jacksonville2' && nstandard==0)
                {
                    volJAK2_10++;
                }
                if(cat=='Lakeland' && nstandard==0)
                {
                    volLAK_10++;
                }
                if(cat=='Minneapolis ' && nstandard==0)
                {
                    volMIN_10++;
                }
                if(cat=='Montreal' && nstandard==0)
                {
                    volMTL_10++;
                }
                if(cat=='New Jersey' && nstandard==0)
                {
                    volNNJ_10++;
                }
                if(cat=='Toronto' && nstandard==0)
                {
                    volTOR_10++;
                }
                if(cat=='Vancouver' && nstandard==0)
                {
                    volVAN_10++;
                }
            }
            //nstandard
            if(cattype=='nstandard' && location1==1) {
                if (cat == 'Columbus' && nstandard>0) {
                    volCol_10++;
                }
                if(cat=='Dallas' && nstandard>0)
                {
                    volDal_10++;
                }
                if(cat=='Jacksonville' && nstandard>0)
                {
                    volJAK_10++;
                }
                if(cat=='Jacksonville2' && nstandard>0)
                {
                    volJAK2_10++;
                }
                if(cat=='Lakeland' && nstandard>0)
                {
                    volLAK_10++;
                }
                if(cat=='Minneapolis ' && nstandard>0)
                {
                    volMIN_10++;
                }
                if(cat=='Montreal' && nstandard>0)
                {
                    volMTL_10++;
                }
                if(cat=='New Jersey' && nstandard>0)
                {
                    volNNJ_10++;
                }
                if(cat=='Toronto' && nstandard>0)
                {
                    volTOR_10++;
                }
                if(cat=='Vancouver' && nstandard>0)
                {
                    volVAN_10++;
                }
            }
            //BY PRODUCTS

            if(cattype=='all' && location1==0) {

                if(in_array("Space",itemcatArr) && !in_array(id,ids1))
                {
                    volSP_10++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && !in_array(id,ids1))
                {
                    volNW_10++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && !in_array(id,ids1))
                {
                    volPW_10++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volXC_10++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volVXC_10++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }


            }
            if(cattype=='standard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volSP_10++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volNW_10++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volPW_10++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volXC_10++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volVXC_10++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }


            }
            if(cattype=='nstandard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volSP_10++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volNW_10++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volPW_10++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volXC_10++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volVXC_10++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }


            }
        }

        if((date >= moment(getWeeks[18], "MM/DD/YYYY")) && (date <= moment(getWeeks[19], "MM/DD/YYYY")))
        {
            //BY LOCATIONS
            if(cattype=='all' && location1==1) {
                if (cat == 'Columbus') {
                    volCol_11++;
                }
                if(cat=='Dallas')
                {
                    volDal_11++;
                }
                if(cat=='Jacksonville')
                {
                    volJAK_11++;
                }
                if(cat=='Jacksonville2')
                {
                    volJAK2_11++;
                }
                if(cat=='Lakeland')
                {
                    volLAK_11++;
                }
                if(cat=='Minneapolis ')
                {
                    volMIN_11++;
                }
                if(cat=='Montreal')
                {
                    volMTL_11++;
                }
                if(cat=='New Jersey')
                {
                    volNNJ_11++;
                }
                if(cat=='Toronto')
                {
                    volTOR_11++;
                }
                if(cat=='Vancouver')
                {
                    volVAN_11++;
                }
            }
            //standard
            if(cattype=='standard' && location1==1) {
                if (cat == 'Columbus' && nstandard==0) {
                    volCol_11++;
                }
                if(cat=='Dallas' && nstandard==0)
                {
                    volDal_11++;
                }
                if(cat=='Jacksonville' && nstandard==0)
                {
                    volJAK_11++;
                }
                if(cat=='Jacksonville2' && nstandard==0)
                {
                    volJAK2_11++;
                }
                if(cat=='Lakeland' && nstandard==0)
                {
                    volLAK_11++;
                }
                if(cat=='Minneapolis ' && nstandard==0)
                {
                    volMIN_11++;
                }
                if(cat=='Montreal' && nstandard==0)
                {
                    volMTL_11++;
                }
                if(cat=='New Jersey' && nstandard==0)
                {
                    volNNJ_11++;
                }
                if(cat=='Toronto' && nstandard==0)
                {
                    volTOR_11++;
                }
                if(cat=='Vancouver' && nstandard==0)
                {
                    volVAN_11++;
                }
            }
            //nstandard
            if(cattype=='nstandard' && location1==1) {
                if (cat == 'Columbus' && nstandard>0) {
                    volCol_11++;
                }
                if(cat=='Dallas' && nstandard>0)
                {
                    volDal_11++;
                }
                if(cat=='Jacksonville' && nstandard>0)
                {
                    volJAK_11++;
                }
                if(cat=='Jacksonville2' && nstandard>0)
                {
                    volJAK2_11++;
                }
                if(cat=='Lakeland' && nstandard>0)
                {
                    volLAK_11++;
                }
                if(cat=='Minneapolis ' && nstandard>0)
                {
                    volMIN_11++;
                }
                if(cat=='Montreal' && nstandard>0)
                {
                    volMTL_11++;
                }
                if(cat=='New Jersey' && nstandard>0)
                {
                    volNNJ_11++;
                }
                if(cat=='Toronto' && nstandard>0)
                {
                    volTOR_11++;
                }
                if(cat=='Vancouver' && nstandard>0)
                {
                    volVAN_11++;
                }
            }
            //BY PRODUCTS

            if(cattype=='all' && location1==0) {

                if(in_array("Space",itemcatArr) && !in_array(id,ids1))
                {
                    volSP_11++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && !in_array(id,ids1))
                {
                    volNW_11++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && !in_array(id,ids1))
                {
                    volPW_11++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volXC_11++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volVXC_11++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }


            }
            if(cattype=='standard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volSP_11++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volNW_11++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volPW_11++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volXC_11++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volVXC_11++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }


            }
            if(cattype=='nstandard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volSP_11++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volNW_11++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volPW_11++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volXC_11++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volVXC_11++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }


            }
        }

        if((date >= moment(getWeeks[20], "MM/DD/YYYY")) && (date <= moment(getWeeks[21], "MM/DD/YYYY")))
        {
            //BY LOCATIONS
            if(cattype=='all' && location1==1) {
                if (cat == 'Columbus') {
                    volCol_12++;
                }
                if(cat=='Dallas')
                {
                    volDal_12++;
                }
                if(cat=='Jacksonville')
                {
                    volJAK_12++;
                }
                if(cat=='Jacksonville2')
                {
                    volJAK2_12++;
                }
                if(cat=='Lakeland')
                {
                    volLAK_12++;
                }
                if(cat=='Minneapolis ')
                {
                    volMIN_12++;
                }
                if(cat=='Montreal')
                {
                    volMTL_12++;
                }
                if(cat=='New Jersey')
                {
                    volNNJ_12++;
                }
                if(cat=='Toronto')
                {
                    volTOR_12++;
                }
                if(cat=='Vancouver')
                {
                    volVAN_12++;
                }
            }
            //standard
            if(cattype=='standard' && location1==1) {
                if (cat == 'Columbus' && nstandard==0) {
                    volCol_12++;
                }
                if(cat=='Dallas' && nstandard==0)
                {
                    volDal_12++;
                }
                if(cat=='Jacksonville' && nstandard==0)
                {
                    volJAK_12++;
                }
                if(cat=='Jacksonville2' && nstandard==0)
                {
                    volJAK2_12++;
                }
                if(cat=='Lakeland' && nstandard==0)
                {
                    volLAK_12++;
                }
                if(cat=='Minneapolis ' && nstandard==0)
                {
                    volMIN_12++;
                }
                if(cat=='Montreal' && nstandard==0)
                {
                    volMTL_12++;
                }
                if(cat=='New Jersey' && nstandard==0)
                {
                    volNNJ_12++;
                }
                if(cat=='Toronto' && nstandard==0)
                {
                    volTOR_12++;
                }
                if(cat=='Vancouver' && nstandard==0)
                {
                    volVAN_12++;
                }
            }
            //nstandard
            if(cattype=='nstandard' && location1==1) {
                if (cat == 'Columbus' && nstandard>0) {
                    volCol_12++;
                }
                if(cat=='Dallas' && nstandard>0)
                {
                    volDal_12++;
                }
                if(cat=='Jacksonville' && nstandard>0)
                {
                    volJAK_12++;
                }
                if(cat=='Jacksonville2' && nstandard>0)
                {
                    volJAK2_12++;
                }
                if(cat=='Lakeland' && nstandard>0)
                {
                    volLAK_12++;
                }
                if(cat=='Minneapolis ' && nstandard>0)
                {
                    volMIN_12++;
                }
                if(cat=='Montreal' && nstandard>0)
                {
                    volMTL_12++;
                }
                if(cat=='New Jersey' && nstandard>0)
                {
                    volNNJ_12++;
                }
                if(cat=='Toronto' && nstandard>0)
                {
                    volTOR_12++;
                }
                if(cat=='Vancouver' && nstandard>0)
                {
                    volVAN_12++;
                }
            }
            //BY PRODUCTS

            if(cattype=='all' && location1==0) {

                if(in_array("Space",itemcatArr) && !in_array(id,ids1))
                {
                    volSP_12++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && !in_array(id,ids1))
                {
                    volNW_12++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && !in_array(id,ids1))
                {
                    volPW_12++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volXC_12++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && !in_array(id,ids1))
                {
                    volVXC_12++;
                    if(cat==0)
                    {
                        ids1.push(id);
                    }
                }


            }
            if(cattype=='standard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volSP_12++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volNW_12++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volPW_12++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volXC_12++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard==0 && !in_array(id,ids1ST))
                {
                    volVXC_12++;
                    if(cat==0)
                    {
                        ids1ST.push(id);
                    }
                }


            }
            if(cattype=='nstandard' && location1==0) {

                if(in_array("Space",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volSP_12++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Network",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volNW_12++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Power",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volPW_12++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volXC_12++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }
                if(in_array("Virtual Interconnection",itemcatArr) && nstandard>0 && !in_array(id,ids1NST))
                {
                    volVXC_12++;
                    if(cat==0)
                    {
                        ids1NST.push(id);
                    }
                }


            }
        }
    }


    if(location1==1)
    {
        series +=
            "{name: 'Columbus',type: 'column',data: [" +
            "{y:"+ volCol_0 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Columbus&location=33&product="+productCat+"&cattype="+catGrid+"&from=past&to=past'}," +
            "{y:"+volCol_1 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Columbus&location=33&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[0]+"&to="+getWeeks[1]+"'}"+','
            +"{y:"+volCol_3 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Columbus&location=33&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[2]+"&to="+getWeeks[3]+"'}"+','
            +"{y:"+volCol_4 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Columbus&location=33&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[4]+"&to="+getWeeks[5]+"'}"+','
            +"{y:"+volCol_5 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Columbus&location=33&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[6]+"&to="+getWeeks[7]+"'}"+','
            +"{y:"+volCol_6 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Columbus&location=33&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[8]+"&to="+getWeeks[9]+"'}"+','
            +"{y:"+volCol_7 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Columbus&location=33&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[10]+"&to="+getWeeks[11]+"'}"+','
            +"{y:"+volCol_8 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Columbus&location=33&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[12]+"&to="+getWeeks[13]+"'}"+','
            +"{y:"+volCol_9 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Columbus&location=33&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[14]+"&to="+getWeeks[15]+"'}"+','
            +"{y:"+volCol_10 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Columbus&location=33&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[16]+"&to="+getWeeks[17]+"'}"+','
            +"{y:"+volCol_11+",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Columbus&location=33&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[18]+"&to="+getWeeks[19]+"'}"+','
            +"{y:"+volCol_12 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Columbus&location=33&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[20]+"&to="+getWeeks[21]+"'}"+''
            +"]},";

        series +=
            "{name: 'Dallas',type: 'column',data: [" +
            "{y:"+ volDal_0 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Dallas&location=20&product="+productCat+"&cattype="+catGrid+"&from=past&to=past'}," +
            "{y:"+volDal_1 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Dallas&location=20&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[0]+"&to="+getWeeks[1]+"'}"+','
            +"{y:"+volDal_3 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Dallas&location=20&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[2]+"&to="+getWeeks[3]+"'}"+','
            +"{y:"+volDal_4 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Dallas&location=20&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[4]+"&to="+getWeeks[5]+"'}"+','
            +"{y:"+volDal_5 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Dallas&location=20&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[6]+"&to="+getWeeks[7]+"'}"+','
            +"{y:"+volDal_6 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Dallas&location=20&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[8]+"&to="+getWeeks[9]+"'}"+','
            +"{y:"+volDal_7 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Dallas&location=20&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[10]+"&to="+getWeeks[11]+"'}"+','
            +"{y:"+volDal_8 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Dallas&location=20&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[12]+"&to="+getWeeks[13]+"'}"+','
            +"{y:"+volDal_9 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Dallas&location=20&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[14]+"&to="+getWeeks[15]+"'}"+','
            +"{y:"+volDal_10 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Dallas&location=20&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[16]+"&to="+getWeeks[17]+"'}"+','
            +"{y:"+volDal_11+",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Dallas&location=20&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[18]+"&to="+getWeeks[19]+"'}"+','
            +"{y:"+volDal_12 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Dallas&location=20&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[20]+"&to="+getWeeks[21]+"'}"+''
            +"]},";
        series +=
            "{name: 'Jacksonville',type: 'column',data: [" +
            "{y:"+ volJAK_0 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville&location=29&product="+productCat+"&cattype="+catGrid+"&from=past&to=past'}," +
            "{y:"+volJAK_1 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville&location=29&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[0]+"&to="+getWeeks[1]+"'}"+','
            +"{y:"+volJAK_3 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville&location=29&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[2]+"&to="+getWeeks[3]+"'}"+','
            +"{y:"+volJAK_4 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville&location=29&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[4]+"&to="+getWeeks[5]+"'}"+','
            +"{y:"+volJAK_5 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville&location=29&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[6]+"&to="+getWeeks[7]+"'}"+','
            +"{y:"+volJAK_6 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville&location=29&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[8]+"&to="+getWeeks[9]+"'}"+','
            +"{y:"+volJAK_7 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville&location=29&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[10]+"&to="+getWeeks[11]+"'}"+','
            +"{y:"+volJAK_8 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville&location=29&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[12]+"&to="+getWeeks[13]+"'}"+','
            +"{y:"+volJAK_9 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville&location=29&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[14]+"&to="+getWeeks[15]+"'}"+','
            +"{y:"+volJAK_10 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville&location=29&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[16]+"&to="+getWeeks[17]+"'}"+','
            +"{y:"+volJAK_11+",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville&location=29&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[18]+"&to="+getWeeks[19]+"'}"+','
            +"{y:"+volJAK_12 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville&location=29&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[20]+"&to="+getWeeks[21]+"'}"+''
            +"]},";
        series +=
            "{name: 'Jacksonville2',type: 'column',data: [" +
            "{y:" +volJAK2_0 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville2&location=43&product="+productCat+"&cattype="+catGrid+"&from=past&to=past'}," +
            "{y:" +volJAK2_1 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville2&location=43&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[0]+"&to="+getWeeks[1]+"'}"+','
            +"{y:"+volJAK2_3 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville2&location=43&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[2]+"&to="+getWeeks[3]+"'}"+','
            +"{y:"+volJAK2_4 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville2&location=43&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[4]+"&to="+getWeeks[5]+"'}"+','
            +"{y:"+volJAK2_5 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville2&location=43&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[6]+"&to="+getWeeks[7]+"'}"+','
            +"{y:"+volJAK2_6 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville2&location=43&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[8]+"&to="+getWeeks[9]+"'}"+','
            +"{y:"+volJAK2_7 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville2&location=43&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[10]+"&to="+getWeeks[11]+"'}"+','
            +"{y:"+volJAK2_8 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville2&location=43&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[12]+"&to="+getWeeks[13]+"'}"+','
            +"{y:"+volJAK2_9 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville2&location=43&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[14]+"&to="+getWeeks[15]+"'}"+','
            +"{y:"+volJAK2_10 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville2&location=43&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[16]+"&to="+getWeeks[17]+"'}"+','
            +"{y:"+volJAK2_11+",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville2&location=43&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[18]+"&to="+getWeeks[19]+"'}"+','
            +"{y:"+volJAK2_12 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Jacksonville2&location=43&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[20]+"&to="+getWeeks[21]+"'}"+''
            +"]},";
        series +=
            "{name: 'Lakeland',type: 'column',data: [" +
            "{y:" +volLAK_0 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Lakeland&location=41&product="+productCat+"&cattype="+catGrid+"&from=past&to=past'}," +
            "{y:" +volLAK_1 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Lakeland&location=41&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[0]+"&to="+getWeeks[1]+"'}"+','
            +"{y:"+volLAK_3 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Lakeland&location=41&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[2]+"&to="+getWeeks[3]+"'}"+','
            +"{y:"+volLAK_4 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Lakeland&location=41&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[4]+"&to="+getWeeks[5]+"'}"+','
            +"{y:"+volLAK_5 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Lakeland&location=41&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[6]+"&to="+getWeeks[7]+"'}"+','
            +"{y:"+volLAK_6 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Lakeland&location=41&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[8]+"&to="+getWeeks[9]+"'}"+','
            +"{y:"+volLAK_7 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Lakeland&location=41&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[10]+"&to="+getWeeks[11]+"'}"+','
            +"{y:"+volLAK_8 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Lakeland&location=41&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[12]+"&to="+getWeeks[13]+"'}"+','
            +"{y:"+volLAK_9 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Lakeland&location=41&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[14]+"&to="+getWeeks[15]+"'}"+','
            +"{y:"+volLAK_10 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Lakeland&location=41&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[16]+"&to="+getWeeks[17]+"'}"+','
            +"{y:"+volLAK_11+",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Lakeland&location=41&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[18]+"&to="+getWeeks[19]+"'}"+','
            +"{y:"+volLAK_12 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Lakeland&location=41&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[20]+"&to="+getWeeks[21]+"'}"+''
            +"]},";

        series +=
            "{name: 'Minneapolis',type: 'column',data: [" +
            "{y:" +volMIN_0 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Minneapolis&location=22&product="+productCat+"&cattype="+catGrid+"&from=past&to=past'}," +
            "{y:" +volMIN_1 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Minneapolis&location=22&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[0]+"&to="+getWeeks[1]+"'}"+','
            +"{y:"+volMIN_3 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Minneapolis&location=22&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[2]+"&to="+getWeeks[3]+"'}"+','
            +"{y:"+volMIN_4 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Minneapolis&location=22&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[4]+"&to="+getWeeks[5]+"'}"+','
            +"{y:"+volMIN_5 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Minneapolis&location=22&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[6]+"&to="+getWeeks[7]+"'}"+','
            +"{y:"+volMIN_6 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Minneapolis&location=22&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[8]+"&to="+getWeeks[9]+"'}"+','
            +"{y:"+volMIN_7 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Minneapolis&location=22&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[10]+"&to="+getWeeks[11]+"'}"+','
            +"{y:"+volMIN_8 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Minneapolis&location=22&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[12]+"&to="+getWeeks[13]+"'}"+','
            +"{y:"+volMIN_9 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Minneapolis&location=22&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[14]+"&to="+getWeeks[15]+"'}"+','
            +"{y:"+volMIN_10 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Minneapolis&location=22&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[16]+"&to="+getWeeks[17]+"'}"+','
            +"{y:"+volMIN_11+",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Minneapolis&location=22&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[18]+"&to="+getWeeks[19]+"'}"+','
            +"{y:"+volMIN_12 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Minneapolis&location=22&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[20]+"&to="+getWeeks[21]+"'}"+''
            +"]},";
        series +=
            "{name: 'Montreal',type: 'column',data: [" +
            "{y:" +volMTL_0 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Montreal&location=25&product="+productCat+"&cattype="+catGrid+"&from=past&to=past'}," +
            "{y:" +volMTL_1 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Montreal&location=25&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[0]+"&to="+getWeeks[1]+"'}"+','
            +"{y:"+volMTL_3 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Montreal&location=25&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[2]+"&to="+getWeeks[3]+"'}"+','
            +"{y:"+volMTL_4 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Montreal&location=25&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[4]+"&to="+getWeeks[5]+"'}"+','
            +"{y:"+volMTL_5 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Montreal&location=25&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[6]+"&to="+getWeeks[7]+"'}"+','
            +"{y:"+volMTL_6 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Montreal&location=25&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[8]+"&to="+getWeeks[9]+"'}"+','
            +"{y:"+volMTL_7 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Montreal&location=25&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[10]+"&to="+getWeeks[11]+"'}"+','
            +"{y:"+volMTL_8 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Montreal&location=25&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[12]+"&to="+getWeeks[13]+"'}"+','
            +"{y:"+volMTL_9 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Montreal&location=25&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[14]+"&to="+getWeeks[15]+"'}"+','
            +"{y:"+volMTL_10 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Montreal&location=25&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[16]+"&to="+getWeeks[17]+"'}"+','
            +"{y:"+volMTL_11+",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Montreal&location=25&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[18]+"&to="+getWeeks[19]+"'}"+','
            +"{y:"+volMTL_12 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Montreal&location=25&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[20]+"&to="+getWeeks[21]+"'}"+''
            +"]},";
        series +=
            "{name: 'New Jersey',type: 'column',data: [" +
            "{y:" +volNNJ_0 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=New Jersey&location=52&product="+productCat+"&cattype="+catGrid+"&from=past&to=past'}," +
            "{y:" +volNNJ_1 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=New Jersey&location=52&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[0]+"&to="+getWeeks[1]+"'}"+','
            +"{y:"+volNNJ_3 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=New Jersey&location=52&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[2]+"&to="+getWeeks[3]+"'}"+','
            +"{y:"+volNNJ_4 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=New Jersey&location=52&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[4]+"&to="+getWeeks[5]+"'}"+','
            +"{y:"+volNNJ_5 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=New Jersey&location=52&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[6]+"&to="+getWeeks[7]+"'}"+','
            +"{y:"+volNNJ_6 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=New Jersey&location=52&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[8]+"&to="+getWeeks[9]+"'}"+','
            +"{y:"+volNNJ_7 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=New Jersey&location=52&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[10]+"&to="+getWeeks[11]+"'}"+','
            +"{y:"+volNNJ_8 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=New Jersey&location=52&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[12]+"&to="+getWeeks[13]+"'}"+','
            +"{y:"+volNNJ_9 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=New Jersey&location=52&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[14]+"&to="+getWeeks[15]+"'}"+','
            +"{y:"+volNNJ_10 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=New Jersey&location=52&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[16]+"&to="+getWeeks[17]+"'}"+','
            +"{y:"+volNNJ_11+",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=New Jersey&location=52&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[18]+"&to="+getWeeks[19]+"'}"+','
            +"{y:"+volNNJ_12 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=New Jersey&location=52&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[20]+"&to="+getWeeks[21]+"'}"+''
            +"]},";
        series +=
            "{name: 'Toronto',type: 'column',data: [" +
            "{y:" +volTOR_0 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Toronto&location=23&product="+productCat+"&cattype="+catGrid+"&from=past&to=past'}," +
            "{y:" +volTOR_1 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Toronto&location=23&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[0]+"&to="+getWeeks[1]+"'}"+','
            +"{y:"+volTOR_3 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Toronto&location=23&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[2]+"&to="+getWeeks[3]+"'}"+','
            +"{y:"+volTOR_4 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Toronto&location=23&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[4]+"&to="+getWeeks[5]+"'}"+','
            +"{y:"+volTOR_5 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Toronto&location=23&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[6]+"&to="+getWeeks[7]+"'}"+','
            +"{y:"+volTOR_6 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Toronto&location=23&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[8]+"&to="+getWeeks[9]+"'}"+','
            +"{y:"+volTOR_7 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Toronto&location=23&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[10]+"&to="+getWeeks[11]+"'}"+','
            +"{y:"+volTOR_8 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Toronto&location=23&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[12]+"&to="+getWeeks[13]+"'}"+','
            +"{y:"+volTOR_9 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Toronto&location=23&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[14]+"&to="+getWeeks[15]+"'}"+','
            +"{y:"+volTOR_10 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Toronto&location=23&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[16]+"&to="+getWeeks[17]+"'}"+','
            +"{y:"+volTOR_11+",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Toronto&location=23&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[18]+"&to="+getWeeks[19]+"'}"+','
            +"{y:"+volTOR_12 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Toronto&location=23&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[20]+"&to="+getWeeks[21]+"'}"+''
            +"]},";
        series +=
            "{name: 'Vancouver',type: 'column',data: [" +
            "{y:" +volVAN_0 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Vancouver&location=26&product="+productCat+"&cattype="+catGrid+"&from=past&to=past'}," +
            "{y:" +volVAN_1 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Vancouver&location=26&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[0]+"&to="+getWeeks[1]+"'}"+','
            +"{y:"+volVAN_3 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Vancouver&location=26&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[2]+"&to="+getWeeks[3]+"'}"+','
            +"{y:"+volVAN_4 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Vancouver&location=26&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[4]+"&to="+getWeeks[5]+"'}"+','
            +"{y:"+volVAN_5 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Vancouver&location=26&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[6]+"&to="+getWeeks[7]+"'}"+','
            +"{y:"+volVAN_6 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Vancouver&location=26&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[8]+"&to="+getWeeks[9]+"'}"+','
            +"{y:"+volVAN_7 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Vancouver&location=26&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[10]+"&to="+getWeeks[11]+"'}"+','
            +"{y:"+volVAN_8 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Vancouver&location=26&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[12]+"&to="+getWeeks[13]+"'}"+','
            +"{y:"+volVAN_9 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Vancouver&location=26&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[14]+"&to="+getWeeks[15]+"'}"+','
            +"{y:"+volVAN_10 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Vancouver&location=26&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[16]+"&to="+getWeeks[17]+"'}"+','
            +"{y:"+volVAN_11+",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Vancouver&location=26&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[18]+"&to="+getWeeks[19]+"'}"+','
            +"{y:"+volVAN_12 +",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Vancouver&location=26&product="+productCat+"&cattype="+catGrid+"&from="+getWeeks[20]+"&to="+getWeeks[21]+"'}"+''
            +"]},";
    }

    if(location1==0) {
        series +=
            "{name: 'Space',type: 'column',data: [" +
            "{y:" + volSP_0 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Space&location="+locGrid+"&product=10&cattype=" + catGrid + "&from=past&to=past'}," +
            "{y:" + volSP_1 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Space&location="+locGrid+"&product=10&cattype=" + catGrid + "&from=" + getWeeks[0] + "&to=" + getWeeks[1] + "'}" + ','
            + "{y:" + volSP_3 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Space&location="+locGrid+"&product=10&cattype=" + catGrid + "&from=" + getWeeks[2] + "&to=" + getWeeks[3] + "'}" + ','
            + "{y:" + volSP_4 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Space&location="+locGrid+"&product=10&cattype=" + catGrid + "&from=" + getWeeks[4] + "&to=" + getWeeks[5] + "'}" + ','
            + "{y:" + volSP_5 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Space&location="+locGrid+"&product=10&cattype=" + catGrid + "&from=" + getWeeks[6] + "&to=" + getWeeks[7] + "'}" + ','
            + "{y:" + volSP_6 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Space&location="+locGrid+"&product=10&cattype=" + catGrid + "&from=" + getWeeks[8] + "&to=" + getWeeks[9] + "'}" + ','
            + "{y:" + volSP_7 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Space&location="+locGrid+"&product=10&cattype=" + catGrid + "&from=" + getWeeks[10] + "&to=" + getWeeks[11] + "'}" + ','
            + "{y:" + volSP_8 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Space&location="+locGrid+"&product=10&cattype=" + catGrid + "&from=" + getWeeks[12] + "&to=" + getWeeks[13] + "'}" + ','
            + "{y:" + volSP_9 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Space&location="+locGrid+"&product=10&cattype=" + catGrid + "&from=" + getWeeks[14] + "&to=" + getWeeks[15] + "'}" + ','
            + "{y:" + volSP_10 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Space&location="+locGrid+"&product=10&cattype=" + catGrid + "&from=" + getWeeks[16] + "&to=" + getWeeks[17] + "'}" + ','
            + "{y:" + volSP_11 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Space&location="+locGrid+"&product=10&cattype=" + catGrid + "&from=" + getWeeks[18] + "&to=" + getWeeks[19] + "'}" + ','
            + "{y:" + volSP_12 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Space&location="+locGrid+"&product=10&cattype=" + catGrid + "&from=" + getWeeks[20] + "&to=" + getWeeks[21] + "'}" + ''
            + "]},";
        series +=
            "{name: 'Power',type: 'column',data: [" +
            "{y:" + volPW_0 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Power&location="+locGrid+"&product=8&cattype=" + catGrid + "&from=past&to=past'}," +
            "{y:" + volPW_1 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Power&location="+locGrid+"&product=8&cattype=" + catGrid + "&from=" + getWeeks[0] + "&to=" + getWeeks[1] + "'}" + ','
            + "{y:" + volPW_3 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Power&location="+locGrid+"&product=8&cattype=" + catGrid + "&from=" + getWeeks[2] + "&to=" + getWeeks[3] + "'}" + ','
            + "{y:" + volPW_4 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Power&location="+locGrid+"&product=8&cattype=" + catGrid + "&from=" + getWeeks[4] + "&to=" + getWeeks[5] + "'}" + ','
            + "{y:" + volPW_5 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Power&location="+locGrid+"&product=8&cattype=" + catGrid + "&from=" + getWeeks[6] + "&to=" + getWeeks[7] + "'}" + ','
            + "{y:" + volPW_6 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Power&location="+locGrid+"&product=8&cattype=" + catGrid + "&from=" + getWeeks[8] + "&to=" + getWeeks[9] + "'}" + ','
            + "{y:" + volPW_7 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Power&location="+locGrid+"&product=8&cattype=" + catGrid + "&from=" + getWeeks[10] + "&to=" + getWeeks[11] + "'}" + ','
            + "{y:" + volPW_8 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Power&location="+locGrid+"&product=8&cattype=" + catGrid + "&from=" + getWeeks[12] + "&to=" + getWeeks[13] + "'}" + ','
            + "{y:" + volPW_9 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Power&location="+locGrid+"&product=8&cattype=" + catGrid + "&from=" + getWeeks[14] + "&to=" + getWeeks[15] + "'}" + ','
            + "{y:" + volPW_10 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Power&location="+locGrid+"&product=8&cattype=" + catGrid + "&from=" + getWeeks[16] + "&to=" + getWeeks[17] + "'}" + ','
            + "{y:" + volPW_11 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Power&location="+locGrid+"&product=8&cattype=" + catGrid + "&from=" + getWeeks[18] + "&to=" + getWeeks[19] + "'}" + ','
            + "{y:" + volPW_12 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Power&location="+locGrid+"&product=8&cattype=" + catGrid + "&from=" + getWeeks[20] + "&to=" + getWeeks[21] + "'}" + ''
            + "]},";

        series +=
            "{name: 'XC',type: 'column',data: [" +
            "{y:" + volXC_0 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Interconnection&location="+locGrid+"&product=4&cattype=" + catGrid + "&from=past&to=past'}," +
            "{y:" + volXC_1 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Interconnection&location="+locGrid+"&product=4&cattype=" + catGrid + "&from=" + getWeeks[0] + "&to=" + getWeeks[1] + "'}" + ','
            + "{y:" + volXC_3 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Interconnection&location="+locGrid+"&product=4&cattype=" + catGrid + "&from=" + getWeeks[2] + "&to=" + getWeeks[3] + "'}" + ','
            + "{y:" + volXC_4 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Interconnection&location="+locGrid+"&product=4&cattype=" + catGrid + "&from=" + getWeeks[4] + "&to=" + getWeeks[5] + "'}" + ','
            + "{y:" + volXC_5 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Interconnection&location="+locGrid+"&product=4&cattype=" + catGrid + "&from=" + getWeeks[6] + "&to=" + getWeeks[7] + "'}" + ','
            + "{y:" + volXC_6 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Interconnection&location="+locGrid+"&product=4&cattype=" + catGrid + "&from=" + getWeeks[8] + "&to=" + getWeeks[9] + "'}" + ','
            + "{y:" + volXC_7 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Interconnection&location="+locGrid+"&product=4&cattype=" + catGrid + "&from=" + getWeeks[10] + "&to=" + getWeeks[11] + "'}" + ','
            + "{y:" + volXC_8 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Interconnection&location="+locGrid+"&product=4&cattype=" + catGrid + "&from=" + getWeeks[12] + "&to=" + getWeeks[13] + "'}" + ','
            + "{y:" + volXC_9 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Interconnection&location="+locGrid+"&product=4&cattype=" + catGrid + "&from=" + getWeeks[14] + "&to=" + getWeeks[15] + "'}" + ','
            + "{y:" + volXC_10 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Interconnection&location="+locGrid+"&product=4&cattype=" + catGrid + "&from=" + getWeeks[16] + "&to=" + getWeeks[17] + "'}" + ','
            + "{y:" + volXC_11 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Interconnection&location="+locGrid+"&product=4&cattype=" + catGrid + "&from=" + getWeeks[18] + "&to=" + getWeeks[19] + "'}" + ','
            + "{y:" + volXC_12 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Interconnection&location="+locGrid+"&product=4&cattype=" + catGrid + "&from=" + getWeeks[20] + "&to=" + getWeeks[21] + "'}" + ''
            + "]},";
        series +=
            "{name: 'Virtual XC',type: 'column',data: [" +
            "{y:" + volVXC_0 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Virtual Interconnection&location="+locGrid+"&product=11&cattype=" + catGrid + "&from=past&to=past'}," +
            "{y:" + volVXC_1 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Virtual Interconnection&location="+locGrid+"&product=11&cattype=" + catGrid + "&from=" + getWeeks[0] + "&to=" + getWeeks[1] + "'}" + ','
            + "{y:" + volVXC_3 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Virtual Interconnection&location="+locGrid+"&product=11&cattype=" + catGrid + "&from=" + getWeeks[2] + "&to=" + getWeeks[3] + "'}" + ','
            + "{y:" + volVXC_4 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Virtual Interconnection&location="+locGrid+"&product=11&cattype=" + catGrid + "&from=" + getWeeks[4] + "&to=" + getWeeks[5] + "'}" + ','
            + "{y:" + volVXC_5 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Virtual Interconnection&location="+locGrid+"&product=11&cattype=" + catGrid + "&from=" + getWeeks[6] + "&to=" + getWeeks[7] + "'}" + ','
            + "{y:" + volVXC_6 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Virtual Interconnection&location="+locGrid+"&product=11&cattype=" + catGrid + "&from=" + getWeeks[8] + "&to=" + getWeeks[9] + "'}" + ','
            + "{y:" + volVXC_7 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Virtual Interconnection&location="+locGrid+"&product=11&cattype=" + catGrid + "&from=" + getWeeks[10] + "&to=" + getWeeks[11] + "'}" + ','
            + "{y:" + volVXC_8 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Virtual Interconnection&location="+locGrid+"&product=11&cattype=" + catGrid + "&from=" + getWeeks[12] + "&to=" + getWeeks[13] + "'}" + ','
            + "{y:" + volVXC_9 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Virtual Interconnection&location="+locGrid+"&product=11&cattype=" + catGrid + "&from=" + getWeeks[14] + "&to=" + getWeeks[15] + "'}" + ','
            + "{y:" + volVXC_10 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Virtual Interconnection&location="+locGrid+"&product=11&cattype=" + catGrid + "&from=" + getWeeks[16] + "&to=" + getWeeks[17] + "'}" + ','
            + "{y:" + volVXC_11 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Virtual Interconnection&location="+locGrid+"&product=11&cattype=" + catGrid + "&from=" + getWeeks[18] + "&to=" + getWeeks[19] + "'}" + ','
            + "{y:" + volVXC_12 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Virtual Interconnection&location="+locGrid+"&product=11&cattype=" + catGrid + "&from=" + getWeeks[20] + "&to=" + getWeeks[21] + "'}" + ''
            + "]},";
        series +=
            "{name: 'Network',type: 'column',data: [" +
            "{y:" + volNW_0 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Network&location="+locGrid+"&product=5&cattype=" + catGrid + "&from=past&to=past'}," +
            "{y:" + volNW_1 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Network&location="+locGrid+"&product=5&cattype=" + catGrid + "&from=" + getWeeks[0] + "&to=" + getWeeks[1] + "'}" + ','
            + "{y:" + volNW_3 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Network&location="+locGrid+"&product=5&cattype=" + catGrid + "&from=" + getWeeks[2] + "&to=" + getWeeks[3] + "'}" + ','
            + "{y:" + volNW_4 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Network&location="+locGrid+"&product=5&cattype=" + catGrid + "&from=" + getWeeks[4] + "&to=" + getWeeks[5] + "'}" + ','
            + "{y:" + volNW_5 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Network&location="+locGrid+"&product=5&cattype=" + catGrid + "&from=" + getWeeks[6] + "&to=" + getWeeks[7] + "'}" + ','
            + "{y:" + volNW_6 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Network&location="+locGrid+"&product=5&cattype=" + catGrid + "&from=" + getWeeks[8] + "&to=" + getWeeks[9] + "'}" + ','
            + "{y:" + volNW_7 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Network&location="+locGrid+"&product=5&cattype=" + catGrid + "&from=" + getWeeks[10] + "&to=" + getWeeks[11] + "'}" + ','
            + "{y:" + volNW_8 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Network&location="+locGrid+"&product=5&cattype=" + catGrid + "&from=" + getWeeks[12] + "&to=" + getWeeks[13] + "'}" + ','
            + "{y:" + volNW_9 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Network&location="+locGrid+"&product=5&cattype=" + catGrid + "&from=" + getWeeks[14] + "&to=" + getWeeks[15] + "'}" + ','
            + "{y:" + volNW_10 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Network&location="+locGrid+"&product=5&cattype=" + catGrid + "&from=" + getWeeks[16] + "&to=" + getWeeks[17] + "'}" + ','
            + "{y:" + volNW_11 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Network&location="+locGrid+"&product=5&cattype=" + catGrid + "&from=" + getWeeks[18] + "&to=" + getWeeks[19] + "'}" + ','
            + "{y:" + volNW_12 + ",url:'/app/site/hosting/scriptlet.nl?script=571&deploy=1&title=Network&location="+locGrid+"&product=5&cattype=" + catGrid + "&from=" + getWeeks[20] + "&to=" + getWeeks[21] + "'}" + ''
            + "]},";
    }
    return series;
}


function return_weeks (){
    var arrWeeks = '';
    for ( var i = 0; i < 12; i++ ) {
        arrWeeks +="'"+(moment().add(i, 'weeks').endOf('isoWeek').format('M/D'))+"',";
    }
    var strLen = arrWeeks.length;
    arrWeeks = arrWeeks.slice(0,strLen-1);
    return arrWeeks;
}
function return_weeksforSearch (){
    var arrWeeks = new Array();
    for ( var i = 0; i < 11; i++ ) {
        arrWeeks.push((moment().add(i, 'weeks').startOf('isoWeek').format('MM/DD/YYYY')));
        arrWeeks.push((moment().add(i, 'weeks').endOf('isoWeek').format('MM/DD/YYYY')));
    }
    return arrWeeks;
}
function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}