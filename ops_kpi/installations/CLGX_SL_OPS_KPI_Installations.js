//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_OPS_KPI_Installations.js
//	Script Name:	CLGX_SL_OPS_KPI_Installations
//	Script Id:		customscript_clgx_sl_ops_kpi_install
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		06/24/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=510&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_ops_kpi_install (request, response){
    try {

        var script = request.getParameter('script');
        var title = request.getParameter('title');
        var location = request.getParameter('location');
        var product = request.getParameter('product');

        var objFile = nlapiLoadFile(2741888);
        var html = objFile.getValue();
        html = html.replace(new RegExp('{weeks}','g'), return_weeks());
        html = html.replace(new RegExp('{title}','g'), 'INstallation Intervals - ' + title);
        response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function return_weeks (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 13; i++ ) {
        arrWeeks.push(moment().endOf('week').add(1, 'days').subtract((13 - i), 'week').format('M/D'));
    }
    return JSON.stringify(arrWeeks);
}
