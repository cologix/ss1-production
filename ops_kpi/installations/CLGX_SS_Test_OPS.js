nlapiLogExecution("audit","FLOStart",new Date().getTime());
function main() {
    var returnFunctionDatesGrid = return_weeksforGrid();
    var from1=returnFunctionDatesGrid[11];
    var to1=addDays(from1,6);
    var x=0;
}
function addDays(theDate, days) {
    theDate=new Date(theDate);
    return new Date(theDate.getTime() + days*24*60*60*1000);
}
function return_weeksforGrid (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 13; i++ ) {
        arrWeeks.push(moment().startOf('week').add(1, 'days').subtract((13 - i), 'week').format('MM/DD/YYYY'));
    }
    return arrWeeks;
}
