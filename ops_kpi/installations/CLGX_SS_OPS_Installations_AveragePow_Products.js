nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_OPS_Installations_AveragePow_Products.js
//	Script Name:	CLGX_SS_OPS_Installations_AveragePow_Products
//	Script Id:		customscript_ops_installations_average_pow
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Released:		July/2015
//-------------------------------------------------------------------------------------------------

function scheduled_create_averageStandard_json () {
    try {
        nlapiLogExecution('DEBUG','Scheduled Script - Create_averageStandard_json','|--------------------STARTED---------------------|');
        //------------- Begin Section 1 -----------------------------------------------------------------------------------
        // Version:	1.1 - 2/8/2012
        // Details:	Creates 4 types of audit cases on the 1st of every month for each facility
        //-----------------------------------------------------------------------------------------------------------------
        var arrAvgSeries=getAvgSeries();
        var seriesForJSON='['+arrAvgSeries+']';
        //  var str = JSON.stringify(seriesForJSON);
        var fileName = nlapiCreateFile('powerAvStandard.json', 'PLAINTEXT', seriesForJSON);
        fileName.setFolder(2275492);
        nlapiSubmitFile(fileName);
        nlapiLogExecution('DEBUG','Scheduled Script - Create_averageStandard_json','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
function return_weeksDates (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 13; i++ ) {
        arrWeeks.push(moment().startOf('week').add(1, 'days').subtract((13 - i), 'week').format('MM/DD/YYYY'));
    }
    var arrayDates=[arrWeeks[0],arrWeeks[arrWeeks.length-1]]
    return arrayDates;
}
function return_weeksforGrid (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 13; i++ ) {
        arrWeeks.push(moment().startOf('week').add(1, 'days').subtract((13 - i), 'week').format('MM/DD/YYYY'));
    }
    return arrWeeks;
}
function addDays(theDate, days) {
    theDate=new Date(theDate);
    return new Date(theDate.getTime() + days*24*60*60*1000);
}
function returnDateStringDT(date){
    var to=addDays(date, 6);
    var dd = to.getDate();
    var mm = to.getMonth() + 1;
    var y = to.getFullYear();
    var toString = mm+ '/'+ dd + '/'+ y;
    return toString;
}
function returnDateString(location,from){
    var toString =returnDateStringDT(from);
    var arrFilters=new Array();
    if(location!=0)
    {
        var arrLocation=clgx_return_child_locations_of_marketid(location);
        if(arrLocation.length==0)
        {
            arrLocation.push(location);
        }
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",arrLocation));
    }
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from,toString));
    return arrFilters;
}
function getAvgSeriesPerWeek(from, to, location,item){
    var arrFilters = new Array();
    var arrColumns = new Array();
    var countSpaceArr=new Array();
    var countNetworkArr=new Array();
    var countPowerArr=new Array();
    var countXCArr=new Array();
    var countVXCArr=new Array();
    var countSpaceSTArr=new Array();
    var countNetworSTkArr=new Array();
    var countPowerSTArr=new Array();
    var countXCSTArr=new Array();
    var countVXSTCArr=new Array();
    var arrFilters = new Array();
    var arrColumns = new Array();
    if(location.length>0)
    {
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",location));
    }
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from,to));
    // arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category","item","anyof",item));
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_install_vol_s_2', arrFilters, arrColumns);
    var arrSOIDs=new Array();
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var itemst=searchIn.getValue(columns[2]);
        var id=searchIn.getValue(columns[3]);
        if(itemst=="Non-Standard")
        {
            arrSOIDs.push(id);
        }
    }
    var spAv=0;
    var spNum=0;
    var vxcAv=0;
    var vxcNum=0;
    var xcAv=0;
    var xcNum=0;
    var netAv=0;
    var netNum=0;
    var powAv=0;
    var powNum=0;

    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var itemcat=searchIn.getText(columns[0]);
        var id=searchIn.getValue(columns[3]);
        if((itemcat=="Power")&&(!in_array(id,arrSOIDs)))
        {
            spAv=parseFloat(spAv)+parseFloat(searchIn.getValue(columns[1]));
            spNum++;
        }

    }
    if(spAv>0)
    {
        countSpaceArr.push(parseFloat(parseFloat(spAv)/parseFloat(spNum)).toFixed(1));
    }
    else
    {
        countSpaceArr.push(0);
    }

    var arrayReturn=[countSpaceArr[0],parseInt(spNum)];
    return arrayReturn;
}

function getNSIds(from, to){

    var arrFilters = new Array();
    var arrColumns = new Array();

    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from,to));
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_install_vol___2', arrFilters, arrColumns);

    var arrSOIDs=new Array();
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var id=searchIn.getValue(columns[0]);

        arrSOIDs.push(id);

    }
    return arrSOIDs;
}
function getAvgSeriesPerWeekNST_(arrSOs,from, to, location,item){
    var arrFilters = new Array();
    var arrColumns = new Array();
    var countSpaceArr=new Array();
    var countNetworkArr=new Array();
    var countPowerArr=new Array();
    var countXCArr=new Array();
    var countVXCArr=new Array();
    var countSpaceSTArr=new Array();
    var countNetworSTkArr=new Array();
    var countPowerSTArr=new Array();
    var countXCSTArr=new Array();
    var countVXSTCArr=new Array();
    var arrFilters = new Array();
    var arrColumns = new Array();
    if(location.length>0)
    {
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",location));
    }
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from,to));

    arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category","item","anyof",item));
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_install_vol__12', arrFilters, arrColumns);

    var spAv=0;
    var spNum=0;

    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var id=searchIn.getValue(columns[1]);
        if((in_array(id,arrSOs))&&(lastid!=id))
        {
            spAv=parseFloat(spAv)+parseFloat(searchIn.getValue(columns[0]));
            spNum++;
        }
        var lastid=id;
    }
    if(spAv>0)
    {
        countSpaceArr.push(parseFloat(parseFloat(spAv)/parseFloat(spNum)).toFixed(1));
    }
    else
    {
        countSpaceArr.push(0);
    }

    var arrayReturn=[countSpaceArr[0],spNum];
    return arrayReturn;
}
function getAvgSeries(){
    var seriesReturn='';
    var returnFunctionDatesGrid=return_weeksforGrid();
    //All Markets
    var location=[];
    var item=8;
    var to12=addDays(returnFunctionDatesGrid[0],6);
    var from12=addDays(returnFunctionDatesGrid[0],-84);
    //week11
    var from11=addDays(returnFunctionDatesGrid[1],-84);
    var to11=addDays(returnFunctionDatesGrid[1],6);
    //week10
    var from10=addDays(returnFunctionDatesGrid[2],-84);
    var to10=addDays(returnFunctionDatesGrid[2],6);
    //week9
    var from9=addDays(returnFunctionDatesGrid[3],-84);
    var to9=addDays(returnFunctionDatesGrid[3],6);
    //week8
    var from8=addDays(returnFunctionDatesGrid[4],-84);
    var to8=addDays(returnFunctionDatesGrid[4],6);
    //week7
    var from7=addDays(returnFunctionDatesGrid[5],-84);
    var to7=addDays(returnFunctionDatesGrid[5],6);
    //week6
    var from6=addDays(returnFunctionDatesGrid[6],-84);
    var to6=addDays(returnFunctionDatesGrid[6],6);
    //week5
    var from5=addDays(returnFunctionDatesGrid[7],-84);
    var to5=addDays(returnFunctionDatesGrid[7],6);
    //week4
    var from4=addDays(returnFunctionDatesGrid[8],-84);
    var to4=addDays(returnFunctionDatesGrid[8],6);

    //week3
    var from3=addDays(returnFunctionDatesGrid[9],-84);
    var to3=addDays(returnFunctionDatesGrid[9],6);

    //week2
    var from2=addDays(returnFunctionDatesGrid[10],-84);
    var to2=addDays(returnFunctionDatesGrid[10],6);
    //week1
    var from1=addDays(returnFunctionDatesGrid[11],-84);
    var to1=addDays(returnFunctionDatesGrid[11],6);
    var arrSOs=getNSIds(from12, to1);


    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    //week11

    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    //week10

    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    //week9

    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    //week8

    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    //week7

    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    //week6

    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    //week5

    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    //week4

    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    //week3

    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    //week1

    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);

    var seriesCorporateStandardPower='{name: \'Corporate Average\',type: \'spline\',color: \'#4D4D4D\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}';

    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "ALL": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+'"] },';


    //Columbus Market

    var location=[34,39];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Columbus Market&location=34;39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "COL": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+'"] },';
    var seriesCorporateStandardPowerCOL='{name: \'Columbus Average\',type: \'spline\',color: \'#DF3D82\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}';


    //Col1&2 Facility
    //week12
    var location=[34];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=34;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=34;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=34;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=34;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=34;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=34;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=34;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=34;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=34;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=34;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=34;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=34;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "COL12": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=34;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerCOL+'"] },';
    //Col3 Facility
    //week12
    var location=[39];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Col12 Facility&location=39;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "COL3": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=39;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerCOL+'"] },';

    //Dallas Market
    //week12
    var location=[2,17];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "Dallas": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+'"] },';
    var seriesCorporateStandardPowerDAL='{name: \'Dallas Average\',type: \'spline\',color: \'#DF3D82\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}';

    //DAL1 Facility
    //week12
    var location=[2];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "DAL1": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=2;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerDAL+'"] },';

    //DAL2 Facility
    //week12
    var location=[17];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "DAL2": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=17;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerDAL+'"] },';

    //Jacksonville Market
    //week12
    var location=[31];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "Jacksonville": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+'"] },';
    var seriesCorporateStandardPowerJAC='{name: \'Jacksonville Average\',type: \'spline\',color: \'#DF3D82\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}';


    //JAX1 Facility
    //week12
    var location=[31];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "JAX1": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=31;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerJAC+'"] },';
    //Jacksonville2 Market
    //week12
    var location=[40];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "Jacksonville2": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+'"] },';
    var seriesCorporateStandardPowerJAC2='{name: \'Jacksonville Average\',type: \'spline\',color: \'#DF3D82\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}';
    //JAX2 Facility
    //week12
    var location=[40];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "JAX2": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=40;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerJAC2+'"] },';
    //Lakeland Market
    //week12
    var location=[42];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "Lakeland": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+'"] },';
    var seriesCorporateStandardPowerLak='{name: \'Lakeland Average\',type: \'spline\',color: \'#DF3D82\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}';

    //LAK1 Facility
    //week12
    var location=[42];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "LAK1": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=42;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerLak+'"] },';
    //Minneapolis Market
    //week12
    var location=[16,35];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "Minneapolis": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+'"] },';
    var seriesCorporateStandardPowerMIN='{name: \'Minneapolis Average\',type: \'spline\',color: \'#DF3D82\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}';


    //MIN1&2 Facility
    //week12
    var location=[16];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);
    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "MIN12": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=16;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerMIN+'"] },';

    var location=[35];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "MIN3": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=35;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerMIN+'"] },';

    //Montreal Market
    //week12
    var location=[5,8,9,10,11,12,27];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "Montreal": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+'"] },';
    var seriesCorporateStandardPowerMTL='{name: \'Montreal Average\',type: \'spline\',color: \'#DF3D82\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;8;9;10;11;12;27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}';

    //MTL1
    //week12
    var location=[5];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "MTL1": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=5;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerMTL+'"] },';

    //MTL2
    //week12
    var location=[8];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "MTL2": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=8;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerMTL+'"] },';

    //MTL3
    //week12
    var location=[9];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "MTL3": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=9;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerMTL+'"] },';

    //MTL4
    //week12
    var location=[10];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);


    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "MTL4": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=10;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerMTL+'"] },';

    //MTL5
    //week12
    var location=[11];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "MTL5": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=11;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerMTL+'"] },';

    //MTL6
    //week12
    var location=[12];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);


    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "MTL6": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=12;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerMTL+'"] },';

    //MTL7
    //week12
    var location=[27];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "MTL7": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=27;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerMTL+'"] },';

    //New Jersey Market
    //week12
    var location=[53,54,55,56];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "NewJersey": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+'"] },';
    var seriesCorporateStandardPowerMTL='{name: \'New Jersey Average\',type: \'spline\',color: \'#DF3D82\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;54;55;56&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}';

    //NNJ1
    //week12
    var location=[53];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "NNJ1": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=53;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerMTL+'"] },';

    //NNJ2
    //week12
    var location=[54];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "NNJ2": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=54;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerMTL+'"] },';

    //NNJ3
    //week12
    var location=[55];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "NNJ3": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=55;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerMTL+'"] },';

    //NNJ4
    //week12
    var location=[56];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);


    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "NNJ4": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=56;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerMTL+'"] },';

    //Toronto Market
    //week12
    var location=[6,15];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "Toronto": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+'"] },';
    var seriesCorporateStandardPowerTOR='{name: \'Toronto Average\',type: \'spline\',color: \'#DF3D82\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}';


    //TOR1
    //week12
    var location=[6];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "TOR1": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=6;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerTOR+'"] },';

    //TOR2
    //week12
    var location=[15];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "TOR2": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=15;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerTOR+'"] },';

    //Vancouver
    //week12
    var location=[28,7];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "Vancouver": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+'"] },';
    var seriesCorporateStandardPowerVAN='{name: \'Vancouver Average\',type: \'spline\',color: \'#DF3D82\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}';

    //VAN1
    //week12
    var location=[7];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "VAN1": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=7;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerVAN+'"] },';
    //VAN1
    //week12
    var location=[28];
    var values12Week =getAvgSeriesPerWeek(from12,to12,location,item);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,location,item);
    //week11
    var values11Week =getAvgSeriesPerWeek(from11,to11,location,item);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,location,item);
    //week10
    var values10Week =getAvgSeriesPerWeek(from10,to10,location,item);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,location,item);
    //week9
    var values9Week =getAvgSeriesPerWeek(from9,to9,location,item);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,location,item);
    //week8
    var values8Week =getAvgSeriesPerWeek(from8,to8,location,item);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,location,item);
    //week7
    var values7Week =getAvgSeriesPerWeek(from7,to7,location,item);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,location,item);
    //week6
    var values6Week =getAvgSeriesPerWeek(from6,to6,location,item);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,location,item);
    //week5
    var values5Week =getAvgSeriesPerWeek(from5,to5,location,item);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,location,item);
    //week4
    var values4Week =getAvgSeriesPerWeek(from4,to4,location,item);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,location,item);
    //week3
    var values3Week =getAvgSeriesPerWeek(from3,to3,location,item);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,location,item);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location,item);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,location,item);
    //week1
    var values1Week =getAvgSeriesPerWeek(from1,to1,location,item);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,location,item);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#b172b2',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    seriesReturn +='{ "VAN2": ["{name: \'Power\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=28;&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+','+seriesCorporateStandardPower+','+seriesCorporateStandardPowerVAN+'"] },';
    return seriesReturn;
}


function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}