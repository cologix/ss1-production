nlapiLogExecution("audit","FLOStart",new Date().getTime());
function getGridSeries(request, response)
{

    var title = request.getParameter('title');
    var location = request.getParameter('location');
    var cattype=request.getParameter('cattype');
    var product = request.getParameter('product');
    var week = request.getParameter('week');
    var comes = request.getParameter('comes');
    var ndays = request.getParameter('ndays');



   // product=10 //space
   // product=5//net
   // product=11//vxc
   // product=4//xc
   // product=8//power
    var target=5;
    var targetP="F";
    if(product==11)//vxc
    {
        targetP="F";
        target=0;
    }
    if(product==10 || product==5 || product==4 ||product==8)
    {
        targetP="T";
    }
    if(product==4)//xc
    {

        target=2;
    }
    var productarr=[product];
    if(product=='all')
    {
        var productarr=[10,11,4,5,8];
    }
    var cattypearr=[cattype];
    if(cattype=='all')
    {
        var cattypearr=[1,2];
    }
    var locationarr=new Array();
    if(comes==2)
    {
        var locationSplit=location.split(';');
        for(var j=0;j<locationSplit.length;j++)
        {
            if(locationSplit[j]!='')
            {
                locationarr.push(locationSplit[j]);
            }
        }
    }
    if((location==0)||(location=='all'))
    {
        locationarr=[];
    }else{
        if(comes!=2)
        {
            locationarr=clgx_return_child_locations_of_marketid (location);
            if(locationarr.length==0){
                locationarr=location;
            }
        }
    }

    var objFile = nlapiLoadFile(2920871);
    var html = objFile.getValue();
    html = html.replace(new RegExp('{title}','g'), 'Installation Intervals Details- ' + title);
    html = html.replace(new RegExp('{series}','g'), getSeries(cattypearr,locationarr,productarr,week,targetP,target,ndays));
    response.write( html );
}
function getSeries(cattype,location,product,week,targetP,target,ndays)
{
    var series='';
    var to=addDays(week, 6);
    var fromstring=week;
    if((ndays!=null)&&(ndays==1))
    {
        var from=addDays(week, -84);
        var dd = from.getDate();
        var mm = from.getMonth() + 1;
        var y = from.getFullYear();
        var fromstring = mm+ '/'+ dd + '/'+ y;
    }
    var dd = to.getDate();
    var mm = to.getMonth() + 1;
    var y = to.getFullYear();
    var toString = mm+ '/'+ dd + '/'+ y;
    var arrFilters = new Array();
    var arrColumns = new Array();
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",fromstring,toString));
    //  if(product.length>0)
    // {
    // arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category","item","anyof",product));
    // }
    var  catitem="all";
    if(product==10){
        var  catitem="Space";
    }
    if(product==8){
        var catitem="Power";
    }
    if(product==5){
        var catitem="Network";
    }
    if(product==4){
        var catitem="Interconnection";
    }
    if(product==11){
        var  catitem="Virtual Interconnection";
    }

    if(location.length>0)
    {
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",location));
    }
    //  if(cattype.length>0)
    // {
    //     arrFilters.push(new nlobjSearchFilter("custitem_clgx_custitem_standard","item","anyof",cattype));
    //  }
//[standard=1,nstandard=2]
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_so_install_vol_loc_s_3', arrFilters, arrColumns);
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var customer = searchIn.getText(columns[0]);
        var so = searchIn.getValue(columns[1]);
        var soid = searchIn.getValue(columns[2]);
        var assi = searchIn.getText(columns[3]);
        var totalmmr = searchIn.getValue(columns[4]);
        var totalnrc = searchIn.getValue(columns[5]);
        var loc = searchIn.getValue(columns[6]);
        var actualinstd = searchIn.getValue(columns[7]);
        var nStandard = searchIn.getValue(columns[8]);
        var standard = searchIn.getValue(columns[9]);
        var itemcat=searchIn.getValue(columns[10]);
        var installdur=searchIn.getValue(columns[11]);
        if(installdur>1)
        {
            var installdurst=searchIn.getValue(columns[11])+' days';
        }
        else
        {
            var installdurst=searchIn.getValue(columns[11])+' day';
        }
        var itemcatArr=itemcat.split(',');
        if((in_array(catitem,itemcatArr))&&(catitem!='all'))
        {
            if(cattype.length==1)
            {
                if((cattype[0]==1)&&(nStandard==0))
                {
                    series += '\n{"CUSTOMER":"' + customer +
                        '","SO":"' + so+
                        '","SOID":' +  soid +
                        ',"INSDUR":"' + installdurst+
                        '","INSDURINT":' + installdur+
                        ',"TARGETP":"' + targetP+
                        '","TARGET":' + target+
                        ',"ASSIGNEDTECHNICIAN":"' + assi+
                        '","TOTALMMR":' +  totalmmr +
                        ',"TOTALNRC":' + totalnrc +
                        ',"LOCATION":"' + loc +
                        '","ACTUALINSTALLDATE":"' + actualinstd +
                        '"},';
                }
                if((cattype[0]==2)&&(((nStandard>0)&&(standard>0))||(nStandard>0)))
                {
                    series += '\n{"CUSTOMER":"' + customer +
                        '","SO":"' + so+
                        '","SOID":' +  soid +
                        ',"INSDUR":"' + installdurst+
                        '","INSDURINT":' + installdur+
                        ',"TARGETP":"' + targetP+
                        '","TARGET":' + target+
                        ',"ASSIGNEDTECHNICIAN":"' + assi+
                        '","TOTALMMR":' +  totalmmr +
                        ',"TOTALNRC":' + totalnrc +
                        ',"LOCATION":"' + loc +
                        '","ACTUALINSTALLDATE":"' + actualinstd +
                        '"},';
                }
            }
            else if(cattype.length>1){
                series += '\n{"CUSTOMER":"' + customer +
                    '","SO":"' + so+
                    '","SOID":' +  soid +
                    ',"INSDUR":"' + installdurst+
                    '","INSDURINT":' + installdur+
                    ',"TARGETP":"' + targetP+
                    '","TARGET":' + target+
                    ',"ASSIGNEDTECHNICIAN":"' + assi+
                    '","TOTALMMR":' +  totalmmr +
                    ',"TOTALNRC":' + totalnrc +
                    ',"LOCATION":"' + loc +
                    '","ACTUALINSTALLDATE":"' + actualinstd +
                    '"},';
            }

        }

        else if(catitem=='all'){
            if(cattype.length==1)
            {
                if((cattype[0]==1)&&(nStandard==0))
                {
                    series += '\n{"CUSTOMER":"' + customer +
                        '","SO":"' + so+
                        '","SOID":' +  soid +
                        ',"INSDUR":"' + installdurst+
                        '","INSDURINT":' + installdur+
                        ',"TARGETP":"' + targetP+
                        '","TARGET":' + target+
                        ',"ASSIGNEDTECHNICIAN":"' + assi+
                        '","TOTALMMR":' +  totalmmr +
                        ',"TOTALNRC":' + totalnrc +
                        ',"LOCATION":"' + loc +
                        '","ACTUALINSTALLDATE":"' + actualinstd +
                        '"},';
                }
                if((cattype[0]==2)&&(((nStandard>0)&&(standard>0))||(nStandard>0)))
                {
                    series += '\n{"CUSTOMER":"' + customer +
                        '","SO":"' + so+
                        '","SOID":' +  soid +
                        ',"INSDUR":"' + installdurst+
                        '","INSDURINT":' + installdur+
                        ',"TARGETP":"' + targetP+
                        '","TARGET":' + target+
                        ',"ASSIGNEDTECHNICIAN":"' + assi+
                        '","TOTALMMR":' +  totalmmr +
                        ',"TOTALNRC":' + totalnrc +
                        ',"LOCATION":"' + loc +
                        '","ACTUALINSTALLDATE":"' + actualinstd +
                        '"},';
                }
            }
            else if(cattype.length>1){
                series += '\n{"CUSTOMER":"' + customer +
                    '","SO":"' + so+
                    '","SOID":' +  soid +
                    ',"INSDUR":"' + installdurst+
                    '","INSDURINT":' + installdur+
                    ',"TARGETP":"' + targetP+
                    '","TARGET":' + target+
                    ',"ASSIGNEDTECHNICIAN":"' + assi+
                    '","TOTALMMR":' +  totalmmr +
                    ',"TOTALNRC":' + totalnrc +
                    ',"LOCATION":"' + loc +
                    '","ACTUALINSTALLDATE":"' + actualinstd +
                    '"},';
            }

        }
    }

    var strLen = series.length;
    if (searchIns != null){
        series = series.slice(0,strLen-1);
    }
    return series;
}
function addDays(theDate, days) {
    theDate=new Date(theDate);
    return new Date(theDate.getTime() + days*24*60*60*1000);
}

function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}