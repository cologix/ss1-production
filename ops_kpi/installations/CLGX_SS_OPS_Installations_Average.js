nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_OPS_Installations_Average.js
//	Script Name:	CLGX_SS_OPS_Installations_Average
//	Script Id:		customscript_ops_installations_average
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Released:		July/2015
//-------------------------------------------------------------------------------------------------

function scheduled_create_averageStandard_json () {
    try {
        nlapiLogExecution('DEBUG','Scheduled Script - Create_averageStandard_json','|--------------------STARTED---------------------|');
        //------------- Begin Section 1 -----------------------------------------------------------------------------------
        // Version:	1.1 - 2/8/2012
        // Details:	Creates 4 types of audit cases on the 1st of every month for each facility
        //-----------------------------------------------------------------------------------------------------------------
        var arrAvgSeries=getAvgSeries();
        var arrAvgSeriesNFile=nlapiLoadFile(2903726);
        var arrAvgSeriesN=arrAvgSeriesNFile.getValue();
        var arrAvgSeriesAllFile=nlapiLoadFile(2906967);
        var arrAvgSeriesAll=arrAvgSeriesAllFile.getValue();
        var seriesForJSON='{"series": {"location": [{"standard":[ '+arrAvgSeries+' ]},{"nstandard":[ '+arrAvgSeriesN+' ]},{"all":[ '+arrAvgSeriesAll+' ]}]}}';
        //  var str = JSON.stringify(seriesForJSON);
        var fileName = nlapiCreateFile('installationSeries.json', 'PLAINTEXT', seriesForJSON);
        fileName.setFolder(2275492);
        nlapiSubmitFile(fileName);
        nlapiLogExecution('DEBUG','Scheduled Script - Create_averageStandard_json','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
function return_weeksDates (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 13; i++ ) {
        arrWeeks.push(moment().startOf('week').add(1, 'days').subtract((13 - i), 'week').format('MM/DD/YYYY'));
    }
    var arrayDates=[arrWeeks[0],arrWeeks[arrWeeks.length-1]]
    return arrayDates;
}
function return_weeksforGrid (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 13; i++ ) {
        arrWeeks.push(moment().startOf('week').add(1, 'days').subtract((13 - i), 'week').format('MM/DD/YYYY'));
    }
    return arrWeeks;
}
function addDays(theDate, days) {
    theDate=new Date(theDate);
    return new Date(theDate.getTime() + days*24*60*60*1000);
}
function returnDateStringDT(date){
    var to=addDays(date, 6);
    var dd = to.getDate();
    var mm = to.getMonth() + 1;
    var y = to.getFullYear();
    var toString = mm+ '/'+ dd + '/'+ y;
    return toString;
}
function returnDateString(location,from){
    var toString =returnDateStringDT(from);
    var arrFilters=new Array();
    if(location!=0)
    {
        var arrLocation=clgx_return_child_locations_of_marketid(location);
        if(arrLocation.length==0)
        {
            arrLocation.push(location);
        }
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",arrLocation));
    }
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from,toString));
    return arrFilters;
}

function getVolumeSeriesStandard (location){
    var series='';
    var arrFilters = new Array();
    var arrColumns = new Array();
    var countSpaceArr=new Array();
    var countNetworkArr=new Array();
    var countPowerArr=new Array();
    var countXCArr=new Array();
    var countVXCArr=new Array();
    var countSpaceSTArr=new Array();
    var countNetworSTkArr=new Array();
    var countPowerSTArr=new Array();
    var countXCSTArr=new Array();
    var countVXSTCArr=new Array();
    var returnFunctionDates=return_weeksDates();
    var returnFunctionDatesGrid=return_weeksforGrid();
    var to=addDays(returnFunctionDates[1], 6);
    var dd = to.getDate();
    var mm = to.getMonth() + 1;
    var y = to.getFullYear();
    var toString = mm+ '/'+ dd + '/'+ y;
    var locationString='';
    if(location.length>0)
    {
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",location));
        for(var i = 0; location != null && i < location.length; i++){
            locationString +=location[i]+';';
        }
    }
    if(location.length>0)
    {
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",location));
    }
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",returnFunctionDates[0],toString));
    var countSpW12=0;
    var countSpW11=0;
    var countSpW10=0;
    var countSpW9=0;
    var countSpW8=0;
    var countSpW7=0;
    var countSpW6=0;
    var countSpW5=0;
    var countSpW4=0;
    var countSpW3=0;
    var countSpW2=0;
    var countSpW1=0;
    //VXC
    var countVXCW12=0;
    var countVXCW11=0;
    var countVXCW10=0;
    var countVXCW9=0;
    var countVXCW8=0;
    var countVXCW7=0;
    var countVXCW6=0;
    var countVXCW5=0;
    var countVXCW4=0;
    var countVXCW3=0;
    var countVXCW2=0;
    var countVXCW1=0;

    //XC
    var countXCW12=0;
    var countXCW11=0;
    var countXCW10=0;
    var countXCW9=0;
    var countXCW8=0;
    var countXCW7=0;
    var countXCW6=0;
    var countXCW5=0;
    var countXCW4=0;
    var countXCW3=0;
    var countXCW2=0;
    var countXCW1=0;
    //Power
    var countPwW12=0;
    var countPwW11=0;
    var countPwW10=0;
    var countPwW9=0;
    var countPwW8=0;
    var countPwW7=0;
    var countPwW6=0;
    var countPwW5=0;
    var countPwW4=0;
    var countPwW3=0;
    var countPwW2=0;
    var countPwW1=0;
    //Network
    var countNtW12=0;
    var countNtW11=0;
    var countNtW10=0;
    var countNtW9=0;
    var countNtW8=0;
    var countNtW7=0;
    var countNtW6=0;
    var countNtW5=0;
    var countNtW4=0;
    var countNtW3=0;
    var countNtW2=0;
    var countNtW1=0;
    //NON-Standard
    var NcountSpW12=0;
    var NcountSpW11=0;
    var NcountSpW10=0;
    var NcountSpW9=0;
    var NcountSpW8=0;
    var NcountSpW7=0;
    var NcountSpW6=0;
    var NcountSpW5=0;
    var NcountSpW4=0;
    var NcountSpW3=0;
    var NcountSpW2=0;
    var NcountSpW1=0;
    //VXC
    var NcountVXCW12=0;
    var NcountVXCW11=0;
    var NcountVXCW10=0;
    var NcountVXCW9=0;
    var NcountVXCW8=0;
    var NcountVXCW7=0;
    var NcountVXCW6=0;
    var NcountVXCW5=0;
    var NcountVXCW4=0;
    var NcountVXCW3=0;
    var NcountVXCW2=0;
    var NcountVXCW1=0;

    //XC
    var NcountXCW12=0;
    var NcountXCW11=0;
    var NcountXCW10=0;
    var NcountXCW9=0;
    var NcountXCW8=0;
    var NcountXCW7=0;
    var NcountXCW6=0;
    var NcountXCW5=0;
    var NcountXCW4=0;
    var NcountXCW3=0;
    var NcountXCW2=0;
    var NcountXCW1=0;
    //Power
    var NcountPwW12=0;
    var NcountPwW11=0;
    var NcountPwW10=0;
    var NcountPwW9=0;
    var NcountPwW8=0;
    var NcountPwW7=0;
    var NcountPwW6=0;
    var NcountPwW5=0;
    var NcountPwW4=0;
    var NcountPwW3=0;
    var NcountPwW2=0;
    var NcountPwW1=0;
    //Network
    var NcountNtW12=0;
    var NcountNtW11=0;
    var NcountNtW10=0;
    var NcountNtW9=0;
    var NcountNtW8=0;
    var NcountNtW7=0;
    var NcountNtW6=0;
    var NcountNtW5=0;
    var NcountNtW4=0;
    var NcountNtW3=0;
    var NcountNtW2=0;
    var NcountNtW1=0;

    var from12=returnFunctionDatesGrid[0];
    var to12=addDays(from12,6);
    var dd12 = to12.getDate();
    var mm12 = to12.getMonth() + 1;
    var y12 = to12.getFullYear();
    var toString12 = mm12+ '/'+ dd12 + '/'+ y12;
    //week11
    var from11=returnFunctionDatesGrid[1];
    var to11=addDays(from11,6);
    var dd11 = to11.getDate();
    var mm11 = to11.getMonth() + 1;
    var y11 = to11.getFullYear();
    var toString11 = mm11+ '/'+ dd11 + '/'+ y11;
    //week10
    var from10=returnFunctionDatesGrid[2];
    var to10=addDays(from10,6);
    var dd10 = to10.getDate();
    var mm10 = to10.getMonth() + 1;
    var y10 = to10.getFullYear();
    var toString10 = mm10+ '/'+ dd10 + '/'+ y10;
    //week9
    var from9=returnFunctionDatesGrid[3];
    var to9=addDays(from9,6);
    var dd9 = to9.getDate();
    var mm9 = to9.getMonth() + 1;
    var y9 = to9.getFullYear();
    var toString9 = mm9+ '/'+ dd9 + '/'+ y9;
    //week8
    var from8=returnFunctionDatesGrid[4];
    var to8=addDays(from8,6);
    var dd8 = to8.getDate();
    var mm8 = to8.getMonth() + 1;
    var y8 = to8.getFullYear();
    var toString8 = mm8+ '/'+ dd8 + '/'+ y8;
    //week7
    var from7=returnFunctionDatesGrid[5];
    var to7=addDays(from7,6);
    var dd7 = to7.getDate();
    var mm7 = to7.getMonth() + 1;
    var y7 = to7.getFullYear();
    var toString7 = mm7+ '/'+ dd7 + '/'+ y7;
    //week6
    var from6=returnFunctionDatesGrid[6];
    var to6=addDays(from6,6);
    var dd6 = to6.getDate();
    var mm6 = to6.getMonth() + 1;
    var y6 = to6.getFullYear();
    var toString6 = mm6+ '/'+ dd6 + '/'+ y6;
    //week5
    var from5=returnFunctionDatesGrid[7];
    var to5=addDays(from5,6);
    var dd5 = to5.getDate();
    var mm5 = to5.getMonth() + 1;
    var y5 = to5.getFullYear();
    var toString5 = mm5+ '/'+ dd5 + '/'+ y5;
    //week4
    var from4=returnFunctionDatesGrid[8];
    var to4=addDays(from4,6);
    var dd4 = to4.getDate();
    var mm4 = to4.getMonth() + 1;
    var y4 = to4.getFullYear();
    var toString4 = mm4+ '/'+ dd4 + '/'+ y4;
    //week3
    var from3=returnFunctionDatesGrid[9];
    var to3=addDays(from3,6);
    var dd3 = to3.getDate();
    var mm3 = to3.getMonth() + 1;
    var y3 = to3.getFullYear();
    var toString3 = mm3+ '/'+ dd3 + '/'+ y3;
    //week2
    var from2=returnFunctionDatesGrid[10];
    var to2=addDays(from2,6);
    var dd2 = to2.getDate();
    var mm2 = to2.getMonth() + 1;
    var y2 = to2.getFullYear();
    var toString2 = mm2+ '/'+ dd2 + '/'+ y2;
    //week1
    var from1=returnFunctionDatesGrid[11];
    var to1=addDays(from1,6);
    var dd1 = to1.getDate();
    var mm1 = to1.getMonth() + 1;
    var y1 = to1.getFullYear();
    var toString1 = mm1+ '/'+ dd1 + '/'+ y1;

    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_install_vol_s_8', arrFilters, arrColumns);
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var itemcat=searchIn.getValue(columns[0]);
        var itemcatArr=itemcat.split(',');
        var date= new Date(searchIn.getValue(columns[2]));
        var stString= searchIn.getValue(columns[3]);
        var stArray= stString.split(',');
        if((in_array("Space",itemcatArr))&&(!in_array("Non-Standard",stArray)))
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                countSpW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                countSpW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                countSpW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                countSpW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                countSpW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                countSpW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                countSpW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                countSpW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                countSpW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                countSpW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                countSpW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                countSpW12++;
            }
        }
        if((in_array("Virtual Interconnection",itemcatArr))&&(!in_array("Non-Standard",stArray)))
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                countVXCW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                countVXCW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                countVXCW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                countVXCW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                countVXCW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                countVXCW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                countVXCW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                countVXCW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                countVXCW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                countVXCW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                countVXCW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                countVXCW12++;
            }
        }
        if((in_array("Interconnection",itemcatArr))&&(!in_array("Non-Standard",stArray)))
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                countXCW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                countXCW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                countXCW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                countXCW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                countXCW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                countXCW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                countXCW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                countXCW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                countXCW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                countXCW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                countXCW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                countXCW12++;
            }
        }
        if((in_array("Network",itemcatArr))&&(!in_array("Non-Standard",stArray)))

        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                countNtW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                countNtW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                countNtW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                countNtW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                countNtW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                countNtW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                countNtW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                countNtW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                countNtW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                countNtW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                countNtW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                countNtW12++;
            }
        }
        if((in_array("Power",itemcatArr))&&(!in_array("Non-Standard",stArray)))
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                countPwW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                countPwW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                countPwW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                countPwW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                countPwW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                countPwW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                countPwW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                countPwW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                countPwW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                countPwW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                countPwW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                countPwW12++;
            }
        }

    }
    countSpaceArr=[countSpW1,countSpW2,countSpW3,countSpW4,countSpW5,countSpW6,countSpW7,countSpW8,countSpW9,countSpW10,countSpW11,countSpW12];
    countNetworkArr=[countNtW1,countNtW2,countNtW3,countNtW4,countNtW5,countNtW6,countNtW7,countNtW8,countNtW9,countNtW10,countNtW11,countNtW12];
    countVXCArr=[countVXCW1,countVXCW2,countVXCW3,countVXCW4,countVXCW5,countVXCW6,countVXCW7,countVXCW8,countVXCW9,countVXCW10,countVXCW11,countVXCW12];
    countXCArr=[countXCW1,countXCW2,countXCW3,countXCW4,countXCW5,countXCW6,countXCW7,countXCW8,countXCW9,countXCW10,countXCW11,countXCW12];
    countPowerArr=[countPwW1,countPwW2,countPwW3,countPwW4,countPwW5,countPwW6,countPwW7,countPwW8,countPwW9,countPwW10,countPwW11,countPwW12];

    //space
    //space
    if(countSpaceArr.length>0)
    {
        series +=  "{name: 'Space',type: 'column',data: [{y:"+countSpaceArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location="+locationString+"&product=10&cattype=1&week="+returnFunctionDatesGrid[0]+"'},{y:"+countSpaceArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location="+locationString+"&product=10&cattype=1&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+countSpaceArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location="+locationString+"&product=10&cattype=1&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+countSpaceArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location="+locationString+"&product=10&cattype=1&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+countSpaceArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location="+locationString+"&product=10&cattype=1&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+countSpaceArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location="+locationString+"&product=10&cattype=1&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+countSpaceArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location="+locationString+"&product=10&cattype=1&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+countSpaceArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location="+locationString+"&product=10&cattype=1&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+countSpaceArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location="+locationString+"&product=10&cattype=1&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+countSpaceArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location="+locationString+"&product=10&cattype=1&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+countSpaceArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location="+locationString+"&product=10&cattype=1&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+countSpaceArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location="+locationString+"&product=10&cattype=1&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }


    //network
    if(countNetworkArr.length>0)
    {
        series +=  "{name: 'Network',type: 'column',data: [{y:"+countNetworkArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=NetWork&location="+locationString+"&product=5&cattype=1&week="+returnFunctionDatesGrid[0]+"'},{y:"+countNetworkArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location="+locationString+"&product=5&cattype=1&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+countNetworkArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location="+locationString+"&product=5&cattype=1&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+countNetworkArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location="+locationString+"&product=5&cattype=1&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+countNetworkArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location="+locationString+"&product=5&cattype=1&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+countNetworkArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location="+locationString+"&product=5&cattype=1&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+countNetworkArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location="+locationString+"&product=5&cattype=1&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+countNetworkArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location="+locationString+"&product=5&cattype=1&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+countNetworkArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location="+locationString+"&product=5&cattype=1&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+countNetworkArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location="+locationString+"&product=5&cattype=1&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+countNetworkArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location="+locationString+"&product=5&cattype=1&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+countNetworkArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location="+locationString+"&product=5&cattype=1&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    //x inter
    if(countVXCArr.length>0)
    {

        series +=  "{name: 'Virtual XC',type: 'column',data: [{y:"+countVXCArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location="+locationString+"&product=11&cattype=1&week="+returnFunctionDatesGrid[0]+"'},{y:"+countVXCArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location="+locationString+"&product=11&cattype=1&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+countVXCArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location="+locationString+"&product=11&cattype=1&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+countVXCArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location="+locationString+"&product=11&cattype=1&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+countVXCArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location="+locationString+"&product=11&cattype=1&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+countVXCArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location="+locationString+"&product=11&cattype=1&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+countVXCArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location="+locationString+"&product=11&cattype=1&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+countVXCArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location="+locationString+"&product=11&cattype=1&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+countVXCArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location="+locationString+"&product=11&cattype=1&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+countVXCArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location="+locationString+"&product=11&cattype=1&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+countVXCArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location="+locationString+"&product=11&cattype=1&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+countVXCArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location="+locationString+"&product=11&cattype=1&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    //inter
    if(countXCArr.length>0)
    {
        series +=  "{name: 'XC',type: 'column',data: [{y:"+countXCArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location="+locationString+"&product=4&cattype=1&week="+returnFunctionDatesGrid[0]+"'},{y:"+countXCArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location="+locationString+"&product=4&cattype=1&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+countXCArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location="+locationString+"&product=4&cattype=1&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+countXCArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location="+locationString+"&product=4&cattype=1&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+countXCArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location="+locationString+"&product=4&cattype=1&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+countXCArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location="+locationString+"&product=4&cattype=1&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+countXCArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location="+locationString+"&product=4&cattype=1&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+countXCArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location="+locationString+"&product=4&cattype=1&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+countXCArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location="+locationString+"&product=4&cattype=1&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+countXCArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location="+locationString+"&product=4&cattype=1&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+countXCArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location="+locationString+"&product=4&cattype=1&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+countXCArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location="+locationString+"&product=4&cattype=1&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    //power
    if(countPowerArr.length>0)
    {
        series +=  "{name: 'Power',type: 'column',data: [{y:"+countPowerArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location="+locationString+"&product=8&cattype=1&week="+returnFunctionDatesGrid[0]+"'},{y:"+countPowerArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location="+locationString+"&product=8&cattype=1&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+countPowerArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location="+locationString+"&product=8&cattype=1&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+countPowerArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location="+locationString+"&product=8&cattype=1&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+countPowerArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location="+locationString+"&product=8&cattype=1&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+countPowerArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location="+locationString+"&product=8&cattype=1&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+countPowerArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location="+locationString+"&product=8&cattype=1&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+countPowerArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location="+locationString+"&product=8&cattype=1&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+countPowerArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location="+locationString+"&product=8&cattype=1&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+countPowerArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location="+locationString+"&product=8&cattype=1&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+countPowerArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location="+locationString+"&product=8&cattype=1&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+countPowerArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location="+locationString+"&product=8&cattype=1&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    var searchSt = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_install_vol_n_3', arrFilters, arrColumns);

    for ( var i = 0; searchSt != null && i < searchSt.length; i++ ) {
        var searchIn = searchSt[i];
        var columns = searchIn.getAllColumns();
        var itemcat=searchIn.getValue(columns[0]);
        var itemcatArr=itemcat.split(',');
        var date= new Date(searchIn.getValue(columns[2]));
        var stString= searchIn.getValue(columns[3]);
        var stArray= stString.split(',');
        if((in_array("Space",itemcatArr))&&(in_array("Non-Standard",stArray)))
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                NcountSpW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                NcountSpW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                NcountSpW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                NcountSpW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                NcountSpW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                NcountSpW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                NcountSpW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                NcountSpW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                NcountSpW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                NcountSpW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                NcountSpW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                NcountSpW12++;
            }
        }
        if((in_array("Virtual Interconnection",itemcatArr))&&(in_array("Non-Standard",stArray)))
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                NcountVXCW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                NcountVXCW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                NcountVXCW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                NcountVXCW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                NcountVXCW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                NcountVXCW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                NcountVXCW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                NcountVXCW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                NcountVXCW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                NcountVXCW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                NcountVXCW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                NcountVXCW12++;
            }
        }
        if((in_array("Interconnection",itemcatArr))&&(in_array("Non-Standard",stArray)))
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                NcountXCW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                NcountXCW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                NcountXCW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                NcountXCW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                NcountXCW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                NcountXCW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                NcountXCW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                NcountXCW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                NcountXCW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                NcountXCW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                NcountXCW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                NcountXCW12++;
            }
        }
        if((in_array("Network",itemcatArr))&&(in_array("Non-Standard",stArray)))
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                NcountNtW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                NcountNtW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                NcountNtW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                NcountNtW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                NcountNtW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                NcountNtW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                NcountNtW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                NcountNtW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                NcountNtW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                NcountNtW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                NcountNtW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                NcountNtW12++;
            }
        }
        if((in_array("Power",itemcatArr))&&(in_array("Non-Standard",stArray)))
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                NcountPwW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                NcountPwW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                NcountPwW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                NcountPwW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                NcountPwW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                NcountPwW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                NcountPwW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                NcountPwW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                NcountPwW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                NcountPwW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                NcountPwW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                NcountPwW12++;
            }
        }

    }


    countSpaceSTArr=[NcountSpW1,NcountSpW2,NcountSpW3,NcountSpW4,NcountSpW5,NcountSpW6,NcountSpW7,NcountSpW8,NcountSpW9,NcountSpW10,NcountSpW11,NcountSpW12];
    countNetworSTkArr=[NcountNtW1,NcountNtW2,NcountNtW3,NcountNtW4,NcountNtW5,NcountNtW6,NcountNtW7,NcountNtW8,NcountNtW9,NcountNtW10,NcountNtW11,NcountNtW12];
    countVXSTCArr=[NcountVXCW1,NcountVXCW2,NcountVXCW3,NcountVXCW4,NcountVXCW5,NcountVXCW6,NcountVXCW7,NcountVXCW8,NcountVXCW9,NcountVXCW10,NcountVXCW11,NcountVXCW12];
    countXCSTArr=[NcountXCW1,NcountXCW2,NcountXCW3,NcountXCW4,NcountXCW5,NcountXCW6,NcountXCW7,NcountXCW8,NcountXCW9,NcountXCW10,NcountXCW11,NcountXCW12];
    countPowerSTArr=[NcountPwW1,NcountPwW2,NcountPwW3,NcountPwW4,NcountPwW5,NcountPwW6,NcountPwW7,NcountPwW8,NcountPwW9,NcountPwW10,NcountPwW11,NcountPwW12];


    var average11=parseFloat(parseFloat(countSpaceSTArr[11])+parseFloat(countPowerSTArr[11])+parseFloat(countNetworSTkArr[11])+parseFloat(countXCSTArr[11])+parseFloat(countVXSTCArr[11]));
    series +=  " {name: 'Non-Standard',type: 'spline','visible': false, color: '#0000CC',data: [{y:"+average11+",url:'#'}," +
        "{y:"+(parseFloat(countSpaceSTArr[10])+parseFloat(countPowerSTArr[10])+parseFloat(countNetworSTkArr[10])+parseFloat(countXCSTArr[10])+parseFloat(countVXSTCArr[10]))+",url:'#'}," +
        "{y:"+(parseFloat(countSpaceSTArr[9])+parseFloat(countPowerSTArr[9])+parseFloat(countNetworSTkArr[9])+parseFloat(countXCSTArr[9])+parseFloat(countVXSTCArr[9]))+",url:'#'}," +
        "{y:"+(parseFloat(countSpaceSTArr[8])+parseFloat(countPowerSTArr[8])+parseFloat(countNetworSTkArr[8])+parseFloat(countXCSTArr[8])+parseFloat(countVXSTCArr[8]))+",url:'#'}," +
        "{y:"+(parseFloat(countSpaceSTArr[7])+parseFloat(countPowerSTArr[7])+parseFloat(countNetworSTkArr[7])+parseFloat(countXCSTArr[7])+parseFloat(countVXSTCArr[7]))+",url:'#'}," +
        "{y:"+(parseFloat(countSpaceSTArr[6])+parseFloat(countPowerSTArr[6])+parseFloat(countNetworSTkArr[6])+parseFloat(countXCSTArr[6])+parseFloat(countVXSTCArr[6]))+",url:'#'}," +
        "{y:"+(parseFloat(countSpaceSTArr[5])+parseFloat(countPowerSTArr[5])+parseFloat(countNetworSTkArr[5])+parseFloat(countXCSTArr[5])+parseFloat(countVXSTCArr[5]))+",url:'#'}," +
        "{y:"+(parseFloat(countSpaceSTArr[4])+parseFloat(countPowerSTArr[4])+parseFloat(countNetworSTkArr[4])+parseFloat(countXCSTArr[4])+parseFloat(countVXSTCArr[4]))+",url:'#'}," +
        "{y:"+(parseFloat(countSpaceSTArr[3])+parseFloat(countPowerSTArr[3])+parseFloat(countNetworSTkArr[3])+parseFloat(countXCSTArr[3])+parseFloat(countVXSTCArr[3]))+",url:'#'}," +
        "{y:"+(parseFloat(countSpaceSTArr[2])+parseFloat(countPowerSTArr[2])+parseFloat(countNetworSTkArr[2])+parseFloat(countXCSTArr[2])+parseFloat(countVXSTCArr[2]))+",url:'#'}," +
        "{y:"+(parseFloat(countSpaceSTArr[1])+parseFloat(countPowerSTArr[1])+parseFloat(countNetworSTkArr[1])+parseFloat(countXCSTArr[1])+parseFloat(countVXSTCArr[1]))+",url:'#'}," +
        "{y:"+(parseFloat(countSpaceSTArr[0])+parseFloat(countPowerSTArr[0])+parseFloat(countNetworSTkArr[0])+parseFloat(countXCSTArr[0])+parseFloat(countVXSTCArr[0]))+",url:'#'}]}";



    return series;
}

function getAvgSeriesPerWeek(from, to, location){
    var arrFilters = new Array();
    var arrColumns = new Array();
    var countSpaceArr=new Array();
    var countNetworkArr=new Array();
    var countPowerArr=new Array();
    var countXCArr=new Array();
    var countVXCArr=new Array();
    var countSpaceSTArr=new Array();
    var countNetworSTkArr=new Array();
    var countPowerSTArr=new Array();
    var countXCSTArr=new Array();
    var countVXSTCArr=new Array();
    var arrFilters = new Array();
    var arrColumns = new Array();
    if(location.length>0)
    {
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",location));
    }
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from,to));
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_install_vol_s_2', arrFilters, arrColumns);
    var arrSOIDs=new Array();
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var itemst=searchIn.getValue(columns[2]);
        var id=searchIn.getValue(columns[3]);
        if(itemst=="Non-Standard")
        {
            arrSOIDs.push(id);
        }
    }
    var spAv=0;
    var spNum=0;
    var vxcAv=0;
    var vxcNum=0;
    var xcAv=0;
    var xcNum=0;
    var netAv=0;
    var netNum=0;
    var powAv=0;
    var powNum=0;

    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var itemcat=searchIn.getText(columns[0]);
        var id=searchIn.getValue(columns[3]);

        if((itemcat=="Space")&&(!in_array(id,arrSOIDs)))
        {
            spAv=parseFloat(spAv)+parseFloat(searchIn.getValue(columns[1]));
            spNum++;
        }
        if((itemcat=="Virtual Interconnection")&&(!in_array(id,arrSOIDs)))
        {
            vxcAv=parseFloat(vxcAv)+parseFloat(searchIn.getValue(columns[1]));
            vxcNum++;
        }

        if((itemcat=="Interconnection")&&(!in_array(id,arrSOIDs)))
        {
            xcAv=parseFloat(xcAv)+parseFloat(searchIn.getValue(columns[1]));
            xcNum++;
        }
        if((itemcat=="Network")&&(!in_array(id,arrSOIDs)))
        {
            netAv=parseFloat(netAv)+parseFloat(searchIn.getValue(columns[1]));
            netNum++;
        }
        if((itemcat=="Power")&&(!in_array(id,arrSOIDs)))
        {
            powAv=parseFloat(powAv)+parseFloat(searchIn.getValue(columns[1]));
            powNum++;
        }
    }
    if(spAv>0)
    {
        countSpaceArr.push(parseFloat(parseFloat(spAv)/parseFloat(spNum)).toFixed(1));
    }
    else
    {
        countSpaceArr.push(0);
    }
    if(vxcAv>0)
    {
        countVXCArr.push(parseFloat(parseFloat(vxcAv)/parseFloat(vxcNum)).toFixed(1));
    }
    else
    {
        countVXCArr.push(0);
    }
    if(xcAv>0)
    {
        countXCArr.push(parseFloat(parseFloat(xcAv)/parseFloat(xcNum)).toFixed(1));
    }
    else
    {
        countXCArr.push(0);
    }
    if(netAv>0)
    {
        countNetworkArr.push(parseFloat(parseFloat(netAv)/parseFloat(netNum)).toFixed(1));
    }
    else
    {
        countNetworkArr.push(0);
    }
    if(powAv>0)
    {
        countPowerArr.push(parseFloat(parseFloat(powAv)/parseFloat(powNum)).toFixed(1));
    }
    else
    {
        countPowerArr.push(0);
    }
    var arrayReturn=[countSpaceArr[0],countVXCArr[0],countXCArr[0],countNetworkArr[0],countPowerArr[0],spNum,vxcNum,xcNum,netNum,powNum];
    return arrayReturn;
}
function getAvgSeriesPerWeekNST(from, to, location){
    var arrFilters = new Array();
    var arrColumns = new Array();
    var countSpaceArr=new Array();
    var countNetworkArr=new Array();
    var countPowerArr=new Array();
    var countXCArr=new Array();
    var countVXCArr=new Array();
    var countSpaceSTArr=new Array();
    var countNetworSTkArr=new Array();
    var countPowerSTArr=new Array();
    var countXCSTArr=new Array();
    var countVXSTCArr=new Array();
    var arrFilters = new Array();
    var arrColumns = new Array();
    if(location.length>0)
    {
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",location));
    }
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from,to));
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_install_vol_s_2', arrFilters, arrColumns);
    var arrSOIDs=new Array();
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var itemst=searchIn.getValue(columns[2]);
        var id=searchIn.getValue(columns[3]);
        if(itemst=="Non-Standard")
        {
            arrSOIDs.push(id);
        }
    }
    var spAv=0;
    var spNum=0;
    var vxcAv=0;
    var vxcNum=0;
    var xcAv=0;
    var xcNum=0;
    var netAv=0;
    var netNum=0;
    var powAv=0;
    var powNum=0;

    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var itemcat=searchIn.getText(columns[0]);
        var id=searchIn.getValue(columns[3]);

        if((itemcat=="Space")&&(in_array(id,arrSOIDs)))
        {
            spAv=parseFloat(spAv)+parseFloat(searchIn.getValue(columns[1]));
            spNum++;
        }
        if((itemcat=="Virtual Interconnection")&&(in_array(id,arrSOIDs)))
        {
            vxcAv=parseFloat(vxcAv)+parseFloat(searchIn.getValue(columns[1]));
            vxcNum++;
        }

        if((itemcat=="Interconnection")&&(in_array(id,arrSOIDs)))
        {
            xcAv=parseFloat(xcAv)+parseFloat(searchIn.getValue(columns[1]));
            xcNum++;
        }
        if((itemcat=="Network")&&(in_array(id,arrSOIDs)))
        {
            netAv=parseFloat(netAv)+parseFloat(searchIn.getValue(columns[1]));
            netNum++;
        }
        if((itemcat=="Power")&&(in_array(id,arrSOIDs)))
        {
            powAv=parseFloat(powAv)+parseFloat(searchIn.getValue(columns[1]));
            powNum++;
        }
    }
    if(spAv>0)
    {
        countSpaceArr.push(parseFloat(parseFloat(spAv)/parseFloat(spNum)).toFixed(1));
    }
    else
    {
        countSpaceArr.push(0);
    }
    if(vxcAv>0)
    {
        countVXCArr.push(parseFloat(parseFloat(vxcAv)/parseFloat(vxcNum)).toFixed(1));
    }
    else
    {
        countVXCArr.push(0);
    }
    if(xcAv>0)
    {
        countXCArr.push(parseFloat(parseFloat(xcAv)/parseFloat(xcNum)).toFixed(1));
    }
    else
    {
        countXCArr.push(0);
    }
    if(netAv>0)
    {
        countNetworkArr.push(parseFloat(parseFloat(netAv)/parseFloat(netNum)).toFixed(1));
    }
    else
    {
        countNetworkArr.push(0);
    }
    if(powAv>0)
    {
        countPowerArr.push(parseFloat(parseFloat(powAv)/parseFloat(powNum)).toFixed(1));
    }
    else
    {
        countPowerArr.push(0);
    }
    var arrayReturn=[countSpaceArr[0],countVXCArr[0],countXCArr[0],countNetworkArr[0],countPowerArr[0]];
    return arrayReturn;
}
function getAvgSeriesPerWeekNST_(from, to, location){
    var arrFilters = new Array();
    var arrColumns = new Array();
    var countSpaceArr=new Array();
    var countNetworkArr=new Array();
    var countPowerArr=new Array();
    var countXCArr=new Array();
    var countVXCArr=new Array();
    var countSpaceSTArr=new Array();
    var countNetworSTkArr=new Array();
    var countPowerSTArr=new Array();
    var countXCSTArr=new Array();
    var countVXSTCArr=new Array();
    var arrFilters = new Array();
    var arrColumns = new Array();
    if(location.length>0)
    {
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",location));
    }
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from,to));
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_install_vol__12', arrFilters, arrColumns);

    var spAv=0;
    var spNum=0;
    var vxcAv=0;
    var vxcNum=0;
    var xcAv=0;
    var xcNum=0;
    var netAv=0;
    var netNum=0;
    var powAv=0;
    var powNum=0;

    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();

        var itemst=searchIn.getValue(columns[2]);
        if(itemst=="Non-Standard")
        {
            spAv=parseFloat(spAv)+parseFloat(searchIn.getValue(columns[0]));
            spNum++;
        }

    }
    if(spAv>0)
    {
        countSpaceArr.push(parseFloat(parseFloat(spAv)/parseFloat(spNum)).toFixed(1));
    }
    else
    {
        countSpaceArr.push(0);
    }

    var arrayReturn=[countSpaceArr[0],spNum];
    return arrayReturn;
}
function getAvgSeries(){
    var series='';
    var returnFunctionDatesGrid=return_weeksforGrid();
    //All Markets
    var location=[];
    var to12=addDays(returnFunctionDatesGrid[0],6);
    var from12=addDays(returnFunctionDatesGrid[0],-84);
    //week11
    var from11=addDays(returnFunctionDatesGrid[1],-84);
    var to11=addDays(returnFunctionDatesGrid[1],6);
    //week10
    var from10=addDays(returnFunctionDatesGrid[2],-84);
    var to10=addDays(returnFunctionDatesGrid[2],6);
    //week9
    var from9=addDays(returnFunctionDatesGrid[3],-84);
    var to9=addDays(returnFunctionDatesGrid[3],6);
    //week8
    var from8=addDays(returnFunctionDatesGrid[4],-84);
    var to8=addDays(returnFunctionDatesGrid[4],6);
    //week7
    var from7=addDays(returnFunctionDatesGrid[5],-84);
    var to7=addDays(returnFunctionDatesGrid[5],6);
    //week6
    var from6=addDays(returnFunctionDatesGrid[6],-84);
    var to6=addDays(returnFunctionDatesGrid[6],6);
    //week5
    var from5=addDays(returnFunctionDatesGrid[7],-84);
    var to5=addDays(returnFunctionDatesGrid[7],6);
    //week4
    var from4=addDays(returnFunctionDatesGrid[8],-84);
    var to4=addDays(returnFunctionDatesGrid[8],6);

    //week3
    var from3=addDays(returnFunctionDatesGrid[9],-84);
    var to3=addDays(returnFunctionDatesGrid[9],6);

    //week2
    var from2=addDays(returnFunctionDatesGrid[10],-84);
    var to2=addDays(returnFunctionDatesGrid[10],6);
    //week1
    var from1=addDays(returnFunctionDatesGrid[11],-84);
    var to1=addDays(returnFunctionDatesGrid[11],6);


    var values12Week =getAvgSeriesPerWeek(from12,to12,location);
    var values12WeekN =getAvgSeriesPerWeekNST_(from12,to12,location);
    //week11

    var values11Week =getAvgSeriesPerWeek(from11,to11,location);
    var values11WeekN =getAvgSeriesPerWeekNST_(from11,to11,location);
    //week10

    var values10Week =getAvgSeriesPerWeek(from10,to10,location);
    var values10WeekN =getAvgSeriesPerWeekNST_(from10,to10,location);
    //week9

    var values9Week =getAvgSeriesPerWeek(from9,to9,location);
    var values9WeekN =getAvgSeriesPerWeekNST_(from9,to9,location);
    //week8

    var values8Week =getAvgSeriesPerWeek(from8,to8,location);
    var values8WeekN =getAvgSeriesPerWeekNST_(from8,to8,location);
    //week7

    var values7Week =getAvgSeriesPerWeek(from7,to7,location);
    var values7WeekN =getAvgSeriesPerWeekNST_(from7,to7,location);
    //week6

    var values6Week =getAvgSeriesPerWeek(from6,to6,location);
    var values6WeekN =getAvgSeriesPerWeekNST_(from6,to6,location);
    //week5

    var values5Week =getAvgSeriesPerWeek(from5,to5,location);
    var values5WeekN =getAvgSeriesPerWeekNST_(from5,to5,location);
    //week4

    var values4Week =getAvgSeriesPerWeek(from4,to4,location);
    var values4WeekN =getAvgSeriesPerWeekNST_(from4,to4,location);
    //week3

    var values3Week =getAvgSeriesPerWeek(from3,to3,location);
    var values3WeekN =getAvgSeriesPerWeekNST_(from3,to3,location);
    //week2
    var values2Week =getAvgSeriesPerWeek(from2,to2,location);
    var values2WeekN =getAvgSeriesPerWeekNST_(from2,to2,location);
    //week1

    var values1Week =getAvgSeriesPerWeek(from1,to1,location);
    var values1WeekN =getAvgSeriesPerWeekNST_(from1,to1,location);

    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#0000CC',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    var seriesVolSt=getVolumeSeriesStandard(location);
    var seriesReturn='{ "ALL": ["'+seriesVolSt+'","{name: \'Space\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location=&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location=&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location=&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location=&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location=&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location=&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location=&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location=&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location=&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location=&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location=&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Space&location=&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},{name: \'Virtual XC\',type: \'spline\',data: [{y:'+values12Week[1]+',z:'+values12Week[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location=&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[1]+',z:'+values11Week[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location=&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[1]+',z:'+values10Week[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location=&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[1]+',z:'+values9Week[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location=&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[1]+',z:'+values8Week[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location=&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[1]+',z:'+values7Week[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location=&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[1]+',z:'+values6Week[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location=&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[1]+',z:'+values5Week[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location=&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[1]+',z:'+values4Week[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location=&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[1]+',z:'+values3Week[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location=&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[1]+',z:'+values2Week[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location=&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[1]+',z:'+values1Week[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Virtual XC&location=&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},{name: \'XC\',type: \'spline\',data: [{y:'+values12Week[2]+',z:'+values12Week[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location=&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[2]+',z:'+values11Week[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location=&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[2]+',z:'+values10Week[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location=&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[2]+',z:'+values9Week[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location=&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[2]+',z:'+values8Week[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location=&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[2]+',z:'+values7Week[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location=&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[2]+',z:'+values6Week[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location=&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[2]+',z:'+values5Week[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location=&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[2]+',z:'+values4Week[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location=&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[2]+',z:'+values3Week[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location=&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[2]+',z:'+values2Week[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location=&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[2]+',z:'+values1Week[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=XC&location=&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},{name: \'Network\',type:  \'spline\',data: [{y:'+values12Week[3]+',z:'+values12Week[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location=&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[3]+',z:'+values11Week[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location=&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[3]+',z:'+values10Week[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location=&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[3]+',z:'+values9Week[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location=&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[3]+',z:'+values8Week[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location=&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[3]+',z:'+values7Week[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location=&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[3]+',z:'+values6Week[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location=&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[3]+',z:'+values5Week[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location=&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[3]+',z:'+values4Week[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location=&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[3]+',z:'+values3Week[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location=&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[3]+',z:'+values2Week[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location=&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[3]+',z:'+values1Week[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Network&location=&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},{name:  \'Power\',type:  \'spline\',data: [{y:'+values12Week[4]+',z:'+values12Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[4]+',z:'+values11Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[4]+',z:'+values10Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[4]+',z:'+values9Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[4]+',z:'+values8Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[4]+',z:'+values7Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[4]+',z:'+values6Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[4]+',z:'+values5Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[4]+',z:'+values4Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[4]+',z:'+values3Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[4]+',z:'+values2Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[4]+',z:'+values1Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=2&title=Power&location=&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+'"] },';
    //Columbus Market

    var location=[34,39];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "COL": ["'+seriesVolSt+'"] },';


    //Col1&2 Facility
    //week12
    var location=[34];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "COL12": ["'+seriesVolSt+'"] },';
    //Col3 Facility
    //week12
    var location=[39];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "COL3": ["'+seriesVolSt+'"] },';

    //Dallas Market
    //week12
    var location=[2,17];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "Dallas": ["'+seriesVolSt+'"] },';

    //DAL1 Facility
    //week12
    var location=[2];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "DAL1": ["'+seriesVolSt+'"] },';

    //DAL2 Facility
    //week12
    var location=[17];

    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "DAL2": ["'+seriesVolSt+'"] },';

    //Jacksonville Market
    //week12
    var location=[31];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "Jacksonville": ["'+seriesVolSt+'"] },';
    //JAX1 Facility
    //week12
    var location=[31];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "JAX1": ["'+seriesVolSt+'"] },';
    //Jacksonville2 Market
    //week12
    var location=[40];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "Jacksonville2": ["'+seriesVolSt+'"] },';
    //JAX2 Facility
    //week12
    var location=[40];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "JAX2": ["'+seriesVolSt+'"] },';
    //Lakeland Market
    //week12
    var location=[42];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "Lakeland": ["'+seriesVolSt+'"] },';
    //LAK1 Facility
    //week12
    var location=[42];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "LAK1": ["'+seriesVolSt+'"] },';
    //Minneapolis Market
    //week12
    var location=[16,35];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "Minneapolis": ["'+seriesVolSt+'"] },';
    //MIN1&2 Facility
    //week12
    var location=[16];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "MIN12": ["'+seriesVolSt+'"] },';
    var location=[35];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "MIN3": ["'+seriesVolSt+'"] },';

    //Montreal Market
    //week12
    var location=[5,8,9,10,11,12,27];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "Montreal": ["'+seriesVolSt+'"] },';

    //MTL1
    //week12
    var location=[5];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "MTL1": ["'+seriesVolSt+'"] },';

    //MTL2
    //week12
    var location=[8];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "MTL2": ["'+seriesVolSt+'"] },';

    //MTL3
    //week12
    var location=[9];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "MTL3": ["'+seriesVolSt+'"] },';
    //MTL4
    //week12
    var location=[10];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "MTL4": ["'+seriesVolSt+'"] },';

    //MTL5
    //week12
    var location=[11];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "MTL5": ["'+seriesVolSt+'"] },';

    //MTL6
    //week12
    var location=[12];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "MTL6": ["'+seriesVolSt+'"] },';

    //MTL7
    //week12
    var location=[27];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "MTL7": ["'+seriesVolSt+'"] },';

    //New Jersey Market
    //week12
    var location=[53,54,55,56];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "NewJersey": ["'+seriesVolSt+'"] },';
    //NNJ1
    //week12
    var location=[53];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "NNJ1": ["'+seriesVolSt+'"] },';
    //NNJ2
    //week12
    var location=[54];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "NNJ2": ["'+seriesVolSt+'"] },';
    //NNJ3
    //week12
    var location=[55];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "NNJ3": ["'+seriesVolSt+'"] },';
    //NNJ4
    //week12
    var location=[56];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "NNJ4": ["'+seriesVolSt+'"] },';
    //Toronto Market
    //week12
    var location=[6,15];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "Toronto": ["'+seriesVolSt+'"] },';

    //TOR1
    //week12
    var location=[6];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "TOR1": ["'+seriesVolSt+'"] },';

    //TOR2
    //week12
    var location=[15];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "TOR2": ["'+seriesVolSt+'"] },';

    //Vancouver
    //week12
    var location=[28,7];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "Vancouver": ["'+seriesVolSt+'"] },';
    //VAN1
    //week12
    var location=[7];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "VAN1": ["'+seriesVolSt+'"] },';
    //VAN1
    //week12
    var location=[28];
    var seriesVolSt=getVolumeSeriesStandard(location);
    seriesReturn +='{ "VAN2": ["'+seriesVolSt+'"] }';
    return seriesReturn;
}

function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}