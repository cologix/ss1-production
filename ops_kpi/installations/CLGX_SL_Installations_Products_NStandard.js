nlapiLogExecution("audit","FLOStart",new Date().getTime());
function suitelet_ops_kpi_inst_loc (request, response){
    try {

        var script = request.getParameter('script');
        var type = request.getParameter('type');
        var category = request.getParameter('category');
        var tarw=0;
        var tarv=0;

        var chartype = 'column';
        if(type == 'avg'){
            chartype = 'spline';
            tarw=1;
        }
        var title = request.getParameter('title');
        var location = request.getParameter('location');
        var cattype=request.getParameter('cattype');
        var product = request.getParameter('product');
        var order = request.getParameter('order');
        var titleav="Standard";
        if(cattype=='standard')
        {
            titleav="Non-Standard";
        }
        if(cattype=='standard')
        {
            if(product=="space"||product=="power"||product=="network")
            {
                tarv=5;
            }
            if(product=="xc")
            {
                tarv=2;
            }
        }

        if(type=='avg')
        {
            var objFile = nlapiLoadFile(3042679);
        }
        else
        {
            var objFile = nlapiLoadFile(2743411);
        }
        var html = objFile.getValue();
        html = html.replace(new RegExp('{weeks}','g'), return_weeks());
        html = html.replace(new RegExp('{chartype}','g'), chartype);
        html = html.replace(new RegExp('{averagetitle}','g'), titleav);
        html = html.replace(new RegExp('{targetwidth}','g'), tarw);
        html = html.replace(new RegExp('{targetvalue}','g'), tarv);
        html = html.replace(new RegExp('{titleav}','g'), title);
        html = html.replace(new RegExp('{title}','g'), 'Installation Intervals - ' + title);
        var cat=[10,11,4,5,8];

        if(type=='avg')
        {
            var seriesVolSt=getAvgAllSeries(product);
        }
        else
        {
            var seriesVolSt=getVolumeAllSeries(product);
        }

        html = html.replace(new RegExp('{series}','g'), seriesVolSt);

        response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function getVolumeAllSeries (product){

    var arrFilters = new Array();
    var arrColumns = new Array();
    var returnFunctionDates=return_weeksDates();
    var returnFunctionDatesGrid=return_weeksforGrid();
    var to=addDays(returnFunctionDates[1], 6);
    var dd = to.getDate();
    var mm = to.getMonth() + 1;
    var y = to.getFullYear();
    var toString = mm+ '/'+ dd + '/'+ y;

    var from12=returnFunctionDatesGrid[0];
    var to12=addDays(from12,6);
    var dd12 = to12.getDate();
    var mm12 = to12.getMonth() + 1;
    var y12 = to12.getFullYear();
    var to12 = mm12+ '/'+ dd12 + '/'+ y12;
    //week11
    var from11=returnFunctionDatesGrid[1];
    var to11=addDays(from11,6);
    var dd11 = to11.getDate();
    var mm11 = to11.getMonth() + 1;
    var y11 = to11.getFullYear();
    var to11 = mm11+ '/'+ dd11 + '/'+ y11;
    //week10
    var from10=returnFunctionDatesGrid[2];
    var to10=addDays(from10,6);
    var dd10 = to10.getDate();
    var mm10 = to10.getMonth() + 1;
    var y10 = to10.getFullYear();
    var to10 = mm10+ '/'+ dd10 + '/'+ y10;
    //week9
    var from9=returnFunctionDatesGrid[3];
    var to9=addDays(from9,6);
    var dd9 = to9.getDate();
    var mm9 = to9.getMonth() + 1;
    var y9 = to9.getFullYear();
    var to9 = mm9+ '/'+ dd9 + '/'+ y9;
    //week8
    var from8=returnFunctionDatesGrid[4];
    var to8=addDays(from8,6);
    var dd8 = to8.getDate();
    var mm8 = to8.getMonth() + 1;
    var y8 = to8.getFullYear();
    var to8 = mm8+ '/'+ dd8 + '/'+ y8;
    //week7
    var from7=returnFunctionDatesGrid[5];
    var to7=addDays(from7,6);
    var dd7 = to7.getDate();
    var mm7 = to7.getMonth() + 1;
    var y7 = to7.getFullYear();
    var to7 = mm7+ '/'+ dd7 + '/'+ y7;
    //week6
    var from6=returnFunctionDatesGrid[6];
    var to6=addDays(from6,6);
    var dd6 = to6.getDate();
    var mm6 = to6.getMonth() + 1;
    var y6 = to6.getFullYear();
    var to6 = mm6+ '/'+ dd6 + '/'+ y6;
    //week5
    var from5=returnFunctionDatesGrid[7];
    var to5=addDays(from5,6);
    var dd5 = to5.getDate();
    var mm5 = to5.getMonth() + 1;
    var y5 = to5.getFullYear();
    var to5 = mm5+ '/'+ dd5 + '/'+ y5;
    //week4
    var from4=returnFunctionDatesGrid[8];
    var to4=addDays(from4,6);
    var dd4 = to4.getDate();
    var mm4 = to4.getMonth() + 1;
    var y4 = to4.getFullYear();
    var to4 = mm4+ '/'+ dd4 + '/'+ y4;
    //week3
    var from3=returnFunctionDatesGrid[9];
    var to3=addDays(from3,6);
    var dd3 = to3.getDate();
    var mm3 = to3.getMonth() + 1;
    var y3 = to3.getFullYear();
    var to3 = mm3+ '/'+ dd3 + '/'+ y3;
    //week2
    var from2=returnFunctionDatesGrid[10];
    var to2=addDays(from2,6);
    var dd2 = to2.getDate();
    var mm2 = to2.getMonth() + 1;
    var y2 = to2.getFullYear();
    var to2 = mm2+ '/'+ dd2 + '/'+ y2;
    //week1
    var from1=returnFunctionDatesGrid[11];
    var to1=addDays(from1,6);
    var dd1 = to1.getDate();
    var mm1 = to1.getMonth() + 1;
    var y1 = to1.getFullYear();
    var to1 = mm1+ '/'+ dd1 + '/'+ y1;
    //ALL
    var cat=[10,11,4,5,8];
    if(product=='space')
    {
        var cat=[10];
        product=10;
    }
    else if(product=='vxc')
    {
        //Virtual XC
        var cat=[11];
        product=11;
    }
    else if(product=='xc')
    {
        // XC
        var cat=[4];
        product=4;
    }
    else if(product=='network')
    {
        // net
        var cat=[5];
        product=5;
    }
    else if(product=='power')
    {
        // power
        var cat=[8];
        product=8;
    }

    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",returnFunctionDates[0],toString));
    arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category","item","anyof",cat));
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_install_volns', arrFilters, arrColumns);
    var volCol=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volDal=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volJac=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volJac2=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volLak=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volMin=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volMTL=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volNNJ=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volTOR=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volVAN=[0,0,0,0,0,0,0,0,0,0,0,0];

    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var location = searchIn.getText(columns[0]);

        var to=searchIn.getValue(columns[2]);

        if(location=='Columbus')
        {
            if(to==to12)
            {
                volCol[11] +=1;
            }
            else if(to==to11){
                volCol[10]+=1;
            }
            else if(to==to10){
                volCol[9]+=1;
            }
            else if(to==to9){
                volCol[8]+=1;
            }
            else if(to==to8){
                volCol[7]+=1;
            }
            else if(to==to7){
                volCol[6]+=1;
            }
            else if(to==to6){
                volCol[5]+=1;
            }
            else if(to==to5){
                volCol[4]+=1;
            }
            else if(to==to4){
                volCol[3]+=1;
            }
            else if(to==to3){
                volCol[2]+=1;
            }
            else if(to==to2){
                volCol[1]+=1;
            }
            else if(to==to1){
                volCol[0]+=1;
            }
        }
        else if(location=='Dallas' )
        {
            if(to==to12)
            {
                volDal[11]+=1;
            }
            else if(to==to11){
                volDal[10]+=1;
            }
            else if(to==to10){
                volDal[9]+=1;
            }
            else if(to==to9){
                volDal[8]+=1;
            }
            else if(to==to8){
                volDal[7]+=1;
            }
            else if(to==to7){
                volDal[6]+=1;
            }
            else if(to==to6){
                volDal[5]+=1;
            }
            else if(to==to5){
                volDal[4]+=1;
            }
            else if(to==to4){
                volDal[3]+=1;
            }
            else if(to==to3){
                volDal[2]+=1;
            }
            else if(to==to2){
                volDal[1]+=1;
            }
            else if(to==to1){
                volDal[0]+=1;
            }

        }
        else  if(location=='Jacksonville' )
        {

            if(to==to12)
            {
                volJac[11]+=1;
            }
            else if(to==to11){
                volJac[10]+=1;
            }
            else if(to==to10){
                volJac[9]+=1;
            }
            else if(to==to9){
                volJac[8]+=1;
            }
            else if(to==to8){
                volJac[7]+=1;
            }
            else if(to==to7){
                volJac[6]+=1;
            }
            else if(to==to6){
                volJac[5]+=1;
            }
            else if(to==to5){
                volJac[4]+=1;
            }
            else if(to==to4){
                volJac[3]+=1;
            }
            else if(to==to3){
                volJac[2]+=1;
            }
            else if(to==to2){
                volJac[1]+=1;
            }
            else if(to==to1){
                volJac[0]+=1;
            }
        }
        else if(location=='Jacksonville2')
        {

            if(to==to12)
            {
                volJac2[11]+=1;
            }
            else if(to==to11){
                volJac2[10]+=1;
            }
            else if(to==to10){
                volJac2[9]+=1;
            }
            else if(to==to9){
                volJac2[8]+=1;
            }
            else if(to==to8){
                volJac2[7]+=1;
            }
            else if(to==to7){
                volJac2[6]+=1;
            }
            else if(to==to6){
                volJac2[5]+=1;
            }
            else if(to==to5){
                volJac2[4]+=1;
            }
            else if(to==to4){
                volJac2[3]+=1;
            }
            else if(to==to3){
                volJac2[2]+=1;
            }
            else if(to==to2){
                volJac2[1]+=1;
            }
            else if(to==to1){
                volJac2[0]+=1;
            }
        }
        else if(location=='Lakeland')
        {

            if(to==to12)
            {
                volLak[11]+=1;
            }
            else if(to==to11){
                volLak[10]+=1;
            }
            else if(to==to10){
                volLak[9]+=1;
            }
            else if(to==to9){
                volLak[8]+=1;
            }
            else if(to==to8){
                volLak[7]+=1;
            }
            else if(to==to7){
                volLak[6]+=1;
            }
            else if(to==to6){
                volLak[5]+=1;
            }
            else if(to==to5){
                volLak[4]+=1;
            }
            else if(to==to4){
                volLak[3]+=1;
            }
            else if(to==to3){
                volLak[2]+=1;
            }
            else if(to==to2){
                volLak[1]+=1;
            }
            else if(to==to1){
                volLak[0]+=1;
            }
        }
        else if(location=='Minneapolis ')
        {

            if(to==to12)
            {
                volMin[11]+=1;
            }
            else if(to==to11){
                volMin[10]+=1;
            }
            else if(to==to10){
                volMin[9]+=1;
            }
            else if(to==to9){
                volMin[8]+=1;
            }
            else if(to==to8){
                volMin[7]+=1;
            }
            else if(to==to7){
                volMin[6]+=1;
            }
            else if(to==to6){
                volMin[5]+=1;
            }
            else if(to==to5){
                volMin[4]+=1;
            }
            else if(to==to4){
                volMin[3]+=1;
            }
            else if(to==to3){
                volMin[2]+=1;
            }
            else if(to==to2){
                volMin[1]+=1;
            }
            else if(to==to1){
                volMin[0]+=1;
            }
        }
        else if(location=='Montreal' )
        {

            if(to==to12)
            {
                volMTL[11]+=1;
            }
            else if(to==to11){
                volMTL[10]+=1;
            }
            else if(to==to10){
                volMTL[9]+=1;
            }
            else if(to==to9){
                volMTL[8]+=1;
            }
            else if(to==to8){
                volMTL[7]+=1;
            }
            else if(to==to7){
                volMTL[6]+=1;
            }
            else if(to==to6){
                volMTL[5]+=1;
            }
            else if(to==to5){
                volMTL[4]+=1;
            }
            else if(to==to4){
                volMTL[3]+=1;
            }
            else if(to==to3){
                volMTL[2]+=1;
            }
            else if(to==to2){
                volMTL[1]+=1;
            }
            else if(to==to1){
                volMTL[0]+=1;
            }
        }
        else if(location=='New Jersey')
        {

            if(to==to12)
            {
                volNNJ[11]+=1;
            }
            else if(to==to11){
                volNNJ[10]+=1;
            }
            else if(to==to10){
                volNNJ[9]+=1;
            }
            else if(to==to9){
                volNNJ[8]+=1;
            }
            else if(to==to8){
                volNNJ[7]+=1;
            }
            else if(to==to7){
                volNNJ[6]+=1;
            }
            else if(to==to6){
                volNNJ[5]+=1;
            }
            else if(to==to5){
                volNNJ[4]+=1;
            }
            else if(to==to4){
                volNNJ[3]+=1;
            }
            else if(to==to3){
                volNNJ[2]+=1;
            }
            else if(to==to2){
                volNNJ[1]+=1;
            }
            else if(to==to1){
                volNNJ[0]+=1;
            }
        }
        else  if(location=='Toronto')
        {

            if(to==to12)
            {
                volTOR[11]+=1;
            }
            else if(to==to11){
                volTOR[10]+=1;
            }
            else if(to==to10){
                volTOR[9]+=1;
            }
            else if(to==to9){
                volTOR[8]+=1;
            }
            else if(to==to8){
                volTOR[7]+=1;
            }
            else if(to==to7){
                volTOR[6]+=1;
            }
            else if(to==to6){
                volTOR[5]+=1;
            }
            else if(to==to5){
                volTOR[4]+=1;
            }
            else if(to==to4){
                volTOR[3]+=1;
            }
            else if(to==to3){
                volTOR[2]+=1;
            }
            else if(to==to2){
                volTOR[1]+=1;
            }
            else if(to==to1){
                volTOR[0]+=1;
            }
        }
        else if(location=='Vancouver')
        {
            if(to==to12)
            {
                volVAN[11]+=1;
            }
            else if(to==to11){
                volVAN[10]+=1;
            }
            else if(to==to10){
                volVAN[9]+=1;
            }
            else if(to==to9){
                volVAN[8]+=1;
            }
            else if(to==to8){
                volVAN[7]+=1;
            }
            else if(to==to7){
                volVAN[6]+=1;
            }
            else if(to==to6){
                volVAN[5]+=1;
            }
            else if(to==to5){
                volVAN[4]+=1;
            }
            else if(to==to4){
                volVAN[3]+=1;
            }
            else if(to==to3){
                volVAN[2]+=1;
            }
            else if(to==to2){
                volVAN[1]+=1;
            }
            else if(to==to1){
                volVAN[0]+=1;
            }
        }
    }
    var series='';

    series +=  "{name: 'Columbus',type: 'column',data: [" +
        "{y:"+volCol[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'}," +
        "{y:"+volCol[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
        +"{y:"+volCol[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
        +"{y:"+volCol[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
        +"{y:"+volCol[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
        +"{y:"+volCol[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+product+"&cattype=2&week=="+returnFunctionDatesGrid[5]+"'}"+','
        +"{y:"+volCol[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
        +"{y:"+volCol[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
        +"{y:"+volCol[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
        +"{y:"+volCol[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
        +"{y:"+volCol[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
        +"{y:"+volCol[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
        +"]},";



    series +=  "{name: 'Dallas',type: 'column',data: [" +
        "{y:"+volDal[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'}," +
        "{y:"+volDal[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
        +"{y:"+volDal[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
        +"{y:"+volDal[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
        +"{y:"+volDal[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
        +"{y:"+volDal[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
        +"{y:"+volDal[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
        +"{y:"+volDal[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
        +"{y:"+volDal[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
        +"{y:"+volDal[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
        +"{y:"+volDal[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
        +"{y:"+volDal[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
        +"]},";

    series +=  "{name: 'Jacksonville',type: 'column',data: [" +
        "{y:"+volJac[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'}," +
        "{y:"+volJac[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
        +"{y:"+volJac[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
        +"{y:"+volJac[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
        +"{y:"+volJac[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
        +"{y:"+volJac[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
        +"{y:"+volJac[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
        +"{y:"+volJac[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
        +"{y:"+volJac[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
        +"{y:"+volJac[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
        +"{y:"+volJac[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
        +"{y:"+volJac[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
        +"]},";

    series +=  "{name: 'Jacksonville2',type: 'column',data: [" +
        "{y:"+volJac2[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'}," +
        "{y:"+volJac2[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
        +"{y:"+volJac2[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
        +"{y:"+volJac2[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
        +"{y:"+volJac2[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
        +"{y:"+volJac2[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
        +"{y:"+volJac2[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
        +"{y:"+volJac2[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
        +"{y:"+volJac2[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
        +"{y:"+volJac2[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
        +"{y:"+volJac2[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
        +"{y:"+volJac2[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
        +"]},";

    series +=  "{name: 'Lakeland',type: 'column',data: [" +
        "{y:"+volLak[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'}," +
        "{y:"+volLak[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
        +"{y:"+volLak[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
        +"{y:"+volLak[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
        +"{y:"+volLak[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
        +"{y:"+volLak[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
        +"{y:"+volLak[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
        +"{y:"+volLak[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
        +"{y:"+volLak[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
        +"{y:"+volLak[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
        +"{y:"+volLak[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
        +"{y:"+volLak[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
        +"]},";

    series +=  "{name: 'Minneapolis',type: 'column',data: [" +
        "{y:"+volMin[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'}," +
        "{y:"+volMin[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
        +"{y:"+volMin[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
        +"{y:"+volMin[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
        +"{y:"+volMin[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
        +"{y:"+volMin[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
        +"{y:"+volMin[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
        +"{y:"+volMin[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
        +"{y:"+volMin[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
        +"{y:"+volMin[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
        +"{y:"+volMin[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
        +"{y:"+volMin[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
        +"]},";

    series +=  "{name: 'Montreal',type: 'column',data: [" +
        "{y:"+volMTL[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'}," +
        "{y:"+volMTL[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
        +"{y:"+volMTL[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
        +"{y:"+volMTL[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
        +"{y:"+volMTL[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
        +"{y:"+volMTL[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
        +"{y:"+volMTL[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
        +"{y:"+volMTL[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
        +"{y:"+volMTL[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
        +"{y:"+volMTL[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
        +"{y:"+volMTL[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
        +"{y:"+volMTL[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
        +"]},";

    series +=  "{name: 'New Jersey',type: 'column',data: [" +
        "{y:"+volNNJ[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=New Jersey&location=52&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'}," +
        "{y:"+volNNJ[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=New Jersey&location=52&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
        +"{y:"+volNNJ[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=New Jersey&location=52&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
        +"{y:"+volNNJ[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=New Jersey&location=52&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
        +"{y:"+volNNJ[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=New Jersey&location=52&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
        +"{y:"+volNNJ[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=New Jersey&location=52&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
        +"{y:"+volNNJ[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=New Jersey&location=52&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
        +"{y:"+volNNJ[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=New Jersey&location=52&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
        +"{y:"+volNNJ[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=New Jersey&location=52&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
        +"{y:"+volNNJ[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=New Jersey&location=52&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
        +"{y:"+volNNJ[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=New Jersey&location=52&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
        +"{y:"+volNNJ[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=New Jersey&location=52&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
        +"]},";

    series +=  "{name: 'Toronto',type: 'column',data: [" +
        "{y:"+volTOR[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'}," +
        "{y:"+volTOR[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
        +"{y:"+volTOR[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
        +"{y:"+volTOR[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
        +"{y:"+volTOR[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
        +"{y:"+volTOR[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
        +"{y:"+volTOR[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
        +"{y:"+volTOR[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
        +"{y:"+volTOR[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
        +"{y:"+volTOR[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
        +"{y:"+volTOR[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
        +"{y:"+volTOR[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
        +"]},";

    series +=  "{name: 'Vancouver',type: 'column',data: [" +
        "{y:"+volVAN[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'},{y:"+volVAN[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
        +"{y:"+volVAN[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
        +"{y:"+volVAN[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
        +"{y:"+volVAN[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
        +"{y:"+volVAN[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
        +"{y:"+volVAN[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
        +"{y:"+volVAN[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
        +"{y:"+volVAN[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
        +"{y:"+volVAN[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
        +"{y:"+volVAN[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
        +"{y:"+volVAN[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
        +"]},";

    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_install_vol_s', arrFilters, arrColumns);

    var volNS=[0,0,0,0,0,0,0,0,0,0,0,0];

    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var vol=searchIn.getValue(columns[0]);
        var to=searchIn.getValue(columns[1]);
        var standard=searchIn.getValue(columns[2]);

        if(standard==0) {
            if (to == to12) {
                volNS[11] +=1;
            }
            else if (to == to11) {
                volNS[10] +=1;
            }
            else if (to == to10) {
                volNS[9] +=1;
            }
            else if (to == to9) {
                volNS[8] +=1;
            }
            else if (to == to8) {
                volNS[7] +=1;
            }
            else if (to == to7) {
                volNS[6] +=1;
            }
            else if (to == to6) {
                volNS[5] +=1;
            }
            else if (to == to5) {
                volNS[4] +=1;
            }
            else if (to == to4) {
                volNS[3] +=1;
            }
            else if (to == to3) {
                volNS[2] +=1;
            }
            else if (to == to2) {
                volNS[1] +=1;
            }
            else if (to == to1) {
                volNS[0] +=1;
            }
        }


    }
    series +=  " {name: 'Standard',type: 'spline','visible': false, color: '#0000CC',data: [" +
        "{y:"+volNS[11]+",url:'#'}," +
        "{y:"+volNS[10]+",url:'#'}," +
        "{y:"+volNS[9]+",url:'#'}," +
        "{y:"+volNS[8]+",url:'#'}," +
        "{y:"+volNS[7]+",url:'#'}," +
        "{y:"+volNS[6]+",url:'#'}," +
        "{y:"+volNS[5]+",url:'#'}," +
        "{y:"+volNS[4]+",url:'#'}," +
        "{y:"+volNS[3]+",url:'#'}," +
        "{y:"+volNS[2]+",url:'#'}," +
        "{y:"+volNS[1]+",url:'#'}," +
        "{y:"+volNS[0]+",url:'#'}]}";

    return series;
}

function return_weeks (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 13; i++ ) {
        arrWeeks.push(moment().endOf('week').add(1, 'days').subtract((13 - i), 'week').format('M/D'));
    }
    return JSON.stringify(arrWeeks);
}
function return_weeksDates (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 13; i++ ) {
        arrWeeks.push(moment().startOf('week').add(1, 'days').subtract((13 - i), 'week').format('MM/DD/YYYY'));
    }
    var arrayDates=[arrWeeks[0],arrWeeks[arrWeeks.length-1]]
    return arrayDates;
}
function return_weeksforGrid (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 13; i++ ) {
        arrWeeks.push(moment().startOf('week').add(1, 'days').subtract((13 - i), 'week').format('MM/DD/YYYY'));
    }
    return arrWeeks;
}
function addDays(theDate, days) {
    theDate=new Date(theDate);
    return new Date(theDate.getTime() + days*24*60*60*1000);
}
function returnDateStringDT(date){
    var to=addDays(date, 6);
    var dd = to.getDate();
    var mm = to.getMonth() + 1;
    var y = to.getFullYear();
    var toString = mm+ '/'+ dd + '/'+ y;
    return toString;
}
function returnDateString(location,from){
    var toString =returnDateStringDT(from);
    var arrFilters=new Array();
    if(location!=0)
    {
        var arrLocation=clgx_return_child_locations_of_marketid(location);
        if(arrLocation.length==0)
        {
            arrLocation.push(location);
        }
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",arrLocation));
    }
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from,toString));
    return arrFilters;
}
function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}
function getAvgAllSeries(product){

    var arrFilters = new Array();
    var arrColumns = new Array();
    var returnFunctionDates=return_weeksDates();
    var returnFunctionDatesGrid=return_weeksforGrid();
    var to=addDays(returnFunctionDates[1], 6);
    var dd = to.getDate();
    var mm = to.getMonth() + 1;
    var y = to.getFullYear();
    var toString = mm+ '/'+ dd + '/'+ y;

    var to12=addDays(returnFunctionDatesGrid[0],6);
    var from12=addDays(returnFunctionDatesGrid[0],-84);
    var dd12 = to12.getDate();
    var mm12 = to12.getMonth() + 1;
    var y12 = to12.getFullYear();
    var toString12 = mm12+ '/'+ dd12 + '/'+ y12;

    //week11
    var from11=addDays(returnFunctionDatesGrid[1],-84);
    var to11=addDays(returnFunctionDatesGrid[1],6);
    var dd11 = to11.getDate();
    var mm11 = to11.getMonth() + 1;
    var y11 = to11.getFullYear();
    var toString11 = mm11+ '/'+ dd11 + '/'+ y11;
    //week10
    var from10=addDays(returnFunctionDatesGrid[2],-84);
    var to10=addDays(returnFunctionDatesGrid[2],6);
    var dd10 = to10.getDate();
    var mm10 = to10.getMonth() + 1;
    var y10 = to10.getFullYear();
    var toString10 = mm10+ '/'+ dd10 + '/'+ y10;
    //week9
    var from9=addDays(returnFunctionDatesGrid[3],-84);
    var to9=addDays(returnFunctionDatesGrid[3],6);
    var dd9 = to9.getDate();
    var mm9 = to9.getMonth() + 1;
    var y9= to9.getFullYear();
    var toString9 = mm9+ '/'+ dd9 + '/'+ y9;
    //week8
    var from8=addDays(returnFunctionDatesGrid[4],-84);
    var to8=addDays(returnFunctionDatesGrid[4],6);
    var dd8 = to8.getDate();
    var mm8 = to8.getMonth() + 1;
    var y8 = to8.getFullYear();
    var toString8 = mm8+ '/'+ dd8 + '/'+ y8;
    //week7
    var from7=addDays(returnFunctionDatesGrid[5],-84);
    var to7=addDays(returnFunctionDatesGrid[5],6);
    var dd7 = to7.getDate();
    var mm7 = to7.getMonth() + 1;
    var y7 = to7.getFullYear();
    var toString7 = mm7+ '/'+ dd7 + '/'+ y7;
    //week6
    var from6=addDays(returnFunctionDatesGrid[6],-84);
    var to6=addDays(returnFunctionDatesGrid[6],6);
    var dd6 = to6.getDate();
    var mm6 = to6.getMonth() + 1;
    var y6 = to6.getFullYear();
    var toString6 = mm6+ '/'+ dd6 + '/'+ y6;
    //week5
    var from5=addDays(returnFunctionDatesGrid[7],-84);
    var to5=addDays(returnFunctionDatesGrid[7],6);
    var dd5 = to5.getDate();
    var mm5 = to5.getMonth() + 1;
    var y5 = to5.getFullYear();
    var toString5 = mm5+ '/'+ dd5 + '/'+ y5;
    //week4
    var from4=addDays(returnFunctionDatesGrid[8],-84);
    var to4=addDays(returnFunctionDatesGrid[8],6);
    var dd4 = to4.getDate();
    var mm4 = to4.getMonth() + 1;
    var y4 = to4.getFullYear();
    var toString4 = mm4+ '/'+ dd4 + '/'+ y4;

    //week3
    var from3=addDays(returnFunctionDatesGrid[9],-84);
    var to3=addDays(returnFunctionDatesGrid[9],6);
    var dd3 = to3.getDate();
    var mm3 = to3.getMonth() + 1;
    var y3 = to3.getFullYear();
    var toString3 = mm3+ '/'+ dd3 + '/'+ y3;


    //week2
    var from2=addDays(returnFunctionDatesGrid[10],-84);
    var to2=addDays(returnFunctionDatesGrid[10],6);
    var dd2 = to2.getDate();
    var mm2 = to2.getMonth() + 1;
    var y2 = to2.getFullYear();
    var toString2 = mm2+ '/'+ dd2 + '/'+ y2;
    //week1
    var from1=addDays(returnFunctionDatesGrid[11],-84);
    var to1=addDays(returnFunctionDatesGrid[11],6);
    var dd1 = to1.getDate();
    var mm1 = to1.getMonth() + 1;
    var y1 = to1.getFullYear();
    var toString1 = mm1+ '/'+ dd1 + '/'+ y1;
    //ALL
    var cat=[10,11,4,5,8];
    if(product=='space')
    {
        var cat=[10];
        product=10;
    }
    else if(product=='vxc')
    {
        //Virtual XC
        var cat=[11];
        product=11;
    }
    else if(product=='xc')
    {
        // XC
        var cat=[4];
        product=4;
    }
    else if(product=='network')
    {
        // net
        var cat=[5];
        product=5;
    }
    else if(product=='power')
    {
        // power
        var cat=[8];
        product=8;
    }
    var sosNS=new Array();
    var searchInsIDS = nlapiSearchRecord('transaction', 'customsearch_clgx_so_install_vol_loc_s_8', arrFilters, arrColumns);
    for ( var i = 0; searchInsIDS != null && i < searchInsIDS.length; i++ ) {
        var searchIn = searchInsIDS[i];
        var columns = searchIn.getAllColumns();
        var id = searchIn.getValue(columns[0]);
        sosNS.push(id);
    }
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from12,to1));
    arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category","item","anyof",cat));
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_inst_nost', arrFilters, arrColumns);
    var avCol=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volCol=[0,0,0,0,0,0,0,0,0,0,0,0];
    var avDal=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volDal=[0,0,0,0,0,0,0,0,0,0,0,0];
    var avJac=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volJac=[0,0,0,0,0,0,0,0,0,0,0,0];
    var avJac2=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volJac2=[0,0,0,0,0,0,0,0,0,0,0,0];
    var avLak=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volLak=[0,0,0,0,0,0,0,0,0,0,0,0];
    var avMin=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volMin=[0,0,0,0,0,0,0,0,0,0,0,0];
    var avMTL=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volMTL=[0,0,0,0,0,0,0,0,0,0,0,0];
    var avNNJ=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volNNJ=[0,0,0,0,0,0,0,0,0,0,0,0];
    var avTOR=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volTOR=[0,0,0,0,0,0,0,0,0,0,0,0];
    var avVAN=[0,0,0,0,0,0,0,0,0,0,0,0];
    var volVAN=[0,0,0,0,0,0,0,0,0,0,0,0];

    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var location = searchIn.getText(columns[0]);
        var average=parseFloat(searchIn.getValue(columns[1])).toFixed(1);
        var date= new Date(searchIn.getValue(columns[2]));
        date=date.setHours(01);
        date=moment(date);
        if(location=='Columbus')
        {
            if((date >= moment(from12)) && (date <= moment(toString12)))
            {
                avCol[11]= parseInt(avCol[11])+parseInt(average);
                volCol[11]= volCol[11]+1;
            }
            if((date >= moment(from11)) && (date <= moment(toString11))){
                avCol[10]= parseInt(avCol[10])+parseInt(average);
                volCol[10]= volCol[10]+1;
            }
            if((date >= moment(from10)) && (date <= moment(toString10))){
                avCol[9]= parseInt(avCol[9])+parseInt(average);
                volCol[9]= volCol[9]+1;
            }
            if((date >= moment(from9)) && (date <= moment(toString9))){
                avCol[8]= parseInt(avCol[8])+parseInt(average);
                volCol[8]= volCol[8]+1;
            }
            if((date >= moment(from8)) && (date <= moment(toString8))){
                avCol[7]= parseInt(avCol[7])+parseInt(average);
                volCol[7]= volCol[7]+1;
            }
            if((date >= moment(from7)) && (date <= moment(toString7))){
                avCol[6]= parseInt(avCol[6])+parseInt(average);
                volCol[6]= volCol[6]+1;
            }
            if((date >= moment(from6)) && (date <= moment(toString6))){
                avCol[5]= parseInt(avCol[5])+parseInt(average);
                volCol[5]= volCol[5]+1;
            }
            if((date >= moment(from5)) && (date <= moment(toString5))){
                avCol[4]= parseInt(avCol[4])+parseInt(average);
                volCol[4]= volCol[4]+1;
            }
            if((date >= moment(from4)) && (date <= moment(toString4))){
                avCol[3]= parseInt(avCol[3])+parseInt(average);
                volCol[3]= volCol[3]+1;
            }
            if((date >= moment(from3)) && (date <= moment(toString3))){
                avCol[2]= parseInt(avCol[2])+parseInt(average);
                volCol[2]= volCol[2]+1;
            }
            if((date >= moment(from2)) && (date <= moment(toString2))){
                avCol[1]= parseInt(avCol[1])+parseInt(average);
                volCol[1]= volCol[1]+1;
            }
            if((date >= moment(from1)) && (date <= moment(toString1))){
                avCol[0]= parseInt(avCol[0])+parseInt(average);
                volCol[0]= volCol[0]+1;
            }
        }
        else if(location=='Dallas' )
        {
            if((date >= moment(from12)) && (date <= moment(toString12)))
            {
                avDal[11]= parseInt(avDal[11])+parseInt(average);
                volDal[11]= volDal[11]+1;
            }
            if((date >= moment(from11)) && (date <= moment(toString11))){
                avDal[10]= parseInt(avDal[10])+parseInt(average);
                volDal[10]= volDal[10]+1;
            }
            if((date >= moment(from10)) && (date <= moment(toString10))){
                avDal[9]= parseInt(avDal[9])+parseInt(average);
                volDal[9]= volDal[9]+1;
            }
            if((date >= moment(from9)) && (date <= moment(toString9))){
                avDal[8]= parseInt(avDal[8])+parseInt(average);
                volDal[8]= volDal[8]+1;
            }
            if((date >= moment(from8)) && (date <= moment(toString8))){
                avDal[7]= parseInt(avDal[7])+parseInt(average);
                volDal[7]= volDal[7]+1;
            }
            if((date >= moment(from7)) && (date <= moment(toString7))){
                avDal[6]= parseInt(avDal[6])+parseInt(average);
                volDal[6]= volDal[6]+1;
            }
            if((date >= moment(from6)) && (date <= moment(toString6))){
                avDal[5]= parseInt(avDal[5])+parseInt(average);
                volDal[5]= volDal[5]+1;
            }
            if((date >= moment(from5)) && (date <= moment(toString5))){
                avDal[4]= parseInt(avDal[4])+parseInt(average);
                volDal[4]= volDal[4]+1;
            }
            if((date >= moment(from4)) && (date <= moment(toString4))){
                avDal[3]= parseInt(avDal[3])+parseInt(average);
                volDal[3]= volDal[3]+1;
            }
            if((date >= moment(from3)) && (date <= moment(toString3))){
                avDal[2]= parseInt(avDal[2])+parseInt(average);
                volDal[2]= volDal[2]+1;
            }
            if((date >= moment(from2)) && (date <= moment(toString2))){
                avDal[1]= parseInt(avDal[1])+parseInt(average);
                volDal[1]= volDal[1]+1;
            }
            if((date >= moment(from1)) && (date <= moment(toString1))){
                avDal[0]= parseInt(avDal[0])+parseInt(average);
                volDal[0]= volDal[0]+1;
            }

        }
        else  if(location=='Jacksonville')
        {

            if((date >= moment(from12)) && (date <= moment(toString12)))
            {
                avJac[11]= parseInt(avJac[11])+parseInt(average);
                volJac[11]= volJac[11]+1;
            }
            if((date >= moment(from11)) && (date <= moment(toString11))){
                avJac[10]= parseInt(avJac[10])+parseInt(average);
                volJac[10]= volJac[10]+1;
            }
            if((date >= moment(from10)) && (date <= moment(toString10))){
                avJac[9]= parseInt(avJac[9])+parseInt(average);
                volJac[9]= volJac[9]+1;
            }
            if((date >= moment(from9)) && (date <= moment(toString9))){
                avJac[8]= parseInt(avJac[8])+parseInt(average);
                volJac[8]= volJac[8]+1;
            }
            if((date >= moment(from8)) && (date <= moment(toString8))){
                avJac[7]= parseInt(avJac[7])+parseInt(average);
                volJac[7]= volJac[7]+1;
            }
            if((date >= moment(from7)) && (date <= moment(toString7))){
                avJac[6]= parseInt(avJac[6])+parseInt(average);
                volJac[6]= volJac[6]+1;
            }
            if((date >= moment(from6)) && (date <= moment(toString6))){
                avJac[5]= parseInt(avJac[5])+parseInt(average);
                volJac[5]= volJac[5]+1;
            }
            if((date >= moment(from5)) && (date <= moment(toString5))){
                avJac[4]= parseInt(avJac[4])+parseInt(average);
                volJac[4]= volJac[4]+1;
            }
            if((date >= moment(from4)) && (date <= moment(toString4))){
                avJac[3]= parseInt(avJac[3])+parseInt(average);
                volJac[3]= volJac[3]+1;
            }
            if((date >= moment(from3)) && (date <= moment(toString3))){
                avJac[2]= parseInt(avJac[2])+parseInt(average);
                volJac[2]= volJac[2]+1;
            }
            if((date >= moment(from2)) && (date <= moment(toString2))){
                avJac[1]= parseInt(avJac[1])+parseInt(average);
                volJac[1]= volJac[1]+1;
            }
            if((date >= moment(from1)) && (date <= moment(toString1))){
                avJac[0]= parseInt(avJac[0])+parseInt(average);
                volJac[0]= volJac[0]+1;
            }
        }
        else if(location=='Jacksonville2' )
        {

            if((date >= moment(from12)) && (date <= moment(toString12)))
            {
                avJac2[11]= parseInt(avJac2[11])+parseInt(average);
                volJac2[11]= volJac2[11]+1;
            }
            if((date >= moment(from11)) && (date <= moment(toString11))){
                avJac2[10]= parseInt(avJac2[10])+parseInt(average);
                volJac2[10]= volJac2[10]+1;
            }
            if((date >= moment(from10)) && (date <= moment(toString10))){
                avJac2[9]= parseInt(avJac2[9])+parseInt(average);
                volJac2[9]= volJac2[9]+1;
            }
            if((date >= moment(from9)) && (date <= moment(toString9))){
                avJac2[8]= parseInt(avJac2[8])+parseInt(average);
                volJac2[8]= volJac2[8]+1;
            }
            if((date >= moment(from8)) && (date <= moment(toString8))){
                avJac2[7]= parseInt(avJac2[7])+parseInt(average);
                volJac2[7]= volJac2[7]+1;
            }
            if((date >= moment(from7)) && (date <= moment(toString7))){
                avJac2[6]= parseInt(avJac2[6])+parseInt(average);
                volJac2[6]= volJac2[6]+1;
            }
            if((date >= moment(from6)) && (date <= moment(toString6))){
                avJac2[5]= parseInt(avJac2[5])+parseInt(average);
                volJac2[5]= volJac2[5]+1;
            }
            if((date >= moment(from5)) && (date <= moment(toString5))){
                avJac2[4]= parseInt(avJac2[4])+parseInt(average);
                volJac2[4]= volJac2[4]+1;
            }
            if((date >= moment(from4)) && (date <= moment(toString4))){
                avJac2[3]= parseInt(avJac2[3])+parseInt(average);
                volJac2[3]= volJac2[3]+1;
            }
            if((date >= moment(from3)) && (date <= moment(toString3))){
                avJac2[2]= parseInt(avJac2[2])+parseInt(average);
                volJac2[2]= volJac2[2]+1;
            }
            if((date >= moment(from2)) && (date <= moment(toString2))){
                avJac2[1]= parseInt(avJac2[1])+parseInt(average);
                volJac2[1]= volJac2[1]+1;
            }
            if((date >= moment(from1)) && (date <= moment(toString1))){
                avJac2[0]= parseInt(avJac2[0])+parseInt(average);
                volJac2[0]= volJac2[0]+1;
            }
        }
        else if(location=='Lakeland')
        {

            if((date >= moment(from12)) && (date <= moment(toString12)))
            {
                avLak[11]= parseInt(avLak[11])+parseInt(average);
                volLak[11]= volLak[11]+1;
            }
            if((date >= moment(from11)) && (date <= moment(toString11))){
                avLak[10]= parseInt(avLak[10])+parseInt(average);
                volLak[10]= volLak[10]+1;
            }
            if((date >= moment(from10)) && (date <= moment(toString10))){
                avLak[9]= parseInt(avLak[9])+parseInt(average);
                volLak[9]= volLak[9]+1;
            }
            if((date >= moment(from9)) && (date <= moment(toString9))){
                avLak[8]= parseInt(avLak[8])+parseInt(average);
                volLak[8]= volLak[8]+1;
            }
            if((date >= moment(from8)) && (date <= moment(toString8))){
                avLak[7]= parseInt(avLak[7])+parseInt(average);
                volLak[7]= volLak[7]+1;
            }
            if((date >= moment(from7)) && (date <= moment(toString7))){
                avLak[6]= parseInt(avLak[6])+parseInt(average);
                volLak[6]= volLak[6]+1;
            }
            if((date >= moment(from6)) && (date <= moment(toString6))){
                avLak[5]= parseInt(avLak[5])+parseInt(average);
                volLak[5]= volLak[5]+1;
            }
            if((date >= moment(from5)) && (date <= moment(toString5))){
                avLak[4]= parseInt(avLak[4])+parseInt(average);
                volLak[4]= volLak[4]+1;
            }
            if((date >= moment(from4)) && (date <= moment(toString4))){
                avLak[3]= parseInt(avLak[3])+parseInt(average);
                volLak[3]= volLak[3]+1;
            }
            if((date >= moment(from3)) && (date <= moment(toString3))){
                avLak[2]= parseInt(avLak[2])+parseInt(average);
                volLak[2]= volLak[2]+1;
            }
            if((date >= moment(from2)) && (date <= moment(toString2))){
                avLak[1]= parseInt(avLak[1])+parseInt(average);
                volLak[1]= volLak[1]+1;
            }
            if((date >= moment(from1)) && (date <= moment(toString1))){
                avLak[0]= parseInt(avLak[0])+parseInt(average);
                volLak[0]= volLak[0]+1;
            }
        }
        else if(location=='Minneapolis ')
        {

            if((date >= moment(from12)) && (date <= moment(toString12)))
            {
                avMin[11]= parseInt(avMin[11])+parseInt(average);
                volMin[11]= volMin[11]+1;
            }
            if((date >= moment(from11)) && (date <= moment(toString11))){
                avMin[10]= parseInt(avMin[10])+parseInt(average);
                volMin[10]= volMin[10]+1;
            }
            if((date >= moment(from10)) && (date <= moment(toString10))){
                avMin[9]= parseInt(avMin[9])+parseInt(average);
                volMin[9]= volMin[9]+1;
            }
            if((date >= moment(from9)) && (date <= moment(toString9))){
                avMin[8]= parseInt(avMin[8])+parseInt(average);
                volMin[8]= volMin[8]+1;
            }
            if((date >= moment(from8)) && (date <= moment(toString8))){
                avMin[7]= parseInt(avMin[7])+parseInt(average);
                volMin[7]= volMin[7]+1;
            }
            if((date >= moment(from7)) && (date <= moment(toString7))){
                avMin[6]= parseInt(avMin[6])+parseInt(average);
                volMin[6]= volMin[6]+1;
            }
            if((date >= moment(from6)) && (date <= moment(toString6))){
                avMin[5]= parseInt(avMin[5])+parseInt(average);
                volMin[5]= volMin[5]+1;
            }
            if((date >= moment(from5)) && (date <= moment(toString5))){
                avMin[4]= parseInt(avMin[4])+parseInt(average);
                volMin[4]= volMin[4]+1;
            }
            if((date >= moment(from4)) && (date <= moment(toString4))){
                avMin[3]= parseInt(avMin[3])+parseInt(average);
                volMin[3]= volMin[3]+1;
            }
            if((date >= moment(from3)) && (date <= moment(toString3))){
                avMin[2]= parseInt(avMin[2])+parseInt(average);
                volMin[2]= volMin[2]+1;
            }
            if((date >= moment(from2)) && (date <= moment(toString2))){
                avMin[1]= parseInt(avMin[1])+parseInt(average);
                volMin[1]= volMin[1]+1;
            }
            if((date >= moment(from1)) && (date <= moment(toString1))){
                avMin[0]= parseInt(avMin[0])+parseInt(average);
                volMin[0]= volMin[0]+1;
            }
        }
        else if(location=='Montreal')
        {

            if((date >= moment(from12)) && (date <= moment(toString12)))
            {
                avMTL[11]= parseInt(avMTL[11])+parseInt(average);
                volMTL[11]= volMTL[11]+1;
            }
            if((date >= moment(from11)) && (date <= moment(toString11))){
                avMTL[10]= parseInt(avMTL[10])+parseInt(average);
                volMTL[10]= volMTL[10]+1;
            }
            if((date >= moment(from10)) && (date <= moment(toString10))){
                avMTL[9]= parseInt(avMTL[9])+parseInt(average);
                volMTL[9]= volMTL[9]+1;
            }
            if((date >= moment(from9)) && (date <= moment(toString9))){
                avMTL[8]= parseInt(avMTL[8])+parseInt(average);
                volMTL[8]= volMTL[8]+1;
            }
            if((date >= moment(from8)) && (date <= moment(toString8))){
                avMTL[7]= parseInt(avMTL[7])+parseInt(average);
                volMTL[7]= volMTL[7]+1;
            }
            if((date >= moment(from7)) && (date <= moment(toString7))){
                avMTL[6]= parseInt(avMTL[6])+parseInt(average);
                volMTL[6]= volMTL[6]+1;
            }
            if((date >= moment(from6)) && (date <= moment(toString6))){
                avMTL[5]= parseInt(avMTL[5])+parseInt(average);
                volMTL[5]= volMTL[5]+1;
            }
            if((date >= moment(from5)) && (date <= moment(toString5))){
                avMTL[4]= parseInt(avMTL[4])+parseInt(average);
                volMTL[4]= volMTL[4]+1;
            }
            if((date >= moment(from4)) && (date <= moment(toString4))){
                avMTL[3]= parseInt(avMTL[3])+parseInt(average);
                volMTL[3]= volMTL[3]+1;
            }
            if((date >= moment(from3)) && (date <= moment(toString3))){
                avMTL[2]= parseInt(avMTL[2])+parseInt(average);
                volMTL[2]= volMTL[2]+1;
            }
            if((date >= moment(from2)) && (date <= moment(toString2))){
                avMTL[1]= parseInt(avMTL[1])+parseInt(average);
                volMTL[1]= volMTL[1]+1;
            }
            if((date >= moment(from1)) && (date <= moment(toString1))){
                avMTL[0]= parseInt(avMTL[0])+parseInt(average);
                volMTL[0]= volMTL[0]+1;
            }
        }
        else if(location=='New Jersey')
        {

            if((date >= moment(from12)) && (date <= moment(toString12)))
            {
                avNNJ[11]= parseInt(avNNJ[11])+parseInt(average);
                volNNJ[11]= volNNJ[11]+1;
            }
            if((date >= moment(from11)) && (date <= moment(toString11))){
                avNNJ[10]= parseInt(avNNJ[10])+parseInt(average);
                volNNJ[10]= volNNJ[10]+1;
            }
            if((date >= moment(from10)) && (date <= moment(toString10))){
                avNNJ[9]= parseInt(avNNJ[9])+parseInt(average);
                volNNJ[9]= volNNJ[9]+1;
            }
            if((date >= moment(from9)) && (date <= moment(toString9))){
                avNNJ[8]= parseInt(avNNJ[8])+parseInt(average);
                volNNJ[8]= volNNJ[8]+1;
            }
            if((date >= moment(from8)) && (date <= moment(toString8))){
                avNNJ[7]= parseInt(avNNJ[7])+parseInt(average);
                volNNJ[7]= volNNJ[7]+1;
            }
            if((date >= moment(from7)) && (date <= moment(toString7))){
                avNNJ[6]= parseInt(avNNJ[6])+parseInt(average);
                volNNJ[6]= volNNJ[6]+1;
            }
            if((date >= moment(from6)) && (date <= moment(toString6))){
                avNNJ[5]= parseInt(avNNJ[5])+parseInt(average);
                volNNJ[5]= volNNJ[5]+1;
            }
            if((date >= moment(from5)) && (date <= moment(toString5))){
                avNNJ[4]= parseInt(avNNJ[4])+parseInt(average);
                volNNJ[4]= volNNJ[4]+1;
            }
            if((date >= moment(from4)) && (date <= moment(toString4))){
                avNNJ[3]= parseInt(avNNJ[3])+parseInt(average);
                volNNJ[3]= volNNJ[3]+1;
            }
            if((date >= moment(from3)) && (date <= moment(toString3))){
                avNNJ[2]= parseInt(avNNJ[2])+parseInt(average);
                volNNJ[2]= volNNJ[2]+1;
            }
            if((date >= moment(from2)) && (date <= moment(toString2))){
                avNNJ[1]= parseInt(avNNJ[1])+parseInt(average);
                volNNJ[1]= volNNJ[1]+1;
            }
            if((date >= moment(from1)) && (date <= moment(toString1))){
                avNNJ[0]= parseInt(avNNJ[0])+parseInt(average);
                volNNJ[0]= volNNJ[0]+1;
            }
        }
        else  if(location=='Toronto')
        {

            if((date >= moment(from12)) && (date <= moment(toString12)))
            {
                avTOR[11]= parseInt(avTOR[11])+parseInt(average);
                volTOR[11]= volTOR[11]+1;
            }
            if((date >= moment(from11)) && (date <= moment(toString11))){
                avTOR[10]= parseInt(avTOR[10])+parseInt(average);
                volTOR[10]= volTOR[10]+1;
            }
            if((date >= moment(from10)) && (date <= moment(toString10))){
                avTOR[9]= parseInt(avTOR[9])+parseInt(average);
                volTOR[9]= volTOR[9]+1;
            }
            if((date >= moment(from9)) && (date <= moment(toString9))){
                avTOR[8]= parseInt(avTOR[8])+parseInt(average);
                volTOR[8]= volTOR[8]+1;
            }
            if((date >= moment(from8)) && (date <= moment(toString8))){
                avTOR[7]= parseInt(avTOR[7])+parseInt(average);
                volTOR[7]= volTOR[7]+1;
            }
            if((date >= moment(from7)) && (date <= moment(toString7))){
                avTOR[6]= parseInt(avTOR[6])+parseInt(average);
                volTOR[6]= volTOR[6]+1;
            }
            if((date >= moment(from6)) && (date <= moment(toString6))){
                avTOR[5]= parseInt(avTOR[5])+parseInt(average);
                volTOR[5]= volTOR[5]+1;
            }
            if((date >= moment(from5)) && (date <= moment(toString5))){
                avTOR[4]= parseInt(avTOR[4])+parseInt(average);
                volTOR[4]= volTOR[4]+1;
            }
            if((date >= moment(from4)) && (date <= moment(toString4))){
                avTOR[3]= parseInt(avTOR[3])+parseInt(average);
                volTOR[3]= volTOR[3]+1;
            }
            if((date >= moment(from3)) && (date <= moment(toString3))){
                avTOR[2]= parseInt(avTOR[2])+parseInt(average);
                volTOR[2]= volTOR[2]+1;
            }
            if((date >= moment(from2)) && (date <= moment(toString2))){
                avTOR[1]= parseInt(avTOR[1])+parseInt(average);
                volTOR[1]= volTOR[1]+1;
            }
            if((date >= moment(from1)) && (date <= moment(toString1))){
                avTOR[0]= parseInt(avTOR[0])+parseInt(average);
                volTOR[0]= volTOR[0]+1;
            }
        }
        else if(location=='Vancouver')
        {
            if((date >= moment(from12)) && (date <= moment(toString12)))
            {
                avVAN[11]= parseInt(avVAN[11])+parseInt(average);
                volVAN[11]= volVAN[11]+1;
            }
            if((date >= moment(from11)) && (date <= moment(toString11))){
                avVAN[10]= parseInt(avVAN[10])+parseInt(average);
                volVAN[10]= volVAN[10]+1;
            }
            if((date >= moment(from10)) && (date <= moment(toString10))){
                avVAN[9]= parseInt(avVAN[9])+parseInt(average);
                volVAN[9]= volVAN[9]+1;
            }
            if((date >= moment(from9)) && (date <= moment(toString9))){
                avVAN[8]= parseInt(avVAN[8])+parseInt(average);
                volVAN[8]= volVAN[8]+1;
            }
            if((date >= moment(from8)) && (date <= moment(toString8))){
                avVAN[7]= parseInt(avVAN[7])+parseInt(average);
                volVAN[7]= volVAN[7]+1;
            }
            if((date >= moment(from7)) && (date <= moment(toString7))){
                avVAN[6]= parseInt(avVAN[6])+parseInt(average);
                volVAN[6]= volVAN[6]+1;
            }
            if((date >= moment(from6)) && (date <= moment(toString6))){
                avVAN[5]= parseInt(avVAN[5])+parseInt(average);
                volVAN[5]= volVAN[5]+1;
            }
            if((date >= moment(from5)) && (date <= moment(toString5))){
                avVAN[4]= parseInt(avVAN[4])+parseInt(average);
                volVAN[4]= volVAN[4]+1;
            }
            if((date >= moment(from4)) && (date <= moment(toString4))){
                avVAN[3]= parseInt(avVAN[3])+parseInt(average);
                volVAN[3]= volVAN[3]+1;
            }
            if((date >= moment(from3)) && (date <= moment(toString3))){
                avVAN[2]= parseInt(avVAN[2])+parseInt(average);
                volVAN[2]= volVAN[2]+1;
            }
            if((date >= moment(from2)) && (date <= moment(toString2))){
                avVAN[1]= parseInt(avVAN[1])+parseInt(average);
                volVAN[1]= volVAN[1]+1;
            }
            if((date >= moment(from1)) && (date <= moment(toString1))){
                avVAN[0]= parseInt(avVAN[0])+parseInt(average);
                volVAN[0]= volVAN[0]+1;
            }
        }
    }
    var avCol0=(parseFloat(avCol[0])!=0)?parseFloat(avCol[0]/volCol[0]).toFixed(1):0;
    var avCol1=(parseFloat(avCol[1])!=0)?parseFloat(avCol[1]/volCol[1]).toFixed(1):0;
    var avCol2=(parseFloat(avCol[2])!=0)?parseFloat(avCol[2]/volCol[2]).toFixed(1):0;
    var avCol3=(parseFloat(avCol[3])!=0)?parseFloat(avCol[3]/volCol[3]).toFixed(1):0;
    var avCol4=(parseFloat(avCol[4])!=0)?parseFloat(avCol[4]/volCol[4]).toFixed(1):0;
    var avCol5=(parseFloat(avCol[5])!=0)?parseFloat(avCol[5]/volCol[5]).toFixed(1):0;
    var avCol6=(parseFloat(avCol[6])!=0)?parseFloat(avCol[6]/volCol[6]).toFixed(1):0;
    var avCol7=(parseFloat(avCol[7])!=0)?parseFloat(avCol[7]/volCol[7]).toFixed(1):0;
    var avCol8=(parseFloat(avCol[8])!=0)?parseFloat(avCol[8]/volCol[8]).toFixed(1):0;
    var avCol9=(parseFloat(avCol[9])!=0)?parseFloat(avCol[9]/volCol[9]).toFixed(1):0;
    var avCol10=(parseFloat(avCol[10])!=0)?parseFloat(avCol[10]/volCol[10]).toFixed(1):0;
    var avCol11=(parseFloat(avCol[11])!=0)?parseFloat(avCol[11]/volCol[11]).toFixed(1):0;

    var avDal0=(parseFloat(avDal[0])!=0)?parseFloat(avDal[0]/volDal[0]).toFixed(1):0;
    var avDal1=(parseFloat(avDal[1])!=0)?parseFloat(avDal[1]/volDal[1]).toFixed(1):0;
    var avDal2=(parseFloat(avDal[2])!=0)?parseFloat(avDal[2]/volDal[2]).toFixed(1):0;
    var avDal3=(parseFloat(avDal[3])!=0)?parseFloat(avDal[3]/volDal[3]).toFixed(1):0;
    var avDal4=(parseFloat(avDal[4])!=0)?parseFloat(avDal[4]/volDal[4]).toFixed(1):0;
    var avDal5=(parseFloat(avDal[5])!=0)?parseFloat(avDal[5]/volDal[5]).toFixed(1):0;
    var avDal6=(parseFloat(avDal[6])!=0)?parseFloat(avDal[6]/volDal[6]).toFixed(1):0;
    var avDal7=(parseFloat(avDal[7])!=0)?parseFloat(avDal[7]/volDal[7]).toFixed(1):0;
    var avDal8=(parseFloat(avDal[8])!=0)?parseFloat(avDal[8]/volDal[8]).toFixed(1):0;
    var avDal9=(parseFloat(avDal[9])!=0)?parseFloat(avDal[9]/volDal[9]).toFixed(1):0;
    var avDal10=(parseFloat(avDal[10])!=0)?parseFloat(avDal[10]/volDal[10]).toFixed(1):0;
    var avDal11=(parseFloat(avDal[11])!=0)?parseFloat(avDal[11]/volDal[11]).toFixed(1):0;

    var avJac0=(parseFloat(avJac[0])!=0)?parseFloat(avJac[0]/volJac[0]).toFixed(1):0;
    var avJac1=(parseFloat(avJac[1])!=0)?parseFloat(avJac[1]/volJac[1]).toFixed(1):0;
    var avJac_2=(parseFloat(avJac[2])!=0)?parseFloat(avJac[2]/volJac[2]).toFixed(1):0;
    var avJac3=(parseFloat(avJac[3])!=0)?parseFloat(avJac[3]/volJac[3]).toFixed(1):0;
    var avJac4=(parseFloat(avJac[4])!=0)?parseFloat(avJac[4]/volJac[4]).toFixed(1):0;
    var avJac5=(parseFloat(avJac[5])!=0)?parseFloat(avJac[5]/volJac[5]).toFixed(1):0;
    var avJac6=(parseFloat(avJac[6])!=0)?parseFloat(avJac[6]/volJac[6]).toFixed(1):0;
    var avJac7=(parseFloat(avJac[7])!=0)?parseFloat(avJac[7]/volJac[7]).toFixed(1):0;
    var avJac8=(parseFloat(avJac[8])!=0)?parseFloat(avJac[8]/volJac[8]).toFixed(1):0;
    var avJac9=(parseFloat(avJac[9])!=0)?parseFloat(avJac[9]/volJac[9]).toFixed(1):0;
    var avJac10=(parseFloat(avJac[10])!=0)?parseFloat(avJac[10]/volJac[10]).toFixed(1):0;
    var avJac11=(parseFloat(avJac[11])!=0)?parseFloat(avJac[11]/volJac[11]).toFixed(1):0;

    var avJact0=(parseFloat(avJac2[0])!=0)?parseFloat(avJac2[0]/volJac2[0]).toFixed(1):0;
    var avJact1=(parseFloat(avJac2[1])!=0)?parseFloat(avJac2[1]/volJac2[1]).toFixed(1):0;
    var avJact2=(parseFloat(avJac2[2])!=0)?parseFloat(avJac2[2]/volJac2[2]).toFixed(1):0;
    var avJact3=(parseFloat(avJac2[3])!=0)?parseFloat(avJac2[3]/volJac2[3]).toFixed(1):0;
    var avJact4=(parseFloat(avJac2[4])!=0)?parseFloat(avJac2[4]/volJac2[4]).toFixed(1):0;
    var avJact5=(parseFloat(avJac2[5])!=0)?parseFloat(avJac2[5]/volJac2[5]).toFixed(1):0;
    var avJact6=(parseFloat(avJac2[6])!=0)?parseFloat(avJac2[6]/volJac2[6]).toFixed(1):0;
    var avJact7=(parseFloat(avJac2[7])!=0)?parseFloat(avJac2[7]/volJac2[7]).toFixed(1):0;
    var avJact8=(parseFloat(avJac2[8])!=0)?parseFloat(avJac2[8]/volJac2[8]).toFixed(1):0;
    var avJact9=(parseFloat(avJac2[9])!=0)?parseFloat(avJac2[9]/volJac2[9]).toFixed(1):0;
    var avJact10=(parseFloat(avJac2[10])!=0)?parseFloat(avJac2[10]/volJac2[10]).toFixed(1):0;
    var avJact11=(parseFloat(avJac2[11])!=0)?parseFloat(avJac2[11]/volJac2[11]).toFixed(1):0;

    var avLak0=(parseFloat(avLak[0])!=0)?parseFloat(avLak[0]/volLak[0]).toFixed(1):0;
    var avLak1=(parseFloat(avLak[1])!=0)?parseFloat(avLak[1]/volLak[1]).toFixed(1):0;
    var avLak2=(parseFloat(avLak[2])!=0)?parseFloat(avLak[2]/volLak[2]).toFixed(1):0;
    var avLak3=(parseFloat(avLak[3])!=0)?parseFloat(avLak[3]/volLak[3]).toFixed(1):0;
    var avLak4=(parseFloat(avLak[4])!=0)?parseFloat(avLak[4]/volLak[4]).toFixed(1):0;
    var avLak5=(parseFloat(avLak[5])!=0)?parseFloat(avLak[5]/volLak[5]).toFixed(1):0;
    var avLak6=(parseFloat(avLak[6])!=0)?parseFloat(avLak[6]/volLak[6]).toFixed(1):0;
    var avLak7=(parseFloat(avLak[7])!=0)?parseFloat(avLak[7]/volLak[7]).toFixed(1):0;
    var avLak8=(parseFloat(avLak[8])!=0)?parseFloat(avLak[8]/volLak[8]).toFixed(1):0;
    var avLak9=(parseFloat(avLak[9])!=0)?parseFloat(avLak[9]/volLak[9]).toFixed(1):0;
    var avLak10=(parseFloat(avLak[10])!=0)?parseFloat(avLak[10]/volLak[10]).toFixed(1):0;
    var avLak11=(parseFloat(avLak[11])!=0)?parseFloat(avLak[11]/volLak[11]).toFixed(1):0;

    var avMin0=(parseFloat(avMin[0])!=0)?parseFloat(avMin[0]/volMin[0]).toFixed(1):0;
    var avMin1=(parseFloat(avMin[1])!=0)?parseFloat(avMin[1]/volMin[1]).toFixed(1):0;
    var avMin2=(parseFloat(avMin[2])!=0)?parseFloat(avMin[2]/volMin[2]).toFixed(1):0;
    var avMin3=(parseFloat(avMin[3])!=0)?parseFloat(avMin[3]/volMin[3]).toFixed(1):0;
    var avMin4=(parseFloat(avMin[4])!=0)?parseFloat(avMin[4]/volMin[4]).toFixed(1):0;
    var avMin5=(parseFloat(avMin[5])!=0)?parseFloat(avMin[5]/volMin[5]).toFixed(1):0;
    var avMin6=(parseFloat(avMin[6])!=0)?parseFloat(avMin[6]/volMin[6]).toFixed(1):0;
    var avMin7=(parseFloat(avMin[7])!=0)?parseFloat(avMin[7]/volMin[7]).toFixed(1):0;
    var avMin8=(parseFloat(avMin[8])!=0)?parseFloat(avMin[8]/volMin[8]).toFixed(1):0;
    var avMin9=(parseFloat(avMin[9])!=0)?parseFloat(avMin[9]/volMin[9]).toFixed(1):0;
    var avMin10=(parseFloat(avMin[10])!=0)?parseFloat(avMin[10]/volMin[10]).toFixed(1):0;
    var avMin11=(parseFloat(avMin[11])!=0)?parseFloat(avMin[11]/volMin[11]).toFixed(1):0;


    var avMTL0=(parseFloat(avMTL[0])!=0)?parseFloat(avMTL[0]/volMTL[0]).toFixed(1):0;
    var avMTL1=(parseFloat(avMTL[1])!=0)?parseFloat(avMTL[1]/volMTL[1]).toFixed(1):0;
    var avMTL2=(parseFloat(avMTL[2])!=0)?parseFloat(avMTL[2]/volMTL[2]).toFixed(1):0;
    var avMTL3=(parseFloat(avMTL[3])!=0)?parseFloat(avMTL[3]/volMTL[3]).toFixed(1):0;
    var avMTL4=(parseFloat(avMTL[4])!=0)?parseFloat(avMTL[4]/volMTL[4]).toFixed(1):0;
    var avMTL5=(parseFloat(avMTL[5])!=0)?parseFloat(avMTL[5]/volMTL[5]).toFixed(1):0;
    var avMTL6=(parseFloat(avMTL[6])!=0)?parseFloat(avMTL[6]/volMTL[6]).toFixed(1):0;
    var avMTL7=(parseFloat(avMTL[7])!=0)?parseFloat(avMTL[7]/volMTL[7]).toFixed(1):0;
    var avMTL8=(parseFloat(avMTL[8])!=0)?parseFloat(avMTL[8]/volMTL[8]).toFixed(1):0;
    var avMTL9=(parseFloat(avMTL[9])!=0)?parseFloat(avMTL[9]/volMTL[9]).toFixed(1):0;
    var avMTL10=(parseFloat(avMTL[10])!=0)?parseFloat(avMTL[10]/volMTL[10]).toFixed(1):0;
    var avMTL11=(parseFloat(avMTL[11])!=0)?parseFloat(avMTL[11]/volMTL[11]).toFixed(1):0;

    var avNNJ0=(parseFloat(avNNJ[0])!=0)?parseFloat(avNNJ[0]/volNNJ[0]).toFixed(1):0;
    var avNNJ1=(parseFloat(avNNJ[1])!=0)?parseFloat(avNNJ[1]/volNNJ[1]).toFixed(1):0;
    var avNNJ2=(parseFloat(avNNJ[2])!=0)?parseFloat(avNNJ[2]/volNNJ[2]).toFixed(1):0;
    var avNNJ3=(parseFloat(avNNJ[3])!=0)?parseFloat(avNNJ[3]/volNNJ[3]).toFixed(1):0;
    var avNNJ4=(parseFloat(avNNJ[4])!=0)?parseFloat(avNNJ[4]/volNNJ[4]).toFixed(1):0;
    var avNNJ5=(parseFloat(avNNJ[5])!=0)?parseFloat(avNNJ[5]/volNNJ[5]).toFixed(1):0;
    var avNNJ6=(parseFloat(avNNJ[6])!=0)?parseFloat(avNNJ[6]/volNNJ[6]).toFixed(1):0;
    var avNNJ7=(parseFloat(avNNJ[7])!=0)?parseFloat(avNNJ[7]/volNNJ[7]).toFixed(1):0;
    var avNNJ8=(parseFloat(avNNJ[8])!=0)?parseFloat(avNNJ[8]/volNNJ[8]).toFixed(1):0;
    var avNNJ9=(parseFloat(avNNJ[9])!=0)?parseFloat(avNNJ[9]/volNNJ[9]).toFixed(1):0;
    var avNNJ10=(parseFloat(avNNJ[10])!=0)?parseFloat(avNNJ[10]/volNNJ[10]).toFixed(1):0;
    var avNNJ11=(parseFloat(avNNJ[11])!=0)?parseFloat(avNNJ[11]/volNNJ[11]).toFixed(1):0;

    var avTOR0=(parseFloat(avTOR[0])!=0)?parseFloat(avTOR[0]/volTOR[0]).toFixed(1):0;
    var avTOR1=(parseFloat(avTOR[1])!=0)?parseFloat(avTOR[1]/volTOR[1]).toFixed(1):0;
    var avTOR2=(parseFloat(avTOR[2])!=0)?parseFloat(avTOR[2]/volTOR[2]).toFixed(1):0;
    var avTOR3=(parseFloat(avTOR[3])!=0)?parseFloat(avTOR[3]/volTOR[3]).toFixed(1):0;
    var avTOR4=(parseFloat(avTOR[4])!=0)?parseFloat(avTOR[4]/volTOR[4]).toFixed(1):0;
    var avTOR5=(parseFloat(avTOR[5])!=0)?parseFloat(avTOR[5]/volTOR[5]).toFixed(1):0;
    var avTOR6=(parseFloat(avTOR[6])!=0)?parseFloat(avTOR[6]/volTOR[6]).toFixed(1):0;
    var avTOR7=(parseFloat(avTOR[7])!=0)?parseFloat(avTOR[7]/volTOR[7]).toFixed(1):0;
    var avTOR8=(parseFloat(avTOR[8])!=0)?parseFloat(avTOR[8]/volTOR[8]).toFixed(1):0;
    var avTOR9=(parseFloat(avTOR[9])!=0)?parseFloat(avTOR[9]/volTOR[9]).toFixed(1):0;
    var avTOR10=(parseFloat(avTOR[10])!=0)?parseFloat(avTOR[10]/volTOR[10]).toFixed(1):0;
    var avTOR11=(parseFloat(avTOR[11])!=0)?parseFloat(avTOR[11]/volTOR[11]).toFixed(1):0;

    var avVAN0=(parseFloat(avVAN[0])!=0)?parseFloat(avVAN[0]/volVAN[0]).toFixed(1):0;
    var avVAN1=(parseFloat(avVAN[1])!=0)?parseFloat(avVAN[1]/volVAN[1]).toFixed(1):0;
    var avVAN2=(parseFloat(avVAN[2])!=0)?parseFloat(avVAN[2]/volVAN[2]).toFixed(1):0;
    var avVAN3=(parseFloat(avVAN[3])!=0)?parseFloat(avVAN[3]/volVAN[3]).toFixed(1):0;
    var avVAN4=(parseFloat(avVAN[4])!=0)?parseFloat(avVAN[4]/volVAN[4]).toFixed(1):0;
    var avVAN5=(parseFloat(avVAN[5])!=0)?parseFloat(avVAN[5]/volVAN[5]).toFixed(1):0;
    var avVAN6=(parseFloat(avVAN[6])!=0)?parseFloat(avVAN[6]/volVAN[6]).toFixed(1):0;
    var avVAN7=(parseFloat(avVAN[7])!=0)?parseFloat(avVAN[7]/volVAN[7]).toFixed(1):0;
    var avVAN8=(parseFloat(avVAN[8])!=0)?parseFloat(avVAN[8]/volVAN[8]).toFixed(1):0;
    var avVAN9=(parseFloat(avVAN[9])!=0)?parseFloat(avVAN[9]/volVAN[9]).toFixed(1):0;
    var avVAN10=(parseFloat(avVAN[10])!=0)?parseFloat(avVAN[10]/volVAN[10]).toFixed(1):0;
    var avVAN11=(parseFloat(avVAN[11])!=0)?parseFloat(avVAN[11]/volVAN[11]).toFixed(1):0;


    var avrST11=parseFloat(avCol[11])+parseFloat(avDal[11])+parseFloat(avJac[11])+parseFloat(avJac2[11])+parseFloat(avLak[11])+parseFloat(avMin[11])+parseFloat(avMTL[11])+parseFloat(avNNJ[11])+parseFloat(avTOR[11])+parseFloat(avVAN[11]);
    var volST11=parseFloat(volCol[11])+parseFloat(volDal[11])+parseFloat(volJac[11])+parseFloat(volJac2[11])+parseFloat(volLak[11])+parseFloat(volMin[11])+parseFloat(volMTL[11])+parseFloat(volNNJ[11])+parseFloat(volTOR[11])+parseFloat(volVAN[11]);
    var avST11=(parseFloat(avrST11)!=0)?parseFloat(avrST11/volST11).toFixed(1):0;

    var avrST10=parseFloat(avCol[10])+parseFloat(avDal[10])+parseFloat(avJac[10])+parseFloat(avJac2[10])+parseFloat(avLak[10])+parseFloat(avMin[10])+parseFloat(avMTL[10])+parseFloat(avNNJ[10])+parseFloat(avTOR[10])+parseFloat(avVAN[10]);
    var volST10=parseFloat(volCol[10])+parseFloat(volDal[10])+parseFloat(volJac[10])+parseFloat(volJac2[10])+parseFloat(volLak[10])+parseFloat(volMin[10])+parseFloat(volMTL[10])+parseFloat(volNNJ[10])+parseFloat(volTOR[10])+parseFloat(volVAN[10]);
    var avST10=(parseFloat(avrST10)!=0)?parseFloat(avrST10/volST10).toFixed(1):0;

    var avrST9=parseFloat(avCol[9])+parseFloat(avDal[9])+parseFloat(avJac[9])+parseFloat(avJac2[9])+parseFloat(avLak[9])+parseFloat(avMin[9])+parseFloat(avMTL[9])+parseFloat(avNNJ[9])+parseFloat(avTOR[9])+parseFloat(avVAN[9]);
    var volST9=parseFloat(volCol[9])+parseFloat(volDal[9])+parseFloat(volJac[9])+parseFloat(volJac2[9])+parseFloat(volLak[9])+parseFloat(volMin[9])+parseFloat(volMTL[9])+parseFloat(volNNJ[9])+parseFloat(volTOR[9])+parseFloat(volVAN[9]);
    var avST9=(parseFloat(avrST9)!=0)?parseFloat(avrST9/volST9).toFixed(1):0;

    var avrST8=parseFloat(avCol[8])+parseFloat(avDal[8])+parseFloat(avJac[8])+parseFloat(avJac2[8])+parseFloat(avLak[8])+parseFloat(avMin[8])+parseFloat(avMTL[8])+parseFloat(avNNJ[8])+parseFloat(avTOR[8])+parseFloat(avVAN[8]);
    var volST8=parseFloat(volCol[8])+parseFloat(volDal[8])+parseFloat(volJac[8])+parseFloat(volJac2[8])+parseFloat(volLak[8])+parseFloat(volMin[8])+parseFloat(volMTL[8])+parseFloat(volNNJ[8])+parseFloat(volTOR[8])+parseFloat(volVAN[8]);
    var avST8=(parseFloat(avrST8)!=0)?parseFloat(avrST8/volST8).toFixed(1):0;

    var avrST7=parseFloat(avCol[7])+parseFloat(avDal[7])+parseFloat(avJac[7])+parseFloat(avJac2[7])+parseFloat(avLak[7])+parseFloat(avMin[7])+parseFloat(avMTL[7])+parseFloat(avNNJ[7])+parseFloat(avTOR[7])+parseFloat(avVAN[7]);
    var volST7=parseFloat(volCol[7])+parseFloat(volDal[7])+parseFloat(volJac[7])+parseFloat(volJac2[7])+parseFloat(volLak[7])+parseFloat(volMin[7])+parseFloat(volMTL[7])+parseFloat(volNNJ[7])+parseFloat(volTOR[7])+parseFloat(volVAN[7]);
    var avST7=(parseFloat(avrST7)!=0)?parseFloat(avrST7/volST7).toFixed(1):0;

    var avrST6=parseFloat(avCol[6])+parseFloat(avDal[6])+parseFloat(avJac[6])+parseFloat(avJac2[6])+parseFloat(avLak[6])+parseFloat(avMin[6])+parseFloat(avMTL[6])+parseFloat(avNNJ[6])+parseFloat(avTOR[6])+parseFloat(avVAN[6]);
    var volST6=parseFloat(volCol[6])+parseFloat(volDal[6])+parseFloat(volJac[6])+parseFloat(volJac2[6])+parseFloat(volLak[6])+parseFloat(volMin[6])+parseFloat(volMTL[6])+parseFloat(volNNJ[6])+parseFloat(volTOR[6])+parseFloat(volVAN[6]);
    var avST6=(parseFloat(avrST6)!=0)?parseFloat(avrST6/volST6).toFixed(1):0;

    var avrST5=parseFloat(avCol[5])+parseFloat(avDal[5])+parseFloat(avJac[5])+parseFloat(avJac2[5])+parseFloat(avLak[5])+parseFloat(avMin[5])+parseFloat(avMTL[5])+parseFloat(avNNJ[5])+parseFloat(avTOR[5])+parseFloat(avVAN[5]);
    var volST5=parseFloat(volCol[5])+parseFloat(volDal[5])+parseFloat(volJac[5])+parseFloat(volJac2[5])+parseFloat(volLak[5])+parseFloat(volMin[5])+parseFloat(volMTL[5])+parseFloat(volNNJ[5])+parseFloat(volTOR[5])+parseFloat(volVAN[5]);
    var avST5=(parseFloat(avrST5)!=0)?parseFloat(avrST5/volST5).toFixed(1):0;

    var avrST4=parseFloat(avCol[4])+parseFloat(avDal[4])+parseFloat(avJac[4])+parseFloat(avJac2[4])+parseFloat(avLak[4])+parseFloat(avMin[4])+parseFloat(avMTL[4])+parseFloat(avNNJ[4])+parseFloat(avTOR[4])+parseFloat(avVAN[4]);
    var volST4=parseFloat(volCol[4])+parseFloat(volDal[4])+parseFloat(volJac[4])+parseFloat(volJac2[4])+parseFloat(volLak[4])+parseFloat(volMin[4])+parseFloat(volMTL[4])+parseFloat(volNNJ[4])+parseFloat(volTOR[4])+parseFloat(volVAN[4]);
    var avST4=(parseFloat(avrST4)!=0)?parseFloat(avrST4/volST4).toFixed(1):0;

    var avrST3=parseFloat(avCol[3])+parseFloat(avDal[3])+parseFloat(avJac[3])+parseFloat(avJac2[3])+parseFloat(avLak[3])+parseFloat(avMin[3])+parseFloat(avMTL[3])+parseFloat(avNNJ[3])+parseFloat(avTOR[3])+parseFloat(avVAN[3]);
    var volST3=parseFloat(volCol[3])+parseFloat(volDal[3])+parseFloat(volJac[3])+parseFloat(volJac2[3])+parseFloat(volLak[3])+parseFloat(volMin[3])+parseFloat(volMTL[3])+parseFloat(volNNJ[3])+parseFloat(volTOR[3])+parseFloat(volVAN[3]);
    var avST3=(parseFloat(avrST3)!=0)?parseFloat(avrST3/volST3).toFixed(1):0;

    var avrST2=parseFloat(avCol[2])+parseFloat(avDal[2])+parseFloat(avJac[2])+parseFloat(avJac2[2])+parseFloat(avLak[2])+parseFloat(avMin[2])+parseFloat(avMTL[2])+parseFloat(avNNJ[2])+parseFloat(avTOR[2])+parseFloat(avVAN[2]);
    var volST2=parseFloat(volCol[2])+parseFloat(volDal[2])+parseFloat(volJac[2])+parseFloat(volJac2[2])+parseFloat(volLak[2])+parseFloat(volMin[2])+parseFloat(volMTL[2])+parseFloat(volNNJ[2])+parseFloat(volTOR[2])+parseFloat(volVAN[2]);
    var avST2=(parseFloat(avrST2)!=0)?parseFloat(avrST2/volST2).toFixed(1):0;


    var avrST1=parseFloat(avCol[1])+parseFloat(avDal[1])+parseFloat(avJac[1])+parseFloat(avJac2[1])+parseFloat(avLak[1])+parseFloat(avMin[1])+parseFloat(avMTL[1])+parseFloat(avNNJ[1])+parseFloat(avTOR[1])+parseFloat(avVAN[1]);
    var volST1=parseFloat(volCol[1])+parseFloat(volDal[1])+parseFloat(volJac[1])+parseFloat(volJac2[1])+parseFloat(volLak[1])+parseFloat(volMin[1])+parseFloat(volMTL[1])+parseFloat(volNNJ[1])+parseFloat(volTOR[1])+parseFloat(volVAN[1]);
    var avST1=(parseFloat(avrST1)!=0)?parseFloat(avrST1/volST1).toFixed(1):0;

    var avrST0=parseFloat(avCol[0])+parseFloat(avDal[0])+parseFloat(avJac[0])+parseFloat(avJac2[0])+parseFloat(avLak[0])+parseFloat(avMin[0])+parseFloat(avMTL[0])+parseFloat(avNNJ[0])+parseFloat(avTOR[0])+parseFloat(avVAN[0]);
    var volST0=parseFloat(volCol[0])+parseFloat(volDal[0])+parseFloat(volJac[0])+parseFloat(volJac2[0])+parseFloat(volLak[0])+parseFloat(volMin[0])+parseFloat(volMTL[0])+parseFloat(volNNJ[0])+parseFloat(volTOR[0])+parseFloat(volVAN[0]);
    var avST0=(parseFloat(avrST0)!=0)?parseFloat(avrST0/volST0).toFixed(1):0;


    var seriesReturn= '{name: \'Columbus\',type: \'spline\',data: [' +
        '{y:'+avCol11+',z:'+volCol[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Columbus&location=33&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[0]+'\'},' +
        '{y:'+avCol10+',z:'+volCol[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Columbus&location=33&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[1]+'\'},' +
        '{y:'+avCol9+',z:'+volCol[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Columbus&location=33&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[2]+'\'},' +
        '{y:'+avCol8+',z:'+volCol[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Columbus&location=33&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[3]+'\'},' +
        '{y:'+avCol7+',z:'+volCol[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Columbus&location=33&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[4]+'\'},' +
        '{y:'+avCol6+',z:'+volCol[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Columbus&location=33&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[5]+'\'},' +
        '{y:'+avCol5+',z:'+volCol[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Columbus&location=33&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[6]+'\'},' +
        '{y:'+avCol4+',z:'+volCol[4]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Columbus&location=33&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[7]+'\'},' +
        '{y:'+avCol3+',z:'+volCol[3]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Columbus&location=33&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[8]+'\'},' +
        '{y:'+avCol2+',z:'+volCol[2]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Columbus&location=33&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[9]+'\'},' +
        '{y:'+avCol1+',z:'+volCol[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Columbus&location=33&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[10]+'\'},' +
        '{y:'+avCol0+',z:'+volCol[0]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Columbus&location=33&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Dallas\',type: \'spline\',data: [' +
        '{y:'+avDal11+',z:'+volDal[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Dallas&location=20&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[0]+'\'},' +
        '{y:'+avDal10+',z:'+volDal[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Dallas&location=20&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[1]+'\'},' +
        '{y:'+avDal9+',z:'+volDal[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Dallas&location=20&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[2]+'\'},' +
        '{y:'+avDal8+',z:'+volDal[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Dallas&location=20&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[3]+'\'},' +
        '{y:'+avDal7+',z:'+volDal[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Dallas&location=20&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[4]+'\'},' +
        '{y:'+avDal6+',z:'+volDal[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Dallas&location=20&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[5]+'\'},' +
        '{y:'+avDal5+',z:'+volDal[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Dallas&location=20&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[6]+'\'},' +
        '{y:'+avDal4+',z:'+volDal[4]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Dallas&location=20&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[7]+'\'},' +
        '{y:'+avDal3+',z:'+volDal[3]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Dallas&location=20&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[8]+'\'},' +
        '{y:'+avDal2+',z:'+volDal[2]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Dallas&location=20&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[9]+'\'},' +
        '{y:'+avDal1+',z:'+volDal[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Dallas&location=20&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[10]+'\'},' +
        '{y:'+avDal0+',z:'+volDal[0]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Dallas&location=20&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Jacksonville\',type: \'spline\',data: [' +
        '{y:'+avJac11+',z:'+volJac[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville&location=29&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[0]+'\'},' +
        '{y:'+avJac10+',z:'+volJac[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville&location=29&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[1]+'\'},' +
        '{y:'+avJac9+',z:'+volJac[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville&location=29&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[2]+'\'},' +
        '{y:'+avJac8+',z:'+volJac[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville&location=29&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[3]+'\'},' +
        '{y:'+avJac7+',z:'+volJac[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville&location=29&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[4]+'\'},' +
        '{y:'+avJac6+',z:'+volJac[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville&location=29&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[5]+'\'},' +
        '{y:'+avJac5+',z:'+volJac[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville&location=29&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[6]+'\'},' +
        '{y:'+avJac4+',z:'+volJac[4]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville&location=29&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[7]+'\'},' +
        '{y:'+avJac3+',z:'+volJac[3]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville&location=29&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[8]+'\'},' +
        '{y:'+avJac_2+',z:'+volJac[2]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville&location=29&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[9]+'\'},' +
        '{y:'+avJac1+',z:'+volJac[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville&location=29&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[10]+'\'},' +
        '{y:'+avJac0+',z:'+volJac[0]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville&location=29&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Jacksonville2\',type:  \'spline\',data: [' +
        '{y:'+avJact11+',z:'+volJac2[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville2&location=43&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[0]+'\'},' +
        '{y:'+avJact10+',z:'+volJac2[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville2&location=43&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[1]+'\'},' +
        '{y:'+avJact9+',z:'+volJac2[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville2&location=43&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[2]+'\'},' +
        '{y:'+avJact8+',z:'+volJac2[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville2&location=43&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[3]+'\'},' +
        '{y:'+avJact7+',z:'+volJac2[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville2&location=43&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[4]+'\'},' +
        '{y:'+avJact6+',z:'+volJac2[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville2&location=43&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[5]+'\'},' +
        '{y:'+avJact5+',z:'+volJac2[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville2&location=43&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[6]+'\'},' +
        '{y:'+avJact4+',z:'+volJac2[4]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville2&location=43&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[7]+'\'},' +
        '{y:'+avJact3+',z:'+volJac2[3]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville2&location=43&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[8]+'\'},' +
        '{y:'+avJact2+',z:'+volJac2[2]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville2&location=43&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[9]+'\'},' +
        '{y:'+avJact1+',z:'+volJac2[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville2&location=43&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[10]+'\'},' +
        '{y:'+avJact0+',z:'+volJac2[0]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Jacksonville2&location=43&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Lakeland\',type:  \'spline\',data: [' +
        '{y:'+avLak11+',z:'+volLak[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Lakeland&location=41&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[0]+'\'},' +
        '{y:'+avLak10+',z:'+volLak[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Lakeland&location=41&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[1]+'\'},' +
        '{y:'+avLak9+',z:'+volLak[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Lakeland&location=41&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[2]+'\'},' +
        '{y:'+avLak8+',z:'+volLak[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Lakeland&location=41&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[3]+'\'},' +
        '{y:'+avLak7+',z:'+volLak[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Lakeland&location=41&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[4]+'\'},' +
        '{y:'+avLak6+',z:'+volLak[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Lakeland&location=41&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[5]+'\'},' +
        '{y:'+avLak5+',z:'+volLak[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Lakeland&location=41&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[6]+'\'},' +
        '{y:'+avLak4+',z:'+volLak[4]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Lakeland&location=41&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[7]+'\'},' +
        '{y:'+avLak3+',z:'+volLak[3]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Lakeland&location=41&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[8]+'\'},' +
        '{y:'+avLak2+',z:'+volLak[2]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Lakeland&location=41&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[9]+'\'},' +
        '{y:'+avLak1+',z:'+volLak[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Lakeland&location=41&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[10]+'\'},' +
        '{y:'+avLak0+',z:'+volLak[0]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Lakeland&location=41&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Minneapolis\',type:  \'spline\',data: [' +
        '{y:'+avMin11+',z:'+volMin[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Minneapolis&location=22&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[0]+'\'},' +
        '{y:'+avMin10+',z:'+volMin[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Minneapolis&location=22&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[1]+'\'},' +
        '{y:'+avMin9+',z:'+volMin[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Minneapolis&location=22&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[2]+'\'},' +
        '{y:'+avMin8+',z:'+volMin[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Minneapolis&location=22&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[3]+'\'},' +
        '{y:'+avMin7+',z:'+volMin[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Minneapolis&location=22&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[4]+'\'},' +
        '{y:'+avMin6+',z:'+volMin[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Minneapolis&location=22&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[5]+'\'},' +
        '{y:'+avMin5+',z:'+volMin[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Minneapolis&location=22&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[6]+'\'},' +
        '{y:'+avMin4+',z:'+volMin[4]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Minneapolis&location=22&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[7]+'\'},' +
        '{y:'+avMin3+',z:'+volMin[3]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Minneapolis&location=22&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[8]+'\'},' +
        '{y:'+avMin2+',z:'+volMin[2]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Minneapolis&location=22&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[9]+'\'},' +
        '{y:'+avMin1+',z:'+volMin[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Minneapolis&location=22&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[10]+'\'},' +
        '{y:'+avMin0+',z:'+volMin[0]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Minneapolis&location=22&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Montreal\',type:  \'spline\',data: [' +
        '{y:'+avMTL11+',z:'+volMTL[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Montreal&location=25&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[0]+'\'},' +
        '{y:'+avMTL10+',z:'+volMTL[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Montreal&location=25&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[1]+'\'},' +
        '{y:'+avMTL9+',z:'+volMTL[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Montreal&location=25&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[2]+'\'},' +
        '{y:'+avMTL8+',z:'+volMTL[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Montreal&location=25&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[3]+'\'},' +
        '{y:'+avMTL7+',z:'+volMTL[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Montreal&location=25&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[4]+'\'},' +
        '{y:'+avMTL6+',z:'+volMTL[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Montreal&location=25&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[5]+'\'},' +
        '{y:'+avMTL5+',z:'+volMTL[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Montreal&location=25&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[6]+'\'},' +
        '{y:'+avMTL4+',z:'+volMTL[4]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Montreal&location=25&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[7]+'\'},' +
        '{y:'+avMTL3+',z:'+volMTL[3]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Montreal&location=25&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[8]+'\'},' +
        '{y:'+avMTL2+',z:'+volMTL[2]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Montreal&location=25&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[9]+'\'},' +
        '{y:'+avMTL1+',z:'+volMTL[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Montreal&location=25&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[10]+'\'},' +
        '{y:'+avMTL0+',z:'+volMTL[0]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Montreal&location=25&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'New Jersey\',type:  \'spline\',data: [' +
        '{y:'+avNNJ11+',z:'+volNNJ[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=New Jersey&location=52&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[0]+'\'},' +
        '{y:'+avNNJ10+',z:'+volNNJ[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=New Jersey&location=52&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[1]+'\'},' +
        '{y:'+avNNJ9+',z:'+volNNJ[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=New Jersey&location=52&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[2]+'\'},' +
        '{y:'+avNNJ8+',z:'+volNNJ[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=New Jersey&location=52&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[3]+'\'},' +
        '{y:'+avNNJ7+',z:'+volNNJ[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=New Jersey&location=52&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[4]+'\'},' +
        '{y:'+avNNJ6+',z:'+volNNJ[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=New Jersey&location=52&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[5]+'\'},' +
        '{y:'+avNNJ5+',z:'+volNNJ[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=New Jersey&location=52&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[6]+'\'},' +
        '{y:'+avNNJ4+',z:'+volNNJ[4]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=New Jersey&location=52&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[7]+'\'},' +
        '{y:'+avNNJ3+',z:'+volNNJ[3]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=New Jersey&location=52&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[8]+'\'},' +
        '{y:'+avNNJ2+',z:'+volNNJ[2]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=New Jersey&location=52&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[9]+'\'},' +
        '{y:'+avNNJ1+',z:'+volNNJ[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=New Jersey&location=52&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[10]+'\'},' +
        '{y:'+avNNJ0+',z:'+volNNJ[0]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=New Jersey&location=52&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Toronto\',type:  \'spline\',data: [' +
        '{y:'+avTOR11+',z:'+volTOR[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Toronto&location=23&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[0]+'\'},' +
        '{y:'+avTOR10+',z:'+volTOR[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Toronto&location=23&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[1]+'\'},' +
        '{y:'+avTOR9+',z:'+volTOR[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Toronto&location=23&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[2]+'\'},' +
        '{y:'+avTOR8+',z:'+volTOR[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Toronto&location=23&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[3]+'\'},' +
        '{y:'+avTOR7+',z:'+volTOR[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Toronto&location=23&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[4]+'\'},' +
        '{y:'+avTOR6+',z:'+volTOR[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Toronto&location=23&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[5]+'\'},' +
        '{y:'+avTOR5+',z:'+volTOR[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Toronto&location=23&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[6]+'\'},' +
        '{y:'+avTOR4+',z:'+volTOR[4]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Toronto&location=23&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[7]+'\'},' +
        '{y:'+avTOR3+',z:'+volTOR[3]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Toronto&location=23&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[8]+'\'},' +
        '{y:'+avTOR2+',z:'+volTOR[2]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Toronto&location=23&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[9]+'\'},' +
        '{y:'+avTOR1+',z:'+volTOR[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Toronto&location=23&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[10]+'\'},' +
        '{y:'+avTOR0+',z:'+volTOR[0]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Toronto&location=23&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Vancouver\',type:  \'spline\',data: [' +
        '{y:'+avVAN11+',z:'+volVAN[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Vancouver&location=26&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[0]+'\'},' +
        '{y:'+avVAN10+',z:'+volVAN[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Vancouver&location=26&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[1]+'\'},' +
        '{y:'+avVAN9+',z:'+volVAN[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Vancouver&location=26&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[2]+'\'},' +
        '{y:'+avVAN8+',z:'+volVAN[8]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Vancouver&location=26&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[3]+'\'},' +
        '{y:'+avVAN7+',z:'+volVAN[7]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Vancouver&location=26&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[4]+'\'},' +
        '{y:'+avVAN6+',z:'+volVAN[6]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Vancouver&location=26&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[5]+'\'},' +
        '{y:'+avVAN5+',z:'+volVAN[5]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Vancouver&location=26&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[6]+'\'},' +
        '{y:'+avVAN4+',z:'+volVAN[4]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Vancouver&location=26&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[7]+'\'},' +
        '{y:'+avVAN3+',z:'+volVAN[3]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Vancouver&location=26&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[8]+'\'},' +
        '{y:'+avVAN2+',z:'+volVAN[2]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Vancouver&location=26&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[9]+'\'},' +
        '{y:'+avVAN1+',z:'+volVAN[1]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Vancouver&location=26&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[10]+'\'},' +
        '{y:'+avVAN0+',z:'+volVAN[0]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&title=Vancouver&location=26&product='+product+'&cattype=2&week='+returnFunctionDatesGrid[11]+'\'}]}';


   seriesReturn += ",{name: 'Non-Standard',type: 'spline','visible': false, color: '#0000CC',data: [" +
        "{y:"+avST11+",z:"+volST11+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&comes=1&title=Non-Standard&location=&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[0]+"\'}," +
        "{y:"+avST10+",z:"+volST10+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&comes=1&title=Non-Standard&location=&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[1]+"\'}," +
        "{y:"+avST9+",z:"+volST9+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&comes=1&title=Non-Standard&location=&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[2]+"\'}," +
        "{y:"+avST8+",z:"+volST8+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&comes=1&title=Non-Standard&location=&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[3]+"\'}," +
        "{y:"+avST7+",z:"+volST7+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&comes=1&title=Non-Standard&location=&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[4]+"\'}," +
        "{y:"+avST6+",z:"+volST6+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&comes=1&title=Non-Standard&location=&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[5]+"\'}," +
        "{y:"+avST5+",z:"+volST5+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&comes=1&title=Non-Standard&location=&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[6]+"\'}," +
        "{y:"+avST4+",z:"+volST4+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&comes=1&title=Non-Standard&location=&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[7]+"\'}," +
        "{y:"+avST3+",z:"+volST3+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&comes=1&title=Non-Standard&location=&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[8]+"\'}," +
        "{y:"+avST2+",z:"+volST2+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&comes=1&title=Non-Standard&location=&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[9]+"\'}," +
        "{y:"+avST1+",z:"+volST1+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&comes=1&title=Non-Standard&location=&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[10]+"\'}," +
        "{y:"+avST0+",z:"+volST0+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&ndays=1&comes=1&title=Non-Standard&location=&product="+product+"&cattype=2&week="+returnFunctionDatesGrid[11]+"\'}]}";

    return seriesReturn;
}
