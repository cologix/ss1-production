nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_OPS_KPI_Installations_Products.js
//	Script Name:	CLGX_SL_OPS_KPI_Installations_Products
//	Script Id:		customscript_clgx_sl_ops_kpi_inst_prod
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		06/24/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=511&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_ops_kpi_inst_prod (request, response){
    try {

        var script = request.getParameter('script');
        var type = request.getParameter('type');
        var category = request.getParameter('category');

        var chartype = 'column';
        if(type == 'avg'){
            chartype = 'spline';
        }


        var title = request.getParameter('title');
        var location = request.getParameter('location');
        var cattype=request.getParameter('cattype');
        var product = request.getParameter('product');
        var order = request.getParameter('order');
        var titleav="Standard";
        if(cattype=='standard')
        {
            titleav="Non-Standard";
        }

        if(type=='avg')
            var objFile = nlapiLoadFile(3042679);
        else
        {
            var objFile = nlapiLoadFile(2743712);
        }
        var returnFunctionDatesGrid=return_weeksforGrid();
        var from1=addDays(returnFunctionDatesGrid[0],0);
        var to12=addDays(returnFunctionDatesGrid[11],6);
        var from1String=(from1.getMonth()+1)+'/'+from1.getDate()+'/'+from1.getFullYear();
        var to12String=(to12.getMonth()+1)+'/'+to12.getDate()+'/'+to12.getFullYear();
        // nlapiSendEmail(206211,206211,'string11tyy',from1String +','+to12String,null,null,null,null);
        var html = objFile.getValue();
        html = html.replace(new RegExp('{weeks}','g'), return_weeks());
        html = html.replace(new RegExp('{chartype}','g'), chartype);
        html = html.replace(new RegExp('{averagetitle}','g'), titleav);
        html = html.replace(new RegExp('{title}','g'), 'Installation Intervals - ' + title);
        if(location==0){
            title='ALL';
        }
        if(location==34){
            title='COL12';
        }
        if(location==16){
            title='MIN12';
        }
        if(location==33){
            title='COL';
        }
        var cattypearr=[cattype];
        if(cattype=='all')
        {
            var cattypearr=[1,2];
        }
        html = html.replace(new RegExp('{series}','g'), return_series(title,cattype,type,order));
        html = html.replace(new RegExp('{seriesCSV}','g'), return_seriesCSV(cattypearr,product,location,from1String,to12String));


        response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function return_series(title,cattype,type,order){
    var objFile = nlapiLoadFile(2886013);
    var fileValue=objFile.getValue();
    var json=JSON.parse(fileValue);
    var tl=title;
    if(cattype=='nstandard')
    {

        if(type=='vol')
        {

            var seriesString=json.series.location[1];
            var seriesInt=seriesString.nstandard[order];
            var series=return_seriesFinalVol (seriesInt,title);
        }
        else
        {
            if(type=='avg')
            {
                var seriesString=json.series.location[1];
                var seriesInt=seriesString.nstandard[order];
                var series=return_seriesFinalAvg (seriesInt,title);
            }
        }

    }
    if(cattype=='all')
    {

        if(type=='vol')
        {

            var seriesString=json.series.location[2];
            var seriesInt=seriesString.all[order];
            var series=return_seriesFinalVol (seriesInt,title);
        }
        else
        {
            if(type=='avg')
            {
                var seriesString=json.series.location[2];
                var seriesInt=seriesString.all[order];
                var series=return_seriesFinalAvg (seriesInt,title);
            }
        }

    }
    if(cattype=='standard')
    {
        if(type=='vol')
        {

            var seriesString=json.series.location[0];
            var seriesInt=seriesString.standard[order];
            var series=return_seriesFinalVol (seriesInt,title);
        }
        else
        {
            if(type=='avg')
            {
                var seriesString=json.series.location[0];
                var seriesInt=seriesString.standard[order];
                var series=return_seriesFinalAvg (seriesInt,title);
            }
        }
    }
    if(type=='vol')
    {
        var ind=0;
    }
    if(type=='avg')
    {
        var ind=1;
    }


    return series;
}
function return_seriesCSV(cattype,product,location,from1String,to12String){
    var series='';
    var arrFilters = new Array();
    var arrColumns = new Array();
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from1String,to12String));
    //  if(product.length>0)
    // {
    // arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category","item","anyof",product));
    // }
    var  catitem="all";
    if(product==10){
        var  catitem="Space";
    }
    if(product==8){
        var catitem="Power";
    }
    if(product==5){
        var catitem="Network";
    }
    if(product==4){
        var catitem="Interconnection";
    }
    if(product==11){
        var  catitem="Virtual Interconnection";
    }

    if(location!=0)
    {
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",location));
    }
    //  if(cattype.length>0)
    // {
    //     arrFilters.push(new nlobjSearchFilter("custitem_clgx_custitem_standard","item","anyof",cattype));
    //  }
//[standard=1,nstandard=2]
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_so_install_vol_loc_s_3', arrFilters, arrColumns);
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var customer = searchIn.getText(columns[0]);
        var so = searchIn.getValue(columns[1]);
        var soid = searchIn.getValue(columns[2]);
        var assi = searchIn.getText(columns[3]);
        var totalmmr = searchIn.getValue(columns[4]);
        var totalnrc = searchIn.getValue(columns[5]);
        var loc = searchIn.getValue(columns[6]);
        var actualinstd = searchIn.getValue(columns[7]);
        var nStandard = searchIn.getValue(columns[8]);
        var standard = searchIn.getValue(columns[9]);
        var itemcat=searchIn.getValue(columns[10]);
        var installdur=searchIn.getValue(columns[11]);
        if(installdur>1)
        {
            var installdurst=searchIn.getValue(columns[11])+' days';
        }
        else
        {
            var installdurst=searchIn.getValue(columns[11])+' day';
        }
        var itemcatArr=itemcat.split(',');
        if((in_array(catitem,itemcatArr))&&(catitem!='all'))
        {
            if(cattype.length==1)
            {
                if((cattype[0]==1)&&(nStandard==0))
                {
                    series += '\n{"Customer":"' + customer +
                        '","SO#":"' + so+
                        '","Installation Duration":"' + installdurst+
                        '","Assigned Technician":"' + assi+
                        '","Total MMR":' +  totalmmr +
                        ',"Total NRC":' + totalnrc +
                        ',"Location":"' + loc +
                        '","Actual Install Date":"' + actualinstd +
                        '"},';
                }
                if((cattype[0]==2)&&(((nStandard>0)&&(standard>0))||(nStandard>0)))
                {
                    series += '\n{"Customer":"' + customer +
                        '","SO#":"' + so+
                        '","Installation Duration":"' + installdurst+
                        '","Assigned Technician":"' + assi+
                        '","Total MMR":' +  totalmmr +
                        ',"Total NRC":' + totalnrc +
                        ',"Location":"' + loc +
                        '","Actual Install Date":"' + actualinstd +
                        '"},';
                }
            }
            else if(cattype.length>1){
                series += '\n{"Customer":"' + customer +
                    '","SO#":"' + so+
                    '","Installation Duration":"' + installdurst+
                    '","Assigned Technician":"' + assi+
                    '","Total MMR":' +  totalmmr +
                    ',"Total NRC":' + totalnrc +
                    ',"Location":"' + loc +
                    '","Actual Install Date":"' + actualinstd +
                    '"},';
            }

        }

        else if(catitem=='all'){
            if(cattype.length==1)
            {
                if((cattype[0]==1)&&(nStandard==0))
                {
                    series += '\n{"Customer":"' + customer +
                        '","SO#":"' + so+
                        '","Installation Duration":"' + installdurst+
                        '","Assigned Technician":"' + assi+
                        '","Total MMR":' +  totalmmr +
                        ',"Total NRC":' + totalnrc +
                        ',"Location":"' + loc +
                        '","Actual Install Date":"' + actualinstd +
                        '"},';
                }
                if((cattype[0]==2)&&(((nStandard>0)&&(standard>0))||(nStandard>0)))
                {
                    series += '\n{"Customer":"' + customer +
                        '","SO#":"' + so+
                        '","Installation Duration":"' + installdurst+
                        '","Assigned Technician":"' + assi+
                        '","Total MMR":' +  totalmmr +
                        ',"Total NRC":' + totalnrc +
                        ',"Location":"' + loc +
                        '","Actual Install Date":"' + actualinstd +
                        '"},';
                }
            }
            else if(cattype.length>1){
                series += '\n{"Customer":"' + customer +
                    '","SO#":"' + so+
                    '","Installation Duration":"' + installdurst+
                    '","Assigned Technician":"' + assi+
                    '","Total MMR":' +  totalmmr +
                    ',"Total NRC":' + totalnrc +
                    ',"Location":"' + loc +
                    '","Actual Install Date":"' + actualinstd +
                    '"},';
            }

        }
    }

    var strLen = series.length;
    if (searchIns != null){
        series = series.slice(0,strLen-1);
    }
    var seriesCSV='{Customer:"Ionescu"}';
    return series;
}
function return_seriesFinalVol (json,title){
    var series='';
    switch (title) {
        case 'ALL':
            series=json.ALL[0];
            break;
        case 'COL':
            series=json.COL[0];
            break;
        case 'COL12':
            series=json.COL12[0];
            break;
        case 'COL3':
            series=json.COL3[0];
            break;
        case 'COL3':
            series=json.COL3[0];
            break;
        case 'Dallas':
            series=json.Dallas[0];
            break;
        case 'DAL1':
            series=json.DAL1[0];
            break;
        case 'DAL2':
            series=json.DAL2[0];
            break;
        case 'Jacksonville':
            series=json.Jacksonville[0];
            break;
        case 'Jacksonville2':
            series=json.Jacksonville2[0];
            break;
        case 'JAX1':
            series=json.JAX1[0];
            break;
        case 'JAX2':
            series=json.JAX2[0];
            break;
        case 'Lakeland':
            series=json.Lakeland[0];
            break;
        case 'LAK1':
            series=json.LAK1[0];
            break;
        case 'Minneapolis':
            series=json.Minneapolis[0];
            break;
        case 'MIN12':
            series=json.MIN12[0];
            break;
        case 'MIN3':
            series=json.MIN3[0];
            break;
        case 'Montreal':
            series=json.Montreal[0];
            break;
        case 'MTL1':
            series=json.MTL1[0];
            break;
        case 'MTL2':
            series=json.MTL2[0];
            break;
        case 'MTL3':
            series=json.MTL3[0];
            break;
        case 'MTL4':
            series=json.MTL4[0];
            break;
        case 'MTL5':
            series=json.MTL5[0];
            break;
        case 'MTL6':
            series=json.MTL6[0];
            break;
        case 'MTL7':
            series=json.MTL7[0];
            break;
        case 'New Jersey':
            series=json.NewJersey[0];
            break;
        case 'NNJ1':
            series=json.NNJ1[0];
            break;
        case 'NNJ2':
            series=json.NNJ2[0];
            break;
        case 'NNJ3':
            series=json.NNJ3[0];
            break;
        case 'NNJ4':
            series=json.NNJ4[0];
            break;
        case 'Toronto':
            series=json.Toronto[0];
            break;
        case 'TOR1':
            series=json.TOR1[0];
            break;
        case 'TOR2':
            series=json.TOR2[0];
            break;
        case 'Vancouver':
            series=json.Vancouver[0];
            break;
        case 'VAN1':
            series=json.VAN1[0];
            break;
        case 'VAN2':
            series=json.VAN2[0];
            break;
    }
    return series;

}
function return_seriesFinalAvg (json,title){
    var series='';
    switch (title) {
        case 'ALL':
            series=json.ALL[1];
            break;
        case 'COL':
            series=json.COL[1];
            break;
        case 'COL12':
            series=json.COL12[1];
            break;
        case 'COL3':
            series=json.COL3[1];
            break;
        case 'COL3':
            series=json.COL3[1];
            break;
        case 'Dallas':
            series=json.Dallas[1];
            break;
        case 'DAL1':
            series=json.DAL1[1];
            break;
        case 'DAL2':
            series=json.DAL2[1];
            break;
        case 'Jacksonville':
            series=json.Jacksonville[1];
            break;
        case 'Jacksonville2':
            series=json.Jacksonville2[1];
            break;
        case 'JAX1':
            series=json.JAX1[1];
            break;
        case 'JAX2':
            series=json.JAX2[1];
            break;
        case 'Lakeland':
            series=json.Lakeland[1];
            break;
        case 'LAK1':
            series=json.LAK1[1];
            break;
        case 'Minneapolis':
            series=json.Minneapolis[1];
            break;
        case 'MIN12':
            series=json.MIN12[1];
            break;
        case 'MIN3':
            series=json.MIN3[1];
            break;
        case 'Montreal':
            series=json.Montreal[1];
            break;
        case 'MTL1':
            series=json.MTL1[1];
            break;
        case 'MTL2':
            series=json.MTL2[1];
            break;
        case 'MTL3':
            series=json.MTL3[1];
            break;
        case 'MTL4':
            series=json.MTL4[1];
            break;
        case 'MTL5':
            series=json.MTL5[1];
            break;
        case 'MTL6':
            series=json.MTL6[1];
            break;
        case 'MTL7':
            series=json.MTL7[1];
            break;
        case 'New Jersey':
            series=json.NewJersey[1];
            break;
        case 'NNJ1':
            series=json.NNJ1[1];
            break;
        case 'NNJ2':
            series=json.NNJ2[1];
            break;
        case 'NNJ3':
            series=json.NNJ3[1];
            break;
        case 'NNJ4':
            series=json.NNJ4[1];
            break;
        case 'Toronto':
            series=json.Toronto[1];
            break;
        case 'TOR1':
            series=json.TOR1[1];
            break;
        case 'TOR2':
            series=json.TOR2[1];
            break;
        case 'Vancouver':
            series=json.Vancouver[1];
            break;
        case 'VAN1':
            series=json.VAN1[1];
            break;
        case 'VAN2':
            series=json.VAN2[1];
            break;
    }

    return series;
}

function return_weeks (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 13; i++ ) {
        arrWeeks.push(moment().endOf('week').add(1, 'days').subtract((13 - i), 'week').format('M/D'));
    }
    return JSON.stringify(arrWeeks);
}
function return_weeksforGrid (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 13; i++ ) {
        arrWeeks.push(moment().startOf('week').add(1, 'days').subtract((13 - i), 'week').format('MM/DD/YYYY'));
    }
    return arrWeeks;
}
function addDays(theDate, days) {
    theDate=new Date(theDate);
    return new Date(theDate.getTime() + days*24*60*60*1000);
}
function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}