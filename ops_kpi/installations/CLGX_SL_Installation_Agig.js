//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_OPS_KPI_Installations_Products.js
//	Script Name:	CLGX_SL_OPS_KPI_Installations_Products
//	Script Id:		customscript_clgx_sl_ops_kpi_inst_prod
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		06/24/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=511&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_ops_kpi_inst_prod (request, response){
    try {

        var script = request.getParameter('script');
        var type = request.getParameter('type');
        var category = request.getParameter('category');
        var chartype = 'spline';
        var title = request.getParameter('title');
        var title1 = request.getParameter('title');
        var location = request.getParameter('location');
        var cattype=request.getParameter('cattype');
        var product = request.getParameter('product');
        var order = request.getParameter('order');
        order=parseInt(order)-parseInt(1);
        var titleav="Standard";
        if(cattype=='standard')
        {
            titleav="Non-Standard";
        }
        if(product=='space')
        {
            var file='3040304';
        }
        //var objFile = nlapiLoadFile(3037706);
        var objFile = nlapiLoadFile(3065136);
        var html = objFile.getValue();
        html = html.replace(new RegExp('{weeks}','g'), return_weeks());
        html = html.replace(new RegExp('{chartype}','g'), chartype);
        html = html.replace(new RegExp('{averagetitle}','g'), titleav);
        html = html.replace(new RegExp('{title}','g'), 'Installation Intervals - ' + title);
        if(location==0){
            title='ALL';
        }
        if(location==34){
            title='COL12';
        }
        if(location==216){
            title='MIN12';
        }
        if(location==33){
            title='COL';
        }
        var filevxc="3040821";
        var filexc="3042142";
        var filepow="3042145";
        var filenet="3042146";
        //
        var filens="3042254";
        var filevxcns="3042255";
        var filexcns="3042249";
        var filepowns="3042256";
        var filenetns="3042257";
        var fileall="2906967";
        var plot5=0;
        var plot2=0;
        var seriesWeek= getAvgSeriesPerWeek(title1,location,cattype,"xc");
        var seriesAgSpace=",{name: 'Open Aging Average',type: 'spline',color: '#008000',data: [" +
            "{y:"+seriesWeek[11]+",z:"+seriesWeek[23]+",url:'#'}," +
            "{y:"+seriesWeek[10]+",z:"+seriesWeek[22]+",url:'#'}," +
            "{y:"+seriesWeek[9]+",z:"+seriesWeek[21]+",url:'#'}," +
            "{y:"+seriesWeek[8]+",z:"+seriesWeek[20]+",url:'#'}," +
            "{y:"+seriesWeek[7]+",z:"+seriesWeek[19]+",url:'#'}," +
            "{y:"+seriesWeek[6]+",z:"+seriesWeek[18]+",url:'#'}," +
            "{y:"+seriesWeek[5]+",z:"+seriesWeek[17]+",url:'#'}," +
            "{y:"+seriesWeek[4]+",z:"+seriesWeek[16]+",url:'#'}," +
            "{y:"+seriesWeek[3]+",z:"+seriesWeek[15]+",url:'#'}," +
            "{y:"+seriesWeek[2]+",z:"+seriesWeek[14]+",url:'#'}," +
            "{y:"+seriesWeek[1]+",z:"+seriesWeek[13]+",url:'#'}," +
            "{y:"+seriesWeek[0]+",z:"+seriesWeek[12]+",url:'#'}]}";
        var seriesAgXC=",{name: 'Open Aging Average',type: 'spline',color: '#008000',data: [" +
            "{y:"+seriesWeek[35]+",z:"+seriesWeek[47]+",url:'#'}," +
            "{y:"+seriesWeek[34]+",z:"+seriesWeek[46]+",url:'#'}," +
            "{y:"+seriesWeek[33]+",z:"+seriesWeek[45]+",url:'#'}," +
            "{y:"+seriesWeek[32]+",z:"+seriesWeek[44]+",url:'#'}," +
            "{y:"+seriesWeek[31]+",z:"+seriesWeek[43]+",url:'#'}," +
            "{y:"+seriesWeek[30]+",z:"+seriesWeek[42]+",url:'#'}," +
            "{y:"+seriesWeek[29]+",z:"+seriesWeek[41]+",url:'#'}," +
            "{y:"+seriesWeek[28]+",z:"+seriesWeek[40]+",url:'#'}," +
            "{y:"+seriesWeek[27]+",z:"+seriesWeek[39]+",url:'#'}," +
            "{y:"+seriesWeek[26]+",z:"+seriesWeek[38]+",url:'#'}," +
            "{y:"+seriesWeek[25]+",z:"+seriesWeek[37]+",url:'#'}," +
            "{y:"+seriesWeek[24]+",z:"+seriesWeek[36]+",url:'#'}]}";
        var seriesAgVXC=",{name: 'Open Aging Average',type: 'spline',color: '#008000',data: [" +
            "{y:"+seriesWeek[59]+",z:"+seriesWeek[71]+",url:'#'}," +
            "{y:"+seriesWeek[58]+",z:"+seriesWeek[70]+",url:'#'}," +
            "{y:"+seriesWeek[57]+",z:"+seriesWeek[69]+",url:'#'}," +
            "{y:"+seriesWeek[56]+",z:"+seriesWeek[68]+",url:'#'}," +
            "{y:"+seriesWeek[55]+",z:"+seriesWeek[67]+",url:'#'}," +
            "{y:"+seriesWeek[54]+",z:"+seriesWeek[66]+",url:'#'}," +
            "{y:"+seriesWeek[53]+",z:"+seriesWeek[65]+",url:'#'}," +
            "{y:"+seriesWeek[52]+",z:"+seriesWeek[64]+",url:'#'}," +
            "{y:"+seriesWeek[51]+",z:"+seriesWeek[63]+",url:'#'}," +
            "{y:"+seriesWeek[50]+",z:"+seriesWeek[62]+",url:'#'}," +
            "{y:"+seriesWeek[49]+",z:"+seriesWeek[61]+",url:'#'}," +
            "{y:"+seriesWeek[48]+",z:"+seriesWeek[60]+",url:'#'}]}";
        var seriesAgPOW=",{name: 'Open Aging Average',type: 'spline',color: '#008000',data: [" +
            "{y:"+seriesWeek[83]+",z:"+seriesWeek[95]+",url:'#'}," +
            "{y:"+seriesWeek[82]+",z:"+seriesWeek[94]+",url:'#'}," +
            "{y:"+seriesWeek[81]+",z:"+seriesWeek[93]+",url:'#'}," +
            "{y:"+seriesWeek[80]+",z:"+seriesWeek[92]+",url:'#'}," +
            "{y:"+seriesWeek[79]+",z:"+seriesWeek[91]+",url:'#'}," +
            "{y:"+seriesWeek[78]+",z:"+seriesWeek[90]+",url:'#'}," +
            "{y:"+seriesWeek[77]+",z:"+seriesWeek[89]+",url:'#'}," +
            "{y:"+seriesWeek[76]+",z:"+seriesWeek[88]+",url:'#'}," +
            "{y:"+seriesWeek[75]+",z:"+seriesWeek[87]+",url:'#'}," +
            "{y:"+seriesWeek[74]+",z:"+seriesWeek[86]+",url:'#'}," +
            "{y:"+seriesWeek[73]+",z:"+seriesWeek[85]+",url:'#'}," +
            "{y:"+seriesWeek[72]+",z:"+seriesWeek[84]+",url:'#'}]}";
        var seriesAgNET=",{name: 'Open Aging Average',type: 'spline',color: '#008000',data: [" +
            "{y:"+seriesWeek[107]+",z:"+seriesWeek[119]+",url:'#'}," +
            "{y:"+seriesWeek[106]+",z:"+seriesWeek[118]+",url:'#'}," +
            "{y:"+seriesWeek[105]+",z:"+seriesWeek[117]+",url:'#'}," +
            "{y:"+seriesWeek[104]+",z:"+seriesWeek[116]+",url:'#'}," +
            "{y:"+seriesWeek[103]+",z:"+seriesWeek[115]+",url:'#'}," +
            "{y:"+seriesWeek[102]+",z:"+seriesWeek[114]+",url:'#'}," +
            "{y:"+seriesWeek[101]+",z:"+seriesWeek[113]+",url:'#'}," +
            "{y:"+seriesWeek[100]+",z:"+seriesWeek[112]+",url:'#'}," +
            "{y:"+seriesWeek[99]+",z:"+seriesWeek[111]+",url:'#'}," +
            "{y:"+seriesWeek[98]+",z:"+seriesWeek[110]+",url:'#'}," +
            "{y:"+seriesWeek[97]+",z:"+seriesWeek[109]+",url:'#'}," +
            "{y:"+seriesWeek[96]+",z:"+seriesWeek[108]+",url:'#'}]}";
        if(cattype=='standard')
        {
            var plot5=5;
            var plot2=2;
            var spaceSeries= return_series(title,order,file);
            var xcSeries=return_series(title,order,filexc);
            // var strLen = spaceSeries.length;

            //spaceSeries = spaceSeries.slice(0,strLen-1);


            var vxcSeries= return_series(title,order,filevxc);

            var powSeries= return_series(title,order,filepow);


            var netSeries= return_series(title,order,filenet);






            html = html.replace(new RegExp('{spaceseries}','g'), spaceSeries+seriesAgSpace);
            html = html.replace(new RegExp('{vxcseries}','g'), vxcSeries+seriesAgVXC);
            html = html.replace(new RegExp('{xcseries}','g'), xcSeries+seriesAgXC);
            html = html.replace(new RegExp('{powseries}','g'), powSeries+seriesAgPOW);
            html = html.replace(new RegExp('{netseries}','g'), netSeries+seriesAgNET);
            html = html.replace(new RegExp('{plot5}','g'), plot5);
            html = html.replace(new RegExp('{plot2}','g'), plot2);
        }
        else  if(cattype=='nstandard')
        {
            var spaceSeries= return_series(title,order,filens);
            var xcSeries=return_series(title,order,filexcns);
            var vxcSeries=  return_series(title,order,filevxcns);
            var powSeries= return_series(title,order,filepowns);
            var netSeries= return_series(title,order,filenetns);

            html = html.replace(new RegExp('{spaceseries}','g'), spaceSeries+seriesAgSpace);
            html = html.replace(new RegExp('{vxcseries}','g'), vxcSeries+seriesAgVXC);
            html = html.replace(new RegExp('{xcseries}','g'), xcSeries+seriesAgXC);
            html = html.replace(new RegExp('{powseries}','g'), powSeries+seriesAgPOW);
            html = html.replace(new RegExp('{netseries}','g'), netSeries+seriesAgNET);
            html = html.replace(new RegExp('{plot5}','g'), plot5);
            html = html.replace(new RegExp('{plot2}','g'), plot2);
        } else  if(cattype=='all'){
            var fileall="3113071";
            var objFile = nlapiLoadFile(fileall);
            var fileValue=objFile.getValue();
            var json1=JSON.parse(fileValue);
            var sp=json1.ALL[0];
            var vxc=json1.ALL[1];
            var xc=json1.ALL[2];
            var net=json1.ALL[3];
            var pow=json1.ALL[4];
            html = html.replace(new RegExp('{spaceseries}','g'), sp+seriesAgSpace);
            html = html.replace(new RegExp('{vxcseries}','g'), vxc+seriesAgVXC);
            html = html.replace(new RegExp('{xcseries}','g'), xc+seriesAgXC);
            html = html.replace(new RegExp('{powseries}','g'), pow+seriesAgPOW);
            html = html.replace(new RegExp('{netseries}','g'), net+seriesAgNET);
            html = html.replace(new RegExp('{plot5}','g'), plot5);
            html = html.replace(new RegExp('{plot2}','g'), plot2);

        }


        response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function return_series(title,order,file){
    var objFile = nlapiLoadFile(file);
    var fileValue=objFile.getValue();
    var json1=JSON.parse(fileValue);
    order=parseInt(order)+parseInt(1);
    var json=json1[order];
    var series=return_seriesFinalAvg (json,title);
    return series;
}
function return_seriesFinalAvg (json,title){
    var series='';
    switch (title) {
        case 'ALL':
            series=json.ALL[0];
            break;
        case 'COL':
            series=json.COL[0];
            break;
        case 'COL12':
            series=json.COL12[0];
            break;
        case 'COL3':
            series=json.COL3[0];
            break;
        case 'COL3':
            series=json.COL3[0];
            break;
        case 'Dallas':
            series=json.Dallas[0];
            break;
        case 'DAL1':
            series=json.DAL1[0];
            break;
        case 'DAL2':
            series=json.DAL2[0];
            break;
        case 'Jacksonville':
            series=json.Jacksonville[0];
            break;
        case 'Jacksonville2':
            series=json.Jacksonville2[0];
            break;
        case 'JAX1':
            series=json.JAX1[0];
            break;
        case 'JAX2':
            series=json.JAX2[0];
            break;
        case 'Lakeland':
            series=json.Lakeland[0];
            break;
        case 'LAK1':
            series=json.LAK1[0];
            break;
        case 'Minneapolis':
            series=json.Minneapolis[0];
            break;
        case 'MIN12':
            series=json.MIN12[0];
            break;
        case 'MIN3':
            series=json.MIN3[0];
            break;
        case 'Montreal':
            series=json.Montreal[0];
            break;
        case 'MTL1':
            series=json.MTL1[0];
            break;
        case 'MTL2':
            series=json.MTL2[0];
            break;
        case 'MTL3':
            series=json.MTL3[0];
            break;
        case 'MTL4':
            series=json.MTL4[0];
            break;
        case 'MTL5':
            series=json.MTL5[0];
            break;
        case 'MTL6':
            series=json.MTL6[0];
            break;
        case 'MTL7':
            series=json.MTL7[0];
            break;
        case 'Toronto':
            series=json.Toronto[0];
            break;
        case 'TOR1':
            series=json.TOR1[0];
            break;
        case 'TOR2':
            series=json.TOR2[0];
            break;
        case 'Vancouver':
            series=json.Vancouver[0];
            break;
        case 'VAN1':
            series=json.VAN1[0];
            break;
        case 'VAN2':
            series=json.VAN2[0];
            break;
    }
    return series;

}
function getAvgSeriesPerWeek(title1,location,cattype,product) {
    var arrFilters = new Array();
    var arrColumns = new Array();
    var returnWeeks=return_weeksforSearch();
    /* arrFilters.push(new nlobjSearchFilter("saleseffectivedate", null, "within", returnWeeks[46], returnWeeks[1]));
     var sosNS = new Array();
     var searchInsIDS = nlapiSearchRecord('transaction', 'customsearch_clgx_so_install_vol_loc__12', arrFilters, arrColumns);
     for (var i = 0; searchInsIDS != null && i < searchInsIDS.length; i++) {
     var searchIn = searchInsIDS[i];
     var columns = searchIn.getAllColumns();
     var id = searchIn.getValue(columns[0]);
     sosNS.push(id);
     }*/
    var arrFilters1 = new Array();
    var arrColumns1 = new Array();
    var cat=0;
    /*  if(product=="space"){
     cat=10;
     }
     if(product=="power"){
     cat=8;
     }
     if(product=="xc"){
     cat=4;
     }
     if(product=="vxc"){
     cat=11;
     }
     if(product=="network"){
     cat=5;
     }*/
    cat=4;
    var productCat=cat;

    //  if(cat!=0) {
    //     arrFilters1.push(new nlobjSearchFilter("custitem_cologix_item_category", "item", "anyof", cat));
    // }
    if(location>1)
    {
        var locationarr=clgx_return_child_locations_of_marketid (location);
        if(locationarr.length==0)
        {
            locationarr.push(location);
        }
        arrFilters1.push(new nlobjSearchFilter("location", null, "anyof", locationarr));


        //     arrFilters1[1] = new nlobjSearchFilter('formulanumeric', null, 'lessthan', 0);
        //    arrFilters1[1].setFormula("CASE WHEN {item.custitem_cologix_item_category} = 'Space' THEN -1000 ELSE 1 END");
    }
    // arrFilters1[0] = new nlobjSearchFilter('formulanumeric', null, 'lessthan', 0);
    // arrFilters1[0].setFormula("CASE WHEN {item.custitem_cologix_item_category} = 'Space' THEN -1000 ELSE 1 END");
    //space
    var agging1=[0,0];
    var agging2=[0,0];
    var agging3=[0,0];
    var agging4=[0,0];
    var agging5=[0,0];
    var agging6=[0,0];
    var agging7=[0,0];
    var agging8=[0,0];
    var agging9=[0,0];
    var agging10=[0,0];
    var agging11=[0,0];
    var agging12=[0,0];
    //xc
    var agging1xc=[0,0];
    var agging2xc=[0,0];
    var agging3xc=[0,0];
    var agging4xc=[0,0];
    var agging5xc=[0,0];
    var agging6xc=[0,0];
    var agging7xc=[0,0];
    var agging8xc=[0,0];
    var agging9xc=[0,0];
    var agging10xc=[0,0];
    var agging11xc=[0,0];
    var agging12xc=[0,0];
    //power
    var agging1pow=[0,0];
    var agging2pow=[0,0];
    var agging3pow=[0,0];
    var agging4pow=[0,0];
    var agging5pow=[0,0];
    var agging6pow=[0,0];
    var agging7pow=[0,0];
    var agging8pow=[0,0];
    var agging9pow=[0,0];
    var agging10pow=[0,0];
    var agging11pow=[0,0];
    var agging12pow=[0,0];
    //vxc
    var agging1vxc=[0,0];
    var agging2vxc=[0,0];
    var agging3vxc=[0,0];
    var agging4vxc=[0,0];
    var agging5vxc=[0,0];
    var agging6vxc=[0,0];
    var agging7vxc=[0,0];
    var agging8vxc=[0,0];
    var agging9vxc=[0,0];
    var agging10vxc=[0,0];
    var agging11vxc=[0,0];
    var agging12vxc=[0,0];
    //network
    var agging1net=[0,0];
    var agging2net=[0,0];
    var agging3net=[0,0];
    var agging4net=[0,0];
    var agging5net=[0,0];
    var agging6net=[0,0];
    var agging7net=[0,0];
    var agging8net=[0,0];
    var agging9net=[0,0];
    var agging10net=[0,0];
    var agging11net=[0,0];
    var agging12net=[0,0];
    var searchIns1 = nlapiSearchRecord('transaction', 'customsearch2787', arrFilters1, arrColumns1);
    for ( var i = 0; searchIns1 != null && i < searchIns1.length; i++ ) {
        var searchIn = searchIns1[i];
        var columns = searchIn.getAllColumns();
        //  var id = searchIn.getValue(columns[0]);
        var weeks=[];
        var weeks1=[];
        var weeks2=[];
        var weeks3=[];
        var weeks4=[];
        var weeks5=[];
        var weeks6=[];
        var weeks7=[];
        var weeks8=[];
        var weeks9=[];
        var weeks10=[];
        var weeks11=[];
        var nonstandard=searchIn.getValue(columns[5]);
        var spacePr=searchIn.getValue(columns[6]);
        var powerPr=searchIn.getValue(columns[7]);
        var xcPr=searchIn.getValue(columns[8]);
        var netPr=searchIn.getValue(columns[9]);
        var vxcPr=searchIn.getValue(columns[10]);

        if(searchIn.getValue(columns[11])!='') {
            weeks.push(searchIn.getValue(columns[11]));
        }
        else{
            weeks.push(-1);
        }
        if(searchIn.getValue(columns[12])!='') {
            weeks.push(searchIn.getValue(columns[12]));
        }
        else{
            weeks.push(-1);
        }
        if(searchIn.getValue(columns[13])!='') {
            weeks.push(searchIn.getValue(columns[13]));
        }
        else{
            weeks.push(-1);
        }
        if(searchIn.getValue(columns[14])!='') {
            weeks.push(searchIn.getValue(columns[14]));
        }
        else{
            weeks.push(-1);
        }
        if(searchIn.getValue(columns[15])!='') {
            weeks.push(searchIn.getValue(columns[15]));
        }
        else{
            weeks.push(-1);
        }
        if(searchIn.getValue(columns[16])!='') {
            weeks.push(searchIn.getValue(columns[16]));
        }
        else{
            weeks.push(-1);
        }
        if(searchIn.getValue(columns[17])!='') {
            weeks.push(searchIn.getValue(columns[17]));
        }
        else{
            weeks.push(-1);
        }
        if(searchIn.getValue(columns[18])!='') {
            weeks.push(searchIn.getValue(columns[18]));
        }
        else{
            weeks.push(-1);
        }
        if(searchIn.getValue(columns[19])!='') {
            weeks.push(searchIn.getValue(columns[19]));
        }
        else{
            weeks.push(-1);
        }
        if(searchIn.getValue(columns[20])!='') {
            weeks.push(searchIn.getValue(columns[20]));
        }
        else{
            weeks.push(-1);
        }
        if(searchIn.getValue(columns[21])!='') {
            weeks.push(searchIn.getValue(columns[21]));
        }
        else{
            weeks.push(-1);
        }
        if(searchIn.getValue(columns[22])!='') {
            weeks.push(searchIn.getValue(columns[22]));
        }
        else{
            weeks.push(-1);
        }
        if(searchIn.getValue(columns[23])!='') {
            weeks.push(searchIn.getValue(columns[23]));
        }
        else{
            weeks.push(-1);
        }




        weeks1=_.rest(weeks, 1);
        if(searchIn.getValue(columns[24])!='') {
            weeks1.push(searchIn.getValue(columns[24]));
        }
        else{
            weeks1.push(-1);
        }
        weeks2=_.rest(weeks1, 1);
        if(searchIn.getValue(columns[25])!='') {
            weeks2.push(searchIn.getValue(columns[25]));
        }
        else{
            weeks2.push(-1);
        }
        weeks3=_.rest(weeks2, 1);
        if(searchIn.getValue(columns[26])!='') {
            weeks3.push(searchIn.getValue(columns[26]));
        }
        else{
            weeks3.push(-1);
        }
        weeks4=_.rest(weeks3, 1);
        if(searchIn.getValue(columns[27])!='') {
            weeks4.push(searchIn.getValue(columns[27]));
        }
        else{
            weeks4.push(-1);
        }
        weeks5=_.rest(weeks4, 1);
        if(searchIn.getValue(columns[28])!='') {
            weeks5.push(searchIn.getValue(columns[28]));
        }
        else{
            weeks5.push(-1);
        }
        weeks6=_.rest(weeks5, 1);
        if(searchIn.getValue(columns[29])!='') {
            weeks6.push(searchIn.getValue(columns[29]));
        }
        else{
            weeks6.push(-1);
        }
        weeks7=_.rest(weeks6, 1);
        if(searchIn.getValue(columns[30])!='') {
            weeks7.push(searchIn.getValue(columns[30]));
        }
        else{
            weeks7.push(-1);
        }
        weeks8=_.rest(weeks7, 1);
        if(searchIn.getValue(columns[31])!='') {
            weeks8.push(searchIn.getValue(columns[31]));
        }
        else{
            weeks8.push(-1);
        }
        weeks9=_.rest(weeks8, 1);
        if(searchIn.getValue(columns[32])!='') {
            weeks9.push(searchIn.getValue(columns[32]));
        }
        else{
            weeks9.push(-1);
        }
        weeks10=_.rest(weeks9, 1);
        if(searchIn.getValue(columns[33])!='') {
            weeks10.push(searchIn.getValue(columns[33]));
        }
        else{
            weeks10.push(-1);
        }
        weeks11=_.rest(weeks10, 1);
        if(searchIn.getValue(columns[34])!='') {
            weeks11.push(searchIn.getValue(columns[34]));
        }
        else{
            weeks11.push(-1);
        }



        weeks=_.sortBy(weeks, function(num){
            return num * -1;
        });
        weeks1=_.sortBy(weeks1, function(num){
            return num * -1;
        });
        weeks2=_.sortBy(weeks2, function(num){
            return num * -1;
        });
        weeks3=_.sortBy(weeks3, function(num){
            return num * -1;
        });
        weeks4=_.sortBy(weeks4, function(num){
            return num * -1;
        });
        weeks5=_.sortBy(weeks5, function(num){
            return num * -1;
        });
        weeks6=_.sortBy(weeks6, function(num){
            return num * -1;
        });
        weeks7=_.sortBy(weeks7, function(num){
            return num * -1;
        });
        weeks8=_.sortBy(weeks8, function(num){
            return num * -1;
        });
        weeks9=_.sortBy(weeks9, function(num){
            return num * -1;
        });
        weeks10=_.sortBy(weeks10, function(num){
            return num * -1;
        });
        weeks11=_.sortBy(weeks11, function(num){
            return num * -1;
        });





        if(cattype=='standard')
        {
            if(nonstandard>0) {
                if (spacePr< 0) {
                    //space
                    if (weeks[0] > 0) {
                        agging1[0] = parseFloat(agging1[0]) + parseFloat(weeks[0]);
                        agging1[1] = parseFloat(agging1[1]) + parseFloat(1);
                    }
                    if (weeks1[0] > 0) {
                        agging2[0] = parseFloat(agging2[0]) + parseFloat(weeks1[0]);
                        agging2[1] = parseFloat(agging2[1]) + parseFloat(1);
                    }
                    if (weeks2[0] > 0) {
                        agging3[0] = parseFloat(agging3[0]) + parseFloat(weeks2[0]);
                        agging3[1] = parseFloat(agging3[1]) + parseFloat(1);
                    }
                    if (weeks3[0] > 0) {
                        agging4[0] = parseFloat(agging4[0]) + parseFloat(weeks3[0]);
                        agging4[1] = parseFloat(agging4[1]) + parseFloat(1);
                    }
                    if (weeks4[0] > 0) {
                        agging5[0] = parseFloat(agging5[0]) + parseFloat(weeks4[0]);
                        agging5[1] = parseFloat(agging5[1]) + parseFloat(1);
                    }
                    if (weeks5[0] > 0) {
                        agging6[0] = parseFloat(agging6[0]) + parseFloat(weeks5[0]);
                        agging6[1] = parseFloat(agging6[1]) + parseFloat(1);
                    }
                    if (weeks6[0] > 0) {
                        agging7[0] = parseFloat(agging7[0]) + parseFloat(weeks6[0]);
                        agging7[1] = parseFloat(agging7[1]) + parseFloat(1);
                    }
                    if (weeks7[0] > 0) {
                        agging8[0] = parseFloat(agging8[0]) + parseFloat(weeks7[0]);
                        agging8[1] = parseFloat(agging8[1]) + parseFloat(1);
                    }
                    if (weeks8[0] > 0) {
                        agging9[0] = parseFloat(agging9[0]) + parseFloat(weeks8[0]);
                        agging9[1] = parseFloat(agging9[1]) + parseFloat(1);
                    }
                    if (weeks9[0] > 0) {
                        agging10[0] = parseFloat(agging10[0]) + parseFloat(weeks9[0]);
                        agging10[1] = parseFloat(agging10[1]) + parseFloat(1);
                    }
                    if (weeks10[0] > 0) {
                        agging11[0] = parseFloat(agging11[0]) + parseFloat(weeks10[0]);
                        agging11[1] = parseFloat(agging11[1]) + parseFloat(1);
                    }
                    if (weeks11[0] > 0) {
                        agging12[0] = parseFloat(agging12[0]) + parseFloat(weeks11[0]);
                        agging12[1] = parseFloat(agging12[1]) + parseFloat(1);
                    }
                }
                //xc
                if (xcPr< 0) {
                    //xc
                    if (weeks[0] > 0) {
                        agging1xc[0] = parseFloat(agging1xc[0]) + parseFloat(weeks[0]);
                        agging1xc[1] = parseFloat(agging1xc[1]) + parseFloat(1);
                    }
                    if (weeks1[0] > 0) {
                        agging2xc[0] = parseFloat(agging2xc[0]) + parseFloat(weeks1[0]);
                        agging2xc[1] = parseFloat(agging2xc[1]) + parseFloat(1);
                    }
                    if (weeks2[0] > 0) {
                        agging3xc[0] = parseFloat(agging3xc[0]) + parseFloat(weeks2[0]);
                        agging3xc[1] = parseFloat(agging3xc[1]) + parseFloat(1);
                    }
                    if (weeks3[0] > 0) {
                        agging4xc[0] = parseFloat(agging4xc[0]) + parseFloat(weeks3[0]);
                        agging4xc[1] = parseFloat(agging4xc[1]) + parseFloat(1);
                    }
                    if (weeks4[0] > 0) {
                        agging5xc[0] = parseFloat(agging5xc[0]) + parseFloat(weeks4[0]);
                        agging5xc[1] = parseFloat(agging5xc[1]) + parseFloat(1);
                    }
                    if (weeks5[0] > 0) {
                        agging6xc[0] = parseFloat(agging6xc[0]) + parseFloat(weeks5[0]);
                        agging6xc[1] = parseFloat(agging6xc[1]) + parseFloat(1);
                    }
                    if (weeks6[0] > 0) {
                        agging7xc[0] = parseFloat(agging7xc[0]) + parseFloat(weeks6[0]);
                        agging7xc[1] = parseFloat(agging7xc[1]) + parseFloat(1);
                    }
                    if (weeks7[0] > 0) {
                        agging8xc[0] = parseFloat(agging8xc[0]) + parseFloat(weeks7[0]);
                        agging8xc[1] = parseFloat(agging8xc[1]) + parseFloat(1);
                    }
                    if (weeks8[0] > 0) {
                        agging9xc[0] = parseFloat(agging9xc[0]) + parseFloat(weeks8[0]);
                        agging9xc[1] = parseFloat(agging9xc[1]) + parseFloat(1);
                    }
                    if (weeks9[0] > 0) {
                        agging10xc[0] = parseFloat(agging10xc[0]) + parseFloat(weeks9[0]);
                        agging10xc[1] = parseFloat(agging10xc[1]) + parseFloat(1);
                    }
                    if (weeks10[0] > 0) {
                        agging11xc[0] = parseFloat(agging11xc[0]) + parseFloat(weeks10[0]);
                        agging11xc[1] = parseFloat(agging11xc[1]) + parseFloat(1);
                    }
                    if (weeks11[0] > 0) {
                        agging12xc[0] = parseFloat(agging12xc[0]) + parseFloat(weeks11[0]);
                        agging12xc[1] = parseFloat(agging12xc[1]) + parseFloat(1);
                    }
                }

                //vxc
                if (vxcPr< 0) {
                    //vxc
                    if (weeks[0] > 0) {
                        agging1vxc[0] = parseFloat(agging1vxc[0]) + parseFloat(weeks[0]);
                        agging1vxc[1] = parseFloat(agging1vxc[1]) + parseFloat(1);
                    }
                    if (weeks1[0] > 0) {
                        agging2vxc[0] = parseFloat(agging2vxc[0]) + parseFloat(weeks1[0]);
                        agging2vxc[1] = parseFloat(agging2vxc[1]) + parseFloat(1);
                    }
                    if (weeks2[0] > 0) {
                        agging3vxc[0] = parseFloat(agging3vxc[0]) + parseFloat(weeks2[0]);
                        agging3vxc[1] = parseFloat(agging3vxc[1]) + parseFloat(1);
                    }
                    if (weeks3[0] > 0) {
                        agging4vxc[0] = parseFloat(agging4vxc[0]) + parseFloat(weeks3[0]);
                        agging4vxc[1] = parseFloat(agging4vxc[1]) + parseFloat(1);
                    }
                    if (weeks4[0] > 0) {
                        agging5vxc[0] = parseFloat(agging5vxc[0]) + parseFloat(weeks4[0]);
                        agging5vxc[1] = parseFloat(agging5vxc[1]) + parseFloat(1);
                    }
                    if (weeks5[0] > 0) {
                        agging6vxc[0] = parseFloat(agging6vxc[0]) + parseFloat(weeks5[0]);
                        agging6vxc[1] = parseFloat(agging6vxc[1]) + parseFloat(1);
                    }
                    if (weeks6[0] > 0) {
                        agging7vxc[0] = parseFloat(agging7vxc[0]) + parseFloat(weeks6[0]);
                        agging7vxc[1] = parseFloat(agging7vxc[1]) + parseFloat(1);
                    }
                    if (weeks7[0] > 0) {
                        agging8vxc[0] = parseFloat(agging8vxc[0]) + parseFloat(weeks7[0]);
                        agging8vxc[1] = parseFloat(agging8vxc[1]) + parseFloat(1);
                    }
                    if (weeks8[0] > 0) {
                        agging9vxc[0] = parseFloat(agging9vxc[0]) + parseFloat(weeks8[0]);
                        agging9vxc[1] = parseFloat(agging9vxc[1]) + parseFloat(1);
                    }
                    if (weeks9[0] > 0) {
                        agging10vxc[0] = parseFloat(agging10vxc[0]) + parseFloat(weeks9[0]);
                        agging10vxc[1] = parseFloat(agging10vxc[1]) + parseFloat(1);
                    }
                    if (weeks10[0] > 0) {
                        agging11vxc[0] = parseFloat(agging11vxc[0]) + parseFloat(weeks10[0]);
                        agging11vxc[1] = parseFloat(agging11vxc[1]) + parseFloat(1);
                    }
                    if (weeks11[0] > 0) {
                        agging12vxc[0] = parseFloat(agging12vxc[0]) + parseFloat(weeks11[0]);
                        agging12vxc[1] = parseFloat(agging12vxc[1]) + parseFloat(1);
                    }
                }

                //power
                if (powerPr< 0) {
                    //power
                    if (weeks[0] > 0) {
                        agging1pow[0] = parseFloat(agging1pow[0]) + parseFloat(weeks[0]);
                        agging1pow[1] = parseFloat(agging1pow[1]) + parseFloat(1);
                    }
                    if (weeks1[0] > 0) {
                        agging2pow[0] = parseFloat(agging2pow[0]) + parseFloat(weeks1[0]);
                        agging2pow[1] = parseFloat(agging2pow[1]) + parseFloat(1);
                    }
                    if (weeks2[0] > 0) {
                        agging3pow[0] = parseFloat(agging3pow[0]) + parseFloat(weeks2[0]);
                        agging3pow[1] = parseFloat(agging3pow[1]) + parseFloat(1);
                    }
                    if (weeks3[0] > 0) {
                        agging4pow[0] = parseFloat(agging4pow[0]) + parseFloat(weeks3[0]);
                        agging4pow[1] = parseFloat(agging4pow[1]) + parseFloat(1);
                    }
                    if (weeks4[0] > 0) {
                        agging5pow[0] = parseFloat(agging5pow[0]) + parseFloat(weeks4[0]);
                        agging5pow[1] = parseFloat(agging5pow[1]) + parseFloat(1);
                    }
                    if (weeks5[0] > 0) {
                        agging6pow[0] = parseFloat(agging6pow[0]) + parseFloat(weeks5[0]);
                        agging6pow[1] = parseFloat(agging6pow[1]) + parseFloat(1);
                    }
                    if (weeks6[0] > 0) {
                        agging7pow[0] = parseFloat(agging7pow[0]) + parseFloat(weeks6[0]);
                        agging7pow[1] = parseFloat(agging7pow[1]) + parseFloat(1);
                    }
                    if (weeks7[0] > 0) {
                        agging8pow[0] = parseFloat(agging8pow[0]) + parseFloat(weeks7[0]);
                        agging8pow[1] = parseFloat(agging8pow[1]) + parseFloat(1);
                    }
                    if (weeks8[0] > 0) {
                        agging9pow[0] = parseFloat(agging9pow[0]) + parseFloat(weeks8[0]);
                        agging9pow[1] = parseFloat(agging9pow[1]) + parseFloat(1);
                    }
                    if (weeks9[0] > 0) {
                        agging10pow[0] = parseFloat(agging10pow[0]) + parseFloat(weeks9[0]);
                        agging10pow[1] = parseFloat(agging10pow[1]) + parseFloat(1);
                    }
                    if (weeks10[0] > 0) {
                        agging11pow[0] = parseFloat(agging11pow[0]) + parseFloat(weeks10[0]);
                        agging11pow[1] = parseFloat(agging11pow[1]) + parseFloat(1);
                    }
                    if (weeks11[0] > 0) {
                        agging12pow[0] = parseFloat(agging12pow[0]) + parseFloat(weeks11[0]);
                        agging12pow[1] = parseFloat(agging12pow[1]) + parseFloat(1);
                    }
                }
                //network
                if (netPr< 0) {
                    //network
                    if (weeks[0] > 0) {
                        agging1net[0] = parseFloat(agging1net[0]) + parseFloat(weeks[0]);
                        agging1net[1] = parseFloat(agging1net[1]) + parseFloat(1);
                    }
                    if (weeks1[0] > 0) {
                        agging2net[0] = parseFloat(agging2net[0]) + parseFloat(weeks1[0]);
                        agging2net[1] = parseFloat(agging2net[1]) + parseFloat(1);
                    }
                    if (weeks2[0] > 0) {
                        agging3net[0] = parseFloat(agging3net[0]) + parseFloat(weeks2[0]);
                        agging3net[1] = parseFloat(agging3net[1]) + parseFloat(1);
                    }
                    if (weeks3[0] > 0) {
                        agging4net[0] = parseFloat(agging4net[0]) + parseFloat(weeks3[0]);
                        agging4net[1] = parseFloat(agging4net[1]) + parseFloat(1);
                    }
                    if (weeks4[0] > 0) {
                        agging5net[0] = parseFloat(agging5net[0]) + parseFloat(weeks4[0]);
                        agging5net[1] = parseFloat(agging5net[1]) + parseFloat(1);
                    }
                    if (weeks5[0] > 0) {
                        agging6net[0] = parseFloat(agging6net[0]) + parseFloat(weeks5[0]);
                        agging6net[1] = parseFloat(agging6net[1]) + parseFloat(1);
                    }
                    if (weeks6[0] > 0) {
                        agging7net[0] = parseFloat(agging7net[0]) + parseFloat(weeks6[0]);
                        agging7net[1] = parseFloat(agging7net[1]) + parseFloat(1);
                    }
                    if (weeks7[0] > 0) {
                        agging8net[0] = parseFloat(agging8net[0]) + parseFloat(weeks7[0]);
                        agging8net[1] = parseFloat(agging8net[1]) + parseFloat(1);
                    }
                    if (weeks8[0] > 0) {
                        agging9net[0] = parseFloat(agging9net[0]) + parseFloat(weeks8[0]);
                        agging9net[1] = parseFloat(agging9net[1]) + parseFloat(1);
                    }
                    if (weeks9[0] > 0) {
                        agging10net[0] = parseFloat(agging10net[0]) + parseFloat(weeks9[0]);
                        agging10net[1] = parseFloat(agging10net[1]) + parseFloat(1);
                    }
                    if (weeks10[0] > 0) {
                        agging11net[0] = parseFloat(agging11net[0]) + parseFloat(weeks10[0]);
                        agging11net[1] = parseFloat(agging11net[1]) + parseFloat(1);
                    }
                    if (weeks11[0] > 0) {
                        agging12net[0] = parseFloat(agging12net[0]) + parseFloat(weeks11[0]);
                        agging12net[1] = parseFloat(agging12net[1]) + parseFloat(1);
                    }
                }
            }
        }
        else if(cattype=='nstandard')
        {
            if(nonstandard<0) {
                if (spacePr< 0) {
                    //space
                    if (weeks[0] > 0) {
                        agging1[0] = parseFloat(agging1[0]) + parseFloat(weeks[0]);
                        agging1[1] = parseFloat(agging1[1]) + parseFloat(1);
                    }
                    if (weeks1[0] > 0) {
                        agging2[0] = parseFloat(agging2[0]) + parseFloat(weeks1[0]);
                        agging2[1] = parseFloat(agging2[1]) + parseFloat(1);
                    }
                    if (weeks2[0] > 0) {
                        agging3[0] = parseFloat(agging3[0]) + parseFloat(weeks2[0]);
                        agging3[1] = parseFloat(agging3[1]) + parseFloat(1);
                    }
                    if (weeks3[0] > 0) {
                        agging4[0] = parseFloat(agging4[0]) + parseFloat(weeks3[0]);
                        agging4[1] = parseFloat(agging4[1]) + parseFloat(1);
                    }
                    if (weeks4[0] > 0) {
                        agging5[0] = parseFloat(agging5[0]) + parseFloat(weeks4[0]);
                        agging5[1] = parseFloat(agging5[1]) + parseFloat(1);
                    }
                    if (weeks5[0] > 0) {
                        agging6[0] = parseFloat(agging6[0]) + parseFloat(weeks5[0]);
                        agging6[1] = parseFloat(agging6[1]) + parseFloat(1);
                    }
                    if (weeks6[0] > 0) {
                        agging7[0] = parseFloat(agging7[0]) + parseFloat(weeks6[0]);
                        agging7[1] = parseFloat(agging7[1]) + parseFloat(1);
                    }
                    if (weeks7[0] > 0) {
                        agging8[0] = parseFloat(agging8[0]) + parseFloat(weeks7[0]);
                        agging8[1] = parseFloat(agging8[1]) + parseFloat(1);
                    }
                    if (weeks8[0] > 0) {
                        agging9[0] = parseFloat(agging9[0]) + parseFloat(weeks8[0]);
                        agging9[1] = parseFloat(agging9[1]) + parseFloat(1);
                    }
                    if (weeks9[0] > 0) {
                        agging10[0] = parseFloat(agging10[0]) + parseFloat(weeks9[0]);
                        agging10[1] = parseFloat(agging10[1]) + parseFloat(1);
                    }
                    if (weeks10[0] > 0) {
                        agging11[0] = parseFloat(agging11[0]) + parseFloat(weeks10[0]);
                        agging11[1] = parseFloat(agging11[1]) + parseFloat(1);
                    }
                    if (weeks11[0] > 0) {
                        agging12[0] = parseFloat(agging12[0]) + parseFloat(weeks11[0]);
                        agging12[1] = parseFloat(agging12[1]) + parseFloat(1);
                    }
                }
                //xc
                if (xcPr< 0) {
                    //xc
                    if (weeks[0] > 0) {
                        agging1xc[0] = parseFloat(agging1xc[0]) + parseFloat(weeks[0]);
                        agging1xc[1] = parseFloat(agging1xc[1]) + parseFloat(1);
                    }
                    if (weeks1[0] > 0) {
                        agging2xc[0] = parseFloat(agging2xc[0]) + parseFloat(weeks1[0]);
                        agging2xc[1] = parseFloat(agging2xc[1]) + parseFloat(1);
                    }
                    if (weeks2[0] > 0) {
                        agging3xc[0] = parseFloat(agging3xc[0]) + parseFloat(weeks2[0]);
                        agging3xc[1] = parseFloat(agging3xc[1]) + parseFloat(1);
                    }
                    if (weeks3[0] > 0) {
                        agging4xc[0] = parseFloat(agging4xc[0]) + parseFloat(weeks3[0]);
                        agging4xc[1] = parseFloat(agging4xc[1]) + parseFloat(1);
                    }
                    if (weeks4[0] > 0) {
                        agging5xc[0] = parseFloat(agging5xc[0]) + parseFloat(weeks4[0]);
                        agging5xc[1] = parseFloat(agging5xc[1]) + parseFloat(1);
                    }
                    if (weeks5[0] > 0) {
                        agging6xc[0] = parseFloat(agging6xc[0]) + parseFloat(weeks5[0]);
                        agging6xc[1] = parseFloat(agging6xc[1]) + parseFloat(1);
                    }
                    if (weeks6[0] > 0) {
                        agging7xc[0] = parseFloat(agging7xc[0]) + parseFloat(weeks6[0]);
                        agging7xc[1] = parseFloat(agging7xc[1]) + parseFloat(1);
                    }
                    if (weeks7[0] > 0) {
                        agging8xc[0] = parseFloat(agging8xc[0]) + parseFloat(weeks7[0]);
                        agging8xc[1] = parseFloat(agging8xc[1]) + parseFloat(1);
                    }
                    if (weeks8[0] > 0) {
                        agging9xc[0] = parseFloat(agging9xc[0]) + parseFloat(weeks8[0]);
                        agging9xc[1] = parseFloat(agging9xc[1]) + parseFloat(1);
                    }
                    if (weeks9[0] > 0) {
                        agging10xc[0] = parseFloat(agging10xc[0]) + parseFloat(weeks9[0]);
                        agging10xc[1] = parseFloat(agging10xc[1]) + parseFloat(1);
                    }
                    if (weeks10[0] > 0) {
                        agging11xc[0] = parseFloat(agging11xc[0]) + parseFloat(weeks10[0]);
                        agging11xc[1] = parseFloat(agging11xc[1]) + parseFloat(1);
                    }
                    if (weeks11[0] > 0) {
                        agging12xc[0] = parseFloat(agging12xc[0]) + parseFloat(weeks11[0]);
                        agging12xc[1] = parseFloat(agging12xc[1]) + parseFloat(1);
                    }
                }

                //vxc
                if (vxcPr< 0) {
                    //vxc
                    if (weeks[0] > 0) {
                        agging1vxc[0] = parseFloat(agging1vxc[0]) + parseFloat(weeks[0]);
                        agging1vxc[1] = parseFloat(agging1vxc[1]) + parseFloat(1);
                    }
                    if (weeks1[0] > 0) {
                        agging2vxc[0] = parseFloat(agging2vxc[0]) + parseFloat(weeks1[0]);
                        agging2vxc[1] = parseFloat(agging2vxc[1]) + parseFloat(1);
                    }
                    if (weeks2[0] > 0) {
                        agging3vxc[0] = parseFloat(agging3vxc[0]) + parseFloat(weeks2[0]);
                        agging3vxc[1] = parseFloat(agging3vxc[1]) + parseFloat(1);
                    }
                    if (weeks3[0] > 0) {
                        agging4vxc[0] = parseFloat(agging4vxc[0]) + parseFloat(weeks3[0]);
                        agging4vxc[1] = parseFloat(agging4vxc[1]) + parseFloat(1);
                    }
                    if (weeks4[0] > 0) {
                        agging5vxc[0] = parseFloat(agging5vxc[0]) + parseFloat(weeks4[0]);
                        agging5vxc[1] = parseFloat(agging5vxc[1]) + parseFloat(1);
                    }
                    if (weeks5[0] > 0) {
                        agging6vxc[0] = parseFloat(agging6vxc[0]) + parseFloat(weeks5[0]);
                        agging6vxc[1] = parseFloat(agging6vxc[1]) + parseFloat(1);
                    }
                    if (weeks6[0] > 0) {
                        agging7vxc[0] = parseFloat(agging7vxc[0]) + parseFloat(weeks6[0]);
                        agging7vxc[1] = parseFloat(agging7vxc[1]) + parseFloat(1);
                    }
                    if (weeks7[0] > 0) {
                        agging8vxc[0] = parseFloat(agging8vxc[0]) + parseFloat(weeks7[0]);
                        agging8vxc[1] = parseFloat(agging8vxc[1]) + parseFloat(1);
                    }
                    if (weeks8[0] > 0) {
                        agging9vxc[0] = parseFloat(agging9vxc[0]) + parseFloat(weeks8[0]);
                        agging9vxc[1] = parseFloat(agging9vxc[1]) + parseFloat(1);
                    }
                    if (weeks9[0] > 0) {
                        agging10vxc[0] = parseFloat(agging10vxc[0]) + parseFloat(weeks9[0]);
                        agging10vxc[1] = parseFloat(agging10vxc[1]) + parseFloat(1);
                    }
                    if (weeks10[0] > 0) {
                        agging11vxc[0] = parseFloat(agging11vxc[0]) + parseFloat(weeks10[0]);
                        agging11vxc[1] = parseFloat(agging11vxc[1]) + parseFloat(1);
                    }
                    if (weeks11[0] > 0) {
                        agging12vxc[0] = parseFloat(agging12vxc[0]) + parseFloat(weeks11[0]);
                        agging12vxc[1] = parseFloat(agging12vxc[1]) + parseFloat(1);
                    }
                }

                //power
                if (powerPr< 0) {
                    //power
                    if (weeks[0] > 0) {
                        agging1pow[0] = parseFloat(agging1pow[0]) + parseFloat(weeks[0]);
                        agging1pow[1] = parseFloat(agging1pow[1]) + parseFloat(1);
                    }
                    if (weeks1[0] > 0) {
                        agging2pow[0] = parseFloat(agging2pow[0]) + parseFloat(weeks1[0]);
                        agging2pow[1] = parseFloat(agging2pow[1]) + parseFloat(1);
                    }
                    if (weeks2[0] > 0) {
                        agging3pow[0] = parseFloat(agging3pow[0]) + parseFloat(weeks2[0]);
                        agging3pow[1] = parseFloat(agging3pow[1]) + parseFloat(1);
                    }
                    if (weeks3[0] > 0) {
                        agging4pow[0] = parseFloat(agging4pow[0]) + parseFloat(weeks3[0]);
                        agging4pow[1] = parseFloat(agging4pow[1]) + parseFloat(1);
                    }
                    if (weeks4[0] > 0) {
                        agging5pow[0] = parseFloat(agging5pow[0]) + parseFloat(weeks4[0]);
                        agging5pow[1] = parseFloat(agging5pow[1]) + parseFloat(1);
                    }
                    if (weeks5[0] > 0) {
                        agging6pow[0] = parseFloat(agging6pow[0]) + parseFloat(weeks5[0]);
                        agging6pow[1] = parseFloat(agging6pow[1]) + parseFloat(1);
                    }
                    if (weeks6[0] > 0) {
                        agging7pow[0] = parseFloat(agging7pow[0]) + parseFloat(weeks6[0]);
                        agging7pow[1] = parseFloat(agging7pow[1]) + parseFloat(1);
                    }
                    if (weeks7[0] > 0) {
                        agging8pow[0] = parseFloat(agging8pow[0]) + parseFloat(weeks7[0]);
                        agging8pow[1] = parseFloat(agging8pow[1]) + parseFloat(1);
                    }
                    if (weeks8[0] > 0) {
                        agging9pow[0] = parseFloat(agging9pow[0]) + parseFloat(weeks8[0]);
                        agging9pow[1] = parseFloat(agging9pow[1]) + parseFloat(1);
                    }
                    if (weeks9[0] > 0) {
                        agging10pow[0] = parseFloat(agging10pow[0]) + parseFloat(weeks9[0]);
                        agging10pow[1] = parseFloat(agging10pow[1]) + parseFloat(1);
                    }
                    if (weeks10[0] > 0) {
                        agging11pow[0] = parseFloat(agging11pow[0]) + parseFloat(weeks10[0]);
                        agging11pow[1] = parseFloat(agging11pow[1]) + parseFloat(1);
                    }
                    if (weeks11[0] > 0) {
                        agging12pow[0] = parseFloat(agging12pow[0]) + parseFloat(weeks11[0]);
                        agging12pow[1] = parseFloat(agging12pow[1]) + parseFloat(1);
                    }
                }
                //network
                if (netPr< 0) {
                    //network
                    if (weeks[0] > 0) {
                        agging1net[0] = parseFloat(agging1net[0]) + parseFloat(weeks[0]);
                        agging1net[1] = parseFloat(agging1net[1]) + parseFloat(1);
                    }
                    if (weeks1[0] > 0) {
                        agging2net[0] = parseFloat(agging2net[0]) + parseFloat(weeks1[0]);
                        agging2net[1] = parseFloat(agging2net[1]) + parseFloat(1);
                    }
                    if (weeks2[0] > 0) {
                        agging3net[0] = parseFloat(agging3net[0]) + parseFloat(weeks2[0]);
                        agging3net[1] = parseFloat(agging3net[1]) + parseFloat(1);
                    }
                    if (weeks3[0] > 0) {
                        agging4net[0] = parseFloat(agging4net[0]) + parseFloat(weeks3[0]);
                        agging4net[1] = parseFloat(agging4net[1]) + parseFloat(1);
                    }
                    if (weeks4[0] > 0) {
                        agging5net[0] = parseFloat(agging5net[0]) + parseFloat(weeks4[0]);
                        agging5net[1] = parseFloat(agging5net[1]) + parseFloat(1);
                    }
                    if (weeks5[0] > 0) {
                        agging6net[0] = parseFloat(agging6net[0]) + parseFloat(weeks5[0]);
                        agging6net[1] = parseFloat(agging6net[1]) + parseFloat(1);
                    }
                    if (weeks6[0] > 0) {
                        agging7net[0] = parseFloat(agging7net[0]) + parseFloat(weeks6[0]);
                        agging7net[1] = parseFloat(agging7net[1]) + parseFloat(1);
                    }
                    if (weeks7[0] > 0) {
                        agging8net[0] = parseFloat(agging8net[0]) + parseFloat(weeks7[0]);
                        agging8net[1] = parseFloat(agging8net[1]) + parseFloat(1);
                    }
                    if (weeks8[0] > 0) {
                        agging9net[0] = parseFloat(agging9net[0]) + parseFloat(weeks8[0]);
                        agging9net[1] = parseFloat(agging9net[1]) + parseFloat(1);
                    }
                    if (weeks9[0] > 0) {
                        agging10net[0] = parseFloat(agging10net[0]) + parseFloat(weeks9[0]);
                        agging10net[1] = parseFloat(agging10net[1]) + parseFloat(1);
                    }
                    if (weeks10[0] > 0) {
                        agging11net[0] = parseFloat(agging11net[0]) + parseFloat(weeks10[0]);
                        agging11net[1] = parseFloat(agging11net[1]) + parseFloat(1);
                    }
                    if (weeks11[0] > 0) {
                        agging12net[0] = parseFloat(agging12net[0]) + parseFloat(weeks11[0]);
                        agging12net[1] = parseFloat(agging12net[1]) + parseFloat(1);
                    }
                }
            }
        }
        else
        {

            if (spacePr< 0) {
                //space
                if (weeks[0] > 0) {
                    agging1[0] = parseFloat(agging1[0]) + parseFloat(weeks[0]);
                    agging1[1] = parseFloat(agging1[1]) + parseFloat(1);
                }
                if (weeks1[0] > 0) {
                    agging2[0] = parseFloat(agging2[0]) + parseFloat(weeks1[0]);
                    agging2[1] = parseFloat(agging2[1]) + parseFloat(1);
                }
                if (weeks2[0] > 0) {
                    agging3[0] = parseFloat(agging3[0]) + parseFloat(weeks2[0]);
                    agging3[1] = parseFloat(agging3[1]) + parseFloat(1);
                }
                if (weeks3[0] > 0) {
                    agging4[0] = parseFloat(agging4[0]) + parseFloat(weeks3[0]);
                    agging4[1] = parseFloat(agging4[1]) + parseFloat(1);
                }
                if (weeks4[0] > 0) {
                    agging5[0] = parseFloat(agging5[0]) + parseFloat(weeks4[0]);
                    agging5[1] = parseFloat(agging5[1]) + parseFloat(1);
                }
                if (weeks5[0] > 0) {
                    agging6[0] = parseFloat(agging6[0]) + parseFloat(weeks5[0]);
                    agging6[1] = parseFloat(agging6[1]) + parseFloat(1);
                }
                if (weeks6[0] > 0) {
                    agging7[0] = parseFloat(agging7[0]) + parseFloat(weeks6[0]);
                    agging7[1] = parseFloat(agging7[1]) + parseFloat(1);
                }
                if (weeks7[0] > 0) {
                    agging8[0] = parseFloat(agging8[0]) + parseFloat(weeks7[0]);
                    agging8[1] = parseFloat(agging8[1]) + parseFloat(1);
                }
                if (weeks8[0] > 0) {
                    agging9[0] = parseFloat(agging9[0]) + parseFloat(weeks8[0]);
                    agging9[1] = parseFloat(agging9[1]) + parseFloat(1);
                }
                if (weeks9[0] > 0) {
                    agging10[0] = parseFloat(agging10[0]) + parseFloat(weeks9[0]);
                    agging10[1] = parseFloat(agging10[1]) + parseFloat(1);
                }
                if (weeks10[0] > 0) {
                    agging11[0] = parseFloat(agging11[0]) + parseFloat(weeks10[0]);
                    agging11[1] = parseFloat(agging11[1]) + parseFloat(1);
                }
                if (weeks11[0] > 0) {
                    agging12[0] = parseFloat(agging12[0]) + parseFloat(weeks11[0]);
                    agging12[1] = parseFloat(agging12[1]) + parseFloat(1);
                }
            }
            //xc
            if (xcPr< 0) {
                //xc
                if (weeks[0] > 0) {
                    agging1xc[0] = parseFloat(agging1xc[0]) + parseFloat(weeks[0]);
                    agging1xc[1] = parseFloat(agging1xc[1]) + parseFloat(1);
                }
                if (weeks1[0] > 0) {
                    agging2xc[0] = parseFloat(agging2xc[0]) + parseFloat(weeks1[0]);
                    agging2xc[1] = parseFloat(agging2xc[1]) + parseFloat(1);
                }
                if (weeks2[0] > 0) {
                    agging3xc[0] = parseFloat(agging3xc[0]) + parseFloat(weeks2[0]);
                    agging3xc[1] = parseFloat(agging3xc[1]) + parseFloat(1);
                }
                if (weeks3[0] > 0) {
                    agging4xc[0] = parseFloat(agging4xc[0]) + parseFloat(weeks3[0]);
                    agging4xc[1] = parseFloat(agging4xc[1]) + parseFloat(1);
                }
                if (weeks4[0] > 0) {
                    agging5xc[0] = parseFloat(agging5xc[0]) + parseFloat(weeks4[0]);
                    agging5xc[1] = parseFloat(agging5xc[1]) + parseFloat(1);
                }
                if (weeks5[0] > 0) {
                    agging6xc[0] = parseFloat(agging6xc[0]) + parseFloat(weeks5[0]);
                    agging6xc[1] = parseFloat(agging6xc[1]) + parseFloat(1);
                }
                if (weeks6[0] > 0) {
                    agging7xc[0] = parseFloat(agging7xc[0]) + parseFloat(weeks6[0]);
                    agging7xc[1] = parseFloat(agging7xc[1]) + parseFloat(1);
                }
                if (weeks7[0] > 0) {
                    agging8xc[0] = parseFloat(agging8xc[0]) + parseFloat(weeks7[0]);
                    agging8xc[1] = parseFloat(agging8xc[1]) + parseFloat(1);
                }
                if (weeks8[0] > 0) {
                    agging9xc[0] = parseFloat(agging9xc[0]) + parseFloat(weeks8[0]);
                    agging9xc[1] = parseFloat(agging9xc[1]) + parseFloat(1);
                }
                if (weeks9[0] > 0) {
                    agging10xc[0] = parseFloat(agging10xc[0]) + parseFloat(weeks9[0]);
                    agging10xc[1] = parseFloat(agging10xc[1]) + parseFloat(1);
                }
                if (weeks10[0] > 0) {
                    agging11xc[0] = parseFloat(agging11xc[0]) + parseFloat(weeks10[0]);
                    agging11xc[1] = parseFloat(agging11xc[1]) + parseFloat(1);
                }
                if (weeks11[0] > 0) {
                    agging12xc[0] = parseFloat(agging12xc[0]) + parseFloat(weeks11[0]);
                    agging12xc[1] = parseFloat(agging12xc[1]) + parseFloat(1);
                }
            }

            //vxc
            if (vxcPr< 0) {
                //vxc
                if (weeks[0] > 0) {
                    agging1vxc[0] = parseFloat(agging1vxc[0]) + parseFloat(weeks[0]);
                    agging1vxc[1] = parseFloat(agging1vxc[1]) + parseFloat(1);
                }
                if (weeks1[0] > 0) {
                    agging2vxc[0] = parseFloat(agging2vxc[0]) + parseFloat(weeks1[0]);
                    agging2vxc[1] = parseFloat(agging2vxc[1]) + parseFloat(1);
                }
                if (weeks2[0] > 0) {
                    agging3vxc[0] = parseFloat(agging3vxc[0]) + parseFloat(weeks2[0]);
                    agging3vxc[1] = parseFloat(agging3vxc[1]) + parseFloat(1);
                }
                if (weeks3[0] > 0) {
                    agging4vxc[0] = parseFloat(agging4vxc[0]) + parseFloat(weeks3[0]);
                    agging4vxc[1] = parseFloat(agging4vxc[1]) + parseFloat(1);
                }
                if (weeks4[0] > 0) {
                    agging5vxc[0] = parseFloat(agging5vxc[0]) + parseFloat(weeks4[0]);
                    agging5vxc[1] = parseFloat(agging5vxc[1]) + parseFloat(1);
                }
                if (weeks5[0] > 0) {
                    agging6vxc[0] = parseFloat(agging6vxc[0]) + parseFloat(weeks5[0]);
                    agging6vxc[1] = parseFloat(agging6vxc[1]) + parseFloat(1);
                }
                if (weeks6[0] > 0) {
                    agging7vxc[0] = parseFloat(agging7vxc[0]) + parseFloat(weeks6[0]);
                    agging7vxc[1] = parseFloat(agging7vxc[1]) + parseFloat(1);
                }
                if (weeks7[0] > 0) {
                    agging8vxc[0] = parseFloat(agging8vxc[0]) + parseFloat(weeks7[0]);
                    agging8vxc[1] = parseFloat(agging8vxc[1]) + parseFloat(1);
                }
                if (weeks8[0] > 0) {
                    agging9vxc[0] = parseFloat(agging9vxc[0]) + parseFloat(weeks8[0]);
                    agging9vxc[1] = parseFloat(agging9vxc[1]) + parseFloat(1);
                }
                if (weeks9[0] > 0) {
                    agging10vxc[0] = parseFloat(agging10vxc[0]) + parseFloat(weeks9[0]);
                    agging10vxc[1] = parseFloat(agging10vxc[1]) + parseFloat(1);
                }
                if (weeks10[0] > 0) {
                    agging11vxc[0] = parseFloat(agging11vxc[0]) + parseFloat(weeks10[0]);
                    agging11vxc[1] = parseFloat(agging11vxc[1]) + parseFloat(1);
                }
                if (weeks11[0] > 0) {
                    agging12vxc[0] = parseFloat(agging12vxc[0]) + parseFloat(weeks11[0]);
                    agging12vxc[1] = parseFloat(agging12vxc[1]) + parseFloat(1);
                }
            }

            //power
            if (powerPr< 0) {
                //power
                if (weeks[0] > 0) {
                    agging1pow[0] = parseFloat(agging1pow[0]) + parseFloat(weeks[0]);
                    agging1pow[1] = parseFloat(agging1pow[1]) + parseFloat(1);
                }
                if (weeks1[0] > 0) {
                    agging2pow[0] = parseFloat(agging2pow[0]) + parseFloat(weeks1[0]);
                    agging2pow[1] = parseFloat(agging2pow[1]) + parseFloat(1);
                }
                if (weeks2[0] > 0) {
                    agging3pow[0] = parseFloat(agging3pow[0]) + parseFloat(weeks2[0]);
                    agging3pow[1] = parseFloat(agging3pow[1]) + parseFloat(1);
                }
                if (weeks3[0] > 0) {
                    agging4pow[0] = parseFloat(agging4pow[0]) + parseFloat(weeks3[0]);
                    agging4pow[1] = parseFloat(agging4pow[1]) + parseFloat(1);
                }
                if (weeks4[0] > 0) {
                    agging5pow[0] = parseFloat(agging5pow[0]) + parseFloat(weeks4[0]);
                    agging5pow[1] = parseFloat(agging5pow[1]) + parseFloat(1);
                }
                if (weeks5[0] > 0) {
                    agging6pow[0] = parseFloat(agging6pow[0]) + parseFloat(weeks5[0]);
                    agging6pow[1] = parseFloat(agging6pow[1]) + parseFloat(1);
                }
                if (weeks6[0] > 0) {
                    agging7pow[0] = parseFloat(agging7pow[0]) + parseFloat(weeks6[0]);
                    agging7pow[1] = parseFloat(agging7pow[1]) + parseFloat(1);
                }
                if (weeks7[0] > 0) {
                    agging8pow[0] = parseFloat(agging8pow[0]) + parseFloat(weeks7[0]);
                    agging8pow[1] = parseFloat(agging8pow[1]) + parseFloat(1);
                }
                if (weeks8[0] > 0) {
                    agging9pow[0] = parseFloat(agging9pow[0]) + parseFloat(weeks8[0]);
                    agging9pow[1] = parseFloat(agging9pow[1]) + parseFloat(1);
                }
                if (weeks9[0] > 0) {
                    agging10pow[0] = parseFloat(agging10pow[0]) + parseFloat(weeks9[0]);
                    agging10pow[1] = parseFloat(agging10pow[1]) + parseFloat(1);
                }
                if (weeks10[0] > 0) {
                    agging11pow[0] = parseFloat(agging11pow[0]) + parseFloat(weeks10[0]);
                    agging11pow[1] = parseFloat(agging11pow[1]) + parseFloat(1);
                }
                if (weeks11[0] > 0) {
                    agging12pow[0] = parseFloat(agging12pow[0]) + parseFloat(weeks11[0]);
                    agging12pow[1] = parseFloat(agging12pow[1]) + parseFloat(1);
                }
            }
            //network
            if (netPr< 0) {
                //network
                if (weeks[0] > 0) {
                    agging1net[0] = parseFloat(agging1net[0]) + parseFloat(weeks[0]);
                    agging1net[1] = parseFloat(agging1net[1]) + parseFloat(1);
                }
                if (weeks1[0] > 0) {
                    agging2net[0] = parseFloat(agging2net[0]) + parseFloat(weeks1[0]);
                    agging2net[1] = parseFloat(agging2net[1]) + parseFloat(1);
                }
                if (weeks2[0] > 0) {
                    agging3net[0] = parseFloat(agging3net[0]) + parseFloat(weeks2[0]);
                    agging3net[1] = parseFloat(agging3net[1]) + parseFloat(1);
                }
                if (weeks3[0] > 0) {
                    agging4net[0] = parseFloat(agging4net[0]) + parseFloat(weeks3[0]);
                    agging4net[1] = parseFloat(agging4net[1]) + parseFloat(1);
                }
                if (weeks4[0] > 0) {
                    agging5net[0] = parseFloat(agging5net[0]) + parseFloat(weeks4[0]);
                    agging5net[1] = parseFloat(agging5net[1]) + parseFloat(1);
                }
                if (weeks5[0] > 0) {
                    agging6net[0] = parseFloat(agging6net[0]) + parseFloat(weeks5[0]);
                    agging6net[1] = parseFloat(agging6net[1]) + parseFloat(1);
                }
                if (weeks6[0] > 0) {
                    agging7net[0] = parseFloat(agging7net[0]) + parseFloat(weeks6[0]);
                    agging7net[1] = parseFloat(agging7net[1]) + parseFloat(1);
                }
                if (weeks7[0] > 0) {
                    agging8net[0] = parseFloat(agging8net[0]) + parseFloat(weeks7[0]);
                    agging8net[1] = parseFloat(agging8net[1]) + parseFloat(1);
                }
                if (weeks8[0] > 0) {
                    agging9net[0] = parseFloat(agging9net[0]) + parseFloat(weeks8[0]);
                    agging9net[1] = parseFloat(agging9net[1]) + parseFloat(1);
                }
                if (weeks9[0] > 0) {
                    agging10net[0] = parseFloat(agging10net[0]) + parseFloat(weeks9[0]);
                    agging10net[1] = parseFloat(agging10net[1]) + parseFloat(1);
                }
                if (weeks10[0] > 0) {
                    agging11net[0] = parseFloat(agging11net[0]) + parseFloat(weeks10[0]);
                    agging11net[1] = parseFloat(agging11net[1]) + parseFloat(1);
                }
                if (weeks11[0] > 0) {
                    agging12net[0] = parseFloat(agging12net[0]) + parseFloat(weeks11[0]);
                    agging12net[1] = parseFloat(agging12net[1]) + parseFloat(1);
                }
            }
        }

    }
//space
    if(agging1[0]!=0)
    {
        var agging1AV= parseFloat(agging1[0])/parseFloat(agging1[1]);
    }
    else
    {
        var agging1AV=0;
    }
    if(agging2[0]!=0)
    {
        var agging2AV= parseFloat(agging2[0])/parseFloat(agging2[1]);
    }
    else
    {
        var agging2AV=0;
    }
    if(agging3[0]!=0)
    {
        var agging3AV= parseFloat(agging3[0])/parseFloat(agging3[1]);
    }
    else
    {
        var agging3AV=0;
    }
    if(agging4[0]!=0)
    {
        var agging4AV= parseFloat(agging4[0])/parseFloat(agging4[1]);
    }
    else
    {
        var agging4AV=0;
    }
    if(agging5[0]!=0)
    {
        var agging5AV= parseFloat(agging5[0])/parseFloat(agging5[1]);
    }
    else
    {
        var agging5AV=0;
    }
    if(agging6[0]!=0)
    {
        var agging6AV= parseFloat(agging6[0])/parseFloat(agging6[1]);
    }
    else
    {
        var agging6AV=0;
    }
    if(agging7[0]!=0)
    {
        var agging7AV= parseFloat(agging7[0])/parseFloat(agging7[1]);
    }
    else
    {
        var agging7AV=0;
    }
    if(agging8[0]!=0)
    {
        var agging8AV= parseFloat(agging8[0])/parseFloat(agging8[1]);
    }
    else
    {
        var agging8AV=0;
    }
    if(agging9[0]!=0)
    {
        var agging9AV= parseFloat(agging9[0])/parseFloat(agging9[1]);
    }
    else
    {
        var agging9AV=0;
    }
    if(agging10[0]!=0)
    {
        var agging10AV= parseFloat(agging10[0])/parseFloat(agging10[1]);
    }
    else
    {
        var agging10AV=0;
    }
    if(agging11[0]!=0)
    {
        var agging11AV= parseFloat(agging11[0])/parseFloat(agging11[1]);
    }
    else
    {
        var agging11AV=0;
    }
    if(agging12[0]!=0)
    {
        var agging12AV= parseFloat(agging12[0])/parseFloat(agging12[1]);
    }
    else
    {
        var agging12AV=0;
    }

    //xc
    if(agging1xc[0]!=0)
    {
        var agging1AVXC= parseFloat(agging1xc[0])/parseFloat(agging1xc[1]);
    }
    else
    {
        var agging1AVXC=0;
    }
    if(agging2xc[0]!=0)
    {
        var agging2AVXC= parseFloat(agging2xc[0])/parseFloat(agging2xc[1]);
    }
    else
    {
        var agging2AVXC=0;
    }
    if(agging3xc[0]!=0)
    {
        var agging3AVXC= parseFloat(agging3xc[0])/parseFloat(agging3xc[1]);
    }
    else
    {
        var agging3AVXC=0;
    }
    if(agging4xc[0]!=0)
    {
        var agging4AVXC= parseFloat(agging4xc[0])/parseFloat(agging4xc[1]);
    }
    else
    {
        var agging4AVXC=0;
    }
    if(agging5xc[0]!=0)
    {
        var agging5AVXC= parseFloat(agging5xc[0])/parseFloat(agging5xc[1]);
    }
    else
    {
        var agging5AVXC=0;
    }
    if(agging6xc[0]!=0)
    {
        var agging6AVXC= parseFloat(agging6xc[0])/parseFloat(agging6xc[1]);
    }
    else
    {
        var agging6AVXC=0;
    }
    if(agging7xc[0]!=0)
    {
        var agging7AVXC= parseFloat(agging7xc[0])/parseFloat(agging7xc[1]);
    }
    else
    {
        var agging7AVXC=0;
    }
    if(agging8xc[0]!=0)
    {
        var agging8AVXC= parseFloat(agging8xc[0])/parseFloat(agging8xc[1]);
    }
    else
    {
        var agging8AVXC=0;
    }
    if(agging9xc[0]!=0)
    {
        var agging9AVXC= parseFloat(agging9xc[0])/parseFloat(agging9xc[1]);
    }
    else
    {
        var agging9AVXC=0;
    }
    if(agging10xc[0]!=0)
    {
        var agging10AVXC= parseFloat(agging10xc[0])/parseFloat(agging10xc[1]);
    }
    else
    {
        var agging10AVXC=0;
    }
    if(agging11xc[0]!=0)
    {
        var agging11AVXC= parseFloat(agging11xc[0])/parseFloat(agging11xc[1]);
    }
    else
    {
        var agging11AVXC=0;
    }
    if(agging12xc[0]!=0)
    {
        var agging12AVXC= parseFloat(agging12xc[0])/parseFloat(agging12xc[1]);
    }
    else
    {
        var agging12AVXC=0;
    }

    //vxc
    if(agging1vxc[0]!=0)
    {
        var agging1AVVXC= parseFloat(agging1vxc[0])/parseFloat(agging1vxc[1]);
    }
    else
    {
        var agging1AVVXC=0;
    }
    if(agging2vxc[0]!=0)
    {
        var agging2AVVXC= parseFloat(agging2vxc[0])/parseFloat(agging2vxc[1]);
    }
    else
    {
        var agging2AVVXC=0;
    }
    if(agging3vxc[0]!=0)
    {
        var agging3AVVXC= parseFloat(agging3vxc[0])/parseFloat(agging3vxc[1]);
    }
    else
    {
        var agging3AVVXC=0;
    }
    if(agging4vxc[0]!=0)
    {
        var agging4AVVXC= parseFloat(agging4vxc[0])/parseFloat(agging4vxc[1]);
    }
    else
    {
        var agging4AVVXC=0;
    }
    if(agging5vxc[0]!=0)
    {
        var agging5AVVXC= parseFloat(agging5vxc[0])/parseFloat(agging5vxc[1]);
    }
    else
    {
        var agging5AVVXC=0;
    }
    if(agging6vxc[0]!=0)
    {
        var agging6AVVXC= parseFloat(agging6vxc[0])/parseFloat(agging6vxc[1]);
    }
    else
    {
        var agging6AVVXC=0;
    }
    if(agging7vxc[0]!=0)
    {
        var agging7AVVXC= parseFloat(agging7vxc[0])/parseFloat(agging7vxc[1]);
    }
    else
    {
        var agging7AVVXC=0;
    }
    if(agging8vxc[0]!=0)
    {
        var agging8AVVXC= parseFloat(agging8vxc[0])/parseFloat(agging8vxc[1]);
    }
    else
    {
        var agging8AVVXC=0;
    }
    if(agging9vxc[0]!=0)
    {
        var agging9AVVXC= parseFloat(agging9vxc[0])/parseFloat(agging9vxc[1]);
    }
    else
    {
        var agging9AVVXC=0;
    }
    if(agging10vxc[0]!=0)
    {
        var agging10AVVXC= parseFloat(agging10vxc[0])/parseFloat(agging10vxc[1]);
    }
    else
    {
        var agging10AVVXC=0;
    }
    if(agging11vxc[0]!=0)
    {
        var agging11AVVXC= parseFloat(agging11vxc[0])/parseFloat(agging11vxc[1]);
    }
    else
    {
        var agging11AVVXC=0;
    }
    if(agging12vxc[0]!=0)
    {
        var agging12AVVXC= parseFloat(agging12vxc[0])/parseFloat(agging12vxc[1]);
    }
    else
    {
        var agging12AVVXC=0;
    }

    //power
    if(agging1pow[0]!=0)
    {
        var agging1AVPOW= parseFloat(agging1pow[0])/parseFloat(agging1pow[1]);
    }
    else
    {
        var agging1AVPOW=0;
    }
    if(agging2pow[0]!=0)
    {
        var agging2AVPOW= parseFloat(agging2pow[0])/parseFloat(agging2pow[1]);
    }
    else
    {
        var agging2AVPOW=0;
    }
    if(agging3pow[0]!=0)
    {
        var agging3AVPOW= parseFloat(agging3pow[0])/parseFloat(agging3pow[1]);
    }
    else
    {
        var agging3AVPOW=0;
    }
    if(agging4pow[0]!=0)
    {
        var agging4AVPOW= parseFloat(agging4pow[0])/parseFloat(agging4pow[1]);
    }
    else
    {
        var agging4AVPOW=0;
    }
    if(agging5pow[0]!=0)
    {
        var agging5AVPOW= parseFloat(agging5pow[0])/parseFloat(agging5pow[1]);
    }
    else
    {
        var agging5AVPOW=0;
    }
    if(agging6pow[0]!=0)
    {
        var agging6AVPOW= parseFloat(agging6pow[0])/parseFloat(agging6pow[1]);
    }
    else
    {
        var agging6AVPOW=0;
    }
    if(agging7pow[0]!=0)
    {
        var agging7AVPOW= parseFloat(agging7pow[0])/parseFloat(agging7pow[1]);
    }
    else
    {
        var agging7AVPOW=0;
    }
    if(agging8pow[0]!=0)
    {
        var agging8AVPOW= parseFloat(agging8pow[0])/parseFloat(agging8pow[1]);
    }
    else
    {
        var agging8AVPOW=0;
    }
    if(agging9pow[0]!=0)
    {
        var agging9AVPOW= parseFloat(agging9pow[0])/parseFloat(agging9pow[1]);
    }
    else
    {
        var agging9AVPOW=0;
    }
    if(agging10pow[0]!=0)
    {
        var agging10AVPOW= parseFloat(agging10pow[0])/parseFloat(agging10pow[1]);
    }
    else
    {
        var agging10AVPOW=0;
    }
    if(agging11pow[0]!=0)
    {
        var agging11AVPOW= parseFloat(agging11pow[0])/parseFloat(agging11pow[1]);
    }
    else
    {
        var agging11AVPOW=0;
    }
    if(agging12pow[0]!=0)
    {
        var agging12AVPOW= parseFloat(agging12pow[0])/parseFloat(agging12pow[1]);
    }
    else
    {
        var agging12AVPOW=0;
    }

    //network
    if(agging1net[0]!=0)
    {
        var agging1AVNET= parseFloat(agging1net[0])/parseFloat(agging1net[1]);
    }
    else
    {
        var agging1AVNET=0;
    }
    if(agging2net[0]!=0)
    {
        var agging2AVNET= parseFloat(agging2net[0])/parseFloat(agging2net[1]);
    }
    else
    {
        var agging2AVNET=0;
    }
    if(agging3net[0]!=0)
    {
        var agging3AVNET= parseFloat(agging3net[0])/parseFloat(agging3net[1]);
    }
    else
    {
        var agging3AVNET=0;
    }
    if(agging4net[0]!=0)
    {
        var agging4AVNET= parseFloat(agging4net[0])/parseFloat(agging4net[1]);
    }
    else
    {
        var agging4AVNET=0;
    }
    if(agging5net[0]!=0)
    {
        var agging5AVNET= parseFloat(agging5net[0])/parseFloat(agging5net[1]);
    }
    else
    {
        var agging5AVNET=0;
    }
    if(agging6net[0]!=0)
    {
        var agging6AVNET= parseFloat(agging6net[0])/parseFloat(agging6net[1]);
    }
    else
    {
        var agging6AVNET=0;
    }
    if(agging7net[0]!=0)
    {
        var agging7AVNET= parseFloat(agging7net[0])/parseFloat(agging7net[1]);
    }
    else
    {
        var agging7AVNET=0;
    }
    if(agging8net[0]!=0)
    {
        var agging8AVNET= parseFloat(agging8net[0])/parseFloat(agging8net[1]);
    }
    else
    {
        var agging8AVNET=0;
    }
    if(agging9net[0]!=0)
    {
        var agging9AVNET= parseFloat(agging9net[0])/parseFloat(agging9net[1]);
    }
    else
    {
        var agging9AVNET=0;
    }
    if(agging10net[0]!=0)
    {
        var agging10AVNET= parseFloat(agging10net[0])/parseFloat(agging10net[1]);
    }
    else
    {
        var agging10AVNET=0;
    }
    if(agging11net[0]!=0)
    {
        var agging11AVNET= parseFloat(agging11net[0])/parseFloat(agging11net[1]);
    }
    else
    {
        var agging11AVNET=0;
    }
    if(agging12net[0]!=0)
    {
        var agging12AVNET= parseFloat(agging12net[0])/parseFloat(agging12net[1]);
    }
    else
    {
        var agging12AVNET=0;
    }
    var arrReturn=[(agging1AV).toFixed(2)
        ,agging2AV.toFixed(2)
        ,agging3AV.toFixed(2)
        ,agging4AV.toFixed(2)
        ,agging5AV.toFixed(2),
        agging6AV.toFixed(2)
        ,agging7AV.toFixed(2)
        ,agging8AV.toFixed(2)
        ,agging9AV.toFixed(2)
        ,agging10AV.toFixed(2)
        ,agging11AV.toFixed(2)
        ,agging12AV.toFixed(2),
        agging1[1]
        ,agging2[1]
        ,agging3[1]
        ,agging4[1]
        ,agging5[1]
        ,agging6[1]
        ,agging7[1]
        ,agging8[1]
        ,agging9[1]
        ,agging10[1]
        ,agging11[1]
        ,agging12[1]
        ,agging1AVXC.toFixed(2)
        ,agging2AVXC.toFixed(2)
        ,agging3AVXC.toFixed(2)
        ,agging4AVXC.toFixed(2)
        ,agging5AVXC.toFixed(2),
        agging6AVXC.toFixed(2)
        ,agging7AVXC.toFixed(2)
        ,agging8AVXC.toFixed(2)
        ,agging9AVXC.toFixed(2)
        ,agging10AVXC.toFixed(2)
        ,agging11AVXC.toFixed(2)
        ,agging12AVXC.toFixed(2),
        agging1xc[1]
        ,agging2xc[1]
        ,agging3xc[1]
        ,agging4xc[1]
        ,agging5xc[1]
        ,agging6xc[1]
        ,agging7xc[1]
        ,agging8xc[1]
        ,agging9xc[1]
        ,agging10xc[1]
        ,agging11xc[1]
        ,agging12xc[1]
        ,agging1AVVXC.toFixed(2)
        ,agging2AVVXC.toFixed(2)
        ,agging3AVVXC.toFixed(2)
        ,agging4AVVXC.toFixed(2)
        ,agging5AVVXC.toFixed(2),
        agging6AVVXC.toFixed(2)
        ,agging7AVVXC.toFixed(2)
        ,agging8AVVXC.toFixed(2)
        ,agging9AVVXC.toFixed(2)
        ,agging10AVVXC.toFixed(2)
        ,agging11AVVXC.toFixed(2)
        ,agging12AVVXC.toFixed(2),
        agging1vxc[1]
        ,agging2vxc[1]
        ,agging3vxc[1]
        ,agging4vxc[1]
        ,agging5vxc[1]
        ,agging6vxc[1]
        ,agging7vxc[1]
        ,agging8vxc[1]
        ,agging9vxc[1]
        ,agging10vxc[1]
        ,agging11vxc[1]
        ,agging12vxc[1]
        ,agging1AVPOW.toFixed(2)
        ,agging2AVPOW.toFixed(2)
        ,agging3AVPOW.toFixed(2)
        ,agging4AVPOW.toFixed(2)
        ,agging5AVPOW.toFixed(2)
        ,agging6AVPOW.toFixed(2)
        ,agging7AVPOW.toFixed(2)
        ,agging8AVPOW.toFixed(2)
        ,agging9AVPOW.toFixed(2)
        ,agging10AVPOW.toFixed(2)
        ,agging11AVPOW.toFixed(2)
        ,agging12AVPOW.toFixed(2),
        agging1pow[1]
        ,agging2pow[1]
        ,agging3pow[1]
        ,agging4pow[1]
        ,agging5pow[1]
        ,agging6pow[1]
        ,agging7pow[1]
        ,agging8pow[1]
        ,agging9pow[1]
        ,agging10pow[1]
        ,agging11pow[1]
        ,agging12pow[1]
        ,agging1AVNET.toFixed(2)
        ,agging2AVNET.toFixed(2)
        ,agging3AVNET.toFixed(2)
        ,agging4AVNET.toFixed(2)
        ,agging5AVNET.toFixed(2),
        agging6AVNET.toFixed(2)
        ,agging7AVNET.toFixed(2)
        ,agging8AVNET.toFixed(2)
        ,agging9AVNET.toFixed(2)
        ,agging10AVNET.toFixed(2)
        ,agging11AVNET.toFixed(2)
        ,agging12AVNET.toFixed(2),
        agging1net[1]
        ,agging2net[1]
        ,agging3net[1]
        ,agging4net[1]
        ,agging5net[1]
        ,agging6net[1]
        ,agging7net[1]
        ,agging8net[1]
        ,agging9net[1]
        ,agging10net[1]
        ,agging11net[1]
        ,agging12net[1]];
    return arrReturn;
}
function return_weeksforSearch (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 25; i++ ) {
        arrWeeks.push((moment().subtract(i, 'weeks').startOf('isoWeek').format('MM/DD/YYYY')));
        arrWeeks.push((moment().subtract(i, 'weeks').endOf('isoWeek').format('MM/DD/YYYY')));
    }
    return arrWeeks;
}
function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}

function return_weeks (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 13; i++ ) {
        arrWeeks.push(moment().endOf('week').add(1, 'days').subtract((13 - i), 'week').format('M/D'));
    }
    return JSON.stringify(arrWeeks);
}