nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_OPS_KPI_Installations_Locations.js
//	Script Name:	CLGX_SL_OPS_KPI_Installations_Locations
//	Script Id:		customscript_clgx_sl_ops_kpi_inst_loc
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		06/24/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=510&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_ops_kpi_inst_loc (request, response){
    try {

        var script = request.getParameter('script');
        var type = request.getParameter('type');
        var category = request.getParameter('category');
        var tarw=0;
        var tarv=0;

        var chartype = 'column';
        if(type == 'avg'){
            chartype = 'spline';
            tarw=1;
        }


        var title = request.getParameter('title');
        var location = request.getParameter('location');
        var cattype=request.getParameter('cattype');
        var product = request.getParameter('product');
        var order = request.getParameter('order');
        var titleav="Standard";
        if(cattype=='standard')
        {
            titleav="Non-Standard";
        }
        if(cattype=='standard')
        {
            if(product=="space"||product=="power"||product=="network")
            {
                tarv=5;
            }
            if(product=="xc")
            {
                tarv=2;
            }
        }

        if(type=='avg')
        {
            var objFile = nlapiLoadFile(3042679);
        }
        else
        {
            var objFile = nlapiLoadFile(2743411);
        }
        var html = objFile.getValue();
        html = html.replace(new RegExp('{weeks}','g'), return_weeks());
        html = html.replace(new RegExp('{chartype}','g'), chartype);
        html = html.replace(new RegExp('{averagetitle}','g'), titleav);
        html = html.replace(new RegExp('{targetwidth}','g'), tarw);
        html = html.replace(new RegExp('{targetvalue}','g'), tarv);
        html = html.replace(new RegExp('{title}','g'), 'Installation Intervals - ' + title);
        if(title=='Standard'){
            title='ALL';
        }
        if(title=='By Products'){
            title='ALL';
        }
        if(title=='Non-Standard'){
            title='ALL';
        }

        html = html.replace(new RegExp('{series}','g'), return_series(title,cattype,type,order));

        response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function return_series(title,cattype,type,order){
    var objFile = nlapiLoadFile(2910583);
    var fileValue=objFile.getValue();
    var json=JSON.parse(fileValue);
    var tl=title;
    if(cattype=='nstandard')
    {

        if(type=='vol')
        {

            var seriesString=json.series.products[1];
            var seriesInt=seriesString.nstandard[order];
            var series=return_seriesFinalVol (seriesInt,title);
        }
        else
        {
            if(type=='avg')
            {
                var seriesString=json.series.products[1];
                var seriesInt=seriesString.nstandard[order];
                var series=return_seriesFinalAvg (seriesInt,title);
            }
        }

    }
    if(cattype=='all')
    {

        if(type=='vol')
        {

            var seriesString=json.series.products[2];
            var seriesInt=seriesString.all[order];
            var series=return_seriesFinalVol (seriesInt,title);
        }
        else
        {
            if(type=='avg')
            {
                var seriesString=json.series.products[2];
                var seriesInt=seriesString.all[order];
                var series=return_seriesFinalAvg (seriesInt,title);
            }
        }

    }
    if(cattype=='standard')
    {
        if(type=='vol')
        {

            var seriesString=json.series.products[0];
            var seriesInt=seriesString.standard[order];
            var series=return_seriesFinalVol (seriesInt,title);
        }
        else
        {
            if(type=='avg')
            {
                var seriesString=json.series.products[0];
                var seriesInt=seriesString.standard[order];
                var series=return_seriesFinalAvg (seriesInt,title);
            }
        }
    }
    if(type=='vol')
    {
        var ind=0;
    }
    if(type=='avg')
    {
        var ind=1;
    }


    return series;
}
function return_seriesFinalVol (json,title){
    var series='';
    switch (title) {
        case 'ALL':
            series=json.ALL[0];
            break;
        case 'Space':
            series=json.Space[0];
            break;
        case 'XC':
            series=json.XC[0];
            break;
        case 'Virtual XC':
            series=json.VXC[0];
            break;
        case 'Network':
            series=json.Network[0];
            break;
        case 'Power':
            series=json.Power[0];
            break;

    }
    return series;

}
function return_seriesFinalAvg (json,title){
    var series='';
    switch (title) {
        case 'ALL':
            series=json.ALL[1];
            break;
        case 'Space':
            series=json.Space[1];
            break;
        case 'XC':
            series=json.XC[1];
            break;
        case 'Virtual XC':
            series=json.VXC[1];
            break;
        case 'Network':
            series=json.Network[1];
            break;
        case 'Power':
            series=json.Power[1];
            break;

    }

    return series;
}

function return_weeks (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 13; i++ ) {
        arrWeeks.push(moment().endOf('week').add(1, 'days').subtract((13 - i), 'week').format('M/D'));
    }
    return JSON.stringify(arrWeeks);
}