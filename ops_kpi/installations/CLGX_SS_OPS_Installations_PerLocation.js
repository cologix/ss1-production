nlapiLogExecution("audit","FLOStart",new Date().getTime());
//	Released:		July/2015
//-------------------------------------------------------------------------------------------------

function scheduled_create_averageStandard_json () {
    try {
        nlapiLogExecution('DEBUG','Scheduled Script - Create_averageStandard_json','|--------------------STARTED---------------------|');
        //------------- Begin Section 1 -----------------------------------------------------------------------------------
        // Version:	1.1 - 2/8/2012
        // Details:	Creates 4 types of audit cases on the 1st of every month for each facility
        //-----------------------------------------------------------------------------------------------------------------
        var arrAvgSeriesAll=getAvgSeriesAll();
        var arrAvgSeries=getAvgSeries();
        //var arrAvgSeriesNFile=nlapiLoadFile(2903726);
        var arrAvgSeriesN=getAvgSeriesN();
        // var arrAvgSeriesAllFile=nlapiLoadFile(2906967);

        var seriesForJSON='{"series": {"products": [{"standard":[ '+arrAvgSeries+' ]},{"nstandard":[ '+arrAvgSeriesN+' ]},{"all":[ '+arrAvgSeriesAll+' ]}]}}';
        //  var str = JSON.stringify(seriesForJSON);
        var fileName = nlapiCreateFile('installationSeriesProduct.json', 'PLAINTEXT', seriesForJSON);
        fileName.setFolder(2275492);
        nlapiSubmitFile(fileName);
        nlapiLogExecution('DEBUG','Scheduled Script - Create_averageStandard_json','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
function return_weeksDates (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 13; i++ ) {
        arrWeeks.push(moment().startOf('week').add(1, 'days').subtract((13 - i), 'week').format('MM/DD/YYYY'));
    }
    var arrayDates=[arrWeeks[0],arrWeeks[arrWeeks.length-1]]
    return arrayDates;
}
function return_weeksforGrid (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 13; i++ ) {
        arrWeeks.push(moment().startOf('week').add(1, 'days').subtract((13 - i), 'week').format('MM/DD/YYYY'));
    }
    return arrWeeks;
}
function addDays(theDate, days) {
    theDate=new Date(theDate);
    return new Date(theDate.getTime() + days*24*60*60*1000);
}
function returnDateStringDT(date){
    var to=addDays(date, 6);
    var dd = to.getDate();
    var mm = to.getMonth() + 1;
    var y = to.getFullYear();
    var toString = mm+ '/'+ dd + '/'+ y;
    return toString;
}
function returnDateString(location,from){
    var toString =returnDateStringDT(from);
    var arrFilters=new Array();
    if(location!=0)
    {
        var arrLocation=clgx_return_child_locations_of_marketid(location);
        if(arrLocation.length==0)
        {
            arrLocation.push(location);
        }
        arrFilters.push(new nlobjSearchFilter("location",null,"anyof",arrLocation));
    }
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from,toString));
    return arrFilters;
}

function getVolumeSeriesStandard (catitem,catstring1){
    var series='';
    var arrFilters = new Array();
    var arrColumns = new Array();
    var countSpaceArr=new Array();
    var countNetworkArr=new Array();
    var countPowerArr=new Array();
    var countXCArr=new Array();
    var countVXCArr=new Array();
    var countSpaceSTArr=[0,0,0,0,0,0,0,0,0,0,0,0];
    var countNetworSTkArr=new Array();
    var countPowerSTArr=new Array();
    var countXCSTArr=new Array();
    var countVXSTCArr=new Array();
    var locationColumbusArr=[];
    var locationDallasArr=[];
    var locationJacksonvilleArr=[];
    var locationJacksonville2Arr=[];
    var locationLakelandArr=[];
    var locationMinneapolisArr=[];
    var locationMontrealArr=[];
    var locationTorontoArr=[];
    var locationVancouverArr=[];
    var returnFunctionDates=return_weeksDates();
    var returnFunctionDatesGrid=return_weeksforGrid();
    var to=addDays(returnFunctionDates[1], 6);
    var dd = to.getDate();
    var mm = to.getMonth() + 1;
    var y = to.getFullYear();
    var toString = mm+ '/'+ dd + '/'+ y;
    var catstring=catstring1[0];
    if(catstring1.length>1){
        catstring="all";
    }
    var arrLocations=['Columbus','Dallas','Jacksonville','Jacksonville2','Lakeland','Minneapolis ','Montreal','Toronto','Vancouver'];    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",returnFunctionDates[0],toString));
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",returnFunctionDates[0],toString));


    var countColW12=0;
    var countColW11=0;
    var countColW10=0;
    var countColW9=0;
    var countColW8=0;
    var countColW7=0;
    var countColW6=0;
    var countColW5=0;
    var countColW4=0;
    var countColW3=0;
    var countColW2=0;
    var countColW1=0;
    //Dal
    var countDalW12=0;
    var countDalW11=0;
    var countDalW10=0;
    var countDalW9=0;
    var countDalW8=0;
    var countDalW7=0;
    var countDalW6=0;
    var countDalW5=0;
    var countDalW4=0;
    var countDalW3=0;
    var countDalW2=0;
    var countDalW1=0;

    //Jac
    var countJacW12=0;
    var countJacW11=0;
    var countJacW10=0;
    var countJacW9=0;
    var countJacW8=0;
    var countJacW7=0;
    var countJacW6=0;
    var countJacW5=0;
    var countJacW4=0;
    var countJacW3=0;
    var countJacW2=0;
    var countJacW1=0;
    //Jac2
    var countJac2W12=0;
    var countJac2W11=0;
    var countJac2W10=0;
    var countJac2W9=0;
    var countJac2W8=0;
    var countJac2W7=0;
    var countJac2W6=0;
    var countJac2W5=0;
    var countJac2W4=0;
    var countJac2W3=0;
    var countJac2W2=0;
    var countJac2W1=0;
    //Jac2
    var countLakW12=0;
    var countLakW11=0;
    var countLakW10=0;
    var countLakW9=0;
    var countLakW8=0;
    var countLakW7=0;
    var countLakW6=0;
    var countLakW5=0;
    var countLakW4=0;
    var countLakW3=0;
    var countLakW2=0;
    var countLakW1=0;
    //Min
    var countMinW12=0;
    var countMinW11=0;
    var countMinW10=0;
    var countMinW9=0;
    var countMinW8=0;
    var countMinW7=0;
    var countMinW6=0;
    var countMinW5=0;
    var countMinW4=0;
    var countMinW3=0;
    var countMinW2=0;
    var countMinW1=0;
    //Mtl
    var countMtlW12=0;
    var countMtlW11=0;
    var countMtlW10=0;
    var countMtlW9=0;
    var countMtlW8=0;
    var countMtlW7=0;
    var countMtlW6=0;
    var countMtlW5=0;
    var countMtlW4=0;
    var countMtlW3=0;
    var countMtlW2=0;
    var countMtlW1=0;
    //Tor
    var countTorW12=0;
    var countTorW11=0;
    var countTorW10=0;
    var countTorW9=0;
    var countTorW8=0;
    var countTorW7=0;
    var countTorW6=0;
    var countTorW5=0;
    var countTorW4=0;
    var countTorW3=0;
    var countTorW2=0;
    var countTorW1=0;
    //Van
    var countVanW12=0;
    var countVanW11=0;
    var countVanW10=0;
    var countVanW9=0;
    var countVanW8=0;
    var countVanW7=0;
    var countVanW6=0;
    var countVanW5=0;
    var countVanW4=0;
    var countVanW3=0;
    var countVanW2=0;
    var countVanW1=0;
    //Non-Standard
    //Van
    var NcountVanW12=0;
    var NcountVanW11=0;
    var NcountVanW10=0;
    var NcountVanW9=0;
    var NcountVanW8=0;
    var NcountVanW7=0;
    var NcountVanW6=0;
    var NcountVanW5=0;
    var NcountVanW4=0;
    var NcountVanW3=0;
    var NcountVanW2=0;
    var NcountVanW1=0;

    var from12=returnFunctionDatesGrid[0];
    var to12=addDays(from12,6);
    var dd12 = to12.getDate();
    var mm12 = to12.getMonth() + 1;
    var y12 = to12.getFullYear();
    var toString12 = mm12+ '/'+ dd12 + '/'+ y12;
    //week11
    var from11=returnFunctionDatesGrid[1];
    var to11=addDays(from11,6);
    var dd11 = to11.getDate();
    var mm11 = to11.getMonth() + 1;
    var y11 = to11.getFullYear();
    var toString11 = mm11+ '/'+ dd11 + '/'+ y11;
    //week10
    var from10=returnFunctionDatesGrid[2];
    var to10=addDays(from10,6);
    var dd10 = to10.getDate();
    var mm10 = to10.getMonth() + 1;
    var y10 = to10.getFullYear();
    var toString10 = mm10+ '/'+ dd10 + '/'+ y10;
    //week9
    var from9=returnFunctionDatesGrid[3];
    var to9=addDays(from9,6);
    var dd9 = to9.getDate();
    var mm9 = to9.getMonth() + 1;
    var y9 = to9.getFullYear();
    var toString9 = mm9+ '/'+ dd9 + '/'+ y9;
    //week8
    var from8=returnFunctionDatesGrid[4];
    var to8=addDays(from8,6);
    var dd8 = to8.getDate();
    var mm8 = to8.getMonth() + 1;
    var y8 = to8.getFullYear();
    var toString8 = mm8+ '/'+ dd8 + '/'+ y8;
    //week7
    var from7=returnFunctionDatesGrid[5];
    var to7=addDays(from7,6);
    var dd7 = to7.getDate();
    var mm7 = to7.getMonth() + 1;
    var y7 = to7.getFullYear();
    var toString7 = mm7+ '/'+ dd7 + '/'+ y7;
    //week6
    var from6=returnFunctionDatesGrid[6];
    var to6=addDays(from6,6);
    var dd6 = to6.getDate();
    var mm6 = to6.getMonth() + 1;
    var y6 = to6.getFullYear();
    var toString6 = mm6+ '/'+ dd6 + '/'+ y6;
    //week5
    var from5=returnFunctionDatesGrid[7];
    var to5=addDays(from5,6);
    var dd5 = to5.getDate();
    var mm5 = to5.getMonth() + 1;
    var y5 = to5.getFullYear();
    var toString5 = mm5+ '/'+ dd5 + '/'+ y5;
    //week4
    var from4=returnFunctionDatesGrid[8];
    var to4=addDays(from4,6);
    var dd4 = to4.getDate();
    var mm4 = to4.getMonth() + 1;
    var y4 = to4.getFullYear();
    var toString4 = mm4+ '/'+ dd4 + '/'+ y4;
    //week3
    var from3=returnFunctionDatesGrid[9];
    var to3=addDays(from3,6);
    var dd3 = to3.getDate();
    var mm3 = to3.getMonth() + 1;
    var y3 = to3.getFullYear();
    var toString3 = mm3+ '/'+ dd3 + '/'+ y3;
    //week2
    var from2=returnFunctionDatesGrid[10];
    var to2=addDays(from2,6);
    var dd2 = to2.getDate();
    var mm2 = to2.getMonth() + 1;
    var y2 = to2.getFullYear();
    var toString2 = mm2+ '/'+ dd2 + '/'+ y2;
    //week1
    var from1=returnFunctionDatesGrid[11];
    var to1=addDays(from1,6);
    var dd1 = to1.getDate();
    var mm1 = to1.getMonth() + 1;
    var y1 = to1.getFullYear();
    var toString1 = mm1+ '/'+ dd1 + '/'+ y1;
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_so_install_vol_loc_s_4', arrFilters, arrColumns);
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var cat = searchIn.getText(columns[0]);
        var date= new Date(searchIn.getValue(columns[2]));
        var nstandard=searchIn.getValue(columns[3]);
        var itemcat=searchIn.getValue(columns[4]);
        var itemcatArr=itemcat.split(',');
        if((in_array(catitem,itemcatArr))&&(catitem!='all'))
        {

            if((cat=='Columbus')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countColW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countColW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countColW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countColW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countColW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countColW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countColW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countColW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countColW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countColW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countColW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countColW12++;
                }
            }
            if((cat=='Dallas')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countDalW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countDalW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countDalW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countDalW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countDalW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countDalW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countDalW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countDalW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countDalW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countDalW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countDalW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countDalW12++;
                }
            }
            if((cat=='Jacksonville')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countJacW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countJacW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countJacW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countJacW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countJacW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countJacW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countJacW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countJacW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countJacW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countJacW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countJacW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countJacW12++;
                }
            }
            if((cat=='Jacksonville2')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countJac2W1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countJac2W2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countJac2W3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countJac2W4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countJac2W5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countJac2W6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countJac2W7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countJac2W8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countJac2W9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countJac2W10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countJac2W11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countJac2W12++;
                }
            }
            if((cat=='Lakeland')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countLakW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countLakW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countLakW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countLakW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countLakW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countLakW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countLakW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countLakW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countLakW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countLakW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countLakW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countLakW12++;
                }
            }
            if((cat=='Minneapolis ')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countMinW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countMinW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countMinW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countMinW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countMinW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countMinW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countMinW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countMinW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countMinW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countMinW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countMinW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countMinW12++;
                }
            }
            if((cat=='Montreal')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countMtlW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countMtlW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countMtlW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countMtlW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countMtlW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countMtlW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countMtlW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countMtlW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countMtlW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countMtlW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countMtlW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countMtlW12++;
                }
            }
            if((cat=='Toronto')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countTorW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countTorW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countTorW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countTorW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countTorW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countTorW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countTorW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countTorW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countTorW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countTorW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countTorW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countTorW12++;
                }
            }
            if((cat=='Vancouver')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countVanW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countVanW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countVanW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countVanW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countVanW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countVanW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countVanW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countVanW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countVanW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countVanW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countVanW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countVanW12++;
                }
            }
        } else if(catitem=="all")
        {
            if((cat=='Columbus')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countColW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countColW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countColW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countColW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countColW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countColW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countColW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countColW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countColW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countColW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countColW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countColW12++;
                }
            }
            if((cat=='Dallas')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countDalW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countDalW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countDalW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countDalW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countDalW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countDalW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countDalW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countDalW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countDalW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countDalW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countDalW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countDalW12++;
                }
            }
            if((cat=='Jacksonville')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countJacW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countJacW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countJacW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countJacW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countJacW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countJacW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countJacW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countJacW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countJacW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countJacW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countJacW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countJacW12++;
                }
            }
            if((cat=='Jacksonville2')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countJac2W1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countJac2W2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countJac2W3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countJac2W4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countJac2W5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countJac2W6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countJac2W7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countJac2W8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countJac2W9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countJac2W10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countJac2W11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countJac2W12++;
                }
            }
            if((cat=='Lakeland')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countLakW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countLakW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countLakW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countLakW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countLakW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countLakW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countLakW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countLakW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countLakW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countLakW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countLakW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countLakW12++;
                }
            }
            if((cat=='Minneapolis ')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countMinW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countMinW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countMinW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countMinW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countMinW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countMinW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countMinW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countMinW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countMinW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countMinW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countMinW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countMinW12++;
                }
            }
            if((cat=='Montreal')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countMtlW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countMtlW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countMtlW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countMtlW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countMtlW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countMtlW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countMtlW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countMtlW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countMtlW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countMtlW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countMtlW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countMtlW12++;
                }
            }
            if((cat=='Toronto')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countTorW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countTorW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countTorW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countTorW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countTorW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countTorW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countTorW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countTorW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countTorW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countTorW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countTorW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countTorW12++;
                }
            }
            if((cat=='Vancouver')&&(nstandard==0))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countVanW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countVanW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countVanW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countVanW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countVanW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countVanW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countVanW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countVanW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countVanW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countVanW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countVanW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countVanW12++;
                }
            }
        }
    }


    locationColumbusArr=[countColW1,countColW2,countColW3,countColW4,countColW5,countColW6,countColW7,countColW8,countColW9,countColW10,countColW11,countColW12];
    locationDallasArr=[countDalW1,countDalW2,countDalW3,countDalW4,countDalW5,countDalW6,countDalW7,countDalW8,countDalW9,countDalW10,countDalW11,countDalW12];
    locationJacksonvilleArr=[countJacW1,countJacW2,countJacW3,countJacW4,countJacW5,countJacW6,countJacW7,countJacW8,countJacW9,countJacW10,countJacW11,countJacW12];
    locationJacksonville2Arr=[countJac2W1,countJac2W2,countJac2W3,countJac2W4,countJac2W5,countJac2W6,countJac2W7,countJac2W8,countJac2W9,countJac2W10,countJac2W11,countJac2W12];
    locationLakelandArr=[countLakW1,countLakW2,countLakW3,countLakW4,countLakW5,countLakW6,countLakW7,countLakW8,countLakW9,countLakW10,countLakW11,countLakW12];
    locationMinneapolisArr=[countMinW1,countMinW2,countMinW3,countMinW4,countMinW5,countMinW6,countMinW7,countMinW8,countMinW9,countMinW10,countMinW11,countMinW12];
    locationMontrealArr=[countMtlW1,countMtlW2,countMtlW3,countMtlW4,countMtlW5,countMtlW6,countMtlW7,countMtlW8,countMtlW9,countMtlW10,countMtlW11,countMtlW12];
    locationTorontoArr=[countTorW1,countTorW2,countTorW3,countTorW4,countTorW5,countTorW6,countTorW7,countTorW8,countTorW9,countTorW10,countTorW11,countTorW12];
    locationVancouverArr=[countVanW1,countVanW2,countVanW3,countVanW4,countVanW5,countVanW6,countVanW7,countVanW8,countVanW9,countVanW10,countVanW11,countVanW12];

    //space
    if(locationColumbusArr.length>0)
    {
        series +=  "{name: 'Columbus',type: 'column',data: [{y:"+locationColumbusArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationColumbusArr[10] +",url:'/app/site/hosting/script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationColumbusArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationColumbusArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationColumbusArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationColumbusArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=1&week=="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationColumbusArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationColumbusArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationColumbusArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationColumbusArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationColumbusArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationColumbusArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    //network
    if(locationDallasArr.length>0)
    {
        series +=  "{name: 'Dallas',type: 'column',data: [{y:"+locationDallasArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationDallasArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationDallasArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationDallasArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationDallasArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationDallasArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationDallasArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationDallasArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationDallasArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationDallasArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationDallasArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationDallasArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    //x inter
    if(locationJacksonvilleArr.length>0)
    {
        series +=  "{name: 'Jacksonville',type: 'column',data: [{y:"+locationJacksonvilleArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationJacksonvilleArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationJacksonvilleArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationJacksonvilleArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationJacksonvilleArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationJacksonvilleArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationJacksonvilleArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationJacksonvilleArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationJacksonvilleArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationJacksonvilleArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationJacksonvilleArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationJacksonvilleArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    if(locationJacksonville2Arr.length>0)
    {
        series +=  "{name: 'Jacksonville2',type: 'column',data: [{y:"+locationJacksonville2Arr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationJacksonville2Arr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationJacksonville2Arr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationJacksonville2Arr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationJacksonville2Arr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationJacksonville2Arr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationJacksonville2Arr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationJacksonville2Arr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationJacksonville2Arr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationJacksonville2Arr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationJacksonville2Arr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationJacksonville2Arr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }


    //inter
    if(locationLakelandArr.length>0)
    {
        series +=  "{name: 'Lakeland',type: 'column',data: [{y:"+locationLakelandArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationLakelandArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationLakelandArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationLakelandArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationLakelandArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationLakelandArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationLakelandArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationLakelandArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationLakelandArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationLakelandArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationLakelandArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationLakelandArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    if(locationMinneapolisArr.length>0)
    {
        series +=  "{name: 'Minneapolis',type: 'column',data: [{y:"+locationMinneapolisArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationMinneapolisArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationMinneapolisArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationMinneapolisArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationMinneapolisArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationMinneapolisArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationMinneapolisArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationMinneapolisArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationMinneapolisArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationMinneapolisArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationMinneapolisArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationMinneapolisArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    //power
    if(locationMontrealArr.length>0)
    {
        series +=  "{name: 'Montreal',type: 'column',data: [{y:"+locationMontrealArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationMontrealArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationMontrealArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationMontrealArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationMontrealArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationMontrealArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationMontrealArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationMontrealArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationMontrealArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationMontrealArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationMontrealArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationMontrealArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    if(locationTorontoArr.length>0)
    {
        series +=  "{name: 'Toronto',type: 'column',data: [{y:"+locationTorontoArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationTorontoArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationTorontoArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationTorontoArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationTorontoArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationTorontoArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationTorontoArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationTorontoArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationTorontoArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationTorontoArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationTorontoArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationTorontoArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    if(locationVancouverArr.length>0)
    {
        series +=  "{name: 'Vancouver',type: 'column',data: [{y:"+locationVancouverArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationVancouverArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationVancouverArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationVancouverArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationVancouverArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationVancouverArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationVancouverArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationVancouverArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationVancouverArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationVancouverArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationVancouverArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationVancouverArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=1&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    var searchSt = nlapiSearchRecord('transaction', 'customsearch_clgx_so_install_vol_loc_s_5', arrFilters, arrColumns);
    var countW1=0;
    var countW2=0;
    var countW3=0;
    var countW4=0;
    var countW5=0;
    var countW6=0;
    var countW7=0;
    var countW8=0;
    var countW9=0;
    var countW10=0;
    var countW11=0;
    var countW12=0;
    for ( var i = 0; searchSt != null && i < searchSt.length; i++ ) {
        var searchIn = searchSt[i];
        var columns = searchIn.getAllColumns();
        var cat = searchIn.getText(columns[0]);
        var date = new Date(searchIn.getValue(columns[2]));
        var nonValue = searchIn.getValue(columns[3]);
        var stValue = searchIn.getValue(columns[4]);
        var itemcat=searchIn.getValue(columns[5]);
        var itemcatArr=itemcat.split(',');
        if((in_array(catitem,itemcatArr))&&(catitem!='all'))
        {
            if(((date >= new Date(from1)) && (date <= new Date(toString1)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW1++;
            }
            if(((date >=new Date( from2)) && (date <= new Date(toString2)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW2++;
            }
            if(((date >=new Date( from3)) && (date <= new Date(toString3)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW3++;
            }
            if(((date >=new Date( from4)) && (date <= new Date(toString4)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW4++;
            }
            if(((date >=new Date( from5)) && (date <= new Date(toString5)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW5++;
            }
            if(((date >=new Date( from6)) && (date <= new Date(toString6)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW6++;
            }
            if(((date >=new Date( from7)) && (date <= new Date(toString7)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW7++;
            }
            if(((date >=new Date( from8)) && (date <= new Date(toString8)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW8++;
            }
            if(((date >=new Date(from9)) && (date <= new Date(toString9)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW9++;
            }
            if(((date >=new Date(from10)) && (date <= new Date(toString10)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW10++;
            }
            if(((date >=new Date(from11)) && (date <= new Date(toString11)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW11++;
            }
            if(((date >=new Date(from12)) && (date <= new Date(toString12)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW12++;
            }
        } else if(catitem=="all"){
            if(((date >= new Date(from1)) && (date <= new Date(toString1)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW1++;
            }
            if(((date >=new Date( from2)) && (date <= new Date(toString2)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW2++;
            }
            if(((date >=new Date( from3)) && (date <= new Date(toString3)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW3++;
            }
            if(((date >=new Date( from4)) && (date <= new Date(toString4)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW4++;
            }
            if(((date >=new Date( from5)) && (date <= new Date(toString5)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW5++;
            }
            if(((date >=new Date( from6)) && (date <= new Date(toString6)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW6++;
            }
            if(((date >=new Date( from7)) && (date <= new Date(toString7)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW7++;
            }
            if(((date >=new Date( from8)) && (date <= new Date(toString8)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW8++;
            }
            if(((date >=new Date(from9)) && (date <= new Date(toString9)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW9++;
            }
            if(((date >=new Date(from10)) && (date <= new Date(toString10)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW10++;
            }
            if(((date >=new Date(from11)) && (date <= new Date(toString11)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW11++;
            }
            if(((date >=new Date(from12)) && (date <= new Date(toString12)))&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                NcountVanW12++;
            }
        }
    }

    series +=  " {name: 'Non-Standard',type: 'spline','visible': false, color: '#0000CC',data: [{y:"+NcountVanW12+",url:'#'}," +
        "{y:"+NcountVanW11+",url:'#'}," +
        "{y:"+NcountVanW10+",url:'#'}," +
        "{y:"+NcountVanW9+",url:'#'}," +
        "{y:"+NcountVanW8+",url:'#'}," +
        "{y:"+NcountVanW7+",url:'#'}," +
        "{y:"+NcountVanW6+",url:'#'}," +
        "{y:"+NcountVanW5+",url:'#'}," +
        "{y:"+NcountVanW4+",url:'#'}," +
        "{y:"+NcountVanW3+",url:'#'}," +
        "{y:"+NcountVanW2+",url:'#'}," +
        "{y:"+NcountVanW1+",url:'#'}]}";



    return series;
}

function getVolumeAllSeries (cat){
    var series='';
    var arrFilters = new Array();
    var arrColumns = new Array();
    var countSpaceArr=new Array();
    var countNetworkArr=new Array();
    var countPowerArr=new Array();
    var countXCArr=new Array();
    var countVXCArr=new Array();
    var countSpaceSTArr=[0,0,0,0,0,0,0,0,0,0,0,0];
    var countNetworSTkArr=new Array();
    var countPowerSTArr=new Array();
    var countXCSTArr=new Array();
    var countVXSTCArr=new Array();
    var locationColumbusArr=[];
    var locationDallasArr=[];
    var locationJacksonvilleArr=[];
    var locationJacksonville2Arr=[];
    var locationLakelandArr=[];
    var locationMinneapolisArr=[];
    var locationMontrealArr=[];
    var locationTorontoArr=[];
    var locationVancouverArr=[];
    var returnFunctionDates=return_weeksDates();
    var returnFunctionDatesGrid=return_weeksforGrid();
    var to=addDays(returnFunctionDates[1], 6);
    var dd = to.getDate();
    var mm = to.getMonth() + 1;
    var y = to.getFullYear();
    var toString = mm+ '/'+ dd + '/'+ y;
    var  catstring=cat[0];
    if(cat.length>1)
    {
        catstring='all';
    }

    var arrLocations=['Columbus','Dallas','Jacksonville','Jacksonville2','Lakeland','Minneapolis ','Montreal','Toronto','Vancouver'];    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",returnFunctionDates[0],toString));
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",returnFunctionDates[0],toString));
    arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category","item","anyof",cat));

    var countColW12=0;
    var countColW11=0;
    var countColW10=0;
    var countColW9=0;
    var countColW8=0;
    var countColW7=0;
    var countColW6=0;
    var countColW5=0;
    var countColW4=0;
    var countColW3=0;
    var countColW2=0;
    var countColW1=0;
    //Dal
    var countDalW12=0;
    var countDalW11=0;
    var countDalW10=0;
    var countDalW9=0;
    var countDalW8=0;
    var countDalW7=0;
    var countDalW6=0;
    var countDalW5=0;
    var countDalW4=0;
    var countDalW3=0;
    var countDalW2=0;
    var countDalW1=0;

    //Jac
    var countJacW12=0;
    var countJacW11=0;
    var countJacW10=0;
    var countJacW9=0;
    var countJacW8=0;
    var countJacW7=0;
    var countJacW6=0;
    var countJacW5=0;
    var countJacW4=0;
    var countJacW3=0;
    var countJacW2=0;
    var countJacW1=0;
    //Jac2
    var countJac2W12=0;
    var countJac2W11=0;
    var countJac2W10=0;
    var countJac2W9=0;
    var countJac2W8=0;
    var countJac2W7=0;
    var countJac2W6=0;
    var countJac2W5=0;
    var countJac2W4=0;
    var countJac2W3=0;
    var countJac2W2=0;
    var countJac2W1=0;
    //Jac2
    var countLakW12=0;
    var countLakW11=0;
    var countLakW10=0;
    var countLakW9=0;
    var countLakW8=0;
    var countLakW7=0;
    var countLakW6=0;
    var countLakW5=0;
    var countLakW4=0;
    var countLakW3=0;
    var countLakW2=0;
    var countLakW1=0;
    //Min
    var countMinW12=0;
    var countMinW11=0;
    var countMinW10=0;
    var countMinW9=0;
    var countMinW8=0;
    var countMinW7=0;
    var countMinW6=0;
    var countMinW5=0;
    var countMinW4=0;
    var countMinW3=0;
    var countMinW2=0;
    var countMinW1=0;
    //Mtl
    var countMtlW12=0;
    var countMtlW11=0;
    var countMtlW10=0;
    var countMtlW9=0;
    var countMtlW8=0;
    var countMtlW7=0;
    var countMtlW6=0;
    var countMtlW5=0;
    var countMtlW4=0;
    var countMtlW3=0;
    var countMtlW2=0;
    var countMtlW1=0;
    //Tor
    var countTorW12=0;
    var countTorW11=0;
    var countTorW10=0;
    var countTorW9=0;
    var countTorW8=0;
    var countTorW7=0;
    var countTorW6=0;
    var countTorW5=0;
    var countTorW4=0;
    var countTorW3=0;
    var countTorW2=0;
    var countTorW1=0;
    //Van
    var countVanW12=0;
    var countVanW11=0;
    var countVanW10=0;
    var countVanW9=0;
    var countVanW8=0;
    var countVanW7=0;
    var countVanW6=0;
    var countVanW5=0;
    var countVanW4=0;
    var countVanW3=0;
    var countVanW2=0;
    var countVanW1=0;


    var from12=returnFunctionDatesGrid[0];
    var to12=addDays(from12,6);
    var dd12 = to12.getDate();
    var mm12 = to12.getMonth() + 1;
    var y12 = to12.getFullYear();
    var toString12 = mm12+ '/'+ dd12 + '/'+ y12;
    //week11
    var from11=returnFunctionDatesGrid[1];
    var to11=addDays(from11,6);
    var dd11 = to11.getDate();
    var mm11 = to11.getMonth() + 1;
    var y11 = to11.getFullYear();
    var toString11 = mm11+ '/'+ dd11 + '/'+ y11;
    //week10
    var from10=returnFunctionDatesGrid[2];
    var to10=addDays(from10,6);
    var dd10 = to10.getDate();
    var mm10 = to10.getMonth() + 1;
    var y10 = to10.getFullYear();
    var toString10 = mm10+ '/'+ dd10 + '/'+ y10;
    //week9
    var from9=returnFunctionDatesGrid[3];
    var to9=addDays(from9,6);
    var dd9 = to9.getDate();
    var mm9 = to9.getMonth() + 1;
    var y9 = to9.getFullYear();
    var toString9 = mm9+ '/'+ dd9 + '/'+ y9;
    //week8
    var from8=returnFunctionDatesGrid[4];
    var to8=addDays(from8,6);
    var dd8 = to8.getDate();
    var mm8 = to8.getMonth() + 1;
    var y8 = to8.getFullYear();
    var toString8 = mm8+ '/'+ dd8 + '/'+ y8;
    //week7
    var from7=returnFunctionDatesGrid[5];
    var to7=addDays(from7,6);
    var dd7 = to7.getDate();
    var mm7 = to7.getMonth() + 1;
    var y7 = to7.getFullYear();
    var toString7 = mm7+ '/'+ dd7 + '/'+ y7;
    //week6
    var from6=returnFunctionDatesGrid[6];
    var to6=addDays(from6,6);
    var dd6 = to6.getDate();
    var mm6 = to6.getMonth() + 1;
    var y6 = to6.getFullYear();
    var toString6 = mm6+ '/'+ dd6 + '/'+ y6;
    //week5
    var from5=returnFunctionDatesGrid[7];
    var to5=addDays(from5,6);
    var dd5 = to5.getDate();
    var mm5 = to5.getMonth() + 1;
    var y5 = to5.getFullYear();
    var toString5 = mm5+ '/'+ dd5 + '/'+ y5;
    //week4
    var from4=returnFunctionDatesGrid[8];
    var to4=addDays(from4,6);
    var dd4 = to4.getDate();
    var mm4 = to4.getMonth() + 1;
    var y4 = to4.getFullYear();
    var toString4 = mm4+ '/'+ dd4 + '/'+ y4;
    //week3
    var from3=returnFunctionDatesGrid[9];
    var to3=addDays(from3,6);
    var dd3 = to3.getDate();
    var mm3 = to3.getMonth() + 1;
    var y3 = to3.getFullYear();
    var toString3 = mm3+ '/'+ dd3 + '/'+ y3;
    //week2
    var from2=returnFunctionDatesGrid[10];
    var to2=addDays(from2,6);
    var dd2 = to2.getDate();
    var mm2 = to2.getMonth() + 1;
    var y2 = to2.getFullYear();
    var toString2 = mm2+ '/'+ dd2 + '/'+ y2;
    //week1
    var from1=returnFunctionDatesGrid[11];
    var to1=addDays(from1,6);
    var dd1 = to1.getDate();
    var mm1 = to1.getMonth() + 1;
    var y1 = to1.getFullYear();
    var toString1 = mm1+ '/'+ dd1 + '/'+ y1;
    var searchIns = nlapiLoadSearch('transaction', 'customsearch_clgx_so_install_vol_loc_s_6');
    searchIns.addColumns(arrColumns);
    searchIns.addFilters(arrFilters);
    var searchI = searchIns.runSearch();
    searchI.forEachResult(function(searchResult) {
        var columns = searchResult.getAllColumns();
        var cat = searchResult.getText(columns[0]);
        var date= new Date(searchResult.getValue(columns[2]));
        if(cat=='Columbus')
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                countColW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                countColW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                countColW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                countColW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                countColW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                countColW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                countColW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                countColW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                countColW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                countColW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                countColW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                countColW12++;
            }
        }
        if(cat=='Dallas')
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                countDalW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                countDalW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                countDalW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                countDalW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                countDalW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                countDalW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                countDalW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                countDalW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                countDalW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                countDalW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                countDalW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                countDalW12++;
            }
        }
        if(cat=='Jacksonville')
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                countJacW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                countJacW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                countJacW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                countJacW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                countJacW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                countJacW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                countJacW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                countJacW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                countJacW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                countJacW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                countJacW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                countJacW12++;
            }
        }
        if(cat=='Jacksonville2')
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                countJac2W1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                countJac2W2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                countJac2W3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                countJac2W4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                countJac2W5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                countJac2W6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                countJac2W7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                countJac2W8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                countJac2W9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                countJac2W10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                countJac2W11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                countJac2W12++;
            }
        }
        if(cat=='Lakeland')
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                countLakW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                countLakW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                countLakW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                countLakW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                countLakW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                countLakW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                countLakW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                countLakW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                countLakW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                countLakW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                countLakW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                countLakW12++;
            }
        }
        if(cat=='Minneapolis ')
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                countMinW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                countMinW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                countMinW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                countMinW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                countMinW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                countMinW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                countMinW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                countMinW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                countMinW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                countMinW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                countMinW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                countMinW12++;
            }
        }
        if(cat=='Montreal')
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                countMtlW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                countMtlW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                countMtlW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                countMtlW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                countMtlW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                countMtlW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                countMtlW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                countMtlW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                countMtlW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                countMtlW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                countMtlW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                countMtlW12++;
            }
        }
        if(cat=='Toronto')
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                countTorW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                countTorW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                countTorW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                countTorW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                countTorW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                countTorW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                countTorW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                countTorW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                countTorW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                countTorW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                countTorW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                countTorW12++;
            }
        }
        if(cat=='Vancouver')
        {
            if((date >= new Date(from1)) && (date <= new Date(toString1)))
            {
                countVanW1++;
            }
            if((date >=new Date( from2)) && (date <= new Date(toString2)))
            {
                countVanW2++;
            }
            if((date >=new Date( from3)) && (date <= new Date(toString3)))
            {
                countVanW3++;
            }
            if((date >=new Date( from4)) && (date <= new Date(toString4)))
            {
                countVanW4++;
            }
            if((date >=new Date( from5)) && (date <= new Date(toString5)))
            {
                countVanW5++;
            }
            if((date >=new Date( from6)) && (date <= new Date(toString6)))
            {
                countVanW6++;
            }
            if((date >=new Date( from7)) && (date <= new Date(toString7)))
            {
                countVanW7++;
            }
            if((date >=new Date( from8)) && (date <= new Date(toString8)))
            {
                countVanW8++;
            }
            if((date >=new Date(from9)) && (date <= new Date(toString9)))
            {
                countVanW9++;
            }
            if((date >=new Date(from10)) && (date <= new Date(toString10)))
            {
                countVanW10++;
            }
            if((date >=new Date(from11)) && (date <= new Date(toString11)))
            {
                countVanW11++;
            }
            if((date >=new Date(from12)) && (date <= new Date(toString12)))
            {
                countVanW12++;
            }
        }

        return true; // return true to keep iterating

    });

    locationColumbusArr=[countColW1,countColW2,countColW3,countColW4,countColW5,countColW6,countColW7,countColW8,countColW9,countColW10,countColW11,countColW12];
    locationDallasArr=[countDalW1,countDalW2,countDalW3,countDalW4,countDalW5,countDalW6,countDalW7,countDalW8,countDalW9,countDalW10,countDalW11,countDalW12];
    locationJacksonvilleArr=[countJacW1,countJacW2,countJacW3,countJacW4,countJacW5,countJacW6,countJacW7,countJacW8,countJacW9,countJacW10,countJacW11,countJacW12];
    locationJacksonville2Arr=[countJac2W1,countJac2W2,countJac2W3,countJac2W4,countJac2W5,countJac2W6,countJac2W7,countJac2W8,countJac2W9,countJac2W10,countJac2W11,countJac2W12];
    locationLakelandArr=[countLakW1,countLakW2,countLakW3,countLakW4,countLakW5,countLakW6,countLakW7,countLakW8,countLakW9,countLakW10,countLakW11,countLakW12];
    locationMinneapolisArr=[countMinW1,countMinW2,countMinW3,countMinW4,countMinW5,countMinW6,countMinW7,countMinW8,countMinW9,countMinW10,countMinW11,countMinW12];
    locationMontrealArr=[countMtlW1,countMtlW2,countMtlW3,countMtlW4,countMtlW5,countMtlW6,countMtlW7,countMtlW8,countMtlW9,countMtlW10,countMtlW11,countMtlW12];
    locationTorontoArr=[countTorW1,countTorW2,countTorW3,countTorW4,countTorW5,countTorW6,countTorW7,countTorW8,countTorW9,countTorW10,countTorW11,countTorW12];
    locationVancouverArr=[countVanW1,countVanW2,countVanW3,countVanW4,countVanW5,countVanW6,countVanW7,countVanW8,countVanW9,countVanW10,countVanW11,countVanW12];

    //space
    if(locationColumbusArr.length>0)
    {
        series +=  "{name: 'Columbus',type: 'column',data: [{y:"+locationColumbusArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationColumbusArr[10] +",url:'/app/site/hosting/script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationColumbusArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationColumbusArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationColumbusArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationColumbusArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=all&week=="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationColumbusArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationColumbusArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationColumbusArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationColumbusArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationColumbusArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationColumbusArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    //network
    if(locationDallasArr.length>0)
    {
        series +=  "{name: 'Dallas',type: 'column',data: [{y:"+locationDallasArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationDallasArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationDallasArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationDallasArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationDallasArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationDallasArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationDallasArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationDallasArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationDallasArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationDallasArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationDallasArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationDallasArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    //x inter
    if(locationJacksonvilleArr.length>0)
    {
        series +=  "{name: 'Jacksonville',type: 'column',data: [{y:"+locationJacksonvilleArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationJacksonvilleArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationJacksonvilleArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationJacksonvilleArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationJacksonvilleArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationJacksonvilleArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationJacksonvilleArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationJacksonvilleArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationJacksonvilleArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationJacksonvilleArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationJacksonvilleArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationJacksonvilleArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    if(locationJacksonville2Arr.length>0)
    {
        series +=  "{name: 'Jacksonville2',type: 'column',data: [{y:"+locationJacksonville2Arr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationJacksonville2Arr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationJacksonville2Arr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationJacksonville2Arr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationJacksonville2Arr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationJacksonville2Arr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationJacksonville2Arr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationJacksonville2Arr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationJacksonville2Arr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationJacksonville2Arr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationJacksonville2Arr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationJacksonville2Arr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }


    //inter
    if(locationLakelandArr.length>0)
    {
        series +=  "{name: 'Lakeland',type: 'column',data: [{y:"+locationLakelandArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationLakelandArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationLakelandArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationLakelandArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationLakelandArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationLakelandArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationLakelandArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationLakelandArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationLakelandArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationLakelandArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationLakelandArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationLakelandArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    if(locationMinneapolisArr.length>0)
    {
        series +=  "{name: 'Minneapolis',type: 'column',data: [{y:"+locationMinneapolisArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationMinneapolisArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationMinneapolisArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationMinneapolisArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationMinneapolisArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationMinneapolisArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationMinneapolisArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationMinneapolisArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationMinneapolisArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationMinneapolisArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationMinneapolisArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationMinneapolisArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    //power
    if(locationMontrealArr.length>0)
    {
        series +=  "{name: 'Montreal',type: 'column',data: [{y:"+locationMontrealArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationMontrealArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationMontrealArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationMontrealArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationMontrealArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationMontrealArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationMontrealArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationMontrealArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationMontrealArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationMontrealArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationMontrealArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationMontrealArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    if(locationTorontoArr.length>0)
    {
        series +=  "{name: 'Toronto',type: 'column',data: [{y:"+locationTorontoArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationTorontoArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationTorontoArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationTorontoArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationTorontoArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationTorontoArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationTorontoArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationTorontoArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationTorontoArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationTorontoArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationTorontoArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationTorontoArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    if(locationVancouverArr.length>0)
    {
        series +=  "{name: 'Vancouver',type: 'column',data: [{y:"+locationVancouverArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationVancouverArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationVancouverArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationVancouverArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationVancouverArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationVancouverArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationVancouverArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationVancouverArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationVancouverArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationVancouverArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationVancouverArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationVancouverArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=all&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }




    return series;
}

function getVolumeSeriesNStandard (catitem,cat){
    var series='';
    var arrFilters = new Array();
    var arrColumns = new Array();
    var countSpaceArr=new Array();
    var countNetworkArr=new Array();
    var countPowerArr=new Array();
    var countXCArr=new Array();
    var countVXCArr=new Array();
    var countSpaceSTArr=[0,0,0,0,0,0,0,0,0,0,0,0];
    var countNetworSTkArr=new Array();
    var countPowerSTArr=new Array();
    var countXCSTArr=new Array();
    var countVXSTCArr=new Array();
    var locationColumbusArr=[];
    var locationDallasArr=[];
    var locationJacksonvilleArr=[];
    var locationJacksonville2Arr=[];
    var locationLakelandArr=[];
    var locationMinneapolisArr=[];
    var locationMontrealArr=[];
    var locationTorontoArr=[];
    var locationVancouverArr=[];
    var returnFunctionDates=return_weeksDates();
    var returnFunctionDatesGrid=return_weeksforGrid();
    var to=addDays(returnFunctionDates[1], 6);
    var dd = to.getDate();
    var mm = to.getMonth() + 1;
    var y = to.getFullYear();
    var toString = mm+ '/'+ dd + '/'+ y;
    var  catstring=cat[0];
    if(cat.length>1)
    {
        catstring='all';
    }

    var arrLocations=['Columbus','Dallas','Jacksonville','Jacksonville2','Lakeland','Minneapolis ','Montreal','Toronto','Vancouver'];    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",returnFunctionDates[0],toString));
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",returnFunctionDates[0],toString));
    //  arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category","item","anyof",cat));

    var countColW12=0;
    var countColW11=0;
    var countColW10=0;
    var countColW9=0;
    var countColW8=0;
    var countColW7=0;
    var countColW6=0;
    var countColW5=0;
    var countColW4=0;
    var countColW3=0;
    var countColW2=0;
    var countColW1=0;
    //Dal
    var countDalW12=0;
    var countDalW11=0;
    var countDalW10=0;
    var countDalW9=0;
    var countDalW8=0;
    var countDalW7=0;
    var countDalW6=0;
    var countDalW5=0;
    var countDalW4=0;
    var countDalW3=0;
    var countDalW2=0;
    var countDalW1=0;

    //Jac
    var countJacW12=0;
    var countJacW11=0;
    var countJacW10=0;
    var countJacW9=0;
    var countJacW8=0;
    var countJacW7=0;
    var countJacW6=0;
    var countJacW5=0;
    var countJacW4=0;
    var countJacW3=0;
    var countJacW2=0;
    var countJacW1=0;
    //Jac2
    var countJac2W12=0;
    var countJac2W11=0;
    var countJac2W10=0;
    var countJac2W9=0;
    var countJac2W8=0;
    var countJac2W7=0;
    var countJac2W6=0;
    var countJac2W5=0;
    var countJac2W4=0;
    var countJac2W3=0;
    var countJac2W2=0;
    var countJac2W1=0;
    //Jac2
    var countLakW12=0;
    var countLakW11=0;
    var countLakW10=0;
    var countLakW9=0;
    var countLakW8=0;
    var countLakW7=0;
    var countLakW6=0;
    var countLakW5=0;
    var countLakW4=0;
    var countLakW3=0;
    var countLakW2=0;
    var countLakW1=0;
    //Min
    var countMinW12=0;
    var countMinW11=0;
    var countMinW10=0;
    var countMinW9=0;
    var countMinW8=0;
    var countMinW7=0;
    var countMinW6=0;
    var countMinW5=0;
    var countMinW4=0;
    var countMinW3=0;
    var countMinW2=0;
    var countMinW1=0;
    //Mtl
    var countMtlW12=0;
    var countMtlW11=0;
    var countMtlW10=0;
    var countMtlW9=0;
    var countMtlW8=0;
    var countMtlW7=0;
    var countMtlW6=0;
    var countMtlW5=0;
    var countMtlW4=0;
    var countMtlW3=0;
    var countMtlW2=0;
    var countMtlW1=0;
    //Tor
    var countTorW12=0;
    var countTorW11=0;
    var countTorW10=0;
    var countTorW9=0;
    var countTorW8=0;
    var countTorW7=0;
    var countTorW6=0;
    var countTorW5=0;
    var countTorW4=0;
    var countTorW3=0;
    var countTorW2=0;
    var countTorW1=0;
    //Van
    var countVanW12=0;
    var countVanW11=0;
    var countVanW10=0;
    var countVanW9=0;
    var countVanW8=0;
    var countVanW7=0;
    var countVanW6=0;
    var countVanW5=0;
    var countVanW4=0;
    var countVanW3=0;
    var countVanW2=0;
    var countVanW1=0;
    //Non-Standard
    //Van
    var NcountVanW12=0;
    var NcountVanW11=0;
    var NcountVanW10=0;
    var NcountVanW9=0;
    var NcountVanW8=0;
    var NcountVanW7=0;
    var NcountVanW6=0;
    var NcountVanW5=0;
    var NcountVanW4=0;
    var NcountVanW3=0;
    var NcountVanW2=0;
    var NcountVanW1=0;

    var from12=returnFunctionDatesGrid[0];
    var to12=addDays(from12,6);
    var dd12 = to12.getDate();
    var mm12 = to12.getMonth() + 1;
    var y12 = to12.getFullYear();
    var toString12 = mm12+ '/'+ dd12 + '/'+ y12;
    //week11
    var from11=returnFunctionDatesGrid[1];
    var to11=addDays(from11,6);
    var dd11 = to11.getDate();
    var mm11 = to11.getMonth() + 1;
    var y11 = to11.getFullYear();
    var toString11 = mm11+ '/'+ dd11 + '/'+ y11;
    //week10
    var from10=returnFunctionDatesGrid[2];
    var to10=addDays(from10,6);
    var dd10 = to10.getDate();
    var mm10 = to10.getMonth() + 1;
    var y10 = to10.getFullYear();
    var toString10 = mm10+ '/'+ dd10 + '/'+ y10;
    //week9
    var from9=returnFunctionDatesGrid[3];
    var to9=addDays(from9,6);
    var dd9 = to9.getDate();
    var mm9 = to9.getMonth() + 1;
    var y9 = to9.getFullYear();
    var toString9 = mm9+ '/'+ dd9 + '/'+ y9;
    //week8
    var from8=returnFunctionDatesGrid[4];
    var to8=addDays(from8,6);
    var dd8 = to8.getDate();
    var mm8 = to8.getMonth() + 1;
    var y8 = to8.getFullYear();
    var toString8 = mm8+ '/'+ dd8 + '/'+ y8;
    //week7
    var from7=returnFunctionDatesGrid[5];
    var to7=addDays(from7,6);
    var dd7 = to7.getDate();
    var mm7 = to7.getMonth() + 1;
    var y7 = to7.getFullYear();
    var toString7 = mm7+ '/'+ dd7 + '/'+ y7;
    //week6
    var from6=returnFunctionDatesGrid[6];
    var to6=addDays(from6,6);
    var dd6 = to6.getDate();
    var mm6 = to6.getMonth() + 1;
    var y6 = to6.getFullYear();
    var toString6 = mm6+ '/'+ dd6 + '/'+ y6;
    //week5
    var from5=returnFunctionDatesGrid[7];
    var to5=addDays(from5,6);
    var dd5 = to5.getDate();
    var mm5 = to5.getMonth() + 1;
    var y5 = to5.getFullYear();
    var toString5 = mm5+ '/'+ dd5 + '/'+ y5;
    //week4
    var from4=returnFunctionDatesGrid[8];
    var to4=addDays(from4,6);
    var dd4 = to4.getDate();
    var mm4 = to4.getMonth() + 1;
    var y4 = to4.getFullYear();
    var toString4 = mm4+ '/'+ dd4 + '/'+ y4;
    //week3
    var from3=returnFunctionDatesGrid[9];
    var to3=addDays(from3,6);
    var dd3 = to3.getDate();
    var mm3 = to3.getMonth() + 1;
    var y3 = to3.getFullYear();
    var toString3 = mm3+ '/'+ dd3 + '/'+ y3;
    //week2
    var from2=returnFunctionDatesGrid[10];
    var to2=addDays(from2,6);
    var dd2 = to2.getDate();
    var mm2 = to2.getMonth() + 1;
    var y2 = to2.getFullYear();
    var toString2 = mm2+ '/'+ dd2 + '/'+ y2;
    //week1
    var from1=returnFunctionDatesGrid[11];
    var to1=addDays(from1,6);
    var dd1 = to1.getDate();
    var mm1 = to1.getMonth() + 1;
    var y1 = to1.getFullYear();
    var toString1 = mm1+ '/'+ dd1 + '/'+ y1;
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_so_install_vol_loc_s_5', arrFilters, arrColumns);
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var cat = searchIn.getText(columns[0]);
        var date= new Date(searchIn.getValue(columns[2]));
        var nonValue= searchIn.getValue(columns[3]);
        var stValue= searchIn.getValue(columns[4]);
        var itemcat=searchIn.getValue(columns[5]);
        var itemcatArr=itemcat.split(',');
        if((in_array(catitem,itemcatArr))&&(catitem!='all'))
        {
            if((cat=='Columbus')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countColW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countColW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countColW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countColW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countColW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countColW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countColW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countColW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countColW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countColW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countColW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countColW12++;
                }
            }
            if((cat=='Dallas')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countDalW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countDalW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countDalW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countDalW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countDalW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countDalW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countDalW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countDalW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countDalW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countDalW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countDalW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countDalW12++;
                }
            }
            if((cat=='Jacksonville')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countJacW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countJacW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countJacW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countJacW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countJacW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countJacW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countJacW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countJacW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countJacW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countJacW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countJacW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countJacW12++;
                }
            }
            if((cat=='Jacksonville2')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countJac2W1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countJac2W2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countJac2W3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countJac2W4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countJac2W5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countJac2W6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countJac2W7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countJac2W8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countJac2W9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countJac2W10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countJac2W11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countJac2W12++;
                }
            }
            if((cat=='Lakeland')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countLakW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countLakW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countLakW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countLakW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countLakW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countLakW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countLakW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countLakW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countLakW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countLakW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countLakW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countLakW12++;
                }
            }
            if((cat=='Minneapolis ')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countMinW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countMinW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countMinW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countMinW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countMinW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countMinW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countMinW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countMinW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countMinW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countMinW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countMinW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countMinW12++;
                }
            }
            if((cat=='Montreal')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countMtlW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countMtlW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countMtlW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countMtlW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countMtlW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countMtlW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countMtlW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countMtlW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countMtlW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countMtlW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countMtlW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countMtlW12++;
                }
            }
            if((cat=='Toronto')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countTorW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countTorW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countTorW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countTorW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countTorW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countTorW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countTorW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countTorW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countTorW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countTorW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countTorW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countTorW12++;
                }
            }
            if((cat=='Vancouver')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countVanW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countVanW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countVanW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countVanW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countVanW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countVanW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countVanW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countVanW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countVanW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countVanW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countVanW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countVanW12++;
                }
            }
        }
        else if(catitem=="all"){
            if((cat=='Columbus')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countColW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countColW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countColW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countColW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countColW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countColW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countColW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countColW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countColW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countColW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countColW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countColW12++;
                }
            }
            if((cat=='Dallas')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countDalW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countDalW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countDalW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countDalW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countDalW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countDalW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countDalW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countDalW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countDalW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countDalW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countDalW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countDalW12++;
                }
            }
            if((cat=='Jacksonville')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countJacW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countJacW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countJacW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countJacW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countJacW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countJacW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countJacW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countJacW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countJacW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countJacW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countJacW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countJacW12++;
                }
            }
            if((cat=='Jacksonville2')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countJac2W1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countJac2W2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countJac2W3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countJac2W4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countJac2W5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countJac2W6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countJac2W7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countJac2W8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countJac2W9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countJac2W10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countJac2W11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countJac2W12++;
                }
            }
            if((cat=='Lakeland')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countLakW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countLakW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countLakW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countLakW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countLakW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countLakW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countLakW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countLakW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countLakW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countLakW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countLakW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countLakW12++;
                }
            }
            if((cat=='Minneapolis ')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countMinW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countMinW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countMinW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countMinW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countMinW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countMinW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countMinW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countMinW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countMinW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countMinW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countMinW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countMinW12++;
                }
            }
            if((cat=='Montreal')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countMtlW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countMtlW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countMtlW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countMtlW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countMtlW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countMtlW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countMtlW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countMtlW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countMtlW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countMtlW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countMtlW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countMtlW12++;
                }
            }
            if((cat=='Toronto')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countTorW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countTorW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countTorW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countTorW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countTorW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countTorW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countTorW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countTorW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countTorW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countTorW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countTorW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countTorW12++;
                }
            }
            if((cat=='Vancouver')&&(((nonValue>0)&&(stValue>0))||(nonValue>0)))
            {
                if((date >= new Date(from1)) && (date <= new Date(toString1)))
                {
                    countVanW1++;
                }
                if((date >=new Date( from2)) && (date <= new Date(toString2)))
                {
                    countVanW2++;
                }
                if((date >=new Date( from3)) && (date <= new Date(toString3)))
                {
                    countVanW3++;
                }
                if((date >=new Date( from4)) && (date <= new Date(toString4)))
                {
                    countVanW4++;
                }
                if((date >=new Date( from5)) && (date <= new Date(toString5)))
                {
                    countVanW5++;
                }
                if((date >=new Date( from6)) && (date <= new Date(toString6)))
                {
                    countVanW6++;
                }
                if((date >=new Date( from7)) && (date <= new Date(toString7)))
                {
                    countVanW7++;
                }
                if((date >=new Date( from8)) && (date <= new Date(toString8)))
                {
                    countVanW8++;
                }
                if((date >=new Date(from9)) && (date <= new Date(toString9)))
                {
                    countVanW9++;
                }
                if((date >=new Date(from10)) && (date <= new Date(toString10)))
                {
                    countVanW10++;
                }
                if((date >=new Date(from11)) && (date <= new Date(toString11)))
                {
                    countVanW11++;
                }
                if((date >=new Date(from12)) && (date <= new Date(toString12)))
                {
                    countVanW12++;
                }
            }
        }

    }
    locationColumbusArr=[countColW1,countColW2,countColW3,countColW4,countColW5,countColW6,countColW7,countColW8,countColW9,countColW10,countColW11,countColW12];
    locationDallasArr=[countDalW1,countDalW2,countDalW3,countDalW4,countDalW5,countDalW6,countDalW7,countDalW8,countDalW9,countDalW10,countDalW11,countDalW12];
    locationJacksonvilleArr=[countJacW1,countJacW2,countJacW3,countJacW4,countJacW5,countJacW6,countJacW7,countJacW8,countJacW9,countJacW10,countJacW11,countJacW12];
    locationJacksonville2Arr=[countJac2W1,countJac2W2,countJac2W3,countJac2W4,countJac2W5,countJac2W6,countJac2W7,countJac2W8,countJac2W9,countJac2W10,countJac2W11,countJac2W12];
    locationLakelandArr=[countLakW1,countLakW2,countLakW3,countLakW4,countLakW5,countLakW6,countLakW7,countLakW8,countLakW9,countLakW10,countLakW11,countLakW12];
    locationMinneapolisArr=[countMinW1,countMinW2,countMinW3,countMinW4,countMinW5,countMinW6,countMinW7,countMinW8,countMinW9,countMinW10,countMinW11,countMinW12];
    locationMontrealArr=[countMtlW1,countMtlW2,countMtlW3,countMtlW4,countMtlW5,countMtlW6,countMtlW7,countMtlW8,countMtlW9,countMtlW10,countMtlW11,countMtlW12];
    locationTorontoArr=[countTorW1,countTorW2,countTorW3,countTorW4,countTorW5,countTorW6,countTorW7,countTorW8,countTorW9,countTorW10,countTorW11,countTorW12];
    locationVancouverArr=[countVanW1,countVanW2,countVanW3,countVanW4,countVanW5,countVanW6,countVanW7,countVanW8,countVanW9,countVanW10,countVanW11,countVanW12];

    //space
    if(locationColumbusArr.length>0)
    {
        series +=  "{name: 'Columbus',type: 'column',data: [{y:"+locationColumbusArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationColumbusArr[10] +",url:'/app/site/hosting/script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationColumbusArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationColumbusArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationColumbusArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationColumbusArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=2&week=="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationColumbusArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationColumbusArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationColumbusArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationColumbusArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationColumbusArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationColumbusArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Columbus&location=33&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    //network
    if(locationDallasArr.length>0)
    {
        series +=  "{name: 'Dallas',type: 'column',data: [{y:"+locationDallasArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationDallasArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationDallasArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationDallasArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationDallasArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationDallasArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationDallasArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationDallasArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationDallasArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationDallasArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationDallasArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationDallasArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Dallas&location=20&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    //x inter
    if(locationJacksonvilleArr.length>0)
    {
        series +=  "{name: 'Jacksonville',type: 'column',data: [{y:"+locationJacksonvilleArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationJacksonvilleArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationJacksonvilleArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationJacksonvilleArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationJacksonvilleArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationJacksonvilleArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationJacksonvilleArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationJacksonvilleArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationJacksonvilleArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationJacksonvilleArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationJacksonvilleArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationJacksonvilleArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville&location=29&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    if(locationJacksonville2Arr.length>0)
    {
        series +=  "{name: 'Jacksonville2',type: 'column',data: [{y:"+locationJacksonville2Arr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationJacksonville2Arr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationJacksonville2Arr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationJacksonville2Arr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationJacksonville2Arr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationJacksonville2Arr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationJacksonville2Arr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationJacksonville2Arr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationJacksonville2Arr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationJacksonville2Arr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationJacksonville2Arr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationJacksonville2Arr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Jacksonville2&location=43&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    //inter
    if(locationLakelandArr.length>0)
    {
        series +=  "{name: 'Lakeland',type: 'column',data: [{y:"+locationLakelandArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationLakelandArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationLakelandArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationLakelandArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationLakelandArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationLakelandArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationLakelandArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationLakelandArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationLakelandArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationLakelandArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationLakelandArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationLakelandArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Lakeland&location=41&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    if(locationMinneapolisArr.length>0)
    {
        series +=  "{name: 'Minneapolis',type: 'column',data: [{y:"+locationMinneapolisArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationMinneapolisArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationMinneapolisArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationMinneapolisArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationMinneapolisArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationMinneapolisArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationMinneapolisArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationMinneapolisArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationMinneapolisArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationMinneapolisArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationMinneapolisArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationMinneapolisArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Minneapolis&location=22&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    //power
    if(locationMontrealArr.length>0)
    {
        series +=  "{name: 'Montreal',type: 'column',data: [{y:"+locationMontrealArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationMontrealArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationMontrealArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationMontrealArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationMontrealArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationMontrealArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationMontrealArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationMontrealArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationMontrealArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationMontrealArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationMontrealArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationMontrealArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Montreal&location=25&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    if(locationTorontoArr.length>0)
    {
        series +=  "{name: 'Toronto',type: 'column',data: [{y:"+locationTorontoArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationTorontoArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationTorontoArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationTorontoArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationTorontoArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationTorontoArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationTorontoArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationTorontoArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationTorontoArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationTorontoArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationTorontoArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationTorontoArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Toronto&location=23&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    if(locationVancouverArr.length>0)
    {
        series +=  "{name: 'Vancouver',type: 'column',data: [{y:"+locationVancouverArr[11] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[0]+"'},{y:"+locationVancouverArr[10] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[1]+"'}"+','
            +"{y:"+locationVancouverArr[9] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[2]+"'}"+','
            +"{y:"+locationVancouverArr[8] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[3]+"'}"+','
            +"{y:"+locationVancouverArr[7] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[4]+"'}"+','
            +"{y:"+locationVancouverArr[6] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[5]+"'}"+','
            +"{y:"+locationVancouverArr[5] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[6]+"'}"+','
            +"{y:"+locationVancouverArr[4] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[7]+"'}"+','
            +"{y:"+locationVancouverArr[3] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[8]+"'}"+','
            +"{y:"+locationVancouverArr[2] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[9]+"'}"+','
            +"{y:"+locationVancouverArr[1] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[10]+"'}"+','
            +"{y:"+locationVancouverArr[0] +",url:'/app/site/hosting/scriptlet.nl?script=527&deploy=1&title=Vancouver&location=26&product="+catstring+"&cattype=2&week="+returnFunctionDatesGrid[11]+"'}"+''
            +"]},";
    }

    var searchSt = nlapiSearchRecord('transaction', 'customsearch_clgx_so_install_vol_loc_s_4', arrFilters, arrColumns);

    for ( var i = 0; searchSt != null && i < searchSt.length; i++ ) {
        var searchIn = searchSt[i];
        var columns = searchIn.getAllColumns();
        var cat = searchIn.getText(columns[0]);
        var date = new Date(searchIn.getValue(columns[2]));
        var nStandard=searchIn.getValue(columns[3]);
        var itemcat=searchIn.getValue(columns[4]);
        var itemcatArr=itemcat.split(',');
        if((in_array(catitem,itemcatArr))&&(catitem!='all'))
        {
            if(((date >= new Date(from1)) && (date <= new Date(toString1)))&&(nStandard==0))
            {
                NcountVanW1++;
            }
            if(((date >=new Date( from2)) && (date <= new Date(toString2)))&&(nStandard==0))
            {
                NcountVanW2++;
            }
            if(((date >=new Date( from3)) && (date <= new Date(toString3)))&&(nStandard==0))
            {
                NcountVanW3++;
            }
            if(((date >=new Date( from4)) && (date <= new Date(toString4)))&&(nStandard==0))
            {
                NcountVanW4++;
            }
            if(((date >=new Date( from5)) && (date <= new Date(toString5)))&&(nStandard==0))
            {
                NcountVanW5++;
            }
            if(((date >=new Date( from6)) && (date <= new Date(toString6)))&&(nStandard==0))
            {
                NcountVanW6++;
            }
            if(((date >=new Date( from7)) && (date <= new Date(toString7)))&&(nStandard==0))
            {
                NcountVanW7++;
            }
            if(((date >=new Date( from8)) && (date <= new Date(toString8)))&&(nStandard==0))
            {
                NcountVanW8++;
            }
            if(((date >=new Date(from9)) && (date <= new Date(toString9)))&&(nStandard==0))
            {
                NcountVanW9++;
            }
            if(((date >=new Date(from10)) && (date <= new Date(toString10)))&&(nStandard==0))
            {
                NcountVanW10++;
            }
            if(((date >=new Date(from11)) && (date <= new Date(toString11)))&&(nStandard==0))
            {
                NcountVanW11++;
            }
            if(((date >=new Date(from12)) && (date <= new Date(toString12)))&&(nStandard==0))
            {
                NcountVanW12++;
            }
        }
        else if(catitem=="all")
        {
            if(((date >= new Date(from1)) && (date <= new Date(toString1)))&&(nStandard==0))
            {
                NcountVanW1++;
            }
            if(((date >=new Date( from2)) && (date <= new Date(toString2)))&&(nStandard==0))
            {
                NcountVanW2++;
            }
            if(((date >=new Date( from3)) && (date <= new Date(toString3)))&&(nStandard==0))
            {
                NcountVanW3++;
            }
            if(((date >=new Date( from4)) && (date <= new Date(toString4)))&&(nStandard==0))
            {
                NcountVanW4++;
            }
            if(((date >=new Date( from5)) && (date <= new Date(toString5)))&&(nStandard==0))
            {
                NcountVanW5++;
            }
            if(((date >=new Date( from6)) && (date <= new Date(toString6)))&&(nStandard==0))
            {
                NcountVanW6++;
            }
            if(((date >=new Date( from7)) && (date <= new Date(toString7)))&&(nStandard==0))
            {
                NcountVanW7++;
            }
            if(((date >=new Date( from8)) && (date <= new Date(toString8)))&&(nStandard==0))
            {
                NcountVanW8++;
            }
            if(((date >=new Date(from9)) && (date <= new Date(toString9)))&&(nStandard==0))
            {
                NcountVanW9++;
            }
            if(((date >=new Date(from10)) && (date <= new Date(toString10)))&&(nStandard==0))
            {
                NcountVanW10++;
            }
            if(((date >=new Date(from11)) && (date <= new Date(toString11)))&&(nStandard==0))
            {
                NcountVanW11++;
            }
            if(((date >=new Date(from12)) && (date <= new Date(toString12)))&&(nStandard==0))
            {
                NcountVanW12++;
            }
        }
    }

    series +=  " {name: 'Standard',type: 'spline','visible': false, color: '#0000CC',data: [{y:"+NcountVanW12+",url:'#'}," +
        "{y:"+NcountVanW11+",url:'#'}," +
        "{y:"+NcountVanW10+",url:'#'}," +
        "{y:"+NcountVanW9+",url:'#'}," +
        "{y:"+NcountVanW8+",url:'#'}," +
        "{y:"+NcountVanW7+",url:'#'}," +
        "{y:"+NcountVanW6+",url:'#'}," +
        "{y:"+NcountVanW5+",url:'#'}," +
        "{y:"+NcountVanW4+",url:'#'}," +
        "{y:"+NcountVanW3+",url:'#'}," +
        "{y:"+NcountVanW2+",url:'#'}," +
        "{y:"+NcountVanW1+",url:'#'}]}";



    return series;
}
function getAvgSeriesPerWeek(from, to,cat){
    var locationColumbusArr=[];
    var locationDallasArr=[];
    var locationJacksonvilleArr=[];
    var locationJacksonville2Arr=[];
    var locationLakelandArr=[];
    var locationMinneapolisArr=[];
    var locationMontrealArr=[];
    var locationTorontoArr=[];
    var locationVancouverArr=[];
    if(cat.length>1)
    {
        var catitem="all";
    }
    else{
        if(cat==10){
            catitem="Space";
        }
        if(cat==8){
            catitem="Power";
        }
        if(cat==5){
            catitem="Network";
        }
        if(cat==4){
            catitem="Interconnection";
        }
        if(cat==11){
            catitem="Virtual Interconnection";
        }
    }
    var arrFilters = new Array();
    var arrColumns = new Array();
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from,to));
    var sosNS=new Array();
    var searchInsIDS = nlapiSearchRecord('transaction', 'customsearch_clgx_so_install_vol_loc_s_8', arrFilters, arrColumns);
    for ( var i = 0; searchInsIDS != null && i < searchInsIDS.length; i++ ) {
        var searchIn = searchInsIDS[i];
        var columns = searchIn.getAllColumns();
        var id = searchIn.getValue(columns[0]);
        sosNS.push(id);
    }
    if(cat.length==1)
    {
        arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category","item","anyof",cat));
    }
    var colAv=0;
    var colNum=0;
    var dalAv=0;
    var dalNum=0;
    var jacAv=0;
    var jacNum=0;
    var jac2Av=0;
    var jac2Num=0;
    var lakAv=0;
    var lakNum=0;
    var minAv=0;
    var minNum=0;
    var monAv=0;
    var monNum=0;
    var torAv=0;
    var torNum=0;
    var vanAv=0;
    var vanNum=0;
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_install_vol_s_5', arrFilters, arrColumns);
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var cat = searchIn.getText(columns[0]);
        var id=searchIn.getValue(columns[2]);
        //  if(catitem=='Interconnection'){
        //   nlapiSendEmail(206211,206211,'Inter',id+', '+cat +', '+searchIn.getValue(columns[1]),null,null,null,null);
        // }

        if((cat=='Columbus')&& (!in_array(id,sosNS)))
        {
            colAv=parseFloat(colAv)+parseFloat(searchIn.getValue(columns[1]));
            colNum++;

        }
        if((cat=='Dallas')&&(!in_array(id,sosNS)))
        {

            dalAv=parseFloat(dalAv)+parseFloat(searchIn.getValue(columns[1]));
            dalNum++;
        }
        if((cat=='Jacksonville')&&(!in_array(id,sosNS)))
        {

            jacAv=parseFloat(jacAv)+parseFloat(searchIn.getValue(columns[1]));
            jacNum++;
        }
        if((cat=='Jacksonville2')&&(!in_array(id,sosNS)))
        {

            jac2Av=parseFloat(jac2Av)+parseFloat(searchIn.getValue(columns[1]));
            jac2Num++;
        }
        if((cat=='Lakeland')&&(!in_array(id,sosNS)))
        {

            lakAv=parseFloat(lakAv)+parseFloat(searchIn.getValue(columns[1]));
            lakNum++;
        }
        if((cat=='Minneapolis ')&&(!in_array(id,sosNS)))
        {

            minAv=parseFloat(minAv)+parseFloat(searchIn.getValue(columns[1]));
            minNum++;
        }
        if((cat=='Montreal')&&(!in_array(id,sosNS)))
        {

            monAv=parseFloat(monAv)+parseFloat(searchIn.getValue(columns[1]));
            monNum++;
        }
        if((cat=='Toronto')&&(!in_array(id,sosNS)))
        {

            torAv=parseFloat(torAv)+parseFloat(searchIn.getValue(columns[1]));
            torNum++;
        }
        if((cat=='Vancouver')&&(!in_array(id,sosNS)))
        {

            vanAv=parseFloat(vanAv)+parseFloat(searchIn.getValue(columns[1]));
            vanNum++;
        }
    }

    if(colAv>0)
    {
        locationColumbusArr.push(parseFloat(parseFloat(colAv)/parseFloat(colNum)).toFixed(1));
    }
    else
    {
        locationColumbusArr.push(0);
    }
    if(dalAv>0)
    {
        locationDallasArr.push(parseFloat(parseFloat(dalAv)/parseFloat(dalNum)).toFixed(1));
    }
    else
    {
        locationDallasArr.push(0);
    }
    if(jacAv>0)
    {
        locationJacksonvilleArr.push(parseFloat(parseFloat(jacAv)/parseFloat(jacNum)).toFixed(1));
    }
    else
    {
        locationJacksonvilleArr.push(0);
    }
    if(jac2Av>0)
    {
        locationJacksonville2Arr.push(parseFloat(parseFloat(jac2Av)/parseFloat(jac2Num)).toFixed(1));
    }
    else
    {
        locationJacksonville2Arr.push(0);
    }
    if(lakAv>0)
    {
        locationLakelandArr.push(parseFloat(parseFloat(lakAv)/parseFloat(lakNum)).toFixed(1));
    }
    else
    {
        locationLakelandArr.push(0);
    }
    if(minAv>0)
    {
        locationMinneapolisArr.push(parseFloat(parseFloat(minAv)/parseFloat(minNum)).toFixed(1));
    }
    else{
        locationMinneapolisArr.push(0);
    }
    if(monAv>0)
    {
        locationMontrealArr.push(parseFloat(parseFloat(monAv)/parseFloat(monNum)).toFixed(1));
    }
    else
    {
        locationMontrealArr.push(0);
    }
    if(torAv>0)
    {
        locationTorontoArr.push(parseFloat(parseFloat(torAv)/parseFloat(torNum)).toFixed(1));
    }
    else{
        locationTorontoArr.push(0);
    }
    if(vanAv>0)
    {
        locationVancouverArr.push(parseFloat(parseFloat(vanAv)/parseFloat(vanNum)).toFixed(1));
    }
    else{
        locationVancouverArr.push(0);
    }
    var arrayReturn=[locationColumbusArr,locationDallasArr,locationJacksonvilleArr,locationJacksonville2Arr,locationLakelandArr,locationMinneapolisArr,locationMontrealArr,locationTorontoArr,locationVancouverArr,colNum,dalNum,jacNum,jac2Num,lakNum,minNum,monNum,torNum,vanNum];
    return arrayReturn;
}
function getNSIds(from, to){

    var arrFilters = new Array();
    var arrColumns = new Array();

    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from,to));
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_install_vol___2', arrFilters, arrColumns);

    var arrSOIDs=new Array();
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var id=searchIn.getValue(columns[0]);

        arrSOIDs.push(id);

    }
    return arrSOIDs;
}
function getAvgSeriesPerWeek_(from, to,cat){
    var locationColumbusArr=[];
    var locationDallasArr=[];
    var locationJacksonvilleArr=[];
    var locationJacksonville2Arr=[];
    var locationLakelandArr=[];
    var locationMinneapolisArr=[];
    var locationMontrealArr=[];
    var locationTorontoArr=[];
    var locationVancouverArr=[];
    if(cat.length>1)
    {
        var catitem="all";
    }
    else{
        if(cat==10){
            catitem="Space";
        }
        if(cat==8){
            catitem="Power";
        }
        if(cat==5){
            catitem="Network";
        }
        if(cat==4){
            catitem="Interconnection";
        }
        if(cat==11){
            catitem="Virtual Interconnection";
        }
    }
    var arrFilters = new Array();
    var arrColumns = new Array();
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from,to));

    if(cat.length==1)
    {
        arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category","item","anyof",cat));
    }
    var colAv=0;
    var colNum=0;
    var dalAv=0;
    var dalNum=0;
    var jacAv=0;
    var jacNum=0;
    var jac2Av=0;
    var jac2Num=0;
    var lakAv=0;
    var lakNum=0;
    var minAv=0;
    var minNum=0;
    var monAv=0;
    var monNum=0;
    var torAv=0;
    var torNum=0;
    var vanAv=0;
    var vanNum=0;
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_install_vol__12', arrFilters, arrColumns);
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var itemst=searchIn.getValue(columns[2]);
        if(itemst=="Standard")
        {
            colAv=parseFloat(colAv)+parseFloat(searchIn.getValue(columns[0]));
            colNum++;

        }

    }


    if(colAv>0)
    {
        locationColumbusArr.push(parseFloat(parseFloat(colAv)/parseFloat(colNum)).toFixed(1));
    }
    else
    {
        locationColumbusArr.push(0);
    }

    var arrayReturn=[locationColumbusArr[0],colNum];
    return arrayReturn;
}
function getAvgSeriesPerWeekNST(from, to,cat){

    var locationColumbusArr=[];
    var locationDallasArr=[];
    var locationJacksonvilleArr=[];
    var locationJacksonville2Arr=[];
    var locationLakelandArr=[];
    var locationMinneapolisArr=[];
    var locationMontrealArr=[];
    var locationTorontoArr=[];
    var locationVancouverArr=[];
    if(cat.length>1)
    {
        var catitem="all";
    }
    else{
        if(cat==10){
            catitem="Space";
        }
        if(cat==8){
            catitem="Power";
        }
        if(cat==5){
            catitem="Network";
        }
        if(cat==4){
            catitem="Interconnection";
        }
        if(cat==11){
            catitem="Virtual Interconnection";
        }
    }
    var arrFilters = new Array();
    var arrColumns = new Array();
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from,to));
    var sosNS=new Array();
    var searchInsIDS = nlapiSearchRecord('transaction', 'customsearch_clgx_so_install_vol_loc_s_8', arrFilters, arrColumns);
    for ( var i = 0; searchInsIDS != null && i < searchInsIDS.length; i++ ) {
        var searchIn = searchInsIDS[i];
        var columns = searchIn.getAllColumns();
        var id = searchIn.getValue(columns[0]);
        sosNS.push(id);
    }
    if(cat.length==1)
    {
        arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category","item","anyof",cat));
    }
    var colAv=0;
    var colNum=0;
    var dalAv=0;
    var dalNum=0;
    var jacAv=0;
    var jacNum=0;
    var jac2Av=0;
    var jac2Num=0;
    var lakAv=0;
    var lakNum=0;
    var minAv=0;
    var minNum=0;
    var monAv=0;
    var monNum=0;
    var torAv=0;
    var torNum=0;
    var vanAv=0;
    var vanNum=0;
    // arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category","item","anyof",cat));
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_install_vol_s_6', arrFilters, arrColumns);
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var cat = searchIn.getText(columns[0]);
        var id=searchIn.getValue(columns[2]);
        //  if(catitem=='Interconnection'){
        //   nlapiSendEmail(206211,206211,'Inter',id+', '+cat +', '+searchIn.getValue(columns[1]),null,null,null,null);
        // }

        if((cat=='Columbus')&& (in_array(id,sosNS)))
        {
            colAv=parseFloat(colAv)+parseFloat(searchIn.getValue(columns[1]));
            colNum++;

        }
        if((cat=='Dallas')&&(in_array(id,sosNS)))
        {

            dalAv=parseFloat(dalAv)+parseFloat(searchIn.getValue(columns[1]));
            dalNum++;
        }
        if((cat=='Jacksonville')&&(in_array(id,sosNS)))
        {

            jacAv=parseFloat(jacAv)+parseFloat(searchIn.getValue(columns[1]));
            jacNum++;
        }
        if((cat=='Jacksonville2')&&(in_array(id,sosNS)))
        {

            jac2Av=parseFloat(jac2Av)+parseFloat(searchIn.getValue(columns[1]));
            jac2Num++;
        }
        if((cat=='Lakeland')&&(in_array(id,sosNS)))
        {

            lakAv=parseFloat(lakAv)+parseFloat(searchIn.getValue(columns[1]));
            lakNum++;
        }
        if((cat=='Minneapolis ')&&(in_array(id,sosNS)))
        {

            minAv=parseFloat(minAv)+parseFloat(searchIn.getValue(columns[1]));
            minNum++;
        }
        if((cat=='Montreal')&&(in_array(id,sosNS)))
        {

            monAv=parseFloat(monAv)+parseFloat(searchIn.getValue(columns[1]));
            monNum++;
        }
        if((cat=='Toronto')&&(in_array(id,sosNS)))
        {

            torAv=parseFloat(torAv)+parseFloat(searchIn.getValue(columns[1]));
            torNum++;
        }
        if((cat=='Vancouver')&&(in_array(id,sosNS)))
        {

            vanAv=parseFloat(vanAv)+parseFloat(searchIn.getValue(columns[1]));
            vanNum++;
        }
    }


    if(colAv>0)
    {
        locationColumbusArr.push(parseFloat(parseFloat(colAv)/parseFloat(colNum)).toFixed(1));
    }
    else
    {
        locationColumbusArr.push(0);
    }
    if(dalAv>0)
    {
        locationDallasArr.push(parseFloat(parseFloat(dalAv)/parseFloat(dalNum)).toFixed(1));
    }
    else
    {
        locationDallasArr.push(0);
    }
    if(jacAv>0)
    {
        locationJacksonvilleArr.push(parseFloat(parseFloat(jacAv)/parseFloat(jacNum)).toFixed(1));
    }
    else
    {
        locationJacksonvilleArr.push(0);
    }
    if(jac2Av>0)
    {
        locationJacksonville2Arr.push(parseFloat(parseFloat(jac2Av)/parseFloat(jac2Num)).toFixed(1));
    }
    else
    {
        locationJacksonville2Arr.push(0);
    }
    if(lakAv>0)
    {
        locationLakelandArr.push(parseFloat(parseFloat(lakAv)/parseFloat(lakNum)).toFixed(1));
    }
    else
    {
        locationLakelandArr.push(0);
    }
    if(minAv>0)
    {
        locationMinneapolisArr.push(parseFloat(parseFloat(minAv)/parseFloat(minNum)).toFixed(1));
    }
    else{
        locationMinneapolisArr.push(0);
    }
    if(monAv>0)
    {
        locationMontrealArr.push(parseFloat(parseFloat(monAv)/parseFloat(monNum)).toFixed(1));
    }
    else
    {
        locationMontrealArr.push(0);
    }
    if(torAv>0)
    {
        locationTorontoArr.push(parseFloat(parseFloat(torAv)/parseFloat(torNum)).toFixed(1));
    }
    else{
        locationTorontoArr.push(0);
    }
    if(vanAv>0)
    {
        locationVancouverArr.push(parseFloat(parseFloat(vanAv)/parseFloat(vanNum)).toFixed(1));
    }
    else{
        locationVancouverArr.push(0);
    }
    var arrayReturn=[locationColumbusArr,locationDallasArr,locationJacksonvilleArr,locationJacksonville2Arr,locationLakelandArr,locationMinneapolisArr,locationMontrealArr,locationTorontoArr,locationVancouverArr,colNum,dalNum,jacNum,jac2Num,lakNum,minNum,monNum,torNum,vanNum];
    return arrayReturn;
}
function getAvgSeriesPerWeekNST_(arrSOs,from, to,cat){

    var locationColumbusArr=[];
    var locationDallasArr=[];
    var locationJacksonvilleArr=[];
    var locationJacksonville2Arr=[];
    var locationLakelandArr=[];
    var locationMinneapolisArr=[];
    var locationMontrealArr=[];
    var locationTorontoArr=[];
    var locationVancouverArr=[];
    if(cat.length>1)
    {
        var catitem="all";
    }
    else{
        if(cat==10){
            catitem="Space";
        }
        if(cat==8){
            catitem="Power";
        }
        if(cat==5){
            catitem="Network";
        }
        if(cat==4){
            catitem="Interconnection";
        }
        if(cat==11){
            catitem="Virtual Interconnection";
        }
    }
    var arrFilters = new Array();
    var arrColumns = new Array();
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from,to));

    if(cat.length==1)
    {
        arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category","item","anyof",cat));
    }
    var colAv=0;
    var colNum=0;
    var dalAv=0;
    var dalNum=0;
    var jacAv=0;
    var jacNum=0;
    var jac2Av=0;
    var jac2Num=0;
    var lakAv=0;
    var lakNum=0;
    var minAv=0;
    var minNum=0;
    var monAv=0;
    var monNum=0;
    var torAv=0;
    var torNum=0;
    var vanAv=0;
    var vanNum=0;
    // arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category","item","anyof",cat));
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_install_vol__12', arrFilters, arrColumns);
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var id=searchIn.getValue(columns[1]);
        if((in_array(id,arrSOs))&&(lastid!=id))
        {
            colAv=parseFloat(colAv)+parseFloat(searchIn.getValue(columns[0]));
            colNum++;

        }
        var lastid=id;
    }


    if(colAv>0)
    {
        locationColumbusArr.push(parseFloat(parseFloat(colAv)/parseFloat(colNum)).toFixed(1));
    }
    else
    {
        locationColumbusArr.push(0);
    }

    var arrayReturn=[locationColumbusArr[0],colNum];
    return arrayReturn;
}


function getAvgSeriesPerWeekAll(from, to,cat){
    var locationColumbusArr=[];
    var locationDallasArr=[];
    var locationJacksonvilleArr=[];
    var locationJacksonville2Arr=[];
    var locationLakelandArr=[];
    var locationMinneapolisArr=[];
    var locationMontrealArr=[];
    var locationNJArr=[];
    var locationTorontoArr=[];
    var locationVancouverArr=[];
    var arrFilters = new Array();
    var arrColumns = new Array();
    arrFilters.push(new nlobjSearchFilter("custbody_cologix_service_actl_instl_dt",null,"within",from,to));
    arrFilters.push(new nlobjSearchFilter("custitem_cologix_item_category","item","anyof",cat));
    var searchIns = nlapiSearchRecord('transaction', 'customsearch_clgx_dur_so_install_vol_s_7', arrFilters, arrColumns);
    var colAv=0;
    var colNum=0;
    var dalAv=0;
    var dalNum=0;
    var jacAv=0;
    var jacNum=0;
    var jac2Av=0;
    var jac2Num=0;
    var lakAv=0;
    var lakNum=0;
    var minAv=0;
    var minNum=0;
    var monAv=0;
    var monNum=0;
    var torAv=0;
    var torNum=0;
    var vanAv=0;
    var vanNum=0;
    for ( var i = 0; searchIns != null && i < searchIns.length; i++ ) {
        var searchIn = searchIns[i];
        var columns = searchIn.getAllColumns();
        var cat = searchIn.getText(columns[0]);
        var id=searchIn.getValue(columns[2]);
        //  if(catitem=='Interconnection'){
        //   nlapiSendEmail(206211,206211,'Inter',id+', '+cat +', '+searchIn.getValue(columns[1]),null,null,null,null);
        // }

        if(cat=='Columbus')
        {
            colAv=parseFloat(colAv)+parseFloat(searchIn.getValue(columns[1]));
            colNum++;

        }
        if(cat=='Dallas')
        {

            dalAv=parseFloat(dalAv)+parseFloat(searchIn.getValue(columns[1]));
            dalNum++;
        }
        if(cat=='Jacksonville')
        {

            jacAv=parseFloat(jacAv)+parseFloat(searchIn.getValue(columns[1]));
            jacNum++;
        }
        if(cat=='Jacksonville2')
        {

            jac2Av=parseFloat(jac2Av)+parseFloat(searchIn.getValue(columns[1]));
            jac2Num++;
        }
        if(cat=='Lakeland')
        {

            lakAv=parseFloat(lakAv)+parseFloat(searchIn.getValue(columns[1]));
            lakNum++;
        }
        if(cat=='Minneapolis ')
        {

            minAv=parseFloat(minAv)+parseFloat(searchIn.getValue(columns[1]));
            minNum++;
        }
        if(cat=='Montreal')
        {

            monAv=parseFloat(monAv)+parseFloat(searchIn.getValue(columns[1]));
            monNum++;
        }
        if(cat=='Toronto')
        {

            torAv=parseFloat(torAv)+parseFloat(searchIn.getValue(columns[1]));
            torNum++;
        }
        if(cat=='Vancouver')
        {

            vanAv=parseFloat(vanAv)+parseFloat(searchIn.getValue(columns[1]));
            vanNum++;
        }
    }


    if(colAv>0)
    {
        locationColumbusArr.push(parseFloat(parseFloat(colAv)/parseFloat(colNum)).toFixed(1));
    }
    else
    {
        locationColumbusArr.push(0);
    }
    if(dalAv>0)
    {
        locationDallasArr.push(parseFloat(parseFloat(dalAv)/parseFloat(dalNum)).toFixed(1));
    }
    else
    {
        locationDallasArr.push(0);
    }
    if(jacAv>0)
    {
        locationJacksonvilleArr.push(parseFloat(parseFloat(jacAv)/parseFloat(jacNum)).toFixed(1));
    }
    else
    {
        locationJacksonvilleArr.push(0);
    }
    if(jac2Av>0)
    {
        locationJacksonville2Arr.push(parseFloat(parseFloat(jac2Av)/parseFloat(jac2Num)).toFixed(1));
    }
    else
    {
        locationJacksonville2Arr.push(0);
    }
    if(lakAv>0)
    {
        locationLakelandArr.push(parseFloat(parseFloat(lakAv)/parseFloat(lakNum)).toFixed(1));
    }
    else
    {
        locationLakelandArr.push(0);
    }
    if(minAv>0)
    {
        locationMinneapolisArr.push(parseFloat(parseFloat(minAv)/parseFloat(minNum)).toFixed(1));
    }
    else{
        locationMinneapolisArr.push(0);
    }
    if(monAv>0)
    {
        locationMontrealArr.push(parseFloat(parseFloat(monAv)/parseFloat(monNum)).toFixed(1));
    }
    else
    {
        locationMontrealArr.push(0);
    }
    if(torAv>0)
    {
        locationTorontoArr.push(parseFloat(parseFloat(torAv)/parseFloat(torNum)).toFixed(1));
    }
    else{
        locationTorontoArr.push(0);
    }
    if(vanAv>0)
    {
        locationVancouverArr.push(parseFloat(parseFloat(vanAv)/parseFloat(vanNum)).toFixed(1));
    }
    else{
        locationVancouverArr.push(0);
    }
    var arrayReturn=[locationColumbusArr,locationDallasArr,locationJacksonvilleArr,locationJacksonville2Arr,locationLakelandArr,locationMinneapolisArr,locationMontrealArr,locationTorontoArr,locationVancouverArr,colNum,dalNum,jacNum,jac2Num,lakNum,minNum,monNum,torNum,vanNum];
    return arrayReturn;
}
function getAvgSeries(){
    var series='';
    var returnFunctionDatesGrid=return_weeksforGrid();
    //All Markets
    var location=[];
    var to12=addDays(returnFunctionDatesGrid[0],6);
    var from12=addDays(returnFunctionDatesGrid[0],-84);


    //week11
    var from11=addDays(returnFunctionDatesGrid[1],-84);
    var to11=addDays(returnFunctionDatesGrid[1],6);
    //week10
    var from10=addDays(returnFunctionDatesGrid[2],-84);
    var to10=addDays(returnFunctionDatesGrid[2],6);
    //week9
    var from9=addDays(returnFunctionDatesGrid[3],-84);
    var to9=addDays(returnFunctionDatesGrid[3],6);
    //week8
    var from8=addDays(returnFunctionDatesGrid[4],-84);
    var to8=addDays(returnFunctionDatesGrid[4],6);
    //week7
    var from7=addDays(returnFunctionDatesGrid[5],-84);
    var to7=addDays(returnFunctionDatesGrid[5],6);
    //week6
    var from6=addDays(returnFunctionDatesGrid[6],-84);
    var to6=addDays(returnFunctionDatesGrid[6],6);
    //week5
    var from5=addDays(returnFunctionDatesGrid[7],-84);
    var to5=addDays(returnFunctionDatesGrid[7],6);
    //week4
    var from4=addDays(returnFunctionDatesGrid[8],-84);
    var to4=addDays(returnFunctionDatesGrid[8],6);

    //week3
    var from3=addDays(returnFunctionDatesGrid[9],-84);
    var to3=addDays(returnFunctionDatesGrid[9],6);

    //week2
    var from2=addDays(returnFunctionDatesGrid[10],-84);
    var to2=addDays(returnFunctionDatesGrid[10],6);
    //week1
    var from1=addDays(returnFunctionDatesGrid[11],-84);
    var to1=addDays(returnFunctionDatesGrid[11],6);
    var arrSOs=getNSIds(from12, to1);
    //ALL
    var cat=[10,11,4,5,8];
    var values12Week =getAvgSeriesPerWeek(from12,to12,cat);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,cat);
    var values11Week =getAvgSeriesPerWeek(from11,to11,cat);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,cat);
    var values10Week =getAvgSeriesPerWeek(from10,to10,cat);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,cat);
    var values9Week =getAvgSeriesPerWeek(from9,to9,cat);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,cat);
    var values8Week =getAvgSeriesPerWeek(from8,to8,cat);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,cat);
    var values7Week =getAvgSeriesPerWeek(from7,to7,cat);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,cat);
    var values6Week =getAvgSeriesPerWeek(from6,to6,cat);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,cat);
    var values5Week =getAvgSeriesPerWeek(from5,to5,cat);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,cat);
    var values4Week =getAvgSeriesPerWeek(from4,to4,cat);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,cat);
    var values3Week =getAvgSeriesPerWeek(from3,to3,cat);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,cat);
    var values2Week =getAvgSeriesPerWeek(from2,to2,cat);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,cat);
    var values1Week =getAvgSeriesPerWeek(from1,to1,cat);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,cat);
    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#0000CC',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=all&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    var seriesVolSt=getVolumeSeriesStandard('all',cat);
    var seriesReturn='{ "ALL": ["'+seriesVolSt+'","{name: \'Columbus\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Dallas\',type: \'spline\',data: [{y:'+values12Week[1]+',z:'+values12Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[1]+',z:'+values11Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[1]+',z:'+values10Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[1]+',z:'+values9Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[1]+',z:'+values8Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[1]+',z:'+values7Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[1]+',z:'+values6Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[1]+',z:'+values5Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[1]+',z:'+values4Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[1]+',z:'+values3Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[1]+',z:'+values2Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[1]+',z:'+values1Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Jacksonville\',type: \'spline\',data: [{y:'+values12Week[2]+',z:'+values12Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[2]+',z:'+values11Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[2]+',z:'+values10Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[2]+',z:'+values9Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[2]+',z:'+values8Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[2]+',z:'+values7Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[2]+',z:'+values6Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[2]+',z:'+values5Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[2]+',z:'+values4Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[2]+',z:'+values3Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[2]+',z:'+values2Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[2]+',z:'+values1Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Jacksonville2\',type:  \'spline\',data: [{y:'+values12Week[3]+',z:'+values12Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[3]+',z:'+values11Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[3]+',z:'+values10Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[3]+',z:'+values9Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[3]+',z:'+values8Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[3]+',z:'+values7Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[3]+',z:'+values6Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[3]+',z:'+values5Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[3]+',z:'+values4Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[3]+',z:'+values3Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[3]+',z:'+values2Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[3]+',z:'+values1Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Lakeland\',type:  \'spline\',data: [{y:'+values12Week[4]+',z:'+values12Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[4]+',z:'+values11Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[4]+',z:'+values10Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[4]+',z:'+values9Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[4]+',z:'+values8Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[4]+',z:'+values7Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[4]+',z:'+values6Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[4]+',z:'+values5Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[4]+',z:'+values4Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[4]+',z:'+values3Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[4]+',z:'+values2Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[4]+',z:'+values1Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Minneapolis\',type:  \'spline\',data: [{y:'+values12Week[5]+',z:'+values12Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[5]+',z:'+values11Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[5]+',z:'+values10Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[5]+',z:'+values9Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[5]+',z:'+values8Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[5]+',z:'+values7Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[5]+',z:'+values6Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[5]+',z:'+values5Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[5]+',z:'+values4Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[5]+',z:'+values3Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[5]+',z:'+values2Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[5]+',z:'+values1Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Montreal\',type:  \'spline\',data: [{y:'+values12Week[6]+',z:'+values12Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[6]+',z:'+values11Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[6]+',z:'+values10Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[6]+',z:'+values9Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[6]+',z:'+values8Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[6]+',z:'+values7Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[6]+',z:'+values6Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[6]+',z:'+values5Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[6]+',z:'+values4Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[6]+',z:'+values3Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[6]+',z:'+values2Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[6]+',z:'+values1Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Toronto\',type:  \'spline\',data: [{y:'+values12Week[7]+',z:'+values12Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[7]+',z:'+values11Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[7]+',z:'+values10Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[7]+',z:'+values9Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[7]+',z:'+values8Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[7]+',z:'+values7Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[7]+',z:'+values6Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[7]+',z:'+values5Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[7]+',z:'+values4Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[7]+',z:'+values3Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[7]+',z:'+values2Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[7]+',z:'+values1Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Vancouver\',type:  \'spline\',data: [{y:'+values12Week[8]+',z:'+values12Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[8]+',z:'+values11Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[8]+',z:'+values10Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[8]+',z:'+values9Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[8]+',z:'+values8Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[8]+',z:'+values7Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[8]+',z:'+values6Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[8]+',z:'+values5Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[8]+',z:'+values4Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[8]+',z:'+values3Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[8]+',z:'+values2Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[8]+',z:'+values1Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+'"] },';

    //Space
    var cat=[10];
    var values12Week =getAvgSeriesPerWeek(from12,to12,cat);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,cat);
    var values11Week =getAvgSeriesPerWeek(from11,to11,cat);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,cat);
    var values10Week =getAvgSeriesPerWeek(from10,to10,cat);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,cat);
    var values9Week =getAvgSeriesPerWeek(from9,to9,cat);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,cat);
    var values8Week =getAvgSeriesPerWeek(from8,to8,cat);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,cat);
    var values7Week =getAvgSeriesPerWeek(from7,to7,cat);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,cat);
    var values6Week =getAvgSeriesPerWeek(from6,to6,cat);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,cat);
    var values5Week =getAvgSeriesPerWeek(from5,to5,cat);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,cat);
    var values4Week =getAvgSeriesPerWeek(from4,to4,cat);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,cat);
    var values3Week =getAvgSeriesPerWeek(from3,to3,cat);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,cat);
    var values2Week =getAvgSeriesPerWeek(from2,to2,cat);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,cat);
    var values1Week =getAvgSeriesPerWeek(from1,to1,cat);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,cat);
    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#0000CC',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=10&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=10&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=10&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=10&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=10&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=10&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=10&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=10&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=10&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=10&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=10&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=10&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    var seriesVolSt=getVolumeSeriesStandard("Space",cat);
    seriesReturn+='{ "Space": ["'+seriesVolSt+'","{name: \'Columbus\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Dallas\',type: \'spline\',data: [{y:'+values12Week[1]+',z:'+values12Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[1]+',z:'+values11Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[1]+',z:'+values10Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[1]+',z:'+values9Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[1]+',z:'+values8Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[1]+',z:'+values7Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[1]+',z:'+values6Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[1]+',z:'+values5Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[1]+',z:'+values4Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[1]+',z:'+values3Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[1]+',z:'+values2Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[1]+',z:'+values1Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Jacksonville\',type: \'spline\',data: [{y:'+values12Week[2]+',z:'+values12Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[2]+',z:'+values11Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[2]+',z:'+values10Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[2]+',z:'+values9Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[2]+',z:'+values8Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[2]+',z:'+values7Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[2]+',z:'+values6Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[2]+',z:'+values5Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[2]+',z:'+values4Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[2]+',z:'+values3Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[2]+',z:'+values2Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[2]+',z:'+values1Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Jacksonville2\',type:  \'spline\',data: [{y:'+values12Week[3]+',z:'+values12Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[3]+',z:'+values11Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[3]+',z:'+values10Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[3]+',z:'+values9Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[3]+',z:'+values8Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[3]+',z:'+values7Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[3]+',z:'+values6Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[3]+',z:'+values5Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[3]+',z:'+values4Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[3]+',z:'+values3Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[3]+',z:'+values2Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[3]+',z:'+values1Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Lakeland\',type:  \'spline\',data: [{y:'+values12Week[4]+',z:'+values12Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[4]+',z:'+values11Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[4]+',z:'+values10Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[4]+',z:'+values9Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[4]+',z:'+values8Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[4]+',z:'+values7Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[4]+',z:'+values6Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[4]+',z:'+values5Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[4]+',z:'+values4Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[4]+',z:'+values3Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[4]+',z:'+values2Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[4]+',z:'+values1Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Minneapolis\',type:  \'spline\',data: [{y:'+values12Week[5]+',z:'+values12Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[5]+',z:'+values11Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[5]+',z:'+values10Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[5]+',z:'+values9Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[5]+',z:'+values8Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[5]+',z:'+values7Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[5]+',z:'+values6Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[5]+',z:'+values5Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[5]+',z:'+values4Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[5]+',z:'+values3Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[5]+',z:'+values2Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[5]+',z:'+values1Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Montreal\',type:  \'spline\',data: [{y:'+values12Week[6]+',z:'+values12Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[6]+',z:'+values11Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[6]+',z:'+values10Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[6]+',z:'+values9Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[6]+',z:'+values8Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[6]+',z:'+values7Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[6]+',z:'+values6Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[6]+',z:'+values5Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[6]+',z:'+values4Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[6]+',z:'+values3Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[6]+',z:'+values2Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[6]+',z:'+values1Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Toronto\',type:  \'spline\',data: [{y:'+values12Week[7]+',z:'+values12Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[7]+',z:'+values11Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[7]+',z:'+values10Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[7]+',z:'+values9Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[7]+',z:'+values8Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[7]+',z:'+values7Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[7]+',z:'+values6Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[7]+',z:'+values5Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[7]+',z:'+values4Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[7]+',z:'+values3Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[7]+',z:'+values2Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[7]+',z:'+values1Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Vancouver\',type:  \'spline\',data: [{y:'+values12Week[8]+',z:'+values12Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[8]+',z:'+values11Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[8]+',z:'+values10Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[8]+',z:'+values9Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[8]+',z:'+values8Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[8]+',z:'+values7Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[8]+',z:'+values6Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[8]+',z:'+values5Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[8]+',z:'+values4Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[8]+',z:'+values3Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[8]+',z:'+values2Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[8]+',z:'+values1Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+'"] },';

    //Virtual XC
    var cat=[11];
    var values12Week =getAvgSeriesPerWeek(from12,to12,cat);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,cat);
    var values11Week =getAvgSeriesPerWeek(from11,to11,cat);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,cat);
    var values10Week =getAvgSeriesPerWeek(from10,to10,cat);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,cat);
    var values9Week =getAvgSeriesPerWeek(from9,to9,cat);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,cat);
    var values8Week =getAvgSeriesPerWeek(from8,to8,cat);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,cat);
    var values7Week =getAvgSeriesPerWeek(from7,to7,cat);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,cat);
    var values6Week =getAvgSeriesPerWeek(from6,to6,cat);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,cat);
    var values5Week =getAvgSeriesPerWeek(from5,to5,cat);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,cat);
    var values4Week =getAvgSeriesPerWeek(from4,to4,cat);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,cat);
    var values3Week =getAvgSeriesPerWeek(from3,to3,cat);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,cat);
    var values2Week =getAvgSeriesPerWeek(from2,to2,cat);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,cat);
    var values1Week =getAvgSeriesPerWeek(from1,to1,cat);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,cat);
    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#0000CC',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=11&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=11&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=11&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=11&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=11&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=11&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=11&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=11&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=11&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=11&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=11&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=11&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    var seriesVolSt=getVolumeSeriesStandard("Virtual Interconnection",cat);
    seriesReturn +='{ "VXC": ["'+seriesVolSt+'","{name: \'Columbus\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Dallas\',type: \'spline\',data: [{y:'+values12Week[1]+',z:'+values12Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[1]+',z:'+values11Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[1]+',z:'+values10Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[1]+',z:'+values9Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[1]+',z:'+values8Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[1]+',z:'+values7Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[1]+',z:'+values6Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[1]+',z:'+values5Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[1]+',z:'+values4Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[1]+',z:'+values3Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[1]+',z:'+values2Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[1]+',z:'+values1Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Jacksonville\',type: \'spline\',data: [{y:'+values12Week[2]+',z:'+values12Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[2]+',z:'+values11Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[2]+',z:'+values10Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[2]+',z:'+values9Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[2]+',z:'+values8Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[2]+',z:'+values7Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[2]+',z:'+values6Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[2]+',z:'+values5Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[2]+',z:'+values4Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[2]+',z:'+values3Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[2]+',z:'+values2Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[2]+',z:'+values1Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Jacksonville2\',type:  \'spline\',data: [{y:'+values12Week[3]+',z:'+values12Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[3]+',z:'+values11Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[3]+',z:'+values10Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[3]+',z:'+values9Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[3]+',z:'+values8Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[3]+',z:'+values7Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[3]+',z:'+values6Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[3]+',z:'+values5Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[3]+',z:'+values4Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[3]+',z:'+values3Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[3]+',z:'+values2Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[3]+',z:'+values1Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Lakeland\',type:  \'spline\',data: [{y:'+values12Week[4]+',z:'+values12Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[4]+',z:'+values11Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[4]+',z:'+values10Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[4]+',z:'+values9Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[4]+',z:'+values8Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[4]+',z:'+values7Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[4]+',z:'+values6Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[4]+',z:'+values5Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[4]+',z:'+values4Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[4]+',z:'+values3Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[4]+',z:'+values2Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[4]+',z:'+values1Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Minneapolis\',type:  \'spline\',data: [{y:'+values12Week[5]+',z:'+values12Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[5]+',z:'+values11Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[5]+',z:'+values10Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[5]+',z:'+values9Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[5]+',z:'+values8Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[5]+',z:'+values7Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[5]+',z:'+values6Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[5]+',z:'+values5Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[5]+',z:'+values4Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[5]+',z:'+values3Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[5]+',z:'+values2Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[5]+',z:'+values1Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Montreal\',type:  \'spline\',data: [{y:'+values12Week[6]+',z:'+values12Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[6]+',z:'+values11Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[6]+',z:'+values10Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[6]+',z:'+values9Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[6]+',z:'+values8Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[6]+',z:'+values7Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[6]+',z:'+values6Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[6]+',z:'+values5Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[6]+',z:'+values4Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[6]+',z:'+values3Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[6]+',z:'+values2Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[6]+',z:'+values1Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Toronto\',type:  \'spline\',data: [{y:'+values12Week[7]+',z:'+values12Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[7]+',z:'+values11Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[7]+',z:'+values10Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[7]+',z:'+values9Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[7]+',z:'+values8Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[7]+',z:'+values7Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[7]+',z:'+values6Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[7]+',z:'+values5Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[7]+',z:'+values4Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[7]+',z:'+values3Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[7]+',z:'+values2Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[7]+',z:'+values1Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Vancouver\',type:  \'spline\',data: [{y:'+values12Week[8]+',z:'+values12Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[8]+',z:'+values11Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[8]+',z:'+values10Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[8]+',z:'+values9Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[8]+',z:'+values8Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[8]+',z:'+values7Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[8]+',z:'+values6Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[8]+',z:'+values5Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[8]+',z:'+values4Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[8]+',z:'+values3Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[8]+',z:'+values2Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[8]+',z:'+values1Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+'"] },';
    // XC
    var cat=[4];
    var values12Week =getAvgSeriesPerWeek(from12,to12,cat);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,cat);
    var values11Week =getAvgSeriesPerWeek(from11,to11,cat);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,cat);
    var values10Week =getAvgSeriesPerWeek(from10,to10,cat);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,cat);
    var values9Week =getAvgSeriesPerWeek(from9,to9,cat);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,cat);
    var values8Week =getAvgSeriesPerWeek(from8,to8,cat);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,cat);
    var values7Week =getAvgSeriesPerWeek(from7,to7,cat);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,cat);
    var values6Week =getAvgSeriesPerWeek(from6,to6,cat);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,cat);
    var values5Week =getAvgSeriesPerWeek(from5,to5,cat);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,cat);
    var values4Week =getAvgSeriesPerWeek(from4,to4,cat);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,cat);
    var values3Week =getAvgSeriesPerWeek(from3,to3,cat);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,cat);
    var values2Week =getAvgSeriesPerWeek(from2,to2,cat);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,cat);
    var values1Week =getAvgSeriesPerWeek(from1,to1,cat);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,cat);
    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#0000CC',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=4&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=4&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=4&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=4&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=4&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=4&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=4&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=4&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=4&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=4&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=4&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=4&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    var seriesVolSt=getVolumeSeriesStandard("Interconnection",cat);
    seriesReturn +='{ "XC": ["'+seriesVolSt+'","{name: \'Columbus\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Dallas\',type: \'spline\',data: [{y:'+values12Week[1]+',z:'+values12Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[1]+',z:'+values11Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[1]+',z:'+values10Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[1]+',z:'+values9Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[1]+',z:'+values8Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[1]+',z:'+values7Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[1]+',z:'+values6Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[1]+',z:'+values5Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[1]+',z:'+values4Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[1]+',z:'+values3Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[1]+',z:'+values2Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[1]+',z:'+values1Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Jacksonville\',type: \'spline\',data: [{y:'+values12Week[2]+',z:'+values12Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[2]+',z:'+values11Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[2]+',z:'+values10Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[2]+',z:'+values9Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[2]+',z:'+values8Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[2]+',z:'+values7Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[2]+',z:'+values6Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[2]+',z:'+values5Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[2]+',z:'+values4Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[2]+',z:'+values3Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[2]+',z:'+values2Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[2]+',z:'+values1Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Jacksonville2\',type:  \'spline\',data: [{y:'+values12Week[3]+',z:'+values12Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[3]+',z:'+values11Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[3]+',z:'+values10Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[3]+',z:'+values9Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[3]+',z:'+values8Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[3]+',z:'+values7Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[3]+',z:'+values6Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[3]+',z:'+values5Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[3]+',z:'+values4Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[3]+',z:'+values3Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[3]+',z:'+values2Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[3]+',z:'+values1Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Lakeland\',type:  \'spline\',data: [{y:'+values12Week[4]+',z:'+values12Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[4]+',z:'+values11Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[4]+',z:'+values10Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[4]+',z:'+values9Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[4]+',z:'+values8Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[4]+',z:'+values7Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[4]+',z:'+values6Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[4]+',z:'+values5Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[4]+',z:'+values4Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[4]+',z:'+values3Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[4]+',z:'+values2Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[4]+',z:'+values1Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Minneapolis\',type:  \'spline\',data: [{y:'+values12Week[5]+',z:'+values12Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[5]+',z:'+values11Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[5]+',z:'+values10Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[5]+',z:'+values9Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[5]+',z:'+values8Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[5]+',z:'+values7Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[5]+',z:'+values6Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[5]+',z:'+values5Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[5]+',z:'+values4Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[5]+',z:'+values3Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[5]+',z:'+values2Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[5]+',z:'+values1Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Montreal\',type:  \'spline\',data: [{y:'+values12Week[6]+',z:'+values12Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[6]+',z:'+values11Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[6]+',z:'+values10Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[6]+',z:'+values9Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[6]+',z:'+values8Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[6]+',z:'+values7Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[6]+',z:'+values6Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[6]+',z:'+values5Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[6]+',z:'+values4Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[6]+',z:'+values3Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[6]+',z:'+values2Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[6]+',z:'+values1Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Toronto\',type:  \'spline\',data: [{y:'+values12Week[7]+',z:'+values12Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[7]+',z:'+values11Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[7]+',z:'+values10Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[7]+',z:'+values9Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[7]+',z:'+values8Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[7]+',z:'+values7Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[7]+',z:'+values6Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[7]+',z:'+values5Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[7]+',z:'+values4Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[7]+',z:'+values3Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[7]+',z:'+values2Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[7]+',z:'+values1Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Vancouver\',type:  \'spline\',data: [{y:'+values12Week[8]+',z:'+values12Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[8]+',z:'+values11Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[8]+',z:'+values10Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[8]+',z:'+values9Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[8]+',z:'+values8Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[8]+',z:'+values7Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[8]+',z:'+values6Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[8]+',z:'+values5Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[8]+',z:'+values4Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[8]+',z:'+values3Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[8]+',z:'+values2Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[8]+',z:'+values1Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+'"] },';
// Network
    var cat=[5];
    var values12Week =getAvgSeriesPerWeek(from12,to12,cat);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,cat);
    var values11Week =getAvgSeriesPerWeek(from11,to11,cat);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,cat);
    var values10Week =getAvgSeriesPerWeek(from10,to10,cat);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,cat);
    var values9Week =getAvgSeriesPerWeek(from9,to9,cat);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,cat);
    var values8Week =getAvgSeriesPerWeek(from8,to8,cat);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,cat);
    var values7Week =getAvgSeriesPerWeek(from7,to7,cat);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,cat);
    var values6Week =getAvgSeriesPerWeek(from6,to6,cat);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,cat);
    var values5Week =getAvgSeriesPerWeek(from5,to5,cat);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,cat);
    var values4Week =getAvgSeriesPerWeek(from4,to4,cat);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,cat);
    var values3Week =getAvgSeriesPerWeek(from3,to3,cat);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,cat);
    var values2Week =getAvgSeriesPerWeek(from2,to2,cat);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,cat);
    var values1Week =getAvgSeriesPerWeek(from1,to1,cat);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,cat);
    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#0000CC',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=5&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=5&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=5&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=5&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=5&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=5&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=5&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=5&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=5&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=5&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=5&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=5&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    var seriesVolSt=getVolumeSeriesStandard("Network",cat);
    seriesReturn +='{ "Network": ["'+seriesVolSt+'","{name: \'Columbus\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Dallas\',type: \'spline\',data: [{y:'+values12Week[1]+',z:'+values12Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[1]+',z:'+values11Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[1]+',z:'+values10Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[1]+',z:'+values9Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[1]+',z:'+values8Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[1]+',z:'+values7Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[1]+',z:'+values6Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[1]+',z:'+values5Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[1]+',z:'+values4Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[1]+',z:'+values3Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[1]+',z:'+values2Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[1]+',z:'+values1Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Jacksonville\',type: \'spline\',data: [{y:'+values12Week[2]+',z:'+values12Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[2]+',z:'+values11Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[2]+',z:'+values10Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[2]+',z:'+values9Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[2]+',z:'+values8Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[2]+',z:'+values7Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[2]+',z:'+values6Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[2]+',z:'+values5Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[2]+',z:'+values4Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[2]+',z:'+values3Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[2]+',z:'+values2Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[2]+',z:'+values1Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Jacksonville2\',type:  \'spline\',data: [{y:'+values12Week[3]+',z:'+values12Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[3]+',z:'+values11Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[3]+',z:'+values10Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[3]+',z:'+values9Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[3]+',z:'+values8Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[3]+',z:'+values7Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[3]+',z:'+values6Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[3]+',z:'+values5Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[3]+',z:'+values4Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[3]+',z:'+values3Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[3]+',z:'+values2Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[3]+',z:'+values1Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Lakeland\',type:  \'spline\',data: [{y:'+values12Week[4]+',z:'+values12Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[4]+',z:'+values11Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[4]+',z:'+values10Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[4]+',z:'+values9Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[4]+',z:'+values8Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[4]+',z:'+values7Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[4]+',z:'+values6Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[4]+',z:'+values5Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[4]+',z:'+values4Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[4]+',z:'+values3Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[4]+',z:'+values2Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[4]+',z:'+values1Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Minneapolis\',type:  \'spline\',data: [{y:'+values12Week[5]+',z:'+values12Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[5]+',z:'+values11Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[5]+',z:'+values10Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[5]+',z:'+values9Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[5]+',z:'+values8Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[5]+',z:'+values7Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[5]+',z:'+values6Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[5]+',z:'+values5Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[5]+',z:'+values4Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[5]+',z:'+values3Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[5]+',z:'+values2Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[5]+',z:'+values1Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Montreal\',type:  \'spline\',data: [{y:'+values12Week[6]+',z:'+values12Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[6]+',z:'+values11Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[6]+',z:'+values10Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[6]+',z:'+values9Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[6]+',z:'+values8Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[6]+',z:'+values7Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[6]+',z:'+values6Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[6]+',z:'+values5Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[6]+',z:'+values4Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[6]+',z:'+values3Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[6]+',z:'+values2Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[6]+',z:'+values1Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Toronto\',type:  \'spline\',data: [{y:'+values12Week[7]+',z:'+values12Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[7]+',z:'+values11Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[7]+',z:'+values10Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[7]+',z:'+values9Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[7]+',z:'+values8Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[7]+',z:'+values7Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[7]+',z:'+values6Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[7]+',z:'+values5Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[7]+',z:'+values4Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[7]+',z:'+values3Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[7]+',z:'+values2Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[7]+',z:'+values1Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Vancouver\',type:  \'spline\',data: [{y:'+values12Week[8]+',z:'+values12Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[8]+',z:'+values11Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[8]+',z:'+values10Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[8]+',z:'+values9Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[8]+',z:'+values8Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[8]+',z:'+values7Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[8]+',z:'+values6Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[8]+',z:'+values5Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[8]+',z:'+values4Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[8]+',z:'+values3Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[8]+',z:'+values2Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[8]+',z:'+values1Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+'"] },';
// Power
    var cat=[8];
    var values12Week =getAvgSeriesPerWeek(from12,to12,cat);
    var values12WeekN =getAvgSeriesPerWeekNST_(arrSOs,from12,to12,cat);
    var values11Week =getAvgSeriesPerWeek(from11,to11,cat);
    var values11WeekN =getAvgSeriesPerWeekNST_(arrSOs,from11,to11,cat);
    var values10Week =getAvgSeriesPerWeek(from10,to10,cat);
    var values10WeekN =getAvgSeriesPerWeekNST_(arrSOs,from10,to10,cat);
    var values9Week =getAvgSeriesPerWeek(from9,to9,cat);
    var values9WeekN =getAvgSeriesPerWeekNST_(arrSOs,from9,to9,cat);
    var values8Week =getAvgSeriesPerWeek(from8,to8,cat);
    var values8WeekN =getAvgSeriesPerWeekNST_(arrSOs,from8,to8,cat);
    var values7Week =getAvgSeriesPerWeek(from7,to7,cat);
    var values7WeekN =getAvgSeriesPerWeekNST_(arrSOs,from7,to7,cat);
    var values6Week =getAvgSeriesPerWeek(from6,to6,cat);
    var values6WeekN =getAvgSeriesPerWeekNST_(arrSOs,from6,to6,cat);
    var values5Week =getAvgSeriesPerWeek(from5,to5,cat);
    var values5WeekN =getAvgSeriesPerWeekNST_(arrSOs,from5,to5,cat);
    var values4Week =getAvgSeriesPerWeek(from4,to4,cat);
    var values4WeekN =getAvgSeriesPerWeekNST_(arrSOs,from4,to4,cat);
    var values3Week =getAvgSeriesPerWeek(from3,to3,cat);
    var values3WeekN =getAvgSeriesPerWeekNST_(arrSOs,from3,to3,cat);
    var values2Week =getAvgSeriesPerWeek(from2,to2,cat);
    var values2WeekN =getAvgSeriesPerWeekNST_(arrSOs,from2,to2,cat);
    var values1Week =getAvgSeriesPerWeek(from1,to1,cat);
    var values1WeekN =getAvgSeriesPerWeekNST_(arrSOs,from1,to1,cat);
    var addseries= "{name: 'Non-Standard',type: 'spline','visible': false, color: '#0000CC',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Non-Standard&location=&product=8&cattype=2&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    var seriesVolSt=getVolumeSeriesStandard("Power",cat);
    seriesReturn +='{ "Power": ["'+seriesVolSt+'","{name: \'Columbus\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Dallas\',type: \'spline\',data: [{y:'+values12Week[1]+',z:'+values12Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[1]+',z:'+values11Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[1]+',z:'+values10Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[1]+',z:'+values9Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[1]+',z:'+values8Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[1]+',z:'+values7Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[1]+',z:'+values6Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[1]+',z:'+values5Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[1]+',z:'+values4Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[1]+',z:'+values3Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[1]+',z:'+values2Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[1]+',z:'+values1Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Jacksonville\',type: \'spline\',data: [{y:'+values12Week[2]+',z:'+values12Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[2]+',z:'+values11Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[2]+',z:'+values10Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[2]+',z:'+values9Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[2]+',z:'+values8Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[2]+',z:'+values7Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[2]+',z:'+values6Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[2]+',z:'+values5Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[2]+',z:'+values4Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[2]+',z:'+values3Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[2]+',z:'+values2Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[2]+',z:'+values1Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Jacksonville2\',type:  \'spline\',data: [{y:'+values12Week[3]+',z:'+values12Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[3]+',z:'+values11Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[3]+',z:'+values10Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[3]+',z:'+values9Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[3]+',z:'+values8Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[3]+',z:'+values7Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[3]+',z:'+values6Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[3]+',z:'+values5Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[3]+',z:'+values4Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[3]+',z:'+values3Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[3]+',z:'+values2Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[3]+',z:'+values1Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Lakeland\',type:  \'spline\',data: [{y:'+values12Week[4]+',z:'+values12Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[4]+',z:'+values11Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[4]+',z:'+values10Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[4]+',z:'+values9Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[4]+',z:'+values8Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[4]+',z:'+values7Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[4]+',z:'+values6Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[4]+',z:'+values5Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[4]+',z:'+values4Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[4]+',z:'+values3Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[4]+',z:'+values2Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[4]+',z:'+values1Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Minneapolis\',type:  \'spline\',data: [{y:'+values12Week[5]+',z:'+values12Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[5]+',z:'+values11Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[5]+',z:'+values10Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[5]+',z:'+values9Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[5]+',z:'+values8Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[5]+',z:'+values7Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[5]+',z:'+values6Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[5]+',z:'+values5Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[5]+',z:'+values4Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[5]+',z:'+values3Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[5]+',z:'+values2Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[5]+',z:'+values1Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Montreal\',type:  \'spline\',data: [{y:'+values12Week[6]+',z:'+values12Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[6]+',z:'+values11Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[6]+',z:'+values10Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[6]+',z:'+values9Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[6]+',z:'+values8Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[6]+',z:'+values7Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[6]+',z:'+values6Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[6]+',z:'+values5Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[6]+',z:'+values4Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[6]+',z:'+values3Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[6]+',z:'+values2Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[6]+',z:'+values1Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Toronto\',type:  \'spline\',data: [{y:'+values12Week[7]+',z:'+values12Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[7]+',z:'+values11Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[7]+',z:'+values10Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[7]+',z:'+values9Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[7]+',z:'+values8Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[7]+',z:'+values7Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[7]+',z:'+values6Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[7]+',z:'+values5Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[7]+',z:'+values4Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[7]+',z:'+values3Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[7]+',z:'+values2Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[7]+',z:'+values1Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Vancouver\',type:  \'spline\',data: [{y:'+values12Week[8]+',z:'+values12Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[8]+',z:'+values11Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[8]+',z:'+values10Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[8]+',z:'+values9Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[8]+',z:'+values8Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[8]+',z:'+values7Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[8]+',z:'+values6Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[8]+',z:'+values5Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[8]+',z:'+values4Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[8]+',z:'+values3Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[8]+',z:'+values2Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[8]+',z:'+values1Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+'"] },';

    return seriesReturn;
}
function getAvgSeriesN(){
    var series='';
    var returnFunctionDatesGrid=return_weeksforGrid();
    //All Markets
    var location=[];
    var to12=addDays(returnFunctionDatesGrid[0],6);
    var from12=addDays(returnFunctionDatesGrid[0],-84);


    //week11
    var from11=addDays(returnFunctionDatesGrid[1],-84);
    var to11=addDays(returnFunctionDatesGrid[1],6);
    //week10
    var from10=addDays(returnFunctionDatesGrid[2],-84);
    var to10=addDays(returnFunctionDatesGrid[2],6);
    //week9
    var from9=addDays(returnFunctionDatesGrid[3],-84);
    var to9=addDays(returnFunctionDatesGrid[3],6);
    //week8
    var from8=addDays(returnFunctionDatesGrid[4],-84);
    var to8=addDays(returnFunctionDatesGrid[4],6);
    //week7
    var from7=addDays(returnFunctionDatesGrid[5],-84);
    var to7=addDays(returnFunctionDatesGrid[5],6);
    //week6
    var from6=addDays(returnFunctionDatesGrid[6],-84);
    var to6=addDays(returnFunctionDatesGrid[6],6);
    //week5
    var from5=addDays(returnFunctionDatesGrid[7],-84);
    var to5=addDays(returnFunctionDatesGrid[7],6);
    //week4
    var from4=addDays(returnFunctionDatesGrid[8],-84);
    var to4=addDays(returnFunctionDatesGrid[8],6);

    //week3
    var from3=addDays(returnFunctionDatesGrid[9],-84);
    var to3=addDays(returnFunctionDatesGrid[9],6);

    //week2
    var from2=addDays(returnFunctionDatesGrid[10],-84);
    var to2=addDays(returnFunctionDatesGrid[10],6);
    //week1
    var from1=addDays(returnFunctionDatesGrid[11],-84);
    var to1=addDays(returnFunctionDatesGrid[11],6);
    //ALL
    var cat=[10,11,4,5,8];
    var values12Week =getAvgSeriesPerWeekNST(from12,to12,cat);
    var values12WeekN =getAvgSeriesPerWeek_(from12,to12,cat);
    var values11Week =getAvgSeriesPerWeekNST(from11,to11,cat);
    var values11WeekN =getAvgSeriesPerWeek_(from11,to11,cat);
    var values10Week =getAvgSeriesPerWeekNST(from10,to10,cat);
    var values10WeekN =getAvgSeriesPerWeek_(from10,to10,cat);
    var values9Week =getAvgSeriesPerWeekNST(from9,to9,cat);
    var values9WeekN =getAvgSeriesPerWeek_(from9,to9,cat);
    var values8Week =getAvgSeriesPerWeekNST(from8,to8,cat);
    var values8WeekN =getAvgSeriesPerWeek_(from8,to8,cat);
    var values7Week =getAvgSeriesPerWeekNST(from7,to7,cat);
    var values7WeekN =getAvgSeriesPerWeek_(from7,to7,cat);
    var values6Week =getAvgSeriesPerWeekNST(from6,to6,cat);
    var values6WeekN =getAvgSeriesPerWeek_(from6,to6,cat);
    var values5Week =getAvgSeriesPerWeekNST(from5,to5,cat);
    var values5WeekN =getAvgSeriesPerWeek_(from5,to5,cat);
    var values4Week =getAvgSeriesPerWeekNST(from4,to4,cat);
    var values4WeekN =getAvgSeriesPerWeek_(from4,to4,cat);
    var values3Week =getAvgSeriesPerWeekNST(from3,to3,cat);
    var values3WeekN =getAvgSeriesPerWeek_(from3,to3,cat);
    var values2Week =getAvgSeriesPerWeekNST(from2,to2,cat);
    var values2WeekN =getAvgSeriesPerWeek_(from2,to2,cat);
    var values1Week =getAvgSeriesPerWeekNST(from1,to1,cat);
    var values1WeekN =getAvgSeriesPerWeek_(from1,to1,cat);
    var addseries= "{name: 'Standard',type: 'spline','visible': false, color: '#0000CC',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=all&cattype=1&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=all&cattype=1&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=all&cattype=1&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=all&cattype=1&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=all&cattype=1&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=all&cattype=1&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=all&cattype=1&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=all&cattype=1&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=all&cattype=1&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=all&cattype=1&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=all&cattype=1&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=all&cattype=1&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    var seriesVolSt=getVolumeSeriesNStandard("all",cat);
    var seriesReturn='{ "ALL": ["'+seriesVolSt+'","{name: \'Columbus\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Dallas\',type: \'spline\',data: [{y:'+values12Week[1]+',z:'+values12Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[1]+',z:'+values11Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[1]+',z:'+values10Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[1]+',z:'+values9Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[1]+',z:'+values8Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[1]+',z:'+values7Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[1]+',z:'+values6Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[1]+',z:'+values5Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[1]+',z:'+values4Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[1]+',z:'+values3Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[1]+',z:'+values2Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[1]+',z:'+values1Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Jacksonville\',type: \'spline\',data: [{y:'+values12Week[2]+',z:'+values12Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[2]+',z:'+values11Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[2]+',z:'+values10Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[2]+',z:'+values9Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[2]+',z:'+values8Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[2]+',z:'+values7Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[2]+',z:'+values6Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[2]+',z:'+values5Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[2]+',z:'+values4Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[2]+',z:'+values3Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[2]+',z:'+values2Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[2]+',z:'+values1Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Jacksonville2\',type:  \'spline\',data: [{y:'+values12Week[3]+',z:'+values12Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[3]+',z:'+values11Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[3]+',z:'+values10Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[3]+',z:'+values9Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[3]+',z:'+values8Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[3]+',z:'+values7Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[3]+',z:'+values6Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[3]+',z:'+values5Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[3]+',z:'+values4Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[3]+',z:'+values3Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[3]+',z:'+values2Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[3]+',z:'+values1Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Lakeland\',type:  \'spline\',data: [{y:'+values12Week[4]+',z:'+values12Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[4]+',z:'+values11Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[4]+',z:'+values10Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[4]+',z:'+values9Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[4]+',z:'+values8Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[4]+',z:'+values7Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[4]+',z:'+values6Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[4]+',z:'+values5Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[4]+',z:'+values4Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[4]+',z:'+values3Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[4]+',z:'+values2Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[4]+',z:'+values1Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Minneapolis\',type:  \'spline\',data: [{y:'+values12Week[5]+',z:'+values12Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[5]+',z:'+values11Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[5]+',z:'+values10Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[5]+',z:'+values9Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[5]+',z:'+values8Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[5]+',z:'+values7Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[5]+',z:'+values6Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[5]+',z:'+values5Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[5]+',z:'+values4Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[5]+',z:'+values3Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[5]+',z:'+values2Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[5]+',z:'+values1Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Montreal\',type:  \'spline\',data: [{y:'+values12Week[6]+',z:'+values12Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[6]+',z:'+values11Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[6]+',z:'+values10Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[6]+',z:'+values9Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[6]+',z:'+values8Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[6]+',z:'+values7Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[6]+',z:'+values6Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[6]+',z:'+values5Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[6]+',z:'+values4Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[6]+',z:'+values3Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[6]+',z:'+values2Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[6]+',z:'+values1Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Toronto\',type:  \'spline\',data: [{y:'+values12Week[7]+',z:'+values12Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[7]+',z:'+values11Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[7]+',z:'+values10Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[7]+',z:'+values9Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[7]+',z:'+values8Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[7]+',z:'+values7Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[7]+',z:'+values6Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[7]+',z:'+values5Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[7]+',z:'+values4Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[7]+',z:'+values3Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[7]+',z:'+values2Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[7]+',z:'+values1Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Vancouver\',type:  \'spline\',data: [{y:'+values12Week[8]+',z:'+values12Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[8]+',z:'+values11Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[8]+',z:'+values10Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[8]+',z:'+values9Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[8]+',z:'+values8Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[8]+',z:'+values7Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[8]+',z:'+values6Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[8]+',z:'+values5Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[8]+',z:'+values4Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[8]+',z:'+values3Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[8]+',z:'+values2Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[8]+',z:'+values1Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=1&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+'"] },';



    //Space
    var cat=[10];
    var values12Week =getAvgSeriesPerWeekNST(from12,to12,cat);
    var values12WeekN =getAvgSeriesPerWeek_(from12,to12,cat);
    var values11Week =getAvgSeriesPerWeekNST(from11,to11,cat);
    var values11WeekN =getAvgSeriesPerWeek_(from11,to11,cat);
    var values10Week =getAvgSeriesPerWeekNST(from10,to10,cat);
    var values10WeekN =getAvgSeriesPerWeek_(from10,to10,cat);
    var values9Week =getAvgSeriesPerWeekNST(from9,to9,cat);
    var values9WeekN =getAvgSeriesPerWeek_(from9,to9,cat);
    var values8Week =getAvgSeriesPerWeekNST(from8,to8,cat);
    var values8WeekN =getAvgSeriesPerWeek_(from8,to8,cat);
    var values7Week =getAvgSeriesPerWeekNST(from7,to7,cat);
    var values7WeekN =getAvgSeriesPerWeek_(from7,to7,cat);
    var values6Week =getAvgSeriesPerWeekNST(from6,to6,cat);
    var values6WeekN =getAvgSeriesPerWeek_(from6,to6,cat);
    var values5Week =getAvgSeriesPerWeekNST(from5,to5,cat);
    var values5WeekN =getAvgSeriesPerWeek_(from5,to5,cat);
    var values4Week =getAvgSeriesPerWeekNST(from4,to4,cat);
    var values4WeekN =getAvgSeriesPerWeek_(from4,to4,cat);
    var values3Week =getAvgSeriesPerWeekNST(from3,to3,cat);
    var values3WeekN =getAvgSeriesPerWeek_(from3,to3,cat);
    var values2Week =getAvgSeriesPerWeekNST(from2,to2,cat);
    var values2WeekN =getAvgSeriesPerWeek_(from2,to2,cat);
    var values1Week =getAvgSeriesPerWeekNST(from1,to1,cat);
    var values1WeekN =getAvgSeriesPerWeek_(from1,to1,cat);
    var addseries= "{name: 'Standard',type: 'spline','visible': false, color: '#0000CC',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=10&cattype=1&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=10&cattype=1&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=10&cattype=1&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=10&cattype=1&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=10&cattype=1&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=10&cattype=1&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=10&cattype=1&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=10&cattype=1&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=10&cattype=1&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=10&cattype=1&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=10&cattype=1&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=10&cattype=1&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    var seriesVolSt=getVolumeSeriesNStandard("Space",cat);
     seriesReturn+='{ "Space": ["'+seriesVolSt+'","{name: \'Columbus\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Dallas\',type: \'spline\',data: [{y:'+values12Week[1]+',z:'+values12Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[1]+',z:'+values11Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[1]+',z:'+values10Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[1]+',z:'+values9Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[1]+',z:'+values8Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[1]+',z:'+values7Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[1]+',z:'+values6Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[1]+',z:'+values5Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[1]+',z:'+values4Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[1]+',z:'+values3Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[1]+',z:'+values2Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[1]+',z:'+values1Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Jacksonville\',type: \'spline\',data: [{y:'+values12Week[2]+',z:'+values12Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[2]+',z:'+values11Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[2]+',z:'+values10Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[2]+',z:'+values9Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[2]+',z:'+values8Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[2]+',z:'+values7Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[2]+',z:'+values6Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[2]+',z:'+values5Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[2]+',z:'+values4Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[2]+',z:'+values3Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[2]+',z:'+values2Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[2]+',z:'+values1Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Jacksonville2\',type:  \'spline\',data: [{y:'+values12Week[3]+',z:'+values12Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[3]+',z:'+values11Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[3]+',z:'+values10Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[3]+',z:'+values9Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[3]+',z:'+values8Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[3]+',z:'+values7Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[3]+',z:'+values6Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[3]+',z:'+values5Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[3]+',z:'+values4Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[3]+',z:'+values3Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[3]+',z:'+values2Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[3]+',z:'+values1Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Lakeland\',type:  \'spline\',data: [{y:'+values12Week[4]+',z:'+values12Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[4]+',z:'+values11Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[4]+',z:'+values10Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[4]+',z:'+values9Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[4]+',z:'+values8Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[4]+',z:'+values7Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[4]+',z:'+values6Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[4]+',z:'+values5Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[4]+',z:'+values4Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[4]+',z:'+values3Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[4]+',z:'+values2Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[4]+',z:'+values1Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Minneapolis\',type:  \'spline\',data: [{y:'+values12Week[5]+',z:'+values12Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[5]+',z:'+values11Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[5]+',z:'+values10Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[5]+',z:'+values9Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[5]+',z:'+values8Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[5]+',z:'+values7Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[5]+',z:'+values6Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[5]+',z:'+values5Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[5]+',z:'+values4Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[5]+',z:'+values3Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[5]+',z:'+values2Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[5]+',z:'+values1Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Montreal\',type:  \'spline\',data: [{y:'+values12Week[6]+',z:'+values12Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[6]+',z:'+values11Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[6]+',z:'+values10Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[6]+',z:'+values9Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[6]+',z:'+values8Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[6]+',z:'+values7Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[6]+',z:'+values6Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[6]+',z:'+values5Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[6]+',z:'+values4Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[6]+',z:'+values3Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[6]+',z:'+values2Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[6]+',z:'+values1Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Toronto\',type:  \'spline\',data: [{y:'+values12Week[7]+',z:'+values12Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[7]+',z:'+values11Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[7]+',z:'+values10Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[7]+',z:'+values9Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[7]+',z:'+values8Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[7]+',z:'+values7Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[7]+',z:'+values6Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[7]+',z:'+values5Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[7]+',z:'+values4Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[7]+',z:'+values3Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[7]+',z:'+values2Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[7]+',z:'+values1Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Vancouver\',type:  \'spline\',data: [{y:'+values12Week[8]+',z:'+values12Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[8]+',z:'+values11Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[8]+',z:'+values10Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[8]+',z:'+values9Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[8]+',z:'+values8Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[8]+',z:'+values7Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[8]+',z:'+values6Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[8]+',z:'+values5Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[8]+',z:'+values4Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[8]+',z:'+values3Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[8]+',z:'+values2Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[8]+',z:'+values1Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=10&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+'"] },';

    //Virtual XC
    var cat=[11];
    var values12Week =getAvgSeriesPerWeekNST(from12,to12,cat);
    var values12WeekN =getAvgSeriesPerWeek_(from12,to12,cat);
    var values11Week =getAvgSeriesPerWeekNST(from11,to11,cat);
    var values11WeekN =getAvgSeriesPerWeek_(from11,to11,cat);
    var values10Week =getAvgSeriesPerWeekNST(from10,to10,cat);
    var values10WeekN =getAvgSeriesPerWeek_(from10,to10,cat);
    var values9Week =getAvgSeriesPerWeekNST(from9,to9,cat);
    var values9WeekN =getAvgSeriesPerWeek_(from9,to9,cat);
    var values8Week =getAvgSeriesPerWeekNST(from8,to8,cat);
    var values8WeekN =getAvgSeriesPerWeek_(from8,to8,cat);
    var values7Week =getAvgSeriesPerWeekNST(from7,to7,cat);
    var values7WeekN =getAvgSeriesPerWeek_(from7,to7,cat);
    var values6Week =getAvgSeriesPerWeekNST(from6,to6,cat);
    var values6WeekN =getAvgSeriesPerWeek_(from6,to6,cat);
    var values5Week =getAvgSeriesPerWeekNST(from5,to5,cat);
    var values5WeekN =getAvgSeriesPerWeek_(from5,to5,cat);
    var values4Week =getAvgSeriesPerWeekNST(from4,to4,cat);
    var values4WeekN =getAvgSeriesPerWeek_(from4,to4,cat);
    var values3Week =getAvgSeriesPerWeekNST(from3,to3,cat);
    var values3WeekN =getAvgSeriesPerWeek_(from3,to3,cat);
    var values2Week =getAvgSeriesPerWeekNST(from2,to2,cat);
    var values2WeekN =getAvgSeriesPerWeek_(from2,to2,cat);
    var values1Week =getAvgSeriesPerWeekNST(from1,to1,cat);
    var values1WeekN =getAvgSeriesPerWeek_(from1,to1,cat);
    var addseries= "{name: 'Standard',type: 'spline','visible': false, color: '#0000CC',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=11&cattype=1&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=11&cattype=1&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=11&cattype=1&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=11&cattype=1&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=11&cattype=1&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=11&cattype=1&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=11&cattype=1&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=11&cattype=1&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=11&cattype=1&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=11&cattype=1&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=11&cattype=1&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=11&cattype=1&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    var seriesVolSt=getVolumeSeriesNStandard("Virtual Interconnection",cat);
    seriesReturn +='{ "VXC": ["'+seriesVolSt+'","{name: \'Columbus\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Dallas\',type: \'spline\',data: [{y:'+values12Week[1]+',z:'+values12Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[1]+',z:'+values11Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[1]+',z:'+values10Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[1]+',z:'+values9Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[1]+',z:'+values8Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[1]+',z:'+values7Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[1]+',z:'+values6Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[1]+',z:'+values5Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[1]+',z:'+values4Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[1]+',z:'+values3Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[1]+',z:'+values2Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[1]+',z:'+values1Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Jacksonville\',type: \'spline\',data: [{y:'+values12Week[2]+',z:'+values12Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[2]+',z:'+values11Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[2]+',z:'+values10Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[2]+',z:'+values9Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[2]+',z:'+values8Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[2]+',z:'+values7Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[2]+',z:'+values6Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[2]+',z:'+values5Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[2]+',z:'+values4Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[2]+',z:'+values3Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[2]+',z:'+values2Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[2]+',z:'+values1Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Jacksonville2\',type:  \'spline\',data: [{y:'+values12Week[3]+',z:'+values12Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[3]+',z:'+values11Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[3]+',z:'+values10Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[3]+',z:'+values9Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[3]+',z:'+values8Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[3]+',z:'+values7Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[3]+',z:'+values6Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[3]+',z:'+values5Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[3]+',z:'+values4Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[3]+',z:'+values3Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[3]+',z:'+values2Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[3]+',z:'+values1Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Lakeland\',type:  \'spline\',data: [{y:'+values12Week[4]+',z:'+values12Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[4]+',z:'+values11Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[4]+',z:'+values10Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[4]+',z:'+values9Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[4]+',z:'+values8Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[4]+',z:'+values7Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[4]+',z:'+values6Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[4]+',z:'+values5Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[4]+',z:'+values4Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[4]+',z:'+values3Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[4]+',z:'+values2Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[4]+',z:'+values1Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Minneapolis\',type:  \'spline\',data: [{y:'+values12Week[5]+',z:'+values12Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[5]+',z:'+values11Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[5]+',z:'+values10Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[5]+',z:'+values9Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[5]+',z:'+values8Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[5]+',z:'+values7Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[5]+',z:'+values6Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[5]+',z:'+values5Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[5]+',z:'+values4Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[5]+',z:'+values3Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[5]+',z:'+values2Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[5]+',z:'+values1Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Montreal\',type:  \'spline\',data: [{y:'+values12Week[6]+',z:'+values12Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[6]+',z:'+values11Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[6]+',z:'+values10Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[6]+',z:'+values9Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[6]+',z:'+values8Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[6]+',z:'+values7Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[6]+',z:'+values6Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[6]+',z:'+values5Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[6]+',z:'+values4Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[6]+',z:'+values3Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[6]+',z:'+values2Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[6]+',z:'+values1Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Toronto\',type:  \'spline\',data: [{y:'+values12Week[7]+',z:'+values12Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[7]+',z:'+values11Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[7]+',z:'+values10Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[7]+',z:'+values9Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[7]+',z:'+values8Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[7]+',z:'+values7Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[7]+',z:'+values6Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[7]+',z:'+values5Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[7]+',z:'+values4Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[7]+',z:'+values3Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[7]+',z:'+values2Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[7]+',z:'+values1Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Vancouver\',type:  \'spline\',data: [{y:'+values12Week[8]+',z:'+values12Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[8]+',z:'+values11Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[8]+',z:'+values10Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[8]+',z:'+values9Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[8]+',z:'+values8Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[8]+',z:'+values7Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[8]+',z:'+values6Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[8]+',z:'+values5Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[8]+',z:'+values4Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[8]+',z:'+values3Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[8]+',z:'+values2Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[8]+',z:'+values1Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=11&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+'"] },';

    // XC
    var cat=[4];
    var values12Week =getAvgSeriesPerWeekNST(from12,to12,cat);
    var values12WeekN =getAvgSeriesPerWeek_(from12,to12,cat);
    var values11Week =getAvgSeriesPerWeekNST(from11,to11,cat);
    var values11WeekN =getAvgSeriesPerWeek_(from11,to11,cat);
    var values10Week =getAvgSeriesPerWeekNST(from10,to10,cat);
    var values10WeekN =getAvgSeriesPerWeek_(from10,to10,cat);
    var values9Week =getAvgSeriesPerWeekNST(from9,to9,cat);
    var values9WeekN =getAvgSeriesPerWeek_(from9,to9,cat);
    var values8Week =getAvgSeriesPerWeekNST(from8,to8,cat);
    var values8WeekN =getAvgSeriesPerWeek_(from8,to8,cat);
    var values7Week =getAvgSeriesPerWeekNST(from7,to7,cat);
    var values7WeekN =getAvgSeriesPerWeek_(from7,to7,cat);
    var values6Week =getAvgSeriesPerWeekNST(from6,to6,cat);
    var values6WeekN =getAvgSeriesPerWeek_(from6,to6,cat);
    var values5Week =getAvgSeriesPerWeekNST(from5,to5,cat);
    var values5WeekN =getAvgSeriesPerWeek_(from5,to5,cat);
    var values4Week =getAvgSeriesPerWeekNST(from4,to4,cat);
    var values4WeekN =getAvgSeriesPerWeek_(from4,to4,cat);
    var values3Week =getAvgSeriesPerWeekNST(from3,to3,cat);
    var values3WeekN =getAvgSeriesPerWeek_(from3,to3,cat);
    var values2Week =getAvgSeriesPerWeekNST(from2,to2,cat);
    var values2WeekN =getAvgSeriesPerWeek_(from2,to2,cat);
    var values1Week =getAvgSeriesPerWeekNST(from1,to1,cat);
    var values1WeekN =getAvgSeriesPerWeek_(from1,to1,cat);
    var addseries= "{name: 'Standard',type: 'spline','visible': false, color: '#0000CC',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=4&cattype=1&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=4&cattype=1&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=4&cattype=1&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=4&cattype=1&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=4&cattype=1&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=4&cattype=1&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=4&cattype=1&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=4&cattype=1&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=4&cattype=1&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=4&cattype=1&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=4&cattype=1&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=4&cattype=1&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    var seriesVolSt=getVolumeSeriesNStandard("Interconnection",cat);
    seriesReturn +='{ "XC": ["'+seriesVolSt+'","{name: \'Columbus\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Dallas\',type: \'spline\',data: [{y:'+values12Week[1]+',z:'+values12Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[1]+',z:'+values11Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[1]+',z:'+values10Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[1]+',z:'+values9Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[1]+',z:'+values8Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[1]+',z:'+values7Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[1]+',z:'+values6Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[1]+',z:'+values5Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[1]+',z:'+values4Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[1]+',z:'+values3Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[1]+',z:'+values2Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[1]+',z:'+values1Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Jacksonville\',type: \'spline\',data: [{y:'+values12Week[2]+',z:'+values12Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[2]+',z:'+values11Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[2]+',z:'+values10Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[2]+',z:'+values9Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[2]+',z:'+values8Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[2]+',z:'+values7Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[2]+',z:'+values6Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[2]+',z:'+values5Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[2]+',z:'+values4Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[2]+',z:'+values3Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[2]+',z:'+values2Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[2]+',z:'+values1Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Jacksonville2\',type:  \'spline\',data: [{y:'+values12Week[3]+',z:'+values12Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[3]+',z:'+values11Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[3]+',z:'+values10Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[3]+',z:'+values9Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[3]+',z:'+values8Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[3]+',z:'+values7Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[3]+',z:'+values6Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[3]+',z:'+values5Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[3]+',z:'+values4Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[3]+',z:'+values3Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[3]+',z:'+values2Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[3]+',z:'+values1Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Lakeland\',type:  \'spline\',data: [{y:'+values12Week[4]+',z:'+values12Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[4]+',z:'+values11Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[4]+',z:'+values10Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[4]+',z:'+values9Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[4]+',z:'+values8Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[4]+',z:'+values7Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[4]+',z:'+values6Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[4]+',z:'+values5Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[4]+',z:'+values4Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[4]+',z:'+values3Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[4]+',z:'+values2Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[4]+',z:'+values1Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Minneapolis\',type:  \'spline\',data: [{y:'+values12Week[5]+',z:'+values12Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[5]+',z:'+values11Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[5]+',z:'+values10Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[5]+',z:'+values9Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[5]+',z:'+values8Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[5]+',z:'+values7Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[5]+',z:'+values6Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[5]+',z:'+values5Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[5]+',z:'+values4Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[5]+',z:'+values3Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[5]+',z:'+values2Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[5]+',z:'+values1Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Montreal\',type:  \'spline\',data: [{y:'+values12Week[6]+',z:'+values12Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[6]+',z:'+values11Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[6]+',z:'+values10Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[6]+',z:'+values9Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[6]+',z:'+values8Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[6]+',z:'+values7Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[6]+',z:'+values6Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[6]+',z:'+values5Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[6]+',z:'+values4Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[6]+',z:'+values3Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[6]+',z:'+values2Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[6]+',z:'+values1Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Toronto\',type:  \'spline\',data: [{y:'+values12Week[7]+',z:'+values12Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[7]+',z:'+values11Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[7]+',z:'+values10Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[7]+',z:'+values9Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[7]+',z:'+values8Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[7]+',z:'+values7Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[7]+',z:'+values6Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[7]+',z:'+values5Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[7]+',z:'+values4Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[7]+',z:'+values3Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[7]+',z:'+values2Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[7]+',z:'+values1Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Vancouver\',type:  \'spline\',data: [{y:'+values12Week[8]+',z:'+values12Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[8]+',z:'+values11Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[8]+',z:'+values10Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[8]+',z:'+values9Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[8]+',z:'+values8Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[8]+',z:'+values7Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[8]+',z:'+values6Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[8]+',z:'+values5Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[8]+',z:'+values4Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[8]+',z:'+values3Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[8]+',z:'+values2Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[8]+',z:'+values1Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=4&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+'"] },';
// Network
    var cat=[5];
    var values12Week =getAvgSeriesPerWeekNST(from12,to12,cat);
    var values12WeekN =getAvgSeriesPerWeek_(from12,to12,cat);
    var values11Week =getAvgSeriesPerWeekNST(from11,to11,cat);
    var values11WeekN =getAvgSeriesPerWeek_(from11,to11,cat);
    var values10Week =getAvgSeriesPerWeekNST(from10,to10,cat);
    var values10WeekN =getAvgSeriesPerWeek_(from10,to10,cat);
    var values9Week =getAvgSeriesPerWeekNST(from9,to9,cat);
    var values9WeekN =getAvgSeriesPerWeek_(from9,to9,cat);
    var values8Week =getAvgSeriesPerWeekNST(from8,to8,cat);
    var values8WeekN =getAvgSeriesPerWeek_(from8,to8,cat);
    var values7Week =getAvgSeriesPerWeekNST(from7,to7,cat);
    var values7WeekN =getAvgSeriesPerWeek_(from7,to7,cat);
    var values6Week =getAvgSeriesPerWeekNST(from6,to6,cat);
    var values6WeekN =getAvgSeriesPerWeek_(from6,to6,cat);
    var values5Week =getAvgSeriesPerWeekNST(from5,to5,cat);
    var values5WeekN =getAvgSeriesPerWeek_(from5,to5,cat);
    var values4Week =getAvgSeriesPerWeekNST(from4,to4,cat);
    var values4WeekN =getAvgSeriesPerWeek_(from4,to4,cat);
    var values3Week =getAvgSeriesPerWeekNST(from3,to3,cat);
    var values3WeekN =getAvgSeriesPerWeek_(from3,to3,cat);
    var values2Week =getAvgSeriesPerWeekNST(from2,to2,cat);
    var values2WeekN =getAvgSeriesPerWeek_(from2,to2,cat);
    var values1Week =getAvgSeriesPerWeekNST(from1,to1,cat);
    var values1WeekN =getAvgSeriesPerWeek_(from1,to1,cat);
    var addseries= "{name: 'Standard',type: 'spline','visible': false, color: '#0000CC',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=5&cattype=1&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=5&cattype=1&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=5&cattype=1&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=5&cattype=1&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=5&cattype=1&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=5&cattype=1&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=5&cattype=1&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=5&cattype=1&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=5&cattype=1&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=5&cattype=1&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=5&cattype=1&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=5&cattype=1&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
    var seriesVolSt=getVolumeSeriesNStandard("Network",cat);
    seriesReturn +='{ "Network": ["'+seriesVolSt+'","{name: \'Columbus\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Dallas\',type: \'spline\',data: [{y:'+values12Week[1]+',z:'+values12Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[1]+',z:'+values11Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[1]+',z:'+values10Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[1]+',z:'+values9Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[1]+',z:'+values8Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[1]+',z:'+values7Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[1]+',z:'+values6Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[1]+',z:'+values5Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[1]+',z:'+values4Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[1]+',z:'+values3Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[1]+',z:'+values2Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[1]+',z:'+values1Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Jacksonville\',type: \'spline\',data: [{y:'+values12Week[2]+',z:'+values12Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[2]+',z:'+values11Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[2]+',z:'+values10Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[2]+',z:'+values9Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[2]+',z:'+values8Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[2]+',z:'+values7Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[2]+',z:'+values6Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[2]+',z:'+values5Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[2]+',z:'+values4Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[2]+',z:'+values3Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[2]+',z:'+values2Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[2]+',z:'+values1Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Jacksonville2\',type:  \'spline\',data: [{y:'+values12Week[3]+',z:'+values12Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[3]+',z:'+values11Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[3]+',z:'+values10Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[3]+',z:'+values9Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[3]+',z:'+values8Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[3]+',z:'+values7Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[3]+',z:'+values6Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[3]+',z:'+values5Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[3]+',z:'+values4Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[3]+',z:'+values3Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[3]+',z:'+values2Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[3]+',z:'+values1Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Lakeland\',type:  \'spline\',data: [{y:'+values12Week[4]+',z:'+values12Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[4]+',z:'+values11Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[4]+',z:'+values10Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[4]+',z:'+values9Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[4]+',z:'+values8Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[4]+',z:'+values7Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[4]+',z:'+values6Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[4]+',z:'+values5Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[4]+',z:'+values4Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[4]+',z:'+values3Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[4]+',z:'+values2Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[4]+',z:'+values1Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Minneapolis\',type:  \'spline\',data: [{y:'+values12Week[5]+',z:'+values12Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[5]+',z:'+values11Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[5]+',z:'+values10Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[5]+',z:'+values9Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[5]+',z:'+values8Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[5]+',z:'+values7Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[5]+',z:'+values6Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[5]+',z:'+values5Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[5]+',z:'+values4Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[5]+',z:'+values3Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[5]+',z:'+values2Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[5]+',z:'+values1Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Montreal\',type:  \'spline\',data: [{y:'+values12Week[6]+',z:'+values12Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[6]+',z:'+values11Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[6]+',z:'+values10Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[6]+',z:'+values9Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[6]+',z:'+values8Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[6]+',z:'+values7Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[6]+',z:'+values6Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[6]+',z:'+values5Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[6]+',z:'+values4Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[6]+',z:'+values3Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[6]+',z:'+values2Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[6]+',z:'+values1Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Toronto\',type:  \'spline\',data: [{y:'+values12Week[7]+',z:'+values12Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[7]+',z:'+values11Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[7]+',z:'+values10Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[7]+',z:'+values9Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[7]+',z:'+values8Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[7]+',z:'+values7Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[7]+',z:'+values6Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[7]+',z:'+values5Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[7]+',z:'+values4Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[7]+',z:'+values3Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[7]+',z:'+values2Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[7]+',z:'+values1Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Vancouver\',type:  \'spline\',data: [{y:'+values12Week[8]+',z:'+values12Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[8]+',z:'+values11Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[8]+',z:'+values10Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[8]+',z:'+values9Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[8]+',z:'+values8Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[8]+',z:'+values7Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[8]+',z:'+values6Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[8]+',z:'+values5Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[8]+',z:'+values4Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[8]+',z:'+values3Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[8]+',z:'+values2Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[8]+',z:'+values1Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=5&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+'"] },';
// Power
    var cat=[8];
    var values12Week =getAvgSeriesPerWeekNST(from12,to12,cat);
    var values12WeekN =getAvgSeriesPerWeek_(from12,to12,cat);
    var values11Week =getAvgSeriesPerWeekNST(from11,to11,cat);
    var values11WeekN =getAvgSeriesPerWeek_(from11,to11,cat);
    var values10Week =getAvgSeriesPerWeekNST(from10,to10,cat);
    var values10WeekN =getAvgSeriesPerWeek_(from10,to10,cat);
    var values9Week =getAvgSeriesPerWeekNST(from9,to9,cat);
    var values9WeekN =getAvgSeriesPerWeek_(from9,to9,cat);
    var values8Week =getAvgSeriesPerWeekNST(from8,to8,cat);
    var values8WeekN =getAvgSeriesPerWeek_(from8,to8,cat);
    var values7Week =getAvgSeriesPerWeekNST(from7,to7,cat);
    var values7WeekN =getAvgSeriesPerWeek_(from7,to7,cat);
    var values6Week =getAvgSeriesPerWeekNST(from6,to6,cat);
    var values6WeekN =getAvgSeriesPerWeek_(from6,to6,cat);
    var values5Week =getAvgSeriesPerWeekNST(from5,to5,cat);
    var values5WeekN =getAvgSeriesPerWeek_(from5,to5,cat);
    var values4Week =getAvgSeriesPerWeekNST(from4,to4,cat);
    var values4WeekN =getAvgSeriesPerWeek_(from4,to4,cat);
    var values3Week =getAvgSeriesPerWeekNST(from3,to3,cat);
    var values3WeekN =getAvgSeriesPerWeek_(from3,to3,cat);
    var values2Week =getAvgSeriesPerWeekNST(from2,to2,cat);
    var values2WeekN =getAvgSeriesPerWeek_(from2,to2,cat);
    var values1Week =getAvgSeriesPerWeekNST(from1,to1,cat);
    var values1WeekN =getAvgSeriesPerWeek_(from1,to1,cat);
    var addseries= "{name: 'Standard',type: 'spline','visible': false, color: '#0000CC',data: [{y:"+values12WeekN[0]+",z:"+values12WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=8&cattype=1&ndays=1&week="+returnFunctionDatesGrid[0]+"\'},{y:"+values11WeekN[0]+",z:"+values11WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=8&cattype=1&ndays=1&week="+returnFunctionDatesGrid[1]+"\'},{y:"+values10WeekN[0]+",z:"+values10WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=8&cattype=1&ndays=1&week="+returnFunctionDatesGrid[2]+"\'},{y:"+values9WeekN[0]+",z:"+values9WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=8&cattype=1&ndays=1&week="+returnFunctionDatesGrid[3]+"\'},{y:"+values8WeekN[0]+",z:"+values8WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=8&cattype=1&ndays=1&week="+returnFunctionDatesGrid[4]+"\'},{y:"+values7WeekN[0]+",z:"+values7WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=8&cattype=1&ndays=1&week="+returnFunctionDatesGrid[5]+"\'},{y:"+values6WeekN[0]+",z:"+values6WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=8&cattype=1&ndays=1&week="+returnFunctionDatesGrid[6]+"\'},{y:"+values5WeekN[0]+",z:"+values5WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=8&cattype=1&ndays=1&week="+returnFunctionDatesGrid[7]+"\'},{y:"+values4WeekN[0]+",z:"+values4WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=8&cattype=1&ndays=1&week="+returnFunctionDatesGrid[8]+"\'},{y:"+values3WeekN[0]+",z:"+values3WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=8&cattype=1&ndays=1&week="+returnFunctionDatesGrid[9]+"\'},{y:"+values2WeekN[0]+",z:"+values2WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=8&cattype=1&ndays=1&week="+returnFunctionDatesGrid[10]+"\'},{y:"+values1WeekN[0]+",z:"+values1WeekN[1]+",url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Standard&location=&product=8&cattype=1&ndays=1&week="+returnFunctionDatesGrid[11]+"\'}]}";
     var seriesVolSt=getVolumeSeriesNStandard("Power",cat);
    seriesReturn +='{ "Power": ["'+seriesVolSt+'","{name: \'Columbus\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Dallas\',type: \'spline\',data: [{y:'+values12Week[1]+',z:'+values12Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[1]+',z:'+values11Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[1]+',z:'+values10Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[1]+',z:'+values9Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[1]+',z:'+values8Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[1]+',z:'+values7Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[1]+',z:'+values6Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[1]+',z:'+values5Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[1]+',z:'+values4Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[1]+',z:'+values3Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[1]+',z:'+values2Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[1]+',z:'+values1Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Jacksonville\',type: \'spline\',data: [{y:'+values12Week[2]+',z:'+values12Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[2]+',z:'+values11Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[2]+',z:'+values10Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[2]+',z:'+values9Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[2]+',z:'+values8Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[2]+',z:'+values7Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[2]+',z:'+values6Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[2]+',z:'+values5Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[2]+',z:'+values4Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[2]+',z:'+values3Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[2]+',z:'+values2Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[2]+',z:'+values1Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Jacksonville2\',type:  \'spline\',data: [{y:'+values12Week[3]+',z:'+values12Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[3]+',z:'+values11Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[3]+',z:'+values10Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[3]+',z:'+values9Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[3]+',z:'+values8Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[3]+',z:'+values7Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[3]+',z:'+values6Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[3]+',z:'+values5Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[3]+',z:'+values4Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[3]+',z:'+values3Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[3]+',z:'+values2Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[3]+',z:'+values1Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Lakeland\',type:  \'spline\',data: [{y:'+values12Week[4]+',z:'+values12Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[4]+',z:'+values11Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[4]+',z:'+values10Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[4]+',z:'+values9Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[4]+',z:'+values8Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[4]+',z:'+values7Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[4]+',z:'+values6Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[4]+',z:'+values5Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[4]+',z:'+values4Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[4]+',z:'+values3Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[4]+',z:'+values2Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[4]+',z:'+values1Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Minneapolis\',type:  \'spline\',data: [{y:'+values12Week[5]+',z:'+values12Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[5]+',z:'+values11Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[5]+',z:'+values10Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[5]+',z:'+values9Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[5]+',z:'+values8Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[5]+',z:'+values7Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[5]+',z:'+values6Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[5]+',z:'+values5Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[5]+',z:'+values4Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[5]+',z:'+values3Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[5]+',z:'+values2Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[5]+',z:'+values1Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Montreal\',type:  \'spline\',data: [{y:'+values12Week[6]+',z:'+values12Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[6]+',z:'+values11Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[6]+',z:'+values10Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[6]+',z:'+values9Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[6]+',z:'+values8Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[6]+',z:'+values7Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[6]+',z:'+values6Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[6]+',z:'+values5Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[6]+',z:'+values4Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[6]+',z:'+values3Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[6]+',z:'+values2Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[6]+',z:'+values1Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Toronto\',type:  \'spline\',data: [{y:'+values12Week[7]+',z:'+values12Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[7]+',z:'+values11Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[7]+',z:'+values10Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[7]+',z:'+values9Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[7]+',z:'+values8Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[7]+',z:'+values7Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[7]+',z:'+values6Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[7]+',z:'+values5Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[7]+',z:'+values4Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[7]+',z:'+values3Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[7]+',z:'+values2Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[7]+',z:'+values1Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Vancouver\',type:  \'spline\',data: [{y:'+values12Week[8]+',z:'+values12Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[8]+',z:'+values11Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[8]+',z:'+values10Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[8]+',z:'+values9Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[8]+',z:'+values8Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[8]+',z:'+values7Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[8]+',z:'+values6Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[8]+',z:'+values5Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[8]+',z:'+values4Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[8]+',z:'+values3Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[8]+',z:'+values2Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[8]+',z:'+values1Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=8&cattype=2&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},'+addseries+'"] },';


    return seriesReturn;
}
function getAvgSeriesAll(){
    var series='';
    var returnFunctionDatesGrid=return_weeksforGrid();
    //All Markets
    var location=[];
    var to12=addDays(returnFunctionDatesGrid[0],6);
    var from12=addDays(returnFunctionDatesGrid[0],-84);


    //week11
    var from11=addDays(returnFunctionDatesGrid[1],-84);
    var to11=addDays(returnFunctionDatesGrid[1],6);
    //week10
    var from10=addDays(returnFunctionDatesGrid[2],-84);
    var to10=addDays(returnFunctionDatesGrid[2],6);
    //week9
    var from9=addDays(returnFunctionDatesGrid[3],-84);
    var to9=addDays(returnFunctionDatesGrid[3],6);
    //week8
    var from8=addDays(returnFunctionDatesGrid[4],-84);
    var to8=addDays(returnFunctionDatesGrid[4],6);
    //week7
    var from7=addDays(returnFunctionDatesGrid[5],-84);
    var to7=addDays(returnFunctionDatesGrid[5],6);
    //week6
    var from6=addDays(returnFunctionDatesGrid[6],-84);
    var to6=addDays(returnFunctionDatesGrid[6],6);
    //week5
    var from5=addDays(returnFunctionDatesGrid[7],-84);
    var to5=addDays(returnFunctionDatesGrid[7],6);
    //week4
    var from4=addDays(returnFunctionDatesGrid[8],-84);
    var to4=addDays(returnFunctionDatesGrid[8],6);

    //week3
    var from3=addDays(returnFunctionDatesGrid[9],-84);
    var to3=addDays(returnFunctionDatesGrid[9],6);

    //week2
    var from2=addDays(returnFunctionDatesGrid[10],-84);
    var to2=addDays(returnFunctionDatesGrid[10],6);
    //week1
    var from1=addDays(returnFunctionDatesGrid[11],-84);
    var to1=addDays(returnFunctionDatesGrid[11],6);
    //ALL
    var cat=[10,11,4,5,8];
    var values12Week =getAvgSeriesPerWeekAll(from12,to12,cat);
    var values11Week =getAvgSeriesPerWeekAll(from11,to11,cat);
    var values10Week =getAvgSeriesPerWeekAll(from10,to10,cat);
    var values9Week =getAvgSeriesPerWeekAll(from9,to9,cat);
    var values8Week =getAvgSeriesPerWeekAll(from8,to8,cat);
    var values7Week =getAvgSeriesPerWeekAll(from7,to7,cat);
    var values6Week =getAvgSeriesPerWeekAll(from6,to6,cat);
    var values5Week =getAvgSeriesPerWeekAll(from5,to5,cat);
    var values4Week =getAvgSeriesPerWeekAll(from4,to4,cat);
    var values3Week =getAvgSeriesPerWeekAll(from3,to3,cat);
    var values2Week =getAvgSeriesPerWeekAll(from2,to2,cat);
    var values1Week =getAvgSeriesPerWeekAll(from1,to1,cat);

    nlapiLogExecution('DEBUG','values1Week', to1 + ': ' + cat);
    var seriesVolSt=getVolumeAllSeries(cat);
    var seriesReturn='{ "ALL": ["'+seriesVolSt+'","{name: \'Columbus\',type: \'spline\',data: [{y:'+values12Week[0]+',z:'+values12Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[0]+',z:'+values11Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[0]+',z:'+values10Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[0]+',z:'+values9Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[0]+',z:'+values8Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[0]+',z:'+values7Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[0]+',z:'+values6Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[0]+',z:'+values5Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[0]+',z:'+values4Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[0]+',z:'+values3Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[0]+',z:'+values2Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[0]+',z:'+values1Week[9]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Columbus&location=33&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Dallas\',type: \'spline\',data: [{y:'+values12Week[1]+',z:'+values12Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[1]+',z:'+values11Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[1]+',z:'+values10Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[1]+',z:'+values9Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[1]+',z:'+values8Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[1]+',z:'+values7Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[1]+',z:'+values6Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[1]+',z:'+values5Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[1]+',z:'+values4Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[1]+',z:'+values3Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[1]+',z:'+values2Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[1]+',z:'+values1Week[10]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Dallas&location=20&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Jacksonville\',type: \'spline\',data: [{y:'+values12Week[2]+',z:'+values12Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[2]+',z:'+values11Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[2]+',z:'+values10Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[2]+',z:'+values9Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[2]+',z:'+values8Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[2]+',z:'+values7Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[2]+',z:'+values6Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[2]+',z:'+values5Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[2]+',z:'+values4Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[2]+',z:'+values3Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[2]+',z:'+values2Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[2]+',z:'+values1Week[11]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville&location=29&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Jacksonville2\',type:  \'spline\',data: [{y:'+values12Week[3]+',z:'+values12Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[3]+',z:'+values11Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[3]+',z:'+values10Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[3]+',z:'+values9Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[3]+',z:'+values8Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[3]+',z:'+values7Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[3]+',z:'+values6Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[3]+',z:'+values5Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[3]+',z:'+values4Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[3]+',z:'+values3Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[3]+',z:'+values2Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[3]+',z:'+values1Week[12]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Jacksonville2&location=43&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]},' +
        '{name: \'Lakeland\',type:  \'spline\',data: [{y:'+values12Week[4]+',z:'+values12Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[4]+',z:'+values11Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[4]+',z:'+values10Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[4]+',z:'+values9Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[4]+',z:'+values8Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[4]+',z:'+values7Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[4]+',z:'+values6Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[4]+',z:'+values5Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[4]+',z:'+values4Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[4]+',z:'+values3Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[4]+',z:'+values2Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[4]+',z:'+values1Week[13]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Lakeland&location=41&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Minneapolis\',type:  \'spline\',data: [{y:'+values12Week[5]+',z:'+values12Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[5]+',z:'+values11Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[5]+',z:'+values10Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[5]+',z:'+values9Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[5]+',z:'+values8Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[5]+',z:'+values7Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[5]+',z:'+values6Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[5]+',z:'+values5Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[5]+',z:'+values4Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[5]+',z:'+values3Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[5]+',z:'+values2Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[5]+',z:'+values1Week[14]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Minneapolis&location=22&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Montreal\',type:  \'spline\',data: [{y:'+values12Week[6]+',z:'+values12Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[6]+',z:'+values11Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[6]+',z:'+values10Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[6]+',z:'+values9Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[6]+',z:'+values8Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[6]+',z:'+values7Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[6]+',z:'+values6Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[6]+',z:'+values5Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[6]+',z:'+values4Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[6]+',z:'+values3Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[6]+',z:'+values2Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[6]+',z:'+values1Week[15]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Montreal&location=25&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Toronto\',type:  \'spline\',data: [{y:'+values12Week[7]+',z:'+values12Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[7]+',z:'+values11Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[7]+',z:'+values10Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[7]+',z:'+values9Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[7]+',z:'+values8Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[7]+',z:'+values7Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[7]+',z:'+values6Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[7]+',z:'+values5Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[7]+',z:'+values4Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[7]+',z:'+values3Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[7]+',z:'+values2Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[7]+',z:'+values1Week[16]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Toronto&location=23&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}' +
        ',{name: \'Vancouver\',type:  \'spline\',data: [{y:'+values12Week[8]+',z:'+values12Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[0]+'\'},{y:'+values11Week[8]+',z:'+values11Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[1]+'\'},{y:'+values10Week[8]+',z:'+values10Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[2]+'\'},{y:'+values9Week[8]+',z:'+values9Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[3]+'\'},{y:'+values8Week[8]+',z:'+values8Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[4]+'\'},{y:'+values7Week[8]+',z:'+values7Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[5]+'\'},{y:'+values6Week[8]+',z:'+values6Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[6]+'\'},{y:'+values5Week[8]+',z:'+values5Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[7]+'\'},{y:'+values4Week[8]+',z:'+values4Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[8]+'\'},{y:'+values3Week[8]+',z:'+values3Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[9]+'\'},{y:'+values2Week[8]+',z:'+values2Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[10]+'\'},{y:'+values1Week[8]+',z:'+values1Week[17]+',url:\'/app/site/hosting/scriptlet.nl?script=527&deploy=1&comes=1&title=Vancouver&location=26&product=all&cattype=all&ndays=1&week='+returnFunctionDatesGrid[11]+'\'}]}"] },';
       return seriesReturn;
}


function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}