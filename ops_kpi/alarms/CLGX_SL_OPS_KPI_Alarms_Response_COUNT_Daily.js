nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_OPS_KPI_Alarms_Response_COUNT_Daily.js
//	Script Name:	CLGX_SL_OPS_KPI_Alarms_Response_COUNT_D
//	Script Id:		customscript_clgx_sl_ops_kpi_alarm_cnt_d
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		06/24/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=522&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_ops_kpi_alarm_count_daily (request, response){
    try {
    	
		var html = '';
    	var type = request.getParameter('type');
    	var categ = request.getParameter('categ');
    	var severity = request.getParameter('severity');
    	
    	var start = moment().subtract('days', 60).format('YYYY-MM-DD');
    	var end = moment().format('YYYY-MM-DD');
    	
    	var arrDaysCategs = new Array();
    	var arrDaysX = new Array();
    	for ( var i = 0; i < 60; i++ ) {
    		arrDaysCategs.push(moment().add('days', i-59).format('YYYY-MM-DD'));
    		arrDaysX.push(moment().add('days', i-59).format('MM-DD'));
    	}
    	
    	if (type == 'chart'){
    		
        	var arrCategNames = ["Comm Error","Breaker >80%","High Humidity","Hi/Low Temp","System Alarm","Others"];
        	var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/alarms/get_alarms_response_count_daily.cfm?start=' + start + '&end=' + end);
        	//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/ops_kpi/get_alarms_response_count_daily.cfm?start=' + start + '&end=' + end);
    	
        	var jsonAlarms = requestURL.body;
    		var arrReq = JSON.parse( jsonAlarms );
    		var arrMarkets = _.uniq(_.pluck(arrReq[1], 'MARKET'));
    		
    		var arrSeries = new Array();
    		
    		if(categ != 'undefined'){
    			
    			var arrRequest = _.filter(arrReq[1], function(arr){
    				return (arr.CATEGORY == arrCategNames[categ]);
    			});
    			
    			
	    		
	        	for ( var i = 0; i < arrMarkets.length; i++ ) {
	        		var objSeries = new Object();
	        		objSeries["name"] = arrMarkets[i];
	        		//objSeries["colorByPoint"] = true;
	        	
	        		var arrDays = new Array();
	        		for ( var j = 0; j < arrDaysCategs.length; j++ ) {
	        			var objDay = new Object();
	        			objDay["name"] = arrDaysX[j];
	        			objDay["drilldown"] = arrMarkets[i] + ' - ' + arrDaysX[j];
	        			
	        			var arrDayAlarm = _.filter(arrRequest, function(arr){
	        				return (arr.MARKET == arrMarkets[i] && arr.DAY == arrDaysCategs[j]);
	        			});
	        			var sum = 0;
						for ( var k = 0; k < arrDayAlarm.length; k++ ) {
							sum += arrDayAlarm[k].ALARMS;
						}
						objDay["y"] = sum;
						arrDays.push(objDay);
	        		}
	        		objSeries["data"] = arrDays;
	        		arrSeries.push(objSeries);
	        	}
	        	
		    	var arrDrillDown = new Array();
		    	for ( var i = 0; i < arrMarkets.length; i++ ) {
		    		
		    		for ( var j = 0; j < arrDaysCategs.length; j++ ) {
		    			var objDrillDown = new Object();
		    			objDrillDown["name"] = arrDaysX[j];
		    			objDrillDown["id"] = arrMarkets[i] + ' - ' + arrDaysX[j];
		    			objDrillDown["type"] = 'column';
		    			
		    			var arrCategLocations = _.filter(arrReq[2], function(arr){
		    				return (arr.CATEGORY == arrCategNames[categ] && arr.MARKET == arrMarkets[i] && arr.DAY == arrDaysCategs[j]);
		    			});
		    			var arrDrill = new Array();
		    			for ( var k = 0; k < arrCategLocations.length; k++ ) {
		    				var objDrill = new Object();
		        			var arrLocation = new Array();
		        			arrLocation.push(arrCategLocations[k].LOCATION);
		        			arrLocation.push(arrCategLocations[k].ALARMS);
		        			arrDrill.push(arrLocation);
						}
		        		objDrillDown["data"] = arrDrill;
		        		arrDrillDown.push(objDrillDown);
		    		}
		    	}
		    	
		    	
		    	// average & standard deviation
		    	var arrAVGs = new Array();
				for ( var i = 0; i < arrDaysCategs.length; i++ ) {
					var sum = 0;
					
					for ( var j = 0; j < arrMarkets.length; j++ ) {
						var objDay = _.find(arrRequest, function(arr){ return (arr.MARKET == arrMarkets[j] && arr.DAY == arrDaysCategs[i]); });
						if(objDay != null){
							sum += parseInt(objDay.ALARMS);
						}
					}
					arrAVGs.push(sum/arrMarkets.length);
				}

				sum = 0;
				for ( var i = 0; i < arrAVGs.length; i++ ) {
					sum += parseFloat(arrAVGs[i]);
				}
				avg = parseFloat((sum/arrAVGs.length).toFixed(2));
				std = parseFloat((math.std(arrAVGs)).toFixed(2));
		        stdfrom = parseFloat((avg - 3*std).toFixed(2));
		        stdto = parseFloat((avg + 3*std).toFixed(2));

    		}
    		else{
    			
    			for ( var j = 0; j < arrCategNames.length; j++ ) {
		    		var objSeries = new Object();
		    		objSeries["name"] = arrCategNames[j];
		    		
		    		var arrDays = new Array();
		    		for ( var k = 0; k < arrDaysCategs.length; k++ ) {
		    			var objDay = new Object();
		    			objDay["name"] = arrDaysX[k];
		    			objDay["drilldown"] = arrCategNames[j] + ' - ' + arrDaysX[k];
		    			
		    			var arrDayAlarm = _.filter(arrReq[0], function(arr){
		    				return (arr.CATEGORY == arrCategNames[j] && arr.DAY == arrDaysCategs[k]);
		    			});
		    			var sum = 0;
		    			for ( var l = 0; l < arrDayAlarm.length; l++ ) {
							sum += parseInt(arrDayAlarm[l].ALARMS);
						}
		    			if(arrDayAlarm.length > 0){
		    				objDay["y"] = sum;
		    			}
		    			else{
		    				objDay["y"] = 0;	
		    			}
		    			arrDays.push(objDay);
		    		}
		    		objSeries["data"] = arrDays;
		    		arrSeries.push(objSeries);
				}
				
		
				var arrDrillDown = new Array();
		    	
				for ( var j = 0; j < arrCategNames.length; j++ ) {
					
		    		for ( var k = 0; k < arrDaysCategs.length; k++ ) {
		    			var objDrillDown = new Object();
		    			objDrillDown["name"] = arrDaysX[k];
		    			objDrillDown["id"] = arrCategNames[j] + ' - ' + arrDaysX[k];
		    			objDrillDown["type"] = 'column';
		    			
		    			var arrCategLocations = _.filter(arrReq[2], function(arr){
		    				return (arr.CATEGORY == arrCategNames[j] && arr.DAY == arrDaysCategs[k]);
		    			});
		    			var arrDrill = new Array();
		    			for ( var l = 0; l < arrCategLocations.length; l++ ) {
		    				var objDrill = new Object();
		        			var arrLocation = new Array();
		        			arrLocation.push(arrCategLocations[l].LOCATION);
		        			arrLocation.push(parseInt(arrCategLocations[l].ALARMS));
		        			arrDrill.push(arrLocation);
						}
		        		objDrillDown["data"] = arrDrill;
		        		arrDrillDown.push(objDrillDown);
		    		}
				
				}
				
	    		var avg = 0;
	    		var std = 0;
		        var stdfrom = 0;
		        var stdto = 0;
			
    		}
    		

        	
	    	var objFile = nlapiLoadFile(2897086);
	        html = objFile.getValue();
	        if(categ != 'undefined'){
	        	html = html.replace(new RegExp('{title}','g'), 'Daily Alarms by Market - ' + arrCategNames[categ]);
	        }
	        else{
	        	html = html.replace(new RegExp('{title}','g'), 'Daily Alarms by Category');
	        }
	        html = html.replace(new RegExp('{subtitle}','g'), ' | From ' + start + ' to ' + end + ' |');
	        html = html.replace(new RegExp('{categories}','g'), JSON.stringify(arrDaysCategs));
	        html = html.replace(new RegExp('{series}','g'), JSON.stringify(arrSeries));
	        html = html.replace(new RegExp('{drilldown}','g'), JSON.stringify(arrDrillDown));
        
	        html = html.replace(new RegExp('{avgtitle}','g'), 'AVG: ' + avg.toFixed(2));
	        html = html.replace(new RegExp('{stdtitle}','g'), ' ' + std.toFixed(2));
	        html = html.replace(new RegExp('{stdfromtitle}','g'), ' ' + stdfrom.toFixed(2));
	        html = html.replace(new RegExp('{stdtotitle}','g'), ' ' + stdto.toFixed(2));
	        
	        html = html.replace(new RegExp('{average}','g'), avg);
	        html = html.replace(new RegExp('{stdev}','g'), std);
	        html = html.replace(new RegExp('{stdfrom}','g'), stdfrom);
	        html = html.replace(new RegExp('{stdto}','g'), stdto);
	        
    	}
    	else if (type == 'grid'){
    		
	    	var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/alarms/get_alarms_response_details.cfm?start=' + start + '&end=' + end);
	    	//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/ops_kpi/get_alarms_response_details.cfm?start=' + start + '&end=' + end);
			var jsonAlarms = requestURL.body;
			var arrAlarms = JSON.parse( jsonAlarms );

			var objFile = nlapiLoadFile(2851269);
	        html = objFile.getValue();
	        html = html.replace(new RegExp('{dataAlarms}','g'), JSON.stringify(arrAlarms));
	        html = html.replace(new RegExp('{title}','g'), '| Alarms ' + ' | From ' + start + ' to ' + end + ' |');
    	}
    	else{
    	}
    	response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

