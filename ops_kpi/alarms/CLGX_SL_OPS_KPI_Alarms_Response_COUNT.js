nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_OPS_KPI_Alarms_Response_COUNT.js
//	Script Name:	CLGX_SL_OPS_KPI_Alarms_Response_COUNT
//	Script Id:		customscript_clgx_sl_ops_kpi_alarm_count
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		06/24/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=520&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_ops_kpi_alarm_count (request, response){
    try {

        var html = '';
        var type = request.getParameter('type');
        var week = request.getParameter('week');
        var start = moment().subtract('weeks', week).startOf('week').format('YYYY-MM-DD');
        var end = moment().subtract('weeks', week).endOf('week').format('YYYY-MM-DD');

        if (type == 'chart'){


            var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/alarms/get_alarms_response_count_weekly.cfm?start=' + start + '&end=' + end);
            //var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/ops_kpi/get_alarms_response_count.cfm?start=' + start + '&end=' + end);
            var jsonAlarms = requestURL.body;
            var arrRequest = JSON.parse( jsonAlarms );

            var arrSeries = new Array();
            //var arrCategNames = ["Comm Error","Breaker >80%","High Humidity","Hi/Low Temp","System Alarm","Others"];
            var arrCategNames = _.uniq(_.pluck(arrRequest, 'CATEGORY'));

            var arrCategories = new Array();
            var sum = 0;
            for ( var i = 0; i < arrCategNames.length; i++ ) {

                var objCategory = new Object();
                objCategory["name"] = arrCategNames[i];
                objCategory["drilldown"] = arrCategNames[i];

                var arrCategLocations = _.filter(arrRequest, function(arr){
                    return (arr.CATEGORY == arrCategNames[i]);
                });
                var total = 0;
                for ( var j = 0; j < arrCategLocations.length; j++ ) {
                    total += arrCategLocations[j].ALARMS;
                }
                sum += total;
                objCategory["y"] = total;
                arrCategories.push(objCategory);
            }
            var arrCategSort = _.sortBy(arrCategories, function(obj){ return obj.y;}).reverse();

            var temp = 0;
            var arrPercentages = new Array();
            for ( var i = 0; i < arrCategSort.length; i++ ) {
                temp += arrCategSort[i].y*100/sum;
                arrPercentages.push(parseFloat(temp.toFixed(2)));
            }

            var arrDrillDown = new Array();
            for ( var i = 0; i < arrCategSort.length; i++ ) {

                var objDrillDown = new Object();
                objDrillDown["name"] = arrCategSort[i].name;
                objDrillDown["id"] = arrCategSort[i].name;

                var arrCategLocations = _.filter(arrRequest, function(arr){
                    return (arr.CATEGORY == arrCategSort[i].name);
                });
                var arrDrill = new Array();
                for ( var j = 0; j < arrCategLocations.length; j++ ) {
                    var objDrill = new Object();
                    var arrLocation = new Array();
                    arrLocation.push(arrCategLocations[j].LOCATION);
                    arrLocation.push(arrCategLocations[j].ALARMS);

                    arrDrill.push(arrLocation);
                }

                objDrillDown["data"] = arrDrill;
                arrDrillDown.push(objDrillDown);
            }

            var objFile = nlapiLoadFile(2864031);
            html = objFile.getValue();
            html = html.replace(new RegExp('{title}','g'), 'Alarms by Category');
            html = html.replace(new RegExp('{subtitle}','g'), ' | Week from ' + start + ' to ' + end + ' |');
            html = html.replace(new RegExp('{categories}','g'), JSON.stringify(arrCategSort));
            html = html.replace(new RegExp('{percentages}','g'), JSON.stringify(arrPercentages));
            html = html.replace(new RegExp('{drilldown}','g'), JSON.stringify(arrDrillDown));

        }
        else if (type == 'grid'){
            nlapiLogExecution('DEBUG','Before Request', 'Before Request');
          nlapiLogExecution('DEBUG','start + end', start+';'+end);
            var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/alarms/get_alarms_response_details.cfm?start=' + start + '&end=' + end);
           nlapiLogExecution('DEBUG','After Request_0', 'After Request_0');
            //var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/ops_kpi/get_alarms_response_details.cfm?start=' + start + '&end=' + end);
            var jsonAlarms = requestURL.body;
            var arrAlarms = JSON.parse( jsonAlarms );
            nlapiLogExecution('DEBUG','After Request', 'After Request');
            var objFile = nlapiLoadFile(2851269);
            html = objFile.getValue();
            html = html.replace(new RegExp('{dataAlarms}','g'), JSON.stringify(arrAlarms));
            html = html.replace(new RegExp('{title}','g'), '| Alarms ' + ' | Week from ' + start + ' to ' + end + ' |');
        }
        else{
        }
        response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function return_weeks (){
    var arrWeeks = new Array();
    for ( var i = 1; i < 13; i++ ) {
        arrWeeks.push(moment().startOf('week').add(1, 'days').subtract((13 - i), 'week').format('M/D'));
    }
    return JSON.stringify(arrWeeks);
}
