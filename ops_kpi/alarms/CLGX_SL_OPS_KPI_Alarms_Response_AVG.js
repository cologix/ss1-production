nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_OPS_KPI_Alarms_Response_AVG.js
//	Script Name:	CLGX_SL_OPS_KPI_Alarms_Response_AVG
//	Script Id:		customscript_clgx_sl_ops_kpi_alarm_avg
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		06/24/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=519&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_ops_kpi_alarm_avg (request, response){
    try {
    	
    	var html = '';
    	var type = request.getParameter('type');
    	var month = request.getParameter('month');
    	var start = moment().subtract('months', month).startOf('month').format('YYYY-MM-DD');
    	var end = moment().subtract('months', month).endOf('month').format('YYYY-MM-DD');
    	var severity = request.getParameter('node');
    	
    	var expected = 60;
    	var filter = 1;
    	var filter = 'None';
    	if(severity == 'LOW'){
    		filter = 2;
    		expected = 60;
    	}
    	else if(severity == 'MED'){
    		filter = 3;
    		expected = 30;
    	}
    	else if(severity == 'HIGH'){
    		filter = 4;
    		expected = 15;
    	}
    	else{
    		filter = 0;
    		expected = 0;
    	}
    	
    	

    	if (type == 'chart'){
    		
	    	var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/alarms/get_alarms_response_avg.cfm?start=' + start + '&end=' + end);
	    	//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/ops_kpi/get_alarms_response_avg.cfm?start=' + start + '&end=' + end);
			var jsonAlarms = requestURL.body;
			var arrRequest = JSON.parse( jsonAlarms );
	    	
			var arrMarkets = _.uniq(_.pluck(arrRequest, 'MARKET'));
			//var arrSeverities = _.uniq(_.pluck(arrRequest, 'SEVERITY'));
			var arrSeverities = [2,3,4];
			var arrSeverityNames = ['','None','Low','Medium','High'];
			
			var arrSeries = new Array();
			
			if(filter > 1){
				
				var arrAlarms = _.filter(arrRequest, function(arr){
			        return (arr.SEVERITY == filter);
				});
				
				var arrAverages = new Array();
				var arrExpected = new Array();
				for ( var i = 0; i < arrAlarms.length; i++ ) {
					arrAverages.push((parseFloat(arrAlarms[i].AVERAGE)));
					arrExpected.push(expected);
				}
				
				var objAverages = new Object();
				objAverages["type"] = 'column';
				objAverages["name"] = 'Markets';
				objAverages["data"] = arrAverages;
				arrSeries.push(objAverages);
		        
				var objExpected = new Object();
				objExpected["type"] = 'spline';
				objExpected["dashStyle"] = 'dot';
				objExpected["color"] = 'crimson';
				objExpected["name"] = 'Expected Response';
				objExpected["data"] = arrExpected;
				arrSeries.push(objExpected);
			}
			else{
				
				var arrAlarms = arrRequest;
				
				for ( var j = 0; j < arrSeverities.length; j++ ) {
					var arrSeverityAlarms = _.filter(arrAlarms, function(arr){
				        return (arr.SEVERITY == arrSeverities[j]);
					});
					var arrAverages = new Array();
					for ( var i = 0; i < arrSeverityAlarms.length; i++ ) {
						arrAverages.push((parseFloat(arrSeverityAlarms[i].AVERAGE)));
					}
					
					var objAverages = new Object();
					objAverages["type"] = 'column';
					objAverages["name"] = arrSeverityNames[arrSeverities[j]];
					objAverages["data"] = arrAverages;
					arrSeries.push(objAverages);
				}
			}
			
			var objFile = nlapiLoadFile(2842922);
	        html = objFile.getValue();
	        html = html.replace(new RegExp('{title}','g'), 'Average Response Time');
	        if(filter > 1){
	        	html = html.replace(new RegExp('{subtitle}','g'), 'Severity ' + severity + ' / ' + moment().subtract('months', month).startOf('month').format('MMM-YYYY'));
	        }
	        else{
	        	html = html.replace(new RegExp('{subtitle}','g'), 'Severity ALL / ' + moment().subtract('months', month).startOf('month').format('MMM-YYYY'));
	        }
	        html = html.replace(new RegExp('{ytitle}','g'), 'Minutes');
	        html = html.replace(new RegExp('{markets}','g'), JSON.stringify(arrMarkets));
	        html = html.replace(new RegExp('{series}','g'), JSON.stringify(arrSeries));
        
    	}
    	else if (type == 'grid'){
    		
	    	var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/alarms/get_alarms_response_details.cfm?start=' + start + '&end=' + end);
	    	//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/ops_kpi/get_alarms_response_details.cfm?start=' + start + '&end=' + end);
			var jsonAlarms = requestURL.body;
			var arrRequest = JSON.parse( jsonAlarms );
    		
			if(filter > 1){
				var arrAlarms = _.filter(arrRequest, function(arr){
			        return (arr.SEVERITY_ID == filter);
				});
				var lblSeverity = severity;
			}
			else{
				var arrAlarms = arrRequest;
				var lblSeverity = 'ALL';
			}
			
			var objFile = nlapiLoadFile(2851269);
	        html = objFile.getValue();
	        html = html.replace(new RegExp('{dataAlarms}','g'), JSON.stringify(arrAlarms));
	        html = html.replace(new RegExp('{title}','g'), '| ALARMS ' + ' | Severity - ' + lblSeverity + ' | Month from ' + start + ' to ' + end + ' |');
    	}
    	else{
    	}
    	response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function return_weeks (){
	var arrWeeks = new Array();
	for ( var i = 1; i < 13; i++ ) {
		arrWeeks.push(moment().startOf('week').add(1, 'days').subtract((13 - i), 'week').format('M/D'));
	}
    return JSON.stringify(arrWeeks);
}
