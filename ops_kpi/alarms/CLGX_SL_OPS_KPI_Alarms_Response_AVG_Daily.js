nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_OPS_KPI_Alarms_Response_AVG_Day.js
//	Script Name:	CLGX_SL_OPS_KPI_Alarms_Response_AVG_D
//	Script Id:		customscript_clgx_sl_ops_kpi_alarm_avg_d
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		06/24/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=524&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_ops_kpi_alarm_avg_daily (request, response){
    try {
    	
		var html = '';
    	var type = request.getParameter('type');
    	var severity = request.getParameter('severity');
    	
    	var start = moment().subtract('days', 60).format('YYYY-MM-DD');
    	var end = moment().format('YYYY-MM-DD');
    	
    	var arrDaysCategs = new Array();
    	var arrDaysX = new Array();
    	for ( var i = 0; i < 60; i++ ) {
    		arrDaysCategs.push(moment().add('days', i-59).format('YYYY-MM-DD'));
    		arrDaysX.push(moment().add('days', i-59).format('MM-DD'));
    	}
    	
    	if (type == 'chart'){
    		
        	var arrSeverities = ["Low","Medium","High"];
        	var arrExpected = [60,30,15];
        	var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/alarms/get_alarms_response_avg_daily.cfm?start=' + start + '&end=' + end + '&severity=' + severity);	
        	//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/ops_kpi/get_alarms_response_avg_daily.cfm?start=' + start + '&end=' + end + '&severity=' + severity);	
        	var jsonAlarms = requestURL.body;
    		var arrReq = JSON.parse( jsonAlarms );
    		
    		var arrSeries = new Array();
    		
    		var avg = 0;
    		var std = 0;
	        var stdfrom = 0;
	        var stdto = 0;
	        
        	if (severity > 0){
        		
        	    var arrRequest = _.filter(arrReq[3], function(arr){
    				return (arr.SEVERITY == severity);
    			});
    			var arrMarkets = _.uniq(_.pluck(arrRequest, 'MARKET'));
        		
	        	for ( var j = 0; j < arrMarkets.length; j++ ) {
		    		var objSeries = new Object();
		    		objSeries["name"] = arrMarkets[j];
		    		
		    		var arrDays = new Array();
		    		for ( var k = 0; k < arrDaysCategs.length; k++ ) {
		    			var objDay = new Object();
		    			objDay["name"] = arrDaysX[k];
		    			objDay["drilldown"] = arrMarkets[j] + ' - ' + arrDaysX[k];
		    			
		    			var arrDayAlarm = _.filter(arrRequest, function(arr){
		    				return (arr.MARKET == arrMarkets[j] && arr.DAY == arrDaysCategs[k]);
		    			});
		    			var sum = 0;
		    			for ( var l = 0; l < arrDayAlarm.length; l++ ) {
							sum += parseFloat(arrDayAlarm[l].AVERAGE);
						}
		    			if(arrDayAlarm.length > 0){
		    				objDay["y"] = parseFloat((sum/arrDayAlarm.length).toFixed(2));
		    			}
		    			else{
		    				objDay["y"] = 0;	
		    			}
		    			arrDays.push(objDay);
		    		}
		    		objSeries["data"] = arrDays;
		    		arrSeries.push(objSeries);
				}
				
		
				var arrDrillDown = new Array();
		    	
				for ( var j = 0; j < arrMarkets.length; j++ ) {
					
		    		for ( var k = 0; k < arrDaysCategs.length; k++ ) {
		    			var objDrillDown = new Object();
		    			objDrillDown["name"] = arrDaysX[k];
		    			objDrillDown["id"] = arrMarkets[j] + ' - ' + arrDaysX[k];
		    			objDrillDown["type"] = 'column';
		    			
		    			var arrCategLocations = _.filter(arrReq[4], function(arr){
		    				return (arr.SEVERITY == severity && arr.MARKET == arrMarkets[j] && arr.DAY == arrDaysCategs[k]);
		    			});
		    			var arrDrill = new Array();
		    			for ( var l = 0; l < arrCategLocations.length; l++ ) {
		    				var objDrill = new Object();
		        			var arrLocation = new Array();
		        			arrLocation.push(arrCategLocations[l].LOCATION);
		        			arrLocation.push(parseFloat((parseFloat(arrCategLocations[l].AVERAGE)).toFixed(2)));
		        			arrDrill.push(arrLocation);
						}
		        		objDrillDown["data"] = arrDrill;
		        		arrDrillDown.push(objDrillDown);
		    		}
				
				}
        	
				var arrExistingDays = _.uniq(_.pluck(arrReq[5], 'DAY'));
				
		    	var arrSeverAVGs = new Array();
				for ( var i = 0; i < arrExistingDays.length; i++ ) {
					var sum = 0;
					
					var arrExistingMarkets = _.filter(arrReq[5], function(arr){
				        return (arr.DAY == arrExistingDays[i]);
					});
					var arrExistingMarketsIDs = _.uniq(_.pluck(arrExistingMarkets, 'MARKET'));
					
					for ( var j = 0; j < arrExistingMarketsIDs.length; j++ ) {
						var objDay = _.find(arrReq[5], function(arr){ return (arr.MARKET == arrExistingMarketsIDs[j] && arr.DAY == arrExistingDays[i]); });
						if(objDay != null){
							sum += parseFloat(objDay.AVERAGE);
						}
					}
					arrSeverAVGs.push(sum/arrExistingMarkets.length);
				}

				sum = 0;
				for ( var i = 0; i < arrSeverAVGs.length; i++ ) {
					sum += parseFloat(arrSeverAVGs[i]);
				}
				avg = parseFloat((sum/arrSeverAVGs.length).toFixed(2));
				std = parseFloat((math.std(arrSeverAVGs)).toFixed(2));
		        stdfrom = parseFloat((avg - 3*std).toFixed(2));
		        stdto = parseFloat((avg + 3*std).toFixed(2));
        		
        	}
        	else{
        		
        	
        		var arrSeveritiesIDs = _.uniq(_.pluck(arrReq[2], 'SEVERITY'));
	        	
	        	for ( var j = 0; j < arrSeveritiesIDs.length; j++ ) {
		    		var objSeries = new Object();
		    		objSeries["name"] = arrSeverities[arrSeveritiesIDs[j]-2];
		    		
		    		var arrDays = new Array();
		    		for ( var k = 0; k < arrDaysCategs.length; k++ ) {
		    			var objDay = new Object();
		    			objDay["name"] = arrDaysX[k];
		    			objDay["drilldown"] = arrSeverities[arrSeveritiesIDs[j]-2] + ' - ' + arrDaysX[k];
		    			
		    			var arrDayAlarm = _.filter(arrReq[3], function(arr){
		    				return (arr.SEVERITY == arrSeveritiesIDs[j] && arr.DAY == arrDaysCategs[k]);
		    			});
		    			var sum = 0;
		    			for ( var l = 0; l < arrDayAlarm.length; l++ ) {
							sum += parseFloat(arrDayAlarm[l].AVERAGE);
						}
		    			if(arrDayAlarm.length > 0){
		    				objDay["y"] = parseFloat((sum/arrDayAlarm.length).toFixed(2));
		    			}
		    			else{
		    				objDay["y"] = 0;	
		    			}
		    			arrDays.push(objDay);
		    		}
		    		objSeries["data"] = arrDays;
		    		arrSeries.push(objSeries);
				}
				
	        	var arrDrillDown = new Array();
		    	
				for ( var j = 0; j < arrSeveritiesIDs.length; j++ ) {
					
		    		for ( var k = 0; k < arrDaysCategs.length; k++ ) {
		    			var objDrillDown = new Object();
		    			objDrillDown["name"] = arrDaysX[k];
		    			objDrillDown["id"] = arrSeverities[arrSeveritiesIDs[j]-2] + ' - ' + arrDaysX[k];
		    			objDrillDown["type"] = 'column';
		    			
		    			var arrCategLocations = _.filter(arrReq[4], function(arr){
		    				return (arr.SEVERITY == arrSeveritiesIDs[j] && arr.DAY == arrDaysCategs[k]);
		    			});
		    			var arrDrill = new Array();
		    			for ( var l = 0; l < arrCategLocations.length; l++ ) {
		    				var objDrill = new Object();
		        			var arrLocation = new Array();
		        			arrLocation.push(arrCategLocations[l].LOCATION);
		        			arrLocation.push(parseFloat((parseFloat(arrCategLocations[l].AVERAGE)).toFixed(2)));
		        			arrDrill.push(arrLocation);
						}
		        		objDrillDown["data"] = arrDrill;
		        		arrDrillDown.push(objDrillDown);
		    		}
				
				}
				
	    		var avg = 0;
	    		
	    		var std = 0;
		        var stdfrom = 0;
		        var stdto = 0;

        	}
        	
        	
        	var objFile = nlapiLoadFile(2906867);
	        html = objFile.getValue();
	        if (severity > 0){
	        	html = html.replace(new RegExp('{title}','g'), 'Average Response Time by Market - Severity ' + arrSeverities[severity-2]);
	        }
	        else{
	        	html = html.replace(new RegExp('{title}','g'), 'Average Response Time by Severity');
	        }
	        
	        html = html.replace(new RegExp('{subtitle}','g'), ' | From ' + start + ' to ' + end + ' |');
	        
	        html = html.replace(new RegExp('{exptitle}','g'), 'EXP: ' + arrExpected[severity-2]);
	        html = html.replace(new RegExp('{avgtitle}','g'), 'AVG: ' + avg.toFixed(2));
	        
	        html = html.replace(new RegExp('{stdtitle}','g'), ' ' + std.toFixed(2));
	        html = html.replace(new RegExp('{stdfromtitle}','g'), ' ' + stdfrom.toFixed(2));
	        html = html.replace(new RegExp('{stdtotitle}','g'), ' ' + stdto.toFixed(2));
	        
	        html = html.replace(new RegExp('{categories}','g'), JSON.stringify(arrDaysCategs));
	        html = html.replace(new RegExp('{series}','g'), JSON.stringify(arrSeries));
	        html = html.replace(new RegExp('{drilldown}','g'), JSON.stringify(arrDrillDown));
	        html = html.replace(new RegExp('{expected}','g'), arrExpected[severity-2]);
	        
	        
	        //html = html.replace(new RegExp('{average}','g'), arrReq[0][0].AVERAGE);
	        html = html.replace(new RegExp('{average}','g'), avg);
	        html = html.replace(new RegExp('{stdev}','g'), std);
	        html = html.replace(new RegExp('{stdfrom}','g'), stdfrom);
	        html = html.replace(new RegExp('{stdto}','g'), stdto);

    	}
    	else if (type == 'grid'){
    		
    		var requestURL = nlapiRequestURL('https://lucee-nnj3.dev.nac.net/odins/alarms/get_alarms_response_details.cfm?start=' + start + '&end=' + end);
			//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/ops_kpi/get_alarms_response_details.cfm?start=' + start + '&end=' + end);
			var jsonAlarms = requestURL.body;
			var arrAlarms = JSON.parse( jsonAlarms );

			var objFile = nlapiLoadFile(2851269);
	        html = objFile.getValue();
	        html = html.replace(new RegExp('{dataAlarms}','g'), JSON.stringify(arrAlarms));
	        html = html.replace(new RegExp('{title}','g'), '| Alarms ' + ' | From ' + start + ' to ' + end + ' |');
    	}
    	else{
    	}
    	response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

