nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_OPS_KPI_Menu.js
//	Script Name:	CLGX_SL_OPS_KPI_Menu
//	Script Id:		customscript_clgx_sl_ops_kpi_menu
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		07/20/2013
//	Includes:		CLGX_LIB_Global.js
//	URL:			/app/site/hosting/scriptlet.nl?script=502&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_ops_kpi_menu (request, response){
    try {

        var userid = nlapiGetUser();
        var roleid = nlapiGetContext().getRole();

        var objFile = nlapiLoadFile(2737514);
        var html = objFile.getValue();

        var objFile = nlapiLoadFile(2741787); // get instalations_menu.json
        var dataInstallations = objFile.getValue();

        var objFile = nlapiLoadFile(2743812); // get alarms_menu.json
        var dataAlarms = objFile.getValue();

        var objFile = nlapiLoadFile(2744013); // get cases_menu.json
        var dataCases = objFile.getValue();

        var objFile = nlapiLoadFile(2743913); // get maintenances_menu.json
        var dataMaintenances = objFile.getValue();

        html = html.replace(new RegExp('{dataInstallations}','g'), dataInstallations);
        html = html.replace(new RegExp('{dataAlarms}','g'), get_menu_alarms());
        html = html.replace(new RegExp('{dataCases}','g'), dataCases);
        html = html.replace(new RegExp('{dataMaintenances}','g'), dataMaintenances);
        html = html.replace(new RegExp('{defaultChart}','g'), '/app/site/hosting/scriptlet.nl?script=517&deploy=1&market=0&location=0&title=All%20Markets');
        if (roleid == '-5' || roleid == '3' || roleid == '18'||roleid == '1033' || roleid == '1004') {
            //html = html.replace(new RegExp('{defaultChart}','g'), '/app/site/hosting/scriptlet.nl?script=510&deploy=1&location=0&product=');
            html = html.replace(new RegExp('{menuItems}','g'), 'treeMaintenances,treeAlarms,treeInstallations,treeInstallFunnel,treeCases');
        }
        // else if (roleid == '1007' || roleid == '1017' ) {
        //html = html.replace(new RegExp('{defaultChart}','g'), '/app/site/hosting/scriptlet.nl?script=510&deploy=1&location=0&product=');
        //    html = html.replace(new RegExp('{menuItems}','g'), 'treeMaintenances,treeAlarms,treeInstallations');
        // }
        else if (roleid == '1033' || roleid == '1004' || roleid == '1017' || roleid == '1007' ) {
            //html = html.replace(new RegExp('{defaultChart}','g'), '/app/site/hosting/scriptlet.nl?script=510&deploy=1&location=0&product=');
            html = html.replace(new RegExp('{menuItems}','g'), 'treeMaintenances,treeAlarms,treeInstallations,treeCases');
        }
        else if (roleid == '1061' || roleid == '1062') {
            //html = html.replace(new RegExp('{defaultChart}','g'), '/app/site/hosting/scriptlet.nl?script=510&deploy=1&location=0&product=');
            html = html.replace(new RegExp('{menuItems}','g'), 'treeCases');
        }

        else{
            //html = html.replace(new RegExp('{defaultChart}','g'), '/app/site/hosting/scriptlet.nl?script=517&deploy=1&market=0&location=0&title=All%20Markets');
            html = html.replace(new RegExp('{menuItems}','g'), 'treeMaintenances,treeAlarms,treeInstallations,treeInstallFunnel,treeCases');
        }

        response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function get_menu_alarms (){

    var arrSever = ["LOW","MED","HIGH"];

    var objTree = new Object();
    objTree["text"] = '.';

    var arrMenus = new Array();

    var objMenu = new Object();
    objMenu["node"] = 'Averages/Month';
    objMenu["script"] = 519;
    //objMenu["iconCls"] = 'chart_bar';
    objMenu["expanded"] = false;
    objMenu["leaf"] = false;

    var arrMonths = new Array();
    for ( var i = 0; i < 12; i++ ) {
        var objMonth = new Object();
        objMonth["node"] = moment().startOf('month').subtract('months', i).format('MM/YY');
        objMonth["month"] = i;
        objMonth["script"] = 519;
        //objMonth["iconCls"] = 'chart_bar';
        objMonth["expanded"] = false;
        objMonth["leaf"] = false;
        var arrSeverities = new Array();
        for ( var j = 0; j < arrSever.length; j++ ) {
            var objSeverity = new Object();
            objSeverity["node"] = arrSever[j];
            objSeverity["month"] = i;
            objSeverity["script"] = 519;
            //objSeverity["iconCls"] = 'chart_bar';
            objSeverity["leaf"] = true;
            arrSeverities.push(objSeverity);
        }
        objMonth["children"] = arrSeverities;
        arrMonths.push(objMonth);
    }
    objMenu["children"] = arrMonths;
    arrMenus.push(objMenu);

    var objMenu = new Object();
    objMenu["node"] = 'Counts/Week';
    objMenu["script"] = 519;
    //objMenu["iconCls"] = 'chart_bar';
    objMenu["expanded"] = false;
    objMenu["leaf"] = false;

    var arrWeeks = new Array();
    for ( var i = 0; i < 12; i++ ) {
        var objWeek = new Object();
        objWeek["node"] = moment().startOf('week').subtract('weeks', i).format('MM/DD/YY');
        objWeek["week"] = i;
        objWeek["script"] = 520;
        //objMonth["iconCls"] = 'chart_bar';
        objWeek["leaf"] = true;
        arrWeeks.push(objWeek);
    }
    objMenu["children"] = arrWeeks;
    arrMenus.push(objMenu);

    var objMenu = new Object();
    objMenu["node"] = 'Daily Count';
    objMenu["script"] = 522;
    //objMenu["iconCls"] = 'chart_bar';
    objMenu["expanded"] = false;
    objMenu["leaf"] = false;

    var arrCategNames = ["Comm Error","Breaker >80%","High Humidity","Hi/Low Temp","System Alarm","Others"];

    var arrSubMenu = new Array();
    for ( var i = 0; i < arrCategNames.length; i++ ) {
        var objSubMenu = new Object();
        objSubMenu["node"] = arrCategNames[i];
        objSubMenu["week"] = i;
        objSubMenu["script"] = 522;
        objSubMenu["leaf"] = true;
        arrSubMenu.push(objSubMenu);
    }
    objMenu["children"] = arrSubMenu;
    arrMenus.push(objMenu);


    var objMenu = new Object();
    objMenu["node"] = 'Response Time';
    objMenu["severity"] = 0;
    objMenu["script"] = 524;
    //objMenu["iconCls"] = 'chart_bar';
    objMenu["expanded"] = false;
    objMenu["leaf"] = false;

    var arrSeverities = ["Low","Medium","High"];

    var arrSubMenu = new Array();
    for ( var i = 0; i < arrSeverities.length; i++ ) {
        var objSubMenu = new Object();
        objSubMenu["node"] = arrSeverities[i];
        objSubMenu["severity"] = i + 2;
        objSubMenu["script"] = 524;
        objSubMenu["leaf"] = true;
        arrSubMenu.push(objSubMenu);
    }
    objMenu["children"] = arrSubMenu;
    arrMenus.push(objMenu);

    objTree["children"] = arrMenus;

    return JSON.stringify(objTree);
}