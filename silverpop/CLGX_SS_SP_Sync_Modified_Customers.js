//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SP_Sync_Modified_Customers.js
//	Script Name:	CLGX_SS_SP_Sync_Modified_Customers
//	Script Id:		customscript_clgx_ss_sp_sync_customers
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		9/24/2015
//-------------------------------------------------------------------------------------------------
function scheduled_sp_sync_customers(){
    try{
    	//nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        var context = nlapiGetContext();
        var nbrCustomers = 0;
        var nbrContacts = 0;
        
        // create array of contact ids already in queue
        var arrQueue = new Array();
    	var searchQueue = nlapiLoadSearch('customrecord_clgx_queue_contacts_to_sp', 'customsearch_clgx_sp_merges_to_delete');
		var resultSet = searchQueue.runSearch();
		resultSet.forEachResult(function(searchResult) {
			arrQueue.push(parseInt(searchResult.getValue('custrecord_clgx_contact_to_sp_contact',null,null)));
        	return true;
		});
		
		// create array of customers ids already modified yesterday
		var arrCustomers = new Array();
    	var searchCustomers = nlapiLoadSearch('customer', 'customsearch_clgx_sp_customers_to_update');
		var resultSet = searchCustomers.runSearch();
		resultSet.forEachResult(function(searchResult) {
			arrCustomers.push(parseInt(searchResult.getValue('internalid',null,null)));
			nbrCustomers += 1;
        	return true;
		});
		
		// create array of contacts
		if(arrCustomers.length > 0){
	        var arrContacts = new Array();
	        var arrFilters = new Array();
	    	arrFilters.push(new nlobjSearchFilter("internalid","company","anyof", arrCustomers)); // all contacts of customers modified yesterday
	    	if(arrQueue.length > 0){
	    		arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof", arrQueue)); // only contacts not in queue
	    	}
	    	var searchContacts = nlapiLoadSearch('contact', 'customsearch_clgx_sp_sync_contacts_ids');
	    	searchContacts.addFilters(arrFilters);
			var resultSet = searchContacts.runSearch();
			resultSet.forEachResult(function(searchResult) {
				var record = nlapiCreateRecord('customrecord_clgx_queue_contacts_to_sp');
				record.setFieldValue('custrecord_clgx_contact_to_sp_contact', parseInt(searchResult.getValue('internalid',null,null)));
				record.setFieldValue('custrecord_clgx_contact_to_sp_action', 1);
				record.setFieldValue('custrecord_clgx_contact_to_sp_done', 'F');
				var idRec = nlapiSubmitRecord(record, false,true);
				nbrContacts += 1;
	        	return true;
			});
		}
		
		var usage = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
        nlapiLogExecution('DEBUG','Usage', 'Usage = '+ usage);
    	nlapiSendEmail(432742,71418,'SP 1.2 - Finish adding ' + nbrContacts + ' contacts from ' + nbrCustomers + ' customers to queue. Usage (' + usage + ')','',null,null,null,null,true);
    	nlapiSendEmail(432742,1349020,'SP 1.2 - Finish adding ' + nbrContacts + ' contacts from ' + nbrCustomers + ' customers to queue. Usage (' + usage + ')','',null,null,null,null,true);
    	
        //var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_others', 'customdeploy_clgx_ss_sp_sync_others');
    	
        //nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
    	var str = String(error);
    	if (str.match('SSS_USAGE_LIMIT_EXCEEDED')) {
    		var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
        	nlapiLogExecution('DEBUG','End', 'Finished processing because of limit exceeded error and reschedule it.');
        }
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}