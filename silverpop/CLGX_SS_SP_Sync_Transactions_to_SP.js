//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SP_Sync_Transactions_to_SP.js
//	Script Name:	CLGX_SS_SP_Sync_Transactions_to_SP
//	Script Id:		customscript_clgx_ss_sp_sync_transact2sp
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		5/1/2015
//-------------------------------------------------------------------------------------------------
function scheduled_sp_sync_transact2sp(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();
        var environment = context.getEnvironment()
    	
    	if(environment == 'PRODUCTION'){
	        var initialTime = moment();
	        
	        var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_transact_to_sp_transact',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_transact_to_sp_type',null,null));
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_transact_to_sp_action',null,null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_transact_to_sp_done",null,"is",'F'));
			var searchQueue = nlapiSearchRecord('customrecord_clgx_queue_transact_to_sp', null, arrFilters, arrColumns);
		
			if(searchQueue != null){
			
		        var date = new Date();
				var startScript = moment();
				var emailAdminSubject = 'SP 1.5 - Transactions to SP - ' + startScript.format('M/D/YYYY');
				var emailAdminBody = '';
				emailAdminBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');
	
				if(searchQueue.length > 100){
					var loopndx = 100;
				}
				else{
					var loopndx = searchQueue.length;
				}

// ============================ Open Session ===============================================================================================
        
		    	var strPost = clgx_sp_login (username,password);
		    	var objHead = clgx_sp_header (strPost.length);
				var requestURL = nlapiRequestURL(strLogURL,strPost,objHead);
				var responseXML = nlapiStringToXML(requestURL.getBody());
				var login = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
				
				if(login == 'true'){
					
					emailAdminBody += '<h2> ' + loopndx + ' from ' + searchQueue.length + ' transactions processed.</h2>';
				    emailAdminBody += '<table border="1" cellpadding="5">';
				    emailAdminBody += '<tr><td>TransactionID</td><td>Type</td><td>Action</td></tr>';
				    
					var jsessionid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SESSIONID');
					strURL += jsessionid;
					
					nlapiLogExecution('DEBUG','Session ID', ' | Session ID = ' + jsessionid + '  |');
		
					for ( var j = 0; searchQueue != null && j < loopndx; j++ ) {
	
			            var queueid = searchQueue[j].getValue('internalid',null,null);
			            var transactionid = searchQueue[j].getValue('custrecord_clgx_transact_to_sp_transact',null,null);
			            var typeid = parseInt(searchQueue[j].getValue('custrecord_clgx_transact_to_sp_type',null,null));
			            var action = parseInt(searchQueue[j].getValue('custrecord_clgx_transact_to_sp_action',null,null));
			            var json = searchQueue[j].getValue('custrecord_clgx_transact_to_sp_json',null,null);
			            
			            nlapiLogExecution('DEBUG','Transaction ID', ' | Transaction ID = ' + transactionid + ' | Type = ' + typeid + ' | Action = ' + action + ' | json = ' + json + '  |');
			            
			            if(action == 2){
			            	
			            	var transactcreate = 'NULL';
				            var jsonSync = [];
			    			var strSync = json;
			    			if(strSync != '' && strSync != null){
			    				nlapiLogExecution('DEBUG','Session ID', ' | strSync = ' + strSync + '  |');
			    				jsonSync = JSON.parse(strSync);
			    				
			    				// delete transaction lines ================================================================================================
			    				var objParams = new Object();
			    				objParams["tableid"] = 3521196;
			    				var strPost = clgx_sp_table_delete_data (objParams,jsonSync);
			    				
			    				nlapiLogExecution('DEBUG','strPost', ' | strPost = ' + strPost + '  |');
			    				
			    		    	var objHead = clgx_sp_header (strPost.length);
			    				var requestURL = nlapiRequestURL(strURL,strPost,objHead);
			    				var strBody = requestURL.getBody();
			    				var responseXML = nlapiStringToXML(strBody);
			    				
			    				var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
			    				if(success == 'true'){
			    					nlapiLogExecution('DEBUG','DELETE ', ' | Delete transaction ID = ' + transactionid + '  |');
			    					nlapiLogExecution('DEBUG','DELETE ', ' | responseXML = ' + responseXML + '  |');
			    					var transactdelete = 'YES';
			    				}
			    				else{
			    					nlapiLogExecution('DEBUG','DELETE ', ' | Did not delete transaction ID = ' + transactionid + '  |');
			    					nlapiLogExecution('DEBUG','DELETE ', ' | responseXML = ' + responseXML + '  |');
			    					var transactdelete = 'NO';
			    				}
			    			}
			    			else{
			    				var transactdelete = 'NULL';
			    			}
		    			
			            }
			            else{
				            
				            if(typeid == 1){
				            	var type = 'Opportunity';
				            }
				            else if(typeid == 2){
				            	var type = 'Proposal';
				            }
				            else{
				            	var type = 'Service Order';
				            }
				        	
				            if(type == 'Opportunity'){
				        		var recTransact = nlapiLoadRecord ('opportunity',transactionid);
				        	}
				        	else if(type == 'Proposal'){
				        		var recTransact = nlapiLoadRecord ('estimate',transactionid);
				        	}
				        	else{ //Service Order
				        		var recTransact = nlapiLoadRecord ('salesorder',transactionid);
				        	}
			    			
				        	var jsonSync = [];
			    			var strSync = recTransact.getFieldValue('custbody_clgx_sp_json_sync');
			    			if(strSync != '' && strSync != null){
			    				jsonSync = JSON.parse(strSync);
			    				
			    				// delete transaction lines ================================================================================================
			    				var objParams = new Object();
			    				objParams["tableid"] = 3521196;
			    				var strPost = clgx_sp_table_delete_data (objParams,jsonSync);
			    		    	var objHead = clgx_sp_header (strPost.length);
			    				var requestURL = nlapiRequestURL(strURL,strPost,objHead);
			    				var strBody = requestURL.getBody();
			    				var responseXML = nlapiStringToXML(strBody);
			    				var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
			    				if(success == 'true'){
			    					//nlapiLogExecution('DEBUG','DELETE ', ' | Delete transaction ID = ' + transactionid + '  |');
			    					//nlapiLogExecution('DEBUG','DELETE ', ' | responseXML = ' + responseXML + '  |');
			    					var transactdelete = 'YES';
			    				}
			    				else{
			    					//nlapiLogExecution('DEBUG','DELETE ', ' | Did not delete transaction ID = ' + transactionid + '  |');
			    					//nlapiLogExecution('DEBUG','DELETE ', ' | responseXML = ' + responseXML + '  |');
			    					var transactdelete = 'NO';
			    				}
			    			}
			    			else{
			    				var transactdelete = 'NULL';
			    			}
			    			
			    			var arrRows = new Array();
			    			var arrRowsJSON = new Array();
			    			
			    			var arrColumns = new Array();
							var arrFilters = new Array();
							arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",transactionid));
							if(type == 'Opportunity'){
								var searchTransactions = nlapiSearchRecord('opportunity', 'customsearch_clgx_sp_opportunities', arrFilters, arrColumns);
				        	}
				        	else if(type == 'Proposal'){
				        		var searchTransactions = nlapiSearchRecord('transaction', 'customsearch_clgx_sp_proposals', arrFilters, arrColumns);
				        	}
				        	else{
				        		var searchTransactions = nlapiSearchRecord('transaction', 'customsearch_clgx_sp_sos', arrFilters, arrColumns);
				        	}
							
							for ( var i = 0; searchTransactions != null && i < searchTransactions.length; i++ ) {
								var searchTransaction = searchTransactions[i];
								var columns = searchTransaction.getAllColumns();
					        	
								var arrColumns = new Array();
					        	var arrColumnsJSON = new Array();
					        	
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Company Internal ID';
					        	objColumn["value"] = parseInt(searchTransaction.getValue('internalid','customer','GROUP'));
					        	arrColumns.push(objColumn);
					        	
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Transaction Internal ID';
					        	objColumn["value"] = parseInt(searchTransaction.getValue('internalid',null,'GROUP'));
					        	arrColumns.push(objColumn);
					        	arrColumnsJSON.push(objColumn);
					        	
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Transaction Number';
					        	objColumn["value"] = searchTransaction.getValue('tranid',null,'GROUP');
					        	arrColumns.push(objColumn);
					        	
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Transaction End Date';
					        	if(type == 'Opportunity'){
					        		objColumn["value"] = ''
					        	}
					        	else{
					        		objColumn["value"] = searchTransaction.getValue('custbody_clgx_renewal_end_date',null,'GROUP');
					        	}
					        	arrColumns.push(objColumn);
					        	
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Transaction Install Date';
					        	if(type == 'Opportunity'){
					        		objColumn["value"] = ''
					        	}
					        	else{
					        		objColumn["value"] = searchTransaction.getValue('custbody_cologix_service_actl_instl_dt',null,'GROUP');
					        	}
					        	arrColumns.push(objColumn);
					        	
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Transaction Sales Effective Date';
					        	if(type == 'Opportunity'){
					        		objColumn["value"] = ''
					        	}
					        	else{
					        		objColumn["value"] = searchTransaction.getValue('saleseffectivedate',null,'GROUP');
					        	}
					        	arrColumns.push(objColumn);
		
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Transaction Status';
					        	if(type == 'Opportunity'){
					        		objColumn["value"] = searchTransaction.getText('entitystatus',null,'GROUP');
					        	}
					        	else{
					        		objColumn["value"] = searchTransaction.getText('status',null,'GROUP');
					        	}
					        	arrColumns.push(objColumn);
					        	
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Transaction Type';
					        	objColumn["value"] = type;
					        	arrColumns.push(objColumn);
					        	
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Transaction Lead Source';
					        	var leadsource = searchTransaction.getText('leadsource',null,'GROUP');
					        	if(leadsource != null && leadsource != '' && leadsource != '- None -'){
					        		objColumn["value"] = leadsource;
					        	}
					        	else{
					        		objColumn["value"] = '';
					        	}
					        	arrColumns.push(objColumn);
					        	
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Transaction Forecast Type';
					        	var forecasttype = searchTransaction.getText('forecasttype',null,'GROUP');
					        	if(forecasttype != null && forecasttype != '' && forecasttype != '- None -'){
					        		objColumn["value"] = forecasttype;
					        	}
					        	else{
					        		objColumn["value"] = '';
					        	}
					        	arrColumns.push(objColumn);
					        	
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Transaction Sale Type';
					        	var saletype = searchTransaction.getText('custbody_cologix_opp_sale_type',null,'GROUP');
					        	if(saletype != null && saletype != '' && saletype != '- None -'){
					        		objColumn["value"] = saletype;
					        	}
					        	else{
					        		objColumn["value"] = '';
					        	}
					        	arrColumns.push(objColumn);
					        	
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Location Internal ID';
					        	if(type == 'Opportunity'){
					        		objColumn["value"] = parseInt(searchTransaction.getValue(columns[7]));
					        	}
					        	else{
					        		objColumn["value"] = parseInt(searchTransaction.getValue('internalid','location','GROUP'));
					        	}
					        	arrColumns.push(objColumn);
					        	arrColumnsJSON.push(objColumn);
					        	
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Location Name';
					        	if(type == 'Opportunity'){
					        		objColumn["value"] = replace_chars (searchTransaction.getValue('locationnohierarchy',null,'GROUP'));
					        	}
					        	else{
					        		objColumn["value"] = replace_chars (searchTransaction.getValue('namenohierarchy','location','GROUP'));
					        	}
					        	arrColumns.push(objColumn);
					        	
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Item Internal ID';
					        	objColumn["value"] = parseInt(searchTransaction.getValue('internalid','item','GROUP'));
					        	arrColumns.push(objColumn);
					        	arrColumnsJSON.push(objColumn);
					        	
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Item Name';
					        	objColumn["value"] = replace_chars (searchTransaction.getValue('name','item','GROUP'));
					        	arrColumns.push(objColumn);
					        	
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Item Category';
					        	objColumn["value"] = searchTransaction.getText('custitem_cologix_item_category','item','GROUP');
					        	arrColumns.push(objColumn);
		
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Item Quantity';
					        	if(type == 'Opportunity'){
					        		objColumn["value"] = parseFloat(searchTransaction.getValue('linequantity',null,'SUM'));
					        	}
					        	else if(type == 'Proposal'){
					        		objColumn["value"] = parseFloat(searchTransaction.getValue('quantity',null,'SUM'));
					        	}
					        	else{
					        		objColumn["value"] = parseFloat(searchTransaction.getValue('custcol_clgx_qty2print',null,'SUM'));
					        	}
					        	arrColumns.push(objColumn);
		
					        	var objColumn = new Object();
					        	objColumn["name"] = 'Item Total';
					        	//objColumn["value"] = parseFloat(searchTransaction.getValue('lineamount',null,'SUM'));
					        	objColumn["value"] = parseFloat(searchTransaction.getValue(columns[16]));
					        	
					        	if(type == 'Opportunity'){
					        		objColumn["value"] = parseFloat(searchTransaction.getValue('lineamount',null,'SUM'));
					        	}
					        	else if(type == 'Proposal'){
					        		objColumn["value"] = parseFloat(searchTransaction.getValue('amount',null,'SUM'));
					        	}
					        	else{
					        		objColumn["value"] = parseFloat(searchTransaction.getValue('custcol_clgx_qty2print',null,'SUM')) * parseFloat(searchTransaction.getValue('rate',null,'GROUP'));
					        	}
					        	
					        	arrColumns.push(objColumn);
					        	
					        	arrRows.push(arrColumns);
					        	arrRowsJSON.push(arrColumnsJSON);
							}
							
							// create transaction lines ================================================================================================
							var objParams = new Object();
							objParams["tableid"] = 3521196;
							var strPost = clgx_sp_table_insert_data (objParams,arrRows);
					    	var objHead = clgx_sp_header (strPost.length);
							var requestURL = nlapiRequestURL(strURL,strPost,objHead);
							var strBody = requestURL.getBody();
							var responseXML = nlapiStringToXML(strBody);
							var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
							if(success == 'true'){
								var updated = 'T';
								var transactcreate = 'YES';
								//nlapiLogExecution('DEBUG','CREATE ', ' | Create transaction ID = ' + transactionid + '  |');
								//nlapiLogExecution('DEBUG','CREATE ', ' | responseXML = ' + responseXML + '  |');
							}
							else{
								var updated = 'F';
								var transactcreate = 'NO';
								//nlapiLogExecution('DEBUG','CREATE ', ' | Did not create transaction ID = ' + transactionid + '  |');
								//nlapiLogExecution('DEBUG','CREATE ', ' | responseXML = ' + responseXML + '  |');
							}
							
				        	if(type == 'Opportunity'){
				        		//nlapiSubmitField('opportunity', transactionid, ['custbody_clgx_sp_json_sync','custbody_clgx_sp_sync'], [JSON.stringify(arrRowsJSON),updated]);
	    						var recOpportunity = nlapiLoadRecord('opportunity', transactionid);
	    						recOpportunity.setFieldValue('custbody_clgx_sp_json_sync', JSON.stringify(arrRowsJSON));
	    						recOpportunity.setFieldValue('custbody_clgx_sp_sync', updated);
	    						nlapiSubmitRecord(recOpportunity, false, true);
				        	}
				        	else if(type == 'Proposal'){
				        		nlapiSubmitField('estimate', transactionid, ['custbody_clgx_sp_json_sync','custbody_clgx_sp_sync'], [JSON.stringify(arrRowsJSON),updated]);
				        	}
				        	else{ //Service Order
				        		nlapiSubmitField('salesorder', transactionid, ['custbody_clgx_sp_json_sync','custbody_clgx_sp_sync'], [JSON.stringify(arrRowsJSON),updated]);
				        	}
			        	
			        	}

			            nlapiDeleteRecord('customrecord_clgx_queue_transact_to_sp', queueid);
			        	emailAdminBody += '<tr><td>' + transactionid + '</td><td>' + type + '</td><td>' + action + '</td></tr>';
			        	
			        	var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
			            var index = j + 1;
			            nlapiLogExecution('DEBUG','INDEX LOOP ', ' | Index = ' + index + ' of ' + loopndx + ' | Transaction ID = '+ transactionid + ' | Delete = '+ transactdelete + ' | Create = '+ transactcreate + ' | Usage = '+ usageConsumtion + '  |');
					}
				
// ============================ Close Session ===============================================================================================
				
			    	var strPost =  clgx_sp_logout ();
					var objHead = clgx_sp_header (strPost.length);
					var requestURL = nlapiRequestURL(strURL,strPost,objHead);
					var strBody = requestURL.getBody();
					var responseXML = nlapiStringToXML(strBody);
					var logout = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
		
					emailAdminBody += '</table>';
				}
				else{
					emailAdminBody += '<h2>0 transactions processed</h2>';
				}
			 
// ============================ Send admin email ===============================================================================================
					
			    var endScript = moment();
				emailAdminBody += '<br><br>';
				emailAdminBody += 'End : ' + endScript.format('M/D/YYYY h:mm:ss a') + '<br>';
				emailAdminBody += 'Total execution minutes : ' + (endScript.diff(startScript)/60000).toFixed(1) + '<br>';
				var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
				emailAdminBody += 'Total usage : ' + usageConsumtion;
				emailAdminBody += '<br><br>';
				emailAdminSubject += ' | ' + loopndx + ' from ' + searchQueue.length + ' transactions in ' + (endScript.diff(startScript)/60000).toFixed(1) + ' mins | ';
	
				var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
	
			}
			else{
				var emailAdminSubject = 'SP 1.6 - Transactions to SP - finish processing transactions';
				var emailAdminBody = '';
				nlapiSendEmail(432742,71418,emailAdminSubject,emailAdminBody,null,null,null,null,true);
				nlapiSendEmail(432742,179749,emailAdminSubject,emailAdminBody,null,null,null,null,true);
				nlapiSendEmail(432742,1349020,emailAdminSubject,emailAdminBody,null,null,null,null,true);
				
			}
	
			//nlapiSendEmail(432742,71418,emailAdminSubject,emailAdminBody,null,null,null,null);
		}
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 

    }
    catch (error){
    	
    	var str = String(error);
        if (str.match('SSS_REQUEST_TIME_EXCEEDED') || str.match('SSS_CONNECTION_TIME_OUT') || str.match('SSS_CONNECTION_CLOSED')) {
        	nlapiSendEmail(432742,71418, str + ' - Finished processing SP transactions because of communication error and reschedule it','',null,null,null,null,true);
        	nlapiSendEmail(432742,179749, str + ' - Finished processing SP transactions because of communication error and reschedule it','',null,null,null,null,true);
        	nlapiSendEmail(432742,1349020, str + ' - Finished processing SP transactions because of communication error and reschedule it','',null,null,null,null,true);
        	
        	var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
        	nlapiLogExecution('DEBUG','End', 'Finished transactions processing because of communication error and reschedule it.');
        }
        
    	if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function replace_chars (str){
	str = str.replace(/\,/g," ");
	str = str.replace(/\&/g," and ");
	str = str.replace(/\"/g,"");
	str = str.replace(/\'/g,"");
	str = str.replace(/\;/g,"");
	str = str.replace(/\:/g,"");
	str = str.replace(/\s{2,}/g, ' ');
	str = str.replace(/\t/g, ' ');
	str = str.toString().trim().replace(/(\r\n|\n|\r)/g,"");
  return str;
}