//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SP_Create_CSVs.js
//	Script Name:	CLGX_SS_SP_Create_CSVs
//	Script Id:		customscript_clgx_ss_sp_create_csvs
//	Script Type:	Scheduled Script
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		24/03/2015
//-------------------------------------------------------------------------------------------------
function scheduled_sp_create_csvs(){
    try{
        
    	/*
    	var sessionid = clgx_sp_contact_login (username,password);
		if(sessionid != ''){
			strURL += sessionid;
			var path = 'export_new_contacts - All - Apr 6 2015 04-44-49 PM';
			//var path = clgx_sp_export_new_contacts (strURL,objRecipient);
			var url = 'https://command1.cologix.com:10313/netsuite/silverpop/export_new_file_name.cfm?path=' + path;
			var requestURL = nlapiRequestURL(url);
 			var response = requestURL.body;
			var logout = clgx_sp_contact_logout (strURL);
		}
    	*/
    	
    	
        var context = nlapiGetContext();
        var letterIndx = context.getSetting('SCRIPT','custscript_clgx_sp_letter_processed');
        if(letterIndx == null){
        	letterIndx = 0;
        }
        
        nlapiSendEmail(71418,71418,'letterIndx',letterIndx,null,null,null,null,true);
        
        var csvContacts = '';
        if(letterIndx == 0){
        	csvContacts += 'position,letter\n';
        }
    	
    	
        
        
        
        
        
        
        
        
        
    	
    	//var startScript = moment();
		//nlapiLogExecution('DEBUG','Started Execution', '|--------------- Start : ' + startScript.format('M/D/YYYY h:mm:ss a') + '-----------------|'); 
        
		//var lstChars = '0,1,2,3,4,5,6,7,8,9,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z';
    	//var arrChars = lstChars.split(',');
    	
    	
    	
    	/*
// ====================================== export contacts ====================================================================================================================
    	
    	var csvContacts = '';
    	csvContacts += 'customerid,customer,contactid,firstname,lastname,name,role,email,phone,subsidiary,subscription,created, firstorder,firstsale, status, category,repname,repemail,rephone,repsupervizor\n';
    	for ( var i = 0; arrChars != null && i < arrChars.length; i++ ) {
    		
    		var arrFilters = new Array();
    		arrFilters.push(new nlobjSearchFilter("email",null,"startswith", arrChars[i]));
    		var searchContacts = nlapiLoadSearch('contact', 'customsearch_clgx_sp_contacts2csv');
    		searchContacts.addFilters(arrFilters);
    		var resultSetContacts = searchContacts.runSearch();
    		resultSetContacts.forEachResult(function(searchResult) {
    			
    			var entityid = searchResult.getValue('entityid', null, null);
    			if(entityid != null && entityid != ''){
    				entityid = replace_chars(entityid);
    			}
    			else{
    				entityid = '';
    			}
    			var firstname = searchResult.getValue('firstname', null, null);
    			if(firstname != null && firstname != ''){
    				firstname = replace_chars(firstname);
    			}
    			else{
    				firstname = '';
    			}
    			var lastname = searchResult.getValue('lastname', null, null);
    			if(lastname != null && lastname != ''){
    				lastname = replace_chars(lastname);
    			}
    			else{
    				lastname = '';
    			}
    			var company = searchResult.getValue('companyname', 'customer', null);
    			//var company = searchResult.getText('company', null, null);
    			if(company != null && company != ''){
    				company = replace_chars(company);
    			}
    			else{
    				company = '';
    			}
    			var email = searchResult.getValue('email', null, null);
    			if(email != null && email != ''){
    				email = replace_chars(email);
    			}
    			else{
    				email = '';
    			}
    			var phone = searchResult.getValue('phone', null, null);
    			if(phone != null && phone != ''){
    				phone = replace_chars(phone);
    			}
    			else{
    				phone = '';
    			}
    			
    			csvContacts += searchResult.getValue('internalid', 'customer', null) + ',';
    			csvContacts +=  company + ',';
    			csvContacts += searchResult.getValue('internalid', null, null) + ',';
        		csvContacts +=  firstname + ',';
        		csvContacts +=  lastname + ',';
        		csvContacts +=  entityid + ',';
        		csvContacts +=  searchResult.getText('contactrole', null, null) + ',';
    			csvContacts +=  email + ',';
    			csvContacts +=  phone + ',';
    			csvContacts += searchResult.getText('subsidiarynohierarchy', null, null) + ',';
    			csvContacts +=  searchResult.getText('globalsubscriptionstatus', null, null) + ',';
    			
    			var created = searchResult.getValue('datecreated', null, null);
    			csvContacts += moment(created).format('M/D/YYYY') + ',';
        		csvContacts +=  searchResult.getValue('firstorderdate', 'customer', null) + ',';
        		csvContacts +=  searchResult.getValue('firstsaledate', 'customer', null) + ',';
        		csvContacts +=  searchResult.getText('status', 'customer', null) + ',';
        		csvContacts +=  searchResult.getText('category', 'customer', null) + ',';
        		
        		var repid = searchResult.getValue('salesrep', 'customer', null);
        		if(repid != null && repid != ''){
            		var recRep = nlapiLoadRecord ('employee',searchResult.getValue('salesrep', 'customer', null));
            		csvContacts +=  searchResult.getValue('salesrep', 'customer', null) + ',';
            		csvContacts +=  recRep.getFieldValue('email') + ',';
            		csvContacts +=  recRep.getFieldValue('phone') + ',';
            		csvContacts +=  recRep.getFieldText('supervisor') + '\n';
        		}
        		else{
            		csvContacts +=  ',';
            		csvContacts +=  ',';
            		csvContacts +=  ',';
            		csvContacts +=  '\n';
        		}

        		
        		return true;
    		});
    	}
    	var csvFileContacts = nlapiCreateFile('contacts_ns.csv', 'PLAINTEXT', csvContacts);
    	csvFileContacts.setFolder(1909409);
		nlapiSubmitFile(csvFileContacts);
		
// ====================================== export customers ====================================================================================================================
		/*
    	var csvCustomers = '';
    	csvCustomers += 'created,firstorder,firstsale,internalid,name,salesrepid,salesrep,salesrepemail,salesrepphone,supervisorid,supervisor,segment,status,subsidiaryid,subsidiary\n';
    	for ( var i = 0; arrChars != null && i < arrChars.length; i++ ) {
    		
    		var arrFilters = new Array();
    		arrFilters.push(new nlobjSearchFilter("entityid",null,"startswith", arrChars[i]));
    		var searchCustomers = nlapiLoadSearch('customer', 'customsearch_clgx_sp_customers2csv');
    		searchCustomers.addFilters(arrFilters);
    		var resultSetCustomers = searchCustomers.runSearch();
    		resultSetCustomers.forEachResult(function(searchResult) {
    		
        		var entityid = searchResult.getValue('entityid', null, null);
    			if(entityid != null && entityid != ''){
    				entityid = replace_chars(entityid);
    			}
    			else{
    				entityid = '';
    			}
    			
    			csvCustomers +=  searchResult.getValue('datecreated', null, null) + ',';
    			csvCustomers +=  searchResult.getValue('firstorderdate', null, null) + ',';
    			csvCustomers +=  searchResult.getValue('firstsaledate', null, null) + ',';
    			csvCustomers += searchResult.getValue('internalid', null, null) + ',';
    			csvCustomers +=  entityid + ',';
    			csvCustomers += searchResult.getValue('salesrep', null, null) + ',';
    			csvCustomers += searchResult.getText('salesrep', null, null) + ',';
        		csvCustomers += searchResult.getValue('email', 'salesrep', null) + ',';
        		csvCustomers += searchResult.getValue('phone', 'salesrep', null) + ',';
        		csvCustomers += searchResult.getValue('supervisor', 'salesrep', null) + ',';
        		csvCustomers += searchResult.getText('supervisor', 'salesrep', null) + ',';
        		csvCustomers += searchResult.getValue('category', null, null) + ',';
        		csvCustomers += searchResult.getValue('entitystatus', null, null) + ',';
        		csvCustomers += searchResult.getValue('subsidiary', null, null) + ',';
        		csvCustomers += searchResult.getText('subsidiarynohierarchy', null, null) + '\n';
        		
        		return true;
        		
    		});
    	}
    	var csvFileCustomers = nlapiCreateFile('customers_ns.csv', 'PLAINTEXT', csvCustomers);
    	csvFileCustomers.setFolder(1909409);
		nlapiSubmitFile(csvFileCustomers);
		
		
// ====================================== export transactions ====================================================================================================================

		
		
		nlapiSendEmail(71418,71418,'Export to SilverPOP','Export CSV file were created.',null,null,null,null);
            
        var endScript = moment();
		var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
		//nlapiLogExecution('DEBUG','Finish Execution', '|--------------- Finish : ' + endScript.format('M/D/YYYY h:mm:ss a') + '-----------------|'); 
        //nlapiLogExecution('DEBUG','Finish Execution', '|--------------- Total execution minutes : ' + (endScript.diff(startScript)/60000).toFixed(1) + '-----------------|'); 
        nlapiLogExecution('DEBUG','Finish Execution', '|--------------- Total usage : ' + usageConsumtion + '-----------------|'); 
        */
    	
    	
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function replace_chars (str){
	str = str.replace(/\,/g," ");
	str = str.replace(/\"/g,"");
	str = str.replace(/\'/g,"");
	str = str.replace(/\;/g,"");
	str = str.replace(/\:/g,"");
	str = str.replace(/\s{2,}/g, ' ');
	str = str.replace(/\t/g, ' ');
	str = str.toString().trim().replace(/(\r\n|\n|\r)/g,"");
  return str;
}


function clgx_sp_export_new_contacts (strURL,objContact) {
	
	// ====== Build Parameters list =================================
	var objParams = new Array();
	objParams["listid"] =  3413712; // export new contacts query
	objParams["email"] = '';
	objParams["exptype"] = 'ALL';
	objParams["expformat"] = 'CSV';
	objParams["encode"] = '';
	objParams["add"] = false;
	objParams["start"] = '';
	objParams["end"] = '';
	objParams["created"] = false;
	objParams["leadsource"] = false;
	objParams["dateformat"] = false;
	// ====== Build Columns Fields Array =================================
	var arrColumns = new Array();
		arrColumns.push('NS Internal ID');
		arrColumns.push('RECIPIENT_ID');
		arrColumns.push('Company Internal ID');
		arrColumns.push('Company Name');
		arrColumns.push('First name');
		arrColumns.push('Last Name');
		arrColumns.push('Name');
		arrColumns.push('Email');
		arrColumns.push('Phone');
		
	var strPost = clgx_sp_export_list (objParams,arrColumns);
	var objHead = clgx_sp_header (strPost.length);
	var requestURL = nlapiRequestURL(strURL,strPost,objHead);
	var strBody = requestURL.getBody();
	var responseXML = nlapiStringToXML(strBody);
	
	var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
	if(success == 'TRUE'){
		var jobid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/JOB_ID');
		var path = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/FILE_PATH');
		
		var fileName = nlapiCreateFile('sp_new_contacts_ftp_last_file_name.txt', 'PLAINTEXT', path);
		fileName.setFolder(1909409);
		nlapiSubmitFile(fileName);
		
		return path;
	}
	else{
		return '';
	}
}


function clgx_sp_contact_login (username,password) {
	
	var strPost = clgx_sp_login (username,password);
	var objHead = clgx_sp_header (strPost.length);
	var requestURL = nlapiRequestURL(strLogURL,strPost,objHead);
	var responseXML = nlapiStringToXML(requestURL.getBody());
	var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
	
	if(success){
		return nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SESSIONID');
	}
	else{
		return '';
	}
}


function clgx_sp_contact_logout (strURL) {
	
	var strPost =  clgx_sp_logout ();
	var objHead = clgx_sp_header (strPost.length);
	var requestURL = nlapiRequestURL(strURL,strPost,objHead);
	var strBody = requestURL.getBody();
	var responseXML = nlapiStringToXML(strBody);
	return nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
}

