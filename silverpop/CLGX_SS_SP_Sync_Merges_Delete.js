//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SP_Sync_Merges_Delete.js
//	Script Name:	CLGX_SS_SP_Sync_Merges_Delete
//	Script Id:		customscript_clgx_ss_sp_sync_merges
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		9/24/2015
//-------------------------------------------------------------------------------------------------
function scheduled_sp_sync_merges(){
    try{
    	//nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        var context = nlapiGetContext();
        var environment = context.getEnvironment();
    	if(environment == 'PRODUCTION'){
					
    		var nbrContacts = 0;
    		
            // create array of contact ids already in queue
            var arrQueue = new Array();
            var arrQueueContactsIDs = new Array();
        	var searchQueue = nlapiLoadSearch('customrecord_clgx_queue_contacts_to_sp', 'customsearch_clgx_sp_merges_to_delete');
    		var resultSet = searchQueue.runSearch();
    		resultSet.forEachResult(function(searchResult) {
        		var objQueue = new Object();
        		objQueue["id"] = parseInt(searchResult.getValue('internalid',null,null));
        		objQueue["contactid"] = parseInt(searchResult.getValue('custrecord_clgx_contact_to_sp_contact',null,null));
    			arrQueue.push(objQueue);
    			arrQueueContactsIDs.push(parseInt(searchResult.getValue('custrecord_clgx_contact_to_sp_contact',null,null)));
            	return true;
    		});
    		
    		if(arrQueue.length > 0){
                // create array of contacts
                var arrContactsIDs = new Array();
                var arrFilters = new Array();
    	    	arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof', arrQueueContactsIDs)); // only contacts from queue
            	var searchQueue = nlapiLoadSearch('contact', 'customsearch_clgx_sp_sync_contacts_all');
            	searchQueue.addFilters(arrFilters);
    			var resultSet = searchQueue.runSearch();
        		resultSet.forEachResult(function(searchResult) {
        			arrContactsIDs.push(parseInt(searchResult.getValue('internalid',null,null)));
                	return true;
        		});
        		
        		// find contacts existing in queue but not in netsuite anymore (probably from merges)
        		var arrDiff = _.difference(arrQueueContactsIDs, arrContactsIDs);
        		
    			for ( var i = 0; arrDiff != null && i < arrDiff.length; i++ ) {
    				var objQueue = _.find(arrQueue, function(arr){ return (arr.contactid == arrDiff[i]); });
    				if(objQueue != null){
    	            	nlapiDeleteRecord('customrecord_clgx_queue_contacts_to_sp', objQueue.id);
    					//nlapiSubmitField('customrecord_clgx_queue_contacts_to_sp', objQueue.id, ['custrecord_clgx_contact_to_sp_action'], [2]);
    	            	nbrContacts += 1;
    				}
    			}
    		}

			var usage = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
	        nlapiLogExecution('DEBUG','Usage', 'Usage = '+ usage);
	    	nlapiSendEmail(432742,71418,'SP 1.1 - Deleted ' + nbrContacts + ' merges from queue. Usage (' + usage + ')','',null,null,null,null,true);
	    	nlapiSendEmail(432742,1349020,'SP 1.1 - Deleted ' + nbrContacts + ' merges from queue. Usage (' + usage + ')','',null,null,null,null,true);
	    	
	    	//var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_customers', 'customdeploy_clgx_ss_sp_sync_customers');
	        //var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_others', 'customdeploy_clgx_ss_sp_sync_others');
	        //var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_customers', 'customdeploy_clgx_ss_sp_sync_customers_2');
	        //var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_contacts', 'customdeploy_clgx_ss_sp_sync_contacts');
	        var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_contacts_ar', 'customdeploy_clgx_ss_sp_sync_contacts_ar');
	        var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_ns2sp0', 'customdeploy_clgx_ss_sp_sync_ns2sp0');
	        var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_transact2sp', 'customdeploy_clgx_ss_sp_sync_transact2sp');
	        var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_ns2ns', 'customdeploy_clgx_ss_sp_sync_ns2ns');
  
	        
	        
    	}
        //nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
    	var str = String(error);
        if (str.match('SSS_USAGE_LIMIT_EXCEEDED')) {
        	var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
        	nlapiLogExecution('DEBUG','End', 'Finished processing because of limit exceeded error and reschedule it.');
        }
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}