//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SP_XML_API.js
//	Script Name:	CLGX_SP_XML_API
//	Script Id:		none
//	Script Runs:	Included On Each Other Script
//	Script Type:	Library
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		3/31/2015
//-------------------------------------------------------------------------------------------------

	var strLogURL = 'https://api3.silverpop.com/XMLAPI';
	var strURL = 'https://api3.silverpop.com/XMLAPI;jsessionid=';
	var username = 'silverpop.api.user@cologix.com';
	var password = 'w9fGx*s+Jn?63p';
	
	var dbid = 3325006; // main contacts database id
	var expnewid = 3413712; // new contacts query id (contacts created on SilverPOP - no netsuite contactid)
    var suppressid = 3080013; // suppression list
    var transactions = 3518268; // transactions
    var customers = 3325726; // customers
    
// ===========================================================================================================================================================
    
function clgx_sp_header (length){
	return {"Content-Type": "text/xml; charset=UTF-8", "Content-Length" : length};
}

//===========================================================================================================================================================

function clgx_sp_login (username,password){
	return '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><Login><USERNAME>' + username + '</USERNAME><PASSWORD>' + password + '</PASSWORD></Login></Body></Envelope>';
}

//===========================================================================================================================================================

function clgx_sp_logout (){
	return '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><Logout/></Body></Envelope>';
}

//===========================================================================================================================================================

function clgx_sp_recipient_create (objParams,arrSync,arrColumns){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><AddRecipient>';
	strXML += '<LIST_ID>' + objParams.listid + '</LIST_ID>';
	strXML += '<CREATED_FROM>' + objParams.from + '</CREATED_FROM>';
	strXML += '<UPDATE_IF_FOUND>' + objParams.update + '</UPDATE_IF_FOUND>';
	if(arrSync.length > 0){
		strXML += '<SYNC_FIELDS>';
		for ( var i = 0; arrSync != null && i < arrSync.length; i++ ) {
			strXML += '<SYNC_FIELD><NAME>' + arrSync[i].name + '</NAME><VALUE>' + arrSync[i].value + '</VALUE></SYNC_FIELD>';
		}
		strXML += '</SYNC_FIELDS>';
	}
	if(arrColumns.length > 0){
		for ( var i = 0; arrColumns != null && i < arrColumns.length; i++ ) {
			strXML += '<COLUMN><NAME>' + arrColumns[i].name + '</NAME><VALUE>' + arrColumns[i].value + '</VALUE></COLUMN>';
		}
	}
	strXML += '</AddRecipient></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_recipient_update (objParams, arrSync, arrColumns){
				
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><UpdateRecipient>';
	strXML += '<LIST_ID>' + objParams.listid + '</LIST_ID>';
	if(objParams.oldemail != ''){
		strXML += '<OLD_EMAIL>' + objParams.oldemail + '</OLD_EMAIL>';
	}
	if(objParams.recipientid > 0){ 
		strXML += '<RECIPIENT_ID>' + objParams.recipientid + '</RECIPIENT_ID>';
	}
	strXML += '<CREATED_FROM>' + objParams.from + '</CREATED_FROM>';
	if(arrSync.length > 0 && objParams.recipientid == 0){
		strXML += '<SYNC_FIELDS>';
		for ( var i = 0; arrSync != null && i < arrSync.length; i++ ) {
			strXML += '<SYNC_FIELD><NAME>' + arrSync[i].name + '</NAME><VALUE>' + arrSync[i].value + '</VALUE></SYNC_FIELD>';
		}
		strXML += '</SYNC_FIELDS>';
	}
	if(arrColumns.length > 0){
		for ( var i = 0; arrColumns != null && i < arrColumns.length; i++ ) {
			strXML += '<COLUMN><NAME>' + arrColumns[i].name + '</NAME><VALUE>' + arrColumns[i].value + '</VALUE></COLUMN>';
		}
	}
	strXML += '</UpdateRecipient></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_recipient_delete (objParams, arrColumns){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><RemoveRecipient>';
	strXML += '<LIST_ID>' + objParams.listid + '</LIST_ID>';
	if(objParams.email != ''){
		strXML += '<EMAIL>' + objParams.email + '</EMAIL>';
	}
	if(arrColumns.length > 0){
		for ( var i = 0; arrColumns != null && i < arrColumns.length; i++ ) {
			strXML += '<COLUMN><NAME>' + arrColumns[i].name + '</NAME><VALUE>' + arrColumns[i].value + '</VALUE></COLUMN>';
		}
	}
	strXML += '</RemoveRecipient></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_recipient_add_to_list (objParams, arrColumns){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><AddContactToContactList>';
	strXML += '<CONTACT_LIST_ID>' + objParams.listid + '</CONTACT_LIST_ID>';
	if(objParams.recipientid != '' && objParams.recipientid != null && objParams.recipientid != 0){
		strXML += '<CONTACT_ID>' + objParams.recipientid + '</CONTACT_ID>';
	}
	if(arrColumns.length > 0){
		for ( var i = 0; arrColumns != null && i < arrColumns.length; i++ ) {
			strXML += '<COLUMN><NAME>' + arrColumns[i].name + '</NAME><VALUE>' + arrColumns[i].value + '</VALUE></COLUMN>';
		}
	}
	strXML += '</AddContactToContactList></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_recipient_optout (objParams, arrColumns){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><OptOutRecipient>';
	strXML += '<LIST_ID>' + objParams.listid + '</LIST_ID>';
	if(objParams.recipientid != '' && objParams.recipientid != null && objParams.recipientid != 0){
		strXML += '<RECIPIENT_ID>' + objParams.recipientid + '</RECIPIENT_ID>';
	}
	if(objParams.mailingid != '' && objParams.mailingid != null && objParams.mailingid != 0){
		strXML += '<MAILING_ID>' + objParams.mailingid + '</MAILING_ID>';
	}
	if(objParams.jobid != '' && objParams.jobid != null && objParams.jobid != 0){
		strXML += '<JOB_ID>' + objParams.jobid + '</JOB_ID>';
	}
	if(objParams.email != ''){
		strXML += '<EMAIL>' + objParams.email + '</EMAIL>';
	}
	if(arrColumns.length > 0){
		for ( var i = 0; arrColumns != null && i < arrColumns.length; i++ ) {
			strXML += '<COLUMN><NAME>' + arrColumns[i].name + '</NAME><VALUE>' + arrColumns[i].value + '</VALUE></COLUMN>';
		}
	}
	strXML += '</OptOutRecipient></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_list_export (objParams,arrColumns){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><ExportList>';
	strXML += '<LIST_ID>' + objParams.listid + '</LIST_ID>';
	if(objParams.email != ''){
		strXML += '<EMAIL>' + objParams.email + '</EMAIL>';
	}
	strXML += '<EXPORT_TYPE>' + objParams.exptype + '</EXPORT_TYPE>';
	strXML += '<EXPORT_FORMAT>' + objParams.expformat + '</EXPORT_FORMAT>';
	if(objParams.encode != ''){
		strXML += '<FILE_ENCODING>' + objParams.encode + '</FILE_ENCODING>';
	}
	if(objParams.add){
		strXML += '<ADD_TO_STORED_FILES/>';
	}
	if(objParams.start != ''){
		strXML += '<DATE_START>' + objParams.start + '</DATE_START>';
	}
	if(objParams.end != ''){
		strXML += '<DATE_END>' + objParams.end + '</DATE_END>';
	}
	if(objParams.created){
		strXML += '<USE_CREATED_DATE/>';
	}
	if(objParams.leadsource && arrColumns == null){ // this, if true, is used only if EXPORT_COLUMNS is not used
		strXML += '<INCLUDE_LEAD_SOURCE>' + objParams.leadsource + '</INCLUDE_LEAD_SOURCE>';
	}
	if(objParams.dateformat != ''){
		strXML += '<LIST_DATE_FORMAT>' + objParams.dateformat + '</LIST_DATE_FORMAT>';
	}
	if(arrColumns.length > 0 && !objParams.leadsource){
		strXML += '<EXPORT_COLUMNS>';
		for ( var i = 0; arrColumns != null && i < arrColumns.length; i++ ) {
			strXML += '<COLUMN>' + arrColumns[i] + '</COLUMN>';
		}
		strXML += '</EXPORT_COLUMNS>';
	}
	strXML += '</ExportList></Body></Envelope>';
	return strXML;
}

// ===========================================================================================================================================================

function clgx_sp_table_create (objParams,arrColumns){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><CreateTable>';
	strXML += '<TABLE_NAME>' + objParams.name + '</TABLE_NAME>';
	strXML += '<COLUMNS>';
	for ( var i = 0; arrColumns != null && i < arrColumns.length; i++ ) {
		strXML += '<COLUMN>';
		strXML += '<NAME>' + arrColumns[i].name + '</NAME>';
		strXML += '<TYPE>' + arrColumns[i].type + '</TYPE>';
		strXML += '<IS_REQUIRED>' + arrColumns[i].required + '</IS_REQUIRED>';
		strXML += '<KEY_COLUMN>' + arrColumns[i].key + '</KEY_COLUMN>';
		if(arrColumns[i].defvalue != ''){
			strXML += '<DEFAULT_VALUE>' + arrColumns[i].defvalue + '</DEFAULT_VALUE>';
		}
		var arrSelections = arrColumns[i].selections;
		if(arrSelections.length > 0){
			strXML += '<SELECTION_VALUES>';
			for ( var j = 0; arrSelections != null && j < arrSelections.length; j++ ) {
				strXML += '<VALUE>' + arrSelections[j] + '</VALUE>';
			}
			strXML += '</SELECTION_VALUES>';
		}
		strXML += '</COLUMN>';
	}
	strXML += '</COLUMNS>';
	strXML += '</CreateTable></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_table_export (objParams){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><ExportTable>';
	if(objParams.name != ''){
		strXML += '<TABLE_NAME>' + objParams.name + '</TABLE_NAME>';
	}
	if(objParams.tableid != ''){
		strXML += '<TABLE_ID>' + objParams.tableid + '</TABLE_ID>';
	}
	if(objParams.visibility != ''){
		strXML += '<TABLE_VISIBILITY>' + objParams.visibility + '</TABLE_VISIBILITY>';
	}
	if(objParams.email != ''){
		strXML += '<EMAIL>' + objParams.email + '</EMAIL>';
	}
	strXML += '<EXPORT_FORMAT>' + objParams.expformat + '</EXPORT_FORMAT>';
	if(objParams.encode != ''){
		strXML += '<FILE_ENCODING>' + objParams.encode + '</FILE_ENCODING>';
	}
	if(objParams.add){
		strXML += '<ADD_TO_STORED_FILES/>';
	}
	if(objParams.start != ''){
		strXML += '<DATE_START>' + objParams.start + '</DATE_START>';
	}
	if(objParams.end != ''){
		strXML += '<DATE_END>' + objParams.end + '</DATE_END>';
	}
	strXML += '</ExportTable></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_table_purge (objParams){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><PurgeTable>';
	strXML += '<TABLE_ID>' + objParams.tableid + '</TABLE_ID>';
	if(objParams.before != ''){
		strXML += '<DELETE_BEFORE>' + objParams.before + '</DELETE_BEFORE>';
	}
	if(objParams.email != ''){
		strXML += '<EMAIL>' + objParams.email + '</EMAIL>';
	}
	strXML += '</PurgeTable></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_table_delete (objParams){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><DeleteTable>>';
	strXML += '<TABLE_ID>' + objParams.tableid + '</TABLE_ID>';
	if(objParams.email != ''){
		strXML += '<EMAIL>' + objParams.email + '</EMAIL>';
	}
	strXML += '</DeleteTable>></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_table_join (objParams,arrFields){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><JoinTable>>';
	strXML += '<TABLE_ID>' + objParams.tableid + '</TABLE_ID>';
	strXML += '<LIST_ID>' + objParams.listid + '</LIST_ID>';
	if(objParams.remove){
		strXML += '<REMOVE>' + objParams.remove + '</REMOVE>';
	}
	if(objParams.email != ''){
		strXML += '<EMAIL>' + objParams.email + '</EMAIL>';
	}
	for ( var i = 0; arrFields != null && i < arrFields.length; i++ ) {
		strXML += '<MAP_FIELD>';
		strXML += '<LIST_FIELD>' + arrFields[i].listfield + '</LIST_FIELD>';
		strXML += '<TABLE_FIELD>' + arrFields[i].tablefield + '</TABLE_FIELD>';
		strXML += '</MAP_FIELD>';
	}
	strXML += '</JoinTable>></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_table_insert_data (objParams,arrRows){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><InsertUpdateRelationalTable>';
	strXML += '<TABLE_ID>' + objParams.tableid + '</TABLE_ID>';
	strXML += '<ROWS>';
	for ( var i = 0; arrRows != null && i < arrRows.length; i++ ) {
		strXML += '<ROW>';
		var arrColumns = arrRows[i];
		for ( var j = 0; arrColumns != null && j < arrColumns.length; j++ ) {
			strXML += '<COLUMN name="' + arrColumns[j].name + '">' + arrColumns[j].value + '</COLUMN>';
		}
		strXML += '</ROW>';
	}
	strXML += '</ROWS>';
	strXML += '</InsertUpdateRelationalTable></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_table_delete_data (objParams,arrRows){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><DeleteRelationalTableData>';
	strXML += '<TABLE_ID>' + objParams.tableid + '</TABLE_ID>';
	strXML += '<ROWS>';
	for ( var i = 0; arrRows != null && i < arrRows.length; i++ ) {
		strXML += '<ROW>';
		var arrKeys = arrRows[i];
		for ( var j = 0; arrKeys != null && j < arrKeys.length; j++ ) {
			strXML += '<KEY_COLUMN name="' + arrKeys[j].name + '">' + arrKeys[j].value + '</KEY_COLUMN>';
		}
		strXML += '</ROW>';
	}
	strXML += '</ROWS>';
	strXML += '</DeleteRelationalTableData></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_recipient_raw_data_export (objParams, arrMailing, arrReports, arrColumns){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><RawRecipientDataExport>';
	if(objParams.listid != ''){
		strXML += '<LIST_ID>' + objParams.listid + '</LIST_ID>';
	}
	if(arrMailing.length > 0){
		for ( var i = 0; arrMailing != null && i < arrMailing.length; i++ ) {
			strXML += '<MAILING> <MAILING_ID>' + arrMailing[i] + '</MAILING_ID></MAILING>';
		}
	}
	if(arrReports.length > 0){
		for ( var i = 0; arrReports != null && i < arrReports.length; i++ ) {
			strXML += '<MAILING> <REPORT_ID>' + arrReports[i] + '</REPORT_ID></MAILING>';
		}
	}
	strXML += '<MOVE_TO_FTP/>';
	strXML += '<EXCLUDE_DELETED/>';
	strXML += '<ALL_EVENT_TYPES/>';
	strXML += '<INCLUDE_INBOX_MONITORING/>';
	if(objParams.email != ''){
		strXML += '<EMAIL>' + objParams.email + '</EMAIL>';
	}
	if(objParams.expformat != ''){
		strXML += '<EXPORT_FORMAT>' + objParams.expformat + '</EXPORT_FORMAT>';
	}
	if(objParams.filename != ''){
		strXML += '<EXPORT_FILE_NAME>' + objParams.filename + '</EXPORT_FILE_NAME>';
	}
	if(arrColumns.length > 0){
		for ( var i = 0; arrColumns != null && i < arrColumns.length; i++ ) {
			strXML += '<COLUMN><NAME>' + arrColumns[i].name + '</NAME><VALUE>' + arrColumns[i].value + '</VALUE></COLUMN>';
		}
	}
	strXML += '</RawRecipientDataExport></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_recipient_mailings (objParams){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><ListRecipientMailings>';
	strXML += '<LIST_ID>' + objParams.listid + '</LIST_ID>';
	strXML += '<RECIPIENT_ID>' + objParams.recipientid + '</RECIPIENT_ID>';
	strXML += '</ListRecipientMailings></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_recipient_programs (objParams, arrTags){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><GetProgramsByContact>';
	strXML += '<LIST_ID>' + objParams.listid + '</LIST_ID>';
	strXML += '<INCLUDE_ACTIVE>' + objParams.active + '</INCLUDE_ACTIVE>';
	strXML += '<INCLUDE_INACTIVE>' + objParams.inactive + '</INCLUDE_INACTIVE>';
	strXML += '<CONTACT_ID>' + objParams.recipientid + '</CONTACT_ID>';
	if(objParams.begin != '' && objParams.end != ''){
		strXML += '<CREATED_DATE_RANGE>';
		strXML += '<BEGIN_DATE>' + objParams.begin + '</BEGIN_DATE>';
		strXML += '<END_DATE>' + objParams.end + '</END_DATE>';
		strXML += '</CREATED_DATE_RANGE>';
	}
	if(objParams.history != ''){
		strXML += '<INCLUDE_HISTORY/>';
	}
	if(objParams.sales != ''){
		strXML += '<APPROVED_FOR_SALES/>';
	}
	if(arrTags.length > 0){
		strXML += '<INCLUDE_TAGS>';
		for ( var i = 0; arrColumns != null && i < arrColumns.length; i++ ) {
			strXML += '<TAG>' + arrTags[i] + '</TAG>';
		}
		strXML += '</INCLUDE_TAGS>';
	}
	if(objParams.track != ''){
		strXML += '<INCLUDE_TRACK/>';
	}
	if(objParams.step != ''){
		strXML += '<INCLUDE_STEP/>';
	}
	strXML += '</GetProgramsByContact></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_recipient_mailing_send (objParams){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><SendMailing>';
	strXML += '<MailingId>' + objParams.mailingid + '</MailingId>';
	strXML += '<RecipientEmail>' + objParams.email + '</RecipientEmail>';
	strXML += '</SendMailing></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_recipient_program_add (objParams){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><AddContactToProgram>';
	strXML += '<PROGRAM_ID>' + objParams.programid + '</PROGRAM_ID>';
	strXML += '<CONTACT_ID>' + objParams.recipientid + '</CONTACT_ID>';
	strXML += '</AddContactToProgram></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_programs (objParams, arrTags){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><GetPrograms>';
	strXML += '<INCLUDE_ACTIVE>' + objParams.active + '</INCLUDE_ACTIVE>';
	strXML += '<INCLUDE_INACTIVE>' + objParams.inactive + '</INCLUDE_INACTIVE>';
	if(objParams.begin != '' && objParams.end != ''){
		strXML += '<CREATED_DATE_RANGE>';
		strXML += '<BEGIN_DATE>' + objParams.begin + '</BEGIN_DATE>';
		strXML += '<END_DATE>' + objParams.end + '</END_DATE>';
		strXML += '</CREATED_DATE_RANGE>';
	}
	if(objParams.listid > 0){
		strXML += '<LIST_ID>' + objParams.listid + '</LIST_ID>';
	}
	if(arrTags.length > 0){
		strXML += '<INCLUDE_TAGS>';
		for ( var i = 0; arrColumns != null && i < arrColumns.length; i++ ) {
			strXML += '<TAG>' + arrTags[i] + '</TAG>';
		}
		strXML += '</INCLUDE_TAGS>';
	}
	if(objParams.sales != ''){
		strXML += '<APPROVED_FOR_SALES/>';
	}
	strXML += '</GetPrograms></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_templates (objParams){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><GetMailingTemplates>';
	strXML += '<VISIBILITY>' + objParams.visibility + '</VISIBILITY>';
	if(objParams.crm != ''){
		strXML += '<IS_CRM_ENABLED>' + objParams.crm + '</IS_CRM_ENABLED>';
	}
	if(objParams.start != ''){
		strXML += '<LAST_MODIFIED_START_DATE>' + objParams.start + '</LAST_MODIFIED_START_DATE>';
	}
	if(objParams.end != ''){
		strXML += '<LAST_MODIFIED_END_DATE>' + objParams.end + '</LAST_MODIFIED_END_DATE>';
	}
	strXML += '</GetMailingTemplates></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================

function clgx_sp_mailing_preview (objParams){
	
	var strXML = '';
	strXML += '<?xml version="1.0" encoding="UTF-8"?><Envelope><Body><PreviewMailing>';
	strXML += '<MailingId>' + objParams.mailingid + '</MailingId>';
	strXML += '</PreviewMailing></Body></Envelope>';
	return strXML;
}

//===========================================================================================================================================================


