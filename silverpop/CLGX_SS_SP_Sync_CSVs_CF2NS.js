//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SP_Sync_CSVs_CF2NS.js
//	Script Name:	CLGX_SS_SP_Sync_CSVs_CF2NS
//	Script Id:		customscript_clgx_ss_sp_sync_csvs_cf2ns
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		04/16/2015
//-------------------------------------------------------------------------------------------------

function suitelet_ss_sp_sync_csvs_cf2ns (request, response){
    try {
    	
    	var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/silverpop/ftp.cfm');
		
		if(requestURL.body == 'true'){
			
		nlapiSendEmail(71418,71418,'success',requestURL.body,null,null,null,null,true);
		
			/*
			var objFile = nlapiLoadFile(2404555);
			var arrFiles = JSON.parse(objFile.getValue());
			for ( var i = 0; arrFiles != null && i < arrFiles.length; i++ ) {
		    	var url = 'https://command1.cologix.com:10313/netsuite/silverpop/get_csv.cfm?csv=' + arrFiles[i].cffile;
				var requestURL = nlapiRequestURL(url);
				var strResponse = requestURL.body;
				
				var csv = nlapiCreateFile(arrFiles[i].cffile, 'PLAINTEXT', strResponse);
				csv.setFolder(1909409);
				nlapiSubmitFile(csv);
			}
			nlapiSendEmail(71418,71418,'3. SilverPOP Sync - Copy CSVs files from CF to NS','',null,null,null,null);
			*/
		}
		else{
			nlapiSendEmail(71418,71418,'fail',requestURL.body,null,null,null,null,true);
			
			//nlapiSendEmail(71418,71418,'2. SilverPOP Sync - Error on CF','',null,null,null,null);
		}
    }
    
// Start Catch Errors Section --------------------------------------------------------------------------

    catch (error) { 
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
