//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SP_Sync_NS_to_NS.js
//	Script Name:	CLGX_SS_SP_Sync_NS_to_NS
//	Script Id:		customscript_clgx_ss_sp_sync_ns2ns
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/20/2015
//-------------------------------------------------------------------------------------------------
function scheduled_sp_sync_ns2ns(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        var context = nlapiGetContext();
        var environment = context.getEnvironment()
    	
    	if(environment == 'PRODUCTION'){
    		
	        var objFile = nlapiLoadFile(2418654);
			var arrContacts = JSON.parse(objFile.getValue());
			var arrNewContacts = JSON.parse(objFile.getValue());
			
			if(arrContacts.length > 0){
				if(arrContacts.length > 300){
			        var loopndx = 300;
				}
				else{
					var loopndx = arrContacts.length;
				}
	
				var date = new Date();
				var startScript = moment();
		        var emailAdminSubject = '1.8 NS to NS - ' + startScript.format('M/D/YYYY');
				var emailAdminBody = '';
				emailAdminBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');
				
				emailAdminBody += '<h2>' + arrContacts.length + ' contacts processed</h2>';
				emailAdminBody += '<table border="1" cellpadding="5">';
			    emailAdminBody += '<tr><td>CustomerID</td><td>ContactID</td><td>RecipientID</td><td>Action</td></tr>';
			    
			    var arrDelete = new Array();
			    
				for ( var j = 0; arrContacts != null && j < loopndx; j++ ) {
					
					var arrColumns = new Array();
					arrColumns.push(new nlobjSearchColumn('internalid',null,null));
					arrColumns.push(new nlobjSearchColumn('comments',null,null));
					var arrFilters = new Array();
					arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrContacts[j]));
					var searchContact = nlapiSearchRecord('contact', null, arrFilters, arrColumns);
					
					if(searchContact != null){ // contact is still in Netsuite
	
						var recContact = nlapiLoadRecord ('contact',arrContacts[j]);
						var comments = recContact.getFieldValue('comments');
						
						var strOldRecipients = recContact.getFieldValue('custentity_clgx_sp_recipients');
						//var headcustomerid = nlapiGetFieldValue('company');
						
						//if(headcustomerid != null && headcustomerid != ''){

							if(strOldRecipients != null && strOldRecipients != ''){
								var arrOldRecipients = JSON.parse(recContact.getFieldValue('custentity_clgx_sp_recipients'));
							}
							else{
								var arrOldRecipients = [];
							}
					    	
					    	var arrNewRecipients = new Array();
					    	var arrColumns = new Array();
					    	var arrFilters = new Array();
					    	arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrContacts[j]));
					    	var searchNewRecipients = nlapiSearchRecord('contact', 'customsearch_clgx_sp_sync_contacts', arrFilters, arrColumns);
					    	
					    	for ( var i = 0; searchNewRecipients != null && i < searchNewRecipients.length; i++ ) {
					    		var searchNewRecipient = searchNewRecipients[i];
					        	var objRecipient = new Object();
					    		var customerid = searchNewRecipient.getValue('internalid','company',null);
					    		
					    		if(customerid != null && customerid != ''){
					    			objRecipient["customerid"] = parseInt(customerid);
						    		objRecipient["contactid"] = parseInt(searchNewRecipient.getValue('internalid',null,null));
						    		arrNewRecipients.push(objRecipient);
					    		}
					    	}
					    	
					    	for ( var i = 0; arrOldRecipients != null && i < arrOldRecipients.length; i++ ) {
					    		var objRecipient = _.find(arrNewRecipients, function(arr){ return (arr.customerid == arrOldRecipients[i].customerid); });
					    		if(objRecipient == null){
					    			if(comments != null){
						    			if(comments.indexOf("SilverPOP") < 0){
						    				arrDelete.push(arrOldRecipients[i].recipientid);
						    			}
					    			}
					    			else{
					    				arrDelete.push(arrOldRecipients[i].recipientid);
					    			}
					    		}
					    	}
					    	
					    	var update = 0;
					    	for ( var i = 0; arrNewRecipients != null && i < arrNewRecipients.length; i++ ) {
					    		var objRecipient = _.find(arrOldRecipients, function(arr){ return (arr.customerid == arrNewRecipients[i].customerid); });
					    		if(objRecipient == null){
					    			var update = 1;
					    		}
					    	}
					    	
					    	if(arrOldRecipients != null && arrNewRecipients != null){
					    		if(arrOldRecipients.length != arrNewRecipients.length){
					    			if(arrOldRecipients[0] != null){
						    			if(arrOldRecipients[0].customerid != ''){
						    				var update = 1;
						    			}
					    			}
					    		}
					    	}
					    	
					    	if(update == 1){
					    		
					    		var arrColumns = new Array();
						    	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
						    	var arrFilters = new Array();
						    	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_contact_to_sp_contact",null,"equalto",arrContacts[j]));
						    	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_contact_to_sp_done',null,'is','F'));
						    	var searchQueue = nlapiSearchRecord('customrecord_clgx_queue_contacts_to_sp', null, arrFilters, arrColumns);
						    	
					    		if(searchQueue == null){ // add contact to queue if not in it
						    		var record = nlapiCreateRecord('customrecord_clgx_queue_contacts_to_sp');
									record.setFieldValue('custrecord_clgx_contact_to_sp_contact', arrContacts[j]);
									record.setFieldValue('custrecord_clgx_contact_to_sp_action', 1);
									record.setFieldValue('custrecord_clgx_contact_to_sp_done', 'F');
									var idRec = nlapiSubmitRecord(record, false,true);
									
									emailAdminBody += '<tr><td></td><td>' + arrContacts[j] + '</td><td></td><td>1</td></tr>';
									nlapiLogExecution('DEBUG','Contacts Queue', ' | Update Contact ID = ' + arrContacts[j] + '  |');
					    		}
					    	}
				    	//}
					}
					else{ // the contact from the queue is no longer in Netsuite, so delete it from SilverPOP too
						
						var arrColumns = new Array();
				    	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
				    	var arrFilters = new Array();
				    	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_contact_to_sp_contact",null,"equalto",arrContacts[j]));
				    	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_contact_to_sp_done',null,'is','F'));
				    	var searchQueue = nlapiSearchRecord('customrecord_clgx_queue_contacts_to_sp', null, arrFilters, arrColumns);
				    	
			    		if(searchQueue == null){ // add contact to queue if not in it
				    		var record = nlapiCreateRecord('customrecord_clgx_queue_contacts_to_sp');
							record.setFieldValue('custrecord_clgx_contact_to_sp_contact', arrContacts[j]);
							record.setFieldValue('custrecord_clgx_contact_to_sp_action', 2);
							record.setFieldValue('custrecord_clgx_contact_to_sp_done', 'F');
							var idRec = nlapiSubmitRecord(record, false,true);
							
							emailAdminBody += '<tr><td></td><td>' + arrContacts[j] + '</td><td></td><td>2</td></tr>';
							nlapiLogExecution('DEBUG','Contacts Queue', ' | Delete Contact ID = ' + arrContacts[j] + '  |');
			    		}
			    		else{ // if in queue change status in case it was different
			            	nlapiSubmitField('customrecord_clgx_queue_contacts_to_sp', searchQueue[0].getValue('internalid',null,null), 'custrecord_clgx_contact_to_sp_action', 2);
			    		}
			    		
						nlapiLogExecution('DEBUG','Contacts Queue', ' | Contact ID = ' + arrContacts[j] + ' does not exist |');
					}
					
			    	arrNewContacts = _.reject(arrNewContacts, function(num){
				        return (num == arrContacts[j]);
					});
			    	
			    	var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
		            var index = j + 1;
		            nlapiLogExecution('DEBUG','Results ', ' | Index = ' + index + ' of ' + arrContacts.length + ' | Usage = '+ usageConsumtion + '  | ContactID = ' + arrContacts[j] + ' |');
				}
				
				var file = nlapiCreateFile('ns_contacts_queue.json', 'PLAINTEXT', JSON.stringify(arrNewContacts));
				file.setFolder(1909409);
				nlapiSubmitFile(file);
				
				
				
				var emailDeleted = '';
				if(arrDelete.length > 0){
					
					emailDeleted += 'Deleted = ' + arrDelete.length + '<br><br>';
					nlapiLogExecution('DEBUG','Contacts Queue', ' | Deleted = ' + arrDelete.length + '  |');
					
	// ============================ Open Session ===============================================================================================
					
			    	var strPost = clgx_sp_login (username,password);
			    	var objHead = clgx_sp_header (strPost.length);
					var requestURL = nlapiRequestURL(strLogURL,strPost,objHead);
					var responseXML = nlapiStringToXML(requestURL.getBody());
					var login = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
					
					emailDeleted += 'Login = ' + login + '<br><br>';
					nlapiLogExecution('DEBUG','Contacts Queue', ' | Login = ' + login + '  |');
					
					if(login == 'true'){
						
						var jsessionid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SESSIONID');
						strURL += jsessionid;
						
						nlapiLogExecution('DEBUG','Session ID', ' | Session ID = ' + jsessionid + '  |');
					
						emailDeleted += 'Deleted = ' + JSON.stringify(arrDelete)+ '<br><br>';
					
						for ( var i = 0; arrDelete != null && i < arrDelete.length; i++ ) {
							/*
							nlapiLogExecution('DEBUG','NOTDELETED', '| recipientid  ' + arrDelete[i]  + ' was deleted |'); 
							emailAdminBody += '<tr><td>Deleted</td><td>' + arrDelete[i] + '</td><td></td><td>2</td></tr>';
							
			    			// ====== Build Parameters list =================================
			    			var objParams = new Object();
			    			objParams["listid"] = '3325006';
			    			objParams["email"] = '';
			    			// ====== Build Columns Fields Array =================================
			    			var arrColumns = new Array();
			    			arrColumns.push(JSON.parse('{"name": "RECIPIENT_ID", "value": "' + arrDelete[i] + '"}'));
			    			var strPost = clgx_sp_recipient_delete (objParams, arrColumns);
			    	    	var objHead = clgx_sp_header (strPost.length);
			    	    	
			    	    	
			    	    	
			    	    	var requestURL = nlapiRequestURL(strURL,strPost,objHead);
			    			var strBody = requestURL.getBody();
			    			var responseXML = nlapiStringToXML(strBody);
		
			    			var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
			    			if(success == 'TRUE'){
			    				nlapiLogExecution('DEBUG','DELETED', '| recipientid  ' + arrDelete[i] + ' was deleted |'); 
			    				
			    			}
			    			else{
			    				nlapiLogExecution('DEBUG','NOTDELETED', '| recipientid  ' + arrDelete[i]  + ' was not deleted |'); 
			    				emailAdminBody += '<tr><td>Not Deleted</td><td>' + arrDelete[i] + '</td><td></td><td>2</td></tr>';
			    			}
			    			*/
			    			
							
						}
	
	// ============================ Close Session ===============================================================================================
						
				    	var strPost =  clgx_sp_logout ();
						var objHead = clgx_sp_header (strPost.length);
						var requestURL = nlapiRequestURL(strURL,strPost,objHead);
						var strBody = requestURL.getBody();
						var responseXML = nlapiStringToXML(strBody);
						var logout = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
						
						emailDeleted += 'Logout = ' + logout + '<br><br>';
						nlapiLogExecution('DEBUG','Contacts Queue', ' | Logout = ' + logout + '  |');
					}
					
				}
				else{
					emailDeleted += 'Deleted = 0<br><br>';
				}
	
	// ============================ Send admin email ===============================================================================================
				
			    emailAdminBody += '</table>';
				var endScript = moment();
				emailAdminBody += '<br><br>';
				emailAdminBody += emailDeleted;
				emailAdminBody += 'End : ' + endScript.format('M/D/YYYY h:mm:ss a') + '<br>';
				emailAdminBody += 'Total execution minutes : ' + (endScript.diff(startScript)/60000).toFixed(1) + '<br>';
				var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
				emailAdminBody += 'Total usage : ' + usageConsumtion;
				emailAdminBody += '<br><br>';
				if(arrContacts != null){
					emailAdminSubject += ' | '+ arrContacts.length + ' contacts in ' + (endScript.diff(startScript)/60000).toFixed(1) + ' mins | ';
				}
				else{
					emailAdminSubject += ' | 0 contacts in ' + (endScript.diff(startScript)/60000).toFixed(1) + ' mins | ';
				}
				//nlapiSendEmail(432742,71418,emailAdminSubject,emailAdminBody,null,null,null,null);
				
				var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
				
			}
			else{
				nlapiSendEmail(432742,71418,'SP 1.8 - Finish NS to NS','',null,null,null,null,true);
				nlapiSendEmail(432742,179749,'SP 1.8 - Finish NS to NS','',null,null,null,null,true);
				nlapiSendEmail(432742,1349020,'SP 1.8 - Finish NS to NS','',null,null,null,null,true);
				
				var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_ns2sp', 'customdeploy_clgx_ss_sp_sync_ns2sp');
			}	
		}
		nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}



