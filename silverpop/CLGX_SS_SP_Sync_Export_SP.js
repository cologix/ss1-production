//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SP_Sync_Export_SP.js
//	Script Name:	CLGX_SS_SP_Sync_Export_SP
//	Script Id:		customscript_clgx_ss_sp_export_sp
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		04/16/2015
//-------------------------------------------------------------------------------------------------

function suitelet_ss_sp_export_sp (request, response){
    try {
    	
        var emailAdminSubject = '1. SilverPOP Sync - Export SilverPOP lists to FTP';
        var emailAdminBody = '';
        
// ============================ Open Session ===============================================================================================
    	
    	var strPost = clgx_sp_login (username,password);
    	var objHead = clgx_sp_header (strPost.length);
		var requestURL = nlapiRequestURL(strLogURL,strPost,objHead);
		var responseXML = nlapiStringToXML(requestURL.getBody());
		var login = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
		
		if(login == 'true'){
			
			var jsessionid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SESSIONID');
			strURL += jsessionid;

			emailAdminBody += 'Login = ' + login + ' | jsessionid = ' + jsessionid + '<br/><br/>';

// ============================ Export Lists ===============================================================================================
	
			var strLists = '[{"listid":3325006,"list":"all","spfile":"","cffile":"sp-contacts-all.csv"},{"listid":3413712,"list":"new","spfile":"","cffile":"sp-contacts-new.csv"},{"listid":3080013,"list":"suppress","spfile":"","cffile":"sp-suppress.csv"}]';
			var arrLists = JSON.parse(strLists);
			
			for ( var i = 0; arrLists != null && i < arrLists.length; i++ ) {
				
				// ====== Build Parameters list =================================
				var objParams = new Array();
				objParams["listid"] = arrLists[i].listid;
				objParams["email"] = '';
				objParams["exptype"] = 'ALL';
				objParams["expformat"] = 'CSV';
				objParams["encode"] = '';
				objParams["add"] = false;
				objParams["start"] = '';
				objParams["end"] = '';
				objParams["created"] = false;
				objParams["leadsource"] = false;
				objParams["dateformat"] = false;
				// ====== Build Columns Fields Array =================================
				
				var arrColumns = new Array();
				if(arrLists[i].listid == 3325006 || arrLists[i].listid == 3413712){
					
					arrColumns.push('RECIPIENT_ID');
					arrColumns.push('Contact Internal ID');
					arrColumns.push('Company Internal ID');
					
					if(arrLists[i].listid == 3413712){
						arrColumns.push('Email');
						arrColumns.push('Company Name');
						arrColumns.push('Company Primary Segment');
						arrColumns.push('Company Secondary Segment');
						arrColumns.push('Company Markets');
						arrColumns.push('Company Lead Source');
						arrColumns.push('Company Status');
						arrColumns.push('Company Subsidiary');
						arrColumns.push('Contact First name');
						
						arrColumns.push('Contact Last Name');
						arrColumns.push('Contact Name');
						arrColumns.push('Contact Phone');
						arrColumns.push('Contact Role');
						arrColumns.push('Contact Subsidiary');
						arrColumns.push('Date Created');
						arrColumns.push('Date of First Order');
						arrColumns.push('Date of First Sale');
						arrColumns.push('Global Subscription Status');
						arrColumns.push('Optimized Send Hour');
						arrColumns.push('Sales Rep');
						arrColumns.push('Sales Rep Email');
						arrColumns.push('Sales Rep Phone');
						arrColumns.push('Sales Rep Supervisor');
					}
				}
				if(arrLists[i].listid == 3080013){
					arrColumns.push('Email');
					arrColumns.push('Opt In Date');
					arrColumns.push('Opt In Details');
					arrColumns.push('Opted Out');
					arrColumns.push('Email Type');
					arrColumns.push('Opted Out Date');
					arrColumns.push('Opt Out Details');
				}
				
				var strPost = clgx_sp_export_list (objParams,arrColumns);
				var objHead = clgx_sp_header (strPost.length);
				var requestURL = nlapiRequestURL(strURL,strPost,objHead);
				var strBody = requestURL.getBody();
				var responseXML = nlapiStringToXML(strBody);
				
				var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
				if(success == 'TRUE'){
					var jobid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/JOB_ID');
					var path = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/FILE_PATH');
					arrLists[i].spfile = path;
				}
				else{
				}
			}
			
// ============================ Close Session ===============================================================================================
			
	    	var strPost =  clgx_sp_logout ();
			var objHead = clgx_sp_header (strPost.length);
			var requestURL = nlapiRequestURL(strURL,strPost,objHead);
			var strBody = requestURL.getBody();
			var responseXML = nlapiStringToXML(strBody);
			var logout = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
			
			emailAdminBody += 'Logout = ' + logout + '<br/><br/>';
	
			
			var strLists = JSON.stringify(arrLists);
			var fileName = nlapiCreateFile('sp_ftp_last_files_names.json', 'PLAINTEXT', strLists);
			fileName.setFolder(1909409);
			nlapiSubmitFile(fileName);
			
			emailAdminBody += 'strLists = ' + strLists + '<br/><br/>';
			
			var url = ('https://command1.cologix.com:10313/netsuite/silverpop/set_sp_ftp_last_files_names.cfm?json=' + strLists).replace(/ /g, '_');
			var requestURLCF = nlapiRequestURL(url);
 			var successCF = requestURLCF.body;
 			
 			emailAdminBody += 'successCF = ' + successCF + '<br/><br/>';
 			
		}
		
		nlapiSendEmail(71418,71418,emailAdminSubject,emailAdminBody,null,null,null,null,true);
    }
    
// Start Catch Errors Section --------------------------------------------------------------------------

    catch (error) { 
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
