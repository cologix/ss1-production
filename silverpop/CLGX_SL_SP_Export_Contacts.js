nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_SP_Export_Contacts.js
//	Script Name:	CLGX_SL_SP_Export_Contacts
//	Script Id:		customscript_clgx_sl_sp_export_contacts
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		24/03/2015
//-------------------------------------------------------------------------------------------------

function suitelet_sp_export_contacts (request, response){
    try {
    	
    	requestIP = request.getHeader('NS-Client-IP');
    	if (requestIP == '64.127.72.198'){
	    	var objFile = nlapiLoadFile(2279974);
    		response.write(objFile.getValue());
    	}
    	else{
    		response.write('test');
    	}
   		
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}