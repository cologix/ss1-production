nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_SP_Export_Customers.js
//	Script Name:	CLGX_SL_SP_Export_Customers
//	Script Id:		customscript_clgx_sl_sp_export_customers
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		24/03/2015
//-------------------------------------------------------------------------------------------------

function suitelet_sp_export_customers (request, response){
    try {
    	
    	requestIP = request.getHeader('NS-Client-IP');
    	if (requestIP == '64.127.72.198'){
	    	var objFile = nlapiLoadFile(2282287);
    		response.write(objFile.getValue());
    	}
    	else{
    		response.write('test');
    	}
   		
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}