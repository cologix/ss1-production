//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SP_Sync_NS_Contacts_Array.js
//	Script Name:	CLGX_SS_SP_Sync_NS_Contacts_Array
//	Script Id:		customscript_clgx_ss_sp_sync_contacts_ar
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		9/28/2015
//-------------------------------------------------------------------------------------------------
function scheduled_sp_sync_contacts_array(){
    try{
    	//nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        var context = nlapiGetContext();
        var environment = context.getEnvironment();
    	if(environment == 'PRODUCTION'){

        	var yesterday = moment().subtract(1, 'days').format('M/D/YYYY');
        	var arrContacts = new Array();
        	var arrAllContacts = new Array();
        	
// modified contacts =================================================================
        	
            var arrColumns = new Array();
        	var arrFilters = new Array();
    		while (true) {
        		searchContacts = nlapiSearchRecord('contact', 'customsearch_clgx_sp_contacts_to_update', arrFilters, arrColumns);
        		if (!searchContacts) {
        			break;
        		}
        		for (var i in searchContacts) {
        			var contactid = parseInt(searchContacts[i].getValue('internalid',null,null));
        			arrContacts.push(contactid);
        		}
        		if (searchContacts.length < 1000) {
        			break;
        		}
        		arrFilters[0] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", contactid);
        	}
        	
// contacts of modified customers =================================================================
        	
            var arrColumns = new Array();
        	var arrFilters = new Array();
    		while (true) {
        		searchCustomers = nlapiSearchRecord('customer', 'customsearch_clgx_sp_customers_to_update', arrFilters, arrColumns);
        		if (!searchCustomers) {
        			break;
        		}
        		for (var i in searchCustomers) {
        			var customerid = parseInt(searchCustomers[i].getValue('internalid',null,null));
        			var contactid = parseInt(searchCustomers[i].getValue('internalid','contact',null));
        			arrContacts.push(contactid);
        		}
        		if (searchCustomers.length < 1000) {
        			break;
        		}
        		arrFilters[0] = new nlobjSearchFilter("internalIdNumber", null, "greaterthanorequalto", customerid);
        	}
        	
// contacts of modified vendors =================================================================
        	
            var arrColumns = new Array();
        	var arrFilters = new Array();
    		while (true) {
        		searchVendors = nlapiSearchRecord('vendor', 'customsearch_clgx_sp_vendors_to_update', arrFilters, arrColumns);
        		if (!searchVendors) {
        			break;
        		}
        		for (var i in searchVendors) {
        			var vendorid = parseInt(searchVendors[i].getValue('internalid',null,null));
        			var contactid = parseInt(searchVendors[i].getValue('internalid','contact',null));
        			arrContacts.push(contactid);
        		}
        		if (searchVendors.length < 1000) {
        			break;
        		}
        		arrFilters[0] = new nlobjSearchFilter("internalIdNumber", null, "greaterthanorequalto", vendorid);
        	}
    		
// contacts of modified partners =================================================================	
    		
            var arrColumns = new Array();
        	var arrFilters = new Array();
    		while (true) {
        		searchPartners = nlapiSearchRecord('partner', 'customsearch_clgx_sp_partners_to_update', arrFilters, arrColumns);
        		if (!searchPartners) {
        			break;
        		}
        		for (var i in searchPartners) {
        			var partnerid = parseInt(searchPartners[i].getValue('internalid',null,null));
        			var contactid = parseInt(searchPartners[i].getValue('internalid','contact',null));
        			arrContacts.push(contactid);
        		}
        		if (searchPartners.length < 1000) {
        			break;
        		}
        		arrFilters[0] = new nlobjSearchFilter("internalIdNumber", null, "greaterthanorequalto", partnerid);
        	}
    		
// create contacts_to_update.json  =================================================================	
    		
    		var arrContacts = _.compact(_.uniq(arrContacts));
    		
    		var file = nlapiCreateFile('ns_contacts_modif.json', 'PLAINTEXT', JSON.stringify(arrContacts));
    		file.setFolder(1909409);
    		var fileid = nlapiSubmitFile(file);
    		
    		var file = nlapiCreateFile('ns_contacts_modif_last.json', 'PLAINTEXT', JSON.stringify(arrContacts));
    		file.setFolder(1909409);
    		var fileid = nlapiSubmitFile(file);
    		
// create all contacts array =================================================================
        	
            var arrColumns = new Array();
        	var arrFilters = new Array();
    		while (true) {
        		searchAllContacts = nlapiSearchRecord('contact', 'customsearch_clgx_sp_sync_contacts_ids', arrFilters, arrColumns);
        		if (!searchAllContacts) {
        			break;
        		}
        		for (var i in searchAllContacts) {
        			var contactid = parseInt(searchAllContacts[i].getValue('internalid',null,null));
        			arrAllContacts.push(contactid);
        		}
        		if (searchAllContacts.length < 1000) {
        			break;
        		}
        		arrFilters[0] = new nlobjSearchFilter("internalIdNumber", null, "greaterthan", contactid);
        	}
    		
    		var arrAllContacts = _.compact(_.uniq(arrAllContacts));
        	
        	var fileNSContactsIDs = nlapiCreateFile('ns_contacts_ids.json', 'PLAINTEXT', JSON.stringify(arrAllContacts));
        	fileNSContactsIDs.setFolder(1909409);
    		nlapiSubmitFile(fileNSContactsIDs);
        	
        	var fileNSContactsIDs = nlapiCreateFile('ns_contacts_queue.json', 'PLAINTEXT', JSON.stringify(arrAllContacts));
        	fileNSContactsIDs.setFolder(1909409);
    		nlapiSubmitFile(fileNSContactsIDs);
    		
    		nlapiSendEmail(432742,71418,'SP 1.2 - Create All Netsuite Contacts Arrays IDs Files','',null,null,null,null,true);
    		nlapiSendEmail(432742,1349020,'SP 1.2 - Create All Netsuite Contacts Arrays IDs Files','',null,null,null,null,true);
			
    	}
        //nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
    	var str = String(error);
        if (str.match('SSS_USAGE_LIMIT_EXCEEDED')) {
        	var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
        	nlapiLogExecution('DEBUG','End', 'Finished processing because of limit exceeded error and reschedule it.');
        }
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}