//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SP_Sync_Modified_Contacts.js
//	Script Name:	CLGX_SS_SP_Sync_Modified_Contacts
//	Script Id:		customscript_clgx_ss_sp_sync_contacts
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		9/24/2015
//-------------------------------------------------------------------------------------------------
function scheduled_sp_sync_contacts(){
    try{
    	//nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        var context = nlapiGetContext();
        var nbrContacts = 0;
        
        // create array of contact ids already in queue
        var arrQueue = new Array();
    	var searchQueue = nlapiLoadSearch('customrecord_clgx_queue_contacts_to_sp', 'customsearch_clgx_sp_merges_to_delete');
		var resultSet = searchQueue.runSearch();
		resultSet.forEachResult(function(searchResult) {
			arrQueue.push(parseInt(searchResult.getValue('custrecord_clgx_contact_to_sp_contact',null,null)));
        	return true;
		});
    	
    	// create array of contacts ids modified yesterday
    	var arrAllContacts = new Array();
		var arrColumns = new Array();
    	var arrFilters = new Array();
    	if(arrQueue.length > 0){
    		arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof", arrQueue)); // only contacts not in queue
    	}
    	var searchAllContacts = nlapiSearchRecord('contact', 'customsearch_clgx_sp_contacts_to_update', arrFilters, arrColumns);
        for ( var i = 0; searchAllContacts != null && i < searchAllContacts.length; i++ ) {
        	arrAllContacts.push(parseInt(searchAllContacts[i].getValue('internalid',null,null)));
        }
        
        if(arrAllContacts.length > 0){
    		var arrContacts = new Array();
            var arrFilters = new Array();
        	if(arrQueue.length > 0){
        		arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof", arrQueue)); // only contacts not in queue
        	}
        	arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof", arrAllContacts));
        	var searchContacts = nlapiLoadSearch('contact', 'customsearch_clgx_sp_sync_contacts_ids');
        	searchContacts.addFilters(arrFilters);
    		var resultSet = searchContacts.runSearch();
    		resultSet.forEachResult(function(searchResult) {
        		var record = nlapiCreateRecord('customrecord_clgx_queue_contacts_to_sp');
    			record.setFieldValue('custrecord_clgx_contact_to_sp_contact', parseInt(searchResult.getValue('internalid',null,null)));
    			record.setFieldValue('custrecord_clgx_contact_to_sp_action', 1);
    			record.setFieldValue('custrecord_clgx_contact_to_sp_done', 'F');
    			var idRec = nlapiSubmitRecord(record, false,true);
    			nbrContacts += 1;
            	return true;
    		});
        }

        var usage = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
        nlapiLogExecution('DEBUG','Usage', 'Usage = '+ usage);
        nlapiSendEmail(432742,71418,'SP 1.4 - Finish adding ' + nbrContacts + ' contacts to queue. Usage (' + usage + ')','',null,null,null,null,true);
        nlapiSendEmail(432742,1349020,'SP 1.4 - Finish adding ' + nbrContacts + ' contacts to queue. Usage (' + usage + ')','',null,null,null,null,true);
        
        //var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_ns2sp', 'customdeploy_clgx_ss_sp_sync_ns2sp');
        
        //nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
    	var str = String(error);
    	if (str.match('SSS_USAGE_LIMIT_EXCEEDED')) {
        	var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
        	nlapiLogExecution('DEBUG','End', 'Finished processing because of limit exceeded error and reschedule it.');
        }
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}