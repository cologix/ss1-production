//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SP_Sync_Create_CSVs.js
//	Script Name:	CLGX_SS_SP_Sync_Create_CSVs
//	Script Id:		customscript_clgx_ss_sp_create_csvs
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		04/16/2015
//-------------------------------------------------------------------------------------------------

function suitelet_ss_sp_create_csvs (request, response){
    try {
    	
    	//nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
    	var context = nlapiGetContext();
    	var environment = context.getEnvironment()
    	
    	if(environment == 'PRODUCTION'){
    	
	        var emailAdminSubject = 'SP 2.1 - Create CSVs on SP, Templates List and Programs List';
	        var emailAdminBody = '';
	        
// ============================ Open Session ===============================================================================================
	    	
	    	var strPost = clgx_sp_login (username,password);
	    	var objHead = clgx_sp_header (strPost.length);
			var requestURL = nlapiRequestURL(strLogURL,strPost,objHead);
			var responseXML = nlapiStringToXML(requestURL.getBody());
			var login = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
			
			if(login == 'true'){
				
				var jsessionid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SESSIONID');
				strURL += jsessionid;
	
				emailAdminBody += 'Login = ' + login + ' | jsessionid = ' + jsessionid + '<br/><br/>';
	
// ============================ Export Lists ===============================================================================================

				var strLists = '[{"listid":3325006,"list":"all","spfile":"","cffile":"sp-contacts-all.csv"},{"listid":3521134,"list":"new","spfile":"","cffile":"sp-contacts-new.csv"},{"listid":3080013,"list":"suppress","spfile":"","cffile":"sp-suppress.csv"}]';
				var arrLists = JSON.parse(strLists);
				
				for ( var i = 0; arrLists != null && i < arrLists.length; i++ ) {
					
					// ====== Build Parameters list =================================
					var objParams = new Object();
					objParams["listid"] = arrLists[i].listid;
					objParams["email"] = '';
					objParams["exptype"] = 'ALL';
					objParams["expformat"] = 'CSV';
					objParams["encode"] = '';
					objParams["add"] = false;
					objParams["start"] = '';
					objParams["end"] = '';
					objParams["created"] = false;
					objParams["leadsource"] = false;
					objParams["dateformat"] = false;
					// ====== Build Columns Fields Array =================================
					
					var arrColumns = new Array();
					if(arrLists[i].listid == 3325006 || arrLists[i].listid == 3521134){
						
						arrColumns.push('RECIPIENT_ID');
						arrColumns.push('Contact Internal ID');
						arrColumns.push('Company Internal ID');
						
						if(arrLists[i].listid == 3521134){
							arrColumns.push('Email');
							arrColumns.push('Company Name');
							arrColumns.push('Contact First name');
							arrColumns.push('Contact Last Name');
							arrColumns.push('Contact Phone');
							arrColumns.push('Title Provided On Webform');
							
							arrColumns.push('Contact Subsidiary');
							arrColumns.push('Company Markets');
							arrColumns.push('Best way to contact you');
							arrColumns.push('Role In Company');
							arrColumns.push('Timeframe of purchase decision');
							arrColumns.push('Data Center Needs');
							arrColumns.push('Type Of Space');
							arrColumns.push('Important Items For Deployment');
							arrColumns.push('Company Provided on Webform');
						}
					}
					if(arrLists[i].listid == 3080013){
						arrColumns.push('RECIPIENT_ID');
						arrColumns.push('Email');
						arrColumns.push('Opt In Date');
						//arrColumns.push('Opt In Details');
						arrColumns.push('Opted Out');
						arrColumns.push('Email Type');
						arrColumns.push('Opted Out Date');
						arrColumns.push('Opt Out Details');
					}
					
					var strPost = clgx_sp_list_export (objParams,arrColumns);
					var objHead = clgx_sp_header (strPost.length);
					var requestURL = nlapiRequestURL(strURL,strPost,objHead);
					var strBody = requestURL.getBody();
					var responseXML = nlapiStringToXML(strBody);
					
					var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
					if(success == 'TRUE'){
						var jobid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/JOB_ID');
						var path = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/FILE_PATH');
						arrLists[i].spfile = path;
					}
					else{
					}
				}

// ======================================= All available programs ===================================================================================================
				
				// ====== Build Parameters list =================================
				var objParams = new Object();
				objParams["active"] = 'true';
				objParams["inactive"] = 'true';
				objParams["begin"] = '';
				objParams["end"] = '';
				objParams["listid"] = '3325006';
				objParams["sales"] = '';
				// ====== Build Columns Tags Array =================================
				var arrTags = new Array();

				var strPost = clgx_sp_programs (objParams, arrTags);
		    	var objHead = clgx_sp_header (strPost.length);
				var requestURL = nlapiRequestURL(strURL,strPost,objHead);
				var strBody = requestURL.getBody();
				var responseXML = nlapiStringToXML(strBody);

				var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
				if(success == 'TRUE'){
					
					var xmlPrograms = nlapiSelectNodes(responseXML, '/Envelope/Body/RESULT/PROGRAMS/PROGRAM');
					var arrPrograms = new Array();
					for ( var i = 0; i < xmlPrograms.length; i++ ) {
						
						var objProgram = new Object();
						objProgram["programid"] = parseInt(nlapiSelectValue(xmlPrograms[i], 'ID'));
						objProgram["program"] = nlapiSelectValue(xmlPrograms[i], 'NAME');
						objProgram["listid"] = parseInt(nlapiSelectValue(xmlPrograms[i], 'LIST_ID'));
						objProgram["status"] = nlapiSelectValue(xmlPrograms[i], 'STATE');
						objProgram["timezone"] = nlapiSelectValue(xmlPrograms[i], 'TIME_ZONE');
						objProgram["created"] = nlapiSelectValue(xmlPrograms[i], 'CREATED');
						objProgram["modified"] = nlapiSelectValue(xmlPrograms[i], 'LAST_MODIFIED');
						objProgram["notes"] = nlapiSelectValue(xmlPrograms[i], 'NOTES');
						objProgram["start"] = nlapiSelectValue(xmlPrograms[i], 'START_DATE');
						objProgram["end"] = nlapiSelectValue(xmlPrograms[i], 'END_DATE');
						objProgram["lastdate"] = nlapiSelectValue(xmlPrograms[i], 'LAST_CONTACT_DATE');
						arrPrograms.push(objProgram);
					}
				}

				var str = JSON.stringify(arrPrograms);
				var fileName = nlapiCreateFile('sp_programs.json', 'PLAINTEXT', str);
				fileName.setFolder(1909409);
				nlapiSubmitFile(fileName);

				var str = JSON.stringify(arrPrograms);
				var fileName = nlapiCreateFile('ns_programs.json', 'PLAINTEXT', str);
				fileName.setFolder(1909409);
				nlapiSubmitFile(fileName);
				
// ======================================= All (netsuite) available templates ===================================================================================================
				
				// ====== Build Parameters list =================================
				var objParams = new Object();
				objParams["visibility"] = 1;
				//objParams["crm"] = 'true';
				objParams["crm"] = '';
				objParams["start"] = '';
				objParams["end"] = '';
				
				var strPost = clgx_sp_templates (objParams);
		    	var objHead = clgx_sp_header (strPost.length);
				var requestURL = nlapiRequestURL(strURL,strPost,objHead);
				var strBody = requestURL.getBody();
				var responseXML = nlapiStringToXML(strBody);

				var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
				if(success == 'TRUE'){
					
					var xmlTemplates = nlapiSelectNodes(responseXML, '/Envelope/Body/RESULT/MAILING_TEMPLATE');
					var arrAllTemplates = new Array();
					for ( var i = 0; i < xmlTemplates.length; i++ ) {
						
						var objTemplate = new Object();
						objTemplate["mailingid"] = parseInt(nlapiSelectValue(xmlTemplates[i], 'MAILING_ID'));
						objTemplate["mailing"] = nlapiSelectValue(xmlTemplates[i], 'MAILING_NAME');
						objTemplate["subject"] = nlapiSelectValue(xmlTemplates[i], 'SUBJECT');
						objTemplate["modified"] = nlapiSelectValue(xmlTemplates[i], 'LAST_MODIFIED');
						objTemplate["backup"] = nlapiSelectValue(xmlTemplates[i], 'FLAGGED_FOR_BACKUP');
						arrAllTemplates.push(objTemplate);
					}
					
				}

				var str = JSON.stringify(arrAllTemplates);
				var fileName = nlapiCreateFile('sp_templates.json', 'PLAINTEXT', str);
				fileName.setFolder(1909409);
				nlapiSubmitFile(fileName);
				
// ============================ Close Session ===============================================================================================
				
		    	var strPost =  clgx_sp_logout ();
				var objHead = clgx_sp_header (strPost.length);
				var requestURL = nlapiRequestURL(strURL,strPost,objHead);
				var strBody = requestURL.getBody();
				var responseXML = nlapiStringToXML(strBody);
				var logout = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
				
				emailAdminBody += 'Logout = ' + logout + '<br/><br/>';
		
				var strLists = JSON.stringify(arrLists);
				var fileName = nlapiCreateFile('sp_ftp_last_files_names.json', 'PLAINTEXT', strLists);
				fileName.setFolder(1909409);
				nlapiSubmitFile(fileName);
				
				emailAdminBody += 'strLists = ' + strLists + '<br/><br/>';
				
				var url = ('https://command1.cologix.com:10313/netsuite/silverpop/set_sp_ftp_last_files_names.cfm?json=' + strLists).replace(/ /g, '_');
				var requestURLCF = nlapiRequestURL(url);
	 			var successCF = requestURLCF.body;
	 			
	 			emailAdminBody += 'successCF = ' + successCF + '<br/><br/>';
	 			
			}
			
			nlapiSendEmail(432742,71418,emailAdminSubject,emailAdminBody,null,null,null,null,true);
			nlapiSendEmail(432742,179749,emailAdminSubject,emailAdminBody,null,null,null,null,true);
			nlapiSendEmail(432742,1349020,emailAdminSubject,emailAdminBody,null,null,null,null,true);
			//var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_ns2ns', 'customdeploy_clgx_ss_sp_sync_ns2ns');
		}
	    nlapiLogExecution('DEBUG','Finish Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 

    }
    
catch (error) { 
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
