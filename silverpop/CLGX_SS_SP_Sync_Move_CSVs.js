//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SP_Sync_Move_CSVs.js
//	Script Name:	CLGX_SS_SP_Sync_Move_CSVs
//	Script Id:		customscript_clgx_ss_sp_move_csvs
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		04/16/2015
//-------------------------------------------------------------------------------------------------

function suitelet_ss_sp_move_csvs (request, response){
    try {
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
    	var context = nlapiGetContext();
        var environment = context.getEnvironment();
    	
    	if(environment == 'PRODUCTION'){
// ======= Trigger CF script to move files from SilverPOP to CF =============================================================================

	    	var requestURLFTP = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/silverpop/ftp.cfm');
			if((requestURLFTP.body).indexOf("ftp_success") > -1){
				
				var objFile = nlapiLoadFile(2404555);
				var arrFiles = JSON.parse(objFile.getValue());
				for ( var i = 0; arrFiles != null && i < arrFiles.length; i++ ) {
			    	var url = 'https://command1.cologix.com:10313/netsuite/silverpop/get_csv.cfm?csv=' + arrFiles[i].cffile;
					var requestURL = nlapiRequestURL(url);
					var strResponse = requestURL.body;
					
					var csv = nlapiCreateFile(arrFiles[i].cffile, 'PLAINTEXT', strResponse);
					csv.setFolder(1909409);
					nlapiSubmitFile(csv);
				}
				nlapiSendEmail(432742,71418,'SP 2.2 - Move CSVs',requestURLFTP.body,null,null,null,null,true);
				nlapiSendEmail(432742,179749,'SP 2.2 - Move CSVs',requestURLFTP.body,null,null,null,null,true);
				nlapiSendEmail(432742,1349020,'SP 2.2 - Move CSVs',requestURLFTP.body,null,null,null,null,true);
				
				
// ======= Create SilverPOP Contacts IDs Array File  ================================================================================================
			
				// load SilverPOP sp-contacts-all.csv
				var objFile = nlapiLoadFile(2410386);
				var csv = (objFile.getValue()).replace(/\"/g,"");
		    	var arrSP = [];
				var lines=csv.split("\n");
				
				for(var i=1; i < lines.length; i++){
					var currentline=lines[i].split(",");
					arrSP.push(parseInt(currentline[1]));
				}
				var arrSP = _.compact(_.uniq(arrSP));
				
		    	var fileSPContactsIDs = nlapiCreateFile('sp_contacts_ids.json', 'PLAINTEXT', JSON.stringify(arrSP));
		    	fileSPContactsIDs.setFolder(1909409);
				nlapiSubmitFile(fileSPContactsIDs);

// ======= Create Intersection and Differences between NS & SP  ================================================================================================
			
				// load ns_contacts_ids.json
				var objFile = nlapiLoadFile(2415532);
				var arrNS = JSON.parse(objFile.getValue());
				
				var arrDiff = _.difference(arrNS, arrSP);
		    	var file = nlapiCreateFile('sp_contacts_ns_not_sp.json', 'PLAINTEXT', JSON.stringify(arrDiff));
		    	file.setFolder(1909409);
				nlapiSubmitFile(file);
				
				// add to update queue all contacts that exist in Netsuite but not on SilverPOP as updates
				var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_ns_not_sp', 'customdeploy_clgx_ss_sp_sync_ns_not_sp');
				
				var arrDiff = _.difference(arrSP, arrNS);
		    	var file = nlapiCreateFile('sp_contacts_sp_not_ns.json', 'PLAINTEXT', JSON.stringify(arrDiff));
		    	file.setFolder(1909409);
				nlapiSubmitFile(file);
				
				// add to update queue all contacts that exist in SilverPOP but not on Netsuite as deletes
				var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_sp_not_ns', 'customdeploy_clgx_ss_sp_sync_sp_not_ns');
				
				var arrBoth = _.intersection(arrNS, arrSP);
		    	var file = nlapiCreateFile('sp_contacts_ns_and_sp.json', 'PLAINTEXT', JSON.stringify(arrBoth));
		    	file.setFolder(1909409);
				nlapiSubmitFile(file);
				// not sure what to do with that, don't think is relevant
				
				nlapiSendEmail(432742,71418,'SP 2.3 - Array Files created','',null,null,null,null,true);
				nlapiSendEmail(432742,1349020,'SP 2.3 - Array Files created','',null,null,null,null,true);
			}
			else{
				nlapiSendEmail(432742,71418,'SP 2.2 - Error on Move CSVs',requestURLFTP.body,null,null,null,null,true);
				nlapiSendEmail(432742,1349020,'SP 2.2 - Error on Move CSVs',requestURLFTP.body,null,null,null,null,true);
			}
			
			var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_sp2ns', 'customdeploy_clgx_ss_sp_sync_sp2ns');
			//var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_ns2sp', 'customdeploy_clgx_ss_sp_sync_ns2sp');
			
			var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
			nlapiLogExecution('DEBUG', 'Usage', '| Usage = '+ usageConsumtion + '  |');
    	}
	    nlapiLogExecution('DEBUG','Finish Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }

catch (error) { 
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}