//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SP_Sync_NS_not_SP.js
//	Script Name:	CLGX_SS_SP_Sync_NS_not_SP
//	Script Id:		customscript_clgx_ss_sp_sync_ns_not_sp
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/20/2015
//-------------------------------------------------------------------------------------------------
function scheduled_sp_sync_ns_not_sp(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        var context = nlapiGetContext();
       
		var objFile = nlapiLoadFile(2415836);
		var arrContacts = JSON.parse(objFile.getValue());
		var arrDiff = JSON.parse(objFile.getValue());
		var arrDiffNew = JSON.parse(objFile.getValue());
		
		// add to update queue all contacts that exist in SilverPOP but not on Netsuite as deletes
		if(arrDiff.length > 0){
			
			if(arrDiff.length > 500){
		        var loopndx = 500;
			}
			else{
				var loopndx = arrDiff.length;
			}

		    for ( var i = 0; arrDiff != null && i < loopndx; i++ ) {
				
				var arrColumns = new Array();
		    	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
		    	var arrFilters = new Array();
		    	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_contact_to_sp_contact",null,"equalto",arrDiff[i]));
		    	arrFilters.push(new nlobjSearchFilter('custrecord_clgx_contact_to_sp_done',null,'is','F'));
		    	var searchQueue = nlapiSearchRecord('customrecord_clgx_queue_contacts_to_sp', null, arrFilters, arrColumns);
		    	
	    		if(searchQueue == null){ // add contact to queue if not in it
		    		var record = nlapiCreateRecord('customrecord_clgx_queue_contacts_to_sp');
					record.setFieldValue('custrecord_clgx_contact_to_sp_contact', arrDiff[i]);
					record.setFieldValue('custrecord_clgx_contact_to_sp_action', 2);
					record.setFieldValue('custrecord_clgx_contact_to_sp_done', 'F');
					var idRec = nlapiSubmitRecord(record, false,true);
	    		}
				
				var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
	            var index = i + 1;
	            nlapiLogExecution('DEBUG','Results ', ' | Index = ' + index + ' of ' + arrDiff.length + ' | Usage = '+ usageConsumtion + '  | ContactID = ' + arrDiff[i] + ' |');
			
	            arrDiffNew = _.reject(arrDiffNew, function(num){
			        return (num == arrDiff[i]);
				});
		    
		    }
			
			var file = nlapiCreateFile('sp_contacts_ns_not_sp.json', 'PLAINTEXT', JSON.stringify(arrDiffNew));
	    	file.setFolder(1909409);
			nlapiSubmitFile(file);
			
			var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
			
		}
		else{
			
			nlapiSendEmail(432742,71418,'SP 2.3 - Finish NS not SP','',null,null,null,null,true);
			nlapiSendEmail(432742,1349020,'SP 2.3 - Finish NS not SP','',null,null,null,null,true);
			
			//var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_ns2sp', 'customdeploy_clgx_ss_sp_sync_ns2sp');
		}	
	
		nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}