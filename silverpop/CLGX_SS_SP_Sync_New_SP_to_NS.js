//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SP_Sync_New_SP_to_NS.js
//	Script Name:	CLGX_SS_SP_Sync_New_SP_to_NS
//	Script Id:		customscript_clgx_ss_sp_sync_new_sp2ns
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------

function scheduled_sp_sync_new_sp2ns (request, response){
    try {

    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
    	var context = nlapiGetContext();
    	
    	var objFile = nlapiLoadFile(2410387);
		var csv = (objFile.getValue()).replace(/\"/g,"");
		var arrCSV = JSON.parse(csvJSON(csv));
        var arrContacts = _.filter(arrCSV, function(arr){
            return (arr.recipientid != '');
        });

    	// john.test@yahoo.ca
        if(arrContacts != null){
        	
            var emailAdminSubject = 'SP 2.4 - Create New Contacts from SP to NS';
            var emailAdminBody = '';
		    emailAdminBody += '<table border="1" cellpadding="5">';
		    emailAdminBody += '<tr><td>ContactID</td><td>RecipientID</td><td>Success</td></tr>';
            
// ============================ Open Session ===============================================================================================
        	
        	var strPost = clgx_sp_login (username,password);
        	var objHead = clgx_sp_header (strPost.length);
    		var requestURL = nlapiRequestURL(strLogURL,strPost,objHead);
    		var responseXML = nlapiStringToXML(requestURL.getBody());
    		var login = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
    		
    		if(login == 'true'){
    			
    			var jsessionid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SESSIONID');
    			strURL += jsessionid;

    			//emailAdminBody += 'Login = ' + login + ' | jsessionid = ' + jsessionid + '<br/><br/>';

    			for ( var i = 0; arrContacts != null && i < arrContacts.length; i++ ) {
        			
    				var customer = replace_chars(arrContacts[i].customer);
    				var firstname = replace_chars(arrContacts[i].firstname);
    				var lastname = replace_chars(arrContacts[i].lastname);
    				var email = replace_chars(arrContacts[i].email);
    				var title = replace_chars(arrContacts[i].title);
    				var phone = replace_chars(arrContacts[i].phone);
    				var recipientid = replace_chars(arrContacts[i].recipientid);
    				
                    var comments = 'New Contact from SilverPOP\n\n' +
                    'Company Name =  ' + customer + '\n' +
                    'Phone =  ' + phone + '\n\n';
                    
                    if(firstname != '' || lastname != ''){
        	            var record = nlapiCreateRecord('contact');
        	            record.setFieldValue('entityid', firstname + ' ' + lastname);
        				record.setFieldValue('firstname', firstname);
        				record.setFieldValue('lastname', lastname);
        	            record.setFieldValue('email', email);
        				record.setFieldValue('title', title);
        				record.setFieldValue('externalid', recipientid);
        				record.setFieldValue('custentity_clgx_sp_recipients', '[{"customerid":"","recipientid":"' + recipientid + '"}]');
        				record.setFieldValue('comments', comments);
        				var contactid = nlapiSubmitRecord(record, false, true);
        				
// ============================ Update Contact ID on Recipient ID ===============================================================================================
        				
        				// ====== Build Parameters list =================================
						var objParams = new Object();
						objParams["listid"] = '3325006';
						objParams["from"] = 0;
						objParams["update"] = true;
						// ====== Build Sync Fields Array =================================
						var arrSync = new Array();
						arrSync.push(JSON.parse('{"name": "RECIPIENT_ID", "value": "' + recipientid + '"}'));
						// ====== Build Columns Fields Array =================================
						var arrColumns = new Array();
						arrColumns.push(JSON.parse('{"name": "Contact Internal ID", "value": "' + contactid + '"}'));
						
						var strPost = clgx_sp_create_recipient (objParams,arrSync,arrColumns);
						var objHead = clgx_sp_header (strPost.length);
						var requestURL = nlapiRequestURL(strURL,strPost,objHead);
						var strBody = requestURL.getBody();
						var responseXML = nlapiStringToXML(strBody);
						
						var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
						var returnedid = '';
						if(success == 'TRUE'){
							returnedid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/RecipientId');
							nlapiLogExecution('DEBUG','Recipient ID', ' | Contact ID = ' + contactid + ' | Recipient ID = ' + recipientid + '  |');
						}
						else{
							nlapiLogExecution('DEBUG','Recipient ID', ' | No returned for recipientid = ' + recipientid  + ' |');
						}
						emailAdminBody += '<tr><td>' + contactid + '</td><td>' + recipientid + '</td><td>' + success + '</td></tr>';
        			}
        		}

// ============================ Close Session ===============================================================================================
    			
    	    	var strPost =  clgx_sp_logout ();
    			var objHead = clgx_sp_header (strPost.length);
    			var requestURL = nlapiRequestURL(strURL,strPost,objHead);
    			var strBody = requestURL.getBody();
    			var responseXML = nlapiStringToXML(strBody);
    			var logout = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
    			
    			//emailAdminBody += 'Logout = ' + logout + '<br/><br/>';
        	}
    		emailAdminBody += '</table>';
        }
        else{
            var emailAdminSubject = 'SP 2.4 - No New Contacts from SP to NS';
            var emailAdminBody = '';
        }

        nlapiSendEmail(432742,71418,emailAdminSubject,emailAdminBody,null,null,null,null,true);
        nlapiSendEmail(432742,1349020,emailAdminSubject,emailAdminBody,null,null,null,null,true);
        
        //var status = nlapiScheduleScript('customscript_clgx_ss_sp_sync_ns2sp', 'customdeploy_clgx_ss_sp_sync_ns2sp');
		
		var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
		nlapiLogExecution('DEBUG', 'Usage', '| Usage = '+ usageConsumtion + '  |');
	    nlapiLogExecution('DEBUG','Finish Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 

    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


//var csv is the CSV file with headers
function csvJSON(csv){
 
  var lines=csv.split("\n");
  var result = [];
  var headers=["recipientid","contactid","customerid","email","customer","firstname","lastname","phone","title"];
 
  for(var i=1;i<lines.length;i++){
 
	  var obj = {};
	  var currentline=lines[i].split(",");
 
	  for(var j=0;j<headers.length;j++){
		  obj[headers[j]] = currentline[j];
	  }
	  result.push(obj);
  }
  return JSON.stringify(result);
}


function replace_chars (str){
	str = str.replace(/\,/g," ");
	str = str.replace(/\"/g,"");
	str = str.replace(/\'/g,"");
	str = str.replace(/\;/g,"");
	str = str.replace(/\:/g,"");
	str = str.replace(/\s{2,}/g, ' ');
	str = str.replace(/\t/g, ' ');
	str = str.toString().trim().replace(/(\r\n|\n|\r)/g,"");
  return str;
}
