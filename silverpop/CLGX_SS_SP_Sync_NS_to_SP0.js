//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SP_Sync_NS_to_SP0.js
//	Script Name:	CLGX_SS_SP_Sync_NS_to_SP0
//	Script Id:		customscript_clgx_ss_sp_sync_ns2sp0
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Various
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------
function scheduled_sp_sync_ns2sp0(){
    try{
    	
    	nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var context = nlapiGetContext();
        var environment = context.getEnvironment();
    	
    	if(environment == 'PRODUCTION'){
        
	        var objFile = nlapiLoadFile(3404116);
			var arrContacts = JSON.parse(objFile.getValue());
			var arrNewContacts = JSON.parse(objFile.getValue());
		
			if(arrContacts.length > 0){
			
				if(arrContacts.length > 100){
					var loopndx = 100;
				}
				else{
					var loopndx = arrContacts.length;
				}
	
// ============================ Open Session ===============================================================================================
	        
		    	var strPost = clgx_sp_login (username,password);
		    	var objHead = clgx_sp_header (strPost.length);
				var requestURL = nlapiRequestURL(strLogURL,strPost,objHead);
				var responseXML = nlapiStringToXML(requestURL.getBody());
				var login = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
				
				if(login == 'true'){
					
					var jsessionid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SESSIONID');
					strURL += jsessionid;
					
					for ( var j = 0; arrContacts != null && j < loopndx; j++ ) {
	
			            var contactid = arrContacts[j];
			            
			            var strError = '';
		        		
			            var arrColumns = new Array();
						arrColumns.push(new nlobjSearchColumn('internalid',null,null).setSort(true));
						var arrFilters = new Array();
						arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",contactid));
						var searchContact = nlapiSearchRecord('contact', null, arrFilters, arrColumns);
						
						if(searchContact != null){
	        			
		        			var recContact = nlapiLoadRecord ('contact',contactid);
				            
				            var headcustomerid = parseInt(recContact.getFieldValue('company'));
				            var externalid = recContact.getFieldValue('externalid');
				            var strJSON = recContact.getFieldValue('custentity_clgx_sp_recipients');
				            
				            var arrJSON = new Array();
				            if(strJSON != '' && strJSON != null){
				            	arrJSON = JSON.parse(strJSON);
				            }
				            var arrReturned = new Array();
				            
				            var strOldRecipients = recContact.getFieldValue('custentity_clgx_sp_recipients');
							if(strOldRecipients != null && strOldRecipients != ''){
								var arrOldRecipients = JSON.parse(recContact.getFieldValue('custentity_clgx_sp_recipients'));
							}
							else{
								var arrOldRecipients = [];
							}
				            
					    	var arrNewRecipients = new Array();
				        	var arrColumns = new Array();
				        	var arrFilters = new Array();
				        	arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",contactid));
				        	var searchRecipients = nlapiSearchRecord('contact', 'customsearch_clgx_sp_sync_contacts', arrFilters, arrColumns);
				        	
				        	if(searchRecipients != null){
				            	
								for ( var i = 0; searchRecipients != null && i < searchRecipients.length; i++ ) {
									
									var searchRecipient = searchRecipients[i];
					        		var customerid = searchRecipient.getValue('internalid','company',null);
					        		
					        		if(customerid != null && customerid != ''){
					        			
					        			var iscustomer = searchRecipient.getValue('internalid','customer',null);
					        			if(iscustomer != null && iscustomer != ''){
					        				var recCustomer = nlapiLoadRecord ('customer',customerid);
					        				var custstatus = recCustomer.getFieldText('entitystatus');
					        			}
					        			else{
					        				var custstatus = '';
					        			}
					        			
						    			var objNewRecipient = new Object();
						    			objNewRecipient["customerid"] = parseInt(customerid);
						    			objNewRecipient["contactid"] = parseInt(searchRecipient.getValue('internalid',null,null));
							    		arrNewRecipients.push(objNewRecipient);
							    		
						        		var objRecipient = new Object();
						        		objRecipient["contactid"] = parseInt(searchRecipient.getValue('internalid',null,null));
						        		objRecipient["firstname"] = replace_chars(searchRecipient.getValue('firstname',null,null));
						        		objRecipient["lastname"] = replace_chars(searchRecipient.getValue('lastname',null,null));
						        		objRecipient["name"] = replace_chars(searchRecipient.getValue('entityid',null,null));
						        		objRecipient["email"] = replace_chars(searchRecipient.getValue('email',null,null));
						        		objRecipient["phone"] = replace_chars(searchRecipient.getValue('phone',null,null));
						        		objRecipient["role"] = searchRecipient.getText('role',null,null);
						        		objRecipient["contactsubsidiary"] = searchRecipient.getText('subsidiarynohierarchy',null,null);
						        		
						        		var created = searchRecipient.getValue('datecreated',null,null);
						        		objRecipient["created"] = moment(created).format('M/D/YYYY');
						        		objRecipient["optout"] = searchRecipient.getText('globalsubscriptionstatus',null,null);
						        		
						        		var objRec = _.find(arrJSON, function(arr){ return (arr.customerid == customerid); });
					        			if(objRec != null){
					        				objRecipient["recipientid"] = objRec.recipientid;
					        			}
					        			else{
					        				if(arrJSON.length > 0){
						        				if(arrJSON[0].recipientid != '' && arrJSON[0].customerid == ''){
						        					objRecipient["recipientid"] = arrJSON[0].recipientid;
						        				}
						        				else{
						        					objRecipient["recipientid"] = '';
						        				}
					        				}
					        				else{
					        					objRecipient["recipientid"] = '';
					        				}
					        			}
					        			
					        			objRecipient["customerid"] = parseInt(customerid);
					        			objRecipient["customer"] = replace_chars(searchRecipient.getValue('companyname','company',null));
					        			objRecipient["customersubsidiary"] = searchRecipient.getText('subsidiarynohierarchy','company',null);
					        			objRecipient["type"] = replace_chars(searchRecipient.getValue('type','company',null));
					        			objRecipient["became"] = searchRecipient.getValue('custentity_clgx_date_became_customer','company',null);
					        			objRecipient["target"] = searchRecipient.getValue('custentity_clgx_target_account','company',null);
					        			
					        			objRecipient["firstorder"] = searchRecipient.getValue('firstorderdate','customer',null);
					        			objRecipient["firstsale"] = searchRecipient.getValue('firstsaledate','customer',null);
					        			//objRecipient["status"] = searchRecipient.getText('entitystatus','customer',null);
					        			objRecipient["status"] = custstatus;
					        			objRecipient["category"] = searchRecipient.getText('category','customer',null);
					        			objRecipient["categories"] = (searchRecipient.getText('custentity_clgx_addtinl_cat','customer',null)).replace(/,/g, ';');
					        			objRecipient["markets"] = (searchRecipient.getText('custentity_clgx_lead_market','customer',null)).replace(/,/g, ';');
					        			objRecipient["leadsource"] = searchRecipient.getText('leadsource','customer',null);
					        			
					        			var repid = searchRecipient.getValue('salesrep','customer',null);
					        			if(repid != null && repid != ''){
					        				var recRep = nlapiLoadRecord ('employee',repid);
					        				objRecipient["rep"] = recRep.getFieldValue('entityid');
					        				objRecipient["repemail"] = recRep.getFieldValue('email');
					        				objRecipient["repphone"] = recRep.getFieldValue('phone');
					        				objRecipient["repsupervisor"] = recRep.getFieldText('supervisor');
					        			}
					        			else{
					        				objRecipient["rep"] = '';
					        				objRecipient["repemail"] = '';
					        				objRecipient["repphone"] = '';
					        				objRecipient["repsupervisor"] = '';
					        			}
					        		
	
						        		
						        		// ====== Build Parameters list =================================
										var objParams = new Object();
										objParams["listid"] = '3325006';
										objParams["from"] = 0;
										objParams["update"] = true;
										// ====== Build Sync Fields Array =================================
										var arrSync = new Array();
										if(objRecipient.recipientid != ''){
											arrSync.push(JSON.parse('{"name": "RECIPIENT_ID", "value": "' + objRecipient.recipientid + '"}'));
										}
										else{
											arrSync.push(JSON.parse('{"name": "Company Internal ID", "value": "' + objRecipient.customerid + '"}'));
											arrSync.push(JSON.parse('{"name": "Contact Internal ID", "value": "' + objRecipient.contactid + '"}'));
										}
										// ====== Build Columns Fields Array =================================
										var arrColumns = new Array();
										
										arrColumns.push(JSON.parse('{"name": "Company Internal ID", "value": "' + objRecipient.customerid + '"}'));
										arrColumns.push(JSON.parse('{"name": "Company Name", "value": "' + objRecipient.customer + '"}'));
										arrColumns.push(JSON.parse('{"name": "Company Primary Segment", "value": "' + objRecipient.category + '"}'));
										arrColumns.push(JSON.parse('{"name": "Company Secondary Segment", "value": "' + objRecipient.categories + '"}'));
										arrColumns.push(JSON.parse('{"name": "Company Markets", "value": "' + objRecipient.markets + '"}'));
										arrColumns.push(JSON.parse('{"name": "Company Lead Source", "value": "' + objRecipient.leadsource + '"}'));
										arrColumns.push(JSON.parse('{"name": "Company Status", "value": "' + objRecipient.status + '"}'));
										arrColumns.push(JSON.parse('{"name": "Company Subsidiary", "value": "' + objRecipient.customersubsidiary + '"}'));
										arrColumns.push(JSON.parse('{"name": "Company Type", "value": "' + objRecipient.type + '"}'));
										arrColumns.push(JSON.parse('{"name": "Date Became Customer", "value": "' + objRecipient.became + '"}'));
										arrColumns.push(JSON.parse('{"name": "Target Account", "value": "' + objRecipient.target + '"}'));
										
										arrColumns.push(JSON.parse('{"name": "Contact First Name", "value": "' + objRecipient.firstname + '"}'));
										arrColumns.push(JSON.parse('{"name": "Contact Internal ID", "value": "' + objRecipient.contactid + '"}'));
										arrColumns.push(JSON.parse('{"name": "Contact Last Name", "value": "' + objRecipient.lastname + '"}'));
										arrColumns.push(JSON.parse('{"name": "Contact Name", "value": "' + objRecipient.name + '"}'));
										arrColumns.push(JSON.parse('{"name": "Email", "value": "' + objRecipient.email + '"}'));
										arrColumns.push(JSON.parse('{"name": "Contact Phone", "value": "' + objRecipient.phone + '"}'));
										arrColumns.push(JSON.parse('{"name": "Contact Role", "value": "' + objRecipient.role + '"}'));
										arrColumns.push(JSON.parse('{"name": "Contact Subsidiary", "value": "' + objRecipient.contactsubsidiary + '"}'));
										
										arrColumns.push(JSON.parse('{"name": "Date Created", "value": "' + objRecipient.created + '"}'));
										arrColumns.push(JSON.parse('{"name": "Date of First Order", "value": "' + objRecipient.firstorder + '"}'));
										arrColumns.push(JSON.parse('{"name": "Date of First Sale", "value": "' + objRecipient.firstsale + '"}'));
										
										arrColumns.push(JSON.parse('{"name": "Global Subscription Status", "value": "' + objRecipient.optout + '"}'));
										
										arrColumns.push(JSON.parse('{"name": "Sales Rep", "value": "' + objRecipient.rep + '"}'));
										arrColumns.push(JSON.parse('{"name": "Sales Rep Email", "value": "' + objRecipient.repemail + '"}'));
										arrColumns.push(JSON.parse('{"name": "Sales Rep Phone", "value": "' + objRecipient.repphone + '"}'));
										arrColumns.push(JSON.parse('{"name": "Sales Rep Supervisor", "value": "' + objRecipient.repsupervisor + '"}'));
										
										var strPost = clgx_sp_recipient_create (objParams,arrSync,arrColumns);
										
										var objHead = clgx_sp_header (strPost.length);
										var requestURL = nlapiRequestURL(strURL,strPost,objHead);
										var strBody = requestURL.getBody();
										var responseXML = nlapiStringToXML(strBody);
										
										var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
										var returnedid = '';
										if(success == 'TRUE'){
											returnedid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/RecipientId');
											
											var objReturned = new Object();
											objReturned["customerid"] = objRecipient.customerid;
											objReturned["recipientid"] = returnedid;
											arrReturned.push(objReturned); 
											
										}
					
// if employee add to the employee list  ====================================================================================================================================
						        		
						        		if((objRecipient.email).indexOf("cologix.com") > -1 && returnedid != ''){
					
						        			// ====== Build Parameters list =================================
											var objParams = new Object();
											objParams["listid"] = '3482555';
											objParams["recipientid"] = returnedid;
											// ====== Build Columns Fields Array =================================
											var arrColumns = new Array();
						
											var strPost = clgx_sp_recipient_add_to_list (objParams, arrColumns);
											var objHead = clgx_sp_header (strPost.length);
											var requestURL = nlapiRequestURL(strURL,strPost,objHead);
											var strBody = requestURL.getBody();
											var responseXML = nlapiStringToXML(strBody);
											
											var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');

						        		}
						        		
						        	}
					        	}
						
					        	var objRet = _.find(arrReturned, function(arr){ return (arr.customerid == headcustomerid); });
				        		if(objRet != null){
				        			recContact.setFieldValue('externalid', objRet.recipientid);
				        		}
				        		
				        		recContact.setFieldValue('custentity_clgx_sp_recipients', JSON.stringify(arrReturned));
				        		var idRecContact = nlapiSubmitRecord(recContact, false, true);
				        	}
	        			}
			       
				    	arrNewContacts = _.reject(arrNewContacts, function(num){
					        return (num == contactid);
						});
				    	
			        	var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
			            var index = j + 1;
			            nlapiLogExecution('DEBUG','Results ', ' | Index = ' + index + ' of ' + arrContacts.length + ' | Usage = '+ usageConsumtion + '  |');
					}
	
// ============================ Close Session ===============================================================================================
				
			    	var strPost =  clgx_sp_logout ();
					var objHead = clgx_sp_header (strPost.length);
					var requestURL = nlapiRequestURL(strURL,strPost,objHead);
					var strBody = requestURL.getBody();
					var responseXML = nlapiStringToXML(strBody);
					var logout = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');

					var file = nlapiCreateFile('ns_contacts_modif.json', 'PLAINTEXT', JSON.stringify(arrNewContacts));
		    		file.setFolder(1909409);
		    		var fileid = nlapiSubmitFile(file);
		    		
					var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
				}
			}
			else{
				nlapiSendEmail(432742,71418,'SP 1.3 - Finish NS to SP 0','',null,null,null,null,true);
				nlapiSendEmail(432742,1349020,'SP 1.3 - Finish NS to SP 0','',null,null,null,null,true);
			}
    	}
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Finish Scheduled Script--------------------------|'); 
    }
    catch (error){
    	
    	var str = String(error);
        if (str.match('SSS_REQUEST_TIME_EXCEEDED') || str.match('SSS_CONNECTION_TIME_OUT') || str.match('SSS_CONNECTION_CLOSED')) {
        	nlapiSendEmail(432742,71418, str + ' - Finished processing NS 2 SP because of communication error and reschedule it','',null,null,null,null,true);
        	nlapiSendEmail(432742,1349020, str + ' - Finished processing NS 2 SP because of communication error and reschedule it','',null,null,null,null,true);
        	var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
        	nlapiLogExecution('DEBUG','End', 'Finished ns2sp processing because of communication error and reschedule it.');
        }
        
    	if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function replace_chars (str){
	str = str.replace(/\,/g," ");
	str = str.replace(/\&/g," and ");
	str = str.replace(/\"/g,"");
	str = str.replace(/\'/g,"");
	str = str.replace(/\;/g,"");
	str = str.replace(/\:/g,"");
	str = str.replace(/\s{2,}/g, ' ');
	str = str.replace(/\t/g, ' ');
	str = str.toString().trim().replace(/(\r\n|\n|\r)/g,"");
  return str;
}