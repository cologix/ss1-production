nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Consumption_Invoice.js
//	Script Name:	CLGX_SU_Consumption_Invoice
//	Script Id:		customscript_clgx_su_consumption_invoice  
//	Script Runs:	On Server
//	Script Type:	User Event
//	Deployments:	Virtual Consolidated Invoice
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		12/2/2014
//-------------------------------------------------------------------------------------------------
function beforeLoad(type, form) {
	try {
		var currentContext = nlapiGetContext();
		if (currentContext.getExecutionContext() == 'userinterface' && type == 'create') {
			
			var roleid = nlapiGetRole();
			var closed = 0;
			if(closed == 1 && (roleid != -5 && roleid != 3 && roleid != 18)){
				var arrParam = new Array();
				arrParam['custscript_internal_message'] = 'This module is closed for modifications. Sorry for the inconvenience. Please come back later.';
				nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
			}
	
			var arrCustomers = new Array();
			
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('parent','custrecord_clgx_dcim_points_day_service','GROUP').setSort(false));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custbody_clgx_pwusg_meter_power_usage","custrecord_clgx_dcim_points_day_so",'is','T'));
			var searchCustomers = nlapiSearchRecord('customrecord_clgx_dcim_points_day', null, arrFilters, arrColumns);

		    for (var i = 0; searchCustomers != null && i < searchCustomers.length; i++) {
		    	arrCustomers[i] = searchCustomers[i].getValue('parent','custrecord_clgx_dcim_points_day_service','GROUP');
			}

			nlapiSetFieldValues('custrecord_clgx_consumption_customers', arrCustomers); 
		}
	}
		
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}



function afterSubmit(type){
    try{
		var currentContext = nlapiGetContext();
		if (currentContext.getExecutionContext() == 'userinterface' && type == 'create') {

	        var recConsolidated = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
	        var stMonth = recConsolidated.getFieldValue('custrecord_consolidated_inv_month');
	        var stYear = recConsolidated.getFieldValue('custrecord_consolidated_inv_year');
	        var stDisplayMonth = recConsolidated.getFieldValue('custrecord_consolidated_month_display');
	        var stDisplayYear = recConsolidated.getFieldValue('custrecord_consolidated_year_display');
	        var output = recConsolidated.getFieldValue('custrecord_output');
	        var locations = recConsolidated.getFieldValue('custrecord_consolidated_locations');
	        var customers = recConsolidated.getFieldValue('custrecord_consolidated_customers');
	        
	        //nlapiLogExecution('DEBUG','Parameters','stMonth=' + stMonth + ' stYear=' + stYear + 'stDisplayMonth=' + stDisplayMonth + ' stDisplayYear=' + stDisplayYear + ' location=' + location + ' customers=' + customers);
	        
	        var arrParam = new Array();
	            arrParam['custscript_consolidate_locations'] = locations;
	            arrParam['custscript_consolidate_customers'] = customers;
	            arrParam['custscript_consolidate_month'] = stMonth;
	            arrParam['custscript_consolidate_year'] = stYear;
	            arrParam['custscript_consolidated_month_display'] = stDisplayMonth;
	            arrParam['custscript_consolidated_year_display'] = stDisplayYear;
	            arrParam['custscript_consolidated_rec_id'] = nlapiGetRecordId();
	            arrParam['custscript_pdf_output_to'] = output;

	        nlapiScheduleScript('customscript_clgx_ss_consolidate_invoice', 'customdeploy_clgx_ss_consolidate_invoice' ,arrParam);       
        }
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}