nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Invoices_Remote_Hands.js
//	Script Name:	CLGX_SS_Invoices_Remote_Hands
//	Script Id:	    customscript_clgx_ss_invoices_cases_rh
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Case
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	10/14/2014
//-------------------------------------------------------------------------------------------------
function scheduled_create_invoice_rh(){
    try{
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 2/2/2011
// Details:	Consolidate invoices.
//-------------------------------------------------------------------------------------------------
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');
        var startScript = moment();
        var context = nlapiGetContext();
        var initialTime = moment();
        var arrDates  = getDateRange(-1);
        var startDate = arrDates[0];
        var endDate  = arrDates[1];
        var arrDates1  = getDateRange(1);
        var caseNumberForError='';
        var startDate1 = arrDates1[0];
        var endDate1  = arrDates1[1];
        var date=new Date();
        var emailBody='';
        var arrFilters=new Array();
        var arrColumns=new Array();
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_rh_from_date",null,"on",startDate));
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_rh_to_date",null,"on",endDate));
        //retrieve script parameters
        var excludeRHID= context.getSetting('SCRIPT','custscript_rhinv_rhid');
        if((excludeRHID!=null)&&(excludeRHID!=''))
        {
            arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",excludeRHID));
        }
        var searchRemote = nlapiSearchRecord('customrecord_clgx_remote_hands',   'customsearch_clgx_search_rhinvoices', arrFilters, arrColumns);
        if(searchRemote!=null)
        {
            for ( var i = 0; searchRemote != null && i < searchRemote.length; i++ ){
                var startExecTime = moment();
                var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);

                if ( (context.getRemainingUsage() <= 300 || totalMinutes > 5) && (i+1) < searchRemote.length ){
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    if ( status == 'QUEUED' ) {
                        nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                        break;
                    }
                }
                var searchRem = searchRemote[i];
                var columns = searchRem.getAllColumns();
                var usedTimeCase=searchRem.getValue(columns[0]);
                var busTimeCase=searchRem.getValue(columns[1]);
                var afterTimeCase=searchRem.getValue(columns[2]);
                var customer=searchRem.getValue(columns[3]);
                              if(customer==2201298){
                    customer=2314925
                }else if(customer==2829568){
                    customer=2829570
                }
                else if(customer==2829564){
                    customer=2829566
                }
                else if(customer==2201305){
                    customer=2309712
                }
                else if(customer==2582986){
                    customer=2582784
                }
                else if(customer==2201296){
                    customer=2314922
                }
                var location=searchRem.getValue(columns[4]);
                var billing=searchRem.getValue(columns[5]);
                var subsidiary=searchRem.getValue(columns[6]);
                var fixedHoursFinished=searchRem.getValue(columns[7]);
                var rhID=searchRem.getValue(columns[8]);
                var roundedBusinessTimeCase=searchRem.getValue(columns[9]);
                var roundedAfterTimeCase=searchRem.getValue(columns[10]);
                var arrayFiltersFreezing=new Array();
                var arrayColumnsFreezing=new Array();
                var hasLaterSO=0;
                var arrayNewBusNumber=new Array();
                var roundedAfterTimeCaseNumber=new Array();
                var roundedBusinessTimeCaseNumber=new Array();
                var date=new Date();
                var month=date.getMonth();
                var monthNames = [ "January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December" ];
                if(month==0)
                {
                    var monthName=monthNames[11];
                    var yr=date.getFullYear();
                    yr=yr-1;

                }
                else{
                    var monthName=monthNames[month-1];
                    var yr=date.getFullYear();
                    month=month.toString();
                }
                yr=yr.toString();
                arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_brate',null,null));
                arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_arate',null,null));
                arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_item',null,null));
                arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_irate',null,null));
                arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_location',null,null));
                //  arrayColumns.push(new nlobjSearchColumn('custrecord_clgx_rh_times',null,null));
                arrayFiltersFreezing.push(new nlobjSearchFilter("custrecord_clgx_freezing_rh_customer",null,"anyof",customer));
                arrayFiltersFreezing.push(new nlobjSearchFilter('custrecord_clgx_freezing_rh_mth',null,'is',monthName));
                arrayFiltersFreezing.push(new nlobjSearchFilter('custrecord_clgx_freezing_rh_yr',null,'is',yr));
                var searchFRRemoteHands = nlapiSearchRecord('customrecord_freezing_rh_pack',null,  arrayFiltersFreezing, arrayColumnsFreezing);
                //get the customer rate from freezing RH Packs
                if(searchFRRemoteHands!=null)
                {
                    var totalratebusinessH = searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_brate',null,null);
                    var totalrateafterH = searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_arate',null,null);
                    var itemSO=searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_item',null,null);
                    var itemRate=searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_irate',null,null);
                    var locationSO=searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_location',null,null);
                }

                if(totalratebusinessH=='')
                {
                    totalratebusinessH =0;
                }
                if(hasLaterSO==1)
                {
                    totalratebusinessH =0;
                }
                if(totalrateafterH =='')
                {
                    totalrateafterH=0;
                }
                if(hasLaterSO==1)
                {
                    totalrateafterH=0;
                }
                if((roundedBusinessTimeCase!='')&&(roundedBusinessTimeCase!=null))
                {
                    //Used Time
                    var roundedBusinessTimeCaseSplit=roundedBusinessTimeCase.split(';');
                    var roundedBusinessTimeCaseNumber=new Array();
                    var roundedBusinessTimeCaseID=new Array();
                    var roundedBusinessTimeCaseMinutes=new Array();
                    var roundedBusinessTimeCaseEmail=new Array();
                    for(var r=0;r<roundedBusinessTimeCaseSplit.length;r++)
                    {
                        if(roundedBusinessTimeCaseSplit[r]!='')
                        {
                            var roundedBusinessTimeCaseSplit_1=roundedBusinessTimeCaseSplit[r].split('=');
                            var caseRecord=nlapiLoadRecord("supportcase", roundedBusinessTimeCaseSplit_1[0]);
                            roundedBusinessTimeCaseNumber.push(caseRecord.getFieldValue('casenumber'));
                            roundedBusinessTimeCaseMinutes.push(roundedBusinessTimeCaseSplit_1[1]);
                            roundedBusinessTimeCaseID.push(roundedBusinessTimeCaseSplit_1[0]);
                            roundedBusinessTimeCaseEmail.push(caseRecord.getFieldValue('email'));
                        }

                    }
                }
                if((roundedAfterTimeCase!='')&&(roundedAfterTimeCase!=null))
                {
                    //Used Time
                    var roundedAfterTimeCaseSplit=roundedAfterTimeCase.split(';');
                    var roundedAfterTimeCaseNumber=new Array();
                    var roundedAfterTimeCaseID=new Array();
                    var roundedAfterTimeCaseMinutes=new Array();
                    var roundedAfterTimeCaseEmail=new Array();
                    for(var r=0;r<roundedAfterTimeCaseSplit.length;r++)
                    {
                        if(roundedAfterTimeCaseSplit[r]!='')
                        {
                            var roundedAfterTimeCaseSplit_1=roundedAfterTimeCaseSplit[r].split('=');
                            var caseRecord=nlapiLoadRecord("supportcase", roundedAfterTimeCaseSplit_1[0]);
                            roundedAfterTimeCaseNumber.push(caseRecord.getFieldValue('casenumber'));
                            roundedAfterTimeCaseMinutes.push(roundedAfterTimeCaseSplit_1[1]);
                            roundedAfterTimeCaseID.push(roundedAfterTimeCaseSplit_1[0]);
                            roundedAfterTimeCaseEmail.push(caseRecord.getFieldValue('email'));
                        }

                    }
                }
                if((busTimeCase!='')&&(busTimeCase!=null))
                {
                    //Business Time
                    var busTimeCaseSplit=busTimeCase.split(';');
                    var busTimeCaseCaseNumber=new Array();
                    var busTimeCaseCaseID=new Array();
                    var busTimeCaseCaseMinutes=new Array();
                    var busTimeCaseCaseEmail=new Array();
                    for(var r=0;r<busTimeCaseSplit.length;r++)
                    {
                        if(busTimeCaseSplit[r]!='')
                        {
                            var busTimeCaseSplit_1=busTimeCaseSplit[r].split('=');
                            var caseRecord=nlapiLoadRecord("supportcase", busTimeCaseSplit_1[0]);
                            busTimeCaseCaseNumber.push(caseRecord.getFieldValue('casenumber'));
                            busTimeCaseCaseMinutes.push(busTimeCaseSplit_1[1]);
                            busTimeCaseCaseID.push(busTimeCaseSplit_1[0]);
                            busTimeCaseCaseEmail.push(caseRecord.getFieldValue('email'));

                        }
                    }
                }
                //after Time
                var afterTimeCaseSplit=afterTimeCase.split(';');
                var afterTimeCaseCaseNumber=new Array();
                var afterTimeCaseCaseID=new Array();
                var afterTimeCaseCaseMinutes=new Array();
                var afterTimeCaseCaseEmail=new Array();
                var positionDeleteBus=-1;
                for(var r=0;r<afterTimeCaseSplit.length;r++)
                {
                    if(afterTimeCaseSplit[r]!='')
                    {
                        var afterTimeCaseSplit_1=afterTimeCaseSplit[r].split('=');
                        var caseRecord=nlapiLoadRecord("supportcase", afterTimeCaseSplit_1[0]);
                        afterTimeCaseCaseNumber.push(caseRecord.getFieldValue('casenumber'));
                        afterTimeCaseCaseMinutes.push(afterTimeCaseSplit_1[1]);
                        afterTimeCaseCaseID.push(afterTimeCaseSplit_1[0]);
                        afterTimeCaseCaseEmail.push(caseRecord.getFieldValue('email'));
                    }


                }
                var newInvoice=nlapiCreateRecord('invoice');
                newInvoice.setFieldValue('location', location);
                var consolidateLocation=clgx_return_consolidate_location(location);
                newInvoice.setFieldValue('custbody_clgx_consolidate_locations', clgx_return_consolidate_location(location));
                newInvoice.setFieldValue('subsidiary', subsidiary);
                var ctr=1;


                if((roundedBusinessTimeCase!='')&&(roundedBusinessTimeCase!=null))
                {
                    for (var v=0;v<roundedBusinessTimeCaseNumber.length;v++)
                    {

                        var timeusedH=roundedBusinessTimeCaseMinutes[v]/60;
                        timeusedH=timeusedH.toFixed(2);


                        if(timeusedH>0)
                        {


                            newInvoice.setLineItemValue('item', 'item', ctr, 544);

                            newInvoice.setLineItemValue('item', 'quantity', ctr, timeusedH);
                            newInvoice.setLineItemValue('item', 'rate',ctr, 0);
                            newInvoice.setLineItemValue('item', 'location',ctr, location);
                            newInvoice.setLineItemValue('item', 'amount',ctr, 0);

                            var returntax=clgx_return_tax(location,customer);
                            if(returntax!='')
                            {
                                newInvoice.setLineItemValue('item', 'taxcode',ctr, returntax[0]);
                                newInvoice.setLineItemValue('item', 'taxcode_display',ctr, returntax[1]);

                            }
                            caseNumberForError=roundedBusinessTimeCaseNumber[v];
                            newInvoice.setLineItemValue('item', 'description',ctr, 'Case\r\n #'+roundedBusinessTimeCaseNumber[v]+ '\r\nRequested by '+roundedBusinessTimeCaseEmail[v]);
                            newInvoice.setLineItemValue('item', 'custcol_clgx_rh_invoice_item_case',ctr, roundedBusinessTimeCaseID[v]);

                            ctr++;
                        }

                    }
                }

                if((roundedAfterTimeCase!='')&&(roundedAfterTimeCase!=null))
                {
                    for (var v=0;v<roundedAfterTimeCaseNumber.length;v++)
                    {

                        var timeusedH=roundedAfterTimeCaseMinutes[v]/60;
                        timeusedH=timeusedH.toFixed(2);


                        if(timeusedH>0)
                        {
                            newInvoice.setLineItemValue('item', 'item', ctr, 545);
                            newInvoice.setLineItemValue('item', 'quantity', ctr, timeusedH);
                            newInvoice.setLineItemValue('item', 'rate',ctr, 0);
                            newInvoice.setLineItemValue('item', 'location',ctr, location);
                            newInvoice.setLineItemValue('item', 'amount',ctr, 0);

                            var returntax=clgx_return_tax(location,customer);
                            if(returntax!='')
                            {
                                newInvoice.setLineItemValue('item', 'taxcode',ctr, returntax[0]);
                                newInvoice.setLineItemValue('item', 'taxcode_display',ctr, returntax[1]);

                            }
                            caseNumberForError=roundedAfterTimeCaseNumber[v];
                            newInvoice.setLineItemValue('item', 'description',ctr, 'Case\r\n #'+roundedAfterTimeCaseNumber[v]+ '\r\nRequested by '+roundedAfterTimeCaseEmail[v]);
                            newInvoice.setLineItemValue('item', 'custcol_clgx_rh_invoice_item_case',ctr, roundedAfterTimeCaseID[v]);

                            ctr++;
                        }


                    }
                }
                //add business items
                if((busTimeCase!='')&&(busTimeCase!=null))
                {
                    for (var v=0;v<busTimeCaseCaseNumber.length;v++)
                    {

                        var timeusedHB=busTimeCaseCaseMinutes[v]/60;
                        timeusedHB=timeusedHB.toFixed(2);
                        if(timeusedHB>0)
                        {
                            newInvoice.setLineItemValue('item', 'item', ctr, 544);


                            newInvoice.setLineItemValue('item', 'item', ctr, 544);
                            newInvoice.setLineItemValue('item', 'rate',ctr, totalratebusinessH);
                            newInvoice.setLineItemValue('item', 'amount',ctr, timeusedHB*totalratebusinessH);

                            newInvoice.setLineItemValue('item', 'quantity', ctr, timeusedHB);
                            newInvoice.setLineItemValue('item', 'location',ctr, location);


                            var returntax=clgx_return_tax(location,customer);
                            if(returntax!='')
                            {
                                newInvoice.setLineItemValue('item', 'taxcode',ctr, returntax[0]);
                                newInvoice.setLineItemValue('item', 'taxcode_display',ctr, returntax[1]);

                            }
                            caseNumberForError=busTimeCaseCaseNumber[v];
                            newInvoice.setLineItemValue('item', 'description',ctr, 'Case\r\n #'+busTimeCaseCaseNumber[v]+ '\r\nRequested by '+busTimeCaseCaseEmail[v]);
                            newInvoice.setLineItemValue('item', 'custcol_clgx_rh_invoice_item_case',ctr, busTimeCaseCaseID[v]);

                            ctr++;
                        }

                    }
                }
                //add after items
                if((afterTimeCase!='')&&(afterTimeCase!=null))
                {
                    for (var v=0;v<afterTimeCaseCaseNumber.length;v++)
                    {
                        var timeusedHA=afterTimeCaseCaseMinutes[v]/60;
                        timeusedHA=timeusedHA.toFixed(2);
                        if(timeusedHA>0)
                        {

                            newInvoice.setLineItemValue('item', 'item', ctr, 545);
                            newInvoice.setLineItemValue('item', 'rate',ctr, totalrateafterH);
                            newInvoice.setLineItemValue('item', 'amount',ctr, timeusedHA*totalrateafterH);

                            newInvoice.setLineItemValue('item', 'quantity', ctr, timeusedHA);

                            newInvoice.setLineItemValue('item', 'location',ctr, location);


                            var returntax=clgx_return_tax(location,customer);
                            if(returntax!='')
                            {
                                newInvoice.setLineItemValue('item', 'taxcode',ctr, returntax[0]);
                                newInvoice.setLineItemValue('item', 'taxcode_display',ctr, returntax[1]);

                            }
                            caseNumberForError=afterTimeCaseCaseNumber[v];
                            newInvoice.setLineItemValue('item', 'description',ctr, 'Case\r\n #'+afterTimeCaseCaseNumber[v]+ '\r\nRequested by '+afterTimeCaseCaseEmail[v]);
                            newInvoice.setLineItemValue('item', 'custcol_clgx_rh_invoice_item_case',ctr, afterTimeCaseCaseID[v]);

                            ctr++;
                        }
                    }
                }

                newInvoice.setFieldValue('entity', customer); //set customer ID
                var billingid='';
                if((billing=='')||(billing==null))
                {
                    var billingid=clgx_return_billing (location, customer)

                }
                if(billingid!='')
                {
                    var custRecord=nlapiLoadRecord('customer',customer);
                    var NRABs = custRecord.getLineItemCount('addressbook');
                    for ( var j = 1; j <= NRABs; j++ ) {
                        var internalid = custRecord.getLineItemValue('addressbook', 'internalid', j);
                        if(internalid== billingid)
                        {
                            var billaddress=custRecord.getLineItemValue('addressbook', 'addressbookaddress_text', j);
                            var billaddresslist=custRecord.getLineItemValue('addressbook', 'addressid', j);
                            var billingaddress_key=custRecord.getLineItemValue('addressbook', 'addressbookaddress_key', j);
                            var billingaddress_text=custRecord.getLineItemValue('addressbook', 'addressbookaddress_text', j);
                            var billingaddress_type=custRecord.getLineItemValue('addressbook', 'addressbookaddress_type', j);
                            var billisresidential=custRecord.getLineItemValue('addressbook', 'isresidential', j);
                            var addressee = custRecord.getLineItemValue('addressbook', 'addressee', j);

                        }


                    }
                    newInvoice.setFieldValue('billaddress', billaddress);
                    newInvoice.setFieldValue('billaddressee', addressee);
                    newInvoice.setFieldValue('billaddresslist', billaddresslist);
                    newInvoice.setFieldValue('billingaddress_key', billingaddress_key);
                    newInvoice.setFieldValue('billingaddress_text', billingaddress_text);
                    newInvoice.setFieldValue('billingaddress_type', billingaddress_type);
                    newInvoice.setFieldValue('billisresidential', 'F');
                } else{
                    newInvoice.setFieldValue('billaddress', billing);
                    //  newInvoice.setFieldValue('billaddressee', addressee);
                }


                //newInvoice.setFieldValue('name', customer);
                newInvoice.setFieldValue('trandate', startDate1);
                newInvoice.setFieldValue('saleseffectivedate', startDate1);

                var invoiceId =nlapiSubmitRecord(newInvoice, false,true);

                emailBody = emailBody+'Please check this invoice and make sure everyting is ok!!!!' +invoiceId+'\r\n';

                //Update the case field billed
                // var caseRecord=nlapiLoadRecord('supportcase',internalID);
                // caseRecord.setFieldValue('custevent_clgx_case_invoiced','T');
                // nlapiSubmitRecord(caseRecord, false,true);
                var remoteHandsRecord=nlapiLoadRecord('customrecord_clgx_remote_hands',rhID );
                remoteHandsRecord.setFieldValue('custrecord_clgx_rh_billed','T');
                nlapiSubmitRecord(remoteHandsRecord, false,true);

            }
            var emailSubject = 'Dear Catalina,';

            clgx_send_employee_emails_from_savedsearch("customsearchclgxe_invoice_errors", emailSubject, emailBody);

            //nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null,true); // Send email to Catalina

            var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
            var index = i + 1;
            nlapiLogExecution('DEBUG','SO ', ' | consolidateinvid =  ' + invoiceId + ' | Index = ' + index + ' of ' + searchRemote.length + ' | Usage - '+ usageConsumtion + '  |');
        }
        var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
        nlapiLogExecution('DEBUG','Finished Execution', 'Total Remaining Usage - ' + context.getRemainingUsage() + 'Total Usage-' + usageConsumtion + '-------------------------- Finished Scheduled Script --------------------------|');

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            var emailSubject = 'The Remote Hands Invoices Script has encountered a problem and needs to be manually adjusted';
            var errorDetails=error.getDetails();
            if(errorDetails.search("location") != -1) {
                //logic
                var strnew=errorDetails.split('for');
                var thenum1 = strnew[0].match(/\d/g);
                thenum1 = thenum1.join("");
                var thenum2 = strnew[1].match(/\d/g);
                thenum2 = thenum2.join("");
                var strnew1= strnew[0].replace(new RegExp("[0-9]", "g"), "");
                var strnew2= strnew[1].replace(new RegExp("[0-9]", "g"), "");
                var customerlocation  =  nlapiLookupField('location', thenum1, 'name');
                var subsidiary=nlapiLookupField('subsidiary', thenum2, 'name');
                var message=strnew1+'<span style="color:red; font-weight: bold;">'+customerlocation+'</span> for'+strnew2+'<span style="color:red; font-weight: bold;">'+subsidiary+'</span>';
            }
            else{
                var message=errorDetails;
            }
            var emailBody='The case  has encountered a problem and needs to be manually adjusted. Error description:'+message+'<br>';

            clgx_send_employee_emails_from_savedsearch("customsearchclgxe_invoice_errors", emailSubject, "<html><body style='font-family:Verdana'>"+emailBody+"</body></html>");

            //nlapiSendEmail(206211, 206211,emailSubject,"<html><body style='font-family:Verdana'>"+emailBody+"</body></html>",null,null,null,null,true); // Send email to Catalina
            //   nlapiSendEmail(206211, 179749,emailSubject,"<html><body style='font-family:Verdana'>"+emailBody+"</body></html>",null,null,null,null); // Send email to Steve
            //nlapiSendEmail(206211, 12073,emailSubject,"<html><body style='font-family:Verdana'>"+emailBody+"</body></html>",null,null,null,null); // Send email to Kristen
            var params = new Array();
            params['custscript_rhinv_rhid'] = rhID;
            var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId(),params);
            if(status == 'QUEUED'){
                nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
            }
            var remoteHandsRecord=nlapiLoadRecord('customrecord_clgx_remote_hands',rhID );
            remoteHandsRecord.setFieldValue('custrecord_clgx_rh_billed','T');
            nlapiSubmitRecord(remoteHandsRecord, false,true);

            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            var errorDetails=error.toString();
            if(errorDetails.search("location") != -1) {
                //logic
                var strnew=errorDetails.split('for');
                var thenum1 = strnew[0].match(/\d/g);
                thenum1 = thenum1.join("");
                var thenum2 = strnew[1].match(/\d/g);
                thenum2 = thenum2.join("");
                var strnew1= strnew[0].replace(new RegExp("[0-9]", "g"), "");
                var strnew2= strnew[1].replace(new RegExp("[0-9]", "g"), "");
                var customerlocation  =  nlapiLookupField('location', thenum1, 'name');
                var subsidiary=nlapiLookupField('subsidiary', thenum2, 'name');
                var message=strnew1+'<span style="color:red; font-weight: bold;">'+customerlocation+'</span> for'+strnew2+'<span style="color:red; font-weight: bold;">'+subsidiary+'</span>';
            }
            else{
                var message=errorDetails;
            }
            var emailSubject = 'The Remote Hands Invoices Script has encountered a problem and needs to be manually adjusted';
            var emailBody='The case has encountered a problem and needs to be manually adjusted. Error description:'+message +'<br>';

            clgx_send_employee_emails_from_savedsearch("customsearchclgxe_invoice_errors", emailSubject, "<html><body style='font-family:Verdana'>"+emailBody+"</body></html>");

            //nlapiSendEmail(206211, 206211,emailSubject,"<html><body style='font-family:Verdana'>"+emailBody+"</body></html>",null,null,null,null,true); // Send email to Catalina
            //  nlapiSendEmail(206211, 179749,emailSubject,"<html><body style='font-family:Verdana'>"+emailBody+"</body></html>",null,null,null,null); // Send email to Steve
            //  nlapiSendEmail(206211, 12073,emailSubject,"<html><body style='font-family:Verdana'>"+emailBody+"</body></html>",null,null,null,null); // Send email to Kristen
            var params = new Array();
            params['custscript_rhinv_rhid'] = rhID;
            var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId(),params);
            if(status == 'QUEUED'){
                nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
            }
            var remoteHandsRecord=nlapiLoadRecord('customrecord_clgx_remote_hands',rhID );
            remoteHandsRecord.setFieldValue('custrecord_clgx_rh_billed','T');
            nlapiSubmitRecord(remoteHandsRecord, false,true);
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function checkFixedRem(customer,so)
{
    var arrDates1  = getDateRange(0);
    var startDate1 = arrDates1[0];
    var endDate1  = arrDates1[1];
    var date=new Date();
    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_rh_customer",null,"anyof",customer));
    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_rh_from_date",null,"on",startDate1));
    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_rh_to_date",null,"on",endDate1));
    var searchRemote = nlapiSearchRecord('customrecord_clgx_remote_hands',   'customsearch_clgx_search_rhfinvoices1', arrFilters, arrColumns);
    var test=0;
    for ( var i = 0; searchRemote != null && i < searchRemote.length; i++ ) {
        var searchRem = searchRemote[i];
        var columns = searchRem.getAllColumns();
        var location=searchRem.getValue(columns[0]);
        var customer=searchRem.getValue(columns[1]);
        var casenumber=searchRem.getValue(columns[2]);
        var email=searchRem.getValue(columns[3]);
        var so1=searchRem.getValue(columns[4]);
        var soLoc=searchRem.getValue(columns[5]);
        var itemrate=searchRem.getValue(columns[6]);
        var prepaidTime=searchRem.getValue(columns[7]);
        var busH=searchRem.getValue(columns[8]);
        var busRate=searchRem.getValue(columns[9]);
        var afterH=searchRem.getValue(columns[10]);
        var afterRate=searchRem.getValue(columns[11]);
        var package=searchRem.getValue(columns[12]);
        var packageH=searchRem.getValue(columns[13]);
    }
    if(so==so1)
    {
        if(prepaidTime>0)
        {
            test=0;

        }
        else{
            test=1;
        }
    }
    return test;
}

function getLocation(location)
{
//COL1&2
    if(location==24)
    {
        var loc=34;
    }
    //TOR 156 Front
    if(location== 23)
    {
        var loc=13;
    }
    //Cologix HQ
    if(location== 16)
    {
        var loc=0;
    }
    //DAL1
    if(location==3)
    {
        var loc=2;
    }
    //DAL2
    if(location==18)
    {
        var loc=17;
    }
    //JAX1
    if(location==21)
    {
        var loc=31;
    }
    //JAX2
    if(location==27)
    {
        var loc=40;
    }
    //MIN1&2
    if(location==17)
    {
        var loc=16;
    }
    //MIN3
    if(location==25)
    {
        var loc=35;
    }
    //MTL1
    if(location==2)
    {
        var loc=5;
    }
    //MTL2
    if(location==5)
    {
        var loc=8;
    }
    //MTL3
    if(location==4)
    {
        var loc=9;
    }
    //MTL4
    if(location==9)
    {
        var loc=10;
    }
    //MTL5
    if(location==6)
    {
        var loc=11;
    }
    //MTL6
    if(location==7)
    {
        var loc=12;
    }
    //MTL7
    if(location==19)
    {
        var loc=27;
    }
    //TOR1
    if(location==8)
    {
        var loc=6;
    }
    //TOR2
    if(location==15)
    {
        var loc=15;
    }
    //VAN1
    if(location==14)
    {
        var loc=28;
    }
    //VAN2
    if(location==20)
    {
        var loc=7;
    }
    return loc;
}

function getDateRange(monthsago){

    var today = new Date();
    var date = nlapiAddMonths(today, monthsago);

    var month = parseInt(date.getMonth());
    var year = date.getFullYear();

    var stDays  = daysInMonth(parseInt(month),parseInt(year));
    var stYear  = year;

    var stMonth = parseInt(month) + 1;

    var stStartDate = stMonth + '/1/' + stYear;
    var stEndDate   = stMonth + '/' + stDays + '/' + stYear;

    var arrDateRange = new Array();
    arrDateRange[0] = stStartDate;
    arrDateRange[1] =stEndDate;

    return arrDateRange;
}

function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}

//check if a value is in the array
function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}

function roundMinBusiness(totbusinessH){
    totbusinessH=totbusinessH.toFixed(2);
    totbusinessH=totbusinessH.toString();
    var hb=totbusinessH.split(".");
    if((hb[1]>0)&&(hb[1]<=15))
    {
        hb[1]=15;
    }
    if((hb[1]>15)&&(hb[1]<=30))
    {
        hb[1]=30;
    }
    if((hb[1]>30)&&(hb[1]<=45))
    {
        hb[1]=45;
    }
    if((hb[1]>45)&&(hb[1]<=59))
    {
        hb[1]=0;
        hb[0]=parseInt(hb[0])+parseInt(1);
    }
    var totalTimeB=hb[0]+'.'+hb[1];
    if(parseInt(hb[0])*parseInt(60)>0)
    {
        totalTimeB=parseInt(hb[0])*parseInt(60)+parseInt(hb[1]);
    }
    else
    {
        totalTimeB=parseInt(hb[1]);
    }

    return totalTimeB;

}

function roundMinAfter(totafterH){
    totafterH=totafterH.toFixed(2);
    totafterH=totafterH.toString();
    var ha=totafterH.split(".");
    if((ha[0]==0)&&(ha[1]==0))
    {
        var totalTimeA=0;
    }
//after hours
    if((ha[0]<1)&&(ha[1]>0))
    {
        var totalTimeA=1;
    }
    else
    {
        if((ha[1]>0)&&(ha[1]<=15))
        {
            ha[1]=15;
        }
        if((ha[1]>15)&&(ha[1]<=30))
        {
            ha[1]=30;
        }
        if((ha[1]>30)&&(ha[1]<=45))
        {
            ha[1]=45;
        }
        if((ha[1]>45)&&(ha[1]<=59))
        {
            ha[1]=0;
            ha[0]=parseInt(ha[0])+parseInt(1);
        }
        var totalTimeA=ha[0]+'.'+ha[1];
    }
    if(parseInt(ha[0])*parseInt(60)>0)
    {
        totalTimeA=parseInt(ha[0])*parseInt(60)+parseInt(ha[1]);
    }
    else
    {
        totalTimeA=parseInt(ha[1]);
    }
    return totalTimeA;
}

//return tax
function clgx_return_tax (locationid, customerid){

    var returntax = '';
    var returntaxText = '';
    var customertax=nlapiLoadRecord('customer', customerid);

    switch(parseInt(locationid)) {

        case 33: // Columbus
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_col'); // COL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_col'); // COL
            break;
        case 34: // COL1&2
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_col'); // COL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_col'); // COL
            break;
        case 39: // COL3
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_col'); // COL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_col'); // COL
            break;
        case 2: // Dallas Infomart
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_dal'); // DAL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_dal'); // DAL
            break;
        case 17: // Dallas Infomart - Ste 2010
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_dal'); // DAL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_dal'); // DAL
            break;
        case 31: // JAX1
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_jax'); // JAX1
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_jax'); // JAX1
            break;
        case 40: // JAX2
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_jax'); // JAX2
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_jax'); // JAX2
            break;
        case 42: // LAK1
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_lak'); // LAK1
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_lak'); // LAK1
            break;
        case 16: // MIN1&2
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_min'); // MIN
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_min'); // MIN
            break;
        case 35: // MIN3
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_min'); // MIN
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_min'); // MIN
            break;
        case 5: // MTL1
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 8: // MTL2
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 9: // MTL3
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 10: // MTL4
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 11: // MTL5
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 12: // MTL6
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 27: // MTL7
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 77: // MTL9
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        case 78: // MTL10
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_mtl'); // MTL
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_mtl'); // MTL
            break;
        ///NNJ
        case 53: // NNJ1
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_nnj'); // NNJ1
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_nnj'); // NNJ1
            break;
        case 54: // NNJ2
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_nnj'); // NNJ2
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_nnj'); // NNJ2
            break;
        case 55: // NNJ3
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_nnj'); // NNJ3
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_nnj'); // NNJ3
            break;
        case 56: // NNJ4
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_nnj'); // NNJ4
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_nnj'); // NNJ4
            break;
        case 13: // 156 Front Street West
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_tor');// TOR
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_tor');// TOR
            break;
        case 14: // Barrie Office
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_tor'); // TOR
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_tor'); // TOR
            break;
        case 6: // 151 Front Street West
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_tor'); // TOR
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_tor'); // TOR
            break;
        case 15: // 905 King Street West
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_tor'); // TOR
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_tor'); // TOR
            break;
        case 18: // 151 Front Street West - Ste 822
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_tor'); // 151 Front Street West - Ste 822
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_tor'); // 151 Front Street West - Ste 822
            break;
        case 28: // 1050 Pender
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_van'); // VAN
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_van'); // VAN
            break;
        case 7: // Harbour Centre
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_van'); // VAN
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_van'); // VAN
            break;
             case 60: // VAN3
            returntax = customertax.getFieldValue('custentity_clgx_tax_item_van'); // VAN
            returntaxText = customertax.getFieldText('custentity_clgx_tax_item_van'); // VAN
            break;

        default:
            returntax = '';

    }
    var returntaxArray=[returntax,returntaxText]
    return returntaxArray;
}
//return billing address
//return tax
function clgx_return_billing (locationid, customerid){

    var returnbilling = '';
//Bell Canada        -          MTL1              -               1114
//                   -          MTL2              -               24133
//                   -          MTL3              -               24159
//                   -          MTL7              -               24653

    if(customerid==2340)
    {
        if(locationid==5)
        {
            returnbilling=1114;
        }
        if(locationid==8)
        {
            returnbilling=24133;
        }
        if(locationid==9)
        {
            returnbilling=24159;
        }
        if(locationid==27)
        {
            returnbilling=24653;
        }
    }

//    Fibrenoire           -           MTL3              -              24130
//                         -           MTL4              -              11411
//                         -           MTL5              -              15044
    //   -                                MTL1              -              24128

    if(customerid==1720)
    {
        if(locationid==5)
        {
            returnbilling=1016;
        }
        else if(locationid==9)
        {
            returnbilling=1016;
        }
        else if(locationid==10)
        {
            returnbilling=1016;
        }
        else if(locationid==11)
        {
            returnbilling=1016;
        }
        else if(locationid==78)
        {
            returnbilling=1016;
        }
        else if(locationid==79)
        {
            returnbilling=1016;
        }
        else if(locationid==8)
        {
            returnbilling=1016;
        }
        else if(locationid==8)
        {
            returnbilling=1016;
        }
        else if(locationid==10)
        {
            returnbilling=1016;
        }
        else if(locationid==11)
        {
            returnbilling=1016;
        }
        else if(locationid==12)
        {
            returnbilling=1016;
        }
        else if(locationid==27)
        {
            returnbilling=1016;
        }
        else if(locationid==74)
        {
            returnbilling=1016;
        }
        else if(locationid==77)
        {
            returnbilling=1016;
        }
        else if(locationid==78)
        {
            returnbilling=1016;
        }
        else if(locationid==79)
        {
            returnbilling=1016;
        }
        else if(locationid==6)
        {
            returnbilling=218497;
        }
        else if(locationid==15)
        {
            returnbilling=218497;
        }
        else if(locationid==71)
        {
            returnbilling=218497;
        }
        else if(locationid==7)
        {
            returnbilling=218497;
        }
        else if(locationid==28)
        {
            returnbilling=105492;
        }
        else if(locationid==60)
        {
            returnbilling=105492;
        }

    }
//    Hurricane Electric CA -     TOR1 or TOR2    -           6232
//                       -          VAN1 OR VAN2  -           15313
//            -   MTL1, MTL2, MTL3, MTL4, MTL5, MTL6, MTL7  - 15314
    if(customerid==6501)
    {
        if((locationid==14)||(locationid==15)||(locationid==6))
        {
            returnbilling=6232;
        }
        if((locationid==7)||(locationid==28))
        {
            returnbilling=15313;
        }
        if((locationid==5)||(locationid==8)||(locationid==9)||(locationid==10)||(locationid==11)||(locationid==12)||(locationid==27))
        {
            returnbilling=15314;
        }
    }
//    Hurricane Electric US -   MIN1&2, MIN3      -           6084
//        -    DAL1 & DAL2       -          15315
//        -    COL1&2, COL3    -           23627

    if(customerid==9519)
    {
        if((locationid==16)||(locationid==35))
        {
            returnbilling=6084;
        }
        if((locationid==2)||(locationid==17))
        {
            returnbilling=15315;
        }
        if((locationid==34)||(locationid==39))
        {
            returnbilling=23627;
        }
    }

//    Hydro One Telecom Inc - TOR1 or TOR2    -           1032
//        -   MTL1, MTL2, MTL3, MTL4, MTL5, MTL6, MTL7  - 12949
    if(customerid==1764)
    {
        if((locationid==14)||(locationid==15))
        {
            returnbilling=40342;
        }
        if((locationid==5)||(locationid==8)||(locationid==9)||(locationid==10)||(locationid==11)||(locationid==12)||(locationid==27))
        {
            returnbilling=1032;
        }
    }
    if(customerid==723161)
    {
        if((locationid==14)||(locationid==15))
        {
            returnbilling=40342;
        }

    }

//    Netelligent Hosting      -          MTL1               -         958
//        -          MTL2               -         11243
//        -          MTL3               -         11244
//        -          MTL4               -         11245
//        -          MTL5               -         11246
//        -      TOR1 or TOR2          -           11372
    if(customerid==2191)
    {
        if((locationid==14)||(locationid==15))
        {
            returnbilling=11372;
        }
        if(locationid==5)
        {
            returnbilling=958;
        }
        if(locationid==8)
        {
            returnbilling=11243;
        }
        if(locationid==9)
        {
            returnbilling=11244;
        }
        if(locationid==10)
        {
            returnbilling=11245;
        }
        if(locationid==11)
        {
            returnbilling=11246;
        }

    }
    if(customerid == 906673) {
        returnbilling = 60117;
    }
    if(customerid == 395249) {
        returnbilling = 60118;
    }
    if(customerid == 395254) {
        returnbilling = 60119;
    }
    if(customerid == 1006770) {
        returnbilling = 60120;
    }
    if(customerid==791328)
    {
        returnbilling=52716;
    }
    if(customerid==137226)
    {
        returnbilling=52717;
    }
    if(customerid==473887)
    {
        returnbilling=54877;
    }
    if(customerid==76059)
    {
        returnbilling=25096;
    }
    if(customerid==76059)
    {
        returnbilling=25096;
    }
    if(customerid==1657)
    {
        returnbilling=1106;
    }
    if(customerid==3847)
    {
        returnbilling=1893;
    }
    if(customerid==10859)
    {
        returnbilling=6878;
    }
    if(customerid==401284)
    {
        returnbilling=24771;
    }
    if(customerid==89)
    {
        returnbilling=36;
    }
    if(customerid==8937)
    {
        returnbilling=5733;
    }
    if(customerid==137232)
    {
        returnbilling=15487;
    }
    if(customerid==922783)
    {
        returnbilling=59322;
    }
    if(customerid==922786)
    {
        returnbilling=59328;
    }
    if(customerid==238400)
    {
        returnbilling=59340;
    }
    if(customerid==789277)
    {
        returnbilling=59336;
    }
    if(customerid==2118931)
    {
        returnbilling=213424;
    }
    if(customerid==373287)
    {
        returnbilling=201176;
    }
    //Oracle - Canada MTL accoun
    if(customerid==2534056)
    {
        returnbilling=212077;
    }
    //Banque Nationale du Canada - CD1:CD2
    if(customerid== 2201295)
    {
        returnbilling=219555;
    }
    return returnbilling;
}


function eliminateDuplicates(arr) {
    var i,
        len=arr.length,
        out=[],
        obj={};

    for (i=0;i<len;i++) {
        obj[arr[i]]=0;
    }
    for (i in obj) {
        out.push(i);
    }
    return out;
}