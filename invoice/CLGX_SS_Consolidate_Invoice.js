nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Consolidate_invoice.js
//	Script Name:	CLGX_SS_Consolidate_invoice
//	Script Id:		customscript_clgx_ss_consolidate_invoice
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Virtual Consolidated Invoice
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		02/02/2011
//	Updated:		04/07/2014
//-------------------------------------------------------------------------------------------------

function scheduled_consolidate_invoice(){
    try{
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 2/2/2011
// Details:	Consolidate invoices.
//-------------------------------------------------------------------------------------------------
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 
        
        var startScript = moment();
        var context = nlapiGetContext();
        var outoftheloop = 0;
        
        //retrieve script parameters
        var arrLocations       = context.getSetting('SCRIPT','custscript_consolidate_locations');
        var arrCustomers       = context.getSetting('SCRIPT','custscript_consolidate_customers');
        var startMonth   = context.getSetting('SCRIPT','custscript_consolidate_month');
        var startYear    = context.getSetting('SCRIPT','custscript_consolidate_year');
        var startDisplayMonth   = context.getSetting('SCRIPT','custscript_consolidated_month_display');
        var startDisplayYear    = context.getSetting('SCRIPT','custscript_consolidated_year_display');
        var consolidateID  = context.getSetting('SCRIPT','custscript_consolidated_rec_id');
        var output       = context.getSetting('SCRIPT','custscript_pdf_output_to');

        if(output == 1){
        	var reportSubject = 'Consolidated Invoices Report - Batch #' + consolidateID + ' - Final Invoices';
        }
        else{
        	var reportSubject = 'Consolidated Invoices Report - Batch #' + consolidateID + ' - Pro Forma Invoices';
        }
        var  reportBody = '';
		reportBody += 'Start : ' + startScript.format('M/D/YYYY h:mm:ss a');
        reportBody += '<br><table border="1" cellpadding="5">';
        reportBody += '<tr><td>Customer<br>Index</td><td>Customer</td><td>Invoice<br>Index</td><td>Consolidated<br>Invoice Nbr</td><td>Template<br>Used</td><td>Currency</td><td>PDF<br>File ID</td><td>Invoices<br>Consolidated</td><td>Sent to</td><td>Date/Time</td><td>Minute</td><td>Usage</td><td>Cumulative<br>Usage</td></tr>';

        // work around returned locations list filter bug (when more than one)
        var listLocations = new Array();
        if (arrLocations !=  null){
        	if(arrLocations.length > 2){
        		var listLocations = arrLocations.split(arrLocations.charAt(2));
        	}
        	else{
        		var listLocations = arrLocations;
        	}
        }
        
        //get the start date and end date in order to use for search filter
        var arrDates  = getDateRange(startMonth,startYear);
        var startDate = arrDates[0];
        var endDate   = arrDates[1];

//-------------------------- Verify Consolidate Locations First ----------------------------------------------------------------------------

        // search for invoices without consolidate location
        var arrColumns = new Array();
        var arrFilters = new Array();
        arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
        arrColumns[1] = new nlobjSearchColumn('location', null, null);
        arrFilters[0] = new nlobjSearchFilter('mainline',null,'is','T');
        arrFilters[1] = new nlobjSearchFilter('location',null,'noneof','1');
        arrFilters[2] = new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof','@NONE@');
        arrFilters[3] = new nlobjSearchFilter('trandate',null,'within',startDate,endDate);
        arrFilters[4] = new nlobjSearchFilter('custbody_consolidate_inv_id',null,'anyof','@NONE@');
        arrFilters[5] = new nlobjSearchFilter('custbody_consolidate',null,'is','T');
        var searchResults = nlapiSearchRecord('invoice', null, arrFilters, arrColumns);

        for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
            
            if((context.getRemainingUsage() < 5000) && ((i + 1) < searchResults.length)){
                var params = new Array();
                    params['custscript_consolidate_locations'] = arrLocations;
                    params['custscript_consolidate_customers'] = arrCustomers;
                    params['custscript_consolidate_month'] = startMonth;
                    params['custscript_consolidate_year'] = startYear;
                    params['custscript_consolidated_month_display'] = startDisplayMonth;
                    params['custscript_consolidated_year_display'] = startDisplayYear;
                    params['custscript_consolidated_rec_id'] = consolidateID;
                    params['custscript_pdf_output_to'] = output;
                
                var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId(),params);
                if(status == 'QUEUED'){
                    nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
    			    break;
                }
            }

        	var searchResult = searchResults[i];
        	var internalID = searchResult.getValue('internalid');
        	var locationID = searchResult.getValue('location');
        	var myRecord = nlapiLoadRecord('invoice', internalID);
        	myRecord.setFieldValue('custbody_clgx_consolidate_locations', clgx_return_consolidate_location(locationID));
    		nlapiSubmitRecord(myRecord, true, true);
    		
            nlapiLogExecution('DEBUG','Invoices and Usage: ','Invoice ' + i + ' of ' + searchResults.length + ' / '+ internalID);
        }
//-------------------------- Finish Verify Consolidate Locations ----------------------------------------------------------------------------
        
		//Determine in what folder on the file cabinet the invoice will be saved
        var fileFolder = 24;
        var linkToFolder = '/app/common/media/mediaitemfolders.nl?folder=';
        fileFolder = function_fileFolder(startDisplayYear,startDisplayMonth,output);
		linkToFolder += fileFolder;

//-------------------------- Look for invoices with no addressee and send email ----------------------------------------------------------------------------
	/*
	var addresseeColumn = new Array();
	var addresseeFilter = new Array();
		addresseeFilter.push(new nlobjSearchFilter('trandate',null,'within',startDate,endDate));
		if (arrCustomers !=  null){
			addresseeFilter.push(new nlobjSearchFilter('internalid','customer','anyof',arrCustomers));
		}
		if (listLocations.length > 0){
			addresseeFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', listLocations));
		}
	var addresseeResults = nlapiSearchRecord('invoice','customsearch_clgx_ci_no_addressee',addresseeFilter,addresseeColumn);
		
	if (addresseeResults != null && addresseeResults.length>0){
		var emailSubjectAddresse = 'ALERT - Invoices excluded from consolidation batch #' + consolidateID + ' (missing billing addressee).';
   		var emailBodyAddresse = '';
   		emailBodyAddresse += '<table>';
   		emailBodyAddresse += '<tr><td>Invoices with no billing addressee</td></tr>';
		
		for ( var i = 0; addresseeResults != null && i < addresseeResults.length; i++ ) {
	    	var addresseeResult = addresseeResults[i];
	    	var invoiceID = addresseeResult.getValue('internalid',null,'GROUP');
	    	var invoiceNbr = addresseeResult.getValue('number',null,'GROUP');
				emailBodyAddresse += '<tr>';
				emailBodyAddresse += '<td>';
				emailBodyAddresse += '<a href=https://system.na2.netsuite.com/app/accounting/transactions/custinvc.nl?id=' + invoiceID + '>' + invoiceNbr + '</a>';
				emailBodyAddresse += '</td>';
				emailBodyAddresse += '</tr>';
		}
		emailBodyAddresse += '</table>';
		nlapiSendEmail(2406, 12073,emailSubjectAddresse,emailBodyAddresse,null,null,null,null); // send to Kristen
		//nlapiSendEmail(2406, 71418,emailSubjectAddresse,emailBodyAddresse,null,null,null,null);  // send to Dan
	}
	*/
//-------------------------- 0 Begin Customers Batch only Loop  ----------------------------------------------------------------------------

	var batchColumn = new Array();
		batchColumn.push(new nlobjSearchColumn('internalid','customer','GROUP').setSort(false));
	var batchFilter = new Array();
		batchFilter.push(new nlobjSearchFilter('trandate',null,'within',startDate,endDate));
		batchFilter.push(new nlobjSearchFilter('mainline',null,'is','T'));
		batchFilter.push(new nlobjSearchFilter('taxline',null,'is','F'));
		batchFilter.push(new nlobjSearchFilter('custbody_consolidate_inv_id',null,'anyof','@NONE@'));
		batchFilter.push(new nlobjSearchFilter('custbody_consolidate',null,'is','T'));
		if (arrCustomers !=  null){
			batchFilter.push(new nlobjSearchFilter('internalid','customer','anyof',arrCustomers));
		}
		if (listLocations.length > 0){
			batchFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', listLocations));
		}
	var batchResult = nlapiSearchRecord('invoice',null,batchFilter,batchColumn);
	
	for(var h=0; batchResult != null && h<batchResult.length; h++ ){
        
//--------------------------  before starting generating this customer invoices verify if there are enough points left to complete - if not re-schedule the script

    	var startExec = moment(); // time when each loop is starting
    	var execMinutes = (startExec.diff(startScript)/60000).toFixed(1); // execution time in minutes
    	
		if ( (context.getRemainingUsage() <= 5000 || execMinutes > 50) && (h+1) < batchResult.length ){
            
			outoftheloop = 1;
			
            var params = new Array();
                params['custscript_consolidate_locations'] = arrLocations;
                params['custscript_consolidate_customers'] = arrCustomers;
                params['custscript_consolidate_month'] = startMonth;
                params['custscript_consolidate_year'] = startYear;
                params['custscript_consolidated_month_display'] = startDisplayMonth;
                params['custscript_consolidated_year_display'] = startDisplayYear;
                params['custscript_consolidated_rec_id'] = consolidateID;
                params['custscript_pdf_output_to'] = output;
                                        
            var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId(),params);
            
         // send email report for this batch, before being rescheduled
            reportSubject += ' - (Rescheduled)';
    		reportBody += '</table>';
    		
    		clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_ci_batch_address", reportSubject, reportBody);
    		
    		/*nlapiSendEmail(12827,71418,reportSubject,reportBody,null,null,null,null,true);
    		nlapiSendEmail(12827,206211,reportSubject,reportBody,null,null,null,null,true);
    		
    		nlapiSendEmail(12827,12073,reportSubject,reportBody,null,null,null,null,true);
    		nlapiSendEmail(12827,336456,reportSubject,reportBody,null,null,null,null,true);
    		nlapiSendEmail(12827,2406,reportSubject,reportBody,null,null,null,null,true);*/
            
            if(status == 'QUEUED'){
                nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and re-schedule it.');
        		//nlapiSendEmail(12827,71418,'Consolidated Invoices Status','Finished scheduled script due to usage limit and re-schedule it',null,null,null,null);
			    break;
            }
            else if(status == 'INQUEUE'){
                nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and because the script is already INQUEUE.');
                //nlapiSendEmail(12827,71418,'Consolidated Invoices Status','Finished scheduled script due to usage limit and because the script is already INQUEUE.',null,null,null,null);
			    break;
            }
            else if(status == 'INPROGRESS'){
                nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and because the script is already INPROGRESS.');
                //nlapiSendEmail(12827,71418,'Consolidated Invoices Status','Finished scheduled script due to usage limit and because the script is already INPROGRESS.',null,null,null,null);
			    break;
            }
            else if(status == 'SCHEDULED'){
                nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and because the script is already SCHEDULED.');
                //nlapiSendEmail(12827,71418,'Consolidated Invoices Status','Finished scheduled script due to usage limit and because the script is already SCHEDULED.',null,null,null,null);
			    break;
            }
            else  {
                nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage limit and other reason.');
                //nlapiSendEmail(12827,71418,'Consolidated Invoices Status','Finished scheduled script due to usage limit and other reason.',null,null,null,null);
			    break;
            }
        }
	            
//-------------------------- 1 Begin Consolidated Invoice Section & Loop (group/split by Customer/Consolidate Location/Currency/Addressee)  ----------------------------------------------------------------------------

		var batchCustomerId = batchResult[h].getValue('internalid','customer','GROUP');
		
		if(nlapiLookupField('customer', batchResult[h].getValue('internalid','customer','GROUP'), 'billaddressee') == ''){
			useAddressee = 'T';
		}

        var batchIndex = h + 1;
        nlapiLogExecution('DEBUG','Start Consolidating Invoices for Customer: ','Start Consolidating Invoices for Customer ' + batchIndex + ' of ' + batchResult.length + ' --------------------------|');
       
        
		var customerColumn = new Array();
			customerColumn.push(new nlobjSearchColumn('internalid','customer','GROUP').setSort(false));
			customerColumn.push(new nlobjSearchColumn('entityid','customer','GROUP'));
			customerColumn.push(new nlobjSearchColumn('currency',null,'GROUP'));
			customerColumn.push(new nlobjSearchColumn('custbody_clgx_consolidate_locations',null,'GROUP'));
			if(useAddressee == 'T'){
				customerColumn.push(new nlobjSearchColumn('billaddressee',null,'GROUP'));
			}
		var customerFilter = new Array();
			customerFilter.push(new nlobjSearchFilter('trandate',null,'within',startDate,endDate));
			customerFilter.push(new nlobjSearchFilter('mainline',null,'is','T'));
			customerFilter.push(new nlobjSearchFilter('taxline',null,'is','F'));
			customerFilter.push(new nlobjSearchFilter('custbody_consolidate_inv_id',null,'anyof','@NONE@'));
			customerFilter.push(new nlobjSearchFilter('custbody_consolidate',null,'is','T'));
			customerFilter.push(new nlobjSearchFilter('internalid','customer','anyof',batchCustomerId));
			if(useAddressee == 'T'){
				customerFilter.push(new nlobjSearchFilter('billaddressee',null,'isnot',''));
			}
			if (arrCustomers !=  null){
				customerFilter.push(new nlobjSearchFilter('internalid','customer','anyof',arrCustomers));
			}
			if (listLocations.length > 0){
				customerFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', listLocations));
			}
		var customerResult = nlapiSearchRecord('invoice',null,customerFilter,customerColumn);
        
		// customer/invoice loop 
       for(var i=0; customerResult != null && i<customerResult.length; i++){
    	   
    	   var startUsageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
    	   
//--------------------------  before starting generating this customer invoice verify if there are enough points left to complete - if not re-schedule the script

			 // initialize invoice totals and arrays
			var sTotalInvoice = 0; 
			var gstInvoice = 0; 
			var pstInvoice = 0; 
			var taxInvoice = 0; 
			var totalInvoice = 0;
			
			var arrInvoiceIds = new Array();
			var arrInvoiceNbrs = new Array();
			var arrOrderNbr = new Array();
			var arrOrderIds = new Array();
			var arrCPIRate = new Array();
       		
	        var arrBillingDates  = getDateRange(startDisplayMonth,startDisplayYear);
	        var startBillingDate = arrBillingDates[0];
	        var endBillingDate   = arrBillingDates[1];
	
            //get customer details from the search
            var customerId = customerResult[i].getValue('internalid','customer','GROUP');
            var customer = customerResult[i].getValue('entityid','customer','GROUP');
            var stCurrency = customerResult[i].getValue('currency',null,'GROUP');
            var stLocation = customerResult[i].getValue('custbody_clgx_consolidate_locations',null,'GROUP');
            var stLocationName = customerResult[i].getText('custbody_clgx_consolidate_locations',null,'GROUP');
            
			if(useAddressee == 'T'){
				var stAddrressee = customerResult[i].getValue('billaddressee',null,'GROUP');
			}
            
    		//Determine currency to display
    		var currencyType = '';
			if (stCurrency == 1){
				currencyType = 'USD';
			}
			else if (stCurrency == 3){
				currencyType = 'CDN';
			}
			else{}

            nlapiLogExecution('DEBUG','Start Consolidating Customer: ','Start Consolidating Customer ' + customer + ' |');
           
            // load customer record to get more details
			var recCustomer = nlapiLoadRecord('customer', customerId);
			var custCompName = recCustomer.getFieldValue('companyname');
			var payTerms = recCustomer.getFieldValue('terms');
			var accountNbr = recCustomer.getFieldValue('accountnumber');
			var languagePref = recCustomer.getFieldValue('language');
			var subsidiary = recCustomer.getFieldValue('subsidiary');
			
			//Determine the 'pay before' date paymentDue
			var dtPaymentDue = new Date();
			switch(parseInt(payTerms)) {
				case 1:
				  dtPaymentDue = nlapiAddDays(nlapiStringToDate(startBillingDate), 15);
				  break;
				case 2:
				  dtPaymentDue = nlapiAddDays(nlapiStringToDate(startBillingDate), 30);
				  break;
				case 3:
				  dtPaymentDue = nlapiAddDays(nlapiStringToDate(startBillingDate), 60);
				  break;
				case 7:
				  dtPaymentDue = nlapiAddDays(nlapiStringToDate(startBillingDate), 45);
				  break;
				case 8:
				  dtPaymentDue = nlapiAddDays(nlapiStringToDate(startBillingDate), 40);
				  break;
				default:
				  dtPaymentDue = nlapiStringToDate(startBillingDate);
			}
			
//--------------------------Language Labels----------------------------------------------------------------------------
		    	if(languagePref == 'en_US' || languagePref == 'en'){
		    		var LBL001 = 'Services Details';
		    		var LBL002 = 'CATEGORY : ';
		    		var LBL003 = 'Order';
		    		var LBL004 = 'Item';
		    		var LBL005 = 'Description';
		    		var LBL006 = 'Qty';
		    		var LBL007 = 'Rate';
		    		var LBL008 = 'Amount';
		    		var LBL009 = 'Subtotal : ';
		    		var LBL010 = 'Subtotal';
		    		var LBL011 = 'Total';
		    		var LBL012 = 'Orders with a new CPI Rate effective ';
		    		if(stLocation == 20){ // If Vancouver different label
		    			var LBL013 = 'GST';
		    		}
		    		else if(stLocation == 25 || stLocation == 26 || stLocation == 27 || stLocation == 28){ // if Jacksonville or Columbus
		    			var LBL013 = 'Sales Tax';
		    		}
		    		else{
		    			var LBL013 = 'GST/HST';
		    		}
		    		if(stLocation == 24){ // Montreal
		    			var LBL014 = 'QST';
		    		}
		    		else{
		    			var LBL014 = 'PST';
		    		}
		    		var LBL015 = 'Account Number';
		    		var LBL016 = 'Total Services';
		    		var LBL017 = 'TOTAL SERVICES';
		    		var LBL018 = 'REFERENCE DISCOUNTS';
		    		var LBL019 = 'Service Discount';
		    	}
		    	else{
		    		var LBL001 = 'D&eacute;tails des Services';
		    		var LBL002 = 'CAT&Eacute;GORIE : ';
		    		var LBL003 = 'Commande';
		    		var LBL004 = 'Produit';
		    		var LBL005 = 'Description';
		    		var LBL006 = 'Qt&eacute;';
		    		var LBL007 = 'Prix';
		    		var LBL008 = 'Total';
		    		var LBL009 = 'Sous-total : ';
		    		var LBL010 = 'Sous-total';
		    		var LBL011 = 'Total';
		    		var LBL012 = 'Contrat de service avec nouveau taux (IPC) en vigueur en date du ';
		    		var LBL013 = 'TPS/TVH';
		    		var LBL014 = 'TVQ';
		    		var LBL015 = 'No du compte';
		    		var LBL016 = 'Total Services';
		    		var LBL017 = 'TOTAL SERVICES';
		    		var LBL018 = 'REFERENCE DISCOUNTS';
		    		var LBL019 = 'Remise sur les Services';
		    	}
		    	
// start look for the address to put on the invoice and build HTML address section ---------------------------------------------------------------------------------------------------
	    		if(useAddressee == 'T'){
					// loop the addresses of the customer and look for the right addressee and get the address
	    			var countAddress = '0';
					var stNbrItems = recCustomer.getLineItemCount('addressbook');
					for (var a = 0; a < parseInt(stNbrItems); a++) {
						var currAddressee = recCustomer.getLineItemValue('addressbook', 'addressee', a + 1);
						if (currAddressee == stAddrressee){
							var addressID = recCustomer.getLineItemValue('addressbook', 'id', a + 1);
							var billAddress1 = recCustomer.getLineItemValue('addressbook', 'addr1', a + 1);
							var billAddress2 = recCustomer.getLineItemValue('addressbook', 'addr2', a + 1);
							var billAddress3 = recCustomer.getLineItemValue('addressbook', 'addr3', a + 1);
							var billAddressee = recCustomer.getLineItemValue('addressbook', 'addressee', a + 1);
							var billAttention = recCustomer.getLineItemValue('addressbook', 'attention', a + 1);
							var billCity = recCustomer.getLineItemValue('addressbook', 'city', a + 1);
							var billCountry = recCustomer.getLineItemValue('addressbook', 'country', a + 1);
							var billState = recCustomer.getLineItemValue('addressbook', 'state', a + 1);
							var billZipcode = recCustomer.getLineItemValue('addressbook', 'zip', a + 1);
							countAddress = '1';
						}
					}
					if(countAddress == '0'){
						var emailSubjectAddressee = 'ALERT - Customer - ' + customer + ' - invoice(s) without an address ( different addressee on invoice then on customer record.)';
						var emailBodyAddressee = '';
						
						clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_ci_batch_address", emailSubjectAddressee, emailBodyAddressee);
						
						/*nlapiSendEmail(2406, 12073,emailSubjectAddressee,emailBodyAddressee,null,null,null,null,true); // send to Kristen
						nlapiSendEmail(12827,206211,emailSubjectAddressee,emailBodyAddressee,null,null,null,null,true);
						nlapiSendEmail(12827,336456,emailSubjectAddressee,emailBodyAddressee,null,null,null,null,true);
						nlapiSendEmail(2406, 71418,emailSubjectAddressee,emailBodyAddressee,null,null,null,null,true);*/  // send to Dan
					}
				}
	    		else{
					// loop the addresses of the customer and look the default billing address
					var stNbrItems = recCustomer.getLineItemCount('addressbook');
					for (var a = 0; a < parseInt(stNbrItems); a++) {
						var isDefaultBilling = recCustomer.getLineItemValue('addressbook', 'defaultbilling', a + 1);
						if (isDefaultBilling == 'T'){
							var addressID = recCustomer.getLineItemValue('addressbook', 'id', a + 1);
							var billAddress1 = recCustomer.getLineItemValue('addressbook', 'addr1', a + 1);
							var billAddress2 = recCustomer.getLineItemValue('addressbook', 'addr2', a + 1);
							var billAddress3 = recCustomer.getLineItemValue('addressbook', 'addr3', a + 1);
							var billAddressee = recCustomer.getLineItemValue('addressbook', 'addressee', a + 1);
							var billAttention = recCustomer.getLineItemValue('addressbook', 'attention', a + 1);
							var billCity = recCustomer.getLineItemValue('addressbook', 'city', a + 1);
							var billCountry = recCustomer.getLineItemValue('addressbook', 'country', a + 1);
							var billState = recCustomer.getLineItemValue('addressbook', 'state', a + 1);
							var billZipcode = recCustomer.getLineItemValue('addressbook', 'zip', a + 1);
						}
					}
	    		}
				var invoiceNbr = consolidateID + '.' + addressID; // this will be the invoice number
				
		
				//build html address section
				var tablehtmlAddress = '<table cellpadding="0" border="0" table-layout="fixed" class="full">';
					tablehtmlAddress += 	'<tr>';
					tablehtmlAddress += 		'<td class="address">';
					tablehtmlAddress += 			'<b>' + nlapiEscapeXML(custCompName) + '</b><br/>';
					if (billAddressee != null && billAddressee != '' && billAddressee != '- None -') {
						tablehtmlAddress += nlapiEscapeXML(billAddressee) + '<br/>';
					}
					if (billAttention != null && billAttention != '' && billAttention != '- None -') {
						tablehtmlAddress += nlapiEscapeXML(billAttention) + '<br/>';
					}
					tablehtmlAddress += nlapiEscapeXML(billAddress1) + '<br/>';
					if (billAddress2 != null && billAddress2 != '' && billAddress2 != '- None -') {
						tablehtmlAddress += nlapiEscapeXML(billAddress2) + '<br/>';
					}
					if (billAddress3 != null && billAddress3 != '' && billAddress3 != '- None -') {
						tablehtmlAddress += nlapiEscapeXML(billAddress3) + '<br/>';
					}
					if (billCity != null && billCity != '' && billCity != '- None -') {
						tablehtmlAddress += nlapiEscapeXML(billCity);
					}
					if (billState != null && billState != '' && billState != '- None -') {
						tablehtmlAddress += ', ' + nlapiEscapeXML(billState);
					}
					if (billZipcode != null && billZipcode != '' && billZipcode != '- None -') {
						tablehtmlAddress += ' ' + nlapiEscapeXML(billZipcode);
					}
					tablehtmlAddress += ' <br/>';
					if (billCountry != null && billCountry != '' && billCountry != '- None -') {
						tablehtmlAddress += nlapiEscapeXML(billCountry);
					}
					
					tablehtmlAddress += 		'</td>';
					tablehtmlAddress += 	'</tr>';
					tablehtmlAddress += '</table>';
// end look for the address to put on the invoice and build HTML address section ---------------------------------------------------------------------------------------------------

            	/*
				var htmlAccountNbr = '';
				if (accountNbr != ''){
					htmlAccountNbr += '<tr>';
					htmlAccountNbr += '<td class="half-content r-border-zero common-padding">' + LBL015 + ':</td>';
					htmlAccountNbr += '<td class="half-content l-border-zero common-padding">' + accountNbr + '</td>';
					htmlAccountNbr += '</tr>';
				}
				*/

//-------------------------- 2 Begin Location Section & Loop----------------------------------------------------------------------------

			/*
	        var locationColumn = new Array();
        		locationColumn.push(new nlobjSearchColumn('address1','location','GROUP').setSort(false));
        		locationColumn.push(new nlobjSearchColumn('location',null,'GROUP').setSort(false));
    		var locationFilter = new Array();
	    		locationFilter.push(new nlobjSearchFilter('trandate',null,'within',startDate,endDate));
	    		locationFilter.push(new nlobjSearchFilter('internalid','customer','anyof',customerId));
	    		locationFilter.push(new nlobjSearchFilter('custbody_consolidate',null,'is','T'));
	    		locationFilter.push(new nlobjSearchFilter('custbody_consolidate_inv_id',null,'anyof','@NONE@'));
	    		locationFilter.push(new nlobjSearchFilter('taxline',null,'is','F'));
	    		//locationFilter.push(new nlobjSearchFilter('mainline',null,'is','F'));
	    		locationFilter.push(new nlobjSearchFilter('memorized',null,'is','F'));
	    		locationFilter.push(new nlobjSearchFilter('currency',null,'is',stCurrency));
	    		locationFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'is',stLocation));
	    		if(useAddressee == 'T'){
					locationFilter.push(new nlobjSearchFilter('billaddressee',null,'is',stAddrressee));
				}
				if (listLocations.length > 0){
					locationFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', listLocations));
				}
    		var locationResults = nlapiSearchRecord('invoice',null,locationFilter,locationColumn);
    		*/
					
            var tablehtml = '';
            tablehtml =  '<table cellpadding="1" class="profile" align="center" table-layout="fixed">';
            tablehtml += '<tr>';
            tablehtml +=     '<td width="7%">&nbsp;</td>';
            tablehtml +=     '<td width="10%">&nbsp;</td>';
            tablehtml +=     '<td width="25%">&nbsp;</td>';
            tablehtml +=     '<td width="28%">&nbsp;</td>';
            tablehtml +=     '<td width="10%">&nbsp;</td>';
            tablehtml +=     '<td width="10%">&nbsp;</td>';
            tablehtml +=     '<td width="10%">&nbsp;</td>';
            tablehtml += '</tr>';
            
    		// location loop
    		//for(var j=0; locationResults != null && j<locationResults.length; j++) {
    		
	    		// initialize location total
	            var sTotalLocation = 0;
    			var gstLocation = 0;
    			var pstLocation = 0;
    			var taxLocation = 0;
            
    			//var locationAddress = locationResults[j].getValue('address1','location','GROUP');
    			//var locationAddressID = locationResults[j].getValue('location',null,'GROUP');

	    		// Build the 'Services Details' head
	                tablehtml += '<tr>';
	                //tablehtml +=        '<td colspan="7"><h2>' + LBL001 + ' - ' + locationAddress + '</h2></td>';
	                tablehtml +=        '<td colspan="7"><h2>' + LBL001 + '</h2></td>';
	                tablehtml += '</tr>';
	                tablehtml += '<tr><td colspan="7">&nbsp;</td></tr>';

//-------------------------- 3 Begin Category Section & Loop ----------------------------------------------------------------------------
		
                //search for the invoices of customer grouped by item category
	        	var itemCategoryColumn = new Array();
	        		itemCategoryColumn.push(new nlobjSearchColumn('custcol_cologix_invoice_item_category',null,'GROUP'));
	    		var itemCategoryFilter = new Array();
		    		itemCategoryFilter.push(new nlobjSearchFilter('trandate',null,'within',startDate,endDate));
		    		itemCategoryFilter.push(new nlobjSearchFilter('internalid','customer','anyof',customerId));
		    		itemCategoryFilter.push(new nlobjSearchFilter('custcol_cologix_invoice_item_category',null,'noneof','@NONE@'));
		    		itemCategoryFilter.push(new nlobjSearchFilter('custbody_consolidate',null,'is','T'));
		    		itemCategoryFilter.push(new nlobjSearchFilter('custbody_consolidate_inv_id',null,'anyof','@NONE@'));
		    		itemCategoryFilter.push(new nlobjSearchFilter('taxline',null,'is','F'));
		    		//itemCategoryFilter.push(new nlobjSearchFilter('mainline',null,'is','F'));
		    		itemCategoryFilter.push(new nlobjSearchFilter('memorized',null,'is','F'));
		    		itemCategoryFilter.push(new nlobjSearchFilter('currency',null,'is',stCurrency));
		    		itemCategoryFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'is',stLocation));
		    		if(useAddressee == 'T'){
		    			itemCategoryFilter.push(new nlobjSearchFilter('billaddressee',null,'is',stAddrressee));
					}
		    		//itemCategoryFilter.push(new nlobjSearchFilter('location',null,'is',locationAddressID));
					if (listLocations.length > 0){
						itemCategoryFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', listLocations));
					}
		    		//itemCategoryFilter.push(new nlobjSearchFilter('location',null,'anyof',arrLocations));
	    		var itemCategoryResults = nlapiSearchRecord('invoice',null,itemCategoryFilter,itemCategoryColumn);

// category loop --------------------------------------------------------------------------------------------------------------------------------------------------------
                    for(var y=0; itemCategoryResults != null && y<itemCategoryResults.length; y++) {
                    	
                    	var sTotalCategory = 0;
                    	 
                        var itemCategoryValue = itemCategoryResults[y].getValue('custcol_cologix_invoice_item_category',null,'GROUP');
                        var itemCategoryText  = itemCategoryResults[y].getText('custcol_cologix_invoice_item_category',null,'GROUP');
                        
                        // Category Title depending on language
                        var categoryText = '';
                        if(languagePref == 'en_US' || languagePref == 'en'){
                        	categoryText = itemCategoryText.toUpperCase();
                        }
                        else{
                        	categoryText = function_FrenchItemCategory(itemCategoryValue);
                        	if (categoryText == null || categoryText == ''){
                        		categoryText = itemCategoryText.toUpperCase();
                        	}
                        }

                            tablehtml += '<tr>';
                            tablehtml +=     '<td colspan="7">' + LBL002 + categoryText + '</td>';
                            tablehtml += '</tr>';
                            tablehtml += '<tr><td colspan="7">&nbsp;</td></tr>';
                            tablehtml += '<tr>';
                            tablehtml +=     '<td class="cool"><b>Ref#</b></td>';
                            tablehtml +=     '<td class="cool"><b>' + LBL003 + '#</b></td>';
                            tablehtml +=     '<td class="cool"><b>' + LBL004 + '</b></td>';
                            tablehtml +=     '<td class="cool" align="left"><b>' + LBL005 + '</b></td>';
                            tablehtml +=     '<td class="cool" align="right"><b>' + LBL006 + '</b></td>';
                            tablehtml +=     '<td class="cool" align="right"><b>' + LBL007 + '</b></td>';
                            tablehtml +=     '<td class="cool" align="right"><b>' + LBL008 + '</b></td>';
                            tablehtml += '</tr>';
                        
//-------------------------- 4 Begin Items Section & Loops ----------------------------------------------------------------------------

	                	var itemColumn = new Array();
	            		var itemFilter = new Array();
	            			itemFilter.push(new nlobjSearchFilter('trandate',null,'within',startDate,endDate));
	            			itemFilter.push(new nlobjSearchFilter('internalid','customer','anyof',customerId));
	            			itemFilter.push(new nlobjSearchFilter('custcol_cologix_invoice_item_category',null,'anyof',itemCategoryValue));
	            			itemFilter.push(new nlobjSearchFilter('currency',null,'is',stCurrency));
	            			itemFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'is',stLocation));
	    		    		if(useAddressee == 'T'){
	    		    			itemFilter.push(new nlobjSearchFilter('billaddressee',null,'is',stAddrressee));
	    					}
	    					if (listLocations.length > 0){
	    						itemFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', listLocations));
	    					}
	    					var itemResults = nlapiSearchRecord('invoice','customsearch_clgx_ci_items',itemFilter,itemColumn);
	            		
                        for(var x=0; itemResults != null && x<itemResults.length; x++){

                        	var myInvoiceFrom = itemResults[x].getText('createdfrom').replace(/\Service Order #/g,"");
                    		var myNextAADate = itemResults[x].getValue('custbody_clgx_next_aa_date');
                    		var contractCycle = itemResults[x].getValue('custbody_clgx_contract_cycle');
                    		var createdFromSO = itemResults[x].getValue('createdfrom');
                    		
                    		var columns = itemResults[x].getAllColumns();
                    		
                    		var itemRate = itemResults[x].getValue('rate');
                    		var itemCalcRate = itemResults[x].getValue(columns[20]);
                    		var itemQty = itemResults[x].getValue('quantity');
                    		var itemAmount = itemResults[x].getValue('amount');
                    		var itemAmountFX = itemResults[x].getValue('fxamount');
                    		
							var contractStartDate = '';
                    		if(createdFromSO != '' && createdFromSO != null){
                    			contractStartDate = nlapiLookupField('salesorder', createdFromSO, 'custbody_cologix_so_contract_start_dat');
                    		}
                    		if (contractStartDate != '' && contractStartDate != null){
                        		var startDateMom = new moment(nlapiStringToDate(startDate));
                        		var contractStartDateMom = new moment(nlapiStringToDate(contractStartDate));
                        		contractStartDateMom.startOf('month');
                        		var difMonths = Math.ceil(startDateMom.diff(contractStartDateMom, 'months', true));
                    		}
                    		else{
                    			var difMonths = 0;
                    		}

                    		var myCPIRate = itemResults[x].getValue('custbody_cologix_annual_accelerator');
                    		var myLegacyOrderNumber = itemResults[x].getValue('custbody_cologix_legacy_so');
                    		
							if (myLegacyOrderNumber == '' || myLegacyOrderNumber == null){ // if no Legacy Order Number take the value of Order Number
								myLegacyOrderNumber = myInvoiceFrom;
							}
							
							// build Order# & CPIRate arrays
							if (!inArray(myLegacyOrderNumber,arrOrderNbr) && // if not already in array
								(myCPIRate != null && myCPIRate != '') && // and if CPI exist
								(nlapiStringToDate(myNextAADate) != '' && nlapiStringToDate(myNextAADate) != null) // and if Next AA date exist
								){
								if ((parseInt(nlapiStringToDate(myNextAADate).getMonth()) + 1 == parseInt(startDisplayMonth))  // and if 'Consolidated Month' + 1 is the same as the month of the 'Next Anual Accelerator Date'
										&& (parseInt(difMonths) > 11)  // there are more than 12 months since contract start date
										//&& (parseInt(nlapiStringToDate(myNextAADate).getFullYear()) == parseInt(startDisplayYear))  // and if 'Consolidated Year' is the same as the year of 'Next Anual Accelerator Date'
									){
									arrOrderNbr.push(myLegacyOrderNumber);
									arrCPIRate.push(myCPIRate);
									arrOrderIds.push(myInvoiceFrom);
								}	
							}
							
                            //get the unique invoice IDs in array
                            var invid = itemResults[x].getValue('internalid');
                            var invnbr = itemResults[x].getValue('tranid');
                            if(!inArray(invid,arrInvoiceIds)){
                                arrInvoiceIds.push(invid);
                                arrInvoiceNbrs.push(invnbr);
                            }
                            
                            // add item on invoice
                            tablehtml += '<tr>';
                            tablehtml +=     '<td>'+ nlapiEscapeXML(itemResults[x].getValue('tranid')) +'</td>';
                            tablehtml +=     '<td>'+ nlapiEscapeXML(myLegacyOrderNumber) +'</td>';
                            if(languagePref == 'en_US' || languagePref == 'en'){
                                tablehtml +=     '<td align="left">'+ nlapiEscapeXML(itemResults[x].getValue('displayname', 'item')) +'</td>';
                            }
                            else{
                            	var myItemRecord = nlapiLoadRecord('serviceitem', itemResults[x].getValue('item'));
                            	var frenchName = myItemRecord.getLineItemValue('translations', 'displayname', 3);
                            	if (frenchName == null || frenchName == ''){
                            	    tablehtml +=     '<td align="left">'+ nlapiEscapeXML(itemResults[x].getValue('displayname', 'item')) +'</td>';
                            	}
                            	else{
                            		tablehtml +=     '<td align="left">'+ nlapiEscapeXML(frenchName) +'</td>';
                            	}
                            }
                            tablehtml +=     '<td>'+ nlapiEscapeXML(itemResults[x].getValue('memo')) +'</td>';
                            tablehtml +=     '<td align="right">'+ nlapiEscapeXML((parseFloat(itemQty).toFixed(2))) +'</td>';
                            
                            
                            if(subsidiary == 6 && stCurrency == 1){ // Canada subsidiary and $US - use itemCalcRate instead of itemRate
                                tablehtml +=     '<td align="right">$'+ nlapiEscapeXML(addCommas(parseFloat(itemCalcRate).toFixed(2))) +'</td>';
                                tablehtml +=     '<td align="right">$'+ nlapiEscapeXML(addCommas((parseFloat(itemQty) * parseFloat(itemCalcRate)).toFixed(2))) +'</td>';
                                
                                sTotalCategory += parseFloat(parseFloat(itemQty) * parseFloat(itemCalcRate));
                                sTotalLocation += parseFloat(parseFloat(itemQty) * parseFloat(itemCalcRate));
                                sTotalInvoice += parseFloat(parseFloat(itemQty) * parseFloat(itemCalcRate));
                            	
                            }
                            else{
                                tablehtml +=     '<td align="right">$'+ nlapiEscapeXML(addCommas(parseFloat(itemRate).toFixed(2))) +'</td>';
                                tablehtml +=     '<td align="right">$'+ nlapiEscapeXML(addCommas((parseFloat(itemQty) * parseFloat(itemRate)).toFixed(2))) +'</td>';
                                
                                sTotalCategory += parseFloat(parseFloat(itemQty) * parseFloat(itemRate));
                                sTotalLocation += parseFloat(parseFloat(itemQty) * parseFloat(itemRate));
                                sTotalInvoice += parseFloat(parseFloat(itemQty) * parseFloat(itemRate));
                            }
                            
                            tablehtml += '</tr>';
                        }
                        
//-------------------------- Add any category discounts ---------------------------------------------------------------------------- 
                        
                        if(itemCategoryValue == 8 || itemCategoryValue == 10){ // if category is Power or Space
                        	if (itemCategoryValue == 8){
                        		var arrDiscounts = [534];
                        	}
                        	else{
                        		var arrDiscounts = [535,551];
                        	}
		                	var categDiscountColumn = new Array();
			            	var categDiscountFilter = new Array();
		            			categDiscountFilter.push(new nlobjSearchFilter('trandate',null,'within',startDate,endDate));
		            			categDiscountFilter.push(new nlobjSearchFilter('internalid','customer','anyof',customerId));
		            			categDiscountFilter.push(new nlobjSearchFilter('item',null,'anyof',arrDiscounts));
		            			categDiscountFilter.push(new nlobjSearchFilter('currency',null,'is',stCurrency));
		            			categDiscountFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'is',stLocation));
		    		    		if(useAddressee == 'T'){
		    		    			categDiscountFilter.push(new nlobjSearchFilter('billaddressee',null,'is',stAddrressee));
		    					}
		    					if (listLocations.length > 0){
		    						categDiscountFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', listLocations));
		    					}
		    					var categDiscountResults = nlapiSearchRecord('invoice','customsearch_clgx_ci_discounts',categDiscountFilter,categDiscountColumn);
		            		
	                        for(var w=0; categDiscountResults != null && w<categDiscountResults.length; w++){

	                        	var myInvoiceFrom = categDiscountResults[w].getText('createdfrom').replace(/\Service Order #/g,"");
	                        	
	                    		var itemRate = categDiscountResults[w].getValue('rate');
	                    		var itemAmount = categDiscountResults[w].getValue('amount');
	                    		var itemAmountFX = categDiscountResults[w].getValue('fxamount');
	                    		
	                    		var myLegacyOrderNumber = categDiscountResults[w].getValue('custbody_cologix_legacy_so');
	                    		if (myLegacyOrderNumber == '' || myLegacyOrderNumber == null){ // if no Legacy Order Number take the value of Order Number
									myLegacyOrderNumber = myInvoiceFrom;
								}

	                    		//if(parseFloat(itemAmountFX) > 0){
	                    			
									// add category discount item to invoice
		                            tablehtml += '<tr>';
		                            tablehtml +=     '<td>'+ nlapiEscapeXML(categDiscountResults[w].getValue('tranid')) +'</td>';
		                            tablehtml +=     '<td>'+ nlapiEscapeXML(myLegacyOrderNumber) +'</td>';
		                            if(languagePref == 'en_US' || languagePref == 'en'){
		                                tablehtml +=     '<td>'+ nlapiEscapeXML(categDiscountResults[w].getValue('displayname', 'item')) +'</td>';
		                            }
		                            else{
		                            	var myItemRecord = nlapiLoadRecord('discountitem', categDiscountResults[w].getValue('item'));
		                            	var frenchName = myItemRecord.getLineItemValue('translations', 'displayname', 3);
		                            	if (frenchName == null || frenchName == ''){
		                            	    tablehtml +=     '<td>'+ nlapiEscapeXML(categDiscountResults[w].getValue('displayname', 'item')) +'</td>';
		                            	}
		                            	else{
		                            		tablehtml +=     '<td>'+ nlapiEscapeXML(frenchName) +'</td>';
		                            	}
		                            }
		                            tablehtml +=     '<td>'+ nlapiEscapeXML(categDiscountResults[w].getValue('memo')) +'</td>';
		                            tablehtml +=     '<td align="right">&nbsp;</td>'; 
		                            tablehtml +=     '<td align="right">$'+ nlapiEscapeXML(addCommas(parseFloat(itemAmountFX).toFixed(2))) +'</td>';
		                            tablehtml +=     '<td align="right">$'+ nlapiEscapeXML(addCommas(parseFloat(itemAmountFX).toFixed(2))) +'</td>';
		                            tablehtml += '</tr>';
		                            
		                            sTotalCategory += parseFloat(itemAmountFX);
		                            sTotalLocation += parseFloat(itemAmountFX);
		                            sTotalInvoice += parseFloat(itemAmountFX);
	                    		//}
	                        }
                        }
                        
//-------------------------- 4 End Item Section & Loop----------------------------------------------------------------------------

                        //Add Category Totals
						tablehtml += '<tr><td colspan="7">&nbsp;</td></tr>';
                        tablehtml += '<tr>';
                        tablehtml +=     '<td colspan="5" class="profile t-border"><b>' + LBL009 + categoryText + '</b></td>';
                        tablehtml +=     '<td colspan="2" align="right" class="profile t-border"><b>'+ nlapiEscapeXML('$' + addCommas(sTotalCategory.toFixed(2))) +'</b></td>';
                        tablehtml += '</tr>';
                        tablehtml += '<tr><td colspan="7">&nbsp;</td></tr>';
                    }
                    
//-------------------------- Add any reference discounts ---------------------------------------------------------------------------- 
                    
                    	var invoiceDiscountColumn = new Array();
	            		var invoiceDiscountFilter = new Array();
	            			invoiceDiscountFilter.push(new nlobjSearchFilter('trandate',null,'within',startDate,endDate));
	            			invoiceDiscountFilter.push(new nlobjSearchFilter('internalid','customer','anyof',customerId));
	            			invoiceDiscountFilter.push(new nlobjSearchFilter('item',null,'anyof',533));
	            			invoiceDiscountFilter.push(new nlobjSearchFilter('currency',null,'is',stCurrency));
	            			invoiceDiscountFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'is',stLocation));
	    		    		if(useAddressee == 'T'){
	    		    			invoiceDiscountFilter.push(new nlobjSearchFilter('billaddressee',null,'is',stAddrressee));
	    					}
	    					if (listLocations.length > 0){
	    						invoiceDiscountFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', listLocations));
	    					}
	    				var invoiceDiscountResults = nlapiSearchRecord('invoice','customsearch_clgx_ci_discounts',invoiceDiscountFilter,invoiceDiscountColumn);

	    				// check here if sum of fxamounts is 0 to avoid building the header
	            		if (invoiceDiscountResults != null){

                            tablehtml += '<tr>';
                            tablehtml +=     '<td colspan="7">' + LBL002 + ' ' + LBL018 + '</td>';
                            tablehtml += '</tr>';
                            tablehtml += '<tr><td colspan="7">&nbsp;</td></tr>';
                            tablehtml += '<tr>';
                            tablehtml +=     '<td class="cool"><b>Ref#</b></td>';
                            tablehtml +=     '<td class="cool"><b>' + LBL003 + '#</b></td>';
                            tablehtml +=     '<td class="cool"><b>' + LBL004 + '</b></td>';
                            tablehtml +=     '<td class="cool" align="left"><b>' + LBL005 + '</b></td>';
                            tablehtml +=     '<td class="cool" align="right"><b>' + LBL006 + '</b></td>';
                            tablehtml +=     '<td class="cool" align="right"><b>' + LBL007 + '</b></td>';
                            tablehtml +=     '<td class="cool" align="right"><b>' + LBL008 + '</b></td>';
                            tablehtml += '</tr>';
		            		
                            var sTotalRefDiscounts = 0;
		            		for(var t=0; invoiceDiscountResults != null && t<invoiceDiscountResults.length; t++){
	
	                        	var myInvoiceFrom = invoiceDiscountResults[t].getText('createdfrom').replace(/\Service Order #/g,"");
	                        	
	                        	var itemRate = invoiceDiscountResults[t].getValue('rate');
	                    		var itemAmount = invoiceDiscountResults[t].getValue('amount');
	                    		var itemAmountFX = invoiceDiscountResults[t].getValue('fxamount');
	                    		
	                        	var myLegacyOrderNumber = invoiceDiscountResults[t].getValue('custbody_cologix_legacy_so');
	                    		if (myLegacyOrderNumber == '' || myLegacyOrderNumber == null){ // if no Legacy Order Number take the value of Order Number
									myLegacyOrderNumber = myInvoiceFrom;
								}
								
	                    		//if(parseFloat(itemAmountFX) > 0){

									//Add Reference Discount Item on Invoice
		                            tablehtml += '<tr>';
		                            tablehtml +=     '<td>'+ nlapiEscapeXML(invoiceDiscountResults[t].getValue('tranid')) +'</td>';
		                            tablehtml +=     '<td>'+ nlapiEscapeXML(myLegacyOrderNumber) +'</td>';
		                            if(languagePref == 'en_US' || languagePref == 'en'){
		                                tablehtml +=     '<td>'+ nlapiEscapeXML(invoiceDiscountResults[t].getValue('displayname', 'item')) +'</td>';
		                            }
		                            else{
		                            	var myItemRecord = nlapiLoadRecord('discountitem', invoiceDiscountResults[t].getValue('item'));
		                            	var frenchName = myItemRecord.getLineItemValue('translations', 'displayname', 3);
		                            	if (frenchName == null || frenchName == ''){
		                            	    tablehtml +=     '<td>'+ nlapiEscapeXML(invoiceDiscountResults[t].getValue('displayname', 'item')) +'</td>';
		                            	}
		                            	else{
		                            		tablehtml +=     '<td>'+ nlapiEscapeXML(frenchName) +'</td>';
		                            	}
		                            }
		                            tablehtml +=     '<td>'+ nlapiEscapeXML(invoiceDiscountResults[t].getValue('memo')) +'</td>';
		                            tablehtml +=     '<td align="right">&nbsp;</td>'; 
		                            tablehtml +=     '<td align="right">$'+ nlapiEscapeXML(addCommas(parseFloat(itemAmountFX).toFixed(2))) +'</td>';
		                            tablehtml +=     '<td align="right">$'+ nlapiEscapeXML(addCommas(parseFloat(itemAmountFX).toFixed(2))) +'</td>';
		                            tablehtml += '</tr>';
		                            
		                            sTotalLocation += parseFloat(itemAmountFX);
		                            sTotalInvoice += parseFloat(itemAmountFX);
		                            sTotalRefDiscounts += parseFloat(itemAmountFX);
		            			//}
	                        }
		            		//if(parseFloat(sTotalRefDiscounts) > 0){
								tablehtml += '<tr><td colspan="7">&nbsp;</td></tr>';
		                        tablehtml += '<tr>';
		                        tablehtml +=     '<td colspan="5" class="profile t-border"><b>' + LBL009 + ' ' + LBL018 + '</b></td>';
		                        tablehtml +=     '<td colspan="2" align="right" class="profile t-border"><b>'+ nlapiEscapeXML('$' + addCommas(sTotalRefDiscounts.toFixed(2))) +'</b></td>';
		                        tablehtml += '</tr>';
		                        tablehtml += '<tr><td colspan="7">&nbsp;</td></tr>';
	                        //}
	            		}

//-------------------------- Add first month free discounts ---------------------------------------------------------------------------- 
	            		
	            		var invoiceDiscountColumn = new Array();
	            		var invoiceDiscountFilter = new Array();
	            			invoiceDiscountFilter.push(new nlobjSearchFilter('trandate',null,'within',startDate,endDate));
	            			invoiceDiscountFilter.push(new nlobjSearchFilter('internalid','customer','anyof',customerId));
	            			invoiceDiscountFilter.push(new nlobjSearchFilter('item',null,'anyof',549));
	            			invoiceDiscountFilter.push(new nlobjSearchFilter('currency',null,'is',stCurrency));
	            			invoiceDiscountFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'is',stLocation));
	    		    		if(useAddressee == 'T'){
	    		    			invoiceDiscountFilter.push(new nlobjSearchFilter('billaddressee',null,'is',stAddrressee));
	    					}
	    					if (listLocations.length > 0){
	    						invoiceDiscountFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', listLocations));
	    					}
	    				var invoiceFirstMonthResults = nlapiSearchRecord('invoice','customsearch_clgx_ci_discounts',invoiceDiscountFilter,invoiceDiscountColumn);
	    				
	    				// check here if sum of fxamounts is 0 to avoid building the header

	            		if (invoiceFirstMonthResults != null){
	            			
	            			var discountsHeader = '';
	            			discountsHeader += '<tr>';
	            			discountsHeader +=     '<td colspan="7">' + LBL002 + ' ' + LBL019 + '</td>';
	            			discountsHeader += '</tr>';
	            			discountsHeader += '<tr><td colspan="7">&nbsp;</td></tr>';
	            			discountsHeader += '<tr>';
	            			discountsHeader +=     '<td class="cool"><b>Ref#</b></td>';
	            			discountsHeader +=     '<td class="cool"><b>' + LBL003 + '#</b></td>';
	            			discountsHeader +=     '<td class="cool"><b>' + LBL004 + '</b></td>';
	            			discountsHeader +=     '<td class="cool" align="left"><b>' + LBL005 + '</b></td>';
	            			discountsHeader +=     '<td class="cool" align="right"><b>' + LBL006 + '</b></td>';
	            			discountsHeader +=     '<td class="cool" align="right"><b>' + LBL007 + '</b></td>';
	            			discountsHeader +=     '<td class="cool" align="right"><b>' + LBL008 + '</b></td>';
	            			discountsHeader += '</tr>';
                            
                            var sTotalFirstMonthDiscounts = 0;
                            var discountsBody = '';
		            		for(var s=0; invoiceFirstMonthResults != null && s<invoiceFirstMonthResults.length; s++){
	
	                        	var myInvoiceFrom = invoiceFirstMonthResults[s].getText('createdfrom').replace(/\Service Order #/g,"");
	                        	
	                        	var itemRate = invoiceFirstMonthResults[s].getValue('rate');
	                    		var itemAmount = invoiceFirstMonthResults[s].getValue('amount');
	                    		var itemAmountFX = invoiceFirstMonthResults[s].getValue('fxamount');
	                    		
	                        	var myLegacyOrderNumber = invoiceFirstMonthResults[s].getValue('custbody_cologix_legacy_so');
	                    		if (myLegacyOrderNumber == '' || myLegacyOrderNumber == null){ // if no Legacy Order Number take the value of Order Number
									myLegacyOrderNumber = myInvoiceFrom;
								}
								
	                    		if(parseFloat(itemAmountFX) != 0){
									
	                    			discountsBody += '<tr>';
	                    			discountsBody +=     '<td>'+ nlapiEscapeXML(invoiceFirstMonthResults[s].getValue('tranid')) +'</td>';
	                    			discountsBody +=     '<td>'+ nlapiEscapeXML(myLegacyOrderNumber) +'</td>';
		                            if(languagePref == 'en_US' || languagePref == 'en'){
		                            	discountsBody +=     '<td>'+ nlapiEscapeXML(invoiceFirstMonthResults[s].getValue('displayname', 'item')) +'</td>';
		                            }
		                            else{
		                            	var myItemRecord = nlapiLoadRecord('discountitem', invoiceFirstMonthResults[s].getValue('item'));
		                            	var frenchName = myItemRecord.getLineItemValue('translations', 'displayname', 3);
		                            	if (frenchName == null || frenchName == ''){
		                            		discountsBody +=     '<td>'+ nlapiEscapeXML(invoiceFirstMonthResults[s].getValue('displayname', 'item')) +'</td>';
		                            	}
		                            	else{
		                            		discountsBody +=     '<td>'+ nlapiEscapeXML(frenchName) +'</td>';
		                            	}
		                            }
		                            discountsBody +=     '<td>'+ nlapiEscapeXML(invoiceFirstMonthResults[s].getValue('memo')) +'</td>';
		                            discountsBody +=     '<td align="right">&nbsp;</td>';
		                            discountsBody +=     '<td align="right">$'+ nlapiEscapeXML(addCommas(parseFloat(itemAmountFX).toFixed(2))) +'</td>';
		                            discountsBody +=     '<td align="right">$'+ nlapiEscapeXML(addCommas(parseFloat(itemAmountFX).toFixed(2))) +'</td>';
		                            discountsBody += '</tr>';
		                            
		                            sTotalLocation += parseFloat(itemAmountFX);
		                            sTotalInvoice += parseFloat(itemAmountFX);
		                            sTotalFirstMonthDiscounts += parseFloat(itemAmountFX);
	                    		}
	                        }
		            		if(parseFloat(sTotalFirstMonthDiscounts) != 0){
		            			
		            			var discountsFooter = '';
		            			discountsFooter += '<tr><td colspan="7">&nbsp;</td></tr>';
		            			discountsFooter += '<tr>';
		            			discountsFooter +=     '<td colspan="5" class="profile t-border"><b>' + LBL009 + ' ' + LBL019 + '</b></td>';
		            			discountsFooter +=     '<td colspan="2" align="right" class="profile t-border"><b>'+ nlapiEscapeXML('$' + addCommas(sTotalFirstMonthDiscounts.toFixed(2))) +'</b></td>';
		            			discountsFooter += '</tr>';
		            			discountsFooter += '<tr><td colspan="7">&nbsp;</td></tr>';
		                        
		            			tablehtml += discountsHeader;
		            			tablehtml += discountsBody;
		            			tablehtml += discountsFooter;
	            			}
	            		}
	            		
//-------------------------- 3 End Category Section & Loop----------------------------------------------------------------------------

					//build subtotal, taxes and total charges
	                	var taxesColumn = new Array();
	            		var taxesFilter = new Array();
		            		taxesFilter.push(new nlobjSearchFilter('trandate',null,'within',startDate,endDate));
		            		taxesFilter.push(new nlobjSearchFilter('entity',null,'anyof',customerId));
		            		taxesFilter.push(new nlobjSearchFilter('currency',null,'is',stCurrency));
		            		taxesFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'is',stLocation));
	    		    		if(useAddressee == 'T'){
	    		    			taxesFilter.push(new nlobjSearchFilter('billaddressee',null,'is',stAddrressee));
	    					}
		            		if (listLocations.length > 0){
	    						taxesFilter.push(new nlobjSearchFilter('custbody_clgx_consolidate_locations',null,'anyof', listLocations));
	    					}
	    					var taxesResults = nlapiSearchRecord('invoice','customsearch_clgx_ci_tax',taxesFilter,taxesColumn);
	            		
						
	            		tablehtml += '<tr><td colspan="7">&nbsp;</td></tr>';
	            		tablehtml += '<tr><td colspan="7">';
            			tablehtml +=  '<table class="profile" align="center" table-layout="fixed"><tr>';
            			tablehtml +=  '<td width="60%" class="profile t-border"><h4>' + LBL016 + '</h4></td>';            
            			tablehtml +=  '<td width="40%" class="profile t-border" align="right"><b>';
            			
	            		if (taxesResults != null) {
	            			
	    					var resultRow = taxesResults[0];
	    					var columns = resultRow.getAllColumns();

	            			gstLocation = resultRow.getValue(columns[2]);
	            			pstLocation = resultRow.getValue(columns[3]);
	            			flstateLocation = resultRow.getValue(columns[4]);
	            			flduvalLocation = resultRow.getValue(columns[5]);
	            			columbusLocation = resultRow.getValue(columns[6]);
	            			flpolkLocation = resultRow.getValue(columns[7]);
	            			flinternetLocation = resultRow.getValue(columns[8]);
	            			taxLocation = parseFloat(gstLocation) + parseFloat(pstLocation);
	            			
	            			if(stLocation == 25 || stLocation == 27){ // if Jacksonville 1 or 2
	            				fltaxLocation = parseFloat(flstateLocation) + parseFloat(flduvalLocation) + parseFloat(flinternetLocation);
	            			}
	            			if(stLocation == 28){ // if Lakeland
	            				fltaxLocation = parseFloat(flstateLocation) + parseFloat(flpolkLocation) + parseFloat(flinternetLocation);
	            			}
	            			
	                		gstInvoice += parseFloat(gstLocation); 
	                		pstInvoice += parseFloat(pstLocation); 
	                		taxInvoice += parseFloat(taxLocation); 
	                		
	                		if(stLocation == 25 || stLocation == 27){ // if Jacksonville 1 or 2
		            			var totalLocation = parseFloat(sTotalLocation) + parseFloat(flstateLocation) + parseFloat(flduvalLocation) + parseFloat(flinternetLocation);
	                		}
	                		else if(stLocation == 28){ // if Lakeland
		            			var totalLocation = parseFloat(sTotalLocation) + parseFloat(flstateLocation) + parseFloat(flpolkLocation) + parseFloat(flinternetLocation);
	                		}
	                		else if(stLocation == 26){ // if Columbus
		            			var totalLocation = parseFloat(sTotalLocation) + parseFloat(columbusLocation);
	                		}
	                		else{ // Canada
		            			var totalLocation = parseFloat(sTotalLocation) + parseFloat(gstLocation) + parseFloat(pstLocation);
	                		}
	                		
	                		
	                       
	                		totalInvoice += parseFloat(totalLocation);

	            			tablehtml +=  '<table class="profile" align="right" width="100%">';
							tablehtml += '<tr>';
								tablehtml += '<td><b>&nbsp;' + LBL010 + '</b></td>';
								tablehtml += '<td align="right"><b>$' + addCommas(parseFloat(sTotalLocation).toFixed(2)) + '</b></td>';
							tablehtml += '</tr>';
							
							if(stLocation != 24 && stLocation != 20){ // If other than Montreal and Vancouver only one tax
								tablehtml += '<tr>';
								tablehtml += '<td><b>&nbsp;' + LBL013 + '</b></td>';
								if(stLocation == 25 || stLocation == 27 || stLocation == 28){ // if Jacksonville 1 or 2 or Lakeland
									tablehtml += '<td align="right"><b>$' + addCommas(parseFloat(fltaxLocation).toFixed(2)) + '</b></td>';
								}
								else if(stLocation == 26){ // if Columbus
									tablehtml += '<td align="right"><b>$' + addCommas(parseFloat(columbusLocation).toFixed(2)) + '</b></td>';
								}
								else{ // else is other Canada
									tablehtml += '<td align="right"><b>$' + addCommas(parseFloat(taxLocation).toFixed(2)) + '</b></td>';
								}
								tablehtml += '</tr>';
							}
							else{ // If Montreal or Vancouver 2 taxes
								tablehtml += '<tr>';
									tablehtml += '<td><b>&nbsp;' + LBL013 + '</b></td>';
									tablehtml += '<td align="right"><b>$' + addCommas(parseFloat(gstLocation).toFixed(2)) + '</b></td>';
								tablehtml += '</tr>';
								
								if(parseFloat(pstLocation) > 0){
									tablehtml += '<tr>';
									tablehtml += '<td><b>&nbsp;' + LBL014 + '</b></td>';
									tablehtml += '<td align="right"><b>$' + addCommas(parseFloat(pstLocation).toFixed(2)) + '</b></td>';
									tablehtml += '</tr>';	
								}
							}
							tablehtml += '<tr>';
								tablehtml += '<td class="profile t-border"><b>&nbsp;' + LBL011 + '</b></td>';
								tablehtml += '<td class="profile t-border" align="right"><b>' + currencyType + ' $' + addCommas(parseFloat(totalLocation).toFixed(2)) + '</b></td>';
							tablehtml += '</tr>';
						
							tablehtml += '</table>';
						}
						else {
	            			var totalLocation = parseFloat(sTotalLocation);
	            			totalInvoice += parseFloat(totalLocation); 
							tablehtml += currencyType + ' $' + addCommas(parseFloat(sTotalLocation).toFixed(2));
						}
	            		
        		       
	            	tablehtml +=  '</b></td>';
          			tablehtml +=  '</tr></table>';
          			tablehtml += '</td></tr>';

    		//}  
    		
//-------------------------- 2 End Location Section & Loop----------------------------------------------------------------------------

//-------------------------- Construct Grand Totals Section ----------------------------------------------------------------------------
          /*
    		// only if there is more then one location grand total is needed
    		if(locationResults != null && parseFloat(locationResults.length) > 1){
    			tablehtml += '<tr><td colspan="7">&nbsp;</td></tr>';
	    		tablehtml += '<tr><td colspan="7">';
				tablehtml +=  '<table class="profile" align="center" table-layout="fixed"><tr>';
				tablehtml +=  '<td width="66%" class="profile t-border"><h4>' + LBL017 + '</h4></td>';            
				tablehtml +=  '<td width="33%" class="profile t-border" align="right"><b>';
				
	    		if (subsidiary == 6) { // if Canada then taxes
	
	    			tablehtml +=  '<table class="profile" align="right" width="100%">';
					tablehtml += '<tr>';
						tablehtml += '<td><b>&nbsp;' + LBL010 + '</b></td>';
						tablehtml += '<td align="right"><b>$' + addCommas(parseFloat(sTotalInvoice).toFixed(2)) + '</b></td>';
					tablehtml += '</tr>';
					
					if(stLocation != 24){ // If not Quebec only one tax
						tablehtml += '<tr>';
						tablehtml += '<td><b>&nbsp;' + LBL013 + '</b></td>';
						tablehtml += '<td align="right"><b>$' + addCommas(parseFloat(taxInvoice).toFixed(2)) + '</b></td>';
						tablehtml += '</tr>';
					}
					else{ // If Quebec then 2 taxes
						tablehtml += '<tr>';
							tablehtml += '<td><b>&nbsp;' + LBL013 + '</b></td>';
							tablehtml += '<td align="right"><b>$' + addCommas(parseFloat(gstInvoice).toFixed(2)) + '</b></td>';
						tablehtml += '</tr>';
						
						if(parseFloat(pstLocation) > 0){
							tablehtml += '<tr>';
							tablehtml += '<td><b>&nbsp;' + LBL014 + '</b></td>';
							tablehtml += '<td align="right"><b>$' + addCommas(parseFloat(pstInvoice).toFixed(2)) + '</b></td>';
							tablehtml += '</tr>';	
						}
					}
					tablehtml += '<tr>';
						tablehtml += '<td class="profile t-border"><b>&nbsp;' + LBL011 + '</b></td>';
						tablehtml += '<td class="profile t-border" align="right"><b>' + currencyType + ' $' + addCommas(parseFloat(totalInvoice).toFixed(2)) + '</b></td>';
					tablehtml += '</tr>';
				
				tablehtml += '</table>';
				}
				else { // no tax if US
					tablehtml = currencyType + ' $' + addCommas(parseFloat(sTotalInvoice).toFixed(2));
				}
		       
		    	tablehtml +=  '</b></td>';
				tablehtml +=  '</tr></table>';
				tablehtml += '</td></tr>';
    		}
    		
    		*/
          	
            tablehtml += '</table>';
            tablehtml += '<br/>';    		

//---------------------------- Construct CPIRate section ------------------------------------------------------------

	                    var tablehtmlCPI =  '<table class="profile" align="left" table-layout="fixed" width="100%">';
	                        tablehtmlCPI += 	'<tr>';
							tablehtmlCPI += 		'<td colspan="2"><h4>' + LBL012 + getDateFormated(startBillingDate,languagePref) + '</h4></td>';
							tablehtmlCPI += 	'</tr>';
							for (var z=0; z<arrOrderNbr.length; ++z) {
								tablehtmlCPI += '<tr>';
								if (z == 0) {
									tablehtmlCPI += '<td class="profile t-border">&nbsp;' + arrOrderNbr[z] + '</td>';
									tablehtmlCPI += '<td class="profile t-border">&nbsp;' + arrCPIRate[z] + '</td>';
								}
								else {
									tablehtmlCPI += '<td>&nbsp;' + arrOrderNbr[z] + '</td>';
									tablehtmlCPI += '<td>&nbsp;' + arrCPIRate[z] + '</td>';
								}
								tablehtmlCPI += '</tr>';
							}
							tablehtmlCPI += '</table>';
							tablehtmlCPI += '<br/>';

//---------------------------- Load template and replace dynamic sections  ------------------------------------------------------------
							
		                    // Determine for what location the invoice is and load corespondent template - PRODUCTION
							if (stLocation == 18){
		                    	var stTemplate = 'DAL';
		                    	var objFile = nlapiLoadFile(48396); // Dallas
		                    }
		                    else if (stLocation == 25 || stLocation == 27 || stLocation == 28){
		                    	var stTemplate = 'JAX';
		                    	var objFile = nlapiLoadFile(705744); //  Jacksonville & Lakeland
		                    }
		                    else if (stLocation == 19){
		                    	var stTemplate = 'TOR';
		                    	var objFile = nlapiLoadFile(48397); // Toronto
		                    }
		                    else if (stLocation == 20){
		                    	var stTemplate = 'VAN';
		                    	var objFile = nlapiLoadFile(363975); // Vancouver
		                    }
		                    else if (stLocation == 21){
		                    	var stTemplate = 'TOR_VAN';
		                    	var objFile = nlapiLoadFile(48397); // Toronto & Vancouver
		                    }
		                    else if (stLocation == 22){
		                    	var stTemplate = 'MIN';
		                    	var objFile = nlapiLoadFile(88014); // Mineapolis
		                    }
		                    else if (stLocation == 26){
		                    	var stTemplate = 'COL';
		                    	var objFile = nlapiLoadFile(1016053); // Columbus
		                    }
		                    else if (stLocation == 23){
		                    	var stTemplate = 'CON';
		                    	var objFile = nlapiLoadFile(89155); // Connex
		                    }
		                    else if (stLocation == 24){
		                    	if(languagePref == 'en_US' || languagePref == 'en'){
		                        	var objFile = nlapiLoadFile(89153); // Montreal EN
		                    		var stTemplate = 'MTL_EN';
		                    	}
		                    	else{
		                        	var objFile = nlapiLoadFile(89154); // Montreal FR
		                    		var stTemplate = 'MTL_FR';
		                    	}
		                    }
		                    else{
		                    	var stTemplate = 'DAL';
		                    	var objFile = nlapiLoadFile(48396); // Dallas by default
		                    }
	
                    var stMainHTML = objFile.getValue();
                        stMainHTML = stMainHTML.replace(new RegExp('billAddress','g'),tablehtmlAddress);
						stMainHTML = stMainHTML.replace(new RegExp('{invoicedate}','g'),getDateFormated(startBillingDate,languagePref));
                        stMainHTML = stMainHTML.replace(new RegExp('{invoiceno}','g'),invoiceNbr);
                        stMainHTML = stMainHTML.replace(new RegExp('{totalcharges}','g'),currencyType + ' $' + addCommas(parseFloat(totalInvoice).toFixed(2)));
                        stMainHTML = stMainHTML.replace(new RegExp('{paymentDue}','g'),getDateFormated(nlapiDateToString(dtPaymentDue),languagePref));
                        stMainHTML = stMainHTML.replace(new RegExp('{billFrom}','g'),getDateFormated(startBillingDate,languagePref));
                        stMainHTML = stMainHTML.replace(new RegExp('{billTo}','g'),getDateFormated(endBillingDate,languagePref));
                        stMainHTML = stMainHTML.replace(new RegExp('{companyName}','g'),nlapiEscapeXML(custCompName));
                        stMainHTML = stMainHTML.replace(new RegExp('{accountNumber}','g'),accountNbr); 
                        stMainHTML = stMainHTML.replace(new RegExp('ServicesCategoryTable','g'),tablehtml);
						stMainHTML = stMainHTML.replace(new RegExp('CPITable','g'),tablehtmlCPI); 
                    
//---------------------------- Strat invoice by email and save as pdf section Email = 1; File Cabinet = 2 ---------------
					
						var arrEmailedTo = new Array;
						var arrEmailedToNames = new Array;
						
					if (output == '1'){ // final invoice   
			            
						// search all billing contacts of this customer
						var arrColumns = new Array();
						arrColumns[0] = new nlobjSearchColumn('internalid', 'contact', null);
						arrColumns[1] = new nlobjSearchColumn('entityid', 'contact', null);
						var arrFilters = new Array();
						arrFilters[0] = new nlobjSearchFilter('internalid',null,'is',customerId);
						arrFilters[1] = new nlobjSearchFilter('contactrole','contact','is',1);
						arrFilters[2] = new nlobjSearchFilter('email','contact','isnotempty');
						arrFilters[3] = new nlobjSearchFilter('isinactive','contact','is','F');
						var searchContacts = nlapiSearchRecord('customer', null, arrFilters, arrColumns);
						
						if (searchContacts == null || searchContacts.length < 1) { // no billing contact to email to, so just save in the file cabinet
							var filePDF = nlapiXMLToPDF(stMainHTML);
							//filePDF.setName(customer + '_' + getDateToday() + '_' + invoiceNbr + '_' + currencyType + '_'+ stTemplate + '.pdf');
							filePDF.setName(customer + '_' + getDateToday() + '_' + invoiceNbr + '_' + currencyType + '_'+ stLocationName + '.pdf');
							
							
							filePDF.setFolder(fileFolder);
						}
						else{ // at least 1 billing contact, so send invoice by email and save to file cabinet with '_&@' mark
							var filePDF = nlapiXMLToPDF(stMainHTML);
							//filePDF.setName(customer + '_' + getDateToday() + '_' + invoiceNbr + '_' + currencyType + '_'+ stTemplate + '_&@.pdf');
							filePDF.setName(customer + '_' + getDateToday() + '_' + invoiceNbr + '_' + currencyType + '_'+ stLocationName + '_&@.pdf');
							filePDF.setFolder(fileFolder);
							
							// build email subject and body depending on language
							if(languagePref == 'en_US' || languagePref == 'en'){

							emailSubject = 'Cologix Invoice';
							//emailSubjectConfirm = 'Cologix Invoice sent to ' + custCompName;
							emailBody = 'Dear ' + custCompName + ',\n\n' +
										'A copy of your invoice is attached for your records. Please remit payment as soon as possible.\n\n' +
										'If you have questions regarding your invoice, please send an email to billing@cologix.com.\n\n' + 
										'Thank you for your business.\n\n' +
										'Sincerely,\n' +
										'Cologix\n' +
										'1-855-IX-BILLS (492-4557)\n';
							}
							else{
								emailSubject = 'Facture Cologix';
								//emailSubjectConfirm = 'Cologix Invoice sent to ' + custCompName;
								emailBody = 'Cher ' + custCompName + ',\n\n<br><br>' +
											'Vous trouverez en pi&egrave;ce jointe une copie de votre facture pour vos dossiers. Veuillez s\'il-vous-pla&icirc;t envoyer votre paiement d&egrave;s que possible.\n\n<br><br>' +
											'Pour toute question concernant votre facture, veuillez envoyer un courriel &agrave; billing@cologix.com.\n\n<br><br>' + 
											'Merci de votre confiance.\n\n<br><br>' +
											'Cordialement,\n<br>' +
											'Cologix\n<br>' +
											'1-855-IX-BILLS (492-4557)\n\n<br><br>' +
											'AVIS DE CONFIDENTIALIT&Eacute;: L\'information contenue dans le pr&eacute;sent message ainsi que dans toute pi&egrave;ce jointe est priv&eacute;e, confidentielle et est la propri&eacute;t&eacute; de Cologix, Inc. L\'information contenue dans le pr&eacute;sent message ainsi que dans toute pi&egrave;ce jointe est &eacute;galement privil&eacute;gi&eacute;e et &agrave; l\'usage exclusif du destinataire ci-dessus. Toute autre personne est par les pr&eacute;sentes avis&eacute;e qu\'il lui est strictement interdit de prendre quelque action que ce soit en se basant sur l\'information contenue dans le pr&eacute;sent message ainsi que dans toute pi&egrave;ce jointe, tout comme il lui est interdit de divulguer, reproduire ou distribuer cette m&ecirc;me information sans autorisation. Si vous avez re&ccedil;u le pr&eacute;sent message par erreur, veuillez en informer l\'exp&eacute;diteur par t&eacute;l&eacute;phone ou par courriel et veuillez par la suite d&eacute;truire imm&eacute;diatement ce message et toute copie de celui-ci. Merci.\n\n<br><br>';
							}
							
							for (var k=0; searchContacts != null && k<searchContacts.length; k++) {// send email to all billing contacts
								nlapiSendEmail(12827,searchContacts[k].getValue(arrColumns[0]),emailSubject,emailBody,null,null,null,filePDF);
								arrEmailedTo.push(searchContacts[k].getValue(arrColumns[0]));
								arrEmailedToNames.push(searchContacts[k].getValue(arrColumns[1]));
							}
						}

					}
						
					else{ // pro forma invoice - create just in the file cabinet
						var filePDF = nlapiXMLToPDF(stMainHTML);
						//filePDF.setName(customer + '_' + getDateToday() + '_' + invoiceNbr + '_' + currencyType + '_'+ stTemplate + '_pro_forma.pdf');
						filePDF.setName(customer + '_' + getDateToday() + '_' + invoiceNbr + '_' + currencyType + '_'+ stLocationName + '_pro_forma.pdf');
						
						filePDF.setFolder(fileFolder);
					}

					//save file in the file cabinet
                    var fileId = nlapiSubmitFile(filePDF);
                    	nlapiSubmitField('customrecord_virtual_consolidate_inv',consolidateID,'custrecord_pdf_doc_link',linkToFolder);  
                    	
                    if (output == '1'){
	                    // create Consolidated Invoices record
	                    var record = nlapiCreateRecord('customrecord_clgx_consolidated_invoices');

	                    record.setFieldValue('custrecord_clgx_consol_inv_customer', customerId);
	                    record.setFieldValue('custrecord_clgx_consol_inv_date', startBillingDate);
	                    record.setFieldValue('custrecord_clgx_consol_inv_pdf_file_id', parseFloat(fileId).toFixed(0));
	                    record.setFieldValue('custrecord_clgx_consol_inv_batch', invoiceNbr);
	                    record.setFieldValue('custrecord_clgx_consol_inv_invoices', arrInvoiceIds.toString().replace(/\,/g,";"));
	                    record.setFieldValue('custrecord_clgx_consol_inv_month', startMonth);
	                    record.setFieldValue('custrecord_clgx_consol_inv_year', startYear);
	                    record.setFieldValue('custrecord_clgx_consol_inv_month_display', startDisplayMonth);
	                    record.setFieldValue('custrecord_clgx_consol_inv_year_display', startDisplayYear);
	                    record.setFieldValue('custrecord_clgx_consol_inv_emailed_to', arrEmailedTo.toString().replace(/\,/g,";"));
	                    record.setFieldValue('custrecord_clgx_consol_inv_output', output);
	                    
	                    if (taxesResults != null) {
	                        record.setFieldValue('custrecord_clgx_consol_inv_subtotal', parseFloat(sTotalInvoice).toFixed(2));
	                        record.setFieldValue('custrecord_clgx_consol_inv_tax_total', parseFloat(gstInvoice).toFixed(2));
	                        record.setFieldValue('custrecord_clgx_consol_inv_tax2_total', parseFloat(pstInvoice).toFixed(2));
	                        record.setFieldValue('custrecord_clgx_consol_inv_total', parseFloat(totalInvoice).toFixed(2));
	                    }
	                    else{
	                        record.setFieldValue('custrecord_clgx_consol_inv_subtotal', 0);
	                        record.setFieldValue('custrecord_clgx_consol_inv_tax_total', 0);
	                        record.setFieldValue('custrecord_clgx_consol_inv_tax2_total', 0);
	                    	record.setFieldValue('custrecord_clgx_consol_inv_total', parseFloat(totalInvoice).toFixed(2));
	                    }
	                    record.setFieldValue('custrecord_clgx_consol_inv_currency', currencyType);
	                    //record.setFieldValue('custrecord_clgx_consol_inv_market', stTemplate);
	                    record.setFieldValue('custrecord_clgx_consol_inv_market', stLocationName);
	                    record.setFieldValue('custrecord_clgx_consol_inv_churn', 'T');
	                    record.setFieldValue('custrecord_clgx_consol_inv_processed', 'T');
	                    var idRec = nlapiSubmitRecord(record, true);

                    }


//---------------------------- End Invoice by email and file pdf section ----------------------------------------------------------
                    	
                	//loop the invoice id array and update flag and invoice number
                	if (output == '1'){ // if final, update both
                        for(var z=0; z<arrInvoiceIds.length; z++){
                            nlapiSubmitField('invoice',arrInvoiceIds[z],['custbody_consolidate_inv_id','custbody_consolidate_inv_nbr','custbody_consolidate'],[consolidateID,invoiceNbr,'F']);
                        }
                	}
                	else{ // if not update just invoice number
                        for(var z=0; z<arrInvoiceIds.length; z++){
                            nlapiSubmitField('invoice',arrInvoiceIds[z],['custbody_consolidate_inv_id','custbody_consolidate_inv_nbr'],[consolidateID,invoiceNbr]);
                        }
                	}
                
                    
                    var strEmailTo = '';
                    if (output == '1'){
	                    for(var m=0; m< arrEmailedTo.length; m++){
	                    	strEmailTo += '<a href="https://1337135.app.netsuite.com/app/common/entity/contact.nl?id=' + arrEmailedTo[m] + '">' + arrEmailedToNames[m] + '</a><br>';
	                    }
                    }
                    var strInvoices = '';
                    for(var m=0; m< arrInvoiceIds.length; m++){
                    	strInvoices += '<a href="https://debugger.netsuite.com/app/accounting/transactions/custinvc.nl?id=' + arrInvoiceIds[m] + '">' + arrInvoiceNbrs[m] + '</a><br>';
                    }
                    
           	    	var endExec = moment();
        	    	var invExecMinutes = (endExec.diff(startScript)/60000).toFixed(1);
        	    	
                    var endUsageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
                    var usageConsumtion = parseInt(endUsageConsumtion) - parseInt(startUsageConsumtion);
                    
                    reportBody += '<tr><td>' + parseInt(h+1) + ' of ' + batchResult.length + '</td><td><a href="https://debugger.netsuite.com/app/common/entity/custjob.nl?id=' + customerId + '">' + customer + '</a></td><td>' + parseInt(i+1) + ' of ' + customerResult.length + '</td><td>' + invoiceNbr + '</td><td>' + stTemplate + '</td><td>' + currencyType + '</td><td>' + fileId + '</td><td>' + strInvoices + '</td><td>' + strEmailTo + '</td><td>' + endExec.format('M/D/YYYY h:mm:ss a') + '</td><td>' + invExecMinutes + '</td><td>' + usageConsumtion + '</td><td>' + endUsageConsumtion + '</td></tr>';

                	
                	
                	
                var custIndex = i + 1;
                nlapiLogExecution('DEBUG','Finish Consolidating Customer: ', 'Finish Consolidating Customer ' + customer +  ' (' + currencyType + ')' + ' | File# ' + fileId+ ' | ConsolidationID ' + invoiceNbr + ' |');

                invoiceNbr = '';
                addressID = '';
				billAddress1 = '';
				billAddress2 = '';
				billAddress3 = '';
				billAddressee = '';
				billAttention = '';
				billCity = '';
				billCountry = '';
				billState = '';
				billZipcode = '';
            }
            
//-------------------------- 1 End Invoice Section & Loop----------------------------------------------------------------------------

           var useAddressee = 'F';

	       nlapiLogExecution('DEBUG','Finish Consolidating Invoices for Customer: ', 'Finish Consolidating Invoices for Customer ' + batchIndex + ' of ' + batchResult.length + ' / Usage - '+ endUsageConsumtion + ' --------------------------|');
    	}
           
//-------------------------- 0 End Customer Loop----------------------------------------------------------------------------
	
		if(outoftheloop == 0){ // if this is not a re-scheduled loop
			// send email report for this batch
			var endScript = moment();
			reportBody += '</table><br>';
			reportBody += 'End : ' + endScript.format('M/D/YYYY h:mm:ss a') + '<br>';
			reportBody += 'Total execution minutes : ' + (endScript.diff(startScript)/60000).toFixed(1) + '<br>';
			var totalUsageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
			reportBody += 'Total usage : ' + totalUsageConsumtion;
			reportBody += '<br><br>';
			reportBody += '<a href="https://1337135.app.netsuite.com/app/site/hosting/scriptlet.nl?script=263&deploy=1">Consolidated Invoices History</a>';
			reportBody += '<br><br>';
			
			
			clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_ci_batch_address", reportSubject, reportBody);
			
			/*nlapiSendEmail(12827,71418,reportSubject,reportBody,null,null,null,null,true);
			nlapiSendEmail(12827,206211,reportSubject,reportBody,null,null,null,null);
			
			nlapiSendEmail(12827,12073,reportSubject,reportBody,null,null,null,null,true);
			nlapiSendEmail(12827,336456,reportSubject,reportBody,null,null,null,null,true);
			nlapiSendEmail(12827,2406,reportSubject,reportBody,null,null,null,null,true);*/
		}

        nlapiLogExecution('DEBUG','Finished Execution', 'Total Remaining Usage - ' + context.getRemainingUsage() + ' -------------------------- Finished Scheduled Script --------------------------|');

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}




function getDueDate(netDays){
	// Return 'pay until' date
    var date = new Date();
	date.setDate(date.getDate() + netDays);
	
    var month = parseInt(date.getMonth()) + 1;
    var day = date.getDate();
    var year = date.getFullYear();
    
    if(parseInt(month) < 10){
        month = '0' + month;
    }
    
    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = month + '/' + day + '/' + year;
    
    return formattedDate;
}

function getDateToday(){
    var date = new Date();
    var month = parseInt(date.getMonth()) + 1;
    var day = date.getDate();
    var year = date.getFullYear();
    
    if(parseInt(month) < 10){
        month = '0' + month;
    }
    
    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = month + '/' + day + '/' + year;
    
    return formattedDate;
}

//get the last day of the month
function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}

//get the start date and end date
function getDateRange(month,year){
    var stMonth = month - 1;
    var stDays  = daysInMonth(parseInt(stMonth),parseInt(year));
    var stYear  = year;
    
    var stStartDate = month + '/1/' + stYear;
    var stEndDate   = month + '/' + stDays + '/' + stYear;
    
    var arrDateRange = new Array();
        arrDateRange[0] = stStartDate;
        arrDateRange[1] =stEndDate;
    
    return arrDateRange;
}

function getDateFormated(date2format,language){
	
    var date = nlapiStringToDate(date2format);
    var month = parseInt(date.getMonth());
    var day = date.getDate();
    var year = date.getFullYear();
    
    var arrMonthEN = new Array();
    var arrMonthFR = new Array();
    
    arrMonthEN[0] = 'JAN';
    arrMonthEN[1] = 'FEB';
    arrMonthEN[2] = 'MAR';
    arrMonthEN[3] = 'APR';
    arrMonthEN[4] = 'MAY';
    arrMonthEN[5] = 'JUN';
    arrMonthEN[6] = 'JUL';
    arrMonthEN[7] = 'AUG';
    arrMonthEN[8] = 'SEP';
    arrMonthEN[9] = 'OCT';
    arrMonthEN[10] = 'NOV';
    arrMonthEN[11] = 'DEC';
    
    arrMonthFR[0] = 'JAN';
    arrMonthFR[1] = 'F&Eacute;V';
    arrMonthFR[2] = 'MAR';
    arrMonthFR[3] = 'AVR';
    arrMonthFR[4] = 'MAI';
    arrMonthFR[5] = 'JUN';
    arrMonthFR[6] = 'JUL';
    arrMonthFR[7] = 'AO&Ucirc;';
    arrMonthFR[8] = 'SEP';
    arrMonthFR[9] = 'OCT';
    arrMonthFR[10] = 'NOV';
    arrMonthFR[11] = 'D&Eacute;C';
    
    if(language == 'en_US' || language == 'en'){
    	var stMonth = arrMonthEN[parseInt(month)];
    }
    else{
    	var stMonth = arrMonthFR[parseInt(month)];
    }
    
    var stDate = day + '-' + stMonth + '-' + year;
    
    return stDate;
}

//check if value is in the array
function inArray(val, arr){	
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}

function addCommas(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function function_FrenchItemCategory(categoryID){	
	var frenchCategory = '';
	switch(categoryID) {
	case '1':
		frenchCategory = 'Location d\'&eacute;quipement';
		break;
	case '2':
		frenchCategory = 'Vente d\'&eacute;quipement';
		break;
	case '3':
		frenchCategory = 'Service d\'installation';
		break;
	case '4':
		frenchCategory = 'Interconnexion';
		break;
	case '5':
		frenchCategory = 'R&eacute;seau';
		break;
	case '6':
		frenchCategory = 'Autre non R&eacute;curent';
		break;
	case '7':
		frenchCategory = 'Autre R&eacute;curent';
		break;
	case '8':
		frenchCategory = '&Eacute;lectricit&eacute;';
		break;
	case '9':
		frenchCategory = 'Support Technique';
		break;
	case '10':
		frenchCategory = 'Espace';
		break;
	case '11':
		frenchCategory = 'Interconnexion virtuelle';
		break;
	default:
		frenchCategory = '';
	}
	return frenchCategory;
}

function function_fileFolder(startDisplayYear,startDisplayMonth,output){

	if(output == 1){
		if(parseInt(startDisplayYear) == 2014){
			switch(parseInt(startDisplayMonth)) {
				case 1:
					fileFolder = 703601; //
					break;
				case 2:
					fileFolder = 703602;
				  break;
				case 3:
					fileFolder = 703603;
				  break;
				case 4:
					fileFolder = 703604;
					break;
				case 5:
					fileFolder = 703605;
				  break;
				case 6:
					fileFolder = 703606;
				  break;  
				case 7:
					fileFolder = 703607;
					break;
				case 8:
					fileFolder = 703608;
				  break;
				case 9:
					fileFolder = 703609;
				  break;
				case 10:
					fileFolder = 703610;
				  break;
				case 11:
					fileFolder = 703611;
				  break;
				case 12:
					fileFolder = 703612;
				  break;
				default:
					fileFolder = 24;
			}
		}
		else if(parseInt(startDisplayYear) == 2015){
			switch(parseInt(startDisplayMonth)) {
				case 1:
					fileFolder = 1594298;
					break;
				case 2:
					fileFolder = 1594299;
				  break;
				case 3:
					fileFolder = 1594300;
				  break;
				case 4:
					fileFolder = 1594301;
					break;
				case 5:
					fileFolder = 1594302;
				  break;
				case 6:
					fileFolder = 1594303;
				  break;  
				case 7:
					fileFolder = 1594304;
					break;
				case 8:
					fileFolder = 1594305;
				  break;
				case 9:
					fileFolder = 1594306;
				  break;
				case 10:
					fileFolder = 1594307;
				  break;
				case 11:
					fileFolder = 1594308;
				  break;
				case 12:
					fileFolder = 1594309;
				  break;
				default:
					fileFolder = 24;
			}
		}
		else{
			fileFolder = 24;
		}
	}
	else{
		if(parseInt(startDisplayYear) == 2014){
			switch(parseInt(startDisplayMonth)) {
				case 12:
					fileFolder = 1530334;
				  break;
				default:
					fileFolder = 24;
			}
		}
		else if(parseInt(startDisplayYear) == 2015){
			switch(parseInt(startDisplayMonth)) {
			case 1:
				fileFolder = 1594410;
				break;
			case 2:
				fileFolder = 1594411;
			  break;
			case 3:
				fileFolder = 1594412;
			  break;
			case 4:
				fileFolder = 1594413;
				break;
			case 5:
				fileFolder = 1594414;
			  break;
			case 6:
				fileFolder = 1594415;
			  break;  
			case 7:
				fileFolder = 1594416;
				break;
			case 8:
				fileFolder = 1594417;
			  break;
			case 9:
				fileFolder = 1594518;
			  break;
			case 10:
				fileFolder = 1594519;
			  break;
			case 11:
				fileFolder = 1594520;
			  break;
			case 12:
				fileFolder = 1594521;
			  break;
			default:
				fileFolder = 24;
		}
		}
		else{
			fileFolder = 24;
		}
	}
	
	return fileFolder;
}