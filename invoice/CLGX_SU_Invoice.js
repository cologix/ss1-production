nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Invoice.js
//	Script Name:	CLGX_SU_Invoice.js
//	Script Id:		customscript_clgx_su_invoice
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Invoice
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		4/23/2012
//-------------------------------------------------------------------------------------------------

function afterSubmit(type){
	try {
		
//------------- Begin Section 1 -------------------------------------------------------------------
		// Created:	02/19/2016
		// Details:	Update balances
//-------------------------------------------------------------------------------------------------
		
		var currentContext = nlapiGetContext();
		if (currentContext.getExecutionContext() == 'userinterface' && type == 'edit'){

			var recordid = nlapiGetRecordId();
			var record = nlapiLoadRecord('invoice',recordid);
			var customerid = record.getFieldValue('entity');
    		
    		var response = clgx_update_balances (customerid);
    		//arrParam['custscript_customerid'] = customerid;
            //var status = nlapiScheduleScript('customscript_clgx_ss_update_balances', null ,arrParam);
    		
			/*
			var row = nlapiLoadRecord('customer', customerid);
			var userid = nlapiGetUser();
			var user = nlapiLookupField('employee', userid, 'entityid');
			
			try {
				
				flexapi('POST','/netsuite/update', {
					'type': 'customer',
					'id': customerid,
					'action': type,
					'userid': userid,
					'user': user,
					'error': false,
					'errorcode': '',
					'errordetails': '',
					'data': json_serialize(row)
				});
				nlapiLogExecution('DEBUG', 'flexapi request: ', customerid);
			}
			catch (error) {
				
				var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
				record.setFieldValue('custrecord_clgx_queue_ping_record_id', customerid);
				record.setFieldValue('custrecord_clgx_queue_ping_record_type', 1);
				record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
				var idRec = nlapiSubmitRecord(record, false,true);
				
			}
			*/
		}
		
//---------- End Section 1 ------------------------------------------------------------------------------------------------

	} 
	catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	}
}