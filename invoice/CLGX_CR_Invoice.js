nlapiLogExecution("audit","FLOStart",new Date().getTime());
/**
 * Created by catalinataran on 2014-04-02.
 */
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_Invoice.js
//	Script Name:	CLGX_CR_Invoice
//	Script Id:		customscript_clgx_cr_invoice
//	Script Runs:	On Client
//	Script Type:	Client Script
//	Deployments:	Proposal - Sandbox | Production
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Released:		04/02/2014
//-------------------------------------------------------------------------------------------------

function saveRecord(){
    try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 4/4/2013
// Details:	If Status is is Closed-Lost, Loss reason is mandatory
//-------------------------------------------------------------------------------------------------
        var allowSave = 1;
        var alertMsg = '';
        var currentContext = nlapiGetContext();
        var usrRole = currentContext.getRole();
        var itemsCount =nlapiGetLineItemCount('item');
        //alert(itemsCount);
        //Only if user has admin, full or AR Clerk roles can add discount items to Invoice
        // if ((usrRole != 1011)&&(itemsCount>0))
        if (itemsCount>0)
        {
            for ( var j = 1; j <= itemsCount; j++ ) {
                //  var itemId= nlapiGetLineItemValue('item', 'item', j);
                var itemType= nlapiGetLineItemValue('item', 'itemtype', j);
                var amount= nlapiGetLineItemValue('item', 'amount', j);
                if((itemType=='Discount')&&(usrRole != 1011 && usrRole != 3 && usrRole != 18 && usrRole!=5))
                {
                    alertMsg = 'Discount Item is not valid for this Invoice';
                    allowSave=0;
                }
                if((itemType!='Discount')&&(usrRole != 3 && usrRole != 18 && usrRole != 1011)&&(amount<0))
                {
                    alertMsg='Negative amounts are not permited';
                    allowSave=0;

                }
            }
        }

        if (allowSave == 0) {
            alert(alertMsg);
            return false;
        }
        else{
            return true;
        }
//-------------------------------------------------------------------------------------------------

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


