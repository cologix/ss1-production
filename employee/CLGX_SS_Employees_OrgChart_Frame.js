nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Employees_Organization_Chart.js
//	Script Name:	CLGX_SS_Employees_Organization_Chart
//	Script Id:		customscript_clgx_ss_emp_org_chart
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		04/13/2012
//-------------------------------------------------------------------------------------------------

function suiteletEmployeesOrgChartFrame(request, response){
	try {
		nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------STARTED---------------------|');

		var form = nlapiCreateForm('Organization Chart');
		var htmlChart = form.addField('custpage_clgx_html','inlinehtml', null, null, null);

		var dataHTML = '';
		dataHTML += '<iframe name="portal" id="portal" src="/app/site/hosting/scriptlet.nl?script=134&deploy=1&compid=1337135" height="600px" width="1200px" frameborder="0" scrolling="no"></iframe>';
		
		htmlChart.setDefaultValue(dataHTML);
		response.writePage( form );
	
//---------- End Section 1 ------------------------------------------------------------------------------------------------


nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------FINISHED---------------------|');  
} 
catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
	if (error.getDetails != undefined){
	    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	    throw error;
	}
	else{
	    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	    throw nlapiCreateError('99999', error.toString());
	}
} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

