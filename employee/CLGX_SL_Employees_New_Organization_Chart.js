nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Employees_New_Organization_Chart.js
//	Script Name:	CLGX_SL_Employees_New_Organization_Chart
//	Script Id:		customscript_clgx_sl_emp_new_org_chart
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		10/23/2014
//-------------------------------------------------------------------------------------------------

function suiteletEmployeesOrgNewChart(request, response){
    try {
        nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------STARTED---------------------|');


        var arrColumns = new Array();
       /* arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        arrColumns.push(new nlobjSearchColumn('entityid',null,null));
        arrColumns.push(new nlobjSearchColumn('title',null,null));
        arrColumns.push(new nlobjSearchColumn('supervisor',null,null));*/
        var arrFilters = new Array();
        //arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
       // arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",12827));
        var searchEmployees = nlapiSearchRecord('employee', 'customsearch_clgx_ss_emp_org_chart', arrFilters, arrColumns);

        var arrEmployees = new Array();
        for ( var i = 0; searchEmployees != null && i < searchEmployees.length; i++ ) {
            var row = searchEmployees[i];
            var columns = row.getAllColumns();
            var internalid = row.getValue(columns[0]);

            if(internalid < 0){
                internalid = 10000;
            }
            var parentid = parseInt(row.getValue(columns[3]));
            if(parentid < 0){
                parentid = 10000;
            }

            var objEmployee = new Object();
            objEmployee["parentid"] = parentid;
            objEmployee["parent"] = row.getText(columns[3]);
            objEmployee["id"] = internalid.toString();
            objEmployee["title"] =  row.getValue(columns[1]);
            objEmployee["subtitle"] =  row.getValue(columns[2]);

           /*var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('internalid',null,null));
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
            arrFilters.push(new nlobjSearchFilter("supervisor",null,"anyof",parseInt(searchEmployees[i].getValue('internalid',null,null))));
            var searchSubordinates = nlapiSearchRecord('employee', null, arrFilters, arrColumns);
*/
            if(row.getValue(columns[3]) != null && row.getValue(columns[3])!=''){
                //objEmployee["type"] = 'staff';
                objEmployee["type"] = 'subordinate';
            }
            else{
                objEmployee["type"] = 'staff';
                //objEmployee["type"] = 'subordinate';
            }
            arrEmployees.push(objEmployee);
        }

        var objOrgChart = new Object();
        objOrgChart["id"] = 2;
        objOrgChart["title"] = 'Org Chart';

        var objRoot = new Object();
        objRoot["parentid"] = '';
        objRoot["parent"] = '';
        objRoot["id"] = '7';
        objRoot["title"] = 'Grant Van Rooyen';
        objRoot["subtitle"] = 'Chief Executive Officer';
        objRoot["type"] = 'subordinate';

        var arrLevel1 = _.filter(arrEmployees, function(arr){
            return (arr.parentid == '7');
        });

        if(arrLevel1.length > 0){
            var arrChidren1 = new Array();
            for ( var i1 = 0; arrLevel1 != null && i1 < arrLevel1.length; i1++ ) {
                var objChild1 = new Object();
                objChild1["parentid"] = arrLevel1[i1].parentid;
                objChild1["parent"] = arrLevel1[i1].parent;
                objChild1["id"] = arrLevel1[i1].id;
                objChild1["title"] = arrLevel1[i1].title;
                objChild1["subtitle"] = arrLevel1[i1].subtitle;
                objChild1["type"] = arrLevel1[i1].type;

                var arrLevel2 = _.filter(arrEmployees, function(arr){
                    return (arr.parentid == arrLevel1[i1].id);
                });
                if(arrLevel2.length > 0){
                    var arrChidren2 = new Array();
                    for ( var i2 = 0; arrLevel2 != null && i2 < arrLevel2.length; i2++ ) {
                        var objChild2 = new Object();
                        objChild2["parentid"] = arrLevel2[i2].parentid;
                        objChild2["parent"] = arrLevel2[i2].parent;
                        objChild2["id"] = arrLevel2[i2].id;
                        objChild2["title"] = arrLevel2[i2].title;
                        objChild2["subtitle"] = arrLevel2[i2].subtitle;
                        objChild2["type"] = arrLevel2[i2].type;

                        var arrLevel3 = _.filter(arrEmployees, function(arr){
                            return (arr.parentid == arrLevel2[i2].id);
                        });
                        if(arrLevel3.length > 0){
                            var arrChidren3 = new Array();
                            for ( var i3 = 0; arrLevel3 != null && i3 < arrLevel3.length; i3++ ) {
                                var objChild3 = new Object();
                                objChild3["parentid"] = arrLevel3[i3].parentid;
                                objChild3["parent"] = arrLevel3[i3].parent;
                                objChild3["id"] = arrLevel3[i3].id;
                                objChild3["title"] = arrLevel3[i3].title;
                                objChild3["subtitle"] = arrLevel3[i3].subtitle;
                                objChild3["type"] = arrLevel3[i3].type;

                                var arrLevel4 = _.filter(arrEmployees, function(arr){
                                    return (arr.parentid == arrLevel3[i3].id);
                                });
                                if(arrLevel4.length > 0){
                                    var arrChidren4 = new Array();
                                    for ( var i4 = 0; arrLevel4 != null && i4 < arrLevel4.length; i4++ ) {
                                        var objChild4 = new Object();
                                        objChild4["parentid"] = arrLevel4[i4].parentid;
                                        objChild4["parent"] = arrLevel4[i4].parent;
                                        objChild4["id"] = arrLevel4[i4].id;
                                        objChild4["title"] = arrLevel4[i4].title;
                                        objChild4["subtitle"] = arrLevel4[i4].subtitle;
                                        objChild4["type"] = arrLevel4[i4].type;

                                        var arrLevel5 = _.filter(arrEmployees, function(arr){
                                            return (arr.parentid == arrLevel4[i4].id);
                                        });
                                        if(arrLevel5.length > 0){
                                            var arrChidren5 = new Array();
                                            for ( var i5 = 0; arrLevel5 != null && i5 < arrLevel5.length; i5++ ) {
                                                var objChild5 = new Object();
                                                objChild5["parentid"] = arrLevel5[i5].parentid;
                                                objChild5["parent"] = arrLevel5[i5].parent;
                                                objChild5["id"] = arrLevel5[i5].id;
                                                objChild5["title"] = arrLevel5[i5].title;
                                                objChild5["subtitle"] = arrLevel5[i5].subtitle;
                                                objChild5["type"] = arrLevel5[i5].type;

                                                var arrLevel6 = _.filter(arrEmployees, function(arr){
                                                    return (arr.parentid == arrLevel5[i5].id);
                                                });
                                                if(arrLevel6.length > 0){
                                                    var arrChidren6 = new Array();
                                                    for ( var i6 = 0; arrLevel6 != null && i6 < arrLevel6.length; i6++ ) {
                                                        var objChild6 = new Object();
                                                        objChild6["parentid"] = arrLevel6[i6].parentid;
                                                        objChild6["parent"] = arrLevel6[i6].parent;
                                                        objChild6["id"] = arrLevel6[i6].id;
                                                        objChild6["title"] = arrLevel6[i6].title;
                                                        objChild6["subtitle"] = arrLevel6[i6].subtitle;
                                                        objChild6["type"] = arrLevel6[i6].type;

                                                        var arrLevel7 = _.filter(arrEmployees, function(arr){
                                                            return (arr.parentid == arrLevel6[i6].id);
                                                        });
                                                        if(arrLevel7.length > 0){
                                                            var arrChidren7 = new Array();
                                                            for ( var i7 = 0; arrLevel7 != null && i7 < arrLevel6.length; i7++ ) {
                                                                var objChild7 = new Object();
                                                                objChild7["parentid"] = arrLevel7[i7].parentid;
                                                                objChild7["parent"] = arrLevel7[i7].parent;
                                                                objChild7["id"] = arrLevel7[i7].id;
                                                                objChild7["title"] = arrLevel7[i7].title;
                                                                objChild7["subtitle"] = arrLevel7[i7].subtitle;
                                                                objChild7["type"] = arrLevel7[i7].type;


                                                                // add here more levels


                                                                arrChidren7.push(objChild7);
                                                            }
                                                            objChild6["children"] = arrChidren7;
                                                        }
                                                        arrChidren6.push(objChild6);
                                                    }
                                                    objChild5["children"] = arrChidren6;
                                                }
                                                arrChidren5.push(objChild5);
                                            }
                                            objChild4["children"] = arrChidren5;
                                        }
                                        arrChidren4.push(objChild4);
                                    }
                                    objChild3["children"] = arrChidren4;
                                }
                                arrChidren3.push(objChild3);
                            }
                            objChild2["children"] = arrChidren3;
                        }
                        arrChidren2.push(objChild2);
                    }
                    objChild1["children"] = arrChidren2;
                }
                arrChidren1.push(objChild1);
            }
            objRoot["children"] = arrChidren1;
        }

        objOrgChart["root"] = objRoot;

        //var strJSON = JSON.stringify(objOrgChart);

        //var jsonFile = nlapiCreateFile('employees_json_tree.json', 'PLAINTEXT', strJSON);
        //jsonFile.setFolder(66215);
        //nlapiSubmitFile(jsonFile);


        //return JSON.stringify(objOrgChart)
        response.write( JSON.stringify(objOrgChart) );

//---------- End Section 1 ------------------------------------------------------------------------------------------------


        nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function create_employee_json_file (){

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    arrColumns.push(new nlobjSearchColumn('entityid',null,null));
    arrColumns.push(new nlobjSearchColumn('title',null,null));
    arrColumns.push(new nlobjSearchColumn('supervisor',null,null));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
    arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",12827));
    var searchEmployees = nlapiSearchRecord('employee', null, arrFilters, arrColumns);

    var arrEmployees = new Array();
    for ( var i = 0; searchEmployees != null && i < searchEmployees.length; i++ ) {

        var internalid = searchEmployees[i].getValue('internalid',null,null);
        if(internalid < 0){
            internalid = 10000;
        }
        var parentid = parseInt(searchEmployees[i].getValue('supervisor',null,null));
        if(parentid < 0){
            parentid = 10000;
        }

        var objEmployee = new Object();
        objEmployee["parentid"] = parentid;
        objEmployee["parent"] = searchEmployees[i].getText('supervisor',null,null);
        objEmployee["id"] = internalid.toString();
        objEmployee["title"] = searchEmployees[i].getValue('entityid',null,null);
        objEmployee["subtitle"] = searchEmployees[i].getValue('title',null,null);

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("isinactive",null,"is",'F'));
        arrFilters.push(new nlobjSearchFilter("supervisor",null,"anyof",parseInt(searchEmployees[i].getValue('internalid',null,null))));
        var searchSubordinates = nlapiSearchRecord('employee', null, arrFilters, arrColumns);

        if(searchSubordinates != null){
            objEmployee["type"] = 'subordinate';
        }
        else{
            objEmployee["type"] = 'staff';
        }
        arrEmployees.push(objEmployee);
    }

    var objOrgChart = new Object();
    objOrgChart["id"] = 2;
    objOrgChart["title"] = 'Org Chart';

    var objRoot = new Object();
    objRoot["parentid"] = '';
    objRoot["parent"] = '';
    objRoot["id"] = '7';
    objRoot["title"] = 'Grant Van Rooyen';
    objRoot["subtitle"] = 'Chief Executive Officer';
    objRoot["type"] = 'subordinate';

    var arrLevel1 = _.filter(arrEmployees, function(arr){
        return (arr.parentid == '7');
    });

    if(arrLevel1.length > 0){
        var arrChidren1 = new Array();
        for ( var i1 = 0; arrLevel1 != null && i1 < arrLevel1.length; i1++ ) {
            var objChild1 = new Object();
            objChild1["parentid"] = arrLevel1[i1].parentid;
            objChild1["parent"] = arrLevel1[i1].parent;
            objChild1["id"] = arrLevel1[i1].id;
            objChild1["title"] = arrLevel1[i1].title;
            objChild1["subtitle"] = arrLevel1[i1].subtitle;
            objChild1["type"] = arrLevel1[i1].type;

            var arrLevel2 = _.filter(arrEmployees, function(arr){
                return (arr.parentid == arrLevel1[i1].id);
            });
            if(arrLevel2.length > 0){
                var arrChidren2 = new Array();
                for ( var i2 = 0; arrLevel2 != null && i2 < arrLevel2.length; i2++ ) {
                    var objChild2 = new Object();
                    objChild2["parentid"] = arrLevel2[i2].parentid;
                    objChild2["parent"] = arrLevel2[i2].parent;
                    objChild2["id"] = arrLevel2[i2].id;
                    objChild2["title"] = arrLevel2[i2].title;
                    objChild2["subtitle"] = arrLevel2[i2].subtitle;
                    objChild2["type"] = arrLevel2[i2].type;

                    var arrLevel3 = _.filter(arrEmployees, function(arr){
                        return (arr.parentid == arrLevel2[i2].id);
                    });
                    if(arrLevel3.length > 0){
                        var arrChidren3 = new Array();
                        for ( var i3 = 0; arrLevel3 != null && i3 < arrLevel3.length; i3++ ) {
                            var objChild3 = new Object();
                            objChild3["parentid"] = arrLevel3[i3].parentid;
                            objChild3["parent"] = arrLevel3[i3].parent;
                            objChild3["id"] = arrLevel3[i3].id;
                            objChild3["title"] = arrLevel3[i3].title;
                            objChild3["subtitle"] = arrLevel3[i3].subtitle;
                            objChild3["type"] = arrLevel3[i3].type;

                            var arrLevel4 = _.filter(arrEmployees, function(arr){
                                return (arr.parentid == arrLevel3[i3].id);
                            });
                            if(arrLevel4.length > 0){
                                var arrChidren4 = new Array();
                                for ( var i4 = 0; arrLevel4 != null && i4 < arrLevel4.length; i4++ ) {
                                    var objChild4 = new Object();
                                    objChild4["parentid"] = arrLevel4[i4].parentid;
                                    objChild4["parent"] = arrLevel4[i4].parent;
                                    objChild4["id"] = arrLevel4[i4].id;
                                    objChild4["title"] = arrLevel4[i4].title;
                                    objChild4["subtitle"] = arrLevel4[i4].subtitle;
                                    objChild4["type"] = arrLevel4[i4].type;

                                    var arrLevel5 = _.filter(arrEmployees, function(arr){
                                        return (arr.parentid == arrLevel4[i4].id);
                                    });
                                    if(arrLevel5.length > 0){
                                        var arrChidren5 = new Array();
                                        for ( var i5 = 0; arrLevel5 != null && i5 < arrLevel5.length; i5++ ) {
                                            var objChild5 = new Object();
                                            objChild5["parentid"] = arrLevel5[i5].parentid;
                                            objChild5["parent"] = arrLevel5[i5].parent;
                                            objChild5["id"] = arrLevel5[i5].id;
                                            objChild5["title"] = arrLevel5[i5].title;
                                            objChild5["subtitle"] = arrLevel5[i5].subtitle;
                                            objChild5["type"] = arrLevel5[i5].type;

                                            var arrLevel6 = _.filter(arrEmployees, function(arr){
                                                return (arr.parentid == arrLevel5[i5].id);
                                            });
                                            if(arrLevel6.length > 0){
                                                var arrChidren6 = new Array();
                                                for ( var i6 = 0; arrLevel6 != null && i6 < arrLevel6.length; i6++ ) {
                                                    var objChild6 = new Object();
                                                    objChild6["parentid"] = arrLevel6[i6].parentid;
                                                    objChild6["parent"] = arrLevel6[i6].parent;
                                                    objChild6["id"] = arrLevel6[i6].id;
                                                    objChild6["title"] = arrLevel6[i6].title;
                                                    objChild6["subtitle"] = arrLevel6[i6].subtitle;
                                                    objChild6["type"] = arrLevel6[i6].type;

                                                    var arrLevel7 = _.filter(arrEmployees, function(arr){
                                                        return (arr.parentid == arrLevel6[i6].id);
                                                    });
                                                    if(arrLevel7.length > 0){
                                                        var arrChidren7 = new Array();
                                                        for ( var i7 = 0; arrLevel7 != null && i7 < arrLevel6.length; i7++ ) {
                                                            var objChild7 = new Object();
                                                            objChild7["parentid"] = arrLevel7[i7].parentid;
                                                            objChild7["parent"] = arrLevel7[i7].parent;
                                                            objChild7["id"] = arrLevel7[i7].id;
                                                            objChild7["title"] = arrLevel7[i7].title;
                                                            objChild7["subtitle"] = arrLevel7[i7].subtitle;
                                                            objChild7["type"] = arrLevel7[i7].type;


                                                            // add here more levels


                                                            arrChidren7.push(objChild7);
                                                        }
                                                        objChild6["children"] = arrChidren7;
                                                    }
                                                    arrChidren6.push(objChild6);
                                                }
                                                objChild5["children"] = arrChidren6;
                                            }
                                            arrChidren5.push(objChild5);
                                        }
                                        objChild4["children"] = arrChidren5;
                                    }
                                    arrChidren4.push(objChild4);
                                }
                                objChild3["children"] = arrChidren4;
                            }
                            arrChidren3.push(objChild3);
                        }
                        objChild2["children"] = arrChidren3;
                    }
                    arrChidren2.push(objChild2);
                }
                objChild1["children"] = arrChidren2;
            }
            arrChidren1.push(objChild1);
        }
        objRoot["children"] = arrChidren1;
    }

    objOrgChart["root"] = objRoot;

    //var strJSON = JSON.stringify(objOrgChart);

    //var jsonFile = nlapiCreateFile('employees_json_tree.json', 'PLAINTEXT', strJSON);
    //jsonFile.setFolder(66215);
    //nlapiSubmitFile(jsonFile);


    return JSON.stringify(objOrgChart)

}
