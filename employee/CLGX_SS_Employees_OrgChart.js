nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Employees_Organization_Chart.js
//	Script Name:	CLGX_SS_Employees_Organization_Chart
//	Script Id:		customscript_clgx_ss_emp_org_chart
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		04/13/2012
//-------------------------------------------------------------------------------------------------

function suiteletEmployeesOrgChart(request, response){
	try {
		nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------STARTED---------------------|');

		var arrColumns = new Array();
		var arrFilters = new Array();
		arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
		arrColumns[1] = new nlobjSearchColumn('entityid', null, null);
		arrColumns[2] = new nlobjSearchColumn('title', null, null);
		arrColumns[3] = new nlobjSearchColumn('supervisor', null, null);
		arrFilters[0] = new nlobjSearchFilter('isinactive',null,'is','F');
		arrFilters[1] = new nlobjSearchFilter('supervisor',null,'anyof',7);
		arrFilters[2] = new nlobjSearchFilter('internalid',null,'noneof',12827); // exclude user "Cologix Billing"
		var searchEmp1 = nlapiSearchRecord('employee', null, arrFilters, arrColumns);
		
		//var form = nlapiCreateForm('');
		//var htmlReport = form.addField('custpage_clgx_html','inlinehtml', null, null, null);

		var empoyeeOrgChartData = 'var json = {'
		empoyeeOrgChartData += 'id: "7",';
		empoyeeOrgChartData += 'name: "<b style=color:gold;>Grant van Rooyen</b></br><i>Chief Executive Officer</i>",';
		empoyeeOrgChartData += 'data: {},';
		empoyeeOrgChartData += 'children: [';
		
		for(var i=0; searchEmp1 != null && i<searchEmp1.length; i++){
			var stID1 = searchEmp1[i].getValue('internalid',null,null);
			var stName1 = searchEmp1[i].getValue('entityid',null,null);
			var stTitle1 = searchEmp1[i].getValue('title',null,null);
			var stSuperID1 = searchEmp1[i].getValue('supervisor',null,null);
			var stSuperName1 = searchEmp1[i].getText('supervisor',null,null);	
				
			empoyeeOrgChartData += '{';
			empoyeeOrgChartData += 'id: "' + stID1 + '",';
			empoyeeOrgChartData += 'name: "<b style=color:gold;>' + stName1 + '</b></br><i>' + stTitle1 + '</i>",';
			empoyeeOrgChartData += 'data:{},';
			empoyeeOrgChartData += 'children: [';
			
			var arrColumns = new Array();
			var arrFilters = new Array();
			arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
			arrColumns[1] = new nlobjSearchColumn('entityid', null, null);
			arrColumns[2] = new nlobjSearchColumn('title', null, null);
			arrColumns[3] = new nlobjSearchColumn('supervisor', null, null);
			arrFilters[0] = new nlobjSearchFilter('isinactive',null,'is','F');
			arrFilters[1] = new nlobjSearchFilter('supervisor',null,'anyof',stID1);
			arrFilters[2] = new nlobjSearchFilter('internalid',null,'noneof',12827); // exclude user "Cologix Billing"
			var searchEmp2 = nlapiSearchRecord('employee', null, arrFilters, arrColumns);

			
			for(var j=0; searchEmp2 != null && j<searchEmp2.length; j++){
				var stID2 = searchEmp2[j].getValue('internalid',null,null);
				var stName2 = searchEmp2[j].getValue('entityid',null,null);
				var stTitle2 = searchEmp2[j].getValue('title',null,null);
				var stSuperID2 = searchEmp2[j].getValue('supervisor',null,null);
				var stSuperName2 = searchEmp2[j].getText('supervisor',null,null);
				
				empoyeeOrgChartData += '{';
				empoyeeOrgChartData += 'id: "' + stID2 + '",';
				empoyeeOrgChartData += 'name: "<b style=color:gold;>' + stName2 + '</b></br><i>' + stTitle2 + '</i>",';
				empoyeeOrgChartData += 'data:{},';
				empoyeeOrgChartData += 'children: [';
				
				var arrColumns = new Array();
				var arrFilters = new Array();
				arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
				arrColumns[1] = new nlobjSearchColumn('entityid', null, null);
				arrColumns[2] = new nlobjSearchColumn('title', null, null);
				arrColumns[3] = new nlobjSearchColumn('supervisor', null, null);
				arrFilters[0] = new nlobjSearchFilter('isinactive',null,'is','F');
				arrFilters[1] = new nlobjSearchFilter('supervisor',null,'anyof',stID2);
				arrFilters[2] = new nlobjSearchFilter('internalid',null,'noneof',12827); // exclude user "Cologix Billing"
				var searchEmp3 = nlapiSearchRecord('employee', null, arrFilters, arrColumns);
				
				for(var k=0; searchEmp3 != null && k<searchEmp3.length; k++){
					var stID3 = searchEmp3[k].getValue('internalid',null,null);
					var stName3 = searchEmp3[k].getValue('entityid',null,null);
					var stTitle3 = searchEmp3[k].getValue('title',null,null);
					var stSuperID3 = searchEmp3[k].getValue('supervisor',null,null);
					var stSuperName3 = searchEmp3[k].getText('supervisor',null,null);
					
					empoyeeOrgChartData += '{';
					empoyeeOrgChartData += 'id: "' + stID3 + '",';
					empoyeeOrgChartData += 'name: "<b style=color:gold;>' + stName3 + '</b></br><i>' + stTitle3 + '</i>",';
					empoyeeOrgChartData += 'data:{},';
					empoyeeOrgChartData += 'children: [';
					
					var arrColumns = new Array();
					var arrFilters = new Array();
					arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
					arrColumns[1] = new nlobjSearchColumn('entityid', null, null);
					arrColumns[2] = new nlobjSearchColumn('title', null, null);
					arrColumns[3] = new nlobjSearchColumn('supervisor', null, null);
					arrFilters[0] = new nlobjSearchFilter('isinactive',null,'is','F');
					arrFilters[1] = new nlobjSearchFilter('supervisor',null,'anyof',stID3);
					arrFilters[2] = new nlobjSearchFilter('internalid',null,'noneof',12827); // exclude user "Cologix Billing"
					var searchEmp4 = nlapiSearchRecord('employee', null, arrFilters, arrColumns);
					
					for(var l=0; searchEmp4 != null && l<searchEmp4.length; l++){
						var stID4 = searchEmp4[l].getValue('internalid',null,null);
						var stName4 = searchEmp4[l].getValue('entityid',null,null);
						var stTitle4 = searchEmp4[l].getValue('title',null,null);
						var stSuperID4 = searchEmp4[l].getValue('supervisor',null,null);
						var stSuperName4 = searchEmp4[l].getText('supervisor',null,null);
						
						empoyeeOrgChartData += '{';
						empoyeeOrgChartData += 'id: "' + stID4 + '",';
						empoyeeOrgChartData += 'name: "<b style=color:gold;>' + stName4 + '</b></br><i>' + stTitle4 + '</i>",';
						empoyeeOrgChartData += 'data:{},';
						empoyeeOrgChartData += 'children: [';
						
						var arrColumns = new Array();
						var arrFilters = new Array();
						arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
						arrColumns[1] = new nlobjSearchColumn('entityid', null, null);
						arrColumns[2] = new nlobjSearchColumn('title', null, null);
						arrColumns[3] = new nlobjSearchColumn('supervisor', null, null);
						arrFilters[0] = new nlobjSearchFilter('isinactive',null,'is','F');
						arrFilters[1] = new nlobjSearchFilter('supervisor',null,'anyof',stID4);
						arrFilters[2] = new nlobjSearchFilter('internalid',null,'noneof',12827); // exclude user "Cologix Billing"
						var searchEmp5 = nlapiSearchRecord('employee', null, arrFilters, arrColumns);
						
						for(var m=0; searchEmp5 != null && m<searchEmp5.length; m++){
							var stID5 = searchEmp5[m].getValue('internalid',null,null);
							var stName5 = searchEmp5[m].getValue('entityid',null,null);
							var stTitle5 = searchEmp5[m].getValue('title',null,null);
							var stSuperID5 = searchEmp5[m].getValue('supervisor',null,null);
							var stSuperName5 = searchEmp5[m].getText('supervisor',null,null);
							
							empoyeeOrgChartData += '{';
							empoyeeOrgChartData += 'id: "' + stID5 + '",';
							empoyeeOrgChartData += 'name: "<b style=color:gold;>' + stName5 + '</b></br><i>' + stTitle5 + '</i>",';
							empoyeeOrgChartData += 'data:{},';
							empoyeeOrgChartData += 'children: [';
							
							
							var arrColumns = new Array();
							var arrFilters = new Array();
							arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
							arrColumns[1] = new nlobjSearchColumn('entityid', null, null);
							arrColumns[2] = new nlobjSearchColumn('title', null, null);
							arrColumns[3] = new nlobjSearchColumn('supervisor', null, null);
							arrFilters[0] = new nlobjSearchFilter('isinactive',null,'is','F');
							arrFilters[1] = new nlobjSearchFilter('supervisor',null,'anyof',stID5);
							arrFilters[2] = new nlobjSearchFilter('internalid',null,'noneof',12827); // exclude user "Cologix Billing"
							var searchEmp6 = nlapiSearchRecord('employee', null, arrFilters, arrColumns);
							
							
							for(var n=0; searchEmp6 != null && n<searchEmp6.length; n++){
								var stID6 = searchEmp6[n].getValue('internalid',null,null);
								var stName6 = searchEmp6[n].getValue('entityid',null,null);
								var stTitle6 = searchEmp6[n].getValue('title',null,null);
								var stSuperID6 = searchEmp6[n].getValue('supervisor',null,null);
								var stSuperName6 = searchEmp6[n].getText('supervisor',null,null);
								
								empoyeeOrgChartData += '{';
								empoyeeOrgChartData += 'id: "' + stID6 + '",';
								empoyeeOrgChartData += 'name: "<b style=color:gold;>' + stName6 + '</b></br><i>' + stTitle6 + '</i>",';
								empoyeeOrgChartData += 'data:{},';
								empoyeeOrgChartData += 'children: [';

								
								empoyeeOrgChartData += ']';
								empoyeeOrgChartData += '}';
								if(n<searchEmp6.length){
									empoyeeOrgChartData += ',';
								}
							}
							
							
							
							
							
							
							empoyeeOrgChartData += ']';
							empoyeeOrgChartData += '}';
							if(m<searchEmp5.length){
								empoyeeOrgChartData += ',';
							}
						}
						empoyeeOrgChartData += ']';
						empoyeeOrgChartData += '}';
						if(l<searchEmp4.length){
							empoyeeOrgChartData += ',';
						}
					}
					empoyeeOrgChartData += ']';
					empoyeeOrgChartData += '}';
					if(k<searchEmp3.length){
						empoyeeOrgChartData += ',';
					}
				}
				empoyeeOrgChartData += ']';
				empoyeeOrgChartData += '}';
				if(j<searchEmp2.length){
					empoyeeOrgChartData += ',';
				}
			}
			empoyeeOrgChartData += ']';
			empoyeeOrgChartData += '}';
			if(i<searchEmp1.length){
				empoyeeOrgChartData += ',';
			}
		}
		empoyeeOrgChartData += ']';
		empoyeeOrgChartData += '};';
		
		var objFile = nlapiLoadFile(147246);
		var dataHTML = objFile.getValue();
		dataHTML = dataHTML.replace(new RegExp('{empoyeeOrgChartData}','g'),empoyeeOrgChartData);
		//htmlReport.setDefaultValue(dataHTML);
		response.write( dataHTML );
	
//---------- End Section 1 ------------------------------------------------------------------------------------------------


nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------FINISHED---------------------|');  
} 
catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
	if (error.getDetails != undefined){
	    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	    throw error;
	}
	else{
	    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	    throw nlapiCreateError('99999', error.toString());
	}
} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

