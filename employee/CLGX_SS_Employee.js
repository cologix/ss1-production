nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Employee.js
//	Script Name:	CLGX_SS_Employee
//	Script Id:		customscript_clgx_ss_employee
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Employee
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		4/6/2012
//-------------------------------------------------------------------------------------------------

function scheduledEmployeeTermination () {
    try {
        nlapiLogExecution('DEBUG','User Event - afterSubmit','|--------------------STARTED---------------------|');
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 4/6/2012
// Details:	Send email starting on termination date of an employee until tear down NT & IT flags are checked.
//-----------------------------------------------------------------------------------------------------------------

        var context = nlapiGetContext();
        var environment = context.getEnvironment();
        if(environment == 'PRODUCTION'){

            var date = new Date();
            var arrColumns = new Array();
            var arrFilters = new Array();
            arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
            arrFilters[0] = new nlobjSearchFilter('custentity_clgx_emp_demand_termination',null,'is','T');
            arrFilters[1] = new nlobjSearchFilter('custentity_clgx_emp_flag_is_terminated',null,'is','F');
            arrFilters[2] = new nlobjSearchFilter('custentity_clgx_emp_termination_date',null,'onorbefore',date);
            var searchResults = nlapiSearchRecord('employee', null, arrFilters, arrColumns);

            for ( var i = 0; searchResults != null && i < searchResults .length; i++ ) {
                var searchResult = searchResults[i];
                var stId = searchResult.getValue('internalid');
                var myRecord = nlapiLoadRecord('employee', stId);
                var stNewEmpFN = myRecord.getFieldValue('firstname');
                var stNewEmpLN = myRecord.getFieldValue('lastname');
                var stSuperviserId = myRecord.getFieldValue('supervisor');
                //get the emp termination date
                var terminationDate= myRecord.getFieldValue('custentity_clgx_emp_termination_date');
                var terminationTime=myRecord.getFieldValue('custentity_clgx_emp_term_time');
                if((terminationDate!=null)&&(terminationDate!=''))
                {
                    var date=new Date(terminationDate);
                    var m_names = new Array("January", "February", "March",
                        "April", "May", "June", "July", "August", "September",
                        "October", "November", "December");
                    var curr_date = date.getDate();
                    var curr_month =date.getMonth();
                    var curr_year = date.getFullYear();
                    var termDat=' on '+m_names[curr_month]+', '+curr_date+ ' '+curr_year+'.';
                    var termDatS=' with termination date on '+m_names[curr_month]+', '+curr_date+ ' '+curr_year+',';
                }
                else
                {
                    var termDat='.';
                    var termDatS=',';
                }


                if (stSuperviserId != null && stSuperviserId != ''){
                    var stSupervisorId = myRecord.getFieldValue('supervisor');
                    var stSupervisor = myRecord.getFieldText('supervisor');
                    var stTeardownNT = myRecord.getFieldValue('custentity_clgx_emp_ns_teardown_cmp');
                    var stTeardownIT = myRecord.getFieldValue('custentity_clgx_emp_it_teardown_cmp');
                    if (stTeardownNT == 'T' && stTeardownIT =='T'){
                        var emailSubject = 'An employee account was terminated.';
                        var emailBody = 'The account of the employee named ' + stNewEmpFN + ' ' + stNewEmpLN + ', was terminated.\n\n\n';
                        emailBody += '<a href="https://1337135.app.netsuite.com/app/common/entity/employee.nl?id=' + stId + '&whence="">You may click here to access the record.</a><br/><br/>';
                        
                        /*nlapiSendEmail(stSuperviserId, 67,emailSubject,emailBody,null,null,null,null,true); // Send email to Kelsey
                        nlapiSendEmail(stSuperviserId, 2498,emailSubject,emailBody,null,null,null,null,true); // Send email to Heidi
                        nlapiSendEmail(stSuperviserId, 410247,emailSubject,emailBody,null,null,null,null,true); // Send email to Don Poskin
                        nlapiSendEmail(stSuperviserId, 2421,emailSubject,emailBody,null,null,null,null,true); // Send email to Keith Manfredi

                        // Send emails to the Process and Technology team
                        nlapiSendEmail(stSuperviserId, 600807,emailSubject,emailBody,null,null,null,null,true); // Send email to S Stogner
                        nlapiSendEmail(stSuperviserId, 9963,emailSubject,emailBody,null,null,null,null,true); // Send email to Lance
                        nlapiSendEmail(stSuperviserId, 277339,emailSubject,emailBody,null,null,null,null,true); // Send email to Lou Lopez
                        nlapiSendEmail(stSuperviserId, 2406,emailSubject,emailBody,null,null,null,null,true); // Send email to Lisa
                        nlapiSendEmail(stSuperviserId, -5,emailSubject,emailBody,null,null,null,null,true); // Send email to Val
                        nlapiSendEmail(stSuperviserId, 71418,emailSubject,emailBody,null,null,null,null,true);*/ // Send email to Dan

                        clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_emp_off_daily_final", emailSubject, emailBody);
                        
                        myRecord.setFieldValue('custentity_clgx_emp_flag_is_terminated', 'T');
                        myRecord.setFieldText('custentity_clgx_emp_status', 'Terminated');
                        nlapiSubmitRecord(myRecord, true, true);

                    }
                    else{

                        var emailSubject = 'Reminder: The termination of an employee was asked by his hiring manager and requires your attention.';
                        var emailBody = 'Termination of the account of the employee named ' + stNewEmpFN + ' ' + stNewEmpLN + termDatS+', '+terminationTime+','+' was asked by his manager, ' +  stSupervisor + ' and requires your attention.<br/><br/>';
                        emailBody += '<a href="https://1337135.app.netsuite.com/app/common/entity/employee.nl?id=' + stId + '&whence="">Please click here to access the record.</a><br/><br/>';

                        /*nlapiSendEmail(stSuperviserId, 67,emailSubject,emailBody,null,null,null,null,true); // Send email to Kelsey
                        nlapiSendEmail(stSuperviserId, 410247,emailSubject,emailBody,null,null,null,null,true); // Send email to Don Poskin
                        nlapiSendEmail(stSuperviserId, 2421,emailSubject,emailBody,null,null,null,null,true); // Send email to Keith Manfredi

                        // Send emails to the Process and Technology team
                        nlapiSendEmail(stSuperviserId, 600807,emailSubject,emailBody,null,null,null,null,true); // Send email to S Stogner
                        nlapiSendEmail(stSuperviserId, 9963,emailSubject,emailBody,null,null,null,null,true); // Send email to Lance
                        nlapiSendEmail(stSuperviserId, 277339,emailSubject,emailBody,null,null,null,null,true); // Send email to Lou Lopez
                        nlapiSendEmail(stSuperviserId, 2406,emailSubject,emailBody,null,null,null,null,true); // Send email to Lisa
                        nlapiSendEmail(stSuperviserId, -5,emailSubject,emailBody,null,null,null,null,true); // Send email to Val
                        nlapiSendEmail(stSuperviserId, 71418,emailSubject,emailBody,null,null,null,null,true);*/ // Send email to Dan

                        clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_emp_off_daily_final", emailSubject, emailBody);
                        
                        var myRecord = nlapiLoadRecord('employee', stId);
                        myRecord.setFieldValue('custentity_clgx_emp_flag_termination', 'T');
                        myRecord.setFieldText('custentity_clgx_emp_status', 'Waiting termination');
                        nlapiSubmitRecord(myRecord, true, true);

                    }

                }

            }

            //---------- End Section 1 ------------------------------------------------------------------------------------------------
        }

        nlapiLogExecution('DEBUG','User Event - beforeSubmit','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}