//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Employee.js
//	Script Name:	CLGX_SU_Employee
//	Script Id:		customscript_clgx_cr_employee
//	Script Runs:	Client
//	Script Type:	Client Script
//	Deployments:	Employee
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		2/28/2012
//-------------------------------------------------------------------------------------------------

function pageInit(type){
	try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 2/28/2012
// Details:	Hides or not field 'Employee Onboard Status' depending on user role.
//-------------------------------------------------------------------------------------------------

		var currentContext = nlapiGetContext();
		var stUserRole = currentContext.getRole();
		var stUserId = nlapiGetUser();
		if (currentContext.getExecutionContext() == 'userinterface'){
			
			// if role Admin, Full, HR Representative enable 'Employee Onboard Status' field
			//if (stUserRole != '3' && stUserRole != '-5' && stUserRole != '18' && stUserRole != '1032'){ 
			//	nlapiDisableField('custentity_clgx_emp_status', false);
			//}
			
			// a new employee record  is created by a hiring manager - put him in the field 'Hired by'
			if (type == 'create') { 
				nlapiSetFieldValue('custentity_clgx_emp_hired_by', stUserId);
				nlapiDisableField('custentity_clgx_emp_demand_termination', true);
				nlapiDisableField('custentity_clgx_emp_termination_date', true);
				nlapiDisableField('custentity_clgx_emp_email_account_off', true);
				nlapiDisableField('custentity_clgx_emp_box_account_off', true);
			}
		}

		
//---------- End Section 1 ------------------------------------------------------------------------------------------------

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
