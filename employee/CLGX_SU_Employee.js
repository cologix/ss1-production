nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Employee.js
//	Script Name:	CLGX_SU_Employee
//	Script Id:		customscript_clgx_su_employee
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Employee
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		2/28/2012
//-------------------------------------------------------------------------------------------------

function afterSubmit (type) {
    try {
        nlapiLogExecution('DEBUG','User Event - afterSubmit','|--------------------STARTED---------------------|');
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.1 - 4/6/2012
// Details:	Manage the on boarding process for a new employee and sends emails depending on the on boarding status.
//-----------------------------------------------------------------------------------------------------------------

        var currentContext = nlapiGetContext();
        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'create' || type == 'edit')) {

            var stUserId = nlapiGetUser();
            var stNewEmpId = nlapiGetRecordId();
            var stNewEmpFN = nlapiGetFieldValue('firstname');
            var stNewEmpLN = nlapiGetFieldValue('lastname');
            var stHiredBy = nlapiGetFieldText('custentity_clgx_emp_hired_by');
            var stHiredById = nlapiGetFieldValue('custentity_clgx_emp_hired_by');
            var stSupervisorId = nlapiGetFieldValue('supervisor');
            var stSupervisor = nlapiGetFieldText('supervisor');
            var startDate=nlapiGetFieldValue('custentity_clgx_emp_start_date');
            if((startDate!=null)&&(startDate!=''))
            {
                var date=new Date(startDate);
                var m_names = new Array("January", "February", "March",
                    "April", "May", "June", "July", "August", "September",
                    "October", "November", "December");
                var curr_date = date.getDate();
                var curr_month =date.getMonth();
                var curr_year = date.getFullYear();
                var startDat='; will start work in '+m_names[curr_month]+', '+curr_date+ ' '+curr_year+' ';
            }
            else
            {
                var startDat=' ';
            }
            if (type == 'create') { // a new employee record  is created by a hiring manager

                var myRecord = nlapiLoadRecord('employee', stNewEmpId);
                myRecord.setFieldText('custentity_clgx_emp_status', 'Waiting configuration'); // configure status
                myRecord.setFieldText('class', 'Recurring'); // configure Expense Class
                var facID=clgx_return_facilityid (nlapiGetFieldValue('location'));
                if(facID!=0) {
                    myRecord.setFieldValue('custentity_cologix_facility',facID);
                }
                nlapiSubmitRecord(myRecord, true, true);

                var stHiredBy = nlapiLookupField('employee', stUserId, 'entityid'); // pull out the name of the hiring manager
                var emailSubject = 'A new employee was hired and requires your attention.';
                var emailBody = 'A new employee, named ' +  stNewEmpFN + ' ' + stNewEmpLN  + ', was hired by ' +  stHiredBy +startDat+'and requires your attention.<br/><br/>';
                
                var employeeType= nlapiGetFieldValue('employeetype');
                if (employeeType == 5){
                	 emailBody += 'Employee is type: Contractor. <br/><br/>';
                }
                
                emailBody += '<a href="https://1337135.app.netsuite.com/app/common/entity/employee.nl?id=' + stNewEmpId + '&whence="">Please click here to access the record.</a><br/><br/>';

                if (stHiredById != null && stHiredById != ''){
                    nlapiSendEmail(stUserId, stHiredById,emailSubject,emailBody,null,null,null,null,true); // Send email to the hiring manager
                }
                if ((stSupervisorId != null && stSupervisorId != '') && (stHiredById != null && stHiredById != '') && stSupervisorId != stHiredById){
                    nlapiSendEmail(stUserId, stSupervisorId,emailSubject,emailBody,null,null,null,null,true); // Send email to Supervisor if other than Hiring Manager
                }

                /*nlapiSendEmail(stUserId, 67,emailSubject,emailBody,null,null,null,null,true); // Send email to Kelsey
                nlapiSendEmail(stUserId, 2498,emailSubject,emailBody,null,null,null,null,true); // Send email to Heidi
                nlapiSendEmail(stUserId, 5782,emailSubject,emailBody,null,null,null,null,true); // Send email to Jessica Bennett
                nlapiSendEmail(stUserId, 8,emailSubject,emailBody,null,null,null,null,true); // Send email to Brian Cox
                nlapiSendEmail(stUserId, 5415,emailSubject,emailBody,null,null,null,null,true); // Send email to Cecily Fink
                nlapiSendEmail(stUserId, 11016,emailSubject,emailBody,null,null,null,null,true); // Send email to Bevin Orlinski
                
                nlapiSendEmail(stUserId, 1403531,emailSubject,emailBody,null,null,null,null,true); // Send email to John Hill
                nlapiSendEmail(stUserId, 1448188,emailSubject,emailBody,null,null,null,null,true); // Send email to Amber Wilson
                
                nlapiSendEmail(stUserId, 410247,emailSubject,emailBody,null,null,null,null,true); // Send email to Don Poskin
                nlapiSendEmail(stUserId, 375733,emailSubject,emailBody,null,null,null,null,true); // Send email to Josh Beck
                nlapiSendEmail(stUserId, 224966,emailSubject,emailBody,null,null,null,null,true); // Send email to Ginette Hannah
                nlapiSendEmail(stUserId, 713181,emailSubject,emailBody,null,null,null,null,true); // Send email to Chad Cummins
                nlapiSendEmail(stUserId, 2421,emailSubject,emailBody,null,null,null,null,true); // Send email to Keith Manfredi 

                // Send emails to the Process and Technology team
                nlapiSendEmail(stUserId, 2406,emailSubject,emailBody,null,null,null,null,true); // Send email to Lisa
                nlapiSendEmail(stUserId, 206211,emailSubject,emailBody,null,null,null,null,true);//Send email to Cat
                nlapiSendEmail(stUserId, -5,emailSubject,emailBody,null,null,null,null,true); // Send email to Val
                nlapiSendEmail(stUserId, 71418,emailSubject,emailBody,null,null,null,null,true);*/ // Send email to Dan
                
                clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_emp_onoff_notify", emailSubject, emailBody);
            }

            else if (type == 'edit') { // something was  modified on the employee record

                var stAskTermination = nlapiGetFieldValue('custentity_clgx_emp_demand_termination');
                var flagActive = nlapiGetFieldValue('custentity_clgx_emp_flag_is_active');
                var flagTermination = nlapiGetFieldValue('custentity_clgx_emp_flag_termination');
                var flagTerminated = nlapiGetFieldValue('custentity_clgx_emp_flag_is_terminated');
                var stNSCompleted = nlapiGetFieldValue('custentity_clgx_emp_ns_setup_cmp');
                var stITCompleted = nlapiGetFieldValue('custentity_clgx_emp_it_setup_cmp');

                if (stNSCompleted == 'T' && stITCompleted == 'T' && stAskTermination == 'F' && flagActive == 'F') {
                    var emailSubject = 'The account of an employee you hired is configured and active.';
                    var emailBody = 'Your new hired, named ' + stNewEmpFN + ' ' + stNewEmpLN + ', has been set up in Netsuite, Box.net and Google Apps, all requirements for hardware and software has been ordered.<br/><br/>';
                    emailBody += '<a href="https://1337135.app.netsuite.com/app/common/entity/employee.nl?id=' + stNewEmpId + '&whence="">Please click here to access the record.</a><br/><br/>';
                    if (stHiredById != null && stHiredById != ''){
                        nlapiSendEmail(stUserId, stHiredById,emailSubject,emailBody,null,null,null,null,true); // Send email to the hiring manager
                    }
                    if ((stSupervisorId != null && stSupervisorId != '') && (stHiredById != null && stHiredById != '') && stSupervisorId != stHiredById){
                        nlapiSendEmail(stUserId, stSupervisorId,emailSubject,emailBody,null,null,null,null,true); // Send email to Supervisor if other than Hiring Manager
                    }
                    
                   	/*nlapiSendEmail(stUserId, 67,emailSubject,emailBody,null,null,null,null,true); // Send email to Kelsey
                    nlapiSendEmail(stUserId, 2498,emailSubject,emailBody,null,null,null,null,true); // Send email to Heidi Diemar
                    nlapiSendEmail(stUserId, 5782,emailSubject,emailBody,null,null,null,null,true); // Send email to Jessica Bennett
                    nlapiSendEmail(stUserId, 8,emailSubject,emailBody,null,null,null,null,true); // Send email to Brian Cox
                    nlapiSendEmail(stUserId, 5415,emailSubject,emailBody,null,null,null,null,true); // Send email to Cecily Fink
                    nlapiSendEmail(stUserId, 11016,emailSubject,emailBody,null,null,null,null,true); // Send email to Bevin Orlinski
                        
                    nlapiSendEmail(stUserId, 1403531,emailSubject,emailBody,null,null,null,null,true); // Send email to John Hill
                    nlapiSendEmail(stUserId, 1448188,emailSubject,emailBody,null,null,null,null,true); // Send email to Amber Wilson
                        
                    nlapiSendEmail(stUserId, 410247,emailSubject,emailBody,null,null,null,null,true); // Send email to Don Poskin
                    nlapiSendEmail(stUserId, 375733,emailSubject,emailBody,null,null,null,null,true); // Send email to Josh Beck
                    nlapiSendEmail(stUserId, 224966,emailSubject,emailBody,null,null,null,null,true); // Send email to Ginette Hannah
                    nlapiSendEmail(stUserId, 713181,emailSubject,emailBody,null,null,null,null,true); // Send email to Chad Cummins
                    nlapiSendEmail(stUserId, 2421,emailSubject,emailBody,null,null,null,null,true); // Send email to Keith Manfredi 


                    // Send emails to the Process and Technology team
                    nlapiSendEmail(stUserId, 2406,emailSubject,emailBody,null,null,null,null,true); // Send email to Lisa
                    nlapiSendEmail(stUserId, 206211,emailSubject,emailBody,null,null,null,null,true);//Send email to Cat
                    nlapiSendEmail(stUserId, -5,emailSubject,emailBody,null,null,null,null,true); // Send email to Val
                    nlapiSendEmail(stUserId, 71418,emailSubject,emailBody,null,null,null,null,true);*/ // Send email to Dan
                        
                    clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_emp_onoff_notify", emailSubject, emailBody);
                    
                    var myRecord = nlapiLoadRecord('employee', stNewEmpId);
                    myRecord.setFieldValue('custentity_clgx_emp_flag_is_active', 'T');
                    myRecord.setFieldText('custentity_clgx_emp_status', 'Active');
                    nlapiSubmitRecord(myRecord, true, true);
                }
                else if (stAskTermination == 'T' && flagTermination == 'F') {

                    var stTerminDate = nlapiGetFieldValue('custentity_clgx_emp_termination_date');
                    var terminationTime=nlapiGetFieldValue('custentity_clgx_emp_term_time');
                    var comp=nlapiGetFieldTexts('custentity_clgx_compliance');
                    var createCase=0;
                    for (var r = 0; comp != null && r < comp.length; r++) {
                        if(comp[r]=='HIPAA-ePHI')
                        {
                            createCase=1;
                        }
                    }
                    if(createCase==1) {
                        var fac = nlapiGetFieldValue('custentity_cologix_facility');
                        nlapiLogExecution('DEBUG', 'createCase', createCase);
                        //Create a Compliance Case
                        var recordCase = nlapiCreateRecord('supportcase');
                        recordCase.setFieldValue('title', 'Off Board ' + stNewEmpFN + ' ' + stNewEmpLN + ' from HIPPA-ePHI systems');
                        recordCase.setFieldValue('company', stSupervisorId);
                        // record.setFieldValue('contact', contactid);
                        // recordCase.setFieldValue('email', emailR);
                        recordCase.setFieldText('custevent_cologix_facility', fac);
                        recordCase.setFieldValue('category', 9);
                        recordCase.setFieldValue('custevent_cologix_sub_case_type', 113);
                        recordCase.setFieldValue('assigned', 1028406);
                        recordCase.setFieldValue('status', 1);
                        recordCase.setFieldText('origin', 'Internal');
                        recordCase.setFieldValue('messagenew', 'T');
                        recordCase.setFieldValue('custevent_cologix_case_sched_followup',stTerminDate);
                        recordCase.setFieldValue('custevent_cologix_sched_start_time', terminationTime);
                        var messageR ='Remove system access for ' + stNewEmpFN + ' ' + stNewEmpLN + ' per the scheduled date and time noted in this case';
                        recordCase.setFieldValue('incomingmessage', messageR);

                        try {
                            var recordId = nlapiSubmitRecord(recordCase, true, true);
                        }
                        catch (error) {
                            nlapiLogExecution('ERROR', 'Process Error', error.getCode() + ': ' + error.getDetails());
                        }
                        //
                    }

                    if((stTerminDate !=null)&&(stTerminDate !=''))
                    {
                        var date=new Date(stTerminDate);
                        var m_names = new Array("January", "February", "March",
                            "April", "May", "June", "July", "August", "September",
                            "October", "November", "December");
                        var curr_date = date.getDate();
                        var curr_month =date.getMonth();
                        var curr_year = date.getFullYear();
                        var termDat=' with termination date on '+m_names[curr_month]+', '+curr_date+ ' '+curr_year+',';
                    }
                    else
                    {
                        var termDat=',';
                    }

                    if (stTerminDate == ''){
                        var emailSubject = 'The termination of an employee account was asked by his hiring manager and requires your attention.';
                        var emailBody = 'Termination of the account of the employee named ' + stNewEmpFN + ' ' + stNewEmpLN+', was asked by his manager, ' +  stSupervisor + ' and requires your attention.<br/><br/>';
                        emailBody += '<a href="https://1337135.app.netsuite.com/app/common/entity/employee.nl?id=' + stNewEmpId + '&whence="">Please click here to access the record.</a><br/><br/>';

                        if (stHiredById != null && stHiredById != ''){
                            nlapiSendEmail(stUserId, stHiredById,emailSubject,emailBody,null,null,null,null,true); // Send email to the hiring manager
                        }
                        if ((stSupervisorId != null && stSupervisorId != '') && (stHiredById != null && stHiredById != '') && stSupervisorId != stHiredById){
                            nlapiSendEmail(stUserId, stSupervisorId,emailSubject,emailBody,null,null,null,null,true); // Send email to Supervisor if other than Hiring Manager
                        }

                        /*nlapiSendEmail(stUserId, 67,emailSubject,emailBody,null,null,null,null,true); // Send email to Kelsey
                        nlapiSendEmail(stUserId, 410247,emailSubject,emailBody,null,null,null,null,true); // Send email to Don Poskin
                        nlapiSendEmail(stUserId, 2421,emailSubject,emailBody,null,null,null,null,true); // Send email to Keith Manfredi

                        nlapiSendEmail(stUserId, 375733,emailSubject,emailBody,null,null,null,null,true); // Send email to Josh Beck
                        nlapiSendEmail(stUserId, 600807,emailSubject,emailBody,null,null,null,null,true); // Send email to Stephen Stogner
                        nlapiSendEmail(stUserId, 224966,emailSubject,emailBody,null,null,null,null,true); // Send email to Ginette Hannah
                        nlapiSendEmail(stUserId, 713181,emailSubject,emailBody,null,null,null,null,true); // Send email to Chad Cummins
                        
                        nlapiSendEmail(stUserId, 1403531,emailSubject,emailBody,null,null,null,null,true); // Send email to John Hill

                        // Send emails to the Process and Technology team
                        nlapiSendEmail(stUserId, 2406,emailSubject,emailBody,null,null,null,null,true); // Send email to Lisa
                        nlapiSendEmail(stUserId, -5,emailSubject,emailBody,null,null,null,null,true); // Send email to Val
                        nlapiSendEmail(stUserId, 71418,emailSubject,emailBody,null,null,null,null,true);*/ // Send email to Dan

                        clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_emp_onoff_notify", emailSubject, emailBody);
                        
                        var myRecord = nlapiLoadRecord('employee', stNewEmpId);
                        //myRecord.setFieldValue('custentity_clgx_emp_flag_termination', 'T');
                        myRecord.setFieldText('custentity_clgx_emp_status', 'Waiting termination');
                        nlapiSubmitRecord(myRecord, true, true);
                    }
                    else{
                        var emailSubject = 'The termination of an employee was asked by his hiring manager and requires your attention.';
                        var emailBody = 'Termination of the account of the employee named ' + stNewEmpFN + ' ' + stNewEmpLN + termDat+', '+terminationTime+','+' was asked by his manager, ' +  stSupervisor + ' and requires your attention.<br/><br/>';
                        emailBody += '<a href="https://1337135.app.netsuite.com/app/common/entity/employee.nl?id=' + stNewEmpId + '&whence="">Please click here to access the record.</a><br/><br/>';

                        /*nlapiSendEmail(stUserId, 67,emailSubject,emailBody,null,null,null,null,true); // Send email to Kelsey Barach
                        nlapiSendEmail(stUserId, 2498,emailSubject,emailBody,null,null,null,null,true); // Send email to Heidi Diemar
                        nlapiSendEmail(stUserId, 5782,emailSubject,emailBody,null,null,null,null,true); // Send email to Jessica Bennett
                        nlapiSendEmail(stUserId, 8,emailSubject,emailBody,null,null,null,null,true); // Send email to Brian Cox
                        nlapiSendEmail(stUserId, 5415,emailSubject,emailBody,null,null,null,null,true); // Send email to Cecily Fink
                        nlapiSendEmail(stUserId, 11016,emailSubject,emailBody,null,null,null,null,true); // Send email to Bevin Orlinski
                        nlapiSendEmail(stUserId, 410247,emailSubject,emailBody,null,null,null,null,true); // Send email to Don Poskin
                        nlapiSendEmail(stUserId, 2421,emailSubject,emailBody,null,null,null,null,true); // Send email to Keith Manfredi
                        nlapiSendEmail(stUserId, 375733,emailSubject,emailBody,null,null,null,null,true); // Send email to Josh Beck
                        nlapiSendEmail(stUserId, 600807,emailSubject,emailBody,null,null,null,null,true); // Send email to Stephen Stogner
                        nlapiSendEmail(stUserId, 224966,emailSubject,emailBody,null,null,null,null,true); // Send email to Ginette Hannah
                        nlapiSendEmail(stUserId, 713181,emailSubject,emailBody,null,null,null,null,true); // Send email to Chad Cummins
                        
                        nlapiSendEmail(stUserId, 1403531,emailSubject,emailBody,null,null,null,null,true); // Send email to John Hill

                        // Send emails to the Process and Technology team
                        nlapiSendEmail(stUserId, 2406,emailSubject,emailBody,null,null,null,null,true); // Send email to Lisa
                        nlapiSendEmail(stUserId, -5,emailSubject,emailBody,null,null,null,null,true); // Send email to Val
                        nlapiSendEmail(stUserId, 71418,emailSubject,emailBody,null,null,null,null,true);*/ // Send email to Dan

                        clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_emp_onoff_notify", emailSubject, emailBody);
                        
                        var myRecord = nlapiLoadRecord('employee', stNewEmpId);
                        myRecord.setFieldValue('custentity_clgx_emp_flag_termination', 'T');
                        myRecord.setFieldText('custentity_clgx_emp_status', 'Waiting termination');
                        nlapiSubmitRecord(myRecord, true, true);
                    }
                }
                else {}
            }
            else {}
        }

//---------- End Section 1 ------------------------------------------------------------------------------------------------
        try {
            //------------- NNJ SYNC -----------------------------------------------------------------------------------
            var currentContext = nlapiGetContext();
            var environment = currentContext.getEnvironment();
            var row = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
            if (environment == 'PRODUCTION' && (type == 'xedit'  || currentContext.getExecutionContext() == 'userinterface')) {

                var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
                record.setFieldValue('custrecord_clgx_queue_ping_record_id', nlapiGetRecordId());
                record.setFieldValue('custrecord_clgx_queue_ping_record_type', 7);
                record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
                var idRec = nlapiSubmitRecord(record, false,true);

                /*
                 try {
                 flexapi('POST','/netsuite/update', {
                 'type': nlapiGetRecordType(),
                 'id': nlapiGetRecordId(),
                 'action': type,
                 'userid': nlapiGetUser(),
                 'user': nlapiLookupField('employee', nlapiGetUser(), 'entityid'),
                 'data': json_serialize(row)
                 });
                 }
                 catch (error) {
                 nlapiLogExecution('DEBUG', 'flexapi error: ', error);
                 var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
                 record.setFieldValue('custrecord_clgx_queue_ping_record_id', nlapiGetRecordId());
                 record.setFieldValue('custrecord_clgx_queue_ping_record_type', 7);
                 record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
                 var idRec = nlapiSubmitRecord(record, false,true);
                 }
                 */
            }
        }
        catch (error) {
            if (error.getDetails != undefined){
                nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
                throw error;
            }
            else{
                nlapiLogExecution('ERROR','Unexpected Error', error.toString());
                throw nlapiCreateError('99999', error.toString());
            }
        }

        nlapiLogExecution('DEBUG','User Event - beforeSubmit','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}