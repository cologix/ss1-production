//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Employees_Organization_Chart.js
//	Script Name:	CLGX_SS_Employees_Organization_Chart
//	Script Id:		customscript_clgx_ss_org_chart
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		04/13/2012
//-------------------------------------------------------------------------------------------------

function suiteletEmployeesOrgChart(request, response){
	try {
		nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------STARTED---------------------|');

		var arrColumns = new Array();
		var arrFilters = new Array();
		arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
		arrColumns[1] = new nlobjSearchColumn('entityid', null, null);
		arrColumns[2] = new nlobjSearchColumn('title', null, null);
		arrColumns[3] = new nlobjSearchColumn('supervisor', null, null);
		arrFilters[0] = new nlobjSearchFilter('isinactive',null,'is','F');
		arrFilters[1] = new nlobjSearchFilter('supervisor',null,'anyof',7);
		var searchEmp1 = nlapiSearchRecord('employee', null, arrFilters, arrColumns);
		
		var form = nlapiCreateForm('Organizational Chart');
		var htmlReport = form.addField('custpage_clgx_html','inlinehtml', null, null, null);
		
		var html = '';
		html += '<link rel="stylesheet" type="text/css" href="//www.cologix.com//netsuite_includes/SlickmapCSS/slickmap.css">';
		
		html += '<ul id="primaryNav" class="col5">';
		html += '<li id="home"><a href="http://sitetitle.com">Home</a></li>';
	
		
		
		for(var i=0; searchEmp1 != null && i<searchEmp1.length; i++){
			var stID1 = searchEmp1[i].getValue('internalid',null,null);
			var stName1 = searchEmp1[i].getValue('entityid',null,null);
			var stTitle1 = searchEmp1[i].getValue('title',null,null);
			var stSuperID1 = searchEmp1[i].getValue('supervisor',null,null);
			var stSuperName1 = searchEmp1[i].getText('supervisor',null,null);
			
			html += '<li><a href="">' + stName1 + '</a>';
			html += '<ul>';
			
			var arrColumns = new Array();
			var arrFilters = new Array();
			arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
			arrColumns[1] = new nlobjSearchColumn('entityid', null, null);
			arrColumns[2] = new nlobjSearchColumn('title', null, null);
			arrColumns[3] = new nlobjSearchColumn('supervisor', null, null);
			arrFilters[0] = new nlobjSearchFilter('isinactive',null,'is','F');
			arrFilters[1] = new nlobjSearchFilter('supervisor',null,'anyof',stID1);
			var searchEmp2 = nlapiSearchRecord('employee', null, arrFilters, arrColumns);
			
			
			for(var j=0; searchEmp2 != null && j<searchEmp2.length; j++){
				var stID2 = searchEmp2[j].getValue('internalid',null,null);
				var stName2 = searchEmp2[j].getValue('entityid',null,null);
				var stTitle2 = searchEmp2[j].getValue('title',null,null);
				var stSuperID2 = searchEmp2[j].getValue('supervisor',null,null);
				var stSuperName2 = searchEmp2[j].getText('supervisor',null,null);
				
				html += '<li><a href="">' + stName2 + '</a>';
				html += '<ul>';
				
				
				
				
				
				
				
				
				html += '</ul>';
			}
			html += '</ul>';
			
			html += '          [{v:"' + stName + '", f:"' + stName + '<div class=title>' + stTitle + '</div>"}, "' + stSuperviser + '", "' + stTitle + '"],';
		
		}
		
		html += '</ul>';

		htmlReport.setDefaultValue(html);
		response.writePage( form );
	
//---------- End Section 1 ------------------------------------------------------------------------------------------------


nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------FINISHED---------------------|');  
} 
catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
	if (error.getDetails != undefined){
	    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	    throw error;
	}
	else{
	    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	    throw nlapiCreateError('99999', error.toString());
	}
} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

