nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Employee_Org_Chart_PDF.js
//	Script Name:	CLGX_SL_Org_Chart_PDF
//	Script Id:		customscript_clgx_sl_empl_org_chart_pdf
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		07/01/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=308&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_org_chart_pdf (request, response){
	try {

		//var objFile = nlapiLoadFile(1691522);
		//var html = objFile.getValue();
		//response.write( html );
		
		
		var form = nlapiCreateForm('Organization Chart to PDF');
		var htmlReport = form.addField('custpage_clgx_org_chart','inlinehtml', null, null, null);
		
		var objFile = nlapiLoadFile(1691522);
		var dataHTML = objFile.getValue();
		
		htmlReport.setDefaultValue(dataHTML);

		response.writePage( form );
		
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
