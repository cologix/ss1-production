//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Report_Capex.js
//	Script Name:	CLGX_SL_Report_Capex
//	Script Id:		customscript_clgx_sl_report_capex
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		3/9/2012
//-------------------------------------------------------------------------------------------------

function suiteletReportCapexProj(request, response){
	try {
		nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------STARTED---------------------|');

		var arrColumns = new Array();
		var arrFilters = new Array();
		arrColumns[0] = new nlobjSearchColumn('subsidiary', null, 'GROUP');
		arrColumns[1] = new nlobjSearchColumn('subsidiarynohierarchy', null, 'GROUP');
		arrFilters[0] = new nlobjSearchFilter('subsidiary',null,'noneof','@NONE@');
		var searchSubsidiaries = nlapiSearchRecord('job', null, arrFilters, arrColumns);
		
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
		arrColumns[1] = new nlobjSearchColumn('altname', null, null).setSort(false);
		var searchFacilities = nlapiSearchRecord('customrecord_cologix_facility', null, arrFilters, arrColumns);
		
		var form = nlapiCreateForm('Capex Report');
		//var groupFilters = form.addFieldGroup('groupFilters', 'Filters',null);
		
		var checkCSV = form.addField('custpage_clgx_csv', 'checkbox', 'Export to CSV', null, null);
		checkCSV.setLayoutType('outside','startcol');
		
        var selectSubsidiaries = form.addField('custpage_clgx_subsidiary','multiselect', 'Subsidaries', null, null);
        var arrSubsidiaries = new Array();
	    for (var i = 0; searchSubsidiaries != null && i < searchSubsidiaries.length; i++) {
	    	selectSubsidiaries.addSelectOption(searchSubsidiaries[i].getValue('subsidiary',null,'GROUP'), searchSubsidiaries[i].getValue('subsidiarynohierarchy',null,'GROUP'));
		}
	    selectSubsidiaries.setLayoutType('normal','startrow');
	    //selectSubsidiaries.setDisplaySize(120, 3);
	    
        var selectFacilities = form.addField('custpage_clgx_facility','multiselect', 'Facilities', null, null);
        var arrfacilities = searchFacilities[0].getValue('custentity_cologix_facility',null,'GROUP');
	    for (var i = 0; i < searchFacilities != null &&  i < searchFacilities.length; i++) {
	    	selectFacilities.addSelectOption(searchFacilities[i].getValue('internalid',null,null), searchFacilities[i].getValue('altname',null,null));
		}
	    selectFacilities.setLayoutType('normal','startrow');
	    //electFacilities.setDisplaySize(170, 3);
	    
	    
		if(request.getMethod() == 'GET'){ //GET call

			//var groupResults = form.addFieldGroup('groupResults', 'Results',null);
			var htmlReport = form.addField('custpage_clgx_html','inlinehtml', null, null, null);
				htmlReport.setLayoutType('outside','startrow');
				
			// main search on Projects on GET
			var searchProj = nlapiSearchRecord('job', 'customsearch_clgx_rep_capex_projects', null, null);	
				
		}
		
		else{ //POST call
			
			//var groupResults = form.addFieldGroup('groupResults', 'Results',null);
			var htmlReport = form.addField('custpage_clgx_html','inlinehtml', null, null, null);
				htmlReport.setLayoutType('outside','startrow');
				
			// main search on Projects on POST - 
			var arrColumns = new Array();
			var arrFilters = new Array();
			var reqSubsidiaries = request.getParameter('custpage_clgx_subsidiary');
			var reqFacilities = request.getParameter('custpage_clgx_facility');
			var reqCSV = request.getParameter('custpage_clgx_csv');
			
			if (reqSubsidiaries != ''){ // if any subsidiary filter is asked
				arrFilters.push(new nlobjSearchFilter('subsidiary',null,'anyof',reqSubsidiaries));
			}
			if (reqFacilities != ''){ // if any subsidiary filter is asked
				arrFilters.push(new nlobjSearchFilter('custentity_cologix_facility',null,'anyof',reqFacilities));
			}
			var searchProj = nlapiSearchRecord('job', 'customsearch_clgx_rep_capex_projects', arrFilters, arrColumns);
				
		}
		
		// child searches
		var searchPOProj = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_po_proj', null, null); 
		var searchBillProj = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_bill_proj', null, null);
		var searchJournalProj = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_journal_proj', null, null);
		var searchBudgetProj = nlapiSearchRecord('customrecord_clgx_budget', 'customsearch_clgx_rep_capex_budget_proj', null, null);
		var searchForecastProj = nlapiSearchRecord('customrecord_clgx_cap_bud_forecast', 'customsearch_clgx_rep_capex_fore_proj', null, null);
		
		arrProjStatus = new Array();
		arrProjSubsidiary = new Array();
		arrProjFacility = new Array();
		arrProjID = new Array();
		arrProjNbr = new Array();
		arrProjName = new Array();
		
		arrOpenPos = new Array();
		
		arrBillsTotal = new Array();
		arrBills2011 = new Array();
		arrBills2012 = new Array();
		arrBills0112 = new Array();
		arrBills0212 = new Array();
		arrBills0312 = new Array();
		arrBills0412 = new Array();
		arrBills0512 = new Array();
		arrBills0612 = new Array();
		arrBills0712 = new Array();
		arrBills0812 = new Array();
		arrBills0912 = new Array();
		arrBills1012 = new Array();
		arrBills1112 = new Array();
		arrBills1212 = new Array();
		
		arrComitCapital = new Array();
		
		arrBudget2011 = new Array();
		arrBudget2012 = new Array();
		arrBudgetTotal = new Array();
		
		arrComitCapitalVar = new Array();
		
		arrForecastTotal = new Array();
		arrForecast2011 = new Array();
		arrForecast2012 = new Array();
		arrForecast0112 = new Array();
		arrForecast0212 = new Array();
		arrForecast0312 = new Array();
		arrForecast0412 = new Array();
		arrForecast0512 = new Array();
		arrForecast0612 = new Array();
		arrForecast0712 = new Array();
		arrForecast0812 = new Array();
		arrForecast0912 = new Array();
		arrForecast1012 = new Array();
		arrForecast1112 = new Array();
		arrForecast1212 = new Array();
		
		arrTotComProj = new Array();
		arrTotProjVar = new Array();
		arr2012BudgetVar = new Array();

		for(var i=0; searchProj != null && i<searchProj.length; i++){
			
		// Projects columns --------------------------------------------------------------------
			arrProjStatus.push(searchProj[i].getValue('formulatext',null,null));
			arrProjSubsidiary.push(searchProj[i].getText('subsidiarynohierarchy',null,null));
			arrProjFacility.push(searchProj[i].getText('custentity_cologix_facility',null,null));
			arrProjID.push(searchProj[i].getValue('internalid',null,null));
			arrProjNbr.push(searchProj[i].getValue('entityid',null,null));
			arrProjName.push(searchProj[i].getValue('jobname',null,null));
		// Projects columns --------------------------------------------------------------------
			
		// Open Purchase Orders--------------------------------------------------------------------
		    var stLine = 0;
			for(var j=0; searchPOProj != null && j<searchPOProj.length; j++){
				var thisProjID = searchPOProj[j].getValue('custbody_cologix_project_name',null,'GROUP');
				if (thisProjID == arrProjID[i] ){
					var thisProjAmount = searchPOProj[j].getValue('amount',null,'SUM');
					arrOpenPos.push(parseFloat(thisProjAmount).toFixed(2));
					stLine = 1;
				}
			}
		    if (stLine == 0){ // no Pos for this project
		    	arrOpenPos.push('0.00');
		    }
		// Open Purchase Orders--------------------------------------------------------------------
			
		// Bills related to Project --------------------------------------------------------------------
		    var stLine = 0;
			for(var j=0; searchBillProj != null && j<searchBillProj.length; j++){
				var thisProjID = searchBillProj[j].getValue('custbody_cologix_project_name',null,'GROUP');
				if (thisProjID == arrProjID[i] ){
					
					var resultRow = searchBillProj[j];
					var columns = resultRow.getAllColumns();
					arrBillsTotal.push(parseFloat(resultRow.getValue(columns[1])).toFixed(2));
					arrBills2011.push(parseFloat(resultRow.getValue(columns[3])).toFixed(2));
					arrBills2012.push(parseFloat(resultRow.getValue(columns[4])).toFixed(2));
					arrBills0112.push(parseFloat(resultRow.getValue(columns[5])).toFixed(2));
					arrBills0212.push(parseFloat(resultRow.getValue(columns[6])).toFixed(2));
					arrBills0312.push(parseFloat(resultRow.getValue(columns[7])).toFixed(2));
					arrBills0412.push(parseFloat(resultRow.getValue(columns[8])).toFixed(2));
					arrBills0512.push(parseFloat(resultRow.getValue(columns[9])).toFixed(2));
					arrBills0612.push(parseFloat(resultRow.getValue(columns[10])).toFixed(2));
					arrBills0712.push(parseFloat(resultRow.getValue(columns[11])).toFixed(2));
					arrBills0812.push(parseFloat(resultRow.getValue(columns[12])).toFixed(2));
					arrBills0912.push(parseFloat(resultRow.getValue(columns[13])).toFixed(2));
					arrBills1012.push(parseFloat(resultRow.getValue(columns[14])).toFixed(2));
					arrBills1112.push(parseFloat(resultRow.getValue(columns[15])).toFixed(2));
					arrBills1212.push(parseFloat(resultRow.getValue(columns[16])).toFixed(2));
					stLine = 1;
				}
			}
		    if (stLine == 0){ // no Bills for this project
		    	arrBillsTotal.push('0.00');
				arrBills2011.push('0.00');
				arrBills2012.push('0.00');
				arrBills0112.push('0.00');
				arrBills0212.push('0.00');
				arrBills0312.push('0.00');
				arrBills0412.push('0.00');
				arrBills0512.push('0.00');
				arrBills0612.push('0.00');
				arrBills0712.push('0.00');
				arrBills0812.push('0.00');
				arrBills0912.push('0.00');
				arrBills1012.push('0.00');
				arrBills1112.push('0.00');
				arrBills1212.push('0.00');
		    }
		// Bills related to Project --------------------------------------------------------------------	
		
		    
		    
		// Add Journal amounts to the existing Bills arrays  --------------------------------------------------------------------	
		//for(var i=0; i<arrProjID.length; i++){

			for(var j=0; searchJournalProj != null && j<searchJournalProj.length; j++){
				var thisProjID = searchJournalProj[j].getValue('custcol_clgx_journal_project_id',null,'GROUP');
				if (thisProjID == arrProjID[i] ){
					
					var resultRow = searchJournalProj[j];
					var columns = resultRow.getAllColumns();
					
			    	arrBillsTotal[i] = ((parseFloat(arrBillsTotal[i]) + parseFloat(resultRow.getValue(columns[1])))).toFixed(2);
					arrBills2011[i] = ((parseFloat(arrBills2011[i]) + parseFloat(resultRow.getValue(columns[3])))).toFixed(2);
					arrBills2012[i] = ((parseFloat(arrBills2012[i]) + parseFloat(resultRow.getValue(columns[4])))).toFixed(2);
					arrBills0112[i] = ((parseFloat(arrBills0112[i]) + parseFloat(resultRow.getValue(columns[5])))).toFixed(2);
					arrBills0212[i] = ((parseFloat(arrBills0212[i]) + parseFloat(resultRow.getValue(columns[6])))).toFixed(2);
					arrBills0312[i] = ((parseFloat(arrBills0312[i]) + parseFloat(resultRow.getValue(columns[7])))).toFixed(2);
					arrBills0412[i] = ((parseFloat(arrBills0412[i]) + parseFloat(resultRow.getValue(columns[8])))).toFixed(2);
					arrBills0512[i] = ((parseFloat(arrBills0512[i]) + parseFloat(resultRow.getValue(columns[9])))).toFixed(2);
					arrBills0612[i] = ((parseFloat(arrBills0612[i]) + parseFloat(resultRow.getValue(columns[10])))).toFixed(2);
					arrBills0712[i] = ((parseFloat(arrBills0712[i]) + parseFloat(resultRow.getValue(columns[11])))).toFixed(2);
					arrBills0812[i] = ((parseFloat(arrBills0812[i]) + parseFloat(resultRow.getValue(columns[12])))).toFixed(2);
					arrBills0912[i] = ((parseFloat(arrBills0912[i]) + parseFloat(resultRow.getValue(columns[13])))).toFixed(2);
					arrBills1012[i] = ((parseFloat(arrBills1012[i]) + parseFloat(resultRow.getValue(columns[14])))).toFixed(2);
					arrBills1112[i] = ((parseFloat(arrBills1112[i]) + parseFloat(resultRow.getValue(columns[15])))).toFixed(2);
					arrBills1212[i] = ((parseFloat(arrBills1212[i]) + parseFloat(resultRow.getValue(columns[16])))).toFixed(2);
				}
			}
		//}
		// Add Journal amounts to the existing Bills arrays  --------------------------------------------------------------------

		    
		// Calculated Committed Capital  --------------------------------------------------------------------	
		    //var stComitCapital = (parseFloat(arrOpenPos[i]) + parseFloat(arrBillsTotal[i])).toFixed(2);
		    var stComitCapital = parseFloat(arrOpenPos[i]) + parseFloat(arrBillsTotal[i]);
		    
		    //if (stComitCapital != 'NaN'){
		    	arrComitCapital.push(stComitCapital.toFixed(2));
		    //}
		    //else{
		    	//arrComitCapital.push('0.00');
		    //}
		// Calculated Committed Capital  --------------------------------------------------------------------	
		    
		// Budget related to Project --------------------------------------------------------------------
		    var stLine = 0;
			for(var j=0; searchBudgetProj != null && j<searchBudgetProj.length; j++){
				var thisProjNbr = searchBudgetProj[j].getValue('custrecord_clgx_budget_project',null,'GROUP');
				if (thisProjNbr == arrProjNbr[i] ){
					
					var resultRow = searchBudgetProj[j];
					var columns = resultRow.getAllColumns();
					
					var thisProjAmount = searchBudgetProj[j].getValue('custrecord_clgx_budget_amount',null,'SUM');
					arrBudgetTotal.push(parseFloat(thisProjAmount).toFixed(2));
					arrBudget2011.push(parseFloat(resultRow.getValue(columns[2])).toFixed(2));
					arrBudget2012.push(parseFloat(resultRow.getValue(columns[3])).toFixed(2));
					stLine = 1;
				}
			}
		    if (stLine == 0){ // no Budget for this project
		    	arrBudget2011.push('0.00');
		    	arrBudget2012.push('0.00');
		    	arrBudgetTotal.push('0.00');
		    }
		// Budget related to Project --------------------------------------------------------------------	  
		 
		// Calculated Committed Capital Variance --------------------------------------------------------------------	
		    var stComitCapitalVar = (parseFloat(arrBudgetTotal[i]) - parseFloat(arrComitCapital[i])).toFixed(2);
		    if (stComitCapitalVar != 'NaN'){
		    	arrComitCapitalVar.push(stComitCapitalVar);
		    }
		    else{
		    	arrComitCapitalVar.push('0.00');
		    }
		// Calculated Committed Capital Variance --------------------------------------------------------------------	
		  
		// Forecasts related to Project --------------------------------------------------------------------
		    var stLine = 0;
			for(var j=0; searchForecastProj != null && j<searchForecastProj.length; j++){
				var thisProjID = searchForecastProj[j].getValue('custrecord_clgx_cap_bud_fore_project_id',null,'GROUP');
				if (thisProjID == arrProjID[i] ){
					
					var resultRow = searchForecastProj[j];
					var columns = resultRow.getAllColumns();
					
					arrForecastTotal.push(parseFloat(resultRow.getValue(columns[1])).toFixed(2));
					arrForecast2011.push(parseFloat(resultRow.getValue(columns[2])).toFixed(2));
					arrForecast2012.push(parseFloat(resultRow.getValue(columns[3])).toFixed(2));
					arrForecast0112.push(parseFloat(resultRow.getValue(columns[4])).toFixed(2));
					arrForecast0212.push(parseFloat(resultRow.getValue(columns[5])).toFixed(2));
					arrForecast0312.push(parseFloat(resultRow.getValue(columns[6])).toFixed(2));
					arrForecast0412.push(parseFloat(resultRow.getValue(columns[7])).toFixed(2));
					arrForecast0512.push(parseFloat(resultRow.getValue(columns[8])).toFixed(2));
					arrForecast0612.push(parseFloat(resultRow.getValue(columns[9])).toFixed(2));
					arrForecast0712.push(parseFloat(resultRow.getValue(columns[10])).toFixed(2));
					arrForecast0812.push(parseFloat(resultRow.getValue(columns[11])).toFixed(2));
					arrForecast0912.push(parseFloat(resultRow.getValue(columns[12])).toFixed(2));
					arrForecast1012.push(parseFloat(resultRow.getValue(columns[13])).toFixed(2));
					arrForecast1112.push(parseFloat(resultRow.getValue(columns[14])).toFixed(2));
					arrForecast1212.push(parseFloat(resultRow.getValue(columns[15])).toFixed(2));
					stLine = 1;
				}
			}
		    if (stLine == 0){ // no Bills for this project
		    	arrForecastTotal.push('0.00');
		    	arrForecast2011.push('0.00');
		    	arrForecast2012.push('0.00');
		    	arrForecast0112.push('0.00');
		    	arrForecast0212.push('0.00');
		    	arrForecast0312.push('0.00');
		    	arrForecast0412.push('0.00');
		    	arrForecast0512.push('0.00');
		    	arrForecast0612.push('0.00');
		    	arrForecast0712.push('0.00');
		    	arrForecast0812.push('0.00');
		    	arrForecast0912.push('0.00');
		    	arrForecast1012.push('0.00');
		    	arrForecast1112.push('0.00');
		    	arrForecast1212.push('0.00');
		    }
		// Forecasts related to Project --------------------------------------------------------------------
		    
		// Calculated Total Committed and Projected --------------------------------------------------------------------	
		    var stTotComProj = (parseFloat(arrForecastTotal[i]) + parseFloat(arrComitCapital[i])).toFixed(2);
		    if (stTotComProj != 'NaN'){
		    	arrTotComProj.push(stTotComProj);
		    }
		    else{
		    	arrTotComProj.push('0.00');
		    }
		// Calculated Total Committed and Projected --------------------------------------------------------------------	
		    
		// Calculated Total Project Variance  --------------------------------------------------------------------	
		    var stTotProjVar = (parseFloat(arrBudgetTotal[i]) - parseFloat(arrTotComProj[i])).toFixed(2);
		    if (stTotProjVar != 'NaN'){
		    	arrTotProjVar.push( stTotProjVar);
		    }
		    else{
		    	arrTotProjVar.push('0.00');
		    }
		// Calculated Total Project Variance  --------------------------------------------------------------------	 
		    
		// Calculated 2012 Budget Variance  --------------------------------------------------------------------	
		    var st2012BudgetVar = (parseFloat(arrBudget2012[i]) - parseFloat(arrBills2012[i])).toFixed(2);
		    if (st2012BudgetVar != 'NaN'){
		    	arr2012BudgetVar.push(st2012BudgetVar);
		    }
		    else{
		    	arr2012BudgetVar.push('0.00');
		    }
		// Calculated 2012 Budget Variance  --------------------------------------------------------------------	   

		}

		
		
		

		
		
		
		
	var html = '';
    html = '<link rel="stylesheet" href="/core/media/media.nl?id=41788&c=1337135&h=b09682241b6cd2f96ce1&_xt=.css" />';
    html += '<script src="/core/media/media.nl?id=41787&c=1337135&h=2b829df96c50f1fed6e5&_xt=.js" type="text/javascript"></script>';
    
    html += '<div style="width:1320px;height:440px;overflow:auto;">';
    
    html += '<table id="myTable" bgcolor="#eeeeee" border="0">';
		html += '<tr>';
		    html += '<td class="col5" align="center" colspan="5">PROJECTS</td>';
		    html += '<td class="col5" align="center">POs</td>';
		    html += '<td class="col5" align="center" colspan="15">BILLS AND JOURNAL ENTRIES</td>';
		    html += '<td class="empty" align="center"></td>';
		    html += '<td class="col5" align="center" colspan="3">BUDGET</td>';
		    html += '<td class="empty" align="center"></td>';
		    html += '<td class="col5" align="center" colspan="15">CAPITAL BUDGET FORECAST</td>';
		    html += '<td class="empty" align="center" colspan="4"></td>';
		html += '</tr>';
	    html += '<tr>';
		    html += '<td class="header">Status</td>';
		    html += '<td class="header">Subsidiary</td>';
		    html += '<td class="header">Facility</td>';
		    html += '<td class="header">Project ID</td>';
		    html += '<td class="header">Description</td>';
		    html += '<td class="col5">Open POs</td>';
		    html += '<td class="header">2011 Actual</td>';
		    html += '<td class="header">Jan-2012 Actual</td>';
		    html += '<td class="header">Feb-2012 Actual</td>';
		    html += '<td class="header">Mar-2012 Actual</td>';
		    html += '<td class="header">Apr-2012 Actual</td>';
		    html += '<td class="header">May-2012 Actual</td>';
		    html += '<td class="header">Jun-2012 Actual</td>';
		    html += '<td class="header">Jul-2012 Actual</td>';
		    html += '<td class="header">Aug-2012 Actual</td>';
		    html += '<td class="header">Sep-2012 Actual</td>';
		    html += '<td class="header">Oct-2012 Actual</td>';
		    html += '<td class="header">Nov-2012 Actual</td>';
		    html += '<td class="header">Dec-2012 Actual</td>';
		    html += '<td class="header">2012 Actual</td>';
		    html += '<td class="header">Total Actual</td>';
		    html += '<td class="col5">Committed Capital</td>';
		    html += '<td class="header">2011 Budget</td>';
		    html += '<td class="header">2012 Budget</td>';
		    html += '<td class="header">Total Budget</td>';
		    html += '<td class="col5">Committed Capital Variance</td>';
		    html += '<td class="header">2011 Forecast</td>';
		    html += '<td class="header">Jan-2012 Forecast</td>';
		    html += '<td class="header">Feb-2012 Forecast</td>';
		    html += '<td class="header">Mar-2012 Forecast</td>';
		    html += '<td class="header">Apr-2012 Forecast</td>';
		    html += '<td class="header">May-2012 Forecast</td>';
		    html += '<td class="header">Jun-2012 Forecast</td>';
		    html += '<td class="header">Jul-2012 Forecast</td>';
		    html += '<td class="header">Aug-2012 Forecast</td>';
		    html += '<td class="header">Sep-2012 Forecast</td>';
		    html += '<td class="header">Oct-2012 Forecast</td>';
		    html += '<td class="header">Nov-2012 Forecast</td>';
		    html += '<td class="header">Dec-2012 Forecast</td>';
		    html += '<td class="header">2012 Forecast</td>';
		    html += '<td class="header">Total Forecast</td>';
		    html += '<td class="col5">Total Committed and Projected</td>';
		    html += '<td class="col5">Total Project Variance</td>';
		    html += '<td class="col5">2012 Budget Variance</td>';
		    html += '<td class="header">Project ID</td>';
	    html += '</tr>';
	    
    for (var i=0; searchProj != null &&  i<arrProjID.length; ++i) {
	    html += '<tr>';
		    html += '<td class="col1">' + arrProjStatus[i] + '</td>';
		    html += '<td class="col2">' + arrProjSubsidiary[i] + '</td>';
		    html += '<td class="col3">' + arrProjFacility[i] + '</td>';
		    html += '<td class="col0"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + arrProjNbr[i] + '</a></td>';
		    html += '<td class="col0">' + arrProjName[i] + '</td>';
		    
		    if(arrOpenPos[i] == '0.00'){html += '<td class="col5" align="right"></td>';}
		    else{html += '<td class="col5" align="right"><a class="report" href="/app/site/hosting/scriptlet.nl?script=100&deploy=1&compid=1337135&whence=&projid=' + arrProjID[i] + '" target="_blank">' + addCommas(arrOpenPos[i]) + '</a></td>';}
		    
		    if(arrBills2011[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/site/hosting/scriptlet.nl?script=101&deploy=1&compid=1337135&whence=&projid=' + arrProjID[i] + '&period=19" target="_blank">' + addCommas(arrBills2011[i]) + '</a></td>';}

		    if(arrBills0112[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/site/hosting/scriptlet.nl?script=101&deploy=1&compid=1337135&whence=&projid=' + arrProjID[i] + '&period=39" target="_blank">' + addCommas(arrBills0112[i]) + '</a></td>';}
		    
		    if(arrBills0212[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/site/hosting/scriptlet.nl?script=101&deploy=1&compid=1337135&whence=&projid=' + arrProjID[i] + '&period=40" target="_blank">' + addCommas(arrBills0212[i]) + '</a></td>';}
		    
		    if(arrBills0312[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/site/hosting/scriptlet.nl?script=101&deploy=1&compid=1337135&whence=&projid=' + arrProjID[i] + '&period=41" target="_blank">' + addCommas(arrBills0312[i]) + '</a></td>';}
		    
		    if(arrBills0412[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/site/hosting/scriptlet.nl?script=101&deploy=1&compid=1337135&whence=&projid=' + arrProjID[i] + '&period=43" target="_blank">' + addCommas(arrBills0412[i]) + '</a></td>';}
		    
		    if(arrBills0512[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/site/hosting/scriptlet.nl?script=101&deploy=1&compid=1337135&whence=&projid=' + arrProjID[i] + '&period=44" target="_blank">' + addCommas(arrBills0512[i]) + '</a></td>';}
		    
		    if(arrBills0612[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/site/hosting/scriptlet.nl?script=101&deploy=1&compid=1337135&whence=&projid=' + arrProjID[i] + '&period=45" target="_blank">' + addCommas(arrBills0612[i]) + '</a></td>';}
		    
		    if(arrBills0712[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/site/hosting/scriptlet.nl?script=101&deploy=1&compid=1337135&whence=&projid=' + arrProjID[i] + '&period=47" target="_blank">' + addCommas(arrBills0712[i]) + '</a></td>';}
		    
		    if(arrBills0812[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/site/hosting/scriptlet.nl?script=101&deploy=1&compid=1337135&whence=&projid=' + arrProjID[i] + '&period=48" target="_blank">' + addCommas(arrBills0812[i]) + '</a></td>';}
		    
		    if(arrBills0912[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/site/hosting/scriptlet.nl?script=101&deploy=1&compid=1337135&whence=&projid=' + arrProjID[i] + '&period=49" target="_blank">' + addCommas(arrBills0912[i]) + '</a></td>';}
		    
		    if(arrBills1012[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/site/hosting/scriptlet.nl?script=101&deploy=1&compid=1337135&whence=&projid=' + arrProjID[i] + '&period=51" target="_blank">' + addCommas(arrBills1012[i]) + '</a></td>';}
		    
		    if(arrBills1112[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/site/hosting/scriptlet.nl?script=101&deploy=1&compid=1337135&whence=&projid=' + arrProjID[i] + '&period=52" target="_blank">' + addCommas(arrBills1112[i]) + '</a></td>';}
		    
		    if(arrBills1212[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/site/hosting/scriptlet.nl?script=101&deploy=1&compid=1337135&whence=&projid=' + arrProjID[i] + '&period=53" target="_blank">' + addCommas(arrBills1212[i]) + '</a></td>';}
		    
		    if(arrBills2012[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/site/hosting/scriptlet.nl?script=101&deploy=1&compid=1337135&whence=&projid=' + arrProjID[i] + '&period=37" target="_blank">' + addCommas(arrBills2012[i]) + '</a></td>';}
		    
		    if(arrBillsTotal[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/site/hosting/scriptlet.nl?script=101&deploy=1&compid=1337135&whence=&projid=' + arrProjID[i] + '&period=" target="_blank">' + addCommas(arrBillsTotal[i]) + '</a></td>';}
		    
		    if(arrComitCapital[i] == '0.00'){html += '<td class="col5" align="right"></td>';}
		    else{html += '<td class="col5" align="right">' + addCommas(arrComitCapital[i]) + '</td>';}
		    
		    if(arrBudget2011[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right">' + addCommas(arrBudget2011[i]) + '</td>';}
		    
		    if(arrBudget2012[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right">' + addCommas(arrBudget2012[i]) + '</td>';}
		    
		    if(arrBudgetTotal[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right">' + addCommas(arrBudgetTotal[i]) + '</td>';}
		    
		    if(arrComitCapitalVar[i] == '0.00'){html += '<td class="col5" align="right"></td>';}
		    else{html += '<td class="col5" align="right">' + addCommas(arrComitCapitalVar[i]) + '</td>';}
		    
		    if(arrForecast2011[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + addCommas(arrForecast2011[i]) + '</a></td>';}
		    
		    if(arrForecast0112[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + addCommas(arrForecast0112[i]) + '</a></td>';}

		    if(arrForecast0212[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + addCommas(arrForecast0212[i]) + '</a></td>';}
		    
		    if(arrForecast0312[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + addCommas(arrForecast0312[i]) + '</a></td>';}
		    
		    if(arrForecast0412[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + addCommas(arrForecast0412[i]) + '</a></td>';}
		    
		    if(arrForecast0512[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + addCommas(arrForecast0512[i]) + '</a></td>';}
		    
		    if(arrForecast0612[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + addCommas(arrForecast0612[i]) + '</a></td>';}
		    
		    if(arrForecast0712[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + addCommas(arrForecast0712[i]) + '</a></td>';}
		    
		    if(arrForecast0812[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + addCommas(arrForecast0812[i]) + '</a></td>';}
		    
		    if(arrForecast0912[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + addCommas(arrForecast0912[i]) + '</a></td>';}
		    
		    if(arrForecast1012[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + addCommas(arrForecast1012[i]) + '</a></td>';}
		    
		    if(arrForecast1112[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + addCommas(arrForecast1112[i]) + '</a></td>';}
		    
		    if(arrForecast1212[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + addCommas(arrForecast1212[i]) + '</a></td>';}
		    
		    if(arrForecast2012[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + addCommas(arrForecast2012[i]) + '</a></td>';}
		    
		    if(arrForecastTotal[i] == '0.00'){html += '<td class="col0" align="right"></td>';}
		    else{html += '<td class="col0" align="right"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + addCommas(arrForecastTotal[i]) + '</a></td>';}
		    
		    if(arrTotComProj[i] == '0.00'){html += '<td class="col5" align="right"></td>';}
		    else{html += '<td class="col5" align="right">' + addCommas(arrTotComProj[i]) + '</td>';}
		    
		    if(arrTotProjVar[i] == '0.00'){html += '<td class="col5" align="right"></td>';}
		    else{html += '<td class="col5" align="right">' + addCommas(arrTotProjVar[i]) + '</td>';}
		    
		    if(arr2012BudgetVar[i] == '0.00'){html += '<td class="col5" align="right"></td>';}
		    else{html += '<td class="col5" align="right">' + addCommas(arr2012BudgetVar[i]) + '</td>';}
		    
		    
		    html += '<td class="col0" align="right"><a class="report" href="/app/common/entity/custjob.nl?id=' + arrProjID[i] + '" target="_blank">' + arrProjNbr[i] + '</a></td>';
	    html += '</tr>';
    }

    html += '<tr>';
	    html += '<td class="totals" align="right" colspan="2">TOTALS</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrOpenPos) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBills2011) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBills0112) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBills0212) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBills0312) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBills0412) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBills0512) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBills0612) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBills0712) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBills0812) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBills0912) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBills1012) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBills1112) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBills1212) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBills2012) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBillsTotal) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrComitCapital) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBudget2011) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBudget2012) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrBudgetTotal) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrComitCapitalVar) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrForecast2011) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrForecast0112) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrForecast0212) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrForecast0312) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrForecast0412) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrForecast0512) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrForecast0612) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrForecast0712) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrForecast0812) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrForecast0912) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrForecast1012) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrForecast1112) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrForecast1212) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrForecast2012) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrForecastTotal) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrTotComProj) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arrTotProjVar) + '</td>';
	    html += '<td class="totals" align="right">' + sumArray(arr2012BudgetVar) + '</td>';
	    html += '<td class="totals" align="right">TOTALS</td>';
    html += '</tr>';

    var imgSRC = '/core/media/media.nl?id=38239&c=1337135&h=4bc57aaf20cc0189b3f8';
    
	    html += '<tr>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="60" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="110" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="80" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="510" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="80" /></td>';
	    html += '</tr>';
    
    html += '</table>';
    
    html += '</div>';
    
    html += '<script type="text/javascript">';
        html += 'addTableRolloverEffect("myTable","tableRollOverEffect1","tableRowClickEffect1");';
    html += '</script>';
    
    
    htmlReport.setDefaultValue(html);
	
	if (reqCSV == 'T' && searchProj != null){
		
		var stCSV = 'Status,Subsidiary,Facility,Project ID,Description,Open POs,2011 Actual,Jan-2012 Actual,Feb-2012 Actual,Mar-2012 Actual,Apr-2012 Actual,May-2012 Actual,Jun-2012 Actual,Jul-2012 Actual,Aug-2012 Actual,Sep-2012 Actual,Oct-2012 Actual,Nov-2012 Actual,Dec-2012 Actual,2012 Actual,Total Actual,Committed Capital,2011 Budget,2012 Budget,Total Budget,Committed Capital Variance,2011 Forecast,Jan-2012 Forecast,Feb-2012 Forecast,Mar-2012 Forecast,Apr-2012 Forecast,May-2012 Forecast,Jun-2012 Forecast,Jul-2012 Forecast,Aug-2012 Forecast,Sep-2012 Forecast,Oct-2012 Forecast,Nov-2012 Forecast,Dec-2012 Forecast,2012 Forecast,Total Forecast,Total Committed and Projected,Total Project Variance,2012 Budget Variance,Project ID\n';
		
		for(var i=0; searchProj != null && i<searchProj.length; i++){
				stCSV += arrProjStatus[i].replace(/\,/g," ") + ',';
				stCSV += arrProjSubsidiary[i].replace(/\,/g," ") + ',';
				stCSV += arrProjFacility[i].replace(/\,/g," ") + ',';
				stCSV += arrProjNbr[i].replace(/\,/g," ") + ',';
				stCSV += arrProjName[i].replace(/\,/g," ") + ',';
				stCSV += arrOpenPos[i].replace(/\,/g," ") + ',';
				stCSV += arrBills2011[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0112[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0212[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0312[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0412[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0512[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0612[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0712[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0812[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0912[i].replace(/\,/g," ") + ',';
				stCSV += arrBills1012[i].replace(/\,/g," ") + ',';
				stCSV += arrBills1112[i].replace(/\,/g," ") + ',';
				stCSV += arrBills1212[i].replace(/\,/g," ") + ',';
				stCSV += arrBills2012[i].replace(/\,/g," ") + ',';
				stCSV += arrBillsTotal[i].replace(/\,/g," ") + ',';
				stCSV += arrComitCapital[i].replace(/\,/g," ") + ',';
				stCSV += arrBudget2011[i].replace(/\,/g," ") + ',';
				stCSV += arrBudget2012[i].replace(/\,/g," ") + ',';
				stCSV += arrBudgetTotal[i].replace(/\,/g," ") + ',';
				stCSV += arrComitCapitalVar[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast2011[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0112[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0212[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0312[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0412[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0512[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0612[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0712[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0812[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0912[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast1012[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast1112[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast1212[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast2012[i].replace(/\,/g," ") + ',';
				stCSV += arrForecastTotal[i].replace(/\,/g," ") + ',';
				stCSV += arrTotComProj[i].replace(/\,/g," ") + ',';
				stCSV += arrTotProjVar[i].replace(/\,/g," ") + ',';
				stCSV += arr2012BudgetVar[i].replace(/\,/g," ") + ',';
				stCSV += arrProjID[i].replace(/\,/g," ") + '\n';
			}
			
		var fileCSV = nlapiCreateFile('capex_report.csv', 'CSV', stCSV);
		fileCSV.setFolder(66212);
		var fileId = nlapiSubmitFile(fileCSV);
		var savedCSV = nlapiLoadFile(fileId);
		var csvURL = savedCSV.getURL();
		var htmlExport = '<script type="text/javascript">window.location = "' + csvURL + '"</script>';
		
		var htmlExportCSV = form.addField('custpage_clgx_export','inlinehtml', null, null, 'groupResults');
		htmlExportCSV.setDefaultValue(htmlExport);
		//var deletedCSV = nlapiDeleteFile(fileId);
	}

    
    

	
	form.addSubmitButton('Submit');
	response.writePage( form );
	
//---------- End Section 1 ------------------------------------------------------------------------------------------------


nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------FINISHED---------------------|');  
} 
catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
	if (error.getDetails != undefined){
	    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	    throw error;
	}
	else{
	    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	    throw nlapiCreateError('99999', error.toString());
	}
} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function addCommas(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function sumArray (arr){
	var sum = 0;
	for (var i=0; i<arr.length; ++i) {
		sum += parseFloat(arr[i]);
	}
	return addCommas(parseFloat(sum).toFixed(2));
}
