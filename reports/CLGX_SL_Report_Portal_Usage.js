nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Report_Portal_Usage.js
//	Script Name:	CLGX_SL_Report_Portal_Usage
//	Script Id:		customscript_clgx_sl_rep_portal_usage
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		09/14/2012
//-------------------------------------------------------------------------------------------------

function suitelet_rep_portal_usage(request, response){
try {

	var currentTab = '';

	var form = nlapiCreateForm('Portal Usage Report');
	var htmlReport = form.addField('custpage_clgx_html','inlinehtml', null, null, null);
	
	var objFile = nlapiLoadFile(190672);
	var dataHTML = objFile.getValue();
	
	dataHTML = dataHTML.replace(new RegExp('{stDate}','g'),dateData());
	dataHTML = dataHTML.replace(new RegExp('{stContacts}','g'),contactsData());
	dataHTML = dataHTML.replace(new RegExp('{stDay}','g'),dayData());
	dataHTML = dataHTML.replace(new RegExp('{currentDay}','g'),returnCurrentDay());
	dataHTML = dataHTML.replace(new RegExp('{curentMonthLine}','g'),returnCurrentMonthLine());
	dataHTML = dataHTML.replace(new RegExp('{stHits}','g'),hitsData());
	
	var usageConsumtion = 1000 - parseInt(nlapiGetContext().getRemainingUsage());
	dataHTML = dataHTML.replace(new RegExp('{usageConsumtion}','g'),usageConsumtion);
	
	htmlReport.setDefaultValue(dataHTML);
	//form.addSubmitButton('Submit');
	response.writePage( form );
	
//---------- End Section 1 ------------------------------------------------------------------------------------------------

} 
catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
	if (error.getDetails != undefined){
	    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	    throw error;
	}
	else{
	    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	    throw nlapiCreateError('99999', error.toString());
	}
} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function dateData(){	
	var searchYears = nlapiSearchRecord('customrecord_clgx_portal_sessions_archiv', 'customsearch_clgx_rep_portal_usage_years', null, null);
	var searchMonths = nlapiSearchRecord('customrecord_clgx_portal_sessions_archiv', 'customsearch_clgx_rep_portal_usage_month', null, null);

	if(searchYears != null && searchMonths != null){
		var stDate = 'var dateData = {"total":' + searchMonths.length +  ',"rows":[';
	}
	else{
		var stDate = 'var dateData = {"total":0,"rows":[';
	}
	
	var sumHits = 0;
	for ( var i = 0; searchYears != null && i < searchYears.length; i++ ) {
		
		var resultRowYear = searchYears[i];
		var columnsYear = resultRowYear.getAllColumns();
		
		stDate += '{"id":' + resultRowYear.getValue(columnsYear[0]) + 
					',"period":"' + resultRowYear.getValue(columnsYear[0]) + 
					'","year":"' + resultRowYear.getValue(columnsYear[0]) + 
					'","month":"' + 
					'","sum":' + resultRowYear.getValue(columnsYear[1]) +  '},';
		
		sumHits += parseInt(resultRowYear.getValue(columnsYear[1]));
		
		for ( var j = 0; searchMonths != null && j < searchMonths.length; j++ ) {
			
			var resultRowMonth = searchMonths[j];
			var columnsMonth = resultRowMonth.getAllColumns();
			
			if (resultRowYear.getValue(columnsYear[0]) == resultRowMonth.getValue(columnsMonth[0])){
				var index = 10 + j;
				
				var lineURL = '/app/site/hosting/scriptlet.nl?script=132&deploy=1&tab=date&year='+ resultRowYear.getValue(columnsYear[0]) +'&month=' + resultRowMonth.getValue(columnsMonth[1]);
				
				stDate += '{"id":' + resultRowYear.getValue(columnsYear[0]) + resultRowMonth.getValue(columnsMonth[1]) + 
						',"period":"' + resultRowMonth.getValue(columnsMonth[1]) + 
						'","year":"' + resultRowYear.getValue(columnsYear[0]) + 
						'","month":"' + resultRowMonth.getValue(columnsMonth[1]) + 
						'","sum":' + resultRowMonth.getValue(columnsMonth[2]) + 
						',"d01":"<a class=report href='  + lineURL + '&day=01>' + resultRowMonth.getValue(columnsMonth[3])  +  '</a>"' + 
						',"d02":"<a class=report href='  + lineURL + '&day=02>' + resultRowMonth.getValue(columnsMonth[4])  +  '</a>"' + 
						',"d03":"<a class=report href='  + lineURL + '&day=03>' + resultRowMonth.getValue(columnsMonth[5])  +  '</a>"' + 
						',"d04":"<a class=report href='  + lineURL + '&day=04>' + resultRowMonth.getValue(columnsMonth[6])  +  '</a>"' + 
						',"d05":"<a class=report href='  + lineURL + '&day=05>' + resultRowMonth.getValue(columnsMonth[7])  +  '</a>"' + 
						',"d06":"<a class=report href='  + lineURL + '&day=06>' + resultRowMonth.getValue(columnsMonth[8])  +  '</a>"' + 
						',"d07":"<a class=report href='  + lineURL + '&day=07>' + resultRowMonth.getValue(columnsMonth[9])  +  '</a>"' + 
						',"d08":"<a class=report href='  + lineURL + '&day=08>' + resultRowMonth.getValue(columnsMonth[10])  +  '</a>"' + 
						',"d09":"<a class=report href='  + lineURL + '&day=09>' + resultRowMonth.getValue(columnsMonth[11])  +  '</a>"' + 
						',"d10":"<a class=report href='  + lineURL + '&day=10>' + resultRowMonth.getValue(columnsMonth[12])  +  '</a>"' + 
						',"d11":"<a class=report href='  + lineURL + '&day=11>' + resultRowMonth.getValue(columnsMonth[13])  +  '</a>"' + 
						',"d12":"<a class=report href='  + lineURL + '&day=12>' + resultRowMonth.getValue(columnsMonth[14])  +  '</a>"' + 
						',"d13":"<a class=report href='  + lineURL + '&day=13>' + resultRowMonth.getValue(columnsMonth[15])  +  '</a>"' + 
						',"d14":"<a class=report href='  + lineURL + '&day=14>' + resultRowMonth.getValue(columnsMonth[16])  +  '</a>"' + 
						',"d15":"<a class=report href='  + lineURL + '&day=15>' + resultRowMonth.getValue(columnsMonth[17])  +  '</a>"' + 
						',"d16":"<a class=report href='  + lineURL + '&day=16>' + resultRowMonth.getValue(columnsMonth[18])  +  '</a>"' + 
						',"d17":"<a class=report href='  + lineURL + '&day=17>' + resultRowMonth.getValue(columnsMonth[19])  +  '</a>"' + 
						',"d18":"<a class=report href='  + lineURL + '&day=16>' + resultRowMonth.getValue(columnsMonth[20])  +  '</a>"' + 
						',"d19":"<a class=report href='  + lineURL + '&day=19>' + resultRowMonth.getValue(columnsMonth[21])  +  '</a>"' + 
						',"d20":"<a class=report href='  + lineURL + '&day=20>' + resultRowMonth.getValue(columnsMonth[22])  +  '</a>"' + 
						',"d21":"<a class=report href='  + lineURL + '&day=21>' + resultRowMonth.getValue(columnsMonth[23])  +  '</a>"' + 
						',"d22":"<a class=report href='  + lineURL + '&day=22>' + resultRowMonth.getValue(columnsMonth[24])  +  '</a>"' + 
						',"d23":"<a class=report href='  + lineURL + '&day=23>' + resultRowMonth.getValue(columnsMonth[25])  +  '</a>"' + 
						',"d24":"<a class=report href='  + lineURL + '&day=24>' + resultRowMonth.getValue(columnsMonth[26])  +  '</a>"' + 
						',"d25":"<a class=report href='  + lineURL + '&day=25>' + resultRowMonth.getValue(columnsMonth[27])  +  '</a>"' + 
						',"d26":"<a class=report href='  + lineURL + '&day=26>' + resultRowMonth.getValue(columnsMonth[28])  +  '</a>"' + 
						',"d27":"<a class=report href='  + lineURL + '&day=27>' + resultRowMonth.getValue(columnsMonth[29])  +  '</a>"' + 
						',"d28":"<a class=report href='  + lineURL + '&day=28>' + resultRowMonth.getValue(columnsMonth[30])  +  '</a>"' + 
						',"d29":"<a class=report href='  + lineURL + '&day=29>' + resultRowMonth.getValue(columnsMonth[31])  +  '</a>"' + 
						',"d30":"<a class=report href='  + lineURL + '&day=30>' + resultRowMonth.getValue(columnsMonth[32])  +  '</a>"' + 
						',"d31":"<a class=report href='  + lineURL + '&day=31>' + resultRowMonth.getValue(columnsMonth[32])  +  '</a>"' + 
						',"_parentId":' + resultRowYear.getValue(columnsYear[0]) +  '},';
			}
		}
	}
	var strLen = stDate.length;
	if (searchYears != null && searchMonths != null){
		stDate = stDate.slice(0,strLen-1);
	}
	stDate += '],"footer":[';
	stDate += '{"sum":' + sumHits +  ',"period":"Totals:"}';
	stDate += ']};';
	
    return stDate;
}

function contactsData(){	
	var searchCustomers = nlapiSearchRecord('customrecord_clgx_portal_sessions_archiv', 'customsearch_clgx_rep_portal_usage_cust', null, null);
	var searchContacts = nlapiSearchRecord('customrecord_clgx_portal_sessions_archiv', 'customsearch_clgx_rep_portal_usage_cont', null, null);

	if(searchCustomers != null && searchContacts != null){
		var stContacts = 'var contactsData = {"total":' + searchContacts.length +  ',"rows":[';
	}
	else{
		var stContacts = 'var contactsData = {"total":0,"rows":[';
	}
	
	var sumM001 = 0;
	var sumM002 = 0;
	var sumM003 = 0;
	var sumM004 = 0;
	var sumA1 = 0;
	var sumM101 = 0;
	var sumM102 = 0;
	var sumM103 = 0;
	var sumM104 = 0;
	var sumM105 = 0;
	var sumM106 = 0;
	var sumM107 = 0;
	var sumM108 = 0;
	var sumM109 = 0;
	var sumM110 = 0;
	var sumM111 = 0;
	var sumM112 = 0;
	var sumA2 = 0;
	
	for ( var i = 0; searchCustomers != null && i < searchCustomers.length; i++ ) {
		
		var resultRowCustomer = searchCustomers[i];
		var columnsCustomer = resultRowCustomer.getAllColumns();
		
		stContacts += '{"id":' + resultRowCustomer.getValue(columnsCustomer[0]) + 
					',"entity":"' + resultRowCustomer.getValue(columnsCustomer[1]) + 
					'","m001":' + resultRowCustomer.getValue(columnsCustomer[2]) + 
					',"m002":' + resultRowCustomer.getValue(columnsCustomer[3])  + 
					',"m003":' + resultRowCustomer.getValue(columnsCustomer[4])  + 
					',"m004":' + resultRowCustomer.getValue(columnsCustomer[5])  + 
					',"a01":' + resultRowCustomer.getValue(columnsCustomer[6])  + 
					',"m101":' + resultRowCustomer.getValue(columnsCustomer[7])  + 
					',"m102":' + resultRowCustomer.getValue(columnsCustomer[8])  + 
					',"m103":' + resultRowCustomer.getValue(columnsCustomer[9])  + 
					',"m104":' + resultRowCustomer.getValue(columnsCustomer[10])  + 
					',"m105":' + resultRowCustomer.getValue(columnsCustomer[11])  + 
					',"m106":' + resultRowCustomer.getValue(columnsCustomer[12])  + 
					',"m107":' + resultRowCustomer.getValue(columnsCustomer[13])  + 
					',"m108":' + resultRowCustomer.getValue(columnsCustomer[14])  + 
					',"m109":' + resultRowCustomer.getValue(columnsCustomer[15])  + 
					',"m110":' + resultRowCustomer.getValue(columnsCustomer[16])  + 
					',"m111":' + resultRowCustomer.getValue(columnsCustomer[17])  + 
					',"m112":' + resultRowCustomer.getValue(columnsCustomer[18])  + 
					',"a02":' + resultRowCustomer.getValue(columnsCustomer[19]) +  '},';
		
		sumM001 += parseInt(resultRowCustomer.getValue(columnsCustomer[2]));
		sumM002 += parseInt(resultRowCustomer.getValue(columnsCustomer[3]));
		sumM003 += parseInt(resultRowCustomer.getValue(columnsCustomer[4]));
		sumM004 += parseInt(resultRowCustomer.getValue(columnsCustomer[5]));
		sumA1 += parseInt(resultRowCustomer.getValue(columnsCustomer[6]));
		sumM101 += parseInt(resultRowCustomer.getValue(columnsCustomer[7]));
		sumM102 += parseInt(resultRowCustomer.getValue(columnsCustomer[8]));
		sumM103 += parseInt(resultRowCustomer.getValue(columnsCustomer[9]));
		sumM104 += parseInt(resultRowCustomer.getValue(columnsCustomer[10]));
		sumM105 += parseInt(resultRowCustomer.getValue(columnsCustomer[11]));
		sumM106 += parseInt(resultRowCustomer.getValue(columnsCustomer[12]));
		sumM107 += parseInt(resultRowCustomer.getValue(columnsCustomer[13]));
		sumM108 += parseInt(resultRowCustomer.getValue(columnsCustomer[14]));
		sumM109 += parseInt(resultRowCustomer.getValue(columnsCustomer[15]));
		sumM110 += parseInt(resultRowCustomer.getValue(columnsCustomer[16]));
		sumM111 += parseInt(resultRowCustomer.getValue(columnsCustomer[17]));
		sumM112 += parseInt(resultRowCustomer.getValue(columnsCustomer[18]));
		sumA2 += parseInt(resultRowCustomer.getValue(columnsCustomer[19]));
		
		for ( var j = 0; searchContacts != null && j < searchContacts.length; j++ ) {
			
			var resultRowContact = searchContacts[j];
			var columnsContact = resultRowContact.getAllColumns();
			
			if (resultRowCustomer.getValue(columnsCustomer[0]) == resultRowContact.getValue(columnsContact[0])){
				
				var lineURL = '/app/site/hosting/scriptlet.nl?script=132&deploy=1&tab=contacts&customerID='+ resultRowContact.getValue(columnsContact[0]) +'&contactID=' + resultRowContact.getValue(columnsContact[2]);
				
				stContacts += '{"id":' + resultRowContact.getValue(columnsContact[2]) + 
						',"entity":"<a class=report href='  + lineURL + '>' + resultRowContact.getValue(columnsContact[3])  +  '</a>"' + 
						',"m001":' + resultRowContact.getValue(columnsContact[4]) + 
						',"m002":' + resultRowContact.getValue(columnsContact[5])  + 
						',"m003":' + resultRowContact.getValue(columnsContact[6])  + 
						',"m004":' + resultRowContact.getValue(columnsContact[7])  + 
						',"a01":' + resultRowContact.getValue(columnsContact[8])  + 
						',"m101":' + resultRowContact.getValue(columnsContact[9])  + 
						',"m102":' + resultRowContact.getValue(columnsContact[10])  + 
						',"m103":' + resultRowContact.getValue(columnsContact[11])  + 
						',"m104":' + resultRowContact.getValue(columnsContact[12])  + 
						',"m105":' + resultRowContact.getValue(columnsContact[13])  + 
						',"m106":' + resultRowContact.getValue(columnsContact[14])  + 
						',"m107":' + resultRowContact.getValue(columnsContact[15])  + 
						',"m108":' + resultRowContact.getValue(columnsContact[16])  + 
						',"m109":' + resultRowContact.getValue(columnsContact[17])  + 
						',"m110":' + resultRowContact.getValue(columnsContact[18])  + 
						',"m111":' + resultRowContact.getValue(columnsContact[19])  + 
						',"m112":' + resultRowContact.getValue(columnsContact[20])  + 
						',"a02":' + resultRowContact.getValue(columnsContact[21])  + 
						',"_parentId":' + resultRowCustomer.getValue(columnsCustomer[0]) +  '},';
			}
		}
	}
	
	var strLen = stContacts.length;
	if (searchCustomers != null && searchContacts != null){
		stContacts = stContacts.slice(0,strLen-1);
	}
	stContacts += '],"footer":[';
	stContacts += '{"entity":"Totals:","m001":' + sumM001 + ',"m002":' + sumM002 + ',"m003":' + sumM003 + ',"m004":' + sumM004 + ',"a01":' + sumA1 + ',"m101":' + sumM101 + ',"m102":' + sumM102 + ',"m103":' + sumM103 + ',"m1401":' + sumM104 + ',"m105":' + sumM105 + ',"m106":' + sumM106 + ',"m107":' + sumM107 + ',"m108":' + sumM108 + ',"m109":' + sumM109 + ',"m110":' + sumM110 + ',"m111":' + sumM111 + ',"m112":' + sumM112 + ',"a02":' + sumA2 + '}';
	stContacts += ']};';

    return stContacts;
}

function dayData(){	
	var currentYear = '';
	var currentMonth = '';
	var currentDay = '';
	
	var currentYear = request.getParameter('year');
	var currentMonth = request.getParameter('month');
	var currentDay = request.getParameter('day');
	if (currentYear == null && currentMonth == null && currentDay == null){
		var stDay = 'var dayData = [];';
	}
	else{
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters[0] = new nlobjSearchFilter('custrecord_clgx_portal_archive_date',null,'on', currentMonth + '/' + currentDay + '/' + currentYear);
		var searchContacts = nlapiSearchRecord('customrecord_clgx_portal_sessions_archiv', 'customsearch_clgx_rep_portal_usage_day', arrFilters, arrColumns);

		var stDay = 'var dayData = [';
		
		for ( var i = 0; searchContacts != null && i < searchContacts.length; i++ ) {
			var searchContact = searchContacts[i];
			
			stDay += '{id:"' + searchContact.getValue('custrecord_clgx_portal_archive_user_id', null, null) + 
							'",customer:"' + searchContact.getValue('custrecord_clgx_portal_archive_cust_name', null, null) + 
							'",contact:"' + searchContact.getValue('custrecord_clgx_portal_archive_user_name', null, null) + 
							'",time:"' + searchContact.getValue('custrecord_clgx_portal_archive_time', null, null) + 
							'",ip:"' + searchContact.getValue('custrecord_clgx_portal_archive_user_ip', null, null) +  '"},';
		}
		var strLen = stDay.length;
		if (searchContacts != null){
			stDay = stDay.slice(0,strLen-1);
		}
		stDay += '];';

	}
    return stDay;
}

function hitsData(){	
	var currentCustomer = '';
	var currentContact = '';
	
	var currentCustomer = request.getParameter('customerID');
	var currentContact = request.getParameter('contactID');
	if (currentCustomer == null && currentContact == null){
		var stHits = 'var hitsData = [];';
	}
	else{
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters[0] = new nlobjSearchFilter('custrecord_clgx_portal_archive_user_id',null,'is', currentContact);
		var searchContacts = nlapiSearchRecord('customrecord_clgx_portal_sessions_archiv', 'customsearch_clgx_rep_portal_usage_day', arrFilters, arrColumns);

		var stHits = 'var hitsData = [';
		
		for ( var i = 0; searchContacts != null && i < searchContacts.length; i++ ) {
			var searchContact = searchContacts[i];
			
			stHits += '{id:"' + i + 
							'",date:"' + searchContact.getValue('custrecord_clgx_portal_archive_date', null, null) + 
							'",time:"' + searchContact.getValue('custrecord_clgx_portal_archive_time', null, null) + 
							'",ip:"' + searchContact.getValue('custrecord_clgx_portal_archive_user_ip', null, null) +  '"},';
		}
		var strLen = stHits.length;
		if (searchContacts != null){
			stHits = stHits.slice(0,strLen-1);
		}
		stHits += '];';

	}
    return stHits;
}

function returnCurrentDay(){	
	var currentYear = '';
	var currentMonth = '';
	var currentDay = '';
	
	var currentYear = request.getParameter('year');
	var currentMonth = request.getParameter('month');
	var currentDay = request.getParameter('day');

	if (currentYear == null && currentMonth == null && currentDay == null){
		var stCurrentDay = '';
	}
	else{
		var stCurrentDay = currentMonth + '/' + currentDay + '/' + currentYear;
	}
    return stCurrentDay;
}

function returnCurrentMonthLine(){	
	var currentYear = '';
	var currentMonth = '';
	
	var currentYear = request.getParameter('year');
	var currentMonth = request.getParameter('month');

	if (currentYear == null && currentMonth == null){
		var stCurrentMonth = 0;
	}
	else{
		var stCurrentMonth = currentYear + currentMonth;
	}
    return stCurrentMonth;
}

