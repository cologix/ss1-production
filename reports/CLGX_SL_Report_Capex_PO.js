//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Report_Capex_PO.js
//	Script Name:	CLGX_SL_Report_Capex_PO
//	Script Id:		customscript_clgx_sl_rep_capex_po
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		3/30/2012
//-------------------------------------------------------------------------------------------------

function suitelet_rep_capex_po(request, response){
	try {
		nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------STARTED---------------------|');


		var projId = request.getParameter('projid');
		var context = request.getParameter('context');
		
		var arrProj = nlapiLookupField('job',projId,['entityid','jobname']);
        var projNbr = arrProj['entityid'];
        var projName = arrProj['jobname'];
		
		var resultsTitle = 'Capex Report - Purchase Orders for ' + projNbr + ' / ' + projName;
		
		
		
		var arrColumns = new Array();
		var arrFilters = new Array();	
		if (projId != ''){
			arrFilters.push(new nlobjSearchFilter('custbody_cologix_project_name',null,'is',projId));
		}
		var searchPOs = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_po', arrFilters, arrColumns); 
		
		
		
		var form = nlapiCreateForm(resultsTitle);
		var groupResults = form.addFieldGroup('groupResults', 'Results',null);
		var htmlReport = form.addField('custpage_clgx_html','inlinehtml', null, null, 'groupResults');
			htmlReport.setLayoutType('normal','startcol');
			

		arrPOIProj = new Array();
		arrPOId = new Array();
		arrPONbr = new Array();
		arrPOPeriod = new Array();
		arrPOAmount = new Array();

		for(var i=0; searchPOs != null && i<searchPOs.length; i++){
			arrPOIProj.push(searchPOs[i].getText('custbody_cologix_project_name',null,'GROUP'));
			arrPOId.push(searchPOs[i].getValue('internalid',null,'GROUP'));
			arrPONbr.push(searchPOs[i].getValue('tranid',null,'GROUP'));
			arrPOPeriod.push(searchPOs[i].getText('postingperiod',null,'GROUP'));
			arrPOAmount.push(searchPOs[i].getValue('amount',null,'SUM'));
		}
	
		var html = '';
	    html = '<link rel="stylesheet" href="/core/media/media.nl?id=41788&c=1337135&h=b09682241b6cd2f96ce1&_xt=.css" />';
	    html += '<script src="/core/media/media.nl?id=41787&c=1337135&h=2b829df96c50f1fed6e5&_xt=.js" type="text/javascript"></script>';
	
	    
	    html += '<table id="myTable" bgcolor="#eeeeee" border="0">';
		    html += '<thead><tr>';
			    html += '<td align="right" class="header">Project</td>';
			    html += '<td align="right" class="header">Period</td>';
			    html += '<td class="header">PO#</td>';
			    html += '<td align="right" class="header">Amount</td>';
		    html += '</tr></thead><tbody>';
		    
	    for (var i=0; searchPOs != null &&  i<searchPOs.length; ++i) {
		    html += '<tr>';
			    html += '<td align="right" class="col0">' + arrPOIProj[i] + '</td>';
			    html += '<td align="right" class="col0">' + arrPOPeriod[i] + '</td>';
			    html += '<td class="col0"><a class="report" href="/app/accounting/transactions/purchord.nl?id=' + arrPOId[i] + '" target="_blank">' + arrPONbr[i] + '</a></td>';
			    html += '<td align="right" class="col5">' + addCommas(parseFloat(arrPOAmount[i]).toFixed(2)) + '</td>';
		    html += '</tr>';
	    }
	
	    html += '<tr>';
		    html += '<td class="totals" align="right" colspan="3">TOTAL</td>';
		    html += '<td class="totals" align="right">' + sumArray(arrPOAmount) + '</td>';
	    html += '</tr>';
	
	    var imgSRC = '/core/media/media.nl?id=38239&c=1337135&h=4bc57aaf20cc0189b3f8';
	    
		    html += '<tr>';
			    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="1" width="75" /></td>';
			    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="1" width="75" /></td>';
			    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="1" width="50" /></td>';
			    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="1" width="75" /></td>';
		    html += '</tr>';
	    
	    html += '</tbody></table>';
	    
	    
	    html += '<script type="text/javascript">';
	        html += 'addTableRolloverEffect("myTable","tableRollOverEffect1","tableRowClickEffect1");';
	    html += '</script>';
	    
	    htmlReport.setDefaultValue(html);
		
		//form.addSubmitButton('Submit');
		response.writePage( form );
	
//---------- End Section 1 ------------------------------------------------------------------------------------------------


nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------FINISHED---------------------|');  
} 
catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
	if (error.getDetails != undefined){
	    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	    throw error;
	}
	else{
	    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	    throw nlapiCreateError('99999', error.toString());
	}
} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function addCommas(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function sumArray (arr){
	var sum = 0;
	for (var i=0; i<arr.length; ++i) {
		sum += parseFloat(arr[i]);
	}
	return addCommas(parseFloat(sum).toFixed(2));
}

