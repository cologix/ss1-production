nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Report_Capex_Projects.js
//	Script Name:	CLGX_SL_Report_Capex_Projects
//	Script Id:		customscript_clgx_sl_rep_capex_projects
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		3/9/2012
//-------------------------------------------------------------------------------------------------

function suitelet_rep_capex_projects(request, response){
    try {
        nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------STARTED---------------------|');

        var context = 'global';
        var reqContext = request.getParameter('custpage_clgx_context');
        if (reqContext == 'T'){
            context = 'local';
            var form = nlapiCreateForm('Capex Report - Local');
        }
        else{
            context = 'global';
            var form = nlapiCreateForm('Capex Report - Global');
        }

        var arrColumns = new Array();
        var arrFilters = new Array();
        arrColumns[0] = new nlobjSearchColumn('subsidiary', null, 'GROUP');
        arrColumns[1] = new nlobjSearchColumn('subsidiarynohierarchy', null, 'GROUP');
        arrFilters[0] = new nlobjSearchFilter('subsidiary',null,'noneof','@NONE@');
        var searchSubsidiaries = nlapiSearchRecord('job', null, arrFilters, arrColumns);

        var arrColumns = new Array();
        var arrFilters = new Array();
        arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
        arrColumns[1] = new nlobjSearchColumn('name', null, null).setSort(false);
        var searchFacilities = nlapiSearchRecord('customrecord_cologix_facility', null, arrFilters, arrColumns);

        var checkCSV = form.addField('custpage_clgx_csv', 'checkbox', 'Export to CSV', null, null);
        checkCSV.setLayoutType('outside','startcol');

        var checkContext = form.addField('custpage_clgx_context', 'checkbox', 'See report in local currencies', null, null);
        checkContext.setLayoutType('outside','startcol');

        var startDate = form.addField('custpage_clgx_start_date', 'date', 'Between : Start Date', null, null);
        startDate.setLayoutType('outside','startcol');

        var endDate = form.addField('custpage_clgx_end_date', 'date', 'And : End Date', null, null);
        endDate.setLayoutType('outside','startcol');

        var selectSubsidiaries = form.addField('custpage_clgx_subsidiary','multiselect', 'Subsidaries', null, null);
        var arrSubsidiaries = new Array();
        for (var i = 0; searchSubsidiaries != null && i < searchSubsidiaries.length; i++) {
            selectSubsidiaries.addSelectOption(searchSubsidiaries[i].getValue('subsidiary',null,'GROUP'), searchSubsidiaries[i].getValue('subsidiarynohierarchy',null,'GROUP'));
        }
        selectSubsidiaries.setLayoutType('normal','startrow');

        var selectFacilities = form.addField('custpage_clgx_facility','multiselect', 'Facilities', null, null);
        var arrfacilities = searchFacilities[0].getValue('custentity_cologix_facility',null,'GROUP');
        for (var i = 0; i < searchFacilities != null &&  i < searchFacilities.length; i++) {
            selectFacilities.addSelectOption(searchFacilities[i].getValue('internalid',null,null), searchFacilities[i].getValue('name',null,null));
        }
        selectFacilities.setLayoutType('normal','startrow');

        var htmlReport = form.addField('custpage_clgx_html','inlinehtml', null, null, null);
        htmlReport.setLayoutType('outside','startrow');

        var arrFiltersProjects = new Array();
        var arrFiltersPOs = new Array();
        var arrFiltersBills = new Array();
        var arrFiltersJournals = new Array();
        var arrFiltersActuals = new Array();
        var arrFiltersCapLabor = new Array();

        if(request.getMethod() == 'POST'){ // if POST add filters if any

            var reqCSV = request.getParameter('custpage_clgx_csv');

            var reqSubsidiaries = request.getParameter('custpage_clgx_subsidiary');
            if (reqSubsidiaries != ''){ // if any subsidiary filter is asked
                arrFiltersProjects.push(new nlobjSearchFilter('subsidiary',null,'anyof',reqSubsidiaries));
            }

            var reqFacilities = request.getParameter('custpage_clgx_facility');
            if (reqFacilities != ''){ // if any subsidiary filter is asked
                arrFiltersProjects.push(new nlobjSearchFilter('custentity_cologix_facility',null,'anyof',reqFacilities));
            }

            var reqStart = request.getParameter('custpage_clgx_start_date');
            var reqEnd = request.getParameter('custpage_clgx_end_date');

            if (reqStart != '' && reqEnd != ''){ // if dates interval filter is asked
                arrFiltersPOs.push(new nlobjSearchFilter('trandate',null,'within',reqStart,reqEnd));
                arrFiltersBills.push(new nlobjSearchFilter('trandate',null,'within',reqStart,reqEnd));
                arrFiltersJournals.push(new nlobjSearchFilter('trandate',null,'within',reqStart,reqEnd));
                arrFiltersActuals.push(new nlobjSearchFilter('trandate',null,'within',reqStart,reqEnd));
                arrFiltersCapLabor.push(new nlobjSearchFilter('trandate',null,'within',reqStart,reqEnd));
            }
        }

        var searchProj = nlapiSearchRecord('job', 'customsearch_clgx_rep_cpx_projects', arrFiltersProjects, null);
        var searchBudgetProj = nlapiSearchRecord('customrecord_clgx_budget', 'customsearch_clgx_rep_cpx_budget', null, null);
        var searchForecastProj = nlapiSearchRecord('customrecord_clgx_cap_bud_forecast', 'customsearch_clgx_rep_cpx_forecasts', null, null);

        // child searches global or local
        if (reqContext == 'T'){ // local
            var searchPOs = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_pos', arrFiltersPOs, null);
            var searchActuals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_local_actuals', arrFiltersActuals, null);
        }
        else{ // global
            var searchPOs = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_global_pos', arrFiltersPOs, null);
            var searchActuals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_cpx_global_actuals', arrFiltersActuals, null);
        }

        arrProjStatus = new Array();
        arrProjSubsidiary = new Array();
        arrProjFacility = new Array();
        arrProjCapexCategory = new Array();
        arrProjID = new Array();
        arrProjNbr = new Array();
        arrProjName = new Array();

        arrOpenPos = new Array();

        arrBillsTotal = new Array();
        arrBills2011 = new Array();
        arrBills0112 = new Array();
        arrBills0212 = new Array();
        arrBills0312 = new Array();
        arrBills0412 = new Array();
        arrBills0512 = new Array();
        arrBills0612 = new Array();
        arrBills0712 = new Array();
        arrBills0812 = new Array();
        arrBills0912 = new Array();
        arrBills1012 = new Array();
        arrBills1112 = new Array();
        arrBills1212 = new Array();
        arrBills2012 = new Array();
        arrBills0113 = new Array();
        arrBills0213 = new Array();
        arrBills0313 = new Array();
        arrBills0413 = new Array();
        arrBills0513 = new Array();
        arrBills0613 = new Array();
        arrBills0713 = new Array();
        arrBills0813 = new Array();
        arrBills0913 = new Array();
        arrBills1013 = new Array();
        arrBills1113 = new Array();
        arrBills1213 = new Array();
        arrBills2013 = new Array();
        arrBills0114 = new Array();
        arrBills0214 = new Array();
        arrBills0314 = new Array();
        arrBills0414 = new Array();
        arrBills0514 = new Array();
        arrBills0614 = new Array();
        arrBills0714 = new Array();
        arrBills0814 = new Array();
        arrBills0914 = new Array();
        arrBills1014 = new Array();
        arrBills1114 = new Array();
        arrBills1214 = new Array();
        arrBills2014 = new Array();
        arrBills0115 = new Array();
        arrBills0215 = new Array();
        arrBills0315 = new Array();
        arrBills0415 = new Array();
        arrBills0515 = new Array();
        arrBills0615 = new Array();
        arrBills0715 = new Array();
        arrBills0815 = new Array();
        arrBills0915 = new Array();
        arrBills1015 = new Array();
        arrBills1115 = new Array();
        arrBills1215 = new Array();
        arrBills2015 = new Array();
        arrBills0116 = new Array();
        arrBills0216 = new Array();
        arrBills0316 = new Array();
        arrBills0416 = new Array();
        arrBills0516 = new Array();
        arrBills0616 = new Array();
        arrBills0716 = new Array();
        arrBills0816 = new Array();
        arrBills0916 = new Array();
        arrBills1016 = new Array();
        arrBills1116 = new Array();
        arrBills1216 = new Array();
        arrBills2016 = new Array();

        arrComitCapital = new Array();

        arrBudget2011 = new Array();
        arrBudget2012 = new Array();
        arrBudget2013 = new Array();
        arrBudget2014 = new Array();
        arrBudget2015 = new Array();
        arrBudget2016 = new Array();
        arrBudgetTotal = new Array();

        arrComitCapitalVar = new Array();

        arrForecastTotal = new Array();
        arrForecast2011 = new Array();
        arrForecast0112 = new Array();
        arrForecast0212 = new Array();
        arrForecast0312 = new Array();
        arrForecast0412 = new Array();
        arrForecast0512 = new Array();
        arrForecast0612 = new Array();
        arrForecast0712 = new Array();
        arrForecast0812 = new Array();
        arrForecast0912 = new Array();
        arrForecast1012 = new Array();
        arrForecast1112 = new Array();
        arrForecast1212 = new Array();
        arrForecast2012 = new Array();
        arrForecast0113 = new Array();
        arrForecast0213 = new Array();
        arrForecast0313 = new Array();
        arrForecast0413 = new Array();
        arrForecast0513 = new Array();
        arrForecast0613 = new Array();
        arrForecast0713 = new Array();
        arrForecast0813 = new Array();
        arrForecast0913 = new Array();
        arrForecast1013 = new Array();
        arrForecast1113 = new Array();
        arrForecast1213 = new Array();
        arrForecast2013 = new Array();
        arrForecast0114 = new Array();
        arrForecast0214 = new Array();
        arrForecast0314 = new Array();
        arrForecast0414 = new Array();
        arrForecast0514 = new Array();
        arrForecast0614 = new Array();
        arrForecast0714 = new Array();
        arrForecast0814 = new Array();
        arrForecast0914 = new Array();
        arrForecast1014 = new Array();
        arrForecast1114 = new Array();
        arrForecast1214 = new Array();
        arrForecast2014 = new Array();
        arrForecast0115 = new Array();
        arrForecast0215 = new Array();
        arrForecast0315 = new Array();
        arrForecast0415 = new Array();
        arrForecast0515 = new Array();
        arrForecast0615 = new Array();
        arrForecast0715 = new Array();
        arrForecast0815 = new Array();
        arrForecast0915 = new Array();
        arrForecast1015 = new Array();
        arrForecast1115 = new Array();
        arrForecast1215 = new Array();
        arrForecast2015 = new Array();
        arrForecast0116 = new Array();
        arrForecast0216 = new Array();
        arrForecast0316 = new Array();
        arrForecast0416 = new Array();
        arrForecast0516 = new Array();
        arrForecast0616 = new Array();
        arrForecast0716 = new Array();
        arrForecast0816 = new Array();
        arrForecast0916 = new Array();
        arrForecast1016 = new Array();
        arrForecast1116 = new Array();
        arrForecast1216 = new Array();
        arrForecast2016 = new Array();

        arrTotComProj = new Array();
        arrTotProjVar = new Array();
        arr2014BudgetVar = new Array();
        arr2015BudgetVar = new Array();
        arr2016BudgetVar = new Array();

        for(var i=0; searchProj != null && i<searchProj.length; i++){

            // Projects columns --------------------------------------------------------------------
            arrProjStatus.push(searchProj[i].getValue('formulatext',null,null));
            arrProjSubsidiary.push(searchProj[i].getText('subsidiarynohierarchy',null,null));
            arrProjFacility.push(searchProj[i].getText('custentity_cologix_facility',null,null));
            arrProjCapexCategory.push(searchProj[i].getText('custentity_clgx_capex_category',null,null));
            arrProjID.push(searchProj[i].getValue('internalid',null,null));
            arrProjNbr.push(searchProj[i].getValue('entityid',null,null));
            arrProjName.push(searchProj[i].getValue('jobname',null,null));
            // Projects columns --------------------------------------------------------------------

            // Open Purchase Orders--------------------------------------------------------------------
            var stLine = 0;
            for(var j=0; searchPOs != null && j<searchPOs.length; j++){
                var thisProjID = searchPOs[j].getValue('custbody_cologix_project_name',null,'GROUP');
                if (thisProjID == arrProjID[i] ){

                    var resultRow = searchPOs[j];
                    var columns = resultRow.getAllColumns();
                    var thisProjAmount = resultRow.getValue(columns[2]);
                    arrOpenPos.push(parseFloat(thisProjAmount).toFixed(2));
                    stLine = 1;
                }
            }
            if (stLine == 0){ // no Pos for this project
                arrOpenPos.push('0.00');
            }
            // Open Purchase Orders--------------------------------------------------------------------

            // Bills related to Project --------------------------------------------------------------------
            var stLine = 0;
            for(var j=0; searchActuals != null && j<searchActuals.length; j++){

                var resultRow = searchActuals[j];
                var columns = resultRow.getAllColumns();

                var thisProjID = resultRow.getValue(columns[0]);
                if (thisProjID == arrProjID[i] ){

                    arrBillsTotal.push(parseFloat(resultRow.getValue(columns[1])).toFixed(2));
                    arrBills2011.push(parseFloat(resultRow.getValue(columns[2])).toFixed(2));
                    arrBills0112.push(parseFloat(resultRow.getValue(columns[3])).toFixed(2));
                    arrBills0212.push(parseFloat(resultRow.getValue(columns[4])).toFixed(2));
                    arrBills0312.push(parseFloat(resultRow.getValue(columns[5])).toFixed(2));
                    arrBills0412.push(parseFloat(resultRow.getValue(columns[6])).toFixed(2));
                    arrBills0512.push(parseFloat(resultRow.getValue(columns[7])).toFixed(2));
                    arrBills0612.push(parseFloat(resultRow.getValue(columns[8])).toFixed(2));
                    arrBills0712.push(parseFloat(resultRow.getValue(columns[9])).toFixed(2));
                    arrBills0812.push(parseFloat(resultRow.getValue(columns[10])).toFixed(2));
                    arrBills0912.push(parseFloat(resultRow.getValue(columns[11])).toFixed(2));
                    arrBills1012.push(parseFloat(resultRow.getValue(columns[12])).toFixed(2));
                    arrBills1112.push(parseFloat(resultRow.getValue(columns[13])).toFixed(2));
                    arrBills1212.push(parseFloat(resultRow.getValue(columns[14])).toFixed(2));
                    arrBills2012.push(parseFloat(resultRow.getValue(columns[15])).toFixed(2));
                    arrBills0113.push(parseFloat(resultRow.getValue(columns[16])).toFixed(2));
                    arrBills0213.push(parseFloat(resultRow.getValue(columns[17])).toFixed(2));
                    arrBills0313.push(parseFloat(resultRow.getValue(columns[18])).toFixed(2));
                    arrBills0413.push(parseFloat(resultRow.getValue(columns[19])).toFixed(2));
                    arrBills0513.push(parseFloat(resultRow.getValue(columns[20])).toFixed(2));
                    arrBills0613.push(parseFloat(resultRow.getValue(columns[21])).toFixed(2));
                    arrBills0713.push(parseFloat(resultRow.getValue(columns[22])).toFixed(2));
                    arrBills0813.push(parseFloat(resultRow.getValue(columns[23])).toFixed(2));
                    arrBills0913.push(parseFloat(resultRow.getValue(columns[24])).toFixed(2));
                    arrBills1013.push(parseFloat(resultRow.getValue(columns[25])).toFixed(2));
                    arrBills1113.push(parseFloat(resultRow.getValue(columns[26])).toFixed(2));
                    arrBills1213.push(parseFloat(resultRow.getValue(columns[27])).toFixed(2));
                    arrBills2013.push(parseFloat(resultRow.getValue(columns[28])).toFixed(2));
                    arrBills0114.push(parseFloat(resultRow.getValue(columns[29])).toFixed(2));
                    arrBills0214.push(parseFloat(resultRow.getValue(columns[30])).toFixed(2));
                    arrBills0314.push(parseFloat(resultRow.getValue(columns[31])).toFixed(2));
                    arrBills0414.push(parseFloat(resultRow.getValue(columns[32])).toFixed(2));
                    arrBills0514.push(parseFloat(resultRow.getValue(columns[33])).toFixed(2));
                    arrBills0614.push(parseFloat(resultRow.getValue(columns[34])).toFixed(2));
                    arrBills0714.push(parseFloat(resultRow.getValue(columns[35])).toFixed(2));
                    arrBills0814.push(parseFloat(resultRow.getValue(columns[36])).toFixed(2));
                    arrBills0914.push(parseFloat(resultRow.getValue(columns[37])).toFixed(2));
                    arrBills1014.push(parseFloat(resultRow.getValue(columns[38])).toFixed(2));
                    arrBills1114.push(parseFloat(resultRow.getValue(columns[39])).toFixed(2));
                    arrBills1214.push(parseFloat(resultRow.getValue(columns[40])).toFixed(2));
                    arrBills2014.push(parseFloat(resultRow.getValue(columns[41])).toFixed(2));
                    arrBills0115.push(parseFloat(resultRow.getValue(columns[42])).toFixed(2));
                    arrBills0215.push(parseFloat(resultRow.getValue(columns[43])).toFixed(2));
                    arrBills0315.push(parseFloat(resultRow.getValue(columns[44])).toFixed(2));
                    arrBills0415.push(parseFloat(resultRow.getValue(columns[45])).toFixed(2));
                    arrBills0515.push(parseFloat(resultRow.getValue(columns[46])).toFixed(2));
                    arrBills0615.push(parseFloat(resultRow.getValue(columns[47])).toFixed(2));
                    arrBills0715.push(parseFloat(resultRow.getValue(columns[48])).toFixed(2));
                    arrBills0815.push(parseFloat(resultRow.getValue(columns[49])).toFixed(2));
                    arrBills0915.push(parseFloat(resultRow.getValue(columns[50])).toFixed(2));
                    arrBills1015.push(parseFloat(resultRow.getValue(columns[51])).toFixed(2));
                    arrBills1115.push(parseFloat(resultRow.getValue(columns[52])).toFixed(2));
                    arrBills1215.push(parseFloat(resultRow.getValue(columns[53])).toFixed(2));
                    arrBills2015.push(parseFloat(resultRow.getValue(columns[54])).toFixed(2));
                    arrBills0116.push(parseFloat(resultRow.getValue(columns[55])).toFixed(2));
                    arrBills0216.push(parseFloat(resultRow.getValue(columns[56])).toFixed(2));
                    arrBills0316.push(parseFloat(resultRow.getValue(columns[57])).toFixed(2));
                    arrBills0416.push(parseFloat(resultRow.getValue(columns[58])).toFixed(2));
                    arrBills0516.push(parseFloat(resultRow.getValue(columns[59])).toFixed(2));
                    arrBills0616.push(parseFloat(resultRow.getValue(columns[60])).toFixed(2));
                    arrBills0716.push(parseFloat(resultRow.getValue(columns[61])).toFixed(2));
                    arrBills0816.push(parseFloat(resultRow.getValue(columns[62])).toFixed(2));
                    arrBills0916.push(parseFloat(resultRow.getValue(columns[63])).toFixed(2));
                    arrBills1016.push(parseFloat(resultRow.getValue(columns[64])).toFixed(2));
                    arrBills1116.push(parseFloat(resultRow.getValue(columns[65])).toFixed(2));
                    arrBills1216.push(parseFloat(resultRow.getValue(columns[66])).toFixed(2));
                    arrBills2016.push(parseFloat(resultRow.getValue(columns[67])).toFixed(2));


                    stLine = 1;
                }
            }
            if (stLine == 0){ // no Bills for this project
                arrBillsTotal.push('0.00');
                arrBills2011.push('0.00');
                arrBills0112.push('0.00');
                arrBills0212.push('0.00');
                arrBills0312.push('0.00');
                arrBills0412.push('0.00');
                arrBills0512.push('0.00');
                arrBills0612.push('0.00');
                arrBills0712.push('0.00');
                arrBills0812.push('0.00');
                arrBills0912.push('0.00');
                arrBills1012.push('0.00');
                arrBills1112.push('0.00');
                arrBills1212.push('0.00');
                arrBills2012.push('0.00');
                arrBills0113.push('0.00');
                arrBills0213.push('0.00');
                arrBills0313.push('0.00');
                arrBills0413.push('0.00');
                arrBills0513.push('0.00');
                arrBills0613.push('0.00');
                arrBills0713.push('0.00');
                arrBills0813.push('0.00');
                arrBills0913.push('0.00');
                arrBills1013.push('0.00');
                arrBills1113.push('0.00');
                arrBills1213.push('0.00');
                arrBills2013.push('0.00');
                arrBills0114.push('0.00');
                arrBills0214.push('0.00');
                arrBills0314.push('0.00');
                arrBills0414.push('0.00');
                arrBills0514.push('0.00');
                arrBills0614.push('0.00');
                arrBills0714.push('0.00');
                arrBills0814.push('0.00');
                arrBills0914.push('0.00');
                arrBills1014.push('0.00');
                arrBills1114.push('0.00');
                arrBills1214.push('0.00');
                arrBills2014.push('0.00');
                arrBills0115.push('0.00');
                arrBills0215.push('0.00');
                arrBills0315.push('0.00');
                arrBills0415.push('0.00');
                arrBills0515.push('0.00');
                arrBills0615.push('0.00');
                arrBills0715.push('0.00');
                arrBills0815.push('0.00');
                arrBills0915.push('0.00');
                arrBills1015.push('0.00');
                arrBills1115.push('0.00');
                arrBills1215.push('0.00');
                arrBills2015.push('0.00');
                arrBills0116.push('0.00');
                arrBills0216.push('0.00');
                arrBills0316.push('0.00');
                arrBills0416.push('0.00');
                arrBills0516.push('0.00');
                arrBills0616.push('0.00');
                arrBills0716.push('0.00');
                arrBills0816.push('0.00');
                arrBills0916.push('0.00');
                arrBills1016.push('0.00');
                arrBills1116.push('0.00');
                arrBills1216.push('0.00');
                arrBills2016.push('0.00');


            }

            // Calculated Committed Capital  --------------------------------------------------------------------
            var stComitCapital = parseFloat(arrOpenPos[i]) + parseFloat(arrBillsTotal[i]);

            if (stComitCapital != 'NaN'){
                arrComitCapital.push(stComitCapital.toFixed(2));
            }
            else{
                arrComitCapital.push('0.00');
            }
            // Calculated Committed Capital  --------------------------------------------------------------------

            // Budget related to Project --------------------------------------------------------------------
            var stLine = 0;
            for(var j=0; searchBudgetProj != null && j<searchBudgetProj.length; j++){
                var thisProjNbr = searchBudgetProj[j].getValue('custrecord_clgx_budget_project',null,'GROUP');
                if (thisProjNbr == arrProjNbr[i] ){

                    var resultRow = searchBudgetProj[j];
                    var columns = resultRow.getAllColumns();

                    var thisProjAmount = searchBudgetProj[j].getValue('custrecord_clgx_budget_amount',null,'SUM');
                    arrBudgetTotal.push(parseFloat(thisProjAmount).toFixed(2));
                    arrBudget2011.push(parseFloat(resultRow.getValue(columns[2])).toFixed(2));
                    arrBudget2012.push(parseFloat(resultRow.getValue(columns[3])).toFixed(2));
                    arrBudget2013.push(parseFloat(resultRow.getValue(columns[4])).toFixed(2));
                    arrBudget2014.push(parseFloat(resultRow.getValue(columns[5])).toFixed(2));
                    arrBudget2015.push(parseFloat(resultRow.getValue(columns[6])).toFixed(2));
                    arrBudget2016.push(parseFloat(resultRow.getValue(columns[7])).toFixed(2));
                    stLine = 1;
                }
            }
            if (stLine == 0){ // no Budget for this project
                arrBudget2011.push('0.00');
                arrBudget2012.push('0.00');
                arrBudget2013.push('0.00');
                arrBudget2014.push('0.00');
                arrBudget2015.push('0.00');
                arrBudget2016.push('0.00');
                arrBudgetTotal.push('0.00');
            }
            // Budget related to Project --------------------------------------------------------------------

            // Calculated Committed Capital Variance --------------------------------------------------------------------
            var stComitCapitalVar = (parseFloat(arrBudgetTotal[i]) - parseFloat(arrComitCapital[i])).toFixed(2);
            if (stComitCapitalVar != 'NaN'){
                arrComitCapitalVar.push(stComitCapitalVar);
            }
            else{
                arrComitCapitalVar.push('0.00');
            }
            // Calculated Committed Capital Variance --------------------------------------------------------------------


            arrForecastTotal = arrBillsTotal;
            arrForecast2011 = arrBills2011;
            arrForecast0112 = arrBills0112;
            arrForecast0212 = arrBills0212;
            arrForecast0312 = arrBills0312;
            arrForecast0412 = arrBills0412;
            arrForecast0512 = arrBills0512;
            arrForecast0612 = arrBills0612;
            arrForecast0712 = arrBills0712;
            arrForecast0812 = arrBills0812;
            arrForecast0912 = arrBills0912;
            arrForecast1012 = arrBills1012;
            arrForecast1112 = arrBills1112;
            arrForecast1212 = arrBills1212;
            arrForecast2012 = arrBills2012;
            arrForecast0113 = arrBills0113;
            arrForecast0213 = arrBills0213;
            arrForecast0313 = arrBills0313;
            arrForecast0413 = arrBills0413;
            arrForecast0513 = arrBills0513;
            arrForecast0613 = arrBills0613;
            arrForecast0713 = arrBills0713;
            arrForecast0813 = arrBills0813;
            arrForecast0913 = arrBills0913;
            arrForecast1013 = arrBills1013;
            arrForecast1113 = arrBills1113;
            arrForecast1213 = arrBills1213;
            arrForecast2013 = arrBills2013;
            arrForecast0114 = arrBills0114;
            arrForecast0214 = arrBills0214;
            arrForecast0314 = arrBills0314;
            arrForecast0414 = arrBills0414;
            arrForecast0514 = arrBills0514;
            arrForecast0614 = arrBills0614;
            arrForecast0714 = arrBills0714;
            arrForecast0814 = arrBills0814;
            arrForecast0914 = arrBills0914;
            arrForecast1014 = arrBills1014;
            arrForecast1114 = arrBills1114;
            arrForecast1214 = arrBills1214;
            arrForecast2014 = arrBills2014;
            arrForecast0115 = arrBills0115;
            arrForecast0215 = arrBills0215;
            arrForecast0315 = arrBills0315;
            arrForecast0415 = arrBills0415;
            arrForecast0515 = arrBills0515;
            arrForecast0615 = arrBills0615;
            arrForecast0715 = arrBills0715;
            arrForecast0815 = arrBills0815;
            arrForecast0915 = arrBills0915;
            arrForecast1015 = arrBills1015;
            arrForecast1115 = arrBills1115;
            arrForecast1215 = arrBills1215;
            arrForecast2015 = arrBills2015;
            arrForecast0116 = arrBills0116;
            arrForecast0216 = arrBills0216;
            arrForecast0316 = arrBills0316;
            arrForecast0416 = arrBills0416;
            arrForecast0516 = arrBills0516;
            arrForecast0616 = arrBills0616;
            arrForecast0716 = arrBills0716;
            arrForecast0816 = arrBills0816;
            arrForecast0916 = arrBills0916;
            arrForecast1016 = arrBills1016;
            arrForecast1116 = arrBills1116;
            arrForecast1216 = arrBills1216;
            arrForecast2016 = arrBills2016;

            // Calculated Total Committed and Projected --------------------------------------------------------------------
            var stTotComProj = (parseFloat(arrForecastTotal[i]) + parseFloat(arrComitCapital[i])).toFixed(2);
            if (stTotComProj != 'NaN'){
                arrTotComProj.push(stTotComProj);
            }
            else{
                arrTotComProj.push('0.00');
            }
            // Calculated Total Committed and Projected --------------------------------------------------------------------

            // Calculated Total Project Variance  --------------------------------------------------------------------
            var stTotProjVar = (parseFloat(arrBudgetTotal[i]) - parseFloat(arrTotComProj[i])).toFixed(2);
            if (stTotProjVar != 'NaN'){
                arrTotProjVar.push( stTotProjVar);
            }
            else{
                arrTotProjVar.push('0.00');
            }
            // Calculated Total Project Variance  --------------------------------------------------------------------

            // Calculated 2014 Budget Variance  --------------------------------------------------------------------
            var st2014BudgetVar = (parseFloat(arrBudget2014[i]) - parseFloat(arrBills2014[i])).toFixed(2);
            if (st2014BudgetVar != 'NaN'){
                arr2014BudgetVar.push(st2014BudgetVar);
            }
            else{
                arr2014BudgetVar.push('0.00');
            }
            // Calculated 2014 Budget Variance  --------------------------------------------------------------------
            // Calculated 2015 Budget Variance  --------------------------------------------------------------------
            var st2015BudgetVar = (parseFloat(arrBudget2015[i]) - parseFloat(arrBills2015[i])).toFixed(2);
            if (st2015BudgetVar != 'NaN'){
                arr2015BudgetVar.push(st2015BudgetVar);
            }
            else{
                arr2015BudgetVar.push('0.00');
            }
            // Calculated 2015 Budget Variance  --------------------------------------------------------------------
            // Calculated 2016 Budget Variance  --------------------------------------------------------------------
            var st2016BudgetVar = (parseFloat(arrBudget2016[i]) - parseFloat(arrBills2016[i])).toFixed(2);
            if (st2016BudgetVar != 'NaN'){
                arr2016BudgetVar.push(st2016BudgetVar);
            }
            else{
                arr2016BudgetVar.push('0.00');
            }
            // Calculated 2015 Budget Variance  --------------------------------------------------------------------

        }


        var html = '';
        html += '<link rel="stylesheet" type="text/css" href="//www.cologix.com/portal/jquery-easyui-1.2.6/themes/cupertino/easyui.css">';
        html += '<link rel="stylesheet" type="text/css" href="//www.cologix.com/portal/jquery-easyui-1.2.6/demo/demo.css">';
        html += '<link rel="stylesheet" type="text/css" href="//www.cologix.com/portal/jquery-easyui-1.2.6/themes/icon.css">';

        html += '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>';
        html += '<script type="text/javascript" src="//www.cologix.com/portal/jquery-easyui-1.2.6/jquery.easyui.min.js"></script>';
        html += '<script type="text/javascript" src="//www.cologix.com/portal/jquery-easyui-1.2.6/locale/easyui-lang-en.js"></script>';


        var stReportData = 'var reportData = [';

        for (var i=0; searchProj != null &&  i<arrProjID.length; ++i) {

            stReportData += '{status:"' + arrProjStatus[i] +
                '",subsidiary:"' + arrProjSubsidiary[i] +
                '",facility:"' + arrProjFacility[i] +
                '",projectID:"' + '<a class=grid href=/app/common/entity/custjob.nl?id=' + arrProjID[i] + ' target=_blank>' + arrProjNbr[i] + '</a>' +
                '",description:"' + arrProjName[i] +
                '",capexcateg:"' + arrProjCapexCategory[i] +
                '",openPOs:"' + addCommas(arrOpenPos[i]) +

                '",actual2011:"' + addCommas(arrBills2011[i]) +
                '",actual0112:"' + addCommas(arrBills0112[i]) +
                '",actual0212:"' + addCommas(arrBills0212[i]) +
                '",actual0312:"' + addCommas(arrBills0312[i]) +
                '",actual0412:"' + addCommas(arrBills0412[i]) +
                '",actual0512:"' + addCommas(arrBills0512[i]) +
                '",actual0612:"' + addCommas(arrBills0612[i]) +
                '",actual0712:"' + addCommas(arrBills0712[i]) +
                '",actual0812:"' + addCommas(arrBills0812[i]) +
                '",actual0912:"' + addCommas(arrBills0912[i]) +
                '",actual1012:"' + addCommas(arrBills1012[i]) +
                '",actual1112:"' + addCommas(arrBills1112[i]) +
                '",actual1212:"' + addCommas(arrBills1212[i]) +
                '",actual2012:"' + addCommas(arrBills2012[i]) +
                '",actual0113:"' + addCommas(arrBills0113[i]) +
                '",actual0213:"' + addCommas(arrBills0213[i]) +
                '",actual0313:"' + addCommas(arrBills0313[i]) +
                '",actual0413:"' + addCommas(arrBills0413[i]) +
                '",actual0513:"' + addCommas(arrBills0513[i]) +
                '",actual0613:"' + addCommas(arrBills0613[i]) +
                '",actual0713:"' + addCommas(arrBills0713[i]) +
                '",actual0813:"' + addCommas(arrBills0813[i]) +
                '",actual0913:"' + addCommas(arrBills0913[i]) +
                '",actual1013:"' + addCommas(arrBills1013[i]) +
                '",actual1113:"' + addCommas(arrBills1113[i]) +
                '",actual1213:"' + addCommas(arrBills1213[i]) +
                '",actual2013:"' + addCommas(arrBills2013[i]) +
                '",actual0114:"' + addCommas(arrBills0114[i]) +
                '",actual0214:"' + addCommas(arrBills0214[i]) +
                '",actual0314:"' + addCommas(arrBills0314[i]) +
                '",actual0414:"' + addCommas(arrBills0414[i]) +
                '",actual0514:"' + addCommas(arrBills0514[i]) +
                '",actual0614:"' + addCommas(arrBills0614[i]) +
                '",actual0714:"' + addCommas(arrBills0714[i]) +
                '",actual0814:"' + addCommas(arrBills0814[i]) +
                '",actual0914:"' + addCommas(arrBills0914[i]) +
                '",actual1014:"' + addCommas(arrBills1014[i]) +
                '",actual1114:"' + addCommas(arrBills1114[i]) +
                '",actual1214:"' + addCommas(arrBills1214[i]) +
                '",actual2014:"' + addCommas(arrBills2014[i]) +
                '",actual0115:"' + addCommas(arrBills0115[i]) +
                '",actual0215:"' + addCommas(arrBills0215[i]) +
                '",actual0315:"' + addCommas(arrBills0315[i]) +
                '",actual0415:"' + addCommas(arrBills0415[i]) +
                '",actual0515:"' + addCommas(arrBills0515[i]) +
                '",actual0615:"' + addCommas(arrBills0615[i]) +
                '",actual0715:"' + addCommas(arrBills0715[i]) +
                '",actual0815:"' + addCommas(arrBills0815[i]) +
                '",actual0915:"' + addCommas(arrBills0915[i]) +
                '",actual1015:"' + addCommas(arrBills1015[i]) +
                '",actual1115:"' + addCommas(arrBills1115[i]) +
                '",actual1215:"' + addCommas(arrBills1215[i]) +
                '",actual2015:"' + addCommas(arrBills2015[i]) +
                '",actual0116:"' + addCommas(arrBills0116[i]) +
                '",actual0216:"' + addCommas(arrBills0216[i]) +
                '",actual0316:"' + addCommas(arrBills0316[i]) +
                '",actual0416:"' + addCommas(arrBills0416[i]) +
                '",actual0516:"' + addCommas(arrBills0516[i]) +
                '",actual0616:"' + addCommas(arrBills0616[i]) +
                '",actual0716:"' + addCommas(arrBills0716[i]) +
                '",actual0816:"' + addCommas(arrBills0816[i]) +
                '",actual0916:"' + addCommas(arrBills0916[i]) +
                '",actual1016:"' + addCommas(arrBills1016[i]) +
                '",actual1116:"' + addCommas(arrBills1116[i]) +
                '",actual1216:"' + addCommas(arrBills1216[i]) +
                '",actual2016:"' + addCommas(arrBills2016[i]) +
                '",totalActual:"' + addCommas(arrBillsTotal[i]) +

                '",committedCapital:"' + addCommas(arrComitCapital[i]) +

                '",budget2011:"' + addCommas(arrBudget2011[i]) +
                '",budget2012:"' + addCommas(arrBudget2012[i]) +
                '",budget2013:"' + addCommas(arrBudget2013[i]) +
                '",budget2014:"' + addCommas(arrBudget2014[i]) +
                '",budget2015:"' + addCommas(arrBudget2015[i]) +
                '",budget2016:"' + addCommas(arrBudget2016[i]) +
                '",totalBudget:"' + addCommas(arrBudgetTotal[i]) +

                '",committedCapitalVariance:"' + addCommas(arrComitCapitalVar[i]) +

                '",forecast2011:"' + addCommas(arrForecast2011[i]) +
                '",forecast0112:"' + addCommas(arrForecast0112[i]) +
                '",forecast0212:"' + addCommas(arrForecast0212[i]) +
                '",forecast0312:"' + addCommas(arrForecast0312[i]) +
                '",forecast0412:"' + addCommas(arrForecast0412[i]) +
                '",forecast0512:"' + addCommas(arrForecast0512[i]) +
                '",forecast0612:"' + addCommas(arrForecast0612[i]) +
                '",forecast0712:"' + addCommas(arrForecast0712[i]) +
                '",forecast0812:"' + addCommas(arrForecast0812[i]) +
                '",forecast0912:"' + addCommas(arrForecast0912[i]) +
                '",forecast1012:"' + addCommas(arrForecast1012[i]) +
                '",forecast1112:"' + addCommas(arrForecast1112[i]) +
                '",forecast1212:"' + addCommas(arrForecast1212[i]) +
                '",forecast2012:"' + addCommas(arrForecast2012[i]) +
                '",forecast0113:"' + addCommas(arrForecast0113[i]) +
                '",forecast0213:"' + addCommas(arrForecast0213[i]) +
                '",forecast0313:"' + addCommas(arrForecast0313[i]) +
                '",forecast0413:"' + addCommas(arrForecast0413[i]) +
                '",forecast0513:"' + addCommas(arrForecast0513[i]) +
                '",forecast0613:"' + addCommas(arrForecast0613[i]) +
                '",forecast0713:"' + addCommas(arrForecast0713[i]) +
                '",forecast0813:"' + addCommas(arrForecast0813[i]) +
                '",forecast0913:"' + addCommas(arrForecast0913[i]) +
                '",forecast1013:"' + addCommas(arrForecast1013[i]) +
                '",forecast1113:"' + addCommas(arrForecast1113[i]) +
                '",forecast1213:"' + addCommas(arrForecast1213[i]) +
                '",forecast2013:"' + addCommas(arrForecast2013[i]) +
                '",forecast0114:"' + addCommas(arrForecast0114[i]) +
                '",forecast0214:"' + addCommas(arrForecast0214[i]) +
                '",forecast0314:"' + addCommas(arrForecast0314[i]) +
                '",forecast0414:"' + addCommas(arrForecast0414[i]) +
                '",forecast0514:"' + addCommas(arrForecast0514[i]) +
                '",forecast0614:"' + addCommas(arrForecast0614[i]) +
                '",forecast0714:"' + addCommas(arrForecast0714[i]) +
                '",forecast0814:"' + addCommas(arrForecast0814[i]) +
                '",forecast0914:"' + addCommas(arrForecast0914[i]) +
                '",forecast1014:"' + addCommas(arrForecast1014[i]) +
                '",forecast1114:"' + addCommas(arrForecast1114[i]) +
                '",forecast1214:"' + addCommas(arrForecast1214[i]) +
                '",forecast2014:"' + addCommas(arrForecast2014[i]) +
                '",forecast0115:"' + addCommas(arrForecast0115[i]) +
                '",forecast0215:"' + addCommas(arrForecast0215[i]) +
                '",forecast0315:"' + addCommas(arrForecast0315[i]) +
                '",forecast0415:"' + addCommas(arrForecast0415[i]) +
                '",forecast0515:"' + addCommas(arrForecast0515[i]) +
                '",forecast0615:"' + addCommas(arrForecast0615[i]) +
                '",forecast0715:"' + addCommas(arrForecast0715[i]) +
                '",forecast0815:"' + addCommas(arrForecast0815[i]) +
                '",forecast0915:"' + addCommas(arrForecast0915[i]) +
                '",forecast1015:"' + addCommas(arrForecast1015[i]) +
                '",forecast1115:"' + addCommas(arrForecast1115[i]) +
                '",forecast1215:"' + addCommas(arrForecast1215[i]) +
                '",forecast2015:"' + addCommas(arrForecast2015[i]) +
                '",forecast0116:"' + addCommas(arrForecast0116[i]) +
                '",forecast0216:"' + addCommas(arrForecast0216[i]) +
                '",forecast0316:"' + addCommas(arrForecast0316[i]) +
                '",forecast0416:"' + addCommas(arrForecast0416[i]) +
                '",forecast0516:"' + addCommas(arrForecast0516[i]) +
                '",forecast0616:"' + addCommas(arrForecast0616[i]) +
                '",forecast0716:"' + addCommas(arrForecast0716[i]) +
                '",forecast0816:"' + addCommas(arrForecast0816[i]) +
                '",forecast0916:"' + addCommas(arrForecast0916[i]) +
                '",forecast1016:"' + addCommas(arrForecast1016[i]) +
                '",forecast1116:"' + addCommas(arrForecast1116[i]) +
                '",forecast1216:"' + addCommas(arrForecast1216[i]) +
                '",forecast2016:"' + addCommas(arrForecast2016[i]) +
                '",totalForecast:"' + addCommas(arrForecastTotal[i]) +

                '",totalCommittedandProjected:"' + addCommas(arrTotComProj[i]) +
                '",totalProjectVariance:"' + addCommas(arrTotProjVar[i]) +
                '",budgetVariance2014:"' + addCommas(arr2014BudgetVar[i]) +
                '",budgetVariance2015:"' + addCommas(arr2015BudgetVar[i]) +
                '",budgetVariance2016:"' + addCommas(arr2016BudgetVar[i]) + '"},';
        }


        stReportData += '{status:"' + '' +
            '",subsidiary:"' + '' +
            '",facility:"' + '' +
            '",projectID:"' + 'TOTALS' +
            '",description:"' + '' +
            '",openPOs:"' + sumArray(arrOpenPos) +

            '",actual2011:"' + sumArray(arrBills2011) +
            '",actual0112:"' + sumArray(arrBills0112) +
            '",actual0212:"' + sumArray(arrBills0212) +
            '",actual0312:"' + sumArray(arrBills0312) +
            '",actual0412:"' + sumArray(arrBills0412) +
            '",actual0512:"' + sumArray(arrBills0512) +
            '",actual0612:"' + sumArray(arrBills0612) +
            '",actual0712:"' + sumArray(arrBills0712) +
            '",actual0812:"' + sumArray(arrBills0812) +
            '",actual0912:"' + sumArray(arrBills0912) +
            '",actual1012:"' + sumArray(arrBills1012) +
            '",actual1112:"' + sumArray(arrBills1112) +
            '",actual1212:"' + sumArray(arrBills1212) +
            '",actual2012:"' + sumArray(arrBills2012) +
            '",actual0113:"' + sumArray(arrBills0113) +
            '",actual0213:"' + sumArray(arrBills0213) +
            '",actual0313:"' + sumArray(arrBills0313) +
            '",actual0413:"' + sumArray(arrBills0413) +
            '",actual0513:"' + sumArray(arrBills0513) +
            '",actual0613:"' + sumArray(arrBills0613) +
            '",actual0713:"' + sumArray(arrBills0713) +
            '",actual0813:"' + sumArray(arrBills0813) +
            '",actual0913:"' + sumArray(arrBills0913) +
            '",actual1013:"' + sumArray(arrBills1013) +
            '",actual1113:"' + sumArray(arrBills1113) +
            '",actual1213:"' + sumArray(arrBills1213) +
            '",actual2013:"' + sumArray(arrBills2013) +
            '",actual0114:"' + sumArray(arrBills0114) +
            '",actual0214:"' + sumArray(arrBills0214) +
            '",actual0314:"' + sumArray(arrBills0314) +
            '",actual0414:"' + sumArray(arrBills0414) +
            '",actual0514:"' + sumArray(arrBills0514) +
            '",actual0614:"' + sumArray(arrBills0614) +
            '",actual0714:"' + sumArray(arrBills0714) +
            '",actual0814:"' + sumArray(arrBills0814) +
            '",actual0914:"' + sumArray(arrBills0914) +
            '",actual1014:"' + sumArray(arrBills1014) +
            '",actual1114:"' + sumArray(arrBills1114) +
            '",actual1214:"' + sumArray(arrBills1214) +
            '",actual2014:"' + sumArray(arrBills2014) +
            '",actual0115:"' + sumArray(arrBills0115) +
            '",actual0215:"' + sumArray(arrBills0215) +
            '",actual0315:"' + sumArray(arrBills0315) +
            '",actual0415:"' + sumArray(arrBills0415) +
            '",actual0515:"' + sumArray(arrBills0515) +
            '",actual0615:"' + sumArray(arrBills0615) +
            '",actual0715:"' + sumArray(arrBills0715) +
            '",actual0815:"' + sumArray(arrBills0815) +
            '",actual0915:"' + sumArray(arrBills0915) +
            '",actual1015:"' + sumArray(arrBills1015) +
            '",actual1115:"' + sumArray(arrBills1115) +
            '",actual1215:"' + sumArray(arrBills1215) +
            '",actual2015:"' + sumArray(arrBills2015) +
            '",actual0116:"' + sumArray(arrBills0116) +
            '",actual0216:"' + sumArray(arrBills0216) +
            '",actual0316:"' + sumArray(arrBills0316) +
            '",actual0416:"' + sumArray(arrBills0416) +
            '",actual0516:"' + sumArray(arrBills0516) +
            '",actual0616:"' + sumArray(arrBills0616) +
            '",actual0716:"' + sumArray(arrBills0716) +
            '",actual0816:"' + sumArray(arrBills0816) +
            '",actual0916:"' + sumArray(arrBills0916) +
            '",actual1016:"' + sumArray(arrBills1016) +
            '",actual1116:"' + sumArray(arrBills1116) +
            '",actual1216:"' + sumArray(arrBills1216) +
            '",actual2016:"' + sumArray(arrBills2016) +
            '",totalActual:"' + sumArray(arrBillsTotal) +

            '",committedCapital:"' + sumArray(arrComitCapital) +

            '",budget2011:"' + sumArray(arrBudget2011) +
            '",budget2012:"' + sumArray(arrBudget2012) +
            '",budget2013:"' + sumArray(arrBudget2013) +
            '",budget2014:"' + sumArray(arrBudget2014) +
            '",budget2015:"' + sumArray(arrBudget2015) +
            '",budget2016:"' + sumArray(arrBudget2016) +
            '",totalBudget:"' + sumArray(arrBudgetTotal) +

            '",committedCapitalVariance:"' + sumArray(arrComitCapitalVar) +

            '",forecast2011:"' + sumArray(arrForecast2011) +
            '",forecast0112:"' + sumArray(arrForecast0112) +
            '",forecast0212:"' + sumArray(arrForecast0212) +
            '",forecast0312:"' + sumArray(arrForecast0312) +
            '",forecast0412:"' + sumArray(arrForecast0412) +
            '",forecast0512:"' + sumArray(arrForecast0512) +
            '",forecast0612:"' + sumArray(arrForecast0612) +
            '",forecast0712:"' + sumArray(arrForecast0712) +
            '",forecast0812:"' + sumArray(arrForecast0812) +
            '",forecast0912:"' + sumArray(arrForecast0912) +
            '",forecast1012:"' + sumArray(arrForecast1012) +
            '",forecast1112:"' + sumArray(arrForecast1112) +
            '",forecast1212:"' + sumArray(arrForecast1212) +
            '",forecast2012:"' + sumArray(arrForecast2012) +
            '",forecast0113:"' + sumArray(arrForecast0113) +
            '",forecast0213:"' + sumArray(arrForecast0213) +
            '",forecast0313:"' + sumArray(arrForecast0313) +
            '",forecast0413:"' + sumArray(arrForecast0413) +
            '",forecast0513:"' + sumArray(arrForecast0513) +
            '",forecast0613:"' + sumArray(arrForecast0613) +
            '",forecast0713:"' + sumArray(arrForecast0713) +
            '",forecast0813:"' + sumArray(arrForecast0813) +
            '",forecast0913:"' + sumArray(arrForecast0913) +
            '",forecast1013:"' + sumArray(arrForecast1013) +
            '",forecast1113:"' + sumArray(arrForecast1113) +
            '",forecast1213:"' + sumArray(arrForecast1213) +
            '",forecast2013:"' + sumArray(arrForecast2013) +
            '",forecast0114:"' + sumArray(arrForecast0114) +
            '",forecast0214:"' + sumArray(arrForecast0214) +
            '",forecast0314:"' + sumArray(arrForecast0314) +
            '",forecast0414:"' + sumArray(arrForecast0414) +
            '",forecast0514:"' + sumArray(arrForecast0514) +
            '",forecast0614:"' + sumArray(arrForecast0614) +
            '",forecast0714:"' + sumArray(arrForecast0714) +
            '",forecast0814:"' + sumArray(arrForecast0814) +
            '",forecast0914:"' + sumArray(arrForecast0914) +
            '",forecast1014:"' + sumArray(arrForecast1014) +
            '",forecast1114:"' + sumArray(arrForecast1114) +
            '",forecast1214:"' + sumArray(arrForecast1214) +
            '",forecast2014:"' + sumArray(arrForecast2014) +
            '",forecast0115:"' + sumArray(arrForecast0115) +
            '",forecast0215:"' + sumArray(arrForecast0215) +
            '",forecast0315:"' + sumArray(arrForecast0315) +
            '",forecast0415:"' + sumArray(arrForecast0415) +
            '",forecast0515:"' + sumArray(arrForecast0515) +
            '",forecast0615:"' + sumArray(arrForecast0615) +
            '",forecast0715:"' + sumArray(arrForecast0715) +
            '",forecast0815:"' + sumArray(arrForecast0815) +
            '",forecast0915:"' + sumArray(arrForecast0915) +
            '",forecast1015:"' + sumArray(arrForecast1015) +
            '",forecast1115:"' + sumArray(arrForecast1115) +
            '",forecast1215:"' + sumArray(arrForecast1215) +
            '",forecast2015:"' + sumArray(arrForecast2015) +
            '",forecast0116:"' + sumArray(arrForecast0116) +
            '",forecast0216:"' + sumArray(arrForecast0216) +
            '",forecast0316:"' + sumArray(arrForecast0316) +
            '",forecast0416:"' + sumArray(arrForecast0416) +
            '",forecast0516:"' + sumArray(arrForecast0516) +
            '",forecast0616:"' + sumArray(arrForecast0616) +
            '",forecast0716:"' + sumArray(arrForecast0716) +
            '",forecast0816:"' + sumArray(arrForecast0816) +
            '",forecast0916:"' + sumArray(arrForecast0916) +
            '",forecast1016:"' + sumArray(arrForecast1016) +
            '",forecast1116:"' + sumArray(arrForecast1116) +
            '",forecast1216:"' + sumArray(arrForecast1216) +
            '",forecast2016:"' + sumArray(arrForecast2016) +
            '",totalForecast:"' + sumArray(arrForecastTotal) +

            '",totalCommittedandProjected:"' + sumArray(arrTotComProj) +
            '",totalProjectVariance:"' + sumArray(arrTotProjVar) +
            '",budgetVariance2014:"' + sumArray(arr2014BudgetVar) +
            '",budgetVariance2015:"' + sumArray(arr2015BudgetVar) +
            '",budgetVariance2016:"' + sumArray(arr2016BudgetVar) +'"}';


        //var strLen = stReportData.length;
        //if (searchProj != null){
        //	stReportData = stReportData.slice(0,strLen-1);
        //}
        stReportData += '];';


        html += '<script>';
        var objFile = nlapiLoadFile(2002382);
        var dataHTML = objFile.getValue();
        dataHTML = dataHTML.replace(new RegExp('{stReportData}','g'),stReportData);
        html += dataHTML;
        html += '</script>';

        html += '<style type="text/css">a.grid{text-decoration: none;color:darkred;}</style>';
        html += '<div style="width:1320px;height:440px;overflow:auto;">';
        html += '<table id="reportGrid"></table>';

        html += '</div>';


        htmlReport.setDefaultValue(html);

        // if CSV export was asked create the CSV file
        if (reqCSV == 'T' && searchProj != null){

            var stCSV = '';
            stCSV += 'Status,Subsidiary,Facility,Capex Category,Project ID,Description,';
            stCSV += 'Open POs,';
            stCSV += '2011 Actuals,';
            stCSV += 'Jan-2012 Actuals,Feb-2012 Actuals,Mar-2012 Actuals,Apr-2012 Actuals,May-2012 Actuals,Jun-2012 Actuals,Jul-2012 Actuals,Aug-2012 Actuals,Sep-2012 Actuals,Oct-2012 Actuals,Nov-2012 Actuals,Dec-2012 Actuals,2012 Actuals,';
            stCSV += 'Jan-2013 Actuals,Feb-2013 Actuals,Mar-2013 Actuals,Apr-2013 Actuals,May-2013 Actuals,Jun-2013 Actuals,Jul-2013 Actuals,Aug-2013 Actuals,Sep-2013 Actuals,Oct-2013 Actuals,Nov-2013 Actuals,Dec-2013 Actuals,2013 Actuals,';
            stCSV += 'Jan-2014 Actuals,Feb-2014 Actuals,Mar-2014 Actuals,Apr-2014 Actuals,May-2014 Actuals,Jun-2014 Actuals,Jul-2014 Actuals,Aug-2014 Actuals,Sep-2014 Actuals,Oct-2014 Actuals,Nov-2014 Actuals,Dec-2014 Actuals,2014 Actuals,';
            stCSV += 'Jan-2015 Actuals,Feb-2015 Actuals,Mar-2015 Actuals,Apr-2015 Actuals,May-2015 Actuals,Jun-2015 Actuals,Jul-2015 Actuals,Aug-2015 Actuals,Sep-2015 Actuals,Oct-2015 Actuals,Nov-2015 Actuals,Dec-2015 Actuals,2015 Actuals,';
            stCSV += 'Jan-2016 Actuals,Feb-2016 Actuals,Mar-2016 Actuals,Apr-2016 Actuals,May-2016 Actuals,Jun-2016 Actuals,Jul-2016 Actuals,Aug-2016 Actuals,Sep-2016 Actuals,Oct-2016 Actuals,Nov-2016 Actuals,Dec-2016 Actuals,2016 Actuals,';



            stCSV += 'Totals Actuals,';
            stCSV += 'Commited Capital,';
            stCSV += '2011 Budget,2012 Budget,2013 Budget,2014 Budget,2015 Budget,2016 Budget,Total Budget,';
            stCSV += 'Commited Capital Variance,';

            stCSV += '2011 Forecast,';
            stCSV += 'Jan-2012 Forecast,Feb-2012 Forecast,Mar-2012 Forecast,Apr-2012 Forecast,May-2012 Forecast,Jun-2012 Forecast,Jul-2012 Forecast,Aug-2012 Forecast,Sep-2012 Forecast,Oct-2012 Forecast,Nov-2012 Forecast,Dec-2012 Forecast,2012 Forecast,';
            stCSV += 'Jan-2013 Forecast,Feb-2013 Forecast,Mar-2013 Forecast,Apr-2013 Forecast,May-2013 Forecast,Jun-2013 Forecast,Jul-2013 Forecast,Aug-2013 Forecast,Sep-2013 Forecast,Oct-2013 Forecast,Nov-2013 Forecast,Dec-2013 Forecast,2013 Forecast,';
            stCSV += 'Jan-2014 Forecast,Feb-2014 Forecast,Mar-2014 Forecast,Apr-2014 Forecast,May-2014 Forecast,Jun-2014 Forecast,Jul-2014 Forecast,Aug-2014 Forecast,Sep-2014 Forecast,Oct-2014 Forecast,Nov-2014 Forecast,Dec-2014 Forecast,2014 Forecast,';
            stCSV += 'Jan-2015 Forecast,Feb-2015 Forecast,Mar-2015 Forecast,Apr-2015 Forecast,May-2015 Forecast,Jun-2015 Forecast,Jul-2015 Forecast,Aug-2015 Forecast,Sep-2015 Forecast,Oct-2015 Forecast,Nov-2015 Forecast,Dec-2015 Forecast,2015 Forecast,';
            stCSV += 'Jan-2016 Forecast,Feb-2016 Forecast,Mar-2016 Forecast,Apr-2016 Forecast,May-2016 Forecast,Jun-2016 Forecast,Jul-2016 Forecast,Aug-2016 Forecast,Sep-2016 Forecast,Oct-2016 Forecast,Nov-2016 Forecast,Dec-2016 Forecast,2016 Forecast,';
            stCSV += 'Total Forecast,';
            stCSV += 'Total Committed and Projected,';
            stCSV += 'Total Project Variance,';
            stCSV += '2014 Budget Variance';
            stCSV += '2015 Budget Variance';
            stCSV += '2016 Budget Variance';
            stCSV += '\n';

            for(var i=0; searchProj != null && i<searchProj.length; i++){
                stCSV += arrProjStatus[i].replace(/\,/g," ") + ',';
                stCSV += arrProjSubsidiary[i].replace(/\,/g," ") + ',';
                stCSV += arrProjFacility[i].replace(/\,/g," ") + ',';
                stCSV += arrProjCapexCategory[i].replace(/\,/g," ") + ',';
                stCSV += arrProjNbr[i].replace(/\,/g," ") + ',';
                stCSV += arrProjName[i].replace(/\,/g," ") + ',';

                stCSV += arrOpenPos[i].replace(/\,/g," ") + ',';

                stCSV += arrBills2011[i].replace(/\,/g," ") + ',';

                stCSV += arrBills0112[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0212[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0312[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0412[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0512[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0612[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0712[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0812[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0912[i].replace(/\,/g," ") + ',';
                stCSV += arrBills1012[i].replace(/\,/g," ") + ',';
                stCSV += arrBills1112[i].replace(/\,/g," ") + ',';
                stCSV += arrBills1212[i].replace(/\,/g," ") + ',';
                stCSV += arrBills2012[i].replace(/\,/g," ") + ',';

                stCSV += arrBills0113[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0213[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0313[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0413[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0513[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0613[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0713[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0813[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0913[i].replace(/\,/g," ") + ',';
                stCSV += arrBills1013[i].replace(/\,/g," ") + ',';
                stCSV += arrBills1113[i].replace(/\,/g," ") + ',';
                stCSV += arrBills1213[i].replace(/\,/g," ") + ',';
                stCSV += arrBills2013[i].replace(/\,/g," ") + ',';

                stCSV += arrBills0114[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0214[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0314[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0414[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0514[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0614[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0714[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0814[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0914[i].replace(/\,/g," ") + ',';
                stCSV += arrBills1014[i].replace(/\,/g," ") + ',';
                stCSV += arrBills1114[i].replace(/\,/g," ") + ',';
                stCSV += arrBills1214[i].replace(/\,/g," ") + ',';
                stCSV += arrBills2014[i].replace(/\,/g," ") + ',';

                stCSV += arrBills0115[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0215[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0315[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0415[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0515[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0615[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0715[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0815[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0915[i].replace(/\,/g," ") + ',';
                stCSV += arrBills1015[i].replace(/\,/g," ") + ',';
                stCSV += arrBills1115[i].replace(/\,/g," ") + ',';
                stCSV += arrBills1215[i].replace(/\,/g," ") + ',';
                stCSV += arrBills2015[i].replace(/\,/g," ") + ',';

                stCSV += arrBills0116[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0216[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0316[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0416[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0516[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0616[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0716[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0816[i].replace(/\,/g," ") + ',';
                stCSV += arrBills0916[i].replace(/\,/g," ") + ',';
                stCSV += arrBills1016[i].replace(/\,/g," ") + ',';
                stCSV += arrBills1116[i].replace(/\,/g," ") + ',';
                stCSV += arrBills1216[i].replace(/\,/g," ") + ',';
                stCSV += arrBills2016[i].replace(/\,/g," ") + ',';

                stCSV += arrBillsTotal[i].replace(/\,/g," ") + ',';

                stCSV += arrComitCapital[i].replace(/\,/g," ") + ',';

                stCSV += arrBudget2011[i].replace(/\,/g," ") + ',';
                stCSV += arrBudget2012[i].replace(/\,/g," ") + ',';
                stCSV += arrBudget2013[i].replace(/\,/g," ") + ',';
                stCSV += arrBudget2014[i].replace(/\,/g," ") + ',';
                stCSV += arrBudget2015[i].replace(/\,/g," ") + ',';
                stCSV += arrBudget2016[i].replace(/\,/g," ") + ',';
                stCSV += arrBudgetTotal[i].replace(/\,/g," ") + ',';

                stCSV += arrComitCapitalVar[i].replace(/\,/g," ") + ',';

                stCSV += arrForecast2011[i].replace(/\,/g," ") + ',';

                stCSV += arrForecast0112[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0212[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0312[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0412[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0512[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0612[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0712[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0812[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0912[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast1012[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast1112[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast1212[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast2012[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0113[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0213[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0313[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0413[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0513[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0613[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0713[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0813[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0913[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast1013[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast1113[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast1213[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast2013[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0114[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0214[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0314[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0414[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0514[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0614[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0714[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0814[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0914[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast1014[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast1114[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast1214[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast2014[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0115[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0215[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0315[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0415[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0515[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0615[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0715[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0815[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0915[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast1015[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast1115[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast1215[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast2015[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0116[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0216[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0316[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0416[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0516[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0616[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0716[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0816[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast0916[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast1016[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast1116[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast1216[i].replace(/\,/g," ") + ',';
                stCSV += arrForecast2016[i].replace(/\,/g," ") + ',';

                stCSV += arrForecastTotal[i].replace(/\,/g," ") + ',';

                stCSV += arrTotComProj[i].replace(/\,/g," ") + ',';
                stCSV += arrTotProjVar[i].replace(/\,/g," ") + ',';
                stCSV += arr2014BudgetVar[i].replace(/\,/g," ") + '\n';
                stCSV += arr2015BudgetVar[i].replace(/\,/g," ") + '\n';
                stCSV += arr2016BudgetVar[i].replace(/\,/g," ") + '\n';
            }

            var fileCSV = nlapiCreateFile('capex_report.csv', 'CSV', stCSV);
            fileCSV.setFolder(66212);
            var fileId = nlapiSubmitFile(fileCSV);
            var savedCSV = nlapiLoadFile(fileId);
            var csvURL = savedCSV.getURL();
            var htmlExport = '<script type="text/javascript">window.location = "' + csvURL + '"</script>';

            var htmlExportCSV = form.addField('custpage_clgx_export','inlinehtml', null, null, 'groupResults');
            htmlExportCSV.setDefaultValue(htmlExport);
            //var deletedCSV = nlapiDeleteFile(fileId);
        }

        form.addSubmitButton('Submit');
        response.writePage( form );

//---------- End Section 1 ------------------------------------------------------------------------------------------------


        nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------FINISHED---------------------|');
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function addCommas(nStr){
    if(nStr == '0.00'){
        return '';
    }
    else{
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
}

function sumArray (arr){
    var sum = 0;
    for (var i=0; i<arr.length; ++i) {
        sum += parseFloat(arr[i]);
    }
    return addCommas(parseFloat(sum).toFixed(2));
}
