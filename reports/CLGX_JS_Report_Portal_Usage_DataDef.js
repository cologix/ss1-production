
{stReportData}

$(function(){
	$('#reportGrid').datagrid({
		width:540,
		height:400,
		singleSelect:true,
		nowrap: false,
		striped: true,
		collapsible:false,
		sortName: 'case',
		sortOrder: 'desc',
		remoteSort: false,
		idField:'id',
		columns:[[
		    {field:'contact',title:'Contact',width:200,sortable:true},
			{field:'sesdate',title:'Date',width:100,sortable:true},
			{field:'sestime',title:'Time',width:80,sortable:true},
			{field:'sesip',title:'IP',width:120,sortable:true}
		]],
		fit:true
	});
	$('#reportGrid').datagrid('loadData', reportData);
});
