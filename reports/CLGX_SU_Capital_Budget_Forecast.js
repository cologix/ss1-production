nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Capital_Budget_Forecast.js
//	Script Name:	CLGX_SU_Capital_Budget_Forecast
//	Script Id:		customscript_clgx_su_cap_budget_forecast
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Capital Budget Forecast
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		3/26/2012
//-------------------------------------------------------------------------------------------------

function beforeSubmit(type){
	
	try {
		nlapiLogExecution('DEBUG','User Event - Before Submit','|-------------STARTED--------------|');
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 3/26/2012
// Details:	The script will calculate the external ID necessary for imports
//-------------------------------------------------------------------------------------------------

		var currentContext = nlapiGetContext();
		if (currentContext.getExecutionContext() == 'userinterface') {

			nlapiSetFieldValue('custrecord_clgx_cap_bud_fore_key', nlapiGetFieldText('custrecord_clgx_cap_bud_fore_project_id') + '/' + nlapiGetFieldText('custrecord_clgx_cap_bud_fore_period'));
			//nlapiSetFieldValue('externalid', nlapiGetFieldText('custrecord_clgx_cap_bud_fore_project_id') + '/' + nlapiGetFieldText('custrecord_clgx_cap_bud_fore_period'));
			
		}
		
//---------- End Section 1 ------------------------------------------------------------------------------------------------


		nlapiLogExecution('DEBUG','User Event - Before Submit','|-------------FINISHED--------------|');  
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

