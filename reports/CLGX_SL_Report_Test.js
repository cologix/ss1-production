//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Report_Capex_Projects.js
//	Script Name:	CLGX_SL_Report_Capex_Projects
//	Script Id:		customscript_clgx_sl_rep_capex_projects
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		3/9/2012
//-------------------------------------------------------------------------------------------------

function suitelet_rep_test(request, response){
	try {
		nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------STARTED---------------------|');
		
		var context = 'global';
		var reqContext = request.getParameter('custpage_clgx_context');
		if (reqContext == 'T'){
			context = 'local';
			var form = nlapiCreateForm('Capex Report - Local');
		}
		else{
			context = 'global';
			var form = nlapiCreateForm('Capex Report - Global');
		}
		
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrColumns[0] = new nlobjSearchColumn('subsidiary', null, 'GROUP');
		arrColumns[1] = new nlobjSearchColumn('subsidiarynohierarchy', null, 'GROUP');
		arrFilters[0] = new nlobjSearchFilter('subsidiary',null,'noneof','@NONE@');
		var searchSubsidiaries = nlapiSearchRecord('job', null, arrFilters, arrColumns);
		
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
		arrColumns[1] = new nlobjSearchColumn('altname', null, null).setSort(false);
		var searchFacilities = nlapiSearchRecord('customrecord_cologix_facility', null, arrFilters, arrColumns);
		
		var checkCSV = form.addField('custpage_clgx_csv', 'checkbox', 'Export to CSV', null, null);
		checkCSV.setLayoutType('outside','startcol');
		
		var checkContext = form.addField('custpage_clgx_context', 'checkbox', 'See report in local currencies', null, null);
		checkContext.setLayoutType('outside','startcol');
		
		var startDate = form.addField('custpage_clgx_start_date', 'date', 'Between : Start Date', null, null);
		startDate.setLayoutType('outside','startcol');
		
		var endDate = form.addField('custpage_clgx_end_date', 'date', 'And : End Date', null, null);
		endDate.setLayoutType('outside','startcol');
		
        var selectSubsidiaries = form.addField('custpage_clgx_subsidiary','multiselect', 'Subsidaries', null, null);
        var arrSubsidiaries = new Array();
	    for (var i = 0; searchSubsidiaries != null && i < searchSubsidiaries.length; i++) {
	    	selectSubsidiaries.addSelectOption(searchSubsidiaries[i].getValue('subsidiary',null,'GROUP'), searchSubsidiaries[i].getValue('subsidiarynohierarchy',null,'GROUP'));
		}
	    selectSubsidiaries.setLayoutType('normal','startrow');
	    
        var selectFacilities = form.addField('custpage_clgx_facility','multiselect', 'Facilities', null, null);
        var arrfacilities = searchFacilities[0].getValue('custentity_cologix_facility',null,'GROUP');
	    for (var i = 0; i < searchFacilities != null &&  i < searchFacilities.length; i++) {
	    	selectFacilities.addSelectOption(searchFacilities[i].getValue('internalid',null,null), searchFacilities[i].getValue('altname',null,null));
		}
	    selectFacilities.setLayoutType('normal','startrow');
	    
		var htmlReport = form.addField('custpage_clgx_html','inlinehtml', null, null, null);
		htmlReport.setLayoutType('outside','startrow');
	    
		var arrFiltersProjects = new Array();
		var arrFiltersPOs = new Array();
		var arrFiltersBills = new Array();
		var arrFiltersJournals = new Array();
		
		if(request.getMethod() == 'POST'){ // if POST add filters if any

			var reqCSV = request.getParameter('custpage_clgx_csv');

			var reqSubsidiaries = request.getParameter('custpage_clgx_subsidiary');
			if (reqSubsidiaries != ''){ // if any subsidiary filter is asked
				arrFiltersProjects.push(new nlobjSearchFilter('subsidiary',null,'anyof',reqSubsidiaries));
			}
			
			var reqFacilities = request.getParameter('custpage_clgx_facility');
			if (reqFacilities != ''){ // if any subsidiary filter is asked
				arrFiltersProjects.push(new nlobjSearchFilter('custentity_cologix_facility',null,'anyof',reqFacilities));
			}
			
			var reqStart = request.getParameter('custpage_clgx_start_date');
			var reqEnd = request.getParameter('custpage_clgx_end_date');
			
			if (reqStart != '' && reqEnd != ''){ // if dates interval filter is asked
				arrFiltersPOs.push(new nlobjSearchFilter('trandate',null,'within',reqStart,reqEnd));
				arrFiltersBills.push(new nlobjSearchFilter('trandate',null,'within',reqStart,reqEnd));
				arrFiltersJournals.push(new nlobjSearchFilter('trandate',null,'within',reqStart,reqEnd));
			}
		}

		var searchProj = nlapiSearchRecord('job', 'customsearch_clgx_rep_capex_projects', arrFiltersProjects, null);	
		var searchBudgetProj = nlapiSearchRecord('customrecord_clgx_budget', 'customsearch_clgx_rep_capex_global_budg', null, null);
		var searchForecastProj = nlapiSearchRecord('customrecord_clgx_cap_bud_forecast', 'customsearch_clgx_rep_capex_global_fore', null, null);
				
		// child searches global or local
		if (reqContext == 'T'){ // local
			var searchPOProj = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_loc_pos', arrFiltersPOs, null); 
			var searchBillProj = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_loc_bills', arrFiltersBills, null);
			var searchJournalProj = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_loc_journals', arrFiltersJournals, null);
			var searchJournalProjCapLabour = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_loc_journ_cl', arrFiltersJournals, null);
		}
		else{ // global
			//context = 'global';
			var searchPOProj = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_glo_pos', arrFiltersPOs, null); 
			var searchBillProj = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_glo_bills', arrFiltersBills, null);
			var searchJournalProj = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_glo_journals', arrFiltersJournals, null);
			var searchJournalProjCapLabour = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_glo_journ_cl', arrFiltersJournals, null);
		}
		
		
		arrProjStatus = new Array();
		arrProjSubsidiary = new Array();
		arrProjSubsidiaryID = new Array();
		arrProjFacility = new Array();
		arrProjFacilityID = new Array();
		arrProjCapexCategory = new Array();
		arrProjID = new Array();
		arrProjNbr = new Array();
		arrProjName = new Array();
		
		arrOpenPos = new Array();
		
		arrBillsTotal = new Array();
		arrBills2011 = new Array();
		arrBills2012 = new Array();
		arrBills0112 = new Array();
		arrBills0212 = new Array();
		arrBills0312 = new Array();
		arrBills0412 = new Array();
		arrBills0512 = new Array();
		arrBills0612 = new Array();
		arrBills0712 = new Array();
		arrBills0812 = new Array();
		arrBills0912 = new Array();
		arrBills1012 = new Array();
		arrBills1112 = new Array();
		arrBills1212 = new Array();
		
		arrBillsTotalCL = new Array();
		arrBills2011CL = new Array();
		arrBills2012CL = new Array();
		arrBills0112CL = new Array();
		arrBills0212CL = new Array();
		arrBills0312CL = new Array();
		arrBills0412CL = new Array();
		arrBills0512CL = new Array();
		arrBills0612CL = new Array();
		arrBills0712CL = new Array();
		arrBills0812CL = new Array();
		arrBills0912CL = new Array();
		arrBills1012CL = new Array();
		arrBills1112CL = new Array();
		arrBills1212CL = new Array();
		
		arrComitCapital = new Array();
		
		arrBudget2011 = new Array();
		arrBudget2012 = new Array();
		arrBudgetTotal = new Array();
		
		arrComitCapitalVar = new Array();
		
		arrForecastTotal = new Array();
		arrForecast2011 = new Array();
		arrForecast2012 = new Array();
		arrForecast0112 = new Array();
		arrForecast0212 = new Array();
		arrForecast0312 = new Array();
		arrForecast0412 = new Array();
		arrForecast0512 = new Array();
		arrForecast0612 = new Array();
		arrForecast0712 = new Array();
		arrForecast0812 = new Array();
		arrForecast0912 = new Array();
		arrForecast1012 = new Array();
		arrForecast1112 = new Array();
		arrForecast1212 = new Array();
		
		arrTotComProj = new Array();
		arrTotProjVar = new Array();
		arr2012BudgetVar = new Array();

		for(var i=0; searchProj != null && i<searchProj.length; i++){
			
		// Projects columns --------------------------------------------------------------------
			arrProjStatus.push(searchProj[i].getValue('formulatext',null,null));
			arrProjSubsidiary.push(searchProj[i].getText('subsidiarynohierarchy',null,null));
			arrProjSubsidiaryID.push(searchProj[i].getValue('subsidiarynohierarchy',null,null));
			arrProjFacility.push(searchProj[i].getText('custentity_cologix_facility',null,null));
			arrProjFacilityID.push(searchProj[i].getValue('custentity_cologix_facility',null,null));
			arrProjCapexCategory.push(searchProj[i].getText('custentity_clgx_capex_category',null,null));
			arrProjID.push(searchProj[i].getValue('internalid',null,null));
			arrProjNbr.push(searchProj[i].getValue('entityid',null,null));
			arrProjName.push(searchProj[i].getValue('jobname',null,null));
		// Projects columns --------------------------------------------------------------------
			
		// Open Purchase Orders--------------------------------------------------------------------
		    var stLine = 0;
			for(var j=0; searchPOProj != null && j<searchPOProj.length; j++){
				var thisProjID = searchPOProj[j].getValue('custbody_cologix_project_name',null,'GROUP');
				if (thisProjID == arrProjID[i] ){
					
					var resultRow = searchPOProj[j];
					var columns = resultRow.getAllColumns();
					
					//var thisProjAmount = searchPOProj[j].getValue('amount',null,'SUM');
					
					var thisProjAmount = resultRow.getValue(columns[1]);
					
					arrOpenPos.push(parseFloat(thisProjAmount).toFixed(2));
					stLine = 1;
				}
			}
		    if (stLine == 0){ // no Pos for this project
		    	arrOpenPos.push('0.00');
		    }
		// Open Purchase Orders--------------------------------------------------------------------
			
		// Bills related to Project --------------------------------------------------------------------
		    var stLine = 0;
			for(var j=0; searchBillProj != null && j<searchBillProj.length; j++){
				
				var resultRow = searchBillProj[j];
				var columns = resultRow.getAllColumns();
				
				var thisProjID = resultRow.getValue(columns[0]);
				if (thisProjID == arrProjID[i] ){
					
					arrBillsTotal.push(parseFloat(resultRow.getValue(columns[1])).toFixed(2));
					arrBills2011.push(parseFloat(resultRow.getValue(columns[3])).toFixed(2));
					arrBills2012.push(parseFloat(resultRow.getValue(columns[4])).toFixed(2));
					arrBills0112.push(parseFloat(resultRow.getValue(columns[5])).toFixed(2));
					arrBills0212.push(parseFloat(resultRow.getValue(columns[6])).toFixed(2));
					arrBills0312.push(parseFloat(resultRow.getValue(columns[7])).toFixed(2));
					arrBills0412.push(parseFloat(resultRow.getValue(columns[8])).toFixed(2));
					arrBills0512.push(parseFloat(resultRow.getValue(columns[9])).toFixed(2));
					arrBills0612.push(parseFloat(resultRow.getValue(columns[10])).toFixed(2));
					arrBills0712.push(parseFloat(resultRow.getValue(columns[11])).toFixed(2));
					arrBills0812.push(parseFloat(resultRow.getValue(columns[12])).toFixed(2));
					arrBills0912.push(parseFloat(resultRow.getValue(columns[13])).toFixed(2));
					arrBills1012.push(parseFloat(resultRow.getValue(columns[14])).toFixed(2));
					arrBills1112.push(parseFloat(resultRow.getValue(columns[15])).toFixed(2));
					arrBills1212.push(parseFloat(resultRow.getValue(columns[16])).toFixed(2));
					stLine = 1;
				}
			}
		    if (stLine == 0){ // no Bills for this project
		    	arrBillsTotal.push('0.00');
				arrBills2011.push('0.00');
				arrBills2012.push('0.00');
				arrBills0112.push('0.00');
				arrBills0212.push('0.00');
				arrBills0312.push('0.00');
				arrBills0412.push('0.00');
				arrBills0512.push('0.00');
				arrBills0612.push('0.00');
				arrBills0712.push('0.00');
				arrBills0812.push('0.00');
				arrBills0912.push('0.00');
				arrBills1012.push('0.00');
				arrBills1112.push('0.00');
				arrBills1212.push('0.00');
		    }
		// Bills related to Project --------------------------------------------------------------------	
		
		   
		    
			// Journals related to Project with Cap Labor --------------------------------------------------------------------
		    var stLine = 0;
			for(var j=0; searchJournalProjCapLabour != null && j<searchJournalProjCapLabour.length; j++){
				
				var resultRow = searchJournalProjCapLabour[j];
				var columns = resultRow.getAllColumns();
				
				var thisProjID = resultRow.getValue(columns[0]);
				if (thisProjID == arrProjID[i] ){
					
					arrBillsTotalCL.push(parseFloat(resultRow.getValue(columns[1])).toFixed(2));
					arrBills2011CL.push(parseFloat(resultRow.getValue(columns[3])).toFixed(2));
					arrBills2012CL.push(parseFloat(resultRow.getValue(columns[4])).toFixed(2));
					arrBills0112CL.push(parseFloat(resultRow.getValue(columns[5])).toFixed(2));
					arrBills0212CL.push(parseFloat(resultRow.getValue(columns[6])).toFixed(2));
					arrBills0312CL.push(parseFloat(resultRow.getValue(columns[7])).toFixed(2));
					arrBills0412CL.push(parseFloat(resultRow.getValue(columns[8])).toFixed(2));
					arrBills0512CL.push(parseFloat(resultRow.getValue(columns[9])).toFixed(2));
					arrBills0612CL.push(parseFloat(resultRow.getValue(columns[10])).toFixed(2));
					arrBills0712CL.push(parseFloat(resultRow.getValue(columns[11])).toFixed(2));
					arrBills0812CL.push(parseFloat(resultRow.getValue(columns[12])).toFixed(2));
					arrBills0912CL.push(parseFloat(resultRow.getValue(columns[13])).toFixed(2));
					arrBills1012CL.push(parseFloat(resultRow.getValue(columns[14])).toFixed(2));
					arrBills1112CL.push(parseFloat(resultRow.getValue(columns[15])).toFixed(2));
					arrBills1212CL.push(parseFloat(resultRow.getValue(columns[16])).toFixed(2));
					stLine = 1;
				}
			}
		    if (stLine == 0){ // no Bills for this project
		    	arrBillsTotalCL.push('0.00');
				arrBills2011CL.push('0.00');
				arrBills2012CL.push('0.00');
				arrBills0112CL.push('0.00');
				arrBills0212CL.push('0.00');
				arrBills0312CL.push('0.00');
				arrBills0412CL.push('0.00');
				arrBills0512CL.push('0.00');
				arrBills0612CL.push('0.00');
				arrBills0712CL.push('0.00');
				arrBills0812CL.push('0.00');
				arrBills0912CL.push('0.00');
				arrBills1012CL.push('0.00');
				arrBills1112CL.push('0.00');
				arrBills1212CL.push('0.00');
		    }
		// Journals related to Project  with Cap Labor --------------------------------------------------------------------  

		 
		// Add Journal (no Cap Labor) amounts to the existing Bills arrays  --------------------------------------------------------------------	
		//for(var i=0; i<arrProjID.length; i++){

			for(var j=0; searchJournalProj != null && j<searchJournalProj.length; j++){
				var thisProjID = searchJournalProj[j].getValue('custcol_clgx_journal_project_id',null,'GROUP');
				if (thisProjID == arrProjID[i] ){
					
					var resultRow = searchJournalProj[j];
					var columns = resultRow.getAllColumns();
					
			    	arrBillsTotal[i] = ((parseFloat(arrBillsTotal[i]) + parseFloat(resultRow.getValue(columns[1])))).toFixed(2);
					arrBills2011[i] = ((parseFloat(arrBills2011[i]) + parseFloat(resultRow.getValue(columns[3])))).toFixed(2);
					arrBills2012[i] = ((parseFloat(arrBills2012[i]) + parseFloat(resultRow.getValue(columns[4])))).toFixed(2);
					arrBills0112[i] = ((parseFloat(arrBills0112[i]) + parseFloat(resultRow.getValue(columns[5])))).toFixed(2);
					arrBills0212[i] = ((parseFloat(arrBills0212[i]) + parseFloat(resultRow.getValue(columns[6])))).toFixed(2);
					arrBills0312[i] = ((parseFloat(arrBills0312[i]) + parseFloat(resultRow.getValue(columns[7])))).toFixed(2);
					arrBills0412[i] = ((parseFloat(arrBills0412[i]) + parseFloat(resultRow.getValue(columns[8])))).toFixed(2);
					arrBills0512[i] = ((parseFloat(arrBills0512[i]) + parseFloat(resultRow.getValue(columns[9])))).toFixed(2);
					arrBills0612[i] = ((parseFloat(arrBills0612[i]) + parseFloat(resultRow.getValue(columns[10])))).toFixed(2);
					arrBills0712[i] = ((parseFloat(arrBills0712[i]) + parseFloat(resultRow.getValue(columns[11])))).toFixed(2);
					arrBills0812[i] = ((parseFloat(arrBills0812[i]) + parseFloat(resultRow.getValue(columns[12])))).toFixed(2);
					arrBills0912[i] = ((parseFloat(arrBills0912[i]) + parseFloat(resultRow.getValue(columns[13])))).toFixed(2);
					arrBills1012[i] = ((parseFloat(arrBills1012[i]) + parseFloat(resultRow.getValue(columns[14])))).toFixed(2);
					arrBills1112[i] = ((parseFloat(arrBills1112[i]) + parseFloat(resultRow.getValue(columns[15])))).toFixed(2);
					arrBills1212[i] = ((parseFloat(arrBills1212[i]) + parseFloat(resultRow.getValue(columns[16])))).toFixed(2);
				}
			}
		//}
		// Add Journal (no Cap Labor) amounts to the existing Bills arrays  --------------------------------------------------------------------
		
		    
		// Calculated Committed Capital  --------------------------------------------------------------------	
		    var stComitCapital = parseFloat(arrOpenPos[i]) + parseFloat(arrBillsTotal[i]);
		    
		    if (stComitCapital != 'NaN'){
		    	arrComitCapital.push(stComitCapital.toFixed(2));
		    }
		    else{
		    	arrComitCapital.push('0.00');
		    }
		// Calculated Committed Capital  --------------------------------------------------------------------	
		    
		// Budget related to Project --------------------------------------------------------------------
		    var stLine = 0;
			for(var j=0; searchBudgetProj != null && j<searchBudgetProj.length; j++){
				var thisProjNbr = searchBudgetProj[j].getValue('custrecord_clgx_budget_project',null,'GROUP');
				if (thisProjNbr == arrProjNbr[i] ){
					
					var resultRow = searchBudgetProj[j];
					var columns = resultRow.getAllColumns();
					
					var thisProjAmount = searchBudgetProj[j].getValue('custrecord_clgx_budget_amount',null,'SUM');
					arrBudgetTotal.push(parseFloat(thisProjAmount).toFixed(2));
					arrBudget2011.push(parseFloat(resultRow.getValue(columns[2])).toFixed(2));
					arrBudget2012.push(parseFloat(resultRow.getValue(columns[3])).toFixed(2));
					stLine = 1;
				}
			}
		    if (stLine == 0){ // no Budget for this project
		    	arrBudget2011.push('0.00');
		    	arrBudget2012.push('0.00');
		    	arrBudgetTotal.push('0.00');
		    }
		// Budget related to Project --------------------------------------------------------------------	  
		 
		// Calculated Committed Capital Variance --------------------------------------------------------------------	
		    var stComitCapitalVar = (parseFloat(arrBudgetTotal[i]) - parseFloat(arrComitCapital[i])).toFixed(2);
		    if (stComitCapitalVar != 'NaN'){
		    	arrComitCapitalVar.push(stComitCapitalVar);
		    }
		    else{
		    	arrComitCapitalVar.push('0.00');
		    }
		// Calculated Committed Capital Variance --------------------------------------------------------------------	
		  
		// Forecasts related to Project --------------------------------------------------------------------
		    var stLine = 0;
			for(var j=0; searchForecastProj != null && j<searchForecastProj.length; j++){
				var thisProjID = searchForecastProj[j].getValue('custrecord_clgx_cap_bud_fore_project_id',null,'GROUP');
				if (thisProjID == arrProjID[i] ){
					
					var resultRow = searchForecastProj[j];
					var columns = resultRow.getAllColumns();
					
					arrForecastTotal.push(parseFloat(resultRow.getValue(columns[1])).toFixed(2));
					arrForecast2011.push(parseFloat(resultRow.getValue(columns[2])).toFixed(2));
					arrForecast2012.push(parseFloat(resultRow.getValue(columns[3])).toFixed(2));
					arrForecast0112.push(parseFloat(resultRow.getValue(columns[4])).toFixed(2));
					arrForecast0212.push(parseFloat(resultRow.getValue(columns[5])).toFixed(2));
					arrForecast0312.push(parseFloat(resultRow.getValue(columns[6])).toFixed(2));
					arrForecast0412.push(parseFloat(resultRow.getValue(columns[7])).toFixed(2));
					arrForecast0512.push(parseFloat(resultRow.getValue(columns[8])).toFixed(2));
					arrForecast0612.push(parseFloat(resultRow.getValue(columns[9])).toFixed(2));
					arrForecast0712.push(parseFloat(resultRow.getValue(columns[10])).toFixed(2));
					arrForecast0812.push(parseFloat(resultRow.getValue(columns[11])).toFixed(2));
					arrForecast0912.push(parseFloat(resultRow.getValue(columns[12])).toFixed(2));
					arrForecast1012.push(parseFloat(resultRow.getValue(columns[13])).toFixed(2));
					arrForecast1112.push(parseFloat(resultRow.getValue(columns[14])).toFixed(2));
					arrForecast1212.push(parseFloat(resultRow.getValue(columns[15])).toFixed(2));
					stLine = 1;
				}
			}
		    if (stLine == 0){ // no Bills for this project
		    	arrForecastTotal.push('0.00');
		    	arrForecast2011.push('0.00');
		    	arrForecast2012.push('0.00');
		    	arrForecast0112.push('0.00');
		    	arrForecast0212.push('0.00');
		    	arrForecast0312.push('0.00');
		    	arrForecast0412.push('0.00');
		    	arrForecast0512.push('0.00');
		    	arrForecast0612.push('0.00');
		    	arrForecast0712.push('0.00');
		    	arrForecast0812.push('0.00');
		    	arrForecast0912.push('0.00');
		    	arrForecast1012.push('0.00');
		    	arrForecast1112.push('0.00');
		    	arrForecast1212.push('0.00');
		    }
		// Forecasts related to Project --------------------------------------------------------------------
		    
		// Calculated Total Committed and Projected --------------------------------------------------------------------	
		    var stTotComProj = (parseFloat(arrForecastTotal[i]) + parseFloat(arrComitCapital[i])).toFixed(2);
		    if (stTotComProj != 'NaN'){
		    	arrTotComProj.push(stTotComProj);
		    }
		    else{
		    	arrTotComProj.push('0.00');
		    }
		// Calculated Total Committed and Projected --------------------------------------------------------------------	
		    
		// Calculated Total Project Variance  --------------------------------------------------------------------	
		    var stTotProjVar = (parseFloat(arrBudgetTotal[i]) - parseFloat(arrTotComProj[i])).toFixed(2);
		    if (stTotProjVar != 'NaN'){
		    	arrTotProjVar.push( stTotProjVar);
		    }
		    else{
		    	arrTotProjVar.push('0.00');
		    }
		// Calculated Total Project Variance  --------------------------------------------------------------------	 
		    
		// Calculated 2012 Budget Variance  --------------------------------------------------------------------	
		    var st2012BudgetVar = (parseFloat(arrBudget2012[i]) - parseFloat(arrBills2012[i])).toFixed(2);
		    if (st2012BudgetVar != 'NaN'){
		    	arr2012BudgetVar.push(st2012BudgetVar);
		    }
		    else{
		    	arr2012BudgetVar.push('0.00');
		    }
		// Calculated 2012 Budget Variance  --------------------------------------------------------------------	   

		}

		
	var html = '';
	html += '<link rel="stylesheet" type="text/css" href="//www.cologix.com/portal/jquery-easyui-1.2.6/themes/cupertino/easyui.css">';
	html += '<link rel="stylesheet" type="text/css" href="//www.cologix.com/portal/jquery-easyui-1.2.6/demo/demo.css">';
	html += '<link rel="stylesheet" type="text/css" href="//www.cologix.com/portal/jquery-easyui-1.2.6/themes/icon.css">';

	html += '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>';
	html += '<script type="text/javascript" src="//www.cologix.com/portal/jquery-easyui-1.2.6/jquery.easyui.min.js"></script>';
	html += '<script type="text/javascript" src="//www.cologix.com/portal/jquery-easyui-1.2.6/locale/easyui-lang-en.js"></script>';
	
	
	var stReportData = 'var reportData = [';
	
	for (var i=0; searchProj != null &&  i<arrProjID.length; ++i) {
		
		stReportData += '{status:"' + arrProjStatus[i] + 
						'",subsidiary:"' + arrProjSubsidiary[i] +	
						'",subsidiaryID:"' + arrProjSubsidiaryID[i] +
						'",facility:"' + arrProjFacility[i] + 
						'",facilityID:"' + arrProjFacilityID[i] + 
						'",projectID:' + arrProjID[i] +
						',projectNbr:"' + arrProjNbr[i] + 
						'",description:"' + arrProjName[i] +
						'",capexcateg:"' + arrProjCapexCategory[i] +
						'",openPOs:' + arrOpenPos[i] + 
						
						',actual2011:' + arrBills2011[i] + 
						',actual2011CL:' + arrBills2011CL[i] + 
						',actual0112:' + arrBills0112[i] + 
						',actual0112CL:' + arrBills0112CL[i] + 
						',actual0212:' + arrBills0212[i] + 
						',actual0212CL:' + arrBills0212CL[i] + 
						',actual0312:' + arrBills0312[i] + 
						',actual0312CL:' + arrBills0312CL[i] + 
						',actual0412:' + arrBills0412[i] + 
						',actual0412CL:' + arrBills0412CL[i] + 
						',actual0512:' + arrBills0512[i] + 
						',actual0512CL:' + arrBills0512CL[i] + 
						',actual0612:' + arrBills0612[i] + 
						',actual0612CL:' + arrBills0612CL[i] + 
						',actual0712:' + arrBills0712[i] + 
						',actual0712CL:' + arrBills0712CL[i] + 
						',actual0812:' + arrBills0812[i] + 
						',actual0812CL:' + arrBills0812CL[i] + 
						',actual0912:' + arrBills0912[i] + 
						',actual0912CL:' + arrBills0912CL[i] + 
						',actual1012:' + arrBills1012[i] + 
						',actual1012CL:' + arrBills1012CL[i] + 
						',actual1112:' + arrBills1112[i] + 
						',actual1112CL:' + arrBills1112CL[i] + 
						',actual1212:' + arrBills1212[i] + 
						',actual1212CL:' + arrBills1212CL[i] + 
						',actual2012:' + arrBills2012[i] + 
						',actual2012CL:' + arrBills2012CL[i] + 
						',totalActual:' + arrBillsTotal[i] + 
						',totalActualCL:' + arrBillsTotalCL[i] + 
						
						',committedCapital:' + arrComitCapital[i] + 
						
						',budget2011:' + arrBudget2011[i] + 
						',budget2012:' + arrBudget2012[i] + 
						',totalBudget:' + arrBudgetTotal[i] + 
						
						',committedCapitalVariance:' + arrComitCapitalVar[i] + 
						
						',forecast2011:' + arrForecast2011[i] + 
						',forecast0112:' + arrForecast0112[i] + 
						',forecast0212:' + arrForecast0212[i] + 
						',forecast0312:' + arrForecast0312[i] + 
						',forecast0412:' + arrForecast0412[i] + 
						',forecast0512:' + arrForecast0512[i] + 
						',forecast0612:' + arrForecast0612[i] + 
						',forecast0712:' + arrForecast0712[i] + 
						',forecast0812:' + arrForecast0812[i] + 
						',forecast0912:' + arrForecast0912[i] + 
						',forecast1012:' + arrForecast1012[i] + 
						',forecast1112:' + arrForecast1112[i] + 
						',forecast1212:' + arrForecast1212[i] + 
						',forecast2012:' + arrForecast2012[i] + 
						',totalForecast:' + arrForecastTotal[i] + 
						
						',totalCommittedandProjected:' + arrTotComProj[i] + 
						',totalProjectVariance:' + arrTotProjVar[i] +
						',budgetVariance2012:' + arr2012BudgetVar[i] +  '},';
	}
	
	stReportData += '{status:"' + '' + 
	'",subsidiary:"' + 'TOTALS' +	
	'",facility:"' + 'TOTALS' + 
	'",projectID:"' + 'TOTALS' +
	'",projectNbr:"' + 'TOTALS' +
	'",description:"' + 'TOTALS' +
	'",capexcateg:"' + 'TOTALS' +
	'",openPOs:' + sumArray(arrOpenPos) +
	
	',actual2011:"' + sumArray(arrBills2011) + 
	'",actual2011CL:"' + sumArray(arrBills2011CL) + 
	'",actual0112:"' + sumArray(arrBills0112) + 
	'",actual0112CL:"' + sumArray(arrBills0112CL) + 
	'",actual0212:"' + sumArray(arrBills0212) + 
	'",actual0212CL:"' + sumArray(arrBills0212CL) + 
	'",actual0312:"' + sumArray(arrBills0312) + 
	'",actual0312CL:"' + sumArray(arrBills0312CL) + 
	'",actual0412:"' + sumArray(arrBills0412) + 
	'",actual0412CL:"' + sumArray(arrBills0412CL) + 
	'",actual0512:"' + sumArray(arrBills0512) + 
	'",actual0512CL:"' + sumArray(arrBills0512CL) +
	'",actual0612:"' + sumArray(arrBills0612) + 
	'",actual0612CL:"' + sumArray(arrBills0612CL) + 
	'",actual0712:"' + sumArray(arrBills0712) + 
	'",actual0712CL:"' + sumArray(arrBills0712CL) + 
	'",actual0812:"' + sumArray(arrBills0812) + 
	'",actual0812CL:"' + sumArray(arrBills0812CL) + 
	'",actual0912:"' + sumArray(arrBills0912) + 
	'",actual0912CL:"' + sumArray(arrBills0912CL) + 
	'",actual1012:"' + sumArray(arrBills1012) + 
	'",actual1012CL:"' + sumArray(arrBills1012CL) + 
	'",actual1112:"' + sumArray(arrBills1112) + 
	'",actual1112CL:"' + sumArray(arrBills1112CL) + 
	'",actual1212:"' + sumArray(arrBills1212) + 
	'",actual1212CL:"' + sumArray(arrBills1212CL) + 
	'",actual2012:"' + sumArray(arrBills2012) + 
	'",actual2012CL:"' + sumArray(arrBills2012CL) + 
	'",totalActual:"' + sumArray(arrBillsTotal) + 
	'",totalActualCL:"' + sumArray(arrBillsTotalCL) + 
	
	'",committedCapital:"' + sumArray(arrComitCapital) + 
	
	'",budget2011:"' + sumArray(arrBudget2011) + 
	'",budget2012:"' + sumArray(arrBudget2012) + 
	'",totalBudget:"' + sumArray(arrBudgetTotal) + 
	
	'",committedCapitalVariance:"' + sumArray(arrComitCapitalVar) + 
	
	'",forecast2011:"' + sumArray(arrForecast2011) + 
	'",forecast0112:"' + sumArray(arrForecast0112) + 
	'",forecast0212:"' + sumArray(arrForecast0212) + 
	'",forecast0312:"' + sumArray(arrForecast0312) + 
	'",forecast0412:"' + sumArray(arrForecast0412) + 
	'",forecast0512:"' + sumArray(arrForecast0512) + 
	'",forecast0612:"' + sumArray(arrForecast0612) + 
	'",forecast0712:"' + sumArray(arrForecast0712) + 
	'",forecast0812:"' + sumArray(arrForecast0812) + 
	'",forecast0912:"' + sumArray(arrForecast0912) + 
	'",forecast1012:"' + sumArray(arrForecast1012) + 
	'",forecast1112:"' + sumArray(arrForecast1112) + 
	'",forecast1212:"' + sumArray(arrForecast1212) + 
	'",forecast2012:"' + sumArray(arrForecast2012) + 
	'",totalForecast:"' + sumArray(arrForecastTotal) + 
	
	'",totalCommittedandProjected:"' + sumArray(arrTotComProj) + 
	'",totalProjectVariance:"' + sumArray(arrTotProjVar) +
	'",budgetVariance2012:"' + sumArray(arr2012BudgetVar) +  '"}';
	
	stReportData += '];';
	
	
	html += '<script>';
	var objFile = nlapiLoadFile(146217);
	var dataHTML = objFile.getValue();
	dataHTML = dataHTML.replace(new RegExp('{stReportData}','g'),stReportData);
	html += dataHTML;
	html += '</script>';
	
	html += '<style type="text/css">a.grid{text-decoration: none;color:darkred;}</style>';
    html += '<div style="width:1320px;height:440px;overflow:auto;">';
    html += '<table id="reportGrid"></table>';

    html += '</div>';

    
    htmlReport.setDefaultValue(html);
	
    // if CSV export was asked create the CSV file
	if (reqCSV == 'T' && searchProj != null){
		
		var stCSV = 'Status,Subsidiary,Facility,Capex Category,Project ID,Description,Open POs,2011,2011 Cap Labor,Jan-2012 Actuals,Jan-2012 Cap Labor,Feb-2012 Actuals,Feb-2012 Cap Labor,Mar-2012 Actuals,Mar-2012 Cap Labor,Apr-2012 Actuals,Apr-2012 Cap Labor,May-2012 Actuals,May-2012 Cap Labor,Jun-2012 Actuals,Jun-2012 Cap Labor,Jul-2012 Actuals,Jul-2012 Cap Labor,Aug-2012 Actuals,Aug-2012 Cap Labor,Sep-2012 Actuals,Sep-2012 Cap Labor,Oct-2012 Actuals,Oct-2012 Cap Labor,Nov-2012 Actuals,Nov-2012 Cap Labor,Dec-2012 Actuals,Dec-2012 Cap Labor,2012 Actuals,2012 Cap Labor,Total Actuals,Total Cap Labor,Committed Capital,2011 Budget,2012 Budget,Total Budget,Committed Capital Variance,2011 Forecast,Jan-2012 Forecast,Feb-2012 Forecast,Mar-2012 Forecast,Apr-2012 Forecast,May-2012 Forecast,Jun-2012 Forecast,Jul-2012 Forecast,Aug-2012 Forecast,Sep-2012 Forecast,Oct-2012 Forecast,Nov-2012 Forecast,Dec-2012 Forecast,2012 Forecast,Total Forecast,Total Committed and Projected,Total Project Variance,2012 Budget Variance,Project ID\n';
		
		for(var i=0; searchProj != null && i<searchProj.length; i++){
				stCSV += arrProjStatus[i].replace(/\,/g," ") + ',';
				stCSV += arrProjSubsidiary[i].replace(/\,/g," ") + ',';
				stCSV += arrProjFacility[i].replace(/\,/g," ") + ',';
				stCSV += arrProjCapexCategory[i].replace(/\,/g," ") + ',';
				stCSV += arrProjNbr[i].replace(/\,/g," ") + ',';
				stCSV += arrProjName[i].replace(/\,/g," ") + ',';
				stCSV += arrOpenPos[i].replace(/\,/g," ") + ',';
				
				stCSV += arrBills2011[i].replace(/\,/g," ") + ',';
				stCSV += arrBills2011CL[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0112[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0112CL[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0212[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0212CL[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0312[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0312CL[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0412[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0412CL[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0512[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0512CL[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0612[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0612CL[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0712[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0712CL[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0812[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0812CL[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0912[i].replace(/\,/g," ") + ',';
				stCSV += arrBills0912CL[i].replace(/\,/g," ") + ',';
				stCSV += arrBills1012[i].replace(/\,/g," ") + ',';
				stCSV += arrBills1012CL[i].replace(/\,/g," ") + ',';
				stCSV += arrBills1112[i].replace(/\,/g," ") + ',';
				stCSV += arrBills1112CL[i].replace(/\,/g," ") + ',';
				stCSV += arrBills1212[i].replace(/\,/g," ") + ',';
				stCSV += arrBills1212CL[i].replace(/\,/g," ") + ',';
				stCSV += arrBills2012[i].replace(/\,/g," ") + ',';
				stCSV += arrBills2012CL[i].replace(/\,/g," ") + ',';
				stCSV += arrBillsTotal[i].replace(/\,/g," ") + ',';
				stCSV += arrBillsTotalCL[i].replace(/\,/g," ") + ',';
				
				stCSV += arrComitCapital[i].replace(/\,/g," ") + ',';
				stCSV += arrBudget2011[i].replace(/\,/g," ") + ',';
				stCSV += arrBudget2012[i].replace(/\,/g," ") + ',';
				stCSV += arrBudgetTotal[i].replace(/\,/g," ") + ',';
				stCSV += arrComitCapitalVar[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast2011[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0112[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0212[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0312[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0412[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0512[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0612[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0712[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0812[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast0912[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast1012[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast1112[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast1212[i].replace(/\,/g," ") + ',';
				stCSV += arrForecast2012[i].replace(/\,/g," ") + ',';
				stCSV += arrForecastTotal[i].replace(/\,/g," ") + ',';
				stCSV += arrTotComProj[i].replace(/\,/g," ") + ',';
				stCSV += arrTotProjVar[i].replace(/\,/g," ") + ',';
				stCSV += arr2012BudgetVar[i].replace(/\,/g," ") + ',';
				stCSV += arrProjID[i].replace(/\,/g," ") + '\n';
			}
			
		var fileCSV = nlapiCreateFile('capex_report.csv', 'CSV', stCSV);
		fileCSV.setFolder(66212);
		var fileId = nlapiSubmitFile(fileCSV);
		var savedCSV = nlapiLoadFile(fileId);
		var csvURL = savedCSV.getURL();
		var htmlExport = '<script type="text/javascript">window.location = "' + csvURL + '"</script>';
		
		var htmlExportCSV = form.addField('custpage_clgx_export','inlinehtml', null, null, 'groupResults');
		htmlExportCSV.setDefaultValue(htmlExport);
		//var deletedCSV = nlapiDeleteFile(fileId);
	}

	form.addSubmitButton('Submit');
	response.writePage( form );
	
//---------- End Section 1 ------------------------------------------------------------------------------------------------


nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------FINISHED---------------------|');  
} 
catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
	if (error.getDetails != undefined){
	    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	    throw error;
	}
	else{
	    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	    throw nlapiCreateError('99999', error.toString());
	}
} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function addCommas(nStr){
	if(nStr == '0.00'){
		return '';
	}
	else{
		nStr += '';
		x = nStr.split('.');
		x1 = x[0];
		x2 = x.length > 1 ? '.' + x[1] : '';
		var rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		return x1 + x2;
	}
}

function sumArray (arr){
	var sum = 0;
	for (var i=0; i<arr.length; ++i) {
		sum += parseFloat(arr[i]);
	}
	return parseFloat(sum).toFixed(2);
}
