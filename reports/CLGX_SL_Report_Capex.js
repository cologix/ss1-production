//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Report_Capex.js
//	Script Name:	CLGX_SL_Report_Capex
//	Script Id:		customscript_clgx_sl_report_capex
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		3/9/2012
//-------------------------------------------------------------------------------------------------

function suiteletReportCapex(request, response){
	try {
		nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------STARTED---------------------|');
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 3/9/2012
// Details:	This script will generate the Capex report
//-----------------------------------------------------------------------------------------------------------------
		
		
		

		// pull out POs recordsets
		var searchPO = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_po', null, null); 
		var searchPOSub = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_po_sub', null, null); 
		var searchPOLoc = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_po_loc', null, null); 
		var searchPOProj = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_po_proj', null, null); 
		// pull out Bills recordsets
		var searchBill = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_bill', null, null);
		var searchBillSub = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_bill_sub', null, null);
		var searchBillLoc = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_bill_loc', null, null);
		var searchBillProj = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_bill_proj', null, null);
		// pull out Budgets recordsets
		var searchBudgetSub = nlapiSearchRecord('customrecord_clgx_budget', 'customsearch_clgx_rep_capex_budget_sub', null, null);
		var searchBudgetLoc = nlapiSearchRecord('customrecord_clgx_budget', 'customsearch_clgx_rep_capex_budget_loc', null, null);
		var searchBudgetProj = nlapiSearchRecord('customrecord_clgx_budget', 'customsearch_clgx_rep_capex_budget_proj', null, null);
		
		
		var arrData = new Array(29);
		for(var a=0; a<29; a++){
			arrData[a] = new Array(searchPO.length);
			for(var b=0; searchPO != null && b<searchPO.length; b++){
				arrData[a][b]=' ';
			}
		}
		

		for(var i=0; searchPO != null && i<searchPO.length; i++){
			
			arrData[0][i] = searchPO[i].getValue('subsidiary',null,'GROUP');
			arrData[1][i] = searchPO[i].getValue('subsidiarynohierarchy',null,'GROUP');
			var arrSubID = arrData[0][i];
			var arrSubName = arrData[1][i];
			for(var j=0; searchBudgetSub != null && j<searchBudgetSub.length; j++){
				var thisSubName = searchBudgetSub[j].getValue('custrecord_clgx_budget_subsidiary',null,'GROUP');
				if (thisSubName == arrSubName){
					arrData[2][i] = addCommas(parseFloat(searchBudgetSub[j].getValue('custrecord_clgx_budget_amount',null,'SUM')).toFixed(2));
				}
			}
			for(var j=0; searchPOSub != null && j<searchPOSub.length; j++){
				var thisSubID = searchPOSub[j].getValue('subsidiary',null,'GROUP');
				if (thisSubID == arrSubID){
					arrData[3][i] = addCommas(parseFloat(searchPOSub[j].getValue('amount',null,'SUM')).toFixed(2));
					arrData[4][i] = addCommas(parseFloat(Math.abs(searchPOSub[j].getValue('taxamount',null,'SUM')).toFixed(2)));
				}
			}
			for(var j=0; searchBillSub != null && j<searchBillSub.length; j++){
				var thisSubID = searchBillSub[j].getValue('subsidiary',null,'GROUP');
				if (thisSubID == arrSubID){
					arrData[5][i] = addCommas(parseFloat(searchBillSub[j].getValue('amount',null,'SUM')).toFixed(2));
					arrData[6][i] = addCommas(parseFloat(Math.abs(searchBillSub[j].getValue('taxamount',null,'SUM')).toFixed(2)));
				}
			}
			arrData[7][i] = searchPO[i].getValue('location',null,'GROUP');
			arrData[8][i] = searchPO[i].getText('location',null,'GROUP');
			var arrLocID = arrData[7][i];
			var arrLocName = arrData[8][i];
			for(var j=0; searchBudgetLoc != null && j<searchBudgetLoc.length; j++){
				var thisSubName = searchBudgetLoc[j].getValue('custrecord_clgx_budget_subsidiary',null,'GROUP');
				var thisLocName = searchBudgetLoc[j].getValue('custrecord_clgx_budget_location',null,'GROUP');
				if (thisLocName == arrLocName && thisSubName == arrSubName){
					arrData[9][i] = addCommas(parseFloat(searchBudgetLoc[j].getValue('custrecord_clgx_budget_amount',null,'SUM')).toFixed(2));
				}
			}
			for(var j=0; searchPOLoc != null && j<searchPOLoc.length; j++){
				var thisSubID = searchPOLoc[j].getValue('subsidiary',null,'GROUP');
				var thisLocID = searchPOLoc[j].getValue('location',null,'GROUP');
				if (thisLocID == arrLocID && thisSubID == arrSubID){
					arrData[10][i] = addCommas(parseFloat(searchPOLoc[j].getValue('amount',null,'SUM')).toFixed(2));
					arrData[11][i] = addCommas(parseFloat(Math.abs(searchPOLoc[j].getValue('taxamount',null,'SUM')).toFixed(2)));
				}
			}
			for(var j=0; searchBillLoc != null && j<searchBillLoc.length; j++){
				var thisSubID = searchBillLoc[j].getValue('subsidiary',null,'GROUP');
				var thisLocID = searchBillLoc[j].getValue('location',null,'GROUP');
				if (thisLocID == arrLocID && thisSubID == arrSubID){
					arrData[12][i] = addCommas(parseFloat(searchBillLoc[j].getValue('amount',null,'SUM')).toFixed(2));
					arrData[13][i] = addCommas(parseFloat(Math.abs(searchBillLoc[j].getValue('taxamount',null,'SUM')).toFixed(2)));
				}
			}
			arrData[14][i] = searchPO[i].getValue('custbody_cologix_project_name',null,'GROUP');
			arrData[15][i] = searchPO[i].getText('custbody_cologix_project_name',null,'GROUP');
			var arrProjID = arrData[14][i];
			var arrProjName = arrData[15][i];
			for(var j=0; searchBudgetProj != null && j<searchBudgetProj.length; j++){
				var thisSubName = searchBudgetProj[j].getValue('custrecord_clgx_budget_subsidiary',null,'GROUP');
				var thisLocName = searchBudgetProj[j].getValue('custrecord_clgx_budget_location',null,'GROUP');
				var thisProjName = searchBudgetProj[j].getValue('custrecord_clgx_budget_project',null,'GROUP');
				if (thisProjName == arrProjName && thisLocName == arrLocName && thisSubName == arrSubName){
					arrData[16][i] = addCommas(parseFloat(searchBudgetProj[j].getValue('custrecord_clgx_budget_amount',null,'SUM')).toFixed(2));
				}
			}
			for(var j=0; searchPOProj != null && j<searchPOProj.length; j++){
				var thisSubID = searchPOProj[j].getValue('subsidiary',null,'GROUP');
				var thisLocID = searchPOProj[j].getValue('location',null,'GROUP');
				var thisProjID = searchPOProj[j].getValue('custbody_cologix_project_name',null,'GROUP');
				if (thisProjID == arrProjID & thisLocID == arrLocID && thisSubID == arrSubID){
					arrData[17][i] = addCommas(parseFloat(searchPOProj[j].getValue('amount',null,'SUM')).toFixed(2));
					arrData[18][i] = addCommas(parseFloat(Math.abs(searchPOProj[j].getValue('taxamount',null,'SUM')).toFixed(2)));
				}
			}
			for(var j=0; searchBillProj != null && j<searchBillProj.length; j++){
				var thisSubID = searchBillProj[j].getValue('subsidiary',null,'GROUP');
				var thisLocID = searchBillProj[j].getValue('location',null,'GROUP');
				var thisProjID = searchBillProj[j].getValue('custbody_cologix_project_name',null,'GROUP');
				if (thisProjID == arrProjID && thisLocID == arrLocID && thisSubID == arrSubID){
					arrData[19][i] = addCommas(parseFloat(searchBillProj[j].getValue('amount',null,'SUM')).toFixed(2));
					arrData[20][i] = addCommas(parseFloat(Math.abs(searchBillProj[j].getValue('taxamount',null,'SUM')).toFixed(2)));
				}
			}
			arrData[21][i] = searchPO[i].getValue('internalid',null,'GROUP');
			arrData[22][i] = searchPO[i].getValue('tranid',null,'GROUP');
			//arrData[23][i]
			//arrData[24][i]
			arrData[25][i] = searchPO[i].getValue('memomain',null,'GROUP').replace(/\'/g,"");
			arrData[26][i] = searchPO[i].getValue('date',null,'GROUP');
			arrData[27][i] = addCommas(parseFloat(searchPO[i].getValue('amount',null,'SUM')).toFixed(2));
			arrData[28][i] = addCommas(parseFloat(Math.abs(searchPO[i].getValue('taxamount',null,'SUM')).toFixed(2)));
		}
		
		
		
		/*
	if(request.getMethod() == 'GET'){ //GET call
		
		var form = nlapiCreateForm('Capex Report');
		/*
		var groupFilters = form.addFieldGroup('groupFilters', 'Filters',null);
		var dtFrom = form.addField('custpage_clgx_from', 'date', 'From', null, 'groupFilters');
		var dtTo = form.addField('custpage_clgx_to', 'date', 'To', null, 'groupFilters');
		
		var field3 = form.addField('field3', 'text', 'Filter 3', null, 'groupFilters');
			field3.setLayoutType('normal','startcol');
		var field4 = form.addField('field4', 'text', 'Filter 4', null, 'groupFilters');
		var field5 = form.addField('field5', 'text', 'Filter 5', null, 'groupFilters');
			field5.setLayoutType('normal','startcol');
		var field6 = form.addField('field6', 'text', 'Filter 6', null, 'groupFilters');
		
		var groupResults = form.addFieldGroup('groupResults', 'Results',null);

		var htmlReport = form.addField('custpage_clgx_html','inlinehtml', null, null, 'groupResults');
			htmlReport.setLayoutType('normal','startcol');
	}
	
	else{ //POST call
		var form = nlapiCreateForm('Capex Report');

		var groupFilters = form.addFieldGroup('groupFilters', 'Filters',null);
		var dtFrom = form.addField('custpage_clgx_from', 'date', 'From', null, 'groupFilters');
			dtFrom.setDefaultValue(request.getParameter('custpage_clgx_from'));
		var dtTo = form.addField('custpage_clgx_to', 'date', 'To', null, 'groupFilters');
			dtTo.setDefaultValue(request.getParameter('custpage_clgx_to'));
			
		var field3 = form.addField('field3', 'text', 'Filter 3', null, 'groupFilters');
			field3.setDefaultValue(request.getParameter('field3'));
			field3.setLayoutType('normal','startcol');
		var field4 = form.addField('field4', 'text', 'Filter 4', null, 'groupFilters');
			field4.setDefaultValue(request.getParameter('field4'));
		var field5 = form.addField('field5', 'text', 'Filter 5', null, 'groupFilters');
			field5.setDefaultValue(request.getParameter('field5'));
			field5.setLayoutType('normal','startcol');
		var field6 = form.addField('field6', 'text', 'Filter 6', null, 'groupFilters');
			field6.setDefaultValue(request.getParameter('field6'));
			
		var groupResults = form.addFieldGroup('groupResults', 'Results',null);

		var htmlReport = form.addField('custpage_clgx_html','inlinehtml', null, null, 'groupResults');
			htmlReport.setLayoutType('normal','startcol');
	}
	*/
	
	
	var form = nlapiCreateForm('Capex Report');
	var htmlReport = form.addField('custpage_clgx_html','inlinehtml', null, null, 'groupResults');
	htmlReport.setLayoutType('normal','startcol');
	
	
	var wBudget = '100';
	var wSTotal = '100';
	var wTotal = '100';
	var wTax = '63';
	
	var html = '';
    html = '<link rel="stylesheet" href="/core/media/media.nl?id=34885&c=1337135&h=c8182f041de548d85444&_xt=.css" />';
    html += '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js" type="text/javascript"></script>';
    html += '<script src="/core/media/media.nl?id=34886&c=1337135&h=5221bf5b415b42075e4e&_xt=.js" type="text/javascript"></script>';
    html += '<script src="/core/media/media.nl?id=34887&c=1337135&h=18f6d4b2cd72b2aae3ee&_xt=.js" type="text/javascript"></script>';
    html += '<div id="tiplayer" style="position:absolute; visibility:hidden; z-index:10000;"></div>';

    
    html += '<div width="1300px" class="canvas">';

    html += '<div style="font-family: Verdana, helvetica, arial, sans-serif;background: #fff;color: #333;padding: 1em;">';
    html += '<div id="sidetree">';
    //html += '<div class="treeheader"></div>';
    html += '<div id="sidetreecontrol">';
	
    html += '<div class="line" style="width:1280px;height:15px;">';
	html += '<div style="width:680px" class="empty" align="center">&nbsp;</div>';
	html += '<div style="width:287px" class="header" align="center" colspan="3">POs</div>';
	html += '<div style="width:287px" class="header" align="center" colspan="3">BILLS</div>';
	html += '</div>';
	
    html += '<div class="line" style="width:1285px;height:15px;">';
    html += '<div style="width:559px" class="subheader"><a class="tree" href="?#">Collapse All</a> | <a class="tree" href="?#">Expand All &nbsp;&nbsp;&nbsp; /Subsidiaries/Locations/Projects/POs/</a></div>';
    html += '<div style="width:' + wBudget + 'px" class="subheader" align="right">BUDGET</div>';
	html += '<div style="width:' + wSTotal + 'px" class="subheader" align="right">Subtotal</div>';
	html += '<div style="width:' + wTax + 'px" class="subheader" align="right">Taxes</div>';
	html += '<div style="width:' + wTotal + 'px" class="subheader" align="right">Total</div>';
	html += '<div style="width:' + wSTotal + 'px" class="subheader" align="right">Subtotal</div>';
	html += '<div style="width:' + wTax + 'px" class="subheader" align="right">Taxes</div>';
	html += '<div style="width:' + wTotal + 'px" class="subheader" align="right">Total</div>';
	html += '</div>';
	
	
	html += '</div>';
	
    html += '<ul id="tree">';
	html += '<ul>';

	
	for (var i=0; i<arrData[0].length; ++i) {
		
    	//////////////////////////////////////////////////
    	var thisSubID = arrData[0][i];
    	var thisSubName  = arrData[1][i];
    	if (i == 0) {
    		var lastSubID = thisSubID;
    		var lastSubName = thisSubName;
    	}
    	else {
    		var lastSubID = arrData[0][i-1];
    		var lastSubName = arrData[1][i-1];
    	}
    	if (i < arrData[0].length - 1) {
    		var nextSubID = arrData[0][i+1];
    		var nextSubName = arrData[1][i+1];
    	}
    	else {
    		var nextSubID = thisSubID;
    		var nextSubName = thisSubName;
    	}
    	/////////////////////////////////////////////////
    	var thisLocID = arrData[7][i];
    	var thisLocName  = arrData[8][i];
    	if (i == 0) {
    		var lastLocID = thisLocID;
    		var lastLocName = thisLocName;
    	}
    	else {
    		var lastLocID = arrData[7][i-1];
    		var lastLocName = arrData[8][i-1];
    	}
    	if (i < arrData[0].length - 1) {
    		var nextLocID = arrData[7][i+1];
    		var nextLocName = arrData[8][i+1];
    	}
    	else {
    		var nextLocID = thisLocID;
    		var nextLocName = thisLocName;
    	}
    	//////////////////////////////////////////////////
    	var thisProjID = arrData[14][i];
    	var thisProjName  = arrData[15][i];
    	if (i == 0) {
    		var lastProjID = thisProjID;
    		var lastProjName = thisProjName;
    	}
    	else {
    		var lastProjID = arrData[14][i-1];
    		var lastProjName = arrData[15][i-1];
    	}
    	if (i < arrData[0].length - 1) {
    		var nextProjID = arrData[14][i+1];
    		var nextProjName = arrData[5][i+1];
    	}
    	else {
    		var nextProjID = thisProjID;
    		var nextProjName = thisProjName;
    	}
    	//////////////////////////////////////////////////
    
		
		if (lastSubID != thisSubID || i == 0){
			var totSubPO = addCommas((parseFloat(arrData[3][i].replace(/\,/g,"")) + parseFloat(arrData[4][i].replace(/\,/g,""))).toFixed(2));
			if (totSubPO == 'NaN'){totSubPO=''};
			var totSubBill = addCommas((parseFloat(arrData[5][i].replace(/\,/g,"")) + parseFloat(arrData[6][i].replace(/\,/g,""))).toFixed(2));
			if (totSubBill == 'NaN'){totSubBill=''};
			html += '<li>';
			html += '<div class="line" style="width:1228px;height:15px;">';
			html += '<div style="width:503px" class="level1">' + arrData[1][i] + '</div>';
			html += '<div style="width:' + wBudget + 'px" class="level1" align="right">' + arrData[2][i] + '</div>';
			html += '<div style="width:' + wSTotal + 'px" class="level1" align="right">' + arrData[3][i] + '</div>';
			html += '<div style="width:' + wTax + 'px" class="level1" align="right">' + arrData[4][i] + '</div>';
			html += '<div style="width:' + wTotal + 'px" class="level1" align="right">' + totSubPO + '</div>';
			html += '<div style="width:' + wSTotal + 'px" class="level1" align="right">' + arrData[5][i] + '</div>';
			html += '<div style="width:' + wTax + 'px" class="level1" align="right">' + arrData[6][i] + '</div>';
			html += '<div style="width:' + wTotal + 'px" class="level1" align="right">' + totSubBill + '</div>';
			html += '</div>';
			html += '<ul>';

		}
		if (lastLocID != thisLocID || i == 0){
			var totLocPO = addCommas((parseFloat(arrData[10][i].replace(/\,/g,"")) + parseFloat(arrData[11][i].replace(/\,/g,""))).toFixed(2));
			if (totLocPO == 'NaN'){totLocPO=''};
			var totLocBill = addCommas((parseFloat(arrData[12][i].replace(/\,/g,"")) + parseFloat(arrData[13][i].replace(/\,/g,""))).toFixed(2));
			if (totLocBill == 'NaN'){totLocBill=''};
			html += '<li>';
			html += '<div class="line" style="width:1214px;height:15px;">';
			html += '<div style="width:486px" class="level2">' + arrData[8][i] + '</div>';
			html += '<div style="width:' + wBudget + 'px" class="level2" align="right">' + arrData[9][i] + '</div>';
			html += '<div style="width:' + wSTotal + 'px" class="level2" align="right">' + arrData[10][i] + '</div>';
			html += '<div style="width:' + wTax + 'px" class="level2" align="right">' + arrData[11][i] + '</div>';
			html += '<div style="width:' + wTotal + 'px" class="level2" align="right">' + totLocPO + '</div>';
			html += '<div style="width:' + wSTotal + 'px" class="level2" align="right">' + arrData[12][i] + '</div>';
			html += '<div style="width:' + wTax + 'px" class="level2" align="right">' + arrData[13][i] + '</div>';
			html += '<div style="width:' + wTotal + 'px" class="level2" align="right">' + totLocBill + '</div>';
			html += '</div>';
			html += '<ul>';
		}
		if (lastProjID != thisProjID || i == 0){

			var arrProjDates = nlapiLookupField('job',thisProjID,['startdate','enddate', 'comments']);
            var projStart = arrProjDates['startdate'];
            var projEnd = arrProjDates['enddate'];
            var projMemo = arrProjDates['comments'];
			var date = new Date();
			if (date < nlapiStringToDate(projStart)){ 
				var imgSRC = '/core/media/media.nl?id=38236&c=1337135&h=726488ea26799444342a';
			}
			else if (date > nlapiStringToDate(projEnd) && projEnd != ''){ 
				var imgSRC = '/core/media/media.nl?id=38238&c=1337135&h=fa7e34db5ea884df6b77 ';
			}
			else if ((date > nlapiStringToDate(projStart) && date < nlapiStringToDate(projEnd)) || (date > nlapiStringToDate(projStart) && projEnd == '')){ 
				var imgSRC = '/core/media/media.nl?id=38237&c=1337135&h=9831fdeaca684eb89530';
			}
			else{
				var imgSRC = '/core/media/media.nl?id=38239&c=1337135&h=4bc57aaf20cc0189b3f8 ';
			}
			
			var totProjPO = addCommas((parseFloat(arrData[17][i].replace(/\,/g,"")) + parseFloat(arrData[18][i].replace(/\,/g,""))).toFixed(2));
			if (totProjPO == 'NaN'){totProjPO=''};
			var totProjBill = addCommas((parseFloat(arrData[19][i].replace(/\,/g,"")) + parseFloat(arrData[20][i].replace(/\,/g,""))).toFixed(2));
			if (totProjBill == 'NaN'){totProjBill=''};
			html += '<li>';
			html += '<div class="line" style="width:1196px;height:15px;">';
			html += '<div style="width:13px" class="empty" align="right"><img src="' + imgSRC + '" alt="" height="13" width="13" /></div>';
        	html += '<div style="width:454px" class="level3"><a title="' + projMemo + '" class="tree" href="/app/common/entity/custjob.nl?id=' + arrData[14][i] + '" target="_blank">' + arrData[15][i].substring(0,70) + '</a></div>';
			html += '<div style="width:' + wBudget + 'px" class="level3" align="right">' + arrData[16][i] + '</div>';
			html += '<div style="width:' + wSTotal + 'px" class="level3" align="right">' + arrData[17][i] + '</div>';
			html += '<div style="width:' + wTax + 'px" class="level3" align="right">' + arrData[18][i] + '</div>';
			html += '<div style="width:' + wTotal + 'px" class="level3" align="right">' + totProjPO + '</div>';
			html += '<div style="width:' + wSTotal + 'px" class="level3" align="right">' + arrData[19][i] + '</div>';
			html += '<div style="width:' + wTax + 'px" class="level3" align="right">' + arrData[20][i] + '</div>';
			html += '<div style="width:' + wTotal + 'px" class="level3" align="right">' + totProjBill + '</div>';
			html += '</div>';
			html += '<ul>';
		}

		html += '<li>';
		

		var arrBills = new Array(6);
		for(var a=0; a<6; a++){
			arrBills[a] = new Array();
		}
		var sumPOBills = 0;
		var sumPOBillsTax = 0;
		var sumPOBillsTotal = 0;
    	for(var j=0; searchBill != null && j<searchBill.length; j++){
    		var stPONbr  = searchBill[j].getValue('createdfrom',null,'GROUP');
    		if(stPONbr == arrData[21][i]){
    			
	    		var stBillId  = searchBill[j].getValue('internalid',null,'GROUP');
	    		var stBillNbr  = searchBill[j].getValue('tranid',null,'GROUP');
	    		var stBillMemo  = searchBill[j].getValue('memomain',null,'GROUP');
	    		var stBillAmount  = searchBill[j].getValue('amount',null,'SUM');
	    		var stBillAmountTax  = Math.abs(searchBill[j].getValue('taxamount',null,'SUM'));
	    		var stBillAmountTotal  = parseFloat(stBillAmount) + parseFloat(stBillAmountTax);
	    		
	    		arrBills[0].push(stBillId);
	    		arrBills[1].push(stBillNbr);
	    		arrBills[2].push(stBillMemo);
	    		arrBills[3].push(addCommas(parseFloat(stBillAmount).toFixed(2)));
	    		arrBills[4].push(addCommas(parseFloat(stBillAmountTax).toFixed(2)));
	    		arrBills[5].push(addCommas(parseFloat(stBillAmountTotal).toFixed(2)));
	    		sumPOBills += parseFloat(stBillAmount);
	    		sumPOBillsTax += parseFloat(stBillAmountTax);
	    		sumPOBillsTotal += parseFloat(stBillAmountTotal);
    		}
    	}
    	sumPOBills = parseFloat(sumPOBills).toFixed(2);
    	sumPOBillsTax = parseFloat(sumPOBillsTax).toFixed(2);
    	sumPOBillsTotal = parseFloat(sumPOBillsTotal).toFixed(2);
    	arrData[23][i] = sumPOBills;
    	arrData[24][i] = sumPOBillsTax;
    	
		html += '<div class="line" style="width:1181px;height:15px;">';
        html += '<div style="width:566px" class="level4"><a title="' + arrData[25][i] + '" class="tree" href="/app/accounting/transactions/purchord.nl?id=' + arrData[21][i] + '" target="_blank">' + 'PO# ' + arrData[22][i] + '</a></div>';
		var totPO = addCommas((parseFloat(arrData[27][i].replace(/\,/g,"")) + parseFloat(arrData[28][i].replace(/\,/g,""))).toFixed(2));
		if (totPO == 'NaN'){totPO=''};
		html += '<div style="width:' + wSTotal + 'px" class="level4" align="right">' + arrData[27][i] + '</div>';
		html += '<div style="width:' + wTax + 'px" class="level4" align="right">' + arrData[28][i] + '</div>';
		html += '<div style="width:' + wTotal + 'px" class="level4" align="right">' + totPO + '</div>';
		html += '<div style="width:' + wSTotal + 'px" class="level4" align="right">' + sumPOBills + '</div>';
		html += '<div style="width:' + wTax + 'px" class="level4" align="right">' + sumPOBillsTax + '</div>';
		html += '<div style="width:' + wTotal + 'px" class="level4" align="right">' + sumPOBillsTotal + '</div>';
		html += '</div>';
		
		
		if (arrBills[0].length > 0){
			html += '<ul>';
			for (var j=0; j<arrBills[0].length; ++j) {
				sumPOBillsTotal = parseFloat(arrBills[3][j]) + parseFloat(arrBills[4][j]);
	    		html += '<li>';
	    		html += '<div class="line" style="width:1168px;height:15px;">';
		        html += '<div style="width:850px" class="level5"><a title="' + arrBills[2][j] + '" class="tree" href="/app/accounting/transactions/vendbill.nl?id=' + arrBills[0][j] + '" target="_blank">' + 'Bill# ' + arrBills[1][j] + '</a></div>';
				html += '<div style="width:' + wSTotal + 'px" class="level5" align="right">' + arrBills[3][j] + '</div>';
				html += '<div style="width:' + wTax + 'px" class="level5" align="right">' + arrBills[4][j] + '</div>';
				html += '<div style="width:' + wTotal + 'px" class="level5" align="right">' + arrBills[5][j] + '</div>';
		    	html += '</div>';
		    	html += '</li>';
	    	}
	    	html += '</ul>';
		}
		

        html += '</li>';
        
        
		if (nextProjID != thisProjID || (i+1) == arrData[0].length){
	    	html += '</ul>';
	        html += '</li>';
		}
		
		if (nextLocID != thisLocID || (i+1) == arrData[0].length){
	    	html += '</ul>';
	        html += '</li>';
		}

		if (nextSubID != thisSubID || (i+1) == arrData[0].length){
	    	html += '</ul>';
	        html += '</li>';
		}

	}
    html += '</ul>';
    html += '</ul>';
    
	var stTotalBudget = 0;
	var stTotalPOs = 0;
	var stTotalPOsTax = 0;
	var stTotalBills = 0;
	var stTotalBillsTax = 0;
	
	for(var j=0; searchBudgetSub != null && j<searchBudgetSub.length; j++){
		stTotalBudget += parseFloat(searchBudgetSub[j].getValue('custrecord_clgx_budget_amount',null,'SUM'));
	}
	for(var j=0; searchPOSub != null && j<searchPOSub.length; j++){
		stTotalPOs += parseFloat(searchPOSub[j].getValue('amount',null,'SUM'));
		stTotalPOsTax += parseFloat(searchPOSub[j].getValue('taxamount',null,'SUM'));
	}

	for(var j=0; searchBillSub != null && j<searchBillSub.length; j++){
		stTotalBills += parseFloat(searchBillSub[j].getValue('amount',null,'SUM'));
		stTotalBillsTax += parseFloat(searchBillSub[j].getValue('taxamount',null,'SUM'));
	}
    
    html += '</div>';

    html += '<div class="line" style="width:1285px;height:15px;">';
	html += '<div style="width:559px" class="totals">TOTALS</div>';
    html += '<div style="width:' + wBudget + 'px" class="subheader" align="right">' + addCommas(parseFloat(stTotalBudget).toFixed(2)) + '</div>';
	html += '<div style="width:' + wSTotal + 'px" class="subheader" align="right">' + addCommas(parseFloat(stTotalPOs).toFixed(2)) + '</div>';
	html += '<div style="width:' + wTax + 'px" class="subheader" align="right">&nbsp;</div>';
	html += '<div style="width:' + wTotal + 'px" class="subheader" align="right">&nbsp;</div>';
	html += '<div style="width:' + wSTotal + 'px" class="subheader" align="right">' + addCommas(parseFloat(stTotalBills).toFixed(2)) + '</div>';
	html += '<div style="width:' + wTax + 'px" class="subheader" align="right">&nbsp;</div>';
	html += '<div style="width:' + wTotal + 'px" class="subheader" align="right">&nbsp;</div>';
	html += '</div>';
	
    html += '</div>';
	html += '</div>';
    
	/*
	var stCSV = 'Subsidiary_ID|Subsidiary_Name|Subsidiary_Budget|Subsidiary_PO|Subsidiary_POTax|Subsidiary_Bill|Subsidiary_BillTax|Location_ID|Location_Name|Location_Budget|Location_PO|Location_POTax|Location_Bill|Location_BillTax|Project_ID|Project_Name|Project_Budget|Project_PO|Project_POTax|Project_Bill|Project_BillTax|PO_ID|PO_Nbr|PO_Bill|PO_BillTax|PO_Memo|PO_Date|PO_Amount|PO_Tax\n';
	
	for(var b=0; searchPO != null && b<searchPO.length; b++){
		for(var a=0; a<29; a++){
			stCSV += arrData[a][b] + '|';
		}
		stCSV += '\n';
	}

	var fileCSV = nlapiCreateFile('capex_report.csv', 'CSV', stCSV);
	fileCSV.setFolder(66212);
	var fileId = nlapiSubmitFile(fileCSV);
	
	//core/media/media.nl?id=38974&c=1337135&h=bf119f8d5b7b88935ac9&_xt=.csv
	*/
	
		
		
    htmlReport.setDefaultValue(html);
	//form.addSubmitButton('Submit');
	response.writePage( form );


//---------- End Section 1 ------------------------------------------------------------------------------------------------


nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------FINISHED---------------------|');  
} 
catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
	if (error.getDetails != undefined){
	    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	    throw error;
	}
	else{
	    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	    throw nlapiCreateError('99999', error.toString());
	}
} // End Catch Errors Section ------------------------------------------------------------------------------------------
}



function addCommas(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function inArray(val, arr){	
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}