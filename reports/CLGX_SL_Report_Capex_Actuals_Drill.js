nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Report_Capex_Actuals_Drill.js
//	Script Name:	CLGX_SL_Report_Capex_Actuals_Drill
//	Script Id:		customscript_clgx_sl_rep_capex_act_drill
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		3/30/2012
//-------------------------------------------------------------------------------------------------

function suitelet_rep_capex_actuals_drill(request, response){
	try {
		nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------STARTED---------------------|');

		var projId = request.getParameter('projid');
		var context = request.getParameter('context');
		var postPeriod = request.getParameter('period');
		var reqStart = request.getParameter('reqStart');
		var reqEnd = request.getParameter('reqEnd');
		
		var arrPeriod2011 = new Array (21,22,23,25,26,27,29,30,31,33,34,35);
		var arrPeriod2012 = new Array (39,40,41,43,44,45,47,48,49,51,52,53);
		
		var arrProj= nlapiLookupField('job',projId,['entityid','jobname']);
        var projNbr = arrProj['entityid'];
        var projName = arrProj['jobname'];
		
		var resultsTitle = 'Capex Report - Bills & Journals for ' + projNbr + ' | ' + postPeriod + ' | ';
		
		var arrColumns = new Array();
		var arrFilters = new Array();	

		if (projId != ''){
			arrFilters.push(new nlobjSearchFilter('custcol_clgx_journal_project_id',null,'is',projId));
		}
		
		if (reqStart != 'undefined' && reqEnd != 'undefined'){ // if dates interval filter is asked
			arrFilters.push(new nlobjSearchFilter('trandate',null,'within',reqStart,reqEnd));
		}
		
		
		if(context == 'local'){
			var searchJournals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_loc_journ_dr', arrFilters, arrColumns); 
		}
		else{
			var searchJournals = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_glo_journ_dr', arrFilters, arrColumns); 
		}

		if(context == 'local'){
			var searchBills = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_loc_bills_dr', arrFilters, arrColumns); 
		}
		else{
			var searchBills = nlapiSearchRecord('transaction', 'customsearch_clgx_rep_capex_glo_bills_dr', arrFilters, arrColumns); 
		}
		

		
		var form = nlapiCreateForm(resultsTitle);
		var groupResults = form.addFieldGroup('groupResults', 'Results',null);
		var htmlReport = form.addField('custpage_clgx_html','inlinehtml', null, null, 'groupResults');
			htmlReport.setLayoutType('normal','startcol');
		
		
		arrBillProj = new Array();
		arrBillId = new Array();
		arrBillNbr = new Array();
		arrBillPost = new Array();
		arrBillAmount = new Array();

		for(var i=0; searchBills != null && i<searchBills.length; i++){
			var thisPeriodID = searchBills[i].getValue('postingperiod',null,'GROUP');
			if((postPeriod == '' || thisPeriodID == postPeriod) || (postPeriod == '37' && inArray(thisPeriodID,arrPeriod2012)) || (postPeriod == '19' && inArray(thisPeriodID,arrPeriod2011))){
				arrBillProj.push(searchBills[i].getText('custcol_clgx_journal_project_id',null,'GROUP'));
				arrBillId.push(searchBills[i].getValue('internalid',null,'GROUP'));
				arrBillNbr.push(searchBills[i].getValue('tranid',null,'GROUP'));
				arrBillPost.push(searchBills[i].getText('postingperiod',null,'GROUP'));
				arrBillAmount.push(searchBills[i].getValue('amount',null,'SUM'));
			}
		}
		
		
		arrJournalType = new Array();
		arrJournalProj = new Array();
		arrJournalId = new Array();
		arrJournalNbr = new Array();
		arrJournalPost = new Array();
		arrJournalAmount = new Array();
		
		for(var i=0; searchJournals != null && i<searchJournals.length; i++){
			var thisPeriodID = searchJournals[i].getValue('postingperiod',null,'GROUP');
			if((postPeriod == '' || thisPeriodID == postPeriod) || (postPeriod == '37' && inArray(thisPeriodID,arrPeriod2012)) || (postPeriod == '19' && inArray(thisPeriodID,arrPeriod2011))){
				arrJournalType.push(searchJournals[i].getText('type',null,'GROUP'));
				arrJournalProj.push(searchJournals[i].getText('custcol_clgx_journal_project_id',null,'GROUP'));
				arrJournalId.push(searchJournals[i].getValue('internalid',null,'GROUP'));
				arrJournalNbr.push(searchJournals[i].getValue('tranid',null,'GROUP'));
				arrJournalPost.push(searchJournals[i].getText('postingperiod',null,'GROUP'));
				arrJournalAmount.push(searchJournals[i].getValue('amount',null,'SUM'));
			}
		}
		
		
		var html = '';
	    html = '<link rel="stylesheet" href="/core/media/media.nl?id=41788&c=1337135&h=b09682241b6cd2f96ce1&_xt=.css" />';
	    html += '<script src="/core/media/media.nl?id=41787&c=1337135&h=2b829df96c50f1fed6e5&_xt=.js" type="text/javascript"></script>';
	
	    // Bills table -------------------------------------------------------------------------
	    html += '<table id="myTable" bgcolor="#eeeeee" border="0">';
			html += '<tr>';
			    html += '<td class="col5" align="center" colspan="4">BILLS</td>';
		    html += '</tr>';
	    
		    html += '<tr>';
			    html += '<td class="header">Project</td>';
			    html += '<td align="right" class="header">Period</td>';
			    html += '<td class="header">Bills</td>';
			    html += '<td align="right" class="header">Amount</td>';
		    html += '</tr>';
		    
		for(var i=0; i<arrBillId.length; i++){
		    html += '<tr>';
		    	html += '<td align="right" class="col0">' + arrBillProj[i] + '</td>';
		    	 html += '<td align="right" class="col0">' + arrBillPost[i] + '</td>';
		    	 html += '<td class="col0"><a class="report" href="/app/accounting/transactions/vendbill.nl?id=' + arrBillId[i] + '" target="_blank">' + arrBillNbr[i] + '</a></td>';
			    html += '<td align="right" class="col5">' + addCommas(parseFloat(arrBillAmount[i]).toFixed(2)) + '</td>';
		    html += '</tr>';
	    }
	
	    html += '<tr>';
		    html += '<td class="totals" align="right" colspan="3">Total Bills</td>';
		    html += '<td class="totals" align="right">' + addCommas(sumArray(arrBillAmount)) + '</td>';
	    html += '</tr>';
	
	    var imgSRC = '/core/media/media.nl?id=38239&c=1337135&h=4bc57aaf20cc0189b3f8';
	    
		    html += '<tr>';
			    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
			    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
			    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
			    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '</tr>';
	    
	    html += '</table><br/>';
	 // Bills table -------------------------------------------------------------------------
	    
	    // Journals table -------------------------------------------------------------------------
	    html += '<table id="myTable2" bgcolor="#eeeeee" border="0">';
			html += '<tr>';
				html += '<td class="col5" align="center" colspan="5">JOURNAL ENTRIES</td>';
	    	html += '</tr>';
		    html += '<tr>';
			    html += '<td class="header" align="left">Project</td>';
			    html += '<td class="header">Type</td>';
			    html += '<td align="right" class="header">Period</td>';
			    html += '<td class="header">IDs</td>';
			    html += '<td align="right" class="header">Amount</td>';
		    html += '</tr>';
		for(var i=0; i<arrJournalId.length; i++){
		    html += '<tr>';
		    	html += '<td align="right" class="col0">' + arrJournalProj[i] + '</td>';  
		    	html += '<td align="right" class="col0">' + arrJournalType[i] + '</td>'; 
		        html += '<td align="right" class="col0">' + arrJournalPost[i] + '</td>';
		        html += '<td class="col0"><a class="report" href="/app/accounting/transactions/journal.nl?id=' + arrJournalId[i] + '" target="_blank">' + arrJournalNbr[i] + '</a></td>';
		        html += '<td align="right" class="col5">' + addCommas(parseFloat(arrJournalAmount[i]).toFixed(2)) + '</td>';
		    html += '</tr>';
	    }
	
	    html += '<tr>';
		    html += '<td class="totals" align="right" colspan="4">Total Journals</td>';
		    html += '<td class="totals" align="right">' + addCommas(sumArray(arrJournalAmount)) + '</td>';
	    html += '</tr>';
	
	    var imgSRC = '/core/media/media.nl?id=38239&c=1337135&h=4bc57aaf20cc0189b3f8';
	    
		    html += '<tr>';
			    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
			    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
			    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
			    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
			    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '</tr>';
	    
	    html += '</table><br/>';
	 // Journals table -------------------------------------------------------------------------
	    
	    var sumTotal = parseFloat(sumArray(arrBillAmount)) + parseFloat(sumArray(arrJournalAmount));
	    //var sumTotal = parseFloat(sumArray(arrJournalAmount).replace(/\,/g,""));
	    // TOTALS table -------------------------------------------------------------------------
	    html += '<table id="myTable" bgcolor="#eeeeee" border="0">';
	    html += '<tr>';
		    html += '<td class="totals" align="right">TOTAL</td>';
		    html += '<td class="totals" align="right">' + addCommas(parseFloat(sumTotal).toFixed(2)) + '</td>';
	    html += '</tr>';
	
	    var imgSRC = '/core/media/media.nl?id=38239&c=1337135&h=4bc57aaf20cc0189b3f8';
	    
		    html += '<tr>';
			    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="235" /></td>';
			    html += '<td class="empty"><img src="' + imgSRC + '" alt="" height="0" width="75" /></td>';
		    html += '</tr>';
	    
	    html += '</table>';
	 // Journals table -------------------------------------------------------------------------
	    
	    
	    
	    
	    html += '<script type="text/javascript">';
	        html += 'addTableRolloverEffect("myTable","tableRollOverEffect1","tableRowClickEffect1");';
	        html += 'addTableRolloverEffect("myTable2","tableRollOverEffect1","tableRowClickEffect1");';
	    html += '</script>';
	    
	    htmlReport.setDefaultValue(html);
		
		//form.addSubmitButton('Submit');
		response.writePage( form );
	
//---------- End Section 1 ------------------------------------------------------------------------------------------------


nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------FINISHED---------------------|');  
} 
catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
	if (error.getDetails != undefined){
	    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	    throw error;
	}
	else{
	    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	    throw nlapiCreateError('99999', error.toString());
	}
} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function addCommas(nStr){
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

function sumArray (arr){
	var sum = 0;
	for (var i=0; i<arr.length; ++i) {
		sum += parseFloat(arr[i]);
	}
	return parseFloat(sum).toFixed(2);
}

//check if value is in the array
function inArray(val, arr){	
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(parseInt(val) == parseInt(arr[i])){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}
