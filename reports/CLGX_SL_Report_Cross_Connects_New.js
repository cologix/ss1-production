nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Report_Cross_Connects.js
//	Script Name:	CLGX_SL_Report_Cross_Connects
//	Script Id:		customscript_clgx_sl_rep_cross_connects
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/13/2012
//-------------------------------------------------------------------------------------------------

function suitelet_rep_cross_connects(request, response){
try {
	
	var currentLocation = '';
	var currentCustomer = '';
	var currentService = '';
	var currentXC = '';
	
	var locationID = request.getParameter('locationID');
	if (locationID == null){
		locationID = 0;
	}
	var customerID = request.getParameter('customerID');
	if (customerID == null){
		customerID = 0;
	}
	else{
		var arrCustomerDetails = nlapiLookupField('customer',customerID,['companyname']);
        currentCustomer = ' / ' + arrCustomerDetails['companyname'];
	}
	
	var serviceID = request.getParameter('serviceID');
	if (serviceID == null){
		serviceID = 0;
	}
	else{
		var arrServiceDetails = nlapiLookupField('job',serviceID,['entityid']);
        currentService = ' / ' + arrServiceDetails['entityid'];
	}
	
	var xcID = request.getParameter('xcID');
	if (xcID == null){
		xcID = 0;
	}
	else{
		var arrXCDetails = nlapiLookupField('customrecord_cologix_crossconnect',xcID,['name']);
		currentXC = '' + currentService + ' / ' +arrXCDetails['name'];
	}

	var form = nlapiCreateForm('Service Inventory Portal');
	var htmlReport = form.addField('custpage_clgx_html','inlinehtml', null, null, null);
	
	var objFile = nlapiLoadFile(211708);
	var dataHTML = objFile.getValue();
	dataHTML = dataHTML.replace(new RegExp('{stLocations}','g'),locationsData());
	dataHTML = dataHTML.replace(new RegExp('{currentLocation}','g'),returnCurrentLocation(locationID));
	dataHTML = dataHTML.replace(new RegExp('{stCustomers}','g'),customersData(locationID));
	dataHTML = dataHTML.replace(new RegExp('{currentCustomer}','g'),currentCustomer);
	dataHTML = dataHTML.replace(new RegExp('{stInterconnections}','g'),interconnectionsData(locationID,customerID));
	dataHTML = dataHTML.replace(new RegExp('{currentService}','g'),currentService);
	dataHTML = dataHTML.replace(new RegExp('{stXCs}','g'),xcsData(locationID,customerID,serviceID));
	dataHTML = dataHTML.replace(new RegExp('{currentXC}','g'),currentXC);
	dataHTML = dataHTML.replace(new RegExp('{stXCsPaths}','g'),pathsData(locationID,customerID,serviceID,xcID));

	var usageConsumtion = 1000 - parseInt(nlapiGetContext().getRemainingUsage());
	dataHTML = dataHTML.replace(new RegExp('{usageConsumtion}','g'),usageConsumtion);
	
	htmlReport.setDefaultValue(dataHTML);
	//form.addSubmitButton('Submit');
	response.writePage( form );
	
//---------- End Section 1 ------------------------------------------------------------------------------------------------

nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------FINISHED---------------------|');  
} 
catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
	if (error.getDetails != undefined){
	    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	    throw error;
	}
	else{
	    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	    throw nlapiCreateError('99999', error.toString());
	}
} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function locationsData(){	

	var searchLocations = nlapiSearchRecord('salesorder', 'customsearch_clgx_rep_xcs_locations', null, null);

	var searchXCs = nlapiSearchRecord('salesorder', 'customsearch_clgx_rep_xcs_xcs_location', null, null);
	
	
	if(searchLocations != null){
		var stLocations = 'var locationsData = {"total":' + searchLocations.length +  ',"rows":[';
	}
	else{
		var stLocations = 'var locationsData = {"total":0,"rows":[';
	}
	
	var sumCustomers = 0;
	var sumSOs = 0;
	var sumQty = 0;
	var sumXCs = 0;
	for ( var i = 0; searchLocations != null && i < searchLocations.length; i++ ) {
		var searchLocation = searchLocations[i];
		
		var sumXCsPerLocation = 0;
		for ( var j = 0; searchXCs != null && j < searchXCs.length; j++ ) {
			var searchXC = searchXCs[j];
			if(searchXC.getValue('location', null, 'GROUP') == searchLocation.getValue('location', null, 'GROUP')){
				sumXCsPerLocation = searchXC.getValue('internalid', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', 'COUNT');
			}
		}
		
		stLocations += '{id:"' + searchLocation.getValue('location', null, 'GROUP') + 
						'",location:"<a class=report href=/app/site/hosting/scriptlet.nl?script=140&deploy=1&locationID=' + searchLocation.getValue('location', null, 'GROUP') + '>' + searchLocation.getValue('locationnohierarchy', null, 'GROUP') + '</a>' + 
						'",customers:"' + searchLocation.getValue('entity', null, 'COUNT') + 
						'",sos:"' + searchLocation.getValue('internalid', null, 'COUNT') + 
						'",qty:"' + searchLocation.getValue('custcol_clgx_qty2print', null, 'SUM') + 
						'",xcs:"' + sumXCsPerLocation +  '"},';
		
		sumCustomers += parseInt(searchLocation.getValue('entity', null, 'COUNT'));
		sumSOs += parseInt(searchLocation.getValue('internalid', null, 'COUNT'));
		sumQty += parseInt(searchLocation.getValue('custcol_clgx_qty2print', null, 'SUM'));
		sumXCs += parseInt(sumXCsPerLocation);
	}
	var strLen = stLocations.length;
	if (searchLocations != null){
		stLocations = stLocations.slice(0,strLen-1);
	}
    stLocations += '],"footer":[';
    stLocations += '{"customers":' + sumCustomers +  ',"sos":' + sumSOs +  ',"qty":' + sumQty +  ',"xcs":' + sumXCs +  ',"location":"Totals:"}';
    stLocations += ']};';
	
    return stLocations;
}

function returnCurrentLocation(locationID){	
	switch(locationID) {
	case '29':
		currentLocation = ' - 421 West Church Street';
		break;
	case '6':
		currentLocation = ' - 151 Front Street West';
		break;
	case '18':
		currentLocation = ' - 151 Front Street West - Ste 822';
		break;
	case '13':
		currentLocation = ' - 156 Front Street West';
		break;
	case '15':
		currentLocation = ' - 905 King Street West';
		break;
	case '14':
		currentLocation = ' - Barrie Office';
		break;
	case '1':
		currentLocation = ' - Cologix HQ';
		break;
	case '2':
		currentLocation = ' - Dallas Infomart';
		break;
	case '17':
		currentLocation = ' - Dallas Infomart - 2nd Floor';
		break;
	case '7':
		currentLocation = ' - Harbour Centre';
		break;
	case '16':
		currentLocation = ' - Minnesota Gateway';
		break;
	case '5':
		currentLocation = ' - MTL1';
		break;
	case '8':
		currentLocation = ' - MTL2';
		break;
	case '9':
		currentLocation = ' - MTL3';
		break;
	case '10':
		currentLocation = ' - MTL4';
		break;
	case '11':
		currentLocation = ' - MTL5';
		break;
	case '12':
		currentLocation = ' - MTL6';
		break;
	default:
		currentLocation = '';
	}
	return currentLocation;
}

function customersData(locationID){	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters[0] = new nlobjSearchFilter('location',null,'anyof',locationID);
	var searchCustomers = nlapiSearchRecord('salesorder', 'customsearch_clgx_rep_xcs_customers', arrFilters, arrColumns);
	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters[0] = new nlobjSearchFilter('location',null,'anyof',locationID);
	var searchXCs = nlapiSearchRecord('salesorder', 'customsearch_clgx_rep_xcs_xcs_customer', null, null);
	
	if(searchCustomers != null){
		var stCustomers = 'var customersData = {"total":' + searchCustomers.length +  ',"rows":[';
	}
	else{
		var stCustomers = 'var customersData = {"total":0,"rows":[';
	}
	var sumSOs = 0;
	var sumQty = 0;
	var sumXCs = 0;
	for ( var i = 0; searchCustomers != null && i < searchCustomers.length; i++ ) {
		var searchCustomer = searchCustomers[i];
		
		var sumXCsPerCustomer = 0;
		for ( var j = 0; searchXCs != null && j < searchXCs.length; j++ ) {
			var searchXC = searchXCs[j];
			if(searchXC.getValue('internalid', 'customerMain', 'GROUP') == searchCustomer.getValue('entity', null, 'GROUP')){
				sumXCsPerCustomer = searchXC.getValue('internalid', 'CUSTRECORD_XCONNECT_SERVICE_ORDER', 'COUNT');
			}
		}
		
		var err = '';
		if((parseInt(sumXCsPerCustomer) - parseInt(searchCustomer.getValue('custcol_clgx_qty2print', null, 'SUM'))) != 0){
			err = 'err';
		}
		
		stCustomers += '{id:"' + searchCustomer.getValue('entity', null, 'GROUP') + 
						'",customer:"<a class=report href=/app/site/hosting/scriptlet.nl?script=140&deploy=1&locationID=' + locationID + '&customerID=' + searchCustomer.getValue('entity', null, 'GROUP') + '>' + searchCustomer.getText('entity', null, 'GROUP') + '</a>' + 
						'",SOs:"' + searchCustomer.getValue('internalid', null, 'COUNT') + 
						'",qty:"' + searchCustomer.getValue('custcol_clgx_qty2print', null, 'SUM') + 
						'",xcs:"' + sumXCsPerCustomer + 
						'",err:"' + err +  '"},';
		sumSOs += parseInt(searchCustomer.getValue('internalid', null, 'COUNT'));
		sumQty += parseInt(searchCustomer.getValue('custcol_clgx_qty2print', null, 'SUM'));
		sumXCs += parseInt(sumXCsPerCustomer);
	}
	var strLen = stCustomers.length;
	if (searchCustomers != null){
		stCustomers = stCustomers.slice(0,strLen-1);
	}
	
	stCustomers += '],"footer":[';
	stCustomers += '{"SOs":' + sumSOs +  ',"qty":' + sumQty +  ',"xcs":' + sumXCs +  ',"customer":"Totals:"}';
	stCustomers += ']};';

    return stCustomers;
}

function interconnectionsData(locationID,customerID){	


	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters[0] = new nlobjSearchFilter('entity',null,'anyof',customerID);
	arrFilters[1] = new nlobjSearchFilter('location',null,'anyof',locationID);
	var searchSOs = nlapiSearchRecord('salesorder', 'customsearch_clgx_rep_xcs_sos', arrFilters, arrColumns);

	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters[0] = new nlobjSearchFilter('entity',null,'anyof',customerID);
	arrFilters[1] = new nlobjSearchFilter('location',null,'anyof',locationID);
	var searchInterconnections = nlapiSearchRecord('salesorder', 'customsearch_clgx_rep_xcs_intercon', arrFilters, arrColumns);

	if(searchInterconnections != null || searchSOs != null){
		var stInterconnections = 'var interconnectionsData = {"total":' + searchInterconnections.length +  ',"rows":[';
	}
	else{
		var stInterconnections = 'var interconnectionsData = {"total":0,"rows":[';
	}
	var sumQty = 0;
	var sumXCs = 0;
	//var sumqty = 0;
	
	for ( var i = 0; searchSOs != null && i < searchSOs.length; i++ ) {
		var searchSO = searchSOs[i];
		var sumqty = searchSO.getValue('custcol_clgx_qty2print', null, 'SUM');
		var soID = searchSO.getValue('internalid', null, 'GROUP');
		
		var arrColumns = new Array();
		var arrFilters = new Array();
		arrFilters[0] = new nlobjSearchFilter('entity',null,'anyof',customerID);
		arrFilters[1] = new nlobjSearchFilter('location',null,'anyof',locationID);
		arrFilters[2] = new nlobjSearchFilter('internalid',null,'anyof',soID);
		var searchInterconnections = nlapiSearchRecord('salesorder', 'customsearch_clgx_rep_xcs_intercon', arrFilters, arrColumns);

		
		
		for ( var j = 0; searchInterconnections != null && j < searchInterconnections.length; j++ ) {
			var searchInterconnection = searchInterconnections[j];
	
			var currentServiceID = searchInterconnection.getValue('custcol_clgx_so_col_service_id', null, 'GROUP');
			var currentSOID = searchInterconnection.getValue('internalid', null, 'GROUP');
			
			if (currentServiceID != null && currentServiceID != ''){
				var myRecord = nlapiLoadRecord('job', parseInt(currentServiceID), null, null);
				var stNbrItems = myRecord.getLineItemCount('recmachcustrecord_cologix_xc_service');
			}
			else{
				var stNbrItems = 0;
			}
			
			var error = '';
			var qty = searchInterconnection.getValue('custcol_clgx_qty2print', null, 'SUM');
			if(stNbrItems != sumqty){
				error = 'Error';
			}
	
			stInterconnections += '{id:"' + searchInterconnection.getValue('custcol_clgx_so_col_service_id', null, 'GROUP') + 
							'",sonbr:"' + '<a class=report href=/app/accounting/transactions/salesord.nl?id=' + searchInterconnection.getValue('internalid', null, 'GROUP') + ' target=_blank>' + searchInterconnection.getValue('number', null, 'GROUP') + '</a>' +
							'",item:"' + searchInterconnection.getText('item', null, 'GROUP') + 
							'",qty:"' + qty + 
							'",sumqty:"' + sumqty + 
							'",service:"<a class=report href=/app/site/hosting/scriptlet.nl?script=140&deploy=1&locationID=' + locationID + '&customerID=' + customerID + '&serviceID=' + currentServiceID + '>' + searchInterconnection.getText('custcol_clgx_so_col_service_id', null, 'GROUP') + '</a>' + 
							'",xcs:"' + stNbrItems +
							'",error:"' + error +  '"},';
			sumQty += parseInt(searchInterconnection.getValue('custcol_clgx_qty2print', null, 'SUM'));
			sumXCs += stNbrItems;
		}
	}
	var strLen = stInterconnections.length;
	if (searchInterconnections != null){
		stInterconnections = stInterconnections.slice(0,strLen-1);
	}

	stInterconnections += '],"footer":[';
	stInterconnections += '{"qty":' + sumQty +  ',"xcs":' + sumXCs +  ',"item":"Totals:"}';
	stInterconnections += ']};';
	
    return stInterconnections;
}

function xcsData(locationID,customerID,serviceID){	
	
	var stXCs = 'var xcsData = [';
	if (serviceID != null && serviceID != ''){

		var myRecord = nlapiLoadRecord('job', serviceID);
		var stNbrItems = myRecord.getLineItemCount('recmachcustrecord_cologix_xc_service');
	
		for (var j = 0; j < parseInt(stNbrItems); j++) {
			var id = myRecord.getLineItemValue('recmachcustrecord_cologix_xc_service', 'id', j + 1);
			var carrier = myRecord.getLineItemValue('recmachcustrecord_cologix_xc_service', 'custrecord_cologix_carrier_name_display', j + 1);
			var circuit = myRecord.getLineItemValue('recmachcustrecord_cologix_xc_service', 'custrecord_cologix_xc_carriercirct', j + 1);
	
			var arrXCDetails = nlapiLookupField('customrecord_cologix_crossconnect',id ,['name']);
			xcid = arrXCDetails['name'];
			
			stXCs += '{id:"' + id + 
						'",xcid:"<a class=report href=/app/site/hosting/scriptlet.nl?script=140&deploy=1&locationID=' + locationID + '&customerID=' + customerID + '&serviceID=' + serviceID + '&xcID=' + id + '>' + xcid + '</a>' + 
						'",carrier:"' + carrier + 
						'",circuit:"' + circuit +  '"},';
		}
		var strLen = stXCs.length;
		if (parseInt(stNbrItems) > 0){
			stXCs = stXCs.slice(0,strLen-1);
		}
	}
	
	stXCs += '];';
	
    return stXCs;
	
	/*
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters[0] = new nlobjSearchFilter('custrecord_cologix_xc_service',null,'anyof',serviceID);
	var searchXCs = nlapiSearchRecord('customrecord_cologix_crossconnect', 'customsearch_clgx_rep_xcs_xcs', arrFilters, arrColumns);

	var stXCs = 'var xcsData = [';
	for ( var i = 0; searchXCs != null && i < searchXCs.length; i++ ) {
		var searchXC = searchXCs[i];
		
		stXCs += '{id:"' + searchXC.getValue('internalid', null, null) + 
						'",xcid:"<a class=report href=/app/site/hosting/scriptlet.nl?script=140&deploy=1&locationID=' + locationID + '&customerID=' + customerID + '&serviceID=' + serviceID + '&xcID=' + searchXC.getValue('internalid', null, null) + '>' + searchXC.getValue('name', null, null) + '</a>' + 
						'",carrier:"' + searchXC.getText('custrecord_cologix_carrier_name', null, null) + 
						'",circuit:"' + searchXC.getValue('custrecord_cologix_xc_carriercirct', null, null) +  '"},';
	}
	var strLen = stXCs.length;
	if (searchXC != null){
		stXCs = stXCs.slice(0,strLen-1);
	}
	stXCs += '];';

    return stXCs;
    */

}

function pathsData(locationID,customerID,serviceID,xcID){	
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters[0] = new nlobjSearchFilter('custrecord_cologix_xcpath_crossconnect',null,'anyof',xcID);
	var searchPaths = nlapiSearchRecord('customrecord_cologix_xcpath', 'customsearch_clgx_rep_xcs_xcs_paths', arrFilters, arrColumns);

	var stPaths = 'var pathsData = [';
	for ( var i = 0; searchPaths != null && i < searchPaths.length; i++ ) {
		var searchPath = searchPaths[i];
		
		var spaceid = searchPath.getValue('custrecord_cologix_xcpath_space', null, null);
		
		if(spaceid == null || spaceid ==''){
	        var spaceName = '';
		}
		else{
			var arrSpaceDetails = nlapiLookupField('customrecord_cologix_space',spaceid,['altname']);
	        var spaceName = arrSpaceDetails['altname'];
		}
		
		stPaths += '{id:"' + searchPath.getValue('internalid', null, null) + 
						'",seq:"' + searchPath.getText('custrecord_cologix_xcpath_seg_seq', null, null) + 
						'",space:"' + searchPath.getText('custrecord_cologix_xcpath_space', null, null) + 
						'",name:"' + spaceName + 
						'",port:"' + searchPath.getValue('custrecord_cologix_xcpath_port', null, null) + 
						'",connector:"' + searchPath.getText('custrecord_cologix_xcpath_connectortype', null, null) + 
						'",cable:"' + searchPath.getValue('custrecord_cologix_xcpath_tiecable', null, null) +  '"},';
	}
	var strLen = stPaths.length;
	if (searchPaths != null){
		stPaths = stPaths.slice(0,strLen-1);
	}
	stPaths += '];';

    return stPaths;
}