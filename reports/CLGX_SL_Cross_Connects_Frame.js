//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Cross_Connects_Frame.js
//	Script Name:	CLGX_SL_Cross_Connects_Frame
//	Script Id:		customscript_clgx_sl_cross_connects_frame
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		04/03/2013
//-------------------------------------------------------------------------------------------------

function suitelet_serv_inv_frame (request, response){
	try {
		var formFrame = nlapiCreateForm('Service Portal');
		var fieldFrame = formFrame.addField('custpage_clgx_frame_html','inlinehtml', null, null, null);
		var frameHTML = '<iframe name="servicePortal" id="servicePortal" src="/app/site/hosting/scriptlet.nl?script=139&deploy=1 " height="700px" width="1320px" frameborder="0" scrolling="no"></iframe>';
		fieldFrame.setDefaultValue(frameHTML);
		response.writePage( formFrame );
} 
catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
	if (error.getDetails != undefined){
	    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	    throw error;
	}
	else{
	    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	    throw nlapiCreateError('99999', error.toString());
	}
} // End Catch Errors Section ------------------------------------------------------------------------------------------
}