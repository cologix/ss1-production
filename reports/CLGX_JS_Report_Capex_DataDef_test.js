{stReportData}

$(function(){
    $('#reportGrid').datagrid({
        width:1320,
        height:440,
        singleSelect:true,
        nowrap: false,
        striped: true,
        collapsible:false,
        sortName: 'case',
        sortOrder: 'desc',
        remoteSort: false,
        idField:'id',
        frozenColumns:[[
            {field:'status',title:'Status',width:50},
            {field:'subsidiary',title:'Subsidiary',width:120},
            {field:'facility',title:'Facility',width:80,sortable:true,hidden:true},
            {field:'projectID',title:'Project ID',width:300},

        ]],
        columns:[[
            {field:'description',title:'Description',width:300},
            {field:'capexcateg',title:'Capex Category',width:120},
            {field:'openPOs',title:'Open POs - Unbilled',width:150},

            {field:'actual2011',title:'2011 Actual',width:130},
            {field:'actual0112',title:'Jan-2012 Actual',width:130},
            {field:'actual0212',title:'Feb-2012 Actual',width:130},
            {field:'actual0312',title:'Mar-2012 Actual',width:130},
            {field:'actual0412',title:'Apr-2012 Actual',width:130},
            {field:'actual0512',title:'May-2012 Actual',width:130},
            {field:'actual0612',title:'Jun-2012 Actual',width:130},
            {field:'actual0712',title:'Jul-2012 Actual',width:130},
            {field:'actual0812',title:'Aug-2012 Actual',width:130},
            {field:'actual0912',title:'Sep-2012 Actual',width:130},
            {field:'actual1012',title:'Oct-2012 Actual',width:130},
            {field:'actual1112',title:'Nov-2012 Actual',width:130},
            {field:'actual1212',title:'Dec-2012 Actual',width:130},
            {field:'actual2012',title:'2012 Actual',width:130},
            {field:'actual0113',title:'Jan-2013 Actual',width:130},
            {field:'actual0213',title:'Feb-2013 Actual',width:130},
            {field:'actual0313',title:'Mar-2013 Actual',width:130},
            {field:'actual0413',title:'Apr-2013 Actual',width:130},
            {field:'actual0513',title:'May-2013 Actual',width:130},
            {field:'actual0613',title:'Jun-2013 Actual',width:130},
            {field:'actual0713',title:'Jul-2013 Actual',width:130},
            {field:'actual0813',title:'Aug-2013 Actual',width:130},
            {field:'actual0913',title:'Sep-2013 Actual',width:130},
            {field:'actual1013',title:'Oct-2013 Actual',width:130},
            {field:'actual1113',title:'Nov-2013 Actual',width:130},
            {field:'actual1213',title:'Dec-2013 Actual',width:130},
            {field:'actual2013',title:'2013 Actual',width:130},
            {field:'actual0114',title:'Jan-2014 Actual',width:130},
            {field:'actual0214',title:'Feb-2014 Actual',width:130},
            {field:'actual0314',title:'Mar-2014 Actual',width:130},
            {field:'actual0414',title:'Apr-2014 Actual',width:130},
            {field:'actual0514',title:'May-2014 Actual',width:130},
            {field:'actual0614',title:'Jun-2014 Actual',width:130},
            {field:'actual0714',title:'Jul-2014 Actual',width:130},
            {field:'actual0814',title:'Aug-2014 Actual',width:130},
            {field:'actual0914',title:'Sep-2014 Actual',width:130},
            {field:'actual1014',title:'Oct-2014 Actual',width:130},
            {field:'actual1114',title:'Nov-2014 Actual',width:130},
            {field:'actual1214',title:'Dec-2014 Actual',width:130},
            {field:'actual2014',title:'2014 Actual',width:130},
            {field:'actual0115',title:'Jan-2015 Actual',width:130},
            {field:'actual0215',title:'Feb-2015 Actual',width:130},
            {field:'actual0315',title:'Mar-2015 Actual',width:130},
            {field:'actual0415',title:'Apr-2015 Actual',width:130},
            {field:'actual0515',title:'May-2015 Actual',width:130},
            {field:'actual0615',title:'Jun-2015 Actual',width:130},
            {field:'actual0715',title:'Jul-2015 Actual',width:130},
            {field:'actual0815',title:'Aug-2015 Actual',width:130},
            {field:'actual0915',title:'Sep-2015 Actual',width:130},
            {field:'actual1015',title:'Oct-2015 Actual',width:130},
            {field:'actual1115',title:'Nov-2015 Actual',width:130},
            {field:'actual1215',title:'Dec-2015 Actual',width:130},
            {field:'actual2015',title:'2015 Actual',width:130},
            {field:'actual0116',title:'Jan-2016 Actual',width:130},
            {field:'actual0216',title:'Feb-2016 Actual',width:130},
            {field:'actual0316',title:'Mar-2016 Actual',width:130},
            {field:'actual0416',title:'Apr-2016 Actual',width:130},
            {field:'actual0516',title:'May-2016 Actual',width:130},
            {field:'actual0616',title:'Jun-2016 Actual',width:130},
            {field:'actual0716',title:'Jul-2016 Actual',width:130},
            {field:'actual0816',title:'Aug-2016 Actual',width:130},
            {field:'actual0916',title:'Sep-2016 Actual',width:130},
            {field:'actual1016',title:'Oct-2016 Actual',width:130},
            {field:'actual1116',title:'Nov-2016 Actual',width:130},
            {field:'actual1216',title:'Dec-2016 Actual',width:130},
            {field:'actual2016',title:'2016 Actual',width:130},

            {field:'totalActual',title:'Total Actual',width:130},

            {field:'committedCapital',title:'Committed Capital',width:150},

            {field:'budget2011',title:'2011 Budget',width:110},
            {field:'budget2012',title:'2012 Budget',width:110},
            {field:'budget2013',title:'2013 Budget',width:110},
            {field:'budget2014',title:'2014 Budget',width:110},
            {field:'budget2015',title:'2015 Budget',width:110},
            {field:'budget2016',title:'2016 Budget',width:110},
            {field:'totalBudget',title:'Total Budget',width:110},

            {field:'committedCapitalVariance',title:'Committed Capital Variance',width:180},

            {field:'forecast2011',title:'2011 Forecast',width:120},
            {field:'forecast0112',title:'Jan-2012 Forecast',width:130},
            {field:'forecast0212',title:'Feb-2012 Forecast',width:130},
            {field:'forecast0312',title:'Mar-2012 Forecast',width:130},
            {field:'forecast0412',title:'Apr-2012 Forecast',width:130},
            {field:'forecast0512',title:'May-2012 Forecast',width:130},
            {field:'forecast0612',title:'Jun-2012 Forecast',width:130},
            {field:'forecast0712',title:'Jul-2012 Forecast',width:130},
            {field:'forecast0812',title:'Aug-2012 Forecast',width:130},
            {field:'forecast0912',title:'Sep-2012 Forecast',width:130},
            {field:'forecast1012',title:'Oct-2012 Forecast',width:130},
            {field:'forecast1112',title:'Nov-2012 Forecast',width:130},
            {field:'forecast1212',title:'Dec-2012 Forecast',width:130},
            {field:'forecast2012',title:'2012 Forecast',width:120},
            {field:'forecast0113',title:'Jan-2013 Forecast',width:130},
            {field:'forecast0213',title:'Feb-2013 Forecast',width:130},
            {field:'forecast0313',title:'Mar-2013 Forecast',width:130},
            {field:'forecast0413',title:'Apr-2013 Forecast',width:130},
            {field:'forecast0513',title:'May-2013 Forecast',width:130},
            {field:'forecast0613',title:'Jun-2013 Forecast',width:130},
            {field:'forecast0713',title:'Jul-2013 Forecast',width:130},
            {field:'forecast0813',title:'Aug-2013 Forecast',width:130},
            {field:'forecast0913',title:'Sep-2013 Forecast',width:130},
            {field:'forecast1013',title:'Oct-2013 Forecast',width:130},
            {field:'forecast1113',title:'Nov-2013 Forecast',width:130},
            {field:'forecast1213',title:'Dec-2013 Forecast',width:130},
            {field:'forecast2013',title:'2013 Forecast',width:120},
            {field:'forecast0114',title:'Jan-2014 Forecast',width:130},
            {field:'forecast0214',title:'Feb-2014 Forecast',width:130},
            {field:'forecast0314',title:'Mar-2014 Forecast',width:130},
            {field:'forecast0414',title:'Apr-2014 Forecast',width:130},
            {field:'forecast0514',title:'May-2014 Forecast',width:130},
            {field:'forecast0614',title:'Jun-2014 Forecast',width:130},
            {field:'forecast0714',title:'Jul-2014 Forecast',width:130},
            {field:'forecast0814',title:'Aug-2014 Forecast',width:130},
            {field:'forecast0914',title:'Sep-2014 Forecast',width:130},
            {field:'forecast1014',title:'Oct-2014 Forecast',width:130},
            {field:'forecast1114',title:'Nov-2014 Forecast',width:130},
            {field:'forecast1214',title:'Dec-2014 Forecast',width:130},
            {field:'forecast2014',title:'2014 Forecast',width:120},
            {field:'forecast0115',title:'Jan-2015 Forecast',width:130},
            {field:'forecast0215',title:'Feb-2015 Forecast',width:130},
            {field:'forecast0315',title:'Mar-2015 Forecast',width:130},
            {field:'forecast0415',title:'Apr-2015 Forecast',width:130},
            {field:'forecast0515',title:'May-2015 Forecast',width:130},
            {field:'forecast0615',title:'Jun-2015 Forecast',width:130},
            {field:'forecast0715',title:'Jul-2015 Forecast',width:130},
            {field:'forecast0815',title:'Aug-2015 Forecast',width:130},
            {field:'forecast0915',title:'Sep-2015 Forecast',width:130},
            {field:'forecast1015',title:'Oct-2015 Forecast',width:130},
            {field:'forecast1115',title:'Nov-2015 Forecast',width:130},
            {field:'forecast1215',title:'Dec-2015 Forecast',width:130},
            {field:'forecast2015',title:'2015 Forecast',width:120},
            {field:'forecast0116',title:'Jan-2016 Forecast',width:130},
            {field:'forecast0216',title:'Feb-2016 Forecast',width:130},
            {field:'forecast0316',title:'Mar-2016 Forecast',width:130},
            {field:'forecast0416',title:'Apr-2016 Forecast',width:130},
            {field:'forecast0516',title:'May-2016 Forecast',width:130},
            {field:'forecast0616',title:'Jun-2016 Forecast',width:130},
            {field:'forecast0716',title:'Jul-2016 Forecast',width:130},
            {field:'forecast0816',title:'Aug-2016 Forecast',width:130},
            {field:'forecast0916',title:'Sep-2016 Forecast',width:130},
            {field:'forecast1016',title:'Oct-2016 Forecast',width:130},
            {field:'forecast1116',title:'Nov-2016 Forecast',width:130},
            {field:'forecast1216',title:'Dec-2016 Forecast',width:130},
            {field:'forecast2016',title:'2016 Forecast',width:120},

            {field:'totalForecast',title:'Total Forecast',width:120},

            {field:'totalCommittedandProjected',title:'Total Committed and Projected',width:200},
            {field:'totalProjectVariance',title:'Total Project Variance',width:150},
            {field:'budgetVariance2014',title:'2014 Budget Variance',width:150},
            {field:'budgetVariance2015',title:'2015 Budget Variance',width:150},
            {field:'budgetVariance2016',title:'2016 Budget Variance',width:150},

        ]],
        fit:true,
        showFooter:true
    });
    $('#reportGrid').datagrid('loadData', reportData);
});

function formatPrice(val,row){
    if (val < 0){
        return '<span style="color:red;">('+val+')</span>';
    } else {
        return val;
    }
}


