nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_MWS_getAllActiveAlarms_JSON.js
//	Script Name:	CLGX_SL_MWS_getAllActiveAlarms_JSON
//	Script Id:		customscript_clgx_sl_msw_active_alarms
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/07/2013
//	Includes:		underscore.js
//-------------------------------------------------------------------------------------------------


function suitelet_modius_ws_getalarms_json (request, response) {

	//var stSOSOAPRequest = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:data="http://data.ws.modius.com/">';
	//stSOSOAPRequest += '<soapenv:Header/>';
	//stSOSOAPRequest += '<soapenv:Body>';
	//stSOSOAPRequest += '<data:getAllActiveAlarms/>';
	//stSOSOAPRequest += '</soapenv:Body>';
	//stSOSOAPRequest += '</soapenv:Envelope>';
	//var soapResponse = nlapiRequestURL('http://10.250.24.11/axis2/services/OpenDataWebService.OpenDataWebServiceHttpSoap11Endpoint/', stSOSOAPRequest);
	//var soapResponse = nlapiRequestURL('http://10.250.24.11/axis2/services/OpenDataWebService.OpenDataWebServiceHttpSoap11Endpoint/getLatestValues?ids=824', stSOSOAPRequest);

	var objFile = nlapiLoadFile(798043);
	var strAlarms = objFile.getValue();
	
	strAlarms = strAlarms.replace(new RegExp(' xmlns="http://data.ws.modius.com/"','g'), '');
	var xmlAlarms = nlapiStringToXML(strAlarms);
	var xmlReturns = nlapiSelectNodes(xmlAlarms, '/soapenv:Envelope/soapenv:Body/getAllActiveAlarmsResponse/return');

	var alarmIdArr = new Array();
	var alarmsArr = new Array();
	// extract unique alarms
	for ( var i = 0; i < xmlReturns.length; i++ ) {
		if(_.indexOf(alarmIdArr, nlapiSelectValue(xmlReturns[i], 'alarmId')) < 0){
			
			var alarmId = nlapiSelectValue(xmlReturns[i], 'alarmId');
			var eventId = nlapiSelectValue(xmlReturns[i], 'eventId');
			var dts = nlapiSelectValue(xmlReturns[i], 'dts').replace("T"," ").substring(0,19);
			var groupId = nlapiSelectValue(xmlReturns[i], 'groupId');
			var groupName = nlapiSelectValue(xmlReturns[i], 'groupDisplayName');
			var deviceId = nlapiSelectValue(xmlReturns[i], 'deviceId');
			var deviceName = nlapiSelectValue(xmlReturns[i], 'deviceDisplayName');
			var pointId = nlapiSelectValue(xmlReturns[i], 'pointId');
			var pointName = nlapiSelectValue(xmlReturns[i], 'pointDisplayName');
			
			var alarmObjNames = ['entityId', 'entityName', 'groupId', 'groupName', 'deviceId', 'deviceName', 'alarmId', 'eventId', 'dts', 'iconCls', 'leaf'];
			var alarmObjValues = [pointId, pointName, groupId, groupName, deviceId, deviceName, alarmId, eventId, dts, 'point', true];
			var alarmObj = _.object(alarmObjNames, alarmObjValues);
			
			alarmIdArr.push(alarmId);
			alarmsArr.push(alarmObj);
		}
	}
	var alarmsObj =  _.object(['totalCount', 'items'], [alarmsArr.length, alarmsArr]);

	var format = request.getParameter('format');
	if(format == 'grid'){
		response.write(JSON.stringify(alarmsObj));
	}
	else if(format == 'tree'){
		
		var groupsArr = new Array();
		var devicesArr = new Array();
		var groupIdArr = new Array();
		var deviceIdArr = new Array();

		var nbrAlarms = alarmsArr.length;
		for ( var i = 0; i < nbrAlarms; i++ ) {
			// build uniques groups objects array
			if(_.indexOf(groupIdArr, alarmsArr[i].groupId) < 0){
				groupIdArr.push(alarmsArr[i].groupId);
				var groupObjNames = ['entityId', 'entityName', 'iconCls', 'leaf'];
				var groupObjValues = [alarmsArr[i].groupId, alarmsArr[i].groupName, 'group', false];
				var groupObj = _.object(groupObjNames, groupObjValues);
				groupsArr.push(groupObj);
			}
			// build uniques devices objects array
			if(_.indexOf(deviceIdArr, alarmsArr[i].deviceId) < 0){
				deviceIdArr.push(alarmsArr[i].deviceId);
				var deviceObjNames = ['groupId', 'entityId', 'entityName','iconCls','leaf'];
				var deviceObjValues = [alarmsArr[i].groupId, alarmsArr[i].deviceId, alarmsArr[i].deviceName, 'device', false];
				var deviceObj = _.object(deviceObjNames, deviceObjValues);
				devicesArr.push(deviceObj);
			}
		}
	

		for ( var i = 0; i < groupsArr.length; i++ ) {
			var groupDevicesArr = new Array();
			for ( var j = 0; j < devicesArr.length; j++ ) {
				if(devicesArr[j].groupId == groupsArr[i].entityId){
					var devicePointsArr = _.where(alarmsArr, {deviceId: devicesArr[j].entityId});
					devicesArr[j]["children"] = devicePointsArr;
					groupDevicesArr.push(devicesArr[j]);
				}
			}
			groupsArr[i]["children"] = groupDevicesArr;
		}

		response.write(JSON.stringify(groupsArr));

	}
	else{
		response.write('No data');
	}

}
