nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CLGXDW_Connect_Test.js
//	Script Name:	CLGX_SL_CLGXDW_Connect_Test
//	Script Id:		customscript_clgx_sl_clgx_connect_test
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/11/2013
//-------------------------------------------------------------------------------------------------

function suitelet_clgx_connect_test (request, response){
	try {

		//var requestURL = nlapiRequestURL('https://command1.cologix.com:10313/netsuite/connect.html'); // Railo SSL
		//var requestURL = nlapiRequestURL('https://64.127.72.198:10313/netsuite/connect.html'); // Railo SSL
		//var requestURL = nlapiRequestURL('https://64.127.72.198:10313/netsuite/connect.html'); // Railo 
		
		//var requestURL = nlapiRequestURL('https://command1.cologix.com:10811/netsuite/connect.html'); // CF SSL
		//var requestURL = nlapiRequestURL('https://command1.cologix.com:10811/netsuite/getnodes.cfm'); // CF SSL
		//var requestURL = nlapiRequestURL('https://64.127.72.198:10811/netsuite/connect.html'); // CF SSL
		//var requestURL = nlapiRequestURL('http://64.127.72.198:10812/netsuite/connect.html'); // CF
		
		//var requestURL = nlapiRequestURL('http://10.250.24.11/opendataEE/reports/Cologix-Powerchain-DAL1-beta.swf');
		
		//var html = requestURL.body;
		//response.write( html );
		
		//var requestURL = nlapiRequestURL('https://command1.cologix.com:10811/netsuite/getnodes.cfm');
		var jsonNodes = requestURL.body;
		var arrNodes= JSON.parse( jsonNodes );
		
		
		response.write( JSON.stringify(arrNodes) );

//---------- End Sections ------------------------------------------------------------------------------------------------
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}