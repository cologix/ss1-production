nlapiLogExecution("audit","FLOStart",new Date().getTime());
//------------------------------------------------------
//	Script:		CLGX_RL_CP_ExpressR.js
//	ScriptID:	customscript_clgx_rl_cp_expressR
//	ScriptType:	RESTlet
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//
//------------------------------------------------------

var post = wrap(function post(datain,obj,srights,companyid,contactid,modifierid,radix,sid,csv) {

    //if(srights.xcs > 0){



    var getER=get_ER(companyid);
    obj["er"]=getER;


    return obj;

});

function get_ER(id){

    var columns = new Array();
    var filters = new Array();
    var arr=new Array();
        filters.push(new nlobjSearchFilter("name","custrecord_cologix_service_order","anyof",id));
        var records = nlapiSearchRecord('customrecord_cologix_vxc','customsearch_clgx_ss_vxc_msft', filters, columns);

        if(records!=null) {
            //nlapiSendEmail(206211,206211,'Test1','Test1',null,null,null,null,true);

            for (var j = 0; records != null && j < records.length; j++) {
                var searchAP = records[j];
                var columns = searchAP.getAllColumns();

                var skey= searchAP.getValue(columns[1]);
                arr.push(skey);

            }

        }




    return arr;
}