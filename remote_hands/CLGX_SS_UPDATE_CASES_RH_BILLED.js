//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_UPDATE_CASES_RH_BILLED.js
//	Script Name:	CLGX_SS_UPDATE_CASES_RH_BILLED
//	Script Id:		CLGX_SS_UPDATE_CASES_RH_BILLED
//	Script Runs:	On Server
//	Script Type:	SS Script
//	Deployments:	Sales Order - Sandbox | Production
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//-------------------------------------------------------------------------------------------------

function run() {
    var arrTimes = new Array();
    var searchColumn = new Array();
    var searchFilter = new Array();
    // searchFilter.push(new nlobjSearchFilter('internalid',null,'anyof',internalid));
   /* var searchRHs = nlapiSearchRecord('customrecord_clgx_remote_hands', 'customsearch7190', searchFilter, searchColumn);

    if (searchRHs != null) {
        for (var i = 0; searchRHs != null && i < searchRHs.length; i++) {
            var searchRH = searchRHs[i];
            var columns = searchRH.getAllColumns();
            var timeid = searchRH.getValue(columns[0]);
            if (timeid.indexOf(',') > -1) {
                var splittms1 = timeid.split(',');
                for (var t1 = 0; t1 < splittms1.length; t1++) {
                    if (!inArray(splittms1[t1], arrTimes)) {
                        arrTimes.push(splittms1[t1]);
                    }
                }

            } else {
                if (timeid != '' && timeid != null && !inArray(timeid, arrTimes)) {
                    arrTimes.push(timeid);
                }
            }
        }
    }
    for (var t2 = 0; t2 < arrTimes.length; t2++) {
        nlapiSubmitField('timebill', arrTimes[t2], 'custcol_clgx_rh_billedtime', 'T');
    }*/
    nlapiLogExecution('DEBUG', 'Here0', 'Here0');
    var arrTimes = new Array();
    var searchColumn = new Array();
    var searchFilter = new Array();
    // searchFilter.push(new nlobjSearchFilter('internalid',null,'anyof',internalid));
    var searchRHs = nlapiSearchRecord('customrecord_clgx_remote_hands', 'customsearch7189', searchFilter, searchColumn);
    if (searchRHs != null) {
        for (var i = 0; searchRHs != null && i < searchRHs.length; i++) {
         var searchRH= searchRHs[i];
         var columns = searchRH.getAllColumns();
        var timeid = searchRH.getValue(columns[0]);
        var invDate = searchRH.getValue(columns[1]);
        if (timeid.indexOf(',') > -1) {
            var splittms1 = timeid.split(',');
            for (var t1 = 0; t1 < splittms1.length; t1++) {
                if (!inArray(splittms1[t1], arrTimes)) {
                    arrTimes.push(splittms1[t1]);
                }
            }

        } else {
            if (timeid != '' && timeid != null && !inArray(timeid, arrTimes)) {
                arrTimes.push(timeid);
            }
        }
    }
}
    for(var t2=0; t2<arrTimes.length;t2++){
        var cs=nlapiLoadRecord('supportcase',parseInt(arrTimes[t2]));
        cs.setFieldValue('custevent_clgx_rh_date', invDate);
        cs.setFieldValue('custevent_clgx_billed_rh', 'T');
        nlapiSubmitRecord(cs, false,true);

    }

    //cases
}

function inArray(val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}