nlapiLogExecution("audit","FLOStart",new Date().getTime());
/**
      * Created by catalinataran on 2014-09-09.
      */
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Remote_Hands_New.js
//	Script Name:	CLGX_SS_Remote_Hands_New
//	Script Id:	    customscript_clgx_ss_remote_hands_new
//	Script Runs:	On Server
//	Script Type:	Scheduled Script]
//	Deployments:	Case
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	09/09/2014
//-------------------------------------------------------------------------------------------------
function scheduled_remote_hands(){
    try{
        //------------- Begin Section 1 -------------------------------------------------------------------
        //	Details:	Check if a Remote Hands Exist for this customer, this month, this location
        //	Created:	04/12/2014
        //-------------------------------------------------------------------------------------------------
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');
   
      var context = nlapiGetContext();
        var initialTime = moment();

        var arrayFiltersCases=new Array();
        var arrayColumnsCases=new Array();
        var arrCustomers=new Array();
        var arrFacilities=new Array();
        var arrObj=[];
        var  arrCs=new Array();
        var searchColumn = new Array();
        var searchFilter=new Array();
        var searchCases1 = nlapiSearchRecord('customrecord_clgx_remote_hands','customsearch6105',searchFilter,searchColumn);
        for ( var i = 0; searchCases1 != null && i < searchCases1.length; i++ ) {
            var searchCas = searchCases1[i];
            var columnsCases = searchCas.getAllColumns();
            var caseid = searchCas.getValue(columnsCases[0]);
            if(caseid.indexOf(',')>-1)
            {
                var splittms1=caseid.split(',');
                for(var t1=0; t1<splittms1.length;t1++)
                {
                    if(!in_array(splittms1[t1],arrCs)) {
                        arrCs.push(splittms1[t1]);
                    }
                }

            }
            else{
                if(caseid!='' && caseid!=null && !in_array(caseid,arrCs)) {
                    arrCs.push(caseid);
                }
            }





        }


        var arrayFiltersCases=new Array();
        var arrayColumnsCases=new Array();
        if(arrCs.length>0)
        {
            arrayFiltersCases.push(new nlobjSearchFilter("internalid",null,"noneof", arrCs));
        }

        var search= nlapiSearchRecord('supportcase', 'customsearch_clgx_caseclnov_remote', arrayFiltersCases, arrayColumnsCases);
        //search remote for no location
        if(search!=null)
        {
            for ( var i= 0; search!= null && i < search.length; i++ ) {
                var startExecTime = moment();
                var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);

                if ( (context.getRemainingUsage() <= 300 || totalMinutes > 5) && (i+1) < search.length ){
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    if ( status == 'QUEUED' ) {
                        nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                        break;
                    }
                }
                //get the case dates to create or Update RH record
                var searchC = search[i];
                var columns = searchC.getAllColumns();
                var customerid = searchC.getValue(columns[0]);
                var facilityid = searchC.getValue(columns[1]);
                var obj= {"customer":customerid,"fac":facilityid};
                arrObj.push(obj);
            }

        }

        arrObj= _.uniqWith(arrObj, _.isEqual);
        if(arrObj.length>0) {
            for (var r = 0; arrObj != null && r < arrObj.length; r++) {
                var arrayFilters=new Array();
                var arrayColumns=new Array();
                arrayFilters.push(new nlobjSearchFilter("internalid", "customer", "anyof", arrObj[r].customer));
                arrayFilters.push(new nlobjSearchFilter("custevent_cologix_facility", null, "anyof", arrObj[r].fac));
                var searchCases = nlapiSearchRecord('supportcase', 'customsearch_clgx_caseclnov_remote', arrayFilters, arrayColumns);
                //search remote for no location
                var timeIdsRH = new Array();
                var casesIdsRH = new Array();
                var tms = new Array();
                var casesArray = new Array();
                var usedCaseAfter = '';
                var usedCaseBusiness = '';
                var rtimeCases = '';
                var caseAfter = '';
                var caseBusiness = '';
                var caseUsed = '';
                var totalUsedAfter = 0;
                var totalUsed = 0;
                var totalUsedBusiness = 0;
                var totalAfter = 0;
                var totalBusiness = 0;
                //  }
                var customer = nlapiLoadRecord('customer', arrObj[r].customer);
                //get the customer remaining hours
                var remainingHCustomer = customer.getFieldValue('custentity_clgx_customer_rh_currmthtl');

                var soCustomer = customer.getFieldValue('custentity_clgx_customer_rh_so');
                // var itemSO = customer.getFieldValue('custentity_clgx_customer_rh_pack');

                //check if the customer already has a case number for remote hands associated for this month
                if (remainingHCustomer != null) {
                    var TotalMinutesRemaning = remainingHCustomer;
                }
                else {
                    var TotalMinutesRemaning = 0;

                }
                var numberHoursSO=remainingHCustomer;
                if (searchCases != null) {
                    for (var q = 0; searchCases != null && q < searchCases.length; q++) {
                        var startExecTime = moment();
                        var totalMinutes = (startExecTime.diff(initialTime) / 60000).toFixed(1);

                        if ((context.getRemainingUsage() <= 300 || totalMinutes > 5) && (q + 1) < searchCases.length) {
                            var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                            if (status == 'QUEUED') {
                                nlapiLogExecution('DEBUG', 'End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                                break;
                            }
                        }
                        //get the case dates to create or Update RH record
                        var searchCas = searchCases[q];
                        var columnsCases = searchCas.getAllColumns();
                        var customerid = searchCas.getValue(columnsCases[0]);
                        var facilityid = searchCas.getValue(columnsCases[1]);
                        var dateclosed = searchCas.getValue(columnsCases[2]);
                        var closeddate = new Date(dateclosed);
                        var casecloseddateyear = closeddate.getFullYear();
                        var casecloseddatemonth = closeddate.getMonth();
                        var casecloseddate = dateclosed;
                        var from = searchCas.getValue(columnsCases[3]);
                        var caseid = searchCas.getValue(columnsCases[4]);
                        var casenumber = searchCas.getValue(columnsCases[5]);
                        var timeDecimal = parseFloat(searchCas.getValue(columnsCases[6]));
                        var timeID = searchCas.getValue(columnsCases[7]);
                        //  var timeInternalID = searchCas.getValue(columnsCases[8]);
                        var customerlocation = nlapiLookupField('customrecord_cologix_facility', facilityid, 'custrecord_clgx_facility_location');

                        var stDays = daysInMonth(parseInt(casecloseddatemonth), parseInt(casecloseddateyear));
                        var stStartDate = (parseInt(casecloseddatemonth) + parseInt(1)) + '/1/' + casecloseddateyear;
                        var stEndDate = (parseInt(casecloseddatemonth) + parseInt(1)) + '/' + stDays + '/' + casecloseddateyear;

                        var todayRH = new Date();
                        var yearRemoteHands = todayRH.getFullYear();
                        var monthRemoteHands = todayRH.getMonth();
                        var stDaysRH = daysInMonth(parseInt(monthRemoteHands), parseInt(yearRemoteHands));
                        var stStartDateRH = (parseInt(monthRemoteHands) + parseInt(1)) + '/1/' + yearRemoteHands;
                        var stEndDateRH = (parseInt(monthRemoteHands) + parseInt(1)) + '/' + stDaysRH + '/' + yearRemoteHands;
                        var arrayFiltersFreezing = new Array();
                        var arrayColumnsFreezing = new Array();
                        var monthNames = ["January", "February", "March", "April", "May", "June",
                            "July", "August", "September", "October", "November", "December"];
                        var monthName = monthNames[casecloseddatemonth];
                        var date = new Date(casecloseddate);
                        var month = date.getMonth();
                        var yr = date.getFullYear();
                        month = month.toString();
                        yr = yr.toString();
                        //curent month
                        var currDate = new Date();
                        var currMth = currDate.getMonth();
                        var currMthName = monthNames[currMth];
                        arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_brate', null, null));
                        arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_arate', null, null));
                        arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_item', null, null));
                        arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_irate', null, null));
                        arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_iqty', null, null));
                        arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_so', null, null));


                        //  arrayColumns.push(new nlobjSearchColumn('custrecord_clgx_rh_times',null,null));
                        arrayFiltersFreezing.push(new nlobjSearchFilter("custrecord_clgx_freezing_rh_customer", null, "anyof", customerid));
                        arrayFiltersFreezing.push(new nlobjSearchFilter('custrecord_clgx_freezing_rh_mth', null, 'is', monthName));
                        arrayFiltersFreezing.push(new nlobjSearchFilter('custrecord_clgx_freezing_rh_yr', null, 'is', yr));
                        var searchFRRemoteHands = nlapiSearchRecord('customrecord_freezing_rh_pack', null, arrayFiltersFreezing, arrayColumnsFreezing);
                        //get the customer rate from freezing RH Packs
                        if (searchFRRemoteHands != null) {
                            var totalratebusinessH = searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_brate', null, null);
                            var totalrateafterH = searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_arate', null, null);
                            var itemSO = searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_item', null, null);
                            var itemRate = searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_irate', null, null);
                            var numberHoursSO = searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_iqty', null, null);
                            var soFreezing = searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_so', null, null);

                        }
                        //if (!in_array(customerid, arrCustomers)) {
                        //    nlapiSubmitField('customer', customerid, 'custentity_clgx_customer_rh_currmthtl', numberHoursSO);
                        //   nlapiSubmitField('customer', customerid, 'custentity_clgx_customer_rh_so', soFreezing);


                        //business hours
                        if (timeID == 544) {
                            casesArray.push(caseid);
                            //  timeIdsRH.push(timeInternalID);
                            var timeBMinutes = timeDecimal * 60;
                            timeBMinutes = (15 * Math.ceil(timeBMinutes / 15));


                            if (TotalMinutesRemaning > 0) {
                                var iTotalMinutesRemaning = TotalMinutesRemaning;
                                TotalMinutesRemaning -= timeBMinutes;
                                if (TotalMinutesRemaning > 0) {
                                    totalUsed += timeBMinutes;
                                    totalUsedBusiness += timeBMinutes;

                                    usedCaseBusiness += caseid + '=' + timeBMinutes + ';';
                                    caseUsed += caseid + '=' + timeBMinutes + ';';

                                    rtimeCases += caseid + '=' + timeBMinutes + ';';
                                }
                                else if (TotalMinutesRemaning < 0) {
                                    var minBUsed = timeBMinutes - iTotalMinutesRemaning;
                                    // minBUsed *= -1;
                                    totalUsed += iTotalMinutesRemaning;
                                    totalUsedBusiness += iTotalMinutesRemaning;
                                    TotalMinutesRemaning = 0;
                                    nlapiSubmitField('customer', customerid, 'custentity_clgx_customer_rh_currmthtl', 0);
                                    usedCaseBusiness += caseid + '=' + iTotalMinutesRemaning  + ';';
                                    caseUsed += caseid + '=' + iTotalMinutesRemaning + ';';
                                    rtimeCases += caseid + '=' + timeBMinutes + ';';
                                    var minBUsedRest = timeBMinutes - minBUsed;
                                    caseBusiness += caseid + '=' + minBUsed + ';';
                                    totalBusiness += minBUsed;
                                }
                                else if (TotalMinutesRemaning == 0 && iTotalMinutesRemaning>0) {
                                    nlapiSubmitField('customer', customerid, 'custentity_clgx_customer_rh_currmthtl', 0);

                                    totalUsed += timeBMinutes;
                                    totalUsedBusiness += timeBMinutes;

                                    usedCaseBusiness += caseid + '=' + timeBMinutes + ';';
                                    caseUsed += caseid + '=' + timeBMinutes + ';';

                                    rtimeCases += caseid + '=' + timeBMinutes + ';';
                                }
                            }
                            else {
                                rtimeCases += caseid + '=' + timeBMinutes + ';';
                                caseBusiness += caseid + '=' + timeBMinutes + ';';
                                totalBusiness += timeBMinutes;
                            }
                        }
                        //after hours
                        if (timeID == 545) {
                            casesArray.push(caseid);
                            // timeIdsRH.push(timeInternalID);
                            var timeAMinutes = timeDecimal * 60;
                            if ((timeAMinutes > 0) && (timeAMinutes <= 60)) {
                                timeAMinutes = 60;
                            } else if (timeAMinutes > 60) {
                                timeAMinutes = (15 * Math.ceil(timeAMinutes / 15));
                            }


                            if (TotalMinutesRemaning > 0) {
                                var iTotalMinutesRemaning = TotalMinutesRemaning;
                                TotalMinutesRemaning -= timeAMinutes;
                                if (TotalMinutesRemaning > 0) {
                                    totalUsed += timeAMinutes;
                                    totalUsedAfter += timeAMinutes;

                                    usedCaseAfter += caseid + '=' + timeAMinutes + ';';
                                    caseUsed += caseid + '=' + timeAMinutes + ';';
                                    rtimeCases += caseid + '=' + timeAMinutes + ';';
                                }
                                else if (TotalMinutesRemaning < 0) {
                                    var minAUsed = timeAMinutes - iTotalMinutesRemaning;
                                    //  minAUsed *= -1;
                                    totalUsed += iTotalMinutesRemaning;
                                    totalUsedAfter += iTotalMinutesRemaning;
                                    TotalMinutesRemaning = 0;
                                    nlapiSubmitField('customer', customerid, 'custentity_clgx_customer_rh_currmthtl', 0);
                                    usedCaseAfter += caseid + '=' + iTotalMinutesRemaning + ';';
                                    caseUsed += caseid + '=' + iTotalMinutesRemaning + ';';
                                    rtimeCases += caseid + '=' + timeAMinutes + ';';
                                    var minAUsedRest = timeAMinutes - minAUsed;
                                    caseAfter += caseid + '=' + minAUsed + ';';
                                    totalAfter += minAUsedRest;
                                }else if (TotalMinutesRemaning == 0 && iTotalMinutesRemaning>0) {
                                    nlapiSubmitField('customer', customerid, 'custentity_clgx_customer_rh_currmthtl', 0);

                                    totalUsed += timeAMinutes;
                                    totalUsedAfter += timeAMinutes;

                                    usedCaseAfter += caseid + '=' + timeAMinutes + ';';
                                    caseUsed += caseid + '=' + timeAMinutes + ';';
                                    rtimeCases += caseid + '=' + timeAMinutes + ';';
                                }
                            }
                            else {
                                rtimeCases += caseid + '=' + timeAMinutes + ';';
                                caseAfter += caseid + '=' + timeAMinutes + ';';
                                totalAfter += timeAMinutes;
                            }
                        }
                    }
                }
                nlapiSubmitField('customer', customerid, 'custentity_clgx_customer_rh_currmthtl', TotalMinutesRemaning);
                nlapiSubmitField('customer', customerid, 'custentity_clgx_customer_rh_currmthstart', stStartDate);
                nlapiSubmitField('customer', customerid, 'custentity_clgx_customer_rh_currmthend', stEndDate);


                //create a new RH Record
                var newRecordRemote = nlapiCreateRecord('customrecord_clgx_remote_hands');
                newRecordRemote.setFieldValue('custrecord_clgx_rh_location', customerlocation);
                newRecordRemote.setFieldValue('custrecord_clgx_rh_customer', customerid);
                newRecordRemote.setFieldValue('custrecord_clgx_rh_from_date', stStartDate);
                newRecordRemote.setFieldValue('custrecord_clgx_rh_to_date', stEndDate);

                if ((itemSO != '') && (itemSO != null)) {
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_package', itemSO);
                }

                if ((numberHoursSO != '') && (numberHoursSO != null)) {
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_packagehours', numberHoursSO);
                }
                if ((itemRate != '') && (itemRate != null)) {
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_itemrate', itemRate);
                }
                if (totalUsedBusiness > 0) {
                    //set the business minutes used
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_roundedbus', usedCaseBusiness);
                }
                //set the after minutes used
                if (totalUsedAfter > 0) {
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_roundedafter', usedCaseAfter);
                }


                if (totalUsed > 0) {
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_time_used', parseInt(totalUsed));
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_usedtimecasenumber', caseUsed);
                }
                else {
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_time_used', 0);
                }

                if (totalBusiness > 0) {
                    totalBusiness = parseInt(totalBusiness);
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_time_biz_hours', totalBusiness);
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_businesstimecase', caseBusiness);
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_biz_hours_rate', totalratebusinessH);
                }
                else {
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_time_biz_hours', 0);
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_biz_hours_rate', 0);

                }
                if (totalAfter > 0) {
                    totalAfter = parseInt(totalAfter);
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_time_after_hours', totalAfter);
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_aftertimecase', caseAfter);
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_after_hours_rate', totalrateafterH);

                }
                else {
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_after_hours_rate', 0);
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_time_after_hours', 0);

                }


                newRecordRemote.setFieldValue('custrecord_clgx_rh_cases', casenumber);
                //  newRecordRemote.setFieldValues('custrecord_clgx_rh_times', timeIdsRH);
                newRecordRemote.setFieldValues('custrecord_clgx_rh_casesids', casesArray);
                newRecordRemote.setFieldValue('custrecord_clgx_rh_casesroundedh', rtimeCases);
                if ((soCustomer != '') && (soCustomer != null)) {
                    newRecordRemote.setFieldValue('custrecord_clgx_rh_service_order', soCustomer);
                }
                // custrecord_clgx_rh_businesstimecase
                //custrecord_clgx_rh_aftertimecase
                //custrecord_clgx_rh_ltimeid
                //custrecord_clgx_rh_casesroundedh


                nlapiSubmitRecord(newRecordRemote, false, true);
                /*    update the customer remote hands fields
                 */
            }
        }

        var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
        nlapiLogExecution('DEBUG','Finished Execution', 'Total Remaining Usage - ' + context.getRemainingUsage() + 'Total Usage-' + usageConsumtion + '-------------------------- Finished Scheduled Script --------------------------|');

    }

    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}

function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}

function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}

function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}