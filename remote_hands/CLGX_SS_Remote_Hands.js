nlapiLogExecution("audit","FLOStart",new Date().getTime());
/**
      * Created by catalinataran on 2014-09-09.
      */
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Remote_Hands.js
//	Script Name:	CLGX_SS_Remote_Hands
//	Script Id:	customscript_clgx_ss_remote_hands
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Case
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//	Created:	09/09/2014
//-------------------------------------------------------------------------------------------------
function scheduled_remote_hands(){
    try{
        //------------- Begin Section 1 -------------------------------------------------------------------
        //	Details:	Check if a Remote Hands Exist for this customer, this month, this location
        //	Created:	04/12/2014
        //-------------------------------------------------------------------------------------------------
       /* nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');
        var context = nlapiGetContext();
        //retrieve script parameters
        var customerid = nlapiGetContext().getSetting('SCRIPT','custscript_ss_remote_hands_cus');
        var facilityid = nlapiGetContext().getSetting('SCRIPT','custscript_ss_remote_hands_customerlocat');
        var casecloseddatemonth = nlapiGetContext().getSetting('SCRIPT','custscript_ss_remote_hands_caseclosedmon');
        var casecloseddateyear = nlapiGetContext().getSetting('SCRIPT','custscript_ss_remote_hands_caseclosedyr');
        var casecloseddate = nlapiGetContext().getSetting('SCRIPT','custscript_ss_remote_hands_casecloseddat');
        var caseid = nlapiGetContext().getSetting('SCRIPT','custscript_ss_remote_hands_caseid');
        var casenumber= nlapiGetContext().getSetting('SCRIPT','custscript_ss_remote_hands_cas');
        var from = nlapiGetContext().getSetting('SCRIPT','custscript_ss_remote_hands_casecreateddt');
        var modifiedcase=nlapiGetContext().getSetting('SCRIPT','custscript_ss_remote_hands_modifiedcase');
        var userid=nlapiGetContext().getSetting('SCRIPT','custscript_ss_remote_hands_userid');
        var updateCustomer=1;
        var customerlocation  =  nlapiLookupField('customrecord_cologix_facility', facilityid, 'custrecord_clgx_facility_location');
        var caseRecord=nlapiLoadRecord('supportcase',caseid);
        var subCaseType=caseRecord.getFieldValue('custevent_cologix_sub_case_type');
        var arrColumnsCustomer = new Array();
        var arrFiltersCustomer = new Array();
        arrFiltersCustomer.push(new nlobjSearchFilter("internalid",null,"anyof", customerid));
        var searchCustomers =nlapiSearchRecord('customer','customsearch_clgx_customer_rh_list',arrFiltersCustomer,arrColumnsCustomer);
        var arrayFiltersConfirm=new Array();
        var arrayColumnsConfirm=new Array();
        // arrayColumns.push(new nlobjSearchColumn('internalid',null,null));
        //  arrayColumns.push(new nlobjSearchColumn('custrecord_clgx_rh_times',null,null));
        arrayFiltersConfirm.push(new nlobjSearchFilter("internalid",null,"anyof",caseid));
        var searchCasesConfirm = nlapiSearchRecord('supportcase', 'customsearch_clgx_sc_confirmrhtime', arrayFiltersConfirm, arrayColumnsConfirm);
        if(searchCasesConfirm!=null)
        {
            if(searchCustomers==null)
            {
                if(subCaseType!=85)
                {
                    var emailSubject = 'For Case number #'+casenumber+' you have selected a Customer that does not yet have services with Cologix.';
                    var emailBody = '<p>'+'[This is a Script Generated Email]'+'<br/><br/>'+'<p>'+'For Case number #'+casenumber+' you have selected a Customer that does not yet have services with Cologix. Please edit the Case and select the correct Customer so they can be billed;'+'</p>'+'<p>'+'If you have any further questions please do not hesitate contacting the Helpdesk at Helpdesk@cologix.com'+'</p>'+'<br/>'+'Thank You'+'<br/><br/>';
                    nlapiSendEmail(179749, userid,emailSubject,"<html><body style='font-family:Verdana'>"+emailBody+"</body>",null,null,null,null,true); // Send email to Catalina

                }
            }
            else{
                var stDays  = daysInMonth(parseInt(casecloseddatemonth),parseInt(casecloseddateyear));
                var stStartDate = (parseInt(casecloseddatemonth)+parseInt(1)) + '/1/' + casecloseddateyear;
                var stEndDate   = (parseInt(casecloseddatemonth)+parseInt(1)) + '/' + stDays + '/' + casecloseddateyear;
                var todayRH=new Date();
                var yearRemoteHands=todayRH.getFullYear();
                var monthRemoteHands=todayRH.getMonth();
                var stDaysRH  = daysInMonth(parseInt(monthRemoteHands),parseInt(yearRemoteHands));
                var stStartDateRH = (parseInt(monthRemoteHands)+parseInt(1)) + '/1/' + yearRemoteHands;
                var stEndDateRH   = (parseInt(monthRemoteHands)+parseInt(1)) + '/' + stDaysRH + '/' + yearRemoteHands;
                var arrayFilters=new Array();
                var arrayColumns=new Array();
                // arrayColumns.push(new nlobjSearchColumn('internalid',null,null));
                //  arrayColumns.push(new nlobjSearchColumn('custrecord_clgx_rh_times',null,null));
                arrayFilters.push(new nlobjSearchFilter("custrecord_clgx_rh_customer",null,"anyof",customerid));
                arrayFilters.push(new nlobjSearchFilter("custrecord_clgx_rh_location",null,"anyof",customerlocation));
                arrayFilters.push(new nlobjSearchFilter("custrecord_clgx_rh_from_date", null, "within",stStartDate,stEndDate));
                arrayFilters.push(new nlobjSearchFilter("custrecord_clgx_rh_billed", null, "is",'F'));
                var searchRemote = nlapiSearchRecord('customrecord_clgx_remote_hands', 'customsearch_clgx_remotehands_currmth', arrayFilters, arrayColumns);
                //search remote for no location

                var arrayFiltersL=new Array();
                var arrayColumnsL=new Array();
                arrayColumnsL.push(new nlobjSearchColumn('custrecord_clgx_rh_location',null,'GROUP'));
                // arrayColumns.push(new nlobjSearchColumn('internalid',null,null));
                //  arrayColumns.push(new nlobjSearchColumn('custrecord_clgx_rh_times',null,null));
                arrayFiltersL.push(new nlobjSearchFilter("custrecord_clgx_rh_customer",null,"anyof",customerid));
                arrayFiltersL.push(new nlobjSearchFilter("custrecord_clgx_rh_from_date", null, "within",stStartDate,stEndDate));
                var searchRemoteL = nlapiSearchRecord('customrecord_clgx_remote_hands', null, arrayFiltersL, arrayColumnsL);
                var arrLocs = new Array();
                for ( var i = 0; searchRemoteL != null && i < searchRemoteL.length; i++ ) {
                    var locationL = searchRemoteL[i].getValue('custrecord_clgx_rh_location',null,'GROUP');

                    arrLocs.push(locationL);
                }
                var arrayFilters1=new Array();
                var arrayColumns1=new Array();
                arrayColumns1.push(new nlobjSearchColumn('internalid',null,null));
                //  arrayColumns.push(new nlobjSearchColumn('custrecord_clgx_rh_times',null,null));
                arrayFilters1.push(new nlobjSearchFilter("custrecord_clgx_rh_customer",null,"anyof",customerid));
                arrayFilters1.push(new nlobjSearchFilter("custrecord_clgx_rh_location",null,"anyof",customerlocation));
                arrayFilters1.push(new nlobjSearchFilter("custrecord_clgx_rh_from_date", null, "within",stStartDate,stEndDate));
                arrayFilters1.push(new nlobjSearchFilter("custrecord_clgx_rh_billed", null, "is",'F'));
                var searchRemote1 = nlapiSearchRecord('customrecord_clgx_remote_hands',null, arrayFilters1, arrayColumns1);
                if(searchRemote1!=null)
                {
                    for ( var z = 0; searchRemote1 != null && z < searchRemote1.length; z++ ) {
                        var searchRem = searchRemote1[z];
                        var columns = searchRem.getAllColumns();
                        var remoteID=searchRem.getValue(columns[0]);
                    }
                }
                //get the customer rate for business and after hours
                var reroundedExtraAfter=0;
                var reroundedExtraBusiness=0;
                var  shouldSummBusinessExtra=0;
                var  shouldSummAfterExtra=0;
                var arrayFiltersFreezing=new Array();
                var arrayColumnsFreezing=new Array();
                var date=new Date(casecloseddate);
                var month=date.getMonth();
                var monthNames = [ "January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November", "December" ];
                var monthName=monthNames[month];
                var yr=date.getFullYear();
                month=month.toString();
                yr=yr.toString();
                //curent month
                var currDate=new Date();
                var currMth=currDate.getMonth();
                var currMthName=monthNames[currMth];
                arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_brate',null,null));
                arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_arate',null,null));
                arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_item',null,null));
                arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_irate',null,null));
                arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_iqty',null,null));
                arrayColumnsFreezing.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_so',null,null));



                //  arrayColumns.push(new nlobjSearchColumn('custrecord_clgx_rh_times',null,null));
                arrayFiltersFreezing.push(new nlobjSearchFilter("custrecord_clgx_freezing_rh_customer",null,"anyof",customerid));
                arrayFiltersFreezing.push(new nlobjSearchFilter('custrecord_clgx_freezing_rh_mth',null,'is',monthName));
                arrayFiltersFreezing.push(new nlobjSearchFilter('custrecord_clgx_freezing_rh_yr',null,'is',yr));
                var searchFRRemoteHands = nlapiSearchRecord('customrecord_freezing_rh_pack',null,  arrayFiltersFreezing, arrayColumnsFreezing);
                //get the customer rate from freezing RH Packs
                if(searchFRRemoteHands!=null)
                {
                    var totalratebusinessH = searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_brate',null,null);
                    var totalrateafterH = searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_arate',null,null);
                    var itemSO=searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_item',null,null);
                    var itemRate=searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_irate',null,null);
                    var numberHoursSO=searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_iqty',null,null);
                    var soFreezing=searchFRRemoteHands[0].getValue('custrecord_clgx_freezing_rh_so',null,null);

                }
                var customer=nlapiLoadRecord('customer',customerid);
                //get the customer remaining hours
                var remainingHCustomer=customer.getFieldValue('custentity_clgx_customer_rh_currmthtl');

                var soCustomer= customer.getFieldValue('custentity_clgx_customer_rh_so');
                //  take the pack and total hours from SO
                //var numberHoursSO=0;
                var rate=0;
                var item='';
                var fixedFinished=0;

                //check if the customer already has a case number for remote hands associated for this month
                if(remainingHCustomer!=null)
                {
                    var TotalMinutesRemaning=remainingHCustomer;
                }
                else{
                    var TotalMinutesRemaning=0;

                }
                var timeIdsRH=new Array();
                var casesIdsRH=new Array();
                var tms=new Array();
                var casesArray=new Array();
                var  oldUsedCaseAfter='';
                var  oldUsedCaseBusiness='';
                var  oldCaseAfter='';
                var  oldCaseBusiness='';
                if(searchRemote!=null)
                {
                    for ( var z = 0; searchRemote != null && z < searchRemote.length; z++ ) {
                        var searchRem = searchRemote[z];
                        var columns = searchRem.getAllColumns();
                        //  remoteID.push(searchRem.getValue(columns[z]));
                        tms.push(searchRem.getValue(columns[0]));
                        casesArray.push(searchRem.getValue(columns[1]));
                        var   oldUsedcaserounded=searchRem.getValue(columns[2]);
                        var  oldUsedCaseAfter=searchRem.getValue(columns[3]);
                        var  oldUsedCaseBusiness=searchRem.getValue(columns[4]);
                        var  oldCaseAfter=searchRem.getValue(columns[5]);
                        var  oldCaseBusiness=searchRem.getValue(columns[6]);
                    }

                    if((remainingHCustomer!=null)&&(remainingHCustomer>0))
                    {
                        if((oldUsedcaserounded!=null)&&(oldUsedcaserounded!='')){
                            var casesRArraySplit= new Array();
                            var valuesRArraySplit=new Array();
                            var changedRValue=0;
                            var newcaseRString='';
                            var splitcaserounded=oldUsedcaserounded.split(';');
                            for(var h=0;h<splitcaserounded.length;h++)
                            {
                                var splcaserounded=splitcaserounded[h].split('=');
                                casesRArraySplit.push(splcaserounded[0]);
                                valuesRArraySplit.push(splcaserounded[1]);

                            }
                            for(var j=0;j<casesRArraySplit.length;j++)
                            {

                                if(caseid==casesRArraySplit[j])
                                {
                                    //update the customer remaining hours

                                    TotalMinutesRemaning=parseInt(remainingHCustomer)+parseInt(valuesRArraySplit[j]);


                                }

                            }
                        }
                    }
                    for(var t1=0; t1<tms.length;t1++)
                    {
                        var splittms=new Array();
                        if(tms[t1].indexOf(',')>-1)
                        {
                            splittms.push(tms[t1].split(','));
                        }
                        else{
                            splittms.push(tms[t1]);
                        }
                        for(var t2=0; t2<splittms.length;t2++)
                        {
                            timeIdsRH.push(splittms[t2]);
                        }

                    }

                    for(var t2=0; t2<casesArray.length;t2++)
                    {
                        var splitcasesArray=new Array();
                        if(casesArray[t2].indexOf(',')>-1)
                        {
                            splitcasesArray.push(casesArray[t2].split(','));
                        }
                        else{
                            splitcasesArray.push(casesArray[t2]);
                        }
                        for(var t3=0; t3<splitcasesArray.length;t3++)
                        {
                            casesIdsRH.push(splitcasesArray[t3]);
                        }

                    }


                }
                //search all time Ids for this case

                var arrayFiltersC=new Array();
                var arrayColumnsC=new Array();
                arrayFiltersC.push(new nlobjSearchFilter("internalid",null,"anyof",caseid));
                var searchCaseTim = nlapiSearchRecord('supportcase', 'customsearch_clgx_caseremhtime', arrayFiltersC, arrayColumnsC);
                var timeCasIds=new Array();
                if(searchCaseTim!=null)
                {
                    for ( var z = 0; searchCaseTim != null && z < searchCaseTim.length; z++ ) {
                        var searchCS = searchCaseTim[z];
                        var columns = searchCS.getAllColumns();
                        var timeIDSearch=searchCS.getValue(columns[0]);
                        if((timeIDSearch!=null)&&(timeIDSearch!=''))
                        {
                            timeCasIds.push(timeIDSearch);
                        }
                    }


                    var remainingminutes=TotalMinutesRemaning;
                    var lastTimeItem=0;
                    var extrabusiness=0;
                    var extraafter=0;
                    var justminutesbusiness=0;
                    var justminutesafter=0;
                    var minutesextrabusiness=0;
                    var minutesextraafter=0;
                    var activatelastItem=0;
                    var timeIdsforUse= timeCasIds;
                    var rerounded=0;
                    var emailSubject = 'Time IDS, Case ID';
                    var emailBody="Case Id: "+caseid + 'Time IDs: ';
                    for(var z=0; z<timeIdsforUse.length; z++)
                    {
                        emailBody +=timeIdsforUse[z]+';'
                    }

                    clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_rh_admin", emailSubject, emailBody);
                    //nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null,true); // Send email to Catalina

                    var roundedBus=0;
                    var roundedAfter=0;
                    var roundedOneTimeAfter=0;
                    var roundedOneTimeBusiness=0;
                    if(timeIdsforUse.length>0)
                    {
                        //test if already this case has been added to a RH
                        if((oldUsedCaseAfter!='')||(oldUsedCaseBusiness!='')||(oldCaseAfter!='')||(oldCaseBusiness))
                        {

                            var casesBusinessUsedArraySplit= new Array();
                            var valuesBusinessUsedArraySplit=new Array();
                            var oldUsedMinBusiness=0;
                            if((oldUsedCaseBusiness!=null)&&(oldUsedCaseBusiness!=''))
                            {
                                var splitcasesBuisnessUsed=oldUsedCaseBusiness.split(';');
                                for(var h=0;h<splitcasesBuisnessUsed.length;h++)
                                {
                                    if(splitcasesBuisnessUsed[h]!='')
                                    {
                                        var splcasebus=splitcasesBuisnessUsed[h].split('=');
                                        casesBusinessUsedArraySplit.push(splcasebus[0]);
                                        valuesBusinessUsedArraySplit.push(splcasebus[1]);
                                    }

                                }
                                for(var j=0;j<casesBusinessUsedArraySplit.length;j++)
                                {
                                    if(caseid==casesBusinessUsedArraySplit[j])
                                    {


                                        oldUsedMinBusiness=valuesBusinessUsedArraySplit[j];


                                    }

                                }
                            }
                            if((oldUsedCaseAfter!=null)&&(oldUsedCaseAfter!=''))
                            {
                                var casesAfterUsedArraySplit= new Array();
                                var valuesAfterUsedArraySplit=new Array();
                                var oldUsedMinAfter=0;
                                var splitcasesAfterUsed=oldUsedCaseAfter.split(';');
                                for(var h=0;h<splitcasesAfterUsed.length;h++)
                                {
                                    if(splitcasesAfterUsed[h]!='')
                                    {
                                        var splcaseafter=splitcasesAfterUsed[h].split('=');
                                        casesAfterUsedArraySplit.push(splcaseafter[0]);
                                        valuesAfterUsedArraySplit.push(splcaseafter[1]);
                                    }

                                }
                                for(var j=0;j<casesAfterUsedArraySplit.length;j++)
                                {
                                    if(caseid==casesAfterUsedArraySplit[j])
                                    {


                                        oldUsedMinAfter=valuesAfterUsedArraySplit[j];


                                    }

                                }
                            }
                            if((oldCaseBusiness!=null)&&(oldCaseBusiness!=''))
                            {
                                var casesBusinessArraySplit= new Array();
                                var valuesBusinessArraySplit=new Array();
                                var oldMinBusiness=0;
                                var splitcasesBuisness=oldCaseBusiness.split(';');

                                for(var h=0;h<splitcasesBuisness.length;h++)
                                {
                                    if(splitcasesBuisness[h]!='')
                                    {
                                        shouldSummBusinessExtra++;

                                        var splcasebus1=splitcasesBuisness[h].split('=');
                                        casesBusinessArraySplit.push(splcasebus1[0]);
                                        valuesBusinessArraySplit.push(splcasebus1[1]);
                                    }

                                }
                                for(var j=0;j<casesBusinessArraySplit.length;j++)
                                {
                                    if(caseid==casesBusinessArraySplit[j])
                                    {


                                        oldMinBusiness=valuesBusinessArraySplit[j];


                                    }

                                }
                            }

                            if((oldCaseAfter!=null)&&(oldCaseAfter!=''))
                            {
                                var casesAfterArraySplit= new Array();
                                var valuesAfterArraySplit=new Array();
                                var oldMinAfter=0;
                                var splitcasesAfter=oldCaseAfter.split(';');

                                for(var h=0;h<splitcasesAfter.length;h++)
                                {
                                    if(splitcasesAfter[h]!=''){
                                        shouldSummAfterExtra++;
                                        var splcaseafter1=splitcasesAfter[h].split('=');
                                        casesAfterArraySplit.push(splcaseafter1[0]);
                                        valuesAfterArraySplit.push(splcaseafter1[1]);
                                    }

                                }
                                for(var j=0;j<casesAfterArraySplit.length;j++)
                                {
                                    if(caseid==casesAfterArraySplit[j])
                                    {


                                        oldMinAfter=valuesAfterArraySplit[j];


                                    }

                                }
                            }
                            var updateCustomer=1;
                            if((monthName!=currMthName)&&(itemSO!=569))

                            {
                                var arrFiltersRH=new Array();
                                var arrColumnsRH=new Array();
                                arrFiltersRH.push(new nlobjSearchFilter("custrecord_clgx_rh_customer",null,"anyof",customerid));
                                arrFiltersRH.push(new nlobjSearchFilter("custrecord_clgx_rh_from_date", null, "within",stStartDate,stEndDate));
                                //  arrFiltersRH.push(new nlobjSearchFilter("internalid", null, "noneof",remoteID));
                                var searchRHUsedTime = nlapiSearchRecord('customrecord_clgx_remote_hands', 'customsearch_clgx_savedsearch_rhusedt', arrFiltersRH, arrColumnsRH);

                                if(searchRHUsedTime!=null)
                                {

                                    var searchRH = searchRHUsedTime[0];
                                    var columns = searchRH.getAllColumns();
                                    var totalUsedHRHs=searchRH.getValue(columns[0]);
                                    if((totalUsedHRHs=='')||(totalUsedHRHs==null))
                                    {
                                        totalUsedHRHs=0;
                                    }
                                    if((numberHoursSO!='')&&(numberHoursSO!=null))
                                    {
                                        TotalMinutesRemaning=parseInt(numberHoursSO)-parseInt(totalUsedHRHs);
                                        /*    if(TotalMinutesRemaning>0)
                                         {
                                         var arrFiltersRH1=new Array();
                                         var arrColumnsRH1=new Array();
                                         arrFiltersRH1.push(new nlobjSearchFilter("custrecord_clgx_rh_customer",null,"anyof",customerid));
                                         arrFiltersRH1.push(new nlobjSearchFilter("custrecord_clgx_rh_from_date", null, "within",stStartDate,stEndDate));
                                         arrFiltersRH1.push(new nlobjSearchFilter("internalid", null, "noneof",remoteID));
                                         var searchRHUsedTime1 = nlapiSearchRecord('customrecord_clgx_remote_hands', 'customsearch_clgx_savedsearch_rhusedt', arrFiltersRH1, arrColumnsRH1);
                                         if(searchRHUsedTime1!=null)
                                         {
    
                                         var searchRH1 = searchRHUsedTime1[0];
                                         var columns1 = searchRH1.getAllColumns();
                                         var totalUsedHRHs1=searchRH1.getValue(columns1[0]);
                                         if((totalUsedHRHs1=='')||(totalUsedHRHs1==null))
                                         {
                                         totalUsedHRHs1=0;
                                         }
                                         if((numberHoursSO!='')&&(numberHoursSO!=null))
                                         {
                                         TotalMinutesRemaning=parseInt(numberHoursSO)-parseInt(totalUsedHRHs1);
    
                                         }
    
                                         }*/

                                        //}
                                 /*       var updateCustomer=0;
                                    }
                                    else{
                                        TotalMinutesRemaning=0;
                                        var updateCustomer=0;
                                    }

                                }
                                else{
                                    if((numberHoursSO!='')&&(numberHoursSO!=null))
                                    {
                                        TotalMinutesRemaning=numberHoursSO;
                                        var updateCustomer=0;
                                    }
                                    else{
                                        TotalMinutesRemaning=0;
                                        var updateCustomer=0;
                                    }
                                }


                            }


                            if(TotalMinutesRemaning>0)
                            {
                                var justminutesbusiness=0;
                                var justminutesafter=0;
                                var justminutesbusinessCase=0;
                                var justminutesafterCase=0;
                                var minutesExtraBusinessCase=0;
                                var minutesExtraAfterCase=0;
                                var remainingIteratorBusiness=0;
                                var remainingIteratorAfter=0;
                                var remainingminutesEditCase=TotalMinutesRemaning;
                                var typeofLastItem='';
                                for(var r=0; r<timeIdsforUse.length; r++)
                                {
                                    var tbill=nlapiLoadRecord('timebill',timeIdsforUse[r]);
                                    var hourstbill=tbill.getFieldValue('hours');
                                    var item=tbill.getFieldValue('item');
                                    var tbillsplit=hourstbill.split(":");
                                    if(tbillsplit[1].charAt(0)==0)
                                    {
                                        tbillsplit[1] = tbillsplit[1].slice(1);
                                    }
                                    if(item==544)
                                    {
                                        var remainingminutesEditCaseBeforeB=parseInt(remainingminutesEditCase);
                                        remainingminutesEditCase=remainingminutesEditCase-((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));
                                        if(remainingminutesEditCase<=0){
                                            remainingIteratorBusiness++;
                                            var customer=nlapiLoadRecord('customer',customerid);
                                            if(updateCustomer==1){
                                                customer.setFieldValue('custentity_clgx_customer_rh_currmthtl',0);
                                                nlapiSubmitRecord(customer, false,true);
                                            }
                                            if(typeofLastItem==''){
                                                typeofLastItem='bus';
                                            }
                                            minutesExtraBusinessCase=remainingminutesEditCase*(-1);
                                            if(remainingIteratorBusiness==1){
                                                var diffRemainingB=parseInt(remainingminutesEditCaseBeforeB);

                                            }

                                            justminutesbusinessCase=parseInt(justminutesbusinessCase)+parseInt(diffRemainingB);
                                        }
                                        else{
                                            justminutesbusinessCase=parseInt(justminutesbusinessCase)+((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));
                                        }
                                    }
                                    if(item==545)
                                    {
                                        var remainingminutesEditCaseBefore=parseInt(remainingminutesEditCase);
                                        remainingminutesEditCase=remainingminutesEditCase-((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));
                                        if(remainingminutesEditCase<=0){
                                            remainingIteratorAfter++;
                                            if(typeofLastItem=='')
                                            {
                                                typeofLastItem='after';
                                            }
                                            var customer=nlapiLoadRecord('customer',customerid);
                                            if(updateCustomer==1){
                                                customer.setFieldValue('custentity_clgx_customer_rh_currmthtl',0);
                                            }

                                            nlapiSubmitRecord(customer, false,true);
                                            minutesExtraAfterCase=remainingminutesEditCase*(-1);
                                            if(remainingIteratorAfter==1){
                                                var diffRemaining=parseInt(remainingminutesEditCaseBefore);

                                            }

                                            justminutesafterCase=parseInt(justminutesafterCase)+parseInt(diffRemaining);

                                        }
                                        else{

                                            justminutesafterCase=parseInt(justminutesafterCase)+((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));
                                        }
                                    }
                                }

                                if(typeofLastItem!='')
                                {
                                    if(typeofLastItem=='bus')
                                    {
                                        var justminutesafterCase_=justminutesafterCase;
                                        var hoursfromMinuteAfter=justminutesafterCase_/60;
                                        hoursfromMinuteAfter=parseFloat(hoursfromMinuteAfter);
                                        hoursfromMinuteAfter=hoursfromMinuteAfter.toFixed(2);
                                        //split the case hours, round and transform it to decimal form again
                                        var splitAfterTimeinHoursperCase=hoursfromMinuteAfter.split('.');
                                        if((splitAfterTimeinHoursperCase[0]==0)&&(splitAfterTimeinHoursperCase[1]!=0))
                                        {
                                            splitAfterTimeinHoursperCase[0]=1;
                                            splitAfterTimeinHoursperCase[1]=0;
                                        }
                                        if((splitAfterTimeinHoursperCase[1]>0)&&(splitAfterTimeinHoursperCase[1]<=25))
                                        {
                                            splitAfterTimeinHoursperCase[1]=15;
                                        }
                                        if((splitAfterTimeinHoursperCase[1]>25)&&(splitAfterTimeinHoursperCase[1]<=50))
                                        {
                                            splitAfterTimeinHoursperCase[1]=30;
                                        }
                                        if((splitAfterTimeinHoursperCase[1]>50)&&(splitAfterTimeinHoursperCase[1]<=75))
                                        {
                                            splitAfterTimeinHoursperCase[1]=45;
                                        }
                                        if((splitAfterTimeinHoursperCase[1]>75)&&(splitAfterTimeinHoursperCase[1]<99))
                                        {
                                            splitAfterTimeinHoursperCase[1]=0;
                                            splitAfterTimeinHoursperCase[0]=parseInt(splitAfterTimeinHoursperCase[0])+parseInt(1);

                                        }
                                        var minutesAfterReRo_= parseInt(splitAfterTimeinHoursperCase[0]*60)+ parseInt(splitAfterTimeinHoursperCase[1]);

                                        var diffAfterReRounding=parseInt(minutesAfterReRo_)-parseInt(justminutesafterCase_);
                                        var justminutesbusinessCase_=justminutesbusinessCase;
                                        justminutesbusinessCase=parseInt(justminutesbusinessCase)-parseInt(diffAfterReRounding);
                                        var diffMinutesBus=parseInt(justminutesbusinessCase_)-parseInt(justminutesbusinessCase);
                                        minutesExtraBusinessCase=parseInt(minutesExtraBusinessCase)+parseInt(diffMinutesBus);
                                    }





                                    if(typeofLastItem=='after')
                                    {
                                        var justminutesbusinessCase_=justminutesbusinessCase;
                                        var hoursfromMinuteBusiness=justminutesbusinessCase_/60;
                                        hoursfromMinuteBusiness=parseFloat(hoursfromMinuteBusiness);
                                        hoursfromMinuteBusiness=hoursfromMinuteBusiness.toFixed(2);
                                        //spli the case hours, round and transorm it to decimal form again
                                        var splitBusinessTimeinHoursperCase=hoursfromMinuteBusiness.split('.');
                                        if((splitBusinessTimeinHoursperCase[1]>0)&&(splitBusinessTimeinHoursperCase[1]<=25))
                                        {
                                            splitBusinessTimeinHoursperCase[1]=15;
                                        }
                                        if((splitBusinessTimeinHoursperCase[1]>25)&&(splitBusinessTimeinHoursperCase[1]<=50))
                                        {
                                            splitBusinessTimeinHoursperCase[1]=30;
                                        }
                                        if((splitBusinessTimeinHoursperCase[1]>50)&&(splitBusinessTimeinHoursperCase[1]<=75))
                                        {
                                            splitBusinessTimeinHoursperCase[1]=45;
                                        }
                                        if((splitBusinessTimeinHoursperCase[1]>75)&&(splitBusinessTimeinHoursperCase[1]<99))
                                        {
                                            splitBusinessTimeinHoursperCase[1]=0;
                                            splitBusinessTimeinHoursperCase[0]=parseInt(splitBusinessTimeinHoursperCase[0])+parseInt(1);

                                        }
                                        var minutesBusinessReRo_= parseInt(splitBusinessTimeinHoursperCase[0]*60)+ parseInt(splitBusinessTimeinHoursperCase[1]);

                                        var diffBusinessReRounding=parseInt(minutesBusinessReRo_)-parseInt(justminutesbusinessCase_);
                                        var justminutesafterCase_=justminutesafterCase;
                                        justminutesafterCase=parseInt(justminutesafterCase)-parseInt(diffBusinessReRounding);
                                        var diffMinutesAfter=parseInt(justminutesafterCase_)-parseInt(justminutesafterCase);
                                        minutesExtraAfterCase=parseInt(minutesExtraAfterCase)+parseInt(diffMinutesAfter);
                                    }

                                }
                                var minutesBusinessCase=parseInt(justminutesbusinessCase)+parseInt(minutesExtraBusinessCase);
                                var minutesAfterCase=parseInt(justminutesafterCase)+parseInt(minutesExtraAfterCase);
                                if(minutesBusinessCase>0)
                                {
                                    var hoursfromMinuteBusiness=minutesBusinessCase/60;
                                    hoursfromMinuteBusiness=parseFloat(hoursfromMinuteBusiness);
                                    hoursfromMinuteBusiness=hoursfromMinuteBusiness.toFixed(2);
                                    //spli the case hours, round and transorm it to decimal form again
                                    var splitBusinessTimeinHoursperCase=hoursfromMinuteBusiness.split('.');
                                    if((splitBusinessTimeinHoursperCase[1]>0)&&(splitBusinessTimeinHoursperCase[1]<=25))
                                    {
                                        splitBusinessTimeinHoursperCase[1]=15;
                                    }
                                    if((splitBusinessTimeinHoursperCase[1]>25)&&(splitBusinessTimeinHoursperCase[1]<=50))
                                    {
                                        splitBusinessTimeinHoursperCase[1]=30;
                                    }
                                    if((splitBusinessTimeinHoursperCase[1]>50)&&(splitBusinessTimeinHoursperCase[1]<=75))
                                    {
                                        splitBusinessTimeinHoursperCase[1]=45;
                                    }
                                    if((splitBusinessTimeinHoursperCase[1]>75)&&(splitBusinessTimeinHoursperCase[1]<99))
                                    {
                                        splitBusinessTimeinHoursperCase[1]=0;
                                        splitBusinessTimeinHoursperCase[0]=parseInt(splitBusinessTimeinHoursperCase[0])+parseInt(1);

                                    }
                                    var minutesBusinessReRo= parseInt(splitBusinessTimeinHoursperCase[0]*60)+ parseInt(splitBusinessTimeinHoursperCase[1]);

                                }
                                if(minutesExtraBusinessCase>0)
                                {
                                    minutesExtraBusinessCase=parseInt(minutesBusinessReRo)-parseInt(justminutesbusinessCase);
                                }
                                if(diffRemainingB==0)
                                {

                                    var aux=minutesExtraBusinessCase;
                                    minutesExtraBusinessCase=justminutesbusinessCase;
                                    justminutesbusinessCase=aux;
                                }
                                if((minutesExtraBusinessCase==0)&&(justminutesbusinessCase>0))
                                {
                                    justminutesbusinessCase=parseInt(minutesBusinessReRo)-parseInt(minutesExtraBusinessCase);
                                }

                                if(minutesAfterCase>0)
                                {
                                    var hoursfromMinuteAfter=minutesAfterCase/60;
                                    hoursfromMinuteAfter=parseFloat(hoursfromMinuteAfter);
                                    hoursfromMinuteAfter=hoursfromMinuteAfter.toFixed(2);
                                    //spli the case hours, round and transorm it to decimal form again
                                    var splitAfterTimeinHoursperCase=hoursfromMinuteAfter.split('.');
                                    if(splitAfterTimeinHoursperCase[0]==0)
                                    {
                                        splitAfterTimeinHoursperCase[0]=1;
                                        splitAfterTimeinHoursperCase[1]=0;
                                    }
                                    if((splitAfterTimeinHoursperCase[1]>0)&&(splitAfterTimeinHoursperCase[1]<=25))
                                    {
                                        splitAfterTimeinHoursperCase[1]=15;
                                    }
                                    if((splitAfterTimeinHoursperCase[1]>25)&&(splitAfterTimeinHoursperCase[1]<=50))
                                    {
                                        splitAfterTimeinHoursperCase[1]=30;
                                    }
                                    if((splitAfterTimeinHoursperCase[1]>50)&&(splitAfterTimeinHoursperCase[1]<=75))
                                    {
                                        splitAfterTimeinHoursperCase[1]=45;
                                    }
                                    if((splitAfterTimeinHoursperCase[1]>75)&&(splitAfterTimeinHoursperCase[1]<99))
                                    {
                                        splitAfterTimeinHoursperCase[1]=0;
                                        splitAfterTimeinHoursperCase[0]=parseInt(splitAfterTimeinHoursperCase[0])+parseInt(1);

                                    }
                                    var minutesAfterReRo= parseInt(splitAfterTimeinHoursperCase[0]*60)+ parseInt(splitAfterTimeinHoursperCase[1]);




                                    //test if already this case has been added to a RH
                                }
                                if(minutesExtraAfterCase>0)
                                {
                                    minutesExtraAfterCase=parseInt(minutesAfterReRo)-parseInt(justminutesafterCase);
                                }
                                if((minutesExtraAfterCase==0)&&(justminutesafterCase>0))
                                {
                                    justminutesafterCase=parseInt(minutesAfterReRo)-parseInt(minutesExtraAfterCase);
                                }
                            }
                            else{

                                var minutesExtraBusinessCase=0;
                                var minutesExtraAfterCase=0;
                                var justminutesbusinessCaseExtra=0;
                                var justminutesafterCaseExtra=0;

                                for(var r=0; r<timeIdsforUse.length; r++)
                                {
                                    var tbill=nlapiLoadRecord('timebill',timeIdsforUse[r]);
                                    var hourstbill=tbill.getFieldValue('hours');
                                    var item=tbill.getFieldValue('item');
                                    var tbillsplit=hourstbill.split(":");
                                    if(tbillsplit[1].charAt(0)==0)
                                    {
                                        tbillsplit[1] = tbillsplit[1].slice(1);
                                    }
                                    if(item==544)
                                    {



                                        justminutesbusinessCaseExtra=parseInt(justminutesbusinessCaseExtra)+((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));

                                    }
                                    if(item==545)
                                    {


                                        justminutesafterCaseExtra=parseInt(justminutesafterCaseExtra)+((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));

                                    }
                                }
                                var minutesAfterReRoExtra=0;
                                var minutesBusinessReRoExtra=0;

                                if(justminutesafterCaseExtra>0)
                                {
                                    var hoursfromMinuteAfter=justminutesafterCaseExtra/60;
                                    hoursfromMinuteAfter=parseFloat(hoursfromMinuteAfter);
                                    hoursfromMinuteAfter=hoursfromMinuteAfter.toFixed(2);
                                    //spli the case hours, round and transorm it to decimal form again
                                    var splitAfterTimeinHoursperCase=hoursfromMinuteAfter.split('.');
                                    if(splitAfterTimeinHoursperCase[0]==0)
                                    {
                                        splitAfterTimeinHoursperCase[0]=1;
                                        splitAfterTimeinHoursperCase[1]=0;
                                    }
                                    if((splitAfterTimeinHoursperCase[1]>0)&&(splitAfterTimeinHoursperCase[1]<=25))
                                    {
                                        splitAfterTimeinHoursperCase[1]=15;
                                    }
                                    if((splitAfterTimeinHoursperCase[1]>25)&&(splitAfterTimeinHoursperCase[1]<=50))
                                    {
                                        splitAfterTimeinHoursperCase[1]=30;
                                    }
                                    if((splitAfterTimeinHoursperCase[1]>50)&&(splitAfterTimeinHoursperCase[1]<=75))
                                    {
                                        splitAfterTimeinHoursperCase[1]=45;
                                    }
                                    if((splitAfterTimeinHoursperCase[1]>75)&&(splitAfterTimeinHoursperCase[1]<99))
                                    {
                                        splitAfterTimeinHoursperCase[1]=0;
                                        splitAfterTimeinHoursperCase[0]=parseInt(splitAfterTimeinHoursperCase[0])+parseInt(1);

                                    }
                                    var minutesAfterReRoExtra= parseInt(splitAfterTimeinHoursperCase[0]*60)+ parseInt(splitAfterTimeinHoursperCase[1]);


                                }

                                if( justminutesbusinessCaseExtra>0)
                                {
                                    var hoursfromMinuteBusiness= justminutesbusinessCaseExtra/60;
                                    hoursfromMinuteBusiness=parseFloat(hoursfromMinuteBusiness);
                                    hoursfromMinuteBusiness=hoursfromMinuteBusiness.toFixed(2);
                                    //spli the case hours, round and transorm it to decimal form again
                                    var splitBusinessTimeinHoursperCase=hoursfromMinuteBusiness.split('.');
                                    if((splitBusinessTimeinHoursperCase[1]>0)&&(splitBusinessTimeinHoursperCase[1]<=25))
                                    {
                                        splitBusinessTimeinHoursperCase[1]=15;
                                    }
                                    if((splitBusinessTimeinHoursperCase[1]>25)&&(splitBusinessTimeinHoursperCase[1]<=50))
                                    {
                                        splitBusinessTimeinHoursperCase[1]=30;
                                    }
                                    if((splitBusinessTimeinHoursperCase[1]>50)&&(splitBusinessTimeinHoursperCase[1]<=75))
                                    {
                                        splitBusinessTimeinHoursperCase[1]=45;
                                    }
                                    if((splitBusinessTimeinHoursperCase[1]>75)&&(splitBusinessTimeinHoursperCase[1]<99))
                                    {
                                        splitBusinessTimeinHoursperCase[1]=0;
                                        splitBusinessTimeinHoursperCase[0]=parseInt(splitBusinessTimeinHoursperCase[0])+parseInt(1);

                                    }
                                    var minutesBusinessReRoExtra= parseInt(splitBusinessTimeinHoursperCase[0]*60)+ parseInt(splitBusinessTimeinHoursperCase[1]);

                                }
                                var oldMinutesAfter=parseInt(oldUsedMinAfter);
                                var oldMinutesBusiness=parseInt(oldUsedMinBusiness);
                                if(minutesAfterReRoExtra>0)
                                {
                                    minutesExtraAfterCase= parseInt(minutesAfterReRoExtra)-parseInt(oldMinutesAfter);
                                }
                                if(minutesBusinessReRoExtra>0)
                                {
                                    minutesExtraBusinessCase= parseInt(minutesBusinessReRoExtra)-parseInt(oldMinutesBusiness);
                                }
                                justminutesbusinessCase=0;
                                justminutesafterCase=0;


                            }

                            /**
                                              * Update the Remote Record
                                              */

                           /* var setMinutesUsedBus=0;
                            var setMinutesUsedAfter=0;
                            var customer=nlapiLoadRecord('customer',customerid);
                            var casecustomernumber=customer.getFieldValue('custentity_clgx_customer_rh_casenumber');
                            if((monthName==currMthName)){
                                if((casecustomernumber!=null)&&(casecustomernumber!=''))
                                {
                                    if((casecustomernumber.indexOf(casenumber)==-1))
                                    {
                                        casecustomernumber='#'+casecustomernumber+', #'+casenumber;
                                        customer.setFieldValue('custentity_clgx_customer_rh_casenumber',casecustomernumber);
                                    }
                                }
                            }
                            nlapiSubmitRecord(customer, false,true);
                            var remoteHandsRecord=nlapiLoadRecord('customrecord_clgx_remote_hands',remoteID);
                            var oldcasesIDs=remoteHandsRecord.getFieldValue('custrecord_clgx_rh_cases');
                            //custrecord_clgx_rh_ltimeid
                            //custrecord_clgx_rh_casesroundedh
                            if(!(oldcasesIDs.indexOf(casenumber)>-1))
                            {
                                oldcasesIDs=oldcasesIDs +','+casenumber;
                                remoteHandsRecord.setFieldValue('custrecord_clgx_rh_cases',oldcasesIDs);
                            }
                            var oldTimeIDs=remoteHandsRecord.getFieldValues('custrecord_clgx_rh_times');

                            oldTimeIDs=oldTimeIDs.concat(timeIdsforUse);
                            var oldCSIDs=remoteHandsRecord.getFieldValues('custrecord_clgx_rh_casesids');
                            oldCSIDs=oldCSIDs.concat(caseid);
                            remoteHandsRecord.setFieldValues('custrecord_clgx_rh_casesids',oldCSIDs);
                            remoteHandsRecord.setFieldValues('custrecord_clgx_rh_times', oldTimeIDs);
                            var oldtotaltimebusinessH=remoteHandsRecord.getFieldValue('custrecord_clgx_rh_time_biz_hours');
                            if(oldtotaltimebusinessH==null)
                            {
                                oldtotaltimebusinessH=0;
                            }
                            var oldtottimealafterH=remoteHandsRecord.getFieldValue('custrecord_clgx_rh_time_after_hours');
                            if(oldtottimealafterH==null)
                            {
                                oldtottimealafterH=0;
                            }
                            var oldTimeRemaining=remoteHandsRecord.getFieldValue('custrecord_clgx_rh_time_used');
                            if(oldTimeRemaining==null)
                            {
                                oldTimeRemaining=0;
                            }
                            var oldmintesRoundedBusiness=remoteHandsRecord.getFieldValue('custrecord_clgx_rh_roundedbus');

                            if((oldmintesRoundedBusiness!=null)&&(oldmintesRoundedBusiness!='')){
                                if (oldmintesRoundedBusiness instanceof Array) {
                                    var   oldmintesRoundedBusiness_1=oldmintesRoundedBusiness[0];
                                } else {
                                    var   oldmintesRoundedBusiness_1=oldmintesRoundedBusiness;
                                }
                                if(justminutesbusinessCase>0){

                                    var casesBusinessUsedArraySplit= new Array();
                                    var valuesBusinessUsedArraySplit=new Array();
                                    var changedBusinessUsedValue=0;
                                    var newcaseBusinesUsedString='';
                                    var splitcasesBuisnessUsed=oldmintesRoundedBusiness_1.split(';');
                                    for(var h=0;h<splitcasesBuisnessUsed.length;h++)
                                    {
                                        if(splitcasesBuisnessUsed[h]!='')
                                        {
                                            var splcasebus=splitcasesBuisnessUsed[h].split('=');
                                            casesBusinessUsedArraySplit.push(splcasebus[0]);
                                            valuesBusinessUsedArraySplit.push(splcasebus[1]);
                                        }

                                    }
                                    for(var j=0;j<casesBusinessUsedArraySplit.length;j++)
                                    {
                                        if(caseid!=casesBusinessUsedArraySplit[j])
                                        {
                                            setMinutesUsedBus=1;
                                            remoteHandsRecord.setFieldValue('custrecord_clgx_rh_roundedbus',oldmintesRoundedBusiness+caseid+'='+parseInt(justminutesbusinessCase)+';');
                                        }
                                        else if(caseid==casesBusinessUsedArraySplit[j])
                                        {

                                            valuesBusinessUsedArraySplit[j]=parseInt(justminutesbusinessCase);
                                            changedBusinessUsedValue=1;


                                        }

                                    }
                                    if(changedBusinessUsedValue>0)
                                    {
                                        for(var j=0;j<casesBusinessUsedArraySplit.length;j++)
                                        {
                                            if(casesBusinessUsedArraySplit[j]!='')
                                            {
                                                newcaseBusinesUsedString=newcaseBusinesUsedString+casesBusinessUsedArraySplit[j]+'='+parseInt(valuesBusinessUsedArraySplit[j])+';';
                                            }
                                        }
                                        setMinutesUsedBus=1;
                                        remoteHandsRecord.setFieldValue('custrecord_clgx_rh_roundedbus', newcaseBusinesUsedString);



                                    }
                                }
                            }
                            else{
                                setMinutesUsedBus=1;
                                remoteHandsRecord.setFieldValue('custrecord_clgx_rh_roundedbus', caseid+'='+ parseInt(justminutesbusinessCase)+';');

                            }




                            var oldmintesRoundedAfter=remoteHandsRecord.getFieldValue('custrecord_clgx_rh_roundedafter');
                            if((oldmintesRoundedAfter!=null)&&(oldmintesRoundedAfter!='')){
                                if (oldmintesRoundedAfter instanceof Array) {
                                    var   oldmintesRoundedAfter_1=oldmintesRoundedAfter[0];
                                } else {
                                    var   oldmintesRoundedAfter_1=oldmintesRoundedAfter;
                                }

                                if(justminutesafterCase>0){
                                    var casesAfterUsedArraySplit= new Array();
                                    var valuesAfterUsedArraySplit=new Array();
                                    var changedAfterUsedValue=0;
                                    var newcaseAfterUsedString='';
                                    var splitcasesAfterUsed=oldmintesRoundedAfter_1.split(';');
                                    for(var h=0;h<splitcasesAfterUsed.length;h++)
                                    {
                                        if(splitcasesAfterUsed[h]!='')
                                        {
                                            var splcaseafter=splitcasesAfterUsed[h].split('=');
                                            casesAfterUsedArraySplit.push(splcaseafter[0]);
                                            valuesAfterUsedArraySplit.push(splcaseafter[1]);
                                        }

                                    }
                                    for(var j=0;j<casesAfterUsedArraySplit.length;j++)

                                    {



                                        if(caseid!=casesAfterUsedArraySplit[j])
                                        {
                                            setMinutesUsedAfter=1;
                                            remoteHandsRecord.setFieldValue('custrecord_clgx_rh_roundedafter',oldmintesRoundedAfter+caseid+'='+parseInt(justminutesafterCase)+';');
                                        }
                                        else if(caseid==casesAfterUsedArraySplit[j])
                                        {

                                            valuesAfterUsedArraySplit[j]=parseInt(justminutesafterCase);
                                            changedAfterUsedValue=1;


                                        }

                                    }
                                    if(changedAfterUsedValue>0)
                                    {
                                        for(var j=0;j<casesAfterUsedArraySplit.length;j++)
                                        {
                                            if(casesAfterUsedArraySplit[j]!='')
                                            {

                                                newcaseAfterUsedString=newcaseAfterUsedString+casesAfterUsedArraySplit[j]+'='+parseInt(valuesAfterUsedArraySplit[j])+';';
                                            }
                                        }
                                        setMinutesUsedAfter=1;
                                        remoteHandsRecord.setFieldValue('custrecord_clgx_rh_roundedafter', newcaseAfterUsedString);



                                    }



                                }
                            }
                            else{
                                setMinutesUsedAfter=1;
                                remoteHandsRecord.setFieldValue('custrecord_clgx_rh_roundedafter', caseid+'='+ parseInt(justminutesafterCase)+';');

                            }



                            if((justminutesafterCase>0)||(justminutesbusinessCase>0))
                            {
                                totalminutesUsed=parseInt(justminutesafterCase)+parseInt(justminutesbusinessCase);
                            } else
                            {
                                totalminutesUsed=0;
                            }


                            if(totalminutesUsed>0)
                            {


                                var difMinutesUsed=parseInt(totalminutesUsed)-parseInt(oldUsedMinBusiness)-parseInt(oldUsedMinAfter);
                                var oldUsedtimecase1= remoteHandsRecord.getFieldValues('custrecord_clgx_rh_usedtimecasenumber');
                                if((oldUsedtimecase1!=null)&&(oldUsedtimecase1!='')){
                                    if (oldUsedtimecase1 instanceof Array) {
                                        var  oldUsedtimecase=oldUsedtimecase1[0];
                                    } else {
                                        var   oldUsedtimecase=oldUsedtimecase1;
                                    }
                                    var casesArraySplit= new Array();
                                    var valuesArraySplit=new Array();
                                    var changedValue=0;
                                    var newcaseString='';
                                    var splitUsedTimecase=oldUsedtimecase.split(';');
                                    for(var h=0;h<splitUsedTimecase.length;h++)
                                    {
                                        if(splitUsedTimecase[h]!='')
                                        {
                                            var splitUsdTimecases=splitUsedTimecase[h].split('=');
                                            casesArraySplit.push(splitUsdTimecases[0]);
                                            valuesArraySplit.push(splitUsdTimecases[1]);
                                        }

                                    }
                                    for(var j=0;j<casesArraySplit.length;j++)
                                    {
                                        if(caseid!=casesArraySplit[j])
                                        {

                                            remoteHandsRecord.setFieldValue('custrecord_clgx_rh_usedtimecasenumber', oldUsedtimecase+caseid+'='+parseInt(totalminutesUsed)+';');
                                        }
                                        else if(caseid==casesArraySplit[j])
                                        {
                                            //update the customer remaining hours
                                            var updateRemaining=parseInt(remainingHCustomer)+parseInt(valuesArraySplit[j]);
                                            var oldTimevalue=parseInt(valuesArraySplit[j]);
                                            valuesArraySplit[j]=parseInt(totalminutesUsed);

                                            changedValue=1;


                                        }

                                    }
                                    if(changedValue>0)
                                    {
                                        for(var j=0;j<casesArraySplit.length;j++)
                                        {
                                            if(casesArraySplit[j]!='')
                                            {
                                                newcaseString=newcaseString+casesArraySplit[j]+'='+parseInt(valuesArraySplit[j])+';';
                                            }
                                        }
                                        remoteHandsRecord.setFieldValue('custrecord_clgx_rh_usedtimecasenumber', newcaseString);
                                    }
                                }
                                else{
                                    remoteHandsRecord.setFieldValue('custrecord_clgx_rh_usedtimecasenumber', caseid+'='+parseInt(totalminutesUsed)+';');

                                }
                            }
                            if(minutesExtraBusinessCase>0)
                            {

                                //update the Business Time cases extra
                                var   oldExtraBusinessCases1=remoteHandsRecord.getFieldValues('custrecord_clgx_rh_businesstimecase');
                                if((oldExtraBusinessCases1!=null)&&(oldExtraBusinessCases1!='')){
                                    if (oldExtraBusinessCases1 instanceof Array) {
                                        var  oldExtraBusinessCases=oldExtraBusinessCases1[0];
                                    } else {
                                        var   oldExtraBusinessCases=oldExtraBusinessCases1;
                                    }

                                    var casesBusinessArraySplit= new Array();
                                    var valuesBusinessArraySplit=new Array();
                                    var changedBusinessValue=0;
                                    var newcaseBusinessString='';
                                    var splitcasesbusiness=oldExtraBusinessCases.split(';');
                                    for(var h=0;h<splitcasesbusiness.length;h++)
                                    {
                                        if(splitcasesbusiness[h]!='')
                                        {
                                            var splcasebusiness=splitcasesbusiness[h].split('=');
                                            casesBusinessArraySplit.push(splcasebusiness[0]);
                                            valuesBusinessArraySplit.push(splcasebusiness[1]);
                                        }

                                    }
                                    for(var j=0;j<casesBusinessArraySplit.length;j++)
                                    {
                                        if(caseid!=casesBusinessArraySplit[j])
                                        {
                                            remoteHandsRecord.setFieldValue('custrecord_clgx_rh_businesstimecase', oldExtraBusinessCases+caseid+'='+ parseInt(minutesExtraBusinessCase)+';');
                                        }
                                        else if(caseid==casesBusinessArraySplit[j])
                                        {
                                            //update the customer remaining hours

                                            valuesBusinessArraySplit[j]=parseInt(minutesExtraBusinessCase);
                                            changedBusinessValue=1;


                                        }

                                    }
                                    if(changedBusinessValue>0)
                                    {
                                        for(var j=0;j<casesBusinessArraySplit.length;j++)
                                        {
                                            if(casesBusinessArraySplit[j]!='')
                                            {
                                                newcaseBusinessString=newcaseBusinessString+casesBusinessArraySplit[j]+'='+parseInt(valuesBusinessArraySplit[j])+';';
                                            }
                                        }
                                        remoteHandsRecord.setFieldValue('custrecord_clgx_rh_businesstimecase', newcaseBusinessString);


                                    }
                                }
                                else{
                                    remoteHandsRecord.setFieldValue('custrecord_clgx_rh_businesstimecase', caseid+'='+ parseInt(minutesExtraBusinessCase)+';');

                                }
                            }


                            // if(!(_.contains(casesIdsRH, caseid)))
                            // {
                            if(minutesExtraAfterCase>0)
                            {




                                //update the After Time cases extra
                                var   oldExtraAfterCases1=remoteHandsRecord.getFieldValues('custrecord_clgx_rh_aftertimecase');
                                if((oldExtraAfterCases1!=null)&&(oldExtraAfterCases1!='')){

                                    if (oldExtraAfterCases1 instanceof Array) {
                                        var  oldExtraAfterCases=oldExtraAfterCases1[0];
                                    } else {
                                        var   oldExtraAfterCases=oldExtraAfterCases1;
                                    }
                                    var casesAfterArraySplit= new Array();
                                    var valuesAfterArraySplit=new Array();
                                    var changedAfterValue=0;
                                    var newcaseAfterString='';
                                    var splitcasesafter=oldExtraAfterCases.split(';');
                                    for(var h=0;h<splitcasesafter.length;h++)
                                    {
                                        if(splitcasesafter[h]!='')
                                        {
                                            var splcaseafter=splitcasesafter[h].split('=');
                                            casesAfterArraySplit.push(splcaseafter[0]);
                                            valuesAfterArraySplit.push(splcaseafter[1]);
                                        }

                                    }
                                    for(var j=0;j<casesAfterArraySplit.length;j++)
                                    {
                                        if(caseid!=casesAfterArraySplit[j])
                                        {
                                            remoteHandsRecord.setFieldValue('custrecord_clgx_rh_aftertimecase', oldExtraAfterCases+caseid+'='+ parseInt(minutesExtraAfterCase)+';');
                                        }
                                        else if(caseid==casesAfterArraySplit[j])
                                        {
                                            //update the customer remaining hours

                                            valuesAfterArraySplit[j]=parseInt(minutesExtraAfterCase);
                                            changedAfterValue=1;


                                        }

                                    }
                                    if(changedAfterValue>0)
                                    {
                                        for(var j=0;j<casesAfterArraySplit.length;j++)
                                        {
                                            if(casesAfterArraySplit[j]!='')
                                            {
                                                newcaseAfterString=newcaseAfterString+casesAfterArraySplit[j]+'='+parseInt(valuesAfterArraySplit[j])+';';
                                            }
                                        }
                                        remoteHandsRecord.setFieldValue('custrecord_clgx_rh_aftertimecase', newcaseAfterString);


                                    }
                                }
                                else{
                                    remoteHandsRecord.setFieldValue('custrecord_clgx_rh_aftertimecase', caseid+'='+ parseInt(minutesExtraAfterCase)+';');

                                }
                            }


                            if((justminutesafterCase>0)||(justminutesbusinessCase>0))
                            {
                                if((justminutesafterCase=='')||(justminutesafterCase==null))
                                {
                                    justminutesafterCase=0;
                                }
                                if((justminutesbusinessCase=='')||(justminutesbusinessCase==null))
                                {
                                    justminutesbusinessCase=0;
                                }
                                if((justminutesafterCase>0)||(justminutesbusinessCase>0))
                                {
                                    totalminutesUsed=parseInt(justminutesafterCase)+parseInt(justminutesbusinessCase);
                                } else
                                {
                                    totalminutesUsed=0;
                                }
                                var customer=nlapiLoadRecord('customer',customerid);
                                var difMinutesUsed=parseInt(totalminutesUsed)-parseInt(oldUsedMinBusiness)-parseInt(oldUsedMinAfter);
                                var remainingCustomer1=parseInt(remainingHCustomer)-parseInt(difMinutesUsed);
                                if(remainingCustomer1>=0){
                                    if(updateCustomer==1){
                                        customer.setFieldValue('custentity_clgx_customer_rh_currmthtl',remainingCustomer1);
                                        nlapiSubmitRecord(customer, false,true);
                                    }
                                }
                                var oldUsedTimeCase=remoteHandsRecord.getFieldValue('custrecord_clgx_rh_time_used');
                                var difMinutesUsed=parseInt(totalminutesUsed)-parseInt(oldUsedMinBusiness)-parseInt(oldUsedMinAfter);
                                var usedNewandOldTime=parseInt(oldUsedTimeCase)+parseInt(difMinutesUsed);





                                remoteHandsRecord.setFieldValue('custrecord_clgx_rh_time_used', usedNewandOldTime);


                            }
                            if(minutesExtraBusinessCase>0)
                            {

                                remoteHandsRecord.setFieldValue('custrecord_clgx_rh_biz_hours_rate', totalratebusinessH);
                            }

                            if(minutesExtraAfterCase>0)
                            {
                                remoteHandsRecord.setFieldValue('custrecord_clgx_rh_after_hours_rate', totalrateafterH);

                            }


                            if(minutesExtraBusinessCase>0)
                            {
                                var oldminutesExtraBusinessCase=remoteHandsRecord.getFieldValue('custrecord_clgx_rh_time_biz_hours');
                                if((oldminutesExtraBusinessCase!=null)&&(oldminutesExtraBusinessCase!=''))
                                {
                                    var difMinutesBusExtra=parseInt(minutesExtraBusinessCase)-parseInt(oldMinBusiness);

                                    remoteHandsRecord.setFieldValue('custrecord_clgx_rh_time_biz_hours',parseInt(oldminutesExtraBusinessCase)+parseInt(difMinutesBusExtra));
                                }
                                else{
                                    remoteHandsRecord.setFieldValue('custrecord_clgx_rh_time_biz_hours',parseInt(minutesExtraBusinessCase));

                                }


                            }
                            if(minutesExtraAfterCase>0)
                            {
                                var oldminutesExtraAfterCase=remoteHandsRecord.getFieldValue('custrecord_clgx_rh_time_after_hours');
                                if((oldminutesExtraAfterCase!=null)&&(oldminutesExtraAfterCase!=''))
                                {
                                    var difMinutesAfterExtra=parseInt(minutesExtraAfterCase)-parseInt(oldMinAfter);
                                    remoteHandsRecord.setFieldValue('custrecord_clgx_rh_time_after_hours',parseInt(oldminutesExtraAfterCase)+parseInt(difMinutesAfterExtra));
                                }
                                else{
                                    remoteHandsRecord.setFieldValue('custrecord_clgx_rh_time_after_hours',parseInt(minutesExtraAfterCase));

                                }


                            }


                            var   oldUsedcaserounded1=remoteHandsRecord.getFieldValues('custrecord_clgx_rh_casesroundedh');
                            if (oldUsedcaserounded1 instanceof Array) {
                                var  oldUsedcaserounded=oldUsedcaserounded1[0];
                            } else {
                                var   oldUsedcaserounded=oldUsedcaserounded1;
                            }

                            if((oldUsedcaserounded!=null)&&(oldUsedcaserounded!='')){
                                var casesRArraySplit= new Array();
                                var valuesRArraySplit=new Array();
                                var changedRValue=0;
                                var newcaseRString='';
                                var splitcaserounded=oldUsedcaserounded.split(';');
                                for(var h=0;h<splitcaserounded.length;h++)
                                {
                                    if(splitcaserounded[h]!='')
                                    {
                                        var splcaserounded=splitcaserounded[h].split('=');
                                        casesRArraySplit.push(splcaserounded[0]);
                                        valuesRArraySplit.push(splcaserounded[1]);
                                    }

                                }
                                for(var j=0;j<casesRArraySplit.length;j++)
                                {
                                    if(caseid!=casesRArraySplit[j])
                                    {
                                        var calculateRounded=parseInt(parseInt(totalminutesUsed)+parseInt(minutesExtraAfterCase)+parseInt(minutesExtraBusinessCase));
                                        if(calculateRounded>0)
                                        {
                                            remoteHandsRecord.setFieldValue('custrecord_clgx_rh_casesroundedh', oldUsedcaserounded+caseid+'='+calculateRounded+';');
                                        }
                                    }
                                    else if(caseid==casesRArraySplit[j])
                                    {
                                        //update the customer remaining hours
                                        if(totalminutesUsed>0)
                                        {
                                            valuesRArraySplit[j]=parseInt(parseInt(totalminutesUsed)+parseInt(minutesExtraAfterCase)+parseInt(oldMinAfter)+parseInt(minutesExtraBusinessCase)+parseInt(oldMinBusiness));
                                        }
                                        else
                                        {
                                            valuesRArraySplit[j]=parseInt(parseInt(oldUsedMinAfter)+parseInt(oldUsedMinBusiness)+parseInt(minutesExtraAfterCase)+parseInt(minutesExtraBusinessCase));

                                        }
                                        changedRValue=1;


                                    }

                                }
                                if(changedRValue>0)
                                {
                                    for(var j=0;j<casesRArraySplit.length;j++)
                                    {
                                        if(casesRArraySplit[j]!='')
                                        {
                                            newcaseRString=newcaseRString+casesRArraySplit[j]+'='+valuesRArraySplit[j]+';';
                                        }
                                    }
                                    remoteHandsRecord.setFieldValue('custrecord_clgx_rh_casesroundedh', newcaseRString);


                                }
                            }
                            else{
                                remoteHandsRecord.setFieldValue('custrecord_clgx_rh_casesroundedh', caseid+'='+  parseInt(parseInt(totalminutesUsed)+parseInt(minutesExtraAfterCase)+parseInt(minutesExtraBusinessCase))+';');

                            }


                            nlapiSubmitRecord(remoteHandsRecord, false,true);




                        }
                        //finish- EDIT RH

                        else{

                            var updateCustomer=1;
                            if((monthName!=currMthName)&&(itemSO!=569))
                            {
                                var arrFiltersRH=new Array();
                                var arrColumnsRH=new Array();
                                arrFiltersRH.push(new nlobjSearchFilter("custrecord_clgx_rh_customer",null,"anyof",customerid));
                                arrFiltersRH.push(new nlobjSearchFilter("custrecord_clgx_rh_from_date", null, "within",stStartDate,stEndDate));
                                var searchRHUsedTime = nlapiSearchRecord('customrecord_clgx_remote_hands', 'customsearch_clgx_savedsearch_rhusedt', arrFiltersRH, arrColumnsRH);

                                if(searchRHUsedTime!=null)
                                {

                                    var searchRH = searchRHUsedTime[0];
                                    var columns = searchRH.getAllColumns();
                                    var totalUsedHRHs=searchRH.getValue(columns[0]);
                                    if((totalUsedHRHs=='')||(totalUsedHRHs==null))
                                    {
                                        totalUsedHRHs=0;
                                    }
                                    if((numberHoursSO!='')&&(numberHoursSO!=null))
                                    {
                                        TotalMinutesRemaning=parseInt(numberHoursSO)-parseInt(totalUsedHRHs);
                                        var updateCustomer=0;
                                    }
                                    else{
                                        TotalMinutesRemaning=0;
                                        var updateCustomer=0;
                                    }

                                }
                                else{
                                    if((numberHoursSO!='')&&(numberHoursSO!=null))
                                    {
                                        TotalMinutesRemaning=numberHoursSO;
                                        var updateCustomer=0;
                                    }
                                    else{
                                        TotalMinutesRemaning=0;
                                        var updateCustomer=0;
                                    }
                                }


                            }

                            if(TotalMinutesRemaning>0)
                            {
                                remainingminutes=TotalMinutesRemaning;
                                if(timeIdsforUse.length==1)
                                {
                                    var tbill=nlapiLoadRecord('timebill',timeIdsforUse[0]);
                                    var hourstbill=tbill.getFieldValue('hours');
                                    var item=tbill.getFieldValue('item');
                                    var tbillsplit=hourstbill.split(":");
                                    if(tbillsplit[1].charAt(0)==0)
                                    {
                                        tbillsplit[1] = tbillsplit[1].slice(1);
                                    }

                                    var minutesOneTime=((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));
                                    //business
                                    if(item==544)
                                    {
                                        var hoursfromMinuteBusiness=minutesOneTime/60;
                                        hoursfromMinuteBusiness=parseFloat(hoursfromMinuteBusiness);
                                        hoursfromMinuteBusiness=hoursfromMinuteBusiness.toFixed(2);
                                        //spli the case hours, round and transorm it to decimal form again
                                        var splitBusinessTimeinHoursperCase=hoursfromMinuteBusiness.split('.');
                                        if((splitBusinessTimeinHoursperCase[1]>0)&&(splitBusinessTimeinHoursperCase[1]<=25))
                                        {
                                            splitBusinessTimeinHoursperCase[1]=15;
                                        }
                                        if((splitBusinessTimeinHoursperCase[1]>25)&&(splitBusinessTimeinHoursperCase[1]<=50))
                                        {
                                            splitBusinessTimeinHoursperCase[1]=30;
                                        }
                                        if((splitBusinessTimeinHoursperCase[1]>50)&&(splitBusinessTimeinHoursperCase[1]<=75))
                                        {
                                            splitBusinessTimeinHoursperCase[1]=45;
                                        }
                                        if((splitBusinessTimeinHoursperCase[1]>75)&&(splitBusinessTimeinHoursperCase[1]<99))
                                        {
                                            splitBusinessTimeinHoursperCase[1]=0;
                                            splitBusinessTimeinHoursperCase[0]=parseInt(splitBusinessTimeinHoursperCase[0])+parseInt(1);

                                        }
                                        roundedOneTimeBusiness=1;
                                        var minutesBusinessOneTime= parseInt(splitBusinessTimeinHoursperCase[0]*60)+ parseInt(splitBusinessTimeinHoursperCase[1]);
                                        remainingminutes=remainingminutes-minutesBusinessOneTime;
                                    }
                                    //after
                                    if(item==545)
                                    {

                                        var hoursfromMinuteAfter=minutesOneTime/60;
                                        hoursfromMinuteAfter=parseFloat(hoursfromMinuteAfter);
                                        hoursfromMinuteAfter=hoursfromMinuteAfter.toFixed(2);
                                        //spli the case hours, round and transorm it to decimal form again
                                        var splitAfterTimeinHoursperCase=hoursfromMinuteAfter.split('.');
                                        if(splitAfterTimeinHoursperCase[0]==0)
                                        {
                                            splitAfterTimeinHoursperCase[0]=1;
                                            splitAfterTimeinHoursperCase[1]=0;
                                        }
                                        if((splitAfterTimeinHoursperCase[1]>0)&&(splitAfterTimeinHoursperCase[1]<=25))
                                        {
                                            splitAfterTimeinHoursperCase[1]=15;
                                        }
                                        if((splitAfterTimeinHoursperCase[1]>25)&&(splitAfterTimeinHoursperCase[1]<=50))
                                        {
                                            splitAfterTimeinHoursperCase[1]=30;
                                        }
                                        if((splitAfterTimeinHoursperCase[1]>50)&&(splitAfterTimeinHoursperCase[1]<=75))
                                        {
                                            splitAfterTimeinHoursperCase[1]=45;
                                        }
                                        if((splitAfterTimeinHoursperCase[1]>75)&&(splitAfterTimeinHoursperCase[1]<99))
                                        {
                                            splitAfterTimeinHoursperCase[1]=0;
                                            splitAfterTimeinHoursperCase[0]=parseInt(splitAfterTimeinHoursperCase[0])+parseInt(1);

                                        }
                                        roundedOneTimeAfter=1;
                                        var minutesAfterOneTime= parseInt(splitAfterTimeinHoursperCase[0]*60)+ parseInt(splitAfterTimeinHoursperCase[1]);
                                        remainingminutes=remainingminutes-minutesAfterOneTime;
                                    }


                                    if(remainingminutes<0)
                                    {
                                        if(itemSO==569)
                                        {
                                            fixedFinished=1;
                                        }
                                        if(updateCustomer==1){
                                            customer.setFieldValue('custentity_clgx_customer_rh_currmthtl',0);
                                        }
                                        if(item==544)
                                        {
                                            minutesextrabusiness=remainingminutes*(-1);
                                        }
                                        if(item==545)
                                        {
                                            minutesextraafter=remainingminutes*(-1);
                                        }
                                        nlapiSubmitRecord(customer, false,true);
                                        lastTimeItem=timeIdsforUse[r];
                                        if(item==544)
                                        {
                                            justminutesbusiness=minutesBusinessOneTime-minutesextrabusiness;
                                        }
                                        if(item==545)
                                        {
                                            justminutesafter=minutesAfterOneTime-minutesextraafter;
                                        }
                                    }
                                    if(remainingminutes==0)
                                    {
                                        if(itemSO==569)
                                        {
                                            fixedFinished=1;
                                        }
                                        if(updateCustomer==1){
                                            customer.setFieldValue('custentity_clgx_customer_rh_currmthtl',0);
                                        }
                                        if(item==544)
                                        {
                                            minutesextrabusiness=remainingminutes*(-1);
                                        }
                                        if(item==545)
                                        {
                                            minutesextraafter=remainingminutes*(-1);
                                        }
                                        nlapiSubmitRecord(customer, false,true);
                                        lastTimeItem=timeIdsforUse[r];
                                        if(item==544)
                                        {
                                            justminutesbusiness=minutesBusinessOneTime-minutesextrabusiness;
                                        }
                                        if(item==545)
                                        {
                                            justminutesafter=minutesAfterOneTime-minutesextraafter;
                                        }
                                    }
                                    if(remainingminutes>0)
                                    {

                                        if(item==544)
                                        {
                                            justminutesbusiness=minutesBusinessOneTime;
                                        }
                                        if(item==545)
                                        {
                                            justminutesafter=minutesAfterOneTime;
                                        }
                                    }

                                }
                                else{
                                    for(var r=0; r<timeIdsforUse.length; r++)
                                    {
                                        var tbill=nlapiLoadRecord('timebill',timeIdsforUse[r]);
                                        var hourstbill=tbill.getFieldValue('hours');
                                        var item=tbill.getFieldValue('item');
                                        var tbillsplit=hourstbill.split(":");
                                        if(tbillsplit[1].charAt(0)==0)
                                        {
                                            tbillsplit[1] = tbillsplit[1].slice(1);
                                        }
                                        if(activatelastItem==0)
                                        {
                                            var remainingminutesBefore=remainingminutes;
                                            remainingminutes=remainingminutes-((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));
                                            if(item==544)
                                            {
                                                if(remainingminutes<=0)
                                                {
                                                    var justminutesbusiness_1=0-remainingminutesBefore;
                                                    justminutesbusiness_1=justminutesbusiness_1*(-1);
                                                    justminutesbusiness=parseInt(justminutesbusiness)+parseInt(justminutesbusiness_1);
                                                    roundedBus=1;
                                                    var minJustBus=justminutesbusiness;

                                                }
                                                else{
                                                    justminutesbusiness=justminutesbusiness+((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));
                                                }
                                            }
                                            if(item==545)
                                            {
                                                if(remainingminutes<=0)
                                                {
                                                    var justminutesafter_1=0-remainingminutesBefore;
                                                    justminutesafter_1=justminutesafter_1*(-1);
                                                    justminutesafter=parseInt(justminutesafter)+parseInt(justminutesafter_1);
                                                    roundedAfter=1;
                                                    var minJustAfter=justminutesafter;

                                                }
                                                else{
                                                    justminutesafter=parseInt(justminutesafter)+((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));
                                                }
                                            }

                                            if(r==0)
                                            {
                                                var remaining1=TotalMinutesRemaning-((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));
                                                if(remaining1<=0)
                                                {

                                                    if(item==544)
                                                    {
                                                        if(remainingminutes<=0)
                                                        {
                                                            var justminutesbusiness_1=0-TotalMinutesRemaning;
                                                            justminutesbusiness_1=justminutesbusiness_1*(-1);
                                                            if(!(in_array(facilityid,arrLocs)))
                                                            {
                                                                justminutesbusiness=parseInt(justminutesbusiness)+parseInt(justminutesbusiness_1);
                                                            }
                                                        }
                                                        else{
                                                            justminutesbusiness=TotalMinutesRemaning-((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));
                                                        }
                                                    }
                                                    if(item==545)
                                                    {
                                                        if(remainingminutes<=0)
                                                        {
                                                            var justminutesafter_1=0-TotalMinutesRemaning;
                                                            justminutesafter_1=justminutesafter_1*(-1);
                                                            if(!(in_array(facilityid,arrLocs)))
                                                            {
                                                                justminutesafter=parseInt(justminutesafter)+parseInt(justminutesafter_1);
                                                            }
                                                        }
                                                        else{
                                                            justminutesafter=TotalMinutesRemaning-((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));
                                                        }
                                                    }
                                                }
                                            }
                                            if(remainingminutes<0)
                                            {
                                                if(itemSO==569)
                                                {
                                                    fixedFinished=1;
                                                }
                                                if(updateCustomer==1){
                                                    customer.setFieldValue('custentity_clgx_customer_rh_currmthtl',0);
                                                }
                                                if(item==544)
                                                {
                                                    minutesextrabusiness=remainingminutes*(-1);
                                                }
                                                if(item==545)
                                                {
                                                    minutesextraafter=remainingminutes*(-1);
                                                }
                                                nlapiSubmitRecord(customer, false,true);
                                                lastTimeItem=timeIdsforUse[r];
                                                var minutesAfterReRo=0;
                                                var minutesBusinessReRo=0;

                                                //I finished the minutes and I reround the minutes
                                                if(rerounded==0)
                                                {
                                                    if(minutesextrabusiness>0)
                                                    {
                                                        //after
                                                        if(justminutesafter>0)
                                                        {
                                                            var hoursfromMinuteAfter=justminutesafter/60;
                                                            hoursfromMinuteAfter=parseFloat(hoursfromMinuteAfter);
                                                            hoursfromMinuteAfter=hoursfromMinuteAfter.toFixed(2);
                                                            //spli the case hours, round and transorm it to decimal form again
                                                            var splitAfterTimeinHoursperCase=hoursfromMinuteAfter.split('.');
                                                            if(splitAfterTimeinHoursperCase[0]==0)
                                                            {
                                                                splitAfterTimeinHoursperCase[0]=1;
                                                                splitAfterTimeinHoursperCase[1]=0;
                                                            }
                                                            if((splitAfterTimeinHoursperCase[1]>0)&&(splitAfterTimeinHoursperCase[1]<=25))
                                                            {
                                                                splitAfterTimeinHoursperCase[1]=15;
                                                            }
                                                            if((splitAfterTimeinHoursperCase[1]>25)&&(splitAfterTimeinHoursperCase[1]<=50))
                                                            {
                                                                splitAfterTimeinHoursperCase[1]=30;
                                                            }
                                                            if((splitAfterTimeinHoursperCase[1]>50)&&(splitAfterTimeinHoursperCase[1]<=75))
                                                            {
                                                                splitAfterTimeinHoursperCase[1]=45;
                                                            }
                                                            if((splitAfterTimeinHoursperCase[1]>75)&&(splitAfterTimeinHoursperCase[1]<99))
                                                            {
                                                                splitAfterTimeinHoursperCase[1]=0;
                                                                splitAfterTimeinHoursperCase[0]=parseInt(splitAfterTimeinHoursperCase[0])+parseInt(1);

                                                            }
                                                            var minutesAfterReRo= parseInt(splitAfterTimeinHoursperCase[0]*60)+ parseInt(splitAfterTimeinHoursperCase[1]);


                                                            rerounded=1;
                                                        }

                                                    }
                                                    if(minutesextraafter>0)
                                                    {
                                                        //business
                                                        var hoursfromMinuteBusiness=justminutesbusiness/60;
                                                        hoursfromMinuteBusiness=parseFloat(hoursfromMinuteBusiness);
                                                        hoursfromMinuteBusiness=hoursfromMinuteBusiness.toFixed(2);
                                                        //spli the case hours, round and transorm it to decimal form again
                                                        var splitBusinessTimeinHoursperCase=hoursfromMinuteBusiness.split('.');
                                                        if((splitBusinessTimeinHoursperCase[1]>0)&&(splitBusinessTimeinHoursperCase[1]<=25))
                                                        {
                                                            splitBusinessTimeinHoursperCase[1]=15;
                                                        }
                                                        if((splitBusinessTimeinHoursperCase[1]>25)&&(splitBusinessTimeinHoursperCase[1]<=50))
                                                        {
                                                            splitBusinessTimeinHoursperCase[1]=30;
                                                        }
                                                        if((splitBusinessTimeinHoursperCase[1]>50)&&(splitBusinessTimeinHoursperCase[1]<=75))
                                                        {
                                                            splitBusinessTimeinHoursperCase[1]=45;
                                                        }
                                                        if((splitBusinessTimeinHoursperCase[1]>75)&&(splitBusinessTimeinHoursperCase[1]<99))
                                                        {
                                                            splitBusinessTimeinHoursperCase[1]=0;
                                                            splitBusinessTimeinHoursperCase[0]=parseInt(splitBusinessTimeinHoursperCase[0])+parseInt(1);

                                                        }
                                                        var minutesBusinessReRo= parseInt(splitBusinessTimeinHoursperCase[0]*60)+ parseInt(splitBusinessTimeinHoursperCase[1]);


                                                        rerounded=1
                                                    }
                                                    if(minutesBusinessReRo>0)
                                                    {
                                                        var difRounded=minutesBusinessReRo-justminutesbusiness;
                                                        var minutesextraafterBefore=justminutesafter;
                                                        justminutesafter=justminutesafter-difRounded;
                                                        var minJustAfter=justminutesafter;
                                                        minutesextraafter=minutesextraafter+parseInt(difRounded);


                                                        justminutesbusiness=minutesBusinessReRo;
                                                    }
                                                    if(minutesAfterReRo>0)
                                                    {
                                                        var difRoundedA=minutesAfterReRo-justminutesafter;
                                                        var minutesextrabusinessBefore=justminutesbusiness;
                                                        justminutesbusiness=justminutesbusiness-difRoundedA;
                                                        var minJustBus=justminutesbusiness;
                                                        minutesextrabusiness=minutesextrabusiness+parseInt(difRoundedA);
                                                        justminutesafter=minutesAfterReRo;

                                                    }
                                                }

                                                activatelastItem=1;
                                            }
                                            else if(remainingminutes==0)
                                            {
                                                if(itemSO==569)
                                                {
                                                    fixedFinished=1;
                                                }
                                                if(updateCustomer==1){
                                                    customer.setFieldValue('custentity_clgx_customer_rh_currmthtl',0);
                                                }
                                                nlapiSubmitRecord(customer, false,true);
                                                lastTimeItem=timeIdsforUse[r];
                                                var minutesAfterReRo=0;
                                                var minutesBusinessReRo=0;

                                                //I finished the minutes and I reround the minutes
                                                if(rerounded==0)
                                                {
                                                    if(minutesextrabusiness>0)
                                                    {
                                                        //after
                                                        if(justminutesafter>0)
                                                        {
                                                            var hoursfromMinuteAfter=justminutesafter/60;
                                                            hoursfromMinuteAfter=parseFloat(hoursfromMinuteAfter);
                                                            hoursfromMinuteAfter=hoursfromMinuteAfter.toFixed(2);
                                                            //spli the case hours, round and transorm it to decimal form again
                                                            var splitAfterTimeinHoursperCase=hoursfromMinuteAfter.split('.');
                                                            if(splitAfterTimeinHoursperCase[0]==0)
                                                            {
                                                                splitAfterTimeinHoursperCase[0]=1;
                                                                splitAfterTimeinHoursperCase[1]=0;
                                                            }
                                                            if((splitAfterTimeinHoursperCase[1]>0)&&(splitAfterTimeinHoursperCase[1]<=25))
                                                            {
                                                                splitAfterTimeinHoursperCase[1]=15;
                                                            }
                                                            if((splitAfterTimeinHoursperCase[1]>25)&&(splitAfterTimeinHoursperCase[1]<=50))
                                                            {
                                                                splitAfterTimeinHoursperCase[1]=30;
                                                            }
                                                            if((splitAfterTimeinHoursperCase[1]>50)&&(splitAfterTimeinHoursperCase[1]<=75))
                                                            {
                                                                splitAfterTimeinHoursperCase[1]=45;
                                                            }
                                                            if((splitAfterTimeinHoursperCase[1]>75)&&(splitAfterTimeinHoursperCase[1]<99))
                                                            {
                                                                splitAfterTimeinHoursperCase[1]=0;
                                                                splitAfterTimeinHoursperCase[0]=parseInt(splitAfterTimeinHoursperCase[0])+parseInt(1);

                                                            }
                                                            var minutesAfterReRo= parseInt(splitAfterTimeinHoursperCase[0]*60)+ parseInt(splitAfterTimeinHoursperCase[1]);
                                                            rerounded=1;
                                                        }

                                                    }
                                                    if(minutesextraafter>0)
                                                    {
                                                        //business
                                                        var hoursfromMinuteBusiness=justminutesbusiness/60;
                                                        hoursfromMinuteBusiness=parseFloat(hoursfromMinuteBusiness);
                                                        hoursfromMinuteBusiness=hoursfromMinuteBusiness.toFixed(2);
                                                        //spli the case hours, round and transorm it to decimal form again
                                                        var splitBusinessTimeinHoursperCase=hoursfromMinuteBusiness.split('.');
                                                        if((splitBusinessTimeinHoursperCase[1]>0)&&(splitBusinessTimeinHoursperCase[1]<=25))
                                                        {
                                                            splitBusinessTimeinHoursperCase[1]=15;
                                                        }
                                                        if((splitBusinessTimeinHoursperCase[1]>25)&&(splitBusinessTimeinHoursperCase[1]<=50))
                                                        {
                                                            splitBusinessTimeinHoursperCase[1]=30;
                                                        }
                                                        if((splitBusinessTimeinHoursperCase[1]>50)&&(splitBusinessTimeinHoursperCase[1]<=75))
                                                        {
                                                            splitBusinessTimeinHoursperCase[1]=45;
                                                        }
                                                        if((splitBusinessTimeinHoursperCase[1]>75)&&(splitBusinessTimeinHoursperCase[1]<99))
                                                        {
                                                            splitBusinessTimeinHoursperCase[1]=0;
                                                            splitBusinessTimeinHoursperCase[0]=parseInt(splitBusinessTimeinHoursperCase[0])+parseInt(1);

                                                        }
                                                        var minutesBusinessReRo= parseInt(splitBusinessTimeinHoursperCase[0]*60)+ parseInt(splitBusinessTimeinHoursperCase[1]);
                                                        rerounded=1
                                                    }
                                                    if(minutesBusinessReRo>0)
                                                    {
                                                        var difRounded=minutesBusinessReRo-justminutesbusiness;
                                                        var minutesextraafterBefore=justminutesafter;
                                                        justminutesafter=justminutesafter-difRounded;
                                                        var minJustAfter=justminutesafter;
                                                        minutesextraafter=minutesextraafter+parseInt(difRounded);


                                                        justminutesbusiness=minutesBusinessReRo;
                                                    }
                                                    if(minutesAfterReRo>0)
                                                    {
                                                        var difRoundedA=minutesAfterReRo-justminutesafter;
                                                        var minutesextrabusinessBefore=justminutesbusiness;
                                                        justminutesbusiness=justminutesbusiness-difRoundedA;
                                                        var minJustBus=justminutesbusiness;
                                                        minutesextrabusiness=minutesextrabusiness+parseInt(difRoundedA);
                                                        justminutesafter=minutesAfterReRo;

                                                    }
                                                }
                                                activatelastItem=1;

                                            }
                                            else if(r==(timeIdsforUse.length-1)){
                                                // customer.setFieldValue('custentity_clgx_customer_rh_currmthtl',remainingminutes);
                                                // nlapiSubmitRecord(customer, false,true);


                                            }




                                        }
                                        else{
                                            if(item==544)
                                            {
                                                minutesextrabusiness=minutesextrabusiness+((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));

                                            }
                                            if(item==545)
                                            {
                                                minutesextraafter=minutesextraafter+((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));

                                            }



                                        }
                                    }
                                }


                            }
                            else
                            {
                                for(var r=0; r<timeIdsforUse.length; r++)
                                {
                                    var tbill=nlapiLoadRecord('timebill',timeIdsforUse[r]);
                                    var hourstbill=tbill.getFieldValue('hours');
                                    var item=tbill.getFieldValue('item');
                                    var tbillsplit=hourstbill.split(":");
                                    if(tbillsplit[1].charAt(0)==0)
                                    {
                                        tbillsplit[1] = tbillsplit[1].slice(1);
                                    }
                                    if(item==544)
                                    {
                                        minutesextrabusiness=minutesextrabusiness+((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));

                                    }
                                    if(item==545)
                                    {
                                        minutesextraafter=minutesextraafter+((parseInt(tbillsplit[0])*60)+parseInt(parseInt(tbillsplit[1])));

                                    }

                                }

                                var casesBusinessUsedArraySplit= new Array();
                                var valuesBusinessUsedArraySplit=new Array();
                                var changedBusinessUsedValue=0;
                                var oldUsedMinBusiness=0;
                                if((oldUsedCaseBusiness!=null)&&(oldUsedCaseBusiness!=''))
                                {
                                    var splitcasesBuisnessUsed=oldUsedCaseBusiness.split(';');
                                    for(var h=0;h<splitcasesBuisnessUsed.length;h++)
                                    {
                                        if(splitcasesBuisnessUsed[h]!='')
                                        {
                                            var splcasebus=splitcasesBuisnessUsed[h].split('=');
                                            casesBusinessUsedArraySplit.push(splcasebus[0]);
                                            valuesBusinessUsedArraySplit.push(splcasebus[1]);
                                        }

                                    }
                                    for(var j=0;j<casesBusinessUsedArraySplit.length;j++)
                                    {
                                        if(caseid==casesBusinessUsedArraySplit[j])
                                        {


                                            oldUsedMinBusiness=valuesBusinessUsedArraySplit[j];


                                        }

                                    }
                                }
                                if((oldUsedCaseAfter!=null)&&(oldUsedCaseAfter!=''))
                                {
                                    var casesAfterUsedArraySplit= new Array();
                                    var valuesAfterUsedArraySplit=new Array();
                                    var oldUsedMinAfter=0;
                                    var splitcasesAfterUsed=oldUsedCaseAfter.split(';');
                                    for(var h=0;h<splitcasesAfterUsed.length;h++)
                                    {
                                        if(splitcasesAfterUsed[h]!='')
                                        {
                                            var splcaseafter=splitcasesAfterUsed[h].split('=');
                                            casesAfterUsedArraySplit.push(splcaseafter[0]);
                                            valuesAfterUsedArraySplit.push(splcaseafter[1]);
                                        }

                                    }
                                    for(var j=0;j<casesAfterUsedArraySplit.length;j++)
                                    {
                                        if(caseid==casesAfterUsedArraySplit[j])
                                        {


                                            oldUsedMinAfter=valuesAfterUsedArraySplit[j];


                                        }

                                    }
                                }
                                if((oldCaseBusiness!=null)&&(oldCaseBusiness!=''))
                                {
                                    var casesBusinessArraySplit= new Array();
                                    var valuesBusinessArraySplit=new Array();
                                    var oldMinBusiness=0;
                                    var splitcasesBuisness=oldCaseBusiness.split(';');

                                    for(var h=0;h<splitcasesBuisness.length;h++)
                                    {
                                        if(splitcasesBuisness[h]!='')
                                        {
                                            shouldSummBusinessExtra++;

                                            var splcasebus1=splitcasesBuisness[h].split('=');
                                            casesBusinessArraySplit.push(splcasebus1[0]);
                                            valuesBusinessArraySplit.push(splcasebus1[1]);
                                        }

                                    }
                                    for(var j=0;j<casesBusinessArraySplit.length;j++)
                                    {
                                        if(caseid==casesBusinessArraySplit[j])
                                        {


                                            oldMinBusiness=valuesBusinessArraySplit[j];


                                        }

                                    }
                                }

                                if((oldCaseAfter!=null)&&(oldCaseAfter!=''))
                                {
                                    var casesAfterArraySplit= new Array();
                                    var valuesAfterArraySplit=new Array();
                                    var oldMinAfter=0;
                                    var splitcasesAfter=oldCaseAfter.split(';');

                                    for(var h=0;h<splitcasesAfter.length;h++)
                                    {
                                        if(splitcasesAfter[h]!=''){
                                            shouldSummAfterExtra++;
                                            var splcaseafter1=splitcasesAfter[h].split('=');
                                            casesAfterArraySplit.push(splcaseafter1[0]);
                                            valuesAfterArraySplit.push(splcaseafter1[1]);
                                        }

                                    }
                                    for(var j=0;j<casesAfterArraySplit.length;j++)
                                    {
                                        if(caseid==casesAfterArraySplit[j])
                                        {


                                            oldMinAfter=valuesAfterArraySplit[j];


                                        }

                                    }
                                }
                                if(oldUsedMinBusiness>0)
                                {
                                    var minutesBusiness=parseInt(oldUsedCaseBusiness)+parseInt(minutesextrabusiness)+parseInt(oldMinBusiness);
                                    var hoursfromMinuteBusiness=minutesBusiness/60;
                                    hoursfromMinuteBusiness=parseFloat(hoursfromMinuteBusiness);
                                    hoursfromMinuteBusiness=hoursfromMinuteBusiness.toFixed(2);
                                    //spli the case hours, round and transorm it to decimal form again
                                    var splitBusinessTimeinHoursperCase=hoursfromMinuteBusiness.split('.');
                                    if((splitBusinessTimeinHoursperCase[1]>0)&&(splitBusinessTimeinHoursperCase[1]<=25))
                                    {
                                        splitBusinessTimeinHoursperCase[1]=15;
                                    }
                                    if((splitBusinessTimeinHoursperCase[1]>25)&&(splitBusinessTimeinHoursperCase[1]<=50))
                                    {
                                        splitBusinessTimeinHoursperCase[1]=30;
                                    }
                                    if((splitBusinessTimeinHoursperCase[1]>50)&&(splitBusinessTimeinHoursperCase[1]<=75))
                                    {
                                        splitBusinessTimeinHoursperCase[1]=45;
                                    }
                                    if((splitBusinessTimeinHoursperCase[1]>75)&&(splitBusinessTimeinHoursperCase[1]<99))
                                    {
                                        splitBusinessTimeinHoursperCase[1]=0;
                                        splitBusinessTimeinHoursperCase[0]=parseInt(splitBusinessTimeinHoursperCase[0])+parseInt(1);

                                    }
                                    var minutesBusinessReRo= parseInt(splitBusinessTimeinHoursperCase[0]*60)+ parseInt(splitBusinessTimeinHoursperCase[1]);
                                    var minutesextrabusiness1=minutesBusinessReRo-oldUsedMinBusiness-oldMinBusiness;
                                    if(minutesextrabusiness1==oldUsedMinBusiness)
                                    {
                                        minutesextrabusiness=0;
                                    }
                                    else{
                                        minutesextrabusiness=minutesextrabusiness1;
                                    }
                                    reroundedExtraBusiness=1;
                                }
                                if(oldUsedMinAfter>0)
                                {
                                    var minutesAfter=parseInt(oldUsedMinAfter)+parseInt(minutesextraafter)+parseInt(oldMinAfter);
                                    var hoursfromMinuteAfter=minutesAfter/60;
                                    hoursfromMinuteAfter=parseFloat(hoursfromMinuteAfter);
                                    hoursfromMinuteAfter=hoursfromMinuteAfter.toFixed(2);
                                    //spli the case hours, round and transorm it to decimal form again
                                    var splitAfterTimeinHoursperCase=hoursfromMinuteAfter.split('.');
                                    if(splitAfterTimeinHoursperCase[0]==0)
                                    {
                                        splitAfterTimeinHoursperCase[0]=1;
                                        splitAfterTimeinHoursperCase[1]=0;
                                    }
                                    if((splitAfterTimeinHoursperCase[1]>0)&&(splitAfterTimeinHoursperCase[1]<=25))
                                    {
                                        splitAfterTimeinHoursperCase[1]=15;
                                    }
                                    if((splitAfterTimeinHoursperCase[1]>25)&&(splitAfterTimeinHoursperCase[1]<=50))
                                    {
                                        splitAfterTimeinHoursperCase[1]=30;
                                    }
                                    if((splitAfterTimeinHoursperCase[1]>50)&&(splitAfterTimeinHoursperCase[1]<=75))
                                    {
                                        splitAfterTimeinHoursperCase[1]=45;
                                    }
                                    if((splitAfterTimeinHoursperCase[1]>75)&&(splitAfterTimeinHoursperCase[1]<99))
                                    {
                                        splitAfterTimeinHoursperCase[1]=0;
                                        splitAfterTimeinHoursperCase[0]=parseInt(splitAfterTimeinHoursperCase[0])+parseInt(1);

                                    }
                                    var minutesAfterReRo= parseInt(splitAfterTimeinHoursperCase[0]*60)+ parseInt(splitAfterTimeinHoursperCase[1]);
                                    var minutesextraafter1=minutesAfterReRo-oldUsedMinAfter-oldMinAfter;
                                    if(minutesextraafter1==oldUsedMinAfter)
                                    {
                                        minutesextraafter=0;
                                    }
                                    else{
                                        minutesextraafter=minutesextraafter1;
                                    }
                                    reroundedExtraAfter=1;


                                }



                            }
                            if(searchRemote1==null)
                            {
                                var customer=nlapiLoadRecord('customer',customerid);
                                var customer_rh_currmthstart=customer.getFieldValue('custentity_clgx_customer_rh_currmthstart');
                                var dateCustomer=new Date(customer_rh_currmthstart);
                                var monthCustomer=dateCustomer.getMonth();
                                var monthNameCustomer=monthNames[monthCustomer];
                                //if(monthNameCustomer!=currMthName){
                                customer.setFieldValue('custentity_clgx_customer_rh_currmthstart',stStartDateRH);
                                customer.setFieldValue('custentity_clgx_customer_rh_currmthend',stEndDateRH);
                                //}
                                var casecustomernumber=customer.getFieldValue('custentity_clgx_customer_rh_casenumber');
                                if((casecustomernumber!=null)&&(casecustomernumber!='')){
                                    if((casecustomernumber.indexOf(casenumber)==-1))
                                    {
                                        casecustomernumber='#'+casecustomernumber+', #'+casenumber;
                                    }
                                }
                                else{
                                    casecustomernumber=casenumber;
                                }
                                if((monthName==currMthName)){
                                    customer.setFieldValue('custentity_clgx_customer_rh_casenumber',casecustomernumber);
                                }
                                var socustomer=  customer.getFieldValues('custentity_clgx_customer_rh_so');
                                nlapiSubmitRecord(customer, false,true);

                                //create a new RH Record
                                var newRecordRemote = nlapiCreateRecord('customrecord_clgx_remote_hands');
                                newRecordRemote.setFieldValue('custrecord_clgx_rh_location', customerlocation);
                                newRecordRemote.setFieldValue('custrecord_clgx_rh_customer', customerid);
                                newRecordRemote.setFieldValue('custrecord_clgx_rh_from_date', stStartDate);
                                newRecordRemote.setFieldValue('custrecord_clgx_rh_to_date', stEndDate);
                                if(lastTimeItem>0)
                                {
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_ltimeid', lastTimeItem);
                                    if(lastTimeItem==544)
                                    {
                                        newRecordRemote.setFieldValue('custrecord_clgx_rh_ltimetype', 'business');

                                    }
                                    if(lastTimeItem==544)
                                    {
                                        newRecordRemote.setFieldValue('custrecord_clgx_rh_ltimetype', 'business');

                                    }

                                }
                                if((itemSO!='')&&(itemSO!=null))
                                {
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_package', itemSO);
                                }
                                if(fixedFinished==1)
                                {
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_fixedfinished', 'T');
                                }
                                if((numberHoursSO!='')&&(numberHoursSO!=null))
                                {
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_packagehours', numberHoursSO);
                                }
                                if((itemRate!='')&&(itemRate!=null))
                                {
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_itemrate', itemRate);
                                }
                                if(remainingminutes>0){
                                    if(justminutesbusiness>0)
                                    {
                                        if(roundedBus==0)
                                        {
                                            var hoursfromMinuteBusiness=justminutesbusiness/60;
                                            hoursfromMinuteBusiness=parseFloat(hoursfromMinuteBusiness);
                                            hoursfromMinuteBusiness=hoursfromMinuteBusiness.toFixed(2);
                                            //spli the case hours, round and transorm it to decimal form again
                                            var splitBusinessTimeinHoursperCase=hoursfromMinuteBusiness.split('.');
                                            if((splitBusinessTimeinHoursperCase[1]>0)&&(splitBusinessTimeinHoursperCase[1]<=25))
                                            {
                                                splitBusinessTimeinHoursperCase[1]=15;
                                            }
                                            if((splitBusinessTimeinHoursperCase[1]>25)&&(splitBusinessTimeinHoursperCase[1]<=50))
                                            {
                                                splitBusinessTimeinHoursperCase[1]=30;
                                            }
                                            if((splitBusinessTimeinHoursperCase[1]>50)&&(splitBusinessTimeinHoursperCase[1]<=75))
                                            {
                                                splitBusinessTimeinHoursperCase[1]=45;
                                            }
                                            if((splitBusinessTimeinHoursperCase[1]>75)&&(splitBusinessTimeinHoursperCase[1]<99))
                                            {
                                                splitBusinessTimeinHoursperCase[1]=0;
                                                splitBusinessTimeinHoursperCase[0]=parseInt(splitBusinessTimeinHoursperCase[0])+parseInt(1);

                                            }
                                            var minutesBusiness= parseInt(splitBusinessTimeinHoursperCase[0]*60)+ parseInt(splitBusinessTimeinHoursperCase[1]);
                                        }
                                    }
                                    else{
                                        var minutesBusiness= 0;
                                    }
                                    //set the business minutes used
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_roundedbus',caseid+'='+parseInt(minutesBusiness)+';');
                                    if(justminutesafter>0)
                                    {
                                        if(roundedAfter==0)
                                        {
                                            var hoursfromMinuteAfter=justminutesafter/60;
                                            hoursfromMinuteAfter=parseFloat(hoursfromMinuteAfter);
                                            hoursfromMinuteAfter=hoursfromMinuteAfter.toFixed(2);
                                            //spli the case hours, round and transorm it to decimal form again
                                            var splitAfterTimeinHoursperCase=hoursfromMinuteAfter.split('.');
                                            if(splitAfterTimeinHoursperCase[0]==0)
                                            {
                                                splitAfterTimeinHoursperCase[0]=1;
                                                splitAfterTimeinHoursperCase[1]=0;
                                            }
                                            if((splitAfterTimeinHoursperCase[1]>0)&&(splitAfterTimeinHoursperCase[1]<=25))
                                            {
                                                splitAfterTimeinHoursperCase[1]=15;
                                            }
                                            if((splitAfterTimeinHoursperCase[1]>25)&&(splitAfterTimeinHoursperCase[1]<=50))
                                            {
                                                splitAfterTimeinHoursperCase[1]=30;
                                            }
                                            if((splitAfterTimeinHoursperCase[1]>50)&&(splitAfterTimeinHoursperCase[1]<=75))
                                            {
                                                splitAfterTimeinHoursperCase[1]=45;
                                            }
                                            if((splitAfterTimeinHoursperCase[1]>75)&&(splitAfterTimeinHoursperCase[1]<99))
                                            {
                                                splitAfterTimeinHoursperCase[1]=0;
                                                splitAfterTimeinHoursperCase[0]=parseInt(splitAfterTimeinHoursperCase[0])+parseInt(1);

                                            }
                                            var minutesAfter= parseInt(splitAfterTimeinHoursperCase[0]*60)+ parseInt(splitAfterTimeinHoursperCase[1]);

                                        }
                                    }

                                    else{
                                        var minutesAfter=0;
                                    }
                                    //set the after minutes used
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_roundedafter', caseid+'='+parseInt(minutesAfter)+';');
                                    var totalminutesUsed=parseInt(minutesAfter)+parseInt(minutesBusiness);
                                    var customer=nlapiLoadRecord('customer',customerid);
                                    var remainingCustomer=parseInt(TotalMinutesRemaning)-parseInt(totalminutesUsed);
                                    if(remainingCustomer>=0){
                                        if(updateCustomer==1){
                                            customer.setFieldValue('custentity_clgx_customer_rh_currmthtl',remainingCustomer);
                                        }
                                    }
                                    nlapiSubmitRecord(customer, false,true);
                                }
                                else{
                                    var totalminutesUsed=parseInt(justminutesafter)+parseInt(justminutesbusiness);
                                    //set the after minutes used
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_roundedafter', caseid+'='+parseInt(justminutesafter)+';');
                                    //set the business minutes used
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_roundedbus',caseid+'='+parseInt(justminutesbusiness)+';');


                                }




                                if(totalminutesUsed>0)
                                {
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_time_used', parseInt(totalminutesUsed));
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_usedtimecasenumber', caseid+'='+totalminutesUsed+';');
                                }
                                else{
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_time_used',0);
                                }
                                if(extrabusiness>0)
                                {
                                    minutesextrabusiness=parseInt(minutesextrabusiness)+parseInt(extrabusiness);
                                }
                                if(extraafter>0)
                                {
                                    minutesextraafter=parseInt(minutesextraafter)+parseInt(extraafter);
                                }
                                if(minutesextrabusiness>0)
                                {
                                    if(roundedBus>0)
                                    {
                                        var hoursfromMinuteBusiness=(parseInt(minutesextrabusiness)+parseInt(minJustBus))/60;
                                        hoursfromMinuteBusiness=parseFloat(hoursfromMinuteBusiness);
                                        hoursfromMinuteBusiness=hoursfromMinuteBusiness.toFixed(2);
                                        //spli the case hours, round and transorm it to decimal form again
                                        var splitBusinessTimeinHoursperCase=hoursfromMinuteBusiness.split('.');
                                        if((splitBusinessTimeinHoursperCase[1]>0)&&(splitBusinessTimeinHoursperCase[1]<=25))
                                        {
                                            splitBusinessTimeinHoursperCase[1]=15;
                                        }
                                        if((splitBusinessTimeinHoursperCase[1]>25)&&(splitBusinessTimeinHoursperCase[1]<=50))
                                        {
                                            splitBusinessTimeinHoursperCase[1]=30;
                                        }
                                        if((splitBusinessTimeinHoursperCase[1]>50)&&(splitBusinessTimeinHoursperCase[1]<=75))
                                        {
                                            splitBusinessTimeinHoursperCase[1]=45;
                                        }
                                        if((splitBusinessTimeinHoursperCase[1]>75)&&(splitBusinessTimeinHoursperCase[1]<99))
                                        {
                                            splitBusinessTimeinHoursperCase[1]=0;
                                            splitBusinessTimeinHoursperCase[0]=parseInt(splitBusinessTimeinHoursperCase[0])+parseInt(1);

                                        }
                                        var minutesBusiness= parseInt(splitBusinessTimeinHoursperCase[0]*60)+ parseInt(splitBusinessTimeinHoursperCase[1]);
                                        minutesBusiness=minutesBusiness-minJustBus;
                                    }
                                    else if(reroundedExtraBusiness==0){
                                        var hoursfromMinuteBusiness=parseInt(minutesextrabusiness)/60;
                                        hoursfromMinuteBusiness=parseFloat(hoursfromMinuteBusiness);
                                        hoursfromMinuteBusiness=hoursfromMinuteBusiness.toFixed(2);
                                        //spli the case hours, round and transorm it to decimal form again
                                        var splitBusinessTimeinHoursperCase=hoursfromMinuteBusiness.split('.');
                                        if((splitBusinessTimeinHoursperCase[1]>0)&&(splitBusinessTimeinHoursperCase[1]<=25))
                                        {
                                            splitBusinessTimeinHoursperCase[1]=15;
                                        }
                                        if((splitBusinessTimeinHoursperCase[1]>25)&&(splitBusinessTimeinHoursperCase[1]<=50))
                                        {
                                            splitBusinessTimeinHoursperCase[1]=30;
                                        }
                                        if((splitBusinessTimeinHoursperCase[1]>50)&&(splitBusinessTimeinHoursperCase[1]<=75))
                                        {
                                            splitBusinessTimeinHoursperCase[1]=45;
                                        }
                                        if((splitBusinessTimeinHoursperCase[1]>75)&&(splitBusinessTimeinHoursperCase[1]<99))
                                        {
                                            splitBusinessTimeinHoursperCase[1]=0;
                                            splitBusinessTimeinHoursperCase[0]=parseInt(splitBusinessTimeinHoursperCase[0])+parseInt(1);

                                        }
                                        var minutesBusiness= parseInt(splitBusinessTimeinHoursperCase[0]*60)+ parseInt(splitBusinessTimeinHoursperCase[1]);

                                    }
                                }
                                else{
                                    var minutesBusiness= 0;
                                }
                                if(minutesextraafter>0)
                                {
                                    if((roundedOneTimeAfter==0)&&(reroundedExtraAfter==0))
                                    {
                                        if(roundedAfter>0)
                                        {
                                            var hoursfromMinuteAfter=(parseInt(minutesextraafter)+parseInt(minJustAfter))/60;
                                            hoursfromMinuteAfter=parseFloat(hoursfromMinuteAfter);
                                            hoursfromMinuteAfter=hoursfromMinuteAfter.toFixed(2);
                                            //spli the case hours, round and transorm it to decimal form again
                                            var splitAfterTimeinHoursperCase=hoursfromMinuteAfter.split('.');
                                            if(splitAfterTimeinHoursperCase[0]==0)
                                            {
                                                splitAfterTimeinHoursperCase[0]=1;
                                                splitAfterTimeinHoursperCase[1]=0;
                                            }
                                            if((splitAfterTimeinHoursperCase[1]>0)&&(splitAfterTimeinHoursperCase[1]<=25))
                                            {
                                                splitAfterTimeinHoursperCase[1]=15;
                                            }
                                            if((splitAfterTimeinHoursperCase[1]>25)&&(splitAfterTimeinHoursperCase[1]<=50))
                                            {
                                                splitAfterTimeinHoursperCase[1]=30;
                                            }
                                            if((splitAfterTimeinHoursperCase[1]>50)&&(splitAfterTimeinHoursperCase[1]<=75))
                                            {
                                                splitAfterTimeinHoursperCase[1]=45;
                                            }
                                            if((splitAfterTimeinHoursperCase[1]>75)&&(splitAfterTimeinHoursperCase[1]<99))
                                            {
                                                splitAfterTimeinHoursperCase[1]=0;
                                                splitAfterTimeinHoursperCase[0]=parseInt(splitAfterTimeinHoursperCase[0])+parseInt(1);

                                            }
                                            var minutesAfter= parseInt(splitAfterTimeinHoursperCase[0]*60)+ parseInt(splitAfterTimeinHoursperCase[1]);
                                            minutesAfter= parseInt(minutesAfter)-parseInt(minJustAfter);
                                        }
                                        else{
                                            var hoursfromMinuteAfter=parseInt(minutesextraafter)/60;
                                            hoursfromMinuteAfter=parseFloat(hoursfromMinuteAfter);
                                            hoursfromMinuteAfter=hoursfromMinuteAfter.toFixed(2);
                                            //spli the case hours, round and transorm it to decimal form again
                                            var splitAfterTimeinHoursperCase=hoursfromMinuteAfter.split('.');
                                            if(splitAfterTimeinHoursperCase[0]==0)
                                            {
                                                splitAfterTimeinHoursperCase[0]=1;
                                                splitAfterTimeinHoursperCase[1]=0;
                                            }
                                            if((splitAfterTimeinHoursperCase[1]>0)&&(splitAfterTimeinHoursperCase[1]<=25))
                                            {
                                                splitAfterTimeinHoursperCase[1]=15;
                                            }
                                            if((splitAfterTimeinHoursperCase[1]>25)&&(splitAfterTimeinHoursperCase[1]<=50))
                                            {
                                                splitAfterTimeinHoursperCase[1]=30;
                                            }
                                            if((splitAfterTimeinHoursperCase[1]>50)&&(splitAfterTimeinHoursperCase[1]<=75))
                                            {
                                                splitAfterTimeinHoursperCase[1]=45;
                                            }
                                            if((splitAfterTimeinHoursperCase[1]>75)&&(splitAfterTimeinHoursperCase[1]<99))
                                            {
                                                splitAfterTimeinHoursperCase[1]=0;
                                                splitAfterTimeinHoursperCase[0]=parseInt(splitAfterTimeinHoursperCase[0])+parseInt(1);

                                            }
                                            var minutesAfter= parseInt(splitAfterTimeinHoursperCase[0]*60)+ parseInt(splitAfterTimeinHoursperCase[1]);

                                        }
                                    }
                                    else{
                                        var minutesAfter=parseInt(minutesextraafter);
                                    }
                                }

                                else{
                                    var minutesAfter= 0;
                                }
                                if(minutesBusiness>0)
                                {
                                    minutesBusiness=parseInt(minutesBusiness);
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_time_biz_hours', minutesBusiness);
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_businesstimecase', caseid+'='+minutesBusiness+';');
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_biz_hours_rate', totalratebusinessH);
                                }
                                else{
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_time_biz_hours', 0);
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_biz_hours_rate', 0);

                                }
                                if(minutesAfter>0)
                                {
                                    minutesAfter=parseInt(minutesAfter);
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_time_after_hours', minutesAfter);
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_aftertimecase', caseid+'='+minutesAfter+';');
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_after_hours_rate', totalrateafterH);

                                }
                                else{
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_after_hours_rate', 0);
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_time_after_hours', 0);

                                }


                                newRecordRemote.setFieldValue('custrecord_clgx_rh_cases', casenumber);
                                newRecordRemote.setFieldValue('custrecord_clgx_rh_casesids', caseid);
                                newRecordRemote.setFieldValue('custrecord_clgx_rh_times', timeIdsforUse);
                                newRecordRemote.setFieldValue('custrecord_clgx_rh_casesroundedh', caseid+'='+parseInt(parseInt(totalminutesUsed)+parseInt(minutesAfter)+parseInt(minutesBusiness))+';');
                                if((soFreezing!='')&&(soFreezing!=null))
                                {
                                    newRecordRemote.setFieldValue('custrecord_clgx_rh_service_order',socustomer);
                                }
                                // custrecord_clgx_rh_businesstimecase
                                //custrecord_clgx_rh_aftertimecase
                                //custrecord_clgx_rh_ltimeid
                                //custrecord_clgx_rh_casesroundedh


                                nlapiSubmitRecord(newRecordRemote, false,true);
                                /*    update the customer remote hands fields
                                 */
                      /*      }

                        }
                    }
                }



                var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
                nlapiLogExecution('DEBUG','Finished Execution', 'Total Remaining Usage - ' + context.getRemainingUsage() + 'Total Usage-' + usageConsumtion + '-------------------------- Finished Scheduled Script --------------------------|');

            }
        }*/
        var x=0;
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}

function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}

function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}

function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}