nlapiLogExecution("audit","FLOStart",new Date().getTime());
/**
 * Created by catalinataran on 2014-10-30.
 */
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Freezing_SOs.js
//	Script Name:	CLGX_SS_Freezing_SOs
//	Script Id:	     customscript_clgx_ss_freezing_sos
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Case
//	@authors:	Catalina Taran - catalina.taran@cologix.com
//-------------------------------------------------------------------------------------------------
function scheduled_create_freezingrh_records(){
    try{
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 2/2/2011
// Details:	Consolidate invoices.
//-------------------------------------------------------------------------------------------------
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');
        var startScript = moment();

        var context = nlapiGetContext();
        var initialTime = moment();
        var hour = moment().hour();
        // if(hour < 10 || hour > 14){ // before 7 AM and after 6 PM on east coast
        var reschedMinutes = 5;
        //}
        //else{
        //   var reschedMinutes = 50;
        // }
        var date=new Date();
        var month=date.getMonth();
        var monthNames = [ "January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December" ];
        var monthName=monthNames[month-1];
        // var monthName="June";
        var arrDates  = getDateRange(-1);
        var startDate = arrDates[0];
        var endDate  = arrDates[1];
        var yr=date.getFullYear();
        month=month.toString();
        yr=yr.toString();
        var searchColumn = new Array();
        var searchFilter=new Array();
        // searchColumn.push(new nlobjSearchColumn('custrecord_clgx_freezing_rh_customer',null,'GROUP'));
        searchFilter.push(new nlobjSearchFilter('custrecord_clgx_freezing_rh_mth',null,'is',monthName));
        searchFilter.push(new nlobjSearchFilter('custrecord_clgx_freezing_rh_yr',null,'is',yr));
        var searchFRH = nlapiLoadSearch('customrecord_freezing_rh_pack','customsearch_frremotehands_allcustomers');
        searchFRH.addColumns(searchColumn);
        searchFRH.addFilters(searchFilter);
        var resultFRH = searchFRH.runSearch();
        var arrCustomers = new Array();
        resultFRH.forEachResult(function(searchResult) {
            var columns = searchResult.getAllColumns();
            var customerid = searchResult.getValue(columns[0]);
            arrCustomers.push(customerid);
            return true; // return true to keep iterating

        });
        var arrColumns = new Array();
        var arrFilters = new Array();
        if(arrCustomers.length>0)
        {
            arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof", arrCustomers));
        }
        var searchServices = nlapiLoadSearch('customer', 'customsearch_clgx_customer_rh_list');
        var resultSet = searchServices.runSearch();
        var customerIds = new Array();
        resultSet.forEachResult(function(searchResult) {

            var customerid = searchResult.getValue('internalid',null,null);
            if((!in_array (customerid, arrCustomers))&&(customerid!=20029)&&(customerid!=165)&&(customerid!=473473)&&(customerid!=500789)&&(customerid!=315477)&&(customerid!=11239)&&(customerid!=7663)&&(customerid!=3)&&(customerid!=11240)&&(customerid!=317320)&&(customerid!=317525)&&(customerid!=10940)&&(customerid!=2773)&&(customerid!=215339)&&(customerid!=19434)){
                customerIds.push(customerid);
            }

            return true; // return true to keep iterating

        });
        for(var t=0; customerIds!= null && t <  customerIds.length; t++ )
        {
            var arrColumns = new Array();
            var arrFilters = new Array();
            //     arrFilters.push(new nlobjSearchFilter("saleseffectivedate", null, "within",startDate,endDate));
            arrFilters.push(new nlobjSearchFilter("entity",null,"anyof", customerIds[t]));
            var searchRemote = nlapiSearchRecord('transaction', 'customsearch_clgx_so_remoteh1', arrFilters, arrColumns);
            var created=0;
            if(searchRemote!=null)
            {
                var startExecTime = moment();
                var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);

                if ( (context.getRemainingUsage() <= 300 || totalMinutes > reschedMinutes) && (t+1) < customerIds.length ){
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    if ( status == 'QUEUED' ) {
                        nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                        break;
                    }
                }
                for(var v=0; searchRemote!= null && v <  searchRemote.length; v++ )
                {

                    var searchRem = searchRemote[v];
                    var columns = searchRem.getAllColumns();
                    var soid=searchRem.getValue(columns[0]);
                    var salesefectivedate=searchRem.getValue(columns[1]);
                    var customer=searchRem.getValue(columns[2]);
                    var location=searchRem.getValue(columns[3]);
                    var status=searchRem.getValue(columns[4]);
                    var bussrate=searchRem.getValue(columns[5]);
                    var afterrate=searchRem.getValue(columns[6]);
                    var numberHours=0;
                    var soItemRH=0;
                    var servOrder=nlapiLoadRecord('salesorder',soid);
                    var NRItems = servOrder.getLineItemCount('item');
                    var found=0;
                    for ( var j = 1; j <= NRItems; j++ ) {
                        var item = servOrder.getLineItemValue('item', 'item', j);
                        var billingSchedule=servOrder.getLineItemValue('item','billingschedule_display',j);
                        if((item==568)&&((billingSchedule!=null)))
                        {

                            numberHours=numberHours+parseFloat(servOrder.getLineItemValue('item', 'custcol_clgx_qty2print', j)*60);
                            var  itemRate=servOrder.getLineItemValue('item', 'rate', j);
                            var onorder=servOrder.getLineItemValue('item', 'quantitybilled', j);
                            var initquantity=servOrder.getLineItemValue('item', 'quantity', j);
                            soItemRH=item;
                            found=1;

                        }else if((item==569)&&((billingSchedule!=null))&&found==0)
                        {

                            numberHours=numberHours+(parseFloat(servOrder.getLineItemValue('item', 'custcol_clgx_qty2print', j))*60);
                            var itemRate=servOrder.getLineItemValue('item', 'rate', j);
                            var onorder=servOrder.getLineItemValue('item', 'quantitybilled', j);
                            var initquantity=servOrder.getLineItemValue('item', 'quantity', j);
                            soItemRH=item;

                        }


                    }
                    var itemquantity=numberHours;
                    var socustomer=searchRem.getValue(columns[7]);
                    var timeleft=searchRem.getValue(columns[8]);
                    initquantity=parseInt(initquantity);
                    onorder=parseInt(onorder);
                    // if(initquantity>onorder)
                    // {
                    var newFreezingRH=nlapiCreateRecord('customrecord_freezing_rh_pack');
                    var dateS=new Date(salesefectivedate);
                    var mth=dateS.getMonth();
                    var year=dateS.getFullYear();
                    mth=mth.toString();
                    year=year.toString();
                    newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_mth', monthName);
                    newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_yr',yr);
                    newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_customer',customer);
                    newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_location',location);
                    newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_brate',bussrate);
                    newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_arate',afterrate);

                    if((soItemRH==569)||(soItemRH==568))
                    {
                        var customerRec=nlapiLoadRecord('customer', customer);
                        newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_so',soid);
                        newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_item',soItemRH);
                        newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_irate',itemRate);
                        if(soItemRH==569)
                        {
                            if(soid==socustomer)
                            {
                                newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_iqty',timeleft);
                            }
                            else{

                                newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_iqty',itemquantity);
                                customerRec.setFieldValue('custentity_clgx_customer_rh_currmthtl',parseInt(itemquantity));
                            }
                        }
                        else{

                            newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_iqty',itemquantity);
                            customerRec.setFieldValue('custentity_clgx_customer_rh_currmthtl',parseInt(itemquantity));
                        }
                        customerRec.setFieldValue('custentity_clgx_customer_rh_pack',soItemRH);
                        customerRec.setFieldValue('custentity_clgx_customer_rh_so',soid);
                        var emptycasenumber='';
                        customerRec.setFieldValue('custentity_clgx_customer_rh_casenumber',emptycasenumber);
                        nlapiSubmitRecord(customerRec, false,true);
                    }


                    var freeID=nlapiSubmitRecord(newFreezingRH, false,true);
                    var created=1;
                    break;
                    // }

                }




            }
            if(created==0)
            {
                var startExecTime = moment();
                var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);

                if ( (context.getRemainingUsage() <= 300 || totalMinutes > 50) && (t+1) < customerIds.length ){
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    if ( status == 'QUEUED' ) {
                        nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                        break;
                    }
                }
                var emptycasenumber='';
                var customerRec=nlapiLoadRecord('customer',customerIds[t] );
                customerRec.setFieldValue('custentity_clgx_customer_rh_casenumber',emptycasenumber);
                nlapiSubmitRecord(customerRec, false,true);
                var newFreezingRH=nlapiCreateRecord('customrecord_freezing_rh_pack');
                var totalratebusinessH=customerRec.getFieldValue('custentity_clgx_normal_hourly_rate');
                var totalrateafterH=customerRec.getFieldValue('custentity_clgx_after_bus_hour');
                newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_mth', monthName);
                newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_yr',yr);
                newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_customer',customerIds[t]);
                newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_brate',totalratebusinessH);
                newFreezingRH.setFieldValue('custrecord_clgx_freezing_rh_arate',totalrateafterH);
                var freeID=nlapiSubmitRecord(newFreezingRH, false,true);
            }
        }

        var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
        var index = t + 1;
        nlapiLogExecution('DEBUG','SO ', ' | freezingrhID =  ' + freeID  + ' | Usage - '+ usageConsumtion + '  |');
        var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
        nlapiLogExecution('DEBUG','Finished Execution', 'Total Remaining Usage - ' + context.getRemainingUsage() + 'Total Usage-' + usageConsumtion + '-------------------------- Finished Scheduled Script --------------------------|');

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
function getDateRange(monthsago){

    var today = new Date();
    var date = nlapiAddMonths(today, monthsago);

    var month = parseInt(date.getMonth());
    var year = date.getFullYear();

    var stDays  = daysInMonth(parseInt(month),parseInt(year));
    var stYear  = year;

    var stMonth = parseInt(month) + 1;

    var stStartDate = stMonth + '/1/' + stYear;
    var stEndDate   = stMonth + '/' + stDays + '/' + stYear;

    var arrDateRange = new Array();
    arrDateRange[0] = stStartDate;
    arrDateRange[1] =stEndDate;

    return arrDateRange;
}

function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}

//check if a value is in the array
function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}
//check if a value is in the array
function in_array (val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;


}