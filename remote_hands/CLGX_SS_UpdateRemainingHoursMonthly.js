nlapiLogExecution("audit","FLOStart",new Date().getTime());
/**
      * Created by catalinataran on 2014-10-01.
      */
    //-------------------------------------------------------------------------------------------------
    //	Script File:	CLGX_SS_UpdateRemainingHoursMonthly.js
    //	Script Name:	CLGX_SS_UpdateRemainingHoursMonthly
    //	Script Id:	    customscript_clgx_ss_updateremaininghoursmonthly
    //	Script Runs:	On Server
    //	Script Type:	Scheduled Script
    //	Deployments:	Customer
    //	@authors:	Catalina Taran - catalina.taran@cologix.com
    //	Created:	10/01/2014
    //-------------------------------------------------------------------------------------------------
function scheduled_customer_remote_hands_update(){
    try{
        //------------- Begin Section 1 -------------------------------------------------------------------
        //for each SO Pending Billing Monthly Hours Pack the script will reset the Remote Hands remaining time
        //-------------------------------------------------------------------------------------------------
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');
        var arrayFilters=new Array();
        var arrayColumns=new Array();
        var searchSO = nlapiSearchRecord('transaction', 'customsearch_clgx_so_remotehandsmonthlyh', arrayFilters, arrayColumns);
        for ( var j = 0; searchSO != null && j < searchSO.length; j++ ) {
            var search = searchSO[j];
            var columns = search.getAllColumns();
            var numberH=search.getValue(columns[0]);
            var customerID=search.getValue(columns[1]);
            var customer=nlapiLoadRecord('customer',customerID);
            customer.setFieldValue('custentity_clgx_customer_rh_currmthtl',parseFloat(numberH));
            nlapiSubmitRecord(customer, false,true);
        }
        nlapiLogExecution('DEBUG','Finished Execution', '|-------------------------- Finished Scheduled Script --------------------------|');
    }

    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
