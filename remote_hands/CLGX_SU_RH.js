//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_RH.js
//	Script Name:	CLGX_SU_RH
//	Script Id:		customscript_CLGX_SU_RH
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Sales Order - Sandbox | Production
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//-------------------------------------------------------------------------------------------------

function beforeSubmit(type) {

    try {

        if(type=='edit'){
            var internalid = nlapiGetRecordId();


//Catalina
            var billed=nlapiGetFieldValue("custrecord_clgx_rh_billed");
            if(billed=='T')
            {

                var arrTimes=new Array();
                var searchColumn = new Array();
                var searchFilter = new Array();
                searchFilter.push(new nlobjSearchFilter('internalid',null,'anyof',internalid));
                var searchRHs = nlapiSearchRecord('customrecord_clgx_remote_hands','customsearch6364',searchFilter,searchColumn);
                if (searchRHs  != null) {
                    var searchRH = searchRHs[0];
                    var columns = searchRH.getAllColumns();
                    var timeid = searchRH.getValue(columns[0]);
                    if(timeid.indexOf(',')>-1)
                    {
                        var splittms1=timeid.split(',');
                        for(var t1=0; t1<splittms1.length;t1++)
                        {
                            if(!in_array(splittms1[t1],arrTimes)) {
                                arrTimes.push(splittms1[t1]);
                            }
                        }

                    }
                    else{
                        if(timeid!='' && timeid!=null && !in_array(timeid,arrTimes)) {
                            arrTimes.push(timeid);
                        }
                    }
                }
                for(var t2=0; t2<arrTimes.length;t2++){
                    nlapiSubmitField('timebill', arrTimes[t2], 'custcol_clgx_rh_billedtime', 'T');
                }
            }
        }


    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

    nlapiLogExecution("DEBUG", "CLGX beforeSubmit - Case #2892137", nlapiGetFieldValue("status"));
}
function afterSubmit(type) {

    try {


        var internalid = nlapiGetRecordId();
        if(internalid!=null && internalid!='') {


//Catalina
            var billed = nlapiGetFieldValue("custrecord_clgx_rh_billed");
            nlapiLogExecution("DEBUG", "billed", billed);
            if (billed == 'T') {

                var arrTimes = new Array();
                var searchColumn = new Array();
                var searchFilter = new Array();
                searchFilter.push(new nlobjSearchFilter('internalid', null, 'anyof', internalid));
                var searchRHs = nlapiSearchRecord('customrecord_clgx_remote_hands', 'customsearch7190', searchFilter, searchColumn);
                if (searchRHs != null) {
                    var searchRH = searchRHs[0];
                    var columns = searchRH.getAllColumns();
                    var timeid = searchRH.getValue(columns[0]);
                    if (timeid.indexOf(',') > -1) {
                        var splittms1 = timeid.split(',');
                        for (var t1 = 0; t1 < splittms1.length; t1++) {
                            if (!in_array(splittms1[t1], arrTimes)) {
                                arrTimes.push(splittms1[t1]);
                            }
                        }

                    } else {
                        if (timeid != '' && timeid != null && !in_array(timeid, arrTimes)) {
                            arrTimes.push(timeid);
                        }
                    }
                }
                for (var t2 = 0; t2 < arrTimes.length; t2++) {
                    var timeRecord = nlapiLoadRecord('timebill', arrTimes[t2]);
                    // nlapiSubmitField('timebill', arrTimes[t2], 'custcol_clgx_rh_billedtime', 'T');
                    timeRecord.setFieldValue('custcol_clgx_rh_billedtime','T');
                    nlapiSubmitRecord(timeRecord, false, true);
                }
                nlapiLogExecution('DEBUG', 'Here0', 'Here0');


                //cases


            }
        }



    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------


}
function in_array(val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}