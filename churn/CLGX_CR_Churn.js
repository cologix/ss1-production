nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_Churn.js
//	Script Name:	CLGX_CR_Churn
//	Script Id:		customscript_clgx_cr_churn
//	Script Runs:	On Server
//	Script Type:	Client Script
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//-------------------------------------------------------------------------------------------------

function pageInit()
{
    //for testing
    //var cs=nlapiGetFieldValue('custrecord_clgx_case_num');
    //alert(cs);
}
function saveRecord(){
    try {
//------------- Request -------------------------------------------------------------------
        // Prevent a user to create a churn record if there is already an active churn record for that customer for that market.
//-------------------------------------------------------------------------------------------------


        var edit=nlapiGetFieldValue('custpage_clgx_churn_edit');

        if(edit=="crt")
        {
            var allowSave = 1;
            var alertMsg = '';

            var currentContext = nlapiGetContext();
            var churnID = nlapiGetRecordId();


            var arrFil = new Array();
            var arrCol = new Array();
            var company = nlapiGetFieldValue('custrecord_clgx_churn_customer');
            var market = nlapiGetFieldValue('custrecord_clgx_churn_location');
            var arrLocation=new Array();
            var arrLoc=new Array();
            var arrStatus=["1", "2", "3"];
            //JAX
            arrLocation[0]=['31'];
            //COL
            arrLocation[1]=['34','38'];
            //DAL
            arrLocation[2]=['2','17'];
            //TOR
            arrLocation[3]=['6','13', '15'];
            //MTL
            arrLocation[4]=['5','8', '9','10','11','12','27'];
            //NNJ
            arrLocation[7]=['53','54','55','56'];
            //VAN
            arrLocation[5]=['28','7'];
            //Min
            arrLocation[6]=['16', '35'];
            for	(var j = 0; j < arrLocation.length; j++) {
                if(contains(arrLocation[j], market))
                {
                    arrLoc=arrLocation[j].slice(0)

                }

            }


            arrCol[0] = new nlobjSearchColumn('internalid', null, null);
            arrFil[0] = new nlobjSearchFilter('custrecord_clgx_churn_location',null,'anyof',arrLoc);
            arrFil[1] = new nlobjSearchFilter('custrecord_clgx_churn_customer',null,'anyof', company);
            arrFil[2]=new nlobjSearchFilter('custrecord_clgx_churn_status', null, 'anyof', arrStatus);
            //arrFil[3]=new nlobjSearchFilter('id', null, 'notequalto', churnID);
            var searchCh = nlapiSearchRecord('customrecord_cologix_churn', null, arrFil, arrCol);
            if(searchCh!=null)
            {


                var allowSave = 0;
                var alertMsg = 'Already an active churn record for that customer for that market exist. Please choose another customer or market.';
            }

            var userRole = nlapiGetRole();

            var cs=nlapiGetFieldValue('custrecord_clgx_case_num');
            // Sales Rep role should not have the ability to set the Status to Cancelled on the Churn record if the Case Number on the Churn record is populated.
            // The case numb is checked. If The Churn has a case numb associated a new field for status is created. Options: At Risk and Expected

            if((userRole==1018)&&(cs==null))
            {
                if  ((nlapiGetFieldText('custrecord_clgx_churn_status') == "Noticed")||(nlapiGetFieldText('custrecord_clgx_churn_status') == "Closed")){
                    var allowSave1 = 0;
                    var alertMsg1 = 'You can only select Status of At Risk, Expected, or Cancelled. Please change your selection.';

                }
            }

            if (allowSave == 0) {
                alert(alertMsg);

                return false;
            }
            else{
                if (allowSave1 == 0) {
                    alert(alertMsg1);

                    return false;
                }
                else{
                    return true;

                }

            }




        }
        else{
            //var roleid = nlapiGetRole();
            var userRole = nlapiGetRole();

            var cs=nlapiGetFieldValue('custrecord_clgx_case_num');
            // Sales Rep role should not have the ability to set the Status to Cancelled on the Churn record if the Case Number on the Churn record is populated.
            // The case numb is checked. If The Churn has a case numb associated a new field for status is created. Options: At Risk and Expected

            if((userRole==1018)&&(cs==''))
            {
                if  ((nlapiGetFieldText('custrecord_clgx_churn_status') == "Noticed")||(nlapiGetFieldText('custrecord_clgx_churn_status') == "Closed")){
                    var allowSave = 0;
                    var alertMsg = 'You can only select Status of At Risk, Expected, or Cancelled. Please change your selection.';

                }
            }
            if (allowSave == 0) {
                alert(alertMsg);

                return false;
            }
            else{
                return true;

            }
        }
//-------------------------------------------------------------------------------------------------

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function ValidateField(type, name)
{

    try {
        var roleid = nlapiGetRole();
        var cs=nlapiGetFieldValue('custrecord_clgx_case_num');
        if((roleid==1018)&&(cs==''))
        {
            if ((name == 'custrecord_clgx_churn_status') && ((nlapiGetFieldText('custrecord_clgx_churn_status') == "Noticed")||(nlapiGetFieldText('custrecord_clgx_churn_status') == "Closed")))
            {
                alert(" You can only select Status of At Risk, Expected, or Cancelled. Please change your selection.");

                return false;
            }



        }
        //  Always return true at this level, to continue validating other fields
        return true;

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }


}

function fieldChange(type, name){

    try {
        var roleid = nlapiGetRole();
        var cs=nlapiGetFieldValue('custrecord_clgx_case_num');
        if((roleid==1018)&&(cs==''))
        {
            if ((name == 'custrecord_clgx_churn_status') && ((nlapiGetFieldText('custrecord_clgx_churn_status') == "Noticed")||(nlapiGetFieldText('custrecord_clgx_churn_status') == "Closed")))
            {
                alert(" You can only select Status of At Risk, Expected, or Cancelled. Please change your selection.");
                nlapiSetFieldValues('custrecord_clgx_churn_status', '');
                return false;
            }


            if ((name == 'custrecord_clgx_churn_reason') && ((nlapiGetFieldText('custrecord_clgx_churn_status') == "Noticed")||(nlapiGetFieldText('custrecord_clgx_churn_status') == "Closed")))
            {
                alert(" You can only select Status of At Risk, Expected, or Cancelled. Please change your selection.");
                nlapiSetFieldValues('custrecord_clgx_churn_status', '');
                return false;
            }
        }

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // E

}
function contains(a, obj) {
    var i = a.length;
    while (i--) {
        if (a[i] === obj) {
            return true;
        }
    }
    return false;
}