nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Churn.js
//	Script Name:	CLGX_SU_Churn
//	Script Id:		customscript_clgx_su_churn
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//-------------------------------------------------------------------------------------------------
function beforeLoad(type, form, request){

    try {
        var userid = nlapiGetUser();
        var roleid = nlapiGetRole();
        //change the type of actual MRR fields for admin&full
        //   -5      Administrator
        //  3       Administrator
        // 18      Full Access
        if(((type=='create')||(type=='edit'))&&((roleid==-5)||(roleid==3)||(roleid==18)))
        {
            form.getField('custrecord_clgx_churn_act_power').setDisplayType('normal');
            form.getField('custrecord_clgx_churn_act_space').setDisplayType('normal');
            form.getField('custrecord_clgx_churn_act_other').setDisplayType('normal');
            form.getField('custrecord_clgx_churn_act_xconn').setDisplayType('normal');
            form.getField('custrecord_clgx_churn_act_network').setDisplayType('normal');
            form.getField('custrecord_clgx_churn_closeddate').setDisplayType('normal');
            form.getField('custrecord_clgx_churn_reason').setMandatory(false);

        }
               if ((type == 'edit')&&(roleid==1018)) {
            var chid = nlapiGetRecordId();
            var recordCh = nlapiLoadRecord('customrecord_cologix_churn',chid);
            var cs=recordCh.getFieldValue('custrecord_clgx_case_num');
            var status=recordCh.getFieldText('custrecord_clgx_churn_status');
            if(status=='Closed')
            {
                form.getField('custrecord_clgx_churn_status').setDisplayType('inline');
            }


            // Sales Rep role should not have the ability to set the Status to Cancelled on the Churn record if the Case Number on the Churn record is populated.
            // The case numb is checked. If The Churn has a case numb associated a new field for status is created. Options: At Risk and Expected
            if(cs!=null){

                form.getField('custrecord_clgx_churn_status').setDisplayType('inline');
            }
        }
        //1018 role is for sales rep
        //Sales Rep role should not have the ability to set the Status on the Churn record as Noticed or Closed.
      /*  if ((type == 'create')&&(roleid==1018))
        {
            //a new field is added to for. This field is visible just for sales rep. This field updates custpage_clgx_churn_status field from Churn
            var select = form.addField('custpage_clgx_churn_status', 'select', 'Status');
            select.addSelectOption('','');
            if ((type == 'edit'))
            {
                var chid = nlapiGetRecordId();
                var recordCh = nlapiLoadRecord('customrecord_cologix_churn',chid);
                var sts=recordCh.getFieldValue('custrecord_clgx_churn_status');
                if(sts==1)
                {
                    select.addSelectOption('1','At Risk', true);
                }
                else{
                    select.addSelectOption('1','At Risk');
                }
                if(sts==2)
                {
                    select.addSelectOption('2','Expected', true);
                }
                else{
                    select.addSelectOption('2','Expected');
                }
                if(sts==5)
                {
                    select.addSelectOption('5','Cancelled', true);
                }
                else{
                    select.addSelectOption('5','Cancelled');
                }

            }
            else{
                select.addSelectOption('1','At Risk');
                select.addSelectOption('2','Expected');
                select.addSelectOption('5','Cancelled');
            }
            form.getField('custpage_clgx_churn_status').setMandatory(true);
            //custpage_clgx_churn_status is hidden for Sales Rep
            form.getField('custrecord_clgx_churn_status').setDisplayType('hidden');


        }
        if ((type == 'edit')&&(roleid==1018)) {
            var chid = nlapiGetRecordId();
            var recordCh = nlapiLoadRecord('customrecord_cologix_churn',chid);
            var cs=recordCh.getFieldValue('custrecord_clgx_case_num');
            // Sales Rep role should not have the ability to set the Status to Cancelled on the Churn record if the Case Number on the Churn record is populated.
            // The case numb is checked. If The Churn has a case numb associated a new field for status is created. Options: At Risk and Expected
            if(cs!=''){
                /*var select = form.addField('custpage_clgx_churn_status', 'select', 'Status');

                select.addSelectOption('','');
                var sts=recordCh.getFieldValue('custrecord_clgx_churn_status');
                if(sts==1)
                {
                    select.addSelectOption('1','At Risk',true);
                }
                else{
                    select.addSelectOption('1','At Risk');
                }
                if(sts==2)
                {
                    select.addSelectOption('2','Expected', true);
                }
                else{
                    select.addSelectOption('2','Expected');
                }
               form.getField('custpage_clgx_churn_status').setMandatory(true);
                form.getField('custrecord_clgx_churn_status').setDisplayType('hidden');
                */
             /*   form.getField('custrecord_clgx_churn_status').setDisplayType('inline');
            }
            else{
                var select = form.addField('custpage_clgx_churn_status', 'select', 'Status');
                select.addSelectOption('','');
                if ((type == 'edit'))
                {
                    var chid = nlapiGetRecordId();
                    var recordCh = nlapiLoadRecord('customrecord_cologix_churn',chid);
                    var sts=recordCh.getFieldValue('custrecord_clgx_churn_status');
                    if(sts==1)
                    {
                        select.addSelectOption('1','At Risk', true);
                    }
                    else{
                        select.addSelectOption('1','At Risk');
                    }
                    if(sts==2)
                    {
                        select.addSelectOption('2','Expected', true);
                    }
                    else{
                        select.addSelectOption('2','Expected');
                    }
                    if(sts==5)
                    {
                        select.addSelectOption('5','Cancelled', true);
                    }
                    else{
                        select.addSelectOption('5','Cancelled');
                    }

                }
                else{
                    select.addSelectOption('1','At Risk');
                    select.addSelectOption('2','Expected');
                    select.addSelectOption('5','Cancelled');
                }
                form.getField('custpage_clgx_churn_status').setMandatory(true);
                form.getField('custrecord_clgx_churn_status').setDisplayType('hidden');

            }

        }
        */
        if ((type == 'edit')||(type == 'create')){
            form.addField('custpage_clgx_churn_edit','text', 'Edit');
            form.getField('custpage_clgx_churn_edit').setDisplayType('hidden');
            nlapiSetFieldValue('custpage_clgx_churn_edit', 'crt',  null, true);

        }
        if(type=="edit")
        {
            nlapiSetFieldValue('custpage_clgx_churn_edit', 'edt',  null, true);
        }
        if(type=="create")
        {
            nlapiSetFieldValue('custpage_clgx_churn_edit', 'crt',  null, true);
        }

        if((roleid!=-5)||(roleid!=3)||(roleid!=18))
        {
            form.getField('custrecord_clgx_churn_reason').setMandatory(true);
       }
        if(roleid==-5)
        {
            form.getField('custrecord_clgx_churn_reason').setMandatory(false);
        }
        if(roleid==3)
        {
            form.getField('custrecord_clgx_churn_reason').setMandatory(false);
        }
        if(roleid==18)
        {
            form.getField('custrecord_clgx_churn_reason').setMandatory(false);
        }
        var d = new Date();
        var nextMonth=d.getMonth() + 1;
        var month = new Array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
        var year= d.getFullYear();
        var valueDefault=month[nextMonth]+' '+year;
        var field = nlapiGetField('custrecord_clgx_churn_expecteddate');
        var record = nlapiGetNewRecord();
        // Get the expected date for this Churn
        var expecteddate = record.getFieldValue("custrecord_clgx_churn_expecteddate");
        expecteddate=parseInt(expecteddate+1);
        form.getField('custrecord_clgx_churn_expecteddate').setDefaultValue(84);
    }

    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } //
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function beforeSubmit(type){


}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only)
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function afterSubmit(type){
    try {
        if((type=="edit")||(type=="create")){
            nlapiLogExecution('DEBUG','User Event - After Submit','|-------------STARTED--------------|');
            var churnid = nlapiGetRecordId();
            var churnCustomer = nlapiGetFieldValue('custrecord_clgx_churn_customer');
            var customer=nlapiLoadRecord('customer', churnCustomer);
            var currency=customer.getFieldValue('currency');
            var currencyText=customer.getFieldText('currency');
            var currNetwork=0;
            var currInterconnection=0;
            var currPower=0;
            var currSpace=0;
            var currOther=0;
            var invoicesIDCs=new Array();
            var roleid = nlapiGetRole();
          //  var statussalesrep=nlapiGetFieldValue('custpage_clgx_churn_status');
            //var statussalesrep1=nlapiGetFieldValue('custpage_clgx_churn_status1');

            // if the churn sales Rep is empty we will take the sales rep active  value from customer record
            if((nlapiGetFieldValue('custrecord_clgx_churn_salesrep')=='')||(nlapiGetFieldValue('custrecord_clgx_churn_salesrep')==null))
            {

                var salesrep=customer.getFieldText('salesrep');
                var salesrepId=customer.getFieldValue('salesrep');
            }
            else{
                var salesrep=nlapiGetFieldText('custrecord_clgx_churn_salesrep');
                var salesrepId=nlapiGetFieldValue('custrecord_clgx_churn_salesrep');
            }
            var emp=nlapiLoadRecord('employee', salesrepId);
            var isinac=emp.getFieldValue('isinactive');
            if(isinac=='T')
            {
                var salesrepId='';
            }
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("name",null,"anyof",churnCustomer));
            var arrColumns = new Array();
            var searchChurns = nlapiSearchRecord('invoice', 'customsearch_clg_invchurn_2', arrFilters, arrColumns);
            for ( var i = 0; searchChurns != null && i < searchChurns.length; i++ ) {
                var searchChurn = searchChurns[i];
                var columns = searchChurn.getAllColumns();
                var invoiceid=searchChurn.getValue(columns[0]);

                invoicesIDCs[i]=invoiceid;
            }
            if(invoicesIDCs.length>0)
            {
                var arrFInvoiceC=new Array();
                var arrColInvoiceC=new Array();
                arrFInvoiceC[0] = new nlobjSearchFilter('internalid',null,'anyof',invoicesIDCs);
                var searchInvoicesC = nlapiSearchRecord('transaction', 'customsearch_clgx_invoicesmrrs', arrFInvoiceC, arrColInvoiceC);
                if(searchInvoicesC!=null)
                {
                    for ( var z = 0; searchInvoicesC != null && z < searchInvoicesC.length; z++ ) {
                        var searchInvoiceActualMRRs = searchInvoicesC[z];
                        var columns = searchInvoiceActualMRRs.getAllColumns();
                        currNetwork=parseFloat(currNetwork)+parseFloat(searchInvoiceActualMRRs.getValue(columns[0]));
                        currInterconnection= parseFloat(currInterconnection)+parseFloat(searchInvoiceActualMRRs.getValue(columns[1]));
                        currPower=parseFloat(currPower)+parseFloat(searchInvoiceActualMRRs.getValue(columns[3]));
                        currSpace=parseFloat(currSpace)+parseFloat(searchInvoiceActualMRRs.getValue(columns[2]));
                        currOther=parseFloat(currOther)+parseFloat(searchInvoiceActualMRRs.getValue(columns[4]))+parseFloat(searchInvoiceActualMRRs.getValue(columns[5]))+parseFloat(searchInvoiceActualMRRs.getValue(columns[6]));
                        //      var loc=searchInvoiceActualMRRs.getValue(columns[14]);
                    }
                }
            }
            //Load and Update the Churn Record with Sales Rep and CURR MRRs
            var churnRecord=nlapiLoadRecord('customrecord_cologix_churn', churnid);
            churnRecord.setFieldValue('custrecord_clgx_churn_currency', currency);
            churnRecord.setFieldValue('custrecord_clgx_churn_currencytxt', currencyText);
            //churnRecord.setFieldValue('custrecord_clgx_churn_salesrepid', salesrepId);
            churnRecord.setFieldValue('custrecord_clgx_churn_salesrep', salesrepId);
            churnRecord.setFieldValue('custrecord_clgx_churn_currentnet', currNetwork);
            churnRecord.setFieldValue('custrecord_clgx_churn_currentint', currInterconnection);
            churnRecord.setFieldValue('custrecord_clgx_churn_currentpow', currPower);
            churnRecord.setFieldValue('custrecord_clgx_churn_currentsp', currSpace);
            churnRecord.setFieldValue('custrecord_clgx_churn_currentother', currOther);
            //Updating the Churn status from Sales Rep form
           // if((type == 'create')&&(roleid==1018)){
             //   churnRecord.setFieldValue('custrecord_clgx_churn_status', statussalesrep);
           // }
           // if ((type == 'edit')&&(roleid==1018)) {
               // if(statussalesrep1!='')
                //{
                 //   churnRecord.setFieldValue('custrecord_clgx_churn_status', statussalesrep1);
               // }
             //   if(statussalesrep!='')
              //  {
                //    churnRecord.setFieldValue('custrecord_clgx_churn_status', statussalesrep);
               // }
           // }

            nlapiSubmitRecord(churnRecord, false,true);

            nlapiLogExecution('DEBUG','User Event - After Submit','|-------------FINISHED--------------|');
        }
    }
        //---------- End Sections ------------------------------------------------------------------------------------------------
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

}