nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Create_Churns.js
//	Script Name:	CLGX_SS_Create_Churns
//	Script Id:		customscript_clgx_ss_create_churns
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Add or update a Churn record
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		30/06/2014
//-------------------------------------------------------------------------------------------------

function scheduled_add_churns(){
    try{
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 2/2/2011
// Details:	Consolidate invoices.
//-------------------------------------------------------------------------------------------------
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');
        var context = nlapiGetContext();
        var initialTime = moment();
        //search for Consolidate invoices with Churn true
        var arrFilInv=new Array();
        var arrColInv=new Array();
        var createdCustomer='';
        var createdMarket='';
        var createdClosedDate='';
        var d = new Date();
        var month = parseInt(d.getMonth());
        var monthChurn = parseInt(d.getMonth());
        var created=0;
        //previous month
        if(month==0)
        {
            month=12;
        }
        var year= d.getFullYear();
        month=month.toFixed();
        year=year.toFixed();
        arrFilInv[0] = new nlobjSearchFilter('custrecord_clgx_consol_inv_month',null,'anyof',month);
        arrFilInv[1] = new nlobjSearchFilter('custrecord_clgx_consol_inv_year',null,'is', year);
        var searchCInv = nlapiSearchRecord('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_cons_inv_createchurn', arrFilInv, arrColInv);
        if(searchCInv!=null)
        {
            for ( var i = 0; searchCInv != null && i < searchCInv.length; i++ ) {
                var startExecTime = moment();
                var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);

                if ( (context.getRemainingUsage() <= 1000 || totalMinutes > 50) && (i+1) < searchCInv.length ){
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    if ( status == 'QUEUED' ) {
                        nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                        break;
                    }
                }
                //quantities initialization
                var qNetworkA=0;
                var qSpaceA=0;
                var qPowerA=0;
                var qInterA=0;
                //actual mrrs initializations
                var actualNetwork=0;
                var actualInterconnection=0;
                var actualPower=0;
                var actualSpace=0;
                var actualOther=0;
                var invString='';
                var searchInvoice= searchCInv[i];
                var columns = searchInvoice.getAllColumns();
                var consolidateInvID=searchInvoice.getValue(columns[2]);
                var invoiceid=searchInvoice.getValue(columns[3]);
                var market=searchInvoice.getValue(columns[0]);
                var company=searchInvoice.getValue(columns[1]);
                var closedDate=searchInvoice.getValue(columns[4]);
                var closedDateArr=closedDate.split("/");
                // if(closedDateArr[0]==1)
                //{
                //  closedDateArr[0]=12;
                // closedDateArr[2]=closedDateArr[2]-1;
                // }
                //else{
                //  closedDateArr[0]=closedDateArr[0]-1;

                // }
                // closedDate=closedDateArr[0]+'/'+closedDateArr[1]+'/'+closedDateArr[2];
                var duedate=getExDate(month, year);
                invString=invoiceid;
                switch (market) {
                    case 'JAX':
                        //JAX
                        var arrLocation=['31'];
                        break;
                    case 'JAX2':
                        //JAX2
                        var arrLocation=['40'];
                        break;
                    case 'JAX1':
                        //JAX1
                        var arrLocation=['31'];
                        break;
                    case 'COL':
                        //COL
                        var arrLocation=['34','38'];
                        break;
                    case 'DAL':
                        //DAL
                        var arrLocation=['2','17'];
                        break;

                    case 'TOR':
                        //TOR
                        var arrLocation=['6','13', '15'];
                        break;
                    case 'MTL_EN':
                        //MTL_EN
                        var arrLocation=['5','8', '9','10','11','12','27'];
                        break;
                    case 'MTL_FR':
                        //MTL_FR
                        var arrLocation=['5','8', '9','10','11','12','27'];
                        break;
                    case 'MTL':
                        //MTL
                        var arrLocation=['5','8', '9','10','11','12','27'];
                        break;
                    case 'VAN':
                        //VAN
                        var arrLocation=['28','7'];
                        break;
                    case 'TOR_VAN':
                        //TOR_VAN
                        var arrLocation=['6','7'];
                        break;
                    case 'MIN':
                        //Min
                        var arrLocation=['16', '35'];
                        break;
                    default:
                        var arrLocation=['0'];
                }


                //consolidates invoices with Churn True
                var invIDs = invString.split(";");
                var arrDates1  = getDateRange(month,year);
                var arrDatesChurn  = getDateRange(monthChurn,year);
                var startDate1 = arrDates1[0];
                var endDate1   = arrDates1[1];
                var startDateChurn = arrDatesChurn[0];
                var endDateChurn   = arrDatesChurn[1];
                var customer=nlapiLoadRecord('customer', company);
                var salesrepId=customer.getFieldValue('salesrep');
                if((salesrepId!=null)&&(salesrepId!=''))
                {
                    var emp=nlapiLoadRecord('employee', salesrepId);
                    var isinac=emp.getFieldValue('isinactive');
                    if(isinac=='T')
                    {
                        var salesrepId='';
                    }
                }
                var arrFilSO=new Array();
                var arrColSO=new Array();
                arrFilSO[0] = new nlobjSearchFilter('name',null,'anyof',company);
                arrFilSO[1] = new nlobjSearchFilter('location',null,'anyof', arrLocation);
                var searchSOs = nlapiSearchRecord('transaction', 'customsearch_clgx_so_churn_active', arrFilSO, arrColSO);
                var arrFInvoice=new Array();
                var arrColInvoice=new Array();
                var x=0;
                for ( var k = 0; invIDs != null && k < invIDs.length; k++ ) {
                    if(invIDs[k]!='')
                    {
                        invIDs[x]=invIDs[k];
                        x++;
                    }
                }
                //calculate actual from Consolidate churn Records Invoices
                arrFInvoice[0] = new nlobjSearchFilter('internalid',null,'anyof',invIDs);
                var searchInvoices = nlapiSearchRecord('transaction', 'customsearch_clgx_invoicesmrrs', arrFInvoice, arrColInvoice);

                if(searchInvoices!=null)
                {
                    for ( var z = 0; searchInvoices != null && z < searchInvoices.length; z++ ) {
                        var searchInvoiceActualMRRs = searchInvoices[z];
                        var columns = searchInvoiceActualMRRs.getAllColumns();
                        var loc=searchInvoiceActualMRRs.getValue(columns[14]);
                        actualNetwork=parseFloat(actualNetwork)+parseFloat(searchInvoiceActualMRRs.getValue(columns[0]));
                        actualInterconnection=parseFloat(actualInterconnection)+parseFloat(searchInvoiceActualMRRs.getValue(columns[1]));
                        actualSpace=parseFloat(actualSpace)+parseFloat(searchInvoiceActualMRRs.getValue(columns[2]));
                        actualPower=parseFloat(actualPower)+parseFloat(searchInvoiceActualMRRs.getValue(columns[3]));
                        actualOther=parseFloat(actualOther)+parseFloat(searchInvoiceActualMRRs.getValue(columns[4]))+parseFloat(searchInvoiceActualMRRs.getValue(columns[5]))+parseFloat(searchInvoiceActualMRRs.getValue(columns[6]));
                        qNetworkA=parseFloat(qNetworkA)+parseFloat(searchInvoiceActualMRRs.getValue(columns[7]));
                        qInterA=parseFloat(qInterA)+parseFloat(searchInvoiceActualMRRs.getValue(columns[8]));
                        qPowerA=parseFloat(qPowerA)+parseFloat(searchInvoiceActualMRRs.getValue(columns[9]));
                        qSpaceA=parseFloat(qSpaceA)+parseFloat(searchInvoiceActualMRRs.getValue(columns[10]));
                              }
                }
                if(searchInvoices!=null)
                {
                    if(searchSOs==null)
                    {
                        var arrFil = new Array();
                        var arrCol = new Array();
                        arrCol[0] = new nlobjSearchColumn('internalid', null, null);
                        arrCol[1] = new nlobjSearchColumn('custrecord_clgx_churn_expecteddate', null, null);
                        arrCol[2] = new nlobjSearchColumn('custrecord_clgx_churn_status', null, null);
                        arrFil[0] = new nlobjSearchFilter('custrecord_clgx_churn_location',null,'anyof',arrLocation);
                        arrFil[1] = new nlobjSearchFilter('custrecord_clgx_churn_customer',null,'anyof', company);
                        var searchCh = nlapiSearchRecord('customrecord_cologix_churn', null, arrFil, arrCol);
                        if(searchCh!=null)
                        {
                            for ( var w = 0; searchCh != null && w < searchCh.length; w++ ) {
                                var searchChurn = searchCh[w];
                                var internalid=searchChurn.getValue('internalid');
                                var sts=searchChurn.getValue('custrecord_clgx_churn_status');
                                var exDate=searchChurn.getText('custrecord_clgx_churn_expecteddate');
                                //var closedDateArr=exDate.split("/");
                                //var clDate=getExDate(closedDateArr[0], closedDateArr[2]);
                                var consDate=getExDate(monthChurn, year);
                                //8/1/2014-closedDate format
                                if(sts<4){
                                    var churn= nlapiLoadRecord('customrecord_cologix_churn', internalid);
                                    churn.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnection);
                                    churn.setFieldValue('custrecord_clgx_churn_act_power', actualPower);
                                    churn.setFieldValue('custrecord_clgx_churn_act_space', actualSpace);
                                    churn.setFieldValue('custrecord_clgx_churn_act_network', actualNetwork);
                                    churn.setFieldValue('custrecord_clgx_churn_act_other', actualOther);
                                    churn.setFieldValue('custrecord_clgx_churn_closeddate', closedDate);
                                    churn.setFieldText('custrecord_clgx_churn_expecteddate', duedate);
                                    nlapiSubmitRecord(churn, false,true);

                                }
                                else
                                {
                                    if(exDate==consDate)
                                    {
                                        var churn= nlapiLoadRecord('customrecord_cologix_churn', internalid);
                                        churn.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnection);
                                        churn.setFieldValue('custrecord_clgx_churn_act_power', actualPower);
                                        churn.setFieldValue('custrecord_clgx_churn_act_space', actualSpace);
                                        churn.setFieldValue('custrecord_clgx_churn_act_network', actualNetwork);
                                        churn.setFieldValue('custrecord_clgx_churn_act_other', actualOther);
                                        churn.setFieldValue('custrecord_clgx_churn_closeddate', closedDate);
                                        churn.setFieldText('custrecord_clgx_churn_expecteddate', duedate);
                                        
                                    var idChurn = nlapiSubmitRecord(churn, false,false);
                                    }
                                    else
                                    {
                                        if((created==0)&&(createdCustomer!=company)&&(createdMarket!=loc)&&(createdClosedDate!=closedDate))

                                        {
                                            /*If No churn record found for that customer
                                             for that market for that month. Create New Churn,
                                             Status=Closed, Reason=Blank Churn Type = Disconnect
                                             Closed Date = Month of the last invoice and include
                                             the Actual MRR per category on the Churn Record with the MRR value from the previous month
                                             consolidated invoice for that customer for that market.
                                             * */

                                            var newRecord = nlapiCreateRecord('customrecord_cologix_churn');

                                            newRecord.setFieldValue('custrecord_clgx_churn_customer', company);
                                            newRecord.setFieldText('custrecord_clgx_churn_location', loc);
                                            newRecord.setFieldValue('custrecord_clgx_churn_status', 4);
                                            newRecord.setFieldText('custrecord_clgx_churn_expecteddate', duedate);
                                            newRecord.setFieldValue('custrecord_clgx_churn_salesrep', salesrepId);
                                            newRecord.setFieldValue('custrecord_clgx_churn_type_churn', 1);
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnection);
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_power', actualPower);
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_space', actualSpace);
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_network', actualNetwork);
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_other', actualOther);
                                            //newRecord.setFieldValue('custrecord_clgx_churn_closeddate', closeDate);
                                            newRecord.setFieldValue('custrecord_clgx_churn_closeddate', closedDate);
                                            nlapiSubmitRecord(newRecord, false,true);
                                            createdCustomer=company;
                                            createdMarket=loc;
                                            createdClosedDate=closedDate;
                                            created=1;
                                        }
                                    }
                                }
                            }
                            //set custrecord_clgx_consol_inv_flagforchurn to true after everything is done
                            var consInvToUpdate=nlapiLoadRecord('customrecord_clgx_consolidated_invoices', consolidateInvID);
                            consInvToUpdate.setFieldValue('custrecord_clgx_consol_inv_flagforchurn', 'T');
                            nlapiSubmitRecord(consInvToUpdate, false,true);

                        }
                        else
                        {
                            /*If No churn record found for that customer
                             for that market for that month. Create New Churn,
                             Status=Closed, Reason=Blank Churn Type = Disconnect
                             Closed Date = Month of the last invoice and include
                             the Actual MRR per category on the Churn Record with the MRR value from the previous month
                             consolidated invoice for that customer for that market.
                             * */
                            if((created==0)&&(createdCustomer!=company)&&(createdMarket!=loc)&&(createdClosedDate!=closedDate))

                            {
                                var newRecord = nlapiCreateRecord('customrecord_cologix_churn');

                                newRecord.setFieldValue('custrecord_clgx_churn_customer', company);
                                newRecord.setFieldText('custrecord_clgx_churn_location', loc);
                                newRecord.setFieldValue('custrecord_clgx_churn_status', 4);
                                newRecord.setFieldText('custrecord_clgx_churn_expecteddate', duedate);
                                newRecord.setFieldValue('custrecord_clgx_churn_salesrep', salesrepId);
                                newRecord.setFieldValue('custrecord_clgx_churn_type_churn', 1);
                                newRecord.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnection);
                                newRecord.setFieldValue('custrecord_clgx_churn_act_power', actualPower);
                                newRecord.setFieldValue('custrecord_clgx_churn_act_space', actualSpace);
                                newRecord.setFieldValue('custrecord_clgx_churn_act_network', actualNetwork);
                                newRecord.setFieldValue('custrecord_clgx_churn_act_other', actualOther);
                                //newRecord.setFieldValue('custrecord_clgx_churn_closeddate', closeDate);
                                newRecord.setFieldValue('custrecord_clgx_churn_closeddate', closedDate);
                                nlapiSubmitRecord(newRecord, false,true);
                                createdCustomer=company;
                                createdMarket=loc;
                                createdClosedDate=closedDate;
                                created=1;
                            }
                        }
                    }
                }
                //set custrecord_clgx_consol_inv_flagforchurn to true after everything is done
                var consInvToUpdate=nlapiLoadRecord('customrecord_clgx_consolidated_invoices', consolidateInvID);
                consInvToUpdate.setFieldValue('custrecord_clgx_consol_inv_flagforchurn', 'T');
                nlapiSubmitRecord(consInvToUpdate, false,true);
                var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
                var index = i + 1;
                nlapiLogExecution('DEBUG','SO ', ' | consolidateinvid =  ' + consolidateInvID + ' | Index = ' + index + ' of ' + searchCInv.length + ' | Usage - '+ usageConsumtion + '  |');


            }
        }




        nlapiLogExecution('DEBUG','Finished Execution', 'Total Remaining Usage - ' + context.getRemainingUsage() + ' -------------------------- Finished Scheduled Script --------------------------|');

    }

    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
function getDueDate(netDays){
    // Return 'pay until' date
    var date = new Date();
    date.setDate(date.getDate() + netDays);

    var month = parseInt(date.getMonth()) + 1;
    var day = date.getDate();
    var year = date.getFullYear();

    if(parseInt(month) < 10){
        month = '0' + month;
    }

    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = month + '/' + day + '/' + year;

    return formattedDate;
}

function getDateToday(){
    var date = new Date();
    var month = parseInt(date.getMonth()) + 1;
    var day = date.getDate();
    var year = date.getFullYear();

    if(parseInt(month) < 10){
        month = '0' + month;
    }

    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = month + '/' + day + '/' + year;

    return formattedDate;
}

//get the last day of the month
function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}

//get the start date and end date
function getDateRange(month,year){
    var stMonth = month - 1;
    var stDays  = daysInMonth(parseInt(stMonth),parseInt(year));
    var stYear  = year;

    var stStartDate = month + '/1/' + stYear;
    var stEndDate   = month + '/' + stDays + '/' + stYear;

    var arrDateRange = new Array();
    arrDateRange[0] = stStartDate;
    arrDateRange[1] =stEndDate;

    return arrDateRange;
}

function getDateFormated(date2format,language){

    var date = nlapiStringToDate(date2format);
    var month = parseInt(date.getMonth());
    var day = date.getDate();
    var year = date.getFullYear();

    var arrMonthEN = new Array();
    var arrMonthFR = new Array();

    arrMonthEN[0] = 'JAN';
    arrMonthEN[1] = 'FEB';
    arrMonthEN[2] = 'MAR';
    arrMonthEN[3] = 'APR';
    arrMonthEN[4] = 'MAY';
    arrMonthEN[5] = 'JUN';
    arrMonthEN[6] = 'JUL';
    arrMonthEN[7] = 'AUG';
    arrMonthEN[8] = 'SEP';
    arrMonthEN[9] = 'OCT';
    arrMonthEN[10] = 'NOV';
    arrMonthEN[11] = 'DEC';

    arrMonthFR[0] = 'JAN';
    arrMonthFR[1] = 'F&Eacute;V';
    arrMonthFR[2] = 'MAR';
    arrMonthFR[3] = 'AVR';
    arrMonthFR[4] = 'MAI';
    arrMonthFR[5] = 'JUN';
    arrMonthFR[6] = 'JUL';
    arrMonthFR[7] = 'AO&Ucirc;';
    arrMonthFR[8] = 'SEP';
    arrMonthFR[9] = 'OCT';
    arrMonthFR[10] = 'NOV';
    arrMonthFR[11] = 'D&Eacute;C';

    if(language == 'en_US' || language == 'en'){
        var stMonth = arrMonthEN[parseInt(month)];
    }
    else{
        var stMonth = arrMonthFR[parseInt(month)];
    }

    var stDate = day + '-' + stMonth + '-' + year;

    return stDate;
}

function getExDate(month,year){


    var arrMonth = new Array();


    arrMonth[1] = 'Jan';
    arrMonth[2] = 'Feb';
    arrMonth[3] = 'Mar';
    arrMonth[4] = 'Apr';
    arrMonth[5] = 'May';
    arrMonth[6] = 'Jun';
    arrMonth[7] = 'Jul';
    arrMonth[8] = 'Aug';
    arrMonth[9] = 'Sep';
    arrMonth[10] = 'Oct';
    arrMonth[11] = 'Nov';
    arrMonth[12] = 'Dec';


    var stMonth = arrMonth[parseInt(month)];

    var stDate = stMonth + ' ' + year;

    return stDate;
}