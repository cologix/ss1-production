nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Consolidate_Churn.js
//	Script Name:	CLGX_SS_Consolidate_churn
//	Script Id:		customscript_clgx_ss_consolidate_churn
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Add or update a Churn record
//	@authors:		Catalina Taran - catalina.taran@cologix.com
//	Created:		18/06/2014
//-------------------------------------------------------------------------------------------------

function scheduled_consolidate_churn(){
    try{
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 2/2/2011
// Details:	Consolidate invoices.
//-------------------------------------------------------------------------------------------------
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');

        var context = nlapiGetContext();
        var initialTime = moment();
        //search for Consolidate invoices with Churn true
        var arrFilInv=new Array();
        var arrColInv=new Array();
        var createdCustomer='';
        var createdMarket='';
        var createdClosedDate='';

        var searchCInv = nlapiSearchRecord('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_cons_inv_churn', arrFilInv, arrColInv);
        if(searchCInv!=null)
        {

            for ( var i = 0; searchCInv != null && i < searchCInv.length; i++ ) {
                var startExecTime = moment();
                var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);

                if ( (context.getRemainingUsage() <= 1000 || totalMinutes > 50) && (i+1) < searchCInv.length ){
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    if ( status == 'QUEUED' ) {
                        nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                        break;
                    }
                }

                //quantities initialization
                var qNetworkA=0;
                var qSpaceA=0;
                var qPowerA=0;
                var qInterA=0;
                var qNetworkL=0;
                var qSpaceL=0;
                var qPowerL=0;
                var qInterL=0;
                var qOther=0;
                var qOtherL=0;
                //actual mrrs initializations
                var actualNetwork=0;
                var actualInterconnection=0;
                var actualPower=0;
                var actualSpace=0;
                var actualNetworkL=0;
                var actualInterconnectionL=0;
                var actualPowerL=0;
                var actualSpaceL=0;
                var actualOther=0;
                var actualOtherL=0;
                var invString='';
                var invStringB='';
                var created=0;

                var searchInvoice= searchCInv[i];
                var columns = searchInvoice.getAllColumns();
                var invoiceid=searchInvoice.getValue(columns[0]);
                var month=searchInvoice.getValue(columns[1]);
                var year=searchInvoice.getValue(columns[2]);
                var market=searchInvoice.getValue(columns[3]);
                var consolidateInvID=searchInvoice.getValue(columns[4]);
                var company=searchInvoice.getValue(columns[5]);
                var closedDate=searchInvoice.getValue(columns[6]);
                //8/1/2014-closedDate format
                var closedDateArr=closedDate.split("/");
                if(closedDateArr[0]==1)
                {
                    closedDateArr[0]=12;
                    closedDateArr[2]=closedDateArr[2]-1;
                }
                else{
                    closedDateArr[0]=closedDateArr[0]-1;

                }
                closedDate=closedDateArr[0]+'/'+closedDateArr[1]+'/'+closedDateArr[2];
                invString=invoiceid;
                var customer=nlapiLoadRecord('customer', company);
                var salesrepId=customer.getFieldValue('salesrep');
                if((salesrepId!=null)&&(salesrepId!=''))
                {
                    var emp=nlapiLoadRecord('employee', salesrepId);
                    var isinac=emp.getFieldValue('isinactive');
                    if(isinac=='T')
                    {
                        var salesrepId='';
                    }
                }
                switch (market) {
                    case 'JAX':
                        //JAX
                        var arrLocation=['31'];
                        break;
                    case 'JAX2':
                        //JAX2
                        var arrLocation=['40'];
                        break;
                    case 'COL':
                        //COL
                        var arrLocation=['34','38'];
                        break;
                    case 'DAL':
                        //DAL
                        var arrLocation=['2','17'];
                        break;

                    case 'TOR':
                        //TOR
                        var arrLocation=['6','13', '15'];
                        break;
                    case 'MTL_EN':
                        //MTL_EN
                        var arrLocation=['5','8', '9','10','11','12','27'];
                        break;
                    case 'MTL_FR':
                        //MTL_FR
                        var arrLocation=['5','8', '9','10','11','12','27'];
                        break;
                    case 'VAN':
                        //VAN
                        var arrLocation=['28','7'];
                        break;
                    case 'TOR_VAN':
                        //TOR_VAN
                        var arrLocation=['6','7'];
                        break;
                    case 'MIN':
                        //Min
                        var arrLocation=['16', '35'];
                        break;
                    default:
                        var arrLocation=['0'];
                }


                //consolidates invoices with Churn True
                var invIDs = invString.split(";");
                if(month==1)
                {
                    var startMonth=12;
                    var startYear=year-1;
                }
                else
                {
                    var startMonth=month-1;
                    var startYear=year;
                }
                if(startMonth==1)
                {
                    var startMonth1=12;
                    var startYear1=startYear-1;
                }
                else
                {
                    var startMonth1=startMonth-1;
                    var startYear1=startYear;
                }
                var arrDates  = getDateRange(startMonth,startYear);
                var arrDates_1  = getDateRange(startMonth1,startYear1);
                var arrDates1  = getDateRange(month,year);
                var startDate = arrDates[0];
                var endDate   = arrDates[1];
                var startDate1 = arrDates1[0];
                var endDate1   = arrDates1[1];
                var startDate_1 = arrDates_1[0];
                var endDate_1   = arrDates_1[1];
                var invoicesIDCs=new Array();
                var invoicesIDCs1= new Array();

                var arrFiltersInvoices = new Array();
                var arrColumnsInvoices = new Array();
                arrFiltersInvoices.push(new nlobjSearchFilter("name",null,"anyof",company));
                arrFiltersInvoices.push(new nlobjSearchFilter("location",null,"anyof",arrLocation));
                arrFiltersInvoices.push(new nlobjSearchFilter("trandate", null, "within",startDate1,endDate1));
                var searchChurns = nlapiSearchRecord('invoice', 'customsearch_clg_invchurn_2_2', arrFiltersInvoices, arrColumnsInvoices);
                if(searchChurns!=null)
                {
                    for ( var p = 0; searchChurns != null && p < searchChurns.length; p++ ) {
                        var searchChurn = searchChurns[p];
                        var columns = searchChurn.getAllColumns();
                        var invoiceid=searchChurn.getValue(columns[0]);

                        invoicesIDCs[p]=invoiceid;
                    }
                }
                //1 month before for the same customer, market
                var arrFiltersInvoices1 = new Array();
                var arrColumnsInvoices1 = new Array();
                arrFiltersInvoices1.push(new nlobjSearchFilter("name",null,"anyof",company));
                arrFiltersInvoices1.push(new nlobjSearchFilter("location",null,"anyof",arrLocation));
                arrFiltersInvoices1.push(new nlobjSearchFilter("trandate", null, "within",startDate,endDate));
                var searchChurns1 = nlapiSearchRecord('invoice', 'customsearch_clg_invchurn_2_2', arrFiltersInvoices1, arrColumnsInvoices1);
                if(searchChurns1!=null)
                {
                    for ( var k = 0; searchChurns1 != null && k < searchChurns1.length; k++ ) {
                        var searchChurn1 = searchChurns1[k];
                        var columns = searchChurn1.getAllColumns();
                        var invoiceid1=searchChurn1.getValue(columns[0]);

                        invoicesIDCs1[k]=invoiceid1;
                    }
                }


                /* var arrFilInvB=new Array();
                 var arrColInvB=new Array();
                 arrColInvB[0] = new nlobjSearchColumn('custrecord_clgx_consol_inv_invoices', null, null);
                 arrFilInvB[0] = new nlobjSearchFilter('custrecord_clgx_consol_inv_month',null,'anyof',startMonth);
                 arrFilInvB[1] = new nlobjSearchFilter('custrecord_clgx_consol_inv_year',null,'is', startYear);
                 arrFilInvB[2] = new nlobjSearchFilter('custrecord_clgx_consol_inv_customer',null,'anyof', company);
                 arrFilInvB[3] = new nlobjSearchFilter('custrecord_clgx_consol_inv_market',null,'is', market);
                 var searchCInvB = nlapiSearchRecord('customrecord_clgx_consolidated_invoices', null, arrFilInvB, arrColInvB);
                 */
                if((invoicesIDCs1.length>0)&&(invoicesIDCs.length>0)){
                    /*for ( var j = 0; searchCInvB != null && j < searchCInvB.length; j++ ) {
                     var searchConsolidateBefore = searchCInvB[j];
                     var invoiceidBefore=searchConsolidateBefore.getValue('custrecord_clgx_consol_inv_invoices');
                     invStringB=invStringB+';'+invoiceidBefore;

                     }
                     var invIDsB = invStringB.split(";");
                     */
                    //calculate actual from Consolidate churn Records Invoices
                    //calculate actual from Consolidate churn Records Invoices
                    var arrFInvoice=new Array();
                    var arrColInvoice=new Array();
                    arrFInvoice[0] = new nlobjSearchFilter('internalid',null,'anyof',invoicesIDCs);
                    var searchInvoices = nlapiSearchRecord('transaction', 'customsearch_clgx_invoicesmrrs', arrFInvoice, arrColInvoice);
                    if(searchInvoices!=null)
                    {
                        for ( var z = 0; searchInvoices != null && z < searchInvoices.length; z++ ) {
                            var searchInvoiceActualMRRs = searchInvoices[z];
                            var columns = searchInvoiceActualMRRs.getAllColumns();
                            actualNetwork=parseFloat(actualNetwork)+parseFloat(searchInvoiceActualMRRs.getValue(columns[0]));
                            actualInterconnection=parseFloat(actualInterconnection)+parseFloat(searchInvoiceActualMRRs.getValue(columns[1]));
                            actualSpace=parseFloat(actualSpace)+parseFloat(searchInvoiceActualMRRs.getValue(columns[2]));
                            actualPower=parseFloat(actualPower)+parseFloat(searchInvoiceActualMRRs.getValue(columns[3]));
                            actualOther=parseFloat(actualOther)+parseFloat(searchInvoiceActualMRRs.getValue(columns[4]))+parseFloat(searchInvoiceActualMRRs.getValue(columns[5]))+parseFloat(searchInvoiceActualMRRs.getValue(columns[6]));
                            qNetworkA=parseFloat(qNetworkA)+parseFloat(searchInvoiceActualMRRs.getValue(columns[7]));
                            qInterA=parseFloat(qInterA)+parseFloat(searchInvoiceActualMRRs.getValue(columns[8]));
                            qPowerA=parseFloat(qPowerA)+parseFloat(searchInvoiceActualMRRs.getValue(columns[9]));
                            qSpaceA=parseFloat(qSpaceA)+parseFloat(searchInvoiceActualMRRs.getValue(columns[10]));
                            qOther=parseFloat(qOther)+parseFloat(searchInvoiceActualMRRs.getValue(columns[11]))+parseFloat(searchInvoiceActualMRRs.getValue(columns[12]))+parseFloat(searchInvoiceActualMRRs.getValue(columns[13]));
                            var loc=searchInvoiceActualMRRs.getValue(columns[14]);
                        }
                    }
                    var arrFInvoiceB=new Array();
                    var arrColInvoiceB=new Array();
                    arrFInvoiceB[0] = new nlobjSearchFilter('internalid',null,'anyof',invoicesIDCs1);
                    var searchInvoicesB = nlapiSearchRecord('transaction', 'customsearch_clgx_invoicesmrrs', arrFInvoiceB, arrColInvoiceB);

                    if(searchInvoicesB!=null)
                    {
                        for ( var z = 0; searchInvoicesB != null && z < searchInvoicesB.length; z++ ) {
                            var searchInvoiceActualMRRsB = searchInvoicesB[z];
                            var columnsB = searchInvoiceActualMRRsB.getAllColumns();
                            actualNetworkL=parseFloat(actualNetworkL)+parseFloat(searchInvoiceActualMRRsB.getValue(columnsB[0]));
                            actualInterconnectionL=parseFloat(actualInterconnectionL)+parseFloat(searchInvoiceActualMRRsB.getValue(columnsB[1]));
                            actualSpaceL=parseFloat(actualSpaceL)+parseFloat(searchInvoiceActualMRRsB.getValue(columnsB[2]));
                            actualPowerL=parseFloat(actualPowerL)+parseFloat(searchInvoiceActualMRRsB.getValue(columnsB[3]));
                            actualOtherL=parseFloat(actualOtherL)+parseFloat(searchInvoiceActualMRRsB.getValue(columnsB[4]))+parseFloat(searchInvoiceActualMRRsB.getValue(columnsB[5]))+parseFloat(searchInvoiceActualMRRsB.getValue(columnsB[6]));
                            qNetworkL=parseFloat(qNetworkL)+parseFloat(searchInvoiceActualMRRsB.getValue(columnsB[7]));
                            qInterL=parseFloat(qInterL)+parseFloat(searchInvoiceActualMRRsB.getValue(columnsB[8]));
                            qPowerL=parseFloat(qPowerL)+parseFloat(searchInvoiceActualMRRsB.getValue(columnsB[9]));
                            qSpaceL=parseFloat(qSpaceL)+parseFloat(searchInvoiceActualMRRsB.getValue(columnsB[10]));
                            qOtherL=parseFloat(qOtherL)+parseFloat(searchInvoiceActualMRRsB.getValue(columnsB[11]))+parseFloat(searchInvoiceActualMRRsB.getValue(columnsB[12]))+parseFloat(searchInvoiceActualMRRsB.getValue(columnsB[13]));
                        }
                    }
                    if((actualInterconnection<actualInterconnectionL)||(actualNetwork<actualNetworkL)||(actualPower<actualPowerL)||(actualSpace<actualSpaceL)||(actualOther<actualOtherL))
                    {

                        var actualNetworkT=actualNetwork-actualNetworkL;
                        var actualInterconnectionT=actualInterconnection-actualInterconnectionL;
                        var actualPowerT=actualPower-actualPowerL;
                        var actualSpaceT=actualSpace-actualSpaceL;
                        var actualOtherT=actualOther-actualOtherL;
                        var arrFil = new Array();
                        var arrCol = new Array();
                        var stsactive=["1", "2", "3"];
                        arrCol[0] = new nlobjSearchColumn('internalid', null, null);
                        arrCol[1] = new nlobjSearchColumn('custrecord_clgx_churn_expecteddate', null, null);
                        arrFil[0] = new nlobjSearchFilter('custrecord_clgx_churn_location',null,'anyof',arrLocation);
                        arrFil[1] = new nlobjSearchFilter('custrecord_clgx_churn_customer',null,'anyof', company);
                        arrFil[2] = new nlobjSearchFilter('custrecord_clgx_churn_status',null,'anyof', stsactive);
                        // arrFil[3] = new nlobjSearchFilter('custrecord_clgx_churn_expecteddate',null,'within',startDate1,endDate1);
                        var searchCh = nlapiSearchRecord('customrecord_cologix_churn', null, arrFil, arrCol);
                        var arrFilClosed = new Array();
                        var arrColClosed = new Array();
                        var stsclose=["4"];
                        // arrColClosed[0] = new nlobjSearchColumn('internalid', null, null);
                        //arrColClosed[1] = new nlobjSearchColumn('custrecord_clgx_churn_expecteddate', null, null);
                        arrFilClosed[0] = new nlobjSearchFilter('custrecord_clgx_churn_location',null,'anyof',arrLocation);
                        arrFilClosed[1] = new nlobjSearchFilter('custrecord_clgx_churn_customer',null,'anyof', company);
                        arrFilClosed[2] = new nlobjSearchFilter('custrecord_clgx_churn_status',null,'anyof', stsclose);
                        // arrFilClosed[3] = new nlobjSearchFilter('custrecord_clgx_churn_expecteddate',null,'within',startDate1,endDate1);
                        var searchChClosed = nlapiSearchRecord('customrecord_cologix_churn', "customsearch_clgx_churn_closed1i", arrFilClosed, arrColClosed);
                        if((searchChClosed!=null)||(searchCh!=null))
                        {
                            if(searchCh!=null)
                            {
                                for ( var w = 0; searchCh != null && w < searchCh.length; w++ ) {
                                    var searchChurn = searchCh[w];
                                    var internalid=searchChurn.getValue('internalid');
                                    var exDate=searchChurn.getText('custrecord_clgx_churn_expecteddate');
                                    var consDate=getExDate(month, year);
                                    // if(exDate==consDate)
                                    //{
                                    var churn= nlapiLoadRecord('customrecord_cologix_churn', internalid);
                                    if(actualInterconnection<actualInterconnectionL){
                                        if(actualInterconnectionT<0)
                                        {
                                            churn.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnectionT*(-1));
                                        }
                                        else{

                                            churn.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnectionT);

                                        }

                                    }else{
                                        churn.setFieldValue('custrecord_clgx_churn_act_xconn', 0);


                                    }
                                    if(actualPower<actualPowerL){
                                        if(actualPowerT<0)
                                        {
                                            churn.setFieldValue('custrecord_clgx_churn_act_power', actualPowerT*(-1));
                                        }
                                        else{

                                            churn.setFieldValue('custrecord_clgx_churn_act_power', actualPowerT);

                                        }

                                    }else{
                                        churn.setFieldValue('custrecord_clgx_churn_act_power', 0);
                                    }
                                    if(actualOther<actualOtherL){
                                        if(actualOtherT<0)
                                        {
                                            churn.setFieldValue('custrecord_clgx_churn_act_other', actualOtherT*(-1));
                                        }
                                        else{

                                            churn.setFieldValue('custrecord_clgx_churn_act_other', actualOtherT);

                                        }

                                    }else{
                                        churn.setFieldValue('custrecord_clgx_churn_act_other', 0);
                                    }
                                    if(actualSpace<actualSpaceL){
                                        if(actualSpaceT<0)
                                        {
                                            churn.setFieldValue('custrecord_clgx_churn_act_space', actualSpaceT*(-1));
                                        }
                                        else{

                                            churn.setFieldValue('custrecord_clgx_churn_act_space', actualSpaceT);

                                        }

                                    }
                                    else{
                                        churn.setFieldValue('custrecord_clgx_churn_act_space', 0);

                                    }
                                    if(actualNetwork<actualNetworkL){
                                        if(actualNetworkT<0)
                                        {
                                            churn.setFieldValue('custrecord_clgx_churn_act_network', actualNetworkT*(-1));
                                        }
                                        else{

                                            churn.setFieldValue('custrecord_clgx_churn_act_network', actualNetworkT);

                                        }

                                    }else{
                                        churn.setFieldValue('custrecord_clgx_churn_act_network', 0);

                                    }
                                    churn.setFieldValue('custrecord_clgx_churn_closeddate', closedDate);
                                    nlapiSubmitRecord(churn, false,true);

                                    createdCustomer=company;
                                    createdMarket=loc;
                                    createdClosedDate=closedDate;
                                   // var emailSubject = 'Churn3';
                                   // var emailBody='createdCustomer: '+createdCustomer+' createdMarket: '+createdMarket+' createdClosedDate: '+createdClosedDate;
                                   // nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null); // Send email to Catalina

                                    // }

                                }

                            }

                            if(searchChClosed!=null)
                            {
                                for ( var z = 0; searchChClosed != null && z < searchChClosed.length; z++ ) {
                                    var searchChurnCl = searchChClosed[z];
                                    var internalidCl=searchChurnCl.getValue('internalid');
                                    var exDateCl=searchChurnCl.getText('custrecord_clgx_churn_expecteddate');
                                    if(month==1)
                                    {
                                        var monthPrev=12;
                                        var yearPrev=year-1;
                                    }
                                    else
                                    {
                                        var monthPrev=month-1;
                                        var yearPrev=year;

                                    }

                                    var consDateCl=getExDate(monthPrev, yearPrev);
                                    if(exDateCl==consDateCl)
                                    {
                                        var churnCl= nlapiLoadRecord('customrecord_cologix_churn', internalidCl);
                                        if(actualInterconnection<actualInterconnectionL){
                                            if(actualInterconnectionT<0)
                                            {
                                                churnCl.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnectionT*(-1));
                                            }
                                            else{

                                                churnCl.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnectionT);

                                            }

                                        }
                                        else{
                                            churnCl.setFieldValue('custrecord_clgx_churn_act_xconn', 0);

                                        }
                                        if(actualPower<actualPowerL){
                                            if(actualPower<actualPowerL){
                                                if(actualPowerT<0)
                                                {
                                                    churnCl.setFieldValue('custrecord_clgx_churn_act_power', actualPowerT*(-1));
                                                }
                                                else{

                                                    churnCl.setFieldValue('custrecord_clgx_churn_act_power', actualPowerT);

                                                }

                                            }
                                        }else{
                                            churnCl.setFieldValue('custrecord_clgx_churn_act_power', 0);

                                        }
                                        if(actualOther<actualOtherL){
                                            if(actualOther<actualOtherL){
                                                if(actualOtherT<0)
                                                {
                                                    churnCl.setFieldValue('custrecord_clgx_churn_act_other', actualOtherT*(-1));
                                                }
                                                else{

                                                    churnCl.setFieldValue('custrecord_clgx_churn_act_other', actualOtherT);

                                                }

                                            }
                                        }
                                        else{
                                            churnCl.setFieldValue('custrecord_clgx_churn_act_other', 0);
                                        }
                                        if(actualSpace<actualSpaceL){
                                            if(actualSpaceT<0)
                                            {
                                                churnCl.setFieldValue('custrecord_clgx_churn_act_space', actualSpaceT*(-1));
                                            }
                                            else{

                                                churnCl.setFieldValue('custrecord_clgx_churn_act_space', actualSpaceT);

                                            }

                                        }else{
                                            churnCl.setFieldValue('custrecord_clgx_churn_act_space', 0);

                                        }
                                        if(actualNetwork<actualNetworkL){
                                            if(actualNetworkT<0)
                                            {
                                                churnCl.setFieldValue('custrecord_clgx_churn_act_network', actualNetworkT*(-1));
                                            }
                                            else{

                                                churnCl.setFieldValue('custrecord_clgx_churn_act_network', actualNetworkT);

                                            }

                                        }else{
                                            churnCl.setFieldValue('custrecord_clgx_churn_act_network', 0);

                                        }
                                        churnCl.setFieldValue('custrecord_clgx_churn_closeddate', closedDate);
                                        nlapiSubmitRecord(churnCl, false,true);

                                        createdCustomer=company;
                                        createdMarket=loc;
                                        createdClosedDate=closedDate;
                                  //      var emailSubject = 'Churn4';
                                   //     var emailBody='createdCustomer: '+createdCustomer+' createdMarket: '+createdMarket+' createdClosedDate: '+createdClosedDate;
                                    //    nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null); // Send email to Catalina

                                    }
                                    else{
                                        if((qInterA==qInterL)&&(qNetworkA==qNetworkL)&&(qPowerA==qPowerL)&&(qSpaceA==qSpaceL)&&(qOther==qOtherL))
                                        {
                                            /*
                                             Create New Churn Record, Status-=Closed,
                                             Reason=Renewal, Churn Type=Decrease. And
                                             include the Actual MRR churn section by category
                                             with the differences found when looking at the
                                             current consolidated invoices record and previous
                                             month’s consolidated invoices record.
                                             */
                                            if((created==0)||((createdCustomer==company)&&(createdMarket==loc)&&(createdClosedDate==closedDate)))
                                            {
                                                break;
                                            }
                                            else{
                                                if(created==0){
                                                    var newRecord = nlapiCreateRecord('customrecord_cologix_churn');
                                                    if(month==1)
                                                    {
                                                        var monthPrev=12;
                                                        var yearPrev=year-1;
                                                    }
                                                    else
                                                    {
                                                        var monthPrev=month-1;
                                                        var yearPrev=year;

                                                    }
                                                    var duedate=getExDate(monthPrev, yearPrev);
                                                    newRecord.setFieldValue('custrecord_clgx_churn_customer', company);
                                                    newRecord.setFieldText('custrecord_clgx_churn_location', loc);
                                                    newRecord.setFieldValue('custrecord_clgx_churn_status', 4);
                                                    newRecord.setFieldText('custrecord_clgx_churn_expecteddate', duedate);
                                                    newRecord.setFieldValue('custrecord_clgx_churn_salesrep', salesrepId);

                                                    newRecord.setFieldValue('custrecord_clgx_churn_type_churn', 3);
                                                    if(actualPower<actualPowerL){
                                                        if(actualPowerT<0)
                                                        {
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_power', actualPowerT*(-1));
                                                        }
                                                        else
                                                        {
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_power', actualPowerT);
                                                        }
                                                    }else{
                                                        newRecord.setFieldValue('custrecord_clgx_churn_act_power', 0);
                                                    }
                                                    if(actualOther<actualOtherL){
                                                        if(actualOtherT<0)
                                                        {
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_other', actualOtherT*(-1));
                                                        }
                                                        else
                                                        {
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_other', actualOtherT);
                                                        }
                                                    }else{
                                                        newRecord.setFieldValue('custrecord_clgx_churn_act_other', 0);
                                                    }
                                                    if(actualInterconnection<actualInterconnectionL){
                                                        if(actualInterconnectionT<0)
                                                        {
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnectionT*(-1));
                                                        }
                                                        else
                                                        {
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnectionT);

                                                        }
                                                    }else{
                                                        newRecord.setFieldValue('custrecord_clgx_churn_act_xconn', 0);
                                                    }
                                                    if(actualSpace<actualSpaceL){
                                                        if(actualSpaceT<0)
                                                        {
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_space', actualSpaceT*(-1));
                                                        }
                                                        else{
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_space', actualSpaceT);
                                                        }
                                                    } else{
                                                        newRecord.setFieldValue('custrecord_clgx_churn_act_space', 0);
                                                    }
                                                    if(actualNetwork<actualNetworkL){

                                                        if(actualNetworkT<0)
                                                        {
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_network', actualNetworkT*(-1));
                                                        }
                                                        else{
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_network', actualNetworkT);
                                                        }
                                                    }else{
                                                        newRecord.setFieldValue('custrecord_clgx_churn_act_network', 0);
                                                    }
                                                    newRecord.setFieldValue('custrecord_clgx_churn_reason', 10);
                                                    var date = new Date();
                                                    var closedate=((date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear());
                                                    //churn.setFieldValue('custrecord_clgx_churn_closeddate', dateParts[0]);
                                                    newRecord.setFieldValue('custrecord_clgx_churn_closeddate', closedDate);
                                                    nlapiSubmitRecord(newRecord, false,true);
                                                    created=1;
                                                    createdCustomer=company;
                                                    createdMarket=loc;
                                                    createdClosedDate=closedDate;
                                                 //   var emailSubject = 'Churn5';
                                                  //  var emailBody='createdCustomer: '+createdCustomer+' createdMarket: '+createdMarket+' createdClosedDate: '+createdClosedDate;
                                                  //  nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null); // Send email to Catalina

                                                }
                                            }


                                        }
                                        else{
                                            /*
                                             No=Create New Churn, Status=Closed, Reason=Blank Churn Type = Downgrade
                                             and include the Actual MRR churn section by category
                                             with the differences found when looking at the current
                                             consolidated invoices record and previous month’s
                                             consolidated invoices record. (This scenario should not
                                             happen because a disco churn record should already exist)
                                             */
                                            if((created==0)||((createdCustomer==company)&&(createdMarket==loc)&&(createdClosedDate==closedDate)))
                                            {
                                                break;
                                            }
                                            else{
                                                if(created==0){
                                                    var newRecord = nlapiCreateRecord('customrecord_cologix_churn');
                                                    if(month==1)
                                                    {
                                                        var monthPrev=12;
                                                        var yearPrev=year-1;
                                                    }
                                                    else
                                                    {
                                                        var monthPrev=month-1;
                                                        var yearPrev=year;

                                                    }
                                                    var duedate=getExDate(monthPrev, yearPrev);
                                                    newRecord.setFieldValue('custrecord_clgx_churn_customer', company);
                                                    newRecord.setFieldText('custrecord_clgx_churn_location', loc);
                                                    newRecord.setFieldValue('custrecord_clgx_churn_status', 4);
                                                    newRecord.setFieldText('custrecord_clgx_churn_expecteddate', duedate);
                                                    newRecord.setFieldValue('custrecord_clgx_churn_salesrep', salesrepId);
                                                    newRecord.setFieldValue('custrecord_clgx_churn_type_churn', 2);
                                                    if(actualPower<actualPowerL){
                                                        if(actualPowerT<0)
                                                        {
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_power', actualPowerT*(-1));
                                                        }
                                                        else
                                                        {
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_power', actualPowerT);
                                                        }
                                                    }else{
                                                        newRecord.setFieldValue('custrecord_clgx_churn_act_power', 0);
                                                    }
                                                    if(actualOther<actualOtherL){
                                                        if(actualOtherT<0)
                                                        {
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_other', actualOtherT*(-1));
                                                        }
                                                        else
                                                        {
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_other', actualOtherT);
                                                        }
                                                    }else{
                                                        newRecord.setFieldValue('custrecord_clgx_churn_act_other', 0);
                                                    }
                                                    if(actualInterconnection<actualInterconnectionL){
                                                        if(actualInterconnectionT<0)
                                                        {
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnectionT*(-1));
                                                        }
                                                        else
                                                        {
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnectionT);

                                                        }
                                                    }else{
                                                        newRecord.setFieldValue('custrecord_clgx_churn_act_xconn', 0);
                                                    }
                                                    if(actualSpace<actualSpaceL){
                                                        if(actualSpaceT<0)
                                                        {
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_space', actualSpaceT*(-1));
                                                        }
                                                        else{
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_space', actualSpaceT);
                                                        }
                                                    }else{
                                                        newRecord.setFieldValue('custrecord_clgx_churn_act_space', 0);
                                                    }
                                                    if(actualNetwork<actualNetworkL){

                                                        if(actualNetworkT<0)
                                                        {
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_network', actualNetworkT*(-1));
                                                        }
                                                        else{
                                                            newRecord.setFieldValue('custrecord_clgx_churn_act_network', actualNetworkT);
                                                        }
                                                    }else{
                                                        newRecord.setFieldValue('custrecord_clgx_churn_act_network', 0);
                                                    }
                                                    //  newRecord.setFieldValue('custrecord_clgx_churn_reason', 9);
                                                    newRecord.setFieldValue('custrecord_clgx_churn_closeddate', closedDate);
                                                    nlapiSubmitRecord(newRecord, false,true);
                                                    created=1;
                                                    createdCustomer=company;
                                                    createdMarket=loc;
                                                    createdClosedDate=closedDate;
                                                  //  var emailSubject = 'Churn6';
                                                  //  var emailBody='createdCustomer: '+createdCustomer+' createdMarket: '+createdMarket+' createdClosedDate: '+createdClosedDate;
                                                  //  nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null); // Send email to Catalina

                                                }
                                            }

                                        }

                                    }
                                }

                            }
                        }
                        else{
                            if((qInterA==qInterL)&&(qNetworkA==qNetworkL)&&(qPowerA==qPowerL)&&(qSpaceA==qSpaceL)&&(qOther==qOtherL))
                            {
                                /*
                                 Create New Churn Record, Status-=Closed,
                                 Reason=Renewal, Churn Type=Decrease. And
                                 include the Actual MRR churn section by category
                                 with the differences found when looking at the
                                 current consolidated invoices record and previous
                                 month’s consolidated invoices record.
                                 */
                                if((createdCustomer==company)&&(createdMarket==loc)&&(createdClosedDate==closedDate))
                                {
                                    break;
                                }
                                else{

                                    var newRecord = nlapiCreateRecord('customrecord_cologix_churn');
                                    if(month==1)
                                    {
                                        var monthPrev=12;
                                        var yearPrev=year-1;
                                    }
                                    else
                                    {
                                        var monthPrev=month-1;
                                        var yearPrev=year;

                                    }
                                    var duedate=getExDate(monthPrev, yearPrev);
                                    newRecord.setFieldValue('custrecord_clgx_churn_customer', company);
                                    newRecord.setFieldText('custrecord_clgx_churn_location', loc);
                                    newRecord.setFieldValue('custrecord_clgx_churn_status', 4);
                                    newRecord.setFieldText('custrecord_clgx_churn_expecteddate', duedate);
                                    newRecord.setFieldValue('custrecord_clgx_churn_salesrep', salesrepId);

                                    newRecord.setFieldValue('custrecord_clgx_churn_type_churn', 3);
                                    if(actualPower<actualPowerL){
                                        if(actualPowerT<0)
                                        {
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_power', actualPowerT*(-1));
                                        }
                                        else
                                        {
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_power', actualPowerT);
                                        }
                                    }else{
                                        newRecord.setFieldValue('custrecord_clgx_churn_act_power', 0);
                                    }
                                    if(actualOther<actualOtherL){
                                        if(actualOtherT<0)
                                        {
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_other', actualOtherT*(-1));
                                        }
                                        else
                                        {
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_other', actualOtherT);
                                        }
                                    }else
                                    {
                                        newRecord.setFieldValue('custrecord_clgx_churn_act_other', 0);
                                    }
                                    if(actualInterconnection<actualInterconnectionL){
                                        if(actualInterconnectionT<0)
                                        {
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnectionT*(-1));
                                        }
                                        else
                                        {
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnectionT);

                                        }
                                    }else{
                                        newRecord.setFieldValue('custrecord_clgx_churn_act_xconn', 0);

                                    }
                                    if(actualSpace<actualSpaceL){
                                        if(actualSpaceT<0)
                                        {
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_space', actualSpaceT*(-1));
                                        }
                                        else{
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_space', actualSpaceT);
                                        }
                                    }else{
                                        newRecord.setFieldValue('custrecord_clgx_churn_act_space', 0);
                                    }
                                    if(actualNetwork<actualNetworkL){

                                        if(actualNetworkT<0)
                                        {
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_network', actualNetworkT*(-1));
                                        }
                                        else{
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_network', actualNetworkT);
                                        }
                                    }else{
                                        newRecord.setFieldValue('custrecord_clgx_churn_act_network', 0);
                                    }
                                    newRecord.setFieldValue('custrecord_clgx_churn_reason', 10);
                                    var date = new Date();
                                    var closedate=((date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear());
                                    newRecord.setFieldValue('custrecord_clgx_churn_closeddate', closedDate);

                                    nlapiSubmitRecord(newRecord, false,true);
                                    createdCustomer=company;
                                    createdMarket=loc;
                                    createdClosedDate=closedDate;
                                   // var emailSubject = 'Churn';
                                   // var emailBody='createdCustomer: '+createdCustomer+' createdMarket: '+createdMarket+' createdClosedDate: '+createdClosedDate;
                                     //   nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null); // Send email to Catalina
                                }

                            }
                            else{
                                /*
                                 No=Create New Churn, Status=Closed, Reason=Blank Churn Type = Downgrade
                                 and include the Actual MRR churn section by category
                                 with the differences found when looking at the current consolidated invoices record
                                 and previous month’s consolidated invoices record.
                                 (This scenario should not happen because a disco churn record should already exist)
                                 */
                                if((createdCustomer==company)&&(createdMarket==loc)&&(createdClosedDate==closedDate))
                                {
                                    break;
                                }
                                else{


                                    var newRecord = nlapiCreateRecord('customrecord_cologix_churn');
                                    if(month==1)
                                    {
                                        var monthPrev=12;
                                        var yearPrev=year-1;
                                    }
                                    else
                                    {
                                        var monthPrev=month-1;
                                        var yearPrev=year;

                                    }
                                    var duedate=getExDate(monthPrev, yearPrev);
                                    newRecord.setFieldValue('custrecord_clgx_churn_customer', company);
                                    newRecord.setFieldText('custrecord_clgx_churn_location', loc);
                                    newRecord.setFieldValue('custrecord_clgx_churn_status', 4);
                                    newRecord.setFieldText('custrecord_clgx_churn_expecteddate', duedate);
                                    newRecord.setFieldValue('custrecord_clgx_churn_salesrep', salesrepId);
                                    newRecord.setFieldValue('custrecord_clgx_churn_type_churn', 2);
                                    if(actualPower<actualPowerL){
                                        if(actualPowerT<0)
                                        {
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_power', actualPowerT*(-1));
                                        }
                                        else
                                        {
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_power', actualPowerT);
                                        }
                                    }
                                    else{
                                        newRecord.setFieldValue('custrecord_clgx_churn_act_power', 0);
                                    }
                                    if(actualOther<actualOtherL){
                                        if(actualOtherT<0)
                                        {
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_other', actualOtherT*(-1));
                                        }
                                        else
                                        {
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_other', actualOtherT);
                                        }
                                    }else{
                                        newRecord.setFieldValue('custrecord_clgx_churn_act_other', 0);

                                    }
                                    if(actualInterconnection<actualInterconnectionL){
                                        if(actualInterconnectionT<0)
                                        {
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnectionT*(-1));
                                        }
                                        else
                                        {
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_xconn', actualInterconnectionT);

                                        }
                                    }else{
                                        newRecord.setFieldValue('custrecord_clgx_churn_act_xconn', 0);


                                    }
                                    if(actualSpace<actualSpaceL){
                                        if(actualSpaceT<0)
                                        {
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_space', actualSpaceT*(-1));
                                        }
                                        else{
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_space', actualSpaceT);
                                        }
                                    }else{
                                        newRecord.setFieldValue('custrecord_clgx_churn_act_space', 0);

                                    }
                                    if(actualNetwork<actualNetworkL){

                                        if(actualNetworkT<0)
                                        {
                                            newRecord.setFieldValue('custrecord_clgx_churn_act_network', actualNetworkT*(-1));
                                        }
                                        else
                                        {

                                            newRecord.setFieldValue('custrecord_clgx_churn_act_network', actualNetworkT);
                                        }
                                    }else{
                                        newRecord.setFieldValue('custrecord_clgx_churn_act_network', 0);
                                    }
                                    //  newRecord.setFieldValue('custrecord_clgx_churn_reason', 9);
                                    newRecord.setFieldValue('custrecord_clgx_churn_closeddate', closedDate);
                                    nlapiSubmitRecord(newRecord, false,true);
                                    createdCustomer=company;
                                    createdMarket=loc;
                                    createdClosedDate=closedDate;
                                  //  var emailSubject = 'Churn2';
                                  //  var emailBody='createdCustomer: '+createdCustomer+' createdMarket: '+createdMarket+' createdClosedDate: '+createdClosedDate;
                                  //  nlapiSendEmail(206211, 206211,emailSubject,emailBody,null,null,null,null); // Send email to Catalina

                                }
                            }

                        }

                    }

                }

                //set churn to false after everything is done
                var consInvToUpdate=nlapiLoadRecord('customrecord_clgx_consolidated_invoices', consolidateInvID);
                consInvToUpdate.setFieldValue('custrecord_clgx_consol_inv_churn', 'F');
                nlapiSubmitRecord(consInvToUpdate, false,true);

                var usageConsumtion = 10000 - parseInt(context.getRemainingUsage());
                var index = i + 1;
                nlapiLogExecution('DEBUG','SO ', ' | consolidateinvid =  ' + consolidateInvID + ' | Index = ' + index + ' of ' + searchCInv.length + ' | Usage - '+ usageConsumtion + '  |');

            }
        }

        nlapiLogExecution('DEBUG','Finished Execution', 'Total Remaining Usage - ' + context.getRemainingUsage() + ' -------------------------- Finished Scheduled Script --------------------------|');

    }

    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}
function getDueDate(netDays){
    // Return 'pay until' date
    var date = new Date();
    date.setDate(date.getDate() + netDays);

    var month = parseInt(date.getMonth()) + 1;
    var day = date.getDate();
    var year = date.getFullYear();

    if(parseInt(month) < 10){
        month = '0' + month;
    }

    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = month + '/' + day + '/' + year;

    return formattedDate;
}

function getDateToday(){
    var date = new Date();
    var month = parseInt(date.getMonth()) + 1;
    var day = date.getDate();
    var year = date.getFullYear();

    if(parseInt(month) < 10){
        month = '0' + month;
    }

    if(parseInt(day) < 10){
        day = '0' + day;
    }
    var formattedDate = month + '/' + day + '/' + year;

    return formattedDate;
}

//get the last day of the month
function daysInMonth(intMonth, intYear){
    if (intMonth < 0 || intMonth > 11){
        throw nlapiCreateError('10010', 'Valid months are from 0 (January) to 11 (December).');
    }
    var lastDayArray = [
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    ];
    if (intMonth != 1){
        return lastDayArray[intMonth];
    }
    if (intYear % 4 != 0){
        return lastDayArray[1];
    }
    if (intYear % 100 == 0 && intYear % 400 != 0){
        return lastDayArray[1];
    }
    return lastDayArray[1] + 1;
}

//get the start date and end date
function getDateRange(month,year){
    var stMonth = month - 1;
    var stDays  = daysInMonth(parseInt(stMonth),parseInt(year));
    var stYear  = year;

    var stStartDate = month + '/1/' + stYear;
    var stEndDate   = month + '/' + stDays + '/' + stYear;

    var arrDateRange = new Array();
    arrDateRange[0] = stStartDate;
    arrDateRange[1] =stEndDate;

    return arrDateRange;
}

function getDateFormated(date2format,language){

    var date = nlapiStringToDate(date2format);
    var month = parseInt(date.getMonth());
    var day = date.getDate();
    var year = date.getFullYear();

    var arrMonthEN = new Array();
    var arrMonthFR = new Array();

    arrMonthEN[0] = 'JAN';
    arrMonthEN[1] = 'FEB';
    arrMonthEN[2] = 'MAR';
    arrMonthEN[3] = 'APR';
    arrMonthEN[4] = 'MAY';
    arrMonthEN[5] = 'JUN';
    arrMonthEN[6] = 'JUL';
    arrMonthEN[7] = 'AUG';
    arrMonthEN[8] = 'SEP';
    arrMonthEN[9] = 'OCT';
    arrMonthEN[10] = 'NOV';
    arrMonthEN[11] = 'DEC';

    arrMonthFR[0] = 'JAN';
    arrMonthFR[1] = 'F&Eacute;V';
    arrMonthFR[2] = 'MAR';
    arrMonthFR[3] = 'AVR';
    arrMonthFR[4] = 'MAI';
    arrMonthFR[5] = 'JUN';
    arrMonthFR[6] = 'JUL';
    arrMonthFR[7] = 'AO&Ucirc;';
    arrMonthFR[8] = 'SEP';
    arrMonthFR[9] = 'OCT';
    arrMonthFR[10] = 'NOV';
    arrMonthFR[11] = 'D&Eacute;C';

    if(language == 'en_US' || language == 'en'){
        var stMonth = arrMonthEN[parseInt(month)];
    }
    else{
        var stMonth = arrMonthFR[parseInt(month)];
    }

    var stDate = day + '-' + stMonth + '-' + year;

    return stDate;
}

function getExDate(month,year){


    var arrMonth = new Array();


    arrMonth[1] = 'Jan';
    arrMonth[2] = 'Feb';
    arrMonth[3] = 'Mar';
    arrMonth[4] = 'Apr';
    arrMonth[5] = 'May';
    arrMonth[6] = 'Jun';
    arrMonth[7] = 'Jul';
    arrMonth[8] = 'Aug';
    arrMonth[9] = 'Sep';
    arrMonth[10] = 'Oct';
    arrMonth[11] = 'Nov';
    arrMonth[12] = 'Dec';


    var stMonth = arrMonth[parseInt(month)];

    var stDate = stMonth + ' ' + year;

    return stDate;
}