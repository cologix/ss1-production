nlapiLogExecution("audit","FLOStart",new Date().getTime());
function afterSubmit (type) {
	try {
		try {
			//------------- NNJ SYNC -----------------------------------------------------------------------------------
			var currentContext = nlapiGetContext();
	        var environment = currentContext.getEnvironment();
	        var row = nlapiLoadRecord(nlapiGetRecordType(),nlapiGetRecordId());
	        var facility = nlapiGetFieldValue('custrecord_clgx_xc_facility');
	        if (facility == 33 || facility == 34 || facility == 35 || facility == 36) {
	    		if (environment == 'PRODUCTION' && (type == 'xedit'  || currentContext.getExecutionContext() == 'userinterface')) {	
	    			
					nlapiLogExecution('DEBUG', 'flexapi error: ', error);
					var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
					record.setFieldValue('custrecord_clgx_queue_ping_record_id', nlapiGetRecordId());
					record.setFieldValue('custrecord_clgx_queue_ping_record_type', 9);
					record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
					var idRec = nlapiSubmitRecord(record, false,true);
	    			
	    			/*
					try {
						flexapi('POST','/netsuite/update', {
							'type': nlapiGetRecordType(),
							'id': nlapiGetRecordId(),
							'action': type,
							'userid': nlapiGetUser(),
							'user': nlapiLookupField('employee', nlapiGetUser(), 'entityid'),
							'data': json_serialize(row)
						});
					}
					catch (error) {
						nlapiLogExecution('DEBUG', 'flexapi error: ', error);
						var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
						record.setFieldValue('custrecord_clgx_queue_ping_record_id', nlapiGetRecordId());
						record.setFieldValue('custrecord_clgx_queue_ping_record_type', 9);
						record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
						var idRec = nlapiSubmitRecord(record, false,true);
					}
					*/
					
					
					
					try {
				        var soid = nlapiGetFieldValue('custrecord_xconnect_service_order');						
				        if (soid >= 0) {
					        var row = nlapiLoadRecord('salesorder', soid);
					        var ret = json_serialize(row);
					        ret = append_services_powers (ret, soid, 'salesorder');
					        
							nlapiLogExecution('DEBUG', 'flexapi error: ', error);
							var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
							record.setFieldValue('custrecord_clgx_queue_ping_record_id', soid);
							record.setFieldValue('custrecord_clgx_queue_ping_record_type', 3);
							record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
							var idRec = nlapiSubmitRecord(record, false,true);
							
							/*
					        flexapi('POST','/netsuite/update', {
					        	'type': 'salesorder',
						        'id': soid,
						        'action': type,
								'userid': nlapiGetUser(),
								'user': nlapiLookupField('employee', nlapiGetUser(), 'entityid'),
						        'data': ret
					        });
					        */
						    //nlapiLogExecution('DEBUG', 'flexapi request soid: ', soid);
						}
					}
					catch (error) {
						nlapiLogExecution('DEBUG', 'flexapi error: ', error);
						var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
						record.setFieldValue('custrecord_clgx_queue_ping_record_id', soid);
						record.setFieldValue('custrecord_clgx_queue_ping_record_type', 3);
						record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
						var idRec = nlapiSubmitRecord(record, false,true);
					}
		        }
	    	}
		}
		catch (error) {
	        if (error.getDetails != undefined){
	            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
	            throw error;
	        }
	        else{
	            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
	            throw nlapiCreateError('99999', error.toString());
	        }
		} 

//---------- End Sections ------------------------------------------------------------------------------------------------
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
