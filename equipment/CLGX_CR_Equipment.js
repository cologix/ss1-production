nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_Equipment.js
//	Script Name:	CLGX_CR_Equipment
//	Script Id:		customscript_clgx_cr_equipment
//	Script Runs:	Client
//	Script Type:	Client Script
//	Deployments:	Equipment
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		10/11/2013
//-------------------------------------------------------------------------------------------------

function saveRecord() {
	try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Details:	Verify if any of the mandatory ports are from the same equipment or unavailable - if yes, display an alert and don't allow saving the record
//-------------------------------------------------------------------------------------------------
		
		var saveOK = 1;
		var response = '';
		
		var equipMake = nlapiGetFieldValue('custrecord_clgx_equipment_make');
		var port0 = nlapiGetFieldValue('custrecord_clgx_equipment_port_count');
	    var equipmentmake = nlapiGetFieldText('custrecord_clgx_equipment_make');
	    var equipmentname = nlapiGetFieldValue('custrecord_clgx_equipment_name');
		var newEquimpentS= nlapiGetFieldValue('custrecord_clgx_equipment_sequence');
		var parentId=nlapiGetFieldValue('custrecord_clgx_equipment_parent');
		var parent = nlapiGetFieldText('custrecord_clgx_equipment_parent'); 
if(parentId!=null && parentId!=''){
	    var recordP = nlapiLoadRecord('customrecord_cologix_equipment',parentId);
	    var itemCount = recordP.getLineItemCount('recmachcustrecord_clgx_equipment_parent');
	    for ( var j = 1; j <= itemCount; j++ ) {
	    var seqChild= recordP.getLineItemValue('recmachcustrecord_clgx_equipment_parent', 'custrecord_clgx_equipment_sequence', j);
	    var nameChild=recordP.getLineItemValue('recmachcustrecord_clgx_equipment_parent', 'custrecord_clgx_equipment_name', j);
	    	if(seqChild==newEquimpentS){
	    		if(equipmentname!=nameChild)
	    			{
	    			
	    		response = 'Another child of this parent has this sequence number, please chose another one!';
				saveOK = 0;
	    			}
	    	}
	    }

		if((newEquimpentS!='' || newEquimpentS!=null)&&(equipmentmake == 'Cisco' && newEquimpentS==0))
			{
		
			response = 'If equipment is Cisco, sequence must be greater than or equal to one';
			saveOK = 0;
			
			}
		if ((equipMake != null || equipMake != '') && (port0 == null || port0 == '')) {
			response = 'If equipment is Cisco or Juniper, you must configure at least Port Count';
			saveOK = 0;
		}
}
		// display alert and permit save or not ----------------------------------------------------------------
		if(saveOK == 0){
			var alert = confirm(response);
			return false;
		}
		else{
			return true;
		}

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
