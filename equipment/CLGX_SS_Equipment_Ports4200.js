nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Equipment_Ports4200.js
//	Script Name:	CLGX_SS_Equipment_Ports4200
//	Script Id:		customscript_clgx_equipment_ports4200
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Equipment
//	@authors:		Catalina Taran
//-------------------------------------------------------------------------------------------------

function scheduled_create_eqports_4200 () {
    try {

        var arrColumns = new Array();
        var arrFilters = new Array();
        var searchEQ = nlapiSearchRecord('customrecord_cologix_equipment','customsearch_clgx_equiupdateport', arrFilters, arrColumns);

        for ( var i = 0; searchEQ != null && i < searchEQ.length; i++ ) { // loop fEQ

            var search = searchEQ[i];
            var columns = search.getAllColumns();
            var idEQ=search.getValue(columns[0]);
            var equiptype = search.getValue(columns[1]);
            var equipmake = search.getValue(columns[2]);
            var equipmentMake = search.getText(columns[2]);
            var newEquimpentS= search.getValue(columns[3]);
            var ports0 = parseInt(search.getText(columns[4]));
            var parent = search.getValue(columns[5]);
            var model=search.getValue(columns[6]);
            clgx_create_active_port (parseInt(ports0)+parseInt(1), newEquimpentS, idEQ, equiptype, equipmake, equipmentMake,1,1,0);
            clgx_create_active_port (parseInt(ports0)+parseInt(2), newEquimpentS, idEQ, equiptype, equipmake, equipmentMake,1,2,0);

        }



//---------- End Section 1 ------------------------------------------------------------------------------------------------

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function clgx_create_active_port (port, seq, equipid, equiptype, equipmake, equipmentMake,extra,order,rename) {

    var newRecord = nlapiCreateRecord('customrecord_clgx_active_port');
    newRecord.setFieldValue('custrecord_clgx_active_port_nbr', port);
    newRecord.setFieldValue('custrecord_clgx_active_port_equipment', equipid);
    newRecord.setFieldValue('custrecord_clgx_active_port_equip_type', equiptype);
    newRecord.setFieldValue('custrecord_clgx_active_port_equip_make', equipmake);
    newRecord.setFieldValue('custrecord_clgx_active_port_status', 2);
    var idNewRec = nlapiSubmitRecord(newRecord, false,true);

    // reload port record and update name
    var record = nlapiLoadRecord('customrecord_clgx_active_port',idNewRec);
    var nameOld = record.getFieldValue('name');
    var activePort = record.getFieldValue('custrecord_clgx_active_port_nbr');
    var equipname = record.getFieldText('custrecord_clgx_active_port_equipment');
    if(extra==0)
    {
        if (equipmentMake == 'Cisco'){
            port += 1;
            if(rename==1)
            {
                var name = equipname + '|' + 'xe-'+seq+'/' + port;
            }
            else
            {
                var name = equipname + '|' + 'fa-'+seq+'/' + port;
            }
        }
        else if(equipmentMake == 'Juniper'){
            if(rename==1)
            {
                var name = equipname + '|' + 'xe-' + seq + '/0/' + port;
            }
            else
            {
                var name = equipname + '|' + 'ge-' + seq + '/0/' + port;
            }
        }
        else if(equipmentMake == 'Aeponyx'){
            port += 1;
            var fields = ['custrecord_clgx_aeponyx_port','custrecord_clgx_aeponyx_wave'];
            var columns = nlapiLookupField('customrecord_clgx_aeponyx_ports', port, fields);
            var portA = columns.custrecord_clgx_aeponyx_port;
            var wave = columns.custrecord_clgx_aeponyx_wave;
            var name = equipname + '|' + portA + '|' + wave + 'nm';
            activePort = portA;
        }
        else{
            var name = nameOld;
        }
    }
    if((extra==1)&&(order==1))
    {

        var name = equipname + '|' + 'xe-1/1/0';
    }
    if((extra==1)&&(order==2))
    {

        var name = equipname + '|' + 'xe-1/1/2';
    }

    record.setFieldValue('name', name);
    record.setFieldValue('custrecord_clgx_active_port_nbr', activePort);
    var idRec = nlapiSubmitRecord(record, false,true);

    return idRec;
}
