nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Equipment.js
//	Script Name:	CLGX_SU_Equipment
//	Script Id:		customscript_clgx_su_equipment
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Employee
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		8/29/2013
//-------------------------------------------------------------------------------------------------

function beforeLoad (type, form) {
	try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Date - 9/9/2013
// Details:	If there are ports, make ports field inline text
//-----------------------------------------------------------------------------------------------------------------

		if (nlapiGetField('custrecord_clgx_equipment_port_count') != null && nlapiGetField('custrecord_clgx_equipment_port_count') != ''){
			var portCount = nlapiGetFieldValue('custrecord_clgx_equipment_port_count');
			if (portCount > 0){
				form.getField('custrecord_clgx_equipment_port_count').setDisplayType('inline');
			}
		}
		if (nlapiGetField('custrecord_clgx_equipment_port_count_1') != null && nlapiGetField('custrecord_clgx_equipment_port_count_1') != ''){
			var port1Count = nlapiGetFieldValue('custrecord_clgx_equipment_port_count_1');
			if (port1Count > 0){
				form.getField('custrecord_clgx_equipment_port_count_1').setDisplayType('inline');
			}
		}
		if (nlapiGetField('custrecord_clgx_equipment_port_count_2') != null && nlapiGetField('custrecord_clgx_equipment_port_count_2') != ''){
			var port2Count = nlapiGetFieldValue('custrecord_clgx_equipment_port_count_2');
			if (port2Count > 0){
				form.getField('custrecord_clgx_equipment_port_count_2').setDisplayType('inline');
			}
		}
		if (nlapiGetField('custrecord_clgx_equipment_port_count_3') != null && nlapiGetField('custrecord_clgx_equipment_port_count_3') != ''){
			var port3Count = nlapiGetFieldValue('custrecord_clgx_equipment_port_count_3');
			if (port3Count > 0){
				form.getField('custrecord_clgx_equipment_port_count_3').setDisplayType('inline');
			}
		}
		if (nlapiGetField('custrecord_clgx_equipment_port_count_4') != null && nlapiGetField('custrecord_clgx_equipment_port_count_4') != ''){
			var port4Count = nlapiGetFieldValue('custrecord_clgx_equipment_port_count_4');
			if (port4Count > 0){
				form.getField('custrecord_clgx_equipment_port_count_4').setDisplayType('inline');
			}
		}
		if (nlapiGetField('custrecord_clgx_active_port_equip_make') != null && nlapiGetField('custrecord_clgx_active_port_equip_make') != ''){
			form.getField('custrecord_clgx_active_port_equip_make').setDisplayType('inline');
		}

//---------- End Sections ------------------------------------------------------------------------------------------------
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function beforeSubmit (type, form) {
	try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Date - 10/10/2013
// Details:	When deleting an existing equipment (only admin roles), update XCS records and delete port records
//-----------------------------------------------------------------------------------------------------------------
		var currentContext = nlapiGetContext();
		stRole = currentContext.getRole();
		var equipid = nlapiGetRecordId();
		
		if (type == 'delete' && (stRole == -5 || stRole == 3)) {
			
			// search port records of this equipment
	        var arrColumns = new Array();
	        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_active_port_xc',null,null));
	        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_active_port_sequence',null,null));
	        var arrFilters = new Array();
	        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_active_port_equipment",null,"anyof",equipid));
	        var searchPorts = nlapiSearchRecord('customrecord_clgx_active_port', null, arrFilters, arrColumns);
	        
	        for ( var i = 0; searchPorts != null && i < searchPorts.length; i++ ) {
	        	var searchPort = searchPorts[i];
	        	var portid = searchPort.getValue('internalid');
	        	var xcid = searchPort.getValue('custrecord_clgx_active_port_xc');
	        	var sequenceid = searchPort.getValue('custrecord_clgx_active_port_sequence');
	        	
	        	// update the port on the related XC
	        	if(sequenceid == 1){ // A end
	        		nlapiSubmitField('customrecord_cologix_crossconnect', xcid, ['custrecord_clgx_a_end_port'], ['']);
	        	}
	        	else if(sequenceid == 2){ // B end
	        		nlapiSubmitField('customrecord_cologix_crossconnect', xcid, ['custrecord_clgx_b_end_port'], ['']);
	        	}
	        	else if(sequenceid == 3){ // C end
	        		nlapiSubmitField('customrecord_cologix_crossconnect', xcid, ['custrecord_clgx_c_end_port'], ['']);
	        	}
	        	else if(sequenceid == 10){ // Z end
	        		nlapiSubmitField('customrecord_cologix_crossconnect', xcid, ['custrecord_clgx_z_end_port'], ['']);
	        	}
	        	else{}
	        	// delete the port
	        	nlapiDeleteRecord('customrecord_clgx_active_port', portid);
	        }
		}
//---------- End Sections ------------------------------------------------------------------------------------------------
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function afterSubmit (type) {
	try {
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Details:	Manage ports for equipment record
//-----------------------------------------------------------------------------------------------------------------

		var currentContext = nlapiGetContext();
		var equipid = nlapiGetRecordId();
		
		if (currentContext.getExecutionContext() == 'userinterface') {
			
        	var equiptype = nlapiGetFieldValue('custrecord_cologix_equipment_type');
        	var equipmake = nlapiGetFieldValue('custrecord_clgx_equipment_make');
        	var equipmentMake = nlapiGetFieldText('custrecord_clgx_equipment_make');
        	
        	var ports0 = nlapiGetFieldText('custrecord_clgx_equipment_port_count');
        	var ports1 = nlapiGetFieldText('custrecord_clgx_equipment_port_count_1');
        	var ports2 = nlapiGetFieldText('custrecord_clgx_equipment_port_count_2');
        	var ports3 = nlapiGetFieldText('custrecord_clgx_equipment_port_count_3');
        	var ports4 = nlapiGetFieldText('custrecord_clgx_equipment_port_count_4');
        	
			// if new equipment, create port records for every available member 
			if (type == 'create') {

				// if number of ports0 is known for Member 0
				if (ports0 != null && ports0 !='') {
	            	for ( var i = 0; i < parseInt(ports0); i++ ) {
	            		var newid = clgx_create_active_port (i, 0, equipid, equiptype, equipmake, equipmentMake);
	            	}
				}
				if (equipmentMake == 'Juniper'){
					// if number of ports1 is known for Member 1
					if (ports1 != null && ports1 !='') {
		            	for ( var i = 0; i < parseInt(ports1); i++ ) {
		            		var newid = clgx_create_active_port (i, 1, equipid, equiptype, equipmake, equipmentMake);
		            	}
					}
					// if number of ports2 is known for Member 2
					if (ports2 != null && ports2 !='') {
		            	for ( var i = 0; i < parseInt(ports2); i++ ) {
		            		var newid = clgx_create_active_port (i, 2, equipid, equiptype, equipmake, equipmentMake);
		            	}
					}
					// if number of ports3 is known for Member 3
					if (ports3 != null && ports3 !='') {
		            	for ( var i = 0; i < parseInt(ports3); i++ ) {
		            		var newid = clgx_create_active_port (i, 3, equipid, equiptype, equipmake, equipmentMake);
		            	}
					}
					// if number of ports4 is known for Member 4
					if (ports4 != null && ports4 !='') {
		            	for ( var i = 0; i < parseInt(ports4); i++ ) {
		            		var newid = clgx_create_active_port (i, 4, equipid, equiptype, equipmake, equipmentMake);
		            	}
					}
				}
			}
			
			// if edit equipment, create port records for any new member 
			if (type == 'edit') {
	        	var oldEquipment = nlapiGetOldRecord();
	        	var ports0Old = oldEquipment.getFieldValue('custrecord_clgx_equipment_port_count');
	        	var ports1Old = oldEquipment.getFieldValue('custrecord_clgx_equipment_port_count_1');
	        	var ports2Old = oldEquipment.getFieldValue('custrecord_clgx_equipment_port_count_2');
	        	var ports3Old = oldEquipment.getFieldValue('custrecord_clgx_equipment_port_count_3');
	        	var ports4Old = oldEquipment.getFieldValue('custrecord_clgx_equipment_port_count_4');
				
				// if number of ports0 is known for Member 0 and if it was empty before editing
				if (ports0 != null && ports0 !='' && (ports0Old == null || ports0Old =='')) {
	            	for ( var i = 0; i < parseInt(ports0); i++ ) {
	            		var newid = clgx_create_active_port (i, 0, equipid, equiptype, equipmake, equipmentMake);
	            	}
				}
				if (equipmentMake == 'Juniper'){
					// if number of ports1 is known for Member 1 and if it was empty before editing
					if (ports1 != null && ports1 !='' && (ports1Old == null || ports1Old =='')) {
		            	for ( var i = 0; i < parseInt(ports1); i++ ) {
		            		var newid = clgx_create_active_port (i, 1, equipid, equiptype, equipmake, equipmentMake);
		            	}
					}
					// if number of ports2 is known for Member 2 and if it was empty before editing
					if (ports2 != null && ports2 !='' && (ports2Old == null || ports2Old =='')) {
		            	for ( var i = 0; i < parseInt(ports2); i++ ) {
		            		var newid = clgx_create_active_port (i, 2, equipid, equiptype, equipmake, equipmentMake);
		            	}
					}
					// if number of ports3 is known for Member 3 and if it was empty before editing
					if (ports3 != null && ports3 !='' && (ports3Old == null || ports3Old =='')) {
		            	for ( var i = 0; i < parseInt(ports3); i++ ) {
		            		var newid = clgx_create_active_port (i, 3, equipid, equiptype, equipmake, equipmentMake);
		            	}
					}
					// if number of ports4 is known for Member 4 and if it was empty before editing
					if (ports4 != null && ports4 !='' && (ports4Old == null || ports4Old =='')) {
		            	for ( var i = 0; i < parseInt(ports4); i++ ) {
		            		var newid = clgx_create_active_port (i, 4, equipid, equiptype, equipmake, equipmentMake);
		            	}
					}
				}

			}
		}
		
//---------- End Sections ------------------------------------------------------------------------------------------------

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function clgx_create_active_port (port, seq, equipid, equiptype, equipmake, equipmentMake) {

	var newRecord = nlapiCreateRecord('customrecord_clgx_active_port');
	newRecord.setFieldValue('custrecord_clgx_active_port_nbr', port);
	newRecord.setFieldValue('custrecord_clgx_active_port_equipment', equipid);
	newRecord.setFieldValue('custrecord_clgx_active_port_equip_type', equiptype);
	newRecord.setFieldValue('custrecord_clgx_active_port_equip_make', equipmake);
	newRecord.setFieldValue('custrecord_clgx_active_port_status', 2);
	var idNewRec = nlapiSubmitRecord(newRecord, false,true);
	
	// reload port record and update name
    var record = nlapiLoadRecord('customrecord_clgx_active_port',idNewRec);
    var nameOld = record.getFieldValue('name');
    var activePort = record.getFieldValue('custrecord_clgx_active_port_nbr');
    var equipname = record.getFieldText('custrecord_clgx_active_port_equipment');
    
	if (equipmentMake == 'Cisco'){
		port += 1;
		var name = equipname + '|' + 'fa-0/' + port;
	}
	else if(equipmentMake == 'Juniper'){
		var name = equipname + '|' + 'ge-' + seq + '/0/' + port;
	}
	else if(equipmentMake == 'Aeponyx'){
		port += 1;
		var fields = ['custrecord_clgx_aeponyx_port','custrecord_clgx_aeponyx_wave'];
		var columns = nlapiLookupField('customrecord_clgx_aeponyx_ports', port, fields);
		var portA = columns.custrecord_clgx_aeponyx_port;
		var wave = columns.custrecord_clgx_aeponyx_wave;
		var name = equipname + '|' + portA + '|' + wave + 'nm';
		activePort = portA;
	}
	else{
		var name = nameOld;
	}
	
    record.setFieldValue('name', name);
    record.setFieldValue('custrecord_clgx_active_port_nbr', activePort);
    var idRec = nlapiSubmitRecord(record, false,true);
    
    return idRec;
}
