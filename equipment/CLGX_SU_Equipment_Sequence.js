nlapiLogExecution("audit","FLOStart",new Date().getTime());
/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       18 Feb 2014     catalinataran
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       17 Feb 2014     catalinataran
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only)
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function beforeLoad (type, form) {
    try {
        //------------- Begin Section 1 -----------------------------------------------------------------------------------
        // Date - 9/9/2013
        // Details:	If there are ports, make ports field inline text
        //-----------------------------------------------------------------------------------------------------------------
        if (type == 'edit') {
            var equipid = nlapiGetRecordId();
            var recordEq = nlapiLoadRecord('customrecord_cologix_equipment',equipid);
            var itemCount = recordEq.getLineItemCount('recmachcustrecord_clgx_equipment_parent');
            if (nlapiGetField('custrecord_clgx_equipment_port_count') != null && nlapiGetField('custrecord_clgx_equipment_port_count') != ''){
                var portCount = nlapiGetFieldValue('custrecord_clgx_equipment_port_count');
                if (portCount > 0){
                    form.getField('custrecord_clgx_equipment_port_count').setDisplayType('inline');
                }
            }
            if(itemCount>0){
                form.getField('custrecord_clgx_equipment_parent').setDisplayType('disabled');
                form.getField('custrecord_clgx_equipment_sequence').setDisplayType('disabled');
                form.getField('custrecord_clgx_equipment_port_count').setDisplayType('disabled');
                form.getField('custrecord_clgx_rack_unit_equip').setMandatory(false);
            }
            if (nlapiGetField('custrecord_clgx_active_port_equip_make') != null && nlapiGetField('custrecord_clgx_active_port_equip_make') != ''){
                form.getField('custrecord_clgx_active_port_equip_make').setDisplayType('inline');
            }

        }
        //---------- End Sections ------------------------------------------------------------------------------------------------
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function beforeSubmit (type, form) {
    try {
        //------------- Begin Section 1 -----------------------------------------------------------------------------------
        // Date - 10/10/2013
        // Details:	When deleting an existing equipment (only admin roles), update XCS records and delete port records
        //-----------------------------------------------------------------------------------------------------------------

        var currentContext = nlapiGetContext();
        stRole = currentContext.getRole();
        var equipid = nlapiGetRecordId();


        if (type == 'delete' && (stRole == -5 || stRole == 3 || stRole == 18)) {

            // search port records of this equipment
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('internalid',null,null));
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_active_port_xc',null,null));
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_active_port_sequence',null,null));
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_active_port_equipment",null,"anyof",equipid));
            var searchPorts = nlapiSearchRecord('customrecord_clgx_active_port', null, arrFilters, arrColumns);

            for ( var i = 0; searchPorts != null && i < searchPorts.length; i++ ) {
                var searchPort = searchPorts[i];
                var portid = searchPort.getValue('internalid');
                var xcid = searchPort.getValue('custrecord_clgx_active_port_xc');
                var sequenceid = searchPort.getValue('custrecord_clgx_active_port_sequence');

                // update the port on the related XC
                if(sequenceid == 1){ // A end
                    nlapiSubmitField('customrecord_cologix_crossconnect', xcid, ['custrecord_clgx_a_end_port'], ['']);
                }
                else if(sequenceid == 2){ // B end
                    nlapiSubmitField('customrecord_cologix_crossconnect', xcid, ['custrecord_clgx_b_end_port'], ['']);
                }
                else if(sequenceid == 3){ // C end
                    nlapiSubmitField('customrecord_cologix_crossconnect', xcid, ['custrecord_clgx_c_end_port'], ['']);
                }
                else if(sequenceid == 10){ // Z end
                    nlapiSubmitField('customrecord_cologix_crossconnect', xcid, ['custrecord_clgx_z_end_port'], ['']);
                }
                else{}
                // delete the port
                nlapiDeleteRecord('customrecord_clgx_active_port', portid);
            }
        }
        //---------- End Sections ------------------------------------------------------------------------------------------------
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function afterSubmit(type){

    try {
        //------------- Begin Section 1 -----------------------------------------------------------------------------------
        // Details:	Manage ports for equipment record
        //-----------------------------------------------------------------------------------------------------------------

        var currentContext = nlapiGetContext();
        var equipid = nlapiGetRecordId();
        if (currentContext.getExecutionContext() == 'userinterface') {

            var equiptype = nlapiGetFieldValue('custrecord_cologix_equipment_type');
            var equipmake = nlapiGetFieldValue('custrecord_clgx_equipment_make');
            var equipmentMake = nlapiGetFieldText('custrecord_clgx_equipment_make');
            var newEquimpentS= nlapiGetFieldValue('custrecord_clgx_equipment_sequence');
            var ports0 = nlapiGetFieldText('custrecord_clgx_equipment_port_count');
            var parent = nlapiGetFieldText('custrecord_clgx_equipment_parent');
            var model=nlapiGetFieldText('custrecord_cologix_equipment_model');


            // if new equipment, create port records for every available member
            if (type == 'create') {
                if(newEquimpentS=='' || newEquimpentS==null){
                    if (equipmentMake == 'Cisco'){
                        newEquimpentS=1;

                    }
                    else{
                        newEquimpentS=0;
                    }


                }

                // if number of ports0 is known for Member 0
                if (ports0 != null && ports0 !='') {
                    for ( var i = 0; i < parseInt(ports0); i++ ) {

                        if(model.indexOf("4500") > -1)
                        {
                            var newid =clgx_create_active_port (i, newEquimpentS, equipid, equiptype, equipmake, equipmentMake,0,0,1,model);
                        }
                        else
                        {
                            var newid = clgx_create_active_port (i, newEquimpentS, equipid, equiptype, equipmake, equipmentMake,0,0,0,model);
                        }
                    }
                    if(model.indexOf("4200") > -1)
                    {
                        clgx_create_active_port (i, newEquimpentS, equipid, equiptype, equipmake, equipmentMake,1,1,0,model);
                        clgx_create_active_port (i, newEquimpentS, equipid, equiptype, equipmake, equipmentMake,1,2,0,model);
                    }

                }
            }
            if (type == 'edit') {
                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('internalid',null,null));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_active_port_xc',null,null));
                arrColumns.push(new nlobjSearchColumn('custrecord_clgx_active_port_sequence',null,null));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter("custrecord_clgx_active_port_equipment",null,"anyof",equipid));
                var searchPorts = nlapiSearchRecord('customrecord_clgx_active_port', null, arrFilters, arrColumns);
                var oldEquipmentSS = nlapiGetOldRecord();
                var ports0Old = oldEquipmentSS.getFieldValue('custrecord_clgx_equipment_port_count');
                var oldEquipmentS = oldEquipmentSS.getFieldValue('custrecord_clgx_equipment_sequence');
                if(newEquimpentS!='' && newEquimpentS!=null){
                    if (oldEquipmentS!=newEquimpentS){
                        for ( var i = 0; searchPorts != null && i < searchPorts.length; i++ ) {

                            var searchPort = searchPorts[i];
                            var portid = searchPort.getValue('internalid');
                            if(model.indexOf("4500") > -1)
                            {
                                clgx_update_active_port (portid, newEquimpentS, equipmentMake,1);
                            }
                            else
                            {
                                clgx_update_active_port (portid, newEquimpentS, equipmentMake,0);
                            }


                        }
                    }
                }

            }
        }
        nlapiLogExecution('DEBUG', 'Remaining usage', currentContext.getRemainingUsage());
    }




        //---------- End Sections ------------------------------------------------------------------------------------------------
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------

}

function clgx_update_active_port (port, seq, equipmake,rename){

    var record = nlapiLoadRecord('customrecord_clgx_active_port',port);
    var nameOld = record.getFieldValue('name');
    var arrA = nameOld.split("|");
    var equipName = arrA[0];
    var oldPortName = arrA[1];
    var oldPort = oldPortName.replace("ge-","");
    var arrB = oldPort.split("/");

    if (equipmake == 'Cisco'){
        if(rename==1)
        {
            var name =equipName + '|xe-' + seq + '/' + arrB[1] ;
        }
        else
        {
            var name =equipName + '|fa-' + seq + '/' + arrB[1] ;
        }
    }
    else if(equipmake == 'Juniper'){
        if(rename==1)
        {
            var name = equipName + '|xe-' + seq + '/' + arrB[1] + '/' + arrB[2];
        }
        else
        {

            var name = equipName + '|ge-' + seq + '/' + arrB[1] + '/' + arrB[2];
        }
    }
    else{
        var name = nameOld;
    }

    record.setFieldValue('name', name);
    var idNRec = nlapiSubmitRecord(record, false,true);

}
function clgx_create_active_port (port, seq, equipid, equiptype, equipmake, equipmentMake,extra,order,rename,model) {

    var newRecord = nlapiCreateRecord('customrecord_clgx_active_port');
    newRecord.setFieldValue('custrecord_clgx_active_port_nbr', port);
    newRecord.setFieldValue('custrecord_clgx_active_port_equipment', equipid);
    newRecord.setFieldValue('custrecord_clgx_active_port_equip_type', equiptype);
    newRecord.setFieldValue('custrecord_clgx_active_port_equip_make', equipmake);
    newRecord.setFieldValue('custrecord_clgx_active_port_status', 2);
    var idNewRec = nlapiSubmitRecord(newRecord, false,true);

    // reload port record and update name
    var record = nlapiLoadRecord('customrecord_clgx_active_port',idNewRec);
    var nameOld = record.getFieldValue('name');
    var activePort = record.getFieldValue('custrecord_clgx_active_port_nbr');
    var equipname = record.getFieldText('custrecord_clgx_active_port_equipment');
    nlapiLogExecution('DEBUG','equipmentMake', equipmentMake + ': ' + model+';'+extra);
    if(extra==0)
    {
        if(equipmentMake == 'Brocade' && model=='NetIron XMR 4000'){
            var name = equipname + '|' + 'eth1/' + port;
        }
        else if (equipmentMake == 'Cisco' && (model=='1841'|| model=='1941')){
            var name = equipname + '|' + 'fa0/0/' + port;
        }
        else if (equipmentMake == 'Cisco' && (model=='2821'|| model=='2960G48' || model=='2970'|| model=='3560G24PS'|| model=='3560G24TS'
            || model=='3560G48PS'|| model=='3750G'|| model=='3845'|| model=='4948'|| model=='494810GE'|| model=='4948e10GE'
            || model=='4948-F'|| model=='5300'|| model=='5505'|| model=='5508'|| model=='5510'|| model=='5515'
            || model=='5520'|| model=='5525'|| model=='6006'|| model=='6504E'|| model=='6509'|| model=='6509V'|| model=='6509ve'
            || model=='7206VXR'|| model=='Cisco 5506x'|| model=='WS-C3750X-12S'|| model=='WS-C4948E'|| model=='WS-C6509-E'|| model=='Cisco 5506x')){
            var name = equipname + '|' + 'gi0/' + port;
        }
        else if (equipmentMake == 'Cisco' && (model=='2948G-GE'|| model=='2960'|| model=='296048'|| model=='296048TT'|| model=='3548-XL-EN'
            || model=='356024PS'|| model=='356024TS'|| model=='356048PS'|| model=='4000'|| model=='6009'|| model=='6506'
            || model=='871-K9'|| model=='93120TX'|| model=='Nexus 3k'|| model=='Nexus 5696'|| model=='Nexus 6k'|| model=='Nexus 7010'
            || model=='WS-C2950G-24-E'|| model=='WS-C2960S-48FPS'|| model=='WS-C3400-24T'|| model=='WS-C3524-XL-EN'|| model=='WS-C3550-48T'
            || model=='WS-C3560-48TS'|| model=='WS-C3750E-24TD-E'|| model=='WS-C3750G-12T-S')){
            var name = equipname + '|' + 'fa0/' + port;
        }
        else if (equipmentMake == 'Cisco' && model=='7609'){
            var name = equipname + '|' + 'te0/' + port;
        }
        else if (equipmentMake == 'Cisco' && model=='Cisco ASA 5516-X'){
            var name = equipname + '|ASA-' + port;
        }
        else if (equipmentMake == 'Force' && (model=='10-s50ndc'|| model=='10-s55')){
            var name = equipname + '|' + 'ge0/' + port;
        }
        else if (equipmentMake == 'Foundry' && (model=='MLXe8'|| model=='NetIron 400')){
            var name = equipname + '|' + 'ethernet0/' + port;
        }
        else if (equipmentMake == 'Hewlett Packard (HP)' && (model=='2824'|| model=='2915')){
            var name = equipname + '|' + port;
        }
        else if (equipmentMake == 'Juniper' && (model=='4550'|| model=='EX2200'|| model=='EX2200-24P-4G (POE)'|| model=='EX2200-24T-4G'|| model=='EX2200-48P-4G (POE)'
            || model=='EX2200-48T-4G'|| model=='EX4200'|| model=='EX4200-24F'|| model=='EX4200-24T'|| model=='EX4200-48T'|| model=='EX4500'|| model=='EX4550'
            || model=='EX4550-32F'|| model=='MX5'|| model=='MX960'|| model=='MX960-S'|| model=='WLA321-WW'|| model=='WLA532-WW')){
            var name = equipname + '|' + 'ge-0/0/' + port;
        }
        else if (equipmentMake == 'Linksys' && (model=='SRW224P')){
            var name = equipname + '|' +'eport-unit1:'+ port;
        }
        else if (equipmentMake == 'Lucent' && (model=='3700 Stack')){
            var name = equipname + '|' +'ge-0/0/'+ port;
        }
        else if (equipmentMake == 'MRV' && (model=='NC316BU-16/15')){
            var name = equipname + '|' +'1.'+ port+'.1';
        }
        else if (equipmentMake == 'Cisco'){
            port += 1;
            if(rename==1)
            {
                var name = equipname + '|' + 'xe-'+seq+'/' + port;
            }
            else
            {
                var name = equipname + '|' + 'fa-'+seq+'/' + port;
            }
        }
        else if(equipmentMake == 'Juniper' ){
            if(model!='QFX5100-48S-6Q' && model!='QFX5110-48S-4C') {
                if (rename == 1) {
                    var name = equipname + '|' + 'xe-' + seq + '/0/' + port;
                }
                else {
                    var name = equipname + '|' + 'ge-' + seq + '/0/' + port;
                }
            }
            else{
                var name = equipname + '|'  + seq + '/0/' + port;
            }
        }
        else if(equipmentMake == 'Aeponyx'){
            port += 1;
            var fields = ['custrecord_clgx_aeponyx_port','custrecord_clgx_aeponyx_wave'];
            var columns = nlapiLookupField('customrecord_clgx_aeponyx_ports', port, fields);
            var portA = columns.custrecord_clgx_aeponyx_port;
            var wave = columns.custrecord_clgx_aeponyx_wave;
            var name = equipname + '|' + portA + '|' + wave + 'nm';
            activePort = portA;
        }
        else{
            var name = nameOld;
        }
    }
    if((extra==1)&&(order==1))
    {

        var name = equipname + '|' + 'xe-1/1/0';
    }
    if((extra==1)&&(order==2))
    {

        var name = equipname + '|' + 'xe-1/1/2';
    }

    record.setFieldValue('name', name);
    record.setFieldValue('custrecord_clgx_active_port_nbr', activePort);
    var idRec = nlapiSubmitRecord(record, false,true);

    return idRec;
}