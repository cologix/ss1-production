nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CI_Years_Customers.js
//	Script Name:	CLGX_SL_CI_Years_Customers
//	Script Id:		customscript_clgx_sl_ci_years_customers
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		4/25/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=297&deploy=1  
//-------------------------------------------------------------------------------------------------

function suitelet_ci_years_customers (request, response){
	try {
		var monthid = request.getParameter('monthid');
		var yearid = request.getParameter('yearid');
		if(monthid > 0){
			var objFile = nlapiLoadFile(1188537);
			var html = objFile.getValue();
			html = html.replace(new RegExp('{customersJSON}','g'), getCustomersGridJSON2(yearid, monthid));
		}
		else{
			var html = 'Please select a month from the left panel.';
		}
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function getCustomersGridJSON(yearid, monthid){

	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_year_display",null,"is",yearid));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_month_display",null,"anyof",monthid));
	var searchCustomers = nlapiLoadSearch('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_ci_customers');
	searchCustomers.addFilters(arrFilters);
	
	var resultSet = searchCustomers.runSearch();
	var arrCustomers = new Array();
	resultSet.forEachResult(function(searchResult) {
		var colObj = new Object();
		colObj = new Object();
		colObj["customerid"] = parseInt(searchResult.getValue('custrecord_clgx_consol_inv_customer', null, 'GROUP'));
		colObj["customer"] = searchResult.getText('custrecord_clgx_consol_inv_customer', null, 'GROUP');
		colObj["yearid"] = parseInt(yearid);
		colObj["monthid"] = monthid;
		arrCustomers.push(colObj);
		return true; // return true to keep iterating
	});

    return JSON.stringify(arrCustomers);
}


function getCustomersGridJSON2(yearid, monthid){

	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_year_display",null,"is",yearid));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_month_display",null,"anyof",monthid));
	var searchCustomers = nlapiLoadSearch('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_ci_customers');
	searchCustomers.addFilters(arrFilters);
	
	var resultSet = searchCustomers.runSearch();
	var arrCust = new Array();
	resultSet.forEachResult(function(searchResult) {
		var cust = searchResult.getValue('custrecord_clgx_consol_inv_customer', null, 'GROUP');
		if(!inArray (cust, arrCust)){
			arrCust.push(cust);
		}
		return true; // return true to keep iterating
	});

	var startDate = moment([yearid, (monthid-1), 1]).format('M/D/YYYY');
	var endDate = moment([yearid, (monthid-1), 1]).add('months', 1).date(1).subtract('days', 1).format('M/D/YYYY');
	
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",arrCust));
	arrFilters.push(new nlobjSearchFilter("entity",null,"noneof",'@NONE@'));
	arrFilters.push(new nlobjSearchFilter('trandate',null,'within',startDate,endDate));
	var searchCustomers = nlapiLoadSearch('transaction', 'customsearch_clgx_ci_customers_paid');
	searchCustomers.addFilters(arrFilters);
	
	var resultSet = searchCustomers.runSearch();
	var arrCustomers = new Array();
	var arrCust = new Array();
	resultSet.forEachResult(function(searchResult) {
		var columns = searchResult.getAllColumns();
		var colObj = new Object();
		colObj = new Object();
		colObj["customerid"] = parseInt(searchResult.getValue('entity', null, 'GROUP'));
		colObj["customer"] = searchResult.getText('entity', null, 'GROUP');
		colObj["yearid"] = parseInt(yearid);
		colObj["monthid"] = monthid;
		colObj["notpaid"] = parseInt(searchResult.getValue(columns[1]));
		arrCustomers.push(colObj);
		return true; // return true to keep iterating
	});

    return JSON.stringify(arrCustomers);
}

//check if value is in the array
function inArray(val, arr){	
  var bIsValueFound = false;
  for(var i = 0; i < arr.length; i++){
      if(val == arr[i]){
          bIsValueFound = true;
          break;
      }
  }
  return bIsValueFound;
}

