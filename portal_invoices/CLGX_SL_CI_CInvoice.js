nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CI_CInvoice.js
//	Script Name:	CLGX_SL_CI_CInvoice
//	Script Id:		customscript_clgx_sl_ci_cinvoice
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/14/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=268&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_so_ci_details (request, response){
	try {
		
		var customerid = request.getParameter('customerid');
		var yearid = request.getParameter('yearid');
		var monthid = request.getParameter('monthid');
		
		if(customerid > 0){
			var custname = nlapiLookupField('customer', customerid, 'entityid');
			var objFile = nlapiLoadFile(1223797);
			var html = objFile.getValue();
			html = html.replace(new RegExp('{custName}','g'),custname);
			html = html.replace(new RegExp('{invoicesJSON}','g'), getInvoicesJSON(customerid, yearid, monthid));
			//var html = ' | ' + customerid + ' | ' + yearid + ' | ' + monthid + ' | ';
		}
		else{
			var html = 'Please select a customer or a month from the left panel.';
		}
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function getInvoicesJSON(customerid, yearid, monthid){

	var objTree = new Object();
	objTree["text"] = '.';

	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_batch',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_date',null,null));
	arrColumns.push(new nlobjSearchColumn('created',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_year',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_month',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_pdf_file_id',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_currency',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_market',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_subtotal',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_tax_total',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_tax2_total',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_total',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_invoices',null,null));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_emailed_to',null,null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_year_display",null,"is",yearid));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_month_display",null,"anyof",monthid));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_customer",null,"anyof",customerid));
	//arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_year_display",null,"is","2014"));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_processed",null,"is",'T'));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_output",null,"anyof",1));
	var searchInvoices = nlapiSearchRecord('customrecord_clgx_consolidated_invoices', null, arrFilters, arrColumns);
	
	var arrCInvoices = new Array();
	for ( var k = 0; searchInvoices != null && k < searchInvoices.length; k++ ) {
		var searchInvoice = searchInvoices[k];
		
		var fileid = parseInt(searchInvoice.getValue('custrecord_clgx_consol_inv_pdf_file_id', null, null));
		try {
        	var objFile = nlapiLoadFile(fileid);
			var filename = objFile.getName();
			var fileurl = objFile.getURL();
        } 
        catch (e) {
            var str = String(e);
            if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
            	var fileid = 0;
				var filename = 'unknown';
				var fileurl = '';
            }
        }

		var objCInvoice = new Object();
		objCInvoice["nodeid"] = parseInt(fileid);
		objCInvoice["node"] = searchInvoice.getValue('custrecord_clgx_consol_inv_batch', null, null);
		objCInvoice["date"] = searchInvoice.getValue('created', null, null);
		objCInvoice["cyear"] = searchInvoice.getValue('custrecord_clgx_consol_inv_year', null, null);
		objCInvoice["cmonth"] = searchInvoice.getValue('custrecord_clgx_consol_inv_month', null, null);
		objCInvoice["fileid"] = parseInt(fileid);
		objCInvoice["file"] = filename;
		objCInvoice["fileurl"] = fileurl;
		objCInvoice["currency"] = searchInvoice.getValue('custrecord_clgx_consol_inv_currency', null, null);
		objCInvoice["template"] = searchInvoice.getValue('custrecord_clgx_consol_inv_market', null, null);
		objCInvoice["subtotal"] = '$' + searchInvoice.getValue('custrecord_clgx_consol_inv_subtotal', null, null);
		objCInvoice["tax1"] = '$' + searchInvoice.getValue('custrecord_clgx_consol_inv_tax_total', null, null);
		objCInvoice["tax2"] = '$' + searchInvoice.getText('custrecord_clgx_consol_inv_tax2_total', null, null);
		objCInvoice["total"] = '$' + searchInvoice.getValue('custrecord_clgx_consol_inv_total', null, null);
		objCInvoice["expanded"] = true;
		objCInvoice["iconCls"] = 'cinvoice';
		objCInvoice["leaf"] = false;
		
		var arrInvoiceChildren = new Array();
		
		var objInvoiceChild = new Object();
		objInvoiceChild["nodeid"] = 2;
		objInvoiceChild["node"] = 'Included Invoices';
		objInvoiceChild["expanded"] = true;
		objInvoiceChild["iconCls"] = 'invoices';
		objInvoiceChild["leaf"] = false;
		
		var arrListInvoices = searchInvoice.getValue('custrecord_clgx_consol_inv_invoices', null, null).split( ";" );
		var arrInvoices = new Array();
		for ( var l = 0; arrListInvoices != null  && l < arrListInvoices.length; l++ ) {
			
			if(parseInt(arrListInvoices[l]) > 0){
				var invoiceid = arrListInvoices[l];
				var invoiceNbr = nlapiLookupField('invoice', arrListInvoices[l], 'tranid');
			}
			else{
				var invoiceid = 0;
				var invoiceNbr = 'unknown';
			}
			var objInvoice = new Object();
			objInvoice["nodeid"] = parseInt(invoiceid);
			objInvoice["node"] = invoiceNbr;
			objInvoice["iconCls"] = 'invoice';
			objInvoice["leaf"] = true;
			arrInvoices.push(objInvoice);
		}
		objInvoiceChild["children"] = arrInvoices;
		arrInvoiceChildren.push(objInvoiceChild);
		
		var objInvoiceChild = new Object();
		objInvoiceChild["nodeid"] = 1;
		objInvoiceChild["node"] = 'Emailed To';
		objInvoiceChild["expanded"] = true;
		objInvoiceChild["iconCls"] = 'contacts';
		objInvoiceChild["leaf"] = false;
		
		var arrListContacts = searchInvoice.getValue('custrecord_clgx_consol_inv_emailed_to', null, null).split( ";" );
		var arrContacts = new Array();
		for ( var m = 0; arrListContacts != null  && m < arrListContacts.length; m++ ) {
			
			if(parseInt(arrListContacts[m]) > 0){
				var contactid = arrListContacts[m];
				var contactName = nlapiLookupField('contact', arrListContacts[m], 'entityid');
			}
			else{
				var contactid = 0;
				var contactName = 'unknown';
			}
			var objContact = new Object();
			objContact["nodeid"] = contactid;
			objContact["node"] = contactName;
			objContact["iconCls"] = 'contact';
			objContact["leaf"] = true;
			arrContacts.push(objContact);
		}
		objInvoiceChild["children"] = arrContacts;
		arrInvoiceChildren.push(objInvoiceChild);

		objCInvoice["children"] = arrInvoiceChildren;
		arrCInvoices.push(objCInvoice);
	}
	objTree["children"] = arrCInvoices;
	
    return JSON.stringify(objTree);
}


//check if value is in the array
function inArray(val, arr){	
  var bIsValueFound = false;
  for(var i = 0; i < arr.length; i++){
      if(val == arr[i]){
          bIsValueFound = true;
          break;
      }
  }
  return bIsValueFound;
}