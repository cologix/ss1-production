nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CI_Frame.js
//	Script Name:	CLGX_SL_CI_Frame
//	Script Id:		customscript_clgx_sl_ci_frame
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/14/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=263&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_so_ci_frame (request, response){
	try {
		var formFrame = nlapiCreateForm('Consolidated Invoices History');
		var fieldFrame = formFrame.addField('custpage_clgx_frame_html','inlinehtml', null, null, null);
		var frameHTML = '<iframe name="ci1" id="ci1" src="/app/site/hosting/scriptlet.nl?script=264&deploy=1" height="600px" width="1235px" frameborder="0" scrolling="no"></iframe>';
		fieldFrame.setDefaultValue(frameHTML);
		response.writePage( formFrame );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}