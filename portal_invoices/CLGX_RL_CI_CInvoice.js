nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_CI_CInvoice.js
//	Script Name:	CLGX_RL_CI_CInvoice
//	Script Id:		customscript_clgx_rl_ci_cinvoice
//	Script Nbr:		265
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		4/28/2014
//	RelativeURL:	/app/site/hosting/restlet.nl?script=265&deploy=1
//-------------------------------------------------------------------------------------------------

function restlet_ci_cinvoice (datain){
    try {
        var txtArg = datain;
        txtArg = txtArg.replace(/\"/g,"");
        txtArg = txtArg.replace(/\:/g,",");
        txtArg = txtArg.replace(/\{/g,"");
        txtArg = txtArg.replace(/\}/g,"");
        var arrArguments = new Array();
        arrArguments = txtArg.split( "," );
        datain=JSON.parse(datain);
        var customerid = datain.customerid;
        var li = datain.li;
        var yearid =datain.yearid;
        var monthid =datain.monthid;
        var statement=datain.statement;
        var from=datain.from;
        var to=datain.to;
        var pdf=datain.pdf;
        // nlapiSendEmail(206211,206211,'string11tyy',string,null,null,null,null);
        if(customerid > 0){
            var custname = nlapiLookupField('customer', customerid, 'entityid');
            custname = custname.replace(/&/g, "&amp;");
            custname = custname.replace(/'/g, "&#39;");
            if((statement!=null)&&(statement==1))
            {
                var objFile = nlapiLoadFile(2835477);
                var html = objFile.getValue();
                var custname = nlapiLookupField('customer', customerid, 'entityid');
                custname = custname.replace(/&/g, "&amp;");

                var language = nlapiLookupField('customer', customerid, 'language');
                //     nlapiSendEmail(206211,206211,'string11tyy',customerid,null,null,null,null);
                html = html.replace(new RegExp('{customeridbutton}','g'),customerid);
                html = html.replace(new RegExp('{customeridsta}','g'),customerid);

                if(li!=0)
                {
                    if(pdf==1)
                    {
                        var sendPDFEmail=1;
                        var returnFunction=getInvoicesJSONStatement(customerid,sendPDFEmail,custname,li,language);
                        html = html.replace(new RegExp('{custName}','g'),custname);
                        html = html.replace(new RegExp('{currencysum}','g'),returnFunction[1]);
                        html = html.replace(new RegExp('{invoicesJSONStatement}','g'),returnFunction[0] );
                        html = html.replace(new RegExp('{invoicesJSONStatementCSV}','g'),returnFunction[2] );
                        html = html.replace(new RegExp('{display}','g'),returnFunction[3] );
                        html = html.replace(new RegExp('{statementdatecsv}','g'),returnFunction[4] );
                        html = html.replace(new RegExp('{accountnumbercsv}','g'),returnFunction[5] );
                        html = html.replace(new RegExp('{typeref}','g'),returnFunction[6] );
                        html = html.replace(new RegExp('{li}','g'), li);
                        html = html.replace(new RegExp('{from}','g'), 0);
                        html = html.replace(new RegExp('{to}','g'), 0);
                        html = html.replace(new RegExp('{alertPDF}','g'),"alert('The PDF Statement File has been sent to your email');");

                    }
                    else
                    {
                        var sendPDFEmail=0;
                        var returnFunction=getInvoicesJSONStatement(customerid,sendPDFEmail,custname,li,language);
                        html = html.replace(new RegExp('{custName}','g'),custname);
                        html = html.replace(new RegExp('{currencysum}','g'),returnFunction[1]);
                        html = html.replace(new RegExp('{invoicesJSONStatement}','g'), returnFunction[0]);
                        html = html.replace(new RegExp('{invoicesJSONStatementCSV}','g'),returnFunction[2] );
                        html = html.replace(new RegExp('{display}','g'),returnFunction[3] );
                        html = html.replace(new RegExp('{statementdatecsv}','g'),returnFunction[4] );
                        html = html.replace(new RegExp('{accountnumbercsv}','g'),returnFunction[5] );
                        html = html.replace(new RegExp('{typeref}','g'),returnFunction[6] );
                        html = html.replace(new RegExp('{li}','g'), li);
                        html = html.replace(new RegExp('{from}','g'), 0);
                        html = html.replace(new RegExp('{to}','g'), 0);
                        html = html.replace(new RegExp('{alertPDF}','g'),'');
                    }
                }else if(li=='')
                {
                    var sendPDFEmail=0;
                    var returnFunction=getInvoicesJSONStatement(customerid,sendPDFEmail,custname,li,language);
                    html = html.replace(new RegExp('{custName}','g'),custname);
                    html = html.replace(new RegExp('{invoicesJSONStatement}','g'), returnFunction[0]);
                    html = html.replace(new RegExp('{invoicesJSONStatementCSV}','g'),returnFunction[2] );
                    html = html.replace(new RegExp('{currencysum}','g'),returnFunction[1]);
                    // html = html.replace(new RegExp('{invoicesJSONStatementCSV}','g'),returnFunction[2] );
                    html = html.replace(new RegExp('{display}','g'),returnFunction[3] );
                    html = html.replace(new RegExp('{statementdatecsv}','g'),returnFunction[4] );
                    html = html.replace(new RegExp('{accountnumbercsv}','g'),returnFunction[5] );
                    html = html.replace(new RegExp('{typeref}','g'),returnFunction[6] );
                    html = html.replace(new RegExp('{li}','g'), li);
                    html = html.replace(new RegExp('{from}','g'), 0);
                    html = html.replace(new RegExp('{to}','g'), 0);
                    html = html.replace(new RegExp('{alertPDF}','g'),'');
                }
                if((from!=0)&&(to!=0))
                {
                    if(pdf==1)
                    {

                        var sendPDFEmail=1;
                        html = html.replace(new RegExp('{custName}','g'),custname);
                        var returnFunction=getInvoicesJSONStatementForm(customerid,sendPDFEmail,custname,from,to,language);
                        html = html.replace(new RegExp('{currencysum}','g'),returnFunction[1]);
                        html = html.replace(new RegExp('{invoicesJSONStatement}','g'),returnFunction[0] );
                        html = html.replace(new RegExp('{invoicesJSONStatementCSV}','g'),returnFunction[2] );
                        html = html.replace(new RegExp('{statementdatecsv}','g'),returnFunction[4] );
                        html = html.replace(new RegExp('{accountnumbercsv}','g'),returnFunction[5] );
                        html = html.replace(new RegExp('{display}','g'),returnFunction[3] );
                        html = html.replace(new RegExp('{typeref}','g'),returnFunction[6] );
                        html = html.replace(new RegExp('{from}','g'), from);
                        html = html.replace(new RegExp('{to}','g'), to);
                        html = html.replace(new RegExp('{li}','g'), 0);
                        html = html.replace(new RegExp('{alertPDF}','g'),"alert('The PDF Statement File has been sent to your email');");

                    }
                    else
                    {
                        var sendPDFEmail=0;
                        html = html.replace(new RegExp('{custName}','g'),custname);
                        var returnFunction=getInvoicesJSONStatementForm(customerid,sendPDFEmail,custname,from,to,language);
                        html = html.replace(new RegExp('{currencysum}','g'),returnFunction[1]);
                        html = html.replace(new RegExp('{invoicesJSONStatement}','g'), returnFunction[0]);
                        html = html.replace(new RegExp('{invoicesJSONStatementCSV}','g'),returnFunction[2] );
                        html = html.replace(new RegExp('{statementdatecsv}','g'),returnFunction[4]);
                        html = html.replace(new RegExp('{accountnumbercsv}','g'),returnFunction[5]);
                        html = html.replace(new RegExp('{display}','g'),returnFunction[3] );
                        html = html.replace(new RegExp('{typeref}','g'),returnFunction[6] );
                        html = html.replace(new RegExp('{from}','g'), from);
                        html = html.replace(new RegExp('{to}','g'), to);
                        html = html.replace(new RegExp('{li}','g'), 0);
                        html = html.replace(new RegExp('{alertPDF}','g'),'');
                    }
                }
            }
            else
            {
                var objFile = nlapiLoadFile(1223797);
                var html = objFile.getValue();
                html = html.replace(new RegExp('{customeridsta}','g'),customerid);
                html = html.replace(new RegExp('{custName}','g'),custname);
                html = html.replace(new RegExp('{invoicesJSON}','g'), getInvoicesJSON(customerid, yearid, monthid));
            }
            //var html = ' | ' + customerid + ' | ' + yearid + ' | ' + monthid + ' | ';
        }
        else{
            var html = 'Please select a customer or a month from the left panel.';
        }
        return html;
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function getInvoicesJSONStatement(customerid,sendPDFEmail,custname,li,language){
    var series='[';
    var seriesCSV='[';
    var lstcolor='#BDBDBD;';
    var listofCustomersWJournals=[217913,1649,217937,347096,347127,1879,1887,2044];
    if(li!='')
    {
        var arrColumns = new Array();
        var arrFilters = new Array();
        var recCustomer = nlapiLoadRecord('customer', customerid);
        var accountNbr = recCustomer.getFieldValue('accountnumber');
        arrFilters.push(new nlobjSearchFilter("trandate",null,"onorafter",li));
        arrFilters.push(new nlobjSearchFilter("name",null,"anyof",customerid));
        var searchRefunds= nlapiSearchRecord('transaction','customsearch2422', arrFilters, arrColumns);
        var display='none';
        if((searchRefunds!=null)||(inArray(customerid,listofCustomersWJournals)))
        {
            var display='block';
        }
       //var searchCInvoices = nlapiSearchRecord('transaction','customsearch2618', arrFilters, arrColumns);
        var searchCInvoices = nlapiSearchRecord('transaction','customsearch7666', arrFilters, arrColumns);
        var statementTableHTML='';
        var invoiceamtPDF=0;
        var amountPaidPDF=0;
        var amountappliedPDF=0;
        var balancePDF=0;
        var invoicesArray=new Array();
        var mS = ['','JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
        var mSCSV = ['','Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        if(language=='fr_CA')
        {


            statementTableHTML =  '<table cellpadding="1" class="profile" align="center" table-layout="fixed">';
            statementTableHTML += '<thead>';
            statementTableHTML += '<tr style="border-right: 1px;border-right-color: #343a45; background-color:#fafafa;  ">';
            statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Date</th>';
            statementTableHTML +="<th style='border-right: 1px;border-right-color:#9ea7af; width:10px;text-align: left;'>Date d'échéance</th>";
            statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">#Facture</th>';
            statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Montant de la facture</th>';
            statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Montant payé</th>';
            statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Date du paiement</th>';
            statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">#Chèque</th>';
            statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:15px; text-align: left;">Crédit montant appliquées</th>';
            statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">#Note de Crédit</th>';
            statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Sold Impayé</th>';
            statementTableHTML += '</tr>';
            statementTableHTML += '</thead>';
        }
        else{
            statementTableHTML =  '<table cellpadding="1" class="profile" align="center" table-layout="fixed">';
            statementTableHTML += '<thead>';
            statementTableHTML += '<tr style="border-right: 1px;border-right-color: #343a45; background-color:#fafafa;  ">';
            statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Date</th>';
            statementTableHTML +=     '<th style="border-right: 1px;border-right-color:#9ea7af; width:10px;text-align: left;">Due Date</th>';
            statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Invoice #</th>';
            statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Invoice Amt</th>';
            statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Amount Paid</th>';
            statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Payment Date</th>';
            statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Check #</th>';
            statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:15px; text-align: left;">Credit Amount</th>';
            statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Other Trans #</th>';
            statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Balance Unpaid</th>';
            statementTableHTML += '</tr>';
            statementTableHTML += '</thead>';
        }

        //custbody_consolidate_inv_nbr
        for ( var i = 0; searchCInvoices != null && i < searchCInvoices.length; i++ ) {

            var searchCInvoice = searchCInvoices[i];
            var columns = searchCInvoice.getAllColumns();
            var creditamountapplied=0;
            var amountpaid=0;
            var balanceunpaid=0;
            var creditmemo="";
            var credittrue=0;
            var typeofPayment=searchCInvoice.getValue(columns[13]);
            var currency=searchCInvoice.getText(columns[11]);
            var amountAss=searchCInvoice.getValue(columns[14]);
            var amountPaidAss=searchCInvoice.getValue(columns[15]);
            var amountRemAss=searchCInvoice.getValue(columns[16]);
            var type=searchCInvoice.getValue(columns[12]);
            var foundCreditMemo=-1;
            if((typeofPayment!=null)&&(typeofPayment!=''))
            {
                //   nlapiSendEmail(206211,206211,'typeofPayment',typeofPayment+', '+searchCInvoice.getValue(columns[0]),null,null,null,null);
                var typeofPaymentArr=typeofPayment.split(',');
                var typeofPaymentArr=_.uniq(typeofPaymentArr);

                foundCreditMemo=_.indexOf(typeofPaymentArr, 'Credit Memo');
            }
            if(foundCreditMemo!=-1)
            {
                //  nlapiSendEmail(206211,206211,'foundCreditMemo',foundCreditMemo+', '+searchCInvoice.getValue(columns[0]),null,null,null,null);
                //custbody_consolidate_inv_nbr
                var arrColumns1 = new Array();
                var arrFilters1 = new Array();
                arrFilters1.push(new nlobjSearchFilter("name",null,"anyof",customerid));
                arrFilters1.push(new nlobjSearchFilter("custbody_consolidate_inv_nbr",null,"is",searchCInvoice.getValue(columns[2])));
                var searchCInvoices1 = nlapiSearchRecord('transaction','customsearch2424', arrFilters1, arrColumns1);
                if(searchCInvoices1!=null)
                {
                    for ( var j = 0; searchCInvoices1 != null && j < searchCInvoices1.length; j++ ) {
                        var searchCInvoice1 = searchCInvoices1[j];
                        var columns1 = searchCInvoice1.getAllColumns();
                        creditamountapplied=parseFloat(creditamountapplied)+parseFloat(searchCInvoice1.getValue(columns1[0]));
                        var creditmemoStr=searchCInvoice1.getValue(columns1[1]);
                        var creditMemoArr=creditmemoStr.split(',');
                        creditMemoArr=_.uniq(creditMemoArr);
                        for(var k=0;k<creditMemoArr.length;k++)
                        {
                            if(k < creditMemoArr.length-1)
                            {
                                creditmemo=creditmemo+creditMemoArr[k]+", ";
                            }
                            else
                            {
                                creditmemo=creditmemo+creditMemoArr[k];
                            }
                        }


                        credittrue=1;
                        // }
                    }
                }
                var valArr=amountAss.split(',');
                var counts = [];
                var duplicates=[];
                var duplicateValue=0;
                for(var p = 0; p < valArr.length; p++) {
                    if(counts[valArr[p]] === undefined) {
                        counts[valArr[p]] = 1;
                    } else {
                        duplicates.push(valArr[p]);
                    }
                }
                if(duplicates.length>0){
                    for(var r=0;r<duplicates.length;r++)
                    {
                        var splitDuplicates=duplicates[r].split('=');
                        duplicateValue=parseFloat(duplicateValue)+parseFloat(splitDuplicates[1]);
                    }

                }
                //duplicates Paid
                var valArrPaid=amountPaidAss.split(',');
                var countsPaid = [];
                var duplicatesPaid=[];
                var duplicateValuePaid=0;
                for(var p = 0; p < valArrPaid.length; p++) {
                    if(countsPaid[valArrPaid[p]] === undefined) {
                        countsPaid[valArrPaid[p]] = 1;
                    } else {
                        duplicatesPaid.push(valArrPaid[p]);
                    }
                }
                if(duplicatesPaid.length>0){
                    for(var r=0;r<duplicatesPaid.length;r++)
                    {
                        var splitDuplicatesPaid=duplicatesPaid[r].split('=');
                        duplicateValuePaid=parseFloat(duplicateValuePaid)+parseFloat(splitDuplicatesPaid[1]);
                    }

                }
                //duplicates Remaining
                var valArrRem=amountRemAss.split(',');
                var countsRem = [];
                var duplicatesRem=[];
                var duplicateValueRem=0;
                for(var p = 0; p < valArrRem.length; p++) {
                    if(countsRem[valArrRem[p]] === undefined) {
                        countsRem[valArrRem[p]] = 1;
                    } else {
                        duplicatesRem.push(valArrRem[p]);
                    }
                }
                if(duplicatesRem.length>0){
                    for(var r=0;r<duplicatesRem.length;r++)
                    {
                        var splitDuplicatesRem=duplicatesRem[r].split('=');
                        duplicateValueRem=parseFloat(duplicateValueRem)+parseFloat(splitDuplicatesRem[1]);
                    }

                }




                var duedateArrFinalStr='';
                var duedate=searchCInvoice.getValue(columns[1]);
                if(duedate!=''){
                    var duedateArr=duedate.split(',');

                    duedateArr=_.uniq(duedateArr);
                    for(var k=0;k<duedateArr.length;k++)
                    {
                        if(duedateArr[k]!=null){
                            //      nlapiSendEmail(206211,206211,'splitduek',duedateArr[k],null,null,null,null);
                            var splitdue=duedateArr[k].split('/');

                            if(k < duedateArr.length-1)
                            {

                                duedateArrFinalStr=duedateArrFinalStr+splitdue[1]+'-'+mS[splitdue[0]]+'-'+splitdue[2]+", ";

                            }
                            else
                            {
                                duedateArrFinalStr=duedateArrFinalStr+splitdue[1]+'-'+mS[splitdue[0]]+'-'+splitdue[2];
                            }
                        }


                    }
                }
                var paymentdate=searchCInvoice.getValue(columns[5]);
                var paymentdateArr=paymentdate.split(',');
                paymentdateArr=_.uniq(paymentdateArr);
                var paymentdateFinalStr='';
                for(var k=0;k<paymentdateArr.length;k++)
                {
                    if((paymentdateArr[k]!=null)&&(paymentdateArr[k]!='')){
                        var splitpay=paymentdateArr[k].split('/');
                        if(k < paymentdateArr.length-1)
                        {
                            paymentdateFinalStr=paymentdateFinalStr+splitpay[1]+'-'+mS[splitpay[0]]+'-'+splitpay[2]+", ";
                        }
                        else
                        {
                            paymentdateFinalStr=paymentdateFinalStr+splitpay[1]+'-'+mS[splitpay[0]]+'-'+splitpay[2];
                        }
                    }
                }
                var check=searchCInvoice.getValue(columns[6]);
                var checkArr=check.split(',');
                var checkFinalStr='';
                checkArr=_.uniq(checkArr);
                checkArr= _.difference(checkArr, creditMemoArr);
                for(var k=0;k<checkArr.length;k++)
                {
                    if(k < checkArr.length-1)
                    {
                        checkFinalStr=checkFinalStr+checkArr[k]+", ";
                    }
                    else
                    {
                        checkFinalStr=checkFinalStr+checkArr[k];
                    }
                }
                if(i>0)
                {
                    if((lstcolor!='')&&(lstcolor=='#BDBDBD;'))
                    {
                        var colorcode='#FAFAFA;';
                    }
                    else{
                        var colorcode='#BDBDBD;';
                    }
                    var lastcolorcode=colorcode;
                }
                else{
                    var colorcode=lstcolor;
                }
                // _.difference([1, 2, 3, 4, 5], [5, 2, 10]);

                var amntForString=(parseFloat(searchCInvoice.getValue(columns[3]))-parseFloat(duplicateValue)).toFixed(2);
                var invAmString=(amntForString < 0) ? '($'+(amntForString)*parseFloat(-1).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+')' : '$'+parseFloat(amntForString).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                var balanceUnp= parseFloat(searchCInvoice.getValue(columns[9]))-parseFloat(duplicateValueRem);
                var amountPaidNoCRedit=0;
                if(creditamountapplied!=parseFloat(searchCInvoice.getValue(columns[4])))
                {
                    var amountPaidNoCRedit=parseFloat(amntForString)-parseFloat(creditamountapplied)-parseFloat(balanceUnp);
                }

                //  nlapiSendEmail(206211,206211,'string11tyy',parseFloat(searchCInvoice.getValue(columns[8])).toFixed(2),null,null,null,null);
                invoicesArray.push(searchCInvoice.getValue(columns[2]));
                series += '\n{"date":"' + ((type =='CustPymt')?'':searchCInvoice.getValue(columns[0]) )  +
                    '","duedate":"' +   duedateArrFinalStr +
                    '","invoice":"' + ((searchCInvoice.getValue(columns[2]) !='- None -') ? searchCInvoice.getValue(columns[2]) : '') +
                    '","invoiceid":"'+searchCInvoice.getValue(columns[2])+' ",' +
                    '"invoiceam":' +   amntForString  +
                    ',"amountpaid":' +  ((type =='CustPymt') ? parseFloat(searchCInvoice.getValue(columns[4]))*parseFloat(-1).toFixed(2) : amountPaidNoCRedit) +
                    ',"paymentdate":"' +  paymentdateFinalStr +
                    '","check":"' +  checkFinalStr +
                    '","creditamountapplied":'+creditamountapplied +',"creditmemo":"'+creditmemo+'","balanceunpaid":' + balanceUnp+
                    ',"unapplied":' +  parseInt(0) +

                    '},';
                var amntForString1=((type =='CustPymt') ? parseFloat(searchCInvoice.getValue(columns[4]))*parseFloat(-1).toFixed(2) : amountPaidNoCRedit);
                var amountpaidString=(amntForString1 < 0) ? '($'+parseFloat(amntForString1)*parseFloat(-1).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+')' : '$'+parseFloat(amntForString1).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                var balanceString=( type =='CustPymt') ? '($'+( parseFloat(balanceUnp)).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+')' : '$'+ parseFloat(balanceUnp).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');


                invoiceamtPDF=parseFloat(invoiceamtPDF)+parseFloat(amntForString);
                amountPaidPDF=parseFloat(amountPaidPDF)+parseFloat(amountPaidNoCRedit);
                amountappliedPDF=parseFloat(amountappliedPDF)+parseFloat(creditamountapplied);
                var datePDFSplit=searchCInvoice.getValue(columns[0]).split('/');
                var dateStringPDF=datePDFSplit[1]+'-'+mS[datePDFSplit[0]]+'-'+datePDFSplit[2];
                seriesCSV += '\n{"Date":"' + ((type =='CustPymt')?'':dateStringPDF )    +
                    '","Due Date":"' +   duedateArrFinalStr +
                    '","Invoice #":"' + ((searchCInvoice.getValue(columns[2]) !='- None -') ? searchCInvoice.getValue(columns[2]) : '') +
                    '","Invoice Amt":"' +  invAmString +
                    '","Amount Paid":"' +  amountpaidString +
                    '","Payment Date":"' +  paymentdateFinalStr +
                    '","Check #":"' +  checkFinalStr +
                    '","Credit Amount":"'+'$'+parseFloat(creditamountapplied).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'","Other Trans#":"'+creditmemo+'","Balance Unpaid":"' + balanceString+
                    '"},';
                var amountBalance=( type =='CustPymt') ? parseFloat(searchCInvoice.getValue(columns[9]))*parseFloat(-1).toFixed(2):parseFloat(searchCInvoice.getValue(columns[9])).toFixed(2);
                balancePDF=parseFloat(balancePDF)+parseFloat(balanceUnp);
                checkFinalStr=checkFinalStr.replace(/&/g, 'AND');
                statementTableHTML += '<tr style="border-right: 1px;border-right-color: #343a45; background-color:#fafafa;">';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+((type =='CustPymt')?'':dateStringPDF ) +'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+duedateArrFinalStr+'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+((searchCInvoice.getValue(columns[2]) !='- None -') ? searchCInvoice.getValue(columns[2]) : '')+'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+ invAmString+'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+amountpaidString+'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+ paymentdateFinalStr +'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+checkFinalStr+'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">$'+parseFloat(creditamountapplied).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+creditmemo+'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+balanceString+'</td>';
                statementTableHTML += '</tr>';
                lstcolor=colorcode;

            }


            else{
                //   nlapiSendEmail(206211,206211,'Credit Memo Emp','',null,null,null,null);
                var duedateArrFinalStr='';
                var duedate=searchCInvoice.getValue(columns[1]);
                var duedateArr=duedate.split(',');
                if(duedate!='')
                {
                    duedateArr=_.uniq(duedateArr);
                    for(var k=0;k<duedateArr.length;k++)
                    {
                        if(duedateArr[k]!=null)
                        {
                            var splitdue=duedateArr[k].split('/');
                            if(k < duedateArr.length-1)
                            {

                                duedateArrFinalStr=duedateArrFinalStr+splitdue[1]+'-'+mS[splitdue[0]]+'-'+splitdue[2]+", ";
                            }
                            else
                            {
                                duedateArrFinalStr=duedateArrFinalStr+splitdue[1]+'-'+mS[splitdue[0]]+'-'+splitdue[2];
                            }
                        }
                    }
                }
                //  nlapiSendEmail(206211,206211,'duedateArrFinalStr',duedateArrFinalStr,null,null,null,null);
                var paymentdate=searchCInvoice.getValue(columns[5]);
                var paymentdateArr=paymentdate.split(',');
                var paymentdateFinalStr='';
                paymentdateArr=_.uniq(paymentdateArr);
                for(var k=0;k<paymentdateArr.length;k++)
                {
                    if((paymentdateArr[k]!=null)&&(paymentdateArr[k]!=''))
                    {
                        var splitpay=paymentdateArr[k].split('/');
                        if(k < paymentdateArr.length-1)
                        {
                            paymentdateFinalStr=paymentdateFinalStr+splitpay[1]+'-'+mS[splitpay[0]]+'-'+splitpay[2]+", ";
                        }
                        else
                        {
                            paymentdateFinalStr=paymentdateFinalStr+splitpay[1]+'-'+mS[splitpay[0]]+'-'+splitpay[2];
                        }
                    }
                }
                var check=searchCInvoice.getValue(columns[6]);
                var checkArr=check.split(',');
                var checkFinalStr='';
                checkArr=_.uniq(checkArr);
                for(var k=0;k<checkArr.length;k++)
                {
                    if(k < checkArr.length-1)
                    {
                        checkFinalStr=checkFinalStr+checkArr[k]+", ";
                    }
                    else
                    {
                        checkFinalStr=checkFinalStr+checkArr[k];
                    }
                }
                //  if(searchCInvoice.getValue(columns[7])!=0)
                // {
                var valArr=amountAss.split(',');
                var counts = [];
                var duplicates=[];
                var duplicateValue=0;
                for(var p = 0; p < valArr.length; p++) {
                    if(counts[valArr[p]] === undefined) {
                        counts[valArr[p]] = 1;
                    } else {
                        duplicates.push(valArr[p]);
                    }
                }
                if(duplicates.length>0){
                    for(var r=0;r<duplicates.length;r++)
                    {
                        var splitDuplicates=duplicates[r].split('=');
                        duplicateValue=parseFloat(duplicateValue)+parseFloat(splitDuplicates[1]);
                    }

                }
                //duplicates Paid
                var valArrPaid=amountPaidAss.split(',');
                var countsPaid = [];
                var duplicatesPaid=[];
                var duplicateValuePaid=0;
                for(var p = 0; p < valArrPaid.length; p++) {
                    if(countsPaid[valArrPaid[p]] === undefined) {
                        countsPaid[valArrPaid[p]] = 1;
                    } else {
                        duplicatesPaid.push(valArrPaid[p]);
                    }
                }
                if(duplicatesPaid.length>0){
                    for(var r=0;r<duplicatesPaid.length;r++)
                    {
                        var splitDuplicatesPaid=duplicatesPaid[r].split('=');
                        duplicateValuePaid=parseFloat(duplicateValuePaid)+parseFloat(splitDuplicatesPaid[1]);
                    }

                }
                //duplicates Remaining
                var valArrRem=amountRemAss.split(',');
                var countsRem = [];
                var duplicatesRem=[];
                var duplicateValueRem=0;
                for(var p = 0; p < valArrRem.length; p++) {
                    if(countsRem[valArrRem[p]] === undefined) {
                        countsRem[valArrRem[p]] = 1;
                    } else {
                        duplicatesRem.push(valArrRem[p]);
                    }
                }

                if(duplicatesRem.length>0){
                    for(var r=0;r<duplicatesRem.length;r++)
                    {
                        var splitDuplicatesRem=duplicatesRem[r].split('=');
                        if((r>0)&&(type!='CustPymt'))
                        {
                            // if((parseFloat(splitDuplicatesRem[1]))!= (parseFloat(duplicateValueRem)))
                            // {
                            duplicateValueRem=parseFloat(duplicateValueRem)+parseFloat(splitDuplicatesRem[1]);

                            //}
                        }
                        else{
                            duplicateValueRem=parseFloat(duplicateValueRem)+parseFloat(splitDuplicatesRem[1]);
                        }

                    }


                }
                var duplicateValueRem1=0;
                var uniq=_.uniq(valArrRem);
                if(uniq.length>0){
                    for(var r=0;r<uniq.length;r++)
                    {
                        var splitDuplicatesRem=uniq[r].split('=');
                        if((r>0)&&(type!='CustPymt'))
                        {
                            // if((parseFloat(splitDuplicatesRem[1]))!= (parseFloat(duplicateValueRem)))
                            //{
                            duplicateValueRem1=parseFloat(duplicateValueRem1)+parseFloat(splitDuplicatesRem[1]);
                            //}
                        }
                        else{
                            duplicateValueRem1=parseFloat(duplicateValueRem1)+parseFloat(splitDuplicatesRem[1]);
                        }

                    }

                }



                //  invoicesArray.push(searchCInvoice.getValue(columns[0]));
                var balanceUnp=( type =='CustPymt') ? ( parseFloat(searchCInvoice.getValue(columns[9]))-parseFloat(duplicateValueRem))*parseFloat(-1) : ( parseFloat(searchCInvoice.getValue(columns[9]))-parseFloat(duplicateValueRem));

                var creditamountapplied=0;
                if(searchCInvoice.getValue(columns[17])=='YES')
                {

                    // var amountPaidInvFinal=Math.abs(parseFloat(duplicateValueRem));
                    //var balanceUnp=amountPaidInvFinal*(-1);
                    var amountPaidInvFinal=Math.abs(parseFloat(balanceUnp));
                    amountpaidString='$'+ Math.abs(parseFloat(duplicateValueRem)).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                }
                else
                {

                    var amountPaidInvFinal=((type =='CustPymt') ? (parseFloat(searchCInvoice.getValue(columns[3]))-parseFloat(duplicateValue))*parseFloat(-1).toFixed(2) : (parseFloat(searchCInvoice.getValue(columns[4]))-parseFloat(duplicateValuePaid)));

                    if(type =='CustCred')
                    {
                        var amountPaidInvFinal=parseFloat(0);
                        if(duplicateValueRem>0)
                        {
                            var creditamountapplied=Math.abs(parseFloat(duplicateValueRem1));
                            //    nlapiSendEmail(206211,206211,'credit am1',duplicateValueRem,null,null,null,null);
                        }
                        else{

                            var creditamountapplied=Math.abs(parseFloat(searchCInvoice.getValue(columns[7])));


                        }
                        var balanceUnp=creditamountapplied*(-1);
                    }
                }
                amountPaidPDF=parseFloat(amountPaidPDF)+parseFloat(amountPaidInvFinal);
                //PDF & CSV END
                var amntForString=((type =='CustInvc') ? (parseFloat(searchCInvoice.getValue(columns[3]))-parseFloat(duplicateValue)).toFixed(2) : 0  );
                var invAmString=(amntForString < 0) ? '($'+(amntForString)*parseFloat(-1).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+')' : '$'+parseFloat(amntForString).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

                var amntForString1=amountPaidInvFinal;
                var amountpaidString=(amntForString1 < 0) ? '($'+(amntForString1).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+')' : '$'+parseFloat(amntForString1).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

                var balanceString=(balanceUnp<0) ? '($'+(parseFloat((balanceUnp))*parseFloat(-1)).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+')' : '$'+(balanceUnp).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

                invoiceamtPDF=parseFloat(invoiceamtPDF)+parseFloat(amntForString);
                amountappliedPDF=parseFloat(amountappliedPDF)+parseFloat(creditamountapplied);
                var amountBalance=parseFloat(balanceUnp);

                balancePDF=parseFloat(balancePDF)+parseFloat(balanceUnp);

                //PDF & CSV END
                series += '\n{"date":"' + ((type =='CustPymt')?'':searchCInvoice.getValue(columns[0]) ) +
                    '","duedate":"' +   duedateArrFinalStr +
                    '","invoice":"' + ((searchCInvoice.getValue(columns[2]) !='- None -') ? searchCInvoice.getValue(columns[2]) : '') +
                    '","invoiceid":"'+searchCInvoice.getValue(columns[2])+' ",' +
                    '"invoiceam":' +   ((type =='CustInvc') ? (parseFloat(searchCInvoice.getValue(columns[3]))-parseFloat(duplicateValue)).toFixed(2) : 0  )+
                    ',"amountpaid":' +  amountPaidInvFinal +
                    ',"paymentdate":"' +  ((type =='CustCred')?'':paymentdateFinalStr )+
                    '","check":"' +  checkFinalStr +
                    '","creditamountapplied":'+creditamountapplied+',"creditmemo":"'+searchCInvoice.getValue(columns[8])+'","balanceunpaid":' + balanceUnp +
                    ',"unapplied":' +  parseInt(0) +

                    '},';

                if(i>0)
                {
                    if((lstcolor!='')&&(lstcolor=='#BDBDBD;'))
                    {
                        var colorcode='#FAFAFA;';
                    }
                    else{
                        var colorcode='#BDBDBD;';
                    }
                    var lastcolorcode=colorcode;
                }
                else{
                    var colorcode=lstcolor;
                }
                var datePDFSplit=searchCInvoice.getValue(columns[0]).split('/');
                var dateStringPDF=datePDFSplit[1]+'-'+mS[datePDFSplit[0]]+'-'+datePDFSplit[2];
                var amountstr='$'+(creditamountapplied).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
                //  amountappliedPDF=parseFloat(amountappliedPDF)+(parseFloat(searchCInvoice.getValue(columns[7])));

                seriesCSV += '\n{"Date":"' + ((type =='CustPymt')?'':dateStringPDF )  +
                    '","Due Date":"' +   duedateArrFinalStr +
                    '","Invoice #":"' + ((searchCInvoice.getValue(columns[2]) !='- None -') ? searchCInvoice.getValue(columns[2]) : '') +
                    '","Invoice Amt":"' +  invAmString +
                    '","Amount Paid":"' +  amountpaidString +
                    '","Payment Date":"' + ((type =='CustCred')?'':paymentdateFinalStr)+
                    '","Check #":"' +  checkFinalStr +
                    '","Credit Amount":"'+amountstr+'","Other Trans#":"'+searchCInvoice.getValue(columns[8])+'","Balance Unpaid":"' + balanceString+
                    '"},';

                checkFinalStr=checkFinalStr.replace(/&/g, 'AND');
                statementTableHTML += '<tr style="border-right: 1px;border-right-color: #343a45; background-color:#fafafa;">';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+((type =='CustPymt')?'':dateStringPDF ) +'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+duedateArrFinalStr+'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+((searchCInvoice.getValue(columns[2]) !='- None -') ? searchCInvoice.getValue(columns[2]) : '')+'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+ invAmString+'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+amountpaidString+'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+ ((type =='CustCred')?'':paymentdateFinalStr )+'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+checkFinalStr+'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+amountstr+'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+searchCInvoice.getValue(columns[8])+'</td>';
                statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+balanceString+'</td>';
                statementTableHTML += '</tr>';
                lstcolor=colorcode;
                // }

            }
        }

        var currSign='$';
        if(currency=="Canadian Dollar")
        {
            currSign='CAD$';
        }
        seriesCSV += '\n{"Date":"",' +
            '"Due Date":" ","Invoice #":" ",' +
            '"Invoice Amt":"' +  currSign+(invoiceamtPDF).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') +
            '","Amount Paid":"' +  currSign+(amountPaidPDF).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') +
            '","Payment Date":"",' +
            '"Check #":"",' +
            '"Credit Amount":"'+currSign+(amountappliedPDF).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'",' +
            '"Credit Memo#":"",' +
            '"Balance Unpaid":"' +  currSign+(balancePDF).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') +
            '"},';
        seriesCSV += '];';


        series += '];';
        var currDate=new Date();
        var day = currDate.getDate();
        var month = currDate.getMonth();
        month=parseInt(month)+parseInt(1);
        var year = currDate.getFullYear();
        var dateStr=day+'-'+mS[month]+'-'+year;
        var dateStrCSV=day+'-'+mSCSV[month]+'-'+year;

        if(sendPDFEmail==1)
        {
            if(language=='fr_CA')
            {
                var objFile = nlapiLoadFile(2835479);
            }
            else{
                var objFile = nlapiLoadFile(2835478);
            }
            var stMainHTML = objFile.getValue();

            statementTableHTML += '<tr style="background-color: #B0C4DE;font-weight: bold;">';
            statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">&nbsp;</td>';
            statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">&nbsp;</td>';
            statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">&nbsp;</td>';
            statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">'+ currSign+(invoiceamtPDF).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'</td>';
            statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">'+currSign+(amountPaidPDF).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'</td>';
            statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">&nbsp;</td>';
            statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">&nbsp;</td>';
            statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">'+currSign+(amountappliedPDF).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'</td>';
            statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">&nbsp;</td>';
            statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">'+currSign+(balancePDF).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'</td>';
            statementTableHTML += '</tr>';
            statementTableHTML += '</table>';
            custname=custname.replace(/&/g,'AND');
            stMainHTML = stMainHTML.replace(new RegExp('{accountNumber}','g'),accountNbr);
            stMainHTML = stMainHTML.replace(new RegExp('{customername}','g'),custname);
            stMainHTML = stMainHTML.replace(new RegExp('{statementdate}','g'),dateStr);
            stMainHTML = stMainHTML.replace(new RegExp('StatementTable','g'),statementTableHTML);
            var filePDF = nlapiXMLToPDF(stMainHTML);
            filePDF.setName(custname+ '_' +'Statement Report' + '.pdf');
            var currentContext = nlapiGetContext();
            var  currentUserID = currentContext.getUser();
            nlapiSendEmail(currentUserID,currentUserID,custname+ '_' +'Statement Report',custname+ '_' +'Statement Report',null,null,null,filePDF,true);


        }


    }
    else{
        series += '\n{"date":"","duedate":" ","invoice":"","invoiceid":" ","invoiceam":"","amountpaid":"","paymentdate":"","check":"","creditamountapplied":"","creditmemo":"","balanceunpaid":"","unapplied":""}];';
        seriesCSV += '\n{"date":"","duedate":" ","invoice":"","invoiceid":" ","invoiceam":"","amountpaid":"","paymentdate":"","check":"","creditamountapplied":"","creditmemo":"","balanceunpaid":"","unapplied":""}];';
        var currSign='';
        var display='none';
        var dateStr='';
        var accountNbr='';
    }
    var typeref='';
    if(searchRefunds!=null)
    {
        typeref='This Customer has a refund';
    }

    if(inArray(customerid,listofCustomersWJournals))
    {
        typeref='Automated Statement is unavailable due to unreconciled Journal on account -- must review account and update statement manually';
    }
    var returnArray=[series,currSign,seriesCSV,display,dateStr,accountNbr,typeref];
    return returnArray;

}
function getInvoicesJSONStatementForm(customerid,sendPDFEmail,custname,from,to,language){
    var series='[';
    var seriesCSV='[';
    var lstcolor='#BDBDBD;';
    var listofCustomersWJournals=[217913,1649,217937,347096,347127,1879,1887,2044];
    var arrColumns = new Array();
    var arrFilters = new Array();
    var recCustomer = nlapiLoadRecord('customer', customerid);
    var accountNbr = recCustomer.getFieldValue('accountnumber');
    arrFilters.push(new nlobjSearchFilter("trandate",null,"within",from,to));
    arrFilters.push(new nlobjSearchFilter("name",null,"anyof",customerid));
    var searchRefunds= nlapiSearchRecord('transaction','customsearch2422', arrFilters, arrColumns);
    var display='none';
    if((searchRefunds!=null)||(inArray(customerid,listofCustomersWJournals)))
    {
        var display='block';
    }
   //var searchCInvoices = nlapiSearchRecord('transaction','customsearch2618', arrFilters, arrColumns);
    var searchCInvoices = nlapiSearchRecord('transaction','customsearch7666', arrFilters, arrColumns);
    var statementTableHTML='';
    var invoiceamtPDF=0;
    var amountPaidPDF=0;
    var amountappliedPDF=0;
    var balancePDF=0;
    var invoicesArray=new Array();
    var mS = ['','JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
    var mSCSV = ['','Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    if(language=='fr_CA')
    {


        statementTableHTML =  '<table cellpadding="1" class="profile" align="center" table-layout="fixed">';
        statementTableHTML += '<thead>';
        statementTableHTML += '<tr style="border-right: 1px;border-right-color: #343a45; background-color:#fafafa;  ">';
        statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Date</th>';
        statementTableHTML +="<th style='border-right: 1px;border-right-color:#9ea7af; width:10px;text-align: left;'>Date d'échéance</th>";
        statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">#Facture</th>';
        statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Montant de la facture</th>';
        statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Montant payé</th>';
        statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Date du paiement</th>';
        statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">#Chèque</th>';
        statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:15px; text-align: left;">Crédit montant appliquées</th>';
        statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">#Note de Crédit</th>';
        statementTableHTML +='<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Sold Impayé</th>';
        statementTableHTML += '</tr>';
        statementTableHTML += '</thead>';
    }
    else{
        statementTableHTML =  '<table cellpadding="1" class="profile" align="center" table-layout="fixed">';
        statementTableHTML += '<thead>';
        statementTableHTML += '<tr style="border-right: 1px;border-right-color: #343a45; background-color:#fafafa;  ">';
        statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Date</th>';
        statementTableHTML +=     '<th style="border-right: 1px;border-right-color:#9ea7af; width:10px;text-align: left;">Due Date</th>';
        statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Invoice #</th>';
        statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Invoice Amt</th>';
        statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Amount Paid</th>';
        statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Payment Date</th>';
        statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Check #</th>';
        statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:15px; text-align: left;">Credit Amount</th>';
        statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Other Trans #</th>';
        statementTableHTML +=     '<th style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">Balance Unpaid</th>';
        statementTableHTML += '</tr>';
        statementTableHTML += '</thead>';
    }

    //custbody_consolidate_inv_nbr
    for ( var i = 0; searchCInvoices != null && i < searchCInvoices.length; i++ ) {

        var searchCInvoice = searchCInvoices[i];
        var columns = searchCInvoice.getAllColumns();
        var creditamountapplied=0;
        var amountpaid=0;
        var balanceunpaid=0;
        var creditmemo="";
        var credittrue=0;
        var typeofPayment=searchCInvoice.getValue(columns[13]);
        var currency=searchCInvoice.getText(columns[11]);
        var amountAss=searchCInvoice.getValue(columns[14]);
        var amountPaidAss=searchCInvoice.getValue(columns[15]);
        var amountRemAss=searchCInvoice.getValue(columns[16]);
        var type=searchCInvoice.getValue(columns[12]);
        var foundCreditMemo=-1;
        if((typeofPayment!=null)&&(typeofPayment!=''))
        {
            //   nlapiSendEmail(206211,206211,'typeofPayment',typeofPayment+', '+searchCInvoice.getValue(columns[0]),null,null,null,null);
            var typeofPaymentArr=typeofPayment.split(',');
            var typeofPaymentArr=_.uniq(typeofPaymentArr);

            foundCreditMemo=_.indexOf(typeofPaymentArr, 'Credit Memo');
        }
        if(foundCreditMemo!=-1)
        {
            //  nlapiSendEmail(206211,206211,'foundCreditMemo',foundCreditMemo+', '+searchCInvoice.getValue(columns[0]),null,null,null,null);
            //custbody_consolidate_inv_nbr
            var arrColumns1 = new Array();
            var arrFilters1 = new Array();
            arrFilters1.push(new nlobjSearchFilter("name",null,"anyof",customerid));
            arrFilters1.push(new nlobjSearchFilter("custbody_consolidate_inv_nbr",null,"is",searchCInvoice.getValue(columns[2])));
            var searchCInvoices1 = nlapiSearchRecord('transaction','customsearch2424', arrFilters1, arrColumns1);
            if(searchCInvoices1!=null)
            {
                for ( var j = 0; searchCInvoices1 != null && j < searchCInvoices1.length; j++ ) {
                    var searchCInvoice1 = searchCInvoices1[j];
                    var columns1 = searchCInvoice1.getAllColumns();
                    creditamountapplied=parseFloat(creditamountapplied)+parseFloat(searchCInvoice1.getValue(columns1[0]));
                    var creditmemoStr=searchCInvoice1.getValue(columns1[1]);
                    var creditMemoArr=creditmemoStr.split(',');
                    creditMemoArr=_.uniq(creditMemoArr);
                    for(var k=0;k<creditMemoArr.length;k++)
                    {
                        if(k < creditMemoArr.length-1)
                        {
                            creditmemo=creditmemo+creditMemoArr[k]+", ";
                        }
                        else
                        {
                            creditmemo=creditmemo+creditMemoArr[k];
                        }
                    }


                    credittrue=1;
                    // }
                }
            }
            var valArr=amountAss.split(',');
            var counts = [];
            var duplicates=[];
            var duplicateValue=0;
            for(var p = 0; p < valArr.length; p++) {
                if(counts[valArr[p]] === undefined) {
                    counts[valArr[p]] = 1;
                } else {
                    duplicates.push(valArr[p]);
                }
            }
            if(duplicates.length>0){
                for(var r=0;r<duplicates.length;r++)
                {
                    var splitDuplicates=duplicates[r].split('=');
                    duplicateValue=parseFloat(duplicateValue)+parseFloat(splitDuplicates[1]);
                }

            }
            //duplicates Paid
            var valArrPaid=amountPaidAss.split(',');
            var countsPaid = [];
            var duplicatesPaid=[];
            var duplicateValuePaid=0;
            for(var p = 0; p < valArrPaid.length; p++) {
                if(countsPaid[valArrPaid[p]] === undefined) {
                    countsPaid[valArrPaid[p]] = 1;
                } else {
                    duplicatesPaid.push(valArrPaid[p]);
                }
            }
            if(duplicatesPaid.length>0){
                for(var r=0;r<duplicatesPaid.length;r++)
                {
                    var splitDuplicatesPaid=duplicatesPaid[r].split('=');
                    duplicateValuePaid=parseFloat(duplicateValuePaid)+parseFloat(splitDuplicatesPaid[1]);
                }

            }
            //duplicates Remaining
            var valArrRem=amountRemAss.split(',');
            var countsRem = [];
            var duplicatesRem=[];
            var duplicateValueRem=0;
            for(var p = 0; p < valArrRem.length; p++) {
                if(countsRem[valArrRem[p]] === undefined) {
                    countsRem[valArrRem[p]] = 1;
                } else {
                    duplicatesRem.push(valArrRem[p]);
                }
            }
            if(duplicatesRem.length>0){
                for(var r=0;r<duplicatesRem.length;r++)
                {
                    var splitDuplicatesRem=duplicatesRem[r].split('=');
                    duplicateValueRem=parseFloat(duplicateValueRem)+parseFloat(splitDuplicatesRem[1]);
                }

            }




            var duedateArrFinalStr='';
            var duedate=searchCInvoice.getValue(columns[1]);
            if(duedate!=''){
                var duedateArr=duedate.split(',');

                duedateArr=_.uniq(duedateArr);
                for(var k=0;k<duedateArr.length;k++)
                {
                    if(duedateArr[k]!=null){
                        //      nlapiSendEmail(206211,206211,'splitduek',duedateArr[k],null,null,null,null);
                        var splitdue=duedateArr[k].split('/');

                        if(k < duedateArr.length-1)
                        {

                            duedateArrFinalStr=duedateArrFinalStr+splitdue[1]+'-'+mS[splitdue[0]]+'-'+splitdue[2]+", ";

                        }
                        else
                        {
                            duedateArrFinalStr=duedateArrFinalStr+splitdue[1]+'-'+mS[splitdue[0]]+'-'+splitdue[2];
                        }
                    }


                }
            }
            var paymentdate=searchCInvoice.getValue(columns[5]);
            var paymentdateArr=paymentdate.split(',');
            paymentdateArr=_.uniq(paymentdateArr);
            var paymentdateFinalStr='';
            for(var k=0;k<paymentdateArr.length;k++)
            {
                if((paymentdateArr[k]!=null)&&(paymentdateArr[k]!='')){
                    var splitpay=paymentdateArr[k].split('/');
                    if(k < paymentdateArr.length-1)
                    {
                        paymentdateFinalStr=paymentdateFinalStr+splitpay[1]+'-'+mS[splitpay[0]]+'-'+splitpay[2]+", ";
                    }
                    else
                    {
                        paymentdateFinalStr=paymentdateFinalStr+splitpay[1]+'-'+mS[splitpay[0]]+'-'+splitpay[2];
                    }
                }
            }
            var check=searchCInvoice.getValue(columns[6]);
            var checkArr=check.split(',');
            var checkFinalStr='';
            checkArr=_.uniq(checkArr);
            checkArr= _.difference(checkArr, creditMemoArr);
            for(var k=0;k<checkArr.length;k++)
            {
                if(k < checkArr.length-1)
                {
                    checkFinalStr=checkFinalStr+checkArr[k]+", ";
                }
                else
                {
                    checkFinalStr=checkFinalStr+checkArr[k];
                }
            }
            if(i>0)
            {
                if((lstcolor!='')&&(lstcolor=='#BDBDBD;'))
                {
                    var colorcode='#FAFAFA;';
                }
                else{
                    var colorcode='#BDBDBD;';
                }
                var lastcolorcode=colorcode;
            }
            else{
                var colorcode=lstcolor;
            }
            // _.difference([1, 2, 3, 4, 5], [5, 2, 10]);

            var amntForString=(parseFloat(searchCInvoice.getValue(columns[3]))-parseFloat(duplicateValue)).toFixed(2);
            var invAmString=(amntForString < 0) ? '($'+(amntForString)*parseFloat(-1).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+')' : '$'+parseFloat(amntForString).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
            var balanceUnp= parseFloat(searchCInvoice.getValue(columns[9]))-parseFloat(duplicateValueRem);
            var amountPaidNoCRedit=0;
            if(creditamountapplied!=parseFloat(searchCInvoice.getValue(columns[4])))
            {
                var amountPaidNoCRedit=parseFloat(amntForString)-parseFloat(creditamountapplied)-parseFloat(balanceUnp);
            }

            //  nlapiSendEmail(206211,206211,'string11tyy',parseFloat(searchCInvoice.getValue(columns[8])).toFixed(2),null,null,null,null);
            invoicesArray.push(searchCInvoice.getValue(columns[2]));
            series += '\n{"date":"' + ((type =='CustPymt')?'':searchCInvoice.getValue(columns[0]) )  +
                '","duedate":"' +   duedateArrFinalStr +
                '","invoice":"' + ((searchCInvoice.getValue(columns[2]) !='- None -') ? searchCInvoice.getValue(columns[2]) : '') +
                '","invoiceid":"'+searchCInvoice.getValue(columns[2])+' ",' +
                '"invoiceam":' +   amntForString  +
                ',"amountpaid":' +  ((type =='CustPymt') ? parseFloat(searchCInvoice.getValue(columns[4]))*parseFloat(-1).toFixed(2) : amountPaidNoCRedit) +
                ',"paymentdate":"' +  paymentdateFinalStr +
                '","check":"' +  checkFinalStr +
                '","creditamountapplied":'+creditamountapplied +',"creditmemo":"'+creditmemo+'","balanceunpaid":' + balanceUnp+
                ',"unapplied":' +  parseInt(0) +

                '},';
            var amntForString1=((type =='CustPymt') ? parseFloat(searchCInvoice.getValue(columns[4]))*parseFloat(-1).toFixed(2) : amountPaidNoCRedit);
            var amountpaidString=(amntForString1 < 0) ? '($'+parseFloat(amntForString1)*parseFloat(-1).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+')' : '$'+parseFloat(amntForString1).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
            var balanceString=( type =='CustPymt') ? '($'+( parseFloat(balanceUnp)).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+')' : '$'+ parseFloat(balanceUnp).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');


            invoiceamtPDF=parseFloat(invoiceamtPDF)+parseFloat(amntForString);
            amountPaidPDF=parseFloat(amountPaidPDF)+parseFloat(amountPaidNoCRedit);
            amountappliedPDF=parseFloat(amountappliedPDF)+parseFloat(creditamountapplied);
            var datePDFSplit=searchCInvoice.getValue(columns[0]).split('/');
            var dateStringPDF=datePDFSplit[1]+'-'+mS[datePDFSplit[0]]+'-'+datePDFSplit[2];
            seriesCSV += '\n{"Date":"' + ((type =='CustPymt')?'':dateStringPDF )    +
                '","Due Date":"' +   duedateArrFinalStr +
                '","Invoice #":"' + ((searchCInvoice.getValue(columns[2]) !='- None -') ? searchCInvoice.getValue(columns[2]) : '') +
                '","Invoice Amt":"' +  invAmString +
                '","Amount Paid":"' +  amountpaidString +
                '","Payment Date":"' +  paymentdateFinalStr +
                '","Check #":"' +  checkFinalStr +
                '","Credit Amount":"'+'$'+parseFloat(creditamountapplied).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'","Credit Memo#":"'+creditmemo+'","Balance Unpaid":"' + balanceString+
                '"},';
            var amountBalance=( type =='CustPymt') ? parseFloat(searchCInvoice.getValue(columns[9]))*parseFloat(-1).toFixed(2):parseFloat(searchCInvoice.getValue(columns[9])).toFixed(2);
            balancePDF=parseFloat(balancePDF)+parseFloat(balanceUnp);
            checkFinalStr=checkFinalStr.replace(/&/g, 'AND');
            statementTableHTML += '<tr style="border-right: 1px;border-right-color: #343a45; background-color:#fafafa;">';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+((type =='CustPymt')?'':dateStringPDF ) +'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+duedateArrFinalStr+'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+((searchCInvoice.getValue(columns[2]) !='- None -') ? searchCInvoice.getValue(columns[2]) : '')+'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+ invAmString+'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+amountpaidString+'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+ paymentdateFinalStr +'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+checkFinalStr+'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">$'+parseFloat(creditamountapplied).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+creditmemo+'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+balanceString+'</td>';
            statementTableHTML += '</tr>';
            lstcolor=colorcode;

        }


        else{
            //   nlapiSendEmail(206211,206211,'Credit Memo Emp','',null,null,null,null);
            var duedateArrFinalStr='';
            var duedate=searchCInvoice.getValue(columns[1]);
            var duedateArr=duedate.split(',');
            if(duedate!='')
            {
                duedateArr=_.uniq(duedateArr);
                for(var k=0;k<duedateArr.length;k++)
                {
                    if(duedateArr[k]!=null)
                    {
                        var splitdue=duedateArr[k].split('/');
                        if(k < duedateArr.length-1)
                        {

                            duedateArrFinalStr=duedateArrFinalStr+splitdue[1]+'-'+mS[splitdue[0]]+'-'+splitdue[2]+", ";
                        }
                        else
                        {
                            duedateArrFinalStr=duedateArrFinalStr+splitdue[1]+'-'+mS[splitdue[0]]+'-'+splitdue[2];
                        }
                    }
                }
            }
            //  nlapiSendEmail(206211,206211,'duedateArrFinalStr',duedateArrFinalStr,null,null,null,null);
            var paymentdate=searchCInvoice.getValue(columns[5]);
            var paymentdateArr=paymentdate.split(',');
            var paymentdateFinalStr='';
            paymentdateArr=_.uniq(paymentdateArr);
            for(var k=0;k<paymentdateArr.length;k++)
            {
                if((paymentdateArr[k]!=null)&&(paymentdateArr[k]!=''))
                {
                    var splitpay=paymentdateArr[k].split('/');
                    if(k < paymentdateArr.length-1)
                    {
                        paymentdateFinalStr=paymentdateFinalStr+splitpay[1]+'-'+mS[splitpay[0]]+'-'+splitpay[2]+", ";
                    }
                    else
                    {
                        paymentdateFinalStr=paymentdateFinalStr+splitpay[1]+'-'+mS[splitpay[0]]+'-'+splitpay[2];
                    }
                }
            }
            var check=searchCInvoice.getValue(columns[6]);
            var checkArr=check.split(',');
            var checkFinalStr='';
            checkArr=_.uniq(checkArr);
            for(var k=0;k<checkArr.length;k++)
            {
                if(k < checkArr.length-1)
                {
                    checkFinalStr=checkFinalStr+checkArr[k]+", ";
                }
                else
                {
                    checkFinalStr=checkFinalStr+checkArr[k];
                }
            }
            checkFinalStr=checkFinalStr.replace(/&/g, 'AND');
            //  if(searchCInvoice.getValue(columns[7])!=0)
            // {
            var valArr=amountAss.split(',');
            var counts = [];
            var duplicates=[];
            var duplicateValue=0;
            for(var p = 0; p < valArr.length; p++) {
                if(counts[valArr[p]] === undefined) {
                    counts[valArr[p]] = 1;
                } else {
                    duplicates.push(valArr[p]);
                }
            }
            if(duplicates.length>0){
                for(var r=0;r<duplicates.length;r++)
                {
                    var splitDuplicates=duplicates[r].split('=');
                    duplicateValue=parseFloat(duplicateValue)+parseFloat(splitDuplicates[1]);
                }

            }
            //duplicates Paid
            var valArrPaid=amountPaidAss.split(',');
            var countsPaid = [];
            var duplicatesPaid=[];
            var duplicateValuePaid=0;
            for(var p = 0; p < valArrPaid.length; p++) {
                if(countsPaid[valArrPaid[p]] === undefined) {
                    countsPaid[valArrPaid[p]] = 1;
                } else {
                    duplicatesPaid.push(valArrPaid[p]);
                }
            }
            if(duplicatesPaid.length>0){
                for(var r=0;r<duplicatesPaid.length;r++)
                {
                    var splitDuplicatesPaid=duplicatesPaid[r].split('=');
                    duplicateValuePaid=parseFloat(duplicateValuePaid)+parseFloat(splitDuplicatesPaid[1]);
                }

            }
            //duplicates Remaining
            var valArrRem=amountRemAss.split(',');
            var countsRem = [];
            var duplicatesRem=[];
            var duplicateValueRem=0;
            for(var p = 0; p < valArrRem.length; p++) {
                if(countsRem[valArrRem[p]] === undefined) {
                    countsRem[valArrRem[p]] = 1;
                } else {
                    duplicatesRem.push(valArrRem[p]);
                }
            }
            if(duplicatesRem.length>0){
                for(var r=0;r<duplicatesRem.length;r++)
                {
                    var splitDuplicatesRem=duplicatesRem[r].split('=');
                    if((r>0)&&(type!='CustPymt'))
                    {
                        // if((parseFloat(splitDuplicatesRem[1]))!= (parseFloat(duplicateValueRem)))
                        //{
                        duplicateValueRem=parseFloat(duplicateValueRem)+parseFloat(splitDuplicatesRem[1]);
                        //}
                    }
                    else{
                        duplicateValueRem=parseFloat(duplicateValueRem)+parseFloat(splitDuplicatesRem[1]);
                    }

                }

            }


            var duplicateValueRem1=0;
            var uniq=_.uniq(valArrRem);
            if(uniq.length>0){
                for(var r=0;r<uniq.length;r++)
                {
                    var splitDuplicatesRem=uniq[r].split('=');
                    if((r>0)&&(type!='CustPymt'))
                    {
                        // if((parseFloat(splitDuplicatesRem[1]))!= (parseFloat(duplicateValueRem)))
                        //{
                        duplicateValueRem1=parseFloat(duplicateValueRem1)+parseFloat(splitDuplicatesRem[1]);
                        //}
                    }
                    else{
                        duplicateValueRem1=parseFloat(duplicateValueRem1)+parseFloat(splitDuplicatesRem[1]);
                    }

                }

            }


            //  invoicesArray.push(searchCInvoice.getValue(columns[0]));
            var balanceUnp=( type =='CustPymt') ? ( parseFloat(searchCInvoice.getValue(columns[9]))-parseFloat(duplicateValueRem))*parseFloat(-1) : ( parseFloat(searchCInvoice.getValue(columns[9]))-parseFloat(duplicateValueRem));

            var creditamountapplied=0;
            if(searchCInvoice.getValue(columns[17])=='YES')
            {

                // var amountPaidInvFinal=Math.abs(parseFloat(duplicateValueRem));
                //var balanceUnp=amountPaidInvFinal*(-1);
                var amountPaidInvFinal=Math.abs(parseFloat(balanceUnp));
                amountpaidString='$'+ Math.abs(parseFloat(duplicateValueRem)).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
            }
            else
            {

                var amountPaidInvFinal=((type =='CustPymt') ? (parseFloat(searchCInvoice.getValue(columns[3]))-parseFloat(duplicateValue))*parseFloat(-1).toFixed(2) : (parseFloat(searchCInvoice.getValue(columns[4]))-parseFloat(duplicateValuePaid)));

                if(type =='CustCred')
                {
                    var amountPaidInvFinal=parseFloat(0);
                    if(duplicateValueRem>0)
                    {
                        var creditamountapplied=Math.abs(parseFloat(duplicateValueRem1));
                    }
                    else{
                        var creditamountapplied=Math.abs(parseFloat(searchCInvoice.getValue(columns[7])));

                    }
                    var balanceUnp=creditamountapplied*(-1);
                }
            }
            amountPaidPDF=parseFloat(amountPaidPDF)+parseFloat(amountPaidInvFinal);
            //PDF & CSV END
            var amntForString=((type =='CustInvc') ? (parseFloat(searchCInvoice.getValue(columns[3]))-parseFloat(duplicateValue)).toFixed(2) : 0  );
            var invAmString=(amntForString < 0) ? '($'+(amntForString)*parseFloat(-1).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+')' : '$'+parseFloat(amntForString).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

            var amntForString1=amountPaidInvFinal;
            var amountpaidString=(amntForString1 < 0) ? '($'+(amntForString1).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+')' : '$'+parseFloat(amntForString1).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

            var balanceString=(balanceUnp<0) ? '($'+(parseFloat((balanceUnp))*parseFloat(-1)).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+')' : '$'+(balanceUnp).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

            invoiceamtPDF=parseFloat(invoiceamtPDF)+parseFloat(amntForString);
            amountappliedPDF=parseFloat(amountappliedPDF)+parseFloat(creditamountapplied);
            var amountBalance=parseFloat(balanceUnp);

            balancePDF=parseFloat(balancePDF)+parseFloat(balanceUnp);
            //PDF & CSV END
            series += '\n{"date":"' + ((type =='CustPymt')?'':searchCInvoice.getValue(columns[0]) ) +
                '","duedate":"' +   duedateArrFinalStr +
                '","invoice":"' + ((searchCInvoice.getValue(columns[2]) !='- None -') ? searchCInvoice.getValue(columns[2]) : '') +
                '","invoiceid":"'+searchCInvoice.getValue(columns[2])+' ",' +
                '"invoiceam":' +   ((type =='CustInvc') ? (parseFloat(searchCInvoice.getValue(columns[3]))-parseFloat(duplicateValue)).toFixed(2) : 0  )+
                ',"amountpaid":' +  amountPaidInvFinal +
                ',"paymentdate":"' +  ((type =='CustCred')?'':paymentdateFinalStr )+
                '","check":"' +  checkFinalStr +
                '","creditamountapplied":'+creditamountapplied+',"creditmemo":"'+searchCInvoice.getValue(columns[8])+'","balanceunpaid":' + balanceUnp +
                ',"unapplied":' +  parseInt(0) +

                '},';

            if(i>0)
            {
                if((lstcolor!='')&&(lstcolor=='#BDBDBD;'))
                {
                    var colorcode='#FAFAFA;';
                }
                else{
                    var colorcode='#BDBDBD;';
                }
                var lastcolorcode=colorcode;
            }
            else{
                var colorcode=lstcolor;
            }
            var datePDFSplit=searchCInvoice.getValue(columns[0]).split('/');
            var dateStringPDF=datePDFSplit[1]+'-'+mS[datePDFSplit[0]]+'-'+datePDFSplit[2];
            var amountstr='$'+(creditamountapplied).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
            //  amountappliedPDF=parseFloat(amountappliedPDF)+(parseFloat(searchCInvoice.getValue(columns[7])));

            seriesCSV += '\n{"Date":"' + ((type =='CustPymt')?'':dateStringPDF )  +
                '","Due Date":"' +   duedateArrFinalStr +
                '","Invoice #":"' + ((searchCInvoice.getValue(columns[2]) !='- None -') ? searchCInvoice.getValue(columns[2]) : '') +
                '","Invoice Amt":"' +  invAmString +
                '","Amount Paid":"' +  amountpaidString +
                '","Payment Date":"' + ((type =='CustCred')?'':paymentdateFinalStr)+
                '","Check #":"' +  checkFinalStr +
                '","Credit Amount":"'+amountstr+'","Credit Memo#":"'+searchCInvoice.getValue(columns[8])+'","Balance Unpaid":"' + balanceString+
                '"},';


            statementTableHTML += '<tr style="border-right: 1px;border-right-color: #343a45; background-color:#fafafa;">';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+((type =='CustPymt')?'':dateStringPDF ) +'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+duedateArrFinalStr+'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+((searchCInvoice.getValue(columns[2]) !='- None -') ? searchCInvoice.getValue(columns[2]) : '')+'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+ invAmString+'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+amountpaidString+'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+ ((type =='CustCred')?'':paymentdateFinalStr )+'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+checkFinalStr+'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+amountstr+'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+searchCInvoice.getValue(columns[8])+'</td>';
            statementTableHTML +=     '<td style="border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;background-color:'+colorcode+'">'+balanceString+'</td>';
            statementTableHTML += '</tr>';
            lstcolor=colorcode;
            // }

        }
    }

    var currSign='$';
    if(currency=="Canadian Dollar")
    {
        currSign='CAD$';
    }
    seriesCSV += '\n{"Date":"",' +
        '"Due Date":" ","Invoice #":" ",' +
        '"Invoice Amt":"' +  currSign+(invoiceamtPDF).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') +
        '","Amount Paid":"' +  currSign+(amountPaidPDF).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') +
        '","Payment Date":"",' +
        '"Check #":"",' +
        '"Credit Amount":"'+currSign+(amountappliedPDF).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'",' +
        '"Credit Memo#":"",' +
        '"Balance Unpaid":"' +  currSign+(balancePDF).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') +
        '"},';
    seriesCSV += '];';


    series += '];';
    var currDate=new Date();
    var day = currDate.getDate();
    var month = currDate.getMonth();
    month=parseInt(month)+parseInt(1);
    var year = currDate.getFullYear();
    var dateStr=day+'-'+mS[month]+'-'+year;
    var dateStrCSV=day+'-'+mSCSV[month]+'-'+year;

    if(sendPDFEmail==1)
    {
        if(language=='fr_CA')
        {
            var objFile = nlapiLoadFile(2835479);
        }
        else{
            var objFile = nlapiLoadFile(2835478);
        }
        var stMainHTML = objFile.getValue();

        statementTableHTML += '<tr style="background-color: #B0C4DE;font-weight: bold;">';
        statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">&nbsp;</td>';
        statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">&nbsp;</td>';
        statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">&nbsp;</td>';
        statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">'+ currSign+(invoiceamtPDF).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'</td>';
        statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">'+currSign+(amountPaidPDF).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'</td>';
        statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">&nbsp;</td>';
        statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">&nbsp;</td>';
        statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">'+currSign+(amountappliedPDF).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'</td>';
        statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">&nbsp;</td>';
        statementTableHTML +=     '<td style="background-color: #B0C4DE;border-right: 1px;border-right-color: #9ea7af; width:10px;text-align: left;">'+currSign+(balancePDF).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')+'</td>';
        statementTableHTML += '</tr>';
        statementTableHTML += '</table>';
        custname=custname.replace('&','AND');
        stMainHTML = stMainHTML.replace(new RegExp('{accountNumber}','g'),accountNbr);
        stMainHTML = stMainHTML.replace(new RegExp('{customername}','g'),custname);
        stMainHTML = stMainHTML.replace(new RegExp('{statementdate}','g'),dateStr);
        stMainHTML = stMainHTML.replace(new RegExp('StatementTable','g'),statementTableHTML);
        var filePDF = nlapiXMLToPDF(stMainHTML);
        filePDF.setName(custname+ '_' +'Statement Report' + '.pdf');
        var currentContext = nlapiGetContext();
        var  currentUserID = currentContext.getUser();
        nlapiSendEmail(currentUserID,currentUserID,custname+ '_' +'Statement Report',custname+ '_' +'Statement Report',null,null,null,filePDF,true);


    }
    var typeref='';
    if(searchRefunds!=null)
    {
        typeref='This Customer has a refund';
    }

    if(inArray(customerid,listofCustomersWJournals))
    {
        typeref='Automated Statement is unavailable due to unreconciled Journal on account -- must review account and update statement manually';
    }
    var returnArray=[series,currSign,seriesCSV,display,dateStr,accountNbr,typeref];
    return returnArray;


}
function getInvoicesJSON(customerid, yearid, monthid){

    var objTree = new Object();
    objTree["text"] = '.';

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_batch',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_date',null,null));
    arrColumns.push(new nlobjSearchColumn('created',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_year',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_month',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_pdf_file_id',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_currency',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_market',null,null));
    //arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_subtotal',null,null));
    //arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_tax_total',null,null));
    //arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_tax2_total',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_total',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_invoices',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_emailed_to',null,null));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_year_display",null,"is",yearid));
    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_month_display",null,"anyof",monthid));
    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_customer",null,"anyof",customerid));
    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_processed",null,"is",'T'));
    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_output",null,"anyof",1));
    var searchCInvoices = nlapiSearchRecord('customrecord_clgx_consolidated_invoices', null, arrFilters, arrColumns);

    var arrCInvoices = new Array();
    for ( var i = 0; searchCInvoices != null && i < searchCInvoices.length; i++ ) {
        var searchCInvoice = searchCInvoices[i];

        var fileid = parseInt(searchCInvoice.getValue('custrecord_clgx_consol_inv_pdf_file_id', null, null));

        try {
            var objFile = nlapiLoadFile(fileid);
            var filename = objFile.getName();
            var fileurl = objFile.getURL();
        }
        catch (e) {
            var str = String(e);
            if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
                var fileid = 0;
                var filename = 'unknown';
                var fileurl = '';
            }
        }

        var arrListInvoices = searchCInvoice.getValue('custrecord_clgx_consol_inv_invoices', null, null).split( ";" );

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('fxamount','applyingtransaction','GROUP').setSort(false));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrListInvoices));
        arrFilters.push(new nlobjSearchFilter("mainline",null,"is",'T'));
        var searchTotalPaid = nlapiSearchRecord('transaction', null, arrFilters, arrColumns);
        var totalPaid = 0;
        if(searchTotalPaid != null){
            totalPaid = Math.abs(searchTotalPaid[0].getValue('fxamount', 'applyingtransaction','GROUP')).toFixed(2);
        }

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
        arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
        arrColumns.push(new nlobjSearchColumn('fxamount',null,'SUM'));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrListInvoices));
        arrFilters.push(new nlobjSearchFilter("mainline",null,"is",'T'));
        var searchTotalInvoices = nlapiSearchRecord('invoice', null, arrFilters, arrColumns);

        var objCInvoice = new Object();
        objCInvoice["nodeid"] = parseInt(fileid);
        objCInvoice["node"] = searchCInvoice.getValue('custrecord_clgx_consol_inv_batch', null, null);
        objCInvoice["date"] = searchCInvoice.getValue('created', null, null);
        objCInvoice["cyear"] = searchCInvoice.getValue('custrecord_clgx_consol_inv_year', null, null);
        objCInvoice["cmonth"] = searchCInvoice.getValue('custrecord_clgx_consol_inv_month', null, null);
        objCInvoice["fileid"] = parseInt(fileid);
        objCInvoice["file"] = filename;
        objCInvoice["fileurl"] = fileurl;
        objCInvoice["currency"] = searchCInvoice.getValue('custrecord_clgx_consol_inv_currency', null, null);
        objCInvoice["template"] = searchCInvoice.getValue('custrecord_clgx_consol_inv_market', null, null);
        //objCInvoice["subtotal"] = '$' + searchInvoice.getValue('custrecord_clgx_consol_inv_subtotal', null, null);
        //objCInvoice["tax1"] = '$' + searchInvoice.getValue('custrecord_clgx_consol_inv_tax_total', null, null);
        //objCInvoice["tax2"] = '$' + searchInvoice.getText('custrecord_clgx_consol_inv_tax2_total', null, null);
        objCInvoice["total"] = '$' + addCommas(searchCInvoice.getValue('custrecord_clgx_consol_inv_total', null, null));
        objCInvoice["paid"] = '$' + addCommas(totalPaid);
        objCInvoice["expanded"] = true;
        objCInvoice["iconCls"] = 'cinvoice';
        objCInvoice["leaf"] = false;
        var arrInvoiceChildren = new Array();
        var objInvoiceChild = new Object();
        objInvoiceChild["nodeid"] = 2;
        objInvoiceChild["node"] = 'Included Invoices';
        objInvoiceChild["expanded"] = true;
        objInvoiceChild["iconCls"] = 'invoices';
        objInvoiceChild["leaf"] = false;
        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
        arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
        arrColumns.push(new nlobjSearchColumn('status',null,'GROUP'));
        arrColumns.push(new nlobjSearchColumn('fxamount',null,'SUM'));
        arrColumns.push(new nlobjSearchColumn('fxamount','applyingtransaction','SUM'));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrListInvoices));
        arrFilters.push(new nlobjSearchFilter("mainline",null,"is",'T'));
        var searchInvoices = nlapiSearchRecord('invoice', null, arrFilters, arrColumns);

        var arrInvoices = new Array();
        for ( var j = 0; searchInvoices != null  && j < searchInvoices.length; j++ ) {

            var searchInvoice = searchInvoices[j];
            var invoiceid = searchInvoice.getValue('internalid', null, 'GROUP');
            var invoiceNbr = searchInvoice.getValue('tranid', null, 'GROUP');
            var status = searchInvoice.getText('status', null, 'GROUP');
            var total = searchInvoice.getValue('fxamount', null, 'SUM');
            var paid = searchInvoice.getValue('fxamount', 'applyingtransaction', 'SUM');

            var objInvoice = new Object();
            objInvoice["nodeid"] = parseInt(invoiceid);
            objInvoice["node"] = invoiceNbr;
            objInvoice["total"] = '$' + addCommas(total);
            objInvoice["status"] = status;
            //objInvoice["paid"] = null;
            objInvoice["iconCls"] = 'CustInvc';
            objInvoice["leaf"] = true;
            arrInvoices.push(objInvoice);
        }
        objInvoiceChild["children"] = arrInvoices;
        arrInvoiceChildren.push(objInvoiceChild);

        var objInvoiceChild = new Object();
        objInvoiceChild["nodeid"] = 1;
        objInvoiceChild["node"] = 'Emailed To';
        objInvoiceChild["expanded"] = true;
        objInvoiceChild["iconCls"] = 'contacts';
        objInvoiceChild["leaf"] = false;


        var arrListContacts = new Array();
        var listContacts = searchCInvoice.getValue('custrecord_clgx_consol_inv_emailed_to', null, null);
        if(listContacts != null){
            arrListContacts = listContacts.split( ";" );
        }

        var arrContacts = new Array();
        for ( var m = 0; arrListContacts != null  && m < arrListContacts.length; m++ ) {

            if(parseInt(arrListContacts[m]) > 0){
                var contactid = arrListContacts[m];
                var contactName = nlapiLookupField('contact', arrListContacts[m], 'entityid');
            }
            else{
                var contactid = 0;
                var contactName = 'unknown';
            }
            var objContact = new Object();
            objContact["nodeid"] = contactid;
            objContact["node"] = contactName;
            objContact["iconCls"] = 'contact';
            objContact["leaf"] = true;
            arrContacts.push(objContact);
        }
        objInvoiceChild["children"] = arrContacts;
        arrInvoiceChildren.push(objInvoiceChild);

        objCInvoice["children"] = arrInvoiceChildren;
        arrCInvoices.push(objCInvoice);
    }
    objTree["children"] = arrCInvoices;
    return JSON.stringify(objTree);
}
//check if value is in the array
function inArray(val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}
function addCommas(nStr){
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}