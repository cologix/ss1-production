nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CI_Customers.js
//	Script Name:	CLGX_SL_CI_Customers
//	Script Id:		customscript_clgx_sl_ci_customers
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/14/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=264&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_ci_customers (request, response){
	try {
		var objFile = nlapiLoadFile(1188536);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{customersJSON}','g'), getCustomersGridJSON());
		html = html.replace(new RegExp('{yearsJSON}','g'), getYearsTreeJSON());
		
		nlapiLogExecution("DEBUG", "getCustomersGridJSON", getCustomersGridJSON());
		
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function getCustomersGridJSON(){

	var searchCustomers = nlapiLoadSearch('customrecord_clgx_consolidated_invoices', 'customsearch_clgx_ci_customers');

	var resultSet = searchCustomers.runSearch();
	var arrCustomers = new Array();
	resultSet.forEachResult(function(searchResult) {
		var colObj = new Object();
		colObj["customerid"] = parseInt(searchResult.getValue('custrecord_clgx_consol_inv_customer', null, 'GROUP'));
		colObj["customer"] = searchResult.getText('custrecord_clgx_consol_inv_customer', null, 'GROUP');
		arrCustomers.push(colObj);

		return true; // return true to keep iterating
		});
	
    return JSON.stringify(arrCustomers);
}

function getYearsTreeJSON(){

	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_year_display',null,'GROUP').setSort(true));
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_month_display',null,'GROUP'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_processed",null,"is",'T'));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_output",null,"anyof",1));
	var searchResults = nlapiSearchRecord('customrecord_clgx_consolidated_invoices', null, arrFilters, arrColumns);

	var arrYears = new Array();
	var arrMonths = new Array();
	for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
        	var searchResult = searchResults[i];
        	var year = searchResult.getValue('custrecord_clgx_consol_inv_year_display',null,'GROUP');
        	var monthid = searchResult.getValue('custrecord_clgx_consol_inv_month_display',null,'GROUP');
        	var month = searchResult.getText('custrecord_clgx_consol_inv_month_display',null,'GROUP');
        	if(_.indexOf(arrYears, year) == -1){
        		arrYears.push(year);
        	}
			var objMonth = new Object();
			objMonth["year"] = year;
			objMonth["month"] = month;
			objMonth["monthid"] = monthid;
			arrMonths.push(objMonth);
	}
	
	var objTree = new Object();
	objTree["text"] = '.';
	var arrY = new Array();
	for ( var i = 0; i < arrYears.length; i++ ) {
		
		objY = new Object();
		objY["entity"] = arrYears[i];
		objY["entityid"] = parseInt(arrYears[i]);
		if(i == 0){
			objY["expanded"] = true;
		}
		else{
			objY["expanded"] = false;
		}
		objY["iconCls"] = 'year';
		objY["leaf"] = false;
		
		var arrYearMonths = _.filter(arrMonths, function(arr){
	        return arr.year === arrYears[i];
		});
		
		var arrM = new Array();
		for ( var j = 0; j < arrYearMonths.length; j++ ) {
			
			objM = new Object();
			objM["entity"] = arrYearMonths[j].month;
			objM["entityid"] = parseInt(arrYearMonths[j].monthid);
			objM["yearid"] = parseInt(arrYears[i]);
			objM["expanded"] = true;
			objM["iconCls"] = 'month';
			objM["leaf"] = true;
			arrM.push(objM);
		}
		arrM = _.sortBy(arrM, function(obj){ return obj.entityid;}).reverse();
		objY["children"] = arrM;
		arrY.push(objY);
	}
	//arrY = _.sortBy(arrY, function(obj){ return obj.entityid;}).reverse();
	objTree["children"] = arrY;
	
	return JSON.stringify(objTree);
}


