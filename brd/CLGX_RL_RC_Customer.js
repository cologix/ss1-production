//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_RC_Customer.js
//	Script Name:	CLGX_RL_RC_Customer
//	Script Id:		customscript_CLGX_RL_RC_Customer
//	Script Runs:	On Server
//	Script Type:	RESTlet
//	@authors:		Catalina Taran - dan.tansanu@cologix.com

//-------------------------------------------------------------------------------------------------

function get (datain){
    try {

        datain=JSON.parse(datain);
        var  custpage_phone=datain.custpage_phone;

        var columns = new Array();
        var filters = new Array();
        filters.push(new nlobjSearchFilter("phone",null,"is",custpage_phone));
        var records = nlapiSearchRecord('contact','customsearch5547', filters, columns);
        var obj=new Object();
        var arrObj=new Array();
        if(records!=null) {

            for ( var j = 0; j < records.length; j++ ) {
                var searchAP = records[j];
                var columns = searchAP.getAllColumns();
                var id = searchAP.getValue(columns[0]);
                var name = searchAP.getValue(columns[1]);

                var rec = new Object();
                rec.name = name;
                rec.url = 'https://debugger.na2.netsuite.com/app/common/entity/contact.nl?id='+id;
                arrObj.push(rec)

            }
            obj["data"] = arrObj;

        }
        else {
            obj["data"]=[];
        }
             return obj;
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}

