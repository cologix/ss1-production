nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_SP_Contact_Programs.js
//	Script Name:	CLGX_SL_SP_Contact_Programs
//	Script Id:		customscript_clgx_sl_sp_contact_programs
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		05/14/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=497&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_sp_contact_programs (request, response){
	try {
		
		//var add = request.getParameter('add');
		var contactid = request.getParameter('contactid');
		
		var objTree = new Object();
		objTree["text"] = '.';
		
		var recContact = nlapiLoadRecord ('contact', contactid);
		var strRecipients = recContact.getFieldValue('custentity_clgx_sp_recipients');
		var contact = recContact.getFieldValue('entityid');
		var fname = recContact.getFieldValue('firstname');
		var lname = recContact.getFieldValue('lastname');
		if(contact == null || contact == ''){
			contact = fname + ' ' + lname;
		}
		
		
		if(strRecipients != null && strRecipients != ''){
			
			var arrRecipients = JSON.parse(strRecipients);
			
			var strPrograms = nlapiLoadFile(2547762);
			var arrPrograms = JSON.parse(strPrograms.getValue());
			
// ============================ Open Session ===============================================================================================
	    	
	    	var strPost = clgx_sp_login (username,password);
	    	var objHead = clgx_sp_header (strPost.length);
			var requestURL = nlapiRequestURL(strLogURL,strPost,objHead);
			var responseXML = nlapiStringToXML(requestURL.getBody());
			var login = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
			
			if(login == 'true'){
				
				var jsessionid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SESSIONID');
				strURL += jsessionid;
	
				var arrRecips = new Array();
				
				for ( var i = 0; arrRecipients != null && i < arrRecipients.length; i++ ) {
					
					var cenroled = 0;
					var recipientid = arrRecipients[i].recipientid;
					var customerid = arrRecipients[i].customerid;
					var customer = nlapiLookupField('customer', customerid, 'entityid');
					var node = contact + ' / ' + customer;
					
					var objRecip = new Object();
					objRecip["node"] = node;
					objRecip["nodeid"] = parseInt(recipientid);
					objRecip["customer"] = customer;
			    	objRecip["customerid"] = parseInt(customerid);
			    	objRecip["expanded"] = true;
			    	//objRecip["iconCls"] = 'fa fa-user';
			    	objRecip["leaf"] = false;
			    	
// ======================================= Recipient Programs ===================================================================================================
			
					// ====== Build Parameters list =================================
					var objParams = new Array();
					objParams["listid"] = '3325006';
					objParams["active"] = 'true';
					objParams["inactive"] = 'true';
					objParams["recipientid"] = recipientid;
					objParams["begin"] = '';
					objParams["end"] = '';
					objParams["history"] = '';
					objParams["sales"] = '';
					objParams["track"] = '';
					objParams["step"] = '';
					// ====== Build Columns Tags Array =================================
					var arrTags = new Array();
					
					var strPost = clgx_sp_recipient_programs (objParams, arrTags);
			    	var objHead = clgx_sp_header (strPost.length);
					var requestURL = nlapiRequestURL(strURL,strPost,objHead);
					var strBody = requestURL.getBody();
					var responseXML = nlapiStringToXML(strBody);
					
					var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
					if(success == 'TRUE'){
						
						var xmlPrograms = nlapiSelectNodes(responseXML, '/Envelope/Body/RESULT/PROGRAMS/PROGRAM');
						var arrRecipientPrograms = new Array();
						for ( var j = 0; j < xmlPrograms.length; j++ ) {
							
							var objRecipientProgram = new Object();
							objRecipientProgram["programid"] = parseInt(nlapiSelectValue(xmlPrograms[j], 'ID'));
							objRecipientProgram["status"] = nlapiSelectValue(xmlPrograms[j], 'STATE');
							arrRecipientPrograms.push(objRecipientProgram);
						}
					}
					else{

					}
					
					var arrProgs = new Array();
			    	for ( var j = 0; arrPrograms != null && j < arrPrograms.length; j++ ) {
			    		
			    		// check if recipient in any program
			    		var objP = _.find(arrRecipientPrograms, function(arr){ return (arr.programid == arrPrograms[j].programid); });
			    		
			    		var enroled = 0;
			    		if(objP != null){
			    			enroled = 1;
			    			cenroled = 1;
			    		}
			    		var objProg = new Object();
						objProg["node"] = arrPrograms[j].program;
						objProg["nodeid"] = parseInt(arrPrograms[j].programid);
						objProg["recipientid"] = parseInt(recipientid);
						objProg["enroled"] = enroled;
				    	//objProg["iconCls"] = 'fa fa-user-plus';
				    	objProg["leaf"] = true;
				    	arrProgs.push(objProg);
			    	}
			    	objRecip["enroled"] = cenroled;
			    	objRecip["children"] = arrProgs;
			    	arrRecips.push(objRecip);
				}
				
				objTree["children"] = arrRecips;
				
// ============================ Close Session ===============================================================================================
				 
		    	var strPost =  clgx_sp_logout ();
				var objHead = clgx_sp_header (strPost.length);
				var requestURL = nlapiRequestURL(strURL,strPost,objHead);
				var strBody = requestURL.getBody();
				var responseXML = nlapiStringToXML(strBody);
				var logout = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
			}
			
		}

		var message = '';
		var objFile = nlapiLoadFile(2548881);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{programsJSON}','g'), JSON.stringify(objTree));
		html = html.replace(new RegExp('{message}','g'), '');
		response.write(html);
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


