nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SU_Contact.js
//	Script Name:	CLGX_SU_Contact
//	Script Id:		customscript_clgx_su_contact
//	Script Runs:	On Server
//	Script Type:	User Event Script
//	Deployments:	Contact
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		2/28/2012
//	Includes:		CLGX_RL_NAC_Library.js
//-------------------------------------------------------------------------------------------------

function beforeLoad(type, form) {
    try {

        var currentContext = nlapiGetContext();
        var userid = nlapiGetUser();
        var roleid = nlapiGetRole();
        var contactid = nlapiGetRecordId();

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.0 - 09/13/2012
// Details:	Add SilverPOP Tab
//-----------------------------------------------------------------------------------------------------------------
        var recipientid = nlapiGetFieldValue('externalid');
/*
        if (currentContext.getExecutionContext() == 'userinterface' && (roleid == '-5' || roleid == '3' || roleid == '18' || roleid == '1010' || roleid == '1006' || roleid == '1017' || roleid == '1018' || roleid == '1019' || roleid == '1039' || roleid == '1046' || roleid == '1040')){
            var spTab = form.addTab('custpage_tab_silverpop', 'Silverpop Insights');
            var spField = form.addField('custpage_silverpop','inlinehtml', null, null, 'custpage_tab_silverpop');
            spField.setDefaultValue('<iframe name="silverpop" id="silverpop" src="/app/site/hosting/scriptlet.nl?script=482&deploy=1&contactid=' + parseInt(contactid) + '" height="700px" width="1400px" frameborder="0" scrolling="no"></iframe>');
        }
*/
        if (currentContext.getExecutionContext() == 'userinterface'){
            var cpTab = form.addTab('custpage_tab_cp_rights', 'CP Rights');
            var cpField = form.addField('custpage_cp_rights','inlinehtml', null, null, 'custpage_tab_cp_rights');
            cpField.setDefaultValue('<iframe name="_cp_rights" id="cp_rights" src="/app/site/hosting/scriptlet.nl?script=695&deploy=1&act=edit&id=' + parseInt(contactid) + '" height="560px" width="530px" frameborder="0" scrolling="no"></iframe>');
        }

//---------- End Sections ------------------------------------------------------------------------------------------------

    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function beforeSubmit (type) {
    try {
    	/*
    	var currentContext = nlapiGetContext();
        
        var userid = nlapiGetUser();
        var roleid = currentContext.getRole();
        var contactid = nlapiGetRecordId();
        var customerid = nlapiGetFieldValue('company');
        var inactive = nlapiGetFieldValue('isinactive');

//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Added:	11/16/2016
// Details:	Create modifier if email was added
//-----------------------------------------------------------------------------------------------------------------
     
	     var email = nlapiGetFieldValue('email') || '';
	     var modifier = nlapiGetFieldValue('custentity_clgx_modifier') || 0;
	     
	     nlapiLogExecution('DEBUG','debug', '| email = ' + email + ' | ');
	     nlapiLogExecution('DEBUG','debug', '| modifier = ' + modifier + ' | ');
	     
	     if (type == 'edit' && email && !modifier) {
	     	
			var oldrec = nlapiGetOldRecord();
			var oldemail = oldrec.getFieldValue('email') || '';
			var entitystatus = nlapiLookupField('customer',customerid,'entitystatus') || 0;
			
			nlapiLogExecution('DEBUG','debug', '| oldemail = ' + oldemail + ' | ');
			
			if(entitystatus==13 && !oldemail) {
				var language = nlapiLookupField('customer',customerid,'language') || 'en';
	            var fname = nlapiGetFieldValue('firstname') || '';
	            var lname = nlapiGetFieldValue('lastname') || '';
	            var newid = clgx_cp_modifier(contactid, fname, lname, language);
	            
	            nlapiLogExecution('DEBUG','debug', '| newid = ' + newid + ' | ');
	         }
	     }
     
     
        
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.1 - 7/12/2012
// Details:	Verify if there is a billing contact for the customer. If yes, modify the custom flag 'Is Billing Contact' on Contact.
//			Verify if there the contact email is unique on the system.
//-----------------------------------------------------------------------------------------------------------------

        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'create' || type == 'edit')) {

            if(type == 'create'){
                // verify first  if the email exist
                var email = nlapiGetFieldValue('email');
                var arrFilters = new Array();
                arrFilters[0] = new nlobjSearchFilter('email',null,'is',email);
                var searchContactEmail = nlapiSearchRecord('contact', null, arrFilters, null);

                if (searchContactEmail != null) { // this email exist
                    nlapiSetFieldValue('email', '');
                    var arrParam = new Array();
                    arrParam['custscript_internal_message'] = 'This email exist. Please consider attaching the contact.';
                    nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
                }

                // if new create portal password
                nlapiSetFieldValue('custentity_clgx_portal_password', generatePassword(8,false));
            }


            // verify if this is a billing contact
            var customerid = nlapiGetFieldValue('company');
            var roleid = nlapiGetFieldValue('contactrole');
            if(roleid == '1'){

                var arrColumns = new Array();
                var arrFilters = new Array();
                arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
                arrFilters[0] = new nlobjSearchFilter('internalid',null,'is',customerid);
                var searchResults = nlapiSearchRecord('customer', null, arrFilters, arrColumns);

                if(searchResults != null){
                    nlapiSubmitField('customer',customerid,['custentity_clgx_has_billing_contact'],['T']);
                    nlapiSetFieldValue('custentity_clgx_is_billing_contact', 'T');
                }

            }
            else{
                nlapiSetFieldValue('custentity_clgx_is_billing_contact', 'F');
            }
        }

//------------- Begin Section 2 -----------------------------------------------------------------------------------
// Version:	1.1 - 10/17/2012
// Details:	Change and email new portal password to contact if asked by user
//-----------------------------------------------------------------------------------------------------------------

        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'edit')) {

            var resetPassword = nlapiGetFieldValue('custentity_clgx_contact_reset_password');
            if(resetPassword == 'T'){ // reset and email new password
                var userID = nlapiGetUser();
                var contactID = nlapiGetRecordId();
                var newPassword = nlapiGetFieldValue('custentity_clgx_portal_password');

                emailSubject = 'Cologix Customer Portal - Your New Password';
                emailBody = 'Dear Customer,\n\n' +
                    'Here is your new password:\n\n' + newPassword + '\n\n' +
                    'Thank you for your business.\n\n' +
                    'Sincerely,\n' +
                    'Cologix\n\n';
                nlapiSendEmail(userID,contactID,emailSubject,emailBody,null,null,null,null);
                nlapiSetFieldValue('custentity_clgx_portal_login_attempts', 0); // reset login attempts to 0
            }
            else{ // if reset password is not checked, then keep old password
                var oldContact = nlapiGetOldRecord();
                var oldPassword = oldContact.getFieldValue('custentity_clgx_portal_password');
                nlapiSetFieldValue('custentity_clgx_portal_password', oldPassword);
            }
        }
*/

//---------- End Sections ------------------------------------------------------------------------------------------------


    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}



function afterSubmit (type) {
    try {

        var currentContext = nlapiGetContext();
        var userid = nlapiGetUser();
        var roleid = currentContext.getRole();
        var contactid = nlapiGetRecordId();
        var customerid = nlapiGetFieldValue('company');
        var inactive = nlapiGetFieldValue('isinactive');


//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Version:	1.1 - 7/12/2012
// Details:	Verify if there is a billing contact for the customer. If yes, modify the custom flag 'Has Billing Contact' on Customer.
//-----------------------------------------------------------------------------------------------------------------

        if ((currentContext.getExecutionContext() == 'userinterface') && (type == 'create' || type == 'edit') && customerid != '') {

            // search all billing contacts of this customer
            var arrColumns = new Array();
            arrColumns[0] = new nlobjSearchColumn('internalid', 'contact', null);
            var arrFilters = new Array();
            arrFilters[0] = new nlobjSearchFilter('internalid',null,'is',customerid);
            arrFilters[1] = new nlobjSearchFilter('contactrole','contact','is',1);
            var searchResults = nlapiSearchRecord('customer', null, arrFilters, arrColumns);

            if (searchResults != null) { // no billing contact
                nlapiSubmitField('customer',customerid,['custentity_clgx_has_billing_contact'],['T']);
            }
            else{
                if(customerid != null && customerid != ''){

                    var arrColumns = new Array();
                    arrColumns[0] = new nlobjSearchColumn('internalid', null, null);
                    var arrFilters = new Array();
                    arrFilters[0] = new nlobjSearchFilter('internalid',null,'is',customerid);
                    var searchCustomer = nlapiSearchRecord('customer', null, arrFilters, arrColumns);

                    if(searchCustomer != null){
                        nlapiSubmitField('customer',customerid,['custentity_clgx_has_billing_contact'],['F']);
                    }
                }
            }
        }

//------------- Begin Section 2 -----------------------------------------------------------------------------------
// Created:	01/27/2016
// Details:	Syncronize with NAC
//-----------------------------------------------------------------------------------------------------------------

        var environment = currentContext.getEnvironment();

        if (environment == 'PRODUCTION' && (type == 'xedit'  || currentContext.getExecutionContext() == 'userinterface')) {

            var contactid = nlapiGetRecordId();
            var customerid = nlapiGetFieldValue('company');
            var userid = nlapiGetUser();
            var user = nlapiLookupField('employee', userid, 'entityid');

            if(customerid != null && customerid != ''){

                var arrColumns = new Array();
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
                var searchSOs = nlapiSearchRecord('salesorder', 'customsearch_clgx_nj_sos_check', arrFilters, arrColumns);

                if(searchSOs != null){
                    var row = nlapiLoadRecord('contact', contactid);
                    var ret = json_serialize(row);
                    var companyid = row.getFieldValue('company');
                    nlapiLogExecution('DEBUG', 'flexapi contactid: ', contactid);
                    nlapiLogExecution('DEBUG', 'flexapi companyid: ', companyid);
                    ret = append_attached_companies(ret, contactid, companyid);

                    var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
                    record.setFieldValue('custrecord_clgx_queue_ping_record_id', contactid);
                    record.setFieldValue('custrecord_clgx_queue_ping_record_type', 2);
                    record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
                    var idRec = nlapiSubmitRecord(record, false,true);
                    
                    /*
                    try {
                        flexapi('POST','/netsuite/update', {
                            'type': 'contact',
                            'id': contactid,
                            'action': type,
                            'userid': userid,
                            'user': user,
                            'data': ret
                        });
                        nlapiLogExecution('DEBUG', 'flexapi request: ', contactid);
                    }
                    catch (error) {
                        var record = nlapiCreateRecord('customrecord_clgx_queue_ping_matrix');
                        record.setFieldValue('custrecord_clgx_queue_ping_record_id', contactid);
                        record.setFieldValue('custrecord_clgx_queue_ping_record_type', 2);
                        record.setFieldValue('custrecord_clgx_queue_ping_processed', 'F');
                        var idRec = nlapiSubmitRecord(record, false,true);
                    }
                    */
                }
            }
        }
        if (environment == 'PRODUCTION' && currentContext.getExecutionContext() == 'userinterface' && type == 'create') {
            var language = nlapiLookupField('customer',customerid,'language')||'en';
            var fname=nlapiLookupField('contact', contactid, 'firstname') || '';
            var lname=nlapiLookupField('contact', contactid, 'lastname') || '';
            var entitystatus=nlapiLookupField('customer',customerid,'entitystatus');
            
            nlapiLogExecution("DEBUG", "bs-language", JSON.stringify(language));
            nlapiLogExecution("DEBUG", "bs-fname", JSON.stringify(fname));
            nlapiLogExecution("DEBUG", "bs-lname", JSON.stringify(lname));
            nlapiLogExecution("DEBUG", "bs-entitystatus", JSON.stringify(entitystatus));
            
            if(entitystatus==13) {
                var modifier = clgx_cp_modifier(contactid, fname, lname, language);
            }
        }
        if (environment == 'PRODUCTION' && currentContext.getExecutionContext() == 'userinterface' && inactive=='T') {
            var modifierID= nlapiGetFieldValue('custentity_clgx_modifier');
            if(modifierID!=null && modifierID!='') {
                var filtersLL = new Array();
                filtersLL.push(new nlobjSearchFilter("internalid", null, "is", modifierID));
                var searchMod = nlapiSearchRecord('customrecord_clgx_modifier', 'customsearch_clgx_cp_pur_active', filtersLL);
                if(searchMod==null) {
                    var modRecord = nlapiLoadRecord('customrecord_clgx_modifier', modifierID);
                    modRecord.setFieldValue('isinactive', 'T');
                    nlapiSubmitRecord(modRecord, true, true);
                }
            }
        }


//------------- Begin Section 3 -----------------------------------------------------------------------------------
// Created:	04/06/2015
// Details:	Queue contact/customers to syncronize with SilverPOP
//-----------------------------------------------------------------------------------------------------------------
        /*
         //if (type == 'xedit' || type == 'delete' || ((currentContext.getExecutionContext() == 'userinterface') && (type == 'create' || type == 'edit' || type == 'delete'))) {

         var action = 1;
         if(type == 'create'){
         action = 0;
         }
         if(type == 'delete'){
         action = 2;
         }
         if(inactive == 'T'){
         action = 3;
         }

         var arrColumns = new Array();
         arrColumns.push(new nlobjSearchColumn('internalid',null,null));
         var arrFilters = new Array();
         arrFilters.push(new nlobjSearchFilter("custrecord_clgx_contact_to_sp_contact",null,"equalto",contactid));
         arrFilters.push(new nlobjSearchFilter('custrecord_clgx_contact_to_sp_done',null,'is','F'));
         var searchQueue = nlapiSearchRecord('customrecord_clgx_queue_contacts_to_sp', null, arrFilters, arrColumns);

         if(searchQueue == null){ // add contact to queue if not in it
         var record = nlapiCreateRecord('customrecord_clgx_queue_contacts_to_sp');
         record.setFieldValue('custrecord_clgx_contact_to_sp_contact', contactid);
         record.setFieldValue('custrecord_clgx_contact_to_sp_action', action);
         record.setFieldValue('custrecord_clgx_contact_to_sp_done', 'F');
         var idRec = nlapiSubmitRecord(record, false,true);
         }
         else{ // if in queue change status in case it was different
         nlapiSubmitField('customrecord_clgx_queue_contacts_to_sp', searchQueue[0].getValue('internalid',null,null), 'custrecord_clgx_contact_to_sp_action', action);
         }

         //}
         */
//---------- End Sections ------------------------------------------------------------------------------------------------

    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

function generatePassword(length, special) {
    var iteration = 0;
    var password = "";
    var randomNumber;
    if(special == undefined){
        var special = false;
    }
    while(iteration < length){
        randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
        if(!special){
            if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
            if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
            if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
            if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
        }
        iteration++;
        password += String.fromCharCode(randomNumber);
    }
    return password;
}




function clgx_get_obj_contact (contactid) {

    var recContact = nlapiLoadRecord ('contact',contactid);

    var arrOldRecipients = new Array();
    var jsonOldRecipients = recContact.getFieldValue('custentity_clgx_sp_recipients');
    if(jsonOldRecipients != null && jsonOldRecipients != ''){
        arrOldRecipients = JSON.parse(jsonOldRecipients);
    }

    var objContact = new Object();
    objContact["contactid"] = parseInt(contactid);
    objContact["firstname"] = recContact.getFieldValue('firstname');
    objContact["lastname"] = recContact.getFieldValue('lastname');
    objContact["name"] = recContact.getFieldValue('entityid');
    objContact["email"] = recContact.getFieldValue('email');
    objContact["phone"] = recContact.getFieldValue('phone');
    var headcustomerid = recContact.getFieldValue('company');
    if(headcustomerid != null && headcustomerid != ''){
        objContact["customerid"] = parseInt(headcustomerid);
    }
    else{
        objContact["customerid"] = '';
    }
    objContact["customer"] = recContact.getFieldText('company');

    var arrRecipients = new Array();
    var arrColumns = new Array();
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",contactid));
    var searchRecipients = nlapiSearchRecord('contact', 'customsearch_clgx_sp_contacts2csv', arrFilters, arrColumns);

    for ( var i = 0; searchRecipients != null && i < searchRecipients.length; i++ ) {
        var searchRecipient = searchRecipients[i];
        var objRecipient = new Object();
        objRecipient["contactid"] = parseInt(searchRecipient.getValue('internalid',null,null));
        objRecipient["firstname"] = searchRecipient.getValue('firstname',null,null);
        objRecipient["lastname"] = searchRecipient.getValue('lastname',null,null);
        objRecipient["name"] = searchRecipient.getValue('entityid',null,null);
        objRecipient["email"] = searchRecipient.getValue('email',null,null);
        objRecipient["phone"] = searchRecipient.getValue('phone',null,null);
        objRecipient["role"] = searchRecipient.getText('role',null,null);

        objRecipient["subsidiary"] = searchRecipient.getText('subsidiarynohierarchy',null,null);

        var created = searchRecipient.getValue('datecreated',null,null);
        objRecipient["created"] = moment(created).format('M/D/YYYY');
        objRecipient["optout"] = searchRecipient.getText('globalsubscriptionstatus',null,null);

        var customerid = searchRecipient.getValue('internalid','customer',null);
        if(customerid != null && customerid != ''){
            objRecipient["customerid"] = parseInt(customerid);
            objRecipient["customer"] = searchRecipient.getValue('companyname','customer',null);

            objRecipient["firstorder"] = searchRecipient.getValue('firstorderdate','customer',null);
            objRecipient["firstsale"] = searchRecipient.getValue('firstsaledate','customer',null);
            objRecipient["status"] = searchRecipient.getText('status','customer',null);
            objRecipient["category"] = searchRecipient.getText('category','customer',null);

            var repid = searchRecipient.getValue('salesrep','customer',null);
            if(repid != null && repid != ''){
                var recRep = nlapiLoadRecord ('employee',repid);
                objRecipient["rep"] = recRep.getFieldValue('entityid');
                objRecipient["repemail"] = recRep.getFieldValue('email');
                objRecipient["repphone"] = recRep.getFieldValue('phone');
                objRecipient["repsupervisor"] = recRep.getFieldValue('supervisor');
            }
            else{
                objRecipient["rep"] = '';
                objRecipient["repemail"] = '';
                objRecipient["repphone"] = '';
                objRecipient["repsupervisor"] = '';
            }
        }
        else{
            objRecipient["customerid"] = '';
            objRecipient["customer"] = '';
            objRecipient["firstorder"] = '';
            objRecipient["firstsale"] = '';
            objRecipient["status"] = '';
            objRecipient["category"] = '';
            objRecipient["rep"] = '';
            objRecipient["repemail"] = '';
            objRecipient["repphone"] = '';
            objRecipient["repsupervisor"] = '';
        }

        if(headcustomerid == customerid){


            objContact["subsidiary"] = objRecipient.subsidiary;
            objContact["created"] = objRecipient.created;
            objContact["optout"] = objRecipient.optout;
            objContact["role"] = objRecipient.role;
            objContact["firstorder"] = objRecipient.firstorder;
            objContact["firstsale"] = objRecipient.firstsale;
            objContact["status"] = objRecipient.status;
            objContact["category"] = objRecipient.category;
            objContact["rep"] = objRecipient.rep;
            objContact["repemail"] = objRecipient.repemail;
            objContact["repphone"] = objRecipient.repphone;
            objContact["repsupervisor"] = objRecipient.repsupervisor;

            objRecipient["head"] = true;
        }
        else{
            objRecipient["head"] = false;
        }
        var objOldRecipient = _.find(arrOldRecipients, function(arr){ return (arr.customerid == parseInt(customerid)); });
        if(objOldRecipient != null){
            objRecipient["action"] = 'update';
        }
        else{
            objRecipient["action"] = 'create';
        }
        arrRecipients.push(objRecipient);
    }

    // find the recipients to delete  - contact is not linked to a customer anymore
    for ( var i = 0; arrOldRecipients != null && i < arrOldRecipients.length; i++ ) {
        var objNewRecipient = _.find(arrRecipients, function(arr){ return (arr.customerid == arrOldRecipients[i].customerid); });
        if(objNewRecipient == null){
            var objRecipient = new Object();
            objRecipient["contactid"] = parseInt(contactid);
            objRecipient["customerid"] = arrOldRecipients[i].customerid;
            objRecipient["action"] = 'delete';
            arrRecipients.push(objRecipient);
        }
    }

    objContact["recipients"] = arrRecipients;

    return objContact;
}


function clgx_sp_contact_create_update (strURL,objContact) {

    // ====== Build Parameters list =================================
    var objParams = new Array();
    objParams["listid"] = '3325006';
    objParams["from"] = 0;
    objParams["update"] = true;
    // ====== Build Sync Fields Array =================================
    var arrSync = new Array();
    arrSync.push(JSON.parse('{"name": "Company Internal ID", "value": "' + objContact.customerid + '"}'));
    arrSync.push(JSON.parse('{"name": "NS Internal ID", "value": "' + objContact.contactid + '"}'));
    // ====== Build Columns Fields Array =================================
    var arrColumns = new Array();
    arrColumns.push(JSON.parse('{"name": "Company Internal ID", "value": "' + objContact.customerid + '"}'));
    arrColumns.push(JSON.parse('{"name": "Company Name", "value": "' + objContact.customer + '"}'));
    arrColumns.push(JSON.parse('{"name": "NS Internal ID", "value": "' + objContact.contactid + '"}'));
    arrColumns.push(JSON.parse('{"name": "Contact  Subsidiary", "value": "' + objContact.subsidiary + '"}'));
    arrColumns.push(JSON.parse('{"name": "Date Created", "value": "' + objContact.created + '"}'));
    arrColumns.push(JSON.parse('{"name": "Role", "value": "' + objContact.role + '"}'));
    arrColumns.push(JSON.parse('{"name": "First name", "value": "' + objContact.firstname + '"}'));
    arrColumns.push(JSON.parse('{"name": "Last Name", "value": "' + objContact.lastname + '"}'));
    arrColumns.push(JSON.parse('{"name": "Name", "value": "' + objContact.name + '"}'));
    arrColumns.push(JSON.parse('{"name": "Email", "value": "' + objContact.email + '"}'));
    arrColumns.push(JSON.parse('{"name": "Phone", "value": "' + objContact.phone + '"}'));
    var strPost = clgx_sp_recipient_create (objParams,arrSync,arrColumns);

    var objHead = clgx_sp_header (strPost.length);
    var requestURL = nlapiRequestURL(strURL,strPost,objHead);
    var strBody = requestURL.getBody();
    var responseXML = nlapiStringToXML(strBody);

    var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');

    if(success == 'TRUE'){
        return nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/RecipientId');
    }
    else{
        return 0;
    }
}

function clgx_sp_contact_delete (strURL,objContact) {

    // ====== Build Parameters list =================================
    var objParams = new Array();
    objParams["listid"] = '3325006';
    objParams["email"] = '';
    // ====== Build Columns Fields Array =================================
    var arrColumns = new Array();
    arrColumns.push(JSON.parse('{"name": "Company Internal ID", "value": "' + objContact.customerid + '"}'));
    arrColumns.push(JSON.parse('{"name": "NS Internal ID", "value": "' + objContact.contactid + '"}'));

    var strPost = clgx_sp_recipient_delete (objParams, arrColumns);

    var objHead = clgx_sp_header (strPost.length);
    var requestURL = nlapiRequestURL(strURL,strPost,objHead);
    var strBody = requestURL.getBody();
    var responseXML = nlapiStringToXML(strBody);



    var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');

    if(success == 'TRUE'){
        return 1;
    }
    else{
        return 0;
    }
}


function clgx_sp_contact_login (username,password) {

    var strPost = clgx_sp_login (username,password);
    var objHead = clgx_sp_header (strPost.length);
    var requestURL = nlapiRequestURL(strLogURL,strPost,objHead);
    var responseXML = nlapiStringToXML(requestURL.getBody());
    var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');

    if(success){
        return nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SESSIONID');
    }
    else{
        return '';
    }
}


function clgx_sp_contact_logout (strURL) {

    var strPost =  clgx_sp_logout ();
    var objHead = clgx_sp_header (strPost.length);
    var requestURL = nlapiRequestURL(strURL,strPost,objHead);
    var strBody = requestURL.getBody();
    var responseXML = nlapiStringToXML(strBody);
    return nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
}


function clgx_cp_modifier (contactId,fname,ltname,language) {
    if (language == 'en_US') {
        var lan = 1;
    }
    if (language == 'fr_CA') {
        var lan = 17;
    }
    var modifierID='';
    var nameModifier = fname.charAt(0).toLowerCase() + (ltname.replace(/\s+/g, '')).toLowerCase();
    nameModifier = nameModifier.replace(/\W+/g, '');
    nameModifier = nameModifier.replace(/[0-9]/g, '');
    
    nlapiLogExecution("DEBUG", "nameModifier", JSON.stringify(nameModifier));
    
    if (nameModifier.length > 1)
    {
        var rec = nlapiCreateRecord('customrecord_clgx_modifier');
        rec.setFieldValue('name', nameModifier);
        rec.setFieldValue('custrecord_clgx_modifier_language', lan);
        var id = nlapiSubmitRecord(rec, true);
        var recordModifier = nlapiLoadRecord('customrecord_clgx_modifier', id);
        var nameMod = recordModifier.getFieldValue('name');
        recordModifier.setFieldValue('name', nameMod + id);
        nlapiSubmitRecord(recordModifier, false, true);
        var recordcontact = nlapiLoadRecord('contact', contactId);
        recordcontact.setFieldValue('custentity_clgx_modifier', id);
        nlapiSubmitRecord(recordcontact, false, false);
        modifierID=id;
    }
    return modifierID;
}