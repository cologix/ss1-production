nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_SP_Contact_Reports.js
//	Script Name:	CLGX_SL_SP_Contact_Reports
//	Script Id:		customscript_clgx_sl_sp_contact_reports
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		05/21/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=499&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_sp_contact_reports (request, response){
	try {
		
		//var add = request.getParameter('add');
		var contactid = request.getParameter('contactid');
		
		var objTree = new Object();
		objTree["text"] = '.';
		
		var recContact = nlapiLoadRecord ('contact', contactid);
		var strRecipients = recContact.getFieldValue('custentity_clgx_sp_recipients');
		var contact = recContact.getFieldValue('entityid');
		var fname = recContact.getFieldValue('firstname');
		var lname = recContact.getFieldValue('lastname');
		if(contact == null || contact == ''){
			contact = fname + ' ' + lname;
		}
		
		if(strRecipients != null && strRecipients != ''){
			
			var arrRecipients = JSON.parse(strRecipients);
			
// ============================ Open Session ===============================================================================================
	    	
	    	var strPost = clgx_sp_login (username,password);
	    	var objHead = clgx_sp_header (strPost.length);
			var requestURL = nlapiRequestURL(strLogURL,strPost,objHead);
			var responseXML = nlapiStringToXML(requestURL.getBody());
			var login = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
			
			if(login == 'true'){
				
				var jsessionid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SESSIONID');
				strURL += jsessionid;
	
				var arrRecips = new Array();
				
				for ( var i = 0; arrRecipients != null && i < arrRecipients.length; i++ ) {
					
					var recipientid = arrRecipients[i].recipientid;
					var customerid = arrRecipients[i].customerid;
					var customer = nlapiLookupField('customer', customerid, 'entityid');
					var node = contact + ' / ' + customer;
					
					var objRecip = new Object();
					objRecip["node"] = node;
					objRecip["nodeid"] = parseInt(recipientid);
					objRecip["customer"] = customer;
			    	objRecip["customerid"] = parseInt(customerid);
			    	objRecip["expanded"] = true;
			    	//objRecip["iconCls"] = 'fa fa-user';
			    	objRecip["leaf"] = false;
					
// ======================================= Recipient Mailings Stats ===================================================================================================
			
				
				// ====== Build Parameters list =================================
				var objParams = new Array();
				objParams["listid"] = '3325006';
				objParams["recipientid"] = recipientid;
				
				var strPost = clgx_sp_recipient_mailings (objParams);
		    	var objHead = clgx_sp_header (strPost.length);
				var requestURL = nlapiRequestURL(strURL,strPost,objHead);
				var strBody = requestURL.getBody();
				var responseXML = nlapiStringToXML(strBody);
				
				var arrMailings = new Array();
				var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
				if(success == 'TRUE'){
					
					var xmlMailings = nlapiSelectNodes(responseXML, '/Envelope/Body/RESULT/Mailing');
					for ( var j = 0; j < xmlMailings.length; j++ ) {
						
						var objMailing = new Object();
						objMailing["nodeid"] = parseInt(nlapiSelectValue(xmlMailings[j], 'MailingId'));
						objMailing["node"] = nlapiSelectValue(xmlMailings[j], 'MailingName');
						objMailing["reportid"] = parseInt(nlapiSelectValue(xmlMailings[j], 'ReportId'));
						objMailing["sent"] = nlapiSelectValue(xmlMailings[j], 'SentTS');
						objMailing["opens"] = nlapiSelectValue(xmlMailings[j], 'TotalOpens');
						objMailing["streams"] = nlapiSelectValue(xmlMailings[j], 'TotalClickstreams');
						objMailing["clicks"] = nlapiSelectValue(xmlMailings[j], 'TotalClicks');
						objMailing["conversions"] = nlapiSelectValue(xmlMailings[j], 'TotalConversions');
						objMailing["status"] = nlapiSelectValue(xmlMailings[j], 'TotalAttachments');
						objMailing["attachments"] = nlapiSelectValue(xmlMailings[j], 'TotalForwards');
						objMailing["media"] = nlapiSelectValue(xmlMailings[j], 'TotalMediaPlays');
						objMailing["bounces"] = nlapiSelectValue(xmlMailings[j], 'TotalBounces');
						objMailing["optouts"] = nlapiSelectValue(xmlMailings[j], 'TotalOptOuts');
						objMailing["leaf"] = true;
						arrMailings.push(objMailing);
					}
				}
				objRecip["children"] = arrMailings;
				arrRecips.push(objRecip);
				}
				objTree["children"] = arrRecips;
				
// ============================ Close Session ===============================================================================================
				 
		    	var strPost =  clgx_sp_logout ();
				var objHead = clgx_sp_header (strPost.length);
				var requestURL = nlapiRequestURL(strURL,strPost,objHead);
				var strBody = requestURL.getBody();
				var responseXML = nlapiStringToXML(strBody);
				var logout = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
			}
		}
		
		var message = '';
		var objFile = nlapiLoadFile(2595974);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{reportsJSON}','g'), JSON.stringify(objTree));
		html = html.replace(new RegExp('{message}','g'), '');
		
		response.write(html);

	}
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


