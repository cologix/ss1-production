//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_Contact_SilverPOP.js
//	Script Name:	CLGX_SL_Contact_SilverPOP
//	Script Id:		customscript_clgx_sl_contact_silverpop
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		07/01/2014
//	URL:			/app/site/hosting/scriptlet.nl?script=482&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet__contact_silverpop (request, response){
	try {

		var currentContext = nlapiGetContext();
		var userid = nlapiGetUser();
		var roleid = nlapiGetRole();
		
		var recipientid = request.getParameter('recipientid');
		
		if ((roleid == '-5' || roleid == '3' || roleid == '18') && (recipientid != null && recipientid != '')) {
			
			var sessionid = clgx_sp_contact_login (username,password);
			if(sessionid != ''){
				strURL += sessionid;
				
				var objParams = new Array();
				objParams["listid"] = '3325006';
				objParams["recipientid"] = recipientid;
				
				var strPost = clgx_sp_recipient_mailings (objParams);
		    	var objHead = clgx_sp_header (strPost.length);
				var requestURL = nlapiRequestURL(strURL,strPost,objHead);
				var strBody = requestURL.getBody();
				var responseXML = nlapiStringToXML(strBody);
				
				var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
				if(success == 'TRUE'){
					
					var mailings = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/Mailing/MailingId');
					
					if(mailings != null){
						var html ='';
						html += '<table border="1" cellpadding="5">';
						html += '<tr><td>RecipientID</td><td>' + recipientid + '</td></tr>';
						html += '<tr><td>MailingId</td><td>' + nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/Mailing/MailingId') + '</td></tr>';
						html += '<tr><td>ReportId</td><td>' + nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/Mailing/ReportId') + '</td></tr>';
						html += '<tr><td>MailingName</td><td>' + nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/Mailing/MailingName') + '</td></tr>';
						html += '<tr><td>SentTS</td><td>' + nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/Mailing/SentTS') + '</td></tr>';
						html += '<tr><td>TotalOpens</td><td>' + nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/Mailing/TotalOpens') + '</td></tr>';
						html += '<tr><td>TotalClickstreams</td><td>' + nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/Mailing/TotalClickstreams') + '</td></tr>';
						html += '<tr><td>TotalClicks</td><td>' + nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/Mailing/TotalClicks') + '</td></tr>';
						html += '<tr><td>TotalConversions</td><td>' + nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/Mailing/TotalConversions') + '</td></tr>';
						html += '<tr><td>TotalAttachments</td><td>' + nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/Mailing/TotalAttachments') + '</td></tr>';
						html += '<tr><td>TotalForwards</td><td>' + nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/Mailing/TotalForwards') + '</td></tr>';
						html += '<tr><td>TotalMediaPlays</td><td>' + nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/Mailing/TotalMediaPlays') + '</td></tr>';
						html += '<tr><td>TotalBounces</td><td>' + nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/Mailing/TotalBounces') + '</td></tr>';
						html += '<tr><td>TotalOptOuts</td><td>' + nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/Mailing/TotalOptOuts') + '</td></tr>';
						html += '</table>';
						response.write(html);
					}
					else{
						response.write('No Mailings');
					}
				}
				else{
					response.write('Connection Error');
				}

				var logout = clgx_sp_contact_logout (strURL);
			}
			else{
				response.write('Connection Error');
			}
		}

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}




function clgx_sp_contact_login (username,password) {
	
	var strPost = clgx_sp_login (username,password);
	var objHead = clgx_sp_header (strPost.length);
	var requestURL = nlapiRequestURL(strLogURL,strPost,objHead);
	var responseXML = nlapiStringToXML(requestURL.getBody());
	var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
	
	if(success){
		return nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SESSIONID');
	}
	else{
		return '';
	}
}


function clgx_sp_contact_logout (strURL) {
	
	var strPost =  clgx_sp_logout ();
	var objHead = clgx_sp_header (strPost.length);
	var requestURL = nlapiRequestURL(strURL,strPost,objHead);
	var strBody = requestURL.getBody();
	var responseXML = nlapiStringToXML(strBody);
	return nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
}




function clgx_sp_contact_delete (strURL,objContact) {
	
	// ====== Build Parameters list =================================
	var objParams = new Array();
	objParams["listid"] = '3325006';
	objParams["email"] = '';
	// ====== Build Columns Fields Array =================================
	var arrColumns = new Array();
	arrColumns.push(JSON.parse('{"name": "Company Internal ID", "value": "' + objContact.customerid + '"}'));
	arrColumns.push(JSON.parse('{"name": "NS Internal ID", "value": "' + objContact.contactid + '"}'));
	
	var strPost = clgx_sp_recipient_delete (objParams, arrColumns);

	var objHead = clgx_sp_header (strPost.length);
	var requestURL = nlapiRequestURL(strURL,strPost,objHead);
	var strBody = requestURL.getBody();
	var responseXML = nlapiStringToXML(strBody);

	
	
	var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
	
	if(success == 'TRUE'){
		return 1;
	}
	else{
		return 0;
	}
}