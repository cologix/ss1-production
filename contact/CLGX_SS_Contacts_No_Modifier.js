nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Contacts_No_Modifier.js
//	Script Name:	CLGX_SS_Contacts_No_Modifier
//	Script Id:		customscript_clgx_ss_contacts_no_modif
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		02/15/2013
//-------------------------------------------------------------------------------------------------

function scheduled_contacts_no_modif(){
	try {

		nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------STARTED---------------------|');
        
        var context = nlapiGetContext();
        var initialTime = moment();
        
        var searchResults = nlapiSearchRecord('contact', 'customsearch_clgx_cp_contacts_no_modif');

        for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
        	var startExecTime = moment();
        	var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);
        	if ( context.getRemainingUsage() <= 100 || totalMinutes > 50 || i > 990 ){
                    var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                    if ( status == 'QUEUED' ) {
                 	   nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                        break; 
                    }
            }
        	
        	var contactid = parseInt(searchResults[i].getValue('internalid',null,null));
        	var firstname = searchResults[i].getValue('firstname',null,null) || '';
        	var lastname = searchResults[i].getValue('lastname',null,null) || '';
        	var language = searchResults[i].getValue('language','customerPrimary',null) || 'en_US';
        	
        	var modifierid = clgx_cp_modifier(contactid, firstname, lastname, language);
			
			var usageConsumtion = 10000 - parseInt(nlapiGetContext().getRemainingUsage());
        	nlapiLogExecution('DEBUG', 'Value', '| ContactID = ' + contactid + ' ModifierID = ' + modifierid + ' | ' + i + '/' + searchResults.length +  ' | Usage = '+ usageConsumtion + ' |');
        }

		
		nlapiLogExecution('DEBUG','User Event - Capex Report','|--------------------FINISHED---------------------|');  
		
	} 
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}

function clgx_cp_modifier (contactId,fname,ltname,language) {
	
    if (language == 'en_US') {
        var lan = 1;
    }
    if (language == 'fr_CA') {
        var lan = 17;
    }
    var modifierID='';
    var nameModifier = fname.charAt(0).toLowerCase() + (ltname.replace(/\s+/g, '')).toLowerCase();
    nameModifier = nameModifier.replace(/\W+/g, '');
    nameModifier = nameModifier.replace(/[0-9]/g, '');
    
    if (nameModifier.length > 1) {
    	
        var rec = nlapiCreateRecord('customrecord_clgx_modifier');
        rec.setFieldValue('name', nameModifier);
        rec.setFieldValue('custrecord_clgx_modifier_language', lan);
        var id = nlapiSubmitRecord(rec, true);
        
        var recordModifier = nlapiLoadRecord('customrecord_clgx_modifier', id);
        var nameMod = recordModifier.getFieldValue('name');
        recordModifier.setFieldValue('name', nameMod + id);
        nlapiSubmitRecord(recordModifier, false, true);
        
        var recordcontact = nlapiLoadRecord('contact', contactId);
        recordcontact.setFieldValue('custentity_clgx_modifier', id);
        nlapiSubmitRecord(recordcontact, false, false);
        
        modifierID=id;
    }
    return modifierID;
}