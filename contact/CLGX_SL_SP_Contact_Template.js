nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_SP_Contact_Template.js
//	Script Name:	CLGX_SL_SP_Contact_Template
//	Script Id:		customscript_clgx_sl_sp_contact_template
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		05/14/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=495&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_sp_contact_template (request, response){
	try {

		var templateid = request.getParameter('templateid');
		
		if(templateid == 0 || templateid == '' || templateid == null){
			
			response.write('Please choose a template from the left panel.');
		}
		else{
			
// ============================ Open Session ===============================================================================================
	    	
	    	var strPost = clgx_sp_login (username,password);
	    	var objHead = clgx_sp_header (strPost.length);
			var requestURL = nlapiRequestURL(strLogURL,strPost,objHead);
			var responseXML = nlapiStringToXML(requestURL.getBody());
			var login = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
			
			if(login == 'true'){
				
				var jsessionid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SESSIONID');
				strURL += jsessionid;
	
// ======================================= Mailing/Template Preview ===================================================================================================
				
				// ====== Build Parameters list =================================
				var objParams = new Object();
				objParams["mailingid"] = templateid;
	
				var strPost = clgx_sp_mailing_preview (objParams);
		    	//response.write(strPost);
				//response.write('<br />================================================<br /><br />');
		    	var objHead = clgx_sp_header (strPost.length);
				var requestURL = nlapiRequestURL(strURL,strPost,objHead);
				var strBody = requestURL.getBody();
				var responseXML = nlapiStringToXML(strBody);
	
				var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
				if(success == 'TRUE'){
					
					var htmlBody = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/HTMLBody');
					var textBody = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/TextBody');
					var aolBody = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/AOLBody');
					var spamScore = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SpamScore');
					
					response.write(htmlBody);

					
				}
				else{
					response.write('error');

				}
				
// ============================ Close Session ===============================================================================================
				
				var strPost =  clgx_sp_logout ();
				var objHead = clgx_sp_header (strPost.length);
				var requestURL = nlapiRequestURL(strURL,strPost,objHead);
				var strBody = requestURL.getBody();
				var responseXML = nlapiStringToXML(strBody);
				var logout = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
			}
		}

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}




function clgx_sp_contact_login (username,password) {
	
	var strPost = clgx_sp_login (username,password);
	var objHead = clgx_sp_header (strPost.length);
	var requestURL = nlapiRequestURL(strLogURL,strPost,objHead);
	var responseXML = nlapiStringToXML(requestURL.getBody());
	var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
	
	if(success){
		return nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SESSIONID');
	}
	else{
		return '';
	}
}


function clgx_sp_contact_logout (strURL) {
	
	var strPost =  clgx_sp_logout ();
	var objHead = clgx_sp_header (strPost.length);
	var requestURL = nlapiRequestURL(strURL,strPost,objHead);
	var strBody = requestURL.getBody();
	var responseXML = nlapiStringToXML(strBody);
	return nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
}




function clgx_sp_contact_delete (strURL,objContact) {
	
	// ====== Build Parameters list =================================
	var objParams = new Array();
	objParams["listid"] = '3325006';
	objParams["email"] = '';
	// ====== Build Columns Fields Array =================================
	var arrColumns = new Array();
	arrColumns.push(JSON.parse('{"name": "Company Internal ID", "value": "' + objContact.customerid + '"}'));
	arrColumns.push(JSON.parse('{"name": "NS Internal ID", "value": "' + objContact.contactid + '"}'));
	
	var strPost = clgx_sp_recipient_delete (objParams, arrColumns);

	var objHead = clgx_sp_header (strPost.length);
	var requestURL = nlapiRequestURL(strURL,strPost,objHead);
	var strBody = requestURL.getBody();
	var responseXML = nlapiStringToXML(strBody);

	
	
	var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
	
	if(success == 'TRUE'){
		return 1;
	}
	else{
		return 0;
	}
}