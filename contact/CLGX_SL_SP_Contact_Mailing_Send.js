nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_SP_Contact_Mailing_Send.js
//	Script Name:	CLGX_SL_SP_Contact_Mailing_Send
//	Script Id:		customscript_clgx_sl_sp_contact_send
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		05/12/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=496&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_sp_contact_mail_send (request, response){
	try {
		
		var email = request.getParameter('email');
		var mailingid = request.getParameter('mailingid');

// ============================ Open Session ===============================================================================================
    	
    	var strPost = clgx_sp_login (username,password);
    	var objHead = clgx_sp_header (strPost.length);
		var requestURL = nlapiRequestURL(strLogURL,strPost,objHead);
		var responseXML = nlapiStringToXML(requestURL.getBody());
		var login = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
		
		if(login == 'true'){
			
			var jsessionid = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SESSIONID');
			strURL += jsessionid;
			
// ============================ Send mailing ===============================================================================================

			var objParams = new Object();
			objParams["mailingid"] = mailingid;
			objParams["email"] = email;
			
			var strPost = clgx_sp_recipient_mailing_send (objParams);
	    	var objHead = clgx_sp_header (strPost.length);
			var requestURL = nlapiRequestURL(strURL,strPost,objHead);
			var strBody = requestURL.getBody();
			var responseXML = nlapiStringToXML(strBody);

			var success = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
			if(success == 'TRUE'){
				response.write('The mailing was sent to the contact.');
			}
			else{
				var strError = nlapiSelectValue(responseXML, '/Envelope/Body/Fault/FaultString');
				//response.write('There was a problem sending the mailing. Please contact support.');
				response.write(strError);
			}

// ============================ Close Session ===============================================================================================
			 
	    	var strPost =  clgx_sp_logout ();
			var objHead = clgx_sp_header (strPost.length);
			var requestURL = nlapiRequestURL(strURL,strPost,objHead);
			var strBody = requestURL.getBody();
			var responseXML = nlapiStringToXML(strBody);
			var logout = nlapiSelectValue(responseXML, '/Envelope/Body/RESULT/SUCCESS');
		}
		else{
			response.write('There was a problem logging to SilverPOP. Please contact support.');
		}
		
	}
	
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}