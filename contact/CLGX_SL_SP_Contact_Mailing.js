nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_SP_Contact_Mailing.js
//	Script Name:	CLGX_SL_SP_Contact_Mailing
//	Script Id:		customscript_clgx_sl_sp_contact_mail
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		05/12/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=494&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_sp_contact_mail (request, response){
	try {

		var contactid = request.getParameter('contactid');
		var mailingid = request.getParameter('mailingid');
		var templateid = request.getParameter('templateid');
		
		if(contactid == 0 || contactid == '' || contactid == null){
			response.write('Please choose a template from the left panel.');
		}
		else{
			var email = nlapiLookupField('contact', contactid, 'email');
			if(email == '' || email == null){
				response.write("You can't send a message to this contact because there is no email.");
			}
			else{
				var objFile = nlapiLoadFile(2542195);
				var html = objFile.getValue();
				html = html.replace(new RegExp('{mailingid}','g'), mailingid);
				html = html.replace(new RegExp('{templateid}','g'), templateid);
				html = html.replace(new RegExp('{email}','g'), email);
				response.write(html);
			}
		}
	}
	
	catch (error) {
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	}
}