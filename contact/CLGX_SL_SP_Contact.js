nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_SP_Contact.js
//	Script Name:	CLGX_SL_SP_Contact
//	Script Id:		customscript_clgx_sl_sp_contact
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		05/12/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=482&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_sp_contact (request, response){
	try {

		var contactid = request.getParameter('contactid');
		
		//var strTemplates = nlapiLoadFile(2547763);
		
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_sp_template',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_sp_templateid',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_sp_mailingid',null,null));
		var arrFilters = new Array();
		var searchTemplates = nlapiSearchRecord('customrecord_clgx_sp_mailing_ids', null, arrFilters, arrColumns);
		var arrTemplates = new Array();
		for ( var i = 0; searchTemplates != null && i < searchTemplates.length; i++ ) {
	    	var objTemplate = new Object();
	    	objTemplate["mailing"] = searchTemplates[i].getValue('custrecord_clgx_sp_template',null,null);
	    	objTemplate["templateid"] = parseInt(searchTemplates[i].getValue('custrecord_clgx_sp_templateid',null,null));
	    	objTemplate["mailingid"] = parseInt(searchTemplates[i].getValue('custrecord_clgx_sp_mailingid',null,null));
	    	arrTemplates.push(objTemplate);
		}
		
		var strPrograms = nlapiLoadFile(2547762);
		
		var objFile = nlapiLoadFile(2525499);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{contactid}','g'), contactid);
		//html = html.replace(new RegExp('{mailingsJSON}','g'), strTemplates.getValue());
		html = html.replace(new RegExp('{mailingsJSON}','g'), JSON.stringify(arrTemplates));
		html = html.replace(new RegExp('{programsJSON}','g'), strPrograms.getValue());
		
		response.write(html);
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
