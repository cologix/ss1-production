nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_CP_Contact_Rights.js
//	Script Name:	CLGX_SL_CP_Contact_Rights
//	Script Id:		customscript_clgx_sl_cp_contact_rights
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/09/2016
//	URL:			/app/site/hosting/scriptlet.nl?script=695&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_cp_contact_rights (request, response){
    try {

        var id = request.getParameter('id') || 0;
        var act = request.getParameter('act') || 'edit';

        if(act == 'update'){

            var rights = get_min_rights();

            rights.users = parseInt(request.getParameter('users')) || 0;
            nlapiLogExecution('DEBUG','rights.users', rights.users);
            rights.security = parseInt(request.getParameter('security')) || 0;
            rights.cases = parseInt(request.getParameter('cases')) || 0;
            rights.casesfin = parseInt(request.getParameter('casesfin')) || 0;
            rights.reports = parseInt(request.getParameter('reports')) || 0;
            rights.visits = parseInt(request.getParameter('visits')) || 0;
            rights.shipments = parseInt(request.getParameter('shipments')) || 0;
            rights.orders = parseInt(request.getParameter('orders')) || 0;
            rights.invoices = parseInt(request.getParameter('invoices')) || 0;
            rights.ccards = parseInt(request.getParameter('ccards')) || 0;
            rights.colocation = parseInt(request.getParameter('colocation')) || 0;
            rights.network = parseInt(request.getParameter('network')) || 0;
            rights.domains = parseInt(request.getParameter('domains')) || 0;
            rights.managed = parseInt(request.getParameter('managed')) || 0;
            var rigths1=JSON.stringify(rights);
            nlapiLogExecution('DEBUG','rights1.users', rigths1);
            nlapiSubmitField('contact', id, 'custentity_clgx_cp_user_rights_json', JSON.stringify(rights));

        }
        nlapiLogExecution('DEBUG','id', parseInt(id));
        var rec = nlapiLoadRecord('contact', parseInt(id));
        var rights = JSON.parse(rec.getFieldValue('custentity_clgx_cp_user_rights_json')) || get_min_rights();

        var role = nlapiGetRole();

        if (role == '-5' || role == '3' || role == '18' || role == '1039' || role == '1054' || role=='1011'){
            //if (role == '-5' || role == '3' || role == '18'){
            var ro = 'false';
        } else {
            var ro = 'true';
        }
        var user = {
            "id": id,
            "rights":rights
        };
        var objFile = nlapiLoadFile(3921536);
        var html = objFile.getValue();
        html = html.replace(new RegExp('{userData}','g'), JSON.stringify(user));
        html = html.replace(new RegExp('{ro}','g'), ro);
        html = html.replace(new RegExp('{cid}','g'), id);

        response.write( html );

    }
    catch (error) {
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}


function get_min_rights() {
    return {
        "users":0,
        "security":0,
        "cases":0,
        "casesfin":0,
        "reports":0,
        "visits":0,
        "shipments":0,
        "orders":0,
        "invoices":0,
        "ccards":0,
        "colocation":0,
        "network":0,
        "domains":0,
        "managed":0,
        "shipments":0
    };
}