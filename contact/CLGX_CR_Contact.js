nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_CR_Contact.js
//	Script Name:	CLGX_CR_Contact
//	Script Id:		customscript_clgx_cr_contact
//	Script Runs:	Client
//	Script Type:	Client Script
//	Deployments:	Contact
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		07/13/2012
//-------------------------------------------------------------------------------------------------

function fieldChanged(type, name, linenum){
	try {
//------------- Begin Section 1 -------------------------------------------------------------------
// Version:	1.0 - 12/23/2011
// Details:	Make email mandatory when role is billing
//-------------------------------------------------------------------------------------------------
		
		if (name == 'contactrole'){
			var roleID = nlapiGetFieldValue('contactrole');
			if (roleID == '1'){
				nlapiSetFieldValue('custentity_clgx_is_billing_contact','T');
			}
			else{
				nlapiSetFieldValue('custentity_clgx_is_billing_contact','F');
			}
		}
		
		if (name == 'custentity_clgx_contact_reset_password'){
			
		nlapiSetFieldValue('custentity_clgx_portal_password',generatePassword(8,false));
		}

//------------- End Section 1 -------------------------------------------------------------------	

	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function generatePassword(length, special) {
	  var iteration = 0;
	  var password = "";
	  var randomNumber;
	  if(special == undefined){
	      var special = false;
	  }
	  while(iteration < length){
	    randomNumber = (Math.floor((Math.random() * 100)) % 94) + 33;
	    if(!special){
	      if ((randomNumber >=33) && (randomNumber <=47)) { continue; }
	      if ((randomNumber >=58) && (randomNumber <=64)) { continue; }
	      if ((randomNumber >=91) && (randomNumber <=96)) { continue; }
	      if ((randomNumber >=123) && (randomNumber <=126)) { continue; }
	    }
	    iteration++;
	    password += String.fromCharCode(randomNumber);
	  }
	  return password;
}