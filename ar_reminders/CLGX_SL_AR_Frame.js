nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_AR_Frame.js
//	Script Name:	CLGX_SL_AR_Frame
//	Script Id:		customscript_clgx_sl_ar_frame
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Derek Hitchman - derek@erpadvisorsgroup.com
//	Created:		03/24/2015, Live 06/23/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=505&deploy=1
//     Description:       This Suitelet is the top level Suitelet for the AR Reminders Report which is viewed by the user. It renders the outermost frame which the user views. It automatically embeds the CLGX_SL_AR_Locations.js Suitelet in an iframe as the next level down which displays a list of locations which the user can select. 
//-------------------------------------------------------------------------------------------------

function suitelet_ar_frame (request, response){
	try {
		var formFrame = nlapiCreateForm('Customer AR Reminders');
		var fieldFrame = formFrame.addField('custpage_clgx_frame_html','inlinehtml', null, null, null);
		var scriptField = formFrame.addField('custpage_clgx_scripts','inlinehtml', null, null, null);
		var frameHTML = '<iframe name="ar1" id="ar1" src="/app/site/hosting/scriptlet.nl?script=504&deploy=1" height="600px" width="1235px" frameborder="0" scrolling="no"></iframe>';
		fieldFrame.setDefaultValue(frameHTML);
		
		var scripts ="<script>jQuery( document ).ready(function() {\
						jQuery('input#send-now-btn').each(function () { this.style.setProperty( 'background-color', '#fbb', 'important' ); }); \
						jQuery('#tbl_send-now-btn').each(function () { \
			 					this.style.setProperty( 'position', 'absolute' );\
			 					this.style.setProperty( 'left', '20px' );\
			 					this.style.setProperty( 'top','780px' );\
								});\
						});</script>";
		scriptField.setDefaultValue(scripts);
		
		  var sendRemindersJqAlert = "\
		  		var addList = '';\
		  		jQuery('#ar1').contents().find('#ar2').contents().find('.customerselect:checked').each(function () {\
		  			customerid = jQuery( this ).data('customerid') ;\
			  		jQuery('#ar1').contents().find('#ar2').contents().find('.cinvoicelink-'+customerid).each(function () {\
			  			addList += jQuery( this ).data('cinvoiceid') + ';';\
			  		});\
			  	});\
		  		var remList = '';\
		  		jQuery('#ar1').contents().find('#ar2').contents().find('.customerselect:not(:checked)').each(function () {\
		  			customerid = jQuery( this ).data('customerid') ;\
			  		jQuery('#ar1').contents().find('#ar2').contents().find('.cinvoicelink-'+customerid).each(function () {\
			  			remList += jQuery( this ).data('cinvoiceid') + ';';\
			  		});\
			  	});\
		  		alert(addList);\
		  		alert(remList);\
		  		";
		  
		  //  jQuery('#ar1').contents().find('#ar2').each(function() { this.contentWindow.location.reload(true); });\
		  
		  
		  var sendRemindersJq = "\
		  		if(!confirm('Are you sure you want to scheduled selected reminders?')) {\
			  		return;\
					}\
			  		var addList = '';\
			  		jQuery('#ar1').contents().find('#ar2').contents().find('.customerselect:checked').each(function () {\
			  			customerid = jQuery( this ).data('customerid') ;\
				  		jQuery('#ar1').contents().find('#ar2').contents().find('.cinvoicelink-'+customerid).each(function () {\
				  			addList += jQuery( this ).data('cinvoiceid') + ';';\
				  		});\
				  	});\
			  		var remList = '';\
			  		jQuery('#ar1').contents().find('#ar2').contents().find('.customerselect:not(:checked)').each(function () {\
			  			customerid = jQuery( this ).data('customerid') ;\
				  		jQuery('#ar1').contents().find('#ar2').contents().find('.cinvoicelink-'+customerid).each(function () {\
				  			remList += jQuery( this ).data('cinvoiceid') + ';';\
				  		});\
				  	});\
			  	jQuery.ajax({\
					url: '/app/site/hosting/scriptlet.nl?script=507&deploy=1&ciadd='+addList+'&cirem='+remList\
					})\
			  .done(function( ) {\
				  alert( 'Completed' );\
				  });\
		  		";

		  var sendNowJq = "\
		  		if(!confirm('Are you sure you want to send scheduled reminders now?')) {\
			  		return;\
					}\
			  	jQuery.ajax({\
					url: '/app/site/hosting/scriptlet.nl?script=509&deploy=1'\
					})\
			  .done(function(data ) {\
			  	  if(data=='QUEUED')  alert( 'Reminders successfully added to queue. Sending now...');\
			  		else alert( 'An internal error occured while processing this request: '+data);\
				  });\
		  		";
		  
		  

		  var  selectAll= "\
		  		jQuery('#ar1').contents().find('#ar2').contents().find('.customerselect').each(function () {\
		  			jQuery( this ).prop('checked',true) + ',';\
		  		});\
		  		";
		  

		  var removeAll = "\
		  		jQuery('#ar1').contents().find('#ar2').contents().find('.customerselect').each(function () {\
		  			jQuery( this ).prop('checked',false) + ',';\
		  		});\
		  		";


		  formFrame.addButton('select-all-btn', 'Select All', selectAll);
		  formFrame.addButton('unselect-all-btn', 'Un-select All', removeAll);
		  formFrame.addButton('send-reminder-btn', 'Save Selections', sendRemindersJq);
		  formFrame.addButton('send-now-btn', 'SEND EMAILS NOW', sendNowJq);

			response.writePage( formFrame  );

		  
		} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}