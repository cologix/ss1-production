/* 	
*   scs.js 
*	Author: Derek Hitchman  derek@erpadvisorsgroup.com
*	Helper class for managing data objects and getting around system limits. 
*/

var SCS = SCS || {};



/*
 * Runs an nlapiCreateSearch working around the 1000 row limit
 */
SCS.Query = function(type, filters, columns, callback){
	
  	var savedsearch = nlapiCreateSearch( type, filters, columns );
	var resultset = savedsearch.runSearch();	
	var resultslice = Array();
	var ix = 0;
	do { 
		var recordset = resultset.getResults( ix, ix+1000 );
	    for (var record in recordset) {
	    	callback(ix, record, recordset);
	    	ix++;
	    }	
	    
	} while (recordset.length >= 1000);
	
};



/*
 * Holds a list of search result objects
 */
SCS.List = function () {
		
		this.__records = new Object();
		
		//add a new record, pass record object
		this.addRecord = function(pk, rec){
	
			this.__records[pk]= new SCS.Record(rec);
			
		};
		
		//check if record exists
		this.hasRecord = function(pk) {
			return this.__records.hasOwnProperty(pk);
		};
		
		//get record
		this.getRecord = function(pk) {
			return this.__records[pk];
		};
		
		//get actual record data object
		this.getRecordObject = function(pk) {
			return this.__records[pk].getObject();
		};
		
		//set meta data on record
		this.setRecordMeta = function(pk, fieldname, value) {
			return this.__records[pk].setMeta(fieldname, value);
		};
		
		//retrieve meta data on a record
		this.getRecordMeta = function(pk,fieldname) {
			return this.__records[pk].getMeta(fieldname);
		}
		
		//return the list of records as a js object
		this.getRecords = function() {
			return this.__records;
		};
		
		//return the total number of records
		this.count = function() {
		    var size = 0;
		    for (rec in this.__records) {
		        if (this.__records.hasOwnProperty(rec)) size++;
		    }
		    return size;
		};
	
		//return the list of record primary keys
		this.getPkList = function() {
			    var pks = new Array();
			    for (rec in this.__records) {
			    	pks.push(rec);
			    }
			    return pks;
		};
		
	};
		

/*
 * Holds and individual search result object and allows assignment of related records
 */
SCS.Record=  function (rec) {

		this.__record = rec;
		
		this.__meta = new Object();
		
		this.__related = new Object();
		
		//retrieve the data object for this record
		this.getObject = function() {
			return this.__record;
		};		

		//set a meta field value for this record
		this.setMeta = function(fieldname, value) {
			this.__meta[fieldname] = value;
		};
		
		//retrieve a meta field value for this record
		this.getMeta = function(fieldname) {
			if(this.__meta.hasOwnProperty(fieldname))
				return this.__meta[fieldname];
			else return null;
		};

		//add a related list
		this.setRelation = function(relname, list ){

				this.__related[relname] = list;
			
		};
		
		
		//add a related list & record to this record, pass a name for the relationship, the pk and the record
		this.addRelated = function(relname, pk, rec ){

			if(!this.__related.hasOwnProperty(relname)){
				this.__related[relname] = new SCS.List();
			}
			
			this.__related[relname].addRecord(pk,rec);
		};
		
		//return the count of related records in a related list
		this.countRelated = function(relname) {
		    var size = 0;
		    if (this.__related.hasOwnProperty(relname)){
			    for (rec in this.__related[relname]) {
			        if (this.__related[relname].hasOwnProperty(rec)) size++;
			    }
			}
		    return size;
		};

		//return a related list as a js object with the pk as the property name
		this.getRelation = function(relname) {
			if(!this.__related.hasOwnProperty(relname)) return new SCS.List();
			else return this.__related[relname];
		};

		//return a related list as a js array in arbitrary sequence
		this.getRelatedArray = function(relname) {
			    var rel = new Array();
				if(this.__related.hasOwnProperty(relname)) {
				    for (rec in this.__related[relname]) {
				    	rel.push( this.__related[relname][rec]);
				    }
				}
			    return pks;
		};
		
		//return the pk values of a related list as a js array
		this.getRelatedPkList = function(relname) {

			if(!this.__related.hasOwnProperty(relname)) return new Array();
			else return this.__related[relname].getPkList();
		};
	
	};
	
	