nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_AR_Invoice_Viewer.js
//	Script Name:	CLGX_SL_AR_Invoice_Viewer
//	Script Id:		customscript_clgx_sl_ar_inv_viewer
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Derek Hitchman - derek@erpadvisorsgroup.com
//	Created:		03/24/2015, Live 06/23/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=506&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_ar_view_inv (request, response){
	try {
		
		try {
	    	var objFile = nlapiLoadFile(request.getParameter('fileid'));
	    	batch='';
	    	try{
	    	batch = request.getParameter('batch');
	    	} catch (e){}
			var filename = objFile.getName();
			var src = objFile.getURL();	 
			var formFrame = nlapiCreateForm('Consolidated Invoice: '+batch);       
			var fieldFrame = formFrame.addField('custpage_clgx_frame_html','inlinehtml', null, null, null);		
			var frameHTML = '<iframe name="inv1" id="inv" src="'+src+'" height="700px" width="800px" frameborder="0" scrolling="yes"></iframe>';
			fieldFrame.setDefaultValue(frameHTML);			
			response.writePage( formFrame );
        } 
        catch (e) {
    		var formFrame = nlapiCreateForm('Consolidated Invoice');
        	response.writePage( 'Unable to load invoice.' );
        }
        


		  
		} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}