nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_AR_Send_Reminders.js
//	Script Name:	CLGX_SS_AR_Send_Reminders
//	Script Id:		customscript_clgx_ss_ar_send_reminders
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	@authors:		Derek Hitchman - derek@erpadvisorsgroup.com
//	Created:		03/24/2015
//     Description:       This scheduled script actually handles sending out the AR Reminders. There content and users to be merged into the emails can be set at the beginning of the script. The script finds all of the records flagged to receive a reminder and then creates merged emails for each of them and sends them out.


//-------------------------------------------------------------------------------------------------
function send_ar_reminders(){
    try{

        //initialize variables
        var initialTime = moment();
        var context = nlapiGetContext();




        //Set Assigned Employees for cases
        var usAssignee = nlapiLoadRecord('employee', 713181) ; //Chad Cummins
        //var usAssignee = nlapiLoadRecord('employee', 12073) ; //Kristen Schultz
        // var canadaAssignee = nlapiLoadRecord('employee', 377363) ; //Danny Pappas
        // var canadaAssignee = nlapiLoadRecord('employee', 142355) ; //Fatim Sissoko


        //Email subject and body, per language: default language option MUST be defined
        var emailSubject = new Object();
        var emailBody = new Object();

        //emailSubject['default'] = "Case {casenumber}: Past Due Invoice";
        /*emailBody['default'] ="\n\
Hello,\n\
In reviewing your account, our records indicate that invoice number {invoicenumbers} has not been paid and is past due. If payment has been made, your diligence is appreciated. Please provide me with details of the payment so I may track the payment on my end. If payment has not been made, please provide me with a payment date. I have attached a copy of the past due invoice for your convenience. If you have any questions, please reply directly to this email. \n\
\n\
\n\
Thank you,\n\
Cologix Billing\n\
billing@cologix.com\n\
";

        emailSubject['fr_CA'] = "Case {casenumber}: Facture en souffrance";
        emailBody['fr_CA'] ="Bonjour,\n\
Votre compte indique que la facture numéro {invoicenumbers} n’a pas été payée et est en souffrance. Si le paiement a été effectué nous vous remercions.\n\
S’il vous plaît envoyez-nous les détails du paiement pour le vérifier de notre côté.\n\
Si le paiement n’a pas été effectué s’il vous plaît donnez-nous une date de paiement. Veuillez trouvez ci-joint une copie de votre facture en souffrance. Si vous avez des questions veuillez répondre directement à ce courriel. \n\
\n\
Merci\n\
Cologix Billing\n\
billing@cologix.com\n\
";*/
        
        emailSubject['en_US'] = "Case {casenumber}: Notice of Past Due Invoice";
        emailBody['en_US'] = "<div style=\"text-align: center;\"><b><u>NOTICE OF PAST DUE PAYMENT</u></b></div><br /><br /><b><i>VIA E-MAIL DELIVERY</i></b><br /><br /><b>{companyname}</b><br /><br /><b>RE: PAST DUE PAYMENT</b><br /><br />";
        emailBody['en_US'] += "Invoice Number(s): {invoicenumbers}<br /><br /><br />Attn: Accounting Department<br /><br />";
        emailBody['en_US'] += "We are writing to notify you that as of {todaysdate}, you have a past due balance of ${totaldue}. Please submit payment for the past due amount immediately and reply directly to this email to notify us when it has been sent.<br /><br />";
        emailBody['en_US'] += "If payment has been made, thank you for your diligence.  Please reply directly to this message and provide payment details so we may track the payment from our end.<br /><br />"; 
        emailBody['en_US'] += "Copies of the invoices referenced above are attached. Please reply directly to this email if you have questions.<br /><br />";
        emailBody['en_US'] += "Please note that a continuing failure by you to pay all past due amounts set forth herein will entitle us to exercise all of our rights and remedies set forth in your master services agreement.<br /><br /><br />";
        emailBody['en_US'] += "Sincerely,<br />{fromname}<br /><br />COLOGIX, INC.<br />Billing and Collections Administration<br />billing@cologix.com<br />1-855-492-4557<br />";

        

        emailSubject['fr_CA'] = "Case {casenumber}: Notification de Non-Paiement de Votre Facture";
        emailBody['fr_CA'] = "<div style=\"text-align: center;\"><b><u>NOTIFICATION DE NON-PAIEMENT DE VOTRE FACTURE</u></b></div><br /><br /><b><i>LIVRAISON PAR COURRIEL</i></b><br /><br /><b>{companyname}</b><br /><br /><b>Objet: PAIEMENT EN SOUFFRANCE</b><br /><br />";
        emailBody['fr_CA'] += "Attn: D&#xE9;partement de la comptabilit&#xE9;<br /><br /><br />N &#xB0; de facture: {invoicenumbers}<br /><br />";
        emailBody['fr_CA'] += "Nous vous &#xE9;crivons pour vous informer qu&#x27;&#xE0; partir de {todaysdate}, vous avez un solde en souffrance de ${totaldue}. S&#x27;il vous pla&#xEE;t soumettre le paiement pour le montant en souffrance imm&#xE9;diatement et r&#xE9;pondre directement &#xE0; ce courriel pour nous informer quand le paiement a &#xE9;t&#xE9; envoy&#xE9;.<br /><br />";
        emailBody['fr_CA'] += "Si le paiement a &#xE9;t&#xE9; effectu&#xE9;, merci de votre diligence. S&#x27;il vous pla&#xEE;t r&#xE9;pondez directement &#xE0; ce message et fournir les d&#xE9;tails de paiement afin que nous puissions suivre le paiement de notre fin.<br /><br />";
        emailBody['fr_CA'] += "Des copies des factures r&#xE9;f&#xE9;renc&#xE9;es ci-dessus sont jointes. Veuillez r&#xE9;pondre directement &#xE0; ce courriel si vous avez des questions.<br /><br />";
        emailBody['fr_CA'] += "Veuillez noter que si vous ne payez pas tous les montants dus, nous aurons le droit d&#x27;exercer tous nos droits et recours pr&#xE9;vus dans votre contrat de services.<br /><br /><br />";
        emailBody['fr_CA'] += "Cordialement,<br />{fromname}<br /><br />COLOGIX, INC.<br />Administration de la facturation et des recouvrements<br />billing@cologix.com<br />1-855-492-4557";


        //Get list of CIs with ar reminder checkbox flagged
        var customers = new SCS.List(); //full list of customers
        var arrColumns = new Array();
        var arrFilters=new Array();
        var parentCustomerIds = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_pdf_file_id',null,null));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_batch',null,null));
        arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_balance',null,null));
        arrColumns.push(new nlobjSearchColumn('language','custrecord_clgx_consol_inv_customer',null));
        arrColumns.push(new nlobjSearchColumn('internalid','custrecord_clgx_consol_inv_customer',null));
        arrColumns.push(new nlobjSearchColumn('subsidiary','custrecord_clgx_consol_inv_customer',null));
        arrColumns.push(new nlobjSearchColumn('parent','custrecord_clgx_consol_inv_customer',null));
        arrColumns.push(new nlobjSearchColumn('custentity_clgx_collection_cust','custrecord_clgx_consol_inv_customer',null));
        arrFilters.push(new nlobjSearchFilter('custrecord_clgx_consol_inv_ar_reminder',null,'is','T'));
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_processed",null,"is",'T'));
        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_output",null,"anyof",1));



        SCS.Query('customrecord_clgx_consolidated_invoices', arrFilters, arrColumns , function(ix, rec, recordset){

            var customerId = recordset[rec].getValue('internalid','custrecord_clgx_consol_inv_customer',null);


            if(!customers.hasRecord(customerId)){
                customers.addRecord( customerId , null );
                customers.setRecordMeta(customerId, "parent", recordset[rec].getValue('parent','custrecord_clgx_consol_inv_customer',null));
                customers.setRecordMeta(customerId, "subsidiary", recordset[rec].getValue('subsidiary','custrecord_clgx_consol_inv_customer',null));
                var collectionId = recordset[rec].getValue('custentity_clgx_collection_cust','custrecord_clgx_consol_inv_customer',null);
                if(collectionId!=null && collectionId!='')
                {
                    customers.setRecordMeta(customerId, "collectionId", collectionId);
                }
                else{
                    customers.setRecordMeta(customerId, "collectionId", 0);
                }
                parentCustomerIds.push(recordset[rec].getValue('parent','custrecord_clgx_consol_inv_customer',null));
            }


            customers.getRecord(customerId).addRelated("consolidated_invoices", recordset[rec].getValue('internalid',null,null), recordset[rec]);


        });






        //Exit if no customers
        if(customers.count() ==0  ){
            nlapiLogExecution('DEBUG','End', status + ' / Finished script - no records to process.');
            return null;

        }


        //Pull parent customers and add to customer list
        var arrColumns = new Array();
        var arrFilters=new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        arrColumns.push(new nlobjSearchColumn('subsidiary',null,null));
        arrColumns.push(new nlobjSearchColumn('parent',null,null));
        arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof',parentCustomerIds));

        SCS.Query('customer', arrFilters, arrColumns , function(ix, rec, recordset){

            var customerId = recordset[rec].getValue('internalid',null,null);
            var collectionId = recordset[rec].getValue('custentity_clgx_collection_cust',null,null);
            if(!customers.hasRecord(customerId)){
                customers.addRecord( customerId , null );
                customers.setRecordMeta(customerId, "parent", recordset[rec].getValue('parent',null,null));
                customers.setRecordMeta(customerId, "subsidiary", recordset[rec].getValue('subsidiary',null,null));

            }


        });





        //Get the list of billing contacts and attach to customer object
        var arrColumns = new Array();
        var arrFilters=new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
        arrColumns.push(new nlobjSearchColumn('email',null,null));
        arrColumns.push(new nlobjSearchColumn('internalid','company',null));
        arrFilters.push(new nlobjSearchFilter('contactrole',null,'anyof',['1','7']));  //1=Billing Contact, 7=Billing - No Invoice
        arrFilters.push(new nlobjSearchFilter('internalid','company','anyof',customers.getPkList()));
        arrFilters.push(new nlobjSearchFilter('isinactive',null,'is', 'F'));
        arrFilters.push(new nlobjSearchFilter('email',null,'isnotempty', null));


        SCS.Query('contact', arrFilters, arrColumns , function(ix, rec, recordset){

            var companyId = recordset[rec].getValue('internalid', 'company', null);
            var contactId = recordset[rec].getValue('internalid', null, null);
            customers.getRecord( companyId).addRelated("billing_contacts", contactId, recordset[rec]);

        });





        //for each customer create a billing customer record either the parent or its self
        var billingCustomerContacts = new SCS.List();  //will be the customer or its parent if applicable
        var cnt=0;
        for (var c in customers.getRecords()) {


            //skip if has no consolidated invoices on self
            if(customers.getRecord(c).countRelated("consolidated_invoices") ==0) {
                continue;
            }



            //June 2015 Logic never creates case under parent
            //by default bill to customer, blank email bill to
            var billcustomerid = c;
            var billcontactid = null;

            //if there is exactly one billing contact send email to the billing contact
            var billingContactCount = customers.getRecord(c).countRelated("billing_contacts");
            if ( billingContactCount==1 ) {
                billcontactid = customers.getRecord(c).getRelatedArray("billing_contacts")[0];
            }




            /*
             // Old Logic Removed  - Removed 9 June 2015

             var billingContactCount = customers.getRecord(c).countRelated("billing_contacts");
             var customerParent = customers.getRecordMeta(c,'parent');
             var parentBillingContactCount = customers.getRecord(customerParent).countRelated("billing_contacts");

             //  -------------------   Case / Contact logic -------------------  //
             //by default bill to customer, blank email bill to
             var billcustomerid = c;
             var billcontactid = null;


             //if there is exactly one billing contact send email to the billing contact
             if ( billingContactCount==1 ) {

             billcontactid = customers.getRecord(c).getRelatedArray("billing_contacts")[0];


             //if ALSO has parent and parent has one contact
             if (customerParent   && parentBillingContactCount == 1) {

             // AND is the same contact, bill parent instead
             if(billcontactid == customers.getRecord(customerParent).getRelatedArray("billing_contacts")[0] ) {
             billcustomerid = customerParent;
             }
             }
             }
             */



            /*
             // --------------Really Old logic-------------
             //if more than one contact then set bill to customer to its self, forget parents
             if(billingContactCount >1){

             var billcustomerid = c;
             var billcontactid = null;
             }
             //if has no parent and has no contact
             else if (!customerParent && billingContactCount==0) {

             var billcustomerid =c;
             var billcontactid = null;
             }
             //if has no parent and has one contact
             else if (!customerParent && billingContactCount==1) {

             var billcustomerid = c;
             var billcontactid = customers.getRecord(c).getRelatedArray("billing_contacts")[0];
             }
             //if has parent and has no contact
             else if (customerParent && billingContactCount==0) {

             var billcustomerid =c;
             var billcontactid = null;
             }
             //if has parent and has one contact
             else if (customerParent && billingContactCount==1 ) {

             var billcustomerid = customerParent;
             var billcontactid = customers.getRecord(c).getRelatedArray("billing_contacts")[0];
             }
             */





            //billkey becomes a unique key field in the format billcustomerid-billcontactid
            var billkey = billcustomerid + ((billcontactid)? ('-'+billcontactid) : (''));


            //create an empty record for billing customer if there is not one
            if(!billingCustomerContacts.hasRecord(billkey)){
                billingCustomerContacts.addRecord( billkey , null );
                billingCustomerContacts.setRecordMeta(billkey,'billcustomerid', billcustomerid); //add customer id to be billed
                billingCustomerContacts.setRecordMeta(billkey,'billcontactid', billcontactid); //add contact id to be billed
                billingCustomerContacts.getRecord( billkey).setRelation("billing_contacts", customers.getRecord(c).getRelation("billing_contacts"));

            }


            var cis  =  customers.getRecord(c).getRelation("consolidated_invoices").getRecords();

            for ( var ci in cis ) {
                billingCustomerContacts.getRecord(billkey).addRelated("consolidated_invoices", ci, cis[ci].getObject() );
            }



        }




        //for each customer record to create case, send email and remove reminder flag
        var cnt=0;
        var cnti=0;
       	var emailCount = 0;
        var emailString = '';
        var emailString10 = '';
        for (var b in billingCustomerContacts.getRecords()) {


            //Handle execution limits
            var startExecTime = moment();
            var totalMinutes = (startExecTime.diff(initialTime)/60000).toFixed(1);
            if ( (context.getRemainingUsage() <= 300 || totalMinutes > 50) && (cnt+1) < customers.count()  ){
                var status = nlapiScheduleScript(context.getScriptId(), context.getDeploymentId());
                //var status = 'QUEUED';
                if ( status == 'QUEUED' ) {
                    nlapiLogExecution('DEBUG','End', status + ' / Finished scheduled script due to usage or time limit and re-schedule it.');
                    break;
                }
            }


            try{

                billcustomerid = billingCustomerContacts.getRecordMeta(b, 'billcustomerid');
                billcontactid = billingCustomerContacts.getRecordMeta(b, 'billcontactid');



                //Create new case and populate fields
                var subsidiary =  customers.getRecordMeta(billcustomerid, 'subsidiary');
                var collectionId= customers.getRecordMeta(billcustomerid, 'collectionId');
                var caserecord = nlapiCreateRecord('supportcase');
                caserecord.setFieldValue('stage', 'OPEN');
                caserecord.setFieldValue('status', 2);
                caserecord.setFieldValue('origin', 4);
                caserecord.setFieldValue('profile', 22);
                caserecord.setFieldValue('category', 11); // 11="Finance Helpdesk"
                caserecord.setFieldValue('custevent_cologix_sub_case_type', 53);
                caserecord.setFieldValue('title', 'Past Due Notice');
                caserecord.setFieldValue('custevent_cologix_facility', 16);


                //  if(subsidiary == 6) var assignee = canadaAssignee;
                //else if(subsidiary == 5)
                var assignee = usAssignee;
                if(collectionId!=0)
                {
                    var assigned=collectionId;
                }
                else
                {
                    var assigned=assignee.getId();
                }
                //  nlapiSendEmail(206211,206211,'assignee',assigned,null,null,null,null);
                caserecord.setFieldValue('assigned',  assigned);


                //this will be either the child or parent company
                caserecord.setFieldValue('company', billcustomerid);


                //Get email list to send to
                var emailList = billingCustomerContacts.getRecord(b).getRelation("billing_contacts").getRecords();
                emailString='';
                for (var e in emailList) {
                    if(emailString!='') emailString +=', ';
                    emailString +=emailList[e].getObject().getValue('email',null,null);
                }
                caserecord.setFieldValue('email',emailString);

                if (emailString=='') continue; //skip if no emails for this one


                //If there is a single billing contact set it ias the contact on the case
                if(customers.getRecord(billcustomerid).countRelated("billing_contacts") ==1){
                    caserecord.setFieldValue('contact', customers.getRecord(billcustomerid).getRelatedArray("billing_contacts")[0]);
                }

                //Commit new case to database
                var caseid = nlapiSubmitRecord(caserecord, true);
                var newCase = nlapiLoadRecord('supportcase',caseid);


                //Loop through consolidated invoices to make a list of batch numbers and PDF files
                var cis =  billingCustomerContacts.getRecord(b).getRelation("consolidated_invoices").getRecords();
                var pdfFiles = new Array();
                var batchNbrs = new Array();
                var totalDue = 0.00;
                
                for (var ci in cis) {
                    // pdfFiles.push(   nlapiLoadFile(cis[ci].getObject().getValue('custrecord_clgx_consol_inv_pdf_file_id',null,null)  )   );
                    pdfFiles.push(   nlapiLoadFile(parseInt(cis[ci].getObject().getValue('custrecord_clgx_consol_inv_pdf_file_id',null,null)))   );
                    batchNbrs.push(   cis[ci].getObject().getValue('custrecord_clgx_consol_inv_batch',null,null)   );
                    totalDue += parseFloat(cis[ci].getObject().getValue('custrecord_clgx_consol_inv_balance',null,null));
                }


                //Merge records to email template and send email
                var language = cis[ci].getObject().getValue ('language','custrecord_clgx_consol_inv_customer',null);


                //Define mege fields
                var mergeFields = new Object();


                if(language=='fr_CA')  mergeFields['invoicenumbers'] = batchNbrs.join(', ');
                else mergeFields['invoicenumbers'] = batchNbrs.join(', ');

                var assignedObject = nlapiLoadRecord("employee", assigned);
                var companyObject  = nlapiLoadRecord("customer", billcustomerid);
                
                var today = new Date();
                mergeFields['todaysdate'] = moment().format('MM.DD.YYYY');
                mergeFields['fromname'] = assignedObject.getFieldValue('firstname') + " " + assignedObject.getFieldValue('lastname');
                mergeFields['casenumber'] = newCase.getFieldValue('casenumber');
                mergeFields['totaldue'] = totalDue.toFixed(2);
                mergeFields['companyname'] = companyObject.getFieldValue("companyname");

                if(emailSubject.hasOwnProperty(language))  var emailSubjectMerged = emailSubject[language];
                else var emailSubjectMerged = emailSubject['en_US'];

                if(emailBody.hasOwnProperty(language))  var emailBodyMerged = emailBody[language];
                else var emailBodyMerged = emailBody['en_US'];

                for (var field in mergeFields){
                    emailSubjectMerged =  emailSubjectMerged.replace("{"+field+"}", mergeFields[field]);
                    emailBodyMerged = emailBodyMerged.replace("{"+field+"}",  mergeFields[field]);
                }
                var records = new Object(); //add the new case as associated with the email
                records['activity'] = caseid;
               // nlapiSendEmail(assigned, emailString, emailSubjectMerged, emailBodyMerged, null, null, records, pdfFiles,true);
             	emailString10 = '';
                emailCount = 0;
                for (var e in emailList) {
                    if(emailString10!=''){
                    	emailString10 +=', ';
                    }
                    emailString10 +=emailList[e].getObject().getValue('email',null,null);
                    emailCount++;
                    if (emailCount > 9){
                    	nlapiSendEmail(assigned, emailString10, emailSubjectMerged, emailBodyMerged, null, null, records, pdfFiles,true);
                    	emailCount = 0;
                    	emailString10 = '';
                    }
                }
                if (emailCount > 0){
                	nlapiSendEmail(assigned, emailString10, emailSubjectMerged, emailBodyMerged, null, null, records, pdfFiles,true);
                }
         



                //clear the ar reminder checkbox
                invList = billingCustomerContacts.getRecord(b).getRelatedPkList("consolidated_invoices");
                for ( var i = 0; invList != null && i < invList.length; i++ ) {
                    nlapiSubmitField('customrecord_clgx_consolidated_invoices', invList[i], 'custrecord_clgx_consol_inv_ar_reminder', 'F', 'F');
                    cnti++;
                }

                cnt++;

                //catch errors for a single customer iteration
            } catch (error){
                if (error.getDetails != undefined){
                    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
                    throw error;
                }
                else{
                    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
                    throw nlapiCreateError('99999', error.toString());
                }
            }

        }  //end loop each bill to customer






    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }

}