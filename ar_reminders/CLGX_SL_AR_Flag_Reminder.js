nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_AR_Flag_Reminder.js
//	Script Name:	CLGX_SL_AR_Flag_Reminder
//	Script Id:		customscript_clgx_sl_ar_flag_reminder
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Derek Hitchman - derek@erpadvisorsgroup.com
//	Created:		03/24/2015, Live 06/23/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=507&deploy=1
//     Description:       This script can be called via AJAX from the AR Reminders report page to flag any number of consolidated invoices as requiring a reminder to be sent. The flag is set on the custrecord_clgx_consol_inv_ar_reminder field. The list of consolidated invoices are passed in as a semicolon separated list. 
//-------------------------------------------------------------------------------------------------

function suitelet_ar_flag_reminder(request, response){
	try {
	var updated = 0;
	var html='';
	
	if(request.getMethod() == 'GET') {
		
		if(request.getParameter('ciadd') != null) {
			var ciadd = request.getParameter('ciadd').split( ";" );
			for ( var i = 0; ciadd != null && i < ciadd.length; i++ ) {
				var id = ciadd[i];
				if( !isNaN(parseFloat(id)) && isFinite(id)){
					html+='ID: '+id+'<br>';
					nlapiSubmitField('customrecord_clgx_consolidated_invoices', id, 'custrecord_clgx_consol_inv_ar_reminder', 'T', true);
					updated++;
					
				}
			}
		}
			

		if(request.getParameter('cirem') != null) {
			var cirem = request.getParameter('cirem').split( ";" );
			for ( var i = 0; cirem != null && i < cirem.length; i++ ) {
				var id = cirem[i];
				if( !isNaN(parseFloat(id)) && isFinite(id)){
					html+='ID: '+id+'<br>';
					nlapiSubmitField('customrecord_clgx_consolidated_invoices', id, 'custrecord_clgx_consol_inv_ar_reminder', 'F', true);
					updated++;
					
				}
			}
		}
			
			
			
		
	}
	html+=updated +' records updated.' 
	response.write( html);
	}
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
