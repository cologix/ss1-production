nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_AR_CInvoice.js
//	Script Name:	CLGX_SL_AR_CInvoice
//	Script Id:		customscript_clgx_sl_ar_cinvoice
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Derek Hitchman - derek@erpadvisorsgroup.com
//	Created:		03/24/2015, Live 06/23/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=503&deploy=1
//     Description:       This Suitelet is the 3rd level down Suitelet for the AR Reminders report. It is embedded as an iframe within the CLGX_SL_AR_Locations.js Suitelet. It generates the actual list of invoices to be viewed. It pulls the data on invoices, customers, consolidated invoices and any other relevant information and forms a JSON data object which is then rendered to screen within the CLGX_HTML_AR_CInvoice.html page. Invoices which are selected or unselected are flagged with an AJAX call to the CLGX_SL_AR_Flag_Reminder.js script and the send reminders button runs the CLGX_SL_AR_Send_Reminders_Now.js script.
//-------------------------------------------------------------------------------------------------

function suitelet_ar_cinvoices (request, response){
    try {

        var locationName = decodeURIComponent(request.getParameter('location_name'));


        if(locationName) {
            var objFile = nlapiLoadFile(2739153);
            var html = objFile.getValue();
            html = html.replace(new RegExp('{locationName}','g'),locationName);
            var locationJson = getInvoicesJSON(locationName);
            if(locationJson)
                html =  html.replace(new RegExp('{invoicesJSON}','g'), locationJson);
            else html = 'No results found in the location "'+locationName+'".';
        }
        else{
            var html = 'Please select a location from the left panel.';
        }
        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error 0', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function getInvoicesJSON(locationName){

    var objTree = new Object();
    objTree["text"] = '.';


    var currencyMultiplier = new Object();
    currencyMultiplier[1] = 1.0;  //Usd


    // -- Pull CI Data ----------------------------


    var cutOffDate =  new Date();
    cutOffDate.setTime(cutOffDate.getTime()-120 * 86400000);

    var searchInvoices = Array();

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_batch',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_date',null,null));
    arrColumns.push(new nlobjSearchColumn('created',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_year',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_month',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_pdf_file_id',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_currency',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_market',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_subtotal',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_tax_total',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_tax2_total',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_total',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_invoices',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_ar_reminder',null,null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_emailed_to',null,null));
    arrColumns.push(new nlobjSearchColumn('internalid','custrecord_clgx_consol_inv_customer',null));
    arrColumns.push(new nlobjSearchColumn('parent','custrecord_clgx_consol_inv_customer',null));
    arrColumns.push(new nlobjSearchColumn('terms','custrecord_clgx_consol_inv_customer',null));
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_customer',null,null));



    var c1 = new nlobjSearchColumn('companyname','custrecord_clgx_consol_inv_customer',null);
    arrColumns.push(c1);
    c1.setSort();

    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_date",null,"after",cutOffDate));
    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_market",null,"is",locationName));
    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_processed",null,"is",'T'));
    arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_output",null,"anyof",1));
    arrFilters.push(new nlobjSearchFilter("amountremainingisabovezero","custrecord_clgx_consol_inv_invs","is","T"));

    var savedsearch = nlapiCreateSearch( 'customrecord_clgx_consolidated_invoices', arrFilters, arrColumns );

    var resultset = savedsearch.runSearch();



    var searchid = 0;
    do {
        var resultslice = resultset.getResults( searchid, searchid+1000 );
        for (var rs in resultslice) {
            searchInvoices.push( resultslice[rs] );
            searchid++;
        }

    } while (resultslice.length >= 1000);


    if(searchInvoices.length ==0) return null;


    try{
        var allTInvoiceIds = Array();
        var allCustomers = Array();
        var allParents = Array();
        var allConsIDs = Array();
        var parentChildCustomer = Object();
        //Loop through first time to pull individual invoice IDs and customers for filters later
        for ( var i = 0; searchInvoices != null && i < searchInvoices.length; i++ ) {

            var searchInvoice = searchInvoices[i];
            var parentId = searchInvoice.getValue('parent','custrecord_clgx_consol_inv_customer', null) ;
            var customerId = searchInvoice.getValue('internalid','custrecord_clgx_consol_inv_customer', null) ;

            allTInvoiceIds = allTInvoiceIds.concat(  searchInvoice.getValue('custrecord_clgx_consol_inv_invoices', null, null).split( ";" )  ) ;
            allCustomers = allCustomers.concat ( customerId);
            allCustomers = allCustomers.concat ( parentId);
            allConsIDs = allConsIDs.concat(  searchInvoice.getValue('internalid', null, null));

            if(!parentChildCustomer.hasOwnProperty(parentId)){
                parentChildCustomer[parentId] = new Array();
            }

            if(!parentChildCustomer[ parentId ].indexOf(customerId) > -1){
                parentChildCustomer[ parentId ].push(customerId);
            }

        }
    } catch (error) {
        nlapiLogExecution('ERROR','Unexpected Error 1', error.toString());
        throw nlapiCreateError('99999', error.toString());
    }

    // -- End Pull CI Data -------------------------




    // -- Pull Supportcase Data ------------------------

    var searchCases = Array();


    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    arrColumns.push(new nlobjSearchColumn('internalid','company',null));
    arrColumns.push(new nlobjSearchColumn('custevent_cologix_sub_case_type',null,null));

    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("internalid",'company',"anyof", allCustomers.concat(allParents)));
    arrFilters.push(new nlobjSearchFilter("stage",null,"is",'OPEN'));
    arrFilters.push(new nlobjSearchFilter("category",null,"is", "Finance Helpdesk" ));
    arrFilters.push(new nlobjSearchFilter("custevent_cologix_sub_case_type",null,"anyof", [53,58,55] ));  //"Collections","Payment Plan","Dispute"

    var savedsearch = nlapiCreateSearch( 'supportcase', arrFilters, arrColumns );

    var resultset = savedsearch.runSearch();
    var resultslice = Array();
    var searchid = 0;
    do {
        var resultslice = resultset.getResults( searchid, searchid+1000 );
        for (var rs in resultslice) {
            searchCases.push( resultslice[rs] );
            searchid++;
        }

    } while (resultslice.length >= 1000);

    // -- End Pull Supportcase Data --------------------







    // -- Pull Credit Memos Data ------------------------

    var creditMemos = Array();

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid','customer',null));
    arrColumns.push(new nlobjSearchColumn('amountRemaining',null,null));
    arrColumns.push(new nlobjSearchColumn('fxamount',null,null));
    arrColumns.push(new nlobjSearchColumn('amount',null,null));

    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("internalid",'customer',"anyof", allCustomers));
    arrFilters.push(new nlobjSearchFilter("amountRemaining",null,"greaterthan",'0'));

    var savedsearch = nlapiCreateSearch( 'creditmemo', arrFilters, arrColumns );

    var resultset = savedsearch.runSearch();
    var resultslice = Array();
    var searchid = 0;
    do {
        var resultslice = resultset.getResults( searchid, searchid+1000 );
        for (var rs in resultslice) {
            creditMemos.push( resultslice[rs] );
            searchid++;
        }

    } while (resultslice.length >= 1000);

    // -- End Pull Credit Memos Data --------------------



    // -- Pull Terms Data ------------------------

    var terms = Array();

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,null));
    arrColumns.push(new nlobjSearchColumn('daysuntilnetdue',null,null));
    arrColumns.push(new nlobjSearchColumn('name',null,null));


    var savedsearch = nlapiCreateSearch( 'term', null, arrColumns );

    var resultset = savedsearch.runSearch();
    var resultslice = Array();
    var searchid = 0;
    do {
        var resultslice = resultset.getResults( searchid, searchid+1000 );
        for (var rs in resultslice) {
            terms.push( resultslice[rs] );
            searchid++;
        }

    } while (resultslice.length >= 1000);

    var termsMap = new Object();
    for ( var k = 0; terms != null && k < terms.length; k++ ) {
        termsMap[  terms[k].getValue('internalid', null, null) ] = terms[k];
    }

    // -- End Pull Terms Data --------------------




    // -- Pull Invoice Data ------------------------

    var searchTInvoices = Array();
    var allInvoiceIds = Array();


    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('trandate',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('trandate',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('currency',null,'GROUP'));
    arrColumns.push(new nlobjSearchColumn('fxamount',null,'SUM'));
    arrColumns.push(new nlobjSearchColumn('amountPaid',null,'SUM'));
    arrColumns.push(new nlobjSearchColumn('amountRemaining',null,'SUM'));



    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("custbody_consolidate_inv_cinvoice",null,"anyof", allConsIDs ));
    arrFilters.push(new nlobjSearchFilter("mainline",null,"is",'T'));

    var savedsearch = nlapiCreateSearch( 'invoice', arrFilters, arrColumns );

    var resultset = savedsearch.runSearch();
    var resultslice = Array();
    var searchid = 0;
    do {
        var resultslice = resultset.getResults( searchid, searchid+1000 );
        for (var rs in resultslice) {
            searchTInvoices.push( resultslice[rs] );
            allInvoiceIds.push(resultslice[rs].getValue('internalid', null, 'GROUP'));
            searchid++;
        }

    } while (resultslice.length >= 1000);


    try{
        var invoiceMap = new Object();
        for ( var k = 0; searchTInvoices != null && k < searchTInvoices.length; k++ ) {
            invoiceMap[  searchTInvoices[k].getValue('internalid', null, 'GROUP') ] = searchTInvoices[k];
        }

    } catch (error) {
        nlapiLogExecution('ERROR','Unexpected Error 2', error.toString());
        throw nlapiCreateError('99999', error.toString());
    }




    // -- End Pull Invoice Data --------------------


    nlapiSendEmail(206211, 206211,'here1','--  End Pull Invoice Data',null,null,null,null); // Send email to Dan

    // -- Pull Paid Amounts Data ------------------------

    var paidAmounts = new Object();

    var arrColumns = new Array();
    // arrColumns.push(new nlobjSearchColumn('appliedtotransaction',null,'GROUP'));
    // arrColumns.push(new nlobjSearchColumn('APPLIEDTOFOREIGNAMOUNT',null,'SUM'));

    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("custbody_consolidate_inv_cinvoice","paidtransaction","anyof", allConsIDs));
    //arrFilters.push(new nlobjSearchFilter("type",null,"is", 'customerpayment'));
    arrFilters.push(new nlobjSearchFilter("mainline",null,"is", 'F'));

    //  var savedsearch = nlapiCreateSearch( 'customerpayment', arrFilters, arrColumns );

    var savedsearch = nlapiLoadSearch( 'customerpayment', 'customsearch_clgx_ss_cust_payment_ar');
    savedsearch .addFilters(arrFilters);
    var resultset = savedsearch.runSearch();
    var resultslice = Array();
    var searchid = 0;
    do {
        var resultslice = resultset.getResults( searchid, searchid+1000 );
        for (var rs in resultslice) {
            var columns = resultslice[rs].getAllColumns();
            var inv=resultslice[rs].getValue(columns[0]);
            var amt = resultslice[rs].getValue(columns[1]);
            //   var inv = resultslice[rs].getValue('appliedtotransaction', null, 'GROUP');
            //   var amt = resultslice[rs].getValue('APPLIEDTOFOREIGNAMOUNT', null, 'SUM');
            if(paidAmounts.hasOwnProperty(inv)){
                paidAmounts[inv] += amt;
            }
            else {
                paidAmounts[inv] = amt;
            }
            searchid++;
        }

    } while (resultslice.length >= 1000);

    // -- End Pull Paid Amounts  Data --------------------


    nlapiSendEmail(206211, 206211,'here','-- End Pull Paid Amounts  Data',null,null,null,null); // Send email to Dan


    // -- Process the data


    var customerMap = new Object();

    var arrCustomers = new Array();

    objTree["children"]= new Array();

    for ( var k = 0; searchInvoices != null && k < searchInvoices.length; k++ ) {
        var searchInvoice = searchInvoices[k];



        var fileid = searchInvoice.getValue('custrecord_clgx_consol_inv_pdf_file_id', null, null);
        var filename = '';
        var fileurl = '';


        //var arrCInvoices = new Array();
        var customerId = parseInt(searchInvoice.getValue('internalid', 'custrecord_clgx_consol_inv_customer', null));




        //If not on customer object yet, then add it
        if(!customerMap[ customerId ] ){

            var companyName = searchInvoice.getText('custrecord_clgx_consol_inv_customer', null, null);
            var objCustomer = new Object();
            objCustomer["nodeid"] = customerId;
            objCustomer["node"] = companyName;
            objCustomer["total"] = 0.00;
            objCustomer["paid"] = 0.00;
            objCustomer["remaining"] = "0.00";
            objCustomer["is_dispute"] = false;
            objCustomer["is_payment_plan"] = false;
            objCustomer["is_collections"] = false;
            objCustomer["expanded"] = true;
            objCustomer["iconCls"] = 'account';
            objCustomer["total_od"] = "$0.00";

            objCustomer["selected"] = 'F';
            objCustomer["leaf"] = false;
            objCustomer["credit"] = 0.00;

            customerMap[ customerId ] = objCustomer;
            customerMap[ customerId ]["children"] = new Array();
        }

        //If this CI invoice is flagged to send reminder, mark customer selected
        if(searchInvoice.getValue('custrecord_clgx_consol_inv_ar_reminder', null, null)=='T') objCustomer["selected"] = 'T';



        var objCInvoice = new Object();
        objCInvoice["nodeid"] = parseInt(fileid);
        objCInvoice["node"] = searchInvoice.getValue('custrecord_clgx_consol_inv_batch', null, null);
        objCInvoice["date"] = searchInvoice.getValue('created', null, null);
        objCInvoice["cyear"] = searchInvoice.getValue('custrecord_clgx_consol_inv_year', null, null);
        objCInvoice["cmonth"] = searchInvoice.getValue('custrecord_clgx_consol_inv_month', null, null);
        objCInvoice["fileid"] = parseInt(fileid);
        objCInvoice["file"] = filename;
        objCInvoice["fileurl"] = fileurl;
        objCInvoice["customerid"] = customerId;
        objCInvoice["cinvoiceid"] = searchInvoice.getValue('internalid', null, null);;
        objCInvoice["paid"] = "$0.00";
        objCInvoice["remaining"] = "0.00";
        objCInvoice["credit"] = "$0.00";
        objCInvoice["total_od"] = "$0.00";
        objCInvoice["currency"] = searchInvoice.getValue('custrecord_clgx_consol_inv_currency', null, null);
        objCInvoice["template"] = searchInvoice.getValue('custrecord_clgx_consol_inv_market', null, null);
        objCInvoice["subtotal"] = '$' + searchInvoice.getValue('custrecord_clgx_consol_inv_subtotal', null, null);
        objCInvoice["tax1"] = '$' + searchInvoice.getValue('custrecord_clgx_consol_inv_tax_total', null, null);
        objCInvoice["tax2"] = '$' + searchInvoice.getText('custrecord_clgx_consol_inv_tax2_total', null, null);
        objCInvoice["total"] = '$' + searchInvoice.getValue('custrecord_clgx_consol_inv_total', null, null);
        objCInvoice["expanded"] = true;
        objCInvoice["iconCls"] = 'cinvoice';
        objCInvoice["leaf"] = false;

        var arrInvoiceChildren = new Array();

        var objInvoiceChild = new Object();
        objInvoiceChild["nodeid"] = 2;
        objInvoiceChild["node"] = 'Included Invoices';
        objInvoiceChild["expanded"] = false;
        objInvoiceChild["iconCls"] = 'invoices';
        objInvoiceChild["leaf"] = false;




        var arrListInvoices='';
        if(searchInvoice.getValue('custrecord_clgx_consol_inv_invoices', null, null))
            arrListInvoices = searchInvoice.getValue('custrecord_clgx_consol_inv_invoices', null, null).split( ";" );
        var arrInvoices = new Array();

        for ( var l = 0; arrListInvoices != null  && l < arrListInvoices.length; l++ ) {

            if(parseInt(arrListInvoices[l]) > 0){
                var invoiceid = arrListInvoices[l];
                var invoiceNbr = '['+arrListInvoices[l]+']';

                //If the invoice actaully exists
                if(invoiceMap[ arrListInvoices[l] ]) {
                    invoiceNbr =invoiceMap[ arrListInvoices[l] ].getValue('tranid', null, 'GROUP');

                    var paid = (paidAmounts.hasOwnProperty(invoiceid)) ? paidAmounts[invoiceid]  :  0.00;
                    var total = invoiceMap[ arrListInvoices[l] ].getValue('fxamount', null, 'SUM');
                    var remaining = parseFloat(total) - parseFloat(paid);
                    if(invoiceMap[ arrListInvoices[l] ].getValue('amountRemaining', null, 'SUM')  == 0  ) remaining = 0;


                    var objInvoice = new Object();
                    objInvoice["nodeid"] = parseInt(invoiceid);
                    objInvoice["node"] = invoiceNbr;
                    objInvoice["iconCls"] = 'invoice';
                    objInvoice["total"] = '$' + total;
                    objInvoice["paid"] = '$' + (parseFloat(paid)).toFixed(2);
                    objInvoice["remaining"] = '$' + (parseFloat(remaining) ).toFixed(2);
                    objInvoice["leaf"] = true;
                    objInvoice["credit"] = "$0.00";
                    objInvoice["total_od"] = "$0.00";


                    //Calculate overdue total
                    var todayDate = new Date();
                    var termsDays = parseInt( termsMap[searchInvoice.getValue('terms','custrecord_clgx_consol_inv_customer',null)].getValue("daysuntilnetdue"));
                    if(!termsDays) termsDays = 0;



                    if( parseInt(invoiceid) == 653364)  nlapiLogExecution('ERROR','Innvoice 653364 Terms Days =' ,termsDays);

                    var invDate =  new Date(  invoiceMap[ arrListInvoices[l] ].getValue('trandate', null, 'GROUP')) ;


                    if( parseInt(invoiceid) == 653364)  nlapiLogExecution('ERROR','Innvoice 653364 Invoice Date =' ,invDate);

                    var odDate = new Date();
                    odDate.setTime(invDate.getTime()+termsDays * 86400000);

                    var ignoreDate = new Date();
                    ignoreDate.setTime(odDate.getTime()+30 * 86400000);



                    objInvoice["date"] = invDate;
                    objInvoice["date_due"] = odDate;
                    objInvoice["date_ignore"] = ignoreDate;

                    if( parseInt(invoiceid) == 653364)  nlapiLogExecution('ERROR','Innvoice 653364 invDate =' ,invDate);
                    if( parseInt(invoiceid) == 653364)  nlapiLogExecution('ERROR','Innvoice 653364 odDate=' ,odDate);
                    if( parseInt(invoiceid) == 653364)  nlapiLogExecution('ERROR','Innvoice 653364 ignoreDate =' ,ignoreDate);

                    if( parseInt(invoiceid) == 653364)  nlapiLogExecution('ERROR','Innvoice 653364 todayDate =' ,todayDate);

                    // if(todayDate >=odDate && todayDate < ignoreDate ){
                    if(todayDate >=odDate ){
                        objInvoice["total_od"] = objInvoice["remaining"];

                        if( parseInt(invoiceid) == 653364)  nlapiLogExecution('ERROR','Innvoice 653364 overdue?' ,'yes');
                    }
                    else continue; //skip invoice if not overdue



                    arrInvoices.push(objInvoice);

                    //Add paid amount to CI Invoice
                    if(  parseFloat(paid)  > 0 ){
                        objCInvoice["paid"] ='$' + (( parseFloat(  objCInvoice["paid"].toString().replace('$','') )
                            +parseFloat(paid)).toFixed(2));
                    }


                    //Add remaining amount to CI Invoice
                    if(  parseFloat(remaining)  > 0 ){
                        objCInvoice["remaining"] ='$' + (( parseFloat(  objCInvoice["remaining"].toString().replace('$','') )
                            +parseFloat(remaining)).toFixed(2));
                    }


                    //Add overdue amount to CI Invoice
                    if( parseFloat(  objInvoice["total_od"].toString().replace('$','') )  > 0 ){

                        objCInvoice["total_od"] ='$' + ( parseFloat(  objCInvoice["total_od"].toString().replace('$','') )
                            +parseFloat(  objInvoice["total_od"].toString().replace('$','') )).toFixed(2);
                    }



                }
            }


        }



        objInvoiceChild["children"] = arrInvoices;
        arrInvoiceChildren.push(objInvoiceChild);


        objCInvoice["children"] = arrInvoiceChildren;

        //If CI is fully paid then skip loop and dont add paid and remaining to Customer totals
        if(parseFloat(objCInvoice["remaining"].toString().replace('$','')).toFixed(2) ==0) continue;

        //Add paid amount to customer line
        customerMap[ customerId ]["paid"] ='$' + (( parseFloat(  customerMap[ customerId ]["paid"].toString().replace('$','') )
            +parseFloat(objCInvoice["paid"].toString().replace('$',''))).toFixed(2));


        //Add remaining amount to customer line
        customerMap[ customerId ]["remaining"] ='$' + (( parseFloat(  customerMap[ customerId ]["remaining"].toString().replace('$','') )
            +parseFloat(objCInvoice["remaining"].toString().replace('$',''))).toFixed(2));


        //Add CI Total amount to customer total
        customerMap[ customerId ]["total"] ='$' + (( parseFloat(  customerMap[ customerId ]["total"].toString().replace('$','') )
            +parseFloat(objCInvoice["total"].toString().replace('$',''))).toFixed(2));


        //Add CI Total amount to customer total overdue
        customerMap[ customerId ]["total_od"] ='$' + (( parseFloat(  customerMap[ customerId ]["total_od"].toString().replace('$','') )
            +parseFloat(objCInvoice["total_od"].toString().replace('$',''))).toFixed(2));


        customerMap[customerId]["children"].push(objCInvoice);
    }








    //Add searchcase data to customer object
    for ( var k = 0; searchCases != null && k < searchCases.length; k++ ) {

        var companyId =searchCases[k].getValue('internalid','company', null);
        var sub = searchCases[k].getValue('custevent_cologix_sub_case_type',null,null) ;

        if(customerMap.hasOwnProperty(companyId)){

            if( sub==55 ) //Dispute
                customerMap[  companyId ]["is_dispute"] = true;
            else if( sub== 58 )//Payment Plan
                customerMap[  companyId ]["is_payment_plan"] = true;
            else if( sub== 53 ) //Collections
                customerMap[  companyId ]["is_collections"] = true;
        }

        /*
         //If the customer with a case has child customers
         if(parentChildCustomer.hasOwnProperty(companyId) ){
         for ( var c = 0; parentChildCustomer[companyId] != null && c < parentChildCustomer[companyId].length; c++ ) {

         childCompanyId = parentChildCustomer[companyId][c];
         if( sub==55 ) //Dispute
         customerMap[  childCompanyId ]["is_dispute"] = true;
         else if( sub== 58 )//Payment Plan
         customerMap[  childCompanyId ]["is_payment_plan"] = true;
         else if( sub== 53 ) //Collections
         customerMap[  childCompanyId ]["is_collections"] = true;

         }
         }
         */

    }





    //Add Credit Memo unapplied amounts data to customer object
    var newCredit = 0.00;
    for ( var m = 0; creditMemos != null && m < creditMemos.length; m++ ) {

        var custId = creditMemos[m].getValue('internalid','customer', null) ;

        var unapplied =
            parseFloat(creditMemos[m].getValue('fxamount',null, null).toString().replace('-','').replace('$','') )
            / parseFloat(creditMemos[m].getValue('amount',null, null).toString().replace('-','').replace('$','') )
            * parseFloat(creditMemos[m].getValue('amountRemaining',null, null).toString().replace('-','').replace('$','') )	;


        var currentCredit = parseFloat(  customerMap[ custId ]["credit"].toString().replace('-','').replace('$','') );


        newCredit= +  parseFloat(unapplied ) +parseFloat(currentCredit);


        customerMap[ custId ]["credit"] = '$' + newCredit.toFixed(2);
    }



    for (var cu in customerMap) {
        //Remove customer if it is a collections or zero amt remaining
        if(
            customerMap[cu]["is_collections"] ==true
            || parseFloat(  customerMap[ cu ]["remaining"].toString().replace('$','') ) ==0
        ) continue;

        objTree["children"].push(customerMap[cu]);
    }



    return JSON.stringify(objTree);
}



//check if value is in the array
function inArray(val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}