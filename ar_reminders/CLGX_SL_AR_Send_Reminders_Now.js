nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_AR_Send_Reminders_Now.js
//	Script Name:	CLGX_SL_AR_Send_Reminders_Now
//	Script Id:		customscript_clgx_sl_ar_send_now
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Derek Hitchman - derek@erpadvisorsgroup.com
//	Created:		03/24/2015, Live 06/23/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=509&deploy=1
//     Description:       This script can be called via AJAX from the AR Reminders report page to immediately run the "send reminders" scheduled script, CLGX_SS_AR_Send_Reminders.js
//-------------------------------------------------------------------------------------------------


function suitelet_ar_send_reminders_now(request, response){
	try {
	var html='';
	
	var status = nlapiScheduleScript('customscript_clgx_ss_ar_send_reminders', 'customdeploy_clgx_ss_ar_send_reminders');
	html+=status;
	response.write( html);
	}
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
			var  err= error.getCode() + ': ' + error.getDetails();
		    nlapiLogExecution('ERROR','Process Error',err);
			html+=status;
			response.write( err);
		    //throw error;
		}
		else{
			var err =  'Unable to schedule script: '+error.toString();
		    nlapiLogExecution('ERROR','Unexpected Error',err);
			html+=status;
			response.write( err);
		    //throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
