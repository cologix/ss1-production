nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_AR_Locations.js
//	Script Name:	CLGX_SL_AR_Locations
//	Script Id:		customscript_clgx_sl_ar_locations
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com, adapted by Derek Hitchman - derek@erpadvisorsgroup.com
//	Created:		03/24/2015, Live 06/23/2015
//	URL:			/app/site/hosting/scriptlet.nl?script=504&deploy=1
//     Description:       This Suitelet is the 2nd level down Suitelet for the AR Reminders report. It is embedded as an iframe within the CLGX_SL_AR_Frame.js Suitelet. It generates the list of locations which can be selected. Once a location is clicked, the selected location Id is passed on to the CLGX_SL_AR_CInvoice.js Suitelet which is opened in another iframe which will render the full list of invoices and customer data. 
//-------------------------------------------------------------------------------------------------

function suitelet_ar_locations (request, response){
	try {
		var objFile = nlapiLoadFile(2739154);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{LocationsJSON}','g'), getLocationsJSON());
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}





function getLocationsJSON(){
	

	var cutOffDate =  new Date();
	cutOffDate.setDate(cutOffDate.getDate()-160);
	
	var searchInvoices = Array();
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_consol_inv_market',null,'GROUP'));
	

	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_date",null,"after",cutOffDate));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_processed",null,"is",'T'));
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_consol_inv_output",null,"anyof",1));
	
	var savedsearch = nlapiCreateSearch( 'customrecord_clgx_consolidated_invoices', arrFilters, arrColumns );

	var resultSetLoc = savedsearch.runSearch();
	


	//var resultSetLoc = searchLocations.runSearch();
	var arrLocations = new Array();
	resultSetLoc.forEachResult(function(searchResult) {
		var colObj = new Object();
		colObj["location_name"] = searchResult.getValue('custrecord_clgx_consol_inv_market', null, 'GROUP');
		arrLocations.push(colObj);
		return true; // return true to keep iterating
		});
	
    return JSON.stringify(arrLocations);
}
	


