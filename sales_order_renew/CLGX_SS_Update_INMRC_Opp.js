nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Update_INMRC_Opp.js
//	Script Name:	CLGX_SS_Update_INMRC_Opp
//	Script Id:		customscript_clgx_ss_updateenmrc_opp
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Service Order
//	@authors:		Catalina Taran
//-------------------------------------------------------------------------------------------------

function scheduled_opp_imrc(){
    try{
//------------- Begin Section 1 -------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');

        var arrColumnsIn = new Array();
        var arrFiltersIn = new Array();
        var searchIncrementalOpps=nlapiSearchRecord('opportunity','customsearch_clgx_ss_pendingrenewalsmrc',arrFiltersIn,arrColumnsIn);
        var oppstr='';
        if(searchIncrementalOpps!=null)
        {
            for ( var i = 0; searchIncrementalOpps != null && i < searchIncrementalOpps.length; i++ ) {
                var searchIncrementalOpp = searchIncrementalOpps[i];
                var columnsIn = searchIncrementalOpp.getAllColumns();
                var renewSOs = searchIncrementalOpp.getValue(columnsIn[2]);
                if(renewSOs.indexOf(",") > -1){
                    var arrRenewSOs=renewSOs.split(",");
                }
                else
                {
                    var arrRenewSOs=renewSOs;
                }
                if((renewSOs!=null)&&(renewSOs!=''))
                {
                    var arrColumns = new Array();
                    // arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_location',null,null).setSort(false));
                    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_total',null,'group'));
                    arrColumns.push(new nlobjSearchColumn('internalid',null,'group'));
                    var arrFilters = new Array();
                    arrFilters.push(new nlobjSearchFilter("internalid","custrecord_clgx_totals_transaction","anyof",arrRenewSOs));
                    //arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_class",null,"anyof",1));
                    var searchTotals = nlapiSearchRecord('customrecord_clgx_totals_transaction', null, arrFilters, arrColumns);

                    if(searchTotals!=null)
                    {
                        var oldIcremental_0=parseFloat(0);
                        for ( var j = 0; searchTotals != null && j < searchTotals.length; j++ ) {
                            var searchTotal = searchTotals[j];
                            oldIcremental_0=parseFloat(oldIcremental_0)+parseFloat(searchTotal.getValue('custrecord_clgx_totals_total',null,'group'));
                        }
                    }
                    var incrementalAmount=parseFloat(searchIncrementalOpp.getValue(columnsIn[3]));
                    var oppID=searchIncrementalOpp.getValue(columnsIn[0]);
                    var incrementalFinal=parseFloat(incrementalAmount)-parseFloat(oldIcremental_0);
                    if(parseFloat(incrementalFinal)>parseFloat(0))
                    {
                        var recOpp=nlapiLoadRecord('opportunity',oppID);
                        recOpp.setFieldValue('custbody_cologix_opp_incremental_mrc', incrementalFinal.toFixed(2));
                        nlapiSubmitRecord(recOpp, false, true);
                        oppstr+='#'+searchIncrementalOpp.getValue(columnsIn[1])+'\n';
                     //   nlapiSendEmail(206211,206211,'Opportunities Updated',oppstr,null,null,null,null); //Send Email to Catalina

                    }
                    else
                    {
                        var recOpp=nlapiLoadRecord('opportunity',oppID);
                        recOpp.setFieldValue('custbody_cologix_opp_incremental_mrc', '');
                        nlapiSubmitRecord(recOpp, false, true);

                    }

                }


            }

            clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_inmrc_admin", "Opportunities Updated", oppstr);
            //nlapiSendEmail(206211,206211,'Opportunities Updated',oppstr,null,null,null,null); //Send Email to Catalina


        }
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }

}


