nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_SO_Renewals_Transactions.js
//	Script Name:	CLGX_RL_SO_Renewals_Transactions
//	Script Id:		customscript_clgx_rl_so_renewals_transact
//	Script Nbr:		264
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		02/11/2014
//	RelativeURL:	/app/site/hosting/restlet.nl?script=264&deploy=1
//-------------------------------------------------------------------------------------------------

function restlet_so_renewals_transactions (datain){
	try {
		
		datain = JSON.parse(datain);
		var customerid = datain['customerid'];
		var custname = nlapiLookupField('customer', customerid, 'entityid');
		
		var objFile = nlapiLoadFile(1019671);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{transactionsJSON}','g'), getTransactionsJSON(customerid));
		html = html.replace(new RegExp('{custName}','g'),custname.replace(new RegExp("'", "g"), "&apos;"));
		return html;
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function getTransactionsJSON(customerid){

	var objTree = new Object();
	objTree["text"] = '.';
	
	var arrSearchOpptys = new Array();
	// look for renewal opportunities for that customer
	var arrColumns = new Array();
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
	var searchOppty = nlapiSearchRecord('transaction', 'customsearch_clgx_so_renewals_opptys', arrFilters, arrColumns);
	
	for ( var i = 0; searchOppty != null && i < searchOppty.length; i++ ) {
		var opptyid = searchOppty[i].getValue('custbody_clgx_so_renewed_on_oppty', null, 'GROUP');
        if(!inArray(opptyid,arrSearchOpptys) && opptyid != null && opptyid != ''){
        	arrSearchOpptys.push(opptyid);
        }
	}

	// look for renewal opportunities for that customer in Processing queue in case it was not processed
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('custrecord_clgx_que_renew_oppty_oppty',null,'GROUP'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("custrecord_clgx_que_renew_oppty_customer",null,"anyof",customerid));
	var searchOpptyQueue = nlapiSearchRecord('customrecord_clgx_que_renew_sos_oppty', null, arrFilters, arrColumns);
	
	for ( var i = 0; searchOpptyQueue != null && i < searchOpptyQueue.length; i++ ) {
		var opptyid = searchOpptyQueue[i].getValue('custrecord_clgx_que_renew_oppty_oppty', null, 'GROUP');
        if(!inArray(opptyid,arrSearchOpptys) && opptyid != null && opptyid != ''){
        	arrSearchOpptys.push(opptyid);
        }
	}
	
	var arrOppty = new Array();
	for ( var m = 0; arrSearchOpptys != null && m < arrSearchOpptys.length; m++ ) {

		var oppty = nlapiLookupField('opportunity', arrSearchOpptys[m], 'tranid');
		
		objOppty = new Object();
		objOppty["entityid"] = arrSearchOpptys[m];
		objOppty["entity"] = 'Opportunity #' + oppty;
		objOppty["expanded"] = true;
		objOppty["iconCls"] = 'oppty';
		objOppty["leaf"] = false;
	
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("opportunity",null,"anyof",arrSearchOpptys[m]));
		var searchProposals = nlapiSearchRecord('estimate', null, arrFilters, arrColumns);
		
		var arrProposals = new Array();
		for ( var i = 0; searchProposals != null && i < searchProposals.length; i++ ) {
			var searchProposal = searchProposals[i];
			var proposalid = searchProposal.getValue('internalid', null, 'GROUP');
			var recProposal = nlapiLoadRecord('estimate', proposalid);
			var nbrSOs = recProposal.getLineItemCount('links');
			
			// check if opportunity has SOs
			var opptyid = recProposal.getFieldValue('opportunity');
			var nbrClosedSOs = 0;
			if(opptyid != null && opptyid != ''){
				var recOppty = nlapiLoadRecord('opportunity', opptyid);
				var nbrOpptySOs = recOppty.getLineItemCount('closed');
			}
			
			
			var bfileurl = '#';
			var bfileid = recProposal.getFieldValue('custbody_clgx_prop_bq_file_id');
			if(bfileid != null && bfileid !=''){
				try {
		        	var objFile = nlapiLoadFile(bfileid);
					var bfileurl = objFile.getURL();
		        } 
		        catch (e) {
		            var str = String(e);
		            if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
		    			bfileid = 0;
		            }
		        }
			}
	
			var cfileurl = '#';
			var cfileid = recProposal.getFieldValue('custbody_clgx_prop_contract_file_id');
			if(cfileid != null && cfileid !=''){
				try {
		        	var objFile = nlapiLoadFile(cfileid);
					var cfileurl = objFile.getURL();
		        } 
		        catch (e) {
		            var str = String(e);
		            if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
		    			cfileid = 0;
		            }
		        }
			}
			
			objProposal = new Object();
			objProposal["entityid"] = proposalid;
			objProposal["entity"] = 'Proposal #' + recProposal.getFieldValue('tranid');
			objProposal["bfileurl"] = bfileurl;
			objProposal["cfileurl"] = cfileurl;
			objProposal["hassos"] = parseInt(nbrSOs) + parseInt(nbrOpptySOs);
			objProposal["expanded"] = true;
			objProposal["iconCls"] = 'proposal';
			objProposal["leaf"] = false;
			
			objFromTo = new Object();
			objFromTo["entityid"] = 1;
			objFromTo["entity"] = 'Created From SOs';
			objFromTo["expanded"] = false;
			objFromTo["iconCls"] = 'fromsos';
			objFromTo["leaf"] = false;
			var arrFromTo = new Array();
			
			var arrFromSOsIDs = new Array();
			var arrFromSOsNames = new Array();
			arrFromSOsIDs = recProposal.getFieldValues('custbody_clgx_renewed_from_sos');
			arrFromSOsNames = recProposal.getFieldTexts('custbody_clgx_renewed_from_sos');
			
			var arrFromSOs = new Array();
			for ( var j = 0; arrFromSOsIDs != null && j < arrFromSOsIDs.length; j++ ) {
				var soid = arrFromSOsIDs[j];
				var sonbr = arrFromSOsNames[j];
	
				var objFromSO = new Object();
				objFromSO["entityid"] = soid;
				objFromSO["entity"] = sonbr;
				objFromSO["expanded"] = false;
				objFromSO["iconCls"] = 'so';
				objFromSO["leaf"] = false;
				
				var arrColumns = new Array();
				arrColumns.push(new nlobjSearchColumn('item',null,null));
				arrColumns.push(new nlobjSearchColumn('custcol_clgx_so_col_service_id',null,null));
				arrColumns.push(new nlobjSearchColumn('custcol_clgx_qty2print',null,null));
				arrColumns.push(new nlobjSearchColumn('quantity',null,null));
				arrColumns.push(new nlobjSearchColumn('rate',null,null));
				arrColumns.push(new nlobjSearchColumn('amount',null,null));
				arrColumns.push(new nlobjSearchColumn('location',null,null));
				arrColumns.push(new nlobjSearchColumn('billingschedule',null,null));
				arrColumns.push(new nlobjSearchColumn('custcol_cologix_invoice_item_category',null,null));
				arrColumns.push(new nlobjSearchColumn('memo',null,null));
				var arrFilters = new Array();
				arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",soid));
				var searchItems = nlapiSearchRecord('transaction', 'customsearch_clgx_so_renewals', arrFilters, arrColumns);
				
				var arrItems = new Array();
				for ( var k = 0; searchItems != null && k < searchItems.length; k++ ) {
					var searchItem = searchItems[k];
					var objItem = new Object();
					objItem["entityid"] = searchItem.getValue('item', null, null);
					objItem["entity"] = searchItem.getText('item', null, null);
					objItem["serviceid"] = searchItem.getValue('custcol_clgx_so_col_service_id', null, null);
					objItem["service"] = searchItem.getText('custcol_clgx_so_col_service_id', null, null);
					objItem["qty2print"] = parseFloat(searchItem.getValue('custcol_clgx_qty2print', null, null));
					objItem["quantity"] = parseFloat(searchItem.getValue('quantity', null, null));
					objItem["rate"] = parseFloat(searchItem.getValue('rate', null, null));
					objItem["amount"] = parseFloat(searchItem.getValue('amount', null, null));
					objItem["locationid"] = searchItem.getValue('location', null, null);
					objItem["location"] = searchItem.getText('location', null, null);
					objItem["billschedid"] = searchItem.getValue('billingschedule', null, null);
					objItem["billsched"] = searchItem.getText('billingschedule', null, null);
					objItem["category"] = searchItem.getText('custcol_cologix_invoice_item_category', null, null);
					objItem["description"] = searchItem.getValue('memo',null, null);
					
					objItem["expanded"] = false;
					objItem["iconCls"] = 'item';
					objItem["leaf"] = true;
					arrItems.push(objItem);
				}
				objFromSO["children"] = arrItems;
				arrFromSOs.push(objFromSO);
			}
			objFromTo["children"] = arrFromSOs;
			arrFromTo.push(objFromTo);
			
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
			arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("createdfrom",null,"anyof",proposalid));
			var searchToSOs = nlapiSearchRecord('salesorder', null, arrFilters, arrColumns);
			
			if(searchToSOs != null){
	
				objToSOs = new Object();
				objToSOs["entityid"] = 1;
				objToSOs["entity"] = 'Promoted To SOs';
				objToSOs["expanded"] = false;
				objToSOs["iconCls"] = 'tosos';
				objToSOs["leaf"] = false;
				
				var arrToSOs = new Array();
				for ( var l = 0; searchToSOs != null && l < searchToSOs.length; l++ ) {
					var searchToSO = searchToSOs[l];
					var soid = searchToSO.getValue('internalid', null, 'GROUP');
					var sonbr = searchToSO.getValue('tranid', null, 'GROUP');
	
					var objToSO = new Object();
					objToSO["entityid"] = soid;
					objToSO["entity"] = sonbr;
					objToSO["expanded"] = false;
					objToSO["iconCls"] = 'so';
					objToSO["leaf"] = false;
					
					var arrColumns = new Array();
					arrColumns.push(new nlobjSearchColumn('item',null,null));
					arrColumns.push(new nlobjSearchColumn('custcol_clgx_so_col_service_id',null,null));
					arrColumns.push(new nlobjSearchColumn('custcol_clgx_qty2print',null,null));
					arrColumns.push(new nlobjSearchColumn('quantity',null,null));
					arrColumns.push(new nlobjSearchColumn('rate',null,null));
					arrColumns.push(new nlobjSearchColumn('amount',null,null));
					arrColumns.push(new nlobjSearchColumn('location',null,null));
					arrColumns.push(new nlobjSearchColumn('billingschedule',null,null));
					arrColumns.push(new nlobjSearchColumn('custcol_cologix_invoice_item_category',null,null));
					arrColumns.push(new nlobjSearchColumn('memo',null,null));
					var arrFilters = new Array();
					arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",soid));
					var searchItems = nlapiSearchRecord('transaction', 'customsearch_clgx_so_renewals_to', arrFilters, arrColumns);
					
					var arrItems = new Array();
					for ( var k = 0; searchItems != null && k < searchItems.length; k++ ) {
						var searchItem = searchItems[k];
						var objItem = new Object();
						objItem["entityid"] = searchItem.getValue('item', null, null);
						objItem["entity"] = searchItem.getText('item', null, null);
						objItem["serviceid"] = searchItem.getValue('custcol_clgx_so_col_service_id', null, null);
						objItem["service"] = searchItem.getText('custcol_clgx_so_col_service_id', null, null);
						objItem["qty2print"] = parseFloat(searchItem.getValue('custcol_clgx_qty2print', null, null));
						objItem["quantity"] = parseFloat(searchItem.getValue('quantity', null, null));
						objItem["rate"] = parseFloat(searchItem.getValue('rate', null, null));
						objItem["amount"] = parseFloat(searchItem.getValue('amount', null, null));
						objItem["locationid"] = searchItem.getValue('location', null, null);
						objItem["location"] = searchItem.getText('location', null, null);
						objItem["billschedid"] = searchItem.getValue('billingschedule', null, null);
						objItem["billsched"] = searchItem.getText('billingschedule', null, null);
						objItem["category"] = searchItem.getText('custcol_cologix_invoice_item_category', null, null);
						objItem["description"] = searchItem.getValue('memo',null, null);
						
						objItem["expanded"] = false;
						objItem["iconCls"] = 'item';
						objItem["leaf"] = true;
						arrItems.push(objItem);
					}
					objToSO["children"] = arrItems;
					arrToSOs.push(objToSO);
				}
				objToSOs["children"] = arrToSOs;
				
				arrFromTo.push(objToSOs);
			}
			objProposal["children"] = arrFromTo;
			arrProposals.push(objProposal);
		}
		objOppty["children"] = arrProposals;
		arrOppty.push(objOppty);
	}
	objTree["children"] = arrOppty;
	
	return JSON.stringify(objTree);
	
	
}

function inArray(val, arr){	
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}