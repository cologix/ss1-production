nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_SO_Renewals.js
//	Script Name:	CLGX_SL_SO_Renewals
//	Script Id:		customscript_clgx_sl_so_renewals
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/22/2014
//-------------------------------------------------------------------------------------------------

function suitelet_so_renewals (request, response){
	try {
		var userid = nlapiGetUser();
		var roleid = nlapiGetRole();
		var closed = 0;
		if(closed == 1 && (roleid != -5 && roleid != 3 && roleid != 18)){ // if module is closed any not admin using it
			var arrParam = new Array();
			arrParam['custscript_internal_message'] = 'This module is closed for modifications. Sorry for the inconvenience. Please come back later.';
			nlapiSetRedirectURL('SUITELET', 'customscript_clgx_sl_general_message', 'customdeploy_clgx_sl_general_message', false, arrParam);
		}
		
		var objFile = nlapiLoadFile(975605);
		var html = objFile.getValue();
		html = html.replace(new RegExp('{customersJSON}','g'), getCustomersJSON(userid,roleid));
		response.write( html );
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}

function getCustomersJSON(userid,roleid){

	var arrFilters = new Array();
	if (roleid == 1018 || roleid == 1043 || roleid == 1047 || roleid == 1040) {
		arrFilters.push(new nlobjSearchFilter("salesrep",'customerMain',"anyof",userid));
	}
	var searchCustomers = nlapiLoadSearch('transaction', 'customsearch_clgx_so_renewals_customers');
	searchCustomers.addFilters(arrFilters);
	
	var rowsArr = new Array();
	var resultSet = searchCustomers.runSearch();
	resultSet.forEachResult(function(searchCustomer) {
		var columns = searchCustomer.getAllColumns();
		var colObj = new Object();
		colObj = new Object();
		colObj["customerid"] = parseInt(searchCustomer.getValue(columns[0]));
		colObj["customer"] = searchCustomer.getText(columns[0]);
		colObj["sos"] = parseInt(searchCustomer.getValue(columns[1]));
		colObj["ro"] = parseInt(searchCustomer.getValue(columns[2]));
		colObj["rp"] = parseInt(searchCustomer.getValue(columns[3]));
		colObj["rs"] = parseInt(searchCustomer.getValue(columns[4]));
		colObj["np"] = parseInt(searchCustomer.getValue(columns[5]));
		colObj["inactives"] = parseInt(searchCustomer.getValue(columns[6]));
		rowsArr.push(colObj);
		return true;                // return true to keep iterating
		});
    return JSON.stringify(rowsArr);
}