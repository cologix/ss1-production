nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_SO_Renewals_SOs.js
//	Script Name:	CLGX_SL_SO_Renewals_SOs
//	Script Id:		customscript_clgx_sl_so_renewals_sos
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		11/19/2014
//-------------------------------------------------------------------------------------------------

function suitelet_so_renewals_sos (request, response){
	try {

		var roleid = nlapiGetRole();

		var customerid = request.getParameter('customerid');
		var ro = request.getParameter('ro');
		var rp = request.getParameter('rp');
		var rs = request.getParameter('rs');

		//var arrCustExcept = [10773,73,87,3208,2191];
		//var arrRoles = [-5,3,18];
		
        //if(inArray(customerid,arrCustExcept) && !inArray(roleid,arrRoles) ){
        //    var html = 'Please contact operations to renew this customer.';
        //}
		if(customerid == 0){
			var html = '';
			html += '<table cellpadding="5" border="0" width="500px" style="font-family: verdana,arial,sans-serif;font-size:12px;text-align:justify;">';
			html += '<tr>';
			html += '<td align="center" valign="top"><img src="//www.cologix.com//images/icon/famfamfam_silk_icons/icons/application_view_icons.png" alt="Service Orders" height="16" width="16"></td>';
			html += '<td valign="top">Please click on this icon to select existing service orders and create opportunities and proposals. If you have created an opportunity that was not promoted to a proposal yet, you will see the Create Proposal form until you finish creating it.</td>';
			html += '<tr>';
			html += '</tr>';
			html += '<td align="center" valign="top"><img src="//www.cologix.com//images/icon/famfamfam_silk_icons/icons/chart_organisation.png" alt="Transactions" height="16" width="16"></td>';
			html += '<td valign="top">Please click on this icon to view all renewed transactions, to edit proposals, to create budgetary quotes and contracts, to upload signed contracts to Box and to create service orders.</td>';
			html += '</tr>';
			html += '</table>';
		}
		else{
			var opptyid = 0;
			
			// look for renewal opportunities for this customer in SOs
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('custbody_clgx_so_renewed_on_oppty',null,'GROUP'));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
			arrFilters.push(new nlobjSearchFilter("custbody_clgx_so_renewed_on_oppty",null,"noneof",'@NONE@'));
			arrFilters.push(new nlobjSearchFilter("custbody_clgx_so_renewed_on_so",null,"anyof",'@NONE@'));
			var searchOppty = nlapiSearchRecord('salesorder', null, arrFilters, arrColumns);
			if(searchOppty != null){
				opptyid = searchOppty[0].getValue('custbody_clgx_so_renewed_on_oppty', null, 'GROUP');
			}
			
			// look for renewal opportunities for that customer in Processing queue in case it was not processed
			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('custrecord_clgx_que_renew_oppty_oppty',null,'GROUP'));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("custrecord_clgx_que_renew_oppty_customer",null,"anyof",customerid));
			var searchOpptyQueue = nlapiSearchRecord('customrecord_clgx_que_renew_sos_oppty', null, arrFilters, arrColumns);
			if(searchOpptyQueue != null){
				opptyid = searchOpptyQueue[0].getValue('custrecord_clgx_que_renew_oppty_oppty', null, 'GROUP');
			}
			
			var legend = '';
			legend += '<table cellpadding="5" border="0" style="font-family: verdana,arial,sans-serif;font-size:12px;padding:5px;text-align:justify;">';
			legend += '<tr>';
			legend += '<td align="center" valign="top" style="padding: 5px;"><img src="//www.cologix.com/images/icon/famfamfam_silk_icons/icons/application_form.png" alt="Service Orders" height="16" width="16"></td>';
			legend += '<td valign="top" style="padding: 5px;">Please select all service orders you want to include on the renewed opportunity. Please be aware that all are selected by default. Also, you will not be able to include a service order that has an inactive item (red line).</td>';
			legend += '<tr>';
			legend += '<tr>';
			legend += '<td align="center" valign="top" style="padding: 5px;"><img src="//www.cologix.com/images/icon/famfamfam_silk_icons/icons/bell.png" alt="Service Orders" height="16" width="16"></td>';
			legend += '<td valign="top" style="padding: 5px;">Please contact the product manager before renewing this service order.</td>';
			legend += '<tr>';
			legend += '</tr>';
			legend += '<td align="center" valign="top" style="padding: 5px;"><img src="//www.cologix.com//images/icon/famfamfam_silk_icons/icons/application_view_list.png" alt="Service Orders" height="16" width="16"></td>';
			legend += '<td valign="top" style="padding: 5px;">Please select an item line to see item details.</td>';
			legend += '</tr>';
			legend += '</table>';
			
			if(opptyid > 0){ // this customer has an open opportunity
				
				var noProposal = nlapiLookupField('opportunity', opptyid, 'custbody_clgx_renewed_oppty_no_prop');
				if(noProposal > 0){ // the opportunity was not fullfield to a proposal - edit the opportunity
					var arrOpportunity = getOpportunityJSON (customerid,opptyid);
					var objFile = nlapiLoadFile(999899);
					var html = objFile.getValue();
					html = html.replace(new RegExp('{opptyRec}','g'),arrOpportunity[0]);
					html = html.replace(new RegExp('{dataItems}','g'),arrOpportunity[1]);
					html = html.replace(new RegExp('{dataContacts}','g'),arrOpportunity[2]);
				}
				else{ // display SOs
					var custname = nlapiLookupField('customer', customerid, 'entityid');
					var objFile = nlapiLoadFile(976008);
					var html = objFile.getValue();
					html = html.replace(new RegExp('{dataSOs}','g'),getSOsJSON(customerid));
					html = html.replace(new RegExp('{custName}','g'),custname);
					html = html.replace(new RegExp('{customerid}','g'),customerid);
					html = html.replace(new RegExp('{ro}','g'),ro);
					html = html.replace(new RegExp('{rp}','g'),rp);
					html = html.replace(new RegExp('{rs}','g'),rs);
					html = html.replace(new RegExp('{legend}','g'),legend);
				}
			}
			else{ // display SOs
				var custname = nlapiLookupField('customer', customerid, 'entityid');
				var objFile = nlapiLoadFile(976008);
				var html = objFile.getValue();
				html = html.replace(new RegExp('{dataSOs}','g'),getSOsJSON(customerid));
				html = html.replace(new RegExp('{custName}','g'),custname);
				html = html.replace(new RegExp('{customerid}','g'),customerid);
				html = html.replace(new RegExp('{ro}','g'),ro);
				html = html.replace(new RegExp('{rp}','g'),rp);
				html = html.replace(new RegExp('{rs}','g'),rs);
				html = html.replace(new RegExp('{legend}','g'),legend);
			}
		}
		
		//var usageConsumtion = 5000 - parseInt(nlapiGetContext().getRemainingUsage());
		//nlapiLogExecution('DEBUG', 'Value', '| Usage = '+ usageConsumtion + ' |');
		//nlapiSendEmail(71418,71418,usageConsumtion,'',null,null,null,null);

		response.write( html );
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function getSOsJSON(customerid){

	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('location',null,'GROUP'));
	arrColumns.push(new nlobjSearchColumn('internalid',null,'COUNT'));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
	arrFilters.push(new nlobjSearchFilter("custbody_clgx_so_renewed_on_so",null,"anyof",'@NONE@'));
	var searchLocations = nlapiSearchRecord('transaction', 'customsearch_clgx_so_renewals', arrFilters, arrColumns);

	var objTree = new Object();
	objTree["text"] = '.';
	var arrLocations = new Array();
	for ( var i = 0; searchLocations != null && i < searchLocations.length; i++ ) {
		var searchLocation = searchLocations[i];
		var locationid = searchLocation.getValue('location', null, 'GROUP');
		var location = searchLocation.getText('location', null, 'GROUP');
		
		objLocation = new Object();
		objLocation["entity"] = location + ' (' + searchLocation.getValue('internalid', null, 'COUNT') + ')';
		objLocation["expanded"] = true;
		objLocation["iconCls"] = 'location';
		objLocation["leaf"] = false;
		
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
		arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
		arrColumns.push(new nlobjSearchColumn('custbody_clgx_so_pm_flag',null,'GROUP'));
		var arrFilters = new Array();
		arrFilters.push(new nlobjSearchFilter("location",null,"anyof",locationid));
		arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
		arrFilters.push(new nlobjSearchFilter("custbody_clgx_so_renewed_on_so",null,"anyof",'@NONE@'));
		var searchSOs = nlapiSearchRecord('transaction', 'customsearch_clgx_so_renewals', arrFilters, arrColumns);
		
		var arrSOs = new Array();
		for ( var j = 0; searchSOs != null && j < searchSOs.length; j++ ) {
			
			var searchSO = searchSOs[j];
			var soid = searchSO.getValue('internalid', null, 'GROUP');
			var sonbr = searchSO.getValue('tranid', null, 'GROUP');
			var pmflag = searchSO.getValue('custbody_clgx_so_pm_flag', null, 'GROUP');
			
			var fields = ['custbody_clgx_so_renewed_on_oppty','custbody_clgx_so_renewed_on_proposal'];
			var columns = nlapiLookupField('salesorder', soid, fields);
			var opptyid = columns.custbody_clgx_so_renewed_on_oppty;
			var proposalid = columns.custbody_clgx_so_renewed_on_proposal;
			if(opptyid != null && opptyid != ''){
				var opportunity = nlapiLookupField('opportunity', opptyid, 'tranid');
			}
			else{
				var opportunity = '';
			}
			if(proposalid != null && proposalid != ''){
				var proposal = nlapiLookupField('estimate', proposalid, 'tranid');
			}
			else{
				var proposal = '';
			}
			
			var objSO = new Object();
			objSO["entityid"] = soid;
			objSO["entity"] = sonbr;
			objSO["checked"] = true;
			objSO["pmflag"] = pmflag;
			objSO["expanded"] = true;
			objSO["opptyid"] = parseInt(opptyid);
			objSO["oppty"] = opportunity;
			objSO["proposalid"] = parseInt(proposalid);
			objSO["proposal"] = proposal;
			objSO["iconCls"] = 'so';
			objSO["leaf"] = false;

			var arrColumns = new Array();
			arrColumns.push(new nlobjSearchColumn('rate',null,null));
			var arrFilters = new Array();
			arrFilters.push(new nlobjSearchFilter("location",null,"anyof",locationid));
			arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",soid));
			var searchItems = nlapiSearchRecord('transaction', 'customsearch_clgx_so_renewals_items', arrFilters, arrColumns);
			
			var arrItems = new Array();
			for ( var k = 0; searchItems != null && k < searchItems.length; k++ ) {
				var searchItem = searchItems[k];
				var itemName = searchItem.getText('item', null, null);
				var objItem = new Object();
				objItem["entityid"] = searchItem.getValue('item', null, null);
				objItem["entity"] = searchItem.getText('item', null, null);
				objItem["entityinactive"] = searchItem.getValue('isinactive', 'item', null);
				objItem["serviceid"] = searchItem.getValue('custcol_clgx_so_col_service_id', null, null);
				objItem["service"] = searchItem.getText('custcol_clgx_so_col_service_id', null, null);
				if(itemName.indexOf('IPv4') == -1){
					var servid = searchItem.getValue('custcol_clgx_so_col_service_id', null, null);
					if(servid != null && servid != ''){
						objItem["serviceinactive"] = nlapiLookupField('job', servid, 'isinactive');
					}
					else{
						objItem["serviceinactive"] = 'F';
					}
				}
				else{
					objItem["serviceinactive"] = 'F';
				}
				objItem["qty2print"] = parseFloat(searchItem.getValue('custcol_clgx_qty2print', null, null));
				objItem["quantity"] = parseFloat(searchItem.getValue('quantity', null, null));
				objItem["rate"] = parseFloat(searchItem.getValue('rate', null, null));
				objItem["amount"] = parseFloat(searchItem.getValue('amount', null, null));
				objItem["locationid"] = searchItem.getValue('location', null, null);
				objItem["location"] = searchItem.getText('location', null, null);
				objItem["billschedid"] = searchItem.getValue('billingschedule', null, null);
				objItem["billsched"] = searchItem.getText('billingschedule', null, null);
				objItem["category"] = searchItem.getText('custcol_cologix_invoice_item_category', null, null);
				objItem["description"] = searchItem.getValue('memo',null, null);
				objItem["iconCls"] = 'item';
				objItem["leaf"] = true;
				arrItems.push(objItem);
			}
			objSO["children"] = arrItems;
			arrSOs.push(objSO);
		}
		objLocation["children"] = arrSOs;
		arrLocations.push(objLocation);
	}
	objTree["children"] = arrLocations;
	
    return JSON.stringify(objTree);
}


function getOpportunityJSON (customerid,opptyid) {
	
	var recOppty = nlapiLoadRecord('opportunity', opptyid);
	
	// build opportunity record JSON
	var objOppty = new Object();
	var objData = new Object();
	objData["customerid"] = parseInt(recOppty.getFieldValue('entity'));
	objData["customer"] = recOppty.getFieldText('entity');
	objData["opptyid"] = parseInt(opptyid);
	objData["opportunity"] = recOppty.getFieldValue('tranid');
	objData["salesrepid"] = parseInt(recOppty.getFieldValue('salesrep'));
	objData["salesrep"] = recOppty.getFieldText('salesrep');
	objData["status"] = recOppty.getFieldValue('status');
	objData["probability"] = recOppty.getFieldValue('probability');
	objData["title"] = recOppty.getFieldValue('title');
	//objData["closedate"] = recOppty.getFieldValue('expectedclosedate');
	objData["closedate"] = '';
	objData["saletype"] = recOppty.getFieldValue('custbody_cologix_opp_sale_type');
	objData["leadsource"] = parseInt(recOppty.getFieldValue('leadsource'));
	objOppty["data"] = objData;

	// build items grid JSON
  	var nbrItemsOppty = recOppty.getLineItemCount('item');
  	var arrItems = new Array();
	for (var j = 0; j < nbrItemsOppty; j++){
		
		var itemid = recOppty.getLineItemValue('item','item', j + 1);
		var iteminactive = 'F';
		if(itemid != null && itemid != ''){
			iteminactive = nlapiLookupField('item', itemid, 'isinactive');
		}
		var serviceid = recOppty.getLineItemValue('item','custcol_clgx_oppty_col_service_id', j + 1);
		var serviceinactive = 'F';
		if(serviceid != null && serviceid != ''){
			serviceinactive = nlapiLookupField('job', serviceid, 'isinactive');
		}
		
		var objItem = new Object();
		objItem["customerid"] = parseInt(customerid);
		objItem["opptyid"] = parseInt(opptyid);
		objItem["lineid"] = j + 1;
		objItem["itemid"] = parseInt(recOppty.getLineItemValue('item','item', j + 1));
		objItem["item"] = recOppty.getLineItemText('item','item', j + 1);
		objItem["iteminactive"] = iteminactive;
		objItem["serviceid"] = parseInt(recOppty.getLineItemValue('item','custcol_clgx_oppty_col_service_id', j + 1));
		objItem["service"] = recOppty.getLineItemText('item','custcol_clgx_oppty_col_service_id', j + 1);
		objItem["serviceinactive"] = serviceinactive;
		objItem["qty2print"] = parseFloat(recOppty.getLineItemValue('item','custcol_clgx_qty2print', j + 1));
		objItem["quantity"] = parseFloat(recOppty.getLineItemValue('item','quantity', j + 1));
		objItem["rate"] = parseFloat(recOppty.getLineItemValue('item','rate', j + 1));
		objItem["amount"] = parseFloat(recOppty.getLineItemValue('item','amount', j + 1));
		objItem["locationid"] = parseInt(recOppty.getLineItemValue('item','location', j + 1));
		objItem["location"] = recOppty.getLineItemText('item','location', j + 1);
		objItem["billschedid"] = parseInt(recOppty.getLineItemValue('item','custcol_clgx_oppty_billing_schedule', j + 1));
		objItem["billsched"] = recOppty.getLineItemText('item','custcol_clgx_oppty_billing_schedule', j + 1);
		objItem["description"] = recOppty.getLineItemValue('item','description', j + 1);
		objItem["categoryid"] = parseInt(recOppty.getLineItemValue('item','custcol_cologix_invoice_item_category', j + 1));
		objItem["category"] = recOppty.getLineItemText('item','custcol_cologix_oppty_item_category', j + 1);
		objItem["classid"] = parseInt(recOppty.getLineItemValue('item','class', j + 1));
		objItem["classname"] = recOppty.getLineItemText('item','class', j + 1);
		objItem["taxcodeid"] = parseInt(recOppty.getLineItemValue('item','taxcode', j + 1));
		objItem["taxcode"] = recOppty.getLineItemText('item','taxcode', j + 1);
		arrItems.push(objItem);
	}
	
	// build contacts grid JSON
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn('internalid', 'contact', null));
	arrColumns.push(new nlobjSearchColumn('entityid', 'contact', null));
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter('internalid',null,'is',customerid));
	arrFilters.push(new nlobjSearchFilter('isinactive','contact','is','F'));
	var searchContacts = nlapiSearchRecord('customer', null, arrFilters, arrColumns);
	
	var arrContacts = new Array();
	for ( var k = 0; searchContacts != null && k < searchContacts.length; k++ ) {
    	var searchContact = searchContacts[k];
    	var contactid = searchContact.getValue('internalid','contact', null);
    	var contact = searchContact.getValue('entityid','contact', null);
    	
    	var objContact = new Object();
    	objContact["value"] = parseInt(contactid);
    	objContact["text"] = contact;
    	arrContacts.push(objContact);
	}
	
	// put all 3 JSONs on one return
	var arrOppty = new Array;
	arrOppty.push(JSON.stringify(objOppty));
	arrOppty.push(JSON.stringify(arrItems));
	arrOppty.push(JSON.stringify(arrContacts));
	return arrOppty;
}

//check if value is in the array
function inArray(val, arr){	
  var bIsValueFound = false;
  for(var i = 0; i < arr.length; i++){
      if(val == arr[i]){
          bIsValueFound = true;
          break;
      }
  }
  return bIsValueFound;
}