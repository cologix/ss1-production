nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_SO_Renewals_Item.js
//	Script Name:	CLGX_SL_SO_Renewals_SOs
//	Script Id:		customscript_clgx_sl_so_renewals_item
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/22/2014
//	RelativeURL:	/app/site/hosting/scriptlet.nl?script=300&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_so_renewals_items (request, response){
    try {
        var customerid = request.getParameter('customerid');
        var opptyid = request.getParameter('opptyid');
        var lineid = request.getParameter('lineid');
        var  rateSum=parseFloat(0);
        var  rateSumOld=parseFloat(0);

        if(customerid > 0 && opptyid > 0 && lineid > 0){
            var update = request.getParameter('update');
            if(update == 1){
                var itemLabel = '<span style="color:green;font-weight:bold">Item was updated</span>';

                var recOppty = nlapiLoadRecord('opportunity', opptyid);
                var rateQTOld=recOppty.getLineItemValue('item','quantity', lineid);
                var rateOPOld=recOppty.getLineItemValue('item','rate', lineid);
                var oldIncremental=recOppty.getFieldValue('custbody_cologix_opp_incremental_mrc');
                var renewSos=recOppty.getFieldValues('custbody_clgx_renewed_from_sos');
                var locop=recOppty.getFieldValue('location');
                recOppty.setLineItemValue('item','description', lineid, request.getParameter('description'));
                recOppty.setLineItemValue('item','quantity', lineid, request.getParameter('quantity'));
                recOppty.setLineItemValue('item','rate', lineid, request.getParameter('rate'));
                rateSumOld =parseFloat(rateSumOld)+parseFloat(rateQTOld)*parseFloat(rateOPOld);
                rateSum =parseFloat(rateSum)+parseFloat(request.getParameter('rate'))*parseFloat(request.getParameter('quantity'));
                recOppty.setLineItemValue('item','custcol_clgx_oppty_billing_schedule', lineid, request.getParameter('billsched'));
               // var incremental_mrc=parseFloat(rateSum)-parseFloat(rateSumOld);
               // if((locop!='')&&(locop!=null)){
                   /* if((oldIncremental!=null)&&(oldIncremental!=''))
                    {
                        incremental_mrc=parseFloat(oldIncremental)+parseFloat(incremental_mrc);
                    }
                    if(parseFloat(incremental_mrc) > parseFloat(0))
                    {
                        recOppty.setFieldValue('custbody_cologix_opp_incremental_mrc', incremental_mrc);
                    }*/
              //  }
                nlapiSubmitRecord(recOppty, false, true);
                if((locop!='')&&(locop!=null)){
                    var rateSumMRCSOS=0;
                    var arrFilterMRC=new Array();
                    var arrColMRC=new Array();
                    arrFilterMRC.push(new nlobjSearchFilter('custrecord_clgx_totals_transaction',null,'anyof', renewSos));
                    var searchSOsMRC = nlapiSearchRecord('customrecord_clgx_totals_transaction','customsearch_clgx_ss_incr_mrc',arrFilterMRC,arrColMRC);
                    if(searchSOsMRC!=null)
                    {
                        for ( var i = 0;searchSOsMRC  != null && i < searchSOsMRC.length; i++ ) {
                            var searchSOMRC = searchSOsMRC [i];
                            var columns = searchSOMRC.getAllColumns();
                            rateSumMRCSOS =parseFloat(searchSOMRC.getValue(columns[0]));
                        }

                    }
                    //opp
                    var recOP=nlapiLoadRecord('opportunity',opptyid);
                    var rateSumMRCOP=0;
                    var NRItems = recOP.getLineItemCount('item');
                    for ( var j = 1; j <= NRItems; j++ ) {
                        rateSumMRCOP= parseFloat(rateSumMRCOP)+(parseFloat(recOP.getLineItemValue('item','rate',j))*parseFloat(recOP.getLineItemValue('item','quantity',j)));
                    }
                    var incremental_mrc=parseFloat(rateSumMRCOP)-parseFloat(rateSumMRCSOS);
                    //   nlapiSendEmail(206211,206211,'rateSumMRCOP1',incremental_mrc+' '+rateSumMRCOP+' '+rateSumMRCSOS,null,null,null,null);
                    if(parseFloat(incremental_mrc) > parseFloat(0))
                    {
                        recOP.setFieldValue('custbody_cologix_opp_incremental_mrc', incremental_mrc);
                        nlapiSubmitRecord(recOP, false, true);
                    }

                }

                var totals = clgx_transaction_totals (customerid, 'oppty', opptyid);
            }
            else{
                var itemLabel = 'Item';
            }

            var objFile = nlapiLoadFile(1005848);
            var html = objFile.getValue();
            html = html.replace(new RegExp('{dataBS}','g'), getBillSchedJSON());
            html = html.replace(new RegExp('{itemRec}','g'), getItemJSON(customerid, opptyid, lineid));
            html = html.replace(new RegExp('{itemLabel}','g'), itemLabel);
        }
        else{
            var html = 'Please select an item from the left panel to edit it.';
        }
        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function getItemJSON (customerid, opptyid, lineid){

    var recOppty = nlapiLoadRecord('opportunity', opptyid);

    var objItem = new Object();
    var objData = new Object();
    objData['customerid'] = parseInt(customerid);
    objData['customer'] = recOppty.getFieldText('entity');
    objData['opptyid'] = parseInt(opptyid);
    objData['lineid'] = parseInt(lineid);
    objData['billsched'] = parseInt(recOppty.getLineItemValue('item','custcol_clgx_oppty_billing_schedule', lineid));
    objData['quantity'] = parseFloat(recOppty.getLineItemValue('item','quantity', lineid));
    objData['serviceid'] = parseInt(recOppty.getLineItemValue('item','custcol_clgx_oppty_col_service_id', lineid));
    objData['service'] = recOppty.getLineItemText('item','custcol_clgx_oppty_col_service_id', lineid);
    objData['locationid'] = parseInt(recOppty.getLineItemValue('item','location', lineid));
    objData['location'] = recOppty.getLineItemText('item','location', lineid);
    objData['itemid'] = parseInt(recOppty.getLineItemValue('item','item', lineid));
    objData['item'] = recOppty.getLineItemText('item','item', lineid);
    objData['categoryid'] = parseInt(recOppty.getLineItemValue('item','custcol_cologix_oppty_item_category', lineid));
    objData['category'] = recOppty.getLineItemText('item','custcol_cologix_oppty_item_category', lineid);
    objData['class'] = recOppty.getLineItemValue('item','class', lineid);
    objData['description'] = recOppty.getLineItemValue('item','description', lineid);
    objData['options'] = recOppty.getLineItemValue('item','options', lineid);
    objData['unevenday'] = recOppty.getLineItemValue('item','custcol_cologix_unevendaystobill', lineid);
    //objData['pricelevels'] = recOppty.getLineItemValue('item','pricelevels', lineid);
    objData['price'] = parseFloat(recOppty.getLineItemValue('item','price', lineid));
    objData['rate'] = parseFloat(recOppty.getLineItemValue('item','rate', lineid));
    //objData['custcol_cologix_rateperday'] = recOppty.getLineItemValue('item','custcol_cologix_rateperday', lineid);
    objData['taxcode'] = recOppty.getLineItemValue('item','taxcode', lineid);
    objData['taxrate1'] = recOppty.getLineItemValue('item','taxrate1', lineid);
    objData['taxrate2'] = recOppty.getLineItemValue('item','taxrate2', lineid);

    objItem['data'] = objData;

    return JSON.stringify(objItem)
}


function getBillSchedJSON(){

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('custrecord_clgx_bs_id',null,null).setSort(false));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("internalid",null,"noneof",63)); // exclude non recurring billing schedule
    var searchBSs = nlapiSearchRecord('customrecord_clgx_billing_schedules', null, arrFilters, arrColumns);

    var rowsArr = new Array();
    for ( var i = 0; searchBSs != null && i < searchBSs.length; i++ ) {
        var searchBS = searchBSs[i];
        var value = parseInt(searchBS.getValue('custrecord_clgx_bs_id',null,null));
        var text = searchBS.getText('custrecord_clgx_bs_id',null,null);
        var colObj = new Object();
        colObj["value"] = value;
        colObj["text"] = text;
        rowsArr.push(colObj);
    }
    return JSON.stringify(rowsArr);
}