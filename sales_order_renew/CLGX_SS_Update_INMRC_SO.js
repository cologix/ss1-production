nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_Update_INMRC_SO.js
//	Script Name:	CLGX_SS_Update_INMRC_SO
//	Script Id:		customscript_clgx_ss_updateenmrc_so
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Service Order
//	@authors:		Catalina Taran
//-------------------------------------------------------------------------------------------------

function scheduled_opp_imrc(){
    try{
//------------- Begin Section 1 -------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|');

        var arrColumnsIn = new Array();
        var arrFiltersIn = new Array();
        var searchIncrementalOpps=nlapiSearchRecord('salesorder','customsearch_clgx_ss_renewals_imrc',arrFiltersIn,arrColumnsIn);
        if(searchIncrementalOpps!=null)
        {
            for ( var i = 0; searchIncrementalOpps != null && i < searchIncrementalOpps.length; i++ ) {
                var searchIncrementalOpp = searchIncrementalOpps[i];
                var columnsIn = searchIncrementalOpp.getAllColumns();
                var renewSOs = searchIncrementalOpp.getValue(columnsIn[2]);
                var proposalid = searchIncrementalOpp.getValue(columnsIn[3]);
                var locationid = searchIncrementalOpp.getValue(columnsIn[4]);
                var bsid= searchIncrementalOpp.getValue(columnsIn[5]);
                var soidIn= searchIncrementalOpp.getValue(columnsIn[0]);
                var arraySOSOLD=new Array();
                if(renewSOs.indexOf(",") > -1){
                    var arrRenewSOs=renewSOs.split(",");
                }
                else
                {
                    var arrRenewSOs=renewSOs;
                }
                if((renewSOs!=null)&&(renewSOs!=''))
                {
                    var arrColumns = new Array();

                    arrColumns.push(new nlobjSearchColumn('custentity_cologix_service_order','custcol_clgx_so_col_service_id',null));

                    var arrFilters = new Array();
                    arrFilters.push(new nlobjSearchFilter('internalid',null,'anyof',proposalid));
                    arrFilters.push(new nlobjSearchFilter('location',null,'anyof',locationid));
                    arrFilters.push(new nlobjSearchFilter('billingschedule',null,'anyof',bsid));
                    var searchItems = nlapiSearchRecord('estimate', 'customsearch_clgx_so_renewals_proposal', arrFilters, arrColumns);


                    for ( var m = 0; searchItems != null && m < searchItems.length; m++ ) {
                        var searchItem = searchItems[m];
                        if((searchItem.getValue('custentity_cologix_service_order','custcol_clgx_so_col_service_id', null)!='')&&(searchItem.getValue('custentity_cologix_service_order','custcol_clgx_so_col_service_id', null)!=null))
                        {

                            arraySOSOLD.push(searchItem.getValue('custentity_cologix_service_order','custcol_clgx_so_col_service_id', null));
                        }

                    }
                    if(arraySOSOLD.length > 0)
                    {
                    var arrColumns1 = new Array();
                    // arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_location',null,null).setSort(false));
                    arrColumns1.push(new nlobjSearchColumn('custrecord_clgx_totals_total',null,'group'));
                    var arrFilters1 = new Array();
                    arrFilters1.push(new nlobjSearchFilter("internalid","custrecord_clgx_totals_transaction","anyof",arraySOSOLD));
                    //arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_class",null,"anyof",1));
                    var searchTotals = nlapiSearchRecord('customrecord_clgx_totals_transaction', null, arrFilters1, arrColumns1);

                    if(searchTotals!=null)
                    {
                        var oldIcremental_0=parseFloat(0);
                        for ( var j = 0; searchTotals != null && j < searchTotals.length; j++ ) {
                            var searchTotal = searchTotals[j];
                            oldIcremental_0=parseFloat(oldIcremental_0)+parseFloat(searchTotal.getValue('custrecord_clgx_totals_total',null,'group'));
                        }
                    }

                    var arrColumns2 = new Array();
                    // arrColumns.push(new nlobjSearchColumn('custrecord_clgx_totals_location',null,null).setSort(false));
                    arrColumns2.push(new nlobjSearchColumn('custrecord_clgx_totals_total',null,'group'));
                    var arrFilters2 = new Array();
                    arrFilters2.push(new nlobjSearchFilter("internalid","custrecord_clgx_totals_transaction","anyof",soidIn));
                    //arrFilters.push(new nlobjSearchFilter("custrecord_clgx_totals_class",null,"anyof",1));
                    var searchTotals2 = nlapiSearchRecord('customrecord_clgx_totals_transaction', null, arrFilters2, arrColumns2);

                    if(searchTotals2!=null)
                    {
                        var incrementalAmount=parseFloat(0);
                        for ( var p = 0; searchTotals2!= null && p < searchTotals2.length; p++ ) {
                            var searchTotal2 = searchTotals2[p];
                            incrementalAmount=parseFloat(incrementalAmount)+parseFloat(searchTotal2.getValue('custrecord_clgx_totals_total',null,'group'));
                        }
                    }

                    var incrementalFinal=parseFloat(incrementalAmount)-parseFloat(oldIcremental_0);
                    if(parseFloat(incrementalFinal)>parseFloat(0))
                    {
                        var recOpp=nlapiLoadRecord('salesorder',soidIn);
                        recOpp.setFieldValue('custbody_clgx_so_incremental_mrc', incrementalFinal.toFixed(2));
                        nlapiSubmitRecord(recOpp, false, true);
                        
                        clgx_send_employee_emails_from_savedsearch("customsearch_clgxe_inmrc_admin", "SOs Updated", searchIncrementalOpp.getValue(columnsIn[1]));
                        //nlapiSendEmail(206211,206211,'SOs Updated',searchIncrementalOpp.getValue(columnsIn[1]),null,null,null,null); //Send Email to Catalina

                    }

                }
                }


            }





        }
    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }

}


