nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_SO_Renewals_Proposal_Edit.js
//	Script Name:	CLGX_SL_SO_Renewals_Proposal_Edit
//	Script Id:		customscript_clgx_sl_so_renewals_prop_ed
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/26/2014
//	RelativeURL:	/app/site/hosting/scriptlet.nl?script=299&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_so_renewals_proposal_edit (request, response){
    try {

        var proposalid = request.getParameter('proposalid');
        var act = request.getParameter('act');

        if(proposalid > 0){

            var recProposal = nlapiLoadRecord('estimate', proposalid);
            var nbrSOs = recProposal.getLineItemCount('links');

            // check if opportunity has SOs
            var opptyid = recProposal.getFieldValue('opportunity');
            var nbrClosedSOs = 0;
            if(opptyid != null && opptyid != ''){
                var recOppty = nlapiLoadRecord('opportunity', opptyid);
                var nbrClosedSOs = recOppty.getLineItemCount('closed');
            }

            if(nbrSOs > 0 || nbrClosedSOs > 0){
                var html = 'You can not edit a proposal created from an opportunity that has service orders.';
            }
            else{

                if(act == 'update' || act == 'bq' || act == 'contract' || act == 'sos'){

                    var proposalid = request.getParameter('proposalid');
                    var title = request.getParameter('title');
                    var closedate = request.getParameter('closedate');
                    var forecasttype = request.getParameter('forecasttype');
                    var contactid = request.getParameter('contact');
                    var status = request.getParameter('status');
                    var lossreason = request.getParameter('lossreason');
                    var tobox = request.getParameter('tobox');

                    var contact = '';
                    if (parseInt(contactid) > 0){
                        var contact = nlapiLookupField('contact', contactid, 'entityid');
                    }
                    else{
                        contactid = '';
                    }

                    if(tobox){
                        var upload2box = 'T';
                    }
                    else{
                        var upload2box = 'F';
                    }

                    if (parseInt(proposalid) > 0){

                        recProposal.setFieldValue('title', title);
                        recProposal.setFieldValue('expectedclosedate', closedate);
                        recProposal.setFieldValue('forecasttype', forecasttype);
                        recProposal.setFieldValue('custbody_clgx_contract_terms_contact', contactid);
                        recProposal.setFieldValue('custbody_clgx_contract_terms_attention', contact);
                        recProposal.setFieldValue('entitystatus', status);
                        recProposal.setFieldValue('custbody_clgx_winlossreason', lossreason);
                        recProposal.setFieldValue('custbody_clgx_uploaded_to_box', upload2box);
                        nlapiSubmitRecord(recProposal, false, true);

                        recProposal.setFieldValue('custbody_clgx_winlossreason', lossreason);

                        //nlapiSubmitField('opportunity', opptyid, ['winlossreason','forecasttype'], [lossreason,forecasttype]);
                        var recOpportunity = nlapiLoadRecord('opportunity', opptyid);
                        recOpportunity.setFieldValue('winlossreason', lossreason);
                        recOpportunity.setFieldValue('forecasttype', forecasttype);
                        nlapiSubmitRecord(recOpportunity, false, true);

                    }

                    if(act == 'update'){
                        proposalLabel = '<span style="color:green;font-weight:bold">Proposal has been updated</span>';
                    }
                    if(act == 'bq' || act == 'contract'){
                        //var fileid = create_contract(proposalid, act);
                        if(act=='bq')
                        {
                            var quote='T';
                            var contract='F';
                        }
                        else{
                            var quote='F';
                            var contract='T';
                        }
                        var arrParam = [];
                        arrParam['custscript_proposal_pdf_proposalid'] = proposalid;
                        arrParam['custscript_proposal_pdf_userid'] =nlapiGetUser();
                        arrParam['custscript_proposal_pdf_quote'] = quote;
                        arrParam['custscript_proposal_pdf_contract'] = contract;
                        var status = nlapiScheduleScript('customscript_clgx_ss_proposal_pdf', null ,arrParam);

                        if(act == 'bq'){
                            proposalLabel = '<span style="color:green;font-weight:bold">Proposal has been updated and the Budgetary Quote created.</span>';
                        }
                        else{
                            proposalLabel = '<span style="color:green;font-weight:bold">Proposal has been updated and the Contract created.</span>';
                        }
                    }
                    if(act == 'sos'){

                        // verify if any proposal, including this one, created from the parent opportunity is in the processing queue
                        var arrPropQue = new Array();
                        var arrColumns = new Array();
                        arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
                        var arrFilters = new Array();
                        arrFilters.push(new nlobjSearchFilter("opportunity",null,"anyof",opptyid));
                        var searchProposals = nlapiSearchRecord('estimate', null, arrFilters, arrColumns);

                        for ( var i = 0; searchProposals != null && i < searchProposals.length; i++ ) {
                            var searchProposal = searchProposals[i];
                            var propid = searchProposal.getValue('internalid',null,'GROUP');
                            if(!inArray(propid,arrPropQue)){
                                arrPropQue.push(propid);
                            }
                        }

                        var arrColumns = new Array();
                        arrColumns.push(new nlobjSearchColumn('internalid',null,null));
                        var arrFilters = new Array();
                        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_proposals_to_sos_propid",null,"anyof",arrPropQue));
                        arrFilters.push(new nlobjSearchFilter("custrecord_clgx_proposals_to_sos_done",null,"is",'F'));
                        var searchQueuedProposal = nlapiSearchRecord('customrecord_clgx_proposals_to_sos', null, arrFilters, arrColumns);

                        if(searchQueuedProposal != null){
                            proposalLabel = '<span style="color:red;font-weight:bold">No service order will be created. This proposal, or another one, created from the same opportunity, is already in the processing queue.</span>';

                        }
                        else{
                            var userid = nlapiGetUser();
                            // put proposal in processing proposal queue
                            var record = nlapiCreateRecord('customrecord_clgx_proposals_to_sos');
                            record.setFieldValue('custrecord_clgx_proposals_to_sos_propid', proposalid);
                            record.setFieldValue('custrecord_clgx_proposals_to_sos_userid', userid);
                            var idRec = nlapiSubmitRecord(record, false, true);

                            var arrParam = new Array();
                            arrParam['custscript_proposalid'] = proposalid;
                            arrParam['custscript_userid'] = userid;
                            var status = nlapiScheduleScript('customscript_clgx_ss_so', null ,arrParam);
                            proposalLabel = '<span style="color:green;font-weight:bold">The Proposal has been updated and the Service Orders are being created.  You will receive an email confirmation when this process is completed.</span>';
                        }
                    }
                }
                else{
                    var proposalLabel = 'Editing Proposal ';
                }

                var arrProposal = getProposalJSON (proposalid);
                var objFile = nlapiLoadFile(1176694);
                var html = objFile.getValue();
                html = html.replace(new RegExp('{proposalRec}','g'),arrProposal[0]);
                html = html.replace(new RegExp('{dataContacts}','g'),arrProposal[1]);
                html = html.replace(new RegExp('{proposalLabel}','g'), proposalLabel);
                html = html.replace(new RegExp('{proposalid}','g'), proposalid);

            }
        }
        else{
            var html = '';
            html += '<table cellpadding="5" border="0" style="font-family: verdana,arial,sans-serif;font-size:12px;text-align:justify;">';
            html += '<tr>';
            html += '<td align="center" valign="top"><img src="//www.cologix.com/images/icon/famfamfam_silk_icons/icons/application_home.png" alt="Edit" height="16" width="16"></td>';
            html += '<td valign="top">Opportunity # - List of all renewed opportunities.</td>';
            html += '<tr>';
            html += '</tr>';
            html += '<tr>';
            html += '<td align="center" valign="top"><img src="//www.cologix.com/images/icon/famfamfam_silk_icons/icons/application_view_tile.png" alt="Edit" height="16" width="16"></td>';
            html += '<td valign="top">Proposal # - List of all proposals created for each opportunity</td>';
            html += '<tr>';
            html += '</tr>';
            html += '<tr>';
            html += '<td align="center" valign="top"><img src="//www.cologix.com/images/icon/famfamfam_silk_icons/icons/folder.png" alt="Edit" height="16" width="16"></td>';
            html += '<td valign="top">Created From SOs - List of all service orders renewed on that specific proposal.</td>';
            html += '<tr>';
            html += '</tr>';
            html += '<tr>';
            html += '<td align="center" valign="top"><img src="//www.cologix.com/images/icon/famfamfam_silk_icons/icons/folder.png" alt="Edit" height="16" width="16"></td>';
            html += '<td valign="top">Promoted to SOs - List of all service orders created from that specific proposal.</td>';
            html += '<tr>';
            html += '</tr>';
            html += '<tr>';
            html += '<td align="center" valign="top"><img src="//www.cologix.com/images/icon/famfamfam_silk_icons/icons/application_edit.png" alt="Edit" height="16" width="16"></td>';
            html += '<td valign="top">Please click on this icon to edit a proposal. If you do not see this icon, it means that this proposal, or another one created from the same opportunity, was already promoted to a service order and you will not be able to edit it any anymore.</td>';
            html += '<tr>';
            html += '</tr>';
            html += '<td align="center" valign="top"><img src="//www.cologix.com/images/icon/application_pdf.png" alt="BQ" height="16" width="16"></td>';
            html += '<td valign="top">Please click on this icon to download the last created budgetary quote. If you do not see this icon, it means that no budgetary quote has been created yet.</td>';
            html += '</tr>';
            html += '<tr>';
            html += '<td align="center" valign="top"><img src="//www.cologix.com/images/icon/acrobat_reader_16x16.gif" alt="Contract" height="16" width="16"></td>';
            html += '<td valign="top">Please click on this icon to download the last created contract. If you do not see this icon, it means that no contract has been created yet.</td>';
            html += '<tr>';
            html += '</tr>';
            html += '<td align="center" valign="top"><img src="//www.cologix.com/images/icon/icon_box.png" alt="Box" height="16" width="16"></td>';
            html += '<td valign="top">Please click on this icon to display the customer folder from Box and upload the signed contract.</td>';
            html += '</tr>';
            html += '</table>';
        }
        response.write( html );
    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
    	nlapiLogExecution("ERROR", "error", JSON.stringify(error));
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function getProposalJSON (proposalid){

    var recProposal = nlapiLoadRecord('estimate', proposalid);


    var bfileurl = ''
    var bfileid = recProposal.getFieldValue('custbody_clgx_prop_bq_file_id');
    if(bfileid != null && bfileid !=''){
        try {
            var bobjFile = nlapiLoadFile(bfileid);
            bfileurl = '<a href="' + bobjFile.getURL() + '" target="_blank"><img src="//www.cologix.com/images/icon/application_pdf.png" alt="BQ" height="16" width="16"></a>';
        }
        catch (e) {
            var str = String(e);
            if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
                bfileid = 0;
            }
        }
    }

    var cfileurl = '';
    var cfileid = recProposal.getFieldValue('custbody_clgx_prop_contract_file_id');
    if(cfileid != null && cfileid !=''){
        try {
            var cobjFile = nlapiLoadFile(cfileid);
            cfileurl = '<a href="' + cobjFile.getURL() + '" target="_blank"><img src="//www.cologix.com/images/icon/acrobat_reader_16x16.gif" alt="BQ" height="16" width="16"></a>';
        }
        catch (e) {
            var str = String(e);
            if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
                cfileid = 0;
            }
        }
    }

    var objProposal = new Object();
    var objData = new Object();

    objData["customerid"] = parseInt(recProposal.getFieldValue('entity'));
    objData["customer"] = recProposal.getFieldText('entity');
    objData["proposalid"] = parseInt(proposalid);
    objData["proposal"] = recProposal.getFieldValue('tranid');
    objData["status"] = parseInt(recProposal.getFieldValue('entitystatus'));
    objData["title"] = recProposal.getFieldValue('title');
    objData["closedate"] = recProposal.getFieldValue('expectedclosedate');
    objData["forecasttype"] = parseInt(recProposal.getFieldValue('forecasttype'));
    objData["lossreason"] = parseInt(recProposal.getFieldValue('custbody_clgx_winlossreason'));
    objData["contact"] = parseInt(recProposal.getFieldValue('custbody_clgx_contract_terms_contact'));
    objData["bfileurl"] = bfileurl;
    objData["cfileurl"] = cfileurl;

    if(recProposal.getFieldValue('custbody_clgx_uploaded_to_box') == 'T'){
        objData["tobox"] = true;
    }
    else{
        objData["tobox"] = false;
    }
    objProposal['data'] = objData;

    // build contacts grid JSON
    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid', 'contact', null));
    arrColumns.push(new nlobjSearchColumn('entityid', 'contact', null));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('internalid',null,'is',recProposal.getFieldValue('entity')));
    arrFilters.push(new nlobjSearchFilter('isinactive','contact','is','F'));
    var searchContacts = nlapiSearchRecord('customer', null, arrFilters, arrColumns);

    var arrContacts = new Array();
    for ( var k = 0; searchContacts != null && k < searchContacts.length; k++ ) {
        var searchContact = searchContacts[k];
        var contactid = searchContact.getValue('internalid','contact', null);
        var contact = searchContact.getValue('entityid','contact', null);

        var objContact = new Object();
        objContact["value"] = parseInt(contactid);
        objContact["text"] = contact;
        arrContacts.push(objContact);
    }

    // put all 3 JSONs on one return
    var arrProposal = new Array;
    arrProposal.push(JSON.stringify(objProposal));
    arrProposal.push(JSON.stringify(arrContacts));
    return arrProposal;
}


function create_contract(proposalid, type){

    var contractPDF = 'F';
    var quotePDF =  'F';
    if(type == 'bq'){
        quotePDF = 'T';
    }
    else{
        contractPDF = 'T';
    }


    if (contractPDF == 'T' || quotePDF == 'T') {

        var linkToFolder = '/app/common/media/mediaitemfolders.nl?folder=';
        var fileFolder = get_contract_fileFolder();
        linkToFolder += fileFolder;

        var recProposal = nlapiLoadRecord('estimate', proposalid);
        var stSONbr = recProposal.getFieldValue('tranid');
        var stSORep = recProposal.getFieldText('salesrep');
        var stSOCustId = recProposal.getFieldValue('entity');
        var stSOAddr = recProposal.getFieldValue('billaddress');
        var stCurrency = recProposal.getFieldValue('currency');
        var stSOAtt = recProposal.getFieldValue('custbody_clgx_contract_terms_attention');
        var stSON = recProposal.getFieldValue('custbody_clgx_contract_terms_notes');
        if(stSON == null){
            stSON = '';
        }
        nlapiLogExecution('DEBUG','stSONotes = ' + recProposal.getFieldValue('custbody_clgx_contract_terms_notes'));
        var stSONotes = convertBR(stSON);
        stSONotes = stSONotes.replace(/\&/g," and ");
        var stSOTerms = convertBR(recProposal.getFieldValue('custbody_clgx_contract_terms_body'));

        var stSOTitle = recProposal.getFieldValue('custbody_clgx_contract_terms_title');
        var stSOLang = nlapiLookupField('customrecord_clgx_contract_terms', stSOTitle,'custrecord_clgx_contract_terms_lang');

        var arrSOs = recProposal.getFieldValues('custbody_clgx_renewed_from_sos');
        var arrSOsNames = recProposal.getFieldTexts('custbody_clgx_renewed_from_sos');

        //Determine currency
        var currencyType = '';
        if (stCurrency == 1){
            currencyType = 'USD';
        }
        else if (stCurrency == 3){
            currencyType = 'CDN';
        }
        else{}

        var arrCustomerDetails = nlapiLookupField('customer', stSOCustId, ['companyname', 'billaddress1', 'billaddress2', 'billcity', 'billcountry', 'billstate', 'billzipcode', 'language']);
        var custCompName = arrCustomerDetails['companyname'];
        custCompName = custCompName.replace(/\&/g,"and");

        var billAddress1 = arrCustomerDetails['billaddress1'];
        var billAddress2 = arrCustomerDetails['billaddress2'];
        var billCity = arrCustomerDetails['billcity'];
        var billCountry = arrCustomerDetails['billcountry'];
        var billState = arrCustomerDetails['billstate'];
        var billZipcode = arrCustomerDetails['billzipcode'];
        var billLanguage = arrCustomerDetails['language'];

        if (stSOLang == 17){ // if French
            var stLang = 1;
            var langPrefix = 'FR_';
            if (quotePDF == 'T'){
                var filePrefix = 'BQ_';
                var template = 346192; // Load French Quote Template
            }
            else{
                var filePrefix = 'SO_';
                var template = 23046; // Load French Contract Template
            }
        }
        else { // if English
            var stLang = 0;
            var langPrefix = 'EN_';
            if (quotePDF == 'T'){
                var filePrefix = 'BQ_';
                var template = 346191; // Load English Quote Template
            }
            else{
                var filePrefix = 'SO_';
                var template = 23041; // Load English Contract Template
            }
        }

        var arrTitleBQ = new Array();
        arrTitleBQ[0] = 'Quote Details';
        arrTitleBQ[1] = 'D&eacute;tails des Services';

        var arrTitleContract = new Array();
        arrTitleContract[0] = 'Services Details';
        arrTitleContract[1] = 'D&eacute;tails des Services';

        var arrTitleBQTotals = new Array();
        arrTitleBQTotals[0] = 'Quote Totals';
        arrTitleBQTotals[1] = 'D&eacute;tails des Services - Totaux ';

        var arrTitleContractTotals = new Array();
        arrTitleContractTotals[0] = 'Services Totals';
        arrTitleContractTotals[1] = 'D&eacute;tails des Services - Totaux ';

        var arrTitleMRC = new Array();
        arrTitleMRC[0] = 'RECURRING CHARGES';
        arrTitleMRC[1] = 'FRAIS R&Eacute;CURRENTS';

        var arrTitleNRC = new Array();
        arrTitleNRC[0] = 'NON RECURRING CHARGES';
        arrTitleNRC[1] = 'FRAIS NON R&Eacute;CURRENTS';

        var arrFootMRC = new Array();
        arrFootMRC[0] = 'Total Recurring Charges';
        arrFootMRC[1] = 'Total des frais r&eacute;currents';

        var arrFootNRC = new Array();
        arrFootNRC[0] = 'Total Non Recurring Charges';
        arrFootNRC[1] = 'Total des frais non r&eacute;currents';

        var arrFootAllRC = new Array();
        arrFootAllRC[0] = 'Grand Total Recurring Charges';
        arrFootAllRC[1] = 'Grand total des frais r&eacute;currents';

        var arrFootAllNRC = new Array();
        arrFootAllNRC[0] = 'Grand Total Non Recurring Charges';
        arrFootAllNRC[1] = 'Grand total des frais non r&eacute;currents';

        //
        var arrAttn = new Array();
        arrAttn[0] = 'Attn';
        arrAttn[1] = '&Agrave; l\'attention de';

        var arrDescr = new Array();
        arrDescr[0] = 'Description';
        arrDescr[1] = 'Description';

        var arrTerms = new Array();
        arrTerms[0] = 'Service Term';
        arrTerms[1] = 'P&eacute;riode de service';

        var arrQty = new Array();
        arrQty[0] = 'Qty';
        arrQty[1] = 'Quantit&eacute;';

        var arrRate = new Array();
        arrRate[0] = 'Rate';
        arrRate[1] = 'Tarif';

        var arrAmount = new Array();
        arrAmount[0] = 'Amount';
        arrAmount[1] = 'Montant';

        var arrNotes = new Array();
        arrNotes[0] = 'Notes';
        arrNotes[1] = 'Notes';

        var arrFromSOs = new Array();
        arrFromSOs[0] = 'This order is replacing the following Service Orders';
        arrFromSOs[1] = 'Cette commande remplace les ordres de service suivants';


        // start items sections --------------------------------------------------------------------------------
        var tabledata = '';

        var nbrItems = recProposal.getLineItemCount('item');
        // build array of unique locations
        var arrLocations = new Array();
        for (var i = 0; i < nbrItems; i++){
            var locid = recProposal.getLineItemValue('item','location', i + 1)
            if(!inArray(locid,arrLocations)){
                arrLocations.push(locid);
            }
        }

        // loop unique locations
        var grandTotalRC = 0;
        var grandTotalNRC = 0;
        for (var l = 0; l < arrLocations.length; l++){
            var arrLocation = nlapiLookupField('location', arrLocations[l], ['name', 'address1', 'address2', 'city', 'country', 'state', 'zip']);
            var stLocName = arrLocation['name'];
            stLocName = stLocName.replace(/\&/g,"and");

            var arrNRC = new Array();
            var arrRC = new Array();
            for (var m = 0; m < parseInt(nbrItems); m++) {
                var stBS = nlapiGetLineItemText('item', 'billingschedule', m + 1);
                if (stBS == 'Non Recurring') {
                    arrNRC.push(m + 1);
                }
                else {
                    arrRC.push(m + 1);
                }
            }

            tabledata += '<table class="profile" align="center">';
            tabledata += '<tr>';
            if (quotePDF == 'T'){
                tabledata += '<td><h2>' + arrTitleBQ[stLang] + ' / ' + stLocName + '</h2></td>';
            }
            else{
                tabledata += '<td><h2>' + arrTitleContract[stLang] + ' / ' + stLocName + '</h2></td>';
            }
            tabledata += '</tr>';
            tabledata += '</table>';
            tabledata += '<table cellpadding="1" class="profile" align="center" table-layout="fixed">';
            tabledata += '<tr>';
            tabledata += '<td width="30%">&nbsp;</td>';
            tabledata += '<td width="40%">&nbsp;</td>';
            tabledata += '<td width="10%">&nbsp;</td>';
            tabledata += '<td width="10%">&nbsp;</td>';
            tabledata += '<td width="10%">&nbsp;</td>';
            tabledata += '</tr>';


            if (arrRC.length > 0) { // there are recurring items, so start recurring block --------------------------------------------------
                tabledata += '<tr>';
                tabledata += '<td colspan="5">' + arrTitleMRC[stLang] + '</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td colspan="5">&nbsp;</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td class="cool"><b>' + arrDescr[stLang] + '</b></td>';
                tabledata += '<td class="cool"><b>' + arrTerms[stLang] + '</b></td>';
                tabledata += '<td class="cool" align="center"><b>' + arrQty[stLang] + '</b></td>';
                tabledata += '<td class="cool" align="right"><b>' + arrRate[stLang] + '</b></td>';
                tabledata += '<td class="cool" align="right"><b>' + arrAmount[stLang] + '</b></td>';
                tabledata += '</tr>';

                var totalRC = 0;
                for (var j = 0; j < nbrItems; ++j) {

                    var currentLocation = recProposal.getLineItemValue('item', 'location', j + 1);
                    var currentBS = recProposal.getLineItemText('item', 'billingschedule', j + 1);

                    if(currentLocation == arrLocations[l] && currentBS != 'Non Recurring'){

                        var stItemId = recProposal.getLineItemValue('item', 'item', j + 1);
                        var stTerms = recProposal.getLineItemText('item', 'billingschedule', j + 1);
                        var recItem = nlapiLoadRecord('serviceitem', stItemId);
                        var stName = recItem.getFieldValue('displayname');

                        if(stLang != 0){
                            var myItemRecord = nlapiLoadRecord('serviceitem', stItemId);
                            var frenchName = myItemRecord.getLineItemValue('translations', 'displayname', 3);
                            if (frenchName != null && frenchName != ''){
                                stName = frenchName;
                            }
                        }

                        var stQty = recProposal.getLineItemValue('item', 'quantity', parseInt(j + 1));
                        var stRate = recProposal.getLineItemValue('item', 'rate', parseInt(j + 1));
                        var stAmnt = recProposal.getLineItemValue('item', 'amount', parseInt(j + 1));

                        if (stItemId != 210) {
                            totalRC += parseFloat(stAmnt);
                            grandTotalRC += parseFloat(stAmnt);
                        }

                        tabledata += '<tr>';
                        tabledata += '<td>' + nlapiEscapeXML(stName) + '</td>';
                        if (stItemId == 210) {
                            tabledata += '<td>&nbsp;</td>';
                        }
                        else{
                            tabledata += '<td>' + nlapiEscapeXML(stTerms) + '</td>';
                        }

                        if (stQty == null || stItemId == 210) {
                            tabledata += '<td align="right">&nbsp;</td>';
                        }
                        else {
                            tabledata += '<td align="center">' + nlapiEscapeXML(parseInt(stQty)) + '</td>';
                        }
                        if (stRate == null || stRate == '') {
                            tabledata += '<td align="right">&nbsp;</td>';
                        }
                        else {
                            tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stRate)) + '</td>';
                        }
                        if (stAmnt == null || stItemId == 210) {
                            tabledata += '<td align="right">&nbsp;</td>';
                        }
                        else {
                            tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stAmnt)) + '</td>';
                        }
                        tabledata += '</tr>';
                    }

                }

                tabledata += '<tr>';
                tabledata += '<td colspan="5">&nbsp;</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td colspan="5" class="profile t-border" align="right"><b>' + arrFootMRC[stLang] + ' (' + stLocName + '): ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(totalRC).toFixed(2))) + '</b></td>';
                tabledata += '</tr>';



            } // end recurring block ---------------------------------------------------------------------------

            if (arrNRC.length > 0) { // start non recurring block -----------------------------------------------

                tabledata += '<tr>';
                tabledata += '<td colspan="5">' + arrTitleNRC[stLang] + '</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td colspan="5">&nbsp;</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td class="cool" colspan="2"><b>' + arrDescr[stLang] + '</b></td>';
                tabledata += '<td class="cool" align="center"><b>' + arrQty[stLang] + '</b></td>';
                tabledata += '<td class="cool" align="right"><b>' + arrRate[stLang] + '</b></td>';
                tabledata += '<td class="cool" align="right"><b>' + arrAmount[stLang] + '</b></td>';
                tabledata += '</tr>';

                var totalNRC = 0;
                for (var k = 0; k < nbrItems; ++k) {

                    var currentLocation = recProposal.getLineItemValue('item', 'location', k + 1);
                    var currentBS = recProposal.getLineItemText('item', 'billingschedule', k + 1);

                    if(currentLocation == arrLocations[l] && currentBS == 'Non Recurring'){

                        var stItemId = recProposal.getLineItemValue('item', 'item', parseInt(k + 1));
                        var recItem = nlapiLoadRecord('serviceitem', stItemId);
                        var stName = recItem.getFieldValue('displayname');

                        if(stLang != 0){
                            var myItemRecord = nlapiLoadRecord('serviceitem', stItemId);
                            var frenchName = myItemRecord.getLineItemValue('translations', 'displayname', 3);
                            if (frenchName != null && frenchName != ''){
                                stName = frenchName;
                            }
                        }

                        var stQty = recProposal.getLineItemValue('item', 'quantity', parseInt(k + 1));
                        var stRate = recProposal.getLineItemValue('item', 'rate', parseInt(k + 1));
                        var stAmnt = recProposal.getLineItemValue('item', 'amount', parseInt(k + 1));

                        totalNRC += parseFloat(stAmnt);
                        garndTotalNRC += parseFloat(stAmnt);

                        tabledata += '<tr>';
                        tabledata += '<td colspan="2">' + nlapiEscapeXML(stName) + '</td>';
                        if (stQty == null || stQty == '') {
                            tabledata += '<td align="right">&nbsp;</td>';
                        }
                        else {
                            tabledata += '<td align="center">' + nlapiEscapeXML(parseInt(stQty)) + '</td>';
                        }
                        if (stRate == null || stRate == '') {
                            tabledata += '<td align="right">&nbsp;</td>';
                        }
                        else {
                            tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stRate)) + '</td>';
                        }
                        if (stAmnt == null || stAmnt == '') {
                            tabledata += '<td align="right">&nbsp;</td>';
                        }
                        else {
                            tabledata += '<td align="right">$' + nlapiEscapeXML(addCommas(stAmnt)) + '</td>';
                        }
                        tabledata += '</tr>';
                    }
                }

                tabledata += '<tr>';
                tabledata += '<td colspan="5">&nbsp;</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td colspan="5" class="profile t-border" align="right"><b>' + arrFootNRC[stLang] + ' (' + stLocName + '): ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(totalNRC).toFixed(2))) + '</b></td>';
                tabledata += '</tr>';
            } // end non recurring block ------------------------------------------------------------------------

            tabledata += '</table>';

        }

        if(parseInt(arrLocations.length) > 1 && (parseInt(grandTotalRC) > 0 || parseInt(grandTotalNRC) > 0)){

            //tabledata += '<br />';


            tabledata += '<table class="profile page-break" align="center">';
            tabledata += '<tr>';
            if (quotePDF == 'T'){
                tabledata += '<td><h2>' + arrTitleBQTotals[stLang] + '</h2></td>';
            }
            else{
                tabledata += '<td><h2>' + arrTitleContractTotals[stLang] + '</h2></td>';
            }
            tabledata += '</tr>';
            tabledata += '</table>';

            tabledata += '<table cellpadding="1" class="profile" align="center" table-layout="fixed">';


            if(parseInt(grandTotalRC) > 0){
                tabledata += '<tr>';
                tabledata += '<td>&nbsp;</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td class="profile t-border" align="right"><b>' + arrFootAllRC[stLang] + ': ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(grandTotalRC).toFixed(2))) + '</b></td>';
                tabledata += '</tr>';
            }
            if(parseInt(grandTotalNRC) > 0){
                tabledata += '<tr>';
                tabledata += '<td>&nbsp;</td>';
                tabledata += '</tr>';
                tabledata += '<tr>';
                tabledata += '<td class="profile t-border" align="right"><b>' + arrFootAllNRC[stLang] + ': ' + currencyType + ' $' + nlapiEscapeXML(addCommas(parseFloat(grandTotalNRC).toFixed(2))) + '</b></td>';
                tabledata += '</tr>';
            }
            tabledata += '</table>';
        }


        //build customer address
        var tablehtmlAddress = '';
        tablehtmlAddress = '<table cellpadding="0" border="0" table-layout="fixed">';
        tablehtmlAddress += '<tr>';
        tablehtmlAddress += '<td>';
        tablehtmlAddress += '<b>' + nlapiEscapeXML(custCompName) + '</b><br/>';
        tablehtmlAddress += '<b>' + arrAttn[stLang] + ':</b>' + nlapiEscapeXML(stSOAtt) + '<br/>';
        tablehtmlAddress += nlapiEscapeXML(billAddress1) + '<br/>';
        //if (billAddress2) {
        //	tablehtmlAddress += nlapiEscapeXML(billAddress2) + '<br/>';
        //}
        tablehtmlAddress += nlapiEscapeXML(billCity) + ', ' + nlapiEscapeXML(billState) + ' ' + nlapiEscapeXML(billZipcode) + ' <br/>';
        tablehtmlAddress += nlapiEscapeXML(billCountry);
        tablehtmlAddress += '</td>';
        tablehtmlAddress += '</tr>';
        tablehtmlAddress += '</table>';

        //build data center/location address

        var arrLocation = nlapiLookupField('location', arrLocations[0], ['name', 'address1', 'address2', 'city', 'country', 'state', 'zip']);
        var stLocName = arrLocation['name'];
        var stLocAddr1 = arrLocation['address1'];
        var stLocAddr2 = arrLocation['address2'];
        var stLocCity = arrLocation['city'];
        var stLocCountry = arrLocation['country'];
        var stLocState = arrLocation['state'];
        var stLocZip = arrLocation['zip'];

        var tablehtmlLocation = '<b>';
        tablehtmlLocation += nlapiEscapeXML(stLocName) + '</b><br/>';
        tablehtmlLocation += nlapiEscapeXML(stLocAddr1) + '<br/>';
        if (stLocAddr2) {
            tablehtmlLocation += nlapiEscapeXML(stLocAddr2) + '<br/>';
        }
        tablehtmlLocation += nlapiEscapeXML(stLocCity) + ', ' + nlapiEscapeXML(stLocState) + ' ' + nlapiEscapeXML(stLocZip) + ' <br/>';
        tablehtmlLocation += nlapiEscapeXML(stLocCountry);


        //build Notes
        var tablehtmlNotes = '';
        if (stSONotes != '') {
            tablehtmlNotes += '<table cellpadding="2" class="profile" align="center" table-layout="fixed" style="page-break-inside: avoid;">';

            // if there is something in Notes
            if(stSONotes != ''){
                tablehtmlNotes += '<tr><td><h2>' + arrNotes[stLang] + '</h2></td></tr>';
                tablehtmlNotes += '<tr><td class="profile t-border" style="font-family: Arial, Helvetica, sans-serif; font-size:7pt;">'+ stSONotes + '</td></tr>';
            }
            tablehtmlNotes += '</table>';
            tablehtmlNotes += '<br/>';
        }

        //build SOs
        var tablehtmlSOs = '';
        if(arrSOs != null){
            if (arrSOs.length > 0) {
                tablehtmlSOs += '<table cellpadding="2" class="profile" align="center" table-layout="fixed" style="page-break-inside: avoid;">';
                // if a renewal, print SOs
                if(arrSOs.length > 0){
                    var txtNotes = '';
                    for (var i = 0; i < arrSOs.length; i++){
                        txtNotes += arrSOsNames[i] + ', ';
                    }
                    txtNotes = txtNotes.slice(0,txtNotes.length-2);
                    tablehtmlSOs += '<tr><td><h2>' + arrFromSOs[stLang] + '</h2></td></tr>';
                    tablehtmlSOs += '<tr><td class="profile t-border" style="font-family: Arial, Helvetica, sans-serif; font-size:7pt;">' + txtNotes + '</td></tr>';
                }
                tablehtmlSOs += '</table>';
                tablehtmlSOs += '<br/>';
            }
        }


        var objFile = nlapiLoadFile(template);
        var stMainHTML = objFile.getValue();

        var d = new moment();

        stMainHTML = stMainHTML.replace(new RegExp('{location}', 'g'), tablehtmlLocation);
        stMainHTML = stMainHTML.replace(new RegExp('{billAddress}', 'g'), tablehtmlAddress);
        stMainHTML = stMainHTML.replace(new RegExp('{sodate}', 'g'), d.format('M/D/YYYY'));
        stMainHTML = stMainHTML.replace(new RegExp('{sonbr}', 'g'), stSONbr);
        stMainHTML = stMainHTML.replace(new RegExp('{sorep}', 'g'), stSORep);
        stMainHTML = stMainHTML.replace(new RegExp('{totalcharges}', 'g'), '');
        stMainHTML = stMainHTML.replace(new RegExp('{items}', 'g'), tabledata);
        stMainHTML = stMainHTML.replace(new RegExp('{notes}', 'g'), tablehtmlNotes);
        stMainHTML = stMainHTML.replace(new RegExp('{sos}', 'g'), tablehtmlSOs);
        stMainHTML = stMainHTML.replace(new RegExp('{terms}', 'g'), stSOTerms);

        stMainHTML = stMainHTML.replace(new RegExp('{ipburst}', 'g'), '');
        stMainHTML = stMainHTML.replace(new RegExp('{usage}', 'g'), '');

        var filePDF = nlapiXMLToPDF(stMainHTML);
        custCompName = custCompName.replace(/\ /g,"_");
        filePDF.setName(filePrefix + langPrefix + stSONbr + '_' + custCompName + '_' + d.format('M/D/YYYY') + '_' + d.format('H-m-s') + '_' + currencyType + '.pdf');
        filePDF.setFolder(fileFolder);

        var fileId = nlapiSubmitFile(filePDF);

        if (quotePDF == 'T'){
            nlapiSubmitField('estimate', proposalid, 'custbody_clgx_prop_bq_file_id', fileId);
        }
        else{
            nlapiSubmitField('estimate', proposalid, 'custbody_clgx_prop_contract_file_id', fileId);
        }

        nlapiSubmitField('estimate', proposalid, 'custbody_clgx_contract_terms_ready', 'F');
        nlapiSubmitField('estimate', proposalid, 'custbody_clgx_print_budgetray_quote', 'F');
    }
    return fileId;
}

function inArray(val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}
