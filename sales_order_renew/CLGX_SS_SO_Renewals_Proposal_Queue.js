/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 May 2019     Alex
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function execute(context) {
	var sc = nlapiGetContext();
	var customerID   = sc.getSetting("SCRIPT", "custscript_clgx_1823_customerid");
	var proposalID   = sc.getSetting("SCRIPT", "custscript_clgx_1823_proposalid");
	var soArray      = sc.getSetting("SCRIPT", "custscript_clgx_1823_sos");
	
	var splitSOArray = soArray.split("\u0005");
	
	for ( var so = 0; so < splitSOArray.length; so++ ) {
		nlapiLogExecution("DEBUG", so, splitSOArray[so]);
        var record = nlapiCreateRecord('customrecord_clgx_que_renew_sos_proposal'); // 2Points
        record.setFieldValue('custrecord_clgx_que_renew_prop_customer', customerID);
        record.setFieldValue('custrecord_clgx_que_renew_prop_prop', proposalID);
        record.setFieldValue('custrecord_clgx_que_renew_prop_so', splitSOArray[so]);
        var idRec = nlapiSubmitRecord(record, true,true); // 4Points
    }
}
