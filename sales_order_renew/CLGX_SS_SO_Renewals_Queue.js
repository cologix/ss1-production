nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SS_SO_Renewals_Queue.js
//	Script Name:	CLGX_SS_SO_Renewals_Queue
//	Script Id:		customscript_clgx_ss_so_renew_queue
//	Script Runs:	On Server
//	Script Type:	Scheduled Script
//	Deployments:	Service Order
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Released:		3/13/2014
//-------------------------------------------------------------------------------------------------
function scheduled_so_renew_queue (){
    try{
//------------- Begin Section 1 -----------------------------------------------------------------------------------
// Details:	Update renewed SOs with opportunity and proposal
//-----------------------------------------------------------------------------------------------------------------
    	
        nlapiLogExecution('DEBUG','Started Execution', '|--------------------------Begin Scheduled Script--------------------------|'); 

        // opportunities to be updated on SOs ---------------------------------------------------------------------------
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_que_renew_oppty_oppty',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_que_renew_oppty_so',null,null));
		var arrFilters = new Array();
		var searchOpptySOs = nlapiSearchRecord('customrecord_clgx_que_renew_sos_oppty', null, arrFilters, arrColumns);

        for ( var i = 0; searchOpptySOs != null && i < searchOpptySOs.length; i++ ) {
			var searchOpptySO = searchOpptySOs[i];
			var recid  = searchOpptySO.getValue('internalid', null, null);
			var opptyid  = searchOpptySO.getValue('custrecord_clgx_que_renew_oppty_oppty', null, null);
			var soid  = searchOpptySO.getValue('custrecord_clgx_que_renew_oppty_so', null, null);
		    nlapiSubmitField('salesorder', soid, 'custbody_clgx_so_renewed_on_oppty',opptyid);
		    nlapiDeleteRecord('customrecord_clgx_que_renew_sos_oppty', recid );
		    
		    nlapiLogExecution('DEBUG','Processed', '| SO = ' + soid + ' | Oppty = ' + opptyid + ' |');
		}

        // proposals to be updated on SOs ---------------------------------------------------------------------------
		var arrColumns = new Array();
		arrColumns.push(new nlobjSearchColumn('internalid',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_que_renew_prop_prop',null,null));
		arrColumns.push(new nlobjSearchColumn('custrecord_clgx_que_renew_prop_so',null,null));
		var arrFilters = new Array();
		var searchOpptySOs = nlapiSearchRecord('customrecord_clgx_que_renew_sos_proposal', null, arrFilters, arrColumns);

        for ( var i = 0; searchOpptySOs != null && i < searchOpptySOs.length; i++ ) {
			var searchOpptySO = searchOpptySOs[i];
			var recid  = searchOpptySO.getValue('internalid', null, null);
			var proposalid  = searchOpptySO.getValue('custrecord_clgx_que_renew_prop_prop', null, null);
			var soid  = searchOpptySO.getValue('custrecord_clgx_que_renew_prop_so', null, null);
		    nlapiSubmitField('salesorder', soid, 'custbody_clgx_so_renewed_on_proposal',proposalid);
		    nlapiDeleteRecord('customrecord_clgx_que_renew_sos_proposal', recid );
		    
		    nlapiLogExecution('DEBUG','Processed', '| SO = ' + soid + ' | Proposal = ' + proposalid + ' |');
		}

        nlapiLogExecution('DEBUG','Finished Execution', '|-------------------------- Finished Scheduled Script --------------------------|');

    }
    catch (error){
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    }
}

