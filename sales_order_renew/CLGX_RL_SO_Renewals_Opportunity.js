nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_RL_SO_Renewals_Opportunity.js
//	Script Name:	CLGX_RL_SO_Renewals_Opportunity
//	Script Id:		customscript_clgx_rl_so_renewals_oppty
//	Script Nbr:		265
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		01/23/2014
//	RelativeURL:	/app/site/hosting/restlet.nl?script=286&deploy=1
//-------------------------------------------------------------------------------------------------

function restlet_so_renewals_oppty (datain){
    try {

		datain = JSON.parse(datain);
		var customerid = datain['customerid'];
		var arrSOs = datain['sosids'].split(",");
		
		/*
    	response.write(countAmps);
    	response.write('<br/>');
    	response.write(countMb);
    	response.write('<br/>');    	
    	response.write(countAll);
    	response.write('<br/><br/>');

    	//return JSON.stringify(arrSOs, null, 4);
    	
        var txtArg = datain;
        txtArg = txtArg.replace(/\"/g,"");
        txtArg = txtArg.replace(/\:/g,",");
        txtArg = txtArg.replace(/\{/g,"");
        txtArg = txtArg.replace(/\}/g,"");
        var arrArguments = new Array();
        arrArguments = txtArg.split( "," );

        var customerid = arrArguments[1];

        var arrSOs = new Array();
        for ( var i = 3;  i < arrArguments.length; i++ ) {
            if (!inArray(arrArguments[i],arrSOs)){
                arrSOs.push(arrArguments[i]);
            }
        }
    	 */
    	
    	
        // check if any inactive item is included in the selected SOs
        var arrColumns = new Array();
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrSOs));
        var searchInactives = nlapiSearchRecord('transaction', 'customsearch_clgx_sos_with_inactives', arrFilters, arrColumns);

        if(searchInactives != null){
            var html = 'You can not include a service order containing an inactive item or service.';
        }
        else{

            var opptyid = 0;
            // look for renewal opportunities for that customer
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('custbody_clgx_so_renewed_on_oppty',null,'GROUP'));
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("entity",null,"anyof",customerid));
            arrFilters.push(new nlobjSearchFilter("custbody_clgx_so_renewed_on_oppty",null,"noneof",'@NONE@')); // verify if an opportunity exist
            arrFilters.push(new nlobjSearchFilter("custbody_clgx_so_renewed_on_so",null,"anyof",'@NONE@'));
            var searchOppty = nlapiSearchRecord('salesorder', 'customsearch_clgx_so_renewals_2', arrFilters, arrColumns);
            if(searchOppty != null){
                opptyid = searchOppty[0].getValue('custbody_clgx_so_renewed_on_oppty', null, 'GROUP');
                var recOppty = nlapiLoadRecord('opportunity', opptyid);
                var nbrClosed = recOppty.getLineItemCount('closed');
                if(parseInt(nbrClosed) > 0){ // if the opportunity have a closed SO, consider it to be closed too
                    opptyid = 0;
                }
            }
            // look for renewal opportunities for that customer in Processing queue in case it was not processed
            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('custrecord_clgx_que_renew_oppty_oppty',null,'GROUP'));
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("custrecord_clgx_que_renew_oppty_customer",null,"anyof",customerid));
            var searchOpptyQueue = nlapiSearchRecord('customrecord_clgx_que_renew_sos_oppty', null, arrFilters, arrColumns);
            if(searchOpptyQueue != null){
                opptyid = searchOpptyQueue[0].getValue('custrecord_clgx_que_renew_oppty_oppty', null, 'GROUP');
                var opptyStatus = nlapiLookupField('estimate', opptyid, 'status'); // this is in queue but maybe alrready has SOs
                if(opptyStatus == 'Closed - Won'){ // this opportunity has created SOs
                    opptyid = 0; // put it back to 0 to allow creation of another Oppty
                }
            }

            if (arrSOs == null){ // no SO was selected or posted
                if (opptyid > 0){ // this is an existing opportunity; just display it
                    var arrOpportunity = getOpportunityJSON (customerid,opptyid);
                    var objFile = nlapiLoadFile(999899);
                    var html = objFile.getValue();
                    html = html.replace(new RegExp('{opptyRec}','g'),arrOpportunity[0]);
                    html = html.replace(new RegExp('{dataItems}','g'),arrOpportunity[1]);
                    html = html.replace(new RegExp('{dataContacts}','g'),arrOpportunity[2]);
                }
                else{
                    var html = 'You must select at least one service order to create or update an opportunity.';
                }
            }
            else{
                if(parseInt(opptyid) > 0){ // Customer has an open opportunity; load it, then delete and replace items with new ones.
                    var recOppty = nlapiLoadRecord('opportunity', opptyid);
                    // remove all existing items
                    var nbrItemsOppty = recOppty.getLineItemCount('item');
                    for (var j = 0; j < nbrItemsOppty; j++){
                        recOppty.removeLineItem('item',1);
                    }
                }
                else { // Customer has no renewal opportunity; create one.
                    var recOppty = nlapiCreateRecord('opportunity');
                }

                var d = new moment();
                var today = d.format('M/D/YYYY');
                var salesrep = nlapiLookupField('customer', customerid, 'salesrep');

                recOppty.setFieldValue('entity', customerid);
                //recOppty.setFieldValue('title', 'Renewal');
                recOppty.setFieldText('custbody_cologix_opp_sale_type', 'Renewal');
                recOppty.setFieldValue('status', 'Qualified');
                recOppty.setFieldValue('probability', 10);
                recOppty.setFieldValue('expectedclosedate', '');
                recOppty.setFieldText('leadsource', 'Other');
                recOppty.setFieldText('forecasttype', 'Upside');
                recOppty.setFieldValue('salesrep', salesrep);
                recOppty.setFieldValues('custbody_clgx_renewed_from_sos', null);
                recOppty.setFieldValues('custbody_clgx_renewed_from_sos', arrSOs);
                recOppty.setFieldValue('custbody_clgx_renewed_oppty_no_prop', 1);

                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('rate',null,null));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",arrSOs));
                var searchItems = nlapiSearchRecord('salesorder', 'customsearch_clgx_so_renewals_items', arrFilters, arrColumns);
                var rateSum=parseFloat(0);
                var rateSumMRC=parseFloat(0);
                var arrLocation = new Array();
                for ( var i = 0; searchItems != null && i < searchItems.length; i++ ) {
                    var searchItem = searchItems[i];

                    var cls = searchItem.getValue('class',  null, null);
                    var clsTxt = searchItem.getText('class',  null, null);
                    var bs = searchItem.getValue('billingschedule',  null, null);
                    var locationid = searchItem.getValue('location',  null, null);

                    if(clsTxt.indexOf("Recurring") > -1 && bs != null && bs != ''){
                        recOppty.selectNewLineItem('item');
                        recOppty.setCurrentLineItemValue('item','custcol_clgx_oppty_col_service_id', searchItem.getValue('custcol_clgx_so_col_service_id',  null, null));
                        recOppty.setCurrentLineItemValue('item','item', searchItem.getValue('item',  null, null));
                        recOppty.setCurrentLineItemValue('item','custcol_clgx_oppty_billing_schedule', bs);
                        recOppty.setCurrentLineItemValue('item','quantity', searchItem.getValue('custcol_clgx_qty2print',  null, null));
                        recOppty.setCurrentLineItemValue('item','description', searchItem.getValue('memo',  null, null));
                        recOppty.setCurrentLineItemValue('item','price', searchItem.getValue('pricelevel',  null, null));
                        rateSum +=parseFloat(searchItem.getValue('rate',  null, null))*parseFloat(searchItem.getValue('custcol_clgx_qty2print',  null, null));
                        recOppty.setCurrentLineItemValue('item','rate', searchItem.getValue('rate',  null, null));
                        recOppty.setCurrentLineItemValue('item','custcol_cologix_oppty_item_taxcode', searchItem.getValue('taxcode',  null, null));
                        recOppty.setCurrentLineItemValue('item','options', searchItem.getValue('options',  null, null));
                        //recOppty.setCurrentLineItemValue('item','costestimatetype', searchItem.getValue('costestimatetype',  null, null));
                        recOppty.setCurrentLineItemValue('item','location', locationid);
                        recOppty.setCurrentLineItemValue('item','class', cls);
                        recOppty.setCurrentLineItemValue('item','custcol_cologix_oppty_item_category', searchItem.getValue('custcol_cologix_invoice_item_category',  null, null));
                        recOppty.commitLineItem('item');
                        if (!inArray(locationid,arrLocation)){
                            arrLocation.push(locationid);
                        }
                    }
                }
                if(arrLocation.length == 1){ // if only one location on oppoty items, write it also in the header
                    var arrFilterMRC=new Array();
                    var arrColMRC=new Array();
                    arrFilterMRC.push(new nlobjSearchFilter('custrecord_clgx_totals_transaction',null,'anyof', arrSOs));
                    var searchSOsMRC = nlapiSearchRecord('customrecord_clgx_totals_transaction','customsearch_clgx_ss_incr_mrc',arrFilterMRC,arrColMRC);
                    if(searchSOsMRC!=null)
                    {
                        for ( var i = 0;searchSOsMRC  != null && i < searchSOsMRC.length; i++ ) {
                            var searchSOMRC = searchSOsMRC [i];
                            var columns = searchSOMRC.getAllColumns();
                            rateSumMRC +=parseFloat(searchSOMRC.getValue(columns[0]));
                        }

                    }
                    var incremental_mrc=parseFloat(rateSum)-parseFloat(rateSumMRC);
                    if(parseFloat(incremental_mrc) > parseFloat(0))
                    {
                        recOppty.setFieldValue('custbody_cologix_opp_incremental_mrc', incremental_mrc);
                    }
                    recOppty.setFieldValue('location', arrLocation[0]);
                }

                var opptyidNew = nlapiSubmitRecord(recOppty, false, true);

                var totals = clgx_transaction_totals (customerid, 'oppty', opptyidNew);

                // put this oppty / SOs in processing queue to update back the SOs on a schedule
                for ( var i = 0;  i < arrSOs.length; i++ ) {
                    var record = nlapiCreateRecord('customrecord_clgx_que_renew_sos_oppty');
                    record.setFieldValue('custrecord_clgx_que_renew_oppty_customer', customerid);
                    record.setFieldValue('custrecord_clgx_que_renew_oppty_oppty', opptyidNew);
                    record.setFieldValue('custrecord_clgx_que_renew_oppty_so', arrSOs[i]);
                    var idRec = nlapiSubmitRecord(record, true,true);
                }

                // display the new created opportunity
                var arrOpportunity = getOpportunityJSON (customerid,opptyidNew);
                var objFile = nlapiLoadFile(999899);
                var html = objFile.getValue();
                html = html.replace(new RegExp('{opptyRec}','g'),arrOpportunity[0]);
                html = html.replace(new RegExp('{dataItems}','g'),arrOpportunity[1]);
                html = html.replace(new RegExp('{dataContacts}','g'),arrOpportunity[2]);
                
                 // after opportunity was created or updated, redirect script to itself to display the opportunity in case not enough points
                 //var objFile = nlapiLoadFile(1025828);
                 //var html = objFile.getValue();
                 //html = html.replace(new RegExp('{redirectURL}','g'),'/app/site/hosting/restlet.nl?script=265&deploy=1&customerid=' + customerid);
                 
            }

        }

        return html;

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function getOpportunityJSON (customerid,opptyid) {

    var recOppty = nlapiLoadRecord('opportunity', opptyid);

    // build opportunity record JSON
    var objOppty = new Object();
    var objData = new Object();
    objData["customerid"] = parseInt(recOppty.getFieldValue('entity'));
    objData["customer"] = recOppty.getFieldText('entity');
    objData["opptyid"] = parseInt(opptyid);
    objData["opportunity"] = recOppty.getFieldValue('tranid');
    objData["salesrepid"] = parseInt(recOppty.getFieldValue('salesrep'));
    objData["salesrep"] = recOppty.getFieldText('salesrep');
    objData["status"] = recOppty.getFieldValue('status');
    objData["probability"] = recOppty.getFieldValue('probability');
    objData["title"] = recOppty.getFieldValue('title');
    //objData["closedate"] = recOppty.getFieldValue('expectedclosedate');
    objData["closedate"] = '';
    objData["saletype"] = recOppty.getFieldValue('custbody_cologix_opp_sale_type');
    objData["leadsource"] = parseInt(recOppty.getFieldValue('leadsource'));
    objData["forecasttype"] = parseInt(recOppty.getFieldValue('forecasttype'));
    objOppty["data"] = objData;

    // build items grid JSON
    var nbrItemsOppty = recOppty.getLineItemCount('item');
    var arrItems = new Array();
    for (var j = 0; j < nbrItemsOppty; j++){

        var objItem = new Object();
        objItem["customerid"] = parseInt(customerid);
        objItem["opptyid"] = parseInt(opptyid);
        objItem["lineid"] = j + 1;
        objItem["itemid"] = parseInt(recOppty.getLineItemValue('item','item', j + 1));
        objItem["item"] = recOppty.getLineItemText('item','item', j + 1);
        objItem["serviceid"] = parseInt(recOppty.getLineItemValue('item','custcol_clgx_oppty_col_service_id', j + 1));
        objItem["service"] = recOppty.getLineItemText('item','custcol_clgx_oppty_col_service_id', j + 1);
        objItem["qty2print"] = parseFloat(recOppty.getLineItemValue('item','custcol_clgx_qty2print', j + 1));
        objItem["quantity"] = parseFloat(recOppty.getLineItemValue('item','quantity', j + 1));
        objItem["rate"] = parseFloat(recOppty.getLineItemValue('item','rate', j + 1));
        objItem["amount"] = parseFloat(recOppty.getLineItemValue('item','amount', j + 1));
        objItem["locationid"] = parseInt(recOppty.getLineItemValue('item','location', j + 1));
        objItem["location"] = recOppty.getLineItemText('item','location', j + 1);
        objItem["billschedid"] = parseInt(recOppty.getLineItemValue('item','custcol_clgx_oppty_billing_schedule', j + 1));
        objItem["billsched"] = recOppty.getLineItemText('item','custcol_clgx_oppty_billing_schedule', j + 1);
        objItem["description"] = recOppty.getLineItemValue('item','description', j + 1);
        objItem["categoryid"] = parseInt(recOppty.getLineItemValue('item','custcol_cologix_oppty_item_category', j + 1));
        objItem["category"] = recOppty.getLineItemText('item','custcol_cologix_oppty_item_category', j + 1);
        objItem["classid"] = parseInt(recOppty.getLineItemValue('item','class', j + 1));
        objItem["classname"] = recOppty.getLineItemText('item','class', j + 1);
        objItem["taxcodeid"] = parseInt(recOppty.getLineItemValue('item','taxcode', j + 1));
        objItem["taxcode"] = recOppty.getLineItemText('item','taxcode', j + 1);
        arrItems.push(objItem);
    }

    // build contacts grid JSON
    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid', 'contact', null));
    arrColumns.push(new nlobjSearchColumn('entityid', 'contact', null));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter('internalid',null,'is',customerid));
    arrFilters.push(new nlobjSearchFilter('isinactive','contact','is','F'));
    var searchContacts = nlapiSearchRecord('customer', null, arrFilters, arrColumns);

    var arrContacts = new Array();
    for ( var k = 0; searchContacts != null && k < searchContacts.length; k++ ) {
        var searchContact = searchContacts[k];
        var contactid = searchContact.getValue('internalid','contact', null);
        var contact = searchContact.getValue('entityid','contact', null);

        var objContact = new Object();
        objContact["value"] = parseInt(contactid);
        objContact["text"] = contact;
        arrContacts.push(objContact);
    }

    // put all 3 JSONs on one return
    var arrOppty = new Array;
    arrOppty.push(JSON.stringify(objOppty));
    arrOppty.push(JSON.stringify(arrItems));
    arrOppty.push(JSON.stringify(arrContacts));
    return arrOppty;
}


//check if value is in the array
function inArray(val, arr){
    var bIsValueFound = false;
    for(var i = 0; i < arr.length; i++){
        if(val == arr[i]){
            bIsValueFound = true;
            break;
        }
    }
    return bIsValueFound;
}
