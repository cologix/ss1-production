nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_SO_Renewals_Proposal.js
//	Script Name:	CLGX_SL_SO_Renewals_Proposal
//	Script Id:		customscript_clgx_sl_so_renewals_proposa
//	Script Nbr:		261
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		02/04/2014
//	RelativeURL:	/app/site/hosting/scriptlet.nl?script=261&deploy=1
//-------------------------------------------------------------------------------------------------

function suitelet_so_renewals_proposal (request, response){
    try {
        var customerid = request.getParameter('customerid');
        var opptyid = request.getParameter('opptyid');

        // save any change to the opportunity first
        var recOppty = nlapiLoadRecord('opportunity', opptyid);
        var arrSOs = recOppty.getFieldValues('custbody_clgx_renewed_from_sos');
        var arrSOsNames = recOppty.getFieldTexts('custbody_clgx_renewed_from_sos');

        recOppty.setFieldValue('title', request.getParameter('title'));
        recOppty.setFieldValue('expectedclosedate', request.getParameter('closedate'));
        recOppty.setFieldValue('leadsource', request.getParameter('leadsource'));
        recOppty.setFieldValue('forecasttype', request.getParameter('forecasttype'));
        recOppty.setFieldValue('custbody_clgx_renewed_oppty_no_prop', 0);
        nlapiSubmitRecord(recOppty, false, true);

        var recProposal = nlapiCreateRecord('estimate');
        recProposal.setFieldValue('entity', customerid);
        recProposal.setFieldValue('opportunity', opptyid);
        recProposal.setFieldValue('title', recOppty.getFieldValue('title'));
        //recProposal.setFieldText('custbody_cologix_opp_sale_type', 'Renewal');
        //recProposal.setFieldValue('status', 'Qualified');
        recProposal.setFieldValue('probability', recOppty.getFieldValue('probability'));
        recProposal.setFieldValue('expectedclosedate', recOppty.getFieldValue('expectedclosedate'));
        recProposal.setFieldText('leadsource', recOppty.getFieldValue('leadsource'));
        recProposal.setFieldValue('forecasttype', recOppty.getFieldValue('forecasttype'));
        recProposal.setFieldValue('salesrep', recOppty.getFieldValue('salesrep'));
        recProposal.setFieldValue('location', recOppty.getFieldValue('location'));
        recProposal.setFieldValues('custbody_clgx_renewed_from_sos', arrSOs);
        recProposal.setFieldValue('custbody_clgx_prop_incremental_mrc', recOppty.getFieldValue('custbody_cologix_opp_incremental_mrc'));
        recProposal.setFieldValue('custbody_clgx_contract_terms_notes', ' ');

        var language = nlapiLookupField('customer', customerid,'language');
        if(language == 'en_US' || language == 'en'){
            recProposal.setFieldValue('custbody_clgx_contract_terms_title', 13);
            var termsBody = nlapiLookupField('customrecord_clgx_contract_terms', 13, 'custrecord_clgx_contract_terms_body');
            recProposal.setFieldValue('custbody_clgx_contract_terms_body', termsBody);
        }
        else{
            recProposal.setFieldValue('custbody_clgx_contract_terms_title', 14);
            var termsBody = nlapiLookupField('customrecord_clgx_contract_terms', 14, 'custrecord_clgx_contract_terms_body');
            recProposal.setFieldValue('custbody_clgx_contract_terms_body', termsBody);
        }

        var contactid = request.getParameter('contact');
        var contact = '';
        if(parseInt(contactid) > 0){
            var contact = nlapiLookupField('contact', contactid, 'entityid');
        }
        else{
            contactid = '';
        }
        recProposal.setFieldValue('custbody_clgx_contract_terms_contact', contactid);
        recProposal.setFieldValue('custbody_clgx_contract_terms_attention', contact);

        /*
         var txtNotes = '';
         if(arrSOs.length > 0){
         txtNotes += 'This order is replacing the following Service Orders: ';
         for (var i = 0; i < arrSOs.length; i++){
         txtNotes += arrSOsNames[i] + ',';
         }
         }
         recProposal.setFieldValue('custbody_clgx_contract_terms_notes', txtNotes);
         */

        var nbrItems = recOppty.getLineItemCount('item');
        for (var i = 0; i < nbrItems; i++){
            recProposal.selectNewLineItem('item');
            recProposal.setCurrentLineItemValue('item','custcol_clgx_so_col_service_id', recOppty.getLineItemValue('item','custcol_clgx_oppty_col_service_id', i + 1));
            recProposal.setCurrentLineItemValue('item','billingschedule', recOppty.getLineItemValue('item','custcol_clgx_oppty_billing_schedule', i + 1));
            recProposal.setCurrentLineItemValue('item','item', recOppty.getLineItemValue('item','item', i + 1));
            recProposal.setCurrentLineItemValue('item','quantity', recOppty.getLineItemValue('item','quantity', i + 1));
            recProposal.setCurrentLineItemValue('item','description', recOppty.getLineItemValue('item','description', i + 1));
            recProposal.setCurrentLineItemValue('item','price', recOppty.getLineItemValue('item','price', i + 1));
            recProposal.setCurrentLineItemValue('item','rate', recOppty.getLineItemValue('item','rate', i + 1));
            recProposal.setCurrentLineItemValue('item','altsalesamt', recOppty.getLineItemValue('item','altsalesamt', i + 1));
            recProposal.setCurrentLineItemValue('item','taxcode', recOppty.getLineItemValue('item','custcol_cologix_oppty_item_taxcode', i + 1));
            recProposal.setCurrentLineItemValue('item','options', recOppty.getLineItemValue('item','options', i + 1));
            recProposal.setCurrentLineItemValue('item','costestimatetype', recOppty.getLineItemValue('item','costestimatetype', i + 1));
            recProposal.setCurrentLineItemValue('item','location', recOppty.getLineItemValue('item','location', i + 1));
            recProposal.setCurrentLineItemValue('item','class', recOppty.getLineItemValue('item','class', i + 1));
            recProposal.setCurrentLineItemValue('item','custcol_cologix_invoice_item_category', recOppty.getLineItemValue('item','custcol_cologix_oppty_item_category', i + 1));
            recProposal.commitLineItem('item');
        }
        var proposalid = nlapiSubmitRecord(recProposal, false, true);

        var totals = clgx_transaction_totals (customerid, 'proposal', proposalid);

        // put this proposal / SOs in processing queue to update back the SOs on a schedule
        //for ( var i = 0;  i < arrSOs.length; i++ ) {
        //    var record = nlapiCreateRecord('customrecord_clgx_que_renew_sos_proposal'); // 2Points
        //    record.setFieldValue('custrecord_clgx_que_renew_prop_customer', customerid);
        //    record.setFieldValue('custrecord_clgx_que_renew_prop_prop', proposalid);
        //    record.setFieldValue('custrecord_clgx_que_renew_prop_so', arrSOs[i]);
        //    var idRec = nlapiSubmitRecord(record, true,true); // 4Points
        //}

        nlapiScheduleScript(1834, null, { custscript_clgx_1823_customerid: customerid, custscript_clgx_1823_proposalid: proposalid, custscript_clgx_1823_sos: arrSOs });
        
        // load transaction map
        var custname = nlapiLookupField('customer', customerid, 'entityid');
        var objFile = nlapiLoadFile(1019671);
        var html = objFile.getValue();
        html = html.replace(new RegExp('{transactionsJSON}','g'), getTransactionsJSON2(opptyid));
        html = html.replace(new RegExp('{custName}','g'),custname.replace(new RegExp("'", "g"), "&apos;"));

        response.write( html );

    }
    catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
        if (error.getDetails != undefined){
            nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
            throw error;
        }
        else{
            nlapiLogExecution('ERROR','Unexpected Error', error.toString());
            throw nlapiCreateError('99999', error.toString());
        }
    } // End Catch Errors Section ------------------------------------------------------------------------------------------
}


function getTransactionsJSON2(opportunityID) {
	var finalObject = new Object();
	
	var proposalObject      = buildProposalObject(opportunityID);
	var salesOrders         = buildSalesOrderObject(proposalObject);
	var promotedSalesOrders = buildNewSalesOrderObject(proposalObject);
	
	//combine proposals and sales orders
	var proposalCount = proposalObject.length;
	for(var pro = 0; pro < proposalCount; pro++) {
		var createdFromSOs = new Object();
		createdFromSOs["entityid"] = 1;
		createdFromSOs["entity"]   = "Created From SOs";
		createdFromSOs["expanded"] = false;
		createdFromSOs["iconCls"]  = "fromsos";
		createdFromSOs["leaf"]     = false;
		createdFromSOs["children"] = _.filter(salesOrders, { "proposalid": proposalObject[pro].entityid });
		
		proposalObject[pro].children.push(createdFromSOs);
		
		if(promotedSalesOrders && promotedSalesOrders.length > 0) {
			var promotedSOs = new Object();
			promotedSOs["entityid"] = 1;
			promotedSOs["entity"]   = "Promoted To SOs";
			promotedSOs["expanded"] = false;
			promotedSOs["iconCls"]  = "tosos";
			promotedSOs["leaf"]     = false;
			promotedSOs["children"] = _.filter(promotedSalesOrders, { "proposalid": proposalObject[pro].entityid });
			proposalObject[pro].children.push(promotedSOs);
		}
	}
	
	
    var opportunityNumber   = nlapiLookupField("opportunity", opportunityID, "tranid");
    
	var opportunityObject = new Object();
	opportunityObject["entityid"] = opportunityID;
	opportunityObject["entity"]   = 'Opportunity #' + opportunityNumber;
	opportunityObject["expanded"] = true;
	opportunityObject["iconCls"]  = 'oppty';
	opportunityObject["leaf"]     = false;
	opportunityObject["children"] = proposalObject;
	
	
	
	//=================================
	finalObject["text"]     = ".";
	finalObject["children"] = [opportunityObject];
	
	return JSON.stringify(finalObject);
}


function buildProposalObject(opportunityID) {
	var proposals = new Array();
	
	var arrColumns = new Array();
	arrColumns.push(new nlobjSearchColumn("internalid", null, "GROUP"));
	
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("opportunity",null,"anyof",opportunityID));
	var searchProposals = nlapiSearchRecord("estimate", "customsearch_clgx_so_renewals_proposal_2", arrFilters, arrColumns);
	
	for ( var i = 0; searchProposals != null && i < searchProposals.length; i++ ) {
		var searchProposal        = searchProposals[i];
		var searchProposalColumns = searchProposal.getAllColumns();
		
		var proposalid      = searchProposal.getValue("internalid", null, "GROUP");
		var salesOrders     = searchProposal.getValue("custbody_clgx_renewed_from_sos", null, "GROUP"); 
		var salesOrderCount = 0; 
		
		if(salesOrders) {
			salesOrders     = salesOrders.split(",");
			salesOrderCount = salesOrders.length;
		}
		
		
		var bfileurl = '#'
		var bfileid = searchProposal.getValue("custbody_clgx_prop_bq_file_id", null, "GROUP");
		if(bfileid != null && bfileid !=''){
			try {
	        	var objFile = nlapiLoadFile(bfileid);
				var bfileurl = objFile.getURL();
	        } 
	        catch (e) {
	            var str = String(e);
	            if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
	    			bfileid = 0;
	            }
	        }
		}

		var cfileurl = '#';
		var cfileid = searchProposal.getValue("custbody_clgx_prop_contract_file_id", null, "GROUP");
		if(cfileid != null && cfileid !=''){
			try {
	        	var objFile = nlapiLoadFile(cfileid);
				var cfileurl = objFile.getURL();
	        } 
	        catch (e) {
	            var str = String(e);
	            if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
	    			cfileid = 0;
	            }
	        }
		}
		
		var tmpProposal = new Object();
		tmpProposal["opportunityid"] = opportunityID;
		tmpProposal["entityid"] = proposalid;
		tmpProposal["entity"] = 'Proposal #' + searchProposal.getValue("tranid", null, "GROUP");
		tmpProposal["bfileurl"] = bfileurl;
		tmpProposal["cfileurl"] = cfileurl;
		tmpProposal["hassos"] = parseInt(salesOrderCount);
		tmpProposal["sos"] = salesOrders;
		tmpProposal["expanded"] = false;
		tmpProposal["iconCls"] = 'proposal';
		tmpProposal["leaf"] = false;
		tmpProposal["children"] = new Array();
		
		proposals.push(tmpProposal);
	}
	
	return proposals;
}


function buildSalesOrderObject(proposalObject) {
	var proposals     = _.map(proposalObject, "sos");
	var salesOrderSearchArray = new Array();
	_.forEach(proposals, function(value, key) {
		salesOrderSearchArray = _.concat(salesOrderSearchArray, value);
	});
	
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("internalid", null, "anyof", _.uniq(salesOrderSearchArray)));
	
	var salesOrders     = new Array();
	var salesOrderItems = new Array();
	var searchOrders    = nlapiSearchRecord("transaction", "customsearch_clgx_so_renewals_2_2", arrFilters, null);
	
	for(var so = 0; searchOrders != null && so < searchOrders.length; so++) {
		var searchOrder        = searchOrders[so];
		var searchOrderColumns = searchOrder.getAllColumns();
		
		var tmpSalesOrder = new Object();
		tmpSalesOrder["proposalid"] = searchOrder.getValue("custbody_clgx_so_renewed_on_proposal");
		tmpSalesOrder["entityid"]   = searchOrder.getValue('internalid');
		tmpSalesOrder["entity"]     = searchOrder.getValue('tranid');
		tmpSalesOrder["expanded"]   = false;
		tmpSalesOrder["iconCls"]    = "so";
		tmpSalesOrder["leaf"]       = false;
		tmpSalesOrder["children"]   = new Array();
		salesOrders.push(tmpSalesOrder);
		
		var tmpSalesOrderItem = new Object();
		tmpSalesOrderItem["salesorderid"] = searchOrder.getValue('internalid');
		tmpSalesOrderItem["entityid"]     = searchOrder.getValue('item');
		tmpSalesOrderItem["entity"]       = searchOrder.getText('item');
		tmpSalesOrderItem["serviceid"]    = searchOrder.getValue('custcol_clgx_so_col_service_id');
		tmpSalesOrderItem["service"]      = searchOrder.getText('custcol_clgx_so_col_service_id');
		tmpSalesOrderItem["qty2print"]    = parseFloat(searchOrder.getValue('custcol_clgx_qty2print'));
		tmpSalesOrderItem["quantity"]     = parseFloat(searchOrder.getValue('quantity'));
		tmpSalesOrderItem["rate"]         = parseFloat(searchOrder.getValue(searchOrderColumns[0]));
		tmpSalesOrderItem["amount"]       = parseFloat(searchOrder.getValue('amount'));
		tmpSalesOrderItem["locationid"]   = searchOrder.getValue('location');
		tmpSalesOrderItem["location"]     = searchOrder.getText('location');
		tmpSalesOrderItem["billschedid"]  = searchOrder.getValue('billingschedule');
		tmpSalesOrderItem["billsched"]    = searchOrder.getText('billingschedule');
		tmpSalesOrderItem["category"]     = searchOrder.getText('custcol_cologix_invoice_item_category');
		tmpSalesOrderItem["description"]  = searchOrder.getValue('memo');
		
		tmpSalesOrderItem["expanded"] = false;
		tmpSalesOrderItem["iconCls"]  = 'item';
		tmpSalesOrderItem["leaf"]     = true;
		salesOrderItems.push(tmpSalesOrderItem);
	}
	
	//Merge sales order items with sales orders
	var uniqSalesOrders = _.uniqBy(salesOrders, "entityid");
	var salesOrderCount = uniqSalesOrders.length;
	
	var uniqSalesOrderItems = _.uniq(salesOrderItems);
	
	for(var so = 0; so < salesOrderCount; so++) {
		uniqSalesOrders[so].children = _.filter(uniqSalesOrderItems, { "salesorderid": uniqSalesOrders[so].entityid });
	}
	
	return uniqSalesOrders;
}


function buildNewSalesOrderObject(proposalObject) {
	var proposals = _.map(proposalObject, "entityid");
	
	var arrFilters = new Array();
	arrFilters.push(new nlobjSearchFilter("createdfrom", null, "anyof", proposals));
	
	var salesOrders     = new Array();
	var salesOrderItems = new Array();
	var searchOrders    = nlapiSearchRecord("transaction", "customsearch_clgx_so_renewals_2_2", arrFilters, null);
	
	for(var so = 0; searchOrders != null && so < searchOrders.length; so++) {
		var searchOrder        = searchOrders[so];
		var searchOrderColumns = searchOrder.getAllColumns();
		
		var tmpSalesOrder = new Object();
		tmpSalesOrder["proposalid"] = searchOrder.getValue("custbody_clgx_so_renewed_on_proposal");
		tmpSalesOrder["entityid"]   = searchOrder.getValue('internalid');
		tmpSalesOrder["entity"]     = searchOrder.getValue('tranid');
		tmpSalesOrder["expanded"]   = false;
		tmpSalesOrder["iconCls"]    = "so";
		tmpSalesOrder["leaf"]       = false;
		tmpSalesOrder["children"]   = new Array();
		salesOrders.push(tmpSalesOrder);
		
		var tmpSalesOrderItem = new Object();
		tmpSalesOrderItem["salesorderid"] = searchOrder.getValue('internalid');
		tmpSalesOrderItem["entityid"]     = searchOrder.getValue('item');
		tmpSalesOrderItem["entity"]       = searchOrder.getText('item');
		tmpSalesOrderItem["serviceid"]    = searchOrder.getValue('custcol_clgx_so_col_service_id');
		tmpSalesOrderItem["service"]      = searchOrder.getText('custcol_clgx_so_col_service_id');
		tmpSalesOrderItem["qty2print"]    = parseFloat(searchOrder.getValue('custcol_clgx_qty2print'));
		tmpSalesOrderItem["quantity"]     = parseFloat(searchOrder.getValue('quantity'));
		tmpSalesOrderItem["rate"]         = parseFloat(searchOrder.getValue(searchOrderColumns[0]));
		tmpSalesOrderItem["amount"]       = parseFloat(searchOrder.getValue('amount'));
		tmpSalesOrderItem["locationid"]   = searchOrder.getValue('location');
		tmpSalesOrderItem["location"]     = searchOrder.getText('location');
		tmpSalesOrderItem["billschedid"]  = searchOrder.getValue('billingschedule');
		tmpSalesOrderItem["billsched"]    = searchOrder.getText('billingschedule');
		tmpSalesOrderItem["category"]     = searchOrder.getText('custcol_cologix_invoice_item_category');
		tmpSalesOrderItem["description"]  = searchOrder.getValue('memo');
		
		tmpSalesOrderItem["expanded"] = false;
		tmpSalesOrderItem["iconCls"]  = 'item';
		tmpSalesOrderItem["leaf"]     = true;
		salesOrderItems.push(tmpSalesOrderItem);
	}
	
	//Merge sales order items with sales orders
	var uniqSalesOrders = _.uniqBy(salesOrders, "entityid");
	var salesOrderCount = uniqSalesOrders.length;
	
	var uniqSalesOrderItems = _.uniq(salesOrderItems);
	
	for(var so = 0; so < salesOrderCount; so++) {
		uniqSalesOrders[so].children = _.filter(uniqSalesOrderItems, { "salesorderid": uniqSalesOrders[so].entityid });
	}
	
	return uniqSalesOrders;
}


function getTransactionsJSON(opptyid){

    var oppty = nlapiLookupField('opportunity', opptyid, 'tranid');

    var arrColumns = new Array();
    arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
    var arrFilters = new Array();
    arrFilters.push(new nlobjSearchFilter("opportunity",null,"anyof",opptyid));
    var searchProposals = nlapiSearchRecord('estimate', null, arrFilters, arrColumns);

    var objTree = new Object();
    objTree["text"] = '.';

    var arrOppty = new Array();
    objOppty = new Object();
    objOppty["entityid"] = opptyid;
    objOppty["entity"] = 'Opportunity #' + oppty;
    objOppty["expanded"] = true;
    objOppty["iconCls"] = 'oppty';
    objOppty["leaf"] = false;

    var arrProposals = new Array();
    for ( var i = 0; searchProposals != null && i < searchProposals.length; i++ ) {
        var searchProposal = searchProposals[i];
        var proposalid = searchProposal.getValue('internalid', null, 'GROUP');
        var recProposal = nlapiLoadRecord('estimate', proposalid);
        var nbrSOs = recProposal.getLineItemCount('links');

        var bfileurl = '#'
        var bfileid = recProposal.getFieldValue('custbody_clgx_prop_bq_file_id');
        if(bfileid != null && bfileid !=''){
            try {
                var objFile = nlapiLoadFile(bfileid);
                var bfileurl = objFile.getURL();
            }
            catch (e) {
                var str = String(e);
                if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
                    bfileid = 0;
                }
            }
        }

        var cfileurl = '#';
        var cfileid = recProposal.getFieldValue('custbody_clgx_prop_contract_file_id');
        if(cfileid != null && cfileid !=''){
            try {
                var objFile = nlapiLoadFile(cfileid);
                var cfileurl = objFile.getURL();
            }
            catch (e) {
                var str = String(e);
                if (str.match('INSUFFICIENT_PERMISSION') || str.match('UNEXPECTED_ERROR')) {
                    cfileid = 0;
                }
            }
        }

        objProposal = new Object();
        objProposal["entityid"] = proposalid;
        objProposal["entity"] = 'Proposal #' + recProposal.getFieldValue('tranid');
        objProposal["bfileurl"] = bfileurl;
        objProposal["cfileurl"] = cfileurl;
        objProposal["hassos"] = parseInt(nbrSOs);
        objProposal["expanded"] = false;
        objProposal["iconCls"] = 'proposal';
        objProposal["leaf"] = false;

        objFromTo = new Object();
        objFromTo["entityid"] = 1;
        objFromTo["entity"] = 'Created From SOs';
        objFromTo["expanded"] = false;
        objFromTo["iconCls"] = 'fromsos';
        objFromTo["leaf"] = false;
        var arrFromTo = new Array();

        var arrFromSOsIDs = new Array();
        var arrFromSOsNames = new Array();
        arrFromSOsIDs = recProposal.getFieldValues('custbody_clgx_renewed_from_sos');
        arrFromSOsNames = recProposal.getFieldTexts('custbody_clgx_renewed_from_sos');

        var arrFromSOs = new Array();
        for ( var j = 0; arrFromSOsIDs != null && j < arrFromSOsIDs.length; j++ ) {
            var soid = arrFromSOsIDs[j];
            var sonbr = arrFromSOsNames[j];

            var objFromSO = new Object();
            objFromSO["entityid"] = soid;
            objFromSO["entity"] = sonbr;
            objFromSO["expanded"] = false;
            objFromSO["iconCls"] = 'so';
            objFromSO["leaf"] = false;

            var arrColumns = new Array();
            arrColumns.push(new nlobjSearchColumn('item',null,null));
            arrColumns.push(new nlobjSearchColumn('custcol_clgx_so_col_service_id',null,null));
            arrColumns.push(new nlobjSearchColumn('custcol_clgx_qty2print',null,null));
            arrColumns.push(new nlobjSearchColumn('quantity',null,null));
            arrColumns.push(new nlobjSearchColumn('rate',null,null));
            arrColumns.push(new nlobjSearchColumn('amount',null,null));
            arrColumns.push(new nlobjSearchColumn('location',null,null));
            arrColumns.push(new nlobjSearchColumn('billingschedule',null,null));
            arrColumns.push(new nlobjSearchColumn('custcol_cologix_invoice_item_category',null,null));
            arrColumns.push(new nlobjSearchColumn('memo',null,null));
            var arrFilters = new Array();
            arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",soid));
            var searchItems = nlapiSearchRecord('transaction', 'customsearch_clgx_so_renewals', arrFilters, arrColumns);

            var arrItems = new Array();
            for ( var k = 0; searchItems != null && k < searchItems.length; k++ ) {
                var searchItem = searchItems[k];
                var objItem = new Object();
                objItem["entityid"] = searchItem.getValue('item', null, null);
                objItem["entity"] = searchItem.getText('item', null, null);
                objItem["serviceid"] = searchItem.getValue('custcol_clgx_so_col_service_id', null, null);
                objItem["service"] = searchItem.getText('custcol_clgx_so_col_service_id', null, null);
                objItem["qty2print"] = parseFloat(searchItem.getValue('custcol_clgx_qty2print', null, null));
                objItem["quantity"] = parseFloat(searchItem.getValue('quantity', null, null));
                objItem["rate"] = parseFloat(searchItem.getValue('rate', null, null));
                objItem["amount"] = parseFloat(searchItem.getValue('amount', null, null));
                objItem["locationid"] = searchItem.getValue('location', null, null);
                objItem["location"] = searchItem.getText('location', null, null);
                objItem["billschedid"] = searchItem.getValue('billingschedule', null, null);
                objItem["billsched"] = searchItem.getText('billingschedule', null, null);
                objItem["category"] = searchItem.getText('custcol_cologix_invoice_item_category', null, null);
                objItem["description"] = searchItem.getValue('memo',null, null);

                objItem["expanded"] = false;
                objItem["iconCls"] = 'item';
                objItem["leaf"] = true;
                arrItems.push(objItem);
            }
            objFromSO["children"] = arrItems;
            arrFromSOs.push(objFromSO);
        }
        objFromTo["children"] = arrFromSOs;
        arrFromTo.push(objFromTo);

        var arrColumns = new Array();
        arrColumns.push(new nlobjSearchColumn('internalid',null,'GROUP'));
        arrColumns.push(new nlobjSearchColumn('tranid',null,'GROUP'));
        var arrFilters = new Array();
        arrFilters.push(new nlobjSearchFilter("createdfrom",null,"anyof",proposalid));
        var searchToSOs = nlapiSearchRecord('salesorder', null, arrFilters, arrColumns);

        if(searchToSOs != null){

            objToSOs = new Object();
            objToSOs["entityid"] = 1;
            objToSOs["entity"] = 'Promoted To SOs';
            objToSOs["expanded"] = false;
            objToSOs["iconCls"] = 'tosos';
            objToSOs["leaf"] = false;

            var arrToSOs = new Array();
            for ( var l = 0; searchToSOs != null && l < searchToSOs.length; l++ ) {
                var searchToSO = searchToSOs[l];
                var soid = searchToSO.getValue('internalid', null, 'GROUP');
                var sonbr = searchToSO.getValue('tranid', null, 'GROUP');

                var objToSO = new Object();
                objToSO["entityid"] = soid;
                objToSO["entity"] = sonbr;
                objToSO["expanded"] = false;
                objToSO["iconCls"] = 'so';
                objToSO["leaf"] = false;

                var arrColumns = new Array();
                arrColumns.push(new nlobjSearchColumn('item',null,null));
                arrColumns.push(new nlobjSearchColumn('custcol_clgx_so_col_service_id',null,null));
                arrColumns.push(new nlobjSearchColumn('custcol_clgx_qty2print',null,null));
                arrColumns.push(new nlobjSearchColumn('quantity',null,null));
                arrColumns.push(new nlobjSearchColumn('rate',null,null));
                arrColumns.push(new nlobjSearchColumn('amount',null,null));
                arrColumns.push(new nlobjSearchColumn('location',null,null));
                arrColumns.push(new nlobjSearchColumn('billingschedule',null,null));
                arrColumns.push(new nlobjSearchColumn('custcol_cologix_invoice_item_category',null,null));
                arrColumns.push(new nlobjSearchColumn('memo',null,null));
                var arrFilters = new Array();
                arrFilters.push(new nlobjSearchFilter("internalid",null,"anyof",soid));
                var searchItems = nlapiSearchRecord('transaction', 'customsearch_clgx_so_renewals_to', arrFilters, arrColumns);

                var arrItems = new Array();
                for ( var k = 0; searchItems != null && k < searchItems.length; k++ ) {
                    var searchItem = searchItems[k];
                    var objItem = new Object();
                    objItem["entityid"] = searchItem.getValue('item', null, null);
                    objItem["entity"] = searchItem.getText('item', null, null);
                    objItem["serviceid"] = searchItem.getValue('custcol_clgx_so_col_service_id', null, null);
                    objItem["service"] = searchItem.getText('custcol_clgx_so_col_service_id', null, null);
                    objItem["qty2print"] = parseFloat(searchItem.getValue('custcol_clgx_qty2print', null, null));
                    objItem["quantity"] = parseFloat(searchItem.getValue('quantity', null, null));
                    objItem["rate"] = parseFloat(searchItem.getValue('rate', null, null));
                    objItem["amount"] = parseFloat(searchItem.getValue('amount', null, null));
                    objItem["locationid"] = searchItem.getValue('location', null, null);
                    objItem["location"] = searchItem.getText('location', null, null);
                    objItem["billschedid"] = searchItem.getValue('billingschedule', null, null);
                    objItem["billsched"] = searchItem.getText('billingschedule', null, null);
                    objItem["category"] = searchItem.getText('custcol_cologix_invoice_item_category', null, null);
                    objItem["description"] = searchItem.getValue('memo',null, null);

                    objItem["expanded"] = false;
                    objItem["iconCls"] = 'item';
                    objItem["leaf"] = true;
                    arrItems.push(objItem);
                }
                objToSO["children"] = arrItems;
                arrToSOs.push(objToSO);
            }
            objToSOs["children"] = arrToSOs;

            arrFromTo.push(objToSOs);
        }
        objProposal["children"] = arrFromTo;
        arrProposals.push(objProposal);
    }
    objOppty["children"] = arrProposals;
    arrOppty.push(objOppty);
    objTree["children"] = arrOppty;

    return JSON.stringify(objTree);
}