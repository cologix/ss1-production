nlapiLogExecution("audit","FLOStart",new Date().getTime());
//-------------------------------------------------------------------------------------------------
//	Script File:	CLGX_SL_SO_Renewals_Box.js
//	Script Name:	CLGX_SL_SO_Renewals_Box
//	Script Id:		customscript_clgx_sl_so_renewals_box
//	Script Runs:	On Server
//	Script Type:	Suitelet
//	@authors:		Dan Tansanu - dan.tansanu@cologix.com
//	Created:		03/27/2014
//-------------------------------------------------------------------------------------------------

function suitelet_so_renewals_box (request, response){
	try {
		
		var proposalid = request.getParameter('proposalid');
		if(proposalid > 0){
			
			var myCustomerID = nlapiLookupField('estimate', proposalid, 'entity');
			var myCustomerRecord = nlapiLoadRecord('customer', myCustomerID);
			record_email = encodeURIComponent(myCustomerRecord.getFieldValue('email'));
			record_name = encodeURIComponent(myCustomerRecord.getFieldValue('companyname'));
			record_id = encodeURIComponent(myCustomerID);
			record_type = encodeURIComponent(myCustomerRecord.getRecordType());
			
			var sso_name = 'customssoboxlive';
			var context = nlapiGetContext();
			var environment = context.getEnvironment();
			var context_email = context.getEmail(); 
			var role_center = context.getRoleCenter();
			
			var url = nlapiResolveURL('SUITELET', 'customscriptboxsuiteletlive','customdeployboxsuiteletlive', null) + '&record_id=' + record_id + '&record_type=' + record_type + '&record_email=' + record_email + '&record_name=' + record_name + '&role=' + nlapiGetRole() + '&sso_name=' + sso_name + '&environment=' + environment;
			
			if (role_center == 'CUSTOMER' || role_center == 'PARTNERCENTER')
			{
				url += '&context_email=' + context_email;
			}
			
			var html = '<script type="text/javascript">';
			/* cross-browser function to get position */
			html += 'function get_position(obj) { var curtop = 0; if (obj.offsetParent) { do { curtop += obj.offsetTop; } while (obj = obj.offsetParent); } return curtop; }';
			html += 'function set_height() {if (typeof window.innerWidth != "undefined") { var viewport_height = window.innerHeight; } else if (typeof document.documentElement != "undefined" && typeof document.documentElement.clientWidth != "undefined" && document.documentElement.clientWidth != 0) { var viewport_height = document.documentElement.clientHeight; } else { var viewport_height = document.body.clientHeight; } var frame = document.getElementById("boxnet_widget_frame"); var frame_pos = get_position(frame); if (viewport_height - frame_pos > 200) { frame.style.height = viewport_height - frame_pos; } else { frame.style.height = 200; }}';
			html += 'var frame = document.getElementById("custpage_boxnet_frame_val");';
			html += 'var frame_table = frame.parentNode.parentNode.parentNode.parentNode;';
			html += 'var tab_div = document.getElementById("custpage_boxnet_tab_div");';
			html += 'frame_table.setAttribute("width", "100%");';
			html += 'frame_table.setAttribute("cellSpacing", "0");';
			
			html += 'if ( typeof window.addEventListener != "undefined" ){ window.addEventListener( "load", set_height, false ); window.addEventListener( "resize", set_height, false ); }';
			html += 'else { window.attachEvent("onresize", set_height); window.attachEvent("onload", set_height); }';
			html += '</script>';
			
			html += '<iframe id="boxnet_widget_frame" src="' + url + '" align="center" style="width: 99%; height:97%; margin:0; border:0; padding:0" frameborder="0"></iframe>';
		}
		else{
			var html = 'Please select a proposal from the left panel.';
		}
		response.write( html );
		
	} 
	catch (error) { // Start Catch Errors Section --------------------------------------------------------------------------
		if (error.getDetails != undefined){
		    nlapiLogExecution('ERROR','Process Error', error.getCode() + ': ' + error.getDetails());
		    throw error;
		}
		else{
		    nlapiLogExecution('ERROR','Unexpected Error', error.toString());
		    throw nlapiCreateError('99999', error.toString());
		}
	} // End Catch Errors Section ------------------------------------------------------------------------------------------
}
